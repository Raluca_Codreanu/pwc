﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealDateTime
{
    /// <summary>
    /// The application's entry point class.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// The application's entry point method.
        /// </summary>
        /// <param name="args">The command line arguments..</param>
        private static void Main(string[] args)
        {
            if (args.Length == 1 && args[0].Trim() == "/?")
            {
                Console.WriteLine();
                Console.WriteLine("This is an app that outputs the current date and time in a specified format.");
                Console.WriteLine("The date and time format must be supplied as the 1st and only argument.");
                Console.WriteLine();
                Console.WriteLine("A reference for the Date and Time format strings can be found at http://msdn.microsoft.com/en-us/library/az4se3k1");

                Environment.Exit((int)ExitCode.Success);
            }

            if (args.Length != 1 || string.IsNullOrWhiteSpace(args[0]))
            {
                Console.WriteLine("Invalid date/time format string argument. You can find a reference for the Date and Time format strings at http://msdn.microsoft.com/en-us/library/az4se3k1");
                Console.WriteLine();
                Environment.Exit((int)ExitCode.InvalidArgument);
            }

            try
            {
                var formattedDateTime = DateTime.Now.ToString(args[0], System.Globalization.CultureInfo.CurrentCulture);
                Console.Write(formattedDateTime);
            }
            catch (FormatException)
            {
                Environment.Exit((int)ExitCode.InvalidFormatString);
            }
        }

        /// <summary>
        /// Application exit codes.
        /// </summary>
        private enum ExitCode
        {
            /// <summary>
            /// The application exited successfully.
            /// </summary>
            Success = 0,

            /// <summary>
            /// An unspecified error occurred.
            /// </summary>
            Error = 1,

            /// <summary>
            /// The argument provided to the application was not valid.
            /// </summary>
            InvalidArgument = 2,

            /// <summary>
            /// The format string provided to the application was not valid.
            /// </summary>
            InvalidFormatString = 3
        }
    }
}
