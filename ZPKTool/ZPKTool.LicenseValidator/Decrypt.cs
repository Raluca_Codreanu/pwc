﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace ZPKTool.LicenseValidator
{
    /// <summary>
    /// Encrypts and decrypts a string.
    /// </summary>
    public static class Decrypt
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Decrypts the string using a chaining mode called cipher block chaining (CBC), 
        /// which requires a key (Key) and an initialization vector (IV) to perform 
        /// cryptographic transformations on data. To decrypt data that was encrypted 
        /// using one of the SymmetricAlgorithm classes, you must set the Key property 
        /// and the IV property to the same values that were used for encryption. 
        /// For a symmetric algorithm to be useful, the secret key must be known only 
        /// to the sender and the receiver.
        /// </summary>
        /// <param name="encryptedText">The encrypted text.</param>
        /// <param name="rgbIV">The RGB IV used for encryption.</param>
        /// <param name="key">The key used for encryption.</param>
        /// <returns>The decrypted string.</returns>       
        public static string DecryptString(string encryptedText, string rgbIV, string key)
        {
            if (string.IsNullOrWhiteSpace(encryptedText))
            {
                throw new ArgumentNullException("encryptedText", "encryptedText parameter is null");
            }

            if (string.IsNullOrWhiteSpace(rgbIV))
            {
                throw new ArgumentNullException("rgbIV", "rgbIV parameter is null");
            }

            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentNullException("key", "key parameter is null");
            }

            try
            {
                byte[] encryptedTextBytes = Convert.FromBase64String(encryptedText);

                SymmetricAlgorithm rijn = SymmetricAlgorithm.Create();
                byte[] rgbIVBytes = Encoding.ASCII.GetBytes(rgbIV);
                byte[] keyBytes = Encoding.ASCII.GetBytes(key);
                var decryptor = rijn.CreateDecryptor(keyBytes, rgbIVBytes);

                MemoryStream ms = new MemoryStream();
                using (CryptoStream cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Write))
                {
                    cs.Write(encryptedTextBytes, 0, encryptedTextBytes.Length);
                    cs.Close();
                }

                return Encoding.UTF8.GetString(ms.ToArray());
            }
            catch (EncoderFallbackException ex)
            {
                log.ErrorException("EncoderFallbackException encountered while trying to get the bytes from source text.", ex);
                throw new LicenseException(LicenseErrorCodes.InternalError, ex);
            }            
            catch (FormatException ex)
            {
                // encryptedText was not a base64 string.
                log.WarnException("The encrypted text was not a base64 string.", ex);
                throw new LicenseException(LicenseErrorCodes.InvalidLicenseFile, ex);
            }
        }
    }
}
