﻿using System;
using System.Runtime.Serialization;
using ZPKTool.Common;

namespace ZPKTool.LicenseValidator
{
    /// <summary>
    /// The exception thrown by the license validation.
    /// </summary>
    [Serializable]
    public class LicenseException : ZPKException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LicenseException"/> class.
        /// </summary>
        public LicenseException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LicenseException"/> class.
        /// </summary>
        /// <param name="errorCode">The error code.</param>
        public LicenseException(string errorCode)
            : base(errorCode)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LicenseException"/> class.
        /// </summary>
        /// <param name="errorCode">The error code.</param>
        /// <param name="ex">The ex.</param>
        public LicenseException(string errorCode, System.Exception ex)
            : base(errorCode, ex)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LicenseException"/> class.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the exception being thrown.</param>
        /// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the source or destination.</param>
        protected LicenseException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
