﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Security.Cryptography.Xml;
using System.Threading;
using System.Timers;
using System.Xml;

namespace ZPKTool.LicenseValidator
{
    /// <summary>
    /// Implements the validation of the application's license.
    /// </summary>    
    public class LicenseValidation
    {
        #region Attributes

        /// <summary>
        /// The Rgb IV key used for encryption/decryption.
        /// </summary>
        private const string EncryptionRgbIV = "ruojvlzmdalyglrj";

        /// <summary>
        /// The Key used for encryption/decryption.
        /// </summary>
        private const string EncryptionKey = "hcxilkqbbhczfeultgbskdmaunivmfuo";

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Represents the list of NIST servers: http://tf.nist.gov/tf-cgi/servers.cgi
        /// The global address time.nist.gov is resolved to all of the server addresses below it in a round-robin sequence to equalize the load across all of the servers.
        /// The other servers should be tried only after trying time.nist.gov (in the rare case that it fails).
        /// </summary>
        private static readonly string[] timeServers = new string[]
            {
                "time.nist.gov",
                "nist1.aol-va.symmetricom.com",
                "utcnist.colorado.edu",
                "time-c.nist.gov",                
                "time-c.timefreq.bldrdoc.gov",                
             };

        /// <summary>
        /// Gets or sets the current date/time obtained from the NIST time servers.
        /// </summary>        
        private static DateTime nistDate;

        /// <summary>
        /// Timer used to validate license expiration by checking the license expiration availability each time the timer elapses.
        /// </summary>
        private static System.Timers.Timer nistDateRefreshTimer;

        /// <summary>
        /// The public Key used by the RSA Crypto Provider.
        /// </summary>
        private readonly string publicKey;

        /// <summary>
        /// The path where the license file is located.
        /// </summary>
        private readonly string licensePath;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="LicenseValidation"/> class.
        /// </summary>
        /// <param name="publicKey">The public key.</param>
        /// <param name="licensePath">The license path.</param>
        public LicenseValidation(string publicKey, string licensePath)
        {
            if (string.IsNullOrWhiteSpace(publicKey))
            {
                throw new ArgumentNullException("publicKey", "The public key was null or empty");
            }

            if (string.IsNullOrWhiteSpace(licensePath))
            {
                throw new ArgumentNullException("licensePath", "The license path can not be null");
            }

            this.publicKey = publicKey;
            this.licensePath = licensePath;
        }

        #endregion

        /// <summary>
        /// Initializes the license validation engine.
        /// </summary>        
        public static void Initialize()
        {
            nistDate = DateTime.Now;

            nistDateRefreshTimer = new System.Timers.Timer();
            nistDateRefreshTimer.Interval = new TimeSpan(0, 0, 30, 0).TotalMilliseconds;
            nistDateRefreshTimer.Elapsed += new System.Timers.ElapsedEventHandler(NistDateTimer_Elapsed);
            nistDateRefreshTimer.Enabled = true;
            nistDateRefreshTimer.Start();
        }

        /// <summary>
        /// Refreshes the current date time used internally from the internet time servers.
        /// </summary>
        public static void RefreshCurrentDateTime()
        {
            nistDate = GetNISTDate(true);
        }

        /// <summary>
        /// Checks if the license currently assigned to the application is valid for the specified version.
        /// </summary>
        /// <param name="version">The version.</param>
        /// <returns>
        /// License information object.
        /// </returns>
        /// <exception cref="LicenseException">
        /// An exception occurred during the validation.
        /// </exception>
        public LicenseInfo ValidateLicense(Version version)
        {
            // Generate the serial number in order to check if there is a primary network card in the system.
            SerialNumberManager.GenerateSerialNumber();

            var license = this.LoadLicenseContentFromFile();

            string serialNumber = license.SerialNumber.Replace("-", string.Empty);
            if (!SerialNumberManager.ValidateSerialNumber(serialNumber))
            {
                throw new LicenseException(LicenseErrorCodes.InvalidSerialNumber);
            }

            if (!this.ValidateVersion(license, version))
            {
                throw new LicenseException(LicenseErrorCodes.InvalidVersion);
            }

            if (license.ExpirationDate.Date < nistDate.Date)
            {
                log.Info("Could not validate license expiration date. Invalid expiration date: {0}.", license.ExpirationDate);
                throw new LicenseException(LicenseErrorCodes.ExpiredLicense);
            }
            else if (license.StartingDate.Date > nistDate.Date)
            {
                log.Info("Could not validate license starting date. Invalid start date: {0}.", license.StartingDate);
                var exception = new LicenseException(LicenseErrorCodes.LicenseStartsInTheFuture);
                exception.ErrorMessageFormatArgs.Add(license.StartingDate.ToShortDateString());
                throw exception;
            }

            return license;
        }

        #region Get the current date/time from NIST time servers

        /// <summary>
        /// Handles the Elapsed event of the Timer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Timers.ElapsedEventArgs"/> instance containing the event data.</param>
        private static void NistDateTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            nistDate = GetNISTDate(true);
        }

        /// <summary>
        /// Gets the NIST date : NIST Internet Time Service (ITS). 
        /// </summary>
        /// <param name="convertToLocalTime">if set to true the returned date is converted to the local time; otherwise, the returned date is in UTC time.</param>
        /// <returns>The internet valid current DateTime.</returns>
        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "It is safer to catch all exceptions than to try to identify and handle every exception that can occur during the TCP connection or while parsing the response.")]
        private static DateTime GetNISTDate(bool convertToLocalTime)
        {
            DateTime date = DateTime.Now;

            // Try each server in random order to avoid blocked requests due to too frequent request  
            foreach (var timeServer in LicenseValidation.timeServers)
            {
                try
                {
                    // Get the DateTime from current server
                    // Any exception will be catch at this level only
                    date = GetNetworkDateTime(timeServer);

                    // Convert it to the current time zone if desired  
                    if (convertToLocalTime)
                    {
                        TimeSpan offsetAmount = TimeZone.CurrentTimeZone.GetUtcOffset(date);
                        date = date + offsetAmount;
                    }

                    // Exit the loop  
                    break;
                }
                catch
                {
                    // SocketException
                    /* Do Nothing...try the next server */
                }
            }

            return date;
        }

        /// <summary>
        /// Gets the current DateTime from <paramref name="ntpServer"/>.
        /// </summary>
        /// <param name="ntpServer">The hostname of the NTP server.</param>
        /// <returns>A DateTime containing the current time.</returns>
        private static DateTime GetNetworkDateTime(string ntpServer)
        {
            DateTime date;

            // VN: 4 = NTP/SNTP version 4
            // Mode: 3 = client
            byte[] ntpData = new byte[48]; // RFC 2030 
            ntpData[0] = 27;

            using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp))
            {
                socket.SendTimeout = 500;
                socket.ReceiveTimeout = 500;

                socket.Connect(ntpServer, 123);

                // Request & Response
                socket.Send(ntpData);
                socket.Receive(ntpData);
            }

            // We read the integer part and fraction part of transmit time (see RFC-2030 - Network Time Protocol)
            byte offsetTransmitTime = 40;
            long intpart = 0;
            long fractpart = 0;

            for (int j = 0; j <= 3; j++)
            {
                intpart = (256 * intpart) + ntpData[offsetTransmitTime + j];
            }

            for (int j = 4; j <= 7; j++)
            {
                fractpart = (256 * fractpart) + ntpData[offsetTransmitTime + j];
            }

            long milliseconds = (intpart * 1000) + ((fractpart * 1000) / 0x100000000L);

            TimeSpan timeSpan = TimeSpan.FromTicks(milliseconds * TimeSpan.TicksPerMillisecond);

            date = new DateTime(1900, 1, 1);
            date += timeSpan;

            return date;
        }

        #endregion Get the current date/time from NIST time servers

        #region Validate license

        /// <summary>
        /// Loads the license content from file.
        /// </summary>
        /// <returns>True if successful, false otherwise.</returns>
        private LicenseInfo LoadLicenseContentFromFile()
        {
            if (!File.Exists(this.licensePath))
            {
                log.Warn("Could not find license file: {0}", licensePath);
                throw new LicenseException(LicenseErrorCodes.LicenseFileNotFound);
            }

            string licenseFileContent = null;
            try
            {
                licenseFileContent = File.ReadAllText(this.licensePath);
            }
            catch (Exception ex)
            {
                log.ErrorException("Error occurred while reading the license file content.", ex);
                throw new LicenseException(LicenseErrorCodes.InvalidLicenseFile, ex);
            }

            if (string.IsNullOrWhiteSpace(licenseFileContent))
            {
                throw new LicenseException(LicenseErrorCodes.InvalidLicenseFile);
            }

            try
            {
                var decryptedlicenseFileContent = Decrypt.DecryptString(licenseFileContent, EncryptionRgbIV, EncryptionKey);
                var documentContent = new XmlDocument();
                documentContent.LoadXml(decryptedlicenseFileContent);

                if (this.CheckValidLicenseFileContent(documentContent))
                {
                    return this.GetLicenseInfo(documentContent);
                }
                else
                {
                    throw new LicenseException(LicenseErrorCodes.InvalidLicenseFile);
                }
            }
            catch (XmlException e)
            {
                log.WarnException("Could not validate license", e);
                throw new LicenseException(LicenseErrorCodes.InvalidLicenseFile);
            }
        }

        /// <summary>
        /// Gets the license info.
        /// </summary>
        /// <param name="document">The document.</param>
        /// <returns>The license info.</returns>
        private LicenseInfo GetLicenseInfo(XmlDocument document)
        {
            LicenseInfo licenseInfo = new LicenseInfo();

            try
            {
                // company
                XmlNode companyName = document.SelectSingleNode("/license/company/name");
                if (companyName == null)
                {
                    log.Warn("Could not find license's company name in license.");
                    throw new LicenseException(LicenseErrorCodes.InvalidFormat);
                }

                licenseInfo.CompanyName = companyName.InnerText;

                // distributor
                XmlNode distributorName = document.SelectSingleNode("/license/distributor/name");
                if (distributorName == null)
                {
                    log.Warn("Could not find license's distributor name in license.");
                    throw new LicenseException(LicenseErrorCodes.InvalidFormat);
                }

                licenseInfo.DistributorName = distributorName.InnerText;

                XmlNode distributorPhone = document.SelectSingleNode("/license/distributor/phone");
                if (distributorPhone == null)
                {
                    log.Warn("Could not find license's distributor phone in license.");
                    throw new LicenseException(LicenseErrorCodes.InvalidFormat);
                }

                licenseInfo.DistributorPhone = distributorPhone.InnerText;

                XmlNode distributorStreet = document.SelectSingleNode("/license/distributor/street");
                if (distributorStreet == null)
                {
                    log.Warn("Could not find license's distributor street in license.");
                    throw new LicenseException(LicenseErrorCodes.InvalidFormat);
                }

                XmlNode distributorCity = document.SelectSingleNode("/license/distributor/city");
                if (distributorCity == null)
                {
                    log.Warn("Could not find license's distributor city in license.");
                    throw new LicenseException(LicenseErrorCodes.InvalidFormat);
                }

                XmlNode distributorPostCode = document.SelectSingleNode("/license/distributor/postcode");
                if (distributorPostCode == null)
                {
                    log.Warn("Could not find license's distributor postcode in license.");
                    throw new LicenseException(LicenseErrorCodes.InvalidFormat);
                }

                licenseInfo.DistributorAddress = distributorStreet.InnerText + ", " + distributorCity + ", " + distributorPostCode;

                XmlNode distributorEmail = document.SelectSingleNode("/license/distributor/email");
                if (distributorEmail == null)
                {
                    log.Warn("Could not find license's distributor email in license.");
                    throw new LicenseException(LicenseErrorCodes.InvalidFormat);
                }

                licenseInfo.DistributorEmail = distributorEmail.InnerText;

                // user
                XmlNode userName = document.SelectSingleNode("/license/user/name");
                if (userName == null)
                {
                    log.Warn("Could not find license's user name in license.");
                    throw new LicenseException(LicenseErrorCodes.InvalidFormat);
                }

                licenseInfo.UserName = userName.InnerText;

                XmlNode userPhone = document.SelectSingleNode("/license/user/phone");
                if (userPhone == null)
                {
                    log.Warn("Could not find license's user phone in license.");
                    throw new LicenseException(LicenseErrorCodes.InvalidFormat);
                }

                licenseInfo.UserPhone = userPhone.InnerText;

                XmlNode userStreet = document.SelectSingleNode("/license/user/street");
                if (userStreet == null)
                {
                    log.Warn("Could not find license's user street in license.");
                    throw new LicenseException(LicenseErrorCodes.InvalidFormat);
                }

                XmlNode userCity = document.SelectSingleNode("/license/user/city");
                if (userCity == null)
                {
                    log.Warn("Could not find license's user city in license.");
                    throw new LicenseException(LicenseErrorCodes.InvalidFormat);
                }

                XmlNode userPostCode = document.SelectSingleNode("/license/user/postcode");
                if (userPostCode == null)
                {
                    log.Warn("Could not find license's user postcode in license.");
                    throw new LicenseException(LicenseErrorCodes.InvalidFormat);
                }

                licenseInfo.UserAddress = userStreet.InnerText + ", " + userCity + ", " + userPostCode;

                XmlNode userEmail = document.SelectSingleNode("/license/user/email");
                if (userEmail == null)
                {
                    log.Warn("Could not find license's user email in license.");
                    throw new LicenseException(LicenseErrorCodes.InvalidFormat);
                }

                licenseInfo.UserEmail = userEmail.InnerText;

                // license
                XmlNode serialNumber = document.SelectSingleNode("/license/serialnumber");
                if (serialNumber == null)
                {
                    log.Warn("Could not find serial-number in license.");
                    throw new LicenseException(LicenseErrorCodes.InvalidFormat);
                }

                licenseInfo.SerialNumber = serialNumber.InnerText;

                XmlNode licenseType = document.SelectSingleNode("/license/type");
                if (licenseType == null)
                {
                    log.Warn("Could not find license type in license file.");
                    throw new LicenseException(LicenseErrorCodes.InvalidFormat);
                }

                licenseInfo.Type = (LicenseType)Enum.Parse(typeof(LicenseType), licenseType.InnerText);

                XmlNode licenseLevel = document.SelectSingleNode("/license/level");
                if (licenseLevel == null)
                {
                    log.Warn("Could not find license level in license file.");
                    throw new LicenseException(LicenseErrorCodes.InvalidFormat);
                }

                licenseInfo.Level = (LicenseLevel)Enum.Parse(typeof(LicenseLevel), licenseLevel.InnerText);

                XmlNode startingDate = document.SelectSingleNode("/license/startingDate");
                if (startingDate == null)
                {
                    log.Warn("Could not find expiration in license.");
                    throw new LicenseException(LicenseErrorCodes.InvalidFormat);
                }

                licenseInfo.StartingDate = DateTime.ParseExact(startingDate.InnerText, "yyyy-MM-dd", CultureInfo.InvariantCulture);

                XmlNode expirationdate = document.SelectSingleNode("/license/expirationDate");
                if (expirationdate == null)
                {
                    log.Warn("Could not find expiration in license.");
                    throw new LicenseException(LicenseErrorCodes.InvalidFormat);
                }

                licenseInfo.ExpirationDate = DateTime.ParseExact(expirationdate.InnerText, "yyyy-MM-dd", CultureInfo.InvariantCulture);

                XmlNode applicationVersion = document.SelectSingleNode("/license/applicationVersion");
                if (applicationVersion == null)
                {
                    log.Warn("Could not find application version in license.");
                    throw new LicenseException(LicenseErrorCodes.InvalidFormat);
                }

                licenseInfo.ApplicationVersion = applicationVersion.InnerText;

                return licenseInfo;
            }
            catch (XmlException ex)
            {
                log.ErrorException("XML Exception found while trying to validate the xml and read the license content.", ex);
                throw new LicenseException(LicenseErrorCodes.InvalidFormat);
            }
        }

        /// <summary>
        /// Tries the get valid document.
        /// </summary>
        /// <param name="doc">The document.</param>
        /// <returns>
        /// True if the document is valid, false otherwise.
        /// </returns>
        private bool CheckValidLicenseFileContent(XmlDocument doc)
        {
            if (doc == null)
            {
                throw new ArgumentNullException("doc");
            }

            try
            {
                var rsa = new RSACryptoServiceProvider();
                rsa.FromXmlString(this.publicKey);

                var namespaceManager = new XmlNamespaceManager(doc.NameTable);
                namespaceManager.AddNamespace("sig", "http://www.w3.org/2000/09/xmldsig#");

                var signedXml = new SignedXml(doc);
                var sig = (XmlElement)doc.SelectSingleNode("//sig:Signature", namespaceManager);
                if (sig == null)
                {
                    log.Warn("Could not find the signature node.");
                    return false;
                }

                signedXml.LoadXml(sig);

                return signedXml.CheckSignature(rsa);
            }
            catch (System.Security.Cryptography.CryptographicException ex)
            {
                log.ErrorException("Security Cryptographic Exception", ex);
                return false;
            }
            catch (XmlException ex)
            {
                log.ErrorException("Xml Exception", ex);
                return false;
            }
        }

        /// <summary>
        /// Checks if the specified license is valid for the specified version.
        /// </summary>
        /// <param name="license">The license.</param>
        /// <param name="version">The version.</param>
        /// <returns>
        /// True if the license is valid for the specified version; otherwise, false.
        /// </returns>
        private bool ValidateVersion(LicenseInfo license, Version version)
        {
            Version licenseFileVersion = new Version(license.ApplicationVersion);
            Version maxValidVersion = new Version(licenseFileVersion.Major, licenseFileVersion.Minor + 1);

            if (version.CompareTo(maxValidVersion) < 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion Validate license
    }
}
