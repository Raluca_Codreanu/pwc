﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Management;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.Text;

namespace ZPKTool.LicenseValidator
{
    /// <summary>
    /// Serial Number Manager, responsible for generating a serial number, encode/decode serial number.
    /// </summary>
    public static class SerialNumberManager
    {
        #region Members

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        #endregion

        /// <summary>
        /// Generates the serial number.
        /// </summary>
        /// <returns>
        /// A license serial-number.
        /// </returns>
        /// <exception cref="ZPKTool.LicenseValidator.LicenseException">A physical network card was not detected.</exception>
        public static string GenerateSerialNumber()
        {
            var macAddress = GetPrimaryNetworkAdapterMacAddress();
            if (string.IsNullOrWhiteSpace(macAddress))
            {
                throw new LicenseException(LicenseErrorCodes.NoNetworkAdapter);
            }

            return ComputeSHA1Hash(macAddress);
        }

        /// <summary>
        /// Determines whether the specified serial number is valid for the current computer.
        /// It checks the serial number against all network adapters because the adapter from which the serial was generated could
        /// no longer be the primary physical adapter (not very likely, but who knows).
        /// </summary>
        /// <param name="serialNumber">The serial number.</param>
        /// <returns>True if the serial number is valid; otherwise, false.</returns>
        /// <exception cref="LicenseException">An error occurred during the validation.</exception>
        public static bool ValidateSerialNumber(string serialNumber)
        {
            try
            {
                foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
                {
                    string identifier = ComputeSHA1Hash(nic.GetPhysicalAddress().ToString());
                    if (identifier.Equals(serialNumber, StringComparison.OrdinalIgnoreCase))
                    {
                        return true;
                    }
                }

                return false;
            }
            catch (NetworkInformationException ex)
            {
                log.ErrorException("Could not get the network interfaces.", ex);
                throw new LicenseException(LicenseErrorCodes.InternalError, ex);
            }
        }

        /// <summary>
        /// Gets the Mac address of the primary network adapter.
        /// <para />
        /// The primary network adapter is the one registered with the lowest index in the system.
        /// </summary>
        /// <returns>
        /// A string representing the Mac address of the primary network adapter or null/empty string if an error occurred while retrieving the MAC address
        /// or there is no network adapter in the system.
        /// </returns>        
        private static string GetPrimaryNetworkAdapterMacAddress()
        {
            string mac = string.Empty;

            try
            {
                // Retrieve the system's network adapters list using the management information API.
                // Query Conditions:
                //     -> PNPDeviceID not starting with "ROOT\": physical devices have PNPDeviceID starting with "PCI\" or something else besides "ROOT\"
                string query = @"SELECT * FROM Win32_NetworkAdapter WHERE NOT PNPDeviceID LIKE 'ROOT\\%'";
                using (var mos = new ManagementObjectSearcher(query))
                {
                    // Find the physical network adapters.
                    var networkAdapters = mos.Get().Cast<ManagementBaseObject>();
                    var physicalNetworkAdapters = FindPhysicalNetworkAdapters(networkAdapters);

                    // Remove all physical network adapters that do not have a MAC address.
                    physicalNetworkAdapters.RemoveAll(adapter =>
                    {
                        bool hasMAC = false;
                        var physicalAdapterProp = adapter.Properties.Cast<PropertyData>().FirstOrDefault(p => p.Name == "MACAddress");
                        if (physicalAdapterProp != null
                            && physicalAdapterProp.Value is string
                            && !string.IsNullOrEmpty((string)physicalAdapterProp.Value))
                        {
                            hasMAC = true;
                        }

                        return !hasMAC;
                    });

                    // Choose the physical network adapter with lowest value for the Index property.                                            
                    ManagementBaseObject primaryNetworkCard = null;
                    uint lowestNICIndex = uint.MaxValue;
                    foreach (ManagementBaseObject managementObject in physicalNetworkAdapters)
                    {
                        // If it will be necessary at some point, the enabled state of a network adapter can be obtained from the "NetEnabled" property.
                        var indexProperty = managementObject.Properties.Cast<PropertyData>().FirstOrDefault(p => p.Name == "Index");
                        if (indexProperty != null
                            && indexProperty.Value is uint
                            && (uint)indexProperty.Value < lowestNICIndex)
                        {
                            lowestNICIndex = (uint)indexProperty.Value;
                            primaryNetworkCard = managementObject;
                        }
                    }

                    if (primaryNetworkCard != null)
                    {
                        PropertyData macAddress = primaryNetworkCard.Properties.Cast<PropertyData>().FirstOrDefault(p => p.Name == "MACAddress");
                        if (macAddress != null && macAddress.Value != null)
                        {
                            mac = macAddress.Value.ToString().Replace(":", string.Empty);
                        }
                    }
                }
            }
            catch (ManagementException ex)
            {
                log.ErrorException("An error occurred while retrieving the primary NIC's MAC address.", ex);
            }

            return mac;
        }

        /// <summary>
        /// Finds the physical network adapters in the specified list of network adapters.
        /// </summary>
        /// <param name="networkAdapters">The network adapters.</param>
        /// <returns>A new list containing only the physical network adapters from the input list of adapters.</returns>
        private static List<ManagementBaseObject> FindPhysicalNetworkAdapters(IEnumerable<ManagementBaseObject> networkAdapters)
        {
            List<ManagementBaseObject> physicalNetworkAdapters = new List<ManagementBaseObject>(networkAdapters);

            // In Windows versions after XP, the PhysicalAdapter property indicates whether a network card is physical or not.                    
            bool supportsPhysicalAdapterProp =
                physicalNetworkAdapters.Any(adapter => adapter.Properties.Cast<PropertyData>().FirstOrDefault(p => p.Name == "PhysicalAdapter") != null);
            if (supportsPhysicalAdapterProp)
            {
                // Remove all adapters that do not have the PhysicalAdapter property set to true.
                physicalNetworkAdapters.RemoveAll(adapter =>
                {
                    bool isPhysicalAdapter = false;
                    var physicalAdapterProp = adapter.Properties.Cast<PropertyData>().FirstOrDefault(p => p.Name == "PhysicalAdapter");
                    if (physicalAdapterProp != null
                        && physicalAdapterProp.Value is bool
                        && (bool)physicalAdapterProp.Value)
                    {
                        isPhysicalAdapter = true;
                    }

                    return !isPhysicalAdapter;
                });
            }
            else
            {
                // In windows XP there is no PhysicalAdapter property, so we use the old filtering algorithm that excludes all Microsoft manufactured network cards, because there's no other way.
                // This algorithm will still fail if the app runs in a virtual machine that uses a Microsoft Virtual Network Adapter.
                physicalNetworkAdapters.RemoveAll(adapter =>
                {
                    var manufacturerProp = adapter.Properties.Cast<PropertyData>().FirstOrDefault(p => p.Name == "Manufacturer");
                    if (manufacturerProp != null)
                    {
                        var manufacturer = manufacturerProp.Value as string;
                        if (manufacturer != null && manufacturer.Equals("Microsoft", StringComparison.OrdinalIgnoreCase))
                        {
                            return true;
                        }
                    }

                    return false;
                });
            }

            return physicalNetworkAdapters;
        }

        /// <summary>
        /// Computes the SHA1 hash for the input string.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>
        /// The computed hash.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">The input parameter for creating sha1 was null.</exception>
        private static string ComputeSHA1Hash(string input)
        {
            if (input == null)
            {
                throw new ArgumentNullException("input", "The input string for creating sha1 was null.");
            }

            using (SHA1 sha = new SHA1Managed())
            {
                byte[] data = Encoding.ASCII.GetBytes(input);
                data = sha.ComputeHash(data);

                StringBuilder sb = new StringBuilder();
                foreach (byte by in data)
                {
                    sb.Append(by.ToString("x2"));
                }

                return sb.ToString();
            }
        }
    }
}
