﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.LicenseValidator
{
    /// <summary>
    /// License specific error codes.
    /// </summary>
    public static class LicenseErrorCodes
    {
        /// <summary>
        /// Internal Error.
        /// </summary>
        public const string InternalError = "Error_Internal";

        /// <summary>
        /// License file not found error code.
        /// </summary>
        public const string LicenseFileNotFound = "License_FileNotFound";

        /// <summary>
        /// The license file content is invalid.
        /// </summary>
        public const string InvalidFormat = "License_InvalidFormat";

        /// <summary>
        /// The license has expired.
        /// </summary>
        public const string ExpiredLicense = "License_LicenseExpired";

        /// <summary>
        /// The license file has an invalid serial number.
        /// </summary>
        public const string InvalidSerialNumber = "License_InvalidSerialNumber";

        /// <summary>
        /// The license file is for another version of application.
        /// </summary>
        public const string InvalidVersion = "License_InvalidVersion";

        /// <summary>
        /// A physical network adapter was not found in the system. License validation can not be performed without it.
        /// </summary>
        public const string NoNetworkAdapter = "License_NoNetworkAdapter";

        /// <summary>
        /// The the licensing period will start in the future.
        /// </summary>
        public const string LicenseStartsInTheFuture = "License_LicenseStartsInTheFuture";

        /// <summary>
        /// The file is not a valid license file.
        /// </summary>
        public const string InvalidLicenseFile = "License_InvalidLicenseFile";
    }
}
