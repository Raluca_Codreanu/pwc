﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.LicenseValidator
{
    #region License Level enum

    /// <summary>
    /// The enum represents the levels of licenses that will be available.
    /// </summary>
    public enum LicenseLevel
    {
        /// <summary>
        /// Trial license level.
        /// </summary>
        Trial = 0,

        /// <summary>
        /// Basic license level.
        /// </summary>
        Basic = 1,

        /// <summary>
        /// Professional Mech license level.
        /// </summary>
        ProfessionalMech = 2,

        /// <summary>
        /// Professional Mech Including Master Data Updates license level.
        /// </summary>
        ProfessionalMechIncMDUpdates = 3
    }

    #endregion

    #region License Type enum

    /// <summary>
    /// The enumeration represents the types of licenses that will be available.
    /// </summary>
    public enum LicenseType
    {
        /// <summary>
        /// Node locked.
        /// </summary>
        Nodelocked = 0
    }

    #endregion

    /// <summary>
    /// Contains information found in the available license.
    /// </summary>
    public class LicenseInfo
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="LicenseInfo"/> class.
        /// </summary>
        public LicenseInfo()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the name of the distributor.
        /// </summary>
        /// <value>The name of the distributor.</value>
        public string DistributorName { get; set; }

        /// <summary>
        /// Gets or sets the distributor phone.
        /// </summary>
        /// <value>The distributor phone.</value>
        public string DistributorPhone { get; set; }

        /// <summary>
        /// Gets or sets the distributor address.
        /// </summary>
        /// <value>The distributor address.</value>
        public string DistributorAddress { get; set; }

        /// <summary>
        /// Gets or sets the distributor email.
        /// </summary>
        /// <value>The distributor email.</value>
        public string DistributorEmail { get; set; }

        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>The name of the user.</value>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the user phone.
        /// </summary>
        /// <value>The user phone.</value>
        public string UserPhone { get; set; }

        /// <summary>
        /// Gets or sets the user address.
        /// </summary>
        /// <value>The user address.</value>
        public string UserAddress { get; set; }

        /// <summary>
        /// Gets or sets the user email.
        /// </summary>
        /// <value>The user email.</value>
        public string UserEmail { get; set; }

        /// <summary>
        /// Gets or sets the company information.
        /// </summary>
        /// <value>The company.</value>
        public string CompanyName { get; set; }

        /// <summary>
        /// Gets or sets the serial number information.
        /// </summary>
        /// <value>The serial number.</value>
        public string SerialNumber { get; set; }

        /// <summary>
        /// Gets or sets the application version.
        /// </summary>
        /// <value>
        /// The application version.
        /// </value>
        public string ApplicationVersion { get; set; }

        /// <summary>
        /// Gets or sets the type information.
        /// </summary>
        /// <value>The license type.</value>
        public LicenseType Type { get; set; }

        /// <summary>
        /// Gets or sets the level.
        /// </summary>
        /// <value>The level.</value>
        public LicenseLevel Level { get; set; }

        /// <summary>
        /// Gets or sets the starting date.
        /// </summary>
        /// <value>The starting date.</value>
        public DateTime StartingDate { get; set; }

        /// <summary>
        /// Gets or sets the expiration date.
        /// </summary>
        /// <value>The expiration date.</value>
        public DateTime ExpirationDate { get; set; }

        #endregion

        #region Format serial number

        /// <summary>
        /// Gets the serial number with dashes.
        /// </summary>
        /// <param name="serialNumber">The serial number.</param>
        /// <returns>
        /// The formatted serial number.
        /// </returns>
        /// <exception cref="ArgumentNullException">Serial number string is empty.</exception>         
        /// <exception cref="ArgumentException">Serial Number length isn't 40 as expected.</exception>
        public static string FormatSerialNumber(string serialNumber)
        {
            if (string.IsNullOrWhiteSpace(serialNumber))
            {
                throw new ArgumentNullException("serialNumber", "Serial number string is empty");
            }

            if (serialNumber.Length != 40)
            {
                log.Error("The serial number found in the license file content has more than 40 characters.");
                throw new LicenseException(LicenseErrorCodes.InvalidFormat);
            }

            return (serialNumber.Substring(0, 8) + "-" +
                      serialNumber.Substring(8, 8) + "-" +
                      serialNumber.Substring(16, 8) + "-" +
                      serialNumber.Substring(24, 8) + "-" +
                      serialNumber.Substring(32, 8)).ToUpperInvariant();
        }

        #endregion
    }
}
