﻿using System.Security.Cryptography;
using System.Text;

namespace ZPKTool.Data
{
    /// <summary>
    /// Generates salt using the RNG cryptographic service provider.
    /// </summary>
    public static class SaltGenerator
    {
        /// <summary>
        /// The salt size.
        /// </summary>
        private const int SaltSize = 32;

        /// <summary>
        /// The RNG cryptographic service provider.
        /// </summary>
        private static RNGCryptoServiceProvider cryptoServiceProvider = new RNGCryptoServiceProvider();

        /// <summary>
        /// Generates a salt using the RNG cryptographic service provider.
        /// </summary>
        /// <returns>The generated salt.</returns>
        public static string GenerateSalt()
        {
            var saltBytes = new byte[SaltSize];

            // Fill the salt with cryptographically strong byte values.
            cryptoServiceProvider.GetNonZeroBytes(saltBytes);

            return Encoding.UTF8.GetString(saltBytes, 0, saltBytes.Length);
        }
    }
}