/*
 * This file is used by Code Analysis to maintain SuppressMessage 
 * attributes that are applied to this project.
 * Project-level suppressions either have no target or are given 
 * a specific target and scoped to a namespace, type, member, etc.
 * 
 * To add a suppression to this file, right-click the message in the 
 * Code Analysis results, point to "Suppress Message", and click 
 * "In Suppression File".
 * You do not need to add suppressions to this file manually.
 */

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Microsoft.Performance", "CA1819:PropertiesShouldNotReturnArrays", Scope = "member",
    Target = "ZPKTool.Data.CycleTimeCalculation.#MachiningCalculationData",
    Justification = "The member is as required by Entity Framework.")]

[assembly: SuppressMessage("Microsoft.Maintainability", "CA1500:VariableNamesShouldNotMatchFieldNames", MessageId = "value", Scope = "member",
    Target = "ZPKTool.Data.DataBaseInfo.#Guid",
    Justification = "There is a conflict between the keyword 'value' used in a property setter and a class member named 'value'")]
[assembly: SuppressMessage("Microsoft.Maintainability", "CA1500:VariableNamesShouldNotMatchFieldNames", MessageId = "value", Scope = "member",
    Target = "ZPKTool.Data.DataBaseInfo.#Key",
    Justification = "There is a conflict between the keyword 'value' used in a property setter and a class member named 'value'")]
[assembly: SuppressMessage("Microsoft.Maintainability", "CA1500:VariableNamesShouldNotMatchFieldNames", MessageId = "value", Scope = "member",
    Target = "ZPKTool.Data.DataBaseInfo.#Value",
    Justification = "There is a conflict between the keyword 'value' used in a property setter and a class member named 'value'")]
[assembly: SuppressMessage("Microsoft.Maintainability", "CA1500:VariableNamesShouldNotMatchFieldNames", MessageId = "value", Scope = "member",
    Target = "ZPKTool.Data.DataBaseInfo.#PropertyChanged",
    Justification = "There is a conflict between a variable named 'value' generated for an event add/remove handler and a class member named 'value'")]
[assembly: SuppressMessage("Microsoft.Maintainability", "CA1500:VariableNamesShouldNotMatchFieldNames", MessageId = "value", Scope = "member",
    Target = "ZPKTool.Data.DataBaseInfo.#PropertyChanged",
    Justification = "There is a conflict between a variable named 'value' generated for an event add/remove handler and a class member named 'value'")]

[assembly: SuppressMessage("Microsoft.Performance", "CA1819:PropertiesShouldNotReturnArrays", Scope = "member",
    Target = "ZPKTool.Data.Media.#Content",
    Justification = "The member is as required by Entity Framework.")]
[assembly: SuppressMessage("Microsoft.Design", "CA1012:AbstractTypesShouldNotHaveConstructors", Scope = "type",
    Target = "ZPKTool.Data.ProcessStep",
    Justification = "Entity Framework requires all classes representing entities to have a public parameter-less constructor.")]

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1028:EnumStorageShouldBeInt32", Scope = "type",
    Target = "ZPKTool.Data.TrashBinItemType",
    Justification = "The underlying type of the enum is saved to the database and it would be to complicated to change it now.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1028:EnumStorageShouldBeInt32", Scope = "type",
    Target = "ZPKTool.Data.BookmarkedItemType",
    Justification = "The underlying type of the enum is saved to the database and it would be to complicated to change it now.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1028:EnumStorageShouldBeInt32", Scope = "type",
    Target = "ZPKTool.Data.DieType",
    Justification = "The underlying type of the enum is saved to the database and it would be to complicated to change it now.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1028:EnumStorageShouldBeInt32", Scope = "type",
    Target = "ZPKTool.Data.MachiningType",
    Justification = "The underlying type of the enum is saved to the database and it would be to complicated to change it now.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1028:EnumStorageShouldBeInt32", Scope = "type",
    Target = "ZPKTool.Data.MeasurementUnitType",
    Justification = "The underlying type of the enum is saved to the database and it would be to complicated to change it now.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1028:EnumStorageShouldBeInt32", Scope = "type",
    Target = "ZPKTool.Data.MediaType",
    Justification = "The underlying type of the enum is saved to the database and it would be to complicated to change it now.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1028:EnumStorageShouldBeInt32", Scope = "type",
    Target = "ZPKTool.Data.PartCalculationAccuracy",
    Justification = "The underlying type of the enum is saved to the database and it would be to complicated to change it now.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1028:EnumStorageShouldBeInt32", Scope = "type",
    Target = "ZPKTool.Data.PartCalculationApproach",
    Justification = "The underlying type of the enum is saved to the database and it would be to complicated to change it now.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1028:EnumStorageShouldBeInt32", Scope = "type",
    Target = "ZPKTool.Data.PartCalculationStatus",
    Justification = "The underlying type of the enum is saved to the database and it would be to complicated to change it now.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1028:EnumStorageShouldBeInt32", Scope = "type",
    Target = "ZPKTool.Data.PartDeliveryType",
    Justification = "The underlying type of the enum is saved to the database and it would be to complicated to change it now.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1028:EnumStorageShouldBeInt32", Scope = "type",
    Target = "ZPKTool.Data.ProcessCalculationAccuracy",
    Justification = "The underlying type of the enum is saved to the database and it would be to complicated to change it now.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1028:EnumStorageShouldBeInt32", Scope = "type",
    Target = "ZPKTool.Data.ProcessTransportCostType",
    Justification = "The underlying type of the enum is saved to the database and it would be to complicated to change it now.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1028:EnumStorageShouldBeInt32", Scope = "type",
    Target = "ZPKTool.Data.ProjectStatus",
    Justification = "The underlying type of the enum is saved to the database and it would be to complicated to change it now.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1028:EnumStorageShouldBeInt32", Scope = "type",
    Target = "ZPKTool.Data.ScrapCalculationType",
    Justification = "The underlying type of the enum is saved to the database and it would be to complicated to change it now.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1028:EnumStorageShouldBeInt32", Scope = "type",
    Target = "ZPKTool.Data.SupplierType",
    Justification = "The underlying type of the enum is saved to the database and it would be to complicated to change it now.")]
