﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Data
{
    /// <summary>
    /// Represents name types of Raw Materials.
    /// </summary>
    public enum RawMaterialLocalisedName
    {
        /// <summary>
        /// German Name - represents the default name.
        /// </summary>
        GermanName = 0,

        /// <summary>
        /// The UK name of Raw Material.
        /// </summary>
        UKName = 1,

        /// <summary>
        /// The US name of Raw Material.
        /// </summary>
        USName = 2
    }
}
