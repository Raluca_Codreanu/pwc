﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace ZPKTool.Data
{
    /// <summary>
    /// Contains the relationships graph that holds for each entity type, information about its relationships with other entities.
    /// </summary>
    public static class RelationshipsGraph
    {
        #region Constructors

        /// <summary>
        /// Initializes static members of the <see cref="RelationshipsGraph"/> class.
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "The constructor is necessary to initialize the relationships graph.")]
        static RelationshipsGraph()
        {
            EntitiesRelationshipsGraph = new Dictionary<Type, RelatedObject>();

            Initialize();
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Gets the relationships graph for all entity types.
        /// </summary>
        public static Dictionary<Type, RelatedObject> EntitiesRelationshipsGraph { get; private set; }

        #endregion Properties

        /// <summary>
        /// Initializes the relationships graph.
        /// </summary>
        private static void Initialize()
        {
            var configurations = ConfigureEntityRelationships();

            var propertiesGraph = new Dictionary<PropertyInfo, RelatedObject>();

            // Create the graph based on the configurations.
            foreach (var configPair in configurations)
            {
                var graph = CreateRelationshipsGraph(null, configPair.Key, configPair.Value, configurations, propertiesGraph);
                EntitiesRelationshipsGraph.Add(configPair.Key, graph);
            }
        }

        /// <summary>
        /// Creates the relationships graph according to the configurations.
        /// </summary>
        /// <param name="property">The property info of the related parent object property. For root objects this is null.</param>
        /// <param name="entityType">The entity type of the property. For properties that implement a generic list is the generic type, else is the property's type.</param>
        /// <param name="configuration">The relationships configuration.</param>
        /// <param name="allConfigurations">All the configurations.</param>
        /// <param name="propertiesGraph">The properties graph.</param>
        /// <returns>The object's relationships graph.</returns>
        /// <exception cref="System.NotSupportedException">The entity type relationships were not configured.</exception>
        private static RelatedObject CreateRelationshipsGraph(
            PropertyInfo property,
            Type entityType,
            RelationshipsConfiguration configuration,
            Dictionary<Type, RelationshipsConfiguration> allConfigurations,
            Dictionary<PropertyInfo, RelatedObject> propertiesGraph)
        {
            var graph = new RelatedObject(property, entityType);

            if (property != null)
            {
                propertiesGraph.Add(property, graph);
            }

            foreach (var reference in configuration.ReferencedProperties)
            {
                graph.Referenced.Add(reference);
            }

            foreach (var ownedProperty in configuration.OwnedProperties)
            {
                var ownedObjectGraph = CreateRelationshipsGraph(ownedProperty, allConfigurations, propertiesGraph);
                if (ownedObjectGraph == null)
                {
                    continue;
                }

                graph.Owned.Add(ownedObjectGraph);
            }

            foreach (var parentProperty in configuration.ParentProperties)
            {
                var parentObjectGraph = CreateRelationshipsGraph(parentProperty, allConfigurations, propertiesGraph);
                if (parentObjectGraph == null)
                {
                    continue;
                }

                graph.HasAsParent.Add(parentObjectGraph);
            }

            return graph;
        }

        /// <summary>
        /// Creates the relationships graph for the property type.
        /// </summary>
        /// <param name="property">The property for which the relationships graph needs to be created.</param>
        /// <param name="allConfigurations">All the configurations.</param>
        /// <param name="propertiesGraph">The properties graph.</param>
        /// <returns>A related object.</returns>
        /// <exception cref="System.NotSupportedException">The entity relationships were not configured for the type  + propertyType + .</exception>
        private static RelatedObject CreateRelationshipsGraph(PropertyInfo property, Dictionary<Type, RelationshipsConfiguration> allConfigurations, Dictionary<PropertyInfo, RelatedObject> propertiesGraph)
        {
            var entityType = property.PropertyType;
            if (entityType.GetInterfaces().Contains(typeof(IEnumerable)))
            {
                entityType = entityType.GetGenericArguments()[0];
            }

            RelationshipsConfiguration objectConfig;
            if (!allConfigurations.TryGetValue(entityType, out objectConfig))
            {
                throw new NotSupportedException("The entity relationships were not configured for the type " + entityType + ".");
            }

            RelatedObject propertyGraph = null;
            propertiesGraph.TryGetValue(property, out propertyGraph);
            if (propertyGraph == null)
            {
                propertyGraph = CreateRelationshipsGraph(property, entityType, objectConfig, allConfigurations, propertiesGraph);
            }

            return propertyGraph;
        }

        #region Configure Relationships

        /// <summary>
        /// Configures the entity relationships.
        /// </summary>
        /// <returns>The relationships configurations.</returns>
        private static Dictionary<Type, RelationshipsConfiguration> ConfigureEntityRelationships()
        {
            var relationshipConfigurations = new Dictionary<Type, RelationshipsConfiguration>();

            relationshipConfigurations[typeof(Assembly)] = ConfigureAssemblyRelationships();
            relationshipConfigurations[typeof(AssemblyProcessStep)] = ConfigureProcessStepRelationships();
            relationshipConfigurations[typeof(Bookmark)] = ConfigureBookmarkRelationships();
            relationshipConfigurations[typeof(CommoditiesPriceHistory)] = ConfigureCommoditiesPriceHistoryRelationships();
            relationshipConfigurations[typeof(Commodity)] = ConfigureCommodityRelationships();
            relationshipConfigurations[typeof(Consumable)] = ConfigureConsumableRelationships();
            relationshipConfigurations[typeof(ConsumablesPriceHistory)] = ConfigureConsumablesPriceHistoryRelationships();
            relationshipConfigurations[typeof(Country)] = ConfigureCountryRelationships();
            relationshipConfigurations[typeof(CountrySetting)] = ConfigureCountrySettingRelationships();
            relationshipConfigurations[typeof(CountrySettingsValueHistory)] = ConfigureCountrySettingsValueHistoryRelationships();
            relationshipConfigurations[typeof(CountryState)] = ConfigureCountryStateRelationships();
            relationshipConfigurations[typeof(Currency)] = ConfigureCurrencyRelationships();
            relationshipConfigurations[typeof(CurrenciesExchangeRateHistory)] = ConfigureCurrenciesExchangeRateHistoryRelationships();
            relationshipConfigurations[typeof(Customer)] = ConfigureCustomerRelationships();
            relationshipConfigurations[typeof(CycleTimeCalculation)] = ConfigureCycleTimeCalculationRelationships();
            relationshipConfigurations[typeof(Die)] = ConfigureDieRelationships();
            relationshipConfigurations[typeof(Machine)] = ConfigureMachineRelationships();
            relationshipConfigurations[typeof(MachinesClassification)] = ConfigureMachinesClassificationRelationships();
            relationshipConfigurations[typeof(Manufacturer)] = ConfigureManufacturerRelationships();
            relationshipConfigurations[typeof(MaterialsClassification)] = ConfigureMaterialsClassificationRelationships();
            relationshipConfigurations[typeof(MeasurementUnit)] = ConfigureMeasurementUnitRelationships();
            relationshipConfigurations[typeof(Media)] = ConfigureMediaRelationships();
            relationshipConfigurations[typeof(OverheadSetting)] = ConfigureOverheadSettingRelationships();
            relationshipConfigurations[typeof(Part)] = ConfigurePartRelationships();
            relationshipConfigurations[typeof(PartProcessStep)] = ConfigureProcessStepRelationships();
            relationshipConfigurations[typeof(PasswordsHistory)] = ConfigurePasswordsHistoryRelationships();
            relationshipConfigurations[typeof(Process)] = ConfigureProcessRelationships();
            relationshipConfigurations[typeof(ProcessStep)] = ConfigureProcessStepRelationships();
            relationshipConfigurations[typeof(ProcessStepAssemblyAmount)] = ConfigureProcessStepAssemblyAmountRelationships();
            relationshipConfigurations[typeof(ProcessStepPartAmount)] = ConfigureProcessStepPartAmountRelationships();
            relationshipConfigurations[typeof(ProcessStepsClassification)] = ConfigureProcessStepsClassificationRelationships();
            relationshipConfigurations[typeof(Project)] = ConfigureProjectRelationships();
            relationshipConfigurations[typeof(ProjectFolder)] = ConfigureProjectFolderRelationships();
            relationshipConfigurations[typeof(RawMaterial)] = ConfigureRawMaterialRelationships();
            relationshipConfigurations[typeof(RawMaterialDeliveryType)] = ConfigureRawMaterialDeliveryTypeRelationships();
            relationshipConfigurations[typeof(RawMaterialsPriceHistory)] = ConfigureRawMaterialsPriceHistoryRelationships();
            relationshipConfigurations[typeof(RawPart)] = ConfigureRawPartRelationships();
            relationshipConfigurations[typeof(TransportCostCalculation)] = ConfigureTransportCostCalculationRelationships();
            relationshipConfigurations[typeof(TransportCostCalculationSetting)] = ConfigureTransportCostCalculationSettingRelationships();
            relationshipConfigurations[typeof(TrashBinItem)] = ConfigureTrashBinItemRelationships();
            relationshipConfigurations[typeof(User)] = ConfigureUserRelationships();

            return relationshipConfigurations;
        }

        /// <summary>
        /// Configures the assembly relationships.
        /// </summary>
        /// <returns>The assembly relationships configuration.</returns>
        private static RelationshipsConfiguration<Assembly> ConfigureAssemblyRelationships()
        {
            return new RelationshipsConfiguration<Assembly>()
                .Owns(a => a.CountrySettings)
                .Owns(a => a.Manufacturer)
                .Owns(a => a.Media)
                .Owns(a => a.OverheadSettings)
                .Owns(a => a.Parts)
                .Owns(a => a.Process)
                .Owns(a => a.ProcessStepAssemblyAmounts)
                .Owns(a => a.Subassemblies)
                .Owns(a => a.TransportCostCalculations)

                .References(a => a.MeasurementUnit)
                .References(a => a.Owner)
                .References(a => a.ParentAssembly)
                .References(a => a.Project)

                .HasAsParent(a => a.ParentAssembly)
                .HasAsParent(a => a.Project);
        }

        /// <summary>
        /// Configures the bookmarks relationships.
        /// </summary>
        /// <returns>The bookmarks relationships configuration.</returns>
        private static RelationshipsConfiguration<Bookmark> ConfigureBookmarkRelationships()
        {
            return new RelationshipsConfiguration<Bookmark>()
                .References(b => b.Owner);
        }

        /// <summary>
        /// Configures the commodities price history relationships.
        /// </summary>
        /// <returns>The commodities price history relationships configuration.</returns>
        private static RelationshipsConfiguration<CommoditiesPriceHistory> ConfigureCommoditiesPriceHistoryRelationships()
        {
            return new RelationshipsConfiguration<CommoditiesPriceHistory>()
                .References(c => c.Commodity)

                .HasAsParent(c => c.Commodity);
        }

        /// <summary>
        /// Configures the commodity relationships.
        /// </summary>
        /// <returns>The commodity relationships configuration.</returns>
        private static RelationshipsConfiguration<Commodity> ConfigureCommodityRelationships()
        {
            return new RelationshipsConfiguration<Commodity>()
                .Owns(c => c.Manufacturer)
                .Owns(c => c.Media)
                .Owns(c => c.PriceHistory)

                .References(c => c.Owner)
                .References(c => c.Part)
                .References(c => c.ProcessStep)
                .References(c => c.WeightUnitBase)

                .HasAsParent(c => c.Part)
                .HasAsParent(c => c.ProcessStep);
        }

        /// <summary>
        /// Configures the consumable relationships.
        /// </summary>
        /// <returns>The consumable relationships configuration.</returns>
        private static RelationshipsConfiguration<Consumable> ConfigureConsumableRelationships()
        {
            return new RelationshipsConfiguration<Consumable>()
                .Owns(c => c.Manufacturer)
                .Owns(c => c.PriceHistory)

                .References(c => c.AmountUnitBase)
                .References(c => c.Owner)
                .References(c => c.PriceUnitBase)
                .References(c => c.ProcessStep)

                .HasAsParent(c => c.ProcessStep);
        }

        /// <summary>
        /// Configures the consumables price history relationships.
        /// </summary>
        /// <returns>The consumables price history configuration.</returns>
        private static RelationshipsConfiguration<ConsumablesPriceHistory> ConfigureConsumablesPriceHistoryRelationships()
        {
            return new RelationshipsConfiguration<ConsumablesPriceHistory>()
                .References(c => c.Consumable)

                .HasAsParent(c => c.Consumable);
        }

        /// <summary>
        /// Configures the country relationships.
        /// </summary>
        /// <returns>The country relationships configuration.</returns>
        private static RelationshipsConfiguration<Country> ConfigureCountryRelationships()
        {
            return new RelationshipsConfiguration<Country>()
                .Owns(c => c.CountrySetting)
                .Owns(c => c.States)

                .References(c => c.Currency)
                .References(c => c.FloorMeasurementUnit)
                .References(c => c.LengthMeasurementUnit)
                .References(c => c.TimeMeasurementUnit)
                .References(c => c.VolumeMeasurementUnit)
                .References(c => c.WeightMeasurementUnit);
        }

        /// <summary>
        /// Configures the country setting relationships.
        /// </summary>
        /// <returns>The country setting relationships configuration.</returns>
        private static RelationshipsConfiguration<CountrySetting> ConfigureCountrySettingRelationships()
        {
            return new RelationshipsConfiguration<CountrySetting>()
                .Owns(c => c.ValueHistory)

                .References(c => c.Assemblies)
                .References(c => c.Countries)
                .References(c => c.CountryStates)
                .References(c => c.Owner)
                .References(c => c.Parts);
        }

        /// <summary>
        /// Configures the country settings value history relationships.
        /// </summary>
        /// <returns>The country settings value history relationships configuration.</returns>
        private static RelationshipsConfiguration<CountrySettingsValueHistory> ConfigureCountrySettingsValueHistoryRelationships()
        {
            return new RelationshipsConfiguration<CountrySettingsValueHistory>()
                .References(c => c.CountrySetting)

                .HasAsParent(c => c.CountrySetting);
        }

        /// <summary>
        /// Configures the country state relationships.
        /// </summary>
        /// <returns>The country state relationships configuration.</returns>
        private static RelationshipsConfiguration<CountryState> ConfigureCountryStateRelationships()
        {
            return new RelationshipsConfiguration<CountryState>()
                .References(c => c.Country)
                .References(c => c.CountrySettings)

                .HasAsParent(c => c.Country);
        }

        /// <summary>
        /// Configures the currency relationships.
        /// </summary>
        /// <returns>The currency relationships configuration.</returns>
        private static RelationshipsConfiguration<Currency> ConfigureCurrencyRelationships()
        {
            return new RelationshipsConfiguration<Currency>()
                .Owns(c => c.CurrenciesExchangeRateHistory)

                .References(c => c.Countries)
                .References(c => c.Project)
                .References(c => c.Projects)
                .References(c => c.Users)

                .HasAsParent(c => c.Project);
        }

        /// <summary>
        /// Configures the configure currencies exchange rate history relationships.
        /// </summary>
        /// <returns>The currencies exchange rate history relationships configuration.</returns>
        private static RelationshipsConfiguration<CurrenciesExchangeRateHistory> ConfigureCurrenciesExchangeRateHistoryRelationships()
        {
            return new RelationshipsConfiguration<CurrenciesExchangeRateHistory>()
                .References(c => c.Currency)

                .HasAsParent(c => c.Currency);
        }

        /// <summary>
        /// Configures the customer relationships.
        /// </summary>
        /// <returns>The customer relationships configuration.</returns>
        private static RelationshipsConfiguration<Customer> ConfigureCustomerRelationships()
        {
            return new RelationshipsConfiguration<Customer>()
                .References(c => c.Owner)
                .References(c => c.Projects)

                .HasAsParent(c => c.Projects);
        }

        /// <summary>
        /// Configures the cycle time calculation relationships.
        /// </summary>
        /// <returns>The cycle time calculation relationships configuration.</returns>
        private static RelationshipsConfiguration<CycleTimeCalculation> ConfigureCycleTimeCalculationRelationships()
        {
            return new RelationshipsConfiguration<CycleTimeCalculation>()
                .References(c => c.Owner)
                .References(c => c.ProcessStep)

                .HasAsParent(c => c.ProcessStep);
        }

        /// <summary>
        /// Configures the die relationships.
        /// </summary>
        /// <returns>The die relationships configuration.</returns>
        private static RelationshipsConfiguration<Die> ConfigureDieRelationships()
        {
            return new RelationshipsConfiguration<Die>()
                .Owns(d => d.Manufacturer)
                .Owns(d => d.Media)

                .References(d => d.Owner)
                .References(d => d.ProcessStep)

                .HasAsParent(d => d.ProcessStep);
        }

        /// <summary>
        /// Configures the machine relationships.
        /// </summary>
        /// <returns>The machine relationships configuration.</returns>
        private static RelationshipsConfiguration<Machine> ConfigureMachineRelationships()
        {
            return new RelationshipsConfiguration<Machine>()
                .Owns(m => m.Manufacturer)
                .Owns(m => m.Media)

                .References(m => m.ClassificationLevel4)
                .References(m => m.MainClassification)
                .References(m => m.Owner)
                .References(m => m.ProcessStep)
                .References(m => m.SubClassification)
                .References(m => m.TypeClassification)

                .HasAsParent(m => m.ProcessStep);
        }

        /// <summary>
        /// Configures the machines classification relationships.
        /// </summary>
        /// <returns>The machines classification relationships configuration.</returns>
        private static RelationshipsConfiguration<MachinesClassification> ConfigureMachinesClassificationRelationships()
        {
            return new RelationshipsConfiguration<MachinesClassification>()
                .Owns(m => m.Children)

                .References(m => m.ClassificationsLevel4)
                .References(m => m.Machines)
                .References(m => m.Parent)
                .References(m => m.SubClassifications)
                .References(m => m.TypeClassifications)

                .HasAsParent(m => m.Parent);
        }

        /// <summary>
        /// Configures the manufacturer relationships.
        /// </summary>
        /// <returns>The manufacturer relationships configuration.</returns>
        private static RelationshipsConfiguration<Manufacturer> ConfigureManufacturerRelationships()
        {
            return new RelationshipsConfiguration<Manufacturer>()
                .References(m => m.Assemblies)
                .References(m => m.Commodities)
                .References(m => m.Consumables)
                .References(m => m.Dies)
                .References(m => m.Machines)
                .References(m => m.Owner)
                .References(m => m.Parts)
                .References(m => m.RawMaterials)

                .HasAsParent(m => m.Assemblies)
                .HasAsParent(m => m.Commodities)
                .HasAsParent(m => m.Consumables)
                .HasAsParent(m => m.Dies)
                .HasAsParent(m => m.Machines)
                .HasAsParent(m => m.Parts)
                .HasAsParent(m => m.RawMaterials);
        }

        /// <summary>
        /// Configures the materials classification relationships.
        /// </summary>
        /// <returns>The materials classification relationships configuration.</returns>
        private static RelationshipsConfiguration<MaterialsClassification> ConfigureMaterialsClassificationRelationships()
        {
            return new RelationshipsConfiguration<MaterialsClassification>()
                .Owns(m => m.Children)

                .References(m => m.Parent)
                .References(m => m.RawMaterials)
                .References(m => m.RawMaterials1)
                .References(m => m.RawMaterials2)
                .References(m => m.RawMaterials3)

                .HasAsParent(m => m.Parent);
        }

        /// <summary>
        /// Configures the measurement unit relationships.
        /// </summary>
        /// <returns>The measurement unit relationships configuration.</returns>
        private static RelationshipsConfiguration<MeasurementUnit> ConfigureMeasurementUnitRelationships()
        {
            return new RelationshipsConfiguration<MeasurementUnit>()
                .References(c => c.Assemblies)
                .References(c => c.Commodities)
                .References(c => c.Consumables)
                .References(c => c.Consumables1)
                .References(c => c.Countries1)
                .References(c => c.Countries3)
                .References(c => c.Countries4)
                .References(c => c.CountriesVolume)
                .References(c => c.CountriesWeight)
                .References(c => c.Parts)
                .References(c => c.ProcessSteps)
                .References(c => c.ProcessSteps1)
                .References(c => c.ProcessSteps2)
                .References(c => c.ProcessSteps3)
                .References(c => c.RawMaterials)
                .References(c => c.RawMaterials1)
                .References(c => c.RawMaterials2);
        }

        /// <summary>
        /// Configures the media relationships.
        /// </summary>
        /// <returns>The media relationships configuration.</returns>
        private static RelationshipsConfiguration<Media> ConfigureMediaRelationships()
        {
            return new RelationshipsConfiguration<Media>()
                .References(m => m.Assembly)
                .References(m => m.Commodities)
                .References(m => m.Dies)
                .References(m => m.Machines)
                .References(m => m.Owner)
                .References(m => m.Part)
                .References(m => m.ProcessSteps)
                .References(m => m.Project)
                .References(m => m.RawMaterials)

                .HasAsParent(m => m.Assembly)
                .HasAsParent(m => m.Commodities)
                .HasAsParent(m => m.Dies)
                .HasAsParent(m => m.Machines)
                .HasAsParent(m => m.Part)
                .HasAsParent(m => m.ProcessSteps)
                .HasAsParent(m => m.Project)
                .HasAsParent(m => m.RawMaterials);
        }

        /// <summary>
        /// Configures the overhead setting relationships.
        /// </summary>
        /// <returns>The overhead setting relationships configuration.</returns>
        private static RelationshipsConfiguration<OverheadSetting> ConfigureOverheadSettingRelationships()
        {
            return new RelationshipsConfiguration<OverheadSetting>()
                .References(o => o.Assemblies)
                .References(o => o.Owner)
                .References(o => o.Parts)
                .References(o => o.Projects)

                .HasAsParent(o => o.Assemblies)
                .HasAsParent(o => o.Parts)
                .HasAsParent(o => o.Projects);
        }

        /// <summary>
        /// Configures the part base relationships.
        /// </summary>
        /// <typeparam name="T">The entity type.</typeparam>
        /// <returns>The part base relationships configuration.</returns>
        private static RelationshipsConfiguration<T> ConfigurePartBaseRelationships<T>() where T : Part
        {
            return new RelationshipsConfiguration<T>()
                .Owns(p => p.Commodities)
                .Owns(p => p.CountrySettings)
                .Owns(p => p.Manufacturer)
                .Owns(p => p.Media)
                .Owns(p => p.OverheadSettings)
                .Owns(p => p.Process)
                .Owns(p => p.ProcessStepPartAmounts)
                .Owns(p => p.RawMaterials)
                .Owns(p => p.TransportCostCalculations)

                .References(p => p.CalculatorUser)
                .References(p => p.MeasurementUnit)
                .References(p => p.Owner);
        }

        /// <summary>
        /// Configures the part relationships.
        /// </summary>
        /// <returns>The part relationships configuration.</returns>
        private static RelationshipsConfiguration<Part> ConfigurePartRelationships()
        {
            return ConfigurePartBaseRelationships<Part>()
                .Owns(p => p.RawPart)

                .References(p => p.Assembly)
                .References(p => p.Project)

                .HasAsParent(a => a.Assembly)
                .HasAsParent(a => a.Project);
        }

        /// <summary>
        /// Configures the raw part relationships.
        /// </summary>
        /// <returns>The raw part relationships configuration.</returns>
        private static RelationshipsConfiguration<RawPart> ConfigureRawPartRelationships()
        {
            return ConfigurePartBaseRelationships<RawPart>()
                .References(p => p.ParentOfRawPart)

                .HasAsParent(a => a.ParentOfRawPart);
        }

        /// <summary>
        /// Configures the passwords history relationships.
        /// </summary>
        /// <returns>The passwords history relationships configuration.</returns>
        private static RelationshipsConfiguration<PasswordsHistory> ConfigurePasswordsHistoryRelationships()
        {
            return new RelationshipsConfiguration<PasswordsHistory>()
                .References(p => p.User);
        }

        /// <summary>
        /// Configures the process relationships.
        /// </summary>
        /// <returns>The process relationships configuration.</returns>
        private static RelationshipsConfiguration<Process> ConfigureProcessRelationships()
        {
            return new RelationshipsConfiguration<Process>()
                .Owns(p => p.Steps)

                .References(p => p.Assemblies)
                .References(p => p.Owner)
                .References(p => p.Parts)

                .HasAsParent(p => p.Assemblies)
                .HasAsParent(p => p.Parts);
        }

        /// <summary>
        /// Configures the process step relationships.
        /// </summary>
        /// <returns>The process step relationships configuration.</returns>
        private static RelationshipsConfiguration<ProcessStep> ConfigureProcessStepRelationships()
        {
            return new RelationshipsConfiguration<ProcessStep>()
                .Owns(s => s.AssemblyAmounts)
                .Owns(s => s.Commodities)
                .Owns(s => s.Consumables)
                .Owns(s => s.CycleTimeCalculations)
                .Owns(s => s.Dies)
                .Owns(s => s.Machines)
                .Owns(s => s.Media)
                .Owns(s => s.PartAmounts)
                .Owns(s => s.Picture)

                .References(s => s.CycleTimeUnit)
                .References(s => s.MaxDownTimeUnit)
                .References(s => s.Owner)
                .References(s => s.Process)
                .References(s => s.ProcessTimeUnit)
                .References(s => s.SubType)
                .References(s => s.Type)

                .HasAsParent(p => p.Process);
        }

        /// <summary>
        /// Configures the process step assembly amount relationships.
        /// </summary>
        /// <returns>The process step assembly amount relationships configuration.</returns>
        private static RelationshipsConfiguration<ProcessStepAssemblyAmount> ConfigureProcessStepAssemblyAmountRelationships()
        {
            return new RelationshipsConfiguration<ProcessStepAssemblyAmount>()
                .References(s => s.Assembly)
                .References(s => s.Owner)
                .References(s => s.ProcessStep)

                .HasAsParent(s => s.Assembly)
                .HasAsParent(s => s.ProcessStep);
        }

        /// <summary>
        /// Configures the process step part amount relationships.
        /// </summary>
        /// <returns>The process step part amount relationships configuration.</returns>
        private static RelationshipsConfiguration<ProcessStepPartAmount> ConfigureProcessStepPartAmountRelationships()
        {
            return new RelationshipsConfiguration<ProcessStepPartAmount>()
                .References(s => s.Owner)
                .References(s => s.Part)
                .References(s => s.ProcessStep)

                .HasAsParent(s => s.Part)
                .HasAsParent(s => s.ProcessStep);
        }

        /// <summary>
        /// Configures the process steps classification relationships.
        /// </summary>
        /// <returns>The process steps classification relationships configuration.</returns>
        private static RelationshipsConfiguration<ProcessStepsClassification> ConfigureProcessStepsClassificationRelationships()
        {
            return new RelationshipsConfiguration<ProcessStepsClassification>()
                .Owns(s => s.Children)

                .References(s => s.Parent)
                .References(s => s.ProcessSteps)
                .References(s => s.SubType)

                .HasAsParent(s => s.Parent);
        }

        /// <summary>
        /// Configures the project relationships.
        /// </summary>
        /// <returns>The project relationships configuration.</returns>
        private static RelationshipsConfiguration<Project> ConfigureProjectRelationships()
        {
            return new RelationshipsConfiguration<Project>()
                .Owns(p => p.Assemblies)
                .Owns(p => p.BaseCurrency)
                .Owns(p => p.Currencies)
                .Owns(p => p.Customer)
                .Owns(p => p.Media)
                .Owns(p => p.OverheadSettings)
                .Owns(p => p.Parts)

                .References(p => p.Owner)
                .References(p => p.ProjectFolder)
                .References(p => p.ProjectLeader)
                .References(p => p.ResponsibleCalculator)

                .HasAsParent(p => p.ProjectFolder);
        }

        /// <summary>
        /// Configures the project folder relationships.
        /// </summary>
        /// <returns>The project folder relationships configuration.</returns>
        private static RelationshipsConfiguration<ProjectFolder> ConfigureProjectFolderRelationships()
        {
            return new RelationshipsConfiguration<ProjectFolder>()
                .Owns(f => f.ChildrenProjectFolders)
                .Owns(f => f.Projects)

                .References(f => f.Owner)
                .References(f => f.ParentProjectFolder)

                .HasAsParent(f => f.ParentProjectFolder);
        }

        /// <summary>
        /// Configures the raw material relationships.
        /// </summary>
        /// <returns>The raw material relationships configuration.</returns>
        private static RelationshipsConfiguration<RawMaterial> ConfigureRawMaterialRelationships()
        {
            return new RelationshipsConfiguration<RawMaterial>()
                .Owns(m => m.Manufacturer)
                .Owns(m => m.Media)
                .Owns(m => m.PriceHistory)

                .References(m => m.DeliveryType)
                .References(m => m.MaterialsClassificationL1)
                .References(m => m.MaterialsClassificationL2)
                .References(m => m.MaterialsClassificationL3)
                .References(m => m.MaterialsClassificationL4)
                .References(m => m.Owner)
                .References(m => m.ParentWeightUnitBase)
                .References(m => m.Part)
                .References(m => m.PriceUnitBase)
                .References(m => m.QuantityUnitBase)

                .HasAsParent(m => m.Part);
        }

        /// <summary>
        /// Configures the raw material delivery type relationships.
        /// </summary>
        /// <returns>The raw material delivery type relationships configuration.</returns>
        private static RelationshipsConfiguration<RawMaterialDeliveryType> ConfigureRawMaterialDeliveryTypeRelationships()
        {
            return new RelationshipsConfiguration<RawMaterialDeliveryType>()
                .References(m => m.RawMaterials);
        }

        /// <summary>
        /// Configures the raw materials price history relationships.
        /// </summary>
        /// <returns>The raw materials price history relationships configuration.</returns>
        private static RelationshipsConfiguration<RawMaterialsPriceHistory> ConfigureRawMaterialsPriceHistoryRelationships()
        {
            return new RelationshipsConfiguration<RawMaterialsPriceHistory>()
                .References(m => m.RawMaterial)

                .HasAsParent(m => m.RawMaterial);
        }

        /// <summary>
        /// Configures the transport cost calculation relationships.
        /// </summary>
        /// <returns>The transport cost calculation relationships configuration.</returns>
        private static RelationshipsConfiguration<TransportCostCalculation> ConfigureTransportCostCalculationRelationships()
        {
            return new RelationshipsConfiguration<TransportCostCalculation>()
                .Owns(c => c.TransportCostCalculationSetting)

                .References(c => c.Assembly)
                .References(c => c.Owner)
                .References(c => c.Part)

                .HasAsParent(c => c.Assembly)
                .HasAsParent(c => c.Part);
        }

        /// <summary>
        /// Configures the transport cost calculation setting relationships.
        /// </summary>
        /// <returns>The transport cost calculation setting relationships configuration.</returns>
        private static RelationshipsConfiguration<TransportCostCalculationSetting> ConfigureTransportCostCalculationSettingRelationships()
        {
            return new RelationshipsConfiguration<TransportCostCalculationSetting>()
                .References(s => s.Owner)
                .References(s => s.TransportCostCalculation)

                .HasAsParent(s => s.TransportCostCalculation);
        }

        /// <summary>
        /// Configures the trash bin item relationships.
        /// </summary>
        /// <returns>The trash bin item relationships configuration.</returns>
        private static RelationshipsConfiguration<TrashBinItem> ConfigureTrashBinItemRelationships()
        {
            return new RelationshipsConfiguration<TrashBinItem>()
                .References(t => t.Owner);
        }

        /// <summary>
        /// Configures the user relationships.
        /// </summary>
        /// <returns>The transport user relationships configuration.</returns>
        private static RelationshipsConfiguration<User> ConfigureUserRelationships()
        {
            return new RelationshipsConfiguration<User>()
                .References(u => u.Assemblies)
                .References(u => u.Bookmarks)
                .References(u => u.Commodities)
                .References(u => u.Consumables)
                .References(u => u.CountrySettings)
                .References(u => u.Customers)
                .References(u => u.CycleTimeCalculations)
                .References(u => u.Dies)
                .References(u => u.Machines)
                .References(u => u.Manufacturers)
                .References(u => u.Media)
                .References(u => u.OverheadSettings)
                .References(u => u.PartsForWichCalculator)
                .References(u => u.PartsOwned)
                .References(u => u.Processes)
                .References(u => u.ProcessStepAssemblyAmounts)
                .References(u => u.ProcessStepPartAmounts)
                .References(u => u.ProcessSteps)
                .References(u => u.ProjectFolders)
                .References(u => u.Projects)
                .References(u => u.ProjectsForWhichCalculator)
                .References(u => u.ProjectsForWhichLeader)
                .References(u => u.RawMaterials)
                .References(u => u.TransportCostCalculations)
                .References(u => u.TransportCostCalculationSettings)
                .References(u => u.TrashBinItems)
                .References(u => u.UICurrency);
        }

        #endregion Configure Relationships

        /// <summary>
        /// Collects the owned objects of the specified entity together with all owned sub-objects in its graph.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>The owned objects.</returns>
        public static ICollection<object> CollectOwnedObjects(object entity)
        {
            var ownedObjects = new List<object>();
            ownedObjects.Add(entity);

            foreach (var ownedObject in EntitiesRelationshipsGraph[entity.GetType()].Owned)
            {
                var child = ownedObject.Property.GetValue(entity, null);
                if (child == null)
                {
                    continue;
                }

                var childsList = child as IEnumerable;
                if (childsList != null)
                {
                    foreach (var childElement in childsList)
                    {
                        ownedObjects.AddRange(CollectOwnedObjects(childElement));
                    }
                }
                else
                {
                    ownedObjects.AddRange(CollectOwnedObjects(child));
                }
            }

            return ownedObjects;
        }

        /// <summary>
        /// Fixes the entity relationships by setting to the child objects their reference to the parent object.
        /// <remarks>
        /// This is necessary after import, when the references are set only top-down (from parent to child).
        /// For cost calculation purposes, it is necessary to also set the child's reference to its parent.
        /// </remarks>
        /// </summary>
        /// <param name="entity">The entity.</param>
        public static void FixupEntityRelationships(object entity)
        {
            FixupEntityRelationships(entity, null, null);
        }

        /// <summary>
        /// Fixes the entity relationships by setting to the child objects their reference to the parent object.
        /// <remarks>
        /// This is necessary after import, when the references are set only top-down (from parent to child).
        /// For cost calculation purposes, it is necessary to also set the child's reference to its parent.
        /// </remarks>
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="entityParent">The entity parent.</param>
        /// <param name="entityGraph">The entity graph.</param>
        private static void FixupEntityRelationships(object entity, object entityParent, RelatedObject entityGraph)
        {
            if (entity == null)
            {
                return;
            }

            if (entityParent != null)
            {
                var parent = entityGraph.HasAsParent.FirstOrDefault(a => a.EntityType == entityParent.GetType());
                if (parent != null)
                {
                    // If the parent of the property is a collection clear it and add the parent
                    // or else set the parent of the property directly.
                    if (parent.Property.PropertyType.IsGenericType && parent.Property.PropertyType.GetGenericTypeDefinition() == typeof(ICollection<>))
                    {
                        var parentValue = parent.Property.GetValue(entity, null);
                        if (parentValue != null)
                        {
                            parent.Property.PropertyType.InvokeMember(
                                "Clear",
                                BindingFlags.InvokeMethod | BindingFlags.Instance | BindingFlags.Public,
                                null,
                                parentValue,
                                null);

                            parent.Property.PropertyType.InvokeMember(
                                "Add",
                                BindingFlags.InvokeMethod | BindingFlags.Instance | BindingFlags.Public,
                                null,
                                parentValue,
                                new object[] { entityParent });
                        }
                    }
                    else
                    {
                        parent.Property.SetValue(entity, entityParent, null);
                    }
                }
            }

            // For each owned object repeat the algorithm that sets the properties parent.
            foreach (var ownedObject in EntitiesRelationshipsGraph[entity.GetType()].Owned)
            {
                var child = ownedObject.Property.GetValue(entity, null);

                var childsList = child as IEnumerable;
                if (childsList != null)
                {
                    foreach (var childElement in childsList)
                    {
                        FixupEntityRelationships(childElement, entity, ownedObject);
                    }
                }
                else
                {
                    FixupEntityRelationships(child, entity, ownedObject);
                }
            }
        }

        #region Inner Classes

        /// <summary>
        /// Holds a relationships configuration that contains the owned, referenced and parent properties.
        /// </summary>
        private class RelationshipsConfiguration
        {
            #region Constructors

            /// <summary>
            /// Initializes a new instance of the <see cref="RelationshipsConfiguration"/> class.
            /// </summary>
            public RelationshipsConfiguration()
            {
                this.OwnedProperties = new List<PropertyInfo>();
                this.ReferencedProperties = new List<PropertyInfo>();
                this.ParentProperties = new List<PropertyInfo>();
            }

            #endregion Constructors

            #region Properties

            /// <summary>
            /// Gets the owned properties.
            /// </summary>
            public ICollection<PropertyInfo> OwnedProperties { get; private set; }

            /// <summary>
            /// Gets the referenced properties.
            /// </summary>
            public ICollection<PropertyInfo> ReferencedProperties { get; private set; }

            /// <summary>
            /// Gets the parent properties.
            /// </summary>
            public ICollection<PropertyInfo> ParentProperties { get; private set; }

            #endregion Properties
        }

        /// <summary>
        /// Implements the relationships configuration for the specified type.
        /// </summary>
        /// <typeparam name="TOwner">The type of the owner.</typeparam>
        private class RelationshipsConfiguration<TOwner> : RelationshipsConfiguration where TOwner : class
        {
            /// <summary>
            /// Owns the specified property.
            /// </summary>
            /// <typeparam name="TOwned">The type of the owned.</typeparam>
            /// <param name="propertyAccessExpr">The property access expression.</param>
            /// <returns>A relationship configuration.</returns>
            [SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "The signature is required in order to accept lambda expressions.")]
            public RelationshipsConfiguration<TOwner> Owns<TOwned>(Expression<Func<TOwner, TOwned>> propertyAccessExpr) where TOwned : class
            {
                var property = GetProperty(propertyAccessExpr);
                this.OwnedProperties.Add(property);

                return this;
            }

            /// <summary>
            /// References the specified property.
            /// </summary>
            /// <typeparam name="TOwned">The type of the owned.</typeparam>
            /// <param name="propertyAccessExpr">The property access expression.</param>
            /// <returns>A relationship configuration.</returns>
            [SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "The signature is required in order to accept lambda expressions.")]
            public RelationshipsConfiguration<TOwner> References<TOwned>(Expression<Func<TOwner, TOwned>> propertyAccessExpr) where TOwned : class
            {
                var property = GetProperty(propertyAccessExpr);
                this.ReferencedProperties.Add(property);

                return this;
            }

            /// <summary>
            /// Has as parent the specified property.
            /// </summary>
            /// <typeparam name="TOwned">The type of the owned.</typeparam>
            /// <param name="propertyAccessExpr">The property access expression.</param>
            /// <returns>A relationship configuration.</returns>
            [SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "The signature is required in order to accept lambda expressions.")]
            public RelationshipsConfiguration<TOwner> HasAsParent<TOwned>(Expression<Func<TOwner, TOwned>> propertyAccessExpr) where TOwned : class
            {
                var property = GetProperty(propertyAccessExpr);
                this.ParentProperties.Add(property);

                return this;
            }
        }

        #endregion Inner Classes

        #region Helpers

        /// <summary>
        /// Gets the property that is accessed using a lambda expression.
        /// <para />
        /// This method should replace the need for code like
        /// <code>
        /// myObject.GetType().GetProperty("PropertyName");
        /// </code>
        /// </summary>
        /// <typeparam name="T">The first type that defines the property accessed by the lambda expression.</typeparam>
        /// <typeparam name="T1">The second type that defines the property accessed by the lambda expression.</typeparam>
        /// <param name="propertyAccessExpr">A lambda expression that accesses the desired property.</param>
        /// <returns>
        /// The <see cref="PropertyInfo" /> instance for the accessed property.
        /// </returns>
        /// <exception cref="System.ArgumentException">The lambda expression did not contain a property definition.;propertyExpression</exception>
        /// <exception cref="ArgumentNullException"><paramref name="propertyAccessExpr" /> was null</exception>
        /// <exception cref="ArgumentException"><paramref name="propertyAccessExpr" /> was empty or did not contain a property</exception>
        [SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "The signature is required in order to accept lambda expressions.")]
        [SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters", Justification = "The type is required in order to pass a delegate as parameter."),]
        private static PropertyInfo GetProperty<T, T1>(Expression<Func<T, T1>> propertyAccessExpr)
        {
            var propInfo = GetAccessedMember(propertyAccessExpr) as PropertyInfo;
            if (propInfo == null)
            {
                throw new ArgumentException("The lambda expression did not contain a property definition.", "propertyAccessExpr");
            }

            return propInfo;
        }

        /// <summary>
        /// Gets the member of a type accessed by the specified lambda expression.
        /// </summary>
        /// <param name="expression">The lambda expression representing the member access.</param>
        /// <returns>A <see cref="MemberInfo"/> instance representing the accessed member or null if the expression did not access a member of a type.</returns>
        /// <exception cref="System.ArgumentNullException">The lambda expression was null.</exception>
        /// <exception cref="System.ArgumentException">The lambda expression's body was empty.</exception>
        public static MemberInfo GetAccessedMember(LambdaExpression expression)
        {
            if (expression == null)
            {
                throw new ArgumentNullException("expression", "The lambda expression was null.");
            }

            if (expression.Body == null)
            {
                throw new ArgumentException("The lambda expression's body was empty.", "expression");
            }

            // Extract the property name from the linq expression
            MemberInfo memberInfo = null;
            Expression body = expression.Body;
            if (body.NodeType == ExpressionType.MemberAccess)
            {
                MemberExpression mexp = body as MemberExpression;
                if (mexp != null)
                {
                    memberInfo = mexp.Member;
                }
            }
            else if (body.NodeType == ExpressionType.Convert)
            {
                UnaryExpression exp = body as UnaryExpression;
                if (exp != null
                    && exp.Operand != null
                    && exp.Operand.NodeType == ExpressionType.MemberAccess)
                {
                    MemberExpression mexp = exp.Operand as MemberExpression;
                    if (mexp != null)
                    {
                        memberInfo = mexp.Member;
                    }
                }
            }

            return memberInfo;
        }

        #endregion Helpers
    }
}