﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ZPKTool.Data
{
    /// <summary>
    /// Holds the related object's information about its relationships with other entities.
    /// </summary>
    public class RelatedObject
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="RelatedObject" /> class.
        /// </summary>
        /// <param name="property">The property.</param>
        /// <param name="entityType">Type of the entity.</param>
        public RelatedObject(PropertyInfo property, Type entityType)
        {
            this.Owned = new List<RelatedObject>();
            this.Referenced = new List<PropertyInfo>();
            this.HasAsParent = new List<RelatedObject>();

            this.EntityType = entityType;
            this.Property = property;
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Gets the property info of the related parent object property.
        /// For root objects this is null.
        /// </summary>
        public PropertyInfo Property { get; private set; }

        /// <summary>
        /// Gets the type of the entity.
        /// For properties that implement a generic list is the generic type, else is the property's type.
        /// </summary>
        public Type EntityType { get; private set; }

        /// <summary>
        /// Gets the owned objects.
        /// </summary>
        public ICollection<RelatedObject> Owned { get; private set; }

        /// <summary>
        /// Gets the referenced properties.
        /// </summary>
        public ICollection<PropertyInfo> Referenced { get; private set; }

        /// <summary>
        /// Gets the parent objects.
        /// </summary>
        public ICollection<RelatedObject> HasAsParent { get; private set; }

        #endregion Properties
    }
}
