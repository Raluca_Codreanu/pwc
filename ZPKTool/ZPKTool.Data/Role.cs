﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace ZPKTool.Data
{
    /// <summary>
    /// Represents the role options available to users.
    /// </summary>
    /// <remarks>
    /// This enumeration represents a set of flags, so its values must be powers of 2.
    /// </remarks>
    [Flags]
    public enum Role
    {
        /// <summary>
        /// No item
        /// </summary>
        None = 0,

        /// <summary>
        /// Has access to all application features.
        /// </summary>
        Admin = 1,

        /// <summary>
        /// Hass access to EditMasterData, ProjectsAndCalculate, ViewAndReports.
        /// </summary>
        KeyUser = 2,

        /// <summary>
        /// Has access to: EditMasterData, ViewAndReports.
        /// </summary>
        DataManager = 4,

        /// <summary>
        /// Has access to ViewAndReports, ReleaseProject.
        /// </summary>
        ProjectLeader = 8,

        /// <summary>
        /// has access to ProjectsAndCalculate and ViewAndReports.
        /// </summary>
        User = 16,

        /// <summary>
        /// Has access to ViewAndReports.
        /// </summary>
        Viewer = 32
    }
}
