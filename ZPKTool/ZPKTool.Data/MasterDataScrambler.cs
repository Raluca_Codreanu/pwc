﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Data
{
    /// <summary>
    /// Has specific methods to scramble master data which must not be visible to the user in the sql database
    /// </summary>        
    public static class MasterDataScrambler
    {
        /// <summary>
        /// The maximum integer number that can be scrambled.
        /// </summary>
        public const int MaxSupportedInt = 1000000004;

        /// <summary>
        /// The minimum integer number that can be scrambled.
        /// </summary>
        public const int MinSupportedInt = -999999999;

        /// <summary>
        /// The maximum integer number that can be scrambled.
        /// </summary>
        /// <remarks>
        /// The maximum supported number is the largest number that has 1 less digit than decimal.MaxValue.
        /// The reason for this is that the scrambling is done by replacing every digit of a number with another predefined digit, so many numbers that
        /// have the same number of digits as decimal.MaxValue could be scrambled into a larger number than decimal.MaxValue.
        /// </remarks>
        public const decimal MaxSupportedDecimal = 9999999999999999999999999999m;

        /// <summary>
        /// The minimum integer number that can be scrambled.
        /// </summary>
        public const decimal MinSupportedDecimal = -9999999999999999999999999999m;

        /// <summary>
        /// Forward mapping of the digits used for the scrambling operation
        /// </summary>
        private static readonly int[] ForwardMap = new int[] { 0, 6, 9, 5, 1, 8, 2, 4, 3, 7 };

        /// <summary>
        /// Backward mapping of the digits used for the unscrambling operation
        /// </summary>
        private static readonly int[] BackwardMap = new int[] { 0, 4, 6, 8, 7, 3, 1, 9, 5, 2 };

        /// <summary>
        /// The offset added or subtracted from the value of each byte of the string
        /// </summary>
        private static readonly byte byteDisplacement = 128;

        /// <summary>
        /// Scrambles a decimal number.
        /// </summary>
        /// <param name="number">The number</param>
        /// <returns>The scrambled number</returns>
        public static decimal? ScrambleNumber(decimal? number)
        {
            if (!number.HasValue)
            {
                return null;
            }

            return ScrambleNumber(number.Value);
        }

        /// <summary>
        /// Scrambles a decimal number
        /// </summary>
        /// <param name="number">The number</param>
        /// <returns>
        /// The scrambled number
        /// </returns>
        /// <exception cref="System.ArgumentException">
        /// The number was larger than the maximum supported value
        /// or
        /// The number was lower than the minimum supported value
        /// </exception>
        public static decimal ScrambleNumber(decimal number)
        {
            if (number > MasterDataScrambler.MaxSupportedDecimal)
            {
                throw new ArgumentException("The number was larger than the maximum supported value.", "number");
            }
            else if (number < MasterDataScrambler.MinSupportedDecimal)
            {
                throw new ArgumentException("The number was lower than the minimum supported value.", "number");
            }

            bool isNegative = false;
            if (number < 0m)
            {
                number = Math.Abs(number);
                isNegative = true;
            }

            // Split the number into the integer part and the fractional part
            decimal integralPart = Math.Truncate(number);
            decimal decimalPart = number - integralPart;

            decimal scrambledIntegralPart = 0m;
            decimal scrambledDecimalPart = 0.0m;

            // number of zeroes at the end of the integer part
            int numberOfPaddingZeroes = 0;

            // number of zeroes after the dot
            int numberOfPaddingDecimals = 0;

            if (integralPart != 0m)
            {
                // count the number of zeroes at the end
                while (integralPart % 10m == 0m)
                {
                    integralPart = Math.Floor(integralPart / 10m); // must apply integer division
                    numberOfPaddingZeroes++;
                }

                // reverse the remaining part of the number and replace each digit with its mapped digit
                while (integralPart != 0m)
                {
                    scrambledIntegralPart = (10m * scrambledIntegralPart) + ForwardMap[(int)(integralPart % 10m)];
                    integralPart = Math.Floor(integralPart / 10m); // must apply integer division
                }

                // Restore the zeroes at the end.
                scrambledIntegralPart *= (decimal)Math.Pow(10d, numberOfPaddingZeroes);
            }

            if (decimalPart != 0m)
            {
                // Count the number of zeroes between the dot and the 1st non-zero digit.
                while (Math.Truncate(decimalPart * 10m) == 0m)
                {
                    decimalPart *= 10m;
                    numberOfPaddingDecimals++;
                }

                // reverse the remaining part of the number and replace each digit with its mapped digit
                while (decimalPart != 0m)
                {
                    scrambledDecimalPart = (scrambledDecimalPart / 10m) + (ForwardMap[(int)(decimalPart * 10m)] / 10m);
                    decimalPart = (10m * decimalPart) - Math.Truncate(10m * decimalPart);
                }

                // restore the zeroes after the dot
                scrambledDecimalPart /= (decimal)Math.Pow(10d, numberOfPaddingDecimals);
            }

            decimal scrambledNumber = scrambledIntegralPart + scrambledDecimalPart;
            return !isNegative ? scrambledNumber : scrambledNumber * -1m;
        }

        /// <summary>
        /// Scrambles an integer number
        /// </summary>
        /// <param name="number">The number</param>
        /// <returns>
        /// The scrambled number
        /// </returns>
        /// <exception cref="System.ArgumentException">
        /// The number was larger than the maximum supported value
        /// or
        /// The number was lower than the minimum supported value
        /// </exception>
        public static int ScrambleNumber(int number)
        {
            if (number > MasterDataScrambler.MaxSupportedInt)
            {
                throw new ArgumentException("The number was larger than the maximum supported value.", "number");
            }
            else if (number < MasterDataScrambler.MinSupportedInt)
            {
                throw new ArgumentException("The number was lower than the minimum supported value.", "number");
            }

            bool isNegative = false;
            if (number < 0)
            {
                isNegative = true;
                number = Math.Abs(number);
            }

            int scrambledNumber = 0;

            // number of zeroes at the end of the number
            int numberOfPaddingZeroes = 0;

            if (number != 0)
            {
                // count the number of zeroes at the end of the number
                while (number % 10 == 0)
                {
                    number /= 10;
                    numberOfPaddingZeroes++;
                }

                // reverse the remaining part of the number and replace each digit with its mapped digit
                while (number != 0)
                {
                    scrambledNumber = (10 * scrambledNumber) + ForwardMap[number % 10];
                    number /= 10;
                }

                // add the zero padding
                scrambledNumber *= (int)Math.Pow(10, numberOfPaddingZeroes);
            }

            return !isNegative ? scrambledNumber : scrambledNumber * -1;
        }

        /// <summary>
        /// Unscrambles a decimal number.
        /// </summary>
        /// <param name="number">The number</param>
        /// <returns>The original number</returns>
        public static decimal? UnscrambleNumber(decimal? number)
        {
            if (!number.HasValue)
            {
                return null;
            }

            return UnscrambleNumber(number.Value);
        }

        /// <summary>
        /// Unscrambles a decimal number
        /// </summary>
        /// <param name="number">The scrambled number</param>
        /// <returns>The original number</returns>
        public static decimal UnscrambleNumber(decimal number)
        {
            bool isNegative = false;
            if (number < 0m)
            {
                isNegative = true;
                number = Math.Abs(number);
            }

            // Split the number into the integer part and the fractional part
            decimal integralPart = Math.Truncate(number);
            decimal decimalPart = number - integralPart;

            decimal unscrambledIntegralPart = 0m;
            decimal unscrambledDecimalPart = 0m;
            int numberOfPaddingZeroes = 0;
            int numberOfPaddingDecimals = 0;

            if (integralPart != 0m)
            {
                // count the number of zeroes at the end of the number
                while (integralPart % 10m == 0)
                {
                    integralPart = Math.Floor(integralPart / 10m);  // must apply integer division.
                    numberOfPaddingZeroes++;
                }

                // reverse the remaining part of the number and replace each digit with its mapped digit
                while (integralPart != 0m)
                {
                    unscrambledIntegralPart = (10m * unscrambledIntegralPart) + BackwardMap[(int)(integralPart % 10m)];
                    integralPart = Math.Floor(integralPart / 10m);
                }

                // add the zero padding
                unscrambledIntegralPart *= (decimal)Math.Pow(10d, numberOfPaddingZeroes);
            }

            if (decimalPart != 0m)
            {
                // count the number of zeroes after the dot
                while (Math.Truncate(decimalPart * 10m) == 0m)
                {
                    decimalPart *= 10m;
                    numberOfPaddingDecimals++;
                }

                // reverse the remaining part of the number and replace each digit with its mapped digit
                while (decimalPart != 0m)
                {
                    unscrambledDecimalPart = (unscrambledDecimalPart / 10m) + (BackwardMap[(int)(decimalPart * 10m)] / 10m);
                    decimalPart = (10m * decimalPart) - Math.Truncate(10m * decimalPart);
                }

                // add the zero padding
                unscrambledDecimalPart /= (decimal)Math.Pow(10d, numberOfPaddingDecimals);
            }

            decimal unscrambledNumber = unscrambledDecimalPart + unscrambledIntegralPart;
            return !isNegative ? unscrambledNumber : unscrambledNumber * -1m;
        }

        /// <summary>
        /// Unscrambles an integer number
        /// </summary>
        /// <param name="number">The scrambled number</param>
        /// <returns>The original number</returns>
        public static int UnscrambleNumber(int number)
        {
            bool isNegative = false;
            if (number < 0m)
            {
                isNegative = true;
                number = Math.Abs(number);
            }

            int unscrambledNumber = 0;
            int numberOfPaddingZeroes = 0;

            if (number != 0)
            {
                // count the number of zeroes at the end of the number
                while (number % 10 == 0)
                {
                    number /= 10;
                    numberOfPaddingZeroes++;
                }

                // reverse the remaining part of the number and replace each digit with its mapped digit
                while (number != 0)
                {
                    unscrambledNumber = (10 * unscrambledNumber) + BackwardMap[number % 10];
                    number /= 10;
                }

                // add the zero padding
                unscrambledNumber *= (int)Math.Pow(10, numberOfPaddingZeroes);
            }

            return !isNegative ? unscrambledNumber : unscrambledNumber * -1;
        }

        /// <summary>
        /// Scrambles a string/text
        /// </summary>
        /// <param name="text">The text</param>
        /// <returns>The scrambled text</returns>
        public static string ScrambleText(string text)
        {
            if (text == null)
            {
                return null;
            }

            // get the bytes of the string (2 bytes for each character)
            byte[] stringBytes = System.Text.Encoding.Unicode.GetBytes(text);
            for (int index = 0; index < stringBytes.Length; index++)
            {
                // modify each byte by a constant
                if (stringBytes[index] < byteDisplacement)
                {
                    stringBytes[index] += byteDisplacement;
                }
                else
                {
                    stringBytes[index] -= byteDisplacement;
                }
            }

            // recreate the string
            return System.Text.Encoding.Unicode.GetString(stringBytes);
        }

        /// <summary>
        /// Unscrambles a string/text
        /// </summary>
        /// <param name="text">The scrambled text</param>
        /// <returns>The original text</returns>
        public static string UnscrambleText(string text)
        {
            if (text == null)
            {
                return null;
            }

            // get the bytes of the string (2 bytes for each character)
            byte[] stringBytes = System.Text.Encoding.Unicode.GetBytes(text);
            for (int index = 0; index < stringBytes.Length; index++)
            {
                // // modify each byte by the same constant to obtain the original string
                if (stringBytes[index] >= byteDisplacement)
                {
                    stringBytes[index] -= byteDisplacement;
                }
                else
                {
                    stringBytes[index] += byteDisplacement;
                }
            }

            // recreate the string
            return System.Text.Encoding.Unicode.GetString(stringBytes);
        }
    }
}