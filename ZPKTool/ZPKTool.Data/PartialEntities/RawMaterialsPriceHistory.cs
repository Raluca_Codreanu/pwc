﻿using System;

namespace ZPKTool.Data
{
    /// <summary>
    /// Partial implementation for the RawMaterialsPriceHistory entity object
    /// </summary>
    public partial class RawMaterialsPriceHistory : IIdentifiable, ICopiable
    {
        /// <summary>
        /// Gets or sets the RawMaterial's Price.
        /// </summary>
        public decimal Price
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledPrice;
                }
                else
                {
                    return MasterDataScrambler.UnscrambleNumber(ScrambledPrice);
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledPrice = value;
                }
                else
                {
                    value = EntityUtils.PrepareNumberForScrambling(value);
                    ScrambledPrice = MasterDataScrambler.ScrambleNumber(value);
                }
            }
        }

        /// <summary>
        /// Called when this instance in initialized (by the constructor defined in the generated code).
        /// Can be used to perform custom construction-time initialization.
        /// </summary>
        partial void OnItitialized()
        {
            this.Guid = Guid.NewGuid();
        }

        /// <summary>
        /// Copies the values from this instance to another instance.
        /// </summary>
        /// <param name="destination">The destination instance.</param>
        /// <exception cref="ArgumentNullException"><paramref name="destination"/> was null</exception>
        /// <exception cref="ArgumentException"><paramref name="destination"/> did not have the expected type</exception>
        public void CopyValuesTo(object destination)
        {
            if (destination == null)
            {
                throw new ArgumentNullException("destination", "Destination was null for RawMaterialsPriceHistory.");
            }

            RawMaterialsPriceHistory temp = destination as RawMaterialsPriceHistory;
            if (temp == null)
            {
                throw new ArgumentException("Destination was not a RawMaterialsPriceHistory.");
            }

            temp.Price = this.Price;
            temp.Timestamp = this.Timestamp;
        }
    }
}
