﻿using System;

namespace ZPKTool.Data
{
    /// <summary>
    /// Partial implementation for the Die entity object
    /// </summary>
    public partial class Die : IIdentifiable, INameable, IMasterDataObject, IOwnedObject, ICopiable, ITrashable,
        IEntityWithMedia
    {
        #region Properties

        /// <summary>
        /// Gets the Wear Percentage.
        /// </summary>
        public decimal? WearPercentage
        {
            get { return this.IsWearInPercentage == true ? this.Wear : null; }
        }

        /// <summary>
        /// Gets the K Value.
        /// </summary>
        public decimal? KValue
        {
            get { return this.IsMaintenanceInPercentage == true ? this.Maintenance : null; }
        }

        #endregion Properties

        #region ITrashable Members

        /// <summary>
        /// Gets the type of the trash bin item.
        /// </summary>
        /// <value>The type of the trash bin item.</value>
        public TrashBinItemType TrashBinItemType
        {
            get { return TrashBinItemType.Die; }
        }

        /// <summary>
        /// Sets the IsDeleted property on this instance and all ITrashable objects in its graph.
        /// </summary>
        /// <param name="newValue">the new value for the IsDeleted property.</param>
        public void SetIsDeleted(bool newValue)
        {
            if (this.IsDeleted != newValue)
            {
                this.IsDeleted = newValue;
            }
        }

        #endregion ITrashable Members

        #region ICopiable Members

        /// <summary>
        /// Copies the values from this instance to another instance.
        /// </summary>
        /// <param name="destination">The destination instance.</param>
        /// <exception cref="ArgumentNullException"><paramref name="destination"/> was null</exception>
        /// <exception cref="ArgumentException"><paramref name="destination"/> did not have the expected type</exception>
        public void CopyValuesTo(object destination)
        {
            if (destination == null)
            {
                throw new ArgumentNullException("destination", "Destination was null.");
            }

            Die temp = destination as Die;
            if (temp == null)
            {
                throw new ArgumentException("Destination was not a Die.");
            }

            temp.AllocationRatio = this.AllocationRatio;
            temp.CostAllocationBasedOnPartsPerLifeTime = this.CostAllocationBasedOnPartsPerLifeTime;
            temp.CostAllocationNumberOfParts = this.CostAllocationNumberOfParts;
            temp.DiesetsNumberPaidByCustomer = this.DiesetsNumberPaidByCustomer;
            temp.Investment = this.Investment;
            temp.IsDeleted = this.IsDeleted;
            temp.IsMaintenanceInPercentage = this.IsMaintenanceInPercentage;
            temp.IsMasterData = this.IsMasterData;
            temp.IsWearInPercentage = this.IsWearInPercentage;
            temp.LifeTime = this.LifeTime;
            temp.Maintenance = this.Maintenance;
            temp.AreAllPaidByCustomer = this.AreAllPaidByCustomer;

            if (this.Name != null)
            {
                temp.Name = this.Name;
            }

            if (this.Remark != null)
            {
                temp.Remark = this.Remark;
            }

            temp.ReusableInvest = this.ReusableInvest;
            temp.Type = this.Type;
            temp.Wear = this.Wear;
            temp.CalculationCreateDate = this.CalculationCreateDate;
        }

        #endregion

        #region SetPropertiesInDepth

        /// <summary>
        /// Sets the value of the <see cref="Owner " /> property on this instance and on all the objects in its graph that implement <see cref="IOwnedObject" />.
        /// </summary>
        /// <param name="newOwner">The new owner.</param>
        public void SetOwner(User newOwner)
        {
            this.Owner = newOwner;
            if (this.Manufacturer != null)
            {
                this.Manufacturer.SetOwner(newOwner);
            }

            if (this.Media != null)
            {
                this.Media.SetOwner(newOwner);
            }
        }

        /// <summary>
        /// Sets the value of the <see cref="IsMasterData" /> property on this instance and on all the objects in its graph that implement <see cref="IMasterDataObject" />.
        /// </summary>
        /// <param name="newValue">The new value for the <see cref="IsMasterData" /> property.</param>
        public void SetIsMasterData(bool newValue)
        {
            this.IsMasterData = newValue;
            if (this.Manufacturer != null)
            {
                this.Manufacturer.IsMasterData = newValue;
            }

            if (this.Media != null)
            {
                this.Media.SetIsMasterData(newValue);
            }
        }

        #endregion SetPropertiesInDepth

        /// <summary>
        /// Called when this instance in initialized (by the constructor defined in the generated code).
        /// Can be used to perform custom construction-time initialization.
        /// </summary>
        partial void OnItitialized()
        {
            this.Guid = Guid.NewGuid();
            this.CreateDate = DateTime.Now;
            this.CalculationCreateDate = DateTime.Now;
        }
    }
}
