﻿using System;

namespace ZPKTool.Data
{
    /// <summary>
    /// Extension of the Machine entity.
    /// </summary>
    public partial class Machine : IIdentifiable, INameable, IMasterDataObject, IOwnedObject, IEntityWithMedia, ICopiable, ITrashable
    {
        #region Constants

        /// <summary>
        /// The default floor size value.
        /// </summary>
        public const decimal DefaultFloorSize = 0m;

        /// <summary>
        /// The default workspace area value.
        /// </summary>
        public const decimal DefaultWorkspaceArea = 0m;

        /// <summary>
        /// The default power consumption value.
        /// </summary>
        public const decimal DefaultPowerConsumption = 0m;

        /// <summary>
        /// The default air consumption value.
        /// </summary>
        public const decimal DefaultAirConsumption = 0m;

        /// <summary>
        /// The default water consumption value.
        /// </summary>
        public const decimal DefaultWaterConsumption = 0m;

        /// <summary>
        /// the default full load rate value.
        /// </summary>
        public const decimal DefaultFullLoadRate = 1m;

        /// <summary>
        /// The default availability value.
        /// </summary>
        public const decimal DefaultAvailability = 0.85m;

        /// <summary>
        /// The default maximum capacity value.
        /// </summary>
        public const int DefaultMaxCapacity = 0;

        /// <summary>
        /// The default OEE value.
        /// </summary>
        public const decimal DefaultOee = 0.65m;

        /// <summary>
        /// The default K value.
        /// </summary>
        public const decimal DefaultKValue = 0.04m;

        /// <summary>
        /// The default Fossil energy consumption value.
        /// </summary>
        public const decimal DefaultFossilEnergyConsumptionRatioValue = 1m;

        /// <summary>
        /// The default percentage value used to calculate the consumables cost.
        /// </summary>
        public const decimal DefaultManualConsumableCostPercentage = 0.04m;

        #endregion Constants

        #region Properties

        /// <summary>
        /// Gets the Renewable Energy Consumption Ratio.
        /// </summary>
        public decimal? RenewableEnergyConsumptionRatio
        {
            get { return 1m - (this.FossilEnergyConsumptionRatio ?? 1m); }
        }

        #endregion Properties

        #region UnscrambledProperties

        /// <summary>
        /// Gets or sets a wrapper property which performs the scramble/unscramble operation over the FloorSize field.
        /// </summary>
        public decimal? FloorSize
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledFloorSize;
                }
                else
                {
                    return MasterDataScrambler.UnscrambleNumber(ScrambledFloorSize);
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledFloorSize = value;
                }
                else
                {
                    value = EntityUtils.PrepareNumberForScrambling(value);
                    ScrambledFloorSize = MasterDataScrambler.ScrambleNumber(value);
                }

                this.OnPropertyChanged("FloorSize");
            }
        }

        /// <summary>
        /// Gets or sets a wrapper property which performs the scramble/unscramble operation over the WorkspaceArea field.
        /// </summary>
        public decimal? WorkspaceArea
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledWorkspaceArea;
                }
                else
                {
                    return MasterDataScrambler.UnscrambleNumber(ScrambledWorkspaceArea);
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledWorkspaceArea = value;
                }
                else
                {
                    value = EntityUtils.PrepareNumberForScrambling(value);
                    ScrambledWorkspaceArea = MasterDataScrambler.ScrambleNumber(value);
                }

                this.OnPropertyChanged("WorkspaceArea");
            }
        }

        /// <summary>
        /// Gets or sets a wrapper property which performs the scramble/unscramble operation over the PowerConsumption field.
        /// </summary>
        public decimal? PowerConsumption
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledPowerConsumption;
                }
                else
                {
                    return MasterDataScrambler.UnscrambleNumber(ScrambledPowerConsumption);
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledPowerConsumption = value;
                }
                else
                {
                    value = EntityUtils.PrepareNumberForScrambling(value);
                    ScrambledPowerConsumption = MasterDataScrambler.ScrambleNumber(value);
                }

                this.OnPropertyChanged("PowerConsumption");
            }
        }

        /// <summary>
        /// Gets or sets a wrapper property which performs the scramble/unscramble operation over the MachineInvestment field.
        /// </summary>
        public decimal? MachineInvestment
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledMachineInvestment;
                }
                else
                {
                    return MasterDataScrambler.UnscrambleNumber(ScrambledMachineInvestment);
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledMachineInvestment = value;
                }
                else
                {
                    value = EntityUtils.PrepareNumberForScrambling(value);
                    ScrambledMachineInvestment = MasterDataScrambler.ScrambleNumber(value);
                }

                this.OnPropertyChanged("MachineInvestment");
            }
        }

        /// <summary>
        /// Gets or sets a wrapper property which performs the scramble/unscramble operation over the SetupInvestment field.
        /// </summary>
        public decimal? SetupInvestment
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledSetupInvestment;
                }
                else
                {
                    return MasterDataScrambler.UnscrambleNumber(ScrambledSetupInvestment);
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledSetupInvestment = value;
                }
                else
                {
                    value = EntityUtils.PrepareNumberForScrambling(value);
                    ScrambledSetupInvestment = MasterDataScrambler.ScrambleNumber(value);
                }

                this.OnPropertyChanged("SetupInvestment");
            }
        }

        /// <summary>
        /// Gets or sets a wrapper property which performs the scramble/unscramble operation over the AdditionalEquipmentInvestment field.
        /// </summary>
        public decimal? AdditionalEquipmentInvestment
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledAdditionalEquipmentInvestment;
                }
                else
                {
                    return MasterDataScrambler.UnscrambleNumber(ScrambledAdditionalEquipmentInvestment);
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledAdditionalEquipmentInvestment = value;
                }
                else
                {
                    value = EntityUtils.PrepareNumberForScrambling(value);
                    ScrambledAdditionalEquipmentInvestment = MasterDataScrambler.ScrambleNumber(value);
                }

                this.OnPropertyChanged("AdditionalEquipmentInvestment");
            }
        }

        /// <summary>
        /// Gets or sets a wrapper property which performs the scramble/unscramble operation over the FundamentalSetupInvestment field.
        /// </summary>
        public decimal? FundamentalSetupInvestment
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledFundamentalSetupInvestment;
                }
                else
                {
                    return MasterDataScrambler.UnscrambleNumber(ScrambledFundamentalSetupInvestment);
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledFundamentalSetupInvestment = value;
                }
                else
                {
                    value = EntityUtils.PrepareNumberForScrambling(value);
                    ScrambledFundamentalSetupInvestment = MasterDataScrambler.ScrambleNumber(value);
                }

                this.OnPropertyChanged("FundamentalSetupInvestment");
            }
        }

        #endregion

        #region ITrashable Members

        /// <summary>
        /// Gets the type of the trash bin item.
        /// </summary>
        /// <value>The type of the trash bin item.</value>
        public TrashBinItemType TrashBinItemType
        {
            get { return TrashBinItemType.Machine; }
        }

        /// <summary>
        /// Sets the IsDeleted property on this instance and all ITrashable objects in its graph.
        /// </summary>
        /// <param name="newValue">the new value for the IsDeleted property.</param>
        public void SetIsDeleted(bool newValue)
        {
            if (this.IsDeleted != newValue)
            {
                this.IsDeleted = newValue;
            }
        }

        #endregion ITrashable Members

        #region ICopiable Members

        /// <summary>
        /// Copies the values from this instance to another instance.
        /// </summary>
        /// <param name="destination">The destination instance.</param>
        /// <exception cref="ArgumentNullException"><paramref name="destination"/> was null</exception>
        /// <exception cref="ArgumentException"><paramref name="destination"/> did not have the expected type</exception>
        public void CopyValuesTo(object destination)
        {
            if (destination == null)
            {
                throw new ArgumentNullException("destination", "Destination was null.");
            }

            Machine temp = destination as Machine;
            if (temp == null)
            {
                throw new ArgumentException("Destination was not a Machine.");
            }

            temp.IsScrambled = this.IsScrambled;
            temp.Index = this.Index;
            temp.AdditionalEquipmentInvestment = this.AdditionalEquipmentInvestment;
            temp.AirConsumption = this.AirConsumption;
            temp.Availability = this.Availability;
            temp.CalculateWithKValue = this.CalculateWithKValue;
            temp.ConsumablesCost = this.ConsumablesCost;
            temp.DepreciationPeriod = this.DepreciationPeriod;
            temp.DepreciationRate = this.DepreciationRate;
            temp.DescriptionOfFunction = this.DescriptionOfFunction;
            temp.ExternalWorkCost = this.ExternalWorkCost;
            temp.FloorSize = this.FloorSize;
            temp.FullLoadRate = this.FullLoadRate;
            temp.FossilEnergyConsumptionRatio = this.FossilEnergyConsumptionRatio;
            temp.FundamentalSetupInvestment = this.FundamentalSetupInvestment;
            temp.InvestmentRemarks = this.InvestmentRemarks;
            temp.IsDeleted = this.IsDeleted;
            temp.IsMasterData = this.IsMasterData;
            temp.IsProjectSpecific = this.IsProjectSpecific;
            temp.KValue = this.KValue;
            temp.MachineInvestment = this.MachineInvestment;
            temp.MachineLeaseCosts = this.MachineLeaseCosts;
            temp.ManualConsumableCostCalc = this.ManualConsumableCostCalc;
            temp.ManualConsumableCostPercentage = this.ManualConsumableCostPercentage;
            temp.ManufacturingYear = this.ManufacturingYear;
            temp.MaterialCost = this.MaterialCost;
            temp.MaxCapacity = this.MaxCapacity;

            if (this.Name != null)
            {
                temp.Name = this.Name;
            }

            temp.OEE = this.OEE;
            temp.PowerConsumption = this.PowerConsumption;

            if (this.RegistrationNumber != null)
            {
                temp.RegistrationNumber = this.RegistrationNumber;
            }

            temp.RepairsCost = this.RepairsCost;
            temp.SetupInvestment = this.SetupInvestment;
            temp.WaterConsumption = this.WaterConsumption;
            temp.WorkspaceArea = this.WorkspaceArea;

            if (this.MountingCubeHeight != null)
            {
                temp.MountingCubeHeight = this.MountingCubeHeight;
            }

            if (this.MountingCubeLength != null)
            {
                temp.MountingCubeLength = this.MountingCubeLength;
            }

            if (this.MountingCubeWidth != null)
            {
                temp.MountingCubeWidth = this.MountingCubeWidth;
            }

            if (this.MaximumSpeed != null)
            {
                temp.MaximumSpeed = this.MaximumSpeed;
            }

            if (this.MaxDiameterRodPerChuckMaxThickness != null)
            {
                temp.MaxDiameterRodPerChuckMaxThickness = this.MaxDiameterRodPerChuckMaxThickness;
            }

            if (this.MaxPartsWeightPerCapacity != null)
            {
                temp.MaxPartsWeightPerCapacity = this.MaxPartsWeightPerCapacity;
            }

            if (this.LockingForce != null)
            {
                temp.LockingForce = this.LockingForce;
            }

            if (this.PressCapacity != null)
            {
                temp.PressCapacity = this.PressCapacity;
            }

            if (this.RapidFeedPerCuttingSpeed != null)
            {
                temp.RapidFeedPerCuttingSpeed = this.RapidFeedPerCuttingSpeed;
            }

            if (this.StrokeRate != null)
            {
                temp.StrokeRate = this.StrokeRate;
            }

            if (this.Other != null)
            {
                temp.Other = this.Other;
            }

            if (this.MaintenanceNotes != null)
            {
                temp.MaintenanceNotes = this.MaintenanceNotes;
            }

            temp.Amount = this.Amount;
            temp.CalculationCreateDate = this.CalculationCreateDate;
        }

        #endregion

        #region SetPropertiesInDepth

        /// <summary>
        /// Sets the value of the <see cref="Owner " /> property on this instance and on all the objects in its graph that implement <see cref="IOwnedObject" />.
        /// </summary>
        /// <param name="newOwner">The new owner.</param>
        public void SetOwner(User newOwner)
        {
            this.Owner = newOwner;
            if (this.Manufacturer != null)
            {
                this.Manufacturer.SetOwner(newOwner);
            }

            if (this.Media != null)
            {
                this.Media.SetOwner(newOwner);
            }
        }

        /// <summary>
        /// Sets the value of the <see cref="IsMasterData" /> property on this instance and on all the objects in its graph that implement <see cref="IMasterDataObject" />.
        /// </summary>
        /// <param name="newValue">The new value for the <see cref="IsMasterData" /> property.</param>
        public void SetIsMasterData(bool newValue)
        {
            this.IsMasterData = newValue;
            if (this.Manufacturer != null)
            {
                this.Manufacturer.IsMasterData = newValue;
            }

            if (this.Media != null)
            {
                this.Media.SetIsMasterData(newValue);
            }
        }

        #endregion SetPropertiesInDepth

        /// <summary>
        /// Called when this instance in initialized (by the constructor defined in the generated code).
        /// Can be used to perform custom construction-time initialization.
        /// </summary>
        partial void OnItitialized()
        {
            this.Guid = Guid.NewGuid();
            this.CreateDate = DateTime.Now;
            this.CalculationCreateDate = DateTime.Now;

            this.FloorSize = DefaultFloorSize;
            this.WorkspaceArea = DefaultWorkspaceArea;
            this.PowerConsumption = DefaultPowerConsumption;
            this.AirConsumption = DefaultAirConsumption;
            this.WaterConsumption = DefaultWaterConsumption;
            this.FullLoadRate = DefaultFullLoadRate;
            this.FossilEnergyConsumptionRatio = DefaultFossilEnergyConsumptionRatioValue;
            this.MaxCapacity = DefaultMaxCapacity;
            this.OEE = DefaultOee;
            this.Availability = DefaultAvailability;
            this.KValue = DefaultKValue;
            this.CalculateWithKValue = true;
            this.ManualConsumableCostPercentage = DefaultManualConsumableCostPercentage;
            this.ManualConsumableCostCalc = false;
            this.Amount = 1;
        }
    }
}