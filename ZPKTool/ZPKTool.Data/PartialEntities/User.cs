﻿using System;

namespace ZPKTool.Data
{
    /// <summary>
    /// Extension class for the User entity
    /// </summary>
    public partial class User : IIdentifiable, INameable, ICopiable, IReleasable
    {
        /// <summary>
        /// Gets or sets a wrapper property which performs the encrypt/decrypt operation over the Username.
        /// </summary>
        public string Username
        {
            get
            {
                return Encryption.Decrypt(EncryptedUsername, Encryption.EncryptionPassword);
            }

            set
            {
                EncryptedUsername = Encryption.Encrypt(value, Encryption.EncryptionPassword);
                this.OnPropertyChanged("Username");
            }
        }

        /// <summary>
        /// Gets or sets a wrapper property which performs the encrypt/decrypt operation over the Roles.
        /// </summary>
        public Role Roles
        {
            get
            {
                if (string.IsNullOrWhiteSpace(EncryptedRoles))
                {
                    return Role.None;
                }

                return (Role)int.Parse(Encryption.Decrypt(EncryptedRoles, Encryption.EncryptionPassword));
            }

            set
            {
                EncryptedRoles = Encryption.Encrypt(((int)value).ToString(), Encryption.EncryptionPassword);
                this.OnPropertyChanged("Roles");
            }
        }

        #region ICopiable Members

        /// <summary>
        /// Copies the values from this instance to another instance.
        /// </summary>
        /// <param name="destination">The destination instance.</param>
        /// <exception cref="ArgumentNullException"><paramref name="destination"/> was null</exception>
        /// <exception cref="ArgumentException"><paramref name="destination"/> did not have the expected type</exception>
        public void CopyValuesTo(object destination)
        {
            if (destination == null)
            {
                throw new ArgumentNullException("destination", "Destination was null.");
            }

            User temp = destination as User;
            if (temp == null)
            {
                throw new ArgumentException("Destination was not a User.");
            }

            if (temp.Name != this.Name)
            {
                temp.Name = this.Name;
            }

            if (temp.Description != this.Description)
            {
                temp.Description = this.Description;
            }

            if (temp.Username != this.Username)
            {
                temp.Username = this.Username;
            }

            if (temp.Password != this.Password)
            {
                temp.Password = this.Password;
            }

            if (temp.Disabled != this.Disabled)
            {
                temp.Disabled = this.Disabled;
            }

            if (temp.Salt != this.Salt)
            {
                temp.Salt = this.Salt;
            }

            if (temp.IsPasswordSetByUser != this.IsPasswordSetByUser)
            {
                temp.IsPasswordSetByUser = this.IsPasswordSetByUser;
            }

            if (temp.Roles != this.Roles)
            {
                temp.Roles = this.Roles;
            }
        }

        #endregion

        /// <summary>
        /// Called when this instance in initialized (by the constructor defined in the generated code).
        /// Can be used to perform custom construction-time initialization.
        /// </summary>
        partial void OnItitialized()
        {
            this.Guid = Guid.NewGuid();
            this.CreateDate = DateTime.Now;
            this.Salt = SaltGenerator.GenerateSalt();
        }
    }
}