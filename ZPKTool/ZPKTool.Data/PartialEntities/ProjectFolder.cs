﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Data
{
    /// <summary>
    /// Extension class for the ProjectFolder entity
    /// </summary>
    public partial class ProjectFolder : IIdentifiable, INameable, IOwnedObject, ICopiable, ITrashable, IReleasable
    {
        #region ITrashable Members

        /// <summary>
        /// Gets the type of the trash bin item.
        /// </summary>
        /// <value>The type of the trash bin item.</value>
        public TrashBinItemType TrashBinItemType
        {
            get { return TrashBinItemType.ProjectFolder; }
        }

        /// <summary>
        /// Sets the IsDeleted property on this instance and all ITrashable objects in its graph.
        /// </summary>
        /// <param name="newValue">the new value for the IsDeleted property.</param>
        public void SetIsDeleted(bool newValue)
        {
            if (this.IsDeleted != newValue)
            {
                this.IsDeleted = newValue;
            }

            foreach (Project proj in this.Projects)
            {
                proj.SetIsDeleted(newValue);
            }

            foreach (ProjectFolder subFolder in this.ChildrenProjectFolders)
            {
                subFolder.SetIsDeleted(newValue);
            }
        }

        #endregion ITrashable Members

        #region IOwnedObject Members

        /// <summary>
        /// Sets the owner.
        /// Also updates sub-objects
        /// </summary>
        /// <param name="newOwner">The owner.</param>
        public void SetOwner(User newOwner)
        {
            this.Owner = newOwner;
            foreach (Project proj in this.Projects)
            {
                proj.SetOwner(newOwner);
            }

            foreach (ProjectFolder child in this.ChildrenProjectFolders)
            {
                child.SetOwner(newOwner);
            }
        }

        #endregion

        /// <summary>
        /// Copies the values from this instance to another instance.
        /// </summary>
        /// <param name="destination">The destination instance.</param>
        /// <exception cref="ArgumentNullException"><paramref name="destination"/> was null</exception>
        /// <exception cref="ArgumentException"><paramref name="destination"/> did not have the expected type</exception>
        public void CopyValuesTo(object destination)
        {
            if (destination == null)
            {
                throw new ArgumentNullException("destination", "Destination was null.");
            }

            ProjectFolder temp = destination as ProjectFolder;
            if (temp == null)
            {
                throw new ArgumentException("Destination was not a ProjectFolder.");
            }

            if (this.Name != null)
            {
                temp.Name = this.Name;
            }

            temp.IsDeleted = this.IsDeleted;
        }

        /// <summary>
        /// Called when this instance in initialized (by the constructor defined in the generated code).
        /// Can be used to perform custom construction-time initialization.
        /// </summary>
        partial void OnItitialized()
        {
            this.Guid = Guid.NewGuid();
            this.CreateDate = DateTime.Now;
        }
    }
}
