﻿using System;

namespace ZPKTool.Data
{
    /// <summary>
    /// Partial implementation of the ProcessStepAssemblyAmount entity object
    /// </summary>
    public partial class ProcessStepAssemblyAmount : IIdentifiable, IMasterDataObject, IOwnedObject, ICopiable
    {
        /// <summary>
        /// Gets the id of the assembly whose amount this instance represents.
        /// It is recommended to use this method instead of accessing the <see cref="ProcessStepAssemblyAmount.AssemblyGuid "/> or
        /// <see cref="ProcessStepAssemblyAmount.Assembly"/> properties because sometimes only one of them is set.
        /// </summary>
        /// <returns>The id of the "parent" assembly.</returns>
        public Guid FindAssemblyId()
        {
            if (this.Assembly != null && this.Assembly.Guid != Guid.Empty)
            {
                return this.Assembly.Guid;
            }
            else
            {
                return this.AssemblyGuid;
            }
        }

        #region ICopiable Members

        /// <summary>
        /// Copies the values from this instance to another instance.
        /// </summary>
        /// <param name="destination">The destination instance.</param>
        /// <exception cref="ArgumentNullException"><paramref name="destination"/> was null</exception>
        /// <exception cref="ArgumentException"><paramref name="destination"/> did not have the expected type</exception>
        public void CopyValuesTo(object destination)
        {
            if (destination == null)
            {
                throw new ArgumentNullException("destination", "Destination was null.");
            }

            ProcessStepAssemblyAmount temp = destination as ProcessStepAssemblyAmount;
            if (temp == null)
            {
                throw new ArgumentException("Destination was not a ProcessStepAssemblyAmount.");
            }

            temp.Amount = this.Amount;
            temp.IsMasterData = this.IsMasterData;
        }

        #endregion

        #region IOwnedObject Members

        /// <summary>
        /// Sets the value of the <see cref="Owner " /> property on this instance and on all the objects in its graph that implement <see cref="IOwnedObject" />.
        /// </summary>
        /// <param name="newOwner">The new owner.</param>
        public void SetOwner(User newOwner)
        {
            this.Owner = newOwner;
        }

        #endregion

        #region IMasterDataObject Members

        /// <summary>
        /// Sets the value of the <see cref="IsMasterData" /> property on this instance and on all the objects in its graph that implement <see cref="IMasterDataObject" />.
        /// </summary>
        /// <param name="newValue">The new value for the <see cref="IsMasterData" /> property.</param>
        public void SetIsMasterData(bool newValue)
        {
            this.IsMasterData = newValue;
        }

        #endregion

        /// <summary>
        /// Called when this instance in initialized (by the constructor defined in the generated code).
        /// Can be used to perform custom construction-time initialization.
        /// </summary>
        partial void OnItitialized()
        {
            this.Guid = Guid.NewGuid();
        }
    }
}
