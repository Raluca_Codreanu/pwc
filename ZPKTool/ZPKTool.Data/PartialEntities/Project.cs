﻿using System;
using System.Collections.Generic;

namespace ZPKTool.Data
{
    /// <summary>
    /// Extension class for the Project entity
    /// </summary>
    public partial class Project : IIdentifiable, INameable, IOwnedObject, IEntityWithMediaCollection, ICopiable, ITrashable, IReleasable
    {
        /// <summary>
        /// Gets the life cycle production quantity.
        /// </summary>
        public int? LifeCycleProductionQuantity
        {
            get
            {
                return this.YearlyProductionQuantity * this.LifeTime;
            }
        }
        
        #region ITrashable Members

        /// <summary>
        /// Gets the type of the trash bin item.
        /// </summary>
        /// <value>The type of the trash bin item.</value>
        public TrashBinItemType TrashBinItemType
        {
            get { return TrashBinItemType.Project; }
        }

        /// <summary>
        /// Sets the IsDeleted property on this instance and all ITrashable objects in its graph.
        /// </summary>
        /// <param name="newValue">the new value for the IsDeleted property.</param>
        public void SetIsDeleted(bool newValue)
        {
            if (this.IsDeleted != newValue)
            {
                this.IsDeleted = newValue;
            }

            foreach (Part part in this.Parts)
            {
                part.SetIsDeleted(newValue);
            }

            foreach (Assembly assembly in this.Assemblies)
            {
                assembly.SetIsDeleted(newValue);
            }
        }

        #endregion ITrashable Members

        /// <summary>
        /// Copies the values from this instance to another instance.
        /// </summary>
        /// <param name="destination">The destination instance.</param>
        /// <exception cref="ArgumentNullException"><paramref name="destination"/> was null</exception>
        /// <exception cref="ArgumentException"><paramref name="destination"/> did not have the expected type</exception>
        public void CopyValuesTo(object destination)
        {
            if (destination == null)
            {
                throw new ArgumentNullException("destination", "Destination was null.");
            }

            Project temp = destination as Project;
            if (temp == null)
            {
                throw new ArgumentException("Destination was not a Project.");
            }

            temp.CustomerDescription = this.CustomerDescription;
            temp.DepreciationPeriod = this.DepreciationPeriod;
            temp.DepreciationRate = this.DepreciationRate;
            temp.EndDate = this.EndDate;
            temp.IsDeleted = this.IsDeleted;
            temp.LifeTime = this.LifeTime;
            temp.LogisticCostRatio = this.LogisticCostRatio;
            temp.Name = this.Name;
            temp.Number = this.Number;
            temp.Version = this.Version;
            temp.VersionDate = this.VersionDate;
            temp.Partner = this.Partner;
            temp.ProductDescription = this.ProductDescription;
            temp.ProductName = this.ProductName;
            temp.ProductNumber = this.ProductNumber;
            temp.Remarks = this.Remarks;
            temp.SourceOverheadSettingsLastUpdate = this.SourceOverheadSettingsLastUpdate;
            temp.StartDate = this.StartDate;
            temp.Status = this.Status;
            temp.YearlyProductionQuantity = this.YearlyProductionQuantity;
            temp.LastChangeTimestamp = this.LastChangeTimestamp;
            temp.ShiftsPerWeek = this.ShiftsPerWeek;
            temp.HoursPerShift = this.HoursPerShift;
            temp.ProductionDaysPerWeek = this.ProductionDaysPerWeek;
            temp.ProductionWeeksPerYear = this.ProductionWeeksPerYear;
        }

        #region SetPropertiesInDepth

        /// <summary>
        /// Sets the owner.
        /// Also updates sub-objects
        /// </summary>
        /// <param name="newOwner">The owner.</param>
        public void SetOwner(User newOwner)
        {
            this.Owner = newOwner;

            if (this.Customer != null)
            {
                this.Customer.SetOwner(newOwner);
            }

            if (this.OverheadSettings != null)
            {
                this.OverheadSettings.SetOwner(newOwner);
            }

            foreach (Media mediaItem in this.Media)
            {
                mediaItem.SetOwner(newOwner);
            }

            foreach (Part part in this.Parts)
            {
                part.SetOwner(newOwner);
            }

            foreach (Assembly assembly in this.Assemblies)
            {
                assembly.SetOwner(newOwner);
            }
        }

        /// <summary>
        /// Sets the IsMasterData property of all object in this instance's graph that implement <see cref="IMasterDataObject"/>.
        /// </summary>
        /// <param name="newValue">The new value for the property.</param>
        public void SetIsMasterData(bool newValue)
        {
            if (this.Customer != null)
            {
                this.Customer.SetIsMasterData(newValue);
            }

            if (this.OverheadSettings != null)
            {
                // Overhead Settings that belong to other entities don not have the IsMasterData flag set even if their parent has it.
                this.OverheadSettings.SetIsMasterData(false);
            }

            foreach (Part part in this.Parts)
            {
                part.SetIsMasterData(newValue);
            }

            foreach (Assembly assembly in this.Assemblies)
            {
                assembly.SetIsMasterData(newValue);
            }
        }

        #endregion SetPropertiesInDepth

        /// <summary>
        /// Called when this instance in initialized (by the constructor defined in the generated code).
        /// Can be used to perform custom construction-time initialization.
        /// </summary>
        partial void OnItitialized()
        {
            this.Guid = Guid.NewGuid();
            this.CreateDate = DateTime.Now;
        }
    }
}