﻿using System;
using System.Collections.Generic;

namespace ZPKTool.Data
{
    /// <summary>
    /// Extension class for the Part entity
    /// </summary>
    public partial class Part : IIdentifiable, INameable, IMasterDataObject, IOwnedObject, IEntityWithMediaCollection,
        ICopiable, ITrashable
    {
        #region Constants

        /// <summary>
        /// The default value for the part's manufacturing ratio.
        /// </summary>
        public const decimal DefaultManufacturingRatio = 0.5M;

        #endregion Constants

        /// <summary>
        /// Gets the type of the trash bin item.
        /// </summary>
        /// <value>The type of the trash bin item.</value>
        public TrashBinItemType TrashBinItemType
        {
            get { return TrashBinItemType.Part; }
        }

        /// <summary>
        /// Called when this instance in initialized (by the constructor defined in the generated code).
        /// Can be used to perform custom construction-time initialization.
        /// </summary>
        partial void OnItitialized()
        {
            this.Guid = Guid.NewGuid();
            this.CreateDate = DateTime.Now;
            this.CalculationCreateDate = DateTime.Now;

            this.SBMActive = false;
            this.IsExternal = false;
            this.CalculateLogisticCost = true;
        }

        /// <summary>
        /// Copies the values from this instance to another instance.
        /// </summary>
        /// <param name="destination">The destination instance.</param>
        /// <exception cref="ArgumentNullException"><paramref name="destination"/> was null</exception>
        /// <exception cref="ArgumentException"><paramref name="destination"/> did not have the expected type</exception>
        public void CopyValuesTo(object destination)
        {
            if (destination == null)
            {
                throw new ArgumentNullException("destination", "Destination was null.");
            }

            Part temp = destination as Part;
            if (temp == null)
            {
                throw new ArgumentException("Destination was not a Part.");
            }

            temp.AdditionalDescription = this.AdditionalDescription;
            temp.AdditionalRemarks = this.AdditionalRemarks;
            temp.AssetRate = this.AssetRate;
            temp.BatchSizePerYear = this.BatchSizePerYear;
            temp.CalculateLogisticCost = this.CalculateLogisticCost;
            temp.CalculationAccuracy = this.CalculationAccuracy;
            temp.CalculationApproach = this.CalculationApproach;
            temp.CalculationStatus = this.CalculationStatus;
            temp.DelivertType = this.DelivertType;
            temp.Description = this.Description;
            temp.DevelopmentCost = this.DevelopmentCost;
            temp.EstimatedCost = this.EstimatedCost;
            temp.IsDeleted = this.IsDeleted;
            temp.IsExternal = this.IsExternal;
            temp.IsMasterData = this.IsMasterData;
            temp.LifeTime = this.LifeTime;
            temp.LogisticCost = this.LogisticCost;
            temp.ManufacturerDescription = this.ManufacturerDescription;
            temp.Name = this.Name;
            temp.Number = this.Number;
            temp.Version = this.Version;
            temp.VersionDate = this.VersionDate;
            temp.OtherCost = this.OtherCost;
            temp.PackagingCost = this.PackagingCost;
            temp.PaymentTerms = this.PaymentTerms;
            temp.ProjectInvest = this.ProjectInvest;
            temp.PurchasePrice = this.PurchasePrice;
            temp.SBMActive = this.SBMActive;
            temp.TargetPrice = this.TargetPrice;
            temp.YearlyProductionQuantity = this.YearlyProductionQuantity;
            temp.LastChangeTimestamp = this.LastChangeTimestamp;
            temp.CalculationVariant = this.CalculationVariant;
            temp.ManufacturingRatio = this.ManufacturingRatio;
            temp.ManufacturingCountry = this.ManufacturingCountry;
            temp.ManufacturingSupplier = this.ManufacturingSupplier;
            temp.Reusable = this.Reusable;
            temp.Weight = this.Weight;
            temp.TransportCost = this.TransportCost;
            temp.Index = this.Index;
            temp.AreAdditionalCostsPerPartsTotal = this.AreAdditionalCostsPerPartsTotal;
            temp.ManufacturingCountryId = this.ManufacturingCountryId;
            temp.ExternalSGA = this.ExternalSGA;
            temp.CalculationCreateDate = this.CalculationCreateDate;

            if (this.AdditionalCostsNotes != null)
            {
                temp.AdditionalCostsNotes = this.AdditionalCostsNotes;
            }
        }

        /// <summary>
        /// Sets the IsDeleted property on this instance and all ITrashable objects in its graph.
        /// </summary>
        /// <param name="newValue">the new value for the IsDeleted property.</param>
        public void SetIsDeleted(bool newValue)
        {
            if (this.IsDeleted != newValue)
            {
                this.IsDeleted = newValue;
            }

            foreach (RawMaterial material in this.RawMaterials)
            {
                material.SetIsDeleted(newValue);
            }

            foreach (Commodity comm in this.Commodities)
            {
                comm.SetIsDeleted(newValue);
            }

            if (this.Process != null)
            {
                this.Process.SetIsDeleted(newValue);
            }

            if (this.RawPart != null)
            {
                this.RawPart.SetIsDeleted(newValue);
            }
        }

        /// <summary>
        /// Sets the value of the <see cref="Owner " /> property on this instance and on all the objects in its graph that implement <see cref="IOwnedObject" />.
        /// </summary>
        /// <param name="newOwner">The new owner.</param>
        public void SetOwner(User newOwner)
        {
            this.Owner = newOwner;
            if (this.Manufacturer != null)
            {
                this.Manufacturer.SetOwner(newOwner);
            }

            if (this.CountrySettings != null)
            {
                this.CountrySettings.SetOwner(newOwner);
            }

            if (this.OverheadSettings != null)
            {
                this.OverheadSettings.SetOwner(newOwner);
            }

            foreach (RawMaterial rawMaterial in this.RawMaterials)
            {
                rawMaterial.SetOwner(newOwner);
            }

            foreach (Commodity commodity in this.Commodities)
            {
                commodity.SetOwner(newOwner);
            }

            foreach (Media value in this.Media)
            {
                value.SetOwner(newOwner);
            }

            foreach (TransportCostCalculation calculation in TransportCostCalculations)
            {
                calculation.SetOwner(newOwner);
            }

            if (this.Process != null)
            {
                this.Process.SetOwner(newOwner);
            }

            if (this.RawPart != null)
            {
                this.RawPart.SetOwner(newOwner);
            }
        }

        /// <summary>
        /// Sets the value of the <see cref="IsMasterData" /> property on this instance and on all the objects in its graph that implement <see cref="IMasterDataObject" />.
        /// </summary>
        /// <param name="newValue">The new value for the <see cref="IsMasterData" /> property.</param>
        public void SetIsMasterData(bool newValue)
        {
            this.IsMasterData = newValue;
            if (this.Manufacturer != null)
            {
                this.Manufacturer.SetIsMasterData(newValue);
            }

            if (this.OverheadSettings != null)
            {
                // Overhead Settings that belong to other entities don not have the IsMasterData flag set even if their parent has it.
                this.OverheadSettings.SetIsMasterData(false);
            }

            foreach (RawMaterial rawMaterial in this.RawMaterials)
            {
                rawMaterial.SetIsMasterData(newValue);
            }

            foreach (Commodity commodity in this.Commodities)
            {
                commodity.SetIsMasterData(newValue);
            }

            if (this.Process != null)
            {
                this.Process.SetIsMasterData(newValue);
            }

            foreach (Media mediaItem in this.Media)
            {
                mediaItem.SetIsMasterData(newValue);
            }

            if (this.RawPart != null)
            {
                this.RawPart.SetIsMasterData(newValue);
            }
        }

        /// <summary>
        /// Sets the calculation variant for this instance and all objects in its graph.
        /// </summary>
        /// <param name="newValue">The new calculation variant value.</param>
        public void SetCalculationVariant(string newValue)
        {
            CalculationVariant = newValue;

            if (RawPart != null)
            {
                RawPart.CalculationVariant = newValue;
            }
        }
    }
}