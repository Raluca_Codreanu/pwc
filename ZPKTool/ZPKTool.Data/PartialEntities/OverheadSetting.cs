﻿using System;

namespace ZPKTool.Data
{
    /// <summary>
    /// This class implements the OverheadSettings concept.
    /// </summary>
    public partial class OverheadSetting : IIdentifiable, IMasterDataObject, IOwnedObject, ICopiable
    {
        /// <summary>
        /// Returns a new instance with all properties that represent table columns copied from this one.        
        /// For safety, the IsMasterData property is set to false.
        /// </summary>
        /// <returns>A new instance containing a copy of this one.</returns>
        public OverheadSetting Copy()
        {
            OverheadSetting copy = new OverheadSetting();
            this.CopyValuesTo(copy);
            copy.IsMasterData = false;

            return copy;
        }

        #region ICopiable Members

        /// <summary>
        /// Copies the values from this instance to another instance.
        /// </summary>
        /// <param name="destination">The destination instance.</param>
        /// <exception cref="ArgumentNullException"><paramref name="destination"/> was null</exception>
        /// <exception cref="ArgumentException"><paramref name="destination"/> did not have the expected type</exception>
        public void CopyValuesTo(object destination)
        {
            if (destination == null)
            {
                throw new ArgumentNullException("destination", "Destination was null.");
            }

            OverheadSetting temp = destination as OverheadSetting;
            if (temp == null)
            {
                throw new ArgumentException("Destination was not an OverheadSetting.");
            }

            if (this.CommodityMargin != temp.CommodityMargin)
            {
                temp.CommodityMargin = this.CommodityMargin;
            }

            if (this.CommodityOverhead != temp.CommodityOverhead)
            {
                temp.CommodityOverhead = this.CommodityOverhead;
            }

            if (this.ConsumableMargin != temp.ConsumableMargin)
            {
                temp.ConsumableMargin = this.ConsumableMargin;
            }

            if (this.ConsumableOverhead != temp.ConsumableOverhead)
            {
                temp.ConsumableOverhead = this.ConsumableOverhead;
            }

            if (this.ExternalWorkMargin != temp.ExternalWorkMargin)
            {
                temp.ExternalWorkMargin = this.ExternalWorkMargin;
            }

            if (this.ExternalWorkOverhead != temp.ExternalWorkOverhead)
            {
                temp.ExternalWorkOverhead = this.ExternalWorkOverhead;
            }

            if (this.LogisticOHValue != temp.LogisticOHValue)
            {
                temp.LogisticOHValue = this.LogisticOHValue;
            }

            if (this.ManufacturingMargin != temp.ManufacturingMargin)
            {
                temp.ManufacturingMargin = this.ManufacturingMargin;
            }

            if (this.ManufacturingOverhead != temp.ManufacturingOverhead)
            {
                temp.ManufacturingOverhead = this.ManufacturingOverhead;
            }

            if (this.MaterialMargin != temp.MaterialMargin)
            {
                temp.MaterialMargin = this.MaterialMargin;
            }

            if (this.MaterialOverhead != temp.MaterialOverhead)
            {
                temp.MaterialOverhead = this.MaterialOverhead;
            }

            if (this.OtherCostOHValue != temp.OtherCostOHValue)
            {
                temp.OtherCostOHValue = this.OtherCostOHValue;
            }

            if (this.PackagingOHValue != temp.PackagingOHValue)
            {
                temp.PackagingOHValue = this.PackagingOHValue;
            }

            if (temp.LastUpdateTime != this.LastUpdateTime)
            {
                temp.LastUpdateTime = this.LastUpdateTime;
            }

            if (temp.SalesAndAdministrationOHValue != this.SalesAndAdministrationOHValue)
            {
                temp.SalesAndAdministrationOHValue = this.SalesAndAdministrationOHValue;
            }

            if (temp.CompanySurchargeOverhead != this.CompanySurchargeOverhead)
            {
                temp.CompanySurchargeOverhead = this.CompanySurchargeOverhead;
            }
        }

        /// <summary>
        /// Compares the values of this instance's properties with another OverheadSetting object type.
        /// </summary>
        /// <param name="comparison">The overhead settings of the comparison object.</param>
        /// <returns>True if the properties of the objects are the same, else otherwise</returns>
        public bool CompareValuesTo(OverheadSetting comparison)
        {
            if (comparison == null)
            {
                throw new ArgumentNullException("comparison", "overheadSettingsToCompareTo was null.");
            }

            if (this.MaterialOverhead != comparison.MaterialOverhead ||
                this.CommodityOverhead != comparison.CommodityOverhead ||
                this.ManufacturingOverhead != comparison.ManufacturingOverhead ||
                this.PackagingOHValue != comparison.PackagingOHValue ||
                this.SalesAndAdministrationOHValue != comparison.SalesAndAdministrationOHValue ||
                this.ConsumableOverhead != comparison.ConsumableOverhead ||
                this.ExternalWorkOverhead != comparison.ExternalWorkOverhead ||
                this.OtherCostOHValue != comparison.OtherCostOHValue ||
                this.LogisticOHValue != comparison.LogisticOHValue ||
                this.MaterialMargin != comparison.MaterialMargin ||
                this.CommodityMargin != comparison.CommodityMargin ||
                this.ManufacturingMargin != comparison.ManufacturingMargin ||
                this.ConsumableMargin != comparison.ConsumableMargin ||
                this.ExternalWorkMargin != comparison.ExternalWorkMargin ||
                this.CompanySurchargeOverhead != comparison.CompanySurchargeOverhead)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        #endregion ICopiable Members

        #region IOwnedObject Members

        /// <summary>
        /// Sets the value of the <see cref="Owner " /> property on this instance and on all the objects in its graph that implement <see cref="IOwnedObject" />.
        /// </summary>
        /// <param name="newOwner">The new owner.</param>
        public void SetOwner(User newOwner)
        {
            this.Owner = newOwner;
        }

        #endregion IOwnedObject Members

        #region IMasterDataObject Members

        /// <summary>
        /// Sets the value of the <see cref="IsMasterData" /> property on this instance and on all the objects in its graph that implement <see cref="IMasterDataObject" />.
        /// </summary>
        /// <param name="newValue">The new value for the <see cref="IsMasterData" /> property.</param>
        public void SetIsMasterData(bool newValue)
        {
            this.IsMasterData = newValue;
        }

        #endregion

        /// <summary>
        /// Called when this instance in initialized (by the constructor defined in the generated code).
        /// Can be used to perform custom construction-time initialization.
        /// </summary>
        partial void OnItitialized()
        {
            this.Guid = Guid.NewGuid();
            this.CreateDate = DateTime.Now;
        }
    }
}