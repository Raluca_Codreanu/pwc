﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Data
{
    /// <summary>
    /// Extension class for the passwordsHistory entity.
    /// </summary>
    public partial class PasswordsHistory : IIdentifiable
    {
        /// <summary>
        /// Called when this instance in initialized (by the constructor defined in the generated code).
        /// Can be used to perform custom construction-time initialization.
        /// </summary>
        partial void OnItitialized()
        {
            this.Guid = Guid.NewGuid();
            this.Date = DateTime.Now;
        }
    }
}
