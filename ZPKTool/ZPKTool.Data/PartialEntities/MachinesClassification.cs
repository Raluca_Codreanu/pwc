﻿using System;

namespace ZPKTool.Data
{
    /// <summary>
    /// Extension class for the MachinesClassification entity
    /// </summary>
    public partial class MachinesClassification : IIdentifiable, INameable, ICopiable, IReleasable
    {
        #region ICopiable Members

        /// <summary>
        /// Copies the values from this instance to another instance.
        /// </summary>
        /// <param name="destination">The destination instance.</param>
        /// <exception cref="ArgumentNullException"><paramref name="destination"/> was null</exception>
        /// <exception cref="ArgumentException"><paramref name="destination"/> did not have the expected type</exception>
        public void CopyValuesTo(object destination)
        {
            if (destination == null)
            {
                throw new ArgumentNullException("destination", "Destination was null for MachinesClassification.");
            }

            MachinesClassification temp = destination as MachinesClassification;
            if (temp == null)
            {
                throw new ArgumentException("Destination was not a MachinesClassification.");
            }

            if (this.Name != null)
            {
                temp.Name = this.Name;
            }
        }

        #endregion ICopiable Members

        /// <summary>
        /// Called when this instance in initialized (by the constructor defined in the generated code).
        /// Can be used to perform custom construction-time initialization.
        /// </summary>
        partial void OnItitialized()
        {
            this.Guid = Guid.NewGuid();
            this.CreateDate = DateTime.Now;
        }
    }
}
