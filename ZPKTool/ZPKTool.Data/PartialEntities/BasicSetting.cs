﻿using System;

namespace ZPKTool.Data
{
    /// <summary>
    /// Extension of the BasicSetting entity.
    /// </summary>
    public partial class BasicSetting : IIdentifiable, ICopiable
    {
        #region ICopiable Members

        /// <summary>
        /// Copies the values from this instance to another instance.
        /// </summary>
        /// <param name="destination">The destination instance.</param>
        /// <exception cref="ArgumentNullException"><paramref name="destination"/> was null</exception>
        /// <exception cref="ArgumentException"><paramref name="destination"/> did not have the expected type</exception>
        public void CopyValuesTo(object destination)
        {
            if (destination == null)
            {
                throw new ArgumentNullException("destination", "Destination was null.");
            }

            BasicSetting temp = destination as BasicSetting;
            if (temp == null)
            {
                throw new ArgumentException("Destination was not a BasicSetting.", "destination");
            }

            if (temp.AssetRate != this.AssetRate)
            {
                temp.AssetRate = this.AssetRate;
            }

            if (temp.DeprPeriod != this.DeprPeriod)
            {
                temp.DeprPeriod = this.DeprPeriod;
            }

            if (temp.DeprRate != this.DeprRate)
            {
                temp.DeprRate = this.DeprRate;
            }

            if (temp.HoursPerShift != this.HoursPerShift)
            {
                temp.HoursPerShift = this.HoursPerShift;
            }

            if (temp.LogisticCostRatio != this.LogisticCostRatio)
            {
                temp.LogisticCostRatio = this.LogisticCostRatio;
            }

            if (temp.PartBatch != this.PartBatch)
            {
                temp.PartBatch = this.PartBatch;
            }

            if (temp.ProdDays != this.ProdDays)
            {
                temp.ProdDays = this.ProdDays;
            }

            if (temp.ProdWeeks != this.ProdWeeks)
            {
                temp.ProdWeeks = this.ProdWeeks;
            }

            if (temp.ShiftsPerWeek != this.ShiftsPerWeek)
            {
                temp.ShiftsPerWeek = this.ShiftsPerWeek;
            }
        }

        #endregion
    }
}
