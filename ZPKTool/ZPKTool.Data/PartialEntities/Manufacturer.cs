﻿using System;

namespace ZPKTool.Data
{
    /// <summary>
    /// Extension class for the Manufacturer entity.
    /// </summary>
    public partial class Manufacturer : IIdentifiable, INameable, IMasterDataObject, IOwnedObject, ICopiable
    {
        #region IOwnedObject Members

        /// <summary>
        /// Sets the value of the <see cref="Owner " /> property on this instance and on all the objects in its graph that implement <see cref="IOwnedObject" />.
        /// </summary>
        /// <param name="newOwner">The new owner.</param>
        public void SetOwner(User newOwner)
        {
            this.Owner = newOwner;
        }

        #endregion

        /// <summary>
        /// Copies this instance.
        /// </summary>
        /// <returns>An instance of Machine.</returns>
        public Manufacturer Copy()
        {
            Manufacturer manufacturer = new Manufacturer();

            CopyValuesTo(manufacturer);
            manufacturer.IsMasterData = false;

            return manufacturer;
        }

        /// <summary>
        /// Sets the value of the <see cref="IsMasterData" /> property on this instance and on all the objects in its graph that implement <see cref="IMasterDataObject" />.
        /// </summary>
        /// <param name="newValue">The new value for the <see cref="IsMasterData" /> property.</param>
        public void SetIsMasterData(bool newValue)
        {
            this.IsMasterData = newValue;
        }

        #region ICopiable Members

        /// <summary>
        /// Copies the values from this instance to another instance.
        /// </summary>
        /// <param name="destination">The destination instance.</param>
        /// <exception cref="ArgumentNullException"><paramref name="destination"/> was null</exception>
        /// <exception cref="ArgumentException"><paramref name="destination"/> did not have the expected type</exception>
        public void CopyValuesTo(object destination)
        {
            if (destination == null)
            {
                throw new ArgumentNullException("destination", "Destination was null.");
            }

            Manufacturer temp = destination as Manufacturer;
            if (temp == null)
            {
                throw new ArgumentException("Destination was not a Manufacturer.");
            }

            if (this.Description != null)
            {
                temp.Description = this.Description;
            }

            temp.IsMasterData = this.IsMasterData;

            if (this.Name != null)
            {
                temp.Name = this.Name;
            }
        }

        #endregion

        /// <summary>
        /// Called when this instance in initialized (by the constructor defined in the generated code).
        /// Can be used to perform custom construction-time initialization.
        /// </summary>
        partial void OnItitialized()
        {
            this.Guid = Guid.NewGuid();
            this.CreateDate = DateTime.Now;
        }
    }
}
