﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Data
{
    /// <summary>
    /// Extension class for the Log entity.
    /// </summary>
    public partial class Log : IIdentifiable, ICopiable
    {
        /// <summary>
        /// Gets or sets a wrapper property which performs the encrypt/decrypt operation over the Details.
        /// </summary>
        public string Details
        {
            get
            {
                if (this.Type == LogEventType.LoginSuccessful
                    || this.Type == LogEventType.LoginFailed
                    || this.Type == LogEventType.CreatedUser
                    || this.Type == LogEventType.EditedUser)
                {
                   return Encryption.Decrypt(EncryptedDetails, Encryption.EncryptionPassword);
                }

                return EncryptedDetails;
            }

            set
            {
                if (this.Type == LogEventType.LoginSuccessful
                    || this.Type == LogEventType.LoginFailed
                    || this.Type == LogEventType.CreatedUser
                    || this.Type == LogEventType.EditedUser)
                {
                    EncryptedDetails = Encryption.Encrypt(value, Encryption.EncryptionPassword);
                }
                else
                {
                    EncryptedDetails = value;
                }

                this.OnPropertyChanged("Details");
            }
        }

        /// <summary>
        /// Gets or sets the ID of the object.
        /// </summary>
        public Guid Guid
        {
            get
            {
                return this.ID;
            }

            set
            {
                this.ID = value;
            }
        }

        /// <summary>
        /// Copies the values from this instance to another instance.
        /// </summary>
        /// <param name="destination">The destination instance.</param>
        /// <exception cref="ArgumentNullException"><paramref name="destination"/> was null</exception>
        /// <exception cref="ArgumentException"><paramref name="destination"/> did not have the expected type</exception>
        public void CopyValuesTo(object destination)
        {
            if (destination == null)
            {
                throw new ArgumentNullException("destination", "Destination was null.");
            }

            Log temp = destination as Log;
            if (temp == null)
            {
                throw new ArgumentException("Destination was not a Log.");
            }

            if (temp.Type != this.Type)
            {
                temp.Type = this.Type;
            }

            if (temp.Details != this.Details)
            {
                temp.Details = this.Details;
            }

            if (temp.ComputerName != this.ComputerName)
            {
                temp.ComputerName = this.ComputerName;
            }
        }

        /// <summary>
        /// Called when this instance in initialized (by the constructor defined in the generated code).
        /// Can be used to perform custom construction-time initialization.
        /// </summary>
        partial void OnItitialized()
        {
            this.ID = Guid.NewGuid();
            this.Date = DateTime.Now;
        }
    }
}
