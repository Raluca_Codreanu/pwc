﻿using System;

namespace ZPKTool.Data
{
    /// <summary>
    /// Partial implementation of the TrashBinItem entity object
    /// </summary>
    public partial class TrashBinItem : IIdentifiable, IOwnedObject, ICopiable
    {        
        #region IOwnedObject Members

        /// <summary>
        /// Sets the value of the <see cref="Owner " /> property on this instance and on all the objects in its graph that implement <see cref="IOwnedObject" />.
        /// </summary>
        /// <param name="newOwner">The new owner.</param>
        public void SetOwner(User newOwner)
        {
            this.Owner = newOwner;
        }

        #endregion

        #region ICopiable Members

        /// <summary>
        /// Copies the values from this instance to another instance.
        /// </summary>
        /// <param name="destination">The destination instance.</param>
        /// <exception cref="ArgumentNullException"><paramref name="destination"/> was null</exception>
        /// <exception cref="ArgumentException"><paramref name="destination"/> did not have the expected type</exception>
        public void CopyValuesTo(object destination)
        {
            if (destination == null)
            {
                throw new ArgumentNullException("destination", "Destination was null.");
            }

            TrashBinItem temp = destination as TrashBinItem;
            if (temp == null)
            {
                throw new ArgumentException("Destination was not a TrashBinItem.");
            }
            
            temp.EntityType = this.EntityType;
            temp.EntityGuid = this.EntityGuid;
            temp.EntityMediaGuid = this.EntityMediaGuid;
            temp.EntityName = this.EntityName;
        }

        #endregion

        /// <summary>
        /// Called when this instance in initialized (by the constructor defined in the generated code).
        /// Can be used to perform custom construction-time initialization.
        /// </summary>
        partial void OnItitialized()
        {
            this.Guid = Guid.NewGuid();
            this.CreateDate = DateTime.Now;
        }
    }
}
