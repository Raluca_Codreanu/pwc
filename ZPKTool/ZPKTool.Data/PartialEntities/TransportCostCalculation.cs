﻿using System;

namespace ZPKTool.Data
{
    /// <summary>
    /// Partial implementation of the TransportCostCalculation entity object
    /// </summary>
    public partial class TransportCostCalculation : IIdentifiable, ICopiable, IOwnedObject
    {
        #region ICopiable Members

        /// <summary>
        /// Copies the values from this instance to another instance.
        /// </summary>
        /// <param name="destination">The destination instance.</param>
        /// <exception cref="ArgumentNullException"><paramref name="destination"/> was null</exception>
        /// <exception cref="ArgumentException"><paramref name="destination"/> did not have the expected type</exception>
        public void CopyValuesTo(object destination)
        {
            if (destination == null)
            {
                throw new ArgumentNullException("destination", "Destination CycleTimeCalculation was null.");
            }

            if (destination.GetType() != typeof(TransportCostCalculation))
            {
                throw new ArgumentException("Destination was not a CycleTimeCalculation.");
            }

            TransportCostCalculation temp = (TransportCostCalculation)destination;
            temp.Index = this.Index;
            temp.PartName = this.PartName;
            temp.PartNumber = this.PartNumber;
            temp.PartWeight = this.PartWeight;
            temp.Supplier = this.Supplier;
            temp.SourceLocation = this.SourceLocation;
            temp.SourceCountry = this.SourceCountry;
            temp.SourceZipCode = this.SourceZipCode;
            temp.SourceLong = this.SourceLong;
            temp.SourceLat = this.SourceLat;
            temp.DestinationLocation = this.DestinationLocation;
            temp.DestinationCountry = this.DestinationCountry;
            temp.DestinationZipCode = this.DestinationZipCode;
            temp.DestinationLong = this.DestinationLong;
            temp.DestinationLat = this.DestinationLat;
            temp.Distance = this.Distance;
            temp.DailyNeededQuantity = this.DailyNeededQuantity;
            temp.PartType = this.PartType;
            temp.QuantityPerPack = this.QuantityPerPack;
            temp.PackagingType = this.PackagingType;
            temp.Description = this.Description;
            temp.CostByPackage = this.CostByPackage;
            temp.PackageWeight = this.PackageWeight;
            temp.BouncingBoxLength = this.BouncingBoxLength;
            temp.BouncingBoxWidth = this.BouncingBoxWidth;
            temp.BouncingBoxHeight = this.BouncingBoxHeight;
            temp.Interference = this.Interference;
            temp.PackingDensity = this.PackingDensity;
            temp.CarrierType = this.CarrierType;
            temp.NetWeight = this.NetWeight;
            temp.CarrierLength = this.CarrierLength;
            temp.CarrierHeight = this.CarrierHeight;
            temp.CarrierWidth = this.CarrierWidth;
            temp.MaxPackageCarrier = this.MaxPackageCarrier;
            temp.MaxLoadCapacity = this.MaxLoadCapacity;
            temp.TransporterType = this.TransporterType;
            temp.TransporterLength = this.TransporterLength;
            temp.TransporterWidth = this.TransporterWidth;
            temp.TransporterHeight = this.TransporterHeight;
            temp.TransporterCostPerKm = this.TransporterCostPerKm;
        }

        #endregion

        #region Set Properties in Depth

        /// <summary>
        /// Sets the owner.
        /// Also updates sub-objects
        /// </summary>
        /// <param name="newOwner">The owner.</param>
        public void SetOwner(User newOwner)
        {
            this.Owner = newOwner;
            this.transportCostCalculationSetting.SetOwner(newOwner);
        }

        #endregion

        /// <summary>
        /// Called when this instance in initialized (by the constructor defined in the generated code).
        /// Can be used to perform custom construction-time initialization.
        /// </summary>
        partial void OnItitialized()
        {
            this.Guid = Guid.NewGuid();
            this.CreateDate = DateTime.Now;
        }
    }
}
