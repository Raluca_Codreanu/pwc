﻿using System;

namespace ZPKTool.Data
{
    /// <summary>
    /// Extension class for the Global Setting entity.
    /// </summary>
    public partial class GlobalSetting : IIdentifiable
    {
        /// <summary>
        /// Gets or sets a wrapper property which performs the encrypt/decrypt operation over the Key.
        /// </summary>
        public string Key
        {
            get
            {
                return Encryption.Decrypt(EncryptedKey, Encryption.EncryptionPassword);
            }

            set
            {
                EncryptedKey = Encryption.Encrypt(value, Encryption.EncryptionPassword);
                this.OnPropertyChanged("Key");
            }
        }

        /// <summary>
        /// Gets or sets a wrapper property which performs the encrypt/decrypt operation over the Value.
        /// </summary>
        public string Value
        {
            get
            {
                return Encryption.Decrypt(EncryptedValue, Encryption.EncryptionPassword);
            }

            set
            {
                EncryptedValue = Encryption.Encrypt(value, Encryption.EncryptionPassword);
                this.OnPropertyChanged("Value");
            }
        }

        /// <summary>
        /// Called when this instance in initialized (by the constructor defined in the generated code).
        /// Can be used to perform custom construction-time initialization.
        /// </summary>
        partial void OnItitialized()
        {
            this.Guid = Guid.NewGuid();
        }
    }
}
