﻿using System;

namespace ZPKTool.Data
{
    /// <summary>
    /// Partial implementation of the Currency entity object
    /// </summary>
    public partial class Currency : IIdentifiable, INameable, ICopiable, IReleasable, IMasterDataObject
    {
        #region ICopiable Members

        /// <summary>
        /// Copies the values from this instance to another instance.
        /// </summary>
        /// <param name="destination">The destination instance.</param>
        /// <exception cref="ArgumentNullException"><paramref name="destination"/> was null</exception>
        /// <exception cref="ArgumentException"><paramref name="destination"/> did not have the expected type</exception>
        public void CopyValuesTo(object destination)
        {
            if (destination == null)
            {
                throw new ArgumentNullException("destination", "Destination was null.");
            }

            Currency temp = destination as Currency;
            if (temp == null)
            {
                throw new ArgumentException("Destination was not a Currency.");
            }

            if (temp.ExchangeRate != this.ExchangeRate)
            {
                temp.ExchangeRate = this.ExchangeRate;
            }

            if (temp.Name != this.Name)
            {
                temp.Name = this.Name;
            }

            if (temp.Symbol != this.Symbol)
            {
                temp.Symbol = this.Symbol;
            }

            if (temp.IsoCode != this.IsoCode)
            {
                temp.IsoCode = this.IsoCode;
            }

            if (temp.IsMasterData != this.IsMasterData)
            {
                temp.IsMasterData = this.IsMasterData;
            }
        }

        #endregion

        /// <summary>
        /// Called when this instance in initialized (by the constructor defined in the generated code).
        /// Can be used to perform custom construction-time initialization.
        /// </summary>
        partial void OnItitialized()
        {
            this.Guid = Guid.NewGuid();
            this.CreateDate = DateTime.Now;
        }

        /// <summary>
        /// Sets the value of the <see cref="IsMasterData" /> property on this instance and on all the objects in its graph that implement <see cref="IMasterDataObject" />.
        /// </summary>
        /// <param name="newValue">The new value for the <see cref="IsMasterData" /> property.</param>
        public void SetIsMasterData(bool newValue)
        {
            this.IsMasterData = newValue;
        }

        /// <summary>
        /// Determines whether this instance represents the same currency as the specified instance.
        /// </summary>
        /// <param name="value">The instance to compare with.</param>
        /// <returns>
        /// true if both instances represent the same currency; otherwise, false.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">The currency to compare with can not be null.</exception>
        public bool IsSameAs(Currency value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value", "The currency to compare with can not be null.");
            }

            return this.IsoCode == value.IsoCode;
        }
    }
}
