﻿using System;

namespace ZPKTool.Data
{
    /// <summary>
    /// Partial implementation of the CycleTimeCalculation entity object
    /// </summary>
    public partial class CycleTimeCalculation : IIdentifiable, IMasterDataObject, ICopiable
    {
        #region ICopiable Members

        /// <summary>
        /// Copies the values from this instance to another instance.
        /// </summary>
        /// <param name="destination">The destination instance.</param>
        /// <exception cref="ArgumentNullException"><paramref name="destination"/> was null</exception>
        /// <exception cref="ArgumentException"><paramref name="destination"/> did not have the expected type</exception>
        public void CopyValuesTo(object destination)
        {
            if (destination == null)
            {
                throw new ArgumentNullException("destination", "Destination CycleTimeCalculation was null.");
            }

            CycleTimeCalculation temp = destination as CycleTimeCalculation;
            if (temp == null)
            {
                throw new ArgumentException("Destination was not a CycleTimeCalculation.");
            }
                        
            temp.Description = this.Description;
            temp.Index = this.Index;
            temp.IsMasterData = this.IsMasterData;
            temp.MachiningTime = this.MachiningTime;
            temp.MachiningType = this.MachiningType;
            temp.MachiningVolume = this.MachiningVolume;
            temp.RawPartDescription = this.RawPartDescription;
            temp.SurfaceType = this.SurfaceType;
            temp.TakeOffTime = this.TakeOffTime;
            temp.ToleranceType = this.ToleranceType;
            temp.ToolChangeTime = this.ToolChangeTime;
            temp.TransportClampingTime = this.TransportClampingTime;
            temp.TransportTimeFromBuffer = this.TransportTimeFromBuffer;

            // Make a copy of the binary data because the simple property assignment will just copy the reference.
            if (this.MachiningCalculationData != null && this.MachiningCalculationData.Length > 0)
            {
                byte[] copy = new byte[this.MachiningCalculationData.LongLength];
                this.MachiningCalculationData.CopyTo(copy, 0);
                temp.MachiningCalculationData = copy;
            }
        }

        #endregion

        #region Set Properties in Depth

        /// <summary>
        /// Sets the owner.
        /// </summary>
        /// <param name="newOwner">The new owner.</param>
        public void SetOwner(User newOwner)
        {
            this.Owner = newOwner;
        }

        /// <summary>
        /// Sets the value of the <see cref="IsMasterData" /> property on this instance and on all the objects in its graph that implement <see cref="IMasterDataObject" />.
        /// </summary>
        /// <param name="newValue">The new value for the <see cref="IsMasterData" /> property.</param>
        public void SetIsMasterData(bool newValue)
        {
            this.IsMasterData = newValue;
        }

        #endregion Set Properties in Depth

        /// <summary>
        /// Called when this instance in initialized (by the constructor defined in the generated code).
        /// Can be used to perform custom construction-time initialization.
        /// </summary>
        partial void OnItitialized()
        {
            this.Guid = Guid.NewGuid();
            this.CreateDate = DateTime.Now;
        }
    }
}
