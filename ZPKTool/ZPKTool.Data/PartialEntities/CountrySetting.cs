﻿using System;

namespace ZPKTool.Data
{
    /// <summary>
    /// Implements the Country Settings concept.
    /// </summary>
    public partial class CountrySetting : IIdentifiable, IOwnedObject, ICopiable
    {
        /// <summary>
        /// The accepted deviation when comparing two values.
        /// </summary>
        public const decimal CompareAcceptedDeviation = 0.000000000000001m;

        #region Property wrappers for scrambling/unscrambling internal properties

        /// <summary>
        /// Gets or sets the Unskilled Labor Cost.
        /// </summary>
        public decimal UnskilledLaborCost
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledUnskilledLaborCost;
                }
                else
                {
                    return MasterDataScrambler.UnscrambleNumber(ScrambledUnskilledLaborCost);
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledUnskilledLaborCost = value;
                }
                else
                {
                    value = EntityUtils.PrepareNumberForScrambling(value);
                    ScrambledUnskilledLaborCost = MasterDataScrambler.ScrambleNumber(value);
                }

                this.OnPropertyChanged("UnskilledLaborCost");
            }
        }

        /// <summary>
        /// Gets or sets Skilled Labor Cost.
        /// </summary>
        public decimal SkilledLaborCost
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledSkilledLaborCost;
                }
                else
                {
                    return MasterDataScrambler.UnscrambleNumber(ScrambledSkilledLaborCost);
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledSkilledLaborCost = value;
                }
                else
                {
                    value = EntityUtils.PrepareNumberForScrambling(value);
                    ScrambledSkilledLaborCost = MasterDataScrambler.ScrambleNumber(value);
                }

                this.OnPropertyChanged("SkilledLaborCost");
            }
        }

        /// <summary>
        /// Gets or sets ForemanCost field
        /// </summary>
        public decimal ForemanCost
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledForemanCost;
                }
                else
                {
                    return MasterDataScrambler.UnscrambleNumber(ScrambledForemanCost);
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledForemanCost = value;
                }
                else
                {
                    value = EntityUtils.PrepareNumberForScrambling(value);
                    ScrambledForemanCost = MasterDataScrambler.ScrambleNumber(value);
                }

                this.OnPropertyChanged("ForemanCost");
            }
        }

        /// <summary>
        /// Gets or sets Technician Cost.
        /// </summary>
        public decimal TechnicianCost
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledTechnicianCost;
                }
                else
                {
                    return MasterDataScrambler.UnscrambleNumber(ScrambledTechnicianCost);
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledTechnicianCost = value;
                }
                else
                {
                    value = EntityUtils.PrepareNumberForScrambling(value);
                    ScrambledTechnicianCost = MasterDataScrambler.ScrambleNumber(value);
                }

                this.OnPropertyChanged("TechnicianCost");
            }
        }

        /// <summary>
        /// Gets or sets the Engineer Cost.
        /// </summary>
        public decimal EngineerCost
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledEngineerCost;
                }
                else
                {
                    return MasterDataScrambler.UnscrambleNumber(ScrambledEngineerCost);
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledEngineerCost = value;
                }
                else
                {
                    value = EntityUtils.PrepareNumberForScrambling(value);
                    ScrambledEngineerCost = MasterDataScrambler.ScrambleNumber(value);
                }

                this.OnPropertyChanged("EngineerCost");
            }
        }

        /// <summary>
        /// Gets or sets the Energy Cost.
        /// </summary>
        public decimal EnergyCost
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledEnergyCost;
                }
                else
                {
                    return MasterDataScrambler.UnscrambleNumber(ScrambledEnergyCost);
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledEnergyCost = value;
                }
                else
                {
                    value = EntityUtils.PrepareNumberForScrambling(value);
                    ScrambledEnergyCost = MasterDataScrambler.ScrambleNumber(value);
                }

                this.OnPropertyChanged("EnergyCost");
            }
        }

        /// <summary>
        /// Gets or sets the Water Cost.
        /// </summary>
        public decimal WaterCost
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledWaterCost;
                }
                else
                {
                    return MasterDataScrambler.UnscrambleNumber(ScrambledWaterCost);
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledWaterCost = value;
                }
                else
                {
                    value = EntityUtils.PrepareNumberForScrambling(value);
                    ScrambledWaterCost = MasterDataScrambler.ScrambleNumber(value);
                }

                this.OnPropertyChanged("WaterCost");
            }
        }

        /// <summary>
        /// Gets or sets the Production Area Rental Cost.
        /// </summary>
        public decimal ProductionAreaRentalCost
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledProductionAreaRentalCost;
                }
                else
                {
                    return MasterDataScrambler.UnscrambleNumber(ScrambledProductionAreaRentalCost);
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledProductionAreaRentalCost = value;
                }
                else
                {
                    value = EntityUtils.PrepareNumberForScrambling(value);
                    ScrambledProductionAreaRentalCost = MasterDataScrambler.ScrambleNumber(value);
                }

                this.OnPropertyChanged("ProductionAreaRentalCost");
            }
        }

        /// <summary>
        /// Gets or sets the Office Area Rental Cost.
        /// </summary>
        public decimal OfficeAreaRentalCost
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledOfficeAreaRentalCost;
                }
                else
                {
                    return MasterDataScrambler.UnscrambleNumber(ScrambledOfficeAreaRentalCost);
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledOfficeAreaRentalCost = value;
                }
                else
                {
                    value = EntityUtils.PrepareNumberForScrambling(value);
                    ScrambledOfficeAreaRentalCost = MasterDataScrambler.ScrambleNumber(value);
                }

                this.OnPropertyChanged("OfficeAreaRentalCost");
            }
        }

        /// <summary>
        /// Gets or sets the extra Shift Charge for the 1-Shift model.
        /// </summary>
        public decimal? ShiftCharge1ShiftModel
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledShiftCharge1ShiftModel;
                }
                else
                {
                    return MasterDataScrambler.UnscrambleNumber(ScrambledShiftCharge1ShiftModel);
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledShiftCharge1ShiftModel = value;
                }
                else
                {
                    value = EntityUtils.PrepareNumberForScrambling(value);
                    ScrambledShiftCharge1ShiftModel = MasterDataScrambler.ScrambleNumber(value);
                }

                this.OnPropertyChanged("ShiftCharge1ShiftModel");
            }
        }

        /// <summary>
        /// Gets or sets the extra Shift Charge for the 2-Shifts model.
        /// </summary>
        public decimal? ShiftCharge2ShiftModel
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledShiftCharge2ShiftModel;
                }
                else
                {
                    return MasterDataScrambler.UnscrambleNumber(ScrambledShiftCharge2ShiftModel);
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledShiftCharge2ShiftModel = value;
                }
                else
                {
                    value = EntityUtils.PrepareNumberForScrambling(value);
                    ScrambledShiftCharge2ShiftModel = MasterDataScrambler.ScrambleNumber(value);
                }

                this.OnPropertyChanged("ShiftCharge2ShiftModel");
            }
        }

        /// <summary>
        /// Gets or sets the extra Shift Charge for the 3-Shift model.
        /// </summary>
        public decimal? ShiftCharge3ShiftModel
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledShiftCharge3ShiftModel;
                }
                else
                {
                    return MasterDataScrambler.UnscrambleNumber(ScrambledShiftCharge3ShiftModel);
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledShiftCharge3ShiftModel = value;
                }
                else
                {
                    value = EntityUtils.PrepareNumberForScrambling(value);
                    ScrambledShiftCharge3ShiftModel = MasterDataScrambler.ScrambleNumber(value);
                }

                this.OnPropertyChanged("ShiftCharge3ShiftModel");
            }
        }

        #endregion

        /// <summary>
        /// Called when this instance in initialized (by the constructor defined in the generated code).
        /// Can be used to perform custom construction-time initialization.
        /// </summary>
        partial void OnItitialized()
        {
            this.Guid = Guid.NewGuid();
            this.CreateDate = DateTime.Now;
            this.LastChangeTimestamp = DateTime.Now;
        }

        /// <summary>
        /// Compares the country settings values of this instance with the values of another instance.
        /// </summary>
        /// <param name="obj">The instance with whose values to compare.</param>
        /// <returns>True if the values in the two instances are the same; false if at least one 
        /// value is different between the two instances.</returns>
        public bool EqualsValues(CountrySetting obj)
        {
            return this.EqualsValues(obj, 0m);
        }

        /// <summary>
        /// Compares the country settings values of this instance with the values of another instance.
        /// </summary>
        /// <param name="obj">The instance with whose values to compare.</param>
        /// <param name="acceptedDeviation">The accepted deviation when comparing the values.</param>
        /// <returns>True if the values in the two instances are the same; false if at least one 
        /// value is different between the two instances.</returns>
        public bool EqualsValues(CountrySetting obj, decimal acceptedDeviation)
        {
            if (Math.Abs(obj.AirCost - this.AirCost) > acceptedDeviation
                || Math.Abs(obj.EnergyCost - this.EnergyCost) > acceptedDeviation
                || Math.Abs(obj.EngineerCost - this.EngineerCost) > acceptedDeviation
                || Math.Abs(obj.ForemanCost - this.ForemanCost) > acceptedDeviation
                || Math.Abs(obj.InterestRate - this.InterestRate) > acceptedDeviation
                || Math.Abs(obj.OfficeAreaRentalCost - this.OfficeAreaRentalCost) > acceptedDeviation
                || Math.Abs(obj.ProductionAreaRentalCost - this.ProductionAreaRentalCost) > acceptedDeviation
                || Math.Abs(obj.SkilledLaborCost - this.SkilledLaborCost) > acceptedDeviation
                || Math.Abs(obj.TechnicianCost - this.TechnicianCost) > acceptedDeviation
                || Math.Abs(obj.UnskilledLaborCost - this.UnskilledLaborCost) > acceptedDeviation
                || Math.Abs(obj.WaterCost - this.WaterCost) > acceptedDeviation
                || this.EqualsDecimals(obj.LaborAvailability, this.LaborAvailability, acceptedDeviation)
                || this.EqualsDecimals(obj.ShiftCharge1ShiftModel, this.ShiftCharge1ShiftModel, acceptedDeviation)
                || this.EqualsDecimals(obj.ShiftCharge2ShiftModel, this.ShiftCharge2ShiftModel, acceptedDeviation)
                || this.EqualsDecimals(obj.ShiftCharge3ShiftModel, this.ShiftCharge3ShiftModel, acceptedDeviation))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Compares two nullable decimal.
        /// </summary>
        /// <param name="firstValue">The first value.</param>
        /// <param name="secondValue">The second value.</param>
        /// <param name="acceptedError">The accepted error.</param>
        /// <returns>True if the values are the same; false if not.</returns>
        private bool EqualsDecimals(decimal? firstValue, decimal? secondValue, decimal acceptedError)
        {
            if ((firstValue.HasValue && !secondValue.HasValue)
                || (!firstValue.HasValue && secondValue.HasValue)
                || (firstValue.HasValue && secondValue.HasValue && Math.Abs(firstValue.Value - secondValue.Value) > acceptedError))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Copies this instance excluding the properties injected by EF.
        /// </summary>
        /// <returns>A copy of this instance (new object).</returns>
        public CountrySetting Copy()
        {
            CountrySetting countrySettings = new CountrySetting();
            this.CopyValuesTo(countrySettings);

            return countrySettings;
        }

        /// <summary>
        /// Copies the values from this instance to another instance.
        /// </summary>
        /// <param name="destination">The destination instance.</param>
        /// <exception cref="ArgumentNullException"><paramref name="destination"/> was null</exception>
        /// <exception cref="ArgumentException"><paramref name="destination"/> did not have the expected type</exception>
        public void CopyValuesTo(object destination)
        {
            if (destination == null)
            {
                throw new ArgumentNullException("destination", "Destination was null.");
            }

            CountrySetting temp = destination as CountrySetting;
            if (temp == null)
            {
                throw new ArgumentException("Destination was not a CountrySetting.");
            }

            if (temp.IsScrambled != this.IsScrambled)
            {
                temp.IsScrambled = this.IsScrambled;
            }

            if (temp.AirCost != this.AirCost)
            {
                temp.AirCost = this.AirCost;
            }

            if (temp.EnergyCost != this.EnergyCost)
            {
                temp.EnergyCost = this.EnergyCost;
            }

            if (temp.EngineerCost != this.EngineerCost)
            {
                temp.EngineerCost = this.EngineerCost;
            }

            if (temp.ForemanCost != this.ForemanCost)
            {
                temp.ForemanCost = this.ForemanCost;
            }

            if (temp.InterestRate != this.InterestRate)
            {
                temp.InterestRate = this.InterestRate;
            }

            if (temp.OfficeAreaRentalCost != this.OfficeAreaRentalCost)
            {
                temp.OfficeAreaRentalCost = this.OfficeAreaRentalCost;
            }

            if (temp.ProductionAreaRentalCost != this.ProductionAreaRentalCost)
            {
                temp.ProductionAreaRentalCost = this.ProductionAreaRentalCost;
            }

            if (temp.SkilledLaborCost != this.SkilledLaborCost)
            {
                temp.SkilledLaborCost = this.SkilledLaborCost;
            }

            if (temp.TechnicianCost != this.TechnicianCost)
            {
                temp.TechnicianCost = this.TechnicianCost;
            }

            if (temp.UnskilledLaborCost != this.UnskilledLaborCost)
            {
                temp.UnskilledLaborCost = this.UnskilledLaborCost;
            }

            if (temp.WaterCost != this.WaterCost)
            {
                temp.WaterCost = this.WaterCost;
            }

            if (temp.ShiftCharge1ShiftModel != this.ShiftCharge1ShiftModel)
            {
                temp.ShiftCharge1ShiftModel = this.ShiftCharge1ShiftModel;
            }

            if (temp.ShiftCharge2ShiftModel != this.ShiftCharge2ShiftModel)
            {
                temp.ShiftCharge2ShiftModel = this.ShiftCharge2ShiftModel;
            }

            if (temp.ShiftCharge3ShiftModel != this.ShiftCharge3ShiftModel)
            {
                temp.ShiftCharge3ShiftModel = this.ShiftCharge3ShiftModel;
            }

            if (temp.LaborAvailability != this.LaborAvailability)
            {
                temp.LaborAvailability = this.LaborAvailability;
            }

            if (temp.LastChangeTimestamp != this.LastChangeTimestamp)
            {
                temp.LastChangeTimestamp = this.LastChangeTimestamp;
            }
        }

        /// <summary>
        /// Sets the value of the <see cref="Owner " /> property on this instance and on all the objects in its graph that implement <see cref="IOwnedObject" />.
        /// </summary>
        /// <param name="newOwner">The new owner.</param>
        public void SetOwner(User newOwner)
        {
            this.Owner = newOwner;
        }
    }
}
