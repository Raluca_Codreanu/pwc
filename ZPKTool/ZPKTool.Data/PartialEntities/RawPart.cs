﻿namespace ZPKTool.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    
    /// <summary>
    /// This class represents the RawPart of a part.
    /// </summary>
    public partial class RawPart : IIdentifiable, ITrashable
    {
        #region ITrashable Members

        /// <summary>
        /// Gets the type of the trash bin item.
        /// </summary>
        /// <value>The type of the trash bin item.</value>
        public new TrashBinItemType TrashBinItemType
        {
            get { return TrashBinItemType.RawPart; }
        }

        #endregion ITrashable Members
    }
}
