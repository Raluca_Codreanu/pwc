﻿using System;

namespace ZPKTool.Data
{
    /// <summary>
    /// Extension class for the MeasurementUnit entity.
    /// </summary>
    public partial class MeasurementUnit : IIdentifiable, INameable, ICopiable, IReleasable
    {
        /// <summary>
        /// Gets a value indicating whether this instance is the base unit of its scale.
        /// </summary>
        public bool IsBaseUnit
        {
            get { return this.ScaleFactor == 1m ? true : false; }
        }

        #region ICopiable Members

        /// <summary>
        /// Copies the values from this instance to another instance.
        /// </summary>
        /// <param name="destination">The destination instance.</param>
        /// <exception cref="ArgumentNullException"><paramref name="destination"/> was null</exception>
        /// <exception cref="ArgumentException"><paramref name="destination"/> did not have the expected type</exception>
        public void CopyValuesTo(object destination)
        {
            if (destination == null)
            {
                throw new ArgumentNullException("destination", "Destination was null.");
            }

            MeasurementUnit temp = destination as MeasurementUnit;
            if (temp == null)
            {
                throw new ArgumentException("Destination was not a MeasurementUnit.");
            }

            if (temp.ConversionRate != this.ConversionRate)
            {
                temp.ConversionRate = this.ConversionRate;
            }

            if (temp.Name != this.Name)
            {
                temp.Name = this.Name;
            }

            if (temp.Symbol != this.Symbol)
            {
                temp.Symbol = this.Symbol;
            }

            if (temp.Type != this.Type)
            {
                temp.Type = this.Type;
            }

            if (temp.ScaleID != this.ScaleID)
            {
                temp.ScaleID = this.ScaleID;
            }

            if (temp.ScaleFactor != this.ScaleFactor)
            {
                temp.ScaleFactor = this.ScaleFactor;
            }
        }

        /// <summary>
        /// Creates a shallow copy of this instance.
        /// </summary>
        /// <returns>A shallow copy of this instance.</returns>
        public MeasurementUnit Copy()
        {
            var copy = new MeasurementUnit();
            this.CopyValuesTo(copy);
            return copy;
        }

        #endregion

        /// <summary>
        /// Called when this instance in initialized (by the constructor defined in the generated code).
        /// Can be used to perform custom construction-time initialization.
        /// </summary>
        partial void OnItitialized()
        {
            this.Guid = Guid.NewGuid();
            this.CreateDate = DateTime.Now;

            // Some very old models contain measurement units from before the ScaleFactor property existed and for them the ScaleFactor value is 0
            // which would lead to division by zero exceptions in many places. Setting it to 1 here solves the exceptions issue; the correct way would be to find where it is used and check it for 0 but this is faster.
            this.ScaleFactor = 1m;
        }
    }
}
