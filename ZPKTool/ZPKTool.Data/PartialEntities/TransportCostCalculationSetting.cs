﻿using System;

namespace ZPKTool.Data
{
    /// <summary>
    /// Partial implementation for the TransportCostCalculationSetting entity.
    /// </summary>
    public partial class TransportCostCalculationSetting : IIdentifiable, ICopiable, IOwnedObject
    {
        #region ICopiable Members

        /// <summary>
        /// Copies the values from this instance to another instance.
        /// </summary>
        /// <param name="destination">The destination instance.</param>
        /// <exception cref="ArgumentNullException"><paramref name="destination"/> was null</exception>
        /// <exception cref="ArgumentException"><paramref name="destination"/> did not have the expected type</exception>
        public void CopyValuesTo(object destination)
        {
            if (destination == null)
            {
                throw new ArgumentNullException("destination", "Destination TransportCostCalculationSetting was null.");
            }

            if (destination.GetType() != typeof(TransportCostCalculationSetting))
            {
                throw new ArgumentException("Destination was not a TransportCostCalculationSetting.");
            }

            TransportCostCalculationSetting temp = (TransportCostCalculationSetting)destination;
            temp.DaysPerYear = this.DaysPerYear;
            temp.AverageLoadTarget = this.AverageLoadTarget;
            temp.VanTypeCost = this.VanTypeCost;
            temp.TruckTrailerCost = this.TruckTrailerCost;
            temp.MegaTrailerCost = this.MegaTrailerCost;
            temp.Truck7tCost = this.Truck7tCost;
            temp.Truck3_5tCost = this.Truck3_5tCost;
            temp.Timestamp = this.Timestamp;
        }

        #endregion

        #region Set Properties in Depth

        /// <summary>
        /// Sets the value of the <see cref="Owner " /> property on this instance and on all the objects in its graph that implement <see cref="IOwnedObject" />.
        /// </summary>
        /// <param name="newOwner">The new owner.</param>
        public void SetOwner(User newOwner)
        {
            this.Owner = newOwner;
        }

        #endregion

        /// <summary>
        /// Called when this instance in initialized (by the constructor defined in the generated code).
        /// Can be used to perform custom construction-time initialization.
        /// </summary>
        partial void OnItitialized()
        {
            this.Guid = Guid.NewGuid();
            this.Timestamp = DateTime.Now;
        }
    }
}
