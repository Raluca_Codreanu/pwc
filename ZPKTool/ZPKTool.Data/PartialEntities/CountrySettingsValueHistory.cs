﻿using System;

namespace ZPKTool.Data
{
    /// <summary>
    /// Partial implementation of the CountrySettingsValueHistory entity object
    /// </summary>
    public partial class CountrySettingsValueHistory : IIdentifiable
    {
        #region Property wrappers for scrambling/unscrambling internal properties

        /// <summary>
        /// Gets or sets the Unskilled Labor Cost.
        /// </summary>
        public decimal UnskilledLaborCost
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledUnskilledLaborCost.GetValueOrDefault();
                }
                else
                {
                    var val = MasterDataScrambler.UnscrambleNumber(ScrambledUnskilledLaborCost);
                    return val.GetValueOrDefault();
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledUnskilledLaborCost = value;
                }
                else
                {
                    value = EntityUtils.PrepareNumberForScrambling(value);
                    ScrambledUnskilledLaborCost = MasterDataScrambler.ScrambleNumber(value);
                }

                this.OnPropertyChanged("UnskilledLaborCost");
            }
        }

        /// <summary>
        /// Gets or sets Skilled Labor Cost.
        /// </summary>
        public decimal SkilledLaborCost
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledSkilledLaborCost.GetValueOrDefault();
                }
                else
                {
                    var val = MasterDataScrambler.UnscrambleNumber(ScrambledSkilledLaborCost);
                    return val.GetValueOrDefault();
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledSkilledLaborCost = value;
                }
                else
                {
                    value = EntityUtils.PrepareNumberForScrambling(value);
                    ScrambledSkilledLaborCost = MasterDataScrambler.ScrambleNumber(value);
                }

                this.OnPropertyChanged("SkilledLaborCost");
            }
        }

        /// <summary>
        /// Gets or sets ForemanCost field
        /// </summary>
        public decimal ForemanCost
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledForemanCost.GetValueOrDefault();
                }
                else
                {
                    var val = MasterDataScrambler.UnscrambleNumber(ScrambledForemanCost);
                    return val.GetValueOrDefault();
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledForemanCost = value;
                }
                else
                {
                    value = EntityUtils.PrepareNumberForScrambling(value);
                    ScrambledForemanCost = MasterDataScrambler.ScrambleNumber(value);
                }

                this.OnPropertyChanged("ForemanCost");
            }
        }

        /// <summary>
        /// Gets or sets Technician Cost.
        /// </summary>
        public decimal TechnicianCost
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledTechnicianCost.GetValueOrDefault();
                }
                else
                {
                    var val = MasterDataScrambler.UnscrambleNumber(ScrambledTechnicianCost);
                    return val.GetValueOrDefault();
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledTechnicianCost = value;
                }
                else
                {
                    value = EntityUtils.PrepareNumberForScrambling(value);
                    ScrambledTechnicianCost = MasterDataScrambler.ScrambleNumber(value);
                }

                this.OnPropertyChanged("TechnicianCost");
            }
        }

        /// <summary>
        /// Gets or sets the Engineer Cost.
        /// </summary>
        public decimal EngineerCost
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledEngineerCost.GetValueOrDefault();
                }
                else
                {
                    var val = MasterDataScrambler.UnscrambleNumber(ScrambledEngineerCost);
                    return val.GetValueOrDefault();
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledEngineerCost = value;
                }
                else
                {
                    value = EntityUtils.PrepareNumberForScrambling(value);
                    ScrambledEngineerCost = MasterDataScrambler.ScrambleNumber(value);
                }

                this.OnPropertyChanged("EngineerCost");
            }
        }

        /// <summary>
        /// Gets or sets the Energy Cost.
        /// </summary>
        public decimal EnergyCost
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledEnergyCost.GetValueOrDefault();
                }
                else
                {
                    var val = MasterDataScrambler.UnscrambleNumber(ScrambledEnergyCost);
                    return val.GetValueOrDefault();
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledEnergyCost = value;
                }
                else
                {
                    value = EntityUtils.PrepareNumberForScrambling(value);
                    ScrambledEnergyCost = MasterDataScrambler.ScrambleNumber(value);
                }

                this.OnPropertyChanged("EnergyCost");
            }
        }

        /// <summary>
        /// Gets or sets the Water Cost.
        /// </summary>
        public decimal WaterCost
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledWaterCost.GetValueOrDefault();
                }
                else
                {
                    var val = MasterDataScrambler.UnscrambleNumber(ScrambledWaterCost);
                    return val.GetValueOrDefault();
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledWaterCost = value;
                }
                else
                {
                    value = EntityUtils.PrepareNumberForScrambling(value);
                    ScrambledWaterCost = MasterDataScrambler.ScrambleNumber(value);
                }

                this.OnPropertyChanged("WaterCost");
            }
        }

        /// <summary>
        /// Gets or sets the Production Area Rental Cost.
        /// </summary>
        public decimal ProductionAreaRentalCost
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledProductionAreaRentalCost.GetValueOrDefault();
                }
                else
                {
                    var val = MasterDataScrambler.UnscrambleNumber(ScrambledProductionAreaRentalCost);
                    return val.GetValueOrDefault();
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledProductionAreaRentalCost = value;
                }
                else
                {
                    value = EntityUtils.PrepareNumberForScrambling(value);
                    ScrambledProductionAreaRentalCost = MasterDataScrambler.ScrambleNumber(value);
                }

                this.OnPropertyChanged("ProductionAreaRentalCost");
            }
        }

        /// <summary>
        /// Gets or sets the Office Area Rental Cost.
        /// </summary>
        public decimal OfficeAreaRentalCost
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledOfficeAreaRentalCost.GetValueOrDefault();
                }
                else
                {
                    var val = MasterDataScrambler.UnscrambleNumber(ScrambledOfficeAreaRentalCost);
                    return val.GetValueOrDefault();
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledOfficeAreaRentalCost = value;
                }
                else
                {
                    value = EntityUtils.PrepareNumberForScrambling(value);
                    ScrambledOfficeAreaRentalCost = MasterDataScrambler.ScrambleNumber(value);
                }

                this.OnPropertyChanged("OfficeAreaRentalCost");
            }
        }

        /// <summary>
        /// Gets or sets the extra Shift Charge for the 1-Shift model.
        /// </summary>
        public decimal ShiftCharge1ShiftModel
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledShiftCharge1ShiftModel.GetValueOrDefault();
                }
                else
                {
                    var val = MasterDataScrambler.UnscrambleNumber(ScrambledShiftCharge1ShiftModel);
                    return val.GetValueOrDefault();
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledShiftCharge1ShiftModel = value;
                }
                else
                {
                    value = EntityUtils.PrepareNumberForScrambling(value);
                    ScrambledShiftCharge1ShiftModel = MasterDataScrambler.ScrambleNumber(value);
                }

                this.OnPropertyChanged("ShiftCharge1ShiftModel");
            }
        }

        /// <summary>
        /// Gets or sets the extra Shift Charge for the 2-Shifts model.
        /// </summary>
        public decimal ShiftCharge2ShiftModel
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledShiftCharge2ShiftModel.GetValueOrDefault();
                }
                else
                {
                    var val = MasterDataScrambler.UnscrambleNumber(ScrambledShiftCharge2ShiftModel);
                    return val.GetValueOrDefault();
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledShiftCharge2ShiftModel = value;
                }
                else
                {
                    value = EntityUtils.PrepareNumberForScrambling(value);
                    ScrambledShiftCharge2ShiftModel = MasterDataScrambler.ScrambleNumber(value);
                }

                this.OnPropertyChanged("ShiftCharge2ShiftModel");
            }
        }

        /// <summary>
        /// Gets or sets the extra Shift Charge for the 3-Shift model.
        /// </summary>
        public decimal ShiftCharge3ShiftModel
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledShiftCharge3ShiftModel.GetValueOrDefault();
                }
                else
                {
                    var val = MasterDataScrambler.UnscrambleNumber(ScrambledShiftCharge3ShiftModel);
                    return val.GetValueOrDefault();
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledShiftCharge3ShiftModel = value;
                }
                else
                {
                    value = EntityUtils.PrepareNumberForScrambling(value);
                    ScrambledShiftCharge3ShiftModel = MasterDataScrambler.ScrambleNumber(value);
                }

                this.OnPropertyChanged("ShiftCharge3ShiftModel");
            }
        }

        #endregion

        /// <summary>
        /// Called when this instance in initialized (by the constructor defined in the generated code).
        /// Can be used to perform custom construction-time initialization.
        /// </summary>
        partial void OnItitialized()
        {
            this.Guid = Guid.NewGuid();
        }
    }
}
