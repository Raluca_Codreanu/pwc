﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ZPKTool.Data
{
    /// <summary>
    /// Extension class for the Assembly entity
    /// </summary>
    public partial class Assembly : IIdentifiable, INameable, IMasterDataObject, IOwnedObject, IEntityWithMediaCollection,
        ICopiable, ITrashable
    {
        /// <summary>
        /// Gets the type of the trash bin item.
        /// </summary>
        /// <value>The type of the trash bin item.</value>
        public TrashBinItemType TrashBinItemType
        {
            get { return TrashBinItemType.Assembly; }
        }

        /// <summary>
        /// Called when this instance in initialized (by the constructor defined in the generated code).
        /// Can be used to perform custom construction-time initialization.
        /// </summary>
        partial void OnItitialized()
        {
            this.CalculateLogisticCost = true;
            this.Guid = Guid.NewGuid();
            this.CreateDate = DateTime.Now;
            this.CalculationCreateDate = DateTime.Now;
        }

        #region SetPropertiesInDepth

        /// <summary>
        /// Sets the owner.
        /// Also updates sub-objects
        /// </summary>
        /// <param name="newOwner">The new owner.</param>
        public void SetOwner(User newOwner)
        {
            this.Owner = newOwner;

            foreach (Media value in this.Media)
            {
                value.SetOwner(newOwner);
            }

            foreach (TransportCostCalculation calculation in TransportCostCalculations)
            {
                calculation.SetOwner(newOwner);
            }

            if (this.Manufacturer != null)
            {
                this.Manufacturer.SetOwner(newOwner);
            }

            if (this.CountrySettings != null)
            {
                this.CountrySettings.SetOwner(newOwner);
            }

            if (this.OverheadSettings != null)
            {
                this.OverheadSettings.SetOwner(newOwner);
            }

            if (this.Process != null)
            {
                this.Process.SetOwner(newOwner);
            }

            foreach (Part part in this.Parts)
            {
                part.SetOwner(newOwner);
            }

            foreach (Assembly assembly in this.Subassemblies)
            {
                assembly.SetOwner(newOwner);
            }
        }

        /// <summary>
        /// Sets the value of the <see cref="IsMasterData" /> property on this instance and on all the objects in its graph that implement <see cref="IMasterDataObject" />.
        /// </summary>
        /// <param name="newValue">The new value for the <see cref="IsMasterData" /> property.</param>
        public void SetIsMasterData(bool newValue)
        {
            this.IsMasterData = newValue;

            if (this.OverheadSettings != null)
            {
                // Overhead Settings that belong to other entities don not have the IsMasterData flag set even if their parent has it.
                this.OverheadSettings.SetIsMasterData(false);
            }

            if (this.Process != null)
            {
                this.Process.SetIsMasterData(newValue);
            }

            if (this.Manufacturer != null)
            {
                this.Manufacturer.SetIsMasterData(newValue);
            }

            foreach (Media value in this.Media)
            {
                value.SetIsMasterData(newValue);
            }

            foreach (Part part in this.Parts)
            {
                part.SetIsMasterData(newValue);
            }

            foreach (Assembly item in this.Subassemblies)
            {
                item.SetIsMasterData(newValue);
            }
        }

        /// <summary>
        /// Sets the value of the <see cref="IsDeleted" /> property on this instance and on all the objects in its graph that implement <see cref="ITrashable" />.
        /// </summary>
        /// <param name="newValue">The new value for the IsDeleted property.</param>
        public void SetIsDeleted(bool newValue)
        {
            if (this.IsDeleted != newValue)
            {
                this.IsDeleted = newValue;
            }

            if (this.Process != null)
            {
                this.Process.SetIsDeleted(newValue);
            }

            foreach (Part part in this.Parts)
            {
                part.SetIsDeleted(newValue);
            }

            foreach (Assembly assembly in this.Subassemblies)
            {
                assembly.SetIsDeleted(newValue);
            }
        }

        /// <summary>
        /// Sets the calculation variant for this instance and all objects in its graph.
        /// </summary>
        /// <param name="newValue">The new calculation variant.</param>
        public void SetCalculationVariant(string newValue)
        {
            this.CalculationVariant = newValue;

            foreach (Part part in this.Parts.Where(p => p.CalculationVariant != newValue))
            {
                part.SetCalculationVariant(newValue);
            }

            foreach (Assembly subAssembly in this.Subassemblies)
            {
                subAssembly.SetCalculationVariant(newValue);
            }
        }

        #endregion SetPropertiesInDepth

        #region ICopiable Members

        /// <summary>
        /// Copies the values from this instance to another instance.
        /// </summary>
        /// <param name="destination">The destination instance.</param>
        /// <exception cref="ArgumentNullException"><paramref name="destination"/> was null</exception>
        /// <exception cref="ArgumentException"><paramref name="destination"/> did not have the expected type</exception>
        public void CopyValuesTo(object destination)
        {
            if (destination == null)
            {
                throw new ArgumentNullException("destination", "Destination was null.");
            }

            Assembly temp = destination as Assembly;
            if (temp == null)
            {
                throw new ArgumentException("Destination was not an Assembly.", "destination");
            }

            temp.AssetRate = this.AssetRate;
            temp.BatchSizePerYear = this.BatchSizePerYear;
            temp.CalculateLogisticCost = this.CalculateLogisticCost;
            temp.CalculationAccuracy = this.CalculationAccuracy;
            temp.DeliveryType = this.DeliveryType;
            temp.DevelopmentCost = this.DevelopmentCost;
            temp.EstimatedCost = this.EstimatedCost;
            temp.IsDeleted = this.IsDeleted;
            temp.IsExternal = this.IsExternal;
            temp.IsMasterData = this.IsMasterData;
            temp.LifeTime = this.LifeTime;
            temp.LogisticCost = this.LogisticCost;

            if (this.Name != null)
            {
                temp.Name = this.Name;
            }

            if (this.Number != null)
            {
                temp.Number = this.Number;
            }

            temp.Version = this.Version;
            temp.VersionDate = this.VersionDate;
            temp.OtherCost = this.OtherCost;
            temp.PackagingCost = this.PackagingCost;         
            temp.PaymentTerms = this.PaymentTerms;
            temp.ProjectInvest = this.ProjectInvest;
            temp.PurchasePrice = this.PurchasePrice;
            temp.SBMActive = this.SBMActive;
            temp.TargetPrice = this.TargetPrice;
            temp.YearlyProductionQuantity = this.YearlyProductionQuantity;
            temp.LastChangeTimestamp = this.LastChangeTimestamp;

            if (this.CalculationVariant != null)
            {
                temp.CalculationVariant = this.CalculationVariant;
            }

            if (this.AssemblingCountry != null)
            {
                temp.AssemblingCountry = this.AssemblingCountry;
            }

            if (this.AssemblingSupplier != null)
            {
                temp.AssemblingSupplier = this.AssemblingSupplier;
            }

            temp.CalculationApproach = this.CalculationApproach;
            temp.Reusable = this.Reusable;

            if (this.Description != null)
            {
                temp.Description = this.Description;
            }

            if (this.AdditionalCostsNotes != null)
            {
                temp.AdditionalCostsNotes = this.AdditionalCostsNotes;
            }

            temp.Weight = this.Weight;
            temp.TransportCost = this.TransportCost;
            temp.Index = this.Index;
            temp.AreAdditionalCostsPerPartsTotal = this.AreAdditionalCostsPerPartsTotal;
            temp.AssemblingCountryId = this.AssemblingCountryId;
            temp.ExternalSGA = this.ExternalSGA;
            temp.CalculationCreateDate = this.CalculationCreateDate;
        }

        #endregion ICopiable Members
    }
}