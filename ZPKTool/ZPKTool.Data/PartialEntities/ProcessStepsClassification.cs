﻿using System;

namespace ZPKTool.Data
{
    /// <summary>
    /// Extension class for the ProcessStepsClassification entity
    /// </summary>
    public partial class ProcessStepsClassification : IIdentifiable, INameable, ICopiable, IReleasable
    {        
        #region ICopiable Members

        /// <summary>
        /// Copies the values from this instance to another instance.
        /// </summary>
        /// <param name="destination">The destination instance.</param>
        /// <exception cref="ArgumentNullException"><paramref name="destination"/> was null</exception>
        /// <exception cref="ArgumentException"><paramref name="destination"/> did not have the expected type</exception>
        public void CopyValuesTo(object destination)
        {
            if (destination == null)
            {
                throw new ArgumentNullException("destination", "Destination was null for ProcessStepsClassification.");
            }

            ProcessStepsClassification temp = destination as ProcessStepsClassification;
            if (temp == null)
            {
                throw new ArgumentException("Destination was not a ProcessStepsClassification.");
            }
                        
            if (this.Name != null)
            {
                temp.Name = this.Name;
            }
        }

        #endregion

        /// <summary>
        /// Called when this instance in initialized (by the constructor defined in the generated code).
        /// Can be used to perform custom construction-time initialization.
        /// </summary>
        partial void OnItitialized()
        {
            this.Guid = Guid.NewGuid();
            this.CreateDate = DateTime.Now;
        }
    }
}
