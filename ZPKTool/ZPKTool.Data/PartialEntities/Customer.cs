﻿using System;

namespace ZPKTool.Data
{
    /// <summary>
    /// Extension class for the Customer entity.
    /// </summary>
    public partial class Customer : IIdentifiable, INameable, IMasterDataObject, IOwnedObject, ICopiable
    {
        #region ICopiable Members

        /// <summary>
        /// Copies the values from this instance to another instance.
        /// </summary>
        /// <param name="destination">The destination instance.</param>
        /// <exception cref="ArgumentNullException"><paramref name="destination"/> was null</exception>
        /// <exception cref="ArgumentException"><paramref name="destination"/> did not have the expected type</exception>
        public void CopyValuesTo(object destination)
        {
            if (destination == null)
            {
                throw new ArgumentNullException("destination", "Destination was null.");
            }

            Customer temp = destination as Customer;
            if (temp == null)
            {
                throw new ArgumentException("Destination was not a Customer.");
            }

            temp.City = this.City;
            temp.Description = this.Description;
            temp.FirstContactName = this.FirstContactName;
            temp.SecondContactName = this.SecondContactName;
            temp.FirstContactEmail = this.FirstContactEmail;
            temp.SecondContactEmail = this.SecondContactEmail;
            temp.FirstContactFax = this.FirstContactFax;
            temp.SecondContactFax = this.SecondContactFax;
            temp.IsMasterData = this.IsMasterData;
            temp.FirstContactMobile = this.FirstContactMobile;
            temp.SecondContactMobile = this.SecondContactMobile;

            if (this.Name != null)
            {
                temp.Name = this.Name;
            }

            temp.Number = this.Number;
            temp.FirstContactPhone = this.FirstContactPhone;
            temp.SecondContactPhone = this.SecondContactPhone;
            temp.StreetAddress = this.StreetAddress;
            temp.Type = this.Type;
            temp.FirstContactWebAddress = this.FirstContactWebAddress;
            temp.SecondContactWebAddress = this.SecondContactWebAddress;
            temp.ZipCode = this.ZipCode;
            temp.Country = this.Country;
            temp.State = this.State;
        }

        #endregion

        #region IOwnedObject Members

        /// <summary>
        /// Sets the value of the <see cref="Owner " /> property on this instance and on all the objects in its graph that implement <see cref="IOwnedObject" />.
        /// </summary>
        /// <param name="newOwner">The new owner.</param>
        public void SetOwner(User newOwner)
        {
            this.Owner = newOwner;
        }

        #endregion

        /// <summary>
        /// Sets the value of the <see cref="IsMasterData" /> property on this instance and on all the objects in its graph that implement <see cref="IMasterDataObject" />.
        /// </summary>
        /// <param name="newValue">The new value for the <see cref="IsMasterData" /> property.</param>
        public void SetIsMasterData(bool newValue)
        {
            this.IsMasterData = newValue;
        }

        /// <summary>
        /// Called when this instance in initialized (by the constructor defined in the generated code).
        /// Can be used to perform custom construction-time initialization.
        /// </summary>
        partial void OnItitialized()
        {
            this.Guid = Guid.NewGuid();
            this.CreateDate = DateTime.Now;
        }
    }
}
