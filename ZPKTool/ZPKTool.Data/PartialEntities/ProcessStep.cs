﻿using System;

namespace ZPKTool.Data
{
    /// <summary>
    /// The base class for manufacturing and assembling process steps.
    /// </summary>
    public partial class ProcessStep : IIdentifiable, INameable, IOwnedObject, IMasterDataObject, IEntityWithMedia, ICopiable
    {
        #region Constants

        /// <summary>
        /// The default value for the Setup Time property of a process step.
        /// </summary>
        public const int DefaultSetupsPerBatch = 1;

        /// <summary>
        /// The default value for the Setup Time property of a process step.
        /// </summary>
        public const decimal DefaultSetupTime = 0;

        /// <summary>
        /// The default value for the Max Downtime property of a process step.
        /// </summary>
        public const decimal DefaultMaxDowntime = 0;

        /// <summary>
        /// The default value for the Amount of Scrap property of a process step.
        /// It represents a percentage so it should be between 0 and 1
        /// having at most 4 decimal places (10% is 0.1, etc).
        /// </summary>
        public const decimal DefaultScrapAmount = 0;

        /// <summary>
        /// The default value for the Rework Ratio property of a process step.
        /// It represents a percentage so it should be between 0 and 1
        /// having at most 4 decimal places (10% is 0.1, etc).
        /// </summary>
        public const decimal DefaultReworkRatio = 0;

        #endregion Constants

        #region IEntityWithMedia Members

        /// <summary>
        /// Gets or sets the Media property - overwrite property picture
        /// </summary>
        public Media Media
        {
            get { return this.Picture; }
            set { this.Picture = value; }
        }

        #endregion IEntityWithMedia Members

        #region ICopiable Members

        /// <summary>
        /// Copies the values from this instance to another instance.
        /// </summary>
        /// <param name="destination">The destination instance.</param>
        /// <exception cref="ArgumentNullException"><paramref name="destination"/> was null</exception>
        /// <exception cref="ArgumentException"><paramref name="destination"/> did not have the expected type</exception>
        public virtual void CopyValuesTo(object destination)
        {
            if (destination == null)
            {
                throw new ArgumentNullException("destination", "Destination was null.");
            }

            ProcessStep temp = destination as ProcessStep;
            if (temp == null)
            {
                throw new ArgumentException("Destination was not a ProcessStep.");
            }

            temp.Accuracy = this.Accuracy;
            temp.BatchSize = this.BatchSize;
            temp.CycleTime = this.CycleTime;
            temp.ExceedShiftCost = this.ExceedShiftCost;
            temp.ExtraShiftsNumber = this.ExtraShiftsNumber;
            temp.HoursPerShift = this.HoursPerShift;
            temp.Index = this.Index;
            temp.IsExternal = this.IsExternal;
            temp.TransportCost = this.TransportCost;
            temp.TransportCostType = this.TransportCostType;
            temp.TransportCostQty = this.TransportCostQty;
            temp.IsMasterData = this.IsMasterData;
            temp.MaxDownTime = this.MaxDownTime;
            temp.Name = this.Name;
            temp.Description = this.Description;
            temp.PartsPerCycle = this.PartsPerCycle;
            temp.Price = this.Price;
            temp.ProcessTime = this.ProcessTime;
            temp.ProductionDaysPerWeek = this.ProductionDaysPerWeek;
            temp.ProductionEngineers = this.ProductionEngineers;
            temp.ProductionForeman = this.ProductionForeman;
            temp.ProductionSkilledLabour = this.ProductionSkilledLabour;
            temp.ProductionTechnicians = this.ProductionTechnicians;
            temp.ProductionUnskilledLabour = this.ProductionUnskilledLabour;
            temp.ProductionWeeksPerYear = this.ProductionWeeksPerYear;
            temp.Rework = this.Rework;
            temp.ScrapAmount = this.ScrapAmount;
            temp.SetupEngineers = this.SetupEngineers;
            temp.SetupForeman = this.SetupForeman;
            temp.SetupSkilledLabour = this.SetupSkilledLabour;
            temp.SetupsPerBatch = this.SetupsPerBatch;
            temp.SetupTechnicians = this.SetupTechnicians;
            temp.SetupTime = this.SetupTime;
            temp.SetupUnskilledLabour = this.SetupUnskilledLabour;
            temp.ShiftCostExceedRatio = this.ShiftCostExceedRatio;
            temp.ShiftsPerWeek = this.ShiftsPerWeek;
            temp.ManufacturingOverhead = this.ManufacturingOverhead;
        }

        #endregion

        #region SetPropertiesInDepth

        /// <summary>
        /// Sets the value of the <see cref="Owner " /> property on this instance and on all the objects in its graph that implement <see cref="IOwnedObject" />.
        /// </summary>
        /// <param name="newOwner">The new owner.</param>
        public void SetOwner(User newOwner)
        {
            this.Owner = newOwner;

            foreach (Machine machine in this.Machines)
            {
                machine.SetOwner(newOwner);
            }

            foreach (Commodity commodity in this.Commodities)
            {
                commodity.SetOwner(newOwner);
            }

            foreach (Consumable consumable in this.Consumables)
            {
                consumable.SetOwner(newOwner);
            }

            foreach (Die die in this.Dies)
            {
                die.SetOwner(newOwner);
            }

            foreach (CycleTimeCalculation calc in this.CycleTimeCalculations)
            {
                calc.SetOwner(newOwner);
            }

            foreach (ProcessStepAssemblyAmount amount in this.AssemblyAmounts)
            {
                amount.SetOwner(newOwner);
            }

            foreach (ProcessStepPartAmount amount in this.PartAmounts)
            {
                amount.SetOwner(newOwner);
            }

            if (this.Media != null)
            {
                this.Media.SetOwner(newOwner);
            }
        }

        /// <summary>
        /// Sets the value of the <see cref="IsMasterData" /> property on this instance and on all the objects in its graph that implement <see cref="IMasterDataObject" />.
        /// </summary>
        /// <param name="newValue">The new value for the <see cref="IsMasterData" /> property.</param>
        public void SetIsMasterData(bool newValue)
        {
            this.IsMasterData = newValue;

            foreach (Machine machine in this.Machines)
            {
                machine.SetIsMasterData(newValue);
            }

            foreach (Commodity commodity in this.Commodities)
            {
                commodity.SetIsMasterData(newValue);
            }

            foreach (Consumable consumable in this.Consumables)
            {
                consumable.SetIsMasterData(newValue);
            }

            foreach (Die die in this.Dies)
            {
                die.SetIsMasterData(newValue);
            }

            foreach (CycleTimeCalculation calc in this.CycleTimeCalculations)
            {
                calc.SetIsMasterData(newValue);
            }

            foreach (ProcessStepAssemblyAmount amount in this.AssemblyAmounts)
            {
                amount.IsMasterData = newValue;
            }

            foreach (ProcessStepPartAmount amount in this.PartAmounts)
            {
                amount.IsMasterData = newValue;
            }

            if (this.Media != null)
            {
                this.Media.SetIsMasterData(newValue);
            }
        }

        #endregion SetPropertiesInDepth

        /// <summary>
        /// Sets the IsDeleted property on all ITrashable objects in this instance's graph.
        /// </summary>
        /// <param name="newValue">the new value for the IsDeleted property.</param>
        public void SetIsDeleted(bool newValue)
        {
            foreach (Commodity comm in this.Commodities)
            {
                comm.SetIsDeleted(newValue);
            }

            foreach (Consumable cons in this.Consumables)
            {
                cons.SetIsDeleted(newValue);
            }

            foreach (Die die in this.Dies)
            {
                die.SetIsDeleted(newValue);
            }

            foreach (Machine mach in this.Machines)
            {
                mach.SetIsDeleted(newValue);
            }
        }

        /// <summary>
        /// Called when this instance in initialized (by the constructor defined in the generated code).
        /// Can be used to perform custom construction-time initialization.
        /// </summary>
        partial void OnItitialized()
        {
            this.Guid = Guid.NewGuid();
            this.CreateDate = DateTime.Now;
            
            this.IsExternal = false;
        }
    }
}
