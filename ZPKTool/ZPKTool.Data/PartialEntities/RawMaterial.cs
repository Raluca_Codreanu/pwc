﻿using System;

namespace ZPKTool.Data
{
    /// <summary>
    /// Extension class for the MaterialBase entity
    /// </summary>
    public partial class RawMaterial : IIdentifiable, INameable, IMasterDataObject, IOwnedObject, IEntityWithMedia,
        ICopiable, ITrashable
    {
        #region Constants

        /// <summary>
        /// Default value for scrap refund ratio.
        /// </summary>
        public const decimal DefaultScrapRefundRatio = 0.4m;

        /// <summary>
        /// Default value for the loss.
        /// </summary>
        public const decimal DefaultLoss = 0m;

        #endregion Constants

        /// <summary>
        /// Gets the type of the trash bin item.
        /// </summary>
        /// <value>The type of the trash bin item.</value>
        public TrashBinItemType TrashBinItemType
        {
            get { return TrashBinItemType.RawMaterial; }
        }

        #region Property wrappers for scrambling/unscrambling internal properties

        /// <summary>
        /// Gets or sets the Price.
        /// </summary>
        public decimal? Price
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledPrice;
                }
                else
                {
                    return MasterDataScrambler.UnscrambleNumber(ScrambledPrice);
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledPrice = value;
                }
                else
                {
                    value = EntityUtils.PrepareNumberForScrambling(value);
                    ScrambledPrice = MasterDataScrambler.ScrambleNumber(value);
                }

                this.OnPropertyChanged("Price");
            }
        }

        /// <summary>
        /// Gets or sets the Yield Strength.
        /// </summary>
        public string YieldStrength
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledYieldStrength;
                }
                else
                {
                    return MasterDataScrambler.UnscrambleText(ScrambledYieldStrength);
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledYieldStrength = value;
                }
                else
                {
                    ScrambledYieldStrength = MasterDataScrambler.ScrambleText(value);
                }

                this.OnPropertyChanged("YieldStrength");
            }
        }

        /// <summary>
        /// Gets or sets the Rupture Strength.
        /// </summary>
        public string RuptureStrength
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledRuptureStrength;
                }
                else
                {
                    return MasterDataScrambler.UnscrambleText(ScrambledRuptureStrength);
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledRuptureStrength = value;
                }
                else
                {
                    ScrambledRuptureStrength = MasterDataScrambler.ScrambleText(value);
                }

                this.OnPropertyChanged("RuptureStrength");
            }
        }

        /// <summary>
        /// Gets or sets the Density.
        /// </summary>
        public string Density
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledDensity;
                }
                else
                {
                    return MasterDataScrambler.UnscrambleText(ScrambledDensity);
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledDensity = value;
                }
                else
                {
                    ScrambledDensity = MasterDataScrambler.ScrambleText(value);
                }

                this.OnPropertyChanged("Density");
            }
        }

        /// <summary>
        /// Gets or sets the Maximum Elongation (MaxElongation).
        /// </summary>
        public string MaxElongation
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledMaxElongation;
                }
                else
                {
                    return MasterDataScrambler.UnscrambleText(ScrambledMaxElongation);
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledMaxElongation = value;
                }
                else
                {
                    ScrambledMaxElongation = MasterDataScrambler.ScrambleText(value);
                }

                this.OnPropertyChanged("MaxElongation");
            }
        }

        /// <summary>
        /// Gets or sets the Glass Transition Temperature.
        /// </summary>
        public string GlassTransitionTemperature
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledGlassTransitionTemperature;
                }
                else
                {
                    return MasterDataScrambler.UnscrambleText(ScrambledGlassTransitionTemperature);
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledGlassTransitionTemperature = value;
                }
                else
                {
                    ScrambledGlassTransitionTemperature = MasterDataScrambler.ScrambleText(value);
                }

                this.OnPropertyChanged("GlassTransitionTemperature");
            }
        }

        /// <summary>
        /// Gets or sets the Rx material property.
        /// </summary>
        public string Rx
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledRx;
                }
                else
                {
                    return MasterDataScrambler.UnscrambleText(ScrambledRx);
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledRx = value;
                }
                else
                {
                    ScrambledRx = MasterDataScrambler.ScrambleText(value);
                }

                this.OnPropertyChanged("Rx");
            }
        }

        /// <summary>
        /// Gets or sets the Rm material property.
        /// </summary>
        public string Rm
        {
            get
            {
                if (!this.IsScrambled)
                {
                    return ScrambledRm;
                }
                else
                {
                    return MasterDataScrambler.UnscrambleText(ScrambledRm);
                }
            }

            set
            {
                if (!this.IsScrambled)
                {
                    ScrambledRm = value;
                }
                else
                {
                    ScrambledRm = MasterDataScrambler.ScrambleText(value);
                }

                this.OnPropertyChanged("Rm");
            }
        }

        #endregion Property wrappers for scrambling/unscrambling internal properties

        #region ICopiable Members

        /// <summary>
        /// Copies the values from this instance to another instance.
        /// </summary>
        /// <param name="destination">The destination instance.</param>
        /// <exception cref="ArgumentNullException"><paramref name="destination"/> was null</exception>
        /// <exception cref="ArgumentException"><paramref name="destination"/> did not have the expected type</exception>
        public void CopyValuesTo(object destination)
        {
            if (destination == null)
            {
                throw new ArgumentNullException("destination", "Destination was null.");
            }

            RawMaterial temp = destination as RawMaterial;
            if (temp == null)
            {
                throw new ArgumentException("Destination was not a RawMaterial.");
            }

            temp.IsScrambled = this.IsScrambled;
            temp.IsDeleted = this.IsDeleted;
            temp.IsMasterData = this.IsMasterData;
            temp.Loss = this.Loss;

            if (this.Name != null)
            {
                temp.Name = this.Name;
            }

            if (this.NameUK != null)
            {
                temp.NameUK = this.NameUK;
            }

            if (this.NameUS != null)
            {
                temp.NameUS = this.NameUS;
            }

            if (this.NormName != null)
            {
                temp.NormName = this.NormName;
            }

            if (this.DeliveryType != null)
            {
                temp.DeliveryType = this.DeliveryType;
            }

            temp.ParentWeight = this.ParentWeight;
            temp.Price = this.Price;
            temp.Quantity = this.Quantity;
            temp.RejectRatio = this.RejectRatio;
            temp.StockKeeping = this.StockKeeping;

            if (this.Remarks != null)
            {
                temp.Remarks = this.Remarks;
            }

            temp.ScrapCalculationType = this.ScrapCalculationType;
            temp.ScrapRefundRatio = this.ScrapRefundRatio;
            temp.Sprue = this.Sprue;
            temp.RecyclingRatio = this.RecyclingRatio;

            if (this.YieldStrength != null)
            {
                temp.YieldStrength = this.YieldStrength;
            }

            if (this.RuptureStrength != null)
            {
                temp.RuptureStrength = this.RuptureStrength;
            }

            if (this.Density != null)
            {
                temp.Density = this.Density;
            }

            if (this.MaxElongation != null)
            {
                temp.MaxElongation = this.MaxElongation;
            }

            if (this.GlassTransitionTemperature != null)
            {
                temp.GlassTransitionTemperature = this.GlassTransitionTemperature;
            }

            if (this.Rx != null)
            {
                temp.Rx = this.Rx;
            }

            if (this.Rm != null)
            {
                temp.Rm = this.Rm;
            }

            temp.CalculationCreateDate = this.CalculationCreateDate;
        }

        #endregion

        #region SetPropertiesInDepth

        /// <summary>
        /// Sets the value of the <see cref="Owner " /> property on this instance and on all the objects in its graph that implement <see cref="IOwnedObject" />.
        /// </summary>
        /// <param name="newOwner">The new owner.</param>
        public void SetOwner(User newOwner)
        {
            this.Owner = newOwner;
            if (this.Manufacturer != null)
            {
                this.Manufacturer.SetOwner(newOwner);
            }

            if (this.Media != null)
            {
                this.Media.SetOwner(newOwner);
            }
        }

        /// <summary>
        /// Sets the value of the <see cref="IsMasterData" /> property on this instance and on all the objects in its graph that implement <see cref="IMasterDataObject" />.
        /// </summary>
        /// <param name="newValue">The new value for the <see cref="IsMasterData" /> property.</param>
        public void SetIsMasterData(bool newValue)
        {
            this.IsMasterData = newValue;
            if (this.Manufacturer != null)
            {
                this.Manufacturer.IsMasterData = newValue;
            }

            if (this.Media != null)
            {
                this.Media.SetIsMasterData(newValue);
            }
        }

        /// <summary>
        /// Sets the IsDeleted property on this instance and all ITrashable objects in its graph.
        /// </summary>
        /// <param name="newValue">the new value for the IsDeleted property.</param>
        public void SetIsDeleted(bool newValue)
        {
            if (this.IsDeleted != newValue)
            {
                this.IsDeleted = newValue;
            }
        }

        #endregion SetPropertiesInDepth

        /// <summary>
        /// Called when this instance in initialized (by the constructor defined in the generated code).
        /// Can be used to perform custom construction-time initialization.
        /// </summary>
        partial void OnItitialized()
        {
            this.Guid = Guid.NewGuid();
            this.CreateDate = DateTime.Now;
            this.CalculationCreateDate = DateTime.Now;

            this.ScrapRefundRatio = DefaultScrapRefundRatio;
            this.Loss = DefaultLoss;
        }
    }
}
