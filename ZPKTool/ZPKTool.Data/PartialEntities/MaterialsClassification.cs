﻿using System;

namespace ZPKTool.Data
{
    /// <summary>
    /// Extension class for the MaterialsClassification entity
    /// </summary>
    public partial class MaterialsClassification : IIdentifiable, INameable, ICopiable, IReleasable
    {        
        #region ICopiable Members

        /// <summary>
        /// Copies the values from this instance to another instance.
        /// </summary>
        /// <param name="destination">The destination instance.</param>
        /// <exception cref="ArgumentNullException"><paramref name="destination"/> was null</exception>
        /// <exception cref="ArgumentException"><paramref name="destination"/> did not have the expected type</exception>
        public void CopyValuesTo(object destination)
        {
            if (destination == null)
            {
                throw new ArgumentNullException("destination", "Destination was null for MaterialsClassification.");
            }

            MaterialsClassification temp = destination as MaterialsClassification;
            if (temp == null)
            {
                throw new ArgumentException("Destination was not a MaterialsClassification.");
            }
                        
            if (this.Name != null)
            {
                temp.Name = this.Name;
            }
        }

        #endregion ICopiable Members

        /// <summary>
        /// Called when this instance in initialized (by the constructor defined in the generated code).
        /// Can be used to perform custom construction-time initialization.
        /// </summary>
        partial void OnItitialized()
        {
            this.Guid = Guid.NewGuid();
            this.CreateDate = DateTime.Now;
        }
    }
}
