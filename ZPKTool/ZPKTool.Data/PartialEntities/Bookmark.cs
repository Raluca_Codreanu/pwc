﻿using System;

namespace ZPKTool.Data
{
    /// <summary>
    /// Extension class for the Bookmark entity
    /// </summary>
    public partial class Bookmark : IIdentifiable, IOwnedObject
    {
        /// <summary>
        /// Sets the Owner property of this instance and of all the objects in its graph that implement IOwnedObject.
        /// </summary>
        /// <param name="newOwner">The new owner.</param>
        public void SetOwner(User newOwner)
        {
            this.Owner = newOwner;
        }

        /// <summary>
        /// Called when this instance in initialized (by the constructor defined in the generated code).
        /// Can be used to perform custom construction-time initialization.
        /// </summary>
        partial void OnItitialized()
        {            
            this.Guid = Guid.NewGuid();
            this.CreateDate = DateTime.Now;
        }
    }
}