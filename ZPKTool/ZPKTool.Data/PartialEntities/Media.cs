﻿using System;
using System.Collections.Generic;

namespace ZPKTool.Data
{
    /// <summary>
    /// Partial implementation of the Media entity object
    /// </summary>
    public partial class Media : IIdentifiable, IMasterDataObject, IOwnedObject, ICopiable
    {
        /// <summary>
        /// Creates a copy of this instance.        
        /// </summary>        
        /// <returns>A copy of the current instance.</returns>
        public Media Copy()
        {
            Media media = new Media();
            this.CopyValuesTo(media);
            return media;
        }

        #region ICopiable Members

        /// <summary>
        /// Copies the values from this instance to another instance.
        /// </summary>
        /// <param name="destination">The destination instance.</param>
        /// <exception cref="ArgumentNullException"><paramref name="destination"/> was null</exception>
        /// <exception cref="ArgumentException"><paramref name="destination"/> did not have the expected type</exception>
        public void CopyValuesTo(object destination)
        {
            if (destination == null)
            {
                throw new ArgumentNullException("destination", "Destination was null.");
            }

            Media temp = destination as Media;
            if (temp == null)
            {
                throw new ArgumentException("Destination was not a Media.", "destination");
            }

            temp.IsMasterData = this.IsMasterData;
            temp.OriginalFileName = this.OriginalFileName;
            temp.Size = this.Size;
            temp.Type = this.Type;

            if (this.Content != null)
            {
                byte[] contentCopy = new byte[this.Content.LongLength];
                this.Content.CopyTo(contentCopy, 0);
                temp.Content = contentCopy;
            }
        }

        #endregion

        #region IOwnedObject Members

        /// <summary>
        /// Sets the value of the <see cref="Owner " /> property on this instance and on all the objects in its graph that implement <see cref="IOwnedObject" />.
        /// </summary>
        /// <param name="newOwner">The new owner.</param>
        public void SetOwner(User newOwner)
        {
            this.Owner = newOwner;
        }

        #endregion

        /// <summary>
        /// Sets the value of the <see cref="IsMasterData" /> property on this instance and on all the objects in its graph that implement <see cref="IMasterDataObject" />.
        /// </summary>
        /// <param name="newValue">The new value for the <see cref="IsMasterData" /> property.</param>
        public void SetIsMasterData(bool newValue)
        {
            this.IsMasterData = newValue;
        }

        /// <summary>
        /// Called when this instance in initialized (by the constructor defined in the generated code).
        /// Can be used to perform custom construction-time initialization.
        /// </summary>
        partial void OnItitialized()
        {
            this.Guid = Guid.NewGuid();
            this.CreateDate = DateTime.Now;
        }
    }
}
