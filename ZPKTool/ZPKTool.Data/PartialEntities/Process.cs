﻿using System;

namespace ZPKTool.Data
{
    /// <summary>
    /// Partial implementation of the Process entity object
    /// </summary>
    public partial class Process : IIdentifiable, IMasterDataObject, IOwnedObject, ICopiable
    {
        #region IOwnedObject Members

        /// <summary>
        /// Sets the value of the <see cref="Owner " /> property on this instance and on all the objects in its graph that implement <see cref="IOwnedObject" />.
        /// </summary>
        /// <param name="newOwner">The new owner.</param>
        public void SetOwner(User newOwner)
        {
            this.Owner = newOwner;

            foreach (ProcessStep step in this.Steps)
            {
                step.SetOwner(newOwner);
            }
        }

        #endregion

        /// <summary>
        /// Copies the values to.
        /// </summary>
        /// <param name="destination">The destination.</param>
        public void CopyValuesTo(object destination)
        {
            if (destination == null)
            {
                throw new ArgumentNullException("destination", "Destination was null.");
            }

            Process temp = destination as Process;
            if (temp == null)
            {
                throw new ArgumentException("Destination was not a Process.");
            }

            temp.IsMasterData = this.IsMasterData;

            // TODO: Owner should not be copied because it may attach the copy to its data context.
            temp.Owner = this.Owner;
        }

        /// <summary>
        /// Sets the value of the <see cref="IsMasterData" /> property on this instance and on all the objects in its graph that implement <see cref="IMasterDataObject" />.
        /// </summary>
        /// <param name="newValue">The new value for the <see cref="IsMasterData" /> property.</param>
        public void SetIsMasterData(bool newValue)
        {
            this.IsMasterData = newValue;
            foreach (ProcessStep ps in this.Steps)
            {
                ps.SetIsMasterData(newValue);
            }
        }

        /// <summary>
        /// Sets the IsDeleted property on all ITrashable objects in this instance's graph.
        /// </summary>
        /// <param name="newValue">the new value for the IsDeleted property.</param>
        public void SetIsDeleted(bool newValue)
        {
            foreach (ProcessStep step in this.Steps)
            {
                step.SetIsDeleted(newValue);
            }
        }

        /// <summary>
        /// Called when this instance in initialized (by the constructor defined in the generated code).
        /// Can be used to perform custom construction-time initialization.
        /// </summary>
        partial void OnItitialized()
        {
            this.Guid = Guid.NewGuid();
            this.CreateDate = DateTime.Now;
        }
    }
}