﻿using System;

namespace ZPKTool.Data
{
    /// <summary>
    /// Partial implementation of the Consumable entity object
    /// </summary>
    public partial class Consumable : IIdentifiable, INameable, IMasterDataObject, IOwnedObject, ICopiable, ITrashable
    {        
        #region Properties

        /// <summary>
        /// Gets the type of the trash bin item.
        /// </summary>
        /// <value>The type of the trash bin item.</value>
        public TrashBinItemType TrashBinItemType
        {
            get { return TrashBinItemType.Consumable; }
        }

        #endregion Properties

        #region ICopiable Members

        /// <summary>
        /// Copies the values from this instance to another instance.
        /// </summary>
        /// <param name="destination">The destination instance.</param>
        /// <exception cref="ArgumentNullException"><paramref name="destination"/> was null</exception>
        /// <exception cref="ArgumentException"><paramref name="destination"/> did not have the expected type</exception>
        public void CopyValuesTo(object destination)
        {
            if (destination == null)
            {
                throw new ArgumentNullException("destination", "Destination was null.");
            }

            Consumable temp = destination as Consumable;
            if (temp == null)
            {
                throw new ArgumentException("Destination was not a Consumable.");
            }

            temp.Amount = this.Amount;

            if (this.Description != null)
            {
                temp.Description = this.Description;
            }

            temp.IsDeleted = this.IsDeleted;
            temp.IsMasterData = this.IsMasterData;

            if (this.Name != null)
            {
                temp.Name = this.Name;
            }

            temp.Price = this.Price;
            temp.CalculationCreateDate = this.CalculationCreateDate;
        }

        #endregion

        #region ITrashable Members

        /// <summary>
        /// Sets the IsDeleted property on this instance and all ITrashable objects in its graph.
        /// </summary>
        /// <param name="newValue">the new value for the IsDeleted property.</param>
        public void SetIsDeleted(bool newValue)
        {
            if (this.IsDeleted != newValue)
            {
                this.IsDeleted = newValue;
            }
        }

        #endregion ITrashable Members

        #region SetPropertiesInDepth

        /// <summary>
        /// Sets the Owner property of this instance and of all the objects in its graph that implement IOwnedObject.
        /// </summary>
        /// <param name="newOwner">The new owner.</param>
        public void SetOwner(User newOwner)
        {
            this.Owner = newOwner;
            if (this.Manufacturer != null)
            {
                this.Manufacturer.SetOwner(newOwner);
            }
        }

        /// <summary>
        /// Sets the value of the <see cref="IsMasterData" /> property on this instance and on all the objects in its graph that implement <see cref="IMasterDataObject" />.
        /// </summary>
        /// <param name="newValue">The new value for the <see cref="IsMasterData" /> property.</param>
        public void SetIsMasterData(bool newValue)
        {
            this.IsMasterData = newValue;
            if (this.Manufacturer != null)
            {
                this.Manufacturer.SetIsMasterData(newValue);
            }
        }

        #endregion SetPropertiesInDepth

        #region Methods

        /// <summary>
        /// Called when this instance in initialized (by the constructor defined in the generated code).
        /// Can be used to perform custom construction-time initialization.
        /// </summary>
        partial void OnItitialized()
        {
            this.Guid = Guid.NewGuid();
            this.CreateDate = DateTime.Now;
            this.CalculationCreateDate = DateTime.Now;
        }

        #endregion Methods
    }
}