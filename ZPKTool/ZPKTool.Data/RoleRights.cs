﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ZPKTool.Data
{
    /// <summary>
    /// Holds the association between role and rights and other helping methods to easier work with them.
    /// </summary>
    public static class RoleRights
    {
        /// <summary>
        /// Holds the role-rights associations.
        /// </summary>
        private static RoleRightsAssociation roleRights = new RoleRightsAssociation
            {
                {
                    Role.None,
                    new List<Right> { }
                },
                {
                    Role.Admin,
                    new List<Right>
                    {
                        Right.AdminTasks,
                        Right.EditMasterData,
                        Right.ViewAndReports,
                        Right.ReleaseProject,
                        Right.ProjectsAndCalculate
                    }
                },
                {
                    Role.KeyUser,
                    new List<Right>
                    {
                        Right.EditMasterData,
                        Right.ViewAndReports,
                        Right.ProjectsAndCalculate
                    }
                },
                {
                    Role.DataManager,
                    new List<Right>
                    {
                        Right.EditMasterData,
                        Right.ViewAndReports
                    }
                },
                {
                    Role.ProjectLeader,
                    new List<Right>
                    {
                        Right.ViewAndReports,
                        Right.ReleaseProject
                    }
                },
                {
                    Role.User,
                    new List<Right>
                    {
                        Right.ViewAndReports,
                        Right.ProjectsAndCalculate
                    }
                },
                {
                    Role.Viewer,
                    new List<Right>
                    {
                        Right.ViewAndReports
                    }
                }
            };

        /// <summary>
        /// Gets the roles separated.
        /// </summary>
        /// <param name="roles">The roles.</param>
        /// <returns>A list of roles.</returns>
        public static IEnumerable<Role> GetRolesSeparated(Role roles)
        {
            if (roles == Role.None)
            {
                yield return Role.None;
            }

            foreach (Role currentRole in Enum.GetValues(roles.GetType()))
            {
                if (roles.HasFlag(currentRole)
                    && currentRole != Role.None)
                {
                    yield return currentRole;
                }
            }
        }

        /// <summary>
        /// Determines whether the specified role has the specified role.
        /// </summary>
        /// <param name="roles">The roles.</param>
        /// <param name="roleToSearch">The role to search.</param>
        /// <returns>True if the role has the role, false otherwise.</returns>
        public static bool HasRole(Role roles, Role roleToSearch)
        {
            foreach (Role currentRole in Enum.GetValues(roles.GetType()))
            {
                if (roles.HasFlag(currentRole)
                    && currentRole == roleToSearch)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Gets the first role.
        /// </summary>
        /// <param name="roles">The roles.</param>
        /// <returns>A role.</returns>
        public static Role GetFirstRole(Role roles)
        {
            foreach (Role currentRole in Enum.GetValues(roles.GetType()))
            {
                if (roles.HasFlag(currentRole)
                    && currentRole != Role.None)
                {
                    return currentRole;
                }
            }

            return Role.None;
        }

        /// <summary>
        /// Determines whether the role contains multiple roles.
        /// </summary>
        /// <param name="roles">The role.</param>
        /// <returns>True if the role has multiple roles, false otherwise.</returns>
        public static bool HasMultipleRoles(Role roles)
        {
            var numberOfRoles = 0;
            foreach (Role currentRole in Enum.GetValues(roles.GetType()))
            {
                if (roles.HasFlag(currentRole)
                    && currentRole != Role.None)
                {
                    numberOfRoles++;
                    if (numberOfRoles >= 2)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Determine if the specified role has the specified right.
        /// </summary>
        /// <param name="roles">The roles.</param>
        /// <param name="right">The right.</param>
        /// <returns>True if the role has the specified rights, false otherwise.</returns>
        public static bool RoleHasRight(Role roles, Right right)
        {
            foreach (Role currentRole in Enum.GetValues(roles.GetType()))
            {
                if (roles.HasFlag(currentRole)
                    && currentRole != Role.None
                    && roleRights.Any(ro => ro.Key == roles && ro.Value.Any(ri => ri == right)))
                {
                    return true;
                }
            }

            return false;
        }
    }
}