﻿using System;
using System.Collections.Generic;

namespace ZPKTool.Data
{
    /// <summary>
    /// Provides utility methods for working with the domain objects.
    /// </summary>
    public static class EntityUtils
    {
        /// <summary>
        /// The maximum number of decimal places saved in the db.
        /// </summary>
        public const int MaxNumberOfDecimalPlacesSavedInDb = 16;

        /// <summary>
        /// The maximum value which can be entered into the largest decimal fields in the db. (i.e. decimal(29, 16));
        /// </summary>
        public const decimal MaxNumberValueAllowedInDb = 1000000000000m;

        /// <summary>
        /// Gets the parent project of a specified assembly. The assembly must be fully loaded.
        /// </summary>
        /// <param name="assy">The assembly.</param>
        /// <returns>A project or null if it does not have one or is not fully loaded.</returns>
        public static Project GetParentProject(Assembly assy)
        {
            if (assy == null)
            {
                return null;
            }

            if (assy.Project != null)
            {
                return assy.Project;
            }

            if (assy.ParentAssembly != null)
            {
                return GetParentProject(assy.ParentAssembly);
            }

            return null;
        }

        /// <summary>
        /// Gets the parent project of a specified part. The part must be fully loaded.
        /// </summary>
        /// <param name="part">The part.</param>
        /// <returns>A project or null if it does not have one or is not fully loaded.</returns>
        public static Project GetParentProject(Part part)
        {
            if (part == null)
            {
                return null;
            }

            if (part.Project != null)
            {
                return part.Project;
            }

            if (part.Assembly != null)
            {
                return GetParentProject(part.Assembly);
            }

            return null;
        }

        /// <summary>
        /// Gets the top assembly of a sub-assembly. The top assembly is obtained by walking up the reference chain without loading anything from the data source.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <returns>The top assembly of the specified assembly or the assembly if does not have an assembly parent.</returns>
        public static Assembly GetTopAssembly(Assembly assembly)
        {
            if (assembly == null)
            {
                return null;
            }

            if (assembly.ParentAssembly != null)
            {
                return GetTopAssembly(assembly.ParentAssembly);
            }

            return assembly;
        }

        /// <summary>
        /// Gets the sub-assembly with the searched Id from the assembly hierarchy.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <param name="assemblyId">The assembly id.</param>
        /// <returns>The assembly with the specified id or null if not found. </returns>
        public static Assembly GetSubAssembly(Assembly assembly, Guid assemblyId)
        {
            if (assembly == null
                || assemblyId == Guid.Empty)
            {
                return null;
            }

            // Search the assembly's sub-assemblies level by level by adding the assemblies to search on each level to an inspection queue.
            Assembly searchedAssembly = null;
            Queue<Assembly> inspectionQueue = new Queue<Assembly>();
            inspectionQueue.Enqueue(assembly);
            while (inspectionQueue.Count > 0)
            {
                var crtAssembly = inspectionQueue.Dequeue();
                if (crtAssembly.Guid == assemblyId)
                {
                    searchedAssembly = crtAssembly;
                    break;
                }

                // The inspected assembly was not the searched one so we add its children to the inspection queue, to be searched.
                foreach (var assy in crtAssembly.Subassemblies)
                {
                    inspectionQueue.Enqueue(assy);
                }
            }

            return searchedAssembly;
        }

        /// <summary>
        /// Determines whether the specified value represents a master data object.
        /// </summary>        
        /// <param name="value">The value.</param>
        /// <returns>
        /// true if the value is a master data object; otherwise, false.
        /// </returns>
        public static bool IsMasterData(object value)
        {
            bool isMasterdata = false;
            var masterObj = value as IMasterDataObject;
            if (masterObj != null)
            {
                isMasterdata = masterObj.IsMasterData;
            }

            return isMasterdata;
        }

        /// <summary>
        /// Converts the specified value to the corresponding value in the specified enumeration.
        /// <para />
        /// The type of the value must be convertible to Enum (should be an integral numeric type, e.g short, int, etc.).
        /// </summary>
        /// <remarks>
        /// This method is useful to convert some entity properties that are stored in db as short numbers to their corresponding enumeration from Data.
        /// </remarks>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <typeparam name="TEnum">The type of the enumeration.</typeparam>
        /// <param name="value">The value.</param>
        /// <param name="defaultValue">The default value to return if <paramref name="value"/> is null.</param>
        /// <exception cref="InvalidOperationException"><typeparamref name="TEnum"/> is not an enumeration</exception>
        /// <returns>
        /// The <typeparamref name="TEnum"/> value corresponding to <paramref name="value"/> or <paramref name="defaultValue"/> if <paramref name="value"/> is null.
        /// </returns>        
        public static TEnum ConvertToEnum<TValue, TEnum>(TValue? value, TEnum defaultValue)
            where TValue : struct
            where TEnum : struct
        {
            if (!typeof(TEnum).IsEnum)
            {
                throw new InvalidOperationException("TEnum type must be an enumeration.");
            }

            if (value.HasValue)
            {
                return (TEnum)Enum.ToObject(typeof(TEnum), value.Value);
            }
            else
            {
                return defaultValue;
            }
        }               
        
        /// <summary>
        /// Prepares the number for scrambling by checking its size and number of decimal places.
        /// If the number has more decimal places than maximum allowed the number will get rounded.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <returns>A number.</returns>
        public static decimal? PrepareNumberForScrambling(decimal? number)
        {
            if (!number.HasValue)
            {
                return null;
            }

            return PrepareNumberForScrambling(number.Value, MaxNumberValueAllowedInDb, MaxNumberOfDecimalPlacesSavedInDb);
        }

        /// <summary>
        /// Prepares the number for scrambling by checking its size and number of decimal places.
        /// If the number has more decimal places than maximum specified the number will get rounded.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <param name="maxSupportedDecimal">The max supported decimal.</param>
        /// <param name="maxNumberOfDecimalPlaces">The max number of decimal places.</param>
        /// <returns>A number.</returns>
        public static decimal? PrepareNumberForScrambling(decimal? number, decimal maxSupportedDecimal, int maxNumberOfDecimalPlaces)
        {
            if (!number.HasValue)
            {
                return null;
            }

            return PrepareNumberForScrambling(number.Value, maxSupportedDecimal, maxNumberOfDecimalPlaces);
        }

        /// <summary>
        /// Prepares the number for scrambling by checking its size and number of decimal places.
        /// If the number has more decimal places than maximum allowed the number will get rounded.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <returns>A number.</returns>
        public static decimal PrepareNumberForScrambling(decimal number)
        {
            return PrepareNumberForScrambling(number, MaxNumberValueAllowedInDb, MaxNumberOfDecimalPlacesSavedInDb);
        }

        /// <summary>
        /// Prepares the number for scrambling by checking its size and number of decimal places.
        /// If the number has more decimal places than maximum specified the number will get rounded.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <param name="maxSupportedDecimal">The max supported decimal.</param>
        /// <param name="maxNumberOfDecimalPlaces">The max number of decimal places.</param>
        /// <returns>A number.</returns>
        /// <exception cref="System.ArgumentException">
        /// The number was larger than the maximum supported value.;number
        /// or
        /// The number was lower than the minimum supported value.;number
        /// </exception>
        public static decimal PrepareNumberForScrambling(decimal number, decimal maxSupportedDecimal, int maxNumberOfDecimalPlaces)
        {
            var minSupportedDecimal = maxSupportedDecimal * -1;
            if (number > maxSupportedDecimal)
            {
                throw new ArgumentException("The number was larger than the maximum supported value.", "number");
            }
            else if (number < minSupportedDecimal)
            {
                throw new ArgumentException("The number was lower than the minimum supported value.", "number");
            }

            return Math.Round(number, maxNumberOfDecimalPlaces);
        }

        /// <summary>
        /// Gets the specified entity name.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>The entity name if it implements <see cref="ZPKTool.Data.INameable"/> interface or <see cref="string.empty"/> otherwise</returns>
        public static string GetEntityName(object entity)
        {
            var objectName = string.Empty;
            var nameableObject = entity as INameable;
            if (nameableObject != null)
            {
                objectName = nameableObject.Name;
            }

            return objectName;
        }
    }
}