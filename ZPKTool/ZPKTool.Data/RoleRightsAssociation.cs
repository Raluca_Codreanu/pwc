﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ZPKTool.Data
{
    /// <summary>
    /// Holds the association between the roles and rights.
    /// </summary>
    [Serializable]
    public class RoleRightsAssociation : Dictionary<Role, IEnumerable<Right>>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RoleRightsAssociation"/> class.
        /// </summary>
        public RoleRightsAssociation()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RoleRightsAssociation"/> class.
        /// </summary>
        /// <param name="info">A <see cref="T:System.Runtime.Serialization.SerializationInfo" /> object containing the information required to serialize the <see cref="T:System.Collections.Generic.Dictionary`2" />.</param>
        /// <param name="context">A <see cref="T:System.Runtime.Serialization.StreamingContext" /> structure containing the source and destination of the serialized stream associated with the <see cref="T:System.Collections.Generic.Dictionary`2" />.</param>
        protected RoleRightsAssociation(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}