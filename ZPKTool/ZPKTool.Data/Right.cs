﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;

namespace ZPKTool.Data
{
    /// <summary>
    /// The types of user access rights.
    /// </summary>
    [SuppressMessage("Microsoft.Design", "CA1008:EnumsShouldHaveZeroValue", Justification = "Can not add a new value without being required by the business logic.")]    
    public enum Right
    {
        //// THE NEXT VALUE TO BE ADDED SHOULD BE 0 (ZERO)

        /// <summary>
        /// Allowed to perform administrative operations (see the specs for a list of them).
        /// </summary>
        AdminTasks = 1,

        /// <summary>
        /// Allowed to perform CRUD operations on Master Data.
        /// </summary>
        EditMasterData = 2,

        /// <summary>
        /// Allowed to create/edit projects and all entities included in a project.
        /// </summary>
        ProjectsAndCalculate = 3,

        /// <summary>
        /// Allowed to view released projects and their cost calculations.
        /// </summary>
        ViewAndReports = 4,

        /// <summary>
        /// Allowed to release a project.
        /// </summary>
        ReleaseProject = 5
    }
}
