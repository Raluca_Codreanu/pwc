﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace ZPKTool.Data
{
    /// <summary>
    /// Utility for encrypting and decrypting text.
    /// </summary>
    public static class Encryption
    {
        /// <summary>
        /// Password to encrypt and decrypt with.
        /// </summary>
        public const string EncryptionPassword = "E295{qS7";

        /// <summary>
        /// Encrypts the text.
        /// </summary>
        /// <param name="plainText">Text to be encrypted.</param>
        /// <param name="passwordEncryption">Password to encrypt with.</param>
        /// <param name="initVector">Needs to be 16 ASCII characters long.</param>
        /// <param name="keysize">Can be 128, 192, or 256 in size.</param>
        /// <returns>An encrypted string.</returns>
        public static string Encrypt(string plainText, string passwordEncryption, string initVector = "aT2XhA;N5xJbRXks", int keysize = 256)
        {
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            byte[] initialVectorBytes = Encoding.UTF8.GetBytes(initVector);            
            PasswordDeriveBytes password = new PasswordDeriveBytes(passwordEncryption, null);
            byte[] keyBytes = password.GetBytes(keysize / 8);
            byte[] cipherTextBytes = null; 
                using (RijndaelManaged symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.Mode = CipherMode.CBC;
                    using (ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initialVectorBytes))
                    {
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            using (CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                            {
                                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                                cryptoStream.FlushFinalBlock();
                                cipherTextBytes = memoryStream.ToArray();                               
                            }
                        }
                    }

                    symmetricKey.Clear();
                 }

                return Convert.ToBase64String(cipherTextBytes);            
        }

        /// <summary>
        /// Decrypts the specified text.
        /// </summary>
        /// <param name="plainText">Text to be decrypted.</param>
        /// <param name="passwordEncryption">Password to decrypt with.</param>
        /// <param name="initVector">Needs to be 16 ASCII characters long.</param>
        /// <param name="keysize">Can be 128, 192, or 256 in size.</param>
        /// <returns>A decrypted string.</returns>
        public static string Decrypt(string plainText, string passwordEncryption, string initVector = "aT2XhA;N5xJbRXks", int keysize = 256)
        {
            if (string.IsNullOrWhiteSpace(plainText))
            {
                return string.Empty;
            }

            byte[] initialVectorBytes = Encoding.UTF8.GetBytes(initVector);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passwordEncryption, null);
            byte[] keyBytes = password.GetBytes(keysize / 8);
            byte[] cipherTextBytes = Convert.FromBase64String(plainText);
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];
            int byteCount = 0;

            using (RijndaelManaged symmetricKey = new RijndaelManaged())
            {
                symmetricKey.Mode = CipherMode.CBC;
                using (ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, initialVectorBytes))
                {
                    using (MemoryStream memStream = new MemoryStream(cipherTextBytes))
                    {
                        using (CryptoStream cryptoStream = new CryptoStream(memStream, decryptor, CryptoStreamMode.Read))
                        {
                            byteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                        }
                    }
                }

                symmetricKey.Clear();
            }

            return Encoding.UTF8.GetString(plainTextBytes, 0, byteCount);
        }
    }
}
