﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Data
{
    /// <summary>
    /// Holds the meta data for a media.
    /// </summary>
    public class MediaMetaData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MediaMetaData"/> class.
        /// </summary>
        public MediaMetaData()
        {
        }

        /// <summary>
        /// Gets or sets the Id of the media.
        /// </summary>
        public Guid Guid { get; set; }

        /// <summary>
        /// Gets or sets the type of the media.
        /// </summary>
        public MediaType Type { get; set; }

        /// <summary>
        /// Gets or sets the size of the media.
        /// </summary>
        public int? Size { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the media is master data.
        /// </summary>
        public bool IsMasterData { get; set; }

        /// <summary>
        /// Gets or sets the name of the original file of the media.
        /// </summary>        
        public string OriginalFileName { get; set; }

        /// <summary>
        /// Gets or sets the id of the media's parent entity.
        /// </summary>        
        public Guid ParentId { get; set; }
    }
}
