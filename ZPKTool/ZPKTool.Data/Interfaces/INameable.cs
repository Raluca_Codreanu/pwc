﻿namespace ZPKTool.Data
{
    /// <summary>
    /// Represents an object that has a name.
    /// </summary>
    public interface INameable
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>        
        string Name { get; set; }
    }
}
