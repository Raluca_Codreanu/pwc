﻿namespace ZPKTool.Data
{
    /// <summary>
    /// Represents an object that can be part of master data.
    /// </summary>
    public interface IMasterDataObject
    {
        /// <summary>
        /// Gets or sets a value indicating whether this instance is part of master data.
        /// </summary>
        bool IsMasterData { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is stock or a custom master data.
        /// </summary>
        bool IsStockMasterData { get; set; }

        /// <summary>
        /// Sets the value of the <see cref="IsMasterData"/> property on this instance and on all the objects in its graph that implement <see cref="IMasterDataObject"/>.
        /// </summary>
        /// <param name="newValue">The new value for the <see cref="IsMasterData"/> property.</param>
        void SetIsMasterData(bool newValue);
    }
}
