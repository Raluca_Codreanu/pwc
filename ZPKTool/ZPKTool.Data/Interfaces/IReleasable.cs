﻿namespace ZPKTool.Data
{
    /// <summary>
    /// Represents an object that can be released (has IsReleased property).
    /// </summary>
    public interface IReleasable
    {
        /// <summary>
        /// Gets or sets a value indicating whether the object is released.
        /// </summary>
        bool IsReleased { get; set; }
    }
}