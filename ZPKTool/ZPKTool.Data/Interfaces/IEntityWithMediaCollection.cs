﻿using System.Collections.Generic;

namespace ZPKTool.Data
{
    /// <summary>
    /// Defines an entity that has multiple media objects.
    /// </summary>
    public interface IEntityWithMediaCollection
    {
        /// <summary>
        /// Gets the collection of media entities.
        /// </summary>        
        ICollection<Media> Media { get; }
    }
}
