﻿namespace ZPKTool.Data
{
    /// <summary>
    /// Represents an object that has an owner.
    /// </summary>
    public interface IOwnedObject
    {
        /// <summary>
        /// Gets or sets the owner.
        /// </summary>
        User Owner { get; set; }

        /// <summary>
        /// Sets the value of the <see cref="Owner "/> property on this instance and on all the objects in its graph that implement <see cref="IOwnedObject"/>.
        /// </summary>
        /// <param name="newOwner">The new owner.</param>
        void SetOwner(User newOwner);
    }
}
