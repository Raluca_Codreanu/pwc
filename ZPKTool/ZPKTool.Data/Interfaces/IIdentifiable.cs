﻿using System;

namespace ZPKTool.Data
{
    /// <summary>
    /// Represents an object that has a unique identifier.
    /// </summary>
    public interface IIdentifiable
    {
        /// <summary>
        /// Gets or sets the unique identifier of the object.
        /// </summary>        
        Guid Guid { get; set; }
    }
}
