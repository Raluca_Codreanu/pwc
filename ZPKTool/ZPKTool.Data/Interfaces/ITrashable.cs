﻿namespace ZPKTool.Data
{
    /// <summary>
    /// Represents an entity that can be deleted to trash bin.
    /// </summary>
    public interface ITrashable : IOwnedObject, IIdentifiable, INameable
    {
        /// <summary>
        /// Gets the type of the trash bin item.
        /// </summary>
        /// <value>The type of the trash bin item.</value>
        TrashBinItemType TrashBinItemType { get; }

        /// <summary>
        /// Gets or sets a value indicating whether this object is deleted.
        /// </summary>
        bool IsDeleted { get; set; }

        /// <summary>
        /// Sets the value of the <see cref="IsDeleted"/> property on this instance and on all the objects in its graph that implement <see cref="ITrashable"/>.
        /// </summary>
        /// <param name="newValue">The new value for the IsDeleted property.</param>
        void SetIsDeleted(bool newValue);
    }
}