﻿using System;

namespace ZPKTool.Data
{
    /// <summary>
    /// Defines methods that allow to copy information from an entity.
    /// </summary>
    public interface ICopiable
    {
        /// <summary>
        /// Copies the values from this instance to another instance.
        /// </summary>
        /// <param name="destination">The destination instance.</param>
        /// <exception cref="ArgumentNullException"><paramref name="destination"/> was null</exception>
        /// <exception cref="ArgumentException"><paramref name="destination"/> did not have the expected type</exception>
        void CopyValuesTo(object destination);
    }
}
