﻿namespace ZPKTool.Data
{
    /// <summary>
    /// Defines an entity that has one media object.
    /// </summary>
    public interface IEntityWithMedia : IIdentifiable
    {
        /// <summary>
        /// Gets or sets the media object
        /// </summary>
        /// <value>The media.</value>
        Media Media { get; set; }
    }
}
