﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Updater
{
    /// <summary>
    /// A data structure created from the information available on the update server
    /// </summary>
    public class UpdateInfo
    {        
        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateInfo"/> class.
        /// </summary>
        public UpdateInfo()
        {
            this.Features = new List<Feature>();
            this.Fixes = new List<Fix>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateInfo" /> class.
        /// </summary>
        /// <param name="features">The features.</param>
        /// <param name="fixes">The fixes.</param>
        public UpdateInfo(IEnumerable<Feature> features, IEnumerable<Fix> fixes)
        {
            if (features == null)
            {
                throw new ArgumentNullException("features", "The features were null.");
            }

            if (fixes == null)
            {
                throw new ArgumentNullException("fixes", "The fixes were null.");
            }

            this.Features = new List<Feature>(features);
            this.Fixes = new List<Fix>(fixes);
        }

        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        /// <value>The version.</value>
        public string Version { get; set; }

        /// <summary>
        /// Gets the fixes.
        /// </summary>
        /// <value>The fixes.</value>
        public ICollection<Fix> Fixes { get; private set; }

        /// <summary>
        /// Gets the features.
        /// </summary>
        /// <value>The features.</value>
        public ICollection<Feature> Features { get; private set; }
    }
}
