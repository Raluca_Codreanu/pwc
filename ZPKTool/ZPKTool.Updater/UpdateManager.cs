﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Timers;
using System.Windows.Threading;
using System.Xml;
using ZPKTool.Common;

namespace ZPKTool.Updater
{
    /// <summary>
    /// Handles the update operations, allowing the user to access the updater's services
    /// </summary>
    public class UpdateManager
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// the name of the update xml
        /// </summary>
        private static readonly string updatexml = "Update.xml";

        /// <summary>
        /// Path to the temp folder where the updater is copied and from where it runs.
        /// </summary>
        private readonly string tempFolderPath = Path.GetTempPath() + Guid.NewGuid().ToString();

        /// <summary>
        /// current app version
        /// </summary>
        private string currentVersion;

        /// <summary>
        /// Path of the update server.
        /// </summary>
        private string updatePath;

        /// <summary>
        /// Product Cost Manager Viewer update paths list.
        /// </summary>
        private List<string> viewerUpdatePaths;

        /// <summary>
        /// The file/folder that will be downloaded
        /// </summary>
        private string setupSource;

        /// <summary>
        /// Path where the install kit is downloaded
        /// </summary>
        private string installKitPath;

        /// <summary>
        /// Timer for the Auto-Update Service. It is initialized at startup or when update settings are modified
        /// </summary>
        private DispatcherTimer timer;

        /// <summary>
        /// Weak event listener for the PropertyChanged notification
        /// </summary>
        private WeakEventListener<PropertyChangedEventArgs> propertyChangedListener;

        /// <summary>
        /// A value indicating whether has been found an update for the Viewer application.
        /// </summary>
        private bool viewerUpdateFound = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateManager"/> class.
        /// </summary>
        public UpdateManager()
        {
            this.viewerUpdatePaths = new List<string>();
            this.viewerUpdatePaths.Add("http://download.me.de/PCM/PCMViewer/");
            this.viewerUpdatePaths.Add("http://www.productcostmanager.com/PCMViewer");

            this.RefreshUpdatePath();
            if (!GlobalSettingsManager.Instance.StartInViewerMode)
            {
                this.RefreshUpdateSettings();
            }

            this.propertyChangedListener = new WeakEventListener<PropertyChangedEventArgs>(SettingsChanged);
            PropertyChangedEventManager.AddListener(GlobalSettingsManager.Instance, propertyChangedListener, ReflectionUtils.GetPropertyName(() => GlobalSettingsManager.Instance.AutomaticUpdatePath));
            PropertyChangedEventManager.AddListener(UserSettingsManager.Instance, propertyChangedListener, ReflectionUtils.GetPropertyName(() => UserSettingsManager.Instance.AutomaticUpdateCheckPeriod));
            PropertyChangedEventManager.AddListener(UserSettingsManager.Instance, propertyChangedListener, ReflectionUtils.GetPropertyName(() => UserSettingsManager.Instance.AutomaticUpdatesOn));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateManager"/> class.
        /// </summary>
        /// <param name="currentVersion">The current version.</param>
        public UpdateManager(string currentVersion)
            : this()
        {
            if (string.IsNullOrEmpty(currentVersion))
            {
                // @coderevnew: If the currentVersion parameter is invalid the updater won't function properly. You have to throw an ArgumentException, not just return.
                return;
            }

            this.currentVersion = currentVersion;
        }

        /// <summary>
        /// Occurs when a new update is found
        /// </summary>
        public event EventHandler UpdateFound;

        /// <summary>
        /// Occurs when progress of the download operation is modified
        /// </summary>
        public event EventHandler<DownloadProgressEventArgs> DownloadProgress;

        /// <summary>
        /// Gets or sets the update data stored after a check was performed
        /// </summary>
        /// <value>The update info.</value>
        public UpdateInfo UpdateInfo { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user pressed cancel, while the downloading operation was underway
        /// </summary>
        public bool DownloadCanceled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user pressed cancel, while the checking operation was underway
        /// </summary>
        public bool CheckCanceled { get; set; }

        /// <summary>
        /// Performs the Update check, reads the xml file and checks if the available version is different from the current one
        /// </summary>
        /// <returns>true if new update is found</returns>
        public bool CheckForUpdate()
        {
            if (this.currentVersion != null)
            {
                if (GlobalSettingsManager.Instance.StartInViewerMode)
                {
                    this.updatePath = this.GetViewerUpdatePath();
                    if (!string.IsNullOrEmpty(this.updatePath))
                    {
                        return true;
                    }
                }
                else
                {
                    var updateData = this.GetUpdateData(this.updatePath);
                    this.UpdateInfo = updateData != null ? updateData.Item1 : null;
                    this.setupSource = updateData != null ? updateData.Item2 : null;
                    if (this.UpdateInfo != null)
                    {
                        if (IsNewer(UpdateInfo.Version, this.currentVersion))
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Called from the Gui, stars the download operation, decide which download mode to start
        /// </summary>
        public void Download()
        {
            this.DownloadCanceled = false;

            // implementation for directory instead of file later
            string sourcePath = Path.Combine(this.updatePath, this.setupSource);

            if (sourcePath.StartsWith("http"))
            {
                this.HttpDownload(sourcePath);
            }
            else if (sourcePath.StartsWith("\\\\"))
            {
                this.LocalNetworkDownload(sourcePath);
            }
            else
            {
                this.DownloadCanceled = true;
                log.Error("Wrong URI format. Source path must be of type http or UNC.");
                throw new UpdateException();
            }
        }

        /// <summary>
        /// called after user confirms the install, starts the Updater process, control is shifter to the Updater at this point
        /// </summary>
        public void StartSetup()
        {
            if (this.installKitPath == null)
            {
                return;
            }

            this.StoreSettingsInTempFolder();

            Process updaterProcess = new Process();
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetEntryAssembly();
            string appPath = Path.GetDirectoryName(assembly.Location);
            string filename = Path.GetFileName(this.setupSource);

            // /copy argument signals the updater to copy itself in the temp folder
            updaterProcess.StartInfo.Arguments = "\"" + tempFolderPath + "\" \"" + appPath + "\" \"" + filename + "\" /copy";
            updaterProcess.StartInfo.FileName = typeof(Updater).Assembly.Location;
            updaterProcess.StartInfo.CreateNoWindow = true;
            updaterProcess.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            updaterProcess.Start();
        }

        /// <summary>
        /// Cleanups the downloaded files.
        /// </summary>
        public void Cleanup()
        {
            if (Directory.Exists(tempFolderPath))
            {
                Directory.Delete(tempFolderPath, true);
            }
        }

        /// <summary>
        /// Handler for settings changed event
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void SettingsChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "AutomaticUpdatePath")
            {
                this.RefreshUpdatePath();
            }
            else if (e.PropertyName == "AutomaticUpdateCheckPeriod" || e.PropertyName == "AutomaticUpdatesOn")
            {
                this.RefreshUpdateSettings();
            }
        }

        /// <summary>
        /// Timers the tick.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void TimerTick(object sender, EventArgs e)
        {
            timer.IsEnabled = false;
            AutoCheck();
            UserSettingsManager.Instance.AutomaticUpdateLastCheck = DateTime.Now;
            UserSettingsManager.Instance.Save();
        }

        /// <summary>
        /// Gets the update info from the xml on the update server
        /// </summary>
        /// <param name="updateSourcePath">Path of the update server.</param>
        /// <returns>
        /// A tuple object containing the UpdateInfo, the setup source and the setup start file.
        /// </returns>
        private Tuple<UpdateInfo, string, string> GetUpdateData(string updateSourcePath)
        {
            if (string.IsNullOrEmpty(updateSourcePath))
            {
                throw new UpdateException("Update server path not specified.");
            }

            if (!(updateSourcePath.StartsWith("http") || updateSourcePath.StartsWith("\\\\")))
            {
                throw new UpdateException("Update server path is invalid.");
            }

            try
            {
                UpdateInfo updateInfo = new UpdateInfo();
                string updateSetupSource = null;
                string updateSetupStartFile = null;
                string xmlPath;
                if (updateSourcePath.StartsWith("http"))
                {
                    if (!updateSourcePath.EndsWith("/"))
                    {
                        updateSourcePath += "/";
                    }

                    xmlPath = updateSourcePath + updatexml;
                }
                else
                {
                    xmlPath = Path.Combine(updateSourcePath, updatexml);
                }

                using (XmlTextReader textReader = new XmlTextReader(xmlPath))
                {
                    while (textReader.Read())
                    {
                        if (this.CheckCanceled)
                        {
                            updateInfo = null;
                            updateSetupSource = null;
                            updateSetupStartFile = null;
                            return null;
                        }

                        XmlNodeType nodeType = textReader.NodeType;
                        if (nodeType == XmlNodeType.Element)
                        {
                            if (textReader.Name.Equals("version", StringComparison.OrdinalIgnoreCase))
                            {
                                updateInfo.Version = textReader.ReadElementContentAsString();
                            }
                            else if (textReader.Name.Equals("feature", StringComparison.OrdinalIgnoreCase))
                            {
                                string title = textReader.ReadElementContentAsString();
                                Feature feature = new Feature();
                                feature.Title = title;
                                updateInfo.Features.Add(feature);
                            }
                            else if (textReader.Name.Equals("fix", StringComparison.OrdinalIgnoreCase))
                            {
                                string title = textReader.ReadElementContentAsString();
                                Fix fix = new Fix();
                                fix.Title = title;
                                updateInfo.Fixes.Add(fix);
                            }
                            else if (textReader.Name.Equals("setupsource", StringComparison.OrdinalIgnoreCase))
                            {
                                updateSetupSource = textReader.ReadElementContentAsString();
                            }
                            else if (textReader.Name.Equals("setupstartfile", StringComparison.OrdinalIgnoreCase))
                            {
                                updateSetupStartFile = textReader.ReadElementContentAsString();
                            }
                        }
                    }
                }

                // check if setupsource, setupstartfile and update version were specified correctly
                if (string.IsNullOrEmpty(updateSetupSource))
                {
                    throw new UpdateFileWrongFormatException("setupsource tag was not found");
                }

                if (string.IsNullOrEmpty(updateSetupStartFile))
                {
                    throw new UpdateFileWrongFormatException("setupstartfile tag was not found");
                }

                if (updateInfo == null || string.IsNullOrEmpty(updateInfo.Version))
                {
                    throw new UpdateFileWrongFormatException("Version tag was not found");
                }

                return new Tuple<UpdateInfo, string, string>(updateInfo, updateSetupSource, updateSetupStartFile);
            }
            catch (XmlException)
            {
                throw new UpdateFileWrongFormatException("Wrong xml file format");
            }
            catch (IOException)
            {
                throw new UpdateServerOfflineException("Server offline");
            }
            catch (WebException)
            {
                throw new UpdateServerOfflineException("Server not found");
            }
            catch (UriFormatException)
            {
                // Is thrown by the XmlTextReader when the URI contains invalid characters
                throw new UpdateException("Update server path contains invalid characters.");
            }
        }

        /// <summary>
        /// Determines whether a specified version is newer than the other one.
        /// </summary>
        /// <param name="firstVersion">The first version.</param>
        /// <param name="secondVersion">The second version.</param>
        /// <returns>
        /// <c>true</c> if the specified first version is newer; otherwise, <c>false</c>.
        /// </returns>
        private bool IsNewer(string firstVersion, string secondVersion)
        {
            if (string.IsNullOrEmpty(firstVersion))
            {
                return false;
            }

            try
            {
                string[] firstVersionSplit = firstVersion.Split(new char[] { '.' });
                string[] secondVersionSplit = secondVersion.Split(new char[] { '.' });

                if (secondVersionSplit.Length != firstVersionSplit.Length)
                {
                    throw new UpdateFileWrongFormatException("Bad format for version in update file");
                }

                for (int index = 0; index < secondVersionSplit.Length; index++)
                {
                    int secondVer = Convert.ToInt32(secondVersionSplit[index]);
                    int firstVer = Convert.ToInt32(firstVersionSplit[index]);
                    if (secondVer < firstVer)
                    {
                        return true;
                    }
                    else if (secondVer > firstVer)
                    {
                        return false;
                    }
                }
            }
            catch (FormatException)
            {
                throw new UpdateFileWrongFormatException("Bad format for version in update file");
            }
            catch (OverflowException)
            {
                throw new UpdateFileWrongFormatException("Overflow Exception when reading the version");
            }

            return false;
        }

        /// <summary>
        /// Refreshes the Update Manager. Called at system startup and when settings are modified
        /// </summary>
        private void RefreshUpdateSettings()
        {
            // timer is running, it needs to be disabled
            if (timer != null)
            {
                timer.IsEnabled = false;
            }

            if (UserSettingsManager.Instance.AutomaticUpdatesOn)
            {
                DateTime lastCheck;
                if (UserSettingsManager.Instance.AutomaticUpdateLastCheck == null)
                {
                    // when the application is first installed, last check will be null
                    lastCheck = DateTime.Now;
                }
                else
                {
                    lastCheck = UserSettingsManager.Instance.AutomaticUpdateLastCheck;
                }

                int checkPeriod = UserSettingsManager.Instance.AutomaticUpdateCheckPeriod;
                TimeSpan diference = DateTime.Now - lastCheck;

                if (timer == null)
                {
                    timer = new DispatcherTimer();
                }

                if (diference > TimeSpan.FromDays(checkPeriod) || GlobalSettingsManager.Instance.StartInViewerMode)
                {
                    timer.Interval = TimeSpan.FromSeconds(1);
                }
                else
                {
                    // calculate the value in milliseconds, if it is larger than int max, set it as int max to avoid error
                    long longVal = ((checkPeriod * 24 * 60 * 60) - diference.Seconds) * 1000L;
                    if (longVal > int.MaxValue)
                    {
                        longVal = int.MaxValue;
                    }

                    timer.Interval = TimeSpan.FromMilliseconds(longVal);
                }

                timer.Tick += new EventHandler(TimerTick);
                timer.IsEnabled = true;
                timer.Start();
            }
        }

        /// <summary>
        /// Refreshes the update path, called when the user modifies the update settings
        /// </summary>
        private void RefreshUpdatePath()
        {
            this.updatePath = GlobalSettingsManager.Instance.StartInViewerMode ? string.Empty : GlobalSettingsManager.Instance.AutomaticUpdatePath;
            this.UpdateInfo = null;
        }

        /// <summary>
        /// Get the Viewer update path, checking the list of viewer paths.
        /// </summary>
        /// <returns>The first available path found / Empty string - otherwise.</returns>
        private string GetViewerUpdatePath()
        {
            if (this.viewerUpdatePaths.Count == 0)
            {
                return string.Empty;
            }

            this.UpdateInfo = null;
            this.setupSource = null;
            var viewerUpdatePath = string.Empty;
            int index = 0;

            do
            {
                if (this.CheckIsUpdateAvailable(this.viewerUpdatePaths[index], this.viewerUpdatePaths.Count == index + 1 ? false : true))
                {
                    viewerUpdatePath = viewerUpdatePaths[index];
                }

                index++;
            }
            while (index < this.viewerUpdatePaths.Count);

            return viewerUpdatePath;
        }

        /// <summary>
        /// Checks if the update path is available.
        /// </summary>
        /// <param name="stringPath">Path of the update server.</param>
        /// <param name="ignoreErrors">A value indicating whether to throw or not the erro.</param>
        /// <returns>True if update path is available, false otherwise.</returns>
        private bool CheckIsUpdateAvailable(string stringPath, bool ignoreErrors)
        {
            if (this.currentVersion != null)
            {
                Tuple<UpdateInfo, string, string> updateData = null;

                try
                {
                    updateData = this.GetUpdateData(stringPath);
                    if (updateData != null && this.UpdateInfo != null)
                    {
                        if (IsNewer(updateData.Item1.Version, this.currentVersion)
                            && IsNewer(updateData.Item1.Version, this.UpdateInfo.Version))
                        {
                            this.UpdateInfo = updateData.Item1;
                            this.setupSource = updateData.Item2;
                            this.viewerUpdateFound = true;
                            return true;
                        }
                    }
                    else if (updateData != null)
                    {
                        if (IsNewer(updateData.Item1.Version, this.currentVersion))
                        {
                            this.UpdateInfo = updateData.Item1;
                            this.setupSource = updateData.Item2;
                            this.viewerUpdateFound = true;
                            return true;
                        }
                        else
                        {
                            this.viewerUpdateFound = true;
                        }
                    }
                }
                catch (UpdateException ex)
                {
                    if (ignoreErrors || this.viewerUpdateFound)
                    {
                        log.InfoException(ex.Message, ex);
                    }
                    else
                    {
                        throw;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Performs the AutoCheck when the timer expires
        /// </summary>
        public void AutoCheck()
        {
            try
            {
                if (this.CheckForUpdate())
                {
                    if (this.UpdateFound != null)
                    {
                        this.UpdateFound(this, EventArgs.Empty);
                    }
                }
            }
            catch (UpdateException ex)
            {
                log.InfoException(ex.Message, ex);
            }
        }

        /// <summary>
        /// Copies the update file from the specified stream into the path specified by the <see cref="ZPKTool.Updater.UpdateManager.installKitPath"/> field.
        /// </summary>
        /// <param name="source">The stream containing the update.</param>
        /// <param name="streamLength">The length of of the stream. The stream's Length property is not set for all types of streams so its length must be
        /// determined where it is created and passed here.</param>
        private void CopyUpdateFromStream(Stream source, long streamLength)
        {
            string filename = this.setupSource.Split(new char[] { '\\' }).Last();
            this.installKitPath = Path.Combine(tempFolderPath, filename);
            if (!Directory.Exists(tempFolderPath))
            {
                Directory.CreateDirectory(tempFolderPath);
            }

            using (Stream strLocal = new FileStream(installKitPath, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                // number of bytes retrieved
                int bytesSize = 0;

                // the actual bytes retrieved from the server
                byte[] downBuffer = new byte[2048];

                // read the bytes from the stream
                while ((bytesSize = source.Read(downBuffer, 0, downBuffer.Length)) > 0)
                {
                    // if user pressed cancel
                    if (this.DownloadCanceled)
                    {
                        break;
                    }

                    // computer percentage of progress
                    long progressPercentage = (strLocal.Length * 100) / streamLength;

                    // send the new value of progress
                    if (DownloadProgress != null)
                    {
                        DownloadProgress(this, new DownloadProgressEventArgs(progressPercentage));
                    }

                    // write the bytes in the local stream
                    strLocal.Write(downBuffer, 0, bytesSize);
                }
            }
        }

        /// <summary>
        /// Performs the setup kit download from the http link specified in the settings
        /// </summary>
        /// <param name="sourcePath">The source path.</param>
        private void HttpDownload(string sourcePath)
        {
            if (string.IsNullOrEmpty(sourcePath))
            {
                return;
            }

            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(sourcePath);
                webRequest.Credentials = CredentialCache.DefaultCredentials;
                HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();
                long fileSize = webResponse.ContentLength;
                using (Stream strResponse = webResponse.GetResponseStream())
                {
                    this.CopyUpdateFromStream(strResponse, fileSize);
                }
            }
            catch (WebException ex)
            {
                this.DownloadCanceled = true;
                log.ErrorException("Update Failed.", ex);
                throw new UpdateServerOfflineException();
            }
            catch (System.Security.SecurityException ex)
            {
                this.DownloadCanceled = true;
                log.ErrorException("Cannot access the specified path.", ex);
                throw new UpdateException();
            }
            catch (UriFormatException ex)
            {
                this.DownloadCanceled = true;
                log.ErrorException("Wrong URI format.", ex);
                throw new UpdateException();
            }
            finally
            {
                if (this.DownloadCanceled)
                {
                    if (File.Exists(installKitPath))
                    {
                        File.Delete(installKitPath);
                    }
                }
            }
        }

        /// <summary>
        /// Downloads the setup file from the local network specified in the settings
        /// </summary>
        /// <param name="sourcePath">The source path.</param>
        private void LocalNetworkDownload(string sourcePath)
        {
            if (string.IsNullOrEmpty(sourcePath))
            {
                return;
            }

            if (File.Exists(sourcePath))
            {
                try
                {
                    using (Stream source = File.Open(sourcePath, FileMode.Open, FileAccess.Read))
                    {
                        long fileSize = new FileInfo(sourcePath).Length;
                        this.CopyUpdateFromStream(source, fileSize);
                    }
                }
                catch (IOException ex)
                {
                    this.DownloadCanceled = true;
                    log.ErrorException("Update Failed.", ex);
                    throw new UpdateServerOfflineException();
                }
                catch (System.Security.SecurityException ex)
                {
                    this.DownloadCanceled = true;
                    log.ErrorException("Cannot access the specified path.", ex);
                    throw new UpdateException();
                }
                catch (UriFormatException ex)
                {
                    this.DownloadCanceled = true;
                    log.ErrorException("Wrong URI format.", ex);
                    throw new UpdateException();
                }
                finally
                {
                    if (this.DownloadCanceled)
                    {
                        if (File.Exists(installKitPath))
                        {
                            File.Delete(installKitPath);
                        }
                    }
                }
            }
            else
            {
                this.DownloadCanceled = true;
                throw new UpdateServerOfflineException();
            }
        }

        /// <summary>
        /// Reads the settings which are needed for the installer and stores them into a serialized dictionary in the temp folder where the updater is copied.
        /// The updater will read them from there.
        /// </summary>        
        private void StoreSettingsInTempFolder()
        {
            Dictionary<string, object> settings = new Dictionary<string, object>();

            if (!GlobalSettingsManager.Instance.StartInViewerMode)
            {
                settings.Add("CentralConnection_DataSource", GlobalSettingsManager.Instance.CentralConnectionDataSource);
                settings.Add("Connection_DataSource", GlobalSettingsManager.Instance.LocalConnectionDataSource);
                settings.Add("Connection_UserId", GlobalSettingsManager.Instance.LocalConnectionUser);
                settings.Add("Connection_PasswordEncrypted", GlobalSettingsManager.Instance.LocalConnectionPasswordEncrypted);
            }

            string settingsPath = Path.Combine(this.tempFolderPath, Updater.SettingsFile);
            if (!Directory.Exists(this.tempFolderPath))
            {
                Directory.CreateDirectory(this.tempFolderPath);
            }

            Stream stream = File.Open(settingsPath, FileMode.Create);
            BinaryFormatter bformatter = new BinaryFormatter();

            bformatter.Serialize(stream, settings);
            stream.Close();
        }
    }
}
