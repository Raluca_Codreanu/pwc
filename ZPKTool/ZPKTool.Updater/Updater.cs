﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Xml;

namespace ZPKTool.Updater
{
    /// <summary>
    /// The process that handles the update to a new version.
    /// </summary>
    public class Updater
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Path of the running application
        /// </summary>
        private string appPath;

        /// <summary>
        /// Name of the install kit
        /// </summary>
        private string installkitName;

        /// <summary>
        /// Path of the temp folder
        /// </summary>
        private string tempPath;

        /// <summary>
        /// Holds the settings of the application, needed for later restoring and for installer arguments
        /// </summary>
        private Dictionary<string, object> settings = null;

        /// <summary>
        /// Name of the settings file which will be used to pass the settings to the installer
        /// </summary>
        public static readonly string SettingsFile = "Settings.cfg";

        /// <summary>
        /// Entry point of the Updater process
        /// </summary>
        /// <param name="args">The command line arguments</param>
        public static void Main(string[] args)
        {
            if (args.Length >= 2 && args[0] == "/start")
            {
                System.Diagnostics.Process appProcess = new System.Diagnostics.Process();
                appProcess.StartInfo.FileName = args[1];
                appProcess.Start();
            }
            else
            {
                // Close any ZPKTool.Updater processes that might be running except this one
                Process thisProc = System.Diagnostics.Process.GetCurrentProcess();
                Process[] runningProcs = System.Diagnostics.Process.GetProcessesByName(thisProc.ProcessName);
                foreach (Process proc in runningProcs)
                {
                    if (proc.Id != thisProc.Id)
                    {
                        proc.Kill();
                    }
                }

                Updater updater = new Updater();
                updater.Start(args);

                // wait indefinitely while all the operations (install, uninstall are performed)
                thisProc.WaitForExit();
            }
        }

        /// <summary>
        /// Starts updater outside the static main method
        /// </summary>
        /// <param name="args">The command line arguments</param>
        public void Start(string[] args)
        {
            if (args.Length < 3)
            {
                return;
            }

            this.tempPath = args[0];
            this.appPath = args[1];
            this.installkitName = args[2];

            if (args.Length > 3 && args[3] == "/copy")
            {
                // if /copy was sent as the 4th argument it means that the updater must copy itself then restart from the temp location
                this.CopyUpdater();
            }
            else
            {
                // if /copy was not sent it means that the updater can start the process
                this.RunProcess();
            }
        }

        /// <summary>
        /// Runs the Uninstall or the install process if the first one is missing
        /// </summary>
        private void RunProcess()
        {
#if DEBUG
            // Uncomment this line to get a prompt to attach the Visual Studio debugger.
            ////Debugger.Launch();
#endif

            this.ConfigureLogging();
            this.StartInstaller();
        }

        /// <summary>
        /// Copies the Updater executable in TEMP and starts it from there.
        /// </summary>
        private void CopyUpdater()
        {
            string newUpdaterLocation = null;
            try
            {
                if (!Directory.Exists(this.tempPath))
                {
                    Directory.CreateDirectory(this.tempPath);
                }

                // Copy the updater exe to TEMP.
                var updaterAssembly = typeof(Updater).Assembly;
                newUpdaterLocation = Path.Combine(this.tempPath, Path.GetFileName(updaterAssembly.Location));
                File.Copy(updaterAssembly.Location, newUpdaterLocation);

                // Copy all its local references to TEMP                
                foreach (var assemblyName in updaterAssembly.GetReferencedAssemblies())
                {
                    var assembly = Assembly.Load(assemblyName);
                    if (assembly.GlobalAssemblyCache)
                    {
                        continue;
                    }

                    File.Copy(assembly.Location, Path.Combine(this.tempPath, Path.GetFileName(assembly.Location)));
                }
            }
            catch (Exception ex)
            {
                log.ErrorException("Error occurred while copying the updater to TEMP.", ex);
                throw;
            }

            try
            {
                // Start a new updater process from the TEMP location where it was copied above.
                Process newUpdater = new Process();
                newUpdater.StartInfo.FileName = newUpdaterLocation;
                newUpdater.StartInfo.CreateNoWindow = true;
                newUpdater.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                newUpdater.StartInfo.Arguments = "\"" + this.tempPath + "\" \"" + this.appPath + "\" \"" + this.installkitName + "\"";
                newUpdater.Start();
            }
            catch (Exception ex)
            {
                log.ErrorException("Failed to restart the updater after copying it to TEMP.", ex);
                throw;
            }
        }

        /// <summary>
        /// Starts the installer process
        /// </summary>
        private void StartInstaller()
        {
            Process install = new Process();
            install.StartInfo.FileName = Path.Combine(this.tempPath, this.installkitName);
            install.StartInfo.Arguments = this.GetInstallerArguments();
            install.Exited += this.InstallFinished;
            install.EnableRaisingEvents = true;
            install.Start();
        }

        /// <summary>
        /// Event handler for install finished
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void InstallFinished(object sender, EventArgs e)
        {
            // The app was restarted by the installer.
            // Remove the updater files from TEMP
            this.Cleanup();
        }

        /// <summary>
        /// Gets the installer arguments according to the design
        /// </summary>
        /// <returns>string of concatenated arguments</returns>        
        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "Failure here should not result in an error dialog.")]
        private string GetInstallerArguments()
        {
            //// TODO: check if this method has any more use and remove it if not.
            // Reading the settings from the serialized file
            string settingsPath = Path.Combine(this.tempPath, SettingsFile);
            if (!File.Exists(settingsPath))
            {
                log.Error("The updater settings file was not found.");
                return string.Empty;
            }

            try
            {
                using (Stream stream = File.Open(settingsPath, FileMode.Open, FileAccess.Read))
                {
                    BinaryFormatter bformatter = new BinaryFormatter();
                    settings = bformatter.Deserialize(stream) as Dictionary<string, object>;
                }
            }
            catch (Exception ex)
            {
                log.ErrorException("Failed to read the updater settings file.", ex);
                return string.Empty;
            }

            // Creating the arguments as specified in the design document.
            string arguments = string.Empty;
            arguments += "/UPD ";

            if (!string.IsNullOrEmpty(appPath))
            {
                arguments += "/DIR=" + appPath + " ";
            }
            else
            {
                log.Error("Argument /DIR was not specified.");
            }

            object value;
            settings.TryGetValue("CentralConnection_DataSource", out value);
            if (value != null)
            {
                arguments += "/CS=" + Convert.ToString(value) + " ";
            }
            else
            {
                log.Error("Argument /CS was not specified.");
            }

            settings.TryGetValue("Connection_DataSource", out value);
            if (value != null)
            {
                arguments += "/LS=" + Convert.ToString(value) + " ";
            }
            else
            {
                log.Error("Argument /LS was not specified.");
            }

            settings.TryGetValue("Connection_UserId", out value);
            if (value != null)
            {
                arguments += "/LSUSR=" + Convert.ToString(value) + " ";
            }
            else
            {
                log.Error("Argument /LSUSR was not specified.");
            }

            settings.TryGetValue("Connection_PasswordEncrypted", out value);
            if (value != null)
            {
                string encryptionKey = ZPKTool.Common.Constants.PasswordEncryptionKey;
                string decryptedPass = ZPKTool.Common.EncryptionManager.Instance.DecryptBlowfish(Convert.ToString(value), encryptionKey);
                arguments += "/LSPASS=" + decryptedPass;
            }
            else
            {
                log.Error("Argument /LSPASS was not specified.");
            }

            return arguments;
        }

        /// <summary>
        /// Cleans up the temporary folder used by the updater.
        /// </summary>
        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "The clean up process is not important enough to show the user an error about it.")]
        private void Cleanup()
        {
            try
            {
                var thisProc = Process.GetCurrentProcess();

                string cleanupBatFileName = "PCMUpdaterCleanup.bat"; // This file name must not contain spaces.
                var cleanupBatchPath = Path.Combine(Path.GetTempPath(), cleanupBatFileName);
                if (File.Exists(cleanupBatchPath))
                {
                    File.Delete(cleanupBatchPath);
                }

                using (var streamWriter = File.CreateText(cleanupBatchPath))
                {
                    streamWriter.WriteLine(string.Format(CultureInfo.InvariantCulture, "taskkill /IM {0}.exe", thisProc.ProcessName));
                    streamWriter.WriteLine(":check");
                    streamWriter.WriteLine(string.Format(CultureInfo.InvariantCulture, @"tasklist /FI ""IMAGENAME eq {0}.exe"" 2>NUL | find /I /N ""{0}.exe"">NUL", thisProc.ProcessName));
                    streamWriter.WriteLine(@"if ""%ERRORLEVEL%""==""0"" echo GOTO check");
                    streamWriter.WriteLine(string.Format(CultureInfo.InvariantCulture, "rmdir /S /Q \"{0}\"", tempPath));
                    streamWriter.Flush();
                }

                string cleanupCmdArgs = string.Format(
                    CultureInfo.InvariantCulture,
                    "/C \"cd /D {0}\" & {1} & del {1}",
                    Path.GetTempPath(),
                    cleanupBatFileName);

                Process cmd = new Process();
                cmd.StartInfo.FileName = "cmd.exe";
                cmd.StartInfo.Arguments = cleanupCmdArgs;
                cmd.StartInfo.CreateNoWindow = true;
                cmd.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                cmd.Start();

                log.Info("Executed cleanup command with arguments {0}", cleanupCmdArgs);
            }
            catch (Exception ex)
            {
                log.ErrorException("The cleanup failed.", ex);
            }
        }

        /// <summary>
        /// Configures the logging service.
        /// </summary>
        private void ConfigureLogging()
        {
            var config = new NLog.Config.LoggingConfiguration();

            var fileTarget = new NLog.Targets.FileTarget();
            config.AddTarget("file", fileTarget);

            fileTarget.Layout = "[${level:uppercase=true:padding=-5}][${longdate}][${callsite}] - ${message} ${onexception:${newline}${exception:format=tostring}}";
            fileTarget.FileName = "${basedir}/updater.log";

            var rule1 = new NLog.Config.LoggingRule("*", NLog.LogLevel.Debug, fileTarget);
            config.LoggingRules.Add(rule1);

            NLog.LogManager.Configuration = config;
            log.Info("Logging initialized.");
        }
    }
}
