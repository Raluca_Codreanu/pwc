﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Updater
{
    /// <summary>
    /// Contains the data for the <see cref="UpdateManager.DownloadProgress"/> event.
    /// </summary>
    public class DownloadProgressEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DownloadProgressEventArgs" /> class.
        /// </summary>
        /// <param name="progressPercentage">The progress percentage.</param>
        public DownloadProgressEventArgs(double progressPercentage)
        {
            this.ProgressPercentage = progressPercentage;
        }

        /// <summary>
        /// Gets the progress percentage.
        /// </summary>
        public double ProgressPercentage { get; private set; }
    }
}
