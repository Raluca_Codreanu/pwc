﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.Tests
{
    /// <summary>
    /// This class contains methods from converting between different types of objects/values.
    /// </summary>
    public static class Converter
    {
        /// <summary>
        /// Converts an empty string to null. A non-empty string is not changed in any way.
        /// </summary>
        /// <param name="src">The input string.</param>
        /// <returns>The input string if it is not empty, null otherwise.</returns>
        public static string EmptyStringAsNull(string src)
        {
            if (src == string.Empty)
            {
                return null;
            }
            else
            {
                return src;
            }
        }

        /// <summary>
        /// Convert a string to an integer number.
        /// </summary>
        /// <param name="src">The string to convert.</param>
        /// <returns>The converted integer or null if the conversion failed of the input was empty/null.</returns>
        public static int? StringToNullableInt(string src)
        {
            if (string.IsNullOrEmpty(src))
            {
                return null;
            }

            int result;
            if (int.TryParse(src, NumberStyles.AllowThousands, CultureInfo.CurrentCulture, out result))
            {
                return result;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Convert a string to an integer number.
        /// </summary>
        /// <param name="src">The string to convert.</param>
        /// <returns>The converted integer.</returns>
        /// <exception cref="ValidationException">Thrown if the conversion failed.</exception>
        public static int StringToInt(string src)
        {
           return int.Parse(src, System.Globalization.NumberStyles.AllowThousands);
        }

        /// <summary>
        /// Convert a string to a decimal number.
        /// </summary>
        /// <param name="src">The string to convert.</param>
        /// <returns>If the conversion succeeded returns the converted decimal;
        /// if it failed or the input empty/null returns null.</returns>
        public static decimal? StringToNullableDecimal(string src)
        {
            if (string.IsNullOrEmpty(src))
            {
                return null;
            }

            decimal result;
            if (decimal.TryParse(src, out result))
            {
                return result;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Convert a string to a decimal number.
        /// </summary>
        /// <param name="src">The string to convert.</param>
        /// <returns>The converted decimal.</returns>
        /// <exception cref="ValidationException">Thrown if the conversion failed.</exception>
        public static decimal StringToDecimal(string src)
        {
            return decimal.Parse(src);
        }
    }
}
