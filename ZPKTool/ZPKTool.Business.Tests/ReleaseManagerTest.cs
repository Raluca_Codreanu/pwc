﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using ZPKTool.Common;
using ZPKTool.DataAccess;

namespace ZPKTool.Business.Tests
{
    /// <summary>
    /// This is a test class for ReleaseManager and is intended
    /// to contain  Unit Tests for all public methods from ReleaseManager
    /// </summary>
    [TestClass]
    public class ReleaseManagerTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReleaseManagerTest"/> class.
        /// </summary>
        public ReleaseManagerTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }

        #endregion Additional test attributes

        #region Test methods

        /// <summary>
        /// A test for ReleaseProject(Guid projectId, EntityContext projectContext) method using invalid data.
        /// </summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void ReleaseInvalidProjectTest()
        {
            ReleaseManager.Instance.ReleaseProject(Guid.Empty, DbIdentifier.LocalDatabase);
        }

        /// <summary>
        /// A test for ReleaseProject(Guid projectId, EntityContext projectContext) method using invalid data.
        /// </summary>
        [TestMethod()]
        [ExpectedException(typeof(BusinessException))]
        public void ReleaseUnexistentProjectTest()
        {
            Project project = new Project();
            ReleaseManager.Instance.ReleaseProject(project.Guid, DbIdentifier.LocalDatabase);
        }

        /// <summary>
        /// A test for ReleaseProject(Guid projectId, EntityContext projectContext) method using invalid data - a released project.
        /// </summary>
        [TestMethod()]
        [ExpectedException(typeof(BusinessException))]
        public void ReleaseReleasedProject()
        {
            Project project = new Project();
            project.Name = "Name" + DateTime.Now.Ticks;
            project.OverheadSettings = new OverheadSetting();
            project.Customer = new Customer();
            project.Customer.Name = "Customer" + DateTime.Now.Ticks;
            project.IsReleased = true;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            ReleaseManager.Instance.ReleaseProject(project.Guid, DbIdentifier.LocalDatabase);
        }

        /// <summary>
        /// A test for ReleaseProject(Guid projectId, EntityContext projectContext) method using invalid data.
        /// </summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void ReleaseProjectFromInvalidContextTest()
        {
            Project project = new Project();
            project.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            project.OverheadSettings = new OverheadSetting();

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            ReleaseManager.Instance.ReleaseProject(project.Guid, DbIdentifier.NotSet);
        }

        /// <summary>
        /// A test for ReleaseProject(Guid projectId, EntityContext projectContext) method 
        /// </summary>
        [TestMethod()]
        public void ReleaseValidProjectTest()
        {
            User user1 = new User();
            user1.Username = user1.Guid.ToString();
            user1.Password = EncryptionManager.Instance.HashSHA256("Password.", user1.Salt, user1.Guid.ToString());
            user1.Name = user1.Guid.ToString();
            user1.Roles = Role.Admin;

            Project project = new Project();
            project.Name = project.Guid.ToString();
            project.Number = EncryptionManager.Instance.GenerateRandomString(10, true);
            project.StartDate = DateTime.Now;
            project.EndDate = DateTime.Now;
            project.Status = ProjectStatus.ValidatedInternal;
            project.Partner = "Partner" + DateTime.Now.Ticks;
            project.ProjectLeader = user1;
            project.ResponsibleCalculator = user1;
            project.Version = 1;
            project.ProductName = "Product" + DateTime.Now.Ticks;
            project.ProductNumber = EncryptionManager.Instance.GenerateRandomString(10, true);
            project.YearlyProductionQuantity = 100;
            project.LifeTime = 10;
            project.ProductDescription = "Description" + DateTime.Now.Ticks;
            project.DepreciationPeriod = 10;
            project.DepreciationRate = 20;
            project.LogisticCostRatio = 300;
            project.Remarks = "Remarks" + DateTime.Now.Ticks;
            project.CustomerDescription = "Customer Description" + DateTime.Now.Ticks;
            project.SourceOverheadSettingsLastUpdate = DateTime.Now;
            project.OverheadSettings = new OverheadSetting();

            Media video = new Media();
            video.Type = MediaType.Video;
            video.Content = new byte[] { 1, 2, 3, 4, 5, 6 };
            video.OriginalFileName = video.Guid.ToString();
            video.Size = video.Content.Length;
            project.Media.Add(video);

            Media document = new Media();
            document.Type = MediaType.Document;
            document.Content = new byte[] { 1, 2, 3, 3, 3, 9 };
            document.OriginalFileName = document.Guid.ToString();
            document.Size = document.Content.Length;
            project.Media.Add(document);

            Customer customer = new Customer();
            customer.Name = customer.Guid.ToString();
            customer.Description = "Description" + DateTime.Now.Ticks;
            customer.Type = (short)SupplierType.CorporateGroup;
            customer.Number = EncryptionManager.Instance.GenerateRandomString(9, true);
            customer.StreetAddress = "Street Address" + DateTime.Now.Ticks;
            customer.ZipCode = EncryptionManager.Instance.GenerateRandomString(5, true);
            customer.Country = "Country" + DateTime.Now.Ticks;
            customer.State = "State" + DateTime.Now.Ticks;
            customer.City = "City" + DateTime.Now.Ticks;
            customer.FirstContactName = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer.FirstContactPhone = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer.FirstContactMobile = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer.FirstContactWebAddress = "www.test.com";
            customer.FirstContactEmail = "test@gmail.com";
            customer.FirstContactFax = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer.SecondContactName = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer.SecondContactPhone = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer.SecondContactMobile = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer.SecondContactWebAddress = "www.test.com";
            customer.SecondContactEmail = "test@gmail.com";
            customer.SecondContactFax = EncryptionManager.Instance.GenerateRandomString(10, true);
            project.Customer = customer;
            project.SetOwner(user1);

            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.Number = EncryptionManager.Instance.GenerateRandomString(10, true);
            part.Version = 1;
            part.Description = "Description" + DateTime.Now.Ticks;
            part.CalculationStatus = PartCalculationStatus.Approved;
            part.CalculationAccuracy = PartCalculationAccuracy.FineCalculation;
            part.CalculationVariant = "9";
            part.CalculatorUser = user1;
            part.BatchSizePerYear = 10;
            part.YearlyProductionQuantity = 90;
            part.LifeTime = 50;
            part.Media.Add(video.Copy());
            part.Media.Add(document.Copy());
            part.ManufacturingCountry = "Country" + DateTime.Now.Ticks;
            part.ManufacturingSupplier = "State" + DateTime.Now.Ticks;
            part.PurchasePrice = 1500;
            part.DelivertType = PartDeliveryType.CIF;
            part.AssetRate = 200;
            part.TargetPrice = 2000;
            part.SBMActive = true;
            part.IsExternal = true;
            part.AdditionalRemarks = "Remarks" + DateTime.Now.Ticks;
            part.AdditionalDescription = "Additional Description" + DateTime.Now.Ticks;
            part.ManufacturerDescription = "Manufacturer Description" + DateTime.Now.Ticks;
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            project.Parts.Add(part);

            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            manufacturer.Description = "Description" + DateTime.Now.Ticks;
            part.Manufacturer = manufacturer;

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MeasurementUnit weightUnit = dataContext1.EntityContext.MeasurementUnitSet.Where(m => m.Name.Equals("Gram") && !m.IsReleased).FirstOrDefault();

            Commodity commodity = new Commodity();
            commodity.Name = commodity.Guid.ToString();
            commodity.PartNumber = EncryptionManager.Instance.GenerateRandomString(11, true);
            commodity.Price = 300;
            commodity.Amount = 2;
            commodity.RejectRatio = 1;
            commodity.Weight = 20;
            commodity.WeightUnitBase = weightUnit;
            commodity.Remarks = "Remark" + DateTime.Now.Ticks;
            commodity.Manufacturer = manufacturer.Copy();
            commodity.Manufacturer.Name = commodity.Manufacturer.Guid.ToString();

            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 1, 2, 9, 9, 9 };
            image.OriginalFileName = image.Guid.ToString();
            image.Size = image.Content.Length;
            commodity.Media = image;
            part.Commodities.Add(commodity);

            RawMaterial rawMaterial = new RawMaterial();
            rawMaterial.Name = rawMaterial.Guid.ToString();
            rawMaterial.NameUK = "UK" + DateTime.Now.Ticks;
            rawMaterial.NameUS = "US" + DateTime.Now.Ticks;
            rawMaterial.NormName = "Norm" + DateTime.Now.Ticks;
            rawMaterial.ScrapCalculationType = (short)ScrapCalculationType.Yield;
            rawMaterial.Price = 1200;
            rawMaterial.PriceUnitBase = weightUnit;
            rawMaterial.ParentWeight = 10;
            rawMaterial.ParentWeightUnitBase = weightUnit;
            rawMaterial.Sprue = 1;
            rawMaterial.Remarks = "Remark" + DateTime.Now.Ticks;
            rawMaterial.ScrapRefundRatio = 150;
            rawMaterial.RejectRatio = 1;
            rawMaterial.YieldStrength = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.RuptureStrength = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.Density = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.MaxElongation = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.GlassTransitionTemperature = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.Rx = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.Rm = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.Manufacturer = manufacturer.Copy();
            rawMaterial.Manufacturer.Name = rawMaterial.Manufacturer.Guid.ToString();
            rawMaterial.Media = image.Copy();
            rawMaterial.Media.OriginalFileName = rawMaterial.Media.Guid.ToString();
            part.RawMaterials.Add(rawMaterial);

            RawMaterialDeliveryType deliveryType = new RawMaterialDeliveryType();
            deliveryType.Name = deliveryType.Guid.ToString();
            rawMaterial.DeliveryType = deliveryType;

            MaterialsClassification materialClassificationLevel4 = new MaterialsClassification();
            materialClassificationLevel4.Name = materialClassificationLevel4.Guid.ToString();
            rawMaterial.MaterialsClassificationL4 = materialClassificationLevel4;

            MaterialsClassification materialClassificationLevel3 = new MaterialsClassification();
            materialClassificationLevel3.Name = materialClassificationLevel3.Guid.ToString();
            rawMaterial.MaterialsClassificationL3 = materialClassificationLevel3;

            MaterialsClassification materialClassificationLevel2 = new MaterialsClassification();
            materialClassificationLevel2.Name = materialClassificationLevel2.Guid.ToString();
            rawMaterial.MaterialsClassificationL2 = materialClassificationLevel2;

            MaterialsClassification materialClassificationLevel1 = new MaterialsClassification();
            materialClassificationLevel1.Name = materialClassificationLevel1.Guid.ToString();
            rawMaterial.MaterialsClassificationL1 = materialClassificationLevel1;

            MeasurementUnit timeUnit = dataContext1.EntityContext.MeasurementUnitSet.Where(m => m.Name.Equals("Minute") && !m.IsReleased).FirstOrDefault();

            ProcessStep step1 = new PartProcessStep();
            step1.Name = step1.Guid.ToString();
            step1.Accuracy = (short)ProcessCalculationAccuracy.Calculated;
            step1.ProcessTime = 10;
            step1.ProcessTimeUnit = timeUnit;
            step1.PartsPerCycle = 5;
            step1.ScrapAmount = 15;
            step1.Rework = 10;
            step1.ShiftsPerWeek = 12;
            step1.HoursPerShift = 9;
            step1.ProductionDaysPerWeek = 4;
            step1.ProductionWeeksPerYear = 250;
            step1.BatchSize = 100;
            step1.SetupsPerBatch = 25;
            step1.SetupTime = 19;
            step1.SetupTimeUnit = timeUnit;
            step1.MaxDownTime = 23;
            step1.MaxDownTimeUnit = timeUnit;
            step1.ProductionUnskilledLabour = 45;
            step1.ProductionSkilledLabour = 50;
            step1.ProductionForeman = 23;
            step1.ProductionTechnicians = 55;
            step1.ProductionEngineers = 123;
            step1.SetupUnskilledLabour = 235;
            step1.SetupSkilledLabour = 12;
            step1.SetupForeman = 355;
            step1.SetupTechnicians = 124;
            step1.SetupEngineers = 235;
            step1.IsExternal = true;
            step1.Price = 199;
            step1.ExceedShiftCost = true;
            step1.ExtraShiftsNumber = 700;
            step1.ShiftCostExceedRatio = 10;
            step1.Description = "Description" + DateTime.Now.Ticks;
            step1.Media = image.Copy();
            step1.Media.OriginalFileName = step1.Media.Guid.ToString();

            CycleTimeCalculation cycleTimeCalculation = new CycleTimeCalculation();
            cycleTimeCalculation.Description = cycleTimeCalculation.Guid.ToString();
            cycleTimeCalculation.RawPartDescription = "Description" + DateTime.Now.Ticks;
            cycleTimeCalculation.MachiningVolume = 10;
            cycleTimeCalculation.MachiningType = MachiningType.Boring;
            cycleTimeCalculation.ToleranceType = "Tolerance" + DateTime.Now.Ticks;
            cycleTimeCalculation.SurfaceType = "Surface" + DateTime.Now.Ticks;
            cycleTimeCalculation.TransportTimeFromBuffer = 70;
            cycleTimeCalculation.ToolChangeTime = 30;
            cycleTimeCalculation.TransportClampingTime = 15;
            cycleTimeCalculation.MachiningTime = 23;
            cycleTimeCalculation.TakeOffTime = 120;
            cycleTimeCalculation.MachiningCalculationData = new byte[] { 1, 2, 3, 4 };
            step1.CycleTimeCalculations.Add(cycleTimeCalculation);
            step1.CycleTimeUnit = timeUnit;
            step1.CycleTime = 1200;
            part.Process = new Process();
            part.Process.Steps.Add(step1);

            ProcessStepsClassification stepSubtype = new ProcessStepsClassification();
            stepSubtype.Name = stepSubtype.Guid.ToString();
            step1.SubType = stepSubtype;

            ProcessStepsClassification stepType = new ProcessStepsClassification();
            stepType.Name = stepType.Guid.ToString();
            step1.Type = stepType;

            Machine machine = new Machine();
            machine.Name = machine.Guid.ToString();
            machine.ManufacturingYear = 2000;
            machine.DepreciationPeriod = 20;
            machine.DepreciationRate = 10;
            machine.RegistrationNumber = EncryptionManager.Instance.GenerateRandomString(10, true);
            machine.DescriptionOfFunction = EncryptionManager.Instance.GenerateRandomString(15, true);
            machine.FloorSize = 10;
            machine.WorkspaceArea = 25;
            machine.PowerConsumption = 100;
            machine.AirConsumption = 230;
            machine.WaterConsumption = 300;
            machine.FullLoadRate = 50;
            machine.MaxCapacity = 1500;
            machine.OEE = 10;
            machine.Availability = 30;
            machine.MachineInvestment = 20;
            machine.SetupInvestment = 30;
            machine.AdditionalEquipmentInvestment = 230;
            machine.FundamentalSetupInvestment = 45;
            machine.InvestmentRemarks = "Remark" + DateTime.Now.Ticks;
            machine.ExternalWorkCost = 1230;
            machine.MaterialCost = 1000;
            machine.ConsumablesCost = 2300;
            machine.RepairsCost = 2345;
            machine.KValue = 1200;
            machine.CalculateWithKValue = true;
            machine.MountingCubeLength = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MountingCubeWidth = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MountingCubeHeight = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MaxDiameterRodPerChuckMaxThickness = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MaxPartsWeightPerCapacity = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.LockingForce = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.PressCapacity = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MaximumSpeed = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.RapidFeedPerCuttingSpeed = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.StrokeRate = EncryptionManager.Instance.GenerateRandomString(2, true);
            machine.Other = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.Manufacturer = manufacturer.Copy();
            machine.Manufacturer.Name = machine.Manufacturer.Guid.ToString();
            machine.Media = image.Copy();
            machine.Media.OriginalFileName = machine.Media.Guid.ToString();
            step1.Machines.Add(machine);

            // Add a classification level 4 to machine
            MachinesClassification machineClassificationLevel4 = new MachinesClassification();
            machineClassificationLevel4.Name = machineClassificationLevel4.Guid.ToString();
            machine.ClassificationLevel4 = machineClassificationLevel4;

            // Add subtype classification to machine
            MachinesClassification machineSubClassification = new MachinesClassification();
            machineSubClassification.Name = machineSubClassification.Guid.ToString();
            machine.SubClassification = machineSubClassification;

            // Add type classification to machine
            MachinesClassification machineTypeClassification = new MachinesClassification();
            machineTypeClassification.Name = machineTypeClassification.Guid.ToString();
            machine.TypeClassification = machineTypeClassification;

            // Add main classification to machine
            MachinesClassification machineMainClassification = new MachinesClassification();
            machineMainClassification.Name = machineMainClassification.Guid.ToString();
            machine.MainClassification = machineMainClassification;

            Consumable consumable = new Consumable();
            consumable.Name = consumable.Guid.ToString();
            consumable.Description = EncryptionManager.Instance.GenerateRandomString(17, true);
            consumable.Price = 1200;
            consumable.PriceUnitBase = weightUnit;
            consumable.Amount = 12;
            consumable.Manufacturer = manufacturer.Copy();
            consumable.Manufacturer.Name = consumable.Manufacturer.Guid.ToString();
            step1.Consumables.Add(consumable);

            Die die = new Die();
            die.Name = die.Guid.ToString();
            die.Type = (short)DieType.CastingDie;
            die.Investment = 100;
            die.ReusableInvest = 1200;
            die.Wear = 300;
            die.Maintenance = 1200;
            die.LifeTime = 300;
            die.AllocationRatio = 2300;
            die.DiesetsNumberPaidByCustomer = 1300;
            die.CostAllocationBasedOnPartsPerLifeTime = true;
            die.CostAllocationNumberOfParts = 234;
            die.Remark = EncryptionManager.Instance.GenerateRandomString(15, true);
            die.Manufacturer = manufacturer.Copy();
            die.Manufacturer.Name = die.Manufacturer.Guid.ToString();
            die.Media = image.Copy();
            die.Media.OriginalFileName = die.Media.Guid.ToString();
            step1.Dies.Add(die);

            Assembly assembly = new Assembly();
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();
            assembly.Name = assembly.Guid.ToString();
            assembly.Number = EncryptionManager.Instance.GenerateRandomString(10, true);
            assembly.AssemblingCountry = "Country" + DateTime.Now.Ticks;
            assembly.AssemblingSupplier = "State" + DateTime.Now.Ticks;
            assembly.Version = 2;
            assembly.CalculationAccuracy = PartCalculationAccuracy.FineCalculation;
            assembly.CalculationApproach = PartCalculationApproach.Brownfield;
            assembly.CalculationVariant = EncryptionManager.Instance.GenerateRandomString(7, true);
            assembly.BatchSizePerYear = 120;
            assembly.YearlyProductionQuantity = 1200;
            assembly.LifeTime = 50;
            assembly.PurchasePrice = 2300;
            assembly.DeliveryType = PartDeliveryType.CPT;
            assembly.AssetRate = 123;
            assembly.TargetPrice = 3400;
            assembly.SBMActive = true;
            assembly.IsExternal = true;
            assembly.Manufacturer = manufacturer.Copy();
            assembly.Manufacturer.Name = assembly.Manufacturer.Guid.ToString();
            assembly.Media.Add(video.Copy());
            assembly.Media.Add(document.Copy());
            project.Assemblies.Add(assembly);

            ProcessStep step2 = new AssemblyProcessStep();
            step1.CopyValuesTo(step2);
            step2.Name = step2.Guid.ToString();
            step2.CycleTimeUnit = timeUnit;
            step2.SetupTimeUnit = timeUnit;
            step2.MaxDownTimeUnit = timeUnit;
            step2.Media = image.Copy();
            step2.Media.OriginalFileName = step2.Media.Guid.ToString();
            step2.Type = new ProcessStepsClassification();
            step2.Type.Name = step2.Type.Guid.ToString();
            step2.SubType = new ProcessStepsClassification();
            step2.SubType.Name = step2.SubType.Guid.ToString();
            step2.Type.Children.Add(step2.SubType);
            assembly.Process = new Process();
            assembly.Process.Steps.Add(step2);

            CycleTimeCalculation cycleTimeCalculation2 = new CycleTimeCalculation();
            cycleTimeCalculation.CopyValuesTo(cycleTimeCalculation2);
            step2.CycleTimeCalculations.Add(cycleTimeCalculation2);

            Machine machine2 = new Machine();
            machine.CopyValuesTo(machine2);
            machine2.Name = machine2.Guid.ToString();
            machine2.Manufacturer = manufacturer.Copy();
            machine2.Manufacturer.Name = machine2.Manufacturer.Guid.ToString();
            machine2.Media = image.Copy();
            machine2.Media.OriginalFileName = machine2.Media.Guid.ToString();
            step2.Machines.Add(machine2);

            Consumable consumable2 = new Consumable();
            consumable.CopyValuesTo(consumable2);
            consumable2.Name = consumable2.Guid.ToString();
            consumable2.PriceUnitBase = weightUnit;
            consumable2.Manufacturer = manufacturer.Copy();
            consumable2.Manufacturer.Name = consumable2.Manufacturer.Guid.ToString();
            step2.Consumables.Add(consumable2);

            Commodity commodity2 = new Commodity();
            commodity.CopyValuesTo(commodity2);
            commodity2.Name = commodity2.Guid.ToString();
            commodity2.WeightUnitBase = weightUnit;
            commodity2.Media = image.Copy();
            commodity2.Media.OriginalFileName = commodity2.Media.Guid.ToString();
            commodity2.Manufacturer = manufacturer.Copy();
            commodity2.Manufacturer.Name = commodity2.Manufacturer.Guid.ToString();
            step2.Commodities.Add(commodity2);

            Die die2 = new Die();
            die.CopyValuesTo(die2);
            die2.Name = die2.Guid.ToString();
            die2.Manufacturer = manufacturer.Copy();
            die2.Manufacturer.Name = die2.Manufacturer.Guid.ToString();
            step2.Dies.Add(die2);

            Assembly assembly2 = new Assembly();
            assembly2.Name = assembly2.Guid.ToString();
            assembly2.Manufacturer = manufacturer.Copy();
            assembly2.Manufacturer.Name = assembly2.Manufacturer.Guid.ToString();
            assembly2.CountrySettings = new CountrySetting();
            assembly2.OverheadSettings = new OverheadSetting();
            assembly.Subassemblies.Add(assembly2);

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.CountrySettings = new CountrySetting();
            part2.OverheadSettings = new OverheadSetting();
            part2.Manufacturer = manufacturer.Copy();
            part2.Manufacturer.Name = part2.Manufacturer.Guid.ToString();
            assembly.Parts.Add(part2);

            ProcessStepPartAmount partAmount = new ProcessStepPartAmount();
            partAmount.Part = part2;
            partAmount.ProcessStep = step2;
            partAmount.Amount = 9;
            step2.PartAmounts.Add(partAmount);

            ProcessStepAssemblyAmount assemblyAmount = new ProcessStepAssemblyAmount();
            assemblyAmount.Assembly = assembly2;
            assemblyAmount.ProcessStep = step2;
            assemblyAmount.Amount = 3;
            step2.AssemblyAmounts.Add(assemblyAmount);

            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            ReleaseManager.Instance.ReleaseProject(project.Guid, DbIdentifier.LocalDatabase);

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            Project releasedProject = dataContext2.EntityContext.ProjectSet.Where(p => string.Equals(p.Name, project.Name) && p.IsReleased).FirstOrDefault();
            Project releasedProjectFull = dataContext2.ProjectRepository.GetProjectFull(releasedProject.Guid);
            bool result = AreEqual(project, DbIdentifier.LocalDatabase, releasedProjectFull, DbIdentifier.CentralDatabase);

            // Verifies that the project was correctly released
            Assert.IsTrue(result, "Failed to release a project");
        }

        /// <summary>
        /// A test for UnReleaseProject(Guid projectId) method using invalid data.
        /// </summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void UnreleaseNullProjectTest()
        {
            ReleaseManager.Instance.UnReleaseProject(Guid.Empty);
        }

        /// <summary>
        /// A test for UnReleaseProject(Guid projectId) method using invalid data - a project which is not released.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(BusinessException))]
        public void UnreleaseNotReleasedProjectTest()
        {
            Project project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();
            project.Customer = new Customer();
            project.Customer.Name = project.Customer.Guid.ToString();

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            ReleaseManager.Instance.UnReleaseProject(project.Guid);
        }

        /// <summary>
        /// A test for UnReleaseProject(Guid projectId) method given a project that was unreleased
        /// - does not exist on central db.
        /// </summary>
        [TestMethod]
        public void UnreleaseUnreleasedProjectTest()
        {
            // No exception should be thrown 
            ReleaseManager.Instance.UnReleaseProject(Guid.NewGuid());
        }

        /// <summary>
        /// A test for UnReleaseProject(Guid projectId) method using valid data.
        /// </summary>
        [TestMethod]
        public void UnreleaseValidProjectTest()
        {
            User user1 = new User();
            user1.Username = user1.Guid.ToString();
            user1.Password = EncryptionManager.Instance.HashSHA256("Password.", user1.Salt, user1.Guid.ToString());
            user1.Name = user1.Guid.ToString();
            user1.Roles = Role.Admin;

            Project project = new Project();
            project.Name = project.Guid.ToString();
            project.Number = EncryptionManager.Instance.GenerateRandomString(10, true);
            project.StartDate = DateTime.Now;
            project.EndDate = DateTime.Now;
            project.Status = ProjectStatus.ValidatedInternal;
            project.Partner = "Partner" + DateTime.Now.Ticks;
            project.ProjectLeader = user1;
            project.ResponsibleCalculator = user1;
            project.Version = 1;
            project.ProductName = "Product" + DateTime.Now.Ticks;
            project.ProductNumber = EncryptionManager.Instance.GenerateRandomString(10, true);
            project.YearlyProductionQuantity = 100;
            project.LifeTime = 10;
            project.ProductDescription = "Description" + DateTime.Now.Ticks;
            project.DepreciationPeriod = 10;
            project.DepreciationRate = 20;
            project.LogisticCostRatio = 300;
            project.Remarks = "Remarks" + DateTime.Now.Ticks;
            project.CustomerDescription = "Customer Description" + DateTime.Now.Ticks;
            project.SourceOverheadSettingsLastUpdate = DateTime.Now;
            project.OverheadSettings = new OverheadSetting();

            Media video = new Media();
            video.Type = MediaType.Video;
            video.Content = new byte[] { 1, 2, 3, 4, 5, 6 };
            video.OriginalFileName = video.Guid.ToString();
            video.Size = video.Content.Length;
            project.Media.Add(video);

            Media document = new Media();
            document.Type = MediaType.Document;
            document.Content = new byte[] { 1, 2, 3, 3, 3, 9 };
            document.OriginalFileName = document.Guid.ToString();
            document.Size = document.Content.Length;
            project.Media.Add(document);

            Customer customer = new Customer();
            customer.Name = customer.Guid.ToString();
            customer.Description = "Description" + DateTime.Now.Ticks;
            customer.Type = (short)SupplierType.CorporateGroup;
            customer.Number = EncryptionManager.Instance.GenerateRandomString(9, true);
            customer.StreetAddress = "Street Address" + DateTime.Now.Ticks;
            customer.ZipCode = EncryptionManager.Instance.GenerateRandomString(5, true);
            customer.Country = "Country" + DateTime.Now.Ticks;
            customer.State = "State" + DateTime.Now.Ticks;
            customer.City = "City" + DateTime.Now.Ticks;
            customer.FirstContactName = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer.FirstContactPhone = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer.FirstContactMobile = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer.FirstContactWebAddress = "www.test.com";
            customer.FirstContactEmail = "test@gmail.com";
            customer.FirstContactFax = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer.SecondContactName = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer.SecondContactPhone = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer.SecondContactMobile = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer.SecondContactWebAddress = "www.test.com";
            customer.SecondContactEmail = "test@gmail.com";
            customer.SecondContactFax = EncryptionManager.Instance.GenerateRandomString(10, true);
            project.Customer = customer;
            project.SetOwner(user1);

            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.Number = EncryptionManager.Instance.GenerateRandomString(10, true);
            part.Version = 1;
            part.Description = "Description" + DateTime.Now.Ticks;
            part.CalculationStatus = PartCalculationStatus.Approved;
            part.CalculationAccuracy = PartCalculationAccuracy.FineCalculation;
            part.CalculationVariant = "9";
            part.CalculatorUser = user1;
            part.BatchSizePerYear = 10;
            part.YearlyProductionQuantity = 90;
            part.LifeTime = 50;
            part.Media.Add(video.Copy());
            part.Media.Add(document.Copy());
            part.ManufacturingCountry = "Country" + DateTime.Now.Ticks;
            part.ManufacturingSupplier = "State" + DateTime.Now.Ticks;
            part.PurchasePrice = 1500;
            part.DelivertType = PartDeliveryType.CIF;
            part.AssetRate = 200;
            part.TargetPrice = 2000;
            part.SBMActive = true;
            part.IsExternal = true;
            part.AdditionalRemarks = "Remarks" + DateTime.Now.Ticks;
            part.AdditionalDescription = "Additional Description" + DateTime.Now.Ticks;
            part.ManufacturerDescription = "Manufacturer Description" + DateTime.Now.Ticks;
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            project.Parts.Add(part);

            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            manufacturer.Description = "Description" + DateTime.Now.Ticks;
            part.Manufacturer = manufacturer;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MeasurementUnit weightUnit = dataContext.EntityContext.MeasurementUnitSet.Where(m => m.Name.Equals("Gram") && !m.IsReleased).FirstOrDefault();

            Commodity commodity = new Commodity();
            commodity.Name = commodity.Guid.ToString();
            commodity.PartNumber = EncryptionManager.Instance.GenerateRandomString(11, true);
            commodity.Price = 300;
            commodity.Amount = 2;
            commodity.RejectRatio = 1;
            commodity.Weight = 20;
            commodity.WeightUnitBase = weightUnit;
            commodity.Remarks = "Remark" + DateTime.Now.Ticks;
            commodity.Manufacturer = manufacturer.Copy();
            commodity.Manufacturer.Name = commodity.Manufacturer.Guid.ToString();

            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 1, 2, 9, 9, 9 };
            image.OriginalFileName = image.Guid.ToString();
            image.Size = image.Content.Length;
            commodity.Media = image;
            part.Commodities.Add(commodity);

            RawMaterial rawMaterial = new RawMaterial();
            rawMaterial.Name = rawMaterial.Guid.ToString();
            rawMaterial.NameUK = "UK" + DateTime.Now.Ticks;
            rawMaterial.NameUS = "US" + DateTime.Now.Ticks;
            rawMaterial.NormName = "Norm" + DateTime.Now.Ticks;
            rawMaterial.ScrapCalculationType = (short)ScrapCalculationType.Yield;
            rawMaterial.Price = 1200;
            rawMaterial.PriceUnitBase = weightUnit;
            rawMaterial.ParentWeight = 10;
            rawMaterial.ParentWeightUnitBase = weightUnit;
            rawMaterial.Sprue = 1;
            rawMaterial.Remarks = "Remark" + DateTime.Now.Ticks;
            rawMaterial.ScrapRefundRatio = 150;
            rawMaterial.RejectRatio = 1;
            rawMaterial.YieldStrength = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.RuptureStrength = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.Density = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.MaxElongation = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.GlassTransitionTemperature = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.Rx = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.Rm = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.Manufacturer = manufacturer.Copy();
            rawMaterial.Manufacturer.Name = rawMaterial.Manufacturer.Guid.ToString();
            rawMaterial.Media = image.Copy();
            rawMaterial.Media.OriginalFileName = rawMaterial.Media.Guid.ToString();
            part.RawMaterials.Add(rawMaterial);

            RawMaterialDeliveryType deliveryType = new RawMaterialDeliveryType();
            deliveryType.Name = deliveryType.Guid.ToString();
            rawMaterial.DeliveryType = deliveryType;

            MaterialsClassification materialClassificationLevel4 = new MaterialsClassification();
            materialClassificationLevel4.Name = materialClassificationLevel4.Guid.ToString();
            rawMaterial.MaterialsClassificationL4 = materialClassificationLevel4;

            MaterialsClassification materialClassificationLevel3 = new MaterialsClassification();
            materialClassificationLevel3.Name = materialClassificationLevel3.Guid.ToString();
            rawMaterial.MaterialsClassificationL3 = materialClassificationLevel3;

            MaterialsClassification materialClassificationLevel2 = new MaterialsClassification();
            materialClassificationLevel2.Name = materialClassificationLevel2.Guid.ToString();
            rawMaterial.MaterialsClassificationL2 = materialClassificationLevel2;

            MaterialsClassification materialClassificationLevel1 = new MaterialsClassification();
            materialClassificationLevel1.Name = materialClassificationLevel1.Guid.ToString();
            rawMaterial.MaterialsClassificationL1 = materialClassificationLevel1;

            MeasurementUnit timeUnit = dataContext.EntityContext.MeasurementUnitSet.Where(m => m.Name.Equals("Minute") && !m.IsReleased).FirstOrDefault();

            ProcessStep step1 = new PartProcessStep();
            step1.Name = step1.Guid.ToString();
            step1.Accuracy = (short)ProcessCalculationAccuracy.Calculated;
            step1.ProcessTime = 10;
            step1.ProcessTimeUnit = timeUnit;
            step1.PartsPerCycle = 5;
            step1.ScrapAmount = 15;
            step1.Rework = 10;
            step1.ShiftsPerWeek = 12;
            step1.HoursPerShift = 9;
            step1.ProductionDaysPerWeek = 4;
            step1.ProductionWeeksPerYear = 250;
            step1.BatchSize = 100;
            step1.SetupsPerBatch = 25;
            step1.SetupTime = 19;
            step1.SetupTimeUnit = timeUnit;
            step1.MaxDownTime = 23;
            step1.MaxDownTimeUnit = timeUnit;
            step1.ProductionUnskilledLabour = 45;
            step1.ProductionSkilledLabour = 50;
            step1.ProductionForeman = 23;
            step1.ProductionTechnicians = 55;
            step1.ProductionEngineers = 123;
            step1.SetupUnskilledLabour = 235;
            step1.SetupSkilledLabour = 12;
            step1.SetupForeman = 355;
            step1.SetupTechnicians = 124;
            step1.SetupEngineers = 235;
            step1.IsExternal = true;
            step1.Price = 199;
            step1.ExceedShiftCost = true;
            step1.ExtraShiftsNumber = 700;
            step1.ShiftCostExceedRatio = 10;
            step1.Description = "Description" + DateTime.Now.Ticks;
            step1.Media = image.Copy();
            step1.Media.OriginalFileName = step1.Media.Guid.ToString();

            CycleTimeCalculation cycleTimeCalculation = new CycleTimeCalculation();
            cycleTimeCalculation.Description = cycleTimeCalculation.Guid.ToString();
            cycleTimeCalculation.RawPartDescription = "Description" + DateTime.Now.Ticks;
            cycleTimeCalculation.MachiningVolume = 10;
            cycleTimeCalculation.MachiningType = MachiningType.Boring;
            cycleTimeCalculation.ToleranceType = "Tolerance" + DateTime.Now.Ticks;
            cycleTimeCalculation.SurfaceType = "Surface" + DateTime.Now.Ticks;
            cycleTimeCalculation.TransportTimeFromBuffer = 70;
            cycleTimeCalculation.ToolChangeTime = 30;
            cycleTimeCalculation.TransportClampingTime = 15;
            cycleTimeCalculation.MachiningTime = 23;
            cycleTimeCalculation.TakeOffTime = 120;
            cycleTimeCalculation.MachiningCalculationData = new byte[] { 1, 2, 3, 4 };
            step1.CycleTimeCalculations.Add(cycleTimeCalculation);
            step1.CycleTimeUnit = timeUnit;
            step1.CycleTime = 1200;
            part.Process = new Process();
            part.Process.Steps.Add(step1);

            ProcessStepsClassification stepSubtype = new ProcessStepsClassification();
            stepSubtype.Name = stepSubtype.Guid.ToString();
            step1.SubType = stepSubtype;

            ProcessStepsClassification stepType = new ProcessStepsClassification();
            stepType.Name = stepType.Guid.ToString();
            step1.Type = stepType;

            Machine machine = new Machine();
            machine.Name = machine.Guid.ToString();
            machine.ManufacturingYear = 2000;
            machine.DepreciationPeriod = 20;
            machine.DepreciationRate = 10;
            machine.RegistrationNumber = EncryptionManager.Instance.GenerateRandomString(10, true);
            machine.DescriptionOfFunction = EncryptionManager.Instance.GenerateRandomString(15, true);
            machine.FloorSize = 10;
            machine.WorkspaceArea = 25;
            machine.PowerConsumption = 100;
            machine.AirConsumption = 230;
            machine.WaterConsumption = 300;
            machine.FullLoadRate = 50;
            machine.MaxCapacity = 1500;
            machine.OEE = 10;
            machine.Availability = 30;
            machine.MachineInvestment = 20;
            machine.SetupInvestment = 30;
            machine.AdditionalEquipmentInvestment = 230;
            machine.FundamentalSetupInvestment = 45;
            machine.InvestmentRemarks = "Remark" + DateTime.Now.Ticks;
            machine.ExternalWorkCost = 1230;
            machine.MaterialCost = 1000;
            machine.ConsumablesCost = 2300;
            machine.RepairsCost = 2345;
            machine.KValue = 1200;
            machine.CalculateWithKValue = true;
            machine.MountingCubeLength = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MountingCubeWidth = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MountingCubeHeight = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MaxDiameterRodPerChuckMaxThickness = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MaxPartsWeightPerCapacity = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.LockingForce = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.PressCapacity = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MaximumSpeed = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.RapidFeedPerCuttingSpeed = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.StrokeRate = EncryptionManager.Instance.GenerateRandomString(2, true);
            machine.Other = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.Manufacturer = manufacturer.Copy();
            machine.Manufacturer.Name = machine.Manufacturer.Guid.ToString();
            machine.Media = image.Copy();
            machine.Media.OriginalFileName = machine.Media.Guid.ToString();
            step1.Machines.Add(machine);

            // Add a classification level 4 to machine
            MachinesClassification machineClassificationLevel4 = new MachinesClassification();
            machineClassificationLevel4.Name = machineClassificationLevel4.Guid.ToString();
            machine.ClassificationLevel4 = machineClassificationLevel4;

            // Add subtype classification to machine
            MachinesClassification machineSubClassification = new MachinesClassification();
            machineSubClassification.Name = machineSubClassification.Guid.ToString();
            machine.SubClassification = machineSubClassification;

            // Add type classification to machine
            MachinesClassification machineTypeClassification = new MachinesClassification();
            machineTypeClassification.Name = machineTypeClassification.Guid.ToString();
            machine.TypeClassification = machineTypeClassification;

            // Add main classification to machine
            MachinesClassification machineMainClassification = new MachinesClassification();
            machineMainClassification.Name = machineMainClassification.Guid.ToString();
            machine.MainClassification = machineMainClassification;

            Consumable consumable = new Consumable();
            consumable.Name = consumable.Guid.ToString();
            consumable.Description = EncryptionManager.Instance.GenerateRandomString(17, true);
            consumable.Price = 1200;
            consumable.PriceUnitBase = weightUnit;
            consumable.Amount = 12;
            consumable.Manufacturer = manufacturer.Copy();
            consumable.Manufacturer.Name = consumable.Manufacturer.Guid.ToString();
            step1.Consumables.Add(consumable);

            Die die = new Die();
            die.Name = die.Guid.ToString();
            die.Type = (short)DieType.CastingDie;
            die.Investment = 100;
            die.ReusableInvest = 1200;
            die.Wear = 300;
            die.Maintenance = 1200;
            die.LifeTime = 300;
            die.AllocationRatio = 2300;
            die.DiesetsNumberPaidByCustomer = 1300;
            die.CostAllocationBasedOnPartsPerLifeTime = true;
            die.CostAllocationNumberOfParts = 234;
            die.Remark = EncryptionManager.Instance.GenerateRandomString(15, true);
            die.Manufacturer = manufacturer.Copy();
            die.Manufacturer.Name = die.Manufacturer.Guid.ToString();
            die.Media = image.Copy();
            die.Media.OriginalFileName = die.Media.Guid.ToString();
            step1.Dies.Add(die);

            Assembly assembly = new Assembly();
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();
            assembly.Name = assembly.Guid.ToString();
            assembly.Number = EncryptionManager.Instance.GenerateRandomString(10, true);
            assembly.AssemblingCountry = "Country" + DateTime.Now.Ticks;
            assembly.AssemblingSupplier = "State" + DateTime.Now.Ticks;
            assembly.Version = 2;
            assembly.CalculationAccuracy = PartCalculationAccuracy.FineCalculation;
            assembly.CalculationApproach = PartCalculationApproach.Brownfield;
            assembly.CalculationVariant = EncryptionManager.Instance.GenerateRandomString(7, true);
            assembly.BatchSizePerYear = 120;
            assembly.YearlyProductionQuantity = 1200;
            assembly.LifeTime = 50;
            assembly.PurchasePrice = 2300;
            assembly.DeliveryType = PartDeliveryType.CPT;
            assembly.AssetRate = 123;
            assembly.TargetPrice = 3400;
            assembly.SBMActive = true;
            assembly.IsExternal = true;
            assembly.Manufacturer = manufacturer.Copy();
            assembly.Manufacturer.Name = assembly.Manufacturer.Guid.ToString();
            assembly.Media.Add(video.Copy());
            assembly.Media.Add(document.Copy());
            project.Assemblies.Add(assembly);

            ProcessStep step2 = new AssemblyProcessStep();
            step1.CopyValuesTo(step2);
            step2.Name = step2.Guid.ToString();
            step2.CycleTimeUnit = timeUnit;
            step2.SetupTimeUnit = timeUnit;
            step2.MaxDownTimeUnit = timeUnit;
            step2.Media = image.Copy();
            step2.Media.OriginalFileName = step2.Media.Guid.ToString();
            step2.Type = new ProcessStepsClassification();
            step2.Type.Name = step2.Type.Guid.ToString();
            step2.SubType = new ProcessStepsClassification();
            step2.SubType.Name = step2.SubType.Guid.ToString();
            step2.Type.Children.Add(step2.SubType);
            assembly.Process = new Process();
            assembly.Process.Steps.Add(step2);

            CycleTimeCalculation cycleTimeCalculation2 = new CycleTimeCalculation();
            cycleTimeCalculation.CopyValuesTo(cycleTimeCalculation2);
            step2.CycleTimeCalculations.Add(cycleTimeCalculation2);

            Machine machine2 = new Machine();
            machine.CopyValuesTo(machine2);
            machine2.Name = machine2.Guid.ToString();
            machine2.Manufacturer = manufacturer.Copy();
            machine2.Manufacturer.Name = machine2.Manufacturer.Guid.ToString();
            machine2.Media = image.Copy();
            machine2.Media.OriginalFileName = machine2.Media.Guid.ToString();
            step2.Machines.Add(machine2);

            Consumable consumable2 = new Consumable();
            consumable.CopyValuesTo(consumable2);
            consumable2.Name = consumable2.Guid.ToString();
            consumable2.PriceUnitBase = weightUnit;
            consumable2.Manufacturer = manufacturer.Copy();
            consumable2.Manufacturer.Name = consumable2.Manufacturer.Guid.ToString();
            step2.Consumables.Add(consumable2);

            Commodity commodity2 = new Commodity();
            commodity.CopyValuesTo(commodity2);
            commodity2.Name = commodity2.Guid.ToString();
            commodity2.WeightUnitBase = weightUnit;
            commodity2.Media = image.Copy();
            commodity2.Media.OriginalFileName = commodity2.Media.Guid.ToString();
            commodity2.Manufacturer = manufacturer.Copy();
            commodity2.Manufacturer.Name = commodity2.Manufacturer.Guid.ToString();
            step2.Commodities.Add(commodity2);

            Die die2 = new Die();
            die.CopyValuesTo(die2);
            die2.Name = die2.Guid.ToString();
            die2.Manufacturer = manufacturer.Copy();
            die2.Manufacturer.Name = die2.Manufacturer.Guid.ToString();
            step2.Dies.Add(die2);

            Assembly assembly2 = new Assembly();
            assembly2.Name = assembly2.Guid.ToString();
            assembly2.Manufacturer = manufacturer.Copy();
            assembly2.Manufacturer.Name = assembly2.Manufacturer.Guid.ToString();
            assembly2.CountrySettings = new CountrySetting();
            assembly2.OverheadSettings = new OverheadSetting();
            assembly.Subassemblies.Add(assembly2);

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.CountrySettings = new CountrySetting();
            part2.OverheadSettings = new OverheadSetting();
            part2.Manufacturer = manufacturer.Copy();
            part2.Manufacturer.Name = part2.Manufacturer.Guid.ToString();
            assembly.Parts.Add(part2);

            ProcessStepPartAmount partAmount = new ProcessStepPartAmount();
            partAmount.Part = part2;
            partAmount.ProcessStep = step2;
            partAmount.Amount = 9;
            step2.PartAmounts.Add(partAmount);

            ProcessStepAssemblyAmount assemblyAmount = new ProcessStepAssemblyAmount();
            assemblyAmount.Assembly = assembly2;
            assemblyAmount.ProcessStep = step2;
            assemblyAmount.Amount = 3;
            step2.AssemblyAmounts.Add(assemblyAmount);

            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            ReleaseManager.Instance.ReleaseProject(project.Guid, DbIdentifier.LocalDatabase);

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            Project releasedProject = dataContext2.EntityContext.ProjectSet.FirstOrDefault(p => string.Equals(p.Name, project.Name) && p.IsReleased);
            Project releasedProjectFull = dataContext2.ProjectRepository.GetProjectFull(releasedProject.Guid);

            ReleaseManager.Instance.UnReleaseProject(releasedProject.Guid);
            bool projectFullyDeleted = ProjectFullyDeleted(releasedProjectFull, DbIdentifier.CentralDatabase);

            Assert.IsTrue(projectFullyDeleted, "The project was not fully deleted");
        }

        #endregion Test methods

        #region Helpers

        /// <summary>
        /// Compares two projects.
        /// </summary>
        /// <param name="project">The compared project</param>
        /// <param name="projectContext">The 'Entity Context' of compared project</param>
        /// <param name="comparedProject">The project to compare with</param>
        /// <param name="comparedProjectContext">The 'Entity Context' of project to compare with</param>
        /// <returns>True if projects are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: IsReleased, Owner, Guid, LastChangeTimestamp.
        /// </remarks>
        private bool AreEqual(
            Project project,
            DbIdentifier projectContext,
            Project comparedProject,
            DbIdentifier comparedProjectContext)
        {
            if (project == null && comparedProject == null)
            {
                return true;
            }

            if ((project == null && comparedProject != null) || (project != null && comparedProject == null))
            {
                return false;
            }

            bool result = string.Equals(project.Name, comparedProject.Name) &&
                          string.Equals(project.Number, comparedProject.Number) &&
                          string.Equals(project.Partner, comparedProject.Partner) &&
                          string.Equals(project.ProductName, comparedProject.ProductName) &&
                          string.Equals(project.ProductNumber, comparedProject.ProductNumber) &&
                          string.Equals(project.ProductDescription, comparedProject.ProductDescription) &&
                          string.Equals(project.CustomerDescription, comparedProject.CustomerDescription) &&
                          string.Equals(project.Remarks, comparedProject.Remarks) &&
                          string.Equals(
                          project.StartDate.GetValueOrDefault().ToShortDateString(),
                          comparedProject.StartDate.GetValueOrDefault().ToShortDateString()) &&
                          string.Equals(
                          project.EndDate.GetValueOrDefault().ToShortDateString(),
                          comparedProject.EndDate.GetValueOrDefault().ToShortDateString()) &&
                          string.Equals(
                          project.SourceOverheadSettingsLastUpdate.GetValueOrDefault().ToShortDateString(),
                          comparedProject.SourceOverheadSettingsLastUpdate.GetValueOrDefault().ToShortDateString()) &&
                          project.Status == comparedProject.Status &&
                          project.YearlyProductionQuantity == comparedProject.YearlyProductionQuantity &&
                          project.LifeTime == comparedProject.LifeTime &&
                          project.DepreciationPeriod == comparedProject.DepreciationPeriod &&
                          project.DepreciationRate == comparedProject.DepreciationRate &&
                          project.IsDeleted == comparedProject.IsDeleted &&
                          project.LogisticCostRatio == comparedProject.LogisticCostRatio &&
                          project.Version == comparedProject.Version;

            // If projects are not equal => return false.
            if (!result)
            {
                return false;
            }

            // Retrieve the media of project
            MediaManager mediaManager1 = new MediaManager(DataAccessFactory.CreateDataSourceManager(projectContext));
            Collection<Media> projectMedia = mediaManager1.GetAllMedia(project);

            // Retrieve the media of compared project
            MediaManager mediaManager2 = new MediaManager(DataAccessFactory.CreateDataSourceManager(comparedProjectContext));
            Collection<Media> comparedProjectMedia = mediaManager2.GetAllMedia(comparedProject);

            // Verifies medias of projects are equal
            foreach (Media media in projectMedia)
            {
                Media correspondingMedia = comparedProjectMedia.Where(m => string.Equals(m.OriginalFileName, media.OriginalFileName) && m.Content.SequenceEqual(media.Content)).FirstOrDefault();

                result = AreEqual(media, correspondingMedia);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if project leaders are equal.
            result = AreEqual(project.ProjectLeader, comparedProject.ProjectLeader);
            if (!result)
            {
                return false;
            }

            // Verifies if ResponsibleCalculators of projects are equal.
            result = AreEqual(project.ResponsibleCalculator, comparedProject.ResponsibleCalculator);
            if (!result)
            {
                return false;
            }

            // Verifies if Customers of projects are equal.
            result = AreEqual(project.Customer, comparedProject.Customer);
            if (!result)
            {
                return false;
            }

            // Verifies if Overhead settings of projects are equal.
            result = AreEqual(project.OverheadSettings, comparedProject.OverheadSettings);
            if (!result)
            {
                return false;
            }

            // Verifies if parts of projects are equal.
            foreach (Part part in project.Parts)
            {
                Part correspondingPart = comparedProject.Parts.Where(p => string.Equals(p.Name, part.Name)).FirstOrDefault();

                result = AreEqual(part, projectContext, correspondingPart, comparedProjectContext);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if subassemblies of projects are equal.
            foreach (Assembly assembly in project.Assemblies)
            {
                Assembly correspondingSubassembly = comparedProject.Assemblies.Where(a => string.Equals(a.Name, assembly.Name)).FirstOrDefault();

                result = AreEqual(assembly, projectContext, correspondingSubassembly, comparedProjectContext);
                if (!result)
                {
                    return false;
                }
            }

            return result;
        }

        /// <summary>
        /// Compares two medias
        /// </summary>
        /// <param name="media">The compared media</param>
        /// <param name="mediaToCompareWith">The media to compare with</param>
        /// <returns>True if medias are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, IsMasterData.
        /// </remarks>
        private bool AreEqual(Media media, Media mediaToCompareWith)
        {
            if (media == null && mediaToCompareWith == null)
            {
                return true;
            }

            if ((media == null && mediaToCompareWith != null) || (media != null && mediaToCompareWith == null))
            {
                return false;
            }

            bool result = media.Type == mediaToCompareWith.Type &&
                          media.Content.SequenceEqual(mediaToCompareWith.Content) &&
                          media.Size == mediaToCompareWith.Size &&
                          string.Equals(media.OriginalFileName, mediaToCompareWith.OriginalFileName);

            return result;
        }

        /// <summary>
        /// Compares two users
        /// </summary>
        /// <param name="user">The compared user</param>
        /// <param name="userToCompareWith">The user to compare with</param>
        /// <returns>True if medias are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: IsReleased, Guid.
        /// </remarks>
        private bool AreEqual(User user, User userToCompareWith)
        {
            if (user == null && userToCompareWith == null)
            {
                return true;
            }

            if ((user == null && userToCompareWith != null) || (user != null && userToCompareWith == null))
            {
                return false;
            }

            bool result = string.Equals(user.Name, userToCompareWith.Name) &&
                          string.Equals(user.Description, userToCompareWith.Description) &&
                          string.Equals(user.Username, userToCompareWith.Username) &&
                          string.Equals(user.Password, userToCompareWith.Password) &&
                          user.Disabled == userToCompareWith.Disabled;

            // If entities are not equal -> return false.
            if (!result)
            {
                return false;
            }

            result = AreEqual(user.UICurrency, userToCompareWith.UICurrency);

            return result;
        }

        /// <summary>
        /// Compares two countries
        /// </summary>
        /// <param name="country">The compared country</param>
        /// <param name="countryToCompareWith">The country to compare with</param>
        /// <returns>True if countries are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: IsReleased, Guid.
        /// </remarks>
        private bool AreEqual(Country country, Country countryToCompareWith)
        {
            if (country == null && countryToCompareWith == null)
            {
                return true;
            }

            if ((country == null && countryToCompareWith != null) || (country != null && countryToCompareWith == null))
            {
                return false;
            }

            bool result = string.Equals(country.Name, countryToCompareWith.Name) &&
                          AreEqual(country.Currency, countryToCompareWith.Currency) &&
                          AreEqual(country.WeightMeasurementUnit, countryToCompareWith.WeightMeasurementUnit) &&
                          AreEqual(country.LengthMeasurementUnit, countryToCompareWith.LengthMeasurementUnit) &&
                          AreEqual(country.VolumeMeasurementUnit, country.VolumeMeasurementUnit) &&
                          AreEqual(country.TimeMeasurementUnit, countryToCompareWith.TimeMeasurementUnit) &&
                          AreEqual(country.FloorMeasurementUnit, countryToCompareWith.FloorMeasurementUnit) &&
                          AreEqual(country.CountrySetting, countryToCompareWith.CountrySetting);

            return result;
        }

        /// <summary>
        /// Compares two currencies
        /// </summary>
        /// <param name="currency">The compared currency</param>
        /// <param name="currencyToCompareWith">The currency to compare with</param>
        /// <returns>True if currencies are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: IsReleased, Guid.
        /// </remarks>
        private bool AreEqual(Currency currency, Currency currencyToCompareWith)
        {
            if (currency == null && currencyToCompareWith == null)
            {
                return true;
            }

            if ((currency == null && currencyToCompareWith != null) || (currency != null && currencyToCompareWith == null))
            {
                return false;
            }

            bool result = currency.Name == currencyToCompareWith.Name
                && currency.Symbol == currencyToCompareWith.Symbol
                && currency.ExchangeRate == currencyToCompareWith.ExchangeRate
                && currency.IsoCode == currencyToCompareWith.IsoCode;

            return result;
        }

        /// <summary>
        /// compares two measurement units
        /// </summary>
        /// <param name="measurementUnit">The compared measurement unit</param>
        /// <param name="measurementUnitToCompareWith">The measurement unit to compare with</param>
        /// <returns>True if measurement units are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: IsReleased, Guid.
        /// </remarks>
        private bool AreEqual(MeasurementUnit measurementUnit, MeasurementUnit measurementUnitToCompareWith)
        {
            if (measurementUnit == null && measurementUnitToCompareWith == null)
            {
                return true;
            }

            if ((measurementUnit == null && measurementUnitToCompareWith != null) || (measurementUnit != null && measurementUnitToCompareWith == null))
            {
                return false;
            }

            bool result = string.Equals(measurementUnit.Name, measurementUnitToCompareWith.Name) &&
                          string.Equals(measurementUnit.Symbol, measurementUnitToCompareWith.Symbol) &&
                          measurementUnit.ConversionRate == measurementUnitToCompareWith.ConversionRate &&
                          measurementUnit.Type == measurementUnitToCompareWith.Type &&
                          measurementUnit.ScaleID == measurementUnitToCompareWith.ScaleID &&
                          measurementUnit.ScaleFactor == measurementUnitToCompareWith.ScaleFactor;

            return result;
        }

        /// <summary>
        /// Compares two country settings
        /// </summary>
        /// <param name="countrySetting">The compared country setting</param>
        /// <param name="countrySettingToCompareWith">The country setting to compare with</param>
        /// <returns>True if country settings are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid.
        /// </remarks>
        private bool AreEqual(CountrySetting countrySetting, CountrySetting countrySettingToCompareWith)
        {
            if (countrySetting == null && countrySettingToCompareWith == null)
            {
                return true;
            }

            if ((countrySetting == null && countrySettingToCompareWith != null) || (countrySetting != null && countrySettingToCompareWith == null))
            {
                return false;
            }

            bool result = countrySetting.UnskilledLaborCost == countrySettingToCompareWith.UnskilledLaborCost &&
                          countrySetting.SkilledLaborCost == countrySettingToCompareWith.SkilledLaborCost &&
                          countrySetting.ForemanCost == countrySettingToCompareWith.ForemanCost &&
                          countrySetting.TechnicianCost == countrySettingToCompareWith.TechnicianCost &&
                          countrySetting.EngineerCost == countrySettingToCompareWith.EngineerCost &&
                          countrySetting.EnergyCost == countrySettingToCompareWith.EnergyCost &&
                          countrySetting.AirCost == countrySettingToCompareWith.AirCost &&
                          countrySetting.WaterCost == countrySettingToCompareWith.WaterCost &&
                          countrySetting.ProductionAreaRentalCost == countrySettingToCompareWith.ProductionAreaRentalCost &&
                          countrySetting.OfficeAreaRentalCost == countrySettingToCompareWith.OfficeAreaRentalCost &&
                          countrySetting.InterestRate == countrySettingToCompareWith.InterestRate &&
                          countrySetting.ShiftCharge1ShiftModel == countrySettingToCompareWith.ShiftCharge1ShiftModel &&
                          countrySetting.ShiftCharge2ShiftModel == countrySettingToCompareWith.ShiftCharge2ShiftModel &&
                          countrySetting.ShiftCharge3ShiftModel == countrySettingToCompareWith.ShiftCharge3ShiftModel &&
                          countrySetting.LaborAvailability == countrySettingToCompareWith.LaborAvailability &&
                          countrySetting.IsScrambled == countrySettingToCompareWith.IsScrambled;

            return result;
        }

        /// <summary>
        /// Compares two customers
        /// </summary>
        /// <param name="customer">The compared customer</param>
        /// <param name="customerToCompareWith">The customer to compare with</param>
        /// <returns>True if customers are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, IsMasterData.
        /// </remarks>
        private bool AreEqual(Customer customer, Customer customerToCompareWith)
        {
            if (customer == null && customerToCompareWith == null)
            {
                return true;
            }

            if ((customer == null && customerToCompareWith != null) || (customer != null && customerToCompareWith == null))
            {
                return false;
            }

            bool result = string.Equals(customer.Name, customerToCompareWith.Name) &&
                          string.Equals(customer.Description, customerToCompareWith.Description) &&
                          string.Equals(customer.Number, customerToCompareWith.Number) &&
                          string.Equals(customer.StreetAddress, customerToCompareWith.StreetAddress) &&
                          string.Equals(customer.ZipCode, customerToCompareWith.ZipCode) &&
                          string.Equals(customer.City, customerToCompareWith.City) &&
                          string.Equals(customer.FirstContactName, customerToCompareWith.FirstContactName) &&
                          string.Equals(customer.FirstContactEmail, customerToCompareWith.FirstContactEmail) &&
                          string.Equals(customer.FirstContactPhone, customerToCompareWith.FirstContactPhone) &&
                          string.Equals(customer.FirstContactFax, customerToCompareWith.FirstContactFax) &&
                          string.Equals(customer.FirstContactMobile, customerToCompareWith.FirstContactMobile) &&
                          string.Equals(customer.FirstContactWebAddress, customerToCompareWith.FirstContactWebAddress) &&
                          string.Equals(customer.SecondContactName, customerToCompareWith.SecondContactName) &&
                          string.Equals(customer.SecondContactEmail, customerToCompareWith.SecondContactEmail) &&
                          string.Equals(customer.SecondContactPhone, customerToCompareWith.SecondContactPhone) &&
                          string.Equals(customer.SecondContactFax, customerToCompareWith.SecondContactFax) &&
                          string.Equals(customer.SecondContactMobile, customerToCompareWith.SecondContactMobile) &&
                          string.Equals(customer.SecondContactWebAddress, customerToCompareWith.SecondContactWebAddress) &&
                          string.Equals(customer.Country, customerToCompareWith.Country) &&
                          string.Equals(customer.State, customerToCompareWith.State) &&
                          customer.Type == customerToCompareWith.Type;

            return result;
        }

        /// <summary>
        /// Compares two overhead settings
        /// </summary>
        /// <param name="setting">The compared overhead setting</param>
        /// <param name="settingToCompareWith">The overhead setting to compare with</param>
        /// <returns>True if overhead settings are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, LastUpdateTime, Guid, IsMasterData.
        /// </remarks>
        private bool AreEqual(OverheadSetting setting, OverheadSetting settingToCompareWith)
        {
            if (setting == null && settingToCompareWith == null)
            {
                return true;
            }

            if ((setting == null && settingToCompareWith != null) || (setting != null && settingToCompareWith == null))
            {
                return false;
            }

            bool result = setting.MaterialOverhead == settingToCompareWith.MaterialOverhead &&
                          setting.ConsumableOverhead == settingToCompareWith.ConsumableOverhead &&
                          setting.CommodityOverhead == settingToCompareWith.CommodityOverhead &&
                          setting.ExternalWorkOverhead == settingToCompareWith.ExternalWorkOverhead &&
                          setting.ManufacturingOverhead == settingToCompareWith.ManufacturingOverhead &&
                          setting.MaterialMargin == settingToCompareWith.MaterialMargin &&
                          setting.ConsumableMargin == settingToCompareWith.ConsumableMargin &&
                          setting.CommodityMargin == settingToCompareWith.CommodityMargin &&
                          setting.ExternalWorkMargin == settingToCompareWith.ExternalWorkMargin &&
                          setting.ManufacturingMargin == settingToCompareWith.ManufacturingMargin &&
                          setting.OtherCostOHValue == settingToCompareWith.OtherCostOHValue &&
                          setting.PackagingOHValue == settingToCompareWith.PackagingOHValue &&
                          setting.LogisticOHValue == settingToCompareWith.LogisticOHValue &&
                          setting.SalesAndAdministrationOHValue == settingToCompareWith.SalesAndAdministrationOHValue;

            return result;
        }

        /// <summary>
        /// Compares two parts 
        /// </summary>
        /// <param name="part">The compared part</param>
        /// <param name="partContext">The 'EntityContext' of compared part</param>
        /// <param name="comparedPart">The part to compare with</param>
        /// <param name="comparedPartContext">The 'EntityContext' part to compare with</param>
        /// <returns>True if parts are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, IsMasterData.
        /// </remarks>
        private bool AreEqual(
            Part part,
            DbIdentifier partContext,
            Part comparedPart,
            DbIdentifier comparedPartContext)
        {
            if (part == null && comparedPart == null)
            {
                return true;
            }

            if ((part == null && comparedPart != null) || (part != null && comparedPart == null))
            {
                return false;
            }

            bool result = string.Equals(part.Name, comparedPart.Name) &&
                          string.Equals(part.Number, comparedPart.Number) &&
                          string.Equals(part.Description, comparedPart.Description) &&
                          string.Equals(part.AdditionalDescription, comparedPart.AdditionalDescription) &&
                          string.Equals(part.ManufacturerDescription, comparedPart.ManufacturerDescription) &&
                          string.Equals(part.AdditionalRemarks, comparedPart.AdditionalRemarks) &&
                          string.Equals(part.CalculationVariant, comparedPart.CalculationVariant) &&
                          string.Equals(part.ManufacturingCountry, comparedPart.ManufacturingCountry) &&
                          string.Equals(part.ManufacturingSupplier, comparedPart.ManufacturingSupplier) &&
                          part.CalculationStatus == comparedPart.CalculationStatus &&
                          part.CalculationAccuracy == comparedPart.CalculationAccuracy &&
                          part.CalculationApproach == comparedPart.CalculationApproach &&
                          part.BatchSizePerYear == comparedPart.BatchSizePerYear &&
                          part.PurchasePrice == comparedPart.PurchasePrice &&
                          part.TargetPrice == comparedPart.TargetPrice &&
                          part.DelivertType == comparedPart.DelivertType &&
                          part.AssetRate == comparedPart.AssetRate &&
                          part.SBMActive == comparedPart.SBMActive &&
                          part.IsExternal == comparedPart.IsExternal &&
                          part.IsDeleted == comparedPart.IsDeleted &&
                          part.DevelopmentCost == comparedPart.DevelopmentCost &&
                          part.ProjectInvest == comparedPart.ProjectInvest &&
                          part.OtherCost == comparedPart.OtherCost &&
                          part.PackagingCost == comparedPart.PackagingCost &&
                          part.CalculateLogisticCost == comparedPart.CalculateLogisticCost &&
                          part.LogisticCost == comparedPart.LogisticCost &&                          
                          part.EstimatedCost == comparedPart.EstimatedCost &&
                          part.YearlyProductionQuantity == comparedPart.YearlyProductionQuantity &&
                          part.LifeTime == comparedPart.LifeTime &&
                          part.PaymentTerms == comparedPart.PaymentTerms &&
                          part.Version == comparedPart.Version &&
                          part.Reusable == comparedPart.Reusable &&
                          part.ManufacturingRatio == comparedPart.ManufacturingRatio;

            // If parts are not equal => return false.
            if (!result)
            {
                return false;
            }

            // Retrieve the part's media
            MediaManager mediaManager1 = new MediaManager(DataAccessFactory.CreateDataSourceManager(partContext));
            Collection<Media> partMedia = mediaManager1.GetAllMedia(part);

            // Retrieve the comparedPart's media
            MediaManager mediaManager2 = new MediaManager(DataAccessFactory.CreateDataSourceManager(comparedPartContext));
            Collection<Media> comparedPartMedia = mediaManager2.GetAllMedia(comparedPart);

            // Verifies if Medias of parts are equal.
            foreach (Media media in partMedia)
            {
                Media correspondingMedia = comparedPartMedia.Where(m => string.Equals(m.OriginalFileName, media.OriginalFileName) && m.Content.SequenceEqual(media.Content)).FirstOrDefault();

                result = AreEqual(media, correspondingMedia);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if commodities of parts are equal.
            foreach (Commodity commodity in part.Commodities)
            {
                Commodity correspondingCommodity = comparedPart.Commodities.Where(c => string.Equals(c.Name, commodity.Name)).FirstOrDefault();

                result = AreEqual(commodity, partContext, correspondingCommodity, comparedPartContext);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if raw materials of parts are equal.
            foreach (RawMaterial material in part.RawMaterials)
            {
                RawMaterial correspondingMaterial = comparedPart.RawMaterials.Where(m => string.Equals(m.Name, material.Name)).FirstOrDefault();

                result = AreEqual(material, partContext, correspondingMaterial, comparedPartContext);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if processes of parts are equals.
            result = AreEqual(part.Process, partContext, comparedPart.Process, comparedPartContext);
            if (!result)
            {
                return false;
            }

            // Verifies if manufacturers of parts are equals.
            result = AreEqual(part.Manufacturer, comparedPart.Manufacturer);
            if (!result)
            {
                return false;
            }

            // Verifies if overhead settings of parts are equals.
            result = AreEqual(part.OverheadSettings, comparedPart.OverheadSettings);
            if (!result)
            {
                return false;
            }

            // Verifies if country settings of parts are equal.
            result = AreEqual(part.CountrySettings, comparedPart.CountrySettings);
            if (!result)
            {
                return false;
            }

            // Verifies if calculators of parts are equal.
            result = AreEqual(part.CalculatorUser, comparedPart.CalculatorUser);

            return result;
        }

        /// <summary>
        /// Compares two assemblies
        /// </summary>
        /// <param name="assembly">The compared assembly</param>
        /// <param name="assemblyContext">The 'Entity Context' of assembly</param>
        /// <param name="comparedAssembly">The assembly to compare with</param>
        /// <param name="comparedAssemblyContext">The 'Entity Context' of compared assembly</param>
        /// <returns>True if assemblies are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, LastChangeTimestamp, Guid, IsMasterData.
        /// </remarks>
        private bool AreEqual(
            Assembly assembly,
            DbIdentifier assemblyContext,
            Assembly comparedAssembly,
            DbIdentifier comparedAssemblyContext)
        {
            if (assembly == null && comparedAssembly == null)
            {
                return true;
            }

            if ((assembly == null && comparedAssembly != null) || (assembly != null && comparedAssembly == null))
            {
                return false;
            }

            bool result = string.Equals(assembly.Name, comparedAssembly.Name) &&
                          string.Equals(assembly.Number, comparedAssembly.Number) &&
                          string.Equals(assembly.CalculationVariant, comparedAssembly.CalculationVariant) &&
                          string.Equals(assembly.AssemblingCountry, comparedAssembly.AssemblingCountry) &&
                          string.Equals(assembly.AssemblingSupplier, comparedAssembly.AssemblingSupplier) &&
                          assembly.IsDeleted == comparedAssembly.IsDeleted &&
                          assembly.DevelopmentCost == comparedAssembly.DevelopmentCost &&
                          assembly.ProjectInvest == comparedAssembly.ProjectInvest &&
                          assembly.OtherCost == comparedAssembly.OtherCost &&
                          assembly.PackagingCost == comparedAssembly.PackagingCost &&
                          assembly.CalculateLogisticCost == comparedAssembly.CalculateLogisticCost &&
                          assembly.LogisticCost == comparedAssembly.LogisticCost &&                          
                          assembly.BatchSizePerYear == comparedAssembly.BatchSizePerYear &&
                          assembly.YearlyProductionQuantity == comparedAssembly.YearlyProductionQuantity &&
                          assembly.LifeTime == comparedAssembly.LifeTime &&
                          assembly.PaymentTerms == comparedAssembly.PaymentTerms &&
                          assembly.PurchasePrice == comparedAssembly.PurchasePrice &&
                          assembly.TargetPrice == comparedAssembly.TargetPrice &&
                          assembly.DeliveryType == comparedAssembly.DeliveryType &&
                          assembly.SBMActive == comparedAssembly.SBMActive &&
                          assembly.AssetRate == comparedAssembly.AssetRate &&
                          assembly.IsExternal == comparedAssembly.IsExternal &&
                          assembly.CalculationAccuracy == comparedAssembly.CalculationAccuracy &&
                          assembly.EstimatedCost == comparedAssembly.EstimatedCost &&
                          assembly.Version == comparedAssembly.Version &&
                          assembly.Reusable == comparedAssembly.Reusable &&
                          assembly.CalculationApproach == comparedAssembly.CalculationApproach;

            // If assemblies are not equal => return false.
            if (!result)
            {
                return false;
            }

            // Retrieve the assembly's media
            MediaManager mediaManager1 = new MediaManager(DataAccessFactory.CreateDataSourceManager(assemblyContext));
            Collection<Media> assemblyMedia = mediaManager1.GetAllMedia(assembly);

            // Retrieve the compared assembly's media
            MediaManager mediaManager2 = new MediaManager(DataAccessFactory.CreateDataSourceManager(comparedAssemblyContext));
            Collection<Media> comparedProjectMedia = mediaManager2.GetAllMedia(comparedAssembly);

            // Verifies if Medias of assemblies are equal.
            foreach (Media media in assemblyMedia)
            {
                Media correspondingMedia = comparedProjectMedia.Where(m => string.Equals(m.OriginalFileName, media.OriginalFileName) && m.Content.SequenceEqual(media.Content)).FirstOrDefault();

                result = AreEqual(media, correspondingMedia);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if processes of assemblies are equals.
            result = AreEqual(assembly.Process, assemblyContext, comparedAssembly.Process, comparedAssemblyContext);
            if (!result)
            {
                return false;
            }

            // Verifies if manufacturers of assemblies are equals.
            result = AreEqual(assembly.Manufacturer, comparedAssembly.Manufacturer);
            if (!result)
            {
                return false;
            }

            // Verifies if overhead settings of assemblies are equals.
            result = AreEqual(assembly.OverheadSettings, comparedAssembly.OverheadSettings);
            if (!result)
            {
                return false;
            }

            // Verifies if country settings of assemblies are equal.
            result = AreEqual(assembly.CountrySettings, comparedAssembly.CountrySettings);
            if (!result)
            {
                return false;
            }

            // Verifies if parts of assemblies are equal.
            foreach (Part part in assembly.Parts)
            {
                Part correspondingPart = comparedAssembly.Parts.Where(p => string.Equals(p.Name, part.Name)).FirstOrDefault();

                result = AreEqual(part, assemblyContext, correspondingPart, comparedAssemblyContext);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if subassemblies of assemblies are equal.
            foreach (Assembly subassembly in assembly.Subassemblies)
            {
                Assembly correspondingSubassembly = comparedAssembly.Subassemblies.Where(s => string.Equals(s.Name, subassembly.Name)).FirstOrDefault();

                result = AreEqual(subassembly, assemblyContext, correspondingSubassembly, comparedAssemblyContext);
                if (!result)
                {
                    return false;
                }
            }

            return result;
        }

        /// <summary>
        /// Compares two raw materials 
        /// </summary>
        /// <param name="material">The compared raw material</param>
        /// <param name="materialContext">The 'EntityContext' of raw material</param>
        /// <param name="comparedMaterial">The raw material to compare with</param>
        /// <param name="comparedMaterialContext">The 'Entity Context' of raw material to compare with</param>
        /// <returns>True if raw materials are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, IsMasterData.
        /// </remarks>
        private bool AreEqual(
            RawMaterial material,
            DbIdentifier materialContext,
            RawMaterial comparedMaterial,
            DbIdentifier comparedMaterialContext)
        {
            if (material == null && comparedMaterial == null)
            {
                return true;
            }

            if ((material == null && comparedMaterial != null) || (material != null && comparedMaterial == null))
            {
                return false;
            }

            bool result = string.Equals(material.Name, comparedMaterial.Name) &&
                          string.Equals(material.NormName, comparedMaterial.NormName) &&
                          string.Equals(material.Remarks, comparedMaterial.Remarks) &&
                          string.Equals(material.NameUK, comparedMaterial.NameUK) &&
                          string.Equals(material.NameUS, comparedMaterial.NameUS) &&
                          material.Price == comparedMaterial.Price &&
                          material.RejectRatio == comparedMaterial.RejectRatio &&
                          material.Remarks == comparedMaterial.Remarks &&
                          material.ParentWeight == comparedMaterial.ParentWeight &&
                          material.Quantity == comparedMaterial.Quantity &&
                          material.ScrapRefundRatio == comparedMaterial.ScrapRefundRatio &&
                          material.Loss == comparedMaterial.Loss &&
                          material.IsDeleted == comparedMaterial.IsDeleted &&
                          material.IsScrambled == comparedMaterial.IsScrambled &&
                          material.Sprue == comparedMaterial.Sprue &&
                          material.ScrapCalculationType == comparedMaterial.ScrapCalculationType &&
                          string.Equals(material.YieldStrength, comparedMaterial.YieldStrength) &&
                          string.Equals(material.RuptureStrength, comparedMaterial.RuptureStrength) &&
                          string.Equals(material.Density, comparedMaterial.Density) &&
                          string.Equals(material.MaxElongation, comparedMaterial.MaxElongation) &&
                          string.Equals(material.GlassTransitionTemperature, comparedMaterial.GlassTransitionTemperature) &&
                          string.Equals(material.Rx, comparedMaterial.Rx) &&
                          string.Equals(material.Rm, comparedMaterial.Rm);

            // If raw materials are not equal => return false.
            if (!result)
            {
                return false;
            }

            // Retrieve the material's media
            MediaManager mediaManager1 = new MediaManager(DataAccessFactory.CreateDataSourceManager(materialContext));
            Media materialMedia = mediaManager1.GetPicture(material);

            // Retrieve the compared material's media
            MediaManager mediaManager2 = new MediaManager(DataAccessFactory.CreateDataSourceManager(comparedMaterialContext));
            Media comparedMaterialMedia = mediaManager2.GetPicture(comparedMaterial);

            // Verifies if Medias of materials are equal.
            result = AreEqual(materialMedia, comparedMaterialMedia);
            if (!result)
            {
                return false;
            }

            // Verifies if Manufacturers of materials are equal.
            result = AreEqual(material.Manufacturer, comparedMaterial.Manufacturer);
            if (!result)
            {
                return false;
            }

            // Verifies if ClassificationLevel1 of materials are equal.
            result = AreEqual(material.MaterialsClassificationL1, comparedMaterial.MaterialsClassificationL1);
            if (!result)
            {
                return false;
            }

            // Verifies if ClassificationLevel2 of materials are equal.
            result = AreEqual(material.MaterialsClassificationL2, comparedMaterial.MaterialsClassificationL2);
            if (!result)
            {
                return false;
            }

            // Verifies if ClassificationLevel3 of materials are equal.
            result = AreEqual(material.MaterialsClassificationL3, comparedMaterial.MaterialsClassificationL3);
            if (!result)
            {
                return false;
            }

            // Verifies if ClassificationLevel4 of materials are equal.
            result = AreEqual(material.MaterialsClassificationL4, comparedMaterial.MaterialsClassificationL4);
            if (!result)
            {
                return false;
            }

            // Verifies if PriceUnitBase of materials are equal.
            result = AreEqual(material.PriceUnitBase, comparedMaterial.PriceUnitBase);
            if (!result)
            {
                return false;
            }

            // Verifies if ParentWeightUnitBase of materials are equal.
            result = AreEqual(material.ParentWeightUnitBase, comparedMaterial.ParentWeightUnitBase);
            if (!result)
            {
                return false;
            }

            // Verifies if QuantityUnitBase of materials are equal.
            result = AreEqual(material.QuantityUnitBase, comparedMaterial.QuantityUnitBase);
            if (!result)
            {
                return false;
            }

            // Verifies if DeliveryTypes of raw materials are equal.
            result = AreEqual(material.DeliveryType, comparedMaterial.DeliveryType);

            return result;
        }

        /// <summary>
        /// Compares two commodities
        /// </summary>
        /// <param name="commodity">The compared commodity</param>
        /// <param name="commodityContext">The 'Entity Context' of commodity</param>
        /// <param name="comparedCommodity">The commodity to compare with</param>
        /// <param name="comparedCommodityContext">The 'Entity Context' of commodity to compare with</param>
        /// <returns>True if commodities are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, IsMasterData.
        /// </remarks>
        private bool AreEqual(
            Commodity commodity,
            DbIdentifier commodityContext,
            Commodity comparedCommodity,
            DbIdentifier comparedCommodityContext)
        {
            if (commodity == null && comparedCommodity == null)
            {
                return true;
            }

            if ((commodity == null && comparedCommodity != null) || (commodity != null && comparedCommodity == null))
            {
                return false;
            }

            bool result = string.Equals(commodity.Name, comparedCommodity.Name) &&
                          string.Equals(commodity.Remarks, comparedCommodity.Remarks) &&
                          string.Equals(commodity.PartNumber, comparedCommodity.PartNumber) &&
                          commodity.Price == comparedCommodity.Price &&
                          commodity.Weight == comparedCommodity.Weight &&
                          commodity.RejectRatio == comparedCommodity.RejectRatio &&
                          commodity.Amount == comparedCommodity.Amount &&
                          commodity.IsDeleted == comparedCommodity.IsDeleted;

            // If commodities are not equal -> return false;
            if (!result)
            {
                return false;
            }

            // Verifies if Manufacturers of commodities are equal.
            result = AreEqual(commodity.Manufacturer, comparedCommodity.Manufacturer);
            if (!result)
            {
                return false;
            }

            // Retrieve the commodity's media
            MediaManager mediaManager1 = new MediaManager(DataAccessFactory.CreateDataSourceManager(commodityContext));
            Media commodityMedia = mediaManager1.GetPicture(commodity);

            // Retrieve the compared commodity's media
            MediaManager mediaManager2 = new MediaManager(DataAccessFactory.CreateDataSourceManager(comparedCommodityContext));
            Media comparedCommodityMedia = mediaManager2.GetPicture(comparedCommodity);

            // Verifies if Medias of commodities are equal.
            result = AreEqual(commodityMedia, comparedCommodityMedia);
            if (!result)
            {
                return false;
            }

            // Verifies if WeightUnits of commodities are equal.
            result = AreEqual(commodity.WeightUnitBase, comparedCommodity.WeightUnitBase);

            return result;
        }

        /// <summary>
        /// Compares two consumables
        /// </summary>
        /// <param name="consumable">The compared consumable</param>
        /// <param name="consumableToCompareWith">The consumable to compare with</param>
        /// <returns>True if consumables are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, IsMasterData.
        /// </remarks>
        private bool AreEqual(Consumable consumable, Consumable consumableToCompareWith)
        {
            if (consumable == null && consumableToCompareWith == null)
            {
                return true;
            }

            if ((consumable == null && consumableToCompareWith != null) || (consumable != null && consumableToCompareWith == null))
            {
                return false;
            }

            bool result = string.Equals(consumable.Name, consumableToCompareWith.Name) &&
                          string.Equals(consumable.Description, consumableToCompareWith.Description) &&
                          consumable.Price == consumableToCompareWith.Price &&
                          consumable.Amount == consumableToCompareWith.Amount &&
                          consumable.IsDeleted == consumableToCompareWith.IsDeleted;

            // If consumables are not equal -> return false.
            if (!result)
            {
                return false;
            }

            // Verifies if Manufacturers are equal.
            result = AreEqual(consumable.Manufacturer, consumableToCompareWith.Manufacturer);
            if (!result)
            {
                return false;
            }

            // Verifies if PriceUnits are equal.
            result = AreEqual(consumable.PriceUnitBase, consumableToCompareWith.PriceUnitBase);
            if (!result)
            {
                return false;
            }

            // Verifies if AmountUnits are equal.
            result = AreEqual(consumable.AmountUnitBase, consumableToCompareWith.AmountUnitBase);

            return result;
        }

        /// <summary>
        /// Compares two machines
        /// </summary>
        /// <param name="machine">The compared machine</param>
        /// <param name="machineContext">The 'EntityContext' of the machine</param>
        /// <param name="comparedMachine">The machine to compare with</param>
        /// <param name="comparedMachineContext">The 'EntityContext' of machine to compare with</param>
        /// <returns>True if machines are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Index, Guid, IsMasterData.
        /// </remarks>
        private bool AreEqual(
            Machine machine,
            DbIdentifier machineContext,
            Machine comparedMachine,
            DbIdentifier comparedMachineContext)
        {
            if (machine == null && comparedMachine == null)
            {
                return true;
            }

            if ((machine == null && comparedMachine != null) || (machine != null && comparedMachine == null))
            {
                return false;
            }

            bool result = string.Equals(machine.Name, comparedMachine.Name) &&
                          string.Equals(machine.RegistrationNumber, comparedMachine.RegistrationNumber) &&
                          string.Equals(machine.DescriptionOfFunction, comparedMachine.DescriptionOfFunction) &&
                          string.Equals(machine.InvestmentRemarks, comparedMachine.InvestmentRemarks) &&
                          string.Equals(machine.MountingCubeLength, comparedMachine.MountingCubeLength) &&
                          string.Equals(machine.MountingCubeWidth, comparedMachine.MountingCubeWidth) &&
                          string.Equals(machine.MountingCubeHeight, comparedMachine.MountingCubeHeight) &&
                          string.Equals(machine.MaxDiameterRodPerChuckMaxThickness, comparedMachine.MaxDiameterRodPerChuckMaxThickness) &&
                          string.Equals(machine.MaxPartsWeightPerCapacity, comparedMachine.MaxPartsWeightPerCapacity) &&
                          string.Equals(machine.LockingForce, comparedMachine.LockingForce) &&
                          string.Equals(machine.PressCapacity, comparedMachine.PressCapacity) &&
                          string.Equals(machine.MaximumSpeed, comparedMachine.MaximumSpeed) &&
                          string.Equals(machine.RapidFeedPerCuttingSpeed, comparedMachine.RapidFeedPerCuttingSpeed) &&
                          string.Equals(machine.StrokeRate, comparedMachine.StrokeRate) &&
                          string.Equals(machine.Other, comparedMachine.Other) &&
                          machine.ManufacturingYear == comparedMachine.ManufacturingYear &&
                          machine.FloorSize == comparedMachine.FloorSize &&
                          machine.WorkspaceArea == comparedMachine.WorkspaceArea &&
                          machine.PowerConsumption == comparedMachine.PowerConsumption &&
                          machine.AirConsumption == comparedMachine.AirConsumption &&
                          machine.WaterConsumption == comparedMachine.WaterConsumption &&
                          machine.FullLoadRate == comparedMachine.FullLoadRate &&
                          machine.MaxCapacity == comparedMachine.MaxCapacity &&
                          machine.OEE == comparedMachine.OEE &&
                          machine.Availability == comparedMachine.Availability &&
                          machine.MachineInvestment == comparedMachine.MachineInvestment &&
                          machine.SetupInvestment == comparedMachine.SetupInvestment &&
                          machine.AdditionalEquipmentInvestment == comparedMachine.AdditionalEquipmentInvestment &&
                          machine.FundamentalSetupInvestment == comparedMachine.FundamentalSetupInvestment &&
                          machine.ExternalWorkCost == comparedMachine.ExternalWorkCost &&
                          machine.MaterialCost == comparedMachine.MaterialCost &&
                          machine.ConsumablesCost == comparedMachine.ConsumablesCost &&
                          machine.RepairsCost == comparedMachine.RepairsCost &&
                          machine.KValue == comparedMachine.KValue &&
                          machine.CalculateWithKValue == comparedMachine.CalculateWithKValue &&
                          machine.IsDeleted == comparedMachine.IsDeleted &&
                          machine.IsScrambled == comparedMachine.IsScrambled &&
                          machine.ManualConsumableCostCalc == comparedMachine.ManualConsumableCostCalc &&
                          machine.ManualConsumableCostPercentage == comparedMachine.ManualConsumableCostPercentage &&
                          machine.IsProjectSpecific == comparedMachine.IsProjectSpecific &&
                          machine.DepreciationPeriod == comparedMachine.DepreciationPeriod &&
                          machine.DepreciationRate == comparedMachine.DepreciationRate;

            // If machines are not equal -> return false.
            if (!result)
            {
                return false;
            }

            // Retrieve the media of machine
            MediaManager mediaManager1 = new MediaManager(DataAccessFactory.CreateDataSourceManager(machineContext));
            Media machineMedia = mediaManager1.GetPicture(machine);

            // Retrieve the media of the machine to compare with
            MediaManager mediaManager2 = new MediaManager(DataAccessFactory.CreateDataSourceManager(comparedMachineContext));
            Media comparedMachineMedia = mediaManager2.GetPicture(comparedMachine);

            // Verifies if Medias are equal.
            result = AreEqual(machineMedia, comparedMachineMedia);
            if (!result)
            {
                return false;
            }

            // Verifies if Manufacturers are equal.
            result = AreEqual(machine.Manufacturer, comparedMachine.Manufacturer);
            if (!result)
            {
                return false;
            }

            // Verifies if main classifications are equal
            result = AreEqual(machine.MainClassification, comparedMachine.MainClassification);
            if (!result)
            {
                return false;
            }

            // Verifies if type classifications are equal.
            result = AreEqual(machine.TypeClassification, comparedMachine.TypeClassification);
            if (!result)
            {
                return false;
            }

            // Verifies if sub classifications are equal.
            result = AreEqual(machine.SubClassification, comparedMachine.SubClassification);
            if (!result)
            {
                return false;
            }

            // Verifies if classifications level4 are equal.
            result = AreEqual(machine.ClassificationLevel4, comparedMachine.ClassificationLevel4);

            return result;
        }

        /// <summary>
        /// Compares two dies
        /// </summary>
        /// <param name="die">The compared die</param>
        /// <param name="dieContext">The 'Entity Context' of die</param>
        /// <param name="comparedDie">The die to compare with</param>
        /// <param name="comparedDieContext">The 'Entity Context' of die to compare with</param>
        /// <returns>True if dies are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, IsMasterData.
        /// </remarks>
        private bool AreEqual(
            Die die,
            DbIdentifier dieContext,
            Die comparedDie,
            DbIdentifier comparedDieContext)
        {
            if (die == null && comparedDie == null)
            {
                return true;
            }

            if ((die == null && comparedDie != null) || (die != null && comparedDie == null))
            {
                return false;
            }

            bool result = string.Equals(die.Name, comparedDie.Name) &&
                          string.Equals(die.Remark, comparedDie.Remark) &&
                          die.IsDeleted == comparedDie.IsDeleted &&
                          die.Type == comparedDie.Type &&
                          die.Investment == comparedDie.Investment &&
                          die.ReusableInvest == comparedDie.ReusableInvest &&
                          die.Wear == comparedDie.Wear &&
                          die.IsWearInPercentage == comparedDie.IsWearInPercentage &&
                          die.Maintenance == comparedDie.Maintenance &&
                          die.IsMaintenanceInPercentage == comparedDie.IsMaintenanceInPercentage &&
                          die.LifeTime == comparedDie.LifeTime &&
                          die.AllocationRatio == comparedDie.AllocationRatio &&
                          die.DiesetsNumberPaidByCustomer == comparedDie.DiesetsNumberPaidByCustomer &&
                          die.CostAllocationBasedOnPartsPerLifeTime == comparedDie.CostAllocationBasedOnPartsPerLifeTime &&
                          die.CostAllocationNumberOfParts == comparedDie.CostAllocationNumberOfParts;

            // If entities are not equal -> return false.
            if (!result)
            {
                return false;
            }

            // Verifies if manufacturers of dies are equal.
            result = AreEqual(die.Manufacturer, comparedDie.Manufacturer);
            if (!result)
            {
                return false;
            }

            // Retrieve the media of die
            MediaManager mediaManager1 = new MediaManager(DataAccessFactory.CreateDataSourceManager(dieContext));
            Media dieMedia = mediaManager1.GetPictureOrVideo(die);

            // Retrieve the media of compared die
            MediaManager mediaManager2 = new MediaManager(DataAccessFactory.CreateDataSourceManager(comparedDieContext));
            Media comparedDieMedia = mediaManager2.GetPictureOrVideo(comparedDie);

            // Verifies if medias of dies are equal.
            result = AreEqual(dieMedia, comparedDieMedia);

            return result;
        }

        /// <summary>
        /// Compares two processes
        /// </summary>
        /// <param name="process">The compared process</param>
        /// <param name="processContext">The 'Entity Context' of compared process</param>
        /// <param name="comparedProcess">The process to compare with</param>
        /// <param name="comparedProcessContext">The 'Entity Context' of process to compare with</param>
        /// <returns>True if processes are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, IsMasterData.
        /// </remarks>
        private bool AreEqual(
            Process process,
            DbIdentifier processContext,
            Process comparedProcess,
            DbIdentifier comparedProcessContext)
        {
            if (process == null && comparedProcess == null)
            {
                return true;
            }

            if ((process == null && comparedProcess != null) || (process != null && comparedProcess == null))
            {
                return false;
            }

            // Verifies if steps of processes are equal.
            bool result = true;
            foreach (ProcessStep step in process.Steps)
            {
                ProcessStep correspondingStep = comparedProcess.Steps.Where(s => string.Equals(s.Name, step.Name)).FirstOrDefault();

                result = result && AreEqual(step, processContext, correspondingStep, comparedProcessContext);
                if (!result)
                {
                    return false;
                }
            }

            return result;
        }

        /// <summary>
        /// Compares two process steps
        /// </summary>
        /// <param name="step">The compared step</param>
        /// <param name="stepContext">The 'Entity Context' of compared step</param>
        /// <param name="comparedStep">The step to compare with</param>
        /// <param name="comparedStepContext">The 'Entity Context' of step to compare with</param>
        /// <returns>True if process steps are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, IsMasterData.
        /// </remarks>
        private bool AreEqual(
            ProcessStep step,
            DbIdentifier stepContext,
            ProcessStep comparedStep,
            DbIdentifier comparedStepContext)
        {
            if (step == null && comparedStep == null)
            {
                return true;
            }

            if ((step == null && comparedStep != null) || (step != null && comparedStep == null))
            {
                return false;
            }

            bool result = string.Equals(step.Name, comparedStep.Name) &&
                          string.Equals(step.Description, comparedStep.Description) &&
                          step.Accuracy == comparedStep.Accuracy &&
                          step.CycleTime == comparedStep.CycleTime &&
                          step.ProcessTime == comparedStep.ProcessTime &&
                          step.PartsPerCycle == comparedStep.PartsPerCycle &&
                          step.ScrapAmount == comparedStep.ScrapAmount &&
                          step.Rework == comparedStep.Rework &&
                          step.ShiftsPerWeek == comparedStep.ShiftsPerWeek &&
                          step.HoursPerShift == comparedStep.HoursPerShift &&
                          step.ProductionDaysPerWeek == comparedStep.ProductionDaysPerWeek &&
                          step.ProductionWeeksPerYear == comparedStep.ProductionWeeksPerYear &&
                          step.BatchSize == comparedStep.BatchSize &&
                          step.SetupsPerBatch == comparedStep.SetupsPerBatch &&
                          step.SetupTime == comparedStep.SetupTime &&
                          step.MaxDownTime == comparedStep.MaxDownTime &&
                          step.ProductionUnskilledLabour == comparedStep.ProductionUnskilledLabour &&
                          step.ProductionSkilledLabour == comparedStep.ProductionSkilledLabour &&
                          step.ProductionForeman == comparedStep.ProductionForeman &&
                          step.ProductionTechnicians == comparedStep.ProductionTechnicians &&
                          step.ProductionEngineers == comparedStep.ProductionEngineers &&
                          step.SetupUnskilledLabour == comparedStep.SetupUnskilledLabour &&
                          step.SetupSkilledLabour == comparedStep.SetupSkilledLabour &&
                          step.SetupForeman == comparedStep.SetupForeman &&
                          step.SetupTechnicians == comparedStep.SetupTechnicians &&
                          step.SetupEngineers == comparedStep.SetupEngineers &&
                          step.IsExternal == comparedStep.IsExternal &&
                          step.Price == comparedStep.Price &&
                          step.Index == comparedStep.Index &&
                          step.ExceedShiftCost == comparedStep.ExceedShiftCost &&
                          step.ExtraShiftsNumber == comparedStep.ExtraShiftsNumber &&
                          step.ShiftCostExceedRatio == comparedStep.ShiftCostExceedRatio;

            // If steps are not equal => return false;
            if (!result)
            {
                return false;
            }

            // Verifies if Types of steps are equal.
            result = AreEqual(step.Type, comparedStep.Type);
            if (!result)
            {
                return false;
            }

            // Verifies if SubTypes of steps are equal.
            result = AreEqual(step.SubType, comparedStep.SubType);
            if (!result)
            {
                return false;
            }

            // Retrieve the media of step
            MediaManager mediaManager1 = new MediaManager(DataAccessFactory.CreateDataSourceManager(stepContext));
            Media stepMedia = mediaManager1.GetPicture(step);

            // Retrieve the media of comparedStep
            MediaManager mediaManager2 = new MediaManager(DataAccessFactory.CreateDataSourceManager(comparedStepContext));
            Media comparedStepMedia = mediaManager2.GetPicture(comparedStep);

            // Verifies if Medias of steps are equal.
            result = AreEqual(stepMedia, comparedStepMedia);
            if (!result)
            {
                return false;
            }

            // Verifies if CycleTimeUnits of steps are equals.
            result = AreEqual(step.CycleTimeUnit, comparedStep.CycleTimeUnit);
            if (!result)
            {
                return false;
            }

            // Verifies if ProcessStepPartAmounts are equals.
            foreach (ProcessStepPartAmount partAmount in step.PartAmounts)
            {
                ProcessStepPartAmount correspondingPartAmount = comparedStep.PartAmounts.Where(p => string.Equals(p.Part.Name, partAmount.Part.Name)).FirstOrDefault();

                result = AreEqual(partAmount, correspondingPartAmount);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if ProcessStepAssemblyAmount are equals.
            foreach (ProcessStepAssemblyAmount assemblyAmount in step.AssemblyAmounts)
            {
                ProcessStepAssemblyAmount correspondingAssemblyAmount = comparedStep.AssemblyAmounts.Where(p => string.Equals(p.Assembly.Name, assemblyAmount.Assembly.Name)).FirstOrDefault();

                result = AreEqual(assemblyAmount, correspondingAssemblyAmount);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if ProcessTimeUnits are equal.
            result = AreEqual(step.ProcessTimeUnit, comparedStep.ProcessTimeUnit);
            if (!result)
            {
                return false;
            }

            // Verifies if SetupTimeUnits are equals.
            result = AreEqual(step.SetupTimeUnit, comparedStep.SetupTimeUnit);
            if (!result)
            {
                return false;
            }

            // Verifies if MaxDownTimeUnits are equals.
            result = AreEqual(step.MaxDownTimeUnit, comparedStep.MaxDownTimeUnit);
            if (!result)
            {
                return false;
            }

            // Verifies if cycle time calculations are equals.
            foreach (CycleTimeCalculation calculation in step.CycleTimeCalculations)
            {
                CycleTimeCalculation correspondingCalculation = comparedStep.CycleTimeCalculations.Where(c => string.Equals(c.Description, calculation.Description)).FirstOrDefault();

                result = AreEqual(calculation, correspondingCalculation);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if commodities of  steps are equal.
            foreach (Commodity commodity in step.Commodities)
            {
                Commodity correspondingCommodity = comparedStep.Commodities.Where(c => string.Equals(c.Name, commodity.Name)).FirstOrDefault();

                result = AreEqual(commodity, stepContext, correspondingCommodity, comparedStepContext);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if consumables of  steps are equal.
            foreach (Consumable consumable in step.Consumables)
            {
                Consumable correspondingConsumable = comparedStep.Consumables.Where(c => string.Equals(c.Name, consumable.Name)).FirstOrDefault();

                result = AreEqual(consumable, correspondingConsumable);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if machines of  steps are equal.
            foreach (Machine machine in step.Machines)
            {
                Machine correspondingMachine = comparedStep.Machines.Where(c => string.Equals(c.Name, machine.Name)).FirstOrDefault();

                result = AreEqual(machine, stepContext, correspondingMachine, comparedStepContext);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if dies of  steps are equal.
            foreach (Die die in step.Dies)
            {
                Die correspondingDie = comparedStep.Dies.Where(d => string.Equals(d.Name, die.Name)).FirstOrDefault();

                result = AreEqual(die, stepContext, correspondingDie, comparedStepContext);
                if (!result)
                {
                    return false;
                }
            }

            return result;
        }

        /// <summary>
        /// Compares two manufacturers
        /// </summary>
        /// <param name="manufacturer">The compared manufacturer</param>
        /// <param name="manufacturerToCompareWith">The manufacturer to compare with</param>
        /// <returns>True if manufacturers are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, IsMasterData.
        /// </remarks>
        private bool AreEqual(Manufacturer manufacturer, Manufacturer manufacturerToCompareWith)
        {
            if (manufacturer == null && manufacturerToCompareWith == null)
            {
                return true;
            }

            if ((manufacturer == null && manufacturerToCompareWith != null) || (manufacturer != null && manufacturerToCompareWith == null))
            {
                return false;
            }

            bool result = string.Equals(manufacturer.Name, manufacturerToCompareWith.Name) &&
                          string.Equals(manufacturer.Description, manufacturerToCompareWith.Description);

            return result;
        }

        /// <summary>
        /// Compares two cycle time calculations
        /// </summary>
        /// <param name="calculation">The compared cycle time calculation</param>
        /// <param name="calculationToCompareWith">The cycle time calculation to compare with</param>
        /// <returns>True if cycle time calculations are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, IsMasterData.
        /// </remarks>
        private bool AreEqual(CycleTimeCalculation calculation, CycleTimeCalculation calculationToCompareWith)
        {
            if (calculation == null && calculationToCompareWith == null)
            {
                return true;
            }

            if ((calculation == null && calculationToCompareWith != null) || (calculation != null && calculationToCompareWith == null))
            {
                return false;
            }

            bool result = string.Equals(calculation.Description, calculationToCompareWith.Description) &&
                          string.Equals(calculation.RawPartDescription, calculationToCompareWith.RawPartDescription) &&
                          string.Equals(calculation.ToleranceType, calculationToCompareWith.ToleranceType) &&
                          string.Equals(calculation.SurfaceType, calculationToCompareWith.SurfaceType) &&
                          calculation.MachiningVolume == calculationToCompareWith.MachiningVolume &&
                          calculation.MachiningType == calculationToCompareWith.MachiningType &&
                          calculation.TransportTimeFromBuffer == calculationToCompareWith.TransportTimeFromBuffer &&
                          calculation.ToolChangeTime == calculationToCompareWith.ToolChangeTime &&
                          calculation.TransportClampingTime == calculationToCompareWith.TransportClampingTime &&
                          calculation.MachiningTime == calculationToCompareWith.MachiningTime &&
                          calculation.TakeOffTime == calculationToCompareWith.TakeOffTime &&
                          calculation.Index == calculationToCompareWith.Index &&
                          calculation.MachiningCalculationData.SequenceEqual(calculationToCompareWith.MachiningCalculationData);

            return result;
        }

        /// <summary>
        /// Compares two process step part amounts
        /// </summary>
        /// <param name="partAmount">The compared process step part amount</param>
        /// <param name="partAmountToComapareWith">The process step part amount to compare with</param>
        /// <returns>True if process step part amounts are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, IsMasterData.
        /// </remarks>
        private bool AreEqual(ProcessStepPartAmount partAmount, ProcessStepPartAmount partAmountToComapareWith)
        {
            if (partAmount == null && partAmountToComapareWith == null)
            {
                return true;
            }

            if ((partAmount == null && partAmountToComapareWith != null) || (partAmount != null && partAmountToComapareWith == null))
            {
                return false;
            }

            bool result = partAmount.Amount == partAmountToComapareWith.Amount &&
                          string.Equals(partAmount.ProcessStep.Name, partAmountToComapareWith.ProcessStep.Name) &&
                          string.Equals(partAmount.Part.Name, partAmountToComapareWith.Part.Name);

            return result;
        }

        /// <summary>
        /// Compares two process step assembly amounts
        /// </summary>
        /// <param name="assemblyAmount">The compared process step assembly amount</param>
        /// <param name="assemblyAmountToComapareWith">The process step assembly amount to compare with</param>
        /// <returns>True if process step assembly amounts are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, IsMasterData.
        /// </remarks>
        private bool AreEqual(ProcessStepAssemblyAmount assemblyAmount, ProcessStepAssemblyAmount assemblyAmountToComapareWith)
        {
            if (assemblyAmount == null && assemblyAmountToComapareWith == null)
            {
                return true;
            }

            if ((assemblyAmount == null && assemblyAmountToComapareWith != null) || (assemblyAmount != null && assemblyAmountToComapareWith == null))
            {
                return false;
            }

            bool result = assemblyAmount.Amount == assemblyAmountToComapareWith.Amount &&
                          string.Equals(assemblyAmount.ProcessStep.Name, assemblyAmountToComapareWith.ProcessStep.Name) &&
                          string.Equals(assemblyAmount.Assembly.Name, assemblyAmountToComapareWith.Assembly.Name);

            return result;
        }

        /// <summary>
        /// Compares two process steps classifications
        /// </summary>
        /// <param name="classification">The compared process step classification</param>
        /// <param name="classificationToCompareWith">The process step classification to compare with</param>
        /// <returns>True if process step classifications are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: IsReleased, Guid.
        /// </remarks>
        private bool AreEqual(ProcessStepsClassification classification, ProcessStepsClassification classificationToCompareWith)
        {
            if (classification == null && classificationToCompareWith == null)
            {
                return true;
            }

            if ((classification == null && classificationToCompareWith != null) || (classification != null && classificationToCompareWith == null))
            {
                return false;
            }

            bool result = string.Equals(classification.Name, classificationToCompareWith.Name);

            return result;
        }

        /// <summary>
        /// Compares two machines classifications
        /// </summary>
        /// <param name="classification">The compared machine classification</param>
        /// <param name="classificationToCompareWith">The machine classification to compare with</param>
        /// <returns>True if machines classifications are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: IsReleased, Guid.
        /// </remarks>
        private bool AreEqual(MachinesClassification classification, MachinesClassification classificationToCompareWith)
        {
            if (classification == null && classificationToCompareWith == null)
            {
                return true;
            }

            if ((classification == null && classificationToCompareWith != null) || (classification != null && classificationToCompareWith == null))
            {
                return false;
            }

            bool result = string.Equals(classification.Name, classificationToCompareWith.Name);

            return result;
        }

        /// <summary>
        /// Compares two materials classifications 
        /// </summary>
        /// <param name="classification">The compared material classification</param>
        /// <param name="classificationToCompareWith">The material classification to compare with</param>
        /// <returns>True if materials classifications are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: IsReleased, Guid.
        /// </remarks>
        private bool AreEqual(MaterialsClassification classification, MaterialsClassification classificationToCompareWith)
        {
            if (classification == null && classificationToCompareWith == null)
            {
                return true;
            }

            if ((classification == null && classificationToCompareWith != null) || (classification != null && classificationToCompareWith == null))
            {
                return false;
            }

            bool result = string.Equals(classification.Name, classificationToCompareWith.Name);

            return result;
        }

        /// <summary>
        /// Compares two raw material delivery types
        /// </summary>
        /// <param name="deliveryType">The compared raw material delivery type</param>
        /// <param name="deliveryTypeToCompareWith">The raw material delivery type to compare with</param>
        /// <returns>True if raw material delivery types are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Guid.
        /// </remarks>
        private bool AreEqual(RawMaterialDeliveryType deliveryType, RawMaterialDeliveryType deliveryTypeToCompareWith)
        {
            if (deliveryType == null && deliveryTypeToCompareWith == null)
            {
                return true;
            }

            if ((deliveryType == null && deliveryTypeToCompareWith != null) || (deliveryType != null && deliveryTypeToCompareWith == null))
            {
                return false;
            }

            bool result = string.Equals(deliveryType.Name, deliveryTypeToCompareWith.Name);

            return result;
        }

        /// <summary>
        /// Verifies if a commodity was deleted from data base and all its sub objects were deleted too.
        /// </summary>
        /// <param name="commodity">The deleted commodity.</param>
        /// <param name="entityContext">The 'EntityContext' of deleted commodity.</param>
        /// <returns>True if commodity was fully deleted, false otherwise.</returns>
        public bool CommodityFullyDeleted(Commodity commodity, DbIdentifier entityContext)
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(entityContext);
            bool commodityNotDeleted = dataContext.CommodityRepository.CheckIfExists(commodity.Guid);
            if (commodityNotDeleted)
            {
                return false;
            }

            // Verifies if media was deleted.
            bool result = MediaDeleted(commodity.Media, entityContext);
            if (!result)
            {
                return false;
            }

            // Verifies if weight unit base was deleted
            result = MeasurementUnitFullyDeleted(commodity.WeightUnitBase, entityContext);
            if (!result)
            {
                return false;
            }

            // Verifies if manufacturer was deleted.
            result = ManufacturerFullyDeleted(commodity.Manufacturer, entityContext);

            return result;
        }

        /// <summary>
        /// Verifies if a consumable was deleted from data base and all its sub objects were deleted too.
        /// </summary>
        /// <param name="consumable">The deleted consumable.</param>
        /// <param name="entityContext">The 'EntityContext' of deleted consumable.</param>
        /// <returns>True if consumable was fully deleted, false otherwise.</returns>
        public bool ConsumableFullyDeleted(Consumable consumable, DbIdentifier entityContext)
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(entityContext);
            bool consumableExists = dataContext.ConsumableRepository.CheckIfExists(consumable.Guid);
            if (consumableExists)
            {
                return false;
            }

            // Verifies if manufacturer was deleted.
            bool result = ManufacturerFullyDeleted(consumable.Manufacturer, entityContext);
            if (!result)
            {
                return false;
            }

            // Verifies if price unit base was deleted
            result = MeasurementUnitFullyDeleted(consumable.PriceUnitBase, entityContext);

            return result;
        }

        /// <summary>
        /// Verifies if a currency was deleted from data base and all its sub objects were deleted too.
        /// </summary>
        /// <param name="currency">The deleted currency.</param>
        /// <param name="entityContext">The 'EntityContext' of deleted currency.</param>
        /// <returns>True if currency was fully deleted, false otherwise.</returns>
        public bool CurrencyFullyDeleted(Currency currency, DbIdentifier entityContext)
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(entityContext);
            bool currencyExists = dataContext.CurrencyRepository.CheckIfExists(currency.Guid);

            return !currencyExists;
        }

        /// <summary>
        /// Verifies if a die was deleted from data base and all its sub objects were deleted too.
        /// </summary>
        /// <param name="die">The deleted die.</param>
        /// <param name="entityContext">The 'EntityContext' of deleted die.</param>
        /// <returns>True if die was fully deleted, false otherwise.</returns>
        public bool DieFullyDeleted(Die die, DbIdentifier entityContext)
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(entityContext);
            bool dieExists = dataContext.DieRepository.CheckIfExists(die.Guid);
            if (dieExists)
            {
                return false;
            }

            // Verifies if media was deleted.
            bool result = MediaDeleted(die.Media, entityContext);
            if (!result)
            {
                return false;
            }

            // Verifies if manufacturer was deleted.
            result = ManufacturerFullyDeleted(die.Manufacturer, entityContext);

            return result;
        }

        /// <summary>
        /// Verifies if a machine was deleted from data base and all its sub objects were deleted too.
        /// </summary>
        /// <param name="machine">The deleted machine.</param>
        /// <param name="entityContext">The 'EntityContext' of deleted machine.</param>
        /// <returns>True if machine was fully deleted, false otherwise.</returns>
        public bool MachineFullyDeleted(Machine machine, DbIdentifier entityContext)
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(entityContext);
            bool machineExists = dataContext.MachineRepository.CheckIfExists(machine.Guid);
            if (machineExists)
            {
                return false;
            }

            // Verifies if media was deleted.
            bool result = MediaDeleted(machine.Media, entityContext);
            if (!result)
            {
                return false;
            }

            // Verifies if manufacturer was deleted.
            result = ManufacturerFullyDeleted(machine.Manufacturer, entityContext);

            return result;
        }

        /// <summary>
        /// Verifies if a media was deleted from data base.
        /// </summary>
        /// <param name="media">The deleted media.</param>
        /// <param name="entityContext">The 'EntityContext' of deleted media.</param>
        /// <returns>True if media was deleted, false otherwise.</returns>
        public bool MediaDeleted(Media media, DbIdentifier entityContext)
        {
            if (media == null)
            {
                return true;
            }

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(entityContext);
            bool mediaExists = dataContext.MediaRepository.CheckIfExists(media.Guid);

            return !mediaExists;
        }

        /// <summary>
        /// Verifies if a raw material was deleted from data base and all its sub objects were deleted too.
        /// </summary>
        /// <param name="material">The deleted raw material.</param>
        /// <param name="entityContext">The 'EntityContext' of deleted raw material.</param>
        /// <returns>True if raw material was fully deleted, false otherwise.</returns>
        public bool RawMaterialFullyDeleted(RawMaterial material, DbIdentifier entityContext)
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(entityContext);
            bool materialExists = dataContext.RawMaterialRepository.CheckIfExists(material.Guid);
            if (materialExists)
            {
                return false;
            }

            // Verifies if media was deleted.
            bool result = MediaDeleted(material.Media, entityContext);
            if (!result)
            {
                return false;
            }

            // Verifies if ParentWeightUnitBase was deleted
            result = MeasurementUnitFullyDeleted(material.ParentWeightUnitBase, entityContext);
            if (!result)
            {
                return false;
            }

            // Verifies if QuantityUnitBase was deleted
            result = MeasurementUnitFullyDeleted(material.QuantityUnitBase, entityContext);
            if (!result)
            {
                return false;
            }

            // Verifies if PriceUnitBase was deleted
            result = MeasurementUnitFullyDeleted(material.PriceUnitBase, entityContext);
            if (!result)
            {
                return false;
            }

            // Verifies if manufacturer was deleted.
            result = ManufacturerFullyDeleted(material.Manufacturer, entityContext);

            return result;
        }

        /// <summary>
        /// Verifies if a process step was deleted from data base and all its sub objects were deleted too.
        /// </summary>
        /// <param name="step">The deleted process step.</param>
        /// <param name="entityContext">The 'EntityContext' of deleted step.</param>
        /// <returns>True if process step was fully deleted, false otherwise.</returns>
        public bool ProcessStepFullyDeleted(ProcessStep step, DbIdentifier entityContext)
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(entityContext);
            bool stepExists = dataContext.ProcessStepRepository.CheckIfExists(step.Guid);
            if (stepExists)
            {
                return false;
            }

            // Verifies if media was deleted.
            bool result = MediaDeleted(step.Media, entityContext);
            if (!result)
            {
                return false;
            }

            // Verifies if CycleTimeUnit was deleted
            result = MeasurementUnitFullyDeleted(step.CycleTimeUnit, entityContext);
            if (!result)
            {
                return false;
            }

            // Verifies if ProcessTimeUnit was deleted
            result = MeasurementUnitFullyDeleted(step.ProcessTimeUnit, entityContext);
            if (!result)
            {
                return false;
            }

            // Verifies if SetupTimeUnit was deleted
            result = MeasurementUnitFullyDeleted(step.SetupTimeUnit, entityContext);
            if (!result)
            {
                return false;
            }

            // Verifies if MaxDowntimeUnit was deleted
            result = MeasurementUnitFullyDeleted(step.MaxDownTimeUnit, entityContext);
            if (!result)
            {
                return false;
            }

            // Verifies if machines were deleted.
            foreach (Machine machine in step.Machines)
            {
                result = MachineFullyDeleted(machine, entityContext);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if consumables were deleted.
            foreach (Consumable consumable in step.Consumables)
            {
                result = ConsumableFullyDeleted(consumable, entityContext);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if commodities were deleted.
            foreach (Commodity commodity in step.Commodities)
            {
                result = CommodityFullyDeleted(commodity, entityContext);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if dies were deleted.
            foreach (Die die in step.Dies)
            {
                result = DieFullyDeleted(die, entityContext);
                if (!result)
                {
                    return false;
                }
            }

            return result;
        }

        /// <summary>
        /// Verifies if a process was deleted from data base and all its sub objects were deleted too.
        /// </summary>
        /// <param name="process">The deleted process.</param>
        /// <param name="entityContext">The 'EntityContext' of deleted process.</param>
        /// <returns>True if process was fully deleted, false otherwise.</returns>
        public bool ProcessFullyDeleted(Process process, DbIdentifier entityContext)
        {
            if (process == null)
            {
                return true;
            }

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(entityContext);
            bool processExists = dataContext.ProcessRepository.CheckIfExists(process.Guid);
            bool result = true;
            if (processExists)
            {
                return false;
            }

            // Verifies if steps were deleted.
            foreach (ProcessStep step in process.Steps)
            {
                result = ProcessStepFullyDeleted(step, entityContext);
                if (!result)
                {
                    return false;
                }
            }

            return result;
        }

        /// <summary>
        /// Verifies if a measurement unit  was  deleted from data base and all its sub objects were deleted too.
        /// </summary>
        /// <param name="measurementUnit">The deleted measurement unit.</param>
        /// <param name="entityContext">The 'EntityContext' of deleted measurement unit.</param>
        /// <returns>True if measurement unit was fully deleted, false otherwise.</returns>
        public bool MeasurementUnitFullyDeleted(MeasurementUnit measurementUnit, DbIdentifier entityContext)
        {
            if (measurementUnit == null)
            {
                return true;
            }

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(entityContext);
            bool measurementUnitExists = dataContext.MeasurementUnitRepository.CheckIfExists(measurementUnit.Guid);

            return !measurementUnitExists;
        }

        /// <summary>
        /// Verifies if a user  was  deleted from data base and all its sub objects were deleted too.
        /// </summary>
        /// <param name="user">The deleted user.</param>
        /// <param name="entityContext">The 'EntityContext' of deleted user.</param>
        /// <returns>True if user was fully deleted, false otherwise.</returns>
        public bool UserFullyDeleted(User user, DbIdentifier entityContext)
        {
            if (user == null)
            {
                return true;
            }

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(entityContext);
            bool userExists = dataContext.UserRepository.CheckIfExists(user.Guid);

            return !userExists;
        }

        /// <summary>
        /// Verifies if an overhead setting was deleted from data base and all its sub objects were deleted too.
        /// </summary>
        /// <param name="overheadSetting">The deleted OH setting.</param>
        /// <param name="entityContext">The 'EntityContext' of deleted overhead setting.</param>
        /// <returns>True if overheadSettings was fully deleted, false otherwise.</returns>
        public bool OverheadSettingFullyDeleted(OverheadSetting overheadSetting, DbIdentifier entityContext)
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(entityContext);
            bool overheadSettingExists = dataContext.OverheadSettingsRepository.CheckIfExists(overheadSetting.Guid);

            return !overheadSettingExists;
        }

        /// <summary>
        /// Verifies if a customer  was  deleted from data base and all its sub objects were deleted too.
        /// </summary>
        /// <param name="customer">The deleted customer.</param>
        /// <param name="entityContext">The 'EntityContext' of deleted customer.</param>
        /// <returns>True if customer was fully deleted, false otherwise.</returns>
        public bool CustomerFullyDeleted(Customer customer, DbIdentifier entityContext)
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(entityContext);
            bool customerExists = dataContext.SupplierRepository.CheckIfExists(customer.Guid);

            return !customerExists;
        }

        /// <summary>
        /// Verifies if a country setting was  deleted from data base and all its sub objects were deleted too.
        /// </summary>
        /// <param name="countrySetting">The deleted country setting.</param>
        /// <param name="entityContext">The 'EntityContext' of deleted country setting.</param>
        /// <returns>True if countrySetting was fully deleted, false otherwise.</returns>
        public bool CountrySettingFullyDeleted(CountrySetting countrySetting, DbIdentifier entityContext)
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(entityContext);
            bool countrySettingExists = dataContext.EntityContext.CountrySettingSet.FirstOrDefault(c => c.Guid == countrySetting.Guid) != null;

            return !countrySettingExists;
        }

        /// <summary>
        /// Verifies if a country was deleted from data base and all its sub objects were deleted too.
        /// </summary>
        /// <param name="country">The deleted country.</param>
        /// <param name="entityContext">The 'EntityContext' of deleted country.</param>
        /// <returns>True if country was fully deleted, false otherwise.</returns>
        public bool CountryFullyDeleted(Country country, DbIdentifier entityContext)
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(entityContext);
            bool countryExists = dataContext.CountryRepository.CheckIfExists(country.Guid);
            if (countryExists)
            {
                return false;
            }

            // Verifies if country setting was deleted.
            bool result = CountrySettingFullyDeleted(country.CountrySetting, entityContext);
            if (!result)
            {
                return false;
            }

            // Verifies if country states were deleted.
            foreach (CountryState state in country.States)
            {
                result = CountryStateFullyDeleted(state, entityContext);
                if (!result)
                {
                    return false;
                }
            }

            return result;
        }

        /// <summary>
        /// Verifies if a manufacturer was deleted from data base and all its sub objects were deleted too.
        /// </summary>
        /// <param name="manufacturer">The deleted manufacturer.</param>
        /// <param name="entityContext">The 'EntityContext' of deleted manufacturer.</param>
        /// <returns>True if manufacturer was fully deleted, false otherwise.</returns>
        public bool ManufacturerFullyDeleted(Manufacturer manufacturer, DbIdentifier entityContext)
        {
            if (manufacturer == null)
            {
                return true;
            }

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(entityContext);
            bool manufacturerExists = dataContext.ManufacturerRepository.CheckIfExists(manufacturer.Guid);

            return !manufacturerExists;
        }

        /// <summary>
        /// Verifies if a country state was deleted from data base and all its sub objects were deleted too.
        /// </summary>
        /// <param name="countryState">The deleted country state.</param>
        /// <param name="entityContext">The 'EntityContext' of deleted country state.</param>
        /// <returns>True if countryState was fully deleted, false otherwise.</returns>
        public bool CountryStateFullyDeleted(CountryState countryState, DbIdentifier entityContext)
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(entityContext);
            bool countryStateExists = dataContext.EntityContext.CountryStateSet.FirstOrDefault(c => c.Guid == countryState.Guid) != null;

            return !countryStateExists;
        }

        /// <summary>
        /// Verifies if a process steps classification was deleted from data base and all its sub objects were deleted too.
        /// </summary>
        /// <param name="classification">The deleted classification.</param>
        /// <param name="entityContext">The 'EntityContext' of deleted classification.</param>
        /// <returns>True if classification was fully deleted, false otherwise.</returns>
        public bool ProcessStepsClassificationFullyDeleted(ProcessStepsClassification classification, DbIdentifier entityContext)
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(entityContext);
            bool classificationExists = dataContext.ProcessStepsClassificationRepository.CheckIfExists(classification.Guid);
            if (classificationExists)
            {
                return false;
            }

            // Verifies if classification children were deleted.
            bool result = true;
            foreach (ProcessStepsClassification child in classification.Children)
            {
                result = ProcessStepsClassificationFullyDeleted(child, entityContext);
                if (!result)
                {
                    return false;
                }
            }

            return result;
        }

        /// <summary>
        /// Verifies if a material classification was deleted from data base and all its sub objects were deleted too.
        /// </summary>
        /// <param name="classification">The deleted classification.</param>
        /// <param name="entityContext">The 'EntityContext' of deleted classification.</param>
        /// <returns>True if classification was fully deleted, false otherwise.</returns>
        public bool MaterialsClassificationFullyDeleted(MaterialsClassification classification, DbIdentifier entityContext)
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(entityContext);
            bool classificationExists = dataContext.MaterialsClassificationRepository.CheckIfExists(classification.Guid);
            bool result = true;
            if (classificationExists)
            {
                return false;
            }

            // Verifies if classification children were deleted.
            foreach (MaterialsClassification child in classification.Children)
            {
                result = MaterialsClassificationFullyDeleted(child, entityContext);
                if (!result)
                {
                    return false;
                }
            }

            return result;
        }

        /// <summary>
        /// Verifies if a machine classification was deleted from data base and all its sub objects were deleted too.
        /// </summary>
        /// <param name="classification">The deleted classification.</param>
        /// <param name="entityContext">The 'EntityContext' of deleted classification.</param>
        /// <returns>True if classification was fully deleted, false otherwise.</returns>
        public bool MachinesClassificationFullyDeleted(MachinesClassification classification, DbIdentifier entityContext)
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(entityContext);
            bool classificationExists = dataContext.EntityContext.MachinesClassificationSet.Where(cls => cls.Guid == classification.Guid).Count() > 0;
            if (classificationExists)
            {
                return false;
            }

            // Verifies if classification children were deleted.
            bool result = true;
            foreach (MachinesClassification child in classification.Children)
            {
                result = MachinesClassificationFullyDeleted(child, entityContext);
                if (!result)
                {
                    return false;
                }
            }

            return result;
        }

        /// <summary>
        /// Verifies if a part was deleted from data base and all its sub objects were deleted too.
        /// </summary>
        /// <param name="part">The deleted part.</param>
        /// <param name="entityContext">The 'EntityContext' of deleted part.</param>
        /// <returns>True if part was fully deleted, false otherwise.</returns>
        public bool PartFullyDeleted(Part part, DbIdentifier entityContext)
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(entityContext);
            bool partExists = dataContext.PartRepository.CheckIfExists(part.Guid);
            if (partExists)
            {
                return false;
            }

            // Verifies if Manufacturer was deleted.
            bool result = ManufacturerFullyDeleted(part.Manufacturer, entityContext);
            if (!result)
            {
                return false;
            }

            // Verifies if Calculator was deleted
            result = UserFullyDeleted(part.CalculatorUser, entityContext);
            if (!result)
            {
                return false;
            }

            // Verifies if country setting was deleted. 
            result = CountrySettingFullyDeleted(part.CountrySettings, entityContext);
            if (!result)
            {
                return false;
            }

            // Verifies if overhead setting was deleted.
            result = OverheadSettingFullyDeleted(part.OverheadSettings, entityContext);
            if (!result)
            {
                return false;
            }

            // Verifies if process was deleted.
            result = ProcessFullyDeleted(part.Process, entityContext);
            if (!result)
            {
                return false;
            }

            // Verifies if media of part were deleted.
            foreach (Media media in part.Media)
            {
                result = MediaDeleted(media, entityContext);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if raw materials were deleted.
            foreach (RawMaterial material in part.RawMaterials)
            {
                result = RawMaterialFullyDeleted(material, entityContext);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if commodities were deleted.
            foreach (Commodity commmodity in part.Commodities)
            {
                result = CommodityFullyDeleted(commmodity, entityContext);
                if (!result)
                {
                    return false;
                }
            }

            return result;
        }

        /// <summary>
        /// Verifies if an assembly was deleted from data base and all its sub objects were deleted too.
        /// </summary>
        /// <param name="assembly">The deleted assembly.</param>
        /// <param name="entityContext">The 'EntityContext' of deleted assembly.</param>
        /// <returns>True if assembly was fully deleted, false otherwise.</returns>
        public bool AssemblyFullyDeleted(Assembly assembly, DbIdentifier entityContext)
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(entityContext);
            bool assemblyExists = dataContext.AssemblyRepository.CheckIfExists(assembly.Guid);
            if (assemblyExists)
            {
                return false;
            }

            // Verifies if Manufacturer was deleted.
            bool result = ManufacturerFullyDeleted(assembly.Manufacturer, entityContext);
            if (!result)
            {
                return false;
            }

            // Verifies if country setting was deleted. 
            result = CountrySettingFullyDeleted(assembly.CountrySettings, entityContext);
            if (!result)
            {
                return false;
            }

            // Verifies if overhead setting was deleted.
            result = OverheadSettingFullyDeleted(assembly.OverheadSettings, entityContext);
            if (!result)
            {
                return false;
            }

            // Verifies if process was deleted.
            result = ProcessFullyDeleted(assembly.Process, entityContext);
            if (!result)
            {
                return false;
            }

            // Verifies if medias were deleted.
            foreach (Media media in assembly.Media)
            {
                result = MediaDeleted(media, entityContext);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if parts were deleted.
            foreach (Part part in assembly.Parts)
            {
                result = PartFullyDeleted(part, entityContext);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if subassemblies were deleted.
            foreach (Assembly subassembly in assembly.Subassemblies)
            {
                result = AssemblyFullyDeleted(subassembly, entityContext);
                if (!result)
                {
                    return false;
                }
            }

            return result;
        }

        /// <summary>
        /// Verifies if a project was deleted from data base and all its sub objects were deleted too.
        /// </summary>
        /// <param name="project">The deleted project.</param>
        /// <param name="entityContext">The 'EntityContext' of deleted project.</param>
        /// <returns>True if project was fully deleted, false otherwise.</returns>
        public bool ProjectFullyDeleted(Project project, DbIdentifier entityContext)
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(entityContext);
            bool projectExists = dataContext.ProjectRepository.CheckIfExists(project.Guid);
            if (projectExists)
            {
                return false;
            }

            // Verifies if Customer was deleted.
            bool result = CustomerFullyDeleted(project.Customer, entityContext);
            if (!result)
            {
                return false;
            }

            // Verifies if ProjectLeader was deleted
            result = UserFullyDeleted(project.ProjectLeader, entityContext);
            if (!result)
            {
                return false;
            }

            // Verifies if ResponsibleCalculator was deleted
            result = UserFullyDeleted(project.ResponsibleCalculator, entityContext);
            if (!result)
            {
                return false;
            }

            // Verifies if overhead setting was deleted.
            result = OverheadSettingFullyDeleted(project.OverheadSettings, entityContext);
            if (!result)
            {
                return false;
            }

            // Verifies if medias were deleted.
            foreach (Media media in project.Media)
            {
                result = MediaDeleted(media, entityContext);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if parts were deleted.
            foreach (Part part in project.Parts)
            {
                result = PartFullyDeleted(part, entityContext);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if subassemblies were deleted.
            foreach (Assembly assembly in project.Assemblies)
            {
                result = AssemblyFullyDeleted(assembly, entityContext);
                if (!result)
                {
                    return false;
                }
            }

            return result;
        }

        #endregion Helpers
    }
}
