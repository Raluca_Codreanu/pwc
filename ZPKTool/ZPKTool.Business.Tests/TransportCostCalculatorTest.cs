﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Business.Resources;
using ZPKTool.Business.TransportCostCalculator;

namespace ZPKTool.Business.Tests
{
    /// <summary>
    /// This is a test class for the TransportCostCalculator class, with unit tests for business logic.
    /// </summary>
    [TestClass]
    public class TransportCostCalculatorTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TransportCostCalculatorTest"/> class.
        /// </summary>
        public TransportCostCalculatorTest()
        {
        }

        #region Get Carriers Test

        /// <summary>
        /// Test the number of carriers returned by the method GetCarriers().
        /// </summary>
        [TestMethod]
        public void CountNumberOfCarriers()
        {
            ICollection<Carrier> carriers = TransportCostCalculator.TransportCostCalculator.GetCarriers();
            Assert.AreEqual(4, carriers.Count, "The method did not returned 4 objects.");
        }

        /// <summary>
        /// Test to check random some values for the carriers returned by the method GetCarriers().
        /// </summary>
        [TestMethod]
        public void CheckCarriers()
        {
            ICollection<Carrier> carriers = TransportCostCalculator.TransportCostCalculator.GetCarriers();
            Assert.AreEqual(LocalizedResources.CarrierType_Manual, carriers.ElementAt(0).Description, "The description for carrier with id 0 is incorrect.");
            Assert.AreEqual(85m, carriers.ElementAt(1).NetWeight, "The net weight for carrier with id 1 is incorrect.");
            Assert.AreEqual(800m, carriers.ElementAt(2).Length, "The length for carrier with id 2 is incorrect.");
            Assert.AreEqual(1000m, carriers.ElementAt(3).LoadCapacity, "The load capacity for carrier with id 3 is incorrect.");
        }

        #endregion

        #region Get Transporters Test

        /// <summary>
        /// Test the number of transporters returned by the method GetTransporters().
        /// </summary>
        [TestMethod]
        public void CountNumberOfTransporters()
        {
            ICollection<Transporter> transporters = TransportCostCalculator.TransportCostCalculator.GetTransporters();
            Assert.AreEqual(6, transporters.Count, "The method did not returned 6 objects.");
        }

        /// <summary>
        /// Test to check random some values for the transporters returned by the method GetTransporters().
        /// </summary>
        [TestMethod]
        public void CheckTransporters()
        {
            ICollection<Transporter> transporters = TransportCostCalculator.TransportCostCalculator.GetTransporters();
            Assert.AreEqual(LocalizedResources.TransporterType_Manual, transporters.ElementAt(0).Description, "The description for transporter with id 0 is incorrect.");
            Assert.AreEqual(13600m, transporters.ElementAt(1).Length, "The length for transporter with id 1 is incorrect.");
            Assert.AreEqual(2450m, transporters.ElementAt(2).Width, "The width for transporter with id 2 is incorrect.");
            Assert.AreEqual(2500m, transporters.ElementAt(3).Height, "The height for transporter with id 3 is incorrect.");
            Assert.AreEqual(15, transporters.ElementAt(4).ParkingSlots, "The parking slots for transporter with id 4 is incorrect.");
            Assert.AreEqual(1.5m, transporters.ElementAt(5).MaxLoadCapacity, "The max load capacity for transporter with id 5 is incorrect.");
        }

        #endregion

        #region Calculator Test

        /// <summary>
        /// A test method for checking some values calculated into transport cost calculator.
        /// The tested values are: filling degree transporter, max theo and parts per carrier.
        /// </summary>
        [TestMethod]
        public void CalculateIntermediateCostsTest()
        {
            var inputData = new TransportCostCalculationInputData();
            inputData.PartWeight = 2m;
            inputData.Distance = 250m;
            inputData.DaysPerYear = 245;
            inputData.DailyNeededQty = 5;
            inputData.PartType = PartType.SingleComponent;
            inputData.PackagingType = PackagingType.Multipack;
            inputData.CostByPackage = 0.05m;
            inputData.PackageWeight = 0.6m;
            inputData.BouncingBoxHeight = 200m;
            inputData.BouncingBoxLength = 300m;
            inputData.BouncingBoxWidth = 200m;
            inputData.Interference = 0.3m;
            inputData.CarrierLength = 1200m;
            inputData.CarrierHeight = 500m;
            inputData.CarrierWidth = 800m;
            inputData.NetWeight = 85m;
            inputData.MaxPackageCarrier = 3;
            inputData.TransporterHeight = 2200m;
            inputData.TransporterLength = 4000m;
            inputData.TransporterWidth = 2000m;
            inputData.CostPerKm = 0.7m;

            decimal fillingDegreeTransporter = 1m;
            decimal maxTheo = 35m;
            decimal partsPerCarrier = 42m;

            var calculator = new TransportCostCalculator.TransportCostCalculator();

            var actualResult = calculator.CalculateTransportCost(inputData);

            Assert.AreEqual(fillingDegreeTransporter, actualResult.FillingDegreeTransporter, "The filling degree transporter is incorrect");
            Assert.AreEqual(maxTheo, actualResult.Maxtheo, "The max theoretical value is incorrect");
            Assert.AreEqual(partsPerCarrier, actualResult.PartsPerCarrier, "The parts per carrier is incorrect");
        }

        /// <summary>
        /// A test method for checking the final results: the annual transport and packaging costs.
        /// </summary>
        [TestMethod]
        public void CalculateAnnualCostsTest()
        {
            var inputData = new TransportCostCalculationInputData();
            inputData.PartWeight = 0.5m;
            inputData.Distance = 1200m;
            inputData.DaysPerYear = 230;
            inputData.DailyNeededQty = 13;
            inputData.PartType = PartType.BulkGoods;
            inputData.PackagingType = PackagingType.IndividualPacking;
            inputData.CostByPackage = 0.6m;
            inputData.PackageWeight = 0.9m;
            inputData.BouncingBoxHeight = 4000m;
            inputData.BouncingBoxLength = 5000m;
            inputData.BouncingBoxWidth = 3000m;
            inputData.PackingDensity = 0.78m;
            inputData.CarrierLength = 2000m;
            inputData.CarrierHeight = 1000m;
            inputData.CarrierWidth = 1000m;
            inputData.NetWeight = 100m;
            inputData.MaxPackageCarrier = 100;
            inputData.TransporterHeight = 2100m;
            inputData.TransporterLength = 3600m;
            inputData.TransporterWidth = 4500m;
            inputData.CostPerKm = 1m;

            decimal packagingCost = 1794m;
            decimal packagingCostPerPcs = 0.6m;
            decimal transportCost = 71760m;
            decimal transportCostPerPcs = 24m;

            var calculator = new TransportCostCalculator.TransportCostCalculator();

            var actualResult = calculator.CalculateTransportCost(inputData);

            // The actual results are rounded to maximum number of decimals because of the results precision.
            Assert.AreEqual(packagingCost, Math.Round(actualResult.PackagingCost, 8), "The packaging cost is incorrect");
            Assert.AreEqual(packagingCostPerPcs, Math.Round(actualResult.PackagingCostPerPcs, 8), "The packaging cost per pieces is incorrect");
            Assert.AreEqual(transportCost, Math.Round(actualResult.TransportCost, 8), "The transport cost is incorrect");
            Assert.AreEqual(transportCostPerPcs, Math.Round(actualResult.TransportCostPerPcs, 8), "The transport cost per pieces is incorrect");
        }

        /// <summary>
        /// A test to check if the parts per carrier value is correctly calculated, according to the selected part type.
        /// </summary>
        [TestMethod]
        public void SelectedPartTypeTest()
        {
            var inputData = new TransportCostCalculationInputData();
            inputData.DaysPerYear = 245;
            inputData.DailyNeededQty = 10;
            inputData.BouncingBoxHeight = 230m;
            inputData.BouncingBoxLength = 340m;
            inputData.BouncingBoxWidth = 450m;
            inputData.CarrierLength = 2300m;
            inputData.CarrierHeight = 3400m;
            inputData.CarrierWidth = 4500m;
            inputData.PackingDensity = 0.7m;
            inputData.Interference = 0.2m;

            var calculator = new TransportCostCalculator.TransportCostCalculator();

            inputData.PartType = PartType.SingleComponent;

            var actualResult1 = calculator.CalculateTransportCost(inputData);
            Assert.AreEqual(960m, actualResult1.PartsPerCarrier, "The parts per carrier value for single component is incorrect.");

            inputData.PartType = PartType.BulkGoods;

            var actualResult2 = calculator.CalculateTransportCost(inputData);
            Assert.AreEqual(700m, actualResult2.PartsPerCarrier, "The parts per carrier value for bulk goods is incorrect.");
        }

        /// <summary>
        /// A test to check if dividing by 0 is skipped, to avoid incorrect values.
        /// </summary>
        [TestMethod]
        public void DivideByZeroTest()
        {
            var inputData = new TransportCostCalculationInputData();
            inputData.BouncingBoxHeight = 0m;
            inputData.BouncingBoxLength = 2300m;
            inputData.BouncingBoxWidth = 3000m;
            inputData.CarrierHeight = 2000m;
            inputData.CarrierLength = 2500m;
            inputData.CarrierWidth = 3000m;
            inputData.Interference = 0.6m;
            inputData.PartType = PartType.SingleComponent;

            var calculator = new TransportCostCalculator.TransportCostCalculator();

            var actualResult = calculator.CalculateTransportCost(inputData);

            Assert.AreEqual(0m, actualResult.PartsPerCarrier, "The parts per carrier value is incorrect.");
            Assert.AreEqual(0m, actualResult.PackageCarrierPerPackaging, "The package carrier per packaging value is incorrect.");
        }

        #endregion
    }
}
