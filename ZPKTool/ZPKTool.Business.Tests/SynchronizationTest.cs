﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using ZPKTool.Common;

namespace ZPKTool.Business.Tests
{
    [TestClass]
    public class SynchronizationTest
    {
        /// <summary>
        /// The Guid of the user created during SynchronizeUserTest()
        /// </summary>
        private Guid sychUserTestGuid = Guid.NewGuid();

        public SynchronizationTest()
        {
            Utils.ConfigureDbAccess();
        }

        //Use TestInitialize to run code before running each test 
        [TestInitialize()]
        public void SynchronizationTestInitialize()
        {
            SynchronizeUserTestCleanup();
        }

        //Use TestCleanup to run code after each test has run
        [TestCleanup()]
        public void SynchronizationTestCleanup()
        {
            SynchronizeUserTestCleanup();
        }

        //[TestMethod]
        //public void SynchronizeUserBeforeLoginTest()
        //{
        //    ContextManager centralContext = PersistenceFactory.GetCentralContextManager();
        //    ContextManager localContext = PersistenceFactory.GetLocalContextManager();
        //    SynchronizationManager synchManager = new SynchronizationManager();

        //    User centralUser = new User();
        //    centralUser.Guid = this.sychUserTestGuid;
        //    centralUser.Name = "test";
        //    centralUser.Username = "test";
        //    centralUser.Password = EncryptionManager.Instance.EncodeMD5("test");
        //    centralUser.Description = "test";
        //    centralUser.Disabled = false;
        //    List<Role> centralRoles = centralContext.RolesPersistence.GetAll<Role>();
        //    centralUser.Roles.Add(centralRoles.First());
        //    List<Country> centralCountries = centralContext.CountryPersistence.GetAll<Country>();
        //    centralUser.FavoriteCountry = centralCountries.ElementAt(centralCountries.Count / 2);

        //    centralContext.UsersPersistence.Add(centralUser);
        //    centralContext.CommitChanges();

        //    synchManager.SynchronizeUserBeforeLogin(centralUser.Username);

        //    User localUser = localContext.UsersPersistence.GetByGuid(centralUser.Guid, true, ZPKTool.Persistence.Constants.DefaultMergeOption, LoadOption.FullDepth);

        //    Assert.IsNotNull(localUser, "Synchronizing a user that did not exist in the local db failed.");

        //    centralUser.Roles.Add(centralRoles.ElementAt(1));
        //    centralUser.Roles.Add(centralRoles.ElementAt(2));
        //    centralContext.CommitChanges();
        //    synchManager.SynchronizeUserBeforeLogin(centralUser.Username);

        //    bool success = localUser.Roles.Where(r => r.Guid == centralRoles.ElementAt(1).Guid).FirstOrDefault() != null &&
        //        localUser.Roles.Where(r => r.Guid == centralRoles.ElementAt(2).Guid).FirstOrDefault() != null;
        //    Assert.IsTrue(success, "Synchronizing a user with new roles has failed.");

        //    centralUser.Roles.Remove(centralRoles.ElementAt(2));
        //    centralContext.CommitChanges();
        //    synchManager.SynchronizeUserBeforeLogin(centralUser.Username);

        //    bool success2 = localUser.Roles.Where(r => r.Guid == centralRoles.ElementAt(2).Guid).FirstOrDefault() == null;
        //    Assert.IsTrue(success2, "Synchronizing a user with deleted roles has failed.");

        //    centralUser.FavoriteCountry = centralCountries.ElementAt(15);
        //    centralContext.CommitChanges();
        //    synchManager.SynchronizeUserBeforeLogin(centralUser.Username);

        //    bool success3 = (localUser.FavoriteCountry == null && centralUser.FavoriteCountry == null) ||
        //        (localUser.FavoriteCountry.Guid == centralUser.FavoriteCountry.Guid);
        //    Assert.IsTrue(success3, "Synchronizing a user's favourite country from central to local has failed.");
        //}

        private void SynchronizeUserTestCleanup()
        {
            //ContextManager centralContext = PersistenceFactory.GetCentralContextManager();
            //ContextManager localContext = PersistenceFactory.GetLocalContextManager();

            //User centralUser = centralContext.UsersPersistence.GetByGuid(this.sychUserTestGuid, true, ZPKTool.Persistence.Constants.DefaultMergeOption, LoadOption.FullDepth);
            //if (centralUser != null)
            //{
            //    centralContext.UsersPersistence.Delete(centralUser);
            //    centralContext.CommitChanges();
            //}

            //User localUser = localContext.UsersPersistence.GetByGuid(this.sychUserTestGuid, true, ZPKTool.Persistence.Constants.DefaultMergeOption, LoadOption.FullDepth);
            //if (localUser != null)
            //{
            //    localContext.UsersPersistence.Delete(localUser);
            //    localContext.CommitChanges();
            //}
        }
    }
}
