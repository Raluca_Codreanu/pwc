﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Business.Search;
using ZPKTool.Data;
using ZPKTool.Common;
using ZPKTool.DataAccess;

namespace ZPKTool.Business.Tests
{
    /// <summary>
    /// This is a test class for SearchManager and is intended
    /// to contain  Unit Tests for all public methods from SearchManager
    /// </summary>
    [TestClass]
    public class SearchManagerTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SearchManagerTest"/> class.
        /// </summary>
        public SearchManagerTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run. 
        // If a user is logged in the application, it should be logged out otherwise will influence other tests.
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if (SecurityManager.Instance.CurrentUser != null)
            {
                SecurityManager.Instance.Logout();
            }
        }

        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// A test for the method SearchEntities[T](string searchText, SearchScopeFilter scopeFilters, out List[T] localData, out List[T] centralData, ref bool stopSearch)
        /// using a null text as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SearchEntitiesGivenNullText()
        {
            var searchParameters = new SimpleSearchParameters() { SearchText = null, ScopeFilters = SearchScopeFilter.MyProjects };
            bool stopSearch = false;

            var result = SearchManager.Instance.SearchEntities<Project>(searchParameters, ref stopSearch);
        }

        /// <summary>
        /// A test for the method SearchEntities[T](string searchText, SearchScopeFilter scopeFilters, out List[T] localData, out List[T] centralData, ref bool stopSearch)
        /// using an empty string as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SearchEntitiesGivenEmptyText()
        {
            var searchParameters = new SimpleSearchParameters() { SearchText = string.Empty, ScopeFilters = SearchScopeFilter.MyProjects };
            bool stopSearch = false;

            var result = SearchManager.Instance.SearchEntities<Project>(searchParameters, ref stopSearch);
        }

        /// <summary>
        /// A test for the method SearchEntities[T](string searchText, SearchScopeFilter scopeFilters, out List[T] localData, out List[T] centralData, ref bool stopSearch)
        /// using None SearchScopeFilter.
        /// </summary>
        [TestMethod]
        public void SearchEntitiesGivenNoneSearchScopeFilter()
        {
            bool stopSearch = false;
            string searchedText = EncryptionManager.Instance.GenerateRandomString(5, true);

            // Set the current user to an admin user
            User user = new User();
            user.Name = EncryptionManager.Instance.GenerateRandomString(5, true);
            user.Username = EncryptionManager.Instance.GenerateRandomString(5, true);
            user.Password = EncryptionManager.Instance.HashSHA256("Password.", user.Salt, user.Guid.ToString());
            user.Roles = Role.Admin;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            user.Roles = Role.Admin;
            dataContext.UserRepository.Add(user);
            dataContext.SaveChanges();
            SecurityManager.Instance.Login(user.Username, "Password.");

            var searchParameters = new SimpleSearchParameters() { SearchText = searchedText, ScopeFilters = SearchScopeFilter.None };
            var result = SearchManager.Instance.SearchEntities<Project>(searchParameters, ref stopSearch);
            Assert.IsTrue(result.LocalData.Count == 0, "The search operation has been perform in local db even if no filter was specified");
            Assert.IsTrue(result.CentralData.Count == 0, "The search operation has been perform in central db even if no filter was specified");
        }

        /// <summary>
        /// A test for the method <see cref="SearchEntities[T]"/> 
        /// using valid data as input data.
        /// </summary>
        [TestMethod]
        public void SearchProjects()
        {
            bool stopSearch = false;
            string searchedText = "*";

            var localContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var centralContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            var project1 = new Project();
            project1.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            project1.OverheadSettings = new OverheadSetting();

            // Create a project and set IsDeleted flag to true.
            var project2 = new Project();
            project2.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            project2.OverheadSettings = new OverheadSetting();
            project2.IsDeleted = true;

            // Create a project and set IsReleased flag to true.
            var project3 = new Project();
            project3.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            project3.OverheadSettings = new OverheadSetting();
            project3.IsReleased = true;

            // Create a user with admin rights which will be the owner of project1 and project2.
            var user1Local = new User();
            user1Local.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            user1Local.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            user1Local.Password = EncryptionManager.Instance.HashSHA256("Password.", user1Local.Salt, user1Local.Guid.ToString());

            user1Local.Roles = Role.User;

            project1.SetOwner(user1Local);
            project2.SetOwner(user1Local);

            localContext.ProjectRepository.Add(project1);
            localContext.ProjectRepository.Add(project2);
            localContext.ProjectRepository.Add(project3);
            localContext.SaveChanges();

            // Create a project that belongs to another user and set it's ResponsibleCalculator to the one of the user who makes the search.
            var project4 = new Project();
            project4.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            project4.OverheadSettings = new OverheadSetting();

            var user1Central = new User();
            user1Central.Guid = user1Local.Guid;
            user1Central.Name = user1Local.Name;
            user1Central.Username = user1Local.Username;
            user1Central.Password = user1Local.Password;

            user1Central.Roles = user1Local.Roles;

            project4.ResponsibleCalculator = user1Central;

            // Create a user which will be the owner of project4.
            var user2 = new User();
            user2.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            user2.Username = EncryptionManager.Instance.GenerateRandomString(5, true);
            user2.Password = EncryptionManager.Instance.HashSHA256("Password.", user2.Salt, user2.Guid.ToString());
            user2.Roles = Role.Admin;
            project4.SetOwner(user2);

            centralContext.ProjectRepository.Add(project4);
            centralContext.SaveChanges();

            // Login into the application with the admin user.
            SecurityManager.Instance.Login(user1Local.Username, "Password.");

            // Search for any project in MyProjects. Deleted projects should not be returned.
            var searchParameters = new SimpleSearchParameters() { SearchText = searchedText, ScopeFilters = SearchScopeFilter.MyProjects };
            var result = SearchManager.Instance.SearchEntities<Project>(searchParameters, ref stopSearch);
            var expectedResult1 = localContext.EntityContext.ProjectSet.Where(p => !p.IsDeleted &&
                                                                                  p.Owner != null &&
                                                                                  p.Owner.Guid == user1Local.Guid).ToList();

            Assert.AreEqual<int>(result.LocalData.Count, expectedResult1.Count, "Failed to search for projects in MyProjects");
            foreach (var project in expectedResult1)
            {
                var correspondingProject = result.LocalData.FirstOrDefault(p => p.Guid == project.Guid);
                if (correspondingProject == null)
                {
                    Assert.Fail("Failed to search projects in MyProjects - wrong projects were returned");
                }
            }

            // Search for any project in ReleasedProjects.
            result.ClearResults();
            searchParameters = new SimpleSearchParameters() { SearchText = searchedText, ScopeFilters = SearchScopeFilter.ReleasedProjects };
            result = SearchManager.Instance.SearchEntities<Project>(searchParameters, ref stopSearch);
            var expectedResult2 = localContext.EntityContext.ProjectSet.Where(p => !p.IsDeleted && p.IsReleased).ToList();

            Assert.AreEqual<int>(result.LocalData.Count, expectedResult2.Count, "Failed to search for projects in RelasedProjects");
            foreach (var project in expectedResult2)
            {
                var correspondingProject = result.LocalData.FirstOrDefault(p => p.Guid == project.Guid);
                if (correspondingProject == null)
                {
                    Assert.Fail("Failed to search projects in ReleasedProjects - wrong projects were returned");
                }
            }

            // Search for any project in OtherUsersProjects.
            result.ClearResults();
            searchParameters = new SimpleSearchParameters() { SearchText = searchedText, ScopeFilters = SearchScopeFilter.OtherUsersProjects };
            result = SearchManager.Instance.SearchEntities<Project>(searchParameters, ref stopSearch);
            var expectedResult3 = new List<Project>() { project4 };

            foreach (var project in expectedResult3)
            {
                var correspondingProject = result.CentralData.FirstOrDefault(p => p.Guid == project.Guid);
                if (correspondingProject == null)
                {
                    Assert.Fail("Failed to search projects in OtherUsersProjects - wrong projects were returned");
                }
            }

            // Search for any project in MasterData.
            result.ClearResults();
            searchParameters = new SimpleSearchParameters() { SearchText = searchedText, ScopeFilters = SearchScopeFilter.MasterData };
            result = SearchManager.Instance.SearchEntities<Project>(searchParameters, ref stopSearch);

            // Verify that no project was returned.
            Assert.IsTrue(result.CentralData.Count == 0 && result.LocalData.Count == 0, "Failed to search for projects in MasterData");

            // Search for all projects in all locations.
            result.ClearResults();
            searchParameters = new SimpleSearchParameters()
                    {
                        SearchText = searchedText,
                        ScopeFilters = SearchScopeFilter.MasterData | SearchScopeFilter.MyProjects | SearchScopeFilter.OtherUsersProjects | SearchScopeFilter.ReleasedProjects
                    };

            result = SearchManager.Instance.SearchEntities<Project>(searchParameters, ref stopSearch);

            // Projects from ReleasedProjects and MyProjects will be searched in local db.
            var expectedLocalResults = localContext.EntityContext.ProjectSet.Where(p => !p.IsDeleted &&
                                                                                            (p.IsReleased ||
                                                                                            (p.Owner != null &&
                                                                                             p.Owner.Guid == user1Local.Guid))).ToList();

            // The other users projects.
            var expectedCentralResults = new List<Project>(expectedResult3);

            // Verifies if returned results are correct.
            Assert.AreEqual<int>(expectedLocalResults.Count, result.LocalData.Count, "Failed to search for any project in all locations");
            foreach (var project in expectedLocalResults)
            {
                var correspondingProject = result.LocalData.FirstOrDefault(p => p.Guid == project.Guid);
                if (correspondingProject == null)
                {
                    Assert.Fail("Failed to search any project in all locations - wrong projects were returned");
                }
            }

            foreach (var project in expectedCentralResults)
            {
                var correspondingProject = result.CentralData.FirstOrDefault(p => p.Guid == project.Guid);
                if (correspondingProject == null)
                {
                    Assert.Fail("Failed to search any project in all locations - wrong projects were returned");
                }
            }
        }

        /// <summary>
        /// A test for the method <see cref="SearchEntities[T]"/> 
        /// using valid data as input data.
        /// </summary>
        [TestMethod]
        public void SearchAssemblies()
        {
            bool stopSearch = false;
            string searchedText = "*";

            var localContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var centralContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            // Create a released project with assemblies.
            var project1 = new Project();
            project1.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            project1.OverheadSettings = new OverheadSetting();
            project1.IsReleased = true;

            var assembly1 = new Assembly();
            assembly1.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            assembly1.OverheadSettings = new OverheadSetting();
            assembly1.CountrySettings = new CountrySetting();
            project1.Assemblies.Add(assembly1);

            var assembly2 = new Assembly();
            assembly2.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            assembly2.OverheadSettings = new OverheadSetting();
            assembly2.CountrySettings = new CountrySetting();
            assembly2.SetIsDeleted(true);
            project1.Assemblies.Add(assembly2);

            var user1Local = new User();
            user1Local.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            user1Local.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            user1Local.Password = EncryptionManager.Instance.HashSHA256("Password.", user1Local.Salt, user1Local.Guid.ToString());

            user1Local.Roles = Role.User;
            localContext.UserRepository.Add(user1Local);

            localContext.ProjectRepository.Add(project1);
            localContext.SaveChanges();

            // Create a project, with a assembly, that belongs to another user and set it's ResponsibleCalculator to the one of the user who makes the search.
            var project2 = new Project();
            project2.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            project2.OverheadSettings = new OverheadSetting();

            var assembly3 = new Assembly();
            assembly3.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            assembly3.OverheadSettings = new OverheadSetting();
            assembly3.CountrySettings = new CountrySetting();
            project2.Assemblies.Add(assembly3);

            var user2 = new User();
            user2.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            user2.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            user2.Password = EncryptionManager.Instance.HashSHA256("Password.", user2.Salt, user2.Guid.ToString());

            user2.Roles = Role.Admin;

            project2.SetOwner(user2);

            var user1Central = new User();
            user1Central.Guid = user1Local.Guid;
            user1Local.CopyValuesTo(user1Central);

            project2.ResponsibleCalculator = user1Central;

            centralContext.ProjectRepository.Add(project2);
            centralContext.SaveChanges();

            // Create a master data assembly.
            var assembly4 = new Assembly();
            assembly4.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            assembly4.OverheadSettings = new OverheadSetting();
            assembly4.CountrySettings = new CountrySetting();
            assembly4.IsMasterData = true;

            centralContext.AssemblyRepository.Add(assembly4);
            centralContext.SaveChanges();

            // Login into the application with the admin user.
            SecurityManager.Instance.Login(user1Local.Username, "Password.");

            // Search for Assemblies in MyProjects.
            var searchParameters = new SimpleSearchParameters() { SearchText = searchedText, ScopeFilters = SearchScopeFilter.MyProjects };
            var result = SearchManager.Instance.SearchEntities<Assembly>(searchParameters, ref stopSearch);
            var expectedResult1 = localContext.EntityContext.AssemblySet.Where(a => !a.IsDeleted && a.Owner != null && a.Owner.Guid == user1Local.Guid).ToList();

            Assert.AreEqual<int>(expectedResult1.Count, result.LocalData.Count, "Failed to search any assembly in MyProjects");
            foreach (var assembly in expectedResult1)
            {
                var correspondingAssembly = result.LocalData.FirstOrDefault(a => a.Guid == assembly.Guid);
                if (correspondingAssembly == null)
                {
                    Assert.Fail("Failed to search any assembly in MyProjects - wrong assemblies were returned");
                }
            }

            // Search for Assemblies in MasterData.
            result.ClearResults();
            searchParameters = new SimpleSearchParameters() { SearchText = searchedText, ScopeFilters = SearchScopeFilter.MasterData };
            result = SearchManager.Instance.SearchEntities<Assembly>(searchParameters, ref stopSearch);
            var expectedResult2 = centralContext.EntityContext.AssemblySet.Where(a => !a.IsDeleted && a.IsMasterData).ToList();

            Assert.AreEqual<int>(expectedResult2.Count, result.CentralData.Count, "Failed to search for any assembly in MasterData");
            foreach (var assembly in expectedResult2)
            {
                var correspondingAssembly = result.CentralData.FirstOrDefault(a => a.Guid == assembly.Guid);
                if (correspondingAssembly == null)
                {
                    Assert.Fail("Failed to search any assembly in MasterData - wrong assemblies were returned");
                }
            }

            // Search for Assemblies in OtherUsersProjects.
            result.ClearResults();
            searchParameters = new SimpleSearchParameters() { SearchText = searchedText, ScopeFilters = SearchScopeFilter.OtherUsersProjects };
            result = SearchManager.Instance.SearchEntities<Assembly>(searchParameters, ref stopSearch);
            var expectedResult3 = new List<Assembly>() { assembly3 };

            Assert.AreEqual<int>(expectedResult3.Count, result.CentralData.Count, "Failed to search for any assembly in OtherUsersProjects");
            foreach (var assembly in expectedResult3)
            {
                var correspondingAssembly = result.CentralData.FirstOrDefault(a => a.Guid == assembly.Guid);
                if (correspondingAssembly == null)
                {
                    Assert.Fail("Failed to search for any assembly in OtherUsersProjects");
                }
            }

            // Search for Assemblies into ReleasedProjects.
            result.ClearResults();
            searchParameters = new SimpleSearchParameters() { SearchText = searchedText, ScopeFilters = SearchScopeFilter.ReleasedProjects };
            result = SearchManager.Instance.SearchEntities<Assembly>(searchParameters, ref stopSearch);
            var expectedResult4 = localContext.EntityContext.AssemblySet.Where(a => !a.IsDeleted && a.Owner == null && !a.IsMasterData).ToList();

            Assert.AreEqual<int>(expectedResult4.Count, result.LocalData.Count, "Failed to search for assemblies into RelasedProjects");
            foreach (var assembly in expectedResult4)
            {
                var correspondingAssembly = result.LocalData.FirstOrDefault(a => a.Guid == assembly.Guid);
                if (correspondingAssembly == null)
                {
                    Assert.Fail("Failed to search for assemblies into ReleasedProjects - wrong assemblies were returned");
                }
            }

            // Search for Assemblies into all locations.
            result.ClearResults();
            searchParameters = new SimpleSearchParameters()
                        {
                            SearchText = searchedText,
                            ScopeFilters = SearchScopeFilter.MyProjects | SearchScopeFilter.MasterData | SearchScopeFilter.OtherUsersProjects | SearchScopeFilter.ReleasedProjects
                        };

            result = SearchManager.Instance.SearchEntities<Assembly>(searchParameters, ref stopSearch);

            // Assemblies from ReleasedProjects and MyProjects will be searched in local db.
            var expectedLocalResult = localContext.EntityContext.AssemblySet.Where(a => !a.IsDeleted &&
                                                                                             ((a.Owner == null && !a.IsMasterData) ||
                                                                                              (a.Owner != null && a.Owner.Guid == user1Local.Guid))).ToList();

            // Assemblies from MasterData will be searched in central db.
            var expectedCentralResult = centralContext.EntityContext.AssemblySet.Where(a => !a.IsDeleted && a.IsMasterData && a.Owner == null).ToList();

            // Add the assemblies that belong to other users.
            expectedCentralResult.AddRange(expectedResult3);

            Assert.AreEqual<int>(expectedLocalResult.Count, result.LocalData.Count, "Failed to search for assemblies in all locations");
            foreach (var assembly in expectedLocalResult)
            {
                var correspondingAssembly = result.LocalData.FirstOrDefault(a => a.Guid == assembly.Guid);
                if (correspondingAssembly == null)
                {
                    Assert.Fail("Failed to search for assemblies in all locations - wrong results were returned");
                }
            }

            Assert.AreEqual<int>(expectedCentralResult.Count, result.CentralData.Count, "Failed to search for assemblies in all locations");
            foreach (var assembly in expectedCentralResult)
            {
                var correspondingAssembly = result.CentralData.FirstOrDefault(a => a.Guid == assembly.Guid);
                if (correspondingAssembly == null)
                {
                    Assert.Fail("Failed to search for assemblies in all locations - wrong assemblies were returned");
                }
            }
        }

        /// <summary>
        /// A test for the method <see cref="SearchEntities[T]"/> 
        /// using valid data as input data.
        /// </summary>
        [TestMethod]
        public void SearchParts()
        {
            bool stopSearch = false;
            string searchedText = "*";

            var localContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var centralContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            // Create a released project with parts.
            var project1 = new Project();
            project1.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            project1.OverheadSettings = new OverheadSetting();
            project1.IsReleased = true;

            var part1 = new Part();
            part1.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            part1.OverheadSettings = new OverheadSetting();
            part1.CountrySettings = new CountrySetting();
            project1.Parts.Add(part1);

            var part2 = new Part();
            part2.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            part2.OverheadSettings = new OverheadSetting();
            part2.CountrySettings = new CountrySetting();
            part2.SetIsDeleted(true);
            project1.Parts.Add(part2);

            var user1Local = new User();
            user1Local.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            user1Local.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            user1Local.Password = EncryptionManager.Instance.HashSHA256("Password.", user1Local.Salt, user1Local.Guid.ToString());
            user1Local.Roles = Role.User;

            project1.SetOwner(user1Local);

            localContext.ProjectRepository.Add(project1);
            localContext.SaveChanges();

            // Create a project, with a part, that belongs to another user and set it's ResponsibleCalculator to the one of the user who makes the search.
            var project2 = new Project();
            project2.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            project2.OverheadSettings = new OverheadSetting();

            var part3 = new Part();
            part3.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            part3.OverheadSettings = new OverheadSetting();
            part3.CountrySettings = new CountrySetting();
            project2.Parts.Add(part3);

            var user2 = new User();
            user2.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            user2.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            user2.Password = EncryptionManager.Instance.HashSHA256("Password.", user2.Salt, user2.Guid.ToString());
            user2.Roles = Role.Admin;

            project2.SetOwner(user2);

            var user1Central = new User();
            user1Central.Guid = user1Local.Guid;
            user1Central.Name = user1Local.Name;
            user1Central.Username = user1Local.Username;
            user1Central.Password = user1Local.Password;
            user1Central.Roles = user1Local.Roles;

            project2.ResponsibleCalculator = user1Central;
            centralContext.ProjectRepository.Add(project2);
            centralContext.SaveChanges();

            // Create a master data part.
            var part4 = new Part();
            part4.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            part4.OverheadSettings = new OverheadSetting();
            part4.CountrySettings = new CountrySetting();
            part4.IsMasterData = true;

            centralContext.PartRepository.Add(part4);
            centralContext.SaveChanges();

            // Login into the application with admin user.
            SecurityManager.Instance.Login(user1Local.Username, "Password.");

            // Search for Parts into MyProjects.
            var searchParameters = new SimpleSearchParameters() { SearchText = searchedText, ScopeFilters = SearchScopeFilter.MyProjects };
            var result = SearchManager.Instance.SearchEntities<Part>(searchParameters, ref stopSearch);
            var expectedResult1 = localContext.EntityContext.PartSet.Where(p => !p.IsDeleted && p.Owner != null && p.Owner.Guid == user1Local.Guid).ToList();

            Assert.AreEqual<int>(expectedResult1.Count, result.LocalData.Count, "Failed to search any part into MyProjects");
            foreach (var part in expectedResult1)
            {
                var correspondingPart = result.LocalData.FirstOrDefault(p => p.Guid == part.Guid);
                if (correspondingPart == null)
                {
                    Assert.Fail("Failed to search any part into MyProjects - wrong parts were returned");
                }
            }

            // Search for Parts into MasterData.
            result.ClearResults();
            searchParameters = new SimpleSearchParameters() { SearchText = searchedText, ScopeFilters = SearchScopeFilter.MasterData };
            result = SearchManager.Instance.SearchEntities<Part>(searchParameters, ref stopSearch);
            List<Part> expectedResult2 = centralContext.EntityContext.PartSet.Where(p => !p.IsDeleted && p.IsMasterData).ToList();

            Assert.AreEqual<int>(expectedResult2.Count, result.CentralData.Count, "Failed to search any part into MasterData");
            foreach (var part in expectedResult2)
            {
                var correspondingPart = result.CentralData.FirstOrDefault(p => p.Guid == part.Guid);
                if (correspondingPart == null)
                {
                    Assert.Fail("Failed to search any part into MasterData - wrong parts were returned");
                }
            }

            // Search for Parts into OtherUsersProjects.
            result.ClearResults();
            searchParameters = new SimpleSearchParameters() { SearchText = searchedText, ScopeFilters = SearchScopeFilter.OtherUsersProjects };
            result = SearchManager.Instance.SearchEntities<Part>(searchParameters, ref stopSearch);
            var expectedResult3 = new List<Part>() { part3 };

            Assert.AreEqual<int>(expectedResult3.Count, result.CentralData.Count, "Failed to search for any part into OtherUsersProjects");
            foreach (var part in expectedResult3)
            {
                var correspondingPart = result.CentralData.FirstOrDefault(p => p.Guid == part.Guid);
                if (correspondingPart == null)
                {
                    Assert.Fail("Failed to search for any part into OtherUsersProjects - wrong parts were returned");
                }
            }

            // Search for Parts into ReleasedProjects.
            result.ClearResults();
            searchParameters = new SimpleSearchParameters() { SearchText = searchedText, ScopeFilters = SearchScopeFilter.ReleasedProjects };
            result = SearchManager.Instance.SearchEntities<Part>(searchParameters, ref stopSearch);
            var expectedResult4 = localContext.EntityContext.PartSet.Where(p => !p.IsDeleted && !(p is RawPart) && p.Owner == null && !p.IsMasterData).ToList();

            Assert.AreEqual<int>(expectedResult4.Count, result.LocalData.Count, "Failed to search for parts into ReleasedProjects");
            foreach (var part in expectedResult4)
            {
                var correspondingPart = result.LocalData.FirstOrDefault(p => p.Guid == part.Guid);
                if (correspondingPart == null)
                {
                    Assert.Fail("Failed to search for parts into ReleasedProjects");
                }
            }

            // Search for Parts into all locations.
            result.ClearResults();
            searchParameters = new SimpleSearchParameters()
            {
                SearchText = searchedText,
                ScopeFilters = SearchScopeFilter.MyProjects | SearchScopeFilter.ReleasedProjects | SearchScopeFilter.OtherUsersProjects | SearchScopeFilter.MasterData
            };

            result = SearchManager.Instance.SearchEntities<Part>(searchParameters, ref stopSearch);

            // The parts belonging to MyProjects and ReleasedProjects will be searched in local db.
            var expectedLocalResults = localContext.EntityContext.PartSet.Where(p => !p.IsDeleted
                                                                                && !(p is RawPart)
                                                                                && ((p.Owner == null && !p.IsMasterData)
                                                                                      || (p.Owner != null && p.Owner.Guid == user1Local.Guid))).ToList();

            // Assemblies from MasterData will be searched in central db.
            var expectedCentralResults = centralContext.EntityContext.PartSet.Where(p => !p.IsDeleted && !(p is RawPart) && p.IsMasterData && p.Owner == null).ToList();

            // Add the assemblies that belong to other users.
            expectedCentralResults.AddRange(expectedResult3);

            Assert.AreEqual<int>(expectedLocalResults.Count, result.LocalData.Count, "Failed to search for parts in all locations");
            foreach (var part in expectedLocalResults)
            {
                var correspondingPart = result.LocalData.FirstOrDefault(p => p.Guid == part.Guid);
                if (correspondingPart == null)
                {
                    Assert.Fail("Failed to search for parts in all locations - wrong parts were returned");
                }
            }

            Assert.AreEqual<int>(expectedCentralResults.Count, result.CentralData.Count, "Failed to search for parts in all locations");
            foreach (var part in expectedCentralResults)
            {
                var correspondingPart = result.CentralData.FirstOrDefault(p => p.Guid == part.Guid);
                if (correspondingPart == null)
                {
                    Assert.Fail("Failed to search for parts in all locations - wrong parts were returned");
                }
            }
        }

        /// <summary>
        /// A test for the method <see cref="SearchEntities[T]"/> 
        /// using valid data as input data.
        /// </summary>
        [TestMethod]
        public void SearchRawMaterials()
        {
            bool stopSearch = false;
            string searchedText = "*";

            var localContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var centralContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            // Create a released project with two raw materials, one is set as deleted.
            var project1 = new Project();
            project1.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            project1.OverheadSettings = new OverheadSetting();
            project1.IsReleased = true;

            var part1 = new Part();
            part1.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            part1.OverheadSettings = new OverheadSetting();
            part1.CountrySettings = new CountrySetting();
            project1.Parts.Add(part1);

            var material1 = new RawMaterial();
            material1.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            part1.RawMaterials.Add(material1);

            var material2 = new RawMaterial();
            material2.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            material2.SetIsDeleted(true);
            part1.RawMaterials.Add(material2);

            var user1Local = new User();
            user1Local.Name = user1Local.Guid.ToString();
            user1Local.Username = user1Local.Guid.ToString();
            user1Local.Password = EncryptionManager.Instance.HashSHA256("Password.", user1Local.Salt, user1Local.Guid.ToString());

            user1Local.Roles = Role.User;

            project1.SetOwner(user1Local);

            localContext.ProjectRepository.Add(project1);
            localContext.SaveChanges();

            // Create a project, with a raw material, that belongs to another user and set it's ResponsibleCalculator to the one of the user who makes the search.
            var project2 = new Project();
            project2.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            project2.OverheadSettings = new OverheadSetting();

            var part2 = new Part();
            part2.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            part2.CountrySettings = new CountrySetting();
            part2.OverheadSettings = new OverheadSetting();
            part2.Process = new Process();
            project2.Parts.Add(part2);

            var material3 = new RawMaterial();
            material3.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            part2.RawMaterials.Add(material3);

            var user2 = new User();
            user2.Name = user2.Guid.ToString();
            user2.Username = user2.Guid.ToString();
            user2.Password = EncryptionManager.Instance.HashSHA256("Password.", user2.Salt, user2.Guid.ToString());
            user2.Roles = Role.Admin;
            project2.SetOwner(user2);

            var user1Central = new User();
            user1Central.Guid = user1Local.Guid;
            user1Central.Name = user1Local.Name;
            user1Central.Username = user1Local.Username;
            user1Central.Password = user1Local.Password;

            user1Central.Roles = user1Local.Roles;

            project2.ResponsibleCalculator = user1Central;

            centralContext.ProjectRepository.Add(project2);
            centralContext.SaveChanges();

            // Create a master data raw material.
            var material4 = new RawMaterial();
            material4.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            material4.IsMasterData = true;

            centralContext.RawMaterialRepository.Add(material4);
            centralContext.SaveChanges();

            // Login into the application with admin user
            SecurityManager.Instance.Login(user1Local.Username, "Password.");

            // Search for Raw Materials into MyProjects
            var searchParameters = new SimpleSearchParameters() { SearchText = searchedText, ScopeFilters = SearchScopeFilter.MyProjects };
            var result = SearchManager.Instance.SearchEntities<RawMaterial>(searchParameters, ref stopSearch);
            var expectedResult1 = localContext.EntityContext.RawMaterialSet.Where(r => !r.IsDeleted && r.Owner != null && r.Owner.Guid == user1Local.Guid).ToList();

            Assert.AreEqual<int>(expectedResult1.Count, result.LocalData.Count, "Failed to search any raw material into MyProjects");
            foreach (var material in expectedResult1)
            {
                var correspondingRawMaterial = result.LocalData.FirstOrDefault(m => m.Guid == material.Guid);
                if (correspondingRawMaterial == null)
                {
                    Assert.Fail("Failed to search for any raw material into MyProjects - wrong raw materials were returned");
                }
            }

            // Search for Raw Materials into OtherUsersProjects
            result.ClearResults();
            searchParameters = new SimpleSearchParameters() { SearchText = searchedText, ScopeFilters = SearchScopeFilter.OtherUsersProjects };
            result = SearchManager.Instance.SearchEntities<RawMaterial>(searchParameters, ref stopSearch);
            var expectedResult2 = new List<RawMaterial>() { material3 };

            Assert.AreEqual<int>(expectedResult2.Count, result.CentralData.Count, "Failed to search any raw material into OtherUsersProjects");
            foreach (var rawMaterial in expectedResult2)
            {
                var correspondingRawMaterial = result.CentralData.FirstOrDefault(r => r.Guid == rawMaterial.Guid);
                if (correspondingRawMaterial == null)
                {
                    Assert.Fail("Failed to search any raw material into OtherUsersProjects - wrong raw materials were returned");
                }
            }

            // Search for Raw Materials into MasterData
            result.ClearResults();
            searchParameters = new SimpleSearchParameters() { SearchText = searchedText, ScopeFilters = SearchScopeFilter.MasterData };
            result = SearchManager.Instance.SearchEntities<RawMaterial>(searchParameters, ref stopSearch);
            var expectedResult3 = centralContext.EntityContext.RawMaterialSet.Where(r => !r.IsDeleted && r.IsMasterData).ToList();

            Assert.AreEqual<int>(expectedResult3.Count, result.CentralData.Count, "Failed to search for any raw material into MasterData");
            foreach (var rawMaterial in expectedResult3)
            {
                var correspondingRawMaterial = result.CentralData.FirstOrDefault(r => r.Guid == rawMaterial.Guid);
                if (correspondingRawMaterial == null)
                {
                    Assert.Fail("Failed to search any raw material into MasterData - wrong raw materials were returned");
                }
            }

            // Search for raw materials into ReleasedProjects
            result.ClearResults();
            searchParameters = new SimpleSearchParameters() { SearchText = searchedText, ScopeFilters = SearchScopeFilter.ReleasedProjects };
            result = SearchManager.Instance.SearchEntities<RawMaterial>(searchParameters, ref stopSearch);
            var expectedResult4 = localContext.EntityContext.RawMaterialSet.Where(m => !m.IsDeleted && m.Owner == null && !m.IsMasterData).ToList();

            Assert.AreEqual<int>(expectedResult4.Count, result.LocalData.Count, "Failed to search for raw materials into ReleasedProjects");
            foreach (var material in expectedResult4)
            {
                var correspondingMaterial = result.LocalData.FirstOrDefault(m => m.Guid == material.Guid);
                if (correspondingMaterial == null)
                {
                    Assert.Fail("Failed to search for raw materials into ReleasedProjects - wrong materials were returned");
                }
            }

            // Search for raw materials into all locations
            result.ClearResults();
            searchParameters = new SimpleSearchParameters()
            {
                SearchText = searchedText,
                ScopeFilters = SearchScopeFilter.ReleasedProjects | SearchScopeFilter.OtherUsersProjects | SearchScopeFilter.MyProjects | SearchScopeFilter.MasterData
            };

            result = SearchManager.Instance.SearchEntities<RawMaterial>(searchParameters, ref stopSearch);

            // The raw materials belonging to MyProjects and ReleasedProjects will be searched into local db.
            var expectedLocalResult = localContext.EntityContext.RawMaterialSet.Where(m => !m.IsDeleted &&
                                                                                        ((m.Owner == null && !m.IsMasterData) ||
                                                                                        (m.Owner != null && m.Owner.Guid == user1Central.Guid))).ToList();

            // The raw materials that are MasterData will be searched in central db.
            var expectedCentralResult = centralContext.EntityContext.RawMaterialSet.Where(m => !m.IsDeleted && m.IsMasterData && m.Owner == null).ToList();

            // Add the raw materials that belong to another user.
            expectedCentralResult.AddRange(expectedResult2);

            Assert.AreEqual<int>(expectedLocalResult.Count, result.LocalData.Count, "Failed to search for raw materials in all locations");
            foreach (var material in expectedLocalResult)
            {
                var correspondingMaterial = result.LocalData.FirstOrDefault(m => m.Guid == material.Guid);
                if (correspondingMaterial == null)
                {
                    Assert.Fail("Failed to search for raw materials into all locations - wrong raw materials were returned");
                }
            }

            Assert.AreEqual<int>(expectedCentralResult.Count, result.CentralData.Count, "Failed to search for raw materials in all locations");
            foreach (var material in expectedCentralResult)
            {
                var correspondingMaterial = result.CentralData.FirstOrDefault(m => m.Guid == material.Guid);
                if (correspondingMaterial == null)
                {
                    Assert.Fail("Failed to search for raw materials into all locations - wrong results were returned");
                }
            }
        }

        /// <summary>
        /// A test for the method <see cref="SearchEntities[T]"/> 
        /// using valid data as input data.
        /// </summary>
        [TestMethod]
        public void SearchCommodities()
        {
            bool stopSearch = false;
            string searchedText = "*";

            var localContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var centralContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            // Create a released project with two commodities, one is set as deleted.
            var project1 = new Project();
            project1.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            project1.OverheadSettings = new OverheadSetting();
            project1.IsReleased = true;

            var part1 = new Part();
            part1.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            part1.OverheadSettings = new OverheadSetting();
            part1.CountrySettings = new CountrySetting();
            project1.Parts.Add(part1);

            var commodity1 = new Commodity();
            commodity1.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            part1.Commodities.Add(commodity1);

            var commodity2 = new Commodity();
            commodity2.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            commodity2.SetIsDeleted(true);
            part1.Commodities.Add(commodity2);

            var user1Local = new User();
            user1Local.Name = user1Local.Guid.ToString();
            user1Local.Username = user1Local.Guid.ToString();
            user1Local.Password = EncryptionManager.Instance.HashSHA256("Password.", user1Local.Salt, user1Local.Guid.ToString());

            user1Local.Roles = Role.User;

            project1.SetOwner(user1Local);

            localContext.ProjectRepository.Add(project1);
            localContext.SaveChanges();

            // Create a project, with a commodity, that belongs to another user and set it's ResponsibleCalculator to the one of the user who makes the search.
            var project2 = new Project();
            project2.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            project2.OverheadSettings = new OverheadSetting();

            var assembly1 = new Assembly();
            assembly1.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            assembly1.CountrySettings = new CountrySetting();
            assembly1.OverheadSettings = new OverheadSetting();
            assembly1.Process = new Process();
            project2.Assemblies.Add(assembly1);

            var step1 = new AssemblyProcessStep();
            step1.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            assembly1.Process.Steps.Add(step1);

            var commodity3 = new Commodity();
            commodity3.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            step1.Commodities.Add(commodity3);

            var user2 = new User();
            user2.Name = user2.Guid.ToString();
            user2.Username = user2.Guid.ToString();
            user2.Password = EncryptionManager.Instance.HashSHA256("Password.", user2.Salt, user2.Guid.ToString());
            user2.Roles = Role.Admin;
            project2.SetOwner(user2);

            var user1Central = new User();
            user1Central.Guid = user1Local.Guid;
            user1Central.Name = user1Local.Name;
            user1Central.Username = user1Local.Username;
            user1Central.Password = user1Local.Password;

            user1Central.Roles = user1Local.Roles;

            project2.ResponsibleCalculator = user1Central;

            centralContext.ProjectRepository.Add(project2);
            centralContext.SaveChanges();

            // Create a master data commodity.
            var commodity4 = new Commodity();
            commodity4.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            commodity4.IsMasterData = true;

            centralContext.CommodityRepository.Add(commodity4);
            centralContext.SaveChanges();

            // Login into the application with admin rights.
            SecurityManager.Instance.Login(user1Local.Username, "Password.");

            // Search for commodities into MyProjects.
            var searchParameters = new SimpleSearchParameters() { SearchText = searchedText, ScopeFilters = SearchScopeFilter.MyProjects };
            var result = SearchManager.Instance.SearchEntities<Commodity>(searchParameters, ref stopSearch);
            var expectedResult1 = localContext.EntityContext.CommoditySet.Where(c => !c.IsDeleted && c.Owner != null && c.Owner.Guid == user1Local.Guid).ToList();

            Assert.AreEqual<int>(expectedResult1.Count, result.LocalData.Count, "Failed to search for any commodity into MyProjects");
            foreach (var commodity in expectedResult1)
            {
                var correspondingCommodity = result.LocalData.FirstOrDefault(c => c.Guid == commodity.Guid);
                if (correspondingCommodity == null)
                {
                    Assert.Fail("Failed to search for any commodity into MyProjects - wrong commodities were returned");
                }
            }

            // Search for commodities into OtherUsersProjects.
            result.ClearResults();
            searchParameters = new SimpleSearchParameters() { SearchText = searchedText, ScopeFilters = SearchScopeFilter.OtherUsersProjects };
            result = SearchManager.Instance.SearchEntities<Commodity>(searchParameters, ref stopSearch);
            var expectedResult2 = new List<Commodity>() { commodity3 };

            Assert.AreEqual<int>(expectedResult2.Count, result.CentralData.Count, "Failed to search for any commodity into OtherUsersProjects");
            foreach (var commodity in expectedResult2)
            {
                var correspondingCommodity = result.CentralData.FirstOrDefault(c => c.Guid == commodity.Guid);
                if (correspondingCommodity == null)
                {
                    Assert.Fail("Failed to search for any commodity into OtherUsersProjects - wrong commodities were returned");
                }
            }

            // Search for commodities into MasterData.
            result.ClearResults();
            searchParameters = new SimpleSearchParameters() { SearchText = searchedText, ScopeFilters = SearchScopeFilter.MasterData };
            result = SearchManager.Instance.SearchEntities<Commodity>(searchParameters, ref stopSearch);
            var expectedResult3 = centralContext.EntityContext.CommoditySet.Where(c => !c.IsDeleted && c.IsMasterData).ToList();

            Assert.AreEqual<int>(expectedResult3.Count, result.CentralData.Count, "Failed to search any commodity into MasterData");
            foreach (var commodity in expectedResult3)
            {
                var correspondingCommodity = result.CentralData.FirstOrDefault(c => c.Guid == commodity.Guid);
                if (correspondingCommodity == null)
                {
                    Assert.Fail("Failed to search any commodity into MasterData");
                }
            }

            // Search for commodities into ReleasedProjects.
            result.ClearResults();
            searchParameters = new SimpleSearchParameters() { SearchText = searchedText, ScopeFilters = SearchScopeFilter.ReleasedProjects };
            result = SearchManager.Instance.SearchEntities<Commodity>(searchParameters, ref stopSearch);
            var expectedResult4 = localContext.EntityContext.CommoditySet.Where(c => !c.IsDeleted && c.Owner == null && !c.IsMasterData).ToList();

            Assert.AreEqual<int>(expectedResult4.Count, result.LocalData.Count, "Failed to search for commodities into ReleasedProjects");
            foreach (var commodity in expectedResult4)
            {
                var correspondingCommodity = result.LocalData.FirstOrDefault(c => c.Guid == commodity.Guid);
                if (correspondingCommodity == null)
                {
                    Assert.Fail("Failed to search for commodities into ReleasedProjects");
                }
            }

            // Search for commodities into all locations.
            result.ClearResults();
            searchParameters = new SimpleSearchParameters()
            {
                SearchText = searchedText,
                ScopeFilters = SearchScopeFilter.ReleasedProjects | SearchScopeFilter.OtherUsersProjects | SearchScopeFilter.MyProjects | SearchScopeFilter.MasterData
            };

            result = SearchManager.Instance.SearchEntities<Commodity>(searchParameters, ref stopSearch);

            // The commodities belonging to MyProjects and ReleasedProjects will be searched in local db.
            var expectedLocalResult = localContext.EntityContext.CommoditySet.Where(c => !c.IsDeleted &&
                                                                                               ((c.Owner == null && !c.IsMasterData) ||
                                                                                               (c.Owner != null && c.Owner.Guid == user1Local.Guid))).ToList();

            // The commodities that are MasterData will be searched in central db.
            var expectedCentralResult = centralContext.EntityContext.CommoditySet.Where(c => !c.IsDeleted && c.IsMasterData && c.Owner == null).ToList();

            // Add the commodities that belong to other users.
            expectedCentralResult.AddRange(expectedResult2);

            Assert.AreEqual<int>(expectedLocalResult.Count, result.LocalData.Count, "Failed to search for commodities in all locations");
            foreach (var commodity in expectedLocalResult)
            {
                var correspondingCommodity = result.LocalData.FirstOrDefault(c => c.Guid == commodity.Guid);
                if (correspondingCommodity == null)
                {
                    Assert.Fail("Failed to search for commodities in all locations - wrong commodities were returned");
                }
            }

            Assert.AreEqual<int>(expectedCentralResult.Count, result.CentralData.Count, "Failed to search for commodities in all locations");
            foreach (var commodity in expectedCentralResult)
            {
                var correspondingCommodity = result.CentralData.FirstOrDefault(c => c.Guid == commodity.Guid);
                if (correspondingCommodity == null)
                {
                    Assert.Fail("Failed to search for commodities into all locations - wrong commodities were returned");
                }
            }
        }

        /// <summary>
        /// A test for the method <see cref="SearchEntities[T]"/> 
        /// using valid data as input data.
        /// </summary>
        [TestMethod]
        public void SearchProcessSteps()
        {
            bool stopSearch = false;
            string searchedText = "*";

            var localContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var centralContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            // Create a project that contains two steps, one of them belongs to an assembly that has it's IsDeleted flag to true.
            var project1 = new Project();
            project1.Name = project1.Guid.ToString();
            project1.OverheadSettings = new OverheadSetting();

            var assembly1 = new Assembly();
            assembly1.Name = assembly1.Guid.ToString();
            assembly1.OverheadSettings = new OverheadSetting();
            assembly1.CountrySettings = new CountrySetting();
            project1.Assemblies.Add(assembly1);

            var step1 = new AssemblyProcessStep();
            step1.Name = step1.Guid.ToString();
            assembly1.Process = new Process();
            assembly1.Process.Steps.Add(step1);

            var assembly2 = new Assembly();
            assembly2.Name = assembly2.Guid.ToString();
            assembly2.OverheadSettings = new OverheadSetting();
            assembly2.CountrySettings = new CountrySetting();
            assembly2.SetIsDeleted(true);
            project1.Assemblies.Add(assembly2);

            var step2 = new AssemblyProcessStep();
            step2.Name = step2.Guid.ToString();
            assembly2.Process = new Process();
            assembly2.Process.Steps.Add(step2);

            var user1Local = new User();
            user1Local.Username = user1Local.Guid.ToString();
            user1Local.Name = user1Local.Guid.ToString();
            user1Local.Password = EncryptionManager.Instance.HashSHA256("Password.", user1Local.Salt, user1Local.Guid.ToString());

            user1Local.Roles = Role.User;

            project1.SetOwner(user1Local);

            localContext.ProjectRepository.Add(project1);
            localContext.SaveChanges();

            // Create a released project that contains a step.
            var project2 = new Project();
            project2.Name = project2.Guid.ToString();
            project2.OverheadSettings = new OverheadSetting();
            project2.IsReleased = true;

            var part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.OverheadSettings = new OverheadSetting();
            part1.CountrySettings = new CountrySetting();
            project2.Parts.Add(part1);

            var step3 = new PartProcessStep();
            step3.Name = step3.Guid.ToString();
            part1.Process = new Process();
            part1.Process.Steps.Add(step3);

            project2.SetOwner(user1Local);

            localContext.ProjectRepository.Add(project2);
            localContext.SaveChanges();

            // Create a project, with a step, that belongs to another user and set it's ResponsibleCalculator to the one of the user who makes the search.
            var project3 = new Project();
            project3.Name = project3.Guid.ToString();
            project3.OverheadSettings = new OverheadSetting();

            var assembly3 = new Assembly();
            assembly3.Name = assembly3.Guid.ToString();
            assembly3.OverheadSettings = new OverheadSetting();
            assembly3.CountrySettings = new CountrySetting();
            project3.Assemblies.Add(assembly3);

            var step4 = new AssemblyProcessStep();
            step4.Name = step4.Guid.ToString();
            assembly3.Process = new Process();
            assembly3.Process.Steps.Add(step4);

            var user2 = new User();
            user2.Name = user2.Guid.ToString();
            user2.Username = user2.Guid.ToString();
            user2.Password = EncryptionManager.Instance.HashSHA256("Password.", user2.Salt, user2.Guid.ToString());

            user2.Roles = Role.Admin;

            project3.SetOwner(user2);

            var user1Central = new User();
            user1Central.Guid = user1Local.Guid;
            user1Central.Name = user1Local.Name;
            user1Central.Username = user1Local.Username;
            user1Central.Password = user1Local.Password;
            user1Central.Roles = Role.Admin;

            project3.ResponsibleCalculator = user1Central;

            centralContext.ProjectRepository.Add(project3);
            centralContext.SaveChanges();

            // Create a master data part that contains a step.
            var part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.OverheadSettings = new OverheadSetting();
            part2.CountrySettings = new CountrySetting();
            part2.IsMasterData = true;

            var step5 = new PartProcessStep();
            step5.Name = step5.Guid.ToString();
            part2.Process = new Process();
            part2.Process.Steps.Add(step5);

            centralContext.PartRepository.Add(part2);
            centralContext.SaveChanges();

            // Login into the application with admin user.
            SecurityManager.Instance.Login(user1Local.Username, "Password.");

            // Search for process steps into MyProjects.
            var searchParameters = new SimpleSearchParameters() { SearchText = searchedText, ScopeFilters = SearchScopeFilter.MyProjects };
            var result = SearchManager.Instance.SearchEntities<ProcessStep>(searchParameters, ref stopSearch);
            var expectedResult1 = localContext.EntityContext.ProcessStepSet.Where(s => s.Owner != null && s.Owner.Guid == user1Local.Guid).ToList();

            Assert.AreEqual<int>(expectedResult1.Count, result.LocalData.Count, "Failed to search for process steps into MyProjects");
            foreach (var step in expectedResult1)
            {
                var correspondingSteps = result.LocalData.FirstOrDefault(s => s.Guid == step.Guid);
                if (correspondingSteps == null)
                {
                    Assert.Fail("Failed to search for process steps into MyProjects");
                }
            }

            // Search for process steps into OtherUsersProjects.
            result.ClearResults();
            searchParameters = new SimpleSearchParameters() { SearchText = searchedText, ScopeFilters = SearchScopeFilter.OtherUsersProjects };
            result = SearchManager.Instance.SearchEntities<ProcessStep>(searchParameters, ref stopSearch);
            var expectedResult2 = new List<ProcessStep>() { step4 };

            foreach (var step in expectedResult2)
            {
                var correspondingStep = result.CentralData.FirstOrDefault(s => s.Guid == step.Guid);
                if (correspondingStep == null)
                {
                    Assert.Fail("Failed to search for process steps into OtherUsersProjects - wrong projects were returned");
                }
            }

            // Search for process steps into MasterData.
            result.ClearResults();
            searchParameters = new SimpleSearchParameters() { SearchText = searchedText, ScopeFilters = SearchScopeFilter.MasterData };
            result = SearchManager.Instance.SearchEntities<ProcessStep>(searchParameters, ref stopSearch);
            var expectedResult3 = centralContext.EntityContext.ProcessStepSet.Where(s => s.IsMasterData).ToList();

            Assert.AreEqual<int>(expectedResult3.Count, result.CentralData.Count, "Failed to search for process steps into MasterData");
            foreach (var step in expectedResult3)
            {
                var correspondingStep = result.CentralData.FirstOrDefault(s => s.Guid == step.Guid);
                if (correspondingStep == null)
                {
                    Assert.Fail("Failed to search for process steps into MasterData");
                }
            }

            // Search for process steps into ReleasedProjects.
            result.ClearResults();
            searchParameters = new SimpleSearchParameters() { SearchText = searchedText, ScopeFilters = SearchScopeFilter.ReleasedProjects };
            result = SearchManager.Instance.SearchEntities<ProcessStep>(searchParameters, ref stopSearch);
            var expectedResult4 = localContext.EntityContext.ProcessStepSet.Where(s => s.Owner == null && !s.IsMasterData).ToList();

            Assert.AreEqual<int>(expectedResult4.Count, result.LocalData.Count, "Failed to search for process steps into ReleasedProjects");
            foreach (var step in expectedResult4)
            {
                var correspondingStep = result.LocalData.FirstOrDefault(s => s.Guid == step.Guid);
                if (correspondingStep == null)
                {
                    Assert.Fail("Failed to search for process steps into ReleasedProjects");
                }
            }

            // Search for process steps in all locations
            result.ClearResults();
            searchParameters = new SimpleSearchParameters()
            {
                SearchText = searchedText,
                ScopeFilters = SearchScopeFilter.ReleasedProjects | SearchScopeFilter.OtherUsersProjects | SearchScopeFilter.MyProjects | SearchScopeFilter.MasterData
            };
            result = SearchManager.Instance.SearchEntities<ProcessStep>(searchParameters, ref stopSearch);

            // The process steps belonging to MyProjects and ReleasedProjects will be searched into local db.
            var expectedLocalResult = localContext.EntityContext.ProcessStepSet.Where(s => (s.Owner == null && !s.IsMasterData) ||
                                                                                                   (s.Owner != null && s.Owner.Guid == user1Local.Guid)).ToList();

            // The machines that are MasterData will be searched in central db
            var expectedCentralResult = centralContext.EntityContext.ProcessStepSet.Where(ps => ps.IsMasterData && ps.Owner == null).ToList();

            // Add the machines that belong to other user.
            expectedCentralResult.AddRange(expectedResult2);

            Assert.AreEqual<int>(expectedLocalResult.Count, result.LocalData.Count, "Failed to search for process steps into all locations");
            foreach (var step in expectedLocalResult)
            {
                var correspondingStep = result.LocalData.FirstOrDefault(s => s.Guid == step.Guid);
                if (correspondingStep == null)
                {
                    Assert.Fail("Failed to search for process steps in all locations - wrong steps were returned");
                }
            }

            foreach (var step in expectedCentralResult)
            {
                var correspondingStep = result.CentralData.FirstOrDefault(s => s.Guid == step.Guid);
                if (correspondingStep == null)
                {
                    Assert.Fail("Failed to search for process steps in all locations - wrong steps were returned");
                }
            }
        }

        /// <summary>
        /// A test for the method <see cref="SearchEntities[T]"/> 
        /// using valid data as input data.
        /// </summary>
        [TestMethod]
        public void SearchMachines()
        {
            bool stopSearch = false;
            string searchedText = "*";

            var localContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var centralContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            // Create a project with two machines, one is set as deleted.
            var project1 = new Project();
            project1.Name = project1.Guid.ToString();
            project1.OverheadSettings = new OverheadSetting();

            var assembly1 = new Assembly();
            assembly1.Name = assembly1.Guid.ToString();
            assembly1.OverheadSettings = new OverheadSetting();
            assembly1.CountrySettings = new CountrySetting();
            project1.Assemblies.Add(assembly1);

            var step1 = new AssemblyProcessStep();
            step1.Name = step1.Guid.ToString();
            assembly1.Process = new Process();
            assembly1.Process.Steps.Add(step1);

            var machine1 = new Machine();
            machine1.Name = machine1.Guid.ToString();
            step1.Machines.Add(machine1);

            var part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.OverheadSettings = new OverheadSetting();
            part1.CountrySettings = new CountrySetting();
            project1.Parts.Add(part1);

            var step2 = new PartProcessStep();
            step2.Name = step2.Guid.ToString();
            part1.Process = new Process();
            part1.Process.Steps.Add(step2);

            var machine2 = new Machine();
            machine2.Name = machine2.Guid.ToString();
            step2.Machines.Add(machine2);
            part1.SetIsDeleted(true);

            // Create a released project with a machine.
            var project2 = new Project();
            project2.Name = project2.Guid.ToString();
            project2.OverheadSettings = new OverheadSetting();
            project2.IsReleased = true;

            var part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.OverheadSettings = new OverheadSetting();
            part2.CountrySettings = new CountrySetting();
            project2.Parts.Add(part2);

            var step3 = new PartProcessStep();
            step3.Name = step3.Guid.ToString();
            part2.Process = new Process();
            part2.Process.Steps.Add(step3);

            var machine3 = new Machine();
            machine3.Name = machine3.Guid.ToString();
            step3.Machines.Add(machine3);

            var user1Local = new User();
            user1Local.Name = user1Local.Guid.ToString();
            user1Local.Username = user1Local.Guid.ToString();
            user1Local.Password = EncryptionManager.Instance.HashSHA256("Password.", user1Local.Salt, user1Local.Guid.ToString());

            user1Local.Roles = Role.User;

            project1.SetOwner(user1Local);

            localContext.ProjectRepository.Add(project1);
            localContext.SaveChanges();

            // Create a project, with a machine, that belongs to another user and set it's ResponsibleCalculator to the one of the user who makes the search.
            var project3 = new Project();
            project3.Name = project3.Guid.ToString();
            project3.OverheadSettings = new OverheadSetting();

            var assembly2 = new Assembly();
            assembly2.Name = assembly2.Guid.ToString();
            assembly2.OverheadSettings = new OverheadSetting();
            assembly2.CountrySettings = new CountrySetting();
            project3.Assemblies.Add(assembly2);

            var step4 = new AssemblyProcessStep();
            step4.Name = step4.Guid.ToString();
            assembly2.Process = new Process();
            assembly2.Process.Steps.Add(step4);

            var machine4 = new Machine();
            machine4.Name = machine4.Guid.ToString();
            step4.Machines.Add(machine4);

            var user2 = new User();
            user2.Name = user2.Guid.ToString();
            user2.Username = user2.Guid.ToString();
            user2.Password = EncryptionManager.Instance.HashSHA256("Password.", user2.Salt, user2.Guid.ToString());
            user2.Roles = Role.Admin;
            project3.SetOwner(user2);

            var user1Central = new User();
            user1Central.Guid = user1Local.Guid;
            user1Central.Name = user1Local.Name;
            user1Central.Username = user1Local.Username;
            user1Central.Password = user1Local.Password;

            user1Central.Roles = user1Local.Roles;

            project3.ResponsibleCalculator = user1Central;

            centralContext.ProjectRepository.Add(project3);
            centralContext.SaveChanges();

            // Create a master data machine.
            var machine5 = new Machine();
            machine5.Name = machine5.Guid.ToString();
            machine5.IsMasterData = true;

            centralContext.MachineRepository.Add(machine5);
            centralContext.SaveChanges();

            // Login into the application with the admin user
            SecurityManager.Instance.Login(user1Local.Username, "Password.");

            // Search for any machine into MyProjects
            var searchParameters = new SimpleSearchParameters() { SearchText = searchedText, ScopeFilters = SearchScopeFilter.MyProjects };
            var result = SearchManager.Instance.SearchEntities<Machine>(searchParameters, ref stopSearch);
            var expectedResult1 = localContext.EntityContext.MachineSet.Where(m => !m.IsDeleted && m.Owner != null && m.Owner.Guid == user1Local.Guid).ToList();

            Assert.AreEqual<int>(expectedResult1.Count, result.LocalData.Count, "Failed to search for machines into MyProjects");
            foreach (var machine in expectedResult1)
            {
                var correspondingMachine = result.LocalData.FirstOrDefault(m => m.Guid == machine.Guid);
                if (correspondingMachine == null)
                {
                    Assert.Fail("Failed to search for machines into MyProjects - wrong machines were displayed");
                }
            }

            // Search for any machine into OtherUsersProjects
            result.ClearResults();
            searchParameters = new SimpleSearchParameters() { SearchText = searchedText, ScopeFilters = SearchScopeFilter.OtherUsersProjects };
            result = SearchManager.Instance.SearchEntities<Machine>(searchParameters, ref stopSearch);
            var expectedResult2 = new List<Machine>() { machine4 };

            Assert.AreEqual<int>(expectedResult2.Count, result.CentralData.Count, "Failed to search for machines into OtherUsersProjects");
            foreach (var machine in expectedResult2)
            {
                var correspondingMachine = result.CentralData.FirstOrDefault(m => m.Guid == machine.Guid);
                if (correspondingMachine == null)
                {
                    Assert.Fail("Failed to search for machines into OtherUsersProjects - wrong machines were returned");
                }
            }

            // Search for any machine into MasterData
            result.ClearResults();
            searchParameters = new SimpleSearchParameters() { SearchText = searchedText, ScopeFilters = SearchScopeFilter.MasterData };
            result = SearchManager.Instance.SearchEntities<Machine>(searchParameters, ref stopSearch);
            var expectedResult3 = centralContext.EntityContext.MachineSet.Where(m => !m.IsDeleted && m.IsMasterData).ToList();

            Assert.AreEqual<int>(expectedResult3.Count, result.CentralData.Count, "Failed to search for machines into MasterData");
            foreach (var machine in expectedResult3)
            {
                var correspondingMachine = result.CentralData.FirstOrDefault(m => m.Guid == machine.Guid);
                if (correspondingMachine == null)
                {
                    Assert.Fail("Failed to search for machines into MasterData - wrong machines were returned");
                }
            }

            // Search for machines into ReleasedProjects
            result.ClearResults();
            searchParameters = new SimpleSearchParameters() { SearchText = searchedText, ScopeFilters = SearchScopeFilter.ReleasedProjects };
            result = SearchManager.Instance.SearchEntities<Machine>(searchParameters, ref stopSearch);
            var expectedResult4 = localContext.EntityContext.MachineSet.Where(m => !m.IsDeleted && m.Owner == null && !m.IsMasterData).ToList();

            Assert.AreEqual<int>(expectedResult4.Count, result.LocalData.Count, "Failed to search for machines into ReleasedProjects");
            foreach (var machine in expectedResult4)
            {
                var correspondingMachine = result.LocalData.FirstOrDefault(m => m.Guid == machine.Guid);
                if (correspondingMachine == null)
                {
                    Assert.Fail("Failed to search for machines into ReleasedProjects");
                }
            }

            // Search for machines in all locations
            result.ClearResults();
            searchParameters = new SimpleSearchParameters()
            {
                SearchText = searchedText,
                ScopeFilters = SearchScopeFilter.ReleasedProjects | SearchScopeFilter.OtherUsersProjects | SearchScopeFilter.MyProjects | SearchScopeFilter.MasterData
            };

            result = SearchManager.Instance.SearchEntities<Machine>(searchParameters, ref stopSearch);

            // The machines belonging to MyProjects and ReleasedProjects will be searched in local db
            var expectedLocalResult = localContext.EntityContext.MachineSet.Where(m => !m.IsDeleted &&
                                                                                           ((m.Owner == null && !m.IsMasterData) ||
                                                                                            (m.Owner != null && m.Owner.Guid == user1Central.Guid))).ToList();

            // The machines that are MasterData will be searched in central db
            var expectedCentralResult = centralContext.EntityContext.MachineSet.Where(m => !m.IsDeleted && m.IsMasterData && m.Owner == null).ToList();

            // Add the machines that belong to other user.
            expectedCentralResult.AddRange(expectedResult2);

            Assert.AreEqual<int>(expectedLocalResult.Count, result.LocalData.Count, "Failed to search for machines in all locations");
            foreach (var machine in expectedLocalResult)
            {
                var correspondingMachine = result.LocalData.FirstOrDefault(m => m.Guid == machine.Guid);
                if (correspondingMachine == null)
                {
                    Assert.Fail("Failed to search for machines in all locations - wrong machines were returned");
                }
            }

            Assert.AreEqual<int>(expectedCentralResult.Count, result.CentralData.Count, "Failed to search for machines in all locations");
            foreach (var machine in expectedCentralResult)
            {
                var correspondingMachine = result.CentralData.FirstOrDefault(m => m.Guid == machine.Guid);
                if (correspondingMachine == null)
                {
                    Assert.Fail("Failed to search for machines in all locations - wrong machines were returned");
                }
            }
        }

        /// <summary>
        /// A test for the method <see cref="SearchEntities[T]"/> 
        /// using valid data as input data.
        /// </summary>
        [TestMethod]
        public void SearchDies()
        {
            bool stopSearch = false;
            string searchedText = "*";

            Project project1 = new Project();
            project1.Name = project1.Guid.ToString();
            project1.OverheadSettings = new OverheadSetting();

            Assembly assembly1 = new Assembly();
            assembly1.Name = assembly1.Guid.ToString();
            assembly1.OverheadSettings = new OverheadSetting();
            assembly1.CountrySettings = new CountrySetting();
            project1.Assemblies.Add(assembly1);

            ProcessStep step1 = new AssemblyProcessStep();
            step1.Name = step1.Guid.ToString();
            assembly1.Process = new Process();
            assembly1.Process.Steps.Add(step1);

            Die die1 = new Die();
            die1.Name = die1.Guid.ToString();
            step1.Dies.Add(die1);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.OverheadSettings = new OverheadSetting();
            part1.CountrySettings = new CountrySetting();
            project1.Parts.Add(part1);

            ProcessStep step2 = new PartProcessStep();
            step2.Name = step2.Guid.ToString();
            part1.Process = new Process();
            part1.Process.Steps.Add(step2);

            Die die2 = new Die();
            die2.Name = die2.Guid.ToString();
            step2.Dies.Add(die2);
            part1.SetIsDeleted(true);

            Project project2 = new Project();
            project2.Name = project2.Guid.ToString();
            project2.OverheadSettings = new OverheadSetting();
            project2.IsReleased = true;

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.OverheadSettings = new OverheadSetting();
            part2.CountrySettings = new CountrySetting();
            project2.Parts.Add(part2);

            ProcessStep step3 = new PartProcessStep();
            step3.Name = step3.Guid.ToString();
            part2.Process = new Process();
            part2.Process.Steps.Add(step3);

            Die die3 = new Die();
            die3.Name = die3.Guid.ToString();
            step3.Dies.Add(die3);

            User user1 = new User();
            user1.Name = user1.Guid.ToString();
            user1.Username = user1.Guid.ToString();
            user1.Password = EncryptionManager.Instance.HashSHA256("Password.", user1.Salt, user1.Guid.ToString());
            user1.Roles = Role.Admin;
            project1.SetOwner(user1);

            IDataSourceManager localContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            localContext.ProjectRepository.Add(project1);
            localContext.ProjectRepository.Add(project2);
            localContext.SaveChanges();

            Die die4 = new Die();
            die4.Name = die4.Guid.ToString();
            die4.IsMasterData = true;

            Project project3 = new Project();
            project3.Name = project3.Guid.ToString();
            project3.OverheadSettings = new OverheadSetting();

            Assembly assembly2 = new Assembly();
            assembly2.Name = assembly2.Guid.ToString();
            assembly2.OverheadSettings = new OverheadSetting();
            assembly2.CountrySettings = new CountrySetting();
            project3.Assemblies.Add(assembly2);

            ProcessStep step4 = new AssemblyProcessStep();
            step4.Name = step4.Guid.ToString();
            assembly2.Process = new Process();
            assembly2.Process.Steps.Add(step4);

            User user2 = new User();
            user2.Name = user2.Guid.ToString();
            user2.Username = user2.Guid.ToString();
            user2.Password = EncryptionManager.Instance.HashSHA256("Password.", user2.Salt, user2.Guid.ToString());
            user2.Roles = Role.Admin;
            project3.SetOwner(user2);

            IDataSourceManager centralContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            centralContext.UserRepository.Add(user1);
            centralContext.DieRepository.Add(die4);
            centralContext.ProjectRepository.Add(project3);
            centralContext.SaveChanges();

            // Login into the application with admin user
            SecurityManager.Instance.Login(user1.Username, "Password.");

            // Search for dies into MyProjects
            var searchParameters = new SimpleSearchParameters() { SearchText = searchedText, ScopeFilters = SearchScopeFilter.MyProjects };
            var result = SearchManager.Instance.SearchEntities<Die>(searchParameters, ref stopSearch);
            List<Die> expectedResult1 = localContext.EntityContext.DieSet.Where(d => !d.IsDeleted && d.Owner != null && d.Owner.Guid == user1.Guid).ToList();

            Assert.AreEqual<int>(expectedResult1.Count, result.LocalData.Count, "Failed to search for dies into MyProjects");
            foreach (Die die in expectedResult1)
            {
                Die correspondingDie = result.LocalData.FirstOrDefault(d => d.Guid == die.Guid);
                if (correspondingDie == null)
                {
                    Assert.Fail("Failed to search the dies into MyProjects - wrong dies were returned");
                }
            }

            // Search for dies into OtherUsersProjects
            result.ClearResults();
            searchParameters = new SimpleSearchParameters() { SearchText = searchedText, ScopeFilters = SearchScopeFilter.OtherUsersProjects };
            result = SearchManager.Instance.SearchEntities<Die>(searchParameters, ref stopSearch);
            List<Die> expectedResult2 = centralContext.EntityContext.DieSet.Where(d => !d.IsDeleted && d.Owner != null && d.Owner.Guid != user1.Guid).ToList();

            Assert.AreEqual<int>(expectedResult2.Count, result.CentralData.Count, "Failed to search for dies into OtherUsersProjects");
            foreach (Die die in expectedResult2)
            {
                Die correspondingDie = result.CentralData.FirstOrDefault(d => d.Guid == die.Guid);
                if (correspondingDie == null)
                {
                    Assert.Fail("Failed to search for dies into OtherUsersProjects - wrong dies were returned");
                }
            }

            // Search for dies into MasterData
            result.ClearResults();
            searchParameters = new SimpleSearchParameters() { SearchText = searchedText, ScopeFilters = SearchScopeFilter.MasterData };
            result = SearchManager.Instance.SearchEntities<Die>(searchParameters, ref stopSearch);
            List<Die> expectedResult3 = centralContext.EntityContext.DieSet.Where(d => !d.IsDeleted && d.IsMasterData).ToList();

            Assert.AreEqual<int>(expectedResult3.Count, result.CentralData.Count, "Failed to search for dies into MasterData");
            foreach (Die die in expectedResult3)
            {
                Die correspondingDie = result.CentralData.FirstOrDefault(d => d.Guid == die.Guid);
                if (correspondingDie == null)
                {
                    Assert.Fail("Failed to search for dies into MasterData - wrong dies were returned");
                }
            }

            // Search for dies into ReleasedProjects
            result.ClearResults();
            searchParameters = new SimpleSearchParameters() { SearchText = searchedText, ScopeFilters = SearchScopeFilter.ReleasedProjects };
            result = SearchManager.Instance.SearchEntities<Die>(searchParameters, ref stopSearch);
            List<Die> expectedResult4 = localContext.EntityContext.DieSet.Where(d => !d.IsDeleted && d.Owner == null && !d.IsMasterData).ToList();

            Assert.AreEqual<int>(expectedResult4.Count, result.LocalData.Count, "Failed to search for machines in all locations");
            foreach (Die die in expectedResult4)
            {
                Die correspondingDie = result.LocalData.FirstOrDefault(d => d.Guid == die.Guid);
                if (correspondingDie == null)
                {
                    Assert.Fail("Failed to search for machine in all locations");
                }
            }

            // Search for dies in all locations
            result.ClearResults();
            searchParameters = new SimpleSearchParameters()
            {
                SearchText = searchedText,
                ScopeFilters = SearchScopeFilter.ReleasedProjects | SearchScopeFilter.OtherUsersProjects | SearchScopeFilter.MyProjects | SearchScopeFilter.MasterData
            };

            result = SearchManager.Instance.SearchEntities<Die>(searchParameters, ref stopSearch);

            // The dies belonging to MyProjects and ReleasedProjects will be searched in local db
            List<Die> expectedLocalResult = localContext.EntityContext.DieSet.Where(d => !d.IsDeleted &&
                                                                                    ((d.Owner == null && !d.IsMasterData) ||
                                                                                     (d.Owner != null && d.Owner.Guid == user1.Guid))).ToList();

            // The dies belonging to OtherUsersProjects and MasterData will be searched in central db
            List<Die> expectedCentralResult = centralContext.EntityContext.DieSet.Where(d => !d.IsDeleted &&
                                                                                       (d.IsMasterData ||
                                                                                       (d.Owner != null &&
                                                                                        d.Owner.Guid != user1.Guid))).ToList();

            Assert.AreEqual<int>(expectedLocalResult.Count, result.LocalData.Count, "Failed to search for dies in all locations");
            foreach (Die die in expectedLocalResult)
            {
                Die correspondingDie = result.LocalData.FirstOrDefault(d => d.Guid == die.Guid);
                if (correspondingDie == null)
                {
                    Assert.Fail("Failed to search for dies in all locations");
                }
            }

            Assert.AreEqual<int>(expectedCentralResult.Count, result.CentralData.Count, "Failed to search for dies in all locations");
            foreach (Die die in expectedCentralResult)
            {
                Die correspondingDie = result.CentralData.FirstOrDefault(d => d.Guid == die.Guid);
            }
        }

        /// <summary>
        /// A test for the method SearchEntities[T](string searchText, SearchScopeFilter scopeFilters, out List[T] localData, out List[T] centralData, ref bool stopSearch)
        /// using valid data as input data.
        /// </summary>
        [TestMethod]
        public void SearchConsumables()
        {
            bool stopSearch = false;
            string searchedText = "*";

            Project project1 = new Project();
            project1.Name = project1.Guid.ToString();
            project1.OverheadSettings = new OverheadSetting();

            Assembly assembly1 = new Assembly();
            assembly1.Name = assembly1.Guid.ToString();
            assembly1.OverheadSettings = new OverheadSetting();
            assembly1.CountrySettings = new CountrySetting();
            project1.Assemblies.Add(assembly1);

            ProcessStep step1 = new AssemblyProcessStep();
            step1.Name = step1.Guid.ToString();
            assembly1.Process = new Process();
            assembly1.Process.Steps.Add(step1);

            Consumable consumable1 = new Consumable();
            consumable1.Name = consumable1.Guid.ToString();
            step1.Consumables.Add(consumable1);

            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.OverheadSettings = new OverheadSetting();
            part1.CountrySettings = new CountrySetting();
            project1.Parts.Add(part1);

            ProcessStep step2 = new PartProcessStep();
            step2.Name = step2.Guid.ToString();
            part1.Process = new Process();
            part1.Process.Steps.Add(step2);

            Consumable consumable2 = new Consumable();
            consumable2.Name = consumable2.Guid.ToString();
            step2.Consumables.Add(consumable2);
            part1.SetIsDeleted(true);

            Project project2 = new Project();
            project2.Name = project2.Guid.ToString();
            project2.OverheadSettings = new OverheadSetting();
            project2.IsReleased = true;

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.OverheadSettings = new OverheadSetting();
            part2.CountrySettings = new CountrySetting();
            project2.Parts.Add(part2);

            ProcessStep step3 = new PartProcessStep();
            step3.Name = step3.Guid.ToString();
            part2.Process = new Process();
            part2.Process.Steps.Add(step3);

            Consumable consumable3 = new Consumable();
            consumable3.Name = consumable3.Guid.ToString();
            step3.Consumables.Add(consumable3);

            User user1 = new User();
            user1.Name = user1.Guid.ToString();
            user1.Username = user1.Guid.ToString();
            user1.Password = EncryptionManager.Instance.HashSHA256("Password.", user1.Salt, user1.Guid.ToString());
            user1.Roles = Role.Admin;
            project1.SetOwner(user1);

            IDataSourceManager localContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            localContext.ProjectRepository.Add(project1);
            localContext.ProjectRepository.Add(project2);
            localContext.SaveChanges();

            Consumable consumable4 = new Consumable();
            consumable4.Name = consumable4.Guid.ToString();
            consumable4.IsMasterData = true;

            Project project3 = new Project();
            project3.Name = project3.Guid.ToString();
            project3.OverheadSettings = new OverheadSetting();

            Assembly assembly2 = new Assembly();
            assembly2.Name = assembly2.Guid.ToString();
            assembly2.OverheadSettings = new OverheadSetting();
            assembly2.CountrySettings = new CountrySetting();

            ProcessStep step4 = new AssemblyProcessStep();
            step4.Name = step3.Guid.ToString();
            assembly2.Process = new Process();
            assembly2.Process.Steps.Add(step4);

            Consumable consumable5 = new Consumable();
            consumable5.Name = consumable5.Guid.ToString();
            step4.Consumables.Add(consumable5);

            User user2 = new User();
            user2.Name = user2.Guid.ToString();
            user2.Username = user2.Guid.ToString();
            user2.Password = EncryptionManager.Instance.HashSHA256("Password.", user2.Salt, user2.Guid.ToString());
            user2.Roles = Role.Admin;
            project3.SetOwner(user2);

            IDataSourceManager centralContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            centralContext.ConsumableRepository.Add(consumable4);
            centralContext.UserRepository.Add(user1);
            centralContext.ProjectRepository.Add(project3);
            centralContext.SaveChanges();

            // Login into the application with the admin user
            SecurityManager.Instance.Login(user1.Username, "Password.");

            // Search for consumables into MyProjects
            var searchParameters = new SimpleSearchParameters() { SearchText = searchedText, ScopeFilters = SearchScopeFilter.MyProjects };
            var result = SearchManager.Instance.SearchEntities<Consumable>(searchParameters, ref stopSearch);
            List<Consumable> expectedResult1 = localContext.EntityContext.ConsumableSet.Where(c => !c.IsDeleted && c.Owner != null && c.Owner.Guid == user1.Guid).ToList();

            Assert.AreEqual<int>(expectedResult1.Count, result.LocalData.Count, "Failed to search for consumables into MyProjects");
            foreach (Consumable consumable in expectedResult1)
            {
                Consumable correspondingConsumable = result.LocalData.FirstOrDefault(c => c.Guid == consumable.Guid);
                if (correspondingConsumable == null)
                {
                    Assert.Fail("Failed to search for consumables into MyProjects - wrong consumables were returned");
                }
            }

            // Search for consumables into OtherUsersProjects
            result.ClearResults();
            searchParameters = new SimpleSearchParameters() { SearchText = searchedText, ScopeFilters = SearchScopeFilter.OtherUsersProjects };
            result = SearchManager.Instance.SearchEntities<Consumable>(searchParameters, ref stopSearch);
            List<Consumable> expectedResult2 = centralContext.EntityContext.ConsumableSet.Where(c => !c.IsDeleted && c.Owner != null && c.Owner.Guid != user1.Guid).ToList();

            Assert.AreEqual<int>(expectedResult2.Count, result.CentralData.Count, "Failed to search for consumables into OtherUsersProjects");
            foreach (Consumable consumable in expectedResult2)
            {
                Consumable correspondingConsumable = result.CentralData.FirstOrDefault(c => c.Guid == consumable.Guid);
                if (correspondingConsumable == null)
                {
                    Assert.Fail("Failed to search for consumables into OtherUsersProjects - wrong consumables were returned");
                }
            }

            // Search for consumables into MasterData
            result.ClearResults();
            searchParameters = new SimpleSearchParameters() { SearchText = searchedText, ScopeFilters = SearchScopeFilter.MasterData };
            result = SearchManager.Instance.SearchEntities<Consumable>(searchParameters, ref stopSearch);
            List<Consumable> expectedResult3 = centralContext.EntityContext.ConsumableSet.Where(c => !c.IsDeleted && c.IsMasterData).ToList();

            Assert.AreEqual<int>(expectedResult3.Count, result.CentralData.Count, "Failed to search for consumables into MasterData");
            foreach (Consumable consumable in expectedResult3)
            {
                Consumable correspondingConsumable = result.CentralData.FirstOrDefault(c => c.Guid == consumable.Guid);
                if (correspondingConsumable == null)
                {
                    Assert.Fail("Failed to search for consumables into MasterData");
                }
            }

            // Search for consumables into ReleasedProjects
            result.ClearResults();
            searchParameters = new SimpleSearchParameters() { SearchText = searchedText, ScopeFilters = SearchScopeFilter.ReleasedProjects };
            result = SearchManager.Instance.SearchEntities<Consumable>(searchParameters, ref stopSearch);
            List<Consumable> expectedResult4 = localContext.EntityContext.ConsumableSet.Where(c => !c.IsDeleted && c.Owner == null && !c.IsMasterData).ToList();

            Assert.AreEqual<int>(expectedResult4.Count, result.LocalData.Count, "Failed to search for consumables into ReleasedProjects");
            foreach (Consumable consumable in expectedResult4)
            {
                Consumable correspondingConsumable = result.LocalData.FirstOrDefault(c => c.Guid == consumable.Guid);
                if (correspondingConsumable == null)
                {
                    Assert.Fail("Failed to search for consumables into ReleasedProjects - wrong consumables were returned");
                }
            }

            // Search for consumables in all locations
            result.ClearResults();
            searchParameters = new SimpleSearchParameters()
                        {
                            SearchText = searchedText,
                            ScopeFilters = SearchScopeFilter.ReleasedProjects | SearchScopeFilter.OtherUsersProjects | SearchScopeFilter.MyProjects | SearchScopeFilter.MasterData
                        };

            result = SearchManager.Instance.SearchEntities<Consumable>(searchParameters, ref stopSearch);

            // The consumables belonging to MyProjects and ReleasedProjects will be searched in local db
            List<Consumable> expectedLocalResult = localContext.EntityContext.ConsumableSet.Where(c => !c.IsDeleted &&
                                                                                                 ((c.Owner == null && !c.IsMasterData) ||
                                                                                                  (c.Owner != null && c.Owner.Guid == user1.Guid))).ToList();

            // The consumables belonging to OtherUsersProjects and MasterData will be searched in central db
            List<Consumable> expectedCentralResult = centralContext.EntityContext.ConsumableSet.Where(c => !c.IsDeleted &&
                                                                                                     (c.IsMasterData ||
                                                                                                     (c.Owner != null &&
                                                                                                      c.Owner.Guid != user1.Guid))).ToList();

            Assert.AreEqual<int>(expectedLocalResult.Count, result.LocalData.Count, "Failed to search for consumables in all locations");
            foreach (Consumable consumable in expectedLocalResult)
            {
                Consumable correspondingConsumable = result.LocalData.FirstOrDefault(c => c.Guid == consumable.Guid);
                if (correspondingConsumable == null)
                {
                    Assert.Fail("Failed to search for consumables in all locations - wrong consumables were returned");
                }
            }

            Assert.AreEqual<int>(expectedCentralResult.Count, result.CentralData.Count, "Failed to search for consumables in all locations");
            foreach (Consumable consumable in expectedCentralResult)
            {
                Consumable correspondingConsumable = result.CentralData.FirstOrDefault(c => c.Guid == consumable.Guid);
                if (correspondingConsumable == null)
                {
                    Assert.Fail("Failed to search for consumables in all locations - wrong consumables were returned");
                }
            }
        }

        /// <summary>
        /// A test for searching projects entities that have distinct names
        /// </summary>
        [TestMethod]
        public void SearchDistinctProjects()
        {
            bool stopSearch = false;
            string searchedText = Guid.NewGuid().ToString();

            var localContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var centralContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            var localUser = new User();
            localUser.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            localUser.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            localUser.Password = EncryptionManager.Instance.HashSHA256("Password.", localUser.Salt, localUser.Guid.ToString());
            localUser.Roles = Role.User;
            localContext.UserRepository.Add(localUser);
            localContext.SaveChanges();

            // Login into the application with the admin user.
            SecurityManager.Instance.Login(localUser.Username, "Password.");

            var centralUser = new User();
            centralUser.Guid = localUser.Guid;
            centralUser.Name = localUser.Name;
            centralUser.Username = localUser.Username;
            centralUser.Password = localUser.Password;
            centralUser.Roles = localUser.Roles;

            // Create a user for projects from central database.
            var user2 = new User();
            user2.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            user2.Username = EncryptionManager.Instance.GenerateRandomString(5, true);
            user2.Password = EncryptionManager.Instance.HashSHA256("Password.", user2.Salt, user2.Guid.ToString());
            user2.Roles = Role.Admin;

            centralContext.UserRepository.Add(centralUser);
            centralContext.SaveChanges();

            var project1 = new Project();
            project1.Name = searchedText;
            project1.OverheadSettings = new OverheadSetting();
            project1.SetOwner(localUser);

            var project2 = new Project();
            project2.Name = searchedText;
            project2.OverheadSettings = new OverheadSetting();
            project2.SetOwner(localUser);

            localContext.ProjectRepository.Add(project1);
            localContext.ProjectRepository.Add(project2);
            localContext.SaveChanges();

            var project3 = new Project();
            project3.Name = searchedText;
            project3.OverheadSettings = new OverheadSetting();
            project3.SetOwner(user2);
            project3.ResponsibleCalculator = centralUser;

            var project4 = new Project();
            project4.Name = searchedText;
            project4.OverheadSettings = new OverheadSetting();
            project4.SetOwner(user2);
            project4.ResponsibleCalculator = centralUser;

            centralContext.ProjectRepository.Add(project3);
            centralContext.ProjectRepository.Add(project4);
            centralContext.SaveChanges();

            var searchParameters = new SimpleSearchParameters()
            {
                SearchText = searchedText,
                ScopeFilters = SearchScopeFilter.MasterData | SearchScopeFilter.MyProjects | SearchScopeFilter.OtherUsersProjects | SearchScopeFilter.ReleasedProjects,
                GetDistinctResults = true
            };
            var distinctResult = SearchManager.Instance.SearchEntities<Project>(searchParameters, ref stopSearch);

            Assert.AreEqual(1, distinctResult.LocalData.Count, "Must return 1 project from the local database");
            Assert.AreEqual(1, distinctResult.CentralData.Count, "Must return 1 project from the central database");
        }

        /// <summary>
        /// A test for searching assemblies entities that have distinct names
        /// </summary>
        [TestMethod]
        public void SearchDistinctAssemblies()
        {
            bool stopSearch = false;
            string searchedText = Guid.NewGuid().ToString();

            var localContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var centralContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            var localUser = new User();
            localUser.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            localUser.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            localUser.Password = EncryptionManager.Instance.HashSHA256("Password.", localUser.Salt, localUser.Guid.ToString());
            localUser.Roles = Role.User;
            localContext.UserRepository.Add(localUser);
            localContext.SaveChanges();

            // Login into the application with the admin user.
            SecurityManager.Instance.Login(localUser.Username, "Password.");

            var centralUser = new User();
            centralUser.Guid = localUser.Guid;
            centralUser.Name = localUser.Name;
            centralUser.Username = localUser.Username;
            centralUser.Password = localUser.Password;
            centralUser.Roles = localUser.Roles;

            // Create a user for projects from central database.
            var user2 = new User();
            user2.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            user2.Username = EncryptionManager.Instance.GenerateRandomString(5, true);
            user2.Password = EncryptionManager.Instance.HashSHA256("Password.", user2.Salt, user2.Guid.ToString());
            user2.Roles = Role.Admin;

            centralContext.UserRepository.Add(centralUser);
            centralContext.SaveChanges();

            var project1 = new Project();
            project1.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            project1.OverheadSettings = new OverheadSetting();
            project1.SetOwner(localUser);

            var assembly1 = new Assembly();
            assembly1.Name = searchedText;
            assembly1.OverheadSettings = new OverheadSetting();
            assembly1.CountrySettings = new CountrySetting();
            project1.Assemblies.Add(assembly1);

            var assembly2 = new Assembly();
            assembly2.Name = searchedText;
            assembly2.OverheadSettings = new OverheadSetting();
            assembly2.CountrySettings = new CountrySetting();
            project1.Assemblies.Add(assembly2);

            localContext.ProjectRepository.Add(project1);
            localContext.AssemblyRepository.Add(assembly1);
            localContext.AssemblyRepository.Add(assembly2);
            localContext.SaveChanges();

            var assembly3 = new Assembly();
            assembly3.Name = searchedText;
            assembly3.OverheadSettings = new OverheadSetting();
            assembly3.CountrySettings = new CountrySetting();
            assembly3.IsMasterData = true;

            var assembly4 = new Assembly();
            assembly4.Name = searchedText;
            assembly4.OverheadSettings = new OverheadSetting();
            assembly4.CountrySettings = new CountrySetting();
            assembly4.IsMasterData = true;

            centralContext.AssemblyRepository.Add(assembly3);
            centralContext.AssemblyRepository.Add(assembly4);
            centralContext.SaveChanges();

            var searchParameters = new SimpleSearchParameters()
            {
                SearchText = searchedText,
                ScopeFilters = SearchScopeFilter.MasterData | SearchScopeFilter.MyProjects | SearchScopeFilter.OtherUsersProjects | SearchScopeFilter.ReleasedProjects,
                GetDistinctResults = true
            };
            var distinctResult = SearchManager.Instance.SearchEntities<Assembly>(searchParameters, ref stopSearch);

            Assert.AreEqual(1, distinctResult.LocalData.Count, "Must return 1 assembly from the local database");
            Assert.AreEqual(1, distinctResult.CentralData.Count, "Must return 1 assembly from the central database");
        }

        /// <summary>
        /// A test for searching parts entities that have distinct names
        /// </summary>
        [TestMethod]
        public void SearchDistinctParts()
        {
            bool stopSearch = false;
            string searchedText = Guid.NewGuid().ToString();

            var localContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var centralContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            var localUser = new User();
            localUser.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            localUser.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            localUser.Password = EncryptionManager.Instance.HashSHA256("Password.", localUser.Salt, localUser.Guid.ToString());
            localUser.Roles = Role.User;
            localContext.UserRepository.Add(localUser);
            localContext.SaveChanges();

            // Login into the application with the admin user.
            SecurityManager.Instance.Login(localUser.Username, "Password.");

            var centralUser = new User();
            centralUser.Guid = localUser.Guid;
            centralUser.Name = localUser.Name;
            centralUser.Username = localUser.Username;
            centralUser.Password = localUser.Password;
            centralUser.Roles = localUser.Roles;

            // Create a user for projects from central database.
            var user2 = new User();
            user2.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            user2.Username = EncryptionManager.Instance.GenerateRandomString(5, true);
            user2.Password = EncryptionManager.Instance.HashSHA256("Password.", user2.Salt, user2.Guid.ToString());
            user2.Roles = Role.Admin;

            centralContext.UserRepository.Add(centralUser);
            centralContext.SaveChanges();

            var project1 = new Project();
            project1.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            project1.OverheadSettings = new OverheadSetting();
            project1.SetOwner(localUser);

            var part1 = new Part();
            part1.Name = searchedText;
            part1.OverheadSettings = new OverheadSetting();
            part1.CountrySettings = new CountrySetting();
            project1.Parts.Add(part1);

            var part2 = new Part();
            part2.Name = searchedText;
            part2.OverheadSettings = new OverheadSetting();
            part2.CountrySettings = new CountrySetting();
            project1.Parts.Add(part2);

            localContext.ProjectRepository.Add(project1);
            localContext.PartRepository.Add(part1);
            localContext.PartRepository.Add(part2);
            localContext.SaveChanges();

            var part3 = new Part();
            part3.Name = searchedText;
            part3.OverheadSettings = new OverheadSetting();
            part3.CountrySettings = new CountrySetting();
            part3.IsMasterData = true;

            var part4 = new Part();
            part4.Name = searchedText;
            part4.OverheadSettings = new OverheadSetting();
            part4.CountrySettings = new CountrySetting();
            part4.IsMasterData = true;

            centralContext.PartRepository.Add(part3);
            centralContext.PartRepository.Add(part4);
            centralContext.SaveChanges();

            var searchParameters = new SimpleSearchParameters()
            {
                SearchText = searchedText,
                ScopeFilters = SearchScopeFilter.MasterData | SearchScopeFilter.MyProjects | SearchScopeFilter.OtherUsersProjects | SearchScopeFilter.ReleasedProjects,
                GetDistinctResults = true
            };
            var distinctResult = SearchManager.Instance.SearchEntities<Part>(searchParameters, ref stopSearch);

            Assert.AreEqual(1, distinctResult.LocalData.Count, "Must return 1 part from the local database");
            Assert.AreEqual(1, distinctResult.CentralData.Count, "Must return 1 part from the central database");
        }

        /// <summary>
        /// A test for searching dies entities that have distinct names
        /// </summary>
        [TestMethod]
        public void SearchDistinctDies()
        {
            bool stopSearch = false;
            string searchedText = Guid.NewGuid().ToString();

            var localContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var centralContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            var localUser = new User();
            localUser.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            localUser.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            localUser.Password = EncryptionManager.Instance.HashSHA256("Password.", localUser.Salt, localUser.Guid.ToString());
            localUser.Roles = Role.User;
            localContext.UserRepository.Add(localUser);
            localContext.SaveChanges();

            // Login into the application with the admin user.
            SecurityManager.Instance.Login(localUser.Username, "Password.");

            var centralUser = new User();
            centralUser.Guid = localUser.Guid;
            centralUser.Name = localUser.Name;
            centralUser.Username = localUser.Username;
            centralUser.Password = localUser.Password;
            centralUser.Roles = localUser.Roles;

            // Create a user for projects from central database.
            var user2 = new User();
            user2.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            user2.Username = EncryptionManager.Instance.GenerateRandomString(5, true);
            user2.Password = EncryptionManager.Instance.HashSHA256("Password.", user2.Salt, user2.Guid.ToString());
            user2.Roles = Role.Admin;

            centralContext.UserRepository.Add(centralUser);
            centralContext.SaveChanges();

            var project1 = new Project();
            project1.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            project1.OverheadSettings = new OverheadSetting();
            project1.SetOwner(localUser);

            var assembly1 = new Assembly();
            assembly1.Name = searchedText;
            assembly1.OverheadSettings = new OverheadSetting();
            assembly1.CountrySettings = new CountrySetting();
            project1.Assemblies.Add(assembly1);

            var assyStep = new AssemblyProcessStep();
            assyStep.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            assembly1.Process = new Process();
            assembly1.Process.Steps.Add(assyStep);

            var die1 = new Die();
            die1.Name = searchedText;
            var die2 = new Die();
            die2.Name = searchedText;
            assyStep.Dies.Add(die1);
            assyStep.Dies.Add(die2);

            localContext.ProjectRepository.Add(project1);
            localContext.AssemblyRepository.Add(assembly1);
            localContext.SaveChanges();

            var assembly2 = new Assembly();
            assembly2.Name = searchedText;
            assembly2.OverheadSettings = new OverheadSetting();
            assembly2.CountrySettings = new CountrySetting();
            assembly2.IsMasterData = true;
            assembly2.Process = new Process();

            var assyStep2 = new AssemblyProcessStep();
            assyStep2.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            assyStep2.IsMasterData = true;
            assembly2.Process.Steps.Add(assyStep2);

            var die3 = new Die();
            die3.Name = searchedText;
            die3.IsMasterData = true;
            var die4 = new Die();
            die4.Name = searchedText;
            die4.IsMasterData = true;
            assyStep2.Dies.Add(die3);
            assyStep2.Dies.Add(die4);

            centralContext.AssemblyRepository.Add(assembly2);
            centralContext.SaveChanges();

            var searchParameters = new SimpleSearchParameters()
            {
                SearchText = searchedText,
                ScopeFilters = SearchScopeFilter.MasterData | SearchScopeFilter.MyProjects | SearchScopeFilter.OtherUsersProjects | SearchScopeFilter.ReleasedProjects,
                GetDistinctResults = true
            };
            var distinctResult = SearchManager.Instance.SearchEntities<Die>(searchParameters, ref stopSearch);

            Assert.AreEqual(1, distinctResult.LocalData.Count, "Must return 1 Die from the local database");
            Assert.AreEqual(1, distinctResult.CentralData.Count, "Must return 1 Die from the central database");
        }

        /// <summary>
        /// A test for searching consumables entities that have distinct names
        /// </summary>
        [TestMethod]
        public void SearchDistinctConsumables()
        {
            bool stopSearch = false;
            string searchedText = Guid.NewGuid().ToString();

            var localContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var centralContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            var localUser = new User();
            localUser.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            localUser.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            localUser.Password = EncryptionManager.Instance.HashSHA256("Password.", localUser.Salt, localUser.Guid.ToString());
            localUser.Roles = Role.User;
            localContext.UserRepository.Add(localUser);
            localContext.SaveChanges();

            // Login into the application with the admin user.
            SecurityManager.Instance.Login(localUser.Username, "Password.");

            var centralUser = new User();
            centralUser.Guid = localUser.Guid;
            centralUser.Name = localUser.Name;
            centralUser.Username = localUser.Username;
            centralUser.Password = localUser.Password;
            centralUser.Roles = localUser.Roles;

            // Create a user for projects from central database.
            var user2 = new User();
            user2.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            user2.Username = EncryptionManager.Instance.GenerateRandomString(5, true);
            user2.Password = EncryptionManager.Instance.HashSHA256("Password.", user2.Salt, user2.Guid.ToString());
            user2.Roles = Role.Admin;

            centralContext.UserRepository.Add(centralUser);
            centralContext.SaveChanges();

            var project1 = new Project();
            project1.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            project1.OverheadSettings = new OverheadSetting();
            project1.SetOwner(localUser);

            var assembly1 = new Assembly();
            assembly1.Name = searchedText;
            assembly1.OverheadSettings = new OverheadSetting();
            assembly1.CountrySettings = new CountrySetting();
            project1.Assemblies.Add(assembly1);

            var assyStep = new AssemblyProcessStep();
            assyStep.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            assembly1.Process = new Process();
            assembly1.Process.Steps.Add(assyStep);

            var consumable1 = new Consumable();
            consumable1.Name = searchedText;
            var consumable2 = new Consumable();
            consumable2.Name = searchedText;
            assyStep.Consumables.Add(consumable1);
            assyStep.Consumables.Add(consumable2);

            localContext.ProjectRepository.Add(project1);
            localContext.AssemblyRepository.Add(assembly1);
            localContext.SaveChanges();

            var assembly2 = new Assembly();
            assembly2.Name = searchedText;
            assembly2.OverheadSettings = new OverheadSetting();
            assembly2.CountrySettings = new CountrySetting();
            assembly2.IsMasterData = true;
            assembly2.Process = new Process();

            var assyStep2 = new AssemblyProcessStep();
            assyStep2.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            assyStep2.IsMasterData = true;
            assembly2.Process.Steps.Add(assyStep2);

            var consumable3 = new Consumable();
            consumable3.Name = searchedText;
            consumable3.IsMasterData = true;
            var consumable4 = new Consumable();
            consumable4.Name = searchedText;
            consumable4.IsMasterData = true;
            assyStep2.Consumables.Add(consumable3);
            assyStep2.Consumables.Add(consumable4);

            centralContext.AssemblyRepository.Add(assembly2);
            centralContext.SaveChanges();

            var searchParameters = new SimpleSearchParameters()
            {
                SearchText = searchedText,
                ScopeFilters = SearchScopeFilter.MasterData | SearchScopeFilter.MyProjects | SearchScopeFilter.OtherUsersProjects | SearchScopeFilter.ReleasedProjects,
                GetDistinctResults = true
            };
            var distinctResult = SearchManager.Instance.SearchEntities<Consumable>(searchParameters, ref stopSearch);

            Assert.AreEqual(1, distinctResult.LocalData.Count, "Must return 1 Consumable from the local database");
            Assert.AreEqual(1, distinctResult.CentralData.Count, "Must return 1 Consumable from the central database");
        }

        /// <summary>
        /// A test for searching commodities entities that have distinct names
        /// </summary>
        [TestMethod]
        public void SearchDistinctCommodities()
        {
            bool stopSearch = false;
            string searchedText = Guid.NewGuid().ToString();

            var localContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var centralContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            var localUser = new User();
            localUser.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            localUser.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            localUser.Password = EncryptionManager.Instance.HashSHA256("Password.", localUser.Salt, localUser.Guid.ToString());
            localUser.Roles = Role.User;
            localContext.UserRepository.Add(localUser);
            localContext.SaveChanges();

            // Login into the application with the admin user.
            SecurityManager.Instance.Login(localUser.Username, "Password.");

            var centralUser = new User();
            centralUser.Guid = localUser.Guid;
            centralUser.Name = localUser.Name;
            centralUser.Username = localUser.Username;
            centralUser.Password = localUser.Password;
            centralUser.Roles = localUser.Roles;

            // Create a user for projects from central database.
            var user2 = new User();
            user2.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            user2.Username = EncryptionManager.Instance.GenerateRandomString(5, true);
            user2.Password = EncryptionManager.Instance.HashSHA256("Password.", user2.Salt, user2.Guid.ToString());
            user2.Roles = Role.Admin;

            centralContext.UserRepository.Add(centralUser);
            centralContext.SaveChanges();

            var project1 = new Project();
            project1.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            project1.OverheadSettings = new OverheadSetting();
            project1.SetOwner(localUser);

            var assembly1 = new Assembly();
            assembly1.Name = searchedText;
            assembly1.OverheadSettings = new OverheadSetting();
            assembly1.CountrySettings = new CountrySetting();
            project1.Assemblies.Add(assembly1);

            var assyStep = new AssemblyProcessStep();
            assyStep.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            assembly1.Process = new Process();
            assembly1.Process.Steps.Add(assyStep);

            var commodity1 = new Commodity();
            commodity1.Name = searchedText;
            var commodity2 = new Commodity();
            commodity2.Name = searchedText;
            assyStep.Commodities.Add(commodity1);
            assyStep.Commodities.Add(commodity2);

            localContext.ProjectRepository.Add(project1);
            localContext.AssemblyRepository.Add(assembly1);
            localContext.SaveChanges();

            var assembly2 = new Assembly();
            assembly2.Name = searchedText;
            assembly2.OverheadSettings = new OverheadSetting();
            assembly2.CountrySettings = new CountrySetting();
            assembly2.IsMasterData = true;
            assembly2.Process = new Process();

            var assyStep2 = new AssemblyProcessStep();
            assyStep2.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            assyStep2.IsMasterData = true;
            assembly2.Process.Steps.Add(assyStep2);

            var commodity3 = new Commodity();
            commodity3.Name = searchedText;
            commodity3.IsMasterData = true;
            var commodity4 = new Commodity();
            commodity4.Name = searchedText;
            commodity4.IsMasterData = true;
            assyStep2.Commodities.Add(commodity3);
            assyStep2.Commodities.Add(commodity4);

            centralContext.AssemblyRepository.Add(assembly2);
            centralContext.SaveChanges();

            var searchParameters = new SimpleSearchParameters()
            {
                SearchText = searchedText,
                ScopeFilters = SearchScopeFilter.MasterData | SearchScopeFilter.MyProjects | SearchScopeFilter.OtherUsersProjects | SearchScopeFilter.ReleasedProjects,
                GetDistinctResults = true
            };
            var distinctResult = SearchManager.Instance.SearchEntities<Commodity>(searchParameters, ref stopSearch);

            Assert.AreEqual(1, distinctResult.LocalData.Count, "Must return 1 Commodity from the local database");
            Assert.AreEqual(1, distinctResult.CentralData.Count, "Must return 1 Commodity from the central database");
        }

        /// <summary>
        /// A test for searching machines entities that have distinct names
        /// </summary>
        [TestMethod]
        public void SearchDistinctMachines()
        {
            bool stopSearch = false;
            string searchedText = Guid.NewGuid().ToString();

            var localContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var centralContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            var localUser = new User();
            localUser.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            localUser.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            localUser.Password = EncryptionManager.Instance.HashSHA256("Password.", localUser.Salt, localUser.Guid.ToString());
            localUser.Roles = Role.User;
            localContext.UserRepository.Add(localUser);
            localContext.SaveChanges();

            // Login into the application with the admin user.
            SecurityManager.Instance.Login(localUser.Username, "Password.");

            var centralUser = new User();
            centralUser.Guid = localUser.Guid;
            centralUser.Name = localUser.Name;
            centralUser.Username = localUser.Username;
            centralUser.Password = localUser.Password;
            centralUser.Roles = localUser.Roles;

            // Create a user for projects from central database.
            var user2 = new User();
            user2.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            user2.Username = EncryptionManager.Instance.GenerateRandomString(5, true);
            user2.Password = EncryptionManager.Instance.HashSHA256("Password.", user2.Salt, user2.Guid.ToString());
            user2.Roles = Role.Admin;

            centralContext.UserRepository.Add(centralUser);
            centralContext.SaveChanges();

            var project1 = new Project();
            project1.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            project1.OverheadSettings = new OverheadSetting();
            project1.SetOwner(localUser);

            var assembly1 = new Assembly();
            assembly1.Name = searchedText;
            assembly1.OverheadSettings = new OverheadSetting();
            assembly1.CountrySettings = new CountrySetting();
            project1.Assemblies.Add(assembly1);

            var assyStep = new AssemblyProcessStep();
            assyStep.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            assembly1.Process = new Process();
            assembly1.Process.Steps.Add(assyStep);

            var machine1 = new Machine();
            machine1.Name = searchedText;
            var machine2 = new Machine();
            machine2.Name = searchedText;
            assyStep.Machines.Add(machine1);
            assyStep.Machines.Add(machine2);

            localContext.ProjectRepository.Add(project1);
            localContext.AssemblyRepository.Add(assembly1);
            localContext.SaveChanges();

            var assembly2 = new Assembly();
            assembly2.Name = searchedText;
            assembly2.OverheadSettings = new OverheadSetting();
            assembly2.CountrySettings = new CountrySetting();
            assembly2.IsMasterData = true;
            assembly2.Process = new Process();

            var assyStep2 = new AssemblyProcessStep();
            assyStep2.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            assyStep2.IsMasterData = true;
            assembly2.Process.Steps.Add(assyStep2);

            var machine3 = new Machine();
            machine3.Name = searchedText;
            machine3.IsMasterData = true;
            var machine4 = new Machine();
            machine4.Name = searchedText;
            machine4.IsMasterData = true;
            assyStep2.Machines.Add(machine3);
            assyStep2.Machines.Add(machine4);

            centralContext.AssemblyRepository.Add(assembly2);
            centralContext.SaveChanges();

            var searchParameters = new SimpleSearchParameters()
            {
                SearchText = searchedText,
                ScopeFilters = SearchScopeFilter.MasterData | SearchScopeFilter.MyProjects | SearchScopeFilter.OtherUsersProjects | SearchScopeFilter.ReleasedProjects,
                GetDistinctResults = true
            };
            var distinctResult = SearchManager.Instance.SearchEntities<Machine>(searchParameters, ref stopSearch);

            Assert.AreEqual(1, distinctResult.LocalData.Count, "Must return 1 Machine from the local database");
            Assert.AreEqual(1, distinctResult.CentralData.Count, "Must return 1 Machine from the central database");
        }

        /// <summary>
        /// A test for searching raw materials entities that have distinct names
        /// </summary>
        [TestMethod]
        public void SearchDistinctRawMaterials()
        {
            bool stopSearch = false;
            string searchedText = Guid.NewGuid().ToString();

            var localContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var centralContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            var localUser = new User();
            localUser.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            localUser.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            localUser.Password = EncryptionManager.Instance.HashSHA256("Password.", localUser.Salt, localUser.Guid.ToString());
            localUser.Roles = Role.User;
            localContext.UserRepository.Add(localUser);
            localContext.SaveChanges();

            // Login into the application with the admin user.
            SecurityManager.Instance.Login(localUser.Username, "Password.");

            var centralUser = new User();
            centralUser.Guid = localUser.Guid;
            centralUser.Name = localUser.Name;
            centralUser.Username = localUser.Username;
            centralUser.Password = localUser.Password;
            centralUser.Roles = localUser.Roles;

            // Create a user for projects from central database.
            var user2 = new User();
            user2.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            user2.Username = EncryptionManager.Instance.GenerateRandomString(5, true);
            user2.Password = EncryptionManager.Instance.HashSHA256("Password.", user2.Salt, user2.Guid.ToString());
            user2.Roles = Role.Admin;

            centralContext.UserRepository.Add(centralUser);
            centralContext.SaveChanges();

            var project1 = new Project();
            project1.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            project1.OverheadSettings = new OverheadSetting();
            project1.SetOwner(localUser);

            var part1 = new Part();
            part1.Name = searchedText;
            part1.OverheadSettings = new OverheadSetting();
            part1.CountrySettings = new CountrySetting();
            project1.Parts.Add(part1);

            var material1 = new RawMaterial();
            material1.Name = searchedText;
            var material2 = new RawMaterial();
            material2.Name = searchedText;
            part1.RawMaterials.Add(material1);
            part1.RawMaterials.Add(material2);

            localContext.ProjectRepository.Add(project1);
            localContext.PartRepository.Add(part1);
            localContext.SaveChanges();

            var part2 = new Part();
            part2.Name = searchedText;
            part2.OverheadSettings = new OverheadSetting();
            part2.CountrySettings = new CountrySetting();
            part2.IsMasterData = true;

            var material3 = new RawMaterial();
            material3.Name = searchedText;
            material3.IsMasterData = true;
            var material4 = new RawMaterial();
            material4.Name = searchedText;
            material4.IsMasterData = true;
            part2.RawMaterials.Add(material3);
            part2.RawMaterials.Add(material4);

            centralContext.PartRepository.Add(part2);
            centralContext.SaveChanges();

            var searchParameters = new SimpleSearchParameters()
            {
                SearchText = searchedText,
                ScopeFilters = SearchScopeFilter.MasterData | SearchScopeFilter.MyProjects | SearchScopeFilter.OtherUsersProjects | SearchScopeFilter.ReleasedProjects,
                GetDistinctResults = true
            };
            var distinctResult = SearchManager.Instance.SearchEntities<RawMaterial>(searchParameters, ref stopSearch);

            Assert.AreEqual(1, distinctResult.LocalData.Count, "Must return 1 RawMaterial from the local database");
            Assert.AreEqual(1, distinctResult.CentralData.Count, "Must return 1 RawMaterial from the central database");
        }

        /// <summary>
        /// A test for searching process steps entities that have distinct names
        /// </summary>
        [TestMethod]
        public void SearchDistinctProcessSteps()
        {
            bool stopSearch = false;
            string searchedText = Guid.NewGuid().ToString();

            var localContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var centralContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

            var localUser = new User();
            localUser.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            localUser.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            localUser.Password = EncryptionManager.Instance.HashSHA256("Password.", localUser.Salt, localUser.Guid.ToString());
            localUser.Roles = Role.User;
            localContext.UserRepository.Add(localUser);
            localContext.SaveChanges();

            // Login into the application with the admin user.
            SecurityManager.Instance.Login(localUser.Username, "Password.");

            var centralUser = new User();
            centralUser.Guid = localUser.Guid;
            centralUser.Name = localUser.Name;
            centralUser.Username = localUser.Username;
            centralUser.Password = localUser.Password;
            centralUser.Roles = localUser.Roles;

            // Create a user for projects from central database.
            var user2 = new User();
            user2.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            user2.Username = EncryptionManager.Instance.GenerateRandomString(5, true);
            user2.Password = EncryptionManager.Instance.HashSHA256("Password.", user2.Salt, user2.Guid.ToString());
            user2.Roles = Role.Admin;

            centralContext.UserRepository.Add(centralUser);
            centralContext.SaveChanges();

            var project1 = new Project();
            project1.Name = EncryptionManager.Instance.GenerateRandomString(10, true);
            project1.OverheadSettings = new OverheadSetting();

            var assembly1 = new Assembly();
            assembly1.Name = searchedText;
            assembly1.OverheadSettings = new OverheadSetting();
            assembly1.CountrySettings = new CountrySetting();
            project1.Assemblies.Add(assembly1);

            var assyStep1 = new AssemblyProcessStep();
            assyStep1.Name = searchedText;
            var assyStep2 = new AssemblyProcessStep();
            assyStep2.Name = searchedText;
            assembly1.Process = new Process();
            assembly1.Process.Steps.Add(assyStep1);
            assembly1.Process.Steps.Add(assyStep2);

            project1.SetOwner(localUser);

            localContext.ProjectRepository.Add(project1);
            localContext.SaveChanges();

            var assembly2 = new Assembly();
            assembly2.Name = searchedText;
            assembly2.OverheadSettings = new OverheadSetting();
            assembly2.CountrySettings = new CountrySetting();
            assembly2.IsMasterData = true;
            assembly2.Process = new Process();

            var assyStep3 = new AssemblyProcessStep();
            assyStep3.Name = searchedText;
            assyStep3.IsMasterData = true;
            var assyStep4 = new AssemblyProcessStep();
            assyStep4.Name = searchedText;
            assyStep4.IsMasterData = true;
            assembly2.Process.Steps.Add(assyStep3);
            assembly2.Process.Steps.Add(assyStep4);

            centralContext.AssemblyRepository.Add(assembly2);
            centralContext.SaveChanges();

            var searchParameters = new SimpleSearchParameters()
            {
                SearchText = searchedText,
                ScopeFilters = SearchScopeFilter.MasterData | SearchScopeFilter.MyProjects | SearchScopeFilter.OtherUsersProjects | SearchScopeFilter.ReleasedProjects,
                GetDistinctResults = true
            };
            var distinctResult = SearchManager.Instance.SearchEntities<ProcessStep>(searchParameters, ref stopSearch);

            Assert.AreEqual(1, distinctResult.LocalData.Count, "Must return 1 ProcessStep from the local database");
            Assert.AreEqual(1, distinctResult.CentralData.Count, "Must return 1 ProcessStep from the central database");
        }

        #endregion Test Methods
    }
}
