﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;

namespace ZPKTool.Business.Tests
{
    /// <summary>
    /// This is a test class for SecurityManager and is intended
    /// to contain all SecurityManagerTest Unit Tests
    /// </summary>
    [TestClass]
    public class SecurityManagerTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SecurityManagerTest"/> class.
        /// </summary>
        public SecurityManagerTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes

        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }

        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }

        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }

        /// <summary>
        /// Use TestCleanup to run code after each test has run
        /// If a user is logged in the application, it should be logged out after the test was done otherwise will influence other tests.
        /// </summary>
        [TestCleanup]
        public void MyTestCleanup()
        {
            if (SecurityManager.Instance.CurrentUser != null)
            {
                SecurityManager.Instance.Logout();
            }
        }

        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// Tests Login method using valid.
        /// </summary>
        [TestMethod]
        public void LoginWithValidUser()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            User user = new User();
            user.Username = "User" + DateTime.Now.Ticks;
            user.Password = EncryptionManager.Instance.HashSHA256("Password.", user.Salt, user.Guid.ToString());
            user.Name = "User" + DateTime.Now.Ticks;
            user.Roles = Role.Admin;

            dataContext.UserRepository.Add(user);
            dataContext.SaveChanges();

            User result = SecurityManager.Instance.Login(user.Username, "Password.");
            SecurityManager.Instance.Logout();

            Assert.IsNotNull(result, "Failed to login a valid user");
            Assert.AreEqual<Guid>(user.Guid, result.Guid, "The current user is different than the logged user");
        }

        /// <summary>
        /// Tests Login method using an empty username parameter.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void LoginWithEmptyUsername()
        {
            User result = SecurityManager.Instance.Login(string.Empty, "Password");
        }

        /// <summary>
        /// Tests Login method using an empty username parameter.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void LoginWithEmptyPassword()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            User user = new User();
            user.Username = "User" + DateTime.Now.Ticks;
            user.Password = EncryptionManager.Instance.EncodeMD5("Password");
            user.Name = "User" + DateTime.Now.Ticks;
            user.Roles = Role.Admin;

            dataContext.UserRepository.Add(user);
            dataContext.SaveChanges();

            User result = SecurityManager.Instance.Login(user.Name, string.Empty);
        }

        /// <summary>
        /// Tests Logout method.
        /// </summary>
        [TestMethod]
        public void LogoutTest()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            User user = new User();
            user.Username = "User" + DateTime.Now.Ticks;
            user.Password = EncryptionManager.Instance.HashSHA256("Password.", user.Salt, user.Guid.ToString());
            user.Name = "User" + DateTime.Now.Ticks;
            user.Roles = Role.Admin;

            dataContext.UserRepository.Add(user);
            dataContext.SaveChanges();

            User currentUser = SecurityManager.Instance.Login(user.Name, "Password.");
            SecurityManager.Instance.Logout();

            Assert.IsNull(SecurityManager.Instance.CurrentUser, "Failed to logout");
        }

        /// <summary>
        /// Tests ChangePassword method using valid data.
        /// </summary>
        [TestMethod]
        public void ChangePasswordWithValidDatas()
        {
            User user = new User();
            user.Username = "User" + DateTime.Now.Ticks;
            user.Password = EncryptionManager.Instance.HashSHA256("Password.", user.Salt, user.Guid.ToString());
            user.Name = "User" + DateTime.Now.Ticks;
            user.Roles = Role.Admin;

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            dataContext1.UserRepository.Add(user);
            dataContext1.SaveChanges();

            string oldPassword = "Password.";
            string newPassword = "NewPassword.";
            SecurityManager.Instance.ChangePassword(user.Guid, oldPassword, newPassword);

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            User updatedUser = dataContext2.EntityContext.UserSet.FirstOrDefault(u => u.Guid == user.Guid);
            string expectedPassword = EncryptionManager.Instance.HashSHA256(newPassword, user.Salt, user.Guid.ToString());

            Assert.AreEqual<string>(expectedPassword, updatedUser.Password, "Failed to change the user's password");
        }

        /// <summary>
        /// Tests ChangePassword method using null value as the old password.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ChangePasswordWithNullOldPassword()
        {
            User user = new User();
            user.Username = "User" + DateTime.Now.Ticks;
            user.Password = EncryptionManager.Instance.HashSHA256("Password.", user.Salt, user.Guid.ToString());
            user.Name = "User" + DateTime.Now.Ticks;
            user.Roles = Role.Admin;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            dataContext.UserRepository.Add(user);
            dataContext.SaveChanges();

            string newPassword = "New Password";
            SecurityManager.Instance.ChangePassword(user.Guid, null, newPassword);
        }

        /// <summary>
        /// Tests ChangePassword method using an empty string as the old password.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ChangePasswordWithEmptyOldPassword()
        {
            User user = new User();
            user.Username = "User" + DateTime.Now.Ticks;
            user.Password = EncryptionManager.Instance.HashSHA256("Password.", user.Salt, user.Guid.ToString());
            user.Name = "User" + DateTime.Now.Ticks;
            user.Roles = Role.Admin;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            dataContext.UserRepository.Add(user);
            dataContext.SaveChanges();

            string newPassword = "NewPassword.";
            SecurityManager.Instance.ChangePassword(user.Guid, string.Empty, newPassword);
        }

        /// <summary>
        /// Tests ChangePassword method using a wrong value for old password param.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(BusinessException))]
        public void ChangePasswordUsingWrongOldPassword()
        {
            User user = new User();
            user.Username = "User" + DateTime.Now.Ticks;
            user.Password = EncryptionManager.Instance.HashSHA256("Password.", user.Salt, user.Guid.ToString());
            user.Name = "User" + DateTime.Now.Ticks;
            user.Roles = Role.Admin;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            dataContext.UserRepository.Add(user);
            dataContext.SaveChanges();

            string wrongOldPassword = EncryptionManager.Instance.GenerateRandomString(15, true);
            string newPassword = "NewPassword.";
            SecurityManager.Instance.ChangePassword(user.Guid, wrongOldPassword, newPassword);
        }

        /// <summary>
        /// Tests ChangePassword method using null value as new password.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ChangePasswordWithNullPassword()
        {
            User user = new User();
            user.Username = "User" + DateTime.Now.Ticks;
            user.Password = EncryptionManager.Instance.HashSHA256("Password.", user.Salt, user.Guid.ToString());
            user.Name = "User" + DateTime.Now.Ticks;
            user.Roles = Role.Admin;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            dataContext.UserRepository.Add(user);
            dataContext.SaveChanges();

            string oldPassword = "Password";
            SecurityManager.Instance.ChangePassword(user.Guid, oldPassword, null);
        }

        /// <summary>
        /// Tests ChangePassword method using an empty string as the new password.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ChangePasswordWithEmptyPassword()
        {
            User user = new User();
            user.Username = "User" + DateTime.Now.Ticks;
            user.Password = EncryptionManager.Instance.HashSHA256("Password.", user.Salt, user.Guid.ToString());
            user.Name = "User" + DateTime.Now.Ticks;
            user.Roles = Role.Admin;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            dataContext.UserRepository.Add(user);
            dataContext.SaveChanges();

            string oldPassword = "Password.";
            SecurityManager.Instance.ChangePassword(user.Guid, oldPassword, string.Empty);
        }

        /// <summary>
        /// Tests ChangePassword method using a wrong guid.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(BusinessException))]
        public void ChangePasswordWithWrongUserGuid()
        {
            string oldPassword = "Password";
            string newPassword = "NewPassword";
            Guid wrongGuid = new Guid();

            SecurityManager.Instance.ChangePassword(wrongGuid, oldPassword, newPassword);
        }

        ///// <summary>
        ///// Tests ChangeCurrentUserUICountry method using valid data.
        ///// </summary>
        //[TestMethod]
        //public void ChangeCurrentUserUICountryWithValidCountry()
        //{
        //    IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
        //    Role keyUserRole = dataContext.RoleRepository.GetAll().FirstOrDefault(r => r.Equals(UserRole.KeyUser));
        //    Country country = dataContext.EntityContext.CountrySet.FirstOrDefault();

        //    User user = new User();
        //    user.Username = "User" + DateTime.Now.Ticks;
        //    user.Password = EncryptionManager.Instance.EncodeMD5("Password");
        //    user.Name = "User" + DateTime.Now.Ticks;
        //    user.Roles.Add(keyUserRole);
        //    user.FavoriteCountry = country;

        //    dataContext.UserRepository.Add(user);
        //    dataContext.SaveChanges();

        //    // Login into the application with created user
        //    SecurityManager.Instance.Login(user.Username, "Password");
        //    Country newCountry = dataContext.EntityContext.CountrySet.FirstOrDefault(c => c.Guid != country.Guid);

        //    SecurityManager.Instance.ChangeCurrentUserUICurrency(newCountry);
        //    Country currentUserCountry = SecurityManager.Instance.CurrentUser.FavoriteCountry;

        //    // Logout from the application
        //    SecurityManager.Instance.Logout();

        //    Assert.IsNotNull(currentUserCountry, "The UICountry of the current user is null");
        //    Assert.AreEqual<Guid>(newCountry.Guid, currentUserCountry.Guid, "Failed to change UICountry of the current user");
        //}

        /// <summary>
        /// Tests CurrentUserHasRole method using valid data.
        /// </summary>
        [TestMethod]
        public void CurrentUserHasRoleTest()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            User user = new User();
            user.Username = "User" + DateTime.Now.Ticks;
            user.Password = EncryptionManager.Instance.EncodeMD5("Password");
            user.Name = "User" + DateTime.Now.Ticks;
            user.Roles = Role.Viewer;
            SecurityManager.Instance.ChangeCurrentUserRole(Role.Viewer);

            dataContext.UserRepository.Add(user);
            dataContext.SaveChanges();

            // Login into the application with the created user
            SecurityManager.Instance.Login(user.Username, "Password");
            bool result1 = SecurityManager.Instance.CurrentUserHasRole(Role.Viewer);

            // Verify if the current user has role for a user that has the specified role
            Assert.IsTrue(result1, "CurrentUserHasRole method returned false for a user which has the specified role");

            bool result2 = SecurityManager.Instance.CurrentUserHasRole(Role.Admin);

            // Verify if the current user has role for a user that doesn't have the specified role
            Assert.IsFalse(result2, "CurrentUserHasRole method returned true for a user which hasn't the specified role");

            SecurityManager.Instance.Logout();
        }

        /// <summary>
        /// A test for the CurrentUserHasRight(Right right) method.
        /// </summary>
        [TestMethod]
        public void CurrentUserHasRightTest()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            User user = new User();
            user.Username = user.Guid.ToString();
            user.Password = EncryptionManager.Instance.HashSHA256("Password.", user.Salt, user.Guid.ToString());
            user.Name = user.Guid.ToString();
            user.Roles = Role.Viewer;
            SecurityManager.Instance.ChangeCurrentUserRole(Role.Viewer);

            dataContext.UserRepository.Add(user);
            dataContext.SaveChanges();

            // Login into the application with the created user
            SecurityManager.Instance.Login(user.Username, "Password.");
            bool result1 = SecurityManager.Instance.CurrentUserHasRight(Right.ViewAndReports);

            Assert.IsTrue(result1, "A viewer should has the 'ViewAndReports' right.");

            // Verify if the current user has a specified right
            bool result2 = SecurityManager.Instance.CurrentUserHasRight(Right.AdminTasks);

            Assert.IsFalse(result2, "A viewer doesn't have admin rights.");

            // Verify if a user with null role has a specified right 
            SecurityManager.Instance.ChangeCurrentUserRole(Role.None);
            bool result3 = SecurityManager.Instance.CurrentUserHasRight(Right.ViewAndReports);

            Assert.IsFalse(result3, "The current user's role is null. The user should not have any access right.");

            // Verify if a null user has a specified right 
            SecurityManager.Instance.ChangeCurrentUserRole(Role.Viewer);
            SecurityManager.Instance.Logout();
            bool result4 = SecurityManager.Instance.CurrentUserHasRight(Right.ViewAndReports);

            Assert.IsFalse(result4, "A null user has no right.");
        }

        /// <summary>
        /// Tests GetUICurrency method.
        /// </summary>
        [TestMethod]
        public void GetUICurrencyTest()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var currency = dataContext.CurrencyRepository.GetByIsoCode("USD");

            User user = new User();
            user.Username = "User" + DateTime.Now.Ticks;
            user.Password = EncryptionManager.Instance.HashSHA256("Password.", user.Salt, user.Guid.ToString());
            user.Name = "User" + DateTime.Now.Ticks;
            user.Roles = Role.KeyUser;
            user.UICurrency = currency;

            dataContext.UserRepository.Add(user);
            dataContext.SaveChanges();

            // Login into the application with created user
            SecurityManager.Instance.Login(user.Username, "Password.");
            var result = SecurityManager.Instance.UICurrency;

            // Logout from the application
            SecurityManager.Instance.Logout();

            Assert.IsNotNull(result, "A null currency was returned for a user which has a favorite country");
            Assert.AreEqual<Guid>(currency.Guid, result.Guid, "Wrong currency returned");
        }

        /// <summary>
        /// A test for RefreshCurrentUser() method. The following input data were used:
        /// Current user = valid user.
        /// </summary>
        [TestMethod]
        public void RefreshCurrentUserTest1()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            User user = new User();
            user.Username = "User" + DateTime.Now.Ticks;
            user.Password = EncryptionManager.Instance.EncodeMD5("Password");
            user.Name = "User" + DateTime.Now.Ticks;
            user.Roles = Role.Admin;

            dataContext.UserRepository.Add(user);
            dataContext.SaveChanges();

            User result = SecurityManager.Instance.Login(user.Username, "Password");
            SecurityManager.Instance.RefreshCurrentUser();
        }

        /// <summary>
        /// A test for RefreshCurrentUser() method. The following input data were used:
        /// Current user = null.
        /// </summary>
        [TestMethod]
        public void RefreshCurrentUserTest2()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            User user = new User();
            user.Username = "User" + DateTime.Now.Ticks;
            user.Password = EncryptionManager.Instance.EncodeMD5("Password");
            user.Name = "User" + DateTime.Now.Ticks;
            user.Roles = Role.Admin;

            dataContext.UserRepository.Add(user);
            dataContext.SaveChanges();

            User result = SecurityManager.Instance.Login(user.Username, "Password");
            SecurityManager.Instance.RefreshCurrentUser();
        }

        #endregion Test Methods
    }
}
