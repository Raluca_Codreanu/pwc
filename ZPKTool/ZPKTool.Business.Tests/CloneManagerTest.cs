﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;

namespace ZPKTool.Business.Tests
{
    /// <summary>
    /// This is a test class for CloneManager and is intended
    /// to contain all CloneManager Unit Tests.
    /// </summary>
    [TestClass]
    public class CloneManagerTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CloneManagerTest"/> class.
        /// </summary>
        public CloneManagerTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes

        //// You can use the following additional attributes as you write your tests:
        ////
        //// Use ClassInitialize to run code before running the first test in the class
        //// [ClassInitialize()]
        //// public static void MyClassInitialize(TestContext testContext) { }
        ////
        //// Use ClassCleanup to run code after all tests in a class have run
        //// [ClassCleanup()]
        //// public static void MyClassCleanup() { }
        ////
        //// Use TestInitialize to run code before running each test 
        //// [TestInitialize()]
        //// public void MyTestInitialize() { }
        //// [TestCleanup()]
        //// public void MyTestCleanup(){}

        #endregion Additional test attributes

        #region Test methods

        /// <summary>
        /// Tests Clone methods using a valid cycle time calculation as input data
        /// </summary>
        [TestMethod]
        public void CloneCycleTimeCalculationTest()
        {
            CycleTimeCalculation calculation1 = new CycleTimeCalculation();
            calculation1.Description = "Description" + DateTime.Now.Ticks;
            calculation1.RawPartDescription = "Raw Part Description" + DateTime.Now.Ticks;
            calculation1.MachiningVolume = 10;
            calculation1.MachiningType = MachiningType.Drilling;
            calculation1.ToleranceType = "Tolerance Type";
            calculation1.SurfaceType = "Surface Type";
            calculation1.TransportTimeFromBuffer = 9;
            calculation1.ToolChangeTime = 30;
            calculation1.TransportClampingTime = 20;
            calculation1.MachiningTime = 15;
            calculation1.TakeOffTime = 33;
            calculation1.IsMasterData = true;
            calculation1.Index = 9;
            calculation1.MachiningCalculationData = new byte[] { 0, 0, 1, 0, 1, 1, 0 };

            CycleTimeCalculation clonedCalculation1 = CloneManager.Clone(calculation1);
            bool result1 = AreEqual(calculation1, clonedCalculation1);

            // Verifies if the cloned cycle time calculation is identical with the calculation
            Assert.IsTrue(result1, "Failed to clone a cycle time calculation");

            // Clone a null cycle time 
            CycleTimeCalculation calculation2 = null;
            CycleTimeCalculation clonedCalculation2 = CloneManager.Clone(calculation2);

            // Verifies if the returned result is null
            Assert.IsNull(clonedCalculation2, "The clone of a null cycle time calculation should be null too.");
        }

        /// <summary>
        /// Tests Clone methods using a valid customer as input data
        /// </summary>
        [TestMethod]
        public void CloneCustomerTest()
        {
            Customer customer1 = new Customer();
            customer1.Name = "Customer" + DateTime.Now.Ticks;
            customer1.Description = "Description Of Customer" + DateTime.Now.Ticks;
            customer1.Number = "Number" + DateTime.Now.Ticks;
            customer1.StreetAddress = "Street Address" + DateTime.Now.Ticks;
            customer1.ZipCode = "123456";
            customer1.City = "City of Customer";
            customer1.FirstContactName = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer1.FirstContactEmail = "customer@gmail.com";
            customer1.FirstContactPhone = "0264567890";
            customer1.FirstContactFax = "0264567891";
            customer1.FirstContactMobile = "0756453234";
            customer1.FirstContactWebAddress = "www.customer.com";
            customer1.SecondContactName = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer1.SecondContactPhone = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer1.SecondContactMobile = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer1.SecondContactWebAddress = "www.test.com";
            customer1.SecondContactEmail = "test@gmail.com";
            customer1.SecondContactFax = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer1.Type = SupplierType.Holding;
            customer1.Country = "Canada";
            customer1.State = "Victoria";
            customer1.IsMasterData = true;

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.SupplierRepository.Add(customer1);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1, dataContext1);
            Customer clonedCustomer1 = cloneManager.Clone(customer1);
            bool result1 = AreEqual(customer1, clonedCustomer1);

            // Verifies if the cloned customer is identical with the customer
            Assert.IsTrue(result1, "Failed to clone a customer");

            // Clone a null customer
            Customer customer2 = null;
            Customer clonedCustomer2 = cloneManager.Clone(customer2);

            Assert.IsNull(clonedCustomer2, "The clone of a null customer should be null too.");
        }

        /// <summary>
        /// Tests Clone methods using a valid manufacturer as input data
        /// </summary>
        [TestMethod]
        public void CloneManufacturerTest()
        {
            Manufacturer manufacturer1 = new Manufacturer();
            manufacturer1.Name = manufacturer1.Guid.ToString();
            manufacturer1.Description = "Description Of Manufacturer" + DateTime.Now.Ticks;
            manufacturer1.IsMasterData = true;

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ManufacturerRepository.Add(manufacturer1);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1, dataContext1);
            Manufacturer clonedManufacturer1 = cloneManager.Clone(manufacturer1);
            bool result1 = AreEqual(manufacturer1, clonedManufacturer1);

            // Verifies if the manufacturer was cloned
            Assert.IsTrue(result1, "Failed to clone a manufacturer.");

            // Clone a null manufacturer
            Manufacturer manufacturer2 = null;
            Manufacturer clonedManufacturer2 = cloneManager.Clone(manufacturer2);

            Assert.IsNull(clonedManufacturer2, "The clone of a null manufacturer should be null too.");
        }

        /// <summary>
        /// Tests Clone methods using a valid consumable
        /// </summary>
        [TestMethod]
        public void CloneConsumableTest()
        {
            Consumable consumable1 = new Consumable();
            consumable1.Name = "Test Consumable" + DateTime.Now.Ticks;
            consumable1.Description = "Description of Consumable";
            consumable1.Price = 200;
            consumable1.Amount = 100;
            consumable1.IsMasterData = true;
            consumable1.IsDeleted = true;

            // Adds a manufacturer to consumable.
            Manufacturer manufacturer1 = new Manufacturer();
            manufacturer1.Name = manufacturer1.Guid.ToString();
            manufacturer1.Description = EncryptionManager.Instance.GenerateRandomString(10, true);
            consumable1.Manufacturer = manufacturer1;

            // Set PriceUnitBase and AmountUnitBase.
            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            consumable1.PriceUnitBase = dataContext1.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Kilogram");
            consumable1.AmountUnitBase = dataContext1.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Kilogram");

            dataContext1.ConsumableRepository.Add(consumable1);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1, dataContext1);
            Consumable clonedConsumable1 = cloneManager.Clone(consumable1);
            bool result1 = AreEqual(consumable1, clonedConsumable1);

            // Verifies if the cloned consumable is identical with the consumable
            Assert.IsTrue(result1, "Failed to clone a consumable.");

            // Clone a null consumable
            Consumable consumable2 = null;
            Consumable clonedConsumable2 = cloneManager.Clone(consumable2);

            Assert.IsNull(clonedConsumable2, "The clone of a null consumable should be null too.");
        }

        /// <summary>
        /// Tests Clone methods using a valid commodity as input data
        /// </summary>
        [TestMethod]
        public void CloneCommodityTest()
        {
            Commodity commodity1 = new Commodity();
            commodity1.Name = "Test Commodity" + DateTime.Now.Ticks;
            commodity1.Price = 1000;
            commodity1.Weight = 10;
            commodity1.RejectRatio = 100;
            commodity1.Remarks = "commodity is used for test";
            commodity1.Amount = 9;
            commodity1.PartNumber = "90";
            commodity1.IsMasterData = true;
            commodity1.IsDeleted = true;

            // Adds a media to commodity.
            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 1, 2, 3, 4 };
            image.Size = image.Content.Length;
            image.OriginalFileName = image.Guid.ToString();
            commodity1.Media = image;

            // Create a manufacturer for commodity.
            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = manufacturer.Guid.ToString();
            manufacturer.Description = EncryptionManager.Instance.GenerateRandomString(15, true);
            commodity1.Manufacturer = manufacturer;

            // Set WeightUnitBase.
            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            commodity1.WeightUnitBase = dataContext1.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Kilogram");

            dataContext1.CommodityRepository.Add(commodity1);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1, dataContext1);
            Commodity cloneCommodity1 = cloneManager.Clone(commodity1);
            bool result1 = AreEqual(commodity1, dataContext1, cloneCommodity1, dataContext1);

            // Verifies if the cloned commodity is identical with the commodity 
            Assert.IsTrue(result1, "Failed to clone a commodity.");

            // Clone a null commodity
            Commodity commodity2 = null;
            Commodity clonedCommodity2 = cloneManager.Clone(commodity2);

            Assert.IsNull(clonedCommodity2, "The clone of a null commodity should be null too.");

            // Clone a commodity that is in Added state
            Commodity commodity3 = new Commodity();
            commodity3.Name = "Test Commodity" + DateTime.Now.Ticks;
            commodity3.Price = 1000;
            commodity3.Weight = 10;
            commodity3.RejectRatio = 100;
            commodity3.Remarks = "commodity is used for test";
            commodity3.Amount = 9;
            commodity3.PartNumber = "90";
            commodity3.IsMasterData = true;
            commodity3.IsDeleted = true;
            dataContext1.CommodityRepository.Add(commodity3);

            Commodity clonedCommodity3 = cloneManager.Clone(commodity3);
            bool result3 = AreEqual(commodity3, dataContext1, clonedCommodity3, dataContext1);

            // Verify that the cloned commodity is identical with its clone
            Assert.IsTrue(result3, "Failed to clone a commodity that is in Added state");

            // Clone a commodity whose media is in Added state
            Commodity commodity4 = new Commodity();
            commodity4.Name = commodity4.Guid.ToString();
            commodity4.Media = image.Copy();
            dataContext1.CommodityRepository.Add(commodity4);

            Commodity clonedCommodity4 = cloneManager.Clone(commodity4);

            // Verify if commodity's media was cloned too
            Assert.IsNotNull(clonedCommodity4.Media, "Failed to clone a media that is in Added state");
        }

        /// <summary>
        /// Tests Clone methods using a valid die
        /// </summary>
        [TestMethod]
        public void CloneDieTest()
        {
            Die die1 = new Die();
            die1.Name = die1.Guid.ToString();
            die1.Remark = "Remark of Die";
            die1.Type = (short)DieType.CastingDie;
            die1.Investment = 100;
            die1.ReusableInvest = 50;
            die1.Wear = 90;
            die1.IsWearInPercentage = true;
            die1.Maintenance = 50;
            die1.IsMaintenanceInPercentage = true;
            die1.LifeTime = 60;
            die1.AllocationRatio = 70;
            die1.DiesetsNumberPaidByCustomer = 80;
            die1.CostAllocationBasedOnPartsPerLifeTime = true;
            die1.CostAllocationNumberOfParts = 1;
            die1.IsMasterData = true;
            die1.IsDeleted = true;

            // Adds media to die.
            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            image.Size = image.Content.Length;
            image.OriginalFileName = image.Guid.ToString();
            die1.Media = image;

            // Adds a manufacturer to die.
            die1.Manufacturer = new Manufacturer();
            die1.Manufacturer.Name = die1.Manufacturer.Guid.ToString();
            die1.Manufacturer.Description = EncryptionManager.Instance.GenerateRandomString(10, true);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.DieRepository.Add(die1);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1, dataContext1);
            Die clonedDie1 = cloneManager.Clone(die1);
            bool result1 = AreEqual(die1, dataContext1, clonedDie1, dataContext1);

            // Verifies if the cloned die is identical with the die
            Assert.IsTrue(result1, "Failed to clone a die");

            // Clone a null die
            Die die2 = null;
            Die clonedDie2 = cloneManager.Clone(die2);

            Assert.IsNull(clonedDie2, "The clone of a null die should be null too.");

            // Clone a die that is in Added state
            Die die3 = new Die();
            die3.Name = die3.Guid.ToString();
            die3.Remark = "Remark of Die";
            die3.Type = (short)DieType.CastingDie;
            die3.Investment = 100;
            die3.ReusableInvest = 50;
            die3.Wear = 90;
            die3.IsWearInPercentage = true;
            die3.Maintenance = 50;
            die3.IsMaintenanceInPercentage = true;
            die3.LifeTime = 60;
            die3.AllocationRatio = 70;
            die3.DiesetsNumberPaidByCustomer = 80;
            die3.CostAllocationBasedOnPartsPerLifeTime = true;
            die3.CostAllocationNumberOfParts = 1;
            die3.IsMasterData = true;
            die3.IsDeleted = true;
            dataContext1.DieRepository.Add(die3);

            Die clonedDie3 = cloneManager.Clone(die3);
            bool result3 = AreEqual(die3, dataContext1, clonedDie3, dataContext1);

            // Verify that the cloned die is identical with its clone
            Assert.IsTrue(result3, "Failed to clone a die that is in Added state");

            // Clone a die whose Media is in Added state
            Die die4 = new Die();
            die4.Name = die4.Guid.ToString();
            die4.Media = image.Copy();
            dataContext1.DieRepository.Add(die4);

            Die clonedDie4 = cloneManager.Clone(die4);

            // Verify that die's media was cloned too
            Assert.IsNotNull(clonedDie4.Media, "Failed to clone a media that is in Added state");
        }

        /// <summary>
        /// Tests Clone methods using a valid raw material as input data
        /// </summary>
        [TestMethod]
        public void CloneRawMaterialTest()
        {
            RawMaterial material1 = new RawMaterial();
            material1.Name = material1.Guid.ToString();
            material1.NormName = "Norm Name Of Raw Material";
            material1.Price = 200;
            material1.RejectRatio = 40;
            material1.Remarks = "Qualitative material";
            material1.ParentWeight = 100;
            material1.Quantity = 200;
            material1.ScrapRefundRatio = 300;
            material1.Loss = 30;
            material1.NameUK = "Plasterboard";
            material1.NameUS = "Plasterboard9";
            material1.Sprue = 300;
            material1.ScrapCalculationType = ScrapCalculationType.Dispose;
            material1.YieldStrength = "1";
            material1.RuptureStrength = "1";
            material1.Density = "1";
            material1.MaxElongation = "1";
            material1.GlassTransitionTemperature = "1";
            material1.Rx = "1";
            material1.Rm = "1";

            // Adds a media to raw material.
            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            image.Size = image.Content.Length;
            image.OriginalFileName = image.Guid.ToString();
            material1.Media = image;

            // Add a manufacturer to created raw material.
            material1.Manufacturer = new Manufacturer();
            material1.Manufacturer.Name = material1.Manufacturer.Guid.ToString();
            material1.Manufacturer.Description = EncryptionManager.Instance.GenerateRandomString(10, true);

            // Set PriceUnitBase,ParentWeightUnitBase and QuantityUnitBase.
            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            material1.PriceUnitBase = dataContext1.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Kilogram");
            material1.ParentWeightUnitBase = dataContext1.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Kilogram");
            material1.QuantityUnitBase = dataContext1.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Kilogram");

            // Set material classifications.
            material1.MaterialsClassificationL1 = new MaterialsClassification();
            material1.MaterialsClassificationL1.Name = material1.MaterialsClassificationL1.Guid.ToString();
            material1.MaterialsClassificationL2 = new MaterialsClassification();
            material1.MaterialsClassificationL2.Name = material1.MaterialsClassificationL2.Guid.ToString();
            material1.MaterialsClassificationL3 = new MaterialsClassification();
            material1.MaterialsClassificationL3.Name = material1.MaterialsClassificationL3.Guid.ToString();
            material1.MaterialsClassificationL4 = new MaterialsClassification();
            material1.MaterialsClassificationL4.Name = material1.MaterialsClassificationL4.Guid.ToString();

            // Set delivery type of raw material.
            material1.DeliveryType = new RawMaterialDeliveryType();
            material1.DeliveryType.Name = material1.DeliveryType.Guid.ToString();

            dataContext1.RawMaterialRepository.Add(material1);
            dataContext1.SaveChanges();

            // Create a user which will be the owner of raw material
            User user = new User();
            user.Name = user.Guid.ToString();
            user.Username = user.Guid.ToString();
            user.Password = EncryptionManager.Instance.EncodeMD5("password");
            material1.SetOwner(user);

            CloneManager cloneManager = new CloneManager(dataContext1, dataContext1);
            RawMaterial clonedMaterial1 = cloneManager.Clone(material1);
            bool result1 = AreEqual(material1, dataContext1, clonedMaterial1, dataContext1);

            // Verifies if the clone of material is identical with the 'material' 
            Assert.IsTrue(result1, "Failed to clone a raw material.");

            // Clone a null raw material
            RawMaterial material2 = null;
            RawMaterial clonedMaterial2 = cloneManager.Clone(material2);

            Assert.IsNull(clonedMaterial2, "The clone of a null raw material should be null too.");

            // Clone a raw material that is in Added state
            RawMaterial material3 = new RawMaterial();
            material3.Name = material3.Guid.ToString();
            material3.Name = material1.Guid.ToString();
            material3.NormName = "Norm Name Of Raw Material";
            material3.Price = 200;
            material3.RejectRatio = 40;
            material3.Remarks = "Qualitative material";
            material3.ParentWeight = 100;
            material3.Quantity = 200;
            material3.ScrapRefundRatio = 300;
            material3.Loss = 30;
            material3.NameUK = "Plasterboard";
            material3.NameUS = "Plasterboard9";
            material3.Sprue = 300;
            material3.ScrapCalculationType = ScrapCalculationType.Dispose;
            material3.YieldStrength = "1";
            material3.RuptureStrength = "1";
            material3.Density = "1";
            material3.MaxElongation = "1";
            material3.GlassTransitionTemperature = "1";
            material3.Rx = "1";
            material3.Rm = "1";
            dataContext1.RawMaterialRepository.Add(material3);

            RawMaterial clonedMaterial3 = cloneManager.Clone(material3);
            bool result3 = AreEqual(material3, dataContext1, clonedMaterial3, dataContext1);

            // Verify that the cloned material is identically with its clone
            Assert.IsTrue(result3, "Failed to clone a raw material that is in Added state");

            // Clone a raw material whose media is in Added state
            RawMaterial material4 = new RawMaterial();
            material4.Name = material4.Guid.ToString();
            material4.Media = image.Copy();
            dataContext1.RawMaterialRepository.Add(material4);

            RawMaterial clonedMaterial4 = cloneManager.Clone(material4);

            // Verify that material's media was cloned
            Assert.IsNotNull(clonedMaterial4.Media, "Failed to clone a media that is in Added state");
        }

        /// <summary>
        /// Tests Clone methods using a valid machine as input data
        /// </summary>
        [TestMethod]
        public void CloneMachineTest()
        {
            Machine machine1 = new Machine();
            machine1.Name = machine1.Guid.ToString();
            machine1.ManufacturingYear = 2000;
            machine1.Index = 99;
            machine1.RegistrationNumber = EncryptionManager.Instance.GenerateRandomString(9, true);
            machine1.DescriptionOfFunction = EncryptionManager.Instance.GenerateRandomString(10, true);
            machine1.FloorSize = 100;
            machine1.WorkspaceArea = 90;
            machine1.PowerConsumption = 105;
            machine1.AirConsumption = 50;
            machine1.WaterConsumption = 100;
            machine1.FullLoadRate = 20;
            machine1.MaxCapacity = 300;
            machine1.OEE = 300;
            machine1.Availability = 30;
            machine1.MachineInvestment = 500;
            machine1.SetupInvestment = 400;
            machine1.AdditionalEquipmentInvestment = 600;
            machine1.FundamentalSetupInvestment = 300;
            machine1.InvestmentRemarks = EncryptionManager.Instance.GenerateRandomString(10, true);
            machine1.ExternalWorkCost = 2500;
            machine1.MaterialCost = 200;
            machine1.ConsumablesCost = 300;
            machine1.RepairsCost = 500;
            machine1.KValue = 20;
            machine1.CalculateWithKValue = true;
            machine1.ManualConsumableCostCalc = true;
            machine1.ManualConsumableCostPercentage = 30;
            machine1.IsProjectSpecific = true;
            machine1.DepreciationPeriod = 20;
            machine1.DepreciationRate = 40;
            machine1.MountingCubeLength = EncryptionManager.Instance.GenerateRandomString(5, true);
            machine1.MountingCubeWidth = EncryptionManager.Instance.GenerateRandomString(5, true);
            machine1.MountingCubeHeight = EncryptionManager.Instance.GenerateRandomString(5, true);
            machine1.MaxDiameterRodPerChuckMaxThickness = EncryptionManager.Instance.GenerateRandomString(5, true);
            machine1.MaxPartsWeightPerCapacity = EncryptionManager.Instance.GenerateRandomString(5, true);
            machine1.LockingForce = EncryptionManager.Instance.GenerateRandomString(5, true);
            machine1.PressCapacity = EncryptionManager.Instance.GenerateRandomString(5, true);
            machine1.MaximumSpeed = EncryptionManager.Instance.GenerateRandomString(5, true);
            machine1.RapidFeedPerCuttingSpeed = EncryptionManager.Instance.GenerateRandomString(5, true);
            machine1.StrokeRate = EncryptionManager.Instance.GenerateRandomString(5, true);
            machine1.Other = EncryptionManager.Instance.GenerateRandomString(5, true);

            // Add media to machine.
            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            image.Size = image.Content.Length;
            image.OriginalFileName = image.Guid.ToString();
            machine1.Media = image;

            // Creates a manufacturer for machine.
            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = manufacturer.Guid.ToString();
            manufacturer.Description = EncryptionManager.Instance.GenerateRandomString(10, true);
            machine1.Manufacturer = manufacturer;

            // Creates a user which will be the Owner of machine
            User user = new User();
            user.Name = user.Guid.ToString();
            user.Username = user.Guid.ToString();
            user.Password = EncryptionManager.Instance.HashSHA256("Password.", user.Salt, user.Guid.ToString());
            user.Roles = Role.Admin;
            machine1.Owner = user;

            // Set machine classifications.
            machine1.MainClassification = new MachinesClassification();
            machine1.MainClassification.Name = machine1.MainClassification.Guid.ToString();
            machine1.TypeClassification = new MachinesClassification();
            machine1.TypeClassification.Name = machine1.TypeClassification.Guid.ToString();
            machine1.SubClassification = new MachinesClassification();
            machine1.SubClassification.Name = machine1.SubClassification.Guid.ToString();
            machine1.ClassificationLevel4 = new MachinesClassification();
            machine1.ClassificationLevel4.Name = machine1.ClassificationLevel4.Guid.ToString();

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.MachineRepository.Add(machine1);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1, dataContext1);
            Machine clonedMachine1 = cloneManager.Clone(machine1);
            bool result1 = AreEqual(machine1, dataContext1, clonedMachine1, dataContext1);

            // Verifies if clone of machine is identical with the machine
            Assert.IsTrue(result1, "Failed to clone a machine.");

            // Clone a null machine
            Machine machine2 = null;
            Machine clonedMachine2 = cloneManager.Clone(machine2);

            Assert.IsNull(clonedMachine2, "The clone of a null machine should be null too.");

            // Clone a machine that is in Added state
            Machine machine3 = new Machine();
            machine3.Name = machine3.Guid.ToString();
            machine3.ManufacturingYear = 2000;
            machine3.Index = 99;
            machine3.RegistrationNumber = EncryptionManager.Instance.GenerateRandomString(9, true);
            machine3.DescriptionOfFunction = EncryptionManager.Instance.GenerateRandomString(10, true);
            machine3.FloorSize = 100;
            machine3.WorkspaceArea = 90;
            machine3.PowerConsumption = 105;
            machine3.AirConsumption = 50;
            machine3.WaterConsumption = 100;
            machine3.FullLoadRate = 20;
            machine3.MaxCapacity = 300;
            machine3.OEE = 300;
            machine3.Availability = 30;
            machine3.MachineInvestment = 500;
            machine3.SetupInvestment = 400;
            machine3.AdditionalEquipmentInvestment = 600;
            machine3.FundamentalSetupInvestment = 300;
            machine3.InvestmentRemarks = EncryptionManager.Instance.GenerateRandomString(10, true);
            machine3.ExternalWorkCost = 2500;
            machine3.MaterialCost = 200;
            machine3.ConsumablesCost = 300;
            machine3.RepairsCost = 500;
            machine3.KValue = 20;
            machine3.CalculateWithKValue = true;
            machine3.ManualConsumableCostCalc = true;
            machine3.ManualConsumableCostPercentage = 30;
            machine3.IsProjectSpecific = true;
            machine3.DepreciationPeriod = 20;
            machine3.DepreciationRate = 40;
            machine3.MountingCubeLength = EncryptionManager.Instance.GenerateRandomString(5, true);
            machine3.MountingCubeWidth = EncryptionManager.Instance.GenerateRandomString(5, true);
            machine3.MountingCubeHeight = EncryptionManager.Instance.GenerateRandomString(5, true);
            machine3.MaxDiameterRodPerChuckMaxThickness = EncryptionManager.Instance.GenerateRandomString(5, true);
            machine3.MaxPartsWeightPerCapacity = EncryptionManager.Instance.GenerateRandomString(5, true);
            machine3.LockingForce = EncryptionManager.Instance.GenerateRandomString(5, true);
            machine3.PressCapacity = EncryptionManager.Instance.GenerateRandomString(5, true);
            machine3.MaximumSpeed = EncryptionManager.Instance.GenerateRandomString(5, true);
            machine3.RapidFeedPerCuttingSpeed = EncryptionManager.Instance.GenerateRandomString(5, true);
            machine3.StrokeRate = EncryptionManager.Instance.GenerateRandomString(5, true);
            machine3.Other = EncryptionManager.Instance.GenerateRandomString(5, true);
            dataContext1.MachineRepository.Add(machine3);

            Machine clonedMachine3 = cloneManager.Clone(machine3);
            bool result3 = AreEqual(machine3, dataContext1, clonedMachine3, dataContext1);

            // Verify if the cloned machine is identically with its clone
            Assert.IsTrue(result3, "Failed to clone a machine that is in Added state");

            // Clone a machine whose media is in Added state
            Machine machine4 = new Machine();
            machine4.Name = machine4.Guid.ToString();
            machine4.Media = image.Copy();
            dataContext1.MachineRepository.Add(machine4);

            Machine clonedMachine4 = cloneManager.Clone(machine4);

            // Verify that machine's media was cloned too
            Assert.IsNotNull(clonedMachine4.Media, "Failed to clone a media that is in Added state");
        }

        /// <summary>
        /// Test Clone methods using a valid process step as input data.
        /// </summary>
        [TestMethod]
        public void ClonePartProcessStepTest()
        {
            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MeasurementUnit timeUnit = dataContext1.EntityContext.MeasurementUnitSet.FirstOrDefault(m => m.Name.Equals("Minute") && !m.IsReleased);
            MeasurementUnit weightUnit = dataContext1.EntityContext.MeasurementUnitSet.FirstOrDefault(m => m.Name.Equals("Gram") && !m.IsReleased);

            ProcessStep partProcessStep1 = new PartProcessStep();
            partProcessStep1.Name = partProcessStep1.Guid.ToString();
            partProcessStep1.Accuracy = (short)ProcessCalculationAccuracy.Calculated;
            partProcessStep1.ProcessTime = 10;
            partProcessStep1.ProcessTimeUnit = timeUnit;
            partProcessStep1.PartsPerCycle = 5;
            partProcessStep1.ScrapAmount = 15;
            partProcessStep1.Rework = 10;
            partProcessStep1.ShiftsPerWeek = 12;
            partProcessStep1.HoursPerShift = 9;
            partProcessStep1.ProductionDaysPerWeek = 4;
            partProcessStep1.ProductionWeeksPerYear = 250;
            partProcessStep1.BatchSize = 100;
            partProcessStep1.SetupsPerBatch = 25;
            partProcessStep1.SetupTime = 19;
            partProcessStep1.SetupTimeUnit = timeUnit;
            partProcessStep1.MaxDownTime = 23;
            partProcessStep1.MaxDownTimeUnit = timeUnit;
            partProcessStep1.ProductionUnskilledLabour = 45;
            partProcessStep1.ProductionSkilledLabour = 50;
            partProcessStep1.ProductionForeman = 23;
            partProcessStep1.ProductionTechnicians = 55;
            partProcessStep1.ProductionEngineers = 123;
            partProcessStep1.SetupUnskilledLabour = 235;
            partProcessStep1.SetupSkilledLabour = 12;
            partProcessStep1.SetupForeman = 355;
            partProcessStep1.SetupTechnicians = 124;
            partProcessStep1.SetupEngineers = 235;
            partProcessStep1.IsExternal = true;
            partProcessStep1.Price = 199;
            partProcessStep1.ExceedShiftCost = true;
            partProcessStep1.ExtraShiftsNumber = 700;
            partProcessStep1.ShiftCostExceedRatio = 10;
            partProcessStep1.Description = "Description" + DateTime.Now.Ticks;

            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 1, 2, 3, 4, 5 };
            image.Size = image.Content.Length;
            image.OriginalFileName = image.Guid.ToString();
            partProcessStep1.Media = image;

            CycleTimeCalculation cycleTimeCalculation = new CycleTimeCalculation();
            cycleTimeCalculation.Description = cycleTimeCalculation.Guid.ToString();
            cycleTimeCalculation.RawPartDescription = "Description" + DateTime.Now.Ticks;
            cycleTimeCalculation.MachiningVolume = 10;
            cycleTimeCalculation.MachiningType = MachiningType.Boring;
            cycleTimeCalculation.ToleranceType = "Tolerance" + DateTime.Now.Ticks;
            cycleTimeCalculation.SurfaceType = "Surface" + DateTime.Now.Ticks;
            cycleTimeCalculation.TransportTimeFromBuffer = 70;
            cycleTimeCalculation.ToolChangeTime = 30;
            cycleTimeCalculation.TransportClampingTime = 15;
            cycleTimeCalculation.MachiningTime = 23;
            cycleTimeCalculation.TakeOffTime = 120;
            cycleTimeCalculation.MachiningCalculationData = new byte[] { 1, 2, 3, 4 };
            partProcessStep1.CycleTimeCalculations.Add(cycleTimeCalculation);
            partProcessStep1.CycleTimeUnit = timeUnit;
            partProcessStep1.CycleTime = 1200;
            partProcessStep1.Process = new Process();

            ProcessStepsClassification stepSubtype = new ProcessStepsClassification();
            stepSubtype.Name = stepSubtype.Guid.ToString();
            partProcessStep1.SubType = stepSubtype;

            ProcessStepsClassification stepType = new ProcessStepsClassification();
            stepType.Name = stepType.Guid.ToString();
            partProcessStep1.Type = stepType;

            Machine machine = new Machine();
            machine.Name = machine.Guid.ToString();
            machine.ManufacturingYear = 2000;
            machine.DepreciationPeriod = 20;
            machine.DepreciationRate = 10;
            machine.RegistrationNumber = EncryptionManager.Instance.GenerateRandomString(10, true);
            machine.DescriptionOfFunction = EncryptionManager.Instance.GenerateRandomString(15, true);
            machine.FloorSize = 10;
            machine.WorkspaceArea = 25;
            machine.PowerConsumption = 100;
            machine.AirConsumption = 230;
            machine.WaterConsumption = 300;
            machine.FullLoadRate = 50;
            machine.MaxCapacity = 1500;
            machine.OEE = 10;
            machine.Availability = 30;
            machine.MachineInvestment = 20;
            machine.SetupInvestment = 30;
            machine.AdditionalEquipmentInvestment = 230;
            machine.FundamentalSetupInvestment = 45;
            machine.InvestmentRemarks = "Remark" + DateTime.Now.Ticks;
            machine.ExternalWorkCost = 1230;
            machine.MaterialCost = 1000;
            machine.ConsumablesCost = 2300;
            machine.RepairsCost = 2345;
            machine.KValue = 1200;
            machine.CalculateWithKValue = true;
            machine.MountingCubeLength = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MountingCubeWidth = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MountingCubeHeight = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MaxDiameterRodPerChuckMaxThickness = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MaxPartsWeightPerCapacity = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.LockingForce = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.PressCapacity = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MaximumSpeed = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.RapidFeedPerCuttingSpeed = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.StrokeRate = EncryptionManager.Instance.GenerateRandomString(2, true);
            machine.Other = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.Media = image.Copy();
            machine.Media.OriginalFileName = machine.Media.Guid.ToString();
            partProcessStep1.Machines.Add(machine);

            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = manufacturer.Guid.ToString();
            manufacturer.Description = EncryptionManager.Instance.EncodeMD5("Password");
            machine.Manufacturer = manufacturer;

            // Add a classification level 4 to machine
            MachinesClassification machineClassificationLevel4 = new MachinesClassification();
            machineClassificationLevel4.Name = machineClassificationLevel4.Guid.ToString();
            machine.ClassificationLevel4 = machineClassificationLevel4;

            // Add subtype classification to machine
            MachinesClassification machineSubClassification = new MachinesClassification();
            machineSubClassification.Name = machineSubClassification.Guid.ToString();
            machine.SubClassification = machineSubClassification;

            // Add type classification to machine
            MachinesClassification machineTypeClassification = new MachinesClassification();
            machineTypeClassification.Name = machineTypeClassification.Guid.ToString();
            machine.TypeClassification = machineTypeClassification;

            // Add main classification to machine
            MachinesClassification machineMainClassification = new MachinesClassification();
            machineMainClassification.Name = machineMainClassification.Guid.ToString();
            machine.MainClassification = machineMainClassification;

            Consumable consumable = new Consumable();
            consumable.Name = consumable.Guid.ToString();
            consumable.Description = EncryptionManager.Instance.GenerateRandomString(17, true);
            consumable.Price = 1200;
            consumable.PriceUnitBase = weightUnit;
            consumable.Amount = 12;
            consumable.Manufacturer = manufacturer.Copy();
            consumable.Manufacturer.Name = consumable.Manufacturer.Guid.ToString();
            partProcessStep1.Consumables.Add(consumable);

            Die die = new Die();
            die.Name = die.Guid.ToString();
            die.Type = (short)DieType.CastingDie;
            die.Investment = 100;
            die.ReusableInvest = 1200;
            die.Wear = 300;
            die.Maintenance = 1200;
            die.LifeTime = 300;
            die.AllocationRatio = 2300;
            die.DiesetsNumberPaidByCustomer = 1300;
            die.CostAllocationBasedOnPartsPerLifeTime = true;
            die.CostAllocationNumberOfParts = 234;
            die.Remark = EncryptionManager.Instance.GenerateRandomString(15, true);
            die.Manufacturer = manufacturer.Copy();
            die.Manufacturer.Name = die.Manufacturer.Guid.ToString();
            die.Media = image.Copy();
            die.Media.OriginalFileName = die.Media.Guid.ToString();
            partProcessStep1.Dies.Add(die);

            dataContext1.ProcessStepRepository.Add(partProcessStep1);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1, dataContext1);
            ProcessStep clonedStep1 = cloneManager.Clone(partProcessStep1, typeof(PartProcessStep));
            bool result1 = AreEqual(partProcessStep1, dataContext1, clonedStep1, dataContext1, false);

            // Verifies if the clone of step is identical with the cloned step
            Assert.IsTrue(result1, "Failed to clone a process step");

            // Clone a valid part step given a null cloneType
            ProcessStep clonedStep2 = cloneManager.Clone(partProcessStep1, null);
            bool result2 = AreEqual(partProcessStep1, dataContext1, clonedStep2, dataContext1, false);

            Assert.IsTrue(result2, "Failed to clone a part process step if the given cloneType was null.");

            // Clone a null step
            PartProcessStep partProcessStep2 = null;
            ProcessStep clonedStep3 = cloneManager.Clone(partProcessStep2, typeof(PartProcessStep));

            Assert.IsNull(clonedStep3, "The clone of a null process step should be null too");
        }

        /// <summary>
        /// A test method for ProcessStep Clone(ProcessStep source, Type cloneType) method using an assembly process step as input data.
        /// </summary>
        [TestMethod]
        public void CloneAssemblyProcessStepTest()
        {
            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();

            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = manufacturer.Guid.ToString();
            manufacturer.Description = EncryptionManager.Instance.GenerateRandomString(10, true);
            assembly.Manufacturer = manufacturer;

            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 1, 8, 6, 7, 9 };
            image.Size = image.Content.Length;
            image.OriginalFileName = image.Guid.ToString();

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MeasurementUnit timeUnit = dataContext1.EntityContext.MeasurementUnitSet.FirstOrDefault(m => m.Name.Equals("Minute") && !m.IsReleased);
            MeasurementUnit weightUnit = dataContext1.EntityContext.MeasurementUnitSet.FirstOrDefault(m => m.Name.Equals("Gram") && !m.IsReleased);

            ProcessStep step1 = new AssemblyProcessStep();
            step1.Name = step1.Guid.ToString();
            step1.Accuracy = (short)ProcessCalculationAccuracy.Calculated;
            step1.ProcessTime = 10;
            step1.ProcessTimeUnit = timeUnit;
            step1.PartsPerCycle = 5;
            step1.ScrapAmount = 15;
            step1.Rework = 10;
            step1.ShiftsPerWeek = 12;
            step1.HoursPerShift = 9;
            step1.ProductionDaysPerWeek = 4;
            step1.ProductionWeeksPerYear = 250;
            step1.BatchSize = 100;
            step1.SetupsPerBatch = 25;
            step1.SetupTime = 19;
            step1.SetupTimeUnit = timeUnit;
            step1.MaxDownTime = 23;
            step1.MaxDownTimeUnit = timeUnit;
            step1.ProductionUnskilledLabour = 45;
            step1.ProductionSkilledLabour = 50;
            step1.ProductionForeman = 23;
            step1.ProductionTechnicians = 55;
            step1.ProductionEngineers = 123;
            step1.SetupUnskilledLabour = 235;
            step1.SetupSkilledLabour = 12;
            step1.SetupForeman = 355;
            step1.SetupTechnicians = 124;
            step1.SetupEngineers = 235;
            step1.IsExternal = true;
            step1.Price = 199;
            step1.ExceedShiftCost = true;
            step1.ExtraShiftsNumber = 700;
            step1.ShiftCostExceedRatio = 10;
            step1.Description = "Description" + DateTime.Now.Ticks;
            step1.Media = image;

            CycleTimeCalculation cycleTimeCalculation = new CycleTimeCalculation();
            cycleTimeCalculation.Description = cycleTimeCalculation.Guid.ToString();
            cycleTimeCalculation.RawPartDescription = "Description" + DateTime.Now.Ticks;
            cycleTimeCalculation.MachiningVolume = 10;
            cycleTimeCalculation.MachiningType = MachiningType.Boring;
            cycleTimeCalculation.ToleranceType = "Tolerance" + DateTime.Now.Ticks;
            cycleTimeCalculation.SurfaceType = "Surface" + DateTime.Now.Ticks;
            cycleTimeCalculation.TransportTimeFromBuffer = 70;
            cycleTimeCalculation.ToolChangeTime = 30;
            cycleTimeCalculation.TransportClampingTime = 15;
            cycleTimeCalculation.MachiningTime = 23;
            cycleTimeCalculation.TakeOffTime = 120;
            cycleTimeCalculation.MachiningCalculationData = new byte[] { 1, 2, 3, 4 };
            step1.CycleTimeCalculations.Add(cycleTimeCalculation);
            step1.CycleTimeUnit = timeUnit;
            step1.CycleTime = 1200;
            assembly.Process = new Process();
            assembly.Process.Steps.Add(step1);

            ProcessStepsClassification stepSubtype = new ProcessStepsClassification();
            stepSubtype.Name = stepSubtype.Guid.ToString();
            step1.SubType = stepSubtype;

            ProcessStepsClassification stepType = new ProcessStepsClassification();
            stepType.Name = stepType.Guid.ToString();
            step1.Type = stepType;

            Machine machine = new Machine();
            machine.Name = machine.Guid.ToString();
            machine.ManufacturingYear = 2000;
            machine.DepreciationPeriod = 20;
            machine.DepreciationRate = 10;
            machine.RegistrationNumber = EncryptionManager.Instance.GenerateRandomString(10, true);
            machine.DescriptionOfFunction = EncryptionManager.Instance.GenerateRandomString(15, true);
            machine.FloorSize = 10;
            machine.WorkspaceArea = 25;
            machine.PowerConsumption = 100;
            machine.AirConsumption = 230;
            machine.WaterConsumption = 300;
            machine.FullLoadRate = 50;
            machine.MaxCapacity = 1500;
            machine.OEE = 10;
            machine.Availability = 30;
            machine.MachineInvestment = 20;
            machine.SetupInvestment = 30;
            machine.AdditionalEquipmentInvestment = 230;
            machine.FundamentalSetupInvestment = 45;
            machine.InvestmentRemarks = "Remark" + DateTime.Now.Ticks;
            machine.ExternalWorkCost = 1230;
            machine.MaterialCost = 1000;
            machine.ConsumablesCost = 2300;
            machine.RepairsCost = 2345;
            machine.KValue = 1200;
            machine.CalculateWithKValue = true;
            machine.MountingCubeLength = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MountingCubeWidth = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MountingCubeHeight = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MaxDiameterRodPerChuckMaxThickness = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MaxPartsWeightPerCapacity = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.LockingForce = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.PressCapacity = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MaximumSpeed = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.RapidFeedPerCuttingSpeed = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.StrokeRate = EncryptionManager.Instance.GenerateRandomString(2, true);
            machine.Other = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.Manufacturer = manufacturer.Copy();
            machine.Manufacturer.Name = machine.Manufacturer.Guid.ToString();
            machine.Media = image.Copy();
            machine.Media.OriginalFileName = machine.Media.Guid.ToString();
            step1.Machines.Add(machine);

            // Add a classification level 4 to machine
            MachinesClassification machineClassificationLevel4 = new MachinesClassification();
            machineClassificationLevel4.Name = machineClassificationLevel4.Guid.ToString();
            machine.ClassificationLevel4 = machineClassificationLevel4;

            // Add subtype classification to machine
            MachinesClassification machineSubClassification = new MachinesClassification();
            machineSubClassification.Name = machineSubClassification.Guid.ToString();
            machine.SubClassification = machineSubClassification;

            // Add type classification to machine
            MachinesClassification machineTypeClassification = new MachinesClassification();
            machineTypeClassification.Name = machineTypeClassification.Guid.ToString();
            machine.TypeClassification = machineTypeClassification;

            // Add main classification to machine
            MachinesClassification machineMainClassification = new MachinesClassification();
            machineMainClassification.Name = machineMainClassification.Guid.ToString();
            machine.MainClassification = machineMainClassification;

            Commodity commodity = new Commodity();
            commodity.Name = commodity.Guid.ToString();
            commodity.PartNumber = EncryptionManager.Instance.GenerateRandomString(11, true);
            commodity.Price = 300;
            commodity.Amount = 2;
            commodity.RejectRatio = 1;
            commodity.Weight = 20;
            commodity.WeightUnitBase = weightUnit;
            commodity.Remarks = "Remark" + DateTime.Now.Ticks;
            commodity.Manufacturer = manufacturer.Copy();
            commodity.Manufacturer.Name = commodity.Manufacturer.Guid.ToString();
            commodity.Media = image;
            commodity.Media.OriginalFileName = commodity.Media.Guid.ToString();
            step1.Commodities.Add(commodity);

            Consumable consumable = new Consumable();
            consumable.Name = consumable.Guid.ToString();
            consumable.Description = EncryptionManager.Instance.GenerateRandomString(17, true);
            consumable.Price = 1200;
            consumable.PriceUnitBase = weightUnit;
            consumable.Amount = 12;
            consumable.Manufacturer = manufacturer.Copy();
            consumable.Manufacturer.Name = consumable.Manufacturer.Guid.ToString();
            step1.Consumables.Add(consumable);

            Die die = new Die();
            die.Name = die.Guid.ToString();
            die.Type = (short)DieType.CastingDie;
            die.Investment = 100;
            die.ReusableInvest = 1200;
            die.Wear = 300;
            die.Maintenance = 1200;
            die.LifeTime = 300;
            die.AllocationRatio = 2300;
            die.DiesetsNumberPaidByCustomer = 1300;
            die.CostAllocationBasedOnPartsPerLifeTime = true;
            die.CostAllocationNumberOfParts = 234;
            die.Remark = EncryptionManager.Instance.GenerateRandomString(15, true);
            die.Manufacturer = manufacturer.Copy();
            die.Manufacturer.Name = die.Manufacturer.Guid.ToString();
            die.Media = image.Copy();
            die.Media.OriginalFileName = die.Media.Guid.ToString();
            step1.Dies.Add(die);

            Assembly assembly2 = new Assembly();
            assembly2.Name = assembly2.Guid.ToString();
            assembly2.Manufacturer = manufacturer.Copy();
            assembly2.Manufacturer.Name = assembly2.Manufacturer.Guid.ToString();
            assembly2.CountrySettings = new CountrySetting();
            assembly2.OverheadSettings = new OverheadSetting();
            assembly.Subassemblies.Add(assembly2);

            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.CountrySettings = new CountrySetting();
            part.OverheadSettings = new OverheadSetting();
            part.Manufacturer = manufacturer.Copy();
            part.Manufacturer.Name = part.Manufacturer.Guid.ToString();
            assembly.Parts.Add(part);

            ProcessStepPartAmount partAmount = new ProcessStepPartAmount();
            partAmount.Part = part;
            partAmount.Amount = 9;
            step1.PartAmounts.Add(partAmount);

            ProcessStepAssemblyAmount assemblyAmount = new ProcessStepAssemblyAmount();
            assemblyAmount.Assembly = assembly2;
            assemblyAmount.Amount = 3;
            step1.AssemblyAmounts.Add(assemblyAmount);

            dataContext1.AssemblyRepository.Add(assembly);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1, dataContext1);
            ProcessStep clonedStep1 = cloneManager.Clone(step1, typeof(AssemblyProcessStep));
            bool result1 = AreEqual(step1, dataContext1, clonedStep1, dataContext1, false);

            // Verifies if the clone of step is identical with the cloned process step
            Assert.IsTrue(result1, "Failed to clone an assembly process step.");

            // Clone a step given a null cloneType as input data
            ProcessStep clonedStep2 = cloneManager.Clone(step1, null);
            bool result2 = AreEqual(step1, dataContext1, clonedStep2, dataContext1, false);

            Assert.IsTrue(result2, "Failed to clone an assembly process step if the 'cloneType' input data was set to null.");

            // Clone a null step
            AssemblyProcessStep step2 = null;
            ProcessStep clonedStep3 = cloneManager.Clone(step2, typeof(AssemblyProcessStep));

            Assert.IsNull(clonedStep3, "The clone of a null process step should be null too.");
        }

        /// <summary>
        /// Tests Clone methods using a valid part as input data
        /// </summary>
        [TestMethod]
        public void ClonePartTest()
        {
            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.Number = EncryptionManager.Instance.GenerateRandomString(10, true);
            part1.Version = 1;
            part1.Description = "Description" + DateTime.Now.Ticks;
            part1.CalculationStatus = PartCalculationStatus.Approved;
            part1.CalculationAccuracy = PartCalculationAccuracy.FineCalculation;
            part1.CalculationVariant = "9";
            part1.BatchSizePerYear = 10;
            part1.YearlyProductionQuantity = 90;
            part1.LifeTime = 50;
            part1.ManufacturingCountry = "Country" + DateTime.Now.Ticks;
            part1.ManufacturingSupplier = "State" + DateTime.Now.Ticks;
            part1.PurchasePrice = 1500;
            part1.DelivertType = PartDeliveryType.CIF;
            part1.AssetRate = 200;
            part1.TargetPrice = 2000;
            part1.SBMActive = true;
            part1.IsExternal = true;
            part1.AdditionalRemarks = "Remarks" + DateTime.Now.Ticks;
            part1.AdditionalDescription = "Additional Description" + DateTime.Now.Ticks;
            part1.ManufacturerDescription = "Manufacturer Description" + DateTime.Now.Ticks;
            part1.OverheadSettings = new OverheadSetting();
            part1.CountrySettings = new CountrySetting();
            part1.IsDeleted = true;

            // Add media to part.
            Media video = new Media();
            video.Type = MediaType.Video;
            video.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            video.Size = video.Content.Length;
            video.OriginalFileName = video.Guid.ToString();
            part1.Media.Add(video);

            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 1, 2, 9, 9, 9 };
            image.OriginalFileName = image.Guid.ToString();
            image.Size = image.Content.Length;
            part1.Media.Add(image);

            Media document = new Media();
            document.Type = MediaType.Document;
            document.Content = new byte[] { 1, 1, 1 };
            document.Size = document.Content.Length;
            document.OriginalFileName = document.Guid.ToString();
            part1.Media.Add(document);

            User user1 = new User();
            user1.Name = user1.Guid.ToString();
            user1.Username = EncryptionManager.Instance.GenerateRandomString(5, true);
            user1.Password = EncryptionManager.Instance.HashSHA256("Password.", user1.Salt, user1.Guid.ToString());
            user1.Roles = Role.Admin;
            part1.CalculatorUser = user1;

            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = manufacturer.Guid.ToString();
            manufacturer.Description = "Description" + DateTime.Now.Ticks;
            part1.Manufacturer = manufacturer;

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MeasurementUnit weightUnit = dataContext1.EntityContext.MeasurementUnitSet.FirstOrDefault(m => m.Name.Equals("Gram") && !m.IsReleased);

            Commodity commodity = new Commodity();
            commodity.Name = commodity.Guid.ToString();
            commodity.PartNumber = EncryptionManager.Instance.GenerateRandomString(11, true);
            commodity.Price = 300;
            commodity.Amount = 2;
            commodity.RejectRatio = 1;
            commodity.Weight = 20;
            commodity.WeightUnitBase = weightUnit;
            commodity.Remarks = "Remark" + DateTime.Now.Ticks;
            commodity.Manufacturer = manufacturer.Copy();
            commodity.Manufacturer.Name = commodity.Manufacturer.Guid.ToString();
            commodity.Media = image.Copy();
            commodity.Media.OriginalFileName = commodity.Media.Guid.ToString();
            part1.Commodities.Add(commodity);

            RawMaterial rawMaterial = new RawMaterial();
            rawMaterial.Name = rawMaterial.Guid.ToString();
            rawMaterial.NameUK = "UK" + DateTime.Now.Ticks;
            rawMaterial.NameUS = "US" + DateTime.Now.Ticks;
            rawMaterial.NormName = "Norm" + DateTime.Now.Ticks;
            rawMaterial.ScrapCalculationType = (short)ScrapCalculationType.Yield;
            rawMaterial.Price = 1200;
            rawMaterial.PriceUnitBase = weightUnit;
            rawMaterial.ParentWeight = 10;
            rawMaterial.ParentWeightUnitBase = weightUnit;
            rawMaterial.Sprue = 1;
            rawMaterial.Remarks = "Remark" + DateTime.Now.Ticks;
            rawMaterial.ScrapRefundRatio = 150;
            rawMaterial.RejectRatio = 1;
            rawMaterial.YieldStrength = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.RuptureStrength = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.Density = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.MaxElongation = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.GlassTransitionTemperature = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.Rx = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.Rm = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.Manufacturer = manufacturer.Copy();
            rawMaterial.Manufacturer.Name = rawMaterial.Manufacturer.Guid.ToString();
            rawMaterial.Media = image.Copy();
            rawMaterial.Media.OriginalFileName = rawMaterial.Media.Guid.ToString();
            part1.RawMaterials.Add(rawMaterial);

            RawMaterialDeliveryType deliveryType = new RawMaterialDeliveryType();
            deliveryType.Name = deliveryType.Guid.ToString();
            rawMaterial.DeliveryType = deliveryType;

            MaterialsClassification materialClassificationLevel4 = new MaterialsClassification();
            materialClassificationLevel4.Name = materialClassificationLevel4.Guid.ToString();
            rawMaterial.MaterialsClassificationL4 = materialClassificationLevel4;

            MaterialsClassification materialClassificationLevel3 = new MaterialsClassification();
            materialClassificationLevel3.Name = materialClassificationLevel3.Guid.ToString();
            rawMaterial.MaterialsClassificationL3 = materialClassificationLevel3;

            MaterialsClassification materialClassificationLevel2 = new MaterialsClassification();
            materialClassificationLevel2.Name = materialClassificationLevel2.Guid.ToString();
            rawMaterial.MaterialsClassificationL2 = materialClassificationLevel2;

            MaterialsClassification materialClassificationLevel1 = new MaterialsClassification();
            materialClassificationLevel1.Name = materialClassificationLevel1.Guid.ToString();
            rawMaterial.MaterialsClassificationL1 = materialClassificationLevel1;

            MeasurementUnit timeUnit = dataContext1.EntityContext.MeasurementUnitSet.FirstOrDefault(m => m.Name.Equals("Minute") && !m.IsReleased);

            ProcessStep step1 = new PartProcessStep();
            step1.Name = step1.Guid.ToString();
            step1.Accuracy = (short)ProcessCalculationAccuracy.Calculated;
            step1.ProcessTime = 10;
            step1.ProcessTimeUnit = timeUnit;
            step1.PartsPerCycle = 5;
            step1.ScrapAmount = 15;
            step1.Rework = 10;
            step1.ShiftsPerWeek = 12;
            step1.HoursPerShift = 9;
            step1.ProductionDaysPerWeek = 4;
            step1.ProductionWeeksPerYear = 250;
            step1.BatchSize = 100;
            step1.SetupsPerBatch = 25;
            step1.SetupTime = 19;
            step1.SetupTimeUnit = timeUnit;
            step1.MaxDownTime = 23;
            step1.MaxDownTimeUnit = timeUnit;
            step1.ProductionUnskilledLabour = 45;
            step1.ProductionSkilledLabour = 50;
            step1.ProductionForeman = 23;
            step1.ProductionTechnicians = 55;
            step1.ProductionEngineers = 123;
            step1.SetupUnskilledLabour = 235;
            step1.SetupSkilledLabour = 12;
            step1.SetupForeman = 355;
            step1.SetupTechnicians = 124;
            step1.SetupEngineers = 235;
            step1.IsExternal = true;
            step1.Price = 199;
            step1.ExceedShiftCost = true;
            step1.ExtraShiftsNumber = 700;
            step1.ShiftCostExceedRatio = 10;
            step1.Description = "Description" + DateTime.Now.Ticks;
            step1.Media = image.Copy();
            step1.Media.OriginalFileName = step1.Media.Guid.ToString();

            CycleTimeCalculation cycleTimeCalculation = new CycleTimeCalculation();
            cycleTimeCalculation.Description = cycleTimeCalculation.Guid.ToString();
            cycleTimeCalculation.RawPartDescription = "Description" + DateTime.Now.Ticks;
            cycleTimeCalculation.MachiningVolume = 10;
            cycleTimeCalculation.MachiningType = MachiningType.Boring;
            cycleTimeCalculation.ToleranceType = "Tolerance" + DateTime.Now.Ticks;
            cycleTimeCalculation.SurfaceType = "Surface" + DateTime.Now.Ticks;
            cycleTimeCalculation.TransportTimeFromBuffer = 70;
            cycleTimeCalculation.ToolChangeTime = 30;
            cycleTimeCalculation.TransportClampingTime = 15;
            cycleTimeCalculation.MachiningTime = 23;
            cycleTimeCalculation.TakeOffTime = 120;
            cycleTimeCalculation.MachiningCalculationData = new byte[] { 1, 2, 3, 4 };
            step1.CycleTimeCalculations.Add(cycleTimeCalculation);
            step1.CycleTimeUnit = timeUnit;
            step1.CycleTime = 1200;
            part1.Process = new Process();
            part1.Process.Steps.Add(step1);

            ProcessStepsClassification stepSubtype = new ProcessStepsClassification();
            stepSubtype.Name = stepSubtype.Guid.ToString();
            step1.SubType = stepSubtype;

            ProcessStepsClassification stepType = new ProcessStepsClassification();
            stepType.Name = stepType.Guid.ToString();
            step1.Type = stepType;

            Machine machine = new Machine();
            machine.Name = machine.Guid.ToString();
            machine.ManufacturingYear = 2000;
            machine.DepreciationPeriod = 20;
            machine.DepreciationRate = 10;
            machine.RegistrationNumber = EncryptionManager.Instance.GenerateRandomString(10, true);
            machine.DescriptionOfFunction = EncryptionManager.Instance.GenerateRandomString(15, true);
            machine.FloorSize = 10;
            machine.WorkspaceArea = 25;
            machine.PowerConsumption = 100;
            machine.AirConsumption = 230;
            machine.WaterConsumption = 300;
            machine.FullLoadRate = 50;
            machine.MaxCapacity = 1500;
            machine.OEE = 10;
            machine.Availability = 30;
            machine.MachineInvestment = 20;
            machine.SetupInvestment = 30;
            machine.AdditionalEquipmentInvestment = 230;
            machine.FundamentalSetupInvestment = 45;
            machine.InvestmentRemarks = "Remark" + DateTime.Now.Ticks;
            machine.ExternalWorkCost = 1230;
            machine.MaterialCost = 1000;
            machine.ConsumablesCost = 2300;
            machine.RepairsCost = 2345;
            machine.KValue = 1200;
            machine.CalculateWithKValue = true;
            machine.MountingCubeLength = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MountingCubeWidth = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MountingCubeHeight = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MaxDiameterRodPerChuckMaxThickness = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MaxPartsWeightPerCapacity = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.LockingForce = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.PressCapacity = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MaximumSpeed = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.RapidFeedPerCuttingSpeed = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.StrokeRate = EncryptionManager.Instance.GenerateRandomString(2, true);
            machine.Other = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.Manufacturer = manufacturer.Copy();
            machine.Manufacturer.Name = machine.Manufacturer.Guid.ToString();
            machine.Media = image.Copy();
            machine.Media.OriginalFileName = machine.Media.Guid.ToString();
            step1.Machines.Add(machine);

            // Add a classification level 4 to machine
            MachinesClassification machineClassificationLevel4 = new MachinesClassification();
            machineClassificationLevel4.Name = machineClassificationLevel4.Guid.ToString();
            machine.ClassificationLevel4 = machineClassificationLevel4;

            // Add subtype classification to machine
            MachinesClassification machineSubClassification = new MachinesClassification();
            machineSubClassification.Name = machineSubClassification.Guid.ToString();
            machine.SubClassification = machineSubClassification;

            // Add type classification to machine
            MachinesClassification machineTypeClassification = new MachinesClassification();
            machineTypeClassification.Name = machineTypeClassification.Guid.ToString();
            machine.TypeClassification = machineTypeClassification;

            // Add main classification to machine
            MachinesClassification machineMainClassification = new MachinesClassification();
            machineMainClassification.Name = machineMainClassification.Guid.ToString();
            machine.MainClassification = machineMainClassification;

            Consumable consumable = new Consumable();
            consumable.Name = consumable.Guid.ToString();
            consumable.Description = EncryptionManager.Instance.GenerateRandomString(17, true);
            consumable.Price = 1200;
            consumable.PriceUnitBase = weightUnit;
            consumable.Amount = 12;
            consumable.Manufacturer = manufacturer.Copy();
            consumable.Manufacturer.Name = consumable.Manufacturer.Guid.ToString();
            step1.Consumables.Add(consumable);

            Die die = new Die();
            die.Name = die.Guid.ToString();
            die.Type = (short)DieType.CastingDie;
            die.Investment = 100;
            die.ReusableInvest = 1200;
            die.Wear = 300;
            die.Maintenance = 1200;
            die.LifeTime = 300;
            die.AllocationRatio = 2300;
            die.DiesetsNumberPaidByCustomer = 1300;
            die.CostAllocationBasedOnPartsPerLifeTime = true;
            die.CostAllocationNumberOfParts = 234;
            die.Remark = EncryptionManager.Instance.GenerateRandomString(15, true);
            die.Manufacturer = manufacturer.Copy();
            die.Manufacturer.Name = die.Manufacturer.Guid.ToString();
            die.Media = image.Copy();
            die.Media.OriginalFileName = die.Media.Guid.ToString();
            step1.Dies.Add(die);
            part1.SetOwner(user1);

            dataContext1.PartRepository.Add(part1);
            dataContext1.SaveChanges();

            // Clone the part including video.
            CloneManager cloneManager = new CloneManager(dataContext1, dataContext1);
            Part clonedPart1 = cloneManager.Clone(part1, true, false);
            bool result1 = AreEqual(part1, dataContext1, clonedPart1, dataContext1, true);

            // Verifies if the clone of part is identical with the part
            Assert.IsTrue(result1, "Failed to clone a part including video.");

            // Clone the part without including its video.
            Part clonedPart2 = cloneManager.Clone(part1, false, false);
            bool result2 = AreEqual(part1, dataContext1, clonedPart2, dataContext1, false);

            // Verifies if the clone of part is identical with the part
            Assert.IsTrue(result2, "Failed to clone a part without including its video");

            // Clone a null part
            Part part2 = null;
            Part clonedPart3 = cloneManager.Clone(part2);

            Assert.IsNull(clonedPart3, "The clone of a null part should be null too.");

            // Clone a part without media that is in Added state
            Part part3 = new Part();
            part3.Name = part3.Guid.ToString();
            part3.Number = EncryptionManager.Instance.GenerateRandomString(10, true);
            part3.Version = 1;
            part3.Description = "Description" + DateTime.Now.Ticks;
            part3.CalculationStatus = PartCalculationStatus.Approved;
            part3.CalculationAccuracy = PartCalculationAccuracy.FineCalculation;
            part3.CalculationVariant = "9";
            part3.BatchSizePerYear = 10;
            part3.YearlyProductionQuantity = 90;
            part3.LifeTime = 50;
            part3.ManufacturingCountry = "Country" + DateTime.Now.Ticks;
            part3.ManufacturingSupplier = "State" + DateTime.Now.Ticks;
            part3.PurchasePrice = 1500;
            part3.DelivertType = PartDeliveryType.CIF;
            part3.AssetRate = 200;
            part3.TargetPrice = 2000;
            part3.SBMActive = true;
            part3.IsExternal = true;
            part3.AdditionalRemarks = "Remarks" + DateTime.Now.Ticks;
            part3.AdditionalDescription = "Additional Description" + DateTime.Now.Ticks;
            part3.ManufacturerDescription = "Manufacturer Description" + DateTime.Now.Ticks;
            part3.OverheadSettings = new OverheadSetting();
            part3.CountrySettings = new CountrySetting();
            part3.IsDeleted = true;
            dataContext1.PartRepository.Add(part3);

            Part clonedPart4 = cloneManager.Clone(part3);
            bool result3 = AreEqual(part3, dataContext1, clonedPart4, dataContext1, true);

            // Verify that the cloned part is identically with its clone
            Assert.IsTrue(result3, "Failed to clone a part that is in Added state");

            // Clone a part whose media is in Added state
            Part part4 = new Part();
            part4.Name = part4.Guid.ToString();
            part4.OverheadSettings = new OverheadSetting();
            part4.CountrySettings = new CountrySetting();
            part4.Media.Add(video.Copy());
            part4.Media.Add(document.Copy());
            dataContext1.PartRepository.Add(part4);

            Part clonedPart5 = cloneManager.Clone(part4, true, false);

            // Verify that part's medias were cloned too
            Assert.IsTrue(clonedPart5.Media.Count == 2, "Failed to clone medias that were in Added state");

            // Clone part's medias without including its videos
            Part clonedPart6 = cloneManager.Clone(part4, false, false);

            // Verify that the part's videos were not included
            Assert.IsTrue(clonedPart6.Media.Count == 1 && clonedPart6.Media.FirstOrDefault(m => m.Type == MediaType.Video) == null, "Failed to clone part's medias without including videos");
        }

        /// <summary>
        /// Tests Clone methods using a valid assembly as input data
        /// </summary>
        [TestMethod]
        public void CloneAssemblyTest()
        {
            Assembly assembly1 = new Assembly();
            assembly1.CountrySettings = new CountrySetting();
            assembly1.OverheadSettings = new OverheadSetting();
            assembly1.Name = assembly1.Guid.ToString();
            assembly1.Number = EncryptionManager.Instance.GenerateRandomString(10, true);
            assembly1.AssemblingCountry = "Country" + DateTime.Now.Ticks;
            assembly1.AssemblingSupplier = "State" + DateTime.Now.Ticks;
            assembly1.Version = 2;
            assembly1.CalculationAccuracy = PartCalculationAccuracy.FineCalculation;
            assembly1.CalculationApproach = PartCalculationApproach.Brownfield;
            assembly1.CalculationVariant = EncryptionManager.Instance.GenerateRandomString(7, true);
            assembly1.BatchSizePerYear = 120;
            assembly1.YearlyProductionQuantity = 1200;
            assembly1.LifeTime = 50;
            assembly1.PurchasePrice = 2300;
            assembly1.DeliveryType = PartDeliveryType.CPT;
            assembly1.AssetRate = 123;
            assembly1.TargetPrice = 3400;
            assembly1.SBMActive = true;
            assembly1.IsExternal = true;

            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = manufacturer.Guid.ToString();
            manufacturer.Description = EncryptionManager.Instance.GenerateRandomString(10, true);
            assembly1.Manufacturer = manufacturer;

            Media video = new Media();
            video.Type = MediaType.Video;
            video.Content = new byte[] { 1, 2, 3, 4, 5 };
            video.Size = video.Content.Length;
            video.OriginalFileName = video.Guid.ToString();
            assembly1.Media.Add(video);

            Media document = new Media();
            document.Type = MediaType.Document;
            document.Content = new byte[] { 1, 2, 3, 4, 5, 6 };
            document.Size = document.Content.Length;
            document.OriginalFileName = document.Guid.ToString();
            assembly1.Media.Add(document);

            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 1, 8, 6, 7, 9 };
            image.Size = image.Content.Length;
            image.OriginalFileName = image.Guid.ToString();

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MeasurementUnit timeUnit = dataContext1.EntityContext.MeasurementUnitSet.FirstOrDefault(m => m.Name.Equals("Minute") && !m.IsReleased);
            MeasurementUnit weightUnit = dataContext1.EntityContext.MeasurementUnitSet.FirstOrDefault(m => m.Name.Equals("Gram") && !m.IsReleased);

            ProcessStep step1 = new AssemblyProcessStep();
            step1.Name = step1.Guid.ToString();
            step1.Accuracy = (short)ProcessCalculationAccuracy.Calculated;
            step1.ProcessTime = 10;
            step1.ProcessTimeUnit = timeUnit;
            step1.PartsPerCycle = 5;
            step1.ScrapAmount = 15;
            step1.Rework = 10;
            step1.ShiftsPerWeek = 12;
            step1.HoursPerShift = 9;
            step1.ProductionDaysPerWeek = 4;
            step1.ProductionWeeksPerYear = 250;
            step1.BatchSize = 100;
            step1.SetupsPerBatch = 25;
            step1.SetupTime = 19;
            step1.SetupTimeUnit = timeUnit;
            step1.MaxDownTime = 23;
            step1.MaxDownTimeUnit = timeUnit;
            step1.ProductionUnskilledLabour = 45;
            step1.ProductionSkilledLabour = 50;
            step1.ProductionForeman = 23;
            step1.ProductionTechnicians = 55;
            step1.ProductionEngineers = 123;
            step1.SetupUnskilledLabour = 235;
            step1.SetupSkilledLabour = 12;
            step1.SetupForeman = 355;
            step1.SetupTechnicians = 124;
            step1.SetupEngineers = 235;
            step1.IsExternal = true;
            step1.Price = 199;
            step1.ExceedShiftCost = true;
            step1.ExtraShiftsNumber = 700;
            step1.ShiftCostExceedRatio = 10;
            step1.Description = "Description" + DateTime.Now.Ticks;
            step1.Media = image;

            CycleTimeCalculation cycleTimeCalculation = new CycleTimeCalculation();
            cycleTimeCalculation.Description = cycleTimeCalculation.Guid.ToString();
            cycleTimeCalculation.RawPartDescription = "Description" + DateTime.Now.Ticks;
            cycleTimeCalculation.MachiningVolume = 10;
            cycleTimeCalculation.MachiningType = MachiningType.Boring;
            cycleTimeCalculation.ToleranceType = "Tolerance" + DateTime.Now.Ticks;
            cycleTimeCalculation.SurfaceType = "Surface" + DateTime.Now.Ticks;
            cycleTimeCalculation.TransportTimeFromBuffer = 70;
            cycleTimeCalculation.ToolChangeTime = 30;
            cycleTimeCalculation.TransportClampingTime = 15;
            cycleTimeCalculation.MachiningTime = 23;
            cycleTimeCalculation.TakeOffTime = 120;
            cycleTimeCalculation.MachiningCalculationData = new byte[] { 1, 2, 3, 4 };
            step1.CycleTimeCalculations.Add(cycleTimeCalculation);
            step1.CycleTimeUnit = timeUnit;
            step1.CycleTime = 1200;
            assembly1.Process = new Process();
            assembly1.Process.Steps.Add(step1);

            ProcessStepsClassification stepSubtype = new ProcessStepsClassification();
            stepSubtype.Name = stepSubtype.Guid.ToString();
            step1.SubType = stepSubtype;

            ProcessStepsClassification stepType = new ProcessStepsClassification();
            stepType.Name = stepType.Guid.ToString();
            step1.Type = stepType;

            Machine machine = new Machine();
            machine.Name = machine.Guid.ToString();
            machine.ManufacturingYear = 2000;
            machine.DepreciationPeriod = 20;
            machine.DepreciationRate = 10;
            machine.RegistrationNumber = EncryptionManager.Instance.GenerateRandomString(10, true);
            machine.DescriptionOfFunction = EncryptionManager.Instance.GenerateRandomString(15, true);
            machine.FloorSize = 10;
            machine.WorkspaceArea = 25;
            machine.PowerConsumption = 100;
            machine.AirConsumption = 230;
            machine.WaterConsumption = 300;
            machine.FullLoadRate = 50;
            machine.MaxCapacity = 1500;
            machine.OEE = 10;
            machine.Availability = 30;
            machine.MachineInvestment = 20;
            machine.SetupInvestment = 30;
            machine.AdditionalEquipmentInvestment = 230;
            machine.FundamentalSetupInvestment = 45;
            machine.InvestmentRemarks = "Remark" + DateTime.Now.Ticks;
            machine.ExternalWorkCost = 1230;
            machine.MaterialCost = 1000;
            machine.ConsumablesCost = 2300;
            machine.RepairsCost = 2345;
            machine.KValue = 1200;
            machine.CalculateWithKValue = true;
            machine.MountingCubeLength = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MountingCubeWidth = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MountingCubeHeight = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MaxDiameterRodPerChuckMaxThickness = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MaxPartsWeightPerCapacity = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.LockingForce = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.PressCapacity = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MaximumSpeed = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.RapidFeedPerCuttingSpeed = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.StrokeRate = EncryptionManager.Instance.GenerateRandomString(2, true);
            machine.Other = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.Manufacturer = manufacturer.Copy();
            machine.Manufacturer.Name = machine.Manufacturer.Guid.ToString();
            machine.Media = image.Copy();
            machine.Media.OriginalFileName = machine.Media.Guid.ToString();
            step1.Machines.Add(machine);

            // Add a classification level 4 to machine
            MachinesClassification machineClassificationLevel4 = new MachinesClassification();
            machineClassificationLevel4.Name = machineClassificationLevel4.Guid.ToString();
            machine.ClassificationLevel4 = machineClassificationLevel4;

            // Add subtype classification to machine
            MachinesClassification machineSubClassification = new MachinesClassification();
            machineSubClassification.Name = machineSubClassification.Guid.ToString();
            machine.SubClassification = machineSubClassification;

            // Add type classification to machine
            MachinesClassification machineTypeClassification = new MachinesClassification();
            machineTypeClassification.Name = machineTypeClassification.Guid.ToString();
            machine.TypeClassification = machineTypeClassification;

            // Add main classification to machine
            MachinesClassification machineMainClassification = new MachinesClassification();
            machineMainClassification.Name = machineMainClassification.Guid.ToString();
            machine.MainClassification = machineMainClassification;

            Commodity commodity = new Commodity();
            commodity.Name = commodity.Guid.ToString();
            commodity.PartNumber = EncryptionManager.Instance.GenerateRandomString(11, true);
            commodity.Price = 300;
            commodity.Amount = 2;
            commodity.RejectRatio = 1;
            commodity.Weight = 20;
            commodity.WeightUnitBase = weightUnit;
            commodity.Remarks = "Remark" + DateTime.Now.Ticks;
            commodity.Manufacturer = manufacturer.Copy();
            commodity.Manufacturer.Name = commodity.Manufacturer.Guid.ToString();
            commodity.Media = image;
            commodity.Media.OriginalFileName = commodity.Media.Guid.ToString();
            step1.Commodities.Add(commodity);

            Consumable consumable = new Consumable();
            consumable.Name = consumable.Guid.ToString();
            consumable.Description = EncryptionManager.Instance.GenerateRandomString(17, true);
            consumable.Price = 1200;
            consumable.PriceUnitBase = weightUnit;
            consumable.Amount = 12;
            consumable.Manufacturer = manufacturer.Copy();
            consumable.Manufacturer.Name = consumable.Manufacturer.Guid.ToString();
            step1.Consumables.Add(consumable);

            Die die = new Die();
            die.Name = die.Guid.ToString();
            die.Type = (short)DieType.CastingDie;
            die.Investment = 100;
            die.ReusableInvest = 1200;
            die.Wear = 300;
            die.Maintenance = 1200;
            die.LifeTime = 300;
            die.AllocationRatio = 2300;
            die.DiesetsNumberPaidByCustomer = 1300;
            die.CostAllocationBasedOnPartsPerLifeTime = true;
            die.CostAllocationNumberOfParts = 234;
            die.Remark = EncryptionManager.Instance.GenerateRandomString(15, true);
            die.Manufacturer = manufacturer.Copy();
            die.Manufacturer.Name = die.Manufacturer.Guid.ToString();
            die.Media = image.Copy();
            die.Media.OriginalFileName = die.Media.Guid.ToString();
            step1.Dies.Add(die);

            Assembly assembly2 = new Assembly();
            assembly2.Name = assembly2.Guid.ToString();
            assembly2.Manufacturer = manufacturer.Copy();
            assembly2.Manufacturer.Name = assembly2.Manufacturer.Guid.ToString();
            assembly2.CountrySettings = new CountrySetting();
            assembly2.OverheadSettings = new OverheadSetting();
            assembly1.Subassemblies.Add(assembly2);

            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.CountrySettings = new CountrySetting();
            part.OverheadSettings = new OverheadSetting();
            part.Manufacturer = manufacturer.Copy();
            part.Manufacturer.Name = part.Manufacturer.Guid.ToString();
            assembly1.Parts.Add(part);

            ProcessStepPartAmount partAmount = new ProcessStepPartAmount();
            partAmount.Part = part;
            partAmount.Amount = 9;
            step1.PartAmounts.Add(partAmount);

            ProcessStepAssemblyAmount assemblyAmount = new ProcessStepAssemblyAmount();
            assemblyAmount.Assembly = assembly2;
            assemblyAmount.Amount = 3;
            step1.AssemblyAmounts.Add(assemblyAmount);

            User user1 = new User();
            user1.Username = user1.Guid.ToString();
            user1.Password = EncryptionManager.Instance.HashSHA256("Password.", user1.Salt, user1.Guid.ToString());
            user1.Name = user1.Guid.ToString();
            user1.Roles = Role.Admin;
            assembly1.SetOwner(user1);

            dataContext1.AssemblyRepository.Add(assembly1);
            dataContext1.SaveChanges();

            // Clone an assembly including video files
            CloneManager cloneManager = new CloneManager(dataContext1, dataContext1);
            Assembly clonedAssembly1 = cloneManager.Clone(assembly1, true, false);
            bool result1 = AreEqual(assembly1, dataContext1, clonedAssembly1, dataContext1, true);

            // Verifies if the clone of assembly is identical with the cloned assembly
            Assert.IsTrue(result1, "Failed to clone an assembly including video files.");

            // Clone an assembly without including video files
            Assembly clonedAssembly2 = cloneManager.Clone(assembly1, false, false);
            bool result2 = AreEqual(assembly1, dataContext1, clonedAssembly2, dataContext1, false);

            // Verifies if the clone of assembly is identical with the cloned assembly
            Assert.IsTrue(result2, "Failed to clone an assembly without including video files.");

            // Clone a null assembly
            Assembly assembly3 = null;
            Assembly clonedAssembly3 = cloneManager.Clone(assembly3);

            Assert.IsNull(clonedAssembly3, "The clone of a null assembly should be null too.");

            // Clone an assembly without media that is in Added state
            Assembly assembly4 = new Assembly();
            assembly4.Name = assembly4.Guid.ToString();
            assembly4.CountrySettings = new CountrySetting();
            assembly4.OverheadSettings = new OverheadSetting();
            assembly4.Name = assembly1.Guid.ToString();
            assembly4.Number = EncryptionManager.Instance.GenerateRandomString(10, true);
            assembly4.AssemblingCountry = "Country" + DateTime.Now.Ticks;
            assembly4.AssemblingSupplier = "State" + DateTime.Now.Ticks;
            assembly4.Version = 2;
            assembly4.CalculationAccuracy = PartCalculationAccuracy.FineCalculation;
            assembly4.CalculationApproach = PartCalculationApproach.Brownfield;
            assembly4.CalculationVariant = EncryptionManager.Instance.GenerateRandomString(7, true);
            assembly4.BatchSizePerYear = 120;
            assembly4.YearlyProductionQuantity = 1200;
            assembly4.LifeTime = 50;
            assembly4.PurchasePrice = 2300;
            assembly4.DeliveryType = PartDeliveryType.CPT;
            assembly4.AssetRate = 123;
            assembly4.TargetPrice = 3400;
            assembly4.SBMActive = true;
            assembly4.IsExternal = true;
            dataContext1.AssemblyRepository.Add(assembly4);

            Assembly clonedAssembly4 = cloneManager.Clone(assembly4, true, false);
            bool result4 = AreEqual(assembly4, dataContext1, clonedAssembly4, dataContext1, true);

            // Verify that the cloned assembly is identically with its clone
            Assert.IsTrue(result4, "Failed to clone an assembly that is in Added state");

            // Clone an assembly whose medias are in Added state
            Assembly assembly5 = new Assembly();
            assembly5.Name = assembly5.Guid.ToString();
            assembly5.OverheadSettings = new OverheadSetting();
            assembly5.CountrySettings = new CountrySetting();
            assembly5.Media.Add(video.Copy());
            assembly5.Media.Add(document.Copy());
            dataContext1.AssemblyRepository.Add(assembly5);

            Assembly clonedAssembly5 = cloneManager.Clone(assembly5, true, false);

            // Verify that the assembly's medias were cloned too
            Assert.IsTrue(clonedAssembly5.Media.Count == 2, "Failed to clone medias that were in Added state");

            // Clone assembly's medias without including videos
            Assembly clonedAssembly6 = cloneManager.Clone(assembly5, false, false);

            // Verify that the assembly's video was not cloned
            Assert.IsTrue(clonedAssembly6.Media.Count == 1 && clonedAssembly6.Media.FirstOrDefault(m => m.Type == MediaType.Video) == null, "Failed to clone an assembly that is in Added state without including its video");
        }

        /// <summary>
        /// A test for Clone(Project source) methods using a valid project as input parameter
        /// </summary>
        [TestMethod]
        public void CloneProjectTest()
        {
            User user1 = new User();
            user1.Username = user1.Guid.ToString();
            user1.Password = EncryptionManager.Instance.HashSHA256("Password.", user1.Salt, user1.Guid.ToString());
            user1.Name = user1.Guid.ToString();
            user1.Roles = Role.Admin;

            Project project1 = new Project();
            project1.Name = project1.Guid.ToString();
            project1.Number = EncryptionManager.Instance.GenerateRandomString(10, true);
            project1.StartDate = DateTime.Now;
            project1.EndDate = DateTime.Now;
            project1.Status = ProjectStatus.ValidatedInternal;
            project1.Partner = "Partner" + DateTime.Now.Ticks;
            project1.ProjectLeader = user1;
            project1.ResponsibleCalculator = user1;
            project1.Version = 1;
            project1.ProductName = "Product" + DateTime.Now.Ticks;
            project1.ProductNumber = EncryptionManager.Instance.GenerateRandomString(10, true);
            project1.YearlyProductionQuantity = 100;
            project1.LifeTime = 10;
            project1.ProductDescription = "Description" + DateTime.Now.Ticks;
            project1.DepreciationPeriod = 10;
            project1.DepreciationRate = 20;
            project1.LogisticCostRatio = 300;
            project1.Remarks = "Remarks" + DateTime.Now.Ticks;
            project1.CustomerDescription = "Customer Description" + DateTime.Now.Ticks;
            project1.SourceOverheadSettingsLastUpdate = DateTime.Now;
            project1.OverheadSettings = new OverheadSetting();

            Media video = new Media();
            video.Type = MediaType.Video;
            video.Content = new byte[] { 1, 2, 3, 4, 5, 6 };
            video.OriginalFileName = video.Guid.ToString();
            video.Size = video.Content.Length;
            project1.Media.Add(video);

            Media document = new Media();
            document.Type = MediaType.Document;
            document.Content = new byte[] { 1, 2, 3, 3, 3, 9 };
            document.OriginalFileName = document.Guid.ToString();
            document.Size = document.Content.Length;
            project1.Media.Add(document);

            Customer customer = new Customer();
            customer.Name = customer.Guid.ToString();
            customer.Description = "Description" + DateTime.Now.Ticks;
            customer.Type = (short)SupplierType.CorporateGroup;
            customer.Number = EncryptionManager.Instance.GenerateRandomString(9, true);
            customer.StreetAddress = "Street Address" + DateTime.Now.Ticks;
            customer.ZipCode = EncryptionManager.Instance.GenerateRandomString(5, true);
            customer.Country = "Country" + DateTime.Now.Ticks;
            customer.State = "State" + DateTime.Now.Ticks;
            customer.City = "City" + DateTime.Now.Ticks;
            customer.FirstContactName = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer.FirstContactPhone = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer.FirstContactMobile = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer.FirstContactWebAddress = "www.test.com";
            customer.FirstContactEmail = "test@gmail.com";
            customer.FirstContactFax = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer.SecondContactName = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer.SecondContactPhone = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer.SecondContactMobile = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer.SecondContactWebAddress = "www.test.com";
            customer.SecondContactEmail = "test@gmail.com";
            customer.SecondContactFax = EncryptionManager.Instance.GenerateRandomString(10, true);
            project1.Customer = customer;
            project1.SetOwner(user1);

            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.Number = EncryptionManager.Instance.GenerateRandomString(10, true);
            part.Version = 1;
            part.Description = "Description" + DateTime.Now.Ticks;
            part.CalculationStatus = PartCalculationStatus.Approved;
            part.CalculationAccuracy = PartCalculationAccuracy.FineCalculation;
            part.CalculationVariant = "9";
            part.CalculatorUser = user1;
            part.BatchSizePerYear = 10;
            part.YearlyProductionQuantity = 90;
            part.LifeTime = 50;
            part.Media.Add(video.Copy());
            part.Media.Add(document.Copy());
            part.ManufacturingCountry = "Country" + DateTime.Now.Ticks;
            part.ManufacturingSupplier = "State" + DateTime.Now.Ticks;
            part.PurchasePrice = 1500;
            part.DelivertType = PartDeliveryType.CIF;
            part.AssetRate = 200;
            part.TargetPrice = 2000;
            part.SBMActive = true;
            part.IsExternal = true;
            part.AdditionalRemarks = "Remarks" + DateTime.Now.Ticks;
            part.AdditionalDescription = "Additional Description" + DateTime.Now.Ticks;
            part.ManufacturerDescription = "Manufacturer Description" + DateTime.Now.Ticks;
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            project1.Parts.Add(part);

            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            manufacturer.Description = "Description" + DateTime.Now.Ticks;
            part.Manufacturer = manufacturer;

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MeasurementUnit weightUnit = dataContext1.EntityContext.MeasurementUnitSet.FirstOrDefault(m => m.Name.Equals("Gram") && !m.IsReleased);

            Commodity commodity = new Commodity();
            commodity.Name = commodity.Guid.ToString();
            commodity.PartNumber = EncryptionManager.Instance.GenerateRandomString(11, true);
            commodity.Price = 300;
            commodity.Amount = 2;
            commodity.RejectRatio = 1;
            commodity.Weight = 20;
            commodity.WeightUnitBase = weightUnit;
            commodity.Remarks = "Remark" + DateTime.Now.Ticks;
            commodity.Manufacturer = manufacturer.Copy();
            commodity.Manufacturer.Name = commodity.Manufacturer.Guid.ToString();

            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 1, 2, 9, 9, 9 };
            image.OriginalFileName = image.Guid.ToString();
            image.Size = image.Content.Length;
            commodity.Media = image;
            part.Commodities.Add(commodity);

            RawMaterial rawMaterial = new RawMaterial();
            rawMaterial.Name = rawMaterial.Guid.ToString();
            rawMaterial.NameUK = "UK" + DateTime.Now.Ticks;
            rawMaterial.NameUS = "US" + DateTime.Now.Ticks;
            rawMaterial.NormName = "Norm" + DateTime.Now.Ticks;
            rawMaterial.ScrapCalculationType = (short)ScrapCalculationType.Yield;
            rawMaterial.Price = 1200;
            rawMaterial.PriceUnitBase = weightUnit;
            rawMaterial.ParentWeight = 10;
            rawMaterial.ParentWeightUnitBase = weightUnit;
            rawMaterial.Sprue = 1;
            rawMaterial.Remarks = "Remark" + DateTime.Now.Ticks;
            rawMaterial.ScrapRefundRatio = 150;
            rawMaterial.RejectRatio = 1;
            rawMaterial.YieldStrength = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.RuptureStrength = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.Density = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.MaxElongation = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.GlassTransitionTemperature = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.Rx = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.Rm = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.Manufacturer = manufacturer.Copy();
            rawMaterial.Manufacturer.Name = rawMaterial.Manufacturer.Guid.ToString();
            rawMaterial.Media = image.Copy();
            rawMaterial.Media.OriginalFileName = rawMaterial.Media.Guid.ToString();
            part.RawMaterials.Add(rawMaterial);

            RawMaterialDeliveryType deliveryType = new RawMaterialDeliveryType();
            deliveryType.Name = deliveryType.Guid.ToString();
            rawMaterial.DeliveryType = deliveryType;

            MaterialsClassification materialClassificationLevel4 = new MaterialsClassification();
            materialClassificationLevel4.Name = materialClassificationLevel4.Guid.ToString();
            rawMaterial.MaterialsClassificationL4 = materialClassificationLevel4;

            MaterialsClassification materialClassificationLevel3 = new MaterialsClassification();
            materialClassificationLevel3.Name = materialClassificationLevel3.Guid.ToString();
            rawMaterial.MaterialsClassificationL3 = materialClassificationLevel3;

            MaterialsClassification materialClassificationLevel2 = new MaterialsClassification();
            materialClassificationLevel2.Name = materialClassificationLevel2.Guid.ToString();
            rawMaterial.MaterialsClassificationL2 = materialClassificationLevel2;

            MaterialsClassification materialClassificationLevel1 = new MaterialsClassification();
            materialClassificationLevel1.Name = materialClassificationLevel1.Guid.ToString();
            rawMaterial.MaterialsClassificationL1 = materialClassificationLevel1;

            MeasurementUnit timeUnit = dataContext1.EntityContext.MeasurementUnitSet.FirstOrDefault(m => m.Name.Equals("Minute") && !m.IsReleased);

            ProcessStep step1 = new PartProcessStep();
            step1.Name = step1.Guid.ToString();
            step1.Accuracy = (short)ProcessCalculationAccuracy.Calculated;
            step1.ProcessTime = 10;
            step1.ProcessTimeUnit = timeUnit;
            step1.PartsPerCycle = 5;
            step1.ScrapAmount = 15;
            step1.Rework = 10;
            step1.ShiftsPerWeek = 12;
            step1.HoursPerShift = 9;
            step1.ProductionDaysPerWeek = 4;
            step1.ProductionWeeksPerYear = 250;
            step1.BatchSize = 100;
            step1.SetupsPerBatch = 25;
            step1.SetupTime = 19;
            step1.SetupTimeUnit = timeUnit;
            step1.MaxDownTime = 23;
            step1.MaxDownTimeUnit = timeUnit;
            step1.ProductionUnskilledLabour = 45;
            step1.ProductionSkilledLabour = 50;
            step1.ProductionForeman = 23;
            step1.ProductionTechnicians = 55;
            step1.ProductionEngineers = 123;
            step1.SetupUnskilledLabour = 235;
            step1.SetupSkilledLabour = 12;
            step1.SetupForeman = 355;
            step1.SetupTechnicians = 124;
            step1.SetupEngineers = 235;
            step1.IsExternal = true;
            step1.Price = 199;
            step1.ExceedShiftCost = true;
            step1.ExtraShiftsNumber = 700;
            step1.ShiftCostExceedRatio = 10;
            step1.Description = "Description" + DateTime.Now.Ticks;
            step1.Media = image.Copy();
            step1.Media.OriginalFileName = step1.Media.Guid.ToString();

            CycleTimeCalculation cycleTimeCalculation = new CycleTimeCalculation();
            cycleTimeCalculation.Description = cycleTimeCalculation.Guid.ToString();
            cycleTimeCalculation.RawPartDescription = "Description" + DateTime.Now.Ticks;
            cycleTimeCalculation.MachiningVolume = 10;
            cycleTimeCalculation.MachiningType = MachiningType.Boring;
            cycleTimeCalculation.ToleranceType = "Tolerance" + DateTime.Now.Ticks;
            cycleTimeCalculation.SurfaceType = "Surface" + DateTime.Now.Ticks;
            cycleTimeCalculation.TransportTimeFromBuffer = 70;
            cycleTimeCalculation.ToolChangeTime = 30;
            cycleTimeCalculation.TransportClampingTime = 15;
            cycleTimeCalculation.MachiningTime = 23;
            cycleTimeCalculation.TakeOffTime = 120;
            cycleTimeCalculation.MachiningCalculationData = new byte[] { 1, 2, 3, 4 };
            step1.CycleTimeCalculations.Add(cycleTimeCalculation);
            step1.CycleTimeUnit = timeUnit;
            step1.CycleTime = 1200;
            part.Process = new Process();
            part.Process.Steps.Add(step1);

            ProcessStepsClassification stepSubtype = new ProcessStepsClassification();
            stepSubtype.Name = stepSubtype.Guid.ToString();
            step1.SubType = stepSubtype;

            ProcessStepsClassification stepType = new ProcessStepsClassification();
            stepType.Name = stepType.Guid.ToString();
            step1.Type = stepType;

            Machine machine = new Machine();
            machine.Name = machine.Guid.ToString();
            machine.ManufacturingYear = 2000;
            machine.DepreciationPeriod = 20;
            machine.DepreciationRate = 10;
            machine.RegistrationNumber = EncryptionManager.Instance.GenerateRandomString(10, true);
            machine.DescriptionOfFunction = EncryptionManager.Instance.GenerateRandomString(15, true);
            machine.FloorSize = 10;
            machine.WorkspaceArea = 25;
            machine.PowerConsumption = 100;
            machine.AirConsumption = 230;
            machine.WaterConsumption = 300;
            machine.FullLoadRate = 50;
            machine.MaxCapacity = 1500;
            machine.OEE = 10;
            machine.Availability = 30;
            machine.MachineInvestment = 20;
            machine.SetupInvestment = 30;
            machine.AdditionalEquipmentInvestment = 230;
            machine.FundamentalSetupInvestment = 45;
            machine.InvestmentRemarks = "Remark" + DateTime.Now.Ticks;
            machine.ExternalWorkCost = 1230;
            machine.MaterialCost = 1000;
            machine.ConsumablesCost = 2300;
            machine.RepairsCost = 2345;
            machine.KValue = 1200;
            machine.CalculateWithKValue = true;
            machine.MountingCubeLength = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MountingCubeWidth = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MountingCubeHeight = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MaxDiameterRodPerChuckMaxThickness = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MaxPartsWeightPerCapacity = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.LockingForce = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.PressCapacity = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MaximumSpeed = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.RapidFeedPerCuttingSpeed = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.StrokeRate = EncryptionManager.Instance.GenerateRandomString(2, true);
            machine.Other = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.Manufacturer = manufacturer.Copy();
            machine.Manufacturer.Name = machine.Manufacturer.Guid.ToString();
            machine.Media = image.Copy();
            machine.Media.OriginalFileName = machine.Media.Guid.ToString();
            step1.Machines.Add(machine);

            // Add a classification level 4 to machine
            MachinesClassification machineClassificationLevel4 = new MachinesClassification();
            machineClassificationLevel4.Name = machineClassificationLevel4.Guid.ToString();
            machine.ClassificationLevel4 = machineClassificationLevel4;

            // Add subtype classification to machine
            MachinesClassification machineSubClassification = new MachinesClassification();
            machineSubClassification.Name = machineSubClassification.Guid.ToString();
            machine.SubClassification = machineSubClassification;

            // Add type classification to machine
            MachinesClassification machineTypeClassification = new MachinesClassification();
            machineTypeClassification.Name = machineTypeClassification.Guid.ToString();
            machine.TypeClassification = machineTypeClassification;

            // Add main classification to machine
            MachinesClassification machineMainClassification = new MachinesClassification();
            machineMainClassification.Name = machineMainClassification.Guid.ToString();
            machine.MainClassification = machineMainClassification;

            Consumable consumable = new Consumable();
            consumable.Name = consumable.Guid.ToString();
            consumable.Description = EncryptionManager.Instance.GenerateRandomString(17, true);
            consumable.Price = 1200;
            consumable.PriceUnitBase = weightUnit;
            consumable.Amount = 12;
            consumable.Manufacturer = manufacturer.Copy();
            consumable.Manufacturer.Name = consumable.Manufacturer.Guid.ToString();
            step1.Consumables.Add(consumable);

            Die die = new Die();
            die.Name = die.Guid.ToString();
            die.Type = (short)DieType.CastingDie;
            die.Investment = 100;
            die.ReusableInvest = 1200;
            die.Wear = 300;
            die.Maintenance = 1200;
            die.LifeTime = 300;
            die.AllocationRatio = 2300;
            die.DiesetsNumberPaidByCustomer = 1300;
            die.CostAllocationBasedOnPartsPerLifeTime = true;
            die.CostAllocationNumberOfParts = 234;
            die.Remark = EncryptionManager.Instance.GenerateRandomString(15, true);
            die.Manufacturer = manufacturer.Copy();
            die.Manufacturer.Name = die.Manufacturer.Guid.ToString();
            die.Media = image.Copy();
            die.Media.OriginalFileName = die.Media.Guid.ToString();
            step1.Dies.Add(die);

            Assembly assembly = new Assembly();
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();
            assembly.Name = assembly.Guid.ToString();
            assembly.Number = EncryptionManager.Instance.GenerateRandomString(10, true);
            assembly.AssemblingCountry = "Country" + DateTime.Now.Ticks;
            assembly.AssemblingSupplier = "State" + DateTime.Now.Ticks;
            assembly.Version = 2;
            assembly.CalculationAccuracy = PartCalculationAccuracy.FineCalculation;
            assembly.CalculationApproach = PartCalculationApproach.Brownfield;
            assembly.CalculationVariant = EncryptionManager.Instance.GenerateRandomString(7, true);
            assembly.BatchSizePerYear = 120;
            assembly.YearlyProductionQuantity = 1200;
            assembly.LifeTime = 50;
            assembly.PurchasePrice = 2300;
            assembly.DeliveryType = PartDeliveryType.CPT;
            assembly.AssetRate = 123;
            assembly.TargetPrice = 3400;
            assembly.SBMActive = true;
            assembly.IsExternal = true;
            assembly.Manufacturer = manufacturer.Copy();
            assembly.Manufacturer.Name = assembly.Manufacturer.Guid.ToString();
            assembly.Media.Add(video.Copy());
            assembly.Media.Add(document.Copy());
            project1.Assemblies.Add(assembly);

            ProcessStep step2 = new AssemblyProcessStep();
            step1.CopyValuesTo(step2);
            step2.Name = step2.Guid.ToString();
            step2.CycleTimeUnit = timeUnit;
            step2.SetupTimeUnit = timeUnit;
            step2.MaxDownTimeUnit = timeUnit;
            step2.Media = image.Copy();
            step2.Media.OriginalFileName = step2.Media.Guid.ToString();
            step2.Type = new ProcessStepsClassification();
            step2.Type.Name = step2.Type.Guid.ToString();
            step2.SubType = new ProcessStepsClassification();
            step2.SubType.Name = step2.SubType.Guid.ToString();
            step2.Type.Children.Add(step2.SubType);
            assembly.Process = new Process();
            assembly.Process.Steps.Add(step2);

            CycleTimeCalculation cycleTimeCalculation2 = new CycleTimeCalculation();
            cycleTimeCalculation.CopyValuesTo(cycleTimeCalculation2);
            step2.CycleTimeCalculations.Add(cycleTimeCalculation2);

            Machine machine2 = new Machine();
            machine.CopyValuesTo(machine2);
            machine2.Name = machine2.Guid.ToString();
            machine2.Manufacturer = manufacturer.Copy();
            machine2.Manufacturer.Name = machine2.Manufacturer.Guid.ToString();
            machine2.Media = image.Copy();
            machine2.Media.OriginalFileName = machine2.Media.Guid.ToString();
            step2.Machines.Add(machine2);

            Consumable consumable2 = new Consumable();
            consumable.CopyValuesTo(consumable2);
            consumable2.Name = consumable2.Guid.ToString();
            consumable2.PriceUnitBase = weightUnit;
            consumable2.Manufacturer = manufacturer.Copy();
            consumable2.Manufacturer.Name = consumable2.Manufacturer.Guid.ToString();
            step2.Consumables.Add(consumable2);

            Commodity commodity2 = new Commodity();
            commodity.CopyValuesTo(commodity2);
            commodity2.Name = commodity2.Guid.ToString();
            commodity2.WeightUnitBase = weightUnit;
            commodity2.Media = image.Copy();
            commodity2.Media.OriginalFileName = commodity2.Media.Guid.ToString();
            commodity2.Manufacturer = manufacturer.Copy();
            commodity2.Manufacturer.Name = commodity2.Manufacturer.Guid.ToString();
            step2.Commodities.Add(commodity2);

            Die die2 = new Die();
            die.CopyValuesTo(die2);
            die2.Name = die2.Guid.ToString();
            die2.Manufacturer = manufacturer.Copy();
            die2.Manufacturer.Name = die2.Manufacturer.Guid.ToString();
            step2.Dies.Add(die2);

            Assembly assembly2 = new Assembly();
            assembly2.Name = assembly2.Guid.ToString();
            assembly2.Manufacturer = manufacturer.Copy();
            assembly2.Manufacturer.Name = assembly2.Manufacturer.Guid.ToString();
            assembly2.CountrySettings = new CountrySetting();
            assembly2.OverheadSettings = new OverheadSetting();
            assembly.Subassemblies.Add(assembly2);

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.CountrySettings = new CountrySetting();
            part2.OverheadSettings = new OverheadSetting();
            part2.Manufacturer = manufacturer.Copy();
            part2.Manufacturer.Name = part2.Manufacturer.Guid.ToString();
            assembly.Parts.Add(part2);

            ProcessStepPartAmount partAmount = new ProcessStepPartAmount();
            partAmount.Part = part2;
            partAmount.ProcessStep = step2;
            partAmount.Amount = 9;
            step2.PartAmounts.Add(partAmount);

            ProcessStepAssemblyAmount assemblyAmount = new ProcessStepAssemblyAmount();
            assemblyAmount.Assembly = assembly2;
            assemblyAmount.ProcessStep = step2;
            assemblyAmount.Amount = 3;
            step2.AssemblyAmounts.Add(assemblyAmount);
            project1.SetOwner(user1);

            dataContext1.ProjectRepository.Add(project1);
            dataContext1.SaveChanges();

            CloneManager cloneManager = new CloneManager(dataContext1, dataContext1);
            Project clonedProject1 = cloneManager.Clone(project1);
            bool result1 = AreEqual(project1, dataContext1, clonedProject1, dataContext1);

            // Verifies if the clone of project is identical with the cloned project
            Assert.IsTrue(result1, "Failed to clone a project");

            // Clone a project without media that is in Added state
            Project project3 = new Project();
            project3.Name = project3.Guid.ToString();
            project3.Number = EncryptionManager.Instance.GenerateRandomString(10, true);
            project3.StartDate = DateTime.Now;
            project3.EndDate = DateTime.Now;
            project3.Status = ProjectStatus.ValidatedInternal;
            project3.Partner = "Partner" + DateTime.Now.Ticks;
            project3.ProjectLeader = user1;
            project3.ResponsibleCalculator = user1;
            project3.Version = 1;
            project3.ProductName = "Product" + DateTime.Now.Ticks;
            project3.ProductNumber = EncryptionManager.Instance.GenerateRandomString(10, true);
            project3.YearlyProductionQuantity = 100;
            project3.LifeTime = 10;
            project3.ProductDescription = "Description" + DateTime.Now.Ticks;
            project3.DepreciationPeriod = 10;
            project3.DepreciationRate = 20;
            project3.LogisticCostRatio = 300;
            project3.Remarks = "Remarks" + DateTime.Now.Ticks;
            project3.CustomerDescription = "Customer Description" + DateTime.Now.Ticks;
            project3.SourceOverheadSettingsLastUpdate = DateTime.Now;
            project3.OverheadSettings = new OverheadSetting();
            dataContext1.ProjectRepository.Add(project3);

            Project clonedProject3 = cloneManager.Clone(project3);
            bool result3 = AreEqual(project3, dataContext1, clonedProject3, dataContext1);

            // Verify that the cloned project is identically with its clone
            Assert.IsTrue(result3, "Failed to clone a project that is in Added state");

            // Clone a project whose medias are in Added state
            Project project4 = new Project();
            project4.Name = project4.Guid.ToString();
            project4.OverheadSettings = new OverheadSetting();
            project4.Media.Add(image.Copy());
            project4.Media.Add(document.Copy());
            dataContext1.ProjectRepository.Add(project4);

            Project clonedProject4 = cloneManager.Clone(project4);

            // Verify that the project's media were cloned too
            Assert.IsTrue(project4.Media.Count == 2, "Failed to clone medias that were in Added state");
        }

        #endregion Test methods

        #region Helpers

        /// <summary>
        /// Compares two cycle time calculations
        /// </summary>
        /// <param name="calculation">The compared cycle time calculation</param>
        /// <param name="calculationToCompareWith">The cycle time calculation to compare with</param>
        /// <returns>True if cycle time calculations are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, IsMasterData.
        /// </remarks>
        private bool AreEqual(CycleTimeCalculation calculation, CycleTimeCalculation calculationToCompareWith)
        {
            if (calculation == null && calculationToCompareWith == null)
            {
                return true;
            }

            if ((calculation == null && calculationToCompareWith != null) || (calculation != null && calculationToCompareWith == null))
            {
                return false;
            }

            bool result = string.Equals(calculation.Description, calculationToCompareWith.Description) &&
                          string.Equals(calculation.RawPartDescription, calculationToCompareWith.RawPartDescription) &&
                          string.Equals(calculation.ToleranceType, calculationToCompareWith.ToleranceType) &&
                          string.Equals(calculation.SurfaceType, calculationToCompareWith.SurfaceType) &&
                          calculation.MachiningVolume == calculationToCompareWith.MachiningVolume &&
                          calculation.MachiningType == calculationToCompareWith.MachiningType &&
                          calculation.TransportTimeFromBuffer == calculationToCompareWith.TransportTimeFromBuffer &&
                          calculation.ToolChangeTime == calculationToCompareWith.ToolChangeTime &&
                          calculation.TransportClampingTime == calculationToCompareWith.TransportClampingTime &&
                          calculation.MachiningTime == calculationToCompareWith.MachiningTime &&
                          calculation.TakeOffTime == calculationToCompareWith.TakeOffTime &&
                          calculation.Index == calculationToCompareWith.Index &&
                          calculation.MachiningCalculationData.SequenceEqual(calculationToCompareWith.MachiningCalculationData);

            return result;
        }

        /// <summary>
        /// Compares two customers
        /// </summary>
        /// <param name="customer">The compared customer</param>
        /// <param name="customerToCompareWith">The customer to compare with</param>
        /// <returns>True if customers are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, IsMasterData.
        /// </remarks>
        private bool AreEqual(Customer customer, Customer customerToCompareWith)
        {
            if (customer == null && customerToCompareWith == null)
            {
                return true;
            }

            if ((customer == null && customerToCompareWith != null) || (customer != null && customerToCompareWith == null))
            {
                return false;
            }

            bool result = string.Equals(customer.Name, customerToCompareWith.Name) &&
                          string.Equals(customer.Description, customerToCompareWith.Description) &&
                          string.Equals(customer.Number, customerToCompareWith.Number) &&
                          string.Equals(customer.StreetAddress, customerToCompareWith.StreetAddress) &&
                          string.Equals(customer.ZipCode, customerToCompareWith.ZipCode) &&
                          string.Equals(customer.City, customerToCompareWith.City) &&
                          string.Equals(customer.FirstContactName, customerToCompareWith.FirstContactName) &&
                          string.Equals(customer.FirstContactEmail, customerToCompareWith.FirstContactEmail) &&
                          string.Equals(customer.FirstContactPhone, customerToCompareWith.FirstContactPhone) &&
                          string.Equals(customer.FirstContactFax, customerToCompareWith.FirstContactFax) &&
                          string.Equals(customer.FirstContactMobile, customerToCompareWith.FirstContactMobile) &&
                          string.Equals(customer.FirstContactWebAddress, customerToCompareWith.FirstContactWebAddress) &&
                          string.Equals(customer.SecondContactName, customerToCompareWith.SecondContactName) &&
                          string.Equals(customer.SecondContactEmail, customerToCompareWith.SecondContactEmail) &&
                          string.Equals(customer.SecondContactPhone, customerToCompareWith.SecondContactPhone) &&
                          string.Equals(customer.SecondContactFax, customerToCompareWith.SecondContactFax) &&
                          string.Equals(customer.SecondContactMobile, customerToCompareWith.SecondContactMobile) &&
                          string.Equals(customer.SecondContactWebAddress, customerToCompareWith.SecondContactWebAddress) &&
                          string.Equals(customer.Country, customerToCompareWith.Country) &&
                          string.Equals(customer.State, customerToCompareWith.State) &&
                          customer.Type == customerToCompareWith.Type;

            return result;
        }

        /// <summary>
        /// Compares two manufacturers
        /// </summary>
        /// <param name="manufacturer">The compared manufacturer</param>
        /// <param name="manufacturerToCompareWith">The manufacturer to compare with</param>
        /// <returns>True if manufacturers are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, IsMasterData.
        /// </remarks>
        private bool AreEqual(Manufacturer manufacturer, Manufacturer manufacturerToCompareWith)
        {
            if (manufacturer == null && manufacturerToCompareWith == null)
            {
                return true;
            }

            if ((manufacturer == null && manufacturerToCompareWith != null) || (manufacturer != null && manufacturerToCompareWith == null))
            {
                return false;
            }

            bool result = string.Equals(manufacturer.Name, manufacturerToCompareWith.Name) &&
                          string.Equals(manufacturer.Description, manufacturerToCompareWith.Description);

            return result;
        }

        /// <summary>
        /// Compares two consumables
        /// </summary>
        /// <param name="consumable">The compared consumable</param>
        /// <param name="consumableToCompareWith">The consumable to compare with</param>
        /// <returns>True if consumables are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, IsMasterData.
        /// </remarks>
        private bool AreEqual(Consumable consumable, Consumable consumableToCompareWith)
        {
            if (consumable == null && consumableToCompareWith == null)
            {
                return true;
            }

            if ((consumable == null && consumableToCompareWith != null) || (consumable != null && consumableToCompareWith == null))
            {
                return false;
            }

            bool result = string.Equals(consumable.Name, consumableToCompareWith.Name) &&
                          string.Equals(consumable.Description, consumableToCompareWith.Description) &&
                          consumable.Price == consumableToCompareWith.Price &&
                          consumable.Amount == consumableToCompareWith.Amount &&
                          consumable.IsDeleted == consumableToCompareWith.IsDeleted;

            // If consumables are not equal -> return false.
            if (!result)
            {
                return false;
            }

            // Verifies if Manufacturers are equal.
            result = AreEqual(consumable.Manufacturer, consumableToCompareWith.Manufacturer);
            if (!result)
            {
                return false;
            }

            // Verifies if PriceUnits are equal.
            result = AreEqual(consumable.PriceUnitBase, consumableToCompareWith.PriceUnitBase);
            if (!result)
            {
                return false;
            }

            // Verifies if AmountUnits are equal.
            result = AreEqual(consumable.AmountUnitBase, consumableToCompareWith.AmountUnitBase);

            return result;
        }

        /// <summary>
        /// compares two measurement units
        /// </summary>
        /// <param name="measurementUnit">The compared measurement unit</param>
        /// <param name="measurementUnitToCompareWith">The measurement unit to compare with</param>
        /// <returns>True if measurement units are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: IsReleased, Guid.
        /// </remarks>
        private bool AreEqual(MeasurementUnit measurementUnit, MeasurementUnit measurementUnitToCompareWith)
        {
            if (measurementUnit == null && measurementUnitToCompareWith == null)
            {
                return true;
            }

            if ((measurementUnit == null && measurementUnitToCompareWith != null) || (measurementUnit != null && measurementUnitToCompareWith == null))
            {
                return false;
            }

            bool result = string.Equals(measurementUnit.Name, measurementUnitToCompareWith.Name) &&
                          string.Equals(measurementUnit.Symbol, measurementUnitToCompareWith.Symbol) &&
                          measurementUnit.ConversionRate == measurementUnitToCompareWith.ConversionRate &&
                          measurementUnit.Type == measurementUnitToCompareWith.Type &&
                          measurementUnit.ScaleID == measurementUnitToCompareWith.ScaleID &&
                          measurementUnit.ScaleFactor == measurementUnitToCompareWith.ScaleFactor;

            return result;
        }

        /// <summary>
        /// Compares two commodities
        /// </summary>
        /// <param name="commodity">The compared commodity</param>
        /// <param name="commodityChangeSet">The 'ChangeSet' of commodity</param>
        /// <param name="comparedCommodity">The commodity to compare with</param>
        /// <param name="comparedCommodityChangeSet">The 'ChangeSet' of commodity to compare with</param>
        /// <returns>True if commodities are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, IsMasterData.
        /// </remarks>
        private bool AreEqual(
            Commodity commodity,
            IDataSourceManager commodityChangeSet,
            Commodity comparedCommodity,
            IDataSourceManager comparedCommodityChangeSet)
        {
            if (commodity == null && comparedCommodity == null)
            {
                return true;
            }

            if ((commodity == null && comparedCommodity != null) || (commodity != null && comparedCommodity == null))
            {
                return false;
            }

            bool result = string.Equals(commodity.Name, comparedCommodity.Name) &&
                          string.Equals(commodity.Remarks, comparedCommodity.Remarks) &&
                          string.Equals(commodity.PartNumber, comparedCommodity.PartNumber) &&
                          commodity.Price == comparedCommodity.Price &&
                          commodity.Weight == comparedCommodity.Weight &&
                          commodity.RejectRatio == comparedCommodity.RejectRatio &&
                          commodity.Amount == comparedCommodity.Amount &&
                          commodity.IsDeleted == comparedCommodity.IsDeleted;

            // If commodities are not equal -> return false;
            if (!result)
            {
                return false;
            }

            // Verifies if Manufacturers of commodities are equal.
            result = AreEqual(commodity.Manufacturer, comparedCommodity.Manufacturer);
            if (!result)
            {
                return false;
            }

            // Retrieve the commodity's media
            Media commodityMedia = commodity.Media;
            if (commodityMedia == null)
            {
                MediaManager mediaManager1 = new MediaManager(commodityChangeSet);
                commodityMedia = mediaManager1.GetPicture(commodity);
            }

            // Retrieve the compared commodity's media
            Media comparedCommodityMedia = comparedCommodity.Media;
            if (comparedCommodityMedia == null)
            {
                MediaManager mediaManager2 = new MediaManager(comparedCommodityChangeSet);
                comparedCommodityMedia = mediaManager2.GetPicture(comparedCommodity);
            }

            // Verifies if Medias of commodities are equal.
            result = AreEqual(commodityMedia, comparedCommodityMedia);
            if (!result)
            {
                return false;
            }

            // Verifies if WeightUnits of commodities are equal.
            result = AreEqual(commodity.WeightUnitBase, comparedCommodity.WeightUnitBase);

            return result;
        }

        /// <summary>
        /// Compares two medias
        /// </summary>
        /// <param name="media">The compared media</param>
        /// <param name="mediaToCompareWith">The media to compare with</param>
        /// <returns>True if medias are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, IsMasterData.
        /// </remarks>
        private bool AreEqual(Media media, Media mediaToCompareWith)
        {
            if (media == null && mediaToCompareWith == null)
            {
                return true;
            }

            if ((media == null && mediaToCompareWith != null) || (media != null && mediaToCompareWith == null))
            {
                return false;
            }

            bool result = media.Type == mediaToCompareWith.Type &&
                          media.Content.SequenceEqual(mediaToCompareWith.Content) &&
                          media.Size == mediaToCompareWith.Size &&
                          string.Equals(media.OriginalFileName, mediaToCompareWith.OriginalFileName);

            return result;
        }

        /// <summary>
        /// Compares two dies
        /// </summary>
        /// <param name="die">The compared die</param>
        /// <param name="dieChangeSet">The 'ChangeSet' of die</param>
        /// <param name="comparedDie">The die to compare with</param>
        /// <param name="comparedDieChangeSet">The 'ChangeSet' of die to compare with</param>
        /// <returns>True if dies are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, IsMasterData.
        /// </remarks>
        private bool AreEqual(Die die, IDataSourceManager dieChangeSet, Die comparedDie, IDataSourceManager comparedDieChangeSet)
        {
            if (die == null && comparedDie == null)
            {
                return true;
            }

            if ((die == null && comparedDie != null) || (die != null && comparedDie == null))
            {
                return false;
            }

            bool result = string.Equals(die.Name, comparedDie.Name) &&
                          string.Equals(die.Remark, comparedDie.Remark) &&
                          die.IsDeleted == comparedDie.IsDeleted &&
                          die.Type == comparedDie.Type &&
                          die.Investment == comparedDie.Investment &&
                          die.ReusableInvest == comparedDie.ReusableInvest &&
                          die.Wear == comparedDie.Wear &&
                          die.IsWearInPercentage == comparedDie.IsWearInPercentage &&
                          die.Maintenance == comparedDie.Maintenance &&
                          die.IsMaintenanceInPercentage == comparedDie.IsMaintenanceInPercentage &&
                          die.LifeTime == comparedDie.LifeTime &&
                          die.AllocationRatio == comparedDie.AllocationRatio &&
                          die.DiesetsNumberPaidByCustomer == comparedDie.DiesetsNumberPaidByCustomer &&
                          die.CostAllocationBasedOnPartsPerLifeTime == comparedDie.CostAllocationBasedOnPartsPerLifeTime &&
                          die.CostAllocationNumberOfParts == comparedDie.CostAllocationNumberOfParts;

            // If entities are not equal -> return false.
            if (!result)
            {
                return false;
            }

            // Verifies if manufacturers of dies are equal.
            result = AreEqual(die.Manufacturer, comparedDie.Manufacturer);
            if (!result)
            {
                return false;
            }

            // Retrieve the media of die
            Media dieMedia = die.Media;
            if (dieMedia == null)
            {
                MediaManager mediaManager1 = new MediaManager(dieChangeSet);
                dieMedia = mediaManager1.GetPictureOrVideo(die);
            }

            // Retrieve the media of compared die
            Media comparedDieMedia = comparedDie.Media;
            if (comparedDieMedia == null)
            {
                MediaManager mediaManager2 = new MediaManager(comparedDieChangeSet);
                comparedDieMedia = mediaManager2.GetPictureOrVideo(comparedDie);
            }

            // Verifies if medias of dies are equal.
            result = AreEqual(dieMedia, comparedDieMedia);

            return result;
        }

        /// <summary>
        /// Compares two raw materials 
        /// </summary>
        /// <param name="material">The compared raw material</param>
        /// <param name="materialChangeSet">The 'ChangeSet' of raw material</param>
        /// <param name="comparedMaterial">The raw material to compare with</param>
        /// <param name="comparedMaterialChangeSet">The 'ChangeSet' of raw material to compare with</param>
        /// <returns>True if raw materials are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, IsMasterData.
        /// </remarks>
        private bool AreEqual(
            RawMaterial material,
            IDataSourceManager materialChangeSet,
            RawMaterial comparedMaterial,
            IDataSourceManager comparedMaterialChangeSet)
        {
            if (material == null && comparedMaterial == null)
            {
                return true;
            }

            if ((material == null && comparedMaterial != null) || (material != null && comparedMaterial == null))
            {
                return false;
            }

            bool result = string.Equals(material.Name, comparedMaterial.Name) &&
                          string.Equals(material.NormName, comparedMaterial.NormName) &&
                          string.Equals(material.Remarks, comparedMaterial.Remarks) &&
                          string.Equals(material.NameUK, comparedMaterial.NameUK) &&
                          string.Equals(material.NameUS, comparedMaterial.NameUS) &&
                          material.Price == comparedMaterial.Price &&
                          material.RejectRatio == comparedMaterial.RejectRatio &&
                          material.Remarks == comparedMaterial.Remarks &&
                          material.ParentWeight == comparedMaterial.ParentWeight &&
                          material.Quantity == comparedMaterial.Quantity &&
                          material.ScrapRefundRatio == comparedMaterial.ScrapRefundRatio &&
                          material.Loss == comparedMaterial.Loss &&
                          material.IsDeleted == comparedMaterial.IsDeleted &&
                          material.IsScrambled == comparedMaterial.IsScrambled &&
                          material.Sprue == comparedMaterial.Sprue &&
                          material.ScrapCalculationType == comparedMaterial.ScrapCalculationType &&
                          string.Equals(material.YieldStrength, comparedMaterial.YieldStrength) &&
                          string.Equals(material.RuptureStrength, comparedMaterial.RuptureStrength) &&
                          string.Equals(material.Density, comparedMaterial.Density) &&
                          string.Equals(material.MaxElongation, comparedMaterial.MaxElongation) &&
                          string.Equals(material.GlassTransitionTemperature, comparedMaterial.GlassTransitionTemperature) &&
                          string.Equals(material.Rx, comparedMaterial.Rx) &&
                          string.Equals(material.Rm, comparedMaterial.Rm);

            // If raw materials are not equal => return false.
            if (!result)
            {
                return false;
            }

            // Retrieve the material's media
            Media materialMedia = material.Media;
            if (materialMedia == null)
            {
                MediaManager mediaManager1 = new MediaManager(materialChangeSet);
                materialMedia = mediaManager1.GetPicture(material);
            }

            // Retrieve the compared material's media
            Media comparedMaterialMedia = comparedMaterial.Media;
            if (comparedMaterialMedia == null)
            {
                MediaManager mediaManager2 = new MediaManager(comparedMaterialChangeSet);
                comparedMaterialMedia = mediaManager2.GetPicture(comparedMaterial);
            }

            // Verifies if Medias of materials are equal.
            result = AreEqual(materialMedia, comparedMaterialMedia);
            if (!result)
            {
                return false;
            }

            // Verifies if Manufacturers of materials are equal.
            result = AreEqual(material.Manufacturer, comparedMaterial.Manufacturer);
            if (!result)
            {
                return false;
            }

            // Verifies if ClassificationLevel1 of materials are equal.
            result = AreEqual(material.MaterialsClassificationL1, comparedMaterial.MaterialsClassificationL1);
            if (!result)
            {
                return false;
            }

            // Verifies if ClassificationLevel2 of materials are equal.
            result = AreEqual(material.MaterialsClassificationL2, comparedMaterial.MaterialsClassificationL2);
            if (!result)
            {
                return false;
            }

            // Verifies if ClassificationLevel3 of materials are equal.
            result = AreEqual(material.MaterialsClassificationL3, comparedMaterial.MaterialsClassificationL3);
            if (!result)
            {
                return false;
            }

            // Verifies if ClassificationLevel4 of materials are equal.
            result = AreEqual(material.MaterialsClassificationL4, comparedMaterial.MaterialsClassificationL4);
            if (!result)
            {
                return false;
            }

            // Verifies if PriceUnitBase of materials are equal.
            result = AreEqual(material.PriceUnitBase, comparedMaterial.PriceUnitBase);
            if (!result)
            {
                return false;
            }

            // Verifies if ParentWeightUnitBase of materials are equal.
            result = AreEqual(material.ParentWeightUnitBase, comparedMaterial.ParentWeightUnitBase);
            if (!result)
            {
                return false;
            }

            // Verifies if QuantityUnitBase of materials are equal.
            result = AreEqual(material.QuantityUnitBase, comparedMaterial.QuantityUnitBase);
            if (!result)
            {
                return false;
            }

            // Verifies if DeliveryTypes of raw materials are equal.
            result = AreEqual(material.DeliveryType, comparedMaterial.DeliveryType);

            return result;
        }

        /// <summary>
        /// Compares two materials classifications 
        /// </summary>
        /// <param name="classification">The compared material classification</param>
        /// <param name="classificationToCompareWith">The material classification to compare with</param>
        /// <returns>True if materials classifications are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: IsReleased, Guid.
        /// </remarks>
        private bool AreEqual(MaterialsClassification classification, MaterialsClassification classificationToCompareWith)
        {
            if (classification == null && classificationToCompareWith == null)
            {
                return true;
            }

            if ((classification == null && classificationToCompareWith != null) || (classification != null && classificationToCompareWith == null))
            {
                return false;
            }

            bool result = string.Equals(classification.Name, classificationToCompareWith.Name);

            return result;
        }

        /// <summary>
        /// Compares two raw material delivery types
        /// </summary>
        /// <param name="deliveryType">The compared raw material delivery type</param>
        /// <param name="deliveryTypeToCompareWith">The raw material delivery type to compare with</param>
        /// <returns>True if raw material delivery types are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Guid.
        /// </remarks>
        private bool AreEqual(RawMaterialDeliveryType deliveryType, RawMaterialDeliveryType deliveryTypeToCompareWith)
        {
            if (deliveryType == null && deliveryTypeToCompareWith == null)
            {
                return true;
            }

            if ((deliveryType == null && deliveryTypeToCompareWith != null) || (deliveryType != null && deliveryTypeToCompareWith == null))
            {
                return false;
            }

            bool result = string.Equals(deliveryType.Name, deliveryTypeToCompareWith.Name);

            return result;
        }

        /// <summary>
        /// Compares two machines
        /// </summary>
        /// <param name="machine">The compared machine</param>
        /// <param name="machineChangeSet">The 'ChangeSet' of the machine</param>
        /// <param name="comparedMachine">The machine to compare with</param>
        /// <param name="comparedMachineChangeSet">The 'ChangeSet' of machine to compare with</param>
        /// <returns>True if machines are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Index, Guid,IsMasterData.
        /// </remarks>
        private bool AreEqual(
            Machine machine,
            IDataSourceManager machineChangeSet,
            Machine comparedMachine,
            IDataSourceManager comparedMachineChangeSet)
        {
            if (machine == null && comparedMachine == null)
            {
                return true;
            }

            if ((machine == null && comparedMachine != null) || (machine != null && comparedMachine == null))
            {
                return false;
            }

            bool result = string.Equals(machine.Name, comparedMachine.Name) &&
                          string.Equals(machine.RegistrationNumber, comparedMachine.RegistrationNumber) &&
                          string.Equals(machine.DescriptionOfFunction, comparedMachine.DescriptionOfFunction) &&
                          string.Equals(machine.InvestmentRemarks, comparedMachine.InvestmentRemarks) &&
                          string.Equals(machine.MountingCubeLength, comparedMachine.MountingCubeLength) &&
                          string.Equals(machine.MountingCubeWidth, comparedMachine.MountingCubeWidth) &&
                          string.Equals(machine.MountingCubeHeight, comparedMachine.MountingCubeHeight) &&
                          string.Equals(machine.MaxDiameterRodPerChuckMaxThickness, comparedMachine.MaxDiameterRodPerChuckMaxThickness) &&
                          string.Equals(machine.MaxPartsWeightPerCapacity, comparedMachine.MaxPartsWeightPerCapacity) &&
                          string.Equals(machine.LockingForce, comparedMachine.LockingForce) &&
                          string.Equals(machine.PressCapacity, comparedMachine.PressCapacity) &&
                          string.Equals(machine.MaximumSpeed, comparedMachine.MaximumSpeed) &&
                          string.Equals(machine.RapidFeedPerCuttingSpeed, comparedMachine.RapidFeedPerCuttingSpeed) &&
                          string.Equals(machine.StrokeRate, comparedMachine.StrokeRate) &&
                          string.Equals(machine.Other, comparedMachine.Other) &&
                          machine.ManufacturingYear == comparedMachine.ManufacturingYear &&
                          machine.FloorSize == comparedMachine.FloorSize &&
                          machine.WorkspaceArea == comparedMachine.WorkspaceArea &&
                          machine.PowerConsumption == comparedMachine.PowerConsumption &&
                          machine.AirConsumption == comparedMachine.AirConsumption &&
                          machine.WaterConsumption == comparedMachine.WaterConsumption &&
                          machine.FullLoadRate == comparedMachine.FullLoadRate &&
                          machine.MaxCapacity == comparedMachine.MaxCapacity &&
                          machine.OEE == comparedMachine.OEE &&
                          machine.Availability == comparedMachine.Availability &&
                          machine.MachineInvestment == comparedMachine.MachineInvestment &&
                          machine.SetupInvestment == comparedMachine.SetupInvestment &&
                          machine.AdditionalEquipmentInvestment == comparedMachine.AdditionalEquipmentInvestment &&
                          machine.FundamentalSetupInvestment == comparedMachine.FundamentalSetupInvestment &&
                          machine.ExternalWorkCost == comparedMachine.ExternalWorkCost &&
                          machine.MaterialCost == comparedMachine.MaterialCost &&
                          machine.ConsumablesCost == comparedMachine.ConsumablesCost &&
                          machine.RepairsCost == comparedMachine.RepairsCost &&
                          machine.KValue == comparedMachine.KValue &&
                          machine.CalculateWithKValue == comparedMachine.CalculateWithKValue &&
                          machine.IsDeleted == comparedMachine.IsDeleted &&
                          machine.IsScrambled == comparedMachine.IsScrambled &&
                          machine.ManualConsumableCostCalc == comparedMachine.ManualConsumableCostCalc &&
                          machine.ManualConsumableCostPercentage == comparedMachine.ManualConsumableCostPercentage &&
                          machine.IsProjectSpecific == comparedMachine.IsProjectSpecific &&
                          machine.DepreciationPeriod == comparedMachine.DepreciationPeriod &&
                          machine.DepreciationRate == comparedMachine.DepreciationRate;

            // If machines are not equal -> return false.
            if (!result)
            {
                return false;
            }

            // Retrieve the media of machine
            Media machineMedia = machine.Media;
            if (machineMedia == null)
            {
                MediaManager mediaManager1 = new MediaManager(machineChangeSet);
                machineMedia = mediaManager1.GetPicture(machine);
            }

            // Retrieve the media of the machine to compare with
            Media comparedMachineMedia = comparedMachine.Media;
            if (comparedMachineMedia == null)
            {
                MediaManager mediaManager2 = new MediaManager(comparedMachineChangeSet);
                comparedMachineMedia = mediaManager2.GetPicture(comparedMachine);
            }

            // Verifies if Medias are equal.
            result = AreEqual(machineMedia, comparedMachineMedia);
            if (!result)
            {
                return false;
            }

            // Verifies if Manufacturers are equal.
            result = AreEqual(machine.Manufacturer, comparedMachine.Manufacturer);
            if (!result)
            {
                return false;
            }

            // Verifies if main classifications are equal
            result = AreEqual(machine.MainClassification, comparedMachine.MainClassification);
            if (!result)
            {
                return false;
            }

            // Verifies if type classifications are equal.
            result = AreEqual(machine.TypeClassification, comparedMachine.TypeClassification);
            if (!result)
            {
                return false;
            }

            // Verifies if sub classifications are equal.
            result = AreEqual(machine.SubClassification, comparedMachine.SubClassification);
            if (!result)
            {
                return false;
            }

            // Verifies if classifications level4 are equal.
            result = AreEqual(machine.ClassificationLevel4, comparedMachine.ClassificationLevel4);

            return result;
        }

        /// <summary>
        /// Compares two machines classifications
        /// </summary>
        /// <param name="classification">The compared machine classification</param>
        /// <param name="classificationToCompareWith">The machine classification to compare with</param>
        /// <returns>True if machines classifications are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: IsReleased, Guid.
        /// </remarks>
        private bool AreEqual(MachinesClassification classification, MachinesClassification classificationToCompareWith)
        {
            if (classification == null && classificationToCompareWith == null)
            {
                return true;
            }

            if ((classification == null && classificationToCompareWith != null) || (classification != null && classificationToCompareWith == null))
            {
                return false;
            }

            bool result = string.Equals(classification.Name, classificationToCompareWith.Name);

            return result;
        }

        /// <summary>
        /// Compares two overhead settings
        /// </summary>
        /// <param name="setting">The compared overhead setting</param>
        /// <param name="settingToCompareWith">The overhead setting to compare with</param>
        /// <returns>True if overhead settings are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, LastUpdateTime, Guid, IsMasterData.
        /// </remarks>
        private bool AreEqual(OverheadSetting setting, OverheadSetting settingToCompareWith)
        {
            if (setting == null && settingToCompareWith == null)
            {
                return true;
            }

            if ((setting == null && settingToCompareWith != null) || (setting != null && settingToCompareWith == null))
            {
                return false;
            }

            bool result = setting.MaterialOverhead == settingToCompareWith.MaterialOverhead &&
                          setting.ConsumableOverhead == settingToCompareWith.ConsumableOverhead &&
                          setting.CommodityOverhead == settingToCompareWith.CommodityOverhead &&
                          setting.ExternalWorkOverhead == settingToCompareWith.ExternalWorkOverhead &&
                          setting.ManufacturingOverhead == settingToCompareWith.ManufacturingOverhead &&
                          setting.MaterialMargin == settingToCompareWith.MaterialMargin &&
                          setting.ConsumableMargin == settingToCompareWith.ConsumableMargin &&
                          setting.CommodityMargin == settingToCompareWith.CommodityMargin &&
                          setting.ExternalWorkMargin == settingToCompareWith.ExternalWorkMargin &&
                          setting.ManufacturingMargin == settingToCompareWith.ManufacturingMargin &&
                          setting.OtherCostOHValue == settingToCompareWith.OtherCostOHValue &&
                          setting.PackagingOHValue == settingToCompareWith.PackagingOHValue &&
                          setting.LogisticOHValue == settingToCompareWith.LogisticOHValue &&
                          setting.SalesAndAdministrationOHValue == settingToCompareWith.SalesAndAdministrationOHValue;

            return result;
        }

        /// <summary>
        /// Compares two parts 
        /// </summary>
        /// <param name="part">The compared part</param>
        /// <param name="partChangeSet">The 'ChangeSet' of compared part</param>
        /// <param name="comparedPart">The part to compare with</param>
        /// <param name="comparedPartChangeSet">The 'ChangeSet' of part to compare with</param>
        /// <param name="includeVideos">Indicates whether to compare parts videos or not. </param>
        /// <returns>True if parts are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, IsMasterData.
        /// </remarks>
        private bool AreEqual(
            Part part,
            IDataSourceManager partChangeSet,
            Part comparedPart,
            IDataSourceManager comparedPartChangeSet,
            bool includeVideos)
        {
            if (part == null && comparedPart == null)
            {
                return true;
            }

            if ((part == null && comparedPart != null) || (part != null && comparedPart == null))
            {
                return false;
            }

            bool result = string.Equals(part.Name, comparedPart.Name) &&
                          string.Equals(part.Number, comparedPart.Number) &&
                          string.Equals(part.Description, comparedPart.Description) &&
                          string.Equals(part.AdditionalDescription, comparedPart.AdditionalDescription) &&
                          string.Equals(part.ManufacturerDescription, comparedPart.ManufacturerDescription) &&
                          string.Equals(part.AdditionalRemarks, comparedPart.AdditionalRemarks) &&
                          string.Equals(part.CalculationVariant, comparedPart.CalculationVariant) &&
                          string.Equals(part.ManufacturingCountry, comparedPart.ManufacturingCountry) &&
                          string.Equals(part.ManufacturingSupplier, comparedPart.ManufacturingSupplier) &&
                          part.CalculationStatus == comparedPart.CalculationStatus &&
                          part.CalculationAccuracy == comparedPart.CalculationAccuracy &&
                          part.CalculationApproach == comparedPart.CalculationApproach &&
                          part.BatchSizePerYear == comparedPart.BatchSizePerYear &&
                          part.PurchasePrice == comparedPart.PurchasePrice &&
                          part.TargetPrice == comparedPart.TargetPrice &&
                          part.DelivertType == comparedPart.DelivertType &&
                          part.AssetRate == comparedPart.AssetRate &&
                          part.SBMActive == comparedPart.SBMActive &&
                          part.IsExternal == comparedPart.IsExternal &&
                          part.IsDeleted == comparedPart.IsDeleted &&
                          part.DevelopmentCost == comparedPart.DevelopmentCost &&
                          part.ProjectInvest == comparedPart.ProjectInvest &&
                          part.OtherCost == comparedPart.OtherCost &&
                          part.PackagingCost == comparedPart.PackagingCost &&
                          part.CalculateLogisticCost == comparedPart.CalculateLogisticCost &&
                          part.LogisticCost == comparedPart.LogisticCost &&                        
                          part.EstimatedCost == comparedPart.EstimatedCost &&
                          part.YearlyProductionQuantity == comparedPart.YearlyProductionQuantity &&
                          part.LifeTime == comparedPart.LifeTime &&
                          part.PaymentTerms == comparedPart.PaymentTerms &&
                          part.Version == comparedPart.Version &&
                          part.Reusable == comparedPart.Reusable &&
                          part.ManufacturingRatio == comparedPart.ManufacturingRatio;

            // If parts are not equal => return false.
            if (!result)
            {
                return false;
            }

            // Retrieve the part's media
            Collection<Media> partMedia = new Collection<Media>(part.Media.ToList());
            if (partMedia.Count == 0)
            {
                MediaManager mediaManager1 = new MediaManager(partChangeSet);
                partMedia = mediaManager1.GetAllMedia(part);
            }

            // Retrieve the comparedPart's media
            Collection<Media> comparedPartMedia = new Collection<Media>(comparedPart.Media.ToList());
            if (comparedPartMedia.Count == 0)
            {
                MediaManager mediaManager2 = new MediaManager(comparedPartChangeSet);
                comparedPartMedia = mediaManager2.GetAllMedia(comparedPart);
            }

            // Verifies if Medias of parts are equal. 
            foreach (Media media in partMedia)
            {
                bool mediaIsVideo = media.Type == MediaType.Video;

                if (includeVideos || !mediaIsVideo)
                {
                    Media correspondingMedia = comparedPartMedia.Where(m => string.Equals(m.OriginalFileName, media.OriginalFileName) && m.Content.SequenceEqual(media.Content)).FirstOrDefault();

                    result = AreEqual(media, correspondingMedia);
                    if (!result)
                    {
                        return false;
                    }
                }
            }

            // Verifies if commodities of parts are equal.
            foreach (Commodity commodity in part.Commodities)
            {
                Commodity correspondingCommodity = comparedPart.Commodities.Where(c => string.Equals(c.Name, commodity.Name)).FirstOrDefault();

                result = AreEqual(commodity, partChangeSet, correspondingCommodity, comparedPartChangeSet);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if raw materials of parts are equal.
            foreach (RawMaterial material in part.RawMaterials)
            {
                RawMaterial correspondingMaterial = comparedPart.RawMaterials.Where(m => string.Equals(m.Name, material.Name)).FirstOrDefault();

                result = AreEqual(material, partChangeSet, correspondingMaterial, comparedPartChangeSet);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if processes of parts are equals.
            result = AreEqual(part.Process, partChangeSet, comparedPart.Process, comparedPartChangeSet);
            if (!result)
            {
                return false;
            }

            // Verifies if manufacturers of parts are equals.
            result = AreEqual(part.Manufacturer, comparedPart.Manufacturer);
            if (!result)
            {
                return false;
            }

            // Verifies if overhead settings of parts are equals.
            result = AreEqual(part.OverheadSettings, comparedPart.OverheadSettings);
            if (!result)
            {
                return false;
            }

            // Verifies if country settings of parts are equal.
            result = AreEqual(part.CountrySettings, comparedPart.CountrySettings);
            if (!result)
            {
                return false;
            }

            // Verifies if calculators of parts are equal.
            result = AreEqual(part.CalculatorUser, comparedPart.CalculatorUser);

            return result;
        }

        /// <summary>
        /// Compares two assemblies
        /// </summary>
        /// <param name="assembly">The compared assembly</param>
        /// <param name="assemblyChangeSet">The 'ChangeSet' of assembly</param>
        /// <param name="comparedAssembly">The assembly to compare with</param>
        /// <param name="comparedAssemblyChangeSet">The 'ChangeSet' of compared assembly</param>
        /// <param name="includeVideos">Indicating whether to include in comparison video files</param>
        /// <returns>True if assemblies are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, LastChangeTimestamp, Guid, IsMasterData.
        /// </remarks>
        private bool AreEqual(
            Assembly assembly,
            IDataSourceManager assemblyChangeSet,
            Assembly comparedAssembly,
            IDataSourceManager comparedAssemblyChangeSet,
            bool includeVideos)
        {
            if (assembly == null && comparedAssembly == null)
            {
                return true;
            }

            if ((assembly == null && comparedAssembly != null) || (assembly != null && comparedAssembly == null))
            {
                return false;
            }

            bool result = string.Equals(assembly.Name, comparedAssembly.Name) &&
                          string.Equals(assembly.Number, comparedAssembly.Number) &&
                          string.Equals(assembly.CalculationVariant, comparedAssembly.CalculationVariant) &&
                          string.Equals(assembly.AssemblingCountry, comparedAssembly.AssemblingCountry) &&
                          string.Equals(assembly.AssemblingSupplier, comparedAssembly.AssemblingSupplier) &&
                          assembly.IsDeleted == comparedAssembly.IsDeleted &&
                          assembly.DevelopmentCost == comparedAssembly.DevelopmentCost &&
                          assembly.ProjectInvest == comparedAssembly.ProjectInvest &&
                          assembly.OtherCost == comparedAssembly.OtherCost &&
                          assembly.PackagingCost == comparedAssembly.PackagingCost &&
                          assembly.CalculateLogisticCost == comparedAssembly.CalculateLogisticCost &&
                          assembly.LogisticCost == comparedAssembly.LogisticCost &&                          
                          assembly.BatchSizePerYear == comparedAssembly.BatchSizePerYear &&
                          assembly.YearlyProductionQuantity == comparedAssembly.YearlyProductionQuantity &&
                          assembly.LifeTime == comparedAssembly.LifeTime &&
                          assembly.PaymentTerms == comparedAssembly.PaymentTerms &&
                          assembly.PurchasePrice == comparedAssembly.PurchasePrice &&
                          assembly.TargetPrice == comparedAssembly.TargetPrice &&
                          assembly.DeliveryType == comparedAssembly.DeliveryType &&
                          assembly.SBMActive == comparedAssembly.SBMActive &&
                          assembly.AssetRate == comparedAssembly.AssetRate &&
                          assembly.IsExternal == comparedAssembly.IsExternal &&
                          assembly.CalculationAccuracy == comparedAssembly.CalculationAccuracy &&
                          assembly.EstimatedCost == comparedAssembly.EstimatedCost &&
                          assembly.Version == comparedAssembly.Version &&
                          assembly.Reusable == comparedAssembly.Reusable &&
                          assembly.CalculationApproach == comparedAssembly.CalculationApproach;

            // If assemblies are not equal => return false.
            if (!result)
            {
                return false;
            }

            // Retrieve the assembly's media
            Collection<Media> assemblyMedia = new Collection<Media>(assembly.Media.ToList());
            if (assemblyMedia.Count == 0)
            {
                MediaManager mediaManager1 = new MediaManager(assemblyChangeSet);
                assemblyMedia = mediaManager1.GetAllMedia(assembly);
            }

            // Retrieve the compared assembly's media
            Collection<Media> comparedAssemblyMedia = new Collection<Media>(comparedAssembly.Media.ToList());
            if (comparedAssemblyMedia.Count == 0)
            {
                MediaManager mediaManager2 = new MediaManager(comparedAssemblyChangeSet);
                comparedAssemblyMedia = mediaManager2.GetAllMedia(comparedAssembly);
            }

            // Verifies if Medias of assemblies are equal.
            foreach (Media media in assemblyMedia)
            {
                bool mediaIsVideo = media.Type == MediaType.Video;

                if (includeVideos || !mediaIsVideo)
                {
                    Media correspondingMedia = comparedAssemblyMedia.Where(m => string.Equals(m.OriginalFileName, media.OriginalFileName) && m.Content.SequenceEqual(media.Content)).FirstOrDefault();

                    result = AreEqual(media, correspondingMedia);
                    if (!result)
                    {
                        return false;
                    }
                }
            }

            // Verifies if processes of assemblies are equals.
            result = AreEqual(assembly.Process, assemblyChangeSet, comparedAssembly.Process, comparedAssemblyChangeSet);
            if (!result)
            {
                return false;
            }

            // Verifies if manufacturers of assemblies are equals.
            result = AreEqual(assembly.Manufacturer, comparedAssembly.Manufacturer);
            if (!result)
            {
                return false;
            }

            // Verifies if overhead settings of assemblies are equals.
            result = AreEqual(assembly.OverheadSettings, comparedAssembly.OverheadSettings);
            if (!result)
            {
                return false;
            }

            // Verifies if country settings of assemblies are equal.
            result = AreEqual(assembly.CountrySettings, comparedAssembly.CountrySettings);
            if (!result)
            {
                return false;
            }

            // Verifies if parts of assemblies are equal.
            foreach (Part part in assembly.Parts)
            {
                Part correspondingPart = comparedAssembly.Parts.Where(p => string.Equals(p.Name, part.Name)).FirstOrDefault();

                result = AreEqual(part, assemblyChangeSet, correspondingPart, comparedAssemblyChangeSet, includeVideos);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if subassemblies of assemblies are equal.
            foreach (Assembly subassembly in assembly.Subassemblies)
            {
                Assembly correspondingSubassembly = comparedAssembly.Subassemblies.Where(s => string.Equals(s.Name, subassembly.Name)).FirstOrDefault();

                result = AreEqual(subassembly, assemblyChangeSet, correspondingSubassembly, comparedAssemblyChangeSet, includeVideos);
                if (!result)
                {
                    return false;
                }
            }

            return result;
        }

        /// <summary>
        /// Compares two projects.
        /// </summary>
        /// <param name="project">The compared project</param>
        /// <param name="projectChangeSet">The 'ChangeSet' of compared project</param>
        /// <param name="comparedProject">The project to compare with</param>
        /// <param name="comparedProjectChangeSet">The 'ChangeSet' of project to compare with</param>
        /// <returns>True if projects are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: IsReleased, Owner, Guid, LastChangeTimestamp.
        /// </remarks>
        private bool AreEqual(
            Project project,
            IDataSourceManager projectChangeSet,
            Project comparedProject,
            IDataSourceManager comparedProjectChangeSet)
        {
            if (project == null && comparedProject == null)
            {
                return true;
            }

            if ((project == null && comparedProject != null) || (project != null && comparedProject == null))
            {
                return false;
            }

            bool result = string.Equals(project.Name, comparedProject.Name) &&
                          string.Equals(project.Number, comparedProject.Number) &&
                          string.Equals(project.Partner, comparedProject.Partner) &&
                          string.Equals(project.ProductName, comparedProject.ProductName) &&
                          string.Equals(project.ProductNumber, comparedProject.ProductNumber) &&
                          string.Equals(project.ProductDescription, comparedProject.ProductDescription) &&
                          string.Equals(project.CustomerDescription, comparedProject.CustomerDescription) &&
                          string.Equals(project.Remarks, comparedProject.Remarks) &&
                          string.Equals(
                          project.StartDate.GetValueOrDefault().ToShortDateString(),
                          comparedProject.StartDate.GetValueOrDefault().ToShortDateString()) &&
                          string.Equals(
                          project.EndDate.GetValueOrDefault().ToShortDateString(),
                          comparedProject.EndDate.GetValueOrDefault().ToShortDateString()) &&
                          string.Equals(
                          project.SourceOverheadSettingsLastUpdate.GetValueOrDefault().ToShortDateString(),
                          comparedProject.SourceOverheadSettingsLastUpdate.GetValueOrDefault().ToShortDateString()) &&
                          project.Status == comparedProject.Status &&
                          project.YearlyProductionQuantity == comparedProject.YearlyProductionQuantity &&
                          project.LifeTime == comparedProject.LifeTime &&
                          project.DepreciationPeriod == comparedProject.DepreciationPeriod &&
                          project.DepreciationRate == comparedProject.DepreciationRate &&
                          project.IsDeleted == comparedProject.IsDeleted &&
                          project.LogisticCostRatio == comparedProject.LogisticCostRatio &&
                          project.Version == comparedProject.Version;

            // If projects are not equal => return false.
            if (!result)
            {
                return false;
            }

            // Retrieve the media of project
            Collection<Media> projectMedia = new Collection<Media>(project.Media.ToList());
            if (projectMedia.Count == 0)
            {
                MediaManager mediaManager1 = new MediaManager(projectChangeSet);
                projectMedia = mediaManager1.GetAllMedia(project);
            }

            // Retrieve the media of compared project
            Collection<Media> comparedProjectMedia = new Collection<Media>(comparedProject.Media.ToList());
            if (comparedProjectMedia.Count == 0)
            {
                MediaManager mediaManager2 = new MediaManager(comparedProjectChangeSet);
                comparedProjectMedia = mediaManager2.GetAllMedia(comparedProject);
            }

            // Verifies medias of projects are equal
            foreach (Media media in projectMedia)
            {
                Media correspondingMedia = comparedProjectMedia.Where(m => string.Equals(m.OriginalFileName, media.OriginalFileName) && m.Content.SequenceEqual(media.Content)).FirstOrDefault();

                result = AreEqual(media, correspondingMedia);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if project leaders are equal.
            result = AreEqual(project.ProjectLeader, comparedProject.ProjectLeader);
            if (!result)
            {
                return false;
            }

            // Verifies if ResponsibleCalculators of projects are equal.
            result = AreEqual(project.ResponsibleCalculator, comparedProject.ResponsibleCalculator);
            if (!result)
            {
                return false;
            }

            // Verifies if Customers of projects are equal.
            result = AreEqual(project.Customer, comparedProject.Customer);
            if (!result)
            {
                return false;
            }

            // Verifies if Overhead settings of projects are equal.
            result = AreEqual(project.OverheadSettings, comparedProject.OverheadSettings);
            if (!result)
            {
                return false;
            }

            // Verifies if parts of projects are equal.
            foreach (Part part in project.Parts)
            {
                Part correspondingPart = comparedProject.Parts.Where(p => string.Equals(p.Name, part.Name)).FirstOrDefault();

                result = AreEqual(part, projectChangeSet, correspondingPart, comparedProjectChangeSet, true);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if subassemblies of projects are equal.
            foreach (Assembly assembly in project.Assemblies)
            {
                Assembly correspondingSubassembly = comparedProject.Assemblies.Where(a => string.Equals(a.Name, assembly.Name)).FirstOrDefault();

                result = AreEqual(assembly, projectChangeSet, correspondingSubassembly, comparedProjectChangeSet, true);
                if (!result)
                {
                    return false;
                }
            }

            return result;
        }

        /// <summary>
        /// Compares two country settings
        /// </summary>
        /// <param name="countrySetting">The compared country setting</param>
        /// <param name="countrySettingToCompareWith">The country setting to compare with</param>
        /// <returns>True if country settings are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid.
        /// </remarks>
        private bool AreEqual(CountrySetting countrySetting, CountrySetting countrySettingToCompareWith)
        {
            if (countrySetting == null && countrySettingToCompareWith == null)
            {
                return true;
            }

            if ((countrySetting == null && countrySettingToCompareWith != null) || (countrySetting != null && countrySettingToCompareWith == null))
            {
                return false;
            }

            bool result = countrySetting.UnskilledLaborCost == countrySettingToCompareWith.UnskilledLaborCost &&
                          countrySetting.SkilledLaborCost == countrySettingToCompareWith.SkilledLaborCost &&
                          countrySetting.ForemanCost == countrySettingToCompareWith.ForemanCost &&
                          countrySetting.TechnicianCost == countrySettingToCompareWith.TechnicianCost &&
                          countrySetting.EngineerCost == countrySettingToCompareWith.EngineerCost &&
                          countrySetting.EnergyCost == countrySettingToCompareWith.EnergyCost &&
                          countrySetting.AirCost == countrySettingToCompareWith.AirCost &&
                          countrySetting.WaterCost == countrySettingToCompareWith.WaterCost &&
                          countrySetting.ProductionAreaRentalCost == countrySettingToCompareWith.ProductionAreaRentalCost &&
                          countrySetting.OfficeAreaRentalCost == countrySettingToCompareWith.OfficeAreaRentalCost &&
                          countrySetting.InterestRate == countrySettingToCompareWith.InterestRate &&
                          countrySetting.ShiftCharge1ShiftModel == countrySettingToCompareWith.ShiftCharge1ShiftModel &&
                          countrySetting.ShiftCharge2ShiftModel == countrySettingToCompareWith.ShiftCharge2ShiftModel &&
                          countrySetting.ShiftCharge3ShiftModel == countrySettingToCompareWith.ShiftCharge3ShiftModel &&
                          countrySetting.LaborAvailability == countrySettingToCompareWith.LaborAvailability &&
                          countrySetting.IsScrambled == countrySettingToCompareWith.IsScrambled;

            return result;
        }

        /// <summary>
        /// Compares two processes
        /// </summary>
        /// <param name="process">The compared process</param>
        /// <param name="processChangeSet">The 'ChangeSet' of compared process</param>
        /// <param name="comparedProcess">The process to compare with</param>
        /// <param name="comparedProcessChangeSet">The 'ChangeSet' of process to compare with</param>
        /// <returns>True if processes are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, IsMasterData.
        /// </remarks>
        private bool AreEqual(
            Process process,
            IDataSourceManager processChangeSet,
            Process comparedProcess,
            IDataSourceManager comparedProcessChangeSet)
        {
            if (process == null && comparedProcess == null)
            {
                return true;
            }

            if ((process == null && comparedProcess != null) || (process != null && comparedProcess == null))
            {
                return false;
            }

            // Verifies if steps of processes are equal.
            bool result = true;
            foreach (ProcessStep step in process.Steps)
            {
                ProcessStep correspondingStep = comparedProcess.Steps.Where(s => string.Equals(s.Name, step.Name)).FirstOrDefault();

                result = result && AreEqual(step, processChangeSet, correspondingStep, comparedProcessChangeSet, true);
                if (!result)
                {
                    return false;
                }
            }

            return result;
        }

        /// <summary>
        /// Compares two process steps
        /// </summary>
        /// <param name="step">The compared step</param>
        /// <param name="stepChangeSet">The 'ChangeSet' of compared step</param>
        /// <param name="comparedStep">The step to compare with</param>
        /// <param name="comparedStepChangeSet">The 'ChangeSet' of step to compare with</param>
        /// <param name="includePartAndAssemblyAmounts">Indicating whether to include in comparison the 
        /// ProcessStepPartAmount and ProcessStepAssemblyAmount</param>
        /// <returns>True if process steps are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, IsMasterData.
        /// </remarks>
        private bool AreEqual(
            ProcessStep step,
            IDataSourceManager stepChangeSet,
            ProcessStep comparedStep,
            IDataSourceManager comparedStepChangeSet,
            bool includePartAndAssemblyAmounts)
        {
            if (step == null && comparedStep == null)
            {
                return true;
            }

            if ((step == null && comparedStep != null) || (step != null && comparedStep == null))
            {
                return false;
            }

            bool result = string.Equals(step.Name, comparedStep.Name) &&
                          string.Equals(step.Description, comparedStep.Description) &&
                          step.Accuracy == comparedStep.Accuracy &&
                          step.CycleTime == comparedStep.CycleTime &&
                          step.ProcessTime == comparedStep.ProcessTime &&
                          step.PartsPerCycle == comparedStep.PartsPerCycle &&
                          step.ScrapAmount == comparedStep.ScrapAmount &&
                          step.Rework == comparedStep.Rework &&
                          step.ShiftsPerWeek == comparedStep.ShiftsPerWeek &&
                          step.HoursPerShift == comparedStep.HoursPerShift &&
                          step.ProductionDaysPerWeek == comparedStep.ProductionDaysPerWeek &&
                          step.ProductionWeeksPerYear == comparedStep.ProductionWeeksPerYear &&
                          step.BatchSize == comparedStep.BatchSize &&
                          step.SetupsPerBatch == comparedStep.SetupsPerBatch &&
                          step.SetupTime == comparedStep.SetupTime &&
                          step.MaxDownTime == comparedStep.MaxDownTime &&
                          step.ProductionUnskilledLabour == comparedStep.ProductionUnskilledLabour &&
                          step.ProductionSkilledLabour == comparedStep.ProductionSkilledLabour &&
                          step.ProductionForeman == comparedStep.ProductionForeman &&
                          step.ProductionTechnicians == comparedStep.ProductionTechnicians &&
                          step.ProductionEngineers == comparedStep.ProductionEngineers &&
                          step.SetupUnskilledLabour == comparedStep.SetupUnskilledLabour &&
                          step.SetupSkilledLabour == comparedStep.SetupSkilledLabour &&
                          step.SetupForeman == comparedStep.SetupForeman &&
                          step.SetupTechnicians == comparedStep.SetupTechnicians &&
                          step.SetupEngineers == comparedStep.SetupEngineers &&
                          step.IsExternal == comparedStep.IsExternal &&
                          step.Price == comparedStep.Price &&
                          step.Index == comparedStep.Index &&
                          step.ExceedShiftCost == comparedStep.ExceedShiftCost &&
                          step.ExtraShiftsNumber == comparedStep.ExtraShiftsNumber &&
                          step.ShiftCostExceedRatio == comparedStep.ShiftCostExceedRatio;

            // If steps are not equal => return false;
            if (!result)
            {
                return false;
            }

            // Verifies if Types of steps are equal.
            result = AreEqual(step.Type, comparedStep.Type);
            if (!result)
            {
                return false;
            }

            // Verifies if SubTypes of steps are equal.
            result = AreEqual(step.SubType, comparedStep.SubType);
            if (!result)
            {
                return false;
            }

            // Retrieve the media of step
            Media stepMedia = step.Media;
            if (stepMedia == null)
            {
                MediaManager mediaManager1 = new MediaManager(stepChangeSet);
                stepMedia = mediaManager1.GetPicture(step);
            }

            // Retrieve the media of comparedStep
            Media comparedStepMedia = comparedStep.Media;
            if (comparedStepMedia == null)
            {
                MediaManager mediaManager2 = new MediaManager(comparedStepChangeSet);
                comparedStepMedia = mediaManager2.GetPicture(comparedStep);
            }

            // Verifies if Medias of steps are equal.
            result = AreEqual(stepMedia, comparedStepMedia);
            if (!result)
            {
                return false;
            }

            // Verifies if CycleTimeUnits of steps are equals.
            result = AreEqual(step.CycleTimeUnit, comparedStep.CycleTimeUnit);
            if (!result)
            {
                return false;
            }

            // Verifies if ProcessStepPartAmounts are equals.
            if (includePartAndAssemblyAmounts)
            {
                foreach (ProcessStepPartAmount partAmount in step.PartAmounts)
                {
                    ProcessStepPartAmount correspondingPartAmount = comparedStep.PartAmounts.Where(p => string.Equals(p.Part.Name, partAmount.Part.Name)).FirstOrDefault();

                    result = AreEqual(partAmount, correspondingPartAmount);
                    if (!result)
                    {
                        return false;
                    }
                }

                // Verifies if ProcessStepAssemblyAmount are equals.
                foreach (ProcessStepAssemblyAmount assemblyAmount in step.AssemblyAmounts)
                {
                    ProcessStepAssemblyAmount correspondingAssemblyAmount = comparedStep.AssemblyAmounts.Where(p => string.Equals(p.Assembly.Name, assemblyAmount.Assembly.Name)).FirstOrDefault();

                    result = AreEqual(assemblyAmount, correspondingAssemblyAmount);
                    if (!result)
                    {
                        return false;
                    }
                }
            }

            // Verifies if ProcessTimeUnits are equal.
            result = AreEqual(step.ProcessTimeUnit, comparedStep.ProcessTimeUnit);
            if (!result)
            {
                return false;
            }

            // Verifies if SetupTimeUnits are equals.
            result = AreEqual(step.SetupTimeUnit, comparedStep.SetupTimeUnit);
            if (!result)
            {
                return false;
            }

            // Verifies if MaxDownTimeUnits are equals.
            result = AreEqual(step.MaxDownTimeUnit, comparedStep.MaxDownTimeUnit);
            if (!result)
            {
                return false;
            }

            // Verifies if cycle time calculations are equals.
            foreach (CycleTimeCalculation calculation in step.CycleTimeCalculations)
            {
                CycleTimeCalculation correspondingCalculation = comparedStep.CycleTimeCalculations.Where(c => string.Equals(c.Description, calculation.Description)).FirstOrDefault();

                result = AreEqual(calculation, correspondingCalculation);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if commodities of  steps are equal.
            foreach (Commodity commodity in step.Commodities)
            {
                Commodity correspondingCommodity = comparedStep.Commodities.Where(c => string.Equals(c.Name, commodity.Name)).FirstOrDefault();

                result = AreEqual(commodity, stepChangeSet, correspondingCommodity, comparedStepChangeSet);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if consumables of  steps are equal.
            foreach (Consumable consumable in step.Consumables)
            {
                Consumable correspondingConsumable = comparedStep.Consumables.Where(c => string.Equals(c.Name, consumable.Name)).FirstOrDefault();

                result = AreEqual(consumable, correspondingConsumable);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if machines of  steps are equal.
            foreach (Machine machine in step.Machines)
            {
                Machine correspondingMachine = comparedStep.Machines.Where(c => string.Equals(c.Name, machine.Name)).FirstOrDefault();

                result = AreEqual(machine, stepChangeSet, correspondingMachine, comparedStepChangeSet);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if dies of  steps are equal.
            foreach (Die die in step.Dies)
            {
                Die correspondingDie = comparedStep.Dies.Where(d => string.Equals(d.Name, die.Name)).FirstOrDefault();

                result = AreEqual(die, stepChangeSet, correspondingDie, comparedStepChangeSet);
                if (!result)
                {
                    return false;
                }
            }

            return result;
        }

        /// <summary>
        /// Compares two process step part amounts
        /// </summary>
        /// <param name="partAmount">The compared process step part amount</param>
        /// <param name="partAmountToComapareWith">The process step part amount to compare with</param>
        /// <returns>True if process step part amounts are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, IsMasterData.
        /// </remarks>
        private bool AreEqual(ProcessStepPartAmount partAmount, ProcessStepPartAmount partAmountToComapareWith)
        {
            if (partAmount == null && partAmountToComapareWith == null)
            {
                return true;
            }

            if ((partAmount == null && partAmountToComapareWith != null) || (partAmount != null && partAmountToComapareWith == null))
            {
                return false;
            }

            bool result = partAmount.Amount == partAmountToComapareWith.Amount &&
                          string.Equals(partAmount.ProcessStep.Name, partAmountToComapareWith.ProcessStep.Name) &&
                          string.Equals(partAmount.Part.Name, partAmountToComapareWith.Part.Name);

            return result;
        }

        /// <summary>
        /// Compares two process step assembly amounts
        /// </summary>
        /// <param name="assemblyAmount">The compared process step assembly amount</param>
        /// <param name="assemblyAmountToComapareWith">The process step assembly amount to compare with</param>
        /// <returns>True if process step assembly amounts are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, IsMasterData.
        /// </remarks>
        private bool AreEqual(ProcessStepAssemblyAmount assemblyAmount, ProcessStepAssemblyAmount assemblyAmountToComapareWith)
        {
            if (assemblyAmount == null && assemblyAmountToComapareWith == null)
            {
                return true;
            }

            if ((assemblyAmount == null && assemblyAmountToComapareWith != null) || (assemblyAmount != null && assemblyAmountToComapareWith == null))
            {
                return false;
            }

            bool result = assemblyAmount.Amount == assemblyAmountToComapareWith.Amount &&
                          string.Equals(assemblyAmount.ProcessStep.Name, assemblyAmountToComapareWith.ProcessStep.Name) &&
                          string.Equals(assemblyAmount.Assembly.Name, assemblyAmountToComapareWith.Assembly.Name);

            return result;
        }

        /// <summary>
        /// Compares two process steps classifications
        /// </summary>
        /// <param name="classification">The compared process step classification</param>
        /// <param name="classificationToCompareWith">The process step classification to compare with</param>
        /// <returns>True if process step classifications are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: IsReleased, Guid.
        /// </remarks>
        private bool AreEqual(ProcessStepsClassification classification, ProcessStepsClassification classificationToCompareWith)
        {
            if (classification == null && classificationToCompareWith == null)
            {
                return true;
            }

            if ((classification == null && classificationToCompareWith != null) || (classification != null && classificationToCompareWith == null))
            {
                return false;
            }

            bool result = string.Equals(classification.Name, classificationToCompareWith.Name);

            return result;
        }

        /// <summary>
        /// Compares two users
        /// </summary>
        /// <param name="user">The compared user</param>
        /// <param name="userToCompareWith">The user to compare with</param>
        /// <returns>True if medias are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: IsReleased, Guid.
        /// </remarks>
        private bool AreEqual(User user, User userToCompareWith)
        {
            if (user == null && userToCompareWith == null)
            {
                return true;
            }

            if ((user == null && userToCompareWith != null) || (user != null && userToCompareWith == null))
            {
                return false;
            }

            bool result = string.Equals(user.Name, userToCompareWith.Name) &&
                          string.Equals(user.Description, userToCompareWith.Description) &&
                          string.Equals(user.Username, userToCompareWith.Username) &&
                          string.Equals(user.Password, userToCompareWith.Password) &&
                          user.Disabled == userToCompareWith.Disabled;

            // If entities are not equal -> return false.
            if (!result)
            {
                return false;
            }

            result = AreEqual(user.UICurrency, userToCompareWith.UICurrency);

            return result;
        }

        /// <summary>
        /// Compares two countries
        /// </summary>
        /// <param name="country">The compared country</param>
        /// <param name="countryToCompareWith">The country to compare with</param>
        /// <returns>True if countries are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: IsReleased, Guid.
        /// </remarks>
        private bool AreEqual(Country country, Country countryToCompareWith)
        {
            if (country == null && countryToCompareWith == null)
            {
                return true;
            }

            if ((country == null && countryToCompareWith != null) || (country != null && countryToCompareWith == null))
            {
                return false;
            }

            bool result = string.Equals(country.Name, countryToCompareWith.Name) &&
                          AreEqual(country.Currency, countryToCompareWith.Currency) &&
                          AreEqual(country.WeightMeasurementUnit, countryToCompareWith.WeightMeasurementUnit) &&
                          AreEqual(country.LengthMeasurementUnit, countryToCompareWith.LengthMeasurementUnit) &&
                          AreEqual(country.VolumeMeasurementUnit, country.VolumeMeasurementUnit) &&
                          AreEqual(country.TimeMeasurementUnit, countryToCompareWith.TimeMeasurementUnit) &&
                          AreEqual(country.FloorMeasurementUnit, countryToCompareWith.FloorMeasurementUnit) &&
                          AreEqual(country.CountrySetting, countryToCompareWith.CountrySetting);

            return result;
        }

        /// <summary>
        /// Compares two currencies
        /// </summary>
        /// <param name="currency">The compared currency</param>
        /// <param name="currencyToCompareWith">The currency to compare with</param>
        /// <returns>True if currencies are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: IsReleased, Guid.
        /// </remarks>
        private bool AreEqual(Currency currency, Currency currencyToCompareWith)
        {
            if (currency == null && currencyToCompareWith == null)
            {
                return true;
            }

            if ((currency == null && currencyToCompareWith != null) || (currency != null && currencyToCompareWith == null))
            {
                return false;
            }

            bool result = currency.Name == currencyToCompareWith.Name
                && currency.Symbol == currencyToCompareWith.Symbol
                && currency.ExchangeRate == currencyToCompareWith.ExchangeRate
                && currency.IsoCode == currencyToCompareWith.IsoCode;

            return result;
        }

        #endregion Helpers
    }
}
