﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using ZPKTool.Common;
using ZPKTool.DataAccess;

namespace ZPKTool.Business.Tests
{
    /// <summary>
    /// This is a test class for TrashBinManager and is intended
    /// to contain all TrashBinManager Unit Tests.
    /// </summary>
    [TestClass]
    public class TrashBinManagerTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TrashBinManagerTest"/> class.
        /// </summary>
        public TrashBinManagerTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }

        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// A test for DeleteToTrashBin(object entity) method using a project folder as input data.
        /// </summary>
        [TestMethod]
        public void DeleteToTrashBinProjectFolderTest()
        {
            ProjectFolder folder1 = new ProjectFolder();
            folder1.Name = folder1.Guid.ToString();

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectFolderRepository.Add(folder1);
            dataContext1.SaveChanges();

            TrashManager trashManager = new TrashManager(dataContext1);
            trashManager.DeleteToTrashBin(folder1);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            ProjectFolder deletedFolder1 = dataContext2.EntityContext.ProjectFolderSet.FirstOrDefault(f => f.Guid == folder1.Guid);
            TrashBinItem trashBinItemOfFolder1 = dataContext2.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.EntityGuid == folder1.Guid);

            // Verifies that the folder was moved in the TrashBin 
            Assert.IsNotNull(deletedFolder1, "A folder that wasn't in Added state was permanently deleted");
            Assert.IsTrue(deletedFolder1.IsDeleted, "Failed to delete to the TrashBin a project folder");
            Assert.IsNotNull(trashBinItemOfFolder1, "Failed to create a trash bin item for the deleted project folder");

            // Delete to TrashBin a folder which is in Deleted state
            ProjectFolder folder3 = new ProjectFolder();
            folder3.Name = folder3.Guid.ToString();

            dataContext1.ProjectFolderRepository.Add(folder3);
            dataContext1.SaveChanges();

            // Delete "folder3" permanently without saving the changes
            dataContext1.ProjectFolderRepository.RemoveAll(folder3);
            trashManager.DeleteToTrashBin(folder3);

            // Verifies that the folder's properties were not modified by DeleteToTrashBin method
            ProjectFolder deletedFolder3 = dataContext1.EntityContext.ProjectFolderSet.FirstOrDefault(f => f.Guid == folder3.Guid);
            Assert.IsNotNull(deletedFolder3, "DeleteToTrashBin method shouldn't modify a folder that is in Deleted state");
            Assert.IsFalse(deletedFolder3.IsDeleted, "DeleteToTrashBin method shouldn't modify a folder that is in Deleted state");

            // Delete to TrashBin a folder that doesn't exist in db => nothing should happen
            ProjectFolder folder4 = new ProjectFolder();
            folder4.Name = folder4.Guid.ToString();
            trashManager.DeleteToTrashBin(folder4);

            ProjectFolder deletedFolder4 = dataContext2.EntityContext.ProjectFolderSet.FirstOrDefault(f => f.Guid == folder4.Guid);
            TrashBinItem trashBinItemOfFolder4 = dataContext2.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.EntityGuid == folder4.Guid);

            Assert.IsNull(deletedFolder4, "DeleteToTrashBin method shouldn't modify a folder that doesn't exist in db");
            Assert.IsNull(trashBinItemOfFolder4, "A trash bin item was created for a folder that doesn't exist in db");
        }

        /// <summary>
        /// A test for DeleteToTrashBin(object entity) method using a valid project as input data.
        /// </summary>
        [TestMethod]
        public void DeleteToTrashBinProjectTest()
        {
            Project project1 = new Project();
            project1.Name = project1.Guid.ToString();
            project1.OverheadSettings = new OverheadSetting();

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectRepository.Add(project1);
            dataContext1.SaveChanges();

            TrashManager trashManager = new TrashManager(dataContext1);
            trashManager.DeleteToTrashBin(project1);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Project deletedProject1 = dataContext2.EntityContext.ProjectSet.FirstOrDefault(p => p.Guid == project1.Guid);
            TrashBinItem trashBinItemOfProject1 = dataContext2.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.EntityGuid == project1.Guid);

            // Verifies that the project was moved in the TrashBin 
            Assert.IsNotNull(deletedProject1, "A project that wasn't in Added state was permanently deleted");
            Assert.IsTrue(deletedProject1.IsDeleted, "Failed to delete to the TrashBin a project");
            Assert.IsNotNull(trashBinItemOfProject1, "Failed to create a trash bin item for the deleted project");

            // Delete to TrashBin a project which is in Deleted state
            Project project3 = new Project();
            project3.Name = project3.Guid.ToString();
            project3.OverheadSettings = new OverheadSetting();

            dataContext1.ProjectRepository.Add(project3);
            dataContext1.SaveChanges();

            dataContext1.ProjectRepository.RemoveAll(project3);
            trashManager.DeleteToTrashBin(project3);

            // Verifies that the project's properties were not modified by DeleteToTrashBin method
            Project deletedProject3 = dataContext1.EntityContext.ProjectSet.FirstOrDefault(p => p.Guid == project3.Guid);
            Assert.IsNotNull(deletedProject3, "DeleteToTrashBin method shouldn't modify a project that is in Deleted state");
            Assert.IsFalse(deletedProject3.IsDeleted, "DeleteToTrashBin method shouldn't modify a project that is in Deleted state");

            // Delete to TrashBin a project that doesn't exist in db => nothing should happen
            Project project4 = new Project();
            project4.Name = project4.Guid.ToString();
            project4.OverheadSettings = new OverheadSetting();
            trashManager.DeleteToTrashBin(project4);

            Project deletedProject4 = dataContext2.EntityContext.ProjectSet.FirstOrDefault(p => p.Guid == project4.Guid);
            TrashBinItem trashBinItemOfProject4 = dataContext2.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.EntityGuid == project4.Guid);

            Assert.IsNull(deletedProject4, "DeleteToTrashBin method shouldn't modify a project that doesn't exist in db");
            Assert.IsNull(trashBinItemOfProject4, "A trash bin item was created for a project that doesn't exist in db");
        }

        /// <summary>
        /// A test for DeleteToTrashBin(object entity) method using a valid assembly as input data.
        /// </summary>
        [TestMethod]
        public void DeleteToTrashBinAssemblyTest()
        {
            Assembly assembly1 = new Assembly();
            assembly1.Name = assembly1.Guid.ToString();
            assembly1.CountrySettings = new CountrySetting();
            assembly1.OverheadSettings = new OverheadSetting();

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(assembly1);
            dataContext1.SaveChanges();

            TrashManager trashManager = new TrashManager(dataContext1);
            trashManager.DeleteToTrashBin(assembly1);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Assembly deletedAssembly1 = dataContext2.EntityContext.AssemblySet.FirstOrDefault(a => a.Guid == assembly1.Guid);
            TrashBinItem trashBinItemOfAssembly1 = dataContext2.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.EntityGuid == assembly1.Guid);

            // Verifies that the assembly was moved in the TrashBin 
            Assert.IsNotNull(deletedAssembly1, "An assembly that wasn't in Added state was permanently deleted");
            Assert.IsTrue(deletedAssembly1.IsDeleted, "Failed to delete to the TrashBin an assembly entity");
            Assert.IsNotNull(trashBinItemOfAssembly1, "Failed to create a trash bin item for the deleted assembly");

            // Delete to TrashBin an assembly which is in Deleted state
            Assembly assembly3 = new Assembly();
            assembly3.Name = assembly3.Guid.ToString();
            assembly3.OverheadSettings = new OverheadSetting();
            assembly3.CountrySettings = new CountrySetting();

            dataContext1.AssemblyRepository.Add(assembly3);
            dataContext1.SaveChanges();

            dataContext1.AssemblyRepository.RemoveAll(assembly3);
            trashManager.DeleteToTrashBin(assembly3);

            // Verifies that the assembly's properties were not modified by DeleteToTrashBin method
            Assembly deletedAssembly3 = dataContext1.EntityContext.AssemblySet.FirstOrDefault(p => p.Guid == assembly3.Guid);
            Assert.IsNotNull(deletedAssembly3, "DeleteToTrashBin method shouldn't modify an assembly that is in Deleted state");
            Assert.IsFalse(deletedAssembly3.IsDeleted, "DeleteToTrashBin method shouldn't modify an assembly that is in Deleted state");

            // Delete to TrashBin an assembly that doesn't exist in db => nothing should happen
            Assembly assembly4 = new Assembly();
            assembly4.Name = assembly4.Guid.ToString();
            assembly4.OverheadSettings = new OverheadSetting();
            assembly4.CountrySettings = new CountrySetting();
            trashManager.DeleteToTrashBin(assembly4);

            Assembly deletedAssembly4 = dataContext2.EntityContext.AssemblySet.FirstOrDefault(a => a.Guid == assembly4.Guid);
            TrashBinItem trashBinItemOfAssembly4 = dataContext2.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.EntityGuid == assembly4.Guid);

            Assert.IsNull(deletedAssembly4, "DeleteToTrashBin method shouldn't modify an assembly that doesn't exist in db");
            Assert.IsNull(trashBinItemOfAssembly4, "A trash bin item was created for an assembly that doesn't exist in db");
        }

        /// <summary>
        /// A test for DeleteToTrashBin(object entity) method using a valid part as input data.
        /// </summary>
        [TestMethod]
        public void DeleteToTrashBinPartTest()
        {
            Part part1 = new Part();
            part1.Name = part1.Guid.ToString();
            part1.OverheadSettings = new OverheadSetting();
            part1.CountrySettings = new CountrySetting();

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part1);
            dataContext1.SaveChanges();

            TrashManager trashManager = new TrashManager(dataContext1);
            trashManager.DeleteToTrashBin(part1);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Part deletedPart1 = dataContext2.EntityContext.PartSet.FirstOrDefault(p => p.Guid == part1.Guid);
            TrashBinItem trashBinItemOfPart1 = dataContext2.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.EntityGuid == part1.Guid);

            // Verifies that the part was moved in the TrashBin 
            Assert.IsNotNull(deletedPart1, "A part that wasn't in Added state was permanently deleted");
            Assert.IsTrue(deletedPart1.IsDeleted, "Failed to delete to the TrashBin a part entity");
            Assert.IsNotNull(trashBinItemOfPart1, "Failed to create a trash bin item for the deleted part");

            // Delete to TrashBin a part which is in Deleted state
            Part part3 = new Part();
            part3.Name = part3.Guid.ToString();
            part3.OverheadSettings = new OverheadSetting();
            part3.CountrySettings = new CountrySetting();

            dataContext1.PartRepository.Add(part3);
            dataContext1.SaveChanges();

            dataContext1.PartRepository.RemoveAll(part3);
            trashManager.DeleteToTrashBin(part3);

            // Verifies that the part's properties were not modified by DeleteToTrashBin method
            Part deletedPart3 = dataContext1.EntityContext.PartSet.FirstOrDefault(p => p.Guid == part3.Guid);
            Assert.IsNotNull(deletedPart3, "DeleteToTrashBin method shouldn't modify a part that is in Deleted state");
            Assert.IsFalse(deletedPart3.IsDeleted, "DeleteToTrashBin method shouldn't modify a part that is in Deleted state");

            // Delete to TrashBin a part that doesn't exist in db => nothing should happen
            Part part4 = new Part();
            part4.Name = part4.Guid.ToString();
            part4.OverheadSettings = new OverheadSetting();
            part4.CountrySettings = new CountrySetting();
            trashManager.DeleteToTrashBin(part4);

            Part deletedPart4 = dataContext2.EntityContext.PartSet.FirstOrDefault(p => p.Guid == part4.Guid);
            TrashBinItem trashBinItemOfPart4 = dataContext2.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.EntityGuid == part4.Guid);

            Assert.IsNull(deletedPart4, "DeleteToTrashBin method shouldn't modify a part that doesn't exist in db");
            Assert.IsNull(trashBinItemOfPart4, "A trash bin item was created for a part that doesn't exist in db");
        }

        /// <summary>
        /// A test for DeleteToTrashBin(object entity) method using a valid raw materials as input data.
        /// </summary>
        [TestMethod]
        public void DeleteToTrashBinRawMaterialTest()
        {
            RawMaterial material1 = new RawMaterial();
            material1.Name = material1.Guid.ToString();

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.RawMaterialRepository.Add(material1);
            dataContext1.SaveChanges();

            TrashManager trashManager = new TrashManager(dataContext1);
            trashManager.DeleteToTrashBin(material1);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            RawMaterial deletedMaterial1 = dataContext2.EntityContext.RawMaterialSet.FirstOrDefault(m => m.Guid == material1.Guid);
            TrashBinItem trashBinItemOfMaterial1 = dataContext2.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.EntityGuid == material1.Guid);

            // Verifies that the raw material was moved in the TrashBin 
            Assert.IsNotNull(deletedMaterial1, "A raw material that wasn't in Added state was permanently deleted");
            Assert.IsTrue(deletedMaterial1.IsDeleted, "Failed to delete to the TrashBin a raw material entity");
            Assert.IsNotNull(trashBinItemOfMaterial1, "Failed to create a trash bin item for the deleted raw material");

            // Delete a raw material which is in Deleted state
            RawMaterial material3 = new RawMaterial();
            material3.Name = material3.Guid.ToString();

            dataContext1.RawMaterialRepository.Add(material3);
            dataContext1.SaveChanges();

            dataContext1.RawMaterialRepository.RemoveAll(material3);
            trashManager.DeleteToTrashBin(material3);

            // Verifies that the material's properties were not modified by DeleteToTrashBin method
            RawMaterial deletedMaterial3 = dataContext1.EntityContext.RawMaterialSet.FirstOrDefault(m => m.Guid == material3.Guid);
            Assert.IsNotNull(deletedMaterial3, "DeleteToTrashBin method shouldn't modify a raw material that is in Deleted state");
            Assert.IsFalse(deletedMaterial3.IsDeleted, "DeleteToTrashBin method shouldn't modify a raw material that is in Deleted state");

            // Delete to TrashBin a raw material that doesn't exist in db => nothing should happen
            RawMaterial material4 = new RawMaterial();
            material4.Name = material4.Guid.ToString();
            trashManager.DeleteToTrashBin(material4);

            RawMaterial deletedMaterial4 = dataContext2.EntityContext.RawMaterialSet.FirstOrDefault(m => m.Guid == material4.Guid);
            TrashBinItem trashBinItemOfMaterial4 = dataContext2.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.EntityGuid == material4.Guid);

            Assert.IsNull(deletedMaterial4, "DeleteToTrashBin method shouldn't modify a raw material that doesn't exist in db");
            Assert.IsNull(trashBinItemOfMaterial4, "A trash bin item was created for a raw material that doesn't exist in db");
        }

        /// <summary>
        /// A test for DeleteToTrashBin(object entity) method using a valid commodity as input data.
        /// </summary>
        [TestMethod]
        public void DeleteToTrashBinCommodityTest()
        {
            Commodity commodity1 = new Commodity();
            commodity1.Name = commodity1.Guid.ToString();

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.CommodityRepository.Add(commodity1);
            dataContext1.SaveChanges();

            TrashManager trashManager = new TrashManager(dataContext1);
            trashManager.DeleteToTrashBin(commodity1);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Commodity deletedCommodity1 = dataContext2.EntityContext.CommoditySet.FirstOrDefault(c => c.Guid == commodity1.Guid);
            TrashBinItem trashBinItemOfCommodity1 = dataContext2.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.EntityGuid == commodity1.Guid);

            // Verifies that the commodity was moved in the TrashBin 
            Assert.IsNotNull(deletedCommodity1, "A commodity that wasn't in Added state was permanently deleted");
            Assert.IsTrue(deletedCommodity1.IsDeleted, "Failed to delete to the TrashBin a commodity entity");
            Assert.IsNotNull(trashBinItemOfCommodity1, "Failed to create a trash bin item for the deleted commodity");

            // Delete a commodity which is in Deleted state
            Commodity commodity3 = new Commodity();
            commodity3.Name = commodity3.Guid.ToString();

            dataContext1.CommodityRepository.Add(commodity3);
            dataContext1.SaveChanges();

            dataContext1.CommodityRepository.RemoveAll(commodity3);
            trashManager.DeleteToTrashBin(commodity3);

            // Verifies that the commodity's properties were not modified by DeleteToTrashBin method
            Commodity deletedCommodity3 = dataContext1.EntityContext.CommoditySet.FirstOrDefault(c => c.Guid == commodity3.Guid);
            Assert.IsNotNull(deletedCommodity3, "DeleteToTrashBin method shouldn't modify a commodity that is in Deleted state");
            Assert.IsFalse(deletedCommodity3.IsDeleted, "DeleteToTrashBin method shouldn't modify a commodity that is in Deleted state");

            // Delete to TrashBin a commodity that doesn't exist in db => nothing should happen
            Commodity commodity4 = new Commodity();
            commodity4.Name = commodity4.Guid.ToString();
            trashManager.DeleteToTrashBin(commodity4);

            Commodity deletedCommodity4 = dataContext2.EntityContext.CommoditySet.FirstOrDefault(c => c.Guid == commodity4.Guid);
            TrashBinItem trashBinItemOfCommodity4 = dataContext2.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.EntityGuid == commodity4.Guid);

            Assert.IsNull(deletedCommodity4, "DeleteToTrashBin method shouldn't modify a commodity that doesn't exist in db");
            Assert.IsNull(trashBinItemOfCommodity4, "A trash bin item was created for a commodity that doesn't exist in db");
        }

        /// <summary>
        /// A test for DeleteToTrashBin(object entity) method using a valid consumable as input data.
        /// </summary>
        [TestMethod]
        public void DeleteToTrashBinConsumableTest()
        {
            Consumable consumable1 = new Consumable();
            consumable1.Name = consumable1.Guid.ToString();

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ConsumableRepository.Add(consumable1);
            dataContext1.SaveChanges();

            TrashManager trashManager = new TrashManager(dataContext1);
            trashManager.DeleteToTrashBin(consumable1);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Consumable deletedConsumable1 = dataContext2.EntityContext.ConsumableSet.FirstOrDefault(c => c.Guid == consumable1.Guid);
            TrashBinItem trashBinItemOfConsumable1 = dataContext2.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.EntityGuid == consumable1.Guid);

            // Verifies that the consumable was moved in the TrashBin 
            Assert.IsNotNull(deletedConsumable1, "A consumable that wasn't in Added state was permanently deleted");
            Assert.IsTrue(deletedConsumable1.IsDeleted, "Failed to delete to the TrashBin a consumable entity");
            Assert.IsNotNull(trashBinItemOfConsumable1, "Failed to create a trash bin item for the deleted consumable");

            // Delete to TrashBin a consumable which is in Deleted state
            Consumable consumable3 = new Consumable();
            consumable3.Name = consumable3.Guid.ToString();

            dataContext1.ConsumableRepository.Add(consumable3);
            dataContext1.SaveChanges();

            dataContext1.ConsumableRepository.RemoveAll(consumable3);
            trashManager.DeleteToTrashBin(consumable3);

            // Verifies that the consumable's properties were not modified by DeleteToTrashBin method
            Consumable deletedConsumable3 = dataContext1.EntityContext.ConsumableSet.FirstOrDefault(c => c.Guid == consumable3.Guid);
            Assert.IsNotNull(deletedConsumable3, "DeleteToTrashBin method shouldn't modify a consumable that is in Deleted state");
            Assert.IsFalse(deletedConsumable3.IsDeleted, "DeleteToTrashBin method shouldn't modify a consumable that is in Deleted state");

            // Delete to TrashBin a consumable that doesn't exist in db => nothing should happen
            Consumable consumable4 = new Consumable();
            consumable4.Name = consumable4.Guid.ToString();
            trashManager.DeleteToTrashBin(consumable4);

            Consumable deletedConsumable4 = dataContext2.EntityContext.ConsumableSet.FirstOrDefault(c => c.Guid == consumable4.Guid);
            TrashBinItem trashBinItemOfConsumable4 = dataContext2.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.EntityGuid == consumable4.Guid);

            Assert.IsNull(deletedConsumable4, "DeleteToTrashBin method shouldn't modify a consumable that doesn't exist in db");
            Assert.IsNull(trashBinItemOfConsumable4, "A trash bin item was created for a consumable that doesn't exist in db");
        }

        /// <summary>
        /// A test for DeleteToTrashBin(object entity) method using a valid machine as input data.
        /// </summary>
        [TestMethod]
        public void DeleteToTrashBinMachineTest()
        {
            Machine machine1 = new Machine();
            machine1.Name = machine1.Guid.ToString();

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.MachineRepository.Add(machine1);
            dataContext1.SaveChanges();

            TrashManager trashManager = new TrashManager(dataContext1);
            trashManager.DeleteToTrashBin(machine1);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Machine deletedMachine1 = dataContext2.EntityContext.MachineSet.FirstOrDefault(m => m.Guid == machine1.Guid);
            TrashBinItem trashBinItemOfMachine1 = dataContext2.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.EntityGuid == machine1.Guid);

            // Verifies that the machine was moved in the TrashBin 
            Assert.IsNotNull(deletedMachine1, "A machine that wasn't in Added state was permanently deleted");
            Assert.IsTrue(deletedMachine1.IsDeleted, "Failed to delete to the TrashBin a machine entity");
            Assert.IsNotNull(trashBinItemOfMachine1, "Failed to create a trash bin item for the deleted machine");

            // Delete to TrashBin a machine which is in Deleted state
            Machine machine3 = new Machine();
            machine3.Name = machine3.Guid.ToString();

            dataContext1.MachineRepository.Add(machine3);
            dataContext1.SaveChanges();

            dataContext1.MachineRepository.RemoveAll(machine3);
            trashManager.DeleteToTrashBin(machine3);

            // Verifies that the machine's properties were not modified by DeleteToTrashBin method
            Machine deletedMachine3 = dataContext1.EntityContext.MachineSet.FirstOrDefault(m => m.Guid == machine3.Guid);
            Assert.IsNotNull(deletedMachine3, "DeleteToTrashBin method shouldn't modify a machine that is in Deleted state");
            Assert.IsFalse(deletedMachine3.IsDeleted, "DeleteToTrashBin method shouldn't modify a machine that is in Deleted state");

            // Delete to TrashBin a machine that doesn't exist in db => nothing should happen
            Machine machine4 = new Machine();
            machine4.Name = machine4.Guid.ToString();
            trashManager.DeleteToTrashBin(machine4);

            Machine deletedMachine4 = dataContext2.EntityContext.MachineSet.FirstOrDefault(m => m.Guid == machine4.Guid);
            TrashBinItem trashBinItemOfMachine4 = dataContext2.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.EntityGuid == machine4.Guid);

            Assert.IsNull(deletedMachine4, "DeleteToTrashBin method shouldn't modify a machine that doesn't exist in db");
            Assert.IsNull(trashBinItemOfMachine4, "A trash bin item was created for a machine that doesn't exist in db");
        }

        /// <summary>
        /// A test for DeleteToTrashBin(object entity) method using a valid die as input data.
        /// </summary>
        [TestMethod]
        public void DeleteToTrashBinDieTest()
        {
            Die die1 = new Die();
            die1.Name = die1.Guid.ToString();

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.DieRepository.Add(die1);
            dataContext1.SaveChanges();

            TrashManager trashManager = new TrashManager(dataContext1);
            trashManager.DeleteToTrashBin(die1);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Die deletedDie1 = dataContext2.EntityContext.DieSet.FirstOrDefault(d => d.Guid == die1.Guid);
            TrashBinItem trashBinItemOfDie1 = dataContext2.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.EntityGuid == die1.Guid);

            // Verifies that the die was moved in the TrashBin 
            Assert.IsNotNull(deletedDie1, "A die that wasn't in Added state was permanently deleted");
            Assert.IsTrue(deletedDie1.IsDeleted, "Failed to delete to the TrashBin a die entity");
            Assert.IsNotNull(trashBinItemOfDie1, "Failed to create a trash bin item for the deleted die");

            // Delete to TrashBin a die which is in Deleted state
            Die die3 = new Die();
            die3.Name = die3.Guid.ToString();

            dataContext1.DieRepository.Add(die3);
            dataContext1.SaveChanges();

            dataContext1.DieRepository.RemoveAll(die3);
            trashManager.DeleteToTrashBin(die3);

            // Verifies that the die's properties were not modified by DeleteToTrashBin method
            Die deletedDie3 = dataContext1.EntityContext.DieSet.FirstOrDefault(m => m.Guid == die3.Guid);
            Assert.IsNotNull(deletedDie3, "DeleteToTrashBin method shouldn't modify a die that is in Deleted state");
            Assert.IsFalse(deletedDie3.IsDeleted, "DeleteToTrashBin method shouldn't modify a die that is in Deleted state");

            // Delete to TrashBin a die that doesn't exist in db => nothing should happen
            Die die4 = new Die();
            die4.Name = die4.Guid.ToString();
            trashManager.DeleteToTrashBin(die4);

            Die deletedDie4 = dataContext2.EntityContext.DieSet.FirstOrDefault(d => d.Guid == die4.Guid);
            TrashBinItem trashBinItemOfDie4 = dataContext2.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.EntityGuid == die4.Guid);

            Assert.IsNull(deletedDie4, "DeleteToTrashBin method shouldn't modify a die that doesn't exist in db");
            Assert.IsNull(trashBinItemOfDie4, "A trash bin item was created for a die that doesn't exist in db");
        }

        /// <summary>
        /// Tests DeleteToTrashBin(object entity) method using a null entity as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void DeleteToTrashBinNullEntityTest()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            TrashManager trashManager = new TrashManager(dataContext);
            trashManager.DeleteToTrashBin(null);
        }

        /// <summary>
        /// Tests EmptyTrashBin() method which deletes all trash bin items of current user.
        /// </summary>
        [TestMethod]
        public void EmptyTrashBinTest()
        {
            User user1 = new User();
            user1.Username = EncryptionManager.Instance.GenerateRandomString(14, true);
            user1.Password = EncryptionManager.Instance.HashSHA256("Password", user1.Salt, user1.Guid.ToString());
            user1.Name = "User" + DateTime.Now.Ticks;
            user1.Roles = Role.Admin;

            User user2 = new User();
            user2.Username = EncryptionManager.Instance.GenerateRandomString(16, true);
            user2.Password = EncryptionManager.Instance.HashSHA256("Password.", user2.Salt, user2.Guid.ToString());
            user2.Name = "User" + DateTime.Now.Ticks;
            user2.Roles = Role.Admin;

            ProjectFolder folder1 = new ProjectFolder();
            folder1.Name = "Folder" + DateTime.Now.Ticks;

            Project project = new Project();
            project.Name = "Project" + DateTime.Now.Ticks;
            project.OverheadSettings = new OverheadSetting();
            project.Customer = new Customer();
            project.Customer.Name = "Customer" + DateTime.Now.Ticks;
            folder1.Projects.Add(project);

            Part part1 = new Part();
            part1.Name = "Part" + DateTime.Now.Ticks;
            part1.OverheadSettings = new OverheadSetting();
            part1.CountrySettings = new CountrySetting();
            part1.Manufacturer = new Manufacturer();
            part1.Manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            project.Parts.Add(part1);

            RawMaterial rawMaterial = new RawMaterial();
            rawMaterial.Name = "Raw Material" + DateTime.Now.Ticks;
            rawMaterial.Manufacturer = new Manufacturer();
            rawMaterial.Manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            part1.RawMaterials.Add(rawMaterial);

            Commodity commodity1 = new Commodity();
            commodity1.Name = "Commodity" + DateTime.Now.Ticks;
            commodity1.Manufacturer = new Manufacturer();
            commodity1.Manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            part1.Commodities.Add(commodity1);

            ProcessStep partStep = new PartProcessStep();
            partStep.Name = "PartStep" + DateTime.Now.Ticks;
            part1.Process = new Process();
            part1.Process.Steps.Add(partStep);

            Machine machine1 = new Machine();
            machine1.Name = "Machine" + DateTime.Now.Ticks;
            machine1.Manufacturer = new Manufacturer();
            machine1.Manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            partStep.Machines.Add(machine1);

            Consumable consumable1 = new Consumable();
            consumable1.Name = "Consumable" + DateTime.Now.Ticks;
            consumable1.Manufacturer = new Manufacturer();
            consumable1.Manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            partStep.Consumables.Add(consumable1);

            Die die1 = new Die();
            die1.Name = "Die" + DateTime.Now.Ticks;
            die1.Manufacturer = new Manufacturer();
            die1.Manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            partStep.Dies.Add(die1);

            Assembly assembly = new Assembly();
            assembly.Name = "Assembly" + DateTime.Now.Ticks;
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            assembly.Manufacturer = new Manufacturer();
            assembly.Manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            project.Assemblies.Add(assembly);
            folder1.SetOwner(user1);

            ProjectFolder folder2 = new ProjectFolder();
            folder2.Name = "Folder" + DateTime.Now.Ticks;
            folder2.SetOwner(user2);

            // Save entities
            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.UserRepository.Add(user1);
            dataContext1.UserRepository.Add(user2);
            dataContext1.ProjectFolderRepository.Add(folder1);
            dataContext1.ProjectFolderRepository.Add(folder2);
            dataContext1.SaveChanges();

            // Delete to Trash Bin the created entities
            TrashManager trashManager = new TrashManager(dataContext1);
            trashManager.DeleteToTrashBin(folder2);
            trashManager.DeleteToTrashBin(assembly);
            trashManager.DeleteToTrashBin(die1);
            trashManager.DeleteToTrashBin(consumable1);
            trashManager.DeleteToTrashBin(commodity1);
            trashManager.DeleteToTrashBin(rawMaterial);
            trashManager.DeleteToTrashBin(part1);
            trashManager.DeleteToTrashBin(project);
            trashManager.DeleteToTrashBin(folder1);
            dataContext1.SaveChanges();

            // Set the current user to 'user1' by login into application with 'user1'
            SecurityManager.Instance.Login(user1.Username, "Password");
            User currentUser = SecurityManager.Instance.CurrentUser;

            trashManager.EmptyTrashBin();
            dataContext1.SaveChanges();

            // Logout from the application
            SecurityManager.Instance.Logout();

            // Retrieve the deleted entities from db
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            List<TrashBinItem> result1 = dataContext2.EntityContext.TrashBinItemSet.Where(t => t.Owner != null && t.Owner.Guid == currentUser.Guid).ToList();
            ProjectFolder folder1Deleted = dataContext2.EntityContext.ProjectFolderSet.Where(f => f.Guid == folder1.Guid).FirstOrDefault();
            Project projectDeleted = dataContext2.EntityContext.ProjectSet.Where(p => p.Guid == project.Guid).FirstOrDefault();
            Assembly assemblyDeleted = dataContext2.EntityContext.AssemblySet.Where(a => a.Guid == assembly.Guid).FirstOrDefault();
            Part partDeleted = dataContext2.EntityContext.PartSet.Where(p => p.Guid == part1.Guid).FirstOrDefault();
            Die dieDeleted = dataContext2.EntityContext.DieSet.Where(d => d.Guid == die1.Guid).FirstOrDefault();
            Commodity commodityDeleted = dataContext2.EntityContext.CommoditySet.Where(c => c.Guid == commodity1.Guid).FirstOrDefault();
            RawMaterial rawMaterialDeleted = dataContext2.EntityContext.RawMaterialSet.Where(r => r.Guid == rawMaterial.Guid).FirstOrDefault();
            Consumable consumableDeleted = dataContext2.EntityContext.ConsumableSet.Where(cons => cons.Guid == consumable1.Guid).FirstOrDefault();
            ProjectFolder result2 = dataContext2.EntityContext.ProjectFolderSet.Where(fo => fo.Guid == folder2.Guid).FirstOrDefault();

            Assert.IsTrue(result1.Count() == 0, "Failed to delete all trash bin items");
            Assert.IsNull(folder1Deleted, "Failed to delete permanently a project folder with Guid {0}", folder1.Guid);
            Assert.IsNull(projectDeleted, "Failed to delete permanently a project");
            Assert.IsNull(assemblyDeleted, "Failed to delete permanently an assembly");
            Assert.IsNull(partDeleted, "Failed to delete permanently a part");
            Assert.IsNull(dieDeleted, "Failed to delete permanently a die");
            Assert.IsNull(commodityDeleted, "Failed to delete permanently a commodity");
            Assert.IsNull(rawMaterialDeleted, "Failed to delete permanently a raw material");
            Assert.IsNull(consumableDeleted, "Failed to delete permanently a consumable");
            Assert.IsNotNull(result2, "An entity which doesn't belong to the current user was permanently deleted");
        }

        /// <summary>
        /// Tests GetAllTrashBinItems() method which gets all trash bin items of current user.
        /// </summary>
        [TestMethod]
        public void GetAllTrashBinItemsTest()
        {
            User user1 = new User();
            user1.Username = "User" + DateTime.Now.Ticks;
            user1.Password = EncryptionManager.Instance.HashSHA256("Password.", user1.Salt, user1.Guid.ToString());
            user1.Name = "User" + DateTime.Now.Ticks;
            user1.Roles = Role.Admin;

            User user2 = new User();
            user2.Username = "User" + DateTime.Now.Ticks;
            user2.Password = EncryptionManager.Instance.HashSHA256("Password.", user2.Salt, user2.Guid.ToString());
            user2.Name = "User" + DateTime.Now.Ticks;
            user2.Roles = Role.Admin;

            ProjectFolder folder = new ProjectFolder();
            folder.Name = "Folder" + DateTime.Now.Ticks;

            Project project = new Project();
            project.Name = "Project" + DateTime.Now.Ticks;
            project.Customer = new Customer();
            project.Customer.Name = "Customer" + DateTime.Now.Ticks;
            project.OverheadSettings = new OverheadSetting();
            folder.Projects.Add(project);

            Part part = new Part();
            part.Name = "Part" + DateTime.Now.Ticks;
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            part.Manufacturer = new Manufacturer();
            part.Manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            project.Parts.Add(part);

            Assembly assembly = new Assembly();
            assembly.Name = "Assembly" + DateTime.Now.Ticks;
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            assembly.Manufacturer = new Manufacturer();
            assembly.Manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            project.Assemblies.Add(assembly);
            folder.SetOwner(user1);

            Project projectOfUser2 = new Project();
            projectOfUser2.Name = "Project" + DateTime.Now.Ticks;
            projectOfUser2.Customer = new Customer();
            projectOfUser2.Customer.Name = "Customer" + DateTime.Now.Ticks;
            projectOfUser2.OverheadSettings = new OverheadSetting();
            projectOfUser2.SetOwner(user2);

            // Save the entities in db
            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.UserRepository.Add(user1);
            dataContext1.UserRepository.Add(user2);
            dataContext1.ProjectFolderRepository.Add(folder);
            dataContext1.ProjectRepository.Add(projectOfUser2);
            dataContext1.SaveChanges();

            // Set the current user to 'user1' by logging into application with 'user1'
            SecurityManager.Instance.Login(user1.Username, "Password.");
            User currentUser = SecurityManager.Instance.CurrentUser;

            // Delete to trash bin the created entities
            TrashManager trashManager = new TrashManager(dataContext1);
            trashManager.DeleteToTrashBin(part);
            trashManager.DeleteToTrashBin(assembly);
            trashManager.DeleteToTrashBin(project);
            trashManager.DeleteToTrashBin(folder);
            trashManager.DeleteToTrashBin(projectOfUser2);
            dataContext1.SaveChanges();

            Collection<TrashBinItem> result = trashManager.GetAllTrashBinItems();
            SecurityManager.Instance.Logout();

            // Get all trash bin items from db
            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            List<TrashBinItem> expectedResult = dataContext2.EntityContext.TrashBinItemSet.Where(t => t.Owner.Guid == currentUser.Guid).ToList();

            Assert.AreEqual<int>(expectedResult.Count(), result.Count(), "Failed to get all trash bin items");
            foreach (TrashBinItem item in expectedResult)
            {
                if (result.FirstOrDefault(i => i.Guid == item.Guid) == null)
                {
                    Assert.Fail("Not all trash bin items were returned");
                }
            }
        }

        /// <summary>
        /// Tests DeleteEntityFromTrashBin(TrashBinItem item) method using a valid item as input parameter.
        /// </summary>
        [TestMethod]
        public void DeleteProjectFromTrashBinTest()
        {
            Project project = new Project();
            project.Name = "Test Project" + DateTime.Now.Ticks;
            project.OverheadSettings = new OverheadSetting();

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            // Delete to Trash Bin the entity and get the trash bin item of deleted entity
            TrashManager trashManager = new TrashManager(dataContext1);
            trashManager.DeleteToTrashBin(project);
            dataContext1.SaveChanges();

            TrashBinItem trashBinItem = dataContext1.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.EntityGuid == project.Guid);
            trashManager.PermanentlyDeleteEntity(trashBinItem);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            TrashBinItem deletedTrashBinItem = dataContext2.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.EntityGuid == project.Guid);
            Project deletedProject = dataContext2.EntityContext.ProjectSet.FirstOrDefault(p => p.Guid == project.Guid);

            Assert.IsNull(deletedTrashBinItem, "Failed to delete a trash bin item of a project");
            Assert.IsNull(deletedProject, "Failed to delete the underlying object of a trash bin item");
        }

        /// <summary>
        /// A test for DeleteEntityFromTrashBin(TrashBinItem item) method using a 
        /// valid project folder as input data.
        /// </summary>
        [TestMethod]
        public void DeleteProjectFolderFromTrashBinTest()
        {
            ProjectFolder folder = new ProjectFolder();
            folder.Name = folder.Guid.ToString();

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectFolderRepository.Add(folder);
            dataContext1.SaveChanges();

            // Delete to Trash Bin the entity and get the trash bin item of deleted entity
            TrashManager trashManager = new TrashManager(dataContext1);
            trashManager.DeleteToTrashBin(folder);
            dataContext1.SaveChanges();

            TrashBinItem trashBinItem = dataContext1.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.EntityGuid == folder.Guid);
            trashManager.PermanentlyDeleteEntity(trashBinItem);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            TrashBinItem deletedTrashBinItem = dataContext2.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.Guid == trashBinItem.Guid);
            ProjectFolder deletedFolder = dataContext2.EntityContext.ProjectFolderSet.FirstOrDefault(f => f.Guid == folder.Guid);

            Assert.IsNull(deletedTrashBinItem, "Failed to delete the trash bin item of a folder.");
            Assert.IsNull(deletedFolder, "Failed to delete the underlying domain object of a trash bin item.");
        }

        /// <summary>
        /// A test for DeleteEntityFromTrashBin(TrashBinItem item) method using a valid assembly
        /// as input data.
        /// </summary>
        [TestMethod]
        public void DeleteAssemblyFromTrashBinTest()
        {
            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.AssemblyRepository.Add(assembly);
            dataContext1.SaveChanges();

            // Move to  the Trash Bin the entity and get the trash bin item of deleted entity
            TrashManager trashManager = new TrashManager(dataContext1);
            trashManager.DeleteToTrashBin(assembly);
            dataContext1.SaveChanges();

            TrashBinItem trashBinItem = dataContext1.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.EntityGuid == assembly.Guid);
            trashManager.PermanentlyDeleteEntity(trashBinItem);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            TrashBinItem deletedTrashBinItem = dataContext2.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.Guid == trashBinItem.Guid);
            Assembly deletedAssembly = dataContext2.EntityContext.AssemblySet.FirstOrDefault(a => a.Guid == assembly.Guid);

            Assert.IsNull(deletedTrashBinItem, "Failed to delete a trash bin item of an assembly");
            Assert.IsNull(deletedAssembly, "Failed to delete the underlying object of a trash bin item");
        }

        /// <summary>
        /// A test for the DeleteEntityFromTrashBin(TrashBinItem item) method using a valid part
        /// as input data.
        /// </summary>
        [TestMethod]
        public void DeletePartFromTrashBinTest()
        {
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.PartRepository.Add(part);
            dataContext1.SaveChanges();

            // Move to  the Trash Bin the entity and get the trash bin item of deleted entity
            TrashManager trashManager = new TrashManager(dataContext1);
            trashManager.DeleteToTrashBin(part);
            dataContext1.SaveChanges();

            TrashBinItem trashBinItem = dataContext1.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.EntityGuid == part.Guid);
            trashManager.PermanentlyDeleteEntity(trashBinItem);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            TrashBinItem deletedTrashBinItem = dataContext2.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.Guid == trashBinItem.Guid);
            Part deletedPart = dataContext2.EntityContext.PartSet.FirstOrDefault(p => p.Guid == part.Guid);

            Assert.IsNull(deletedTrashBinItem, "Failed to delete a trash bin item of a part.");
            Assert.IsNull(deletedPart, "Failed to delete the underlying object of a trash bin item.");
        }

        /// <summary>
        /// A test for the DeleteEntityFromTrashBin(TrashBinItem item) method using a valid raw material
        /// as input data.
        /// </summary>
        [TestMethod]
        public void DeleteRawMaterialFromTrashBinTest()
        {
            RawMaterial rawMaterial = new RawMaterial();
            rawMaterial.Name = rawMaterial.Guid.ToString();

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.RawMaterialRepository.Add(rawMaterial);
            dataContext1.SaveChanges();

            // Move to  the Trash Bin the entity and get the trash bin item of deleted entity
            TrashManager trashManager = new TrashManager(dataContext1);
            trashManager.DeleteToTrashBin(rawMaterial);
            dataContext1.SaveChanges();

            TrashBinItem trashBinItem = dataContext1.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.EntityGuid == rawMaterial.Guid);
            trashManager.PermanentlyDeleteEntity(trashBinItem);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            TrashBinItem deletedTrashBinItem = dataContext2.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.Guid == trashBinItem.Guid);
            RawMaterial deletedMaterial = dataContext2.EntityContext.RawMaterialSet.FirstOrDefault(m => m.Guid == rawMaterial.Guid);

            Assert.IsNull(deletedTrashBinItem, "Failed to delete a trash bin item of a raw material");
            Assert.IsNull(deletedMaterial, "Failed to delete the underlying object of a trash bin item");
        }

        /// <summary>
        /// A test for the DeleteEntityFromTrashBin(TrashBinItem item) method using a valid commodity
        /// as input data.
        /// </summary>
        [TestMethod]
        public void DeleteCommodityFromTrashBinTest()
        {
            Commodity commodity = new Commodity();
            commodity.Name = commodity.Guid.ToString();

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.CommodityRepository.Add(commodity);
            dataContext1.SaveChanges();

            // Move to  the Trash Bin the entity and get the trash bin item of deleted entity
            TrashManager trashManager = new TrashManager(dataContext1);
            trashManager.DeleteToTrashBin(commodity);
            dataContext1.SaveChanges();

            TrashBinItem trashBinItem = dataContext1.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.EntityGuid == commodity.Guid);
            trashManager.PermanentlyDeleteEntity(trashBinItem);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            TrashBinItem deletedTrashBinItem = dataContext2.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.Guid == trashBinItem.Guid);
            Commodity deletedCommodity = dataContext2.EntityContext.CommoditySet.FirstOrDefault(c => c.Guid == commodity.Guid);

            Assert.IsNull(deletedTrashBinItem, "Failed to delete a trash bin item of a commodity");
            Assert.IsNull(deletedCommodity, "Failed to delete the underlying object of a trash bin item");
        }

        /// <summary>
        /// A test for the DeleteEntityFromTrashBin(TrashBinItem item) method using a valid consumable
        /// as input data.
        /// </summary>
        [TestMethod]
        public void DeleteConsumableFromTrashBinTest()
        {
            Consumable consumable = new Consumable();
            consumable.Name = consumable.Guid.ToString();

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ConsumableRepository.Add(consumable);
            dataContext1.SaveChanges();

            // Move to  the Trash Bin the entity and get the trash bin item of deleted entity
            TrashManager trashManager = new TrashManager(dataContext1);
            trashManager.DeleteToTrashBin(consumable);
            dataContext1.SaveChanges();

            TrashBinItem trashBinItem = dataContext1.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.EntityGuid == consumable.Guid);
            trashManager.PermanentlyDeleteEntity(trashBinItem);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            TrashBinItem deletedTrashBinItem = dataContext2.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.Guid == trashBinItem.Guid);
            Consumable deletedConsumable = dataContext2.EntityContext.ConsumableSet.FirstOrDefault(c => c.Guid == consumable.Guid);

            Assert.IsNull(deletedTrashBinItem, "Failed to delete a trash bin item of a consumable");
            Assert.IsNull(deletedConsumable, "Failed to delete the underlying object of a trash bin item");
        }

        /// <summary>
        /// A test for the DeleteEntityFromTrashBin(TrashBinItem item) method using a valid die
        /// as input data.
        /// </summary>
        [TestMethod]
        public void DeleteDieFromTrashBinTest()
        {
            Die die = new Die();
            die.Name = die.Guid.ToString();

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.DieRepository.Add(die);
            dataContext1.SaveChanges();

            // Move to the Trash Bin the entity and get the trash bin item of deleted entity
            TrashManager trashManager = new TrashManager(dataContext1);
            trashManager.DeleteToTrashBin(die);
            dataContext1.SaveChanges();

            TrashBinItem trashBinItem = dataContext1.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.EntityGuid == die.Guid);
            trashManager.PermanentlyDeleteEntity(trashBinItem);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            TrashBinItem deletedTrashBinItem = dataContext2.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.Guid == trashBinItem.Guid);
            Die deletedDie = dataContext2.EntityContext.DieSet.FirstOrDefault(d => d.Guid == die.Guid);

            Assert.IsNull(deletedTrashBinItem, "Failed to delete a trash bin item of a die");
            Assert.IsNull(deletedDie, "Failed to delete the underlying object of a trash bin item");
        }

        /// <summary>
        /// A test for the DeleteEntityFromTrashBin(TrashBinItem item) method using a valid machine
        /// as input data.
        /// </summary>
        [TestMethod]
        public void DeleteMachineFromTrashBinTest()
        {
            Machine machine = new Machine();
            machine.Name = machine.Guid.ToString();

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.MachineRepository.Add(machine);
            dataContext1.SaveChanges();

            // Move to the Trash Bin the entity and get the trash bin item of deleted entity
            TrashManager trashManager = new TrashManager(dataContext1);
            trashManager.DeleteToTrashBin(machine);
            dataContext1.SaveChanges();

            TrashBinItem trashBinItem = dataContext1.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.EntityGuid == machine.Guid);
            trashManager.PermanentlyDeleteEntity(trashBinItem);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            TrashBinItem deletedTrashBinItem = dataContext2.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.Guid == trashBinItem.Guid);
            Machine deletedMachine = dataContext2.EntityContext.MachineSet.FirstOrDefault(m => m.Guid == machine.Guid);

            Assert.IsNull(deletedTrashBinItem, "Failed to delete a trash bin item of a machine");
            Assert.IsNull(deletedMachine, "Failed to delete the underlying object of a trash bin item");
        }

        /// <summary>
        /// Tests  DeleteEntityFromTrashBin(TrashBinItem item) method using a null item as input parameter.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void DeleteNullEntityFromTrashBinTest()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            TrashManager trashManager = new TrashManager(dataContext);
            trashManager.PermanentlyDeleteEntity(null);
        }

        /// <summary>
        /// Tests GetItemsCount() method.
        /// </summary>
        [TestMethod]
        public void GetItemsCountTest()
        {
            User u1 = new User();
            u1.Username = EncryptionManager.Instance.GenerateRandomString(10, true);
            u1.Password = EncryptionManager.Instance.HashSHA256("Password.", u1.Salt, u1.Guid.ToString());
            u1.Name = EncryptionManager.Instance.GenerateRandomString(20, true);
            u1.Roles = Role.Admin;

            Die die = new Die();
            die.Name = "Test Die" + DateTime.Now.Ticks;
            die.Owner = u1;

            Commodity commodity = new Commodity();
            commodity.Name = "Commodity" + DateTime.Now.Ticks;
            commodity.Owner = u1;

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.DieRepository.Add(die);
            dataContext1.CommodityRepository.Add(commodity);
            dataContext1.SaveChanges();

            // Delete to Trash Bin the created entities
            TrashManager trashManager = new TrashManager(dataContext1);
            trashManager.DeleteToTrashBin(die);
            trashManager.DeleteToTrashBin(commodity);
            dataContext1.SaveChanges();

            // Log in because the trash bin items are retrieved for the current user.
            SecurityManager.Instance.Login(u1.Username, "Password.");

            // Retrieve the items count of Trash Bin
            int result = trashManager.GetItemsCount();

            SecurityManager.Instance.Logout();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            int expectedResult = dataContext2.EntityContext.TrashBinItemSet.Count(it => it.Owner.Guid == u1.Guid);

            Assert.AreEqual<int>(expectedResult, result, "Failed to get the items count");
        }

        /// <summary>
        /// Tests TrashBinItem RestoreEntityFromTrashBin(TrashBinItem item) method using a null parameter.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RestoreNullEntityFromTrashBinTest()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            TrashManager trashManager = new TrashManager(dataContext);
            trashManager.RestoreEntity(null);
        }

        /// <summary>
        /// Tests RestoreEntityFromTrashBin(TrashBinItem item) method using an invalid item as input parameter - 
        /// an item whose underlying object was permanently deleted.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(BusinessException))]
        public void RestoreInvalidEntityFromTrashBinTest()
        {
            Die die = new Die();
            die.Name = "Test Die" + DateTime.Now.Ticks;

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.DieRepository.Add(die);
            dataContext1.SaveChanges();

            // Delete the die to Trash Bin 
            TrashManager trashManager = new TrashManager(dataContext1);
            trashManager.DeleteToTrashBin(die);
            dataContext1.SaveChanges();

            // Permanently delete the created die
            TrashBinItem dieItem = dataContext1.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.EntityGuid == die.Guid);
            dataContext1.DieRepository.DeleteByStoredProcedure(die);
            dataContext1.SaveChanges();

            // Restore from Trash Bin die item
            trashManager.RestoreEntity(dieItem);
        }

        /// <summary>
        /// Tests RestoreEntityFromTrashBin(TrashBinItem item) method using valid data as input parameters -
        /// delete a folder and then recover the deleted folder and verifies that all its children were recovered.
        /// </summary>
        [TestMethod]
        public void RestoreValidEntityFromTrashBinTest1()
        {
            ProjectFolder folder1 = new ProjectFolder();
            folder1.Name = "Folder" + DateTime.Now.Ticks;

            ProjectFolder folder2 = new ProjectFolder();
            folder2.Name = "Folder" + DateTime.Now.Ticks;
            folder1.ChildrenProjectFolders.Add(folder2);

            Project project = new Project();
            project.Name = "Project" + DateTime.Now.Ticks;
            project.OverheadSettings = new OverheadSetting();
            project.Customer = new Customer();
            project.Customer.Name = "Customer" + DateTime.Now.Ticks;
            folder1.Projects.Add(project);

            Part part1 = new Part();
            part1.Name = "Part" + DateTime.Now.Ticks;
            part1.OverheadSettings = new OverheadSetting();
            part1.CountrySettings = new CountrySetting();
            part1.Manufacturer = new Manufacturer();
            part1.Manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            project.Parts.Add(part1);

            RawMaterial rawMaterial = new RawMaterial();
            rawMaterial.Name = "Raw Material" + DateTime.Now.Ticks;
            rawMaterial.Manufacturer = new Manufacturer();
            rawMaterial.Manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            part1.RawMaterials.Add(rawMaterial);

            Commodity commodity1 = new Commodity();
            commodity1.Name = "Commodity" + DateTime.Now.Ticks;
            commodity1.Manufacturer = new Manufacturer();
            commodity1.Manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            part1.Commodities.Add(commodity1);

            ProcessStep partStep = new PartProcessStep();
            partStep.Name = "PartStep" + DateTime.Now.Ticks;
            part1.Process = new Process();
            part1.Process.Steps.Add(partStep);

            Machine machine1 = new Machine();
            machine1.Name = "Machine" + DateTime.Now.Ticks;
            machine1.Manufacturer = new Manufacturer();
            machine1.Manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            partStep.Machines.Add(machine1);

            Consumable consumable1 = new Consumable();
            consumable1.Name = "Consumable" + DateTime.Now.Ticks;
            consumable1.Manufacturer = new Manufacturer();
            consumable1.Manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            partStep.Consumables.Add(consumable1);

            Die die1 = new Die();
            die1.Name = "Die" + DateTime.Now.Ticks;
            die1.Manufacturer = new Manufacturer();
            die1.Manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            partStep.Dies.Add(die1);

            Assembly assembly = new Assembly();
            assembly.Name = "Assembly" + DateTime.Now.Ticks;
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            assembly.Manufacturer = new Manufacturer();
            assembly.Manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            project.Assemblies.Add(assembly);

            ProcessStep assemblyStep = new AssemblyProcessStep();
            assemblyStep.Name = "Step" + DateTime.Now.Ticks;
            assembly.Process = new Process();
            assembly.Process.Steps.Add(assemblyStep);

            Machine machine2 = new Machine();
            machine2.Name = "Machine" + DateTime.Now.Ticks;
            machine2.Manufacturer = new Manufacturer();
            machine2.Manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            assemblyStep.Machines.Add(machine2);

            Commodity commodity2 = new Commodity();
            commodity2.Name = "Commodity" + DateTime.Now.Ticks;
            commodity2.Manufacturer = new Manufacturer();
            commodity2.Manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            assemblyStep.Commodities.Add(commodity2);

            Consumable consumable2 = new Consumable();
            consumable2.Name = "Consumable" + DateTime.Now.Ticks;
            consumable2.Manufacturer = new Manufacturer();
            consumable2.Manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            assemblyStep.Consumables.Add(consumable2);

            Die die2 = new Die();
            die2.Name = "Die" + DateTime.Now.Ticks;
            die2.Manufacturer = new Manufacturer();
            die2.Manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            assemblyStep.Dies.Add(die2);

            Assembly subassembly = new Assembly();
            subassembly.Name = "Subassembly" + DateTime.Now.Ticks;
            subassembly.OverheadSettings = new OverheadSetting();
            subassembly.CountrySettings = new CountrySetting();
            subassembly.Manufacturer = new Manufacturer();
            subassembly.Manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            subassembly.Process = new Process();
            assembly.Subassemblies.Add(subassembly);

            Part part2 = new Part();
            part2.Name = "Part" + DateTime.Now.Ticks;
            part2.OverheadSettings = new OverheadSetting();
            part2.CountrySettings = new CountrySetting();
            part2.Manufacturer = new Manufacturer();
            part2.Manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            part2.Process = new Process();
            assembly.Parts.Add(part2);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectFolderRepository.Add(folder1);
            dataContext1.SaveChanges();

            TrashManager trashManager = new TrashManager(dataContext1);
            trashManager.DeleteToTrashBin(folder1);
            dataContext1.SaveChanges();

            // Recover the deleted folder
            TrashBinItem folder1Item = dataContext1.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.EntityGuid == folder1.Guid);
            trashManager.RestoreEntity(folder1Item);
            dataContext1.SaveChanges();

            // Verifies that the folder and its children were recovered
            Assert.IsFalse(folder1.IsDeleted, "Failed to restore from Trash Bin a folder");
            Assert.IsFalse(folder2.IsDeleted, "Failed to restore from Trash Bin a child folder");
            Assert.IsFalse(project.IsDeleted, "Failed to restore from Trash Bin a project");
            Assert.IsFalse(part1.IsDeleted, "Failed to restore from Trash Bin a part");
            Assert.IsFalse(rawMaterial.IsDeleted, "Failed to restore from Trash Bin a raw material");
            Assert.IsFalse(commodity1.IsDeleted, "Failed to restore from Trash Bin part's commodity");
            Assert.IsFalse(machine1.IsDeleted, "Failed to restore from Trash Bin part's machine");
            Assert.IsFalse(consumable1.IsDeleted, "Failed to restore from Trash Bin part's consumable");
            Assert.IsFalse(die1.IsDeleted, "Failed to restore from Trash Bin part's die");
            Assert.IsFalse(assembly.IsDeleted, "Failed to restore from Trash Bin an assembly");
            Assert.IsFalse(machine2.IsDeleted, "Failed to restore from Trash Bin assembly's machine");
            Assert.IsFalse(consumable2.IsDeleted, "Failed to restore from Trash Bin assembly's consumable");
            Assert.IsFalse(die2.IsDeleted, "Failed to restore from Trash Bin assembly's die");
            Assert.IsFalse(commodity2.IsDeleted, "Failed to restore from Trash Bin assembly's commodity");
            Assert.IsFalse(subassembly.IsDeleted, "Failed to restore from Trash Bin a subassembly");
            Assert.IsFalse(part2.IsDeleted, "Failed to restore from Trash Bin a part belonging to an assembly");
        }

        /// <summary>
        /// Tests RestoreEntityFromTrashBin(TrashBinItem item) method using valid datas as input parameters -
        /// delete a folder and all its children and recover a folder's child .
        /// </summary>
        [TestMethod]
        public void RestoreValidEntityFromTrashBinTest2()
        {
            ProjectFolder folder1 = new ProjectFolder();
            folder1.Name = "Folder" + DateTime.Now.Ticks;

            ProjectFolder folder2 = new ProjectFolder();
            folder2.Name = "Folder" + DateTime.Now.Ticks;
            folder1.ChildrenProjectFolders.Add(folder2);

            Project project = new Project();
            project.Name = "Project" + DateTime.Now.Ticks;
            project.OverheadSettings = new OverheadSetting();
            project.Customer = new Customer();
            project.Customer.Name = "Customer" + DateTime.Now.Ticks;
            folder1.Projects.Add(project);

            Part part1 = new Part();
            part1.Name = "Part" + DateTime.Now.Ticks;
            part1.OverheadSettings = new OverheadSetting();
            part1.CountrySettings = new CountrySetting();
            part1.Manufacturer = new Manufacturer();
            part1.Manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            project.Parts.Add(part1);

            RawMaterial rawMaterial = new RawMaterial();
            rawMaterial.Name = "Raw Material" + DateTime.Now.Ticks;
            rawMaterial.Manufacturer = new Manufacturer();
            rawMaterial.Manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            part1.RawMaterials.Add(rawMaterial);

            Commodity commodity1 = new Commodity();
            commodity1.Name = "Commodity" + DateTime.Now.Ticks;
            commodity1.Manufacturer = new Manufacturer();
            commodity1.Manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            part1.Commodities.Add(commodity1);

            ProcessStep partStep = new PartProcessStep();
            partStep.Name = "PartStep" + DateTime.Now.Ticks;
            part1.Process = new Process();
            part1.Process.Steps.Add(partStep);

            Machine machine1 = new Machine();
            machine1.Name = "Machine" + DateTime.Now.Ticks;
            machine1.Manufacturer = new Manufacturer();
            machine1.Manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            partStep.Machines.Add(machine1);

            Consumable consumable1 = new Consumable();
            consumable1.Name = "Consumable" + DateTime.Now.Ticks;
            consumable1.Manufacturer = new Manufacturer();
            consumable1.Manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            partStep.Consumables.Add(consumable1);

            Die die1 = new Die();
            die1.Name = "Die" + DateTime.Now.Ticks;
            die1.Manufacturer = new Manufacturer();
            die1.Manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            partStep.Dies.Add(die1);

            Assembly assembly = new Assembly();
            assembly.Name = "Assembly" + DateTime.Now.Ticks;
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            assembly.Manufacturer = new Manufacturer();
            assembly.Manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            project.Assemblies.Add(assembly);

            ProcessStep assemblyStep = new AssemblyProcessStep();
            assemblyStep.Name = "Step" + DateTime.Now.Ticks;
            assembly.Process = new Process();
            assembly.Process.Steps.Add(assemblyStep);

            Machine machine2 = new Machine();
            machine2.Name = "Machine" + DateTime.Now.Ticks;
            machine2.Manufacturer = new Manufacturer();
            machine2.Manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            assemblyStep.Machines.Add(machine2);

            Commodity commodity2 = new Commodity();
            commodity2.Name = "Commodity" + DateTime.Now.Ticks;
            commodity2.Manufacturer = new Manufacturer();
            commodity2.Manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            assemblyStep.Commodities.Add(commodity2);

            Consumable consumable2 = new Consumable();
            consumable2.Name = "Consumable" + DateTime.Now.Ticks;
            consumable2.Manufacturer = new Manufacturer();
            consumable2.Manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            assemblyStep.Consumables.Add(consumable2);

            Die die2 = new Die();
            die2.Name = "Die" + DateTime.Now.Ticks;
            die2.Manufacturer = new Manufacturer();
            die2.Manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            assemblyStep.Dies.Add(die2);

            Assembly subassembly = new Assembly();
            subassembly.Name = "Subassembly" + DateTime.Now.Ticks;
            subassembly.OverheadSettings = new OverheadSetting();
            subassembly.CountrySettings = new CountrySetting();
            subassembly.Manufacturer = new Manufacturer();
            subassembly.Manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            subassembly.Process = new Process();
            assembly.Subassemblies.Add(subassembly);

            Part part2 = new Part();
            part2.Name = "Part" + DateTime.Now.Ticks;
            part2.OverheadSettings = new OverheadSetting();
            part2.CountrySettings = new CountrySetting();
            part2.Manufacturer = new Manufacturer();
            part2.Manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            part2.Process = new Process();
            assembly.Parts.Add(part2);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectFolderRepository.Add(folder1);
            dataContext1.SaveChanges();

            // Delete to Trash Bin the entities from down to top
            TrashManager trashManager = new TrashManager(dataContext1);
            trashManager.DeleteToTrashBin(folder2);
            trashManager.DeleteToTrashBin(rawMaterial);
            trashManager.DeleteToTrashBin(commodity1);
            trashManager.DeleteToTrashBin(machine1);
            trashManager.DeleteToTrashBin(consumable1);
            trashManager.DeleteToTrashBin(die1);
            trashManager.DeleteToTrashBin(part1);
            trashManager.DeleteToTrashBin(part2);
            trashManager.DeleteToTrashBin(commodity2);
            trashManager.DeleteToTrashBin(machine2);
            trashManager.DeleteToTrashBin(consumable2);
            trashManager.DeleteToTrashBin(die2);
            trashManager.DeleteToTrashBin(assembly);
            trashManager.DeleteToTrashBin(project);
            trashManager.DeleteToTrashBin(folder1);
            dataContext1.SaveChanges();

            // Recover the die1
            TrashBinItem die1Item = dataContext1.EntityContext.TrashBinItemSet.FirstOrDefault(t => t.EntityGuid == die1.Guid);
            trashManager.RestoreEntity(die1Item, true);
            dataContext1.SaveChanges();

            // Verifies that the whole tree of die was recovered
            Assert.IsFalse(folder1.IsDeleted, "Failed to restore from Trash Bin a folder");
            Assert.IsFalse(folder2.IsDeleted, "Failed to restore from Trash Bin a child folder");
            Assert.IsFalse(project.IsDeleted, "Failed to restore from Trash Bin a project");
            Assert.IsFalse(part1.IsDeleted, "Failed to restore from Trash Bin a part");
            Assert.IsFalse(rawMaterial.IsDeleted, "Failed to restore from Trash Bin a raw material");
            Assert.IsFalse(commodity1.IsDeleted, "Failed to restore from Trash Bin part's commodity");
            Assert.IsFalse(machine1.IsDeleted, "Failed to restore from Trash Bin part's machine");
            Assert.IsFalse(consumable1.IsDeleted, "Failed to restore from Trash Bin part's consumable");
            Assert.IsFalse(die1.IsDeleted, "Failed to restore from Trash Bin part's die");
            Assert.IsFalse(assembly.IsDeleted, "Failed to restore from Trash Bin an assembly");
            Assert.IsFalse(machine2.IsDeleted, "Failed to restore from Trash Bin assembly's machine");
            Assert.IsFalse(consumable2.IsDeleted, "Failed to restore from Trash Bin assembly's consumable");
            Assert.IsFalse(die2.IsDeleted, "Failed to restore from Trash Bin assembly's die");
            Assert.IsFalse(commodity2.IsDeleted, "Failed to restore from Trash Bin assembly's commodity");
            Assert.IsFalse(subassembly.IsDeleted, "Failed to restore from Trash Bin a subassembly");
            Assert.IsFalse(part2.IsDeleted, "Failed to restore from Trash Bin a part belonging to an assembly");
        }

        /// <summary>
        /// A test for DeleteToTrashBin(object entity) method using an object in Added state
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NotSupportedException))]
        public void DeleteToTrashBinAddedAssyTest()
        {
            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            TrashManager trashManager = new TrashManager(dataContext1);

            // Delete to TrashBin an assembly which is in Added state
            Assembly assembly2 = new Assembly();
            assembly2.Name = assembly2.Guid.ToString();
            assembly2.CountrySettings = new CountrySetting();
            assembly2.OverheadSettings = new OverheadSetting();
            dataContext1.AssemblyRepository.Add(assembly2);

            trashManager.DeleteToTrashBin(assembly2);
            dataContext1.SaveChanges();
        }

        /// <summary>
        /// A test for DeleteToTrashBin(object entity) method using an object in Added state
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NotSupportedException))]
        public void DeleteToTrashBinAddedPartTest()
        {
            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            TrashManager trashManager = new TrashManager(dataContext1);

            // Delete to TrashBin a part which is in Added state
            Part part2 = new Part();
            part2.OverheadSettings = new OverheadSetting();
            part2.CountrySettings = new CountrySetting();
            dataContext1.PartRepository.Add(part2);

            trashManager.DeleteToTrashBin(part2);
            dataContext1.SaveChanges();
        }

        /// <summary>
        /// A test for DeleteToTrashBin(object entity) method using an object in Added state
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NotSupportedException))]
        public void DeleteToTrashBinAddedCommodityTest()
        {
            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            TrashManager trashManager = new TrashManager(dataContext1);

            // Delete to TrashBin a commodity which is in Added state
            Commodity commodity2 = new Commodity();
            commodity2.Name = commodity2.Guid.ToString();
            dataContext1.CommodityRepository.Add(commodity2);

            trashManager.DeleteToTrashBin(commodity2);
            dataContext1.SaveChanges();
        }

        /// <summary>
        /// A test for DeleteToTrashBin(object entity) method using an object in Added state
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NotSupportedException))]
        public void DeleteToTrashBinAddedConsumableTest()
        {
            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            TrashManager trashManager = new TrashManager(dataContext1);

            // Delete to TrashBin a consumable which is in Added state
            Consumable consumable2 = new Consumable();
            consumable2.Name = consumable2.Guid.ToString();
            dataContext1.ConsumableRepository.Add(consumable2);

            trashManager.DeleteToTrashBin(consumable2);
            dataContext1.SaveChanges();
        }

        /// <summary>
        /// A test for DeleteToTrashBin(object entity) method using an object in Added state
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NotSupportedException))]
        public void DeleteToTrashBinAddedDieTest()
        {
            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            TrashManager trashManager = new TrashManager(dataContext1);

            // Delete to TrashBin a die which is in Added state
            Die die2 = new Die();
            die2.Name = die2.Guid.ToString();
            dataContext1.DieRepository.Add(die2);

            trashManager.DeleteToTrashBin(die2);
            dataContext1.SaveChanges();
        }

        /// <summary>
        /// A test for DeleteToTrashBin(object entity) method using an object in Added state
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NotSupportedException))]
        public void DeleteToTrashBinAddedMachineTest()
        {
            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            TrashManager trashManager = new TrashManager(dataContext1);

            // Delete to TrashBin a machine which is in Added state
            Machine machine2 = new Machine();
            machine2.Name = machine2.Guid.ToString();
            dataContext1.MachineRepository.Add(machine2);

            trashManager.DeleteToTrashBin(machine2);
            dataContext1.SaveChanges();
        }

        /// <summary>
        /// A test for DeleteToTrashBin(object entity) method using an object in Added state
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NotSupportedException))]
        public void DeleteToTrashBinAddedProjectFolderTest()
        {
            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            TrashManager trashManager = new TrashManager(dataContext1);

            // Delete to TrashBin a project folder that is in 'Added' state
            ProjectFolder folder2 = new ProjectFolder();
            folder2.Name = folder2.Guid.ToString();
            dataContext1.ProjectFolderRepository.Add(folder2);

            trashManager.DeleteToTrashBin(folder2);
            dataContext1.SaveChanges();
        }

        /// <summary>
        /// A test for DeleteToTrashBin(object entity) method using an object in Added state
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NotSupportedException))]
        public void DeleteToTrashBinAddedProjectTest()
        {
            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            TrashManager trashManager = new TrashManager(dataContext1);

            // Delete to TrashBin a project which is in Added state
            Project project2 = new Project();
            project2.Name = project2.Guid.ToString();
            project2.OverheadSettings = new OverheadSetting();
            dataContext1.ProjectRepository.Add(project2);

            trashManager.DeleteToTrashBin(project2);
            dataContext1.SaveChanges();
        }

        /// <summary>
        /// A test for DeleteToTrashBin(object entity) method using an object in Added state
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NotSupportedException))]
        public void DeleteToTrashBinAddedMaterialTest()
        {
            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            TrashManager trashManager = new TrashManager(dataContext1);

            // Delete to TrashBin a raw material which is in Added state
            RawMaterial material2 = new RawMaterial();
            material2.Name = material2.Guid.ToString();
            dataContext1.RawMaterialRepository.Add(material2);

            trashManager.DeleteToTrashBin(material2);
            dataContext1.SaveChanges();
        }

        #endregion Test Methods
    }
}