﻿using Ionic.Zip;
using ZPKTool.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using ZPKTool.Business.Export;
using ZPKTool.Data;

namespace ZPKTool.Business.Tests
{
    /// <summary>
    /// Helper class containing useful methods used in Business unit tests.
    /// </summary>
    /// <remarks></remarks>
    public static class Utils
    {
        /// <summary>
        /// Configures the database access. Necessary in order to use Persistence.        
        /// </summary>
        public static void ConfigureDbAccess()
        {
            DbConnectionInfo localConnParams = new DbConnectionInfo();
            localConnParams.InitialCatalog = "ZPKTool";
            localConnParams.DataSource = @"localhost\SQLEXPRESS";
            localConnParams.UserId = "PROimpensUser";
            localConnParams.Password = "iRl9VE3o1n1GPJexKN6RjA==";
            localConnParams.PasswordEncryptionKey = ZPKTool.Common.Constants.PasswordEncryptionKey;

            DbConnectionInfo centralConnParams = new DbConnectionInfo();
            centralConnParams.InitialCatalog = "ZPKToolCentral";
            centralConnParams.DataSource = @"localhost\SQLEXPRESS";
            centralConnParams.UserId = "PROimpensUser";
            centralConnParams.Password = "iRl9VE3o1n1GPJexKN6RjA==";
            centralConnParams.PasswordEncryptionKey = ZPKTool.Common.Constants.PasswordEncryptionKey;

            ZPKTool.DataAccess.ConnectionConfiguration.Initialize(localConnParams, centralConnParams);
        }

        /// <summary>
        /// Delete a file from a specified path.
        /// </summary>
        /// <param name="path">The file's path.</param>
        public static void DeleteFile(string path)
        {
            try
            {
                File.Delete(path);
            }
            catch (Exception)
            {
                // Do not throw any exception if the delete operation failed
            }
        }

        /// <summary>
        /// Imports an entity from a specified file.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <returns>
        /// The entity imported from the file.
        /// </returns>
        public static object ImportEntity(string filePath)
        {
            using (MemoryStream extractedFile = new MemoryStream())
            {
                using (ZipFile zip = ZipFile.Read(filePath))
                {
                    ZipEntry entry = zip.FirstOrDefault();
                    entry.Extract(extractedFile);
                }

                extractedFile.Seek(0, SeekOrigin.Begin);
                var serializer = new ZPKTool.Business.Export.Serializer();
                ExportedObject obj = (ExportedObject)serializer.Deserialize(extractedFile);

                ExportMapper mapper = new ExportMapper();
                return mapper.MapExportedObject(obj);
            }
        }
    }
}
