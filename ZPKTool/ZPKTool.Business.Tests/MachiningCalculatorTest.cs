﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Business.MachiningCalculator;

namespace ZPKTool.Business.Tests
{
    /// <summary>
    /// Summary description for MachiningCalculatorTest
    /// </summary>
    [TestClass]
    public class MachiningCalculatorTest
    {
        public MachiningCalculatorTest()
        {
        }

        private TestContext testContextInstance;

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }

            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes

        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MachiningCalculatorTestInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MachiningCalculatorTestCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void TestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void TestCleanup() { }

        #endregion

        #region Drilling Calculations Tests

        [TestMethod]
        public void CalculateDrillingOperationInExpertMode()
        {
            DrillingCalculationInputData inputData = new DrillingCalculationInputData()
            {
                Material = new DrillingMaterialToBeMachined() { CuttingSpeed = 35, CuttingFeed = 0.2m },
                DrillingType = DrillingType.ThroughHole,
                Boring = false,
                MachineFeedSpeed = 20,
                StartStopTimeForMachineFeed = 0.1m,
                DrillingDiameter = 10,
                DrillingDepth = 10,
                HolesNumber = 10,
                AvgHolesDistance = 10,
                ToolChangeTime = 3,
                ExpertModeCalculation = true,
                TurningSpeed = 1500
            };

            DrillingCalculationResult expected = new DrillingCalculationResult()
            {
                DrillingTime = 3.5451m,
                GrossProcessTime = 40.7505m
            };

            var calculator = new MachiningCalculator.MachiningCalculator();
            var result = calculator.CalculateDrillingOperation(inputData);

            Assert.AreEqual(expected.DrillingTime, Math.Round(result.DrillingTime.Value, 4));
            Assert.AreEqual(expected.GrossProcessTime, Math.Round(result.GrossProcessTime.Value, 4));
        }

        [TestMethod]
        public void CalculateDrillingOperation_ThroughHole_NoBoring()
        {
            DrillingCalculationInputData inputData = new DrillingCalculationInputData()
            {
                Material = new DrillingMaterialToBeMachined() { CuttingSpeed = 35, CuttingFeed = 0.2m },
                DrillingType = DrillingType.ThroughHole,
                Boring = false,
                MachineFeedSpeed = 20,
                StartStopTimeForMachineFeed = 0.1m,
                DrillingDiameter = 10,
                DrillingDepth = 10,
                HolesNumber = 10,
                AvgHolesDistance = 10,
                ToolChangeTime = 3
            };

            DrillingCalculationResult expected = new DrillingCalculationResult()
            {
                TurningSpeed = 1114.6497m,
                DrillingTime = 4.6196m,
                GrossProcessTime = 51.4965m
            };

            var calculator = new MachiningCalculator.MachiningCalculator();
            var result = calculator.CalculateDrillingOperation(inputData);

            Assert.AreEqual(expected.TurningSpeed, Math.Round(result.TurningSpeed.Value, 4));
            Assert.AreEqual(expected.DrillingTime, Math.Round(result.DrillingTime.Value, 4));
            Assert.AreEqual(expected.GrossProcessTime, Math.Round(result.GrossProcessTime.Value, 4));
        }

        [TestMethod]
        public void CalculateDrillingOperation_ThroughHole_Boring()
        {
            DrillingCalculationInputData inputData = new DrillingCalculationInputData()
            {
                Material = new DrillingMaterialToBeMachined() { CuttingSpeed = 35, CuttingFeed = 0.2m },
                DrillingType = DrillingType.ThroughHole,
                Boring = true,
                MachineFeedSpeed = 20,
                StartStopTimeForMachineFeed = 0.1m,
                DrillingDiameter = 10,
                DrillingDepth = 10,
                HolesNumber = 10,
                AvgHolesDistance = 10,
                ToolChangeTime = 3
            };

            DrillingCalculationResult expected = new DrillingCalculationResult()
            {
                TurningSpeed = 1114.6497m,
                DrillingTime = 2.7718m,
                GrossProcessTime = 33.0179m
            };

            var calculator = new MachiningCalculator.MachiningCalculator();
            var result = calculator.CalculateDrillingOperation(inputData);

            Assert.AreEqual(expected.TurningSpeed, Math.Round(result.TurningSpeed.Value, 4));
            Assert.AreEqual(expected.DrillingTime, Math.Round(result.DrillingTime.Value, 4));
            Assert.AreEqual(expected.GrossProcessTime, Math.Round(result.GrossProcessTime.Value, 4));
        }

        [TestMethod]
        public void CalculateDrillingOperation_BlindHole_NoBoring()
        {
            DrillingCalculationInputData inputData = new DrillingCalculationInputData()
            {
                Material = new DrillingMaterialToBeMachined() { CuttingSpeed = 35, CuttingFeed = 0.2m },
                DrillingType = DrillingType.BlindHole,
                Boring = false,
                MachineFeedSpeed = 20,
                StartStopTimeForMachineFeed = 0.1m,
                DrillingDiameter = 10,
                DrillingDepth = 10,
                HolesNumber = 10,
                AvgHolesDistance = 10,
                ToolChangeTime = 3
            };

            DrillingCalculationResult expected = new DrillingCalculationResult()
            {
                TurningSpeed = 1114.6497m,
                DrillingTime = 4.0422m,
                GrossProcessTime = 45.7219m
            };

            var calculator = new MachiningCalculator.MachiningCalculator();
            var result = calculator.CalculateDrillingOperation(inputData);

            Assert.AreEqual(expected.TurningSpeed, Math.Round(result.TurningSpeed.Value, 4));
            Assert.AreEqual(expected.DrillingTime, Math.Round(result.DrillingTime.Value, 4));
            Assert.AreEqual(expected.GrossProcessTime, Math.Round(result.GrossProcessTime.Value, 4));
        }

        [TestMethod]
        public void CalculateDrillingOperation_BlindHole_Boring()
        {
            DrillingCalculationInputData inputData = new DrillingCalculationInputData()
            {
                Material = new DrillingMaterialToBeMachined() { CuttingSpeed = 35, CuttingFeed = 0.2m },
                DrillingType = DrillingType.BlindHole,
                Boring = true,
                MachineFeedSpeed = 20,
                StartStopTimeForMachineFeed = 0.1m,
                DrillingDiameter = 10,
                DrillingDepth = 10,
                HolesNumber = 10,
                AvgHolesDistance = 10,
                ToolChangeTime = 3
            };

            DrillingCalculationResult expected = new DrillingCalculationResult()
            {
                TurningSpeed = 1114.6497m,
                DrillingTime = 2.4253m,
                GrossProcessTime = 29.5531m
            };

            var calculator = new MachiningCalculator.MachiningCalculator();
            var result = calculator.CalculateDrillingOperation(inputData);

            Assert.AreEqual(expected.TurningSpeed, Math.Round(result.TurningSpeed.Value, 4));
            Assert.AreEqual(expected.DrillingTime, Math.Round(result.DrillingTime.Value, 4));
            Assert.AreEqual(expected.GrossProcessTime, Math.Round(result.GrossProcessTime.Value, 4));
        }

        [TestMethod]
        public void CalculateDrillingOperation_Counterbore()
        {
            DrillingCalculationInputData inputData = new DrillingCalculationInputData()
            {
                Material = new DrillingMaterialToBeMachined() { CuttingSpeed = 35, CuttingFeed = 0.2m },
                DrillingType = DrillingType.Counterbore,
                Boring = true,
                MachineFeedSpeed = 20,
                StartStopTimeForMachineFeed = 0.1m,
                DrillingDiameter = 10,
                DrillingDepth = 10,
                HolesNumber = 10,
                AvgHolesDistance = 10,
                ToolChangeTime = 3
            };

            DrillingCalculationResult expected = new DrillingCalculationResult()
            {
                TurningSpeed = 1114.6497m,
                DrillingTime = 3.8497m,
                GrossProcessTime = 43.7971m
            };

            var calculator = new MachiningCalculator.MachiningCalculator();
            var result = calculator.CalculateDrillingOperation(inputData);

            Assert.AreEqual(expected.TurningSpeed, Math.Round(result.TurningSpeed.Value, 4));
            Assert.AreEqual(expected.DrillingTime, Math.Round(result.DrillingTime.Value, 4));
            Assert.AreEqual(expected.GrossProcessTime, Math.Round(result.GrossProcessTime.Value, 4));
        }

        [TestMethod]
        public void CalculateDrillingOperation_Tap()
        {
            DrillingCalculationInputData inputData = new DrillingCalculationInputData()
            {
                Material = new DrillingMaterialToBeMachined() { CuttingSpeed = 35, CuttingFeed = 0.2m },
                DrillingType = DrillingType.Tap,
                MachineFeedSpeed = 20,
                StartStopTimeForMachineFeed = 0.1m,
                DrillingDiameter = 10,
                DrillingDepth = 10,
                HolesNumber = 10,
                AvgHolesDistance = 10,
                ToolChangeTime = 3
            };

            DrillingCalculationResult expected = new DrillingCalculationResult()
            {
                TurningSpeed = 1114.6497m,
                DrillingTime = 7.6994m,
                GrossProcessTime = 82.2941m
            };

            var calculator = new MachiningCalculator.MachiningCalculator();
            var result = calculator.CalculateDrillingOperation(inputData);

            Assert.AreEqual(expected.TurningSpeed, Math.Round(result.TurningSpeed.Value, 4));
            Assert.AreEqual(expected.DrillingTime, Math.Round(result.DrillingTime.Value, 4));
            Assert.AreEqual(expected.GrossProcessTime, Math.Round(result.GrossProcessTime.Value, 4));
        }

        [TestMethod]
        public void CalculateDrillingOperation_Reaming()
        {
            DrillingCalculationInputData inputData = new DrillingCalculationInputData()
            {
                Material = new DrillingMaterialToBeMachined() { CuttingSpeed = 35, CuttingFeed = 0.2m },
                DrillingType = DrillingType.Reaming,
                Boring = false,
                MachineFeedSpeed = 20,
                StartStopTimeForMachineFeed = 0.1m,
                DrillingDiameter = 10,
                DrillingDepth = 10,
                HolesNumber = 10,
                AvgHolesDistance = 10,
                ToolChangeTime = 3
            };

            DrillingCalculationResult expected = new DrillingCalculationResult()
            {
                TurningSpeed = 1114.6497m,
                DrillingTime = 12.8324m,
                GrossProcessTime = 133.6235m
            };

            var calculator = new MachiningCalculator.MachiningCalculator();
            var result = calculator.CalculateDrillingOperation(inputData);

            Assert.AreEqual(expected.TurningSpeed, Math.Round(result.TurningSpeed.Value, 4));
            Assert.AreEqual(expected.DrillingTime, Math.Round(result.DrillingTime.Value, 4));
            Assert.AreEqual(expected.GrossProcessTime, Math.Round(result.GrossProcessTime.Value, 4));
        }

        #endregion Drilling Calculations Tests

        #region Turning Calculation Tests

        [TestMethod]
        public void CalculateTurningOperationInExpertMode()
        {
            var inputData = CreateTurningOperationTestData();
            inputData.ExpertModeCalculation = true;
            inputData.TurningSpeed = 600;
            inputData.FeedRate = 0.4m;

            var expected = new TurningCalculationResult()
            {
                TurningTime = 9.09m,
                GrossProcessTime = 12.293m,
                MachiningVolume = 4945.5m
            };

            var calculator = new MachiningCalculator.MachiningCalculator();
            var result = calculator.CalculateTurningOperation(inputData);

            Assert.AreEqual(expected.TurningTime, Math.Round(result.TurningTime.Value, 4));
            Assert.AreEqual(expected.GrossProcessTime, Math.Round(result.GrossProcessTime.Value, 4));
            Assert.AreEqual(expected.MachiningVolume, Math.Round(result.MachiningVolume.Value, 4));
        }

        [TestMethod]
        public void CalculateTurningOperation_Axial_ContinuousCut()
        {
            var inputData = CreateTurningOperationTestData();
            inputData.MachiningType = TurningMachiningType.RoughChipping;
            inputData.TurningType = TurningType.Axial;
            inputData.TurningSituation = TurningSituation.ContinuousCut;

            var expected = new TurningCalculationResult()
            {
                CalculatedFeedRate = 0.684m,
                GrossProcessTime = 10.829m,
                MachiningVolume = 4945.5m,
                TurningSpeed = 477.7070m,
                TurningTime = 7.626m
            };

            var calculator = new MachiningCalculator.MachiningCalculator();
            var actual = calculator.CalculateTurningOperation(inputData);
            VerifyResult(expected, actual);
        }

        [TestMethod]
        public void CalculateTurningOperation_Axial_ModerateIntersections()
        {
            var inputData = CreateTurningOperationTestData();
            inputData.MachiningType = TurningMachiningType.FineFeed;
            inputData.TurningType = TurningType.Axial;
            inputData.TurningSituation = TurningSituation.ModerateIntersections;

            var expected = new TurningCalculationResult()
            {
                CalculatedFeedRate = 0.285m,
                GrossProcessTime = 17.7921m,
                MachiningVolume = 4945.5m,
                TurningSpeed = 663.482m,
                TurningTime = 14.5891m
            };

            var calculator = new MachiningCalculator.MachiningCalculator();
            var actual = calculator.CalculateTurningOperation(inputData);
            VerifyResult(expected, actual);
        }

        [TestMethod]
        public void CalculateTurningOperation_Axial_UnfavourableIntersections()
        {
            var inputData = CreateTurningOperationTestData();
            inputData.MachiningType = TurningMachiningType.Chipping;
            inputData.TurningType = TurningType.Axial;
            inputData.TurningSituation = TurningSituation.UnfavourableIntersections;

            var expected = new TurningCalculationResult()
            {
                CalculatedFeedRate = 0.513m,
                GrossProcessTime = 13.6294m,
                MachiningVolume = 4945.5m,
                TurningSpeed = 583.8641m,
                TurningTime = 10.4264m
            };

            var calculator = new MachiningCalculator.MachiningCalculator();
            var actual = calculator.CalculateTurningOperation(inputData);
            VerifyResult(expected, actual);
        }

        [TestMethod]
        public void CalculateTurningOperation_Radial_ContinuousCut()
        {
            var inputData = CreateTurningOperationTestData();
            inputData.MachiningType = TurningMachiningType.FinestFeed;
            inputData.TurningType = TurningType.Radial;
            inputData.TurningSituation = TurningSituation.ContinuousCut;

            var expected = new TurningCalculationResult()
            {
                CalculatedFeedRate = 0.171m,
                GrossProcessTime = 17.2195m,
                MachiningVolume = 4945.5m,
                TurningSpeed = 796.1783m,
                TurningTime = 14.0165m
            };

            var calculator = new MachiningCalculator.MachiningCalculator();
            var actual = calculator.CalculateTurningOperation(inputData);
            VerifyResult(expected, actual);
        }

        [TestMethod]
        public void CalculateTurningOperation_Radial_ModerateIntersections()
        {
            var inputData = CreateTurningOperationTestData();
            inputData.MachiningType = TurningMachiningType.Chipping;
            inputData.TurningType = TurningType.Radial;
            inputData.TurningSituation = TurningSituation.ModerateIntersections;

            var expected = new TurningCalculationResult()
            {
                CalculatedFeedRate = 0.513m,
                GrossProcessTime = 11.8113m,
                MachiningVolume = 4945.5m,
                TurningSpeed = 583.8641m,
                TurningTime = 8.6083m
            };

            var calculator = new MachiningCalculator.MachiningCalculator();
            var actual = calculator.CalculateTurningOperation(inputData);
            VerifyResult(expected, actual);
        }

        [TestMethod]
        public void CalculateTurningOperation_Radial_UnfavourableIntersections()
        {
            var inputData = CreateTurningOperationTestData();
            inputData.MachiningType = TurningMachiningType.RoughChipping;
            inputData.TurningType = TurningType.Radial;
            inputData.TurningSituation = TurningSituation.UnfavourableIntersections;

            var expected = new TurningCalculationResult()
            {
                CalculatedFeedRate = 0.684m,
                GrossProcessTime = 13.0242m,
                MachiningVolume = 4945.5m,
                TurningSpeed = 477.707m,
                TurningTime = 9.8212m
            };

            var calculator = new MachiningCalculator.MachiningCalculator();
            var actual = calculator.CalculateTurningOperation(inputData);
            VerifyResult(expected, actual);
        }

        #endregion Turning Calculation Tests

        #region Milling Calculation Tests

        [TestMethod]
        public void CalculateMillingOperationInExpertMode()
        {
            var inputData = CreateMillingOperationTestData();
            inputData.ExpertModeCalculation = true;
            inputData.TurningSpeed = 130;
            inputData.FeedRatePerTooth = 0.2m;

            var expected = new MillingCalculationResult()
            {
                MillingTime = 2215.3846M,
                GrossProcessTime = 2230.4146M,
                MachiningVolume = 600000M
            };

            var calculator = new MachiningCalculator.MachiningCalculator();
            var result = calculator.CalculateMillingOperation(inputData);

            Assert.AreEqual(expected.MillingTime, Math.Round(result.MillingTime.Value, 4));
            Assert.AreEqual(expected.GrossProcessTime, Math.Round(result.GrossProcessTime.Value, 4));
            Assert.AreEqual(expected.MachiningVolume, Math.Round(result.MachiningVolume.Value, 4));
        }

        [TestMethod]
        public void CalculateMillingOperation_CylindricalCutter()
        {
            var inputData = CreateMillingOperationTestData();
            inputData.ToolType = MillingToolType.CylindricalCutter;

            var expected = new MillingCalculationResult()
            {
                TurningSpeed = 111.465M,
                FeedRatePerTooth = 0.0963M,
                MillingTime = 5366.0881M,
                GrossProcessTime = 5381.1181M,
                MachiningVolume = 600000M
            };

            var calculator = new MachiningCalculator.MachiningCalculator();
            var actual = calculator.CalculateMillingOperation(inputData);
            VerifyResult(expected, actual);
        }

        [TestMethod]
        public void CalculateMillingOperation_ShellEndMill()
        {
            var inputData = CreateMillingOperationTestData();
            inputData.ToolType = MillingToolType.ShellEndMill;

            var expected = new MillingCalculationResult()
            {
                TurningSpeed = 111.465M,
                FeedRatePerTooth = 0.0856M,
                MillingTime = 6036.8491M,
                GrossProcessTime = 6048.8791M,
                MachiningVolume = 600000M
            };

            var calculator = new MachiningCalculator.MachiningCalculator();
            var actual = calculator.CalculateMillingOperation(inputData);
            VerifyResult(expected, actual);
        }

        [TestMethod]
        public void CalculateMillingOperation_MillingHead()
        {
            var inputData = CreateMillingOperationTestData();
            inputData.ToolType = MillingToolType.MillingHead;

            var expected = new MillingCalculationResult()
            {
                TurningSpeed = 995.2229M,
                FeedRatePerTooth = 0.0856M,
                MillingTime = 676.1271M,
                GrossProcessTime = 688.1571M,
                MachiningVolume = 600000M
            };

            var calculator = new MachiningCalculator.MachiningCalculator();
            var actual = calculator.CalculateMillingOperation(inputData);
            VerifyResult(expected, actual);
        }

        [TestMethod]
        public void CalculateMillingOperation_EndMillCutter()
        {
            var inputData = CreateMillingOperationTestData();
            inputData.ToolType = MillingToolType.EndMillCutter;

            var expected = new MillingCalculationResult()
            {
                TurningSpeed = 318.4713M,
                FeedRatePerTooth = 0.1177M,
                MillingTime = 1536.6525M,
                GrossProcessTime = 1548.6825M,
                MachiningVolume = 600000M
            };

            var calculator = new MachiningCalculator.MachiningCalculator();
            var actual = calculator.CalculateMillingOperation(inputData);
            VerifyResult(expected, actual);
        }

        [TestMethod]
        public void CalculateMillingOperation_SideMillCutter()
        {
            var inputData = CreateMillingOperationTestData();
            inputData.ToolType = MillingToolType.SideMillCutter;

            var expected = new MillingCalculationResult()
            {
                TurningSpeed = 477.707M,
                FeedRatePerTooth = 0.0535M,
                MillingTime = 2253.757M,
                GrossProcessTime = 2268.787M,
                MachiningVolume = 600000M
            };

            var calculator = new MachiningCalculator.MachiningCalculator();
            var actual = calculator.CalculateMillingOperation(inputData);
            VerifyResult(expected, actual);
        }

        [TestMethod]
        public void CalculateMillingOperation_MillingSaw()
        {
            var inputData = CreateMillingOperationTestData();
            inputData.ToolType = MillingToolType.MillingSaw;

            var expected = new MillingCalculationResult()
            {
                TurningSpeed = 517.5159M,
                FeedRatePerTooth = 0.0642M,
                MillingTime = 1733.6592M,
                GrossProcessTime = 1745.6892M,
                MachiningVolume = 600000M
            };

            var calculator = new MachiningCalculator.MachiningCalculator();
            var actual = calculator.CalculateMillingOperation(inputData);
            VerifyResult(expected, actual);
        }

        #endregion Milling Calculation Tests

        #region Gear Hobbing Calculation Tests

        [TestMethod]
        public void CalculateGearHobbingOperationInExpertMode()
        {
            var inputData = CreateGearHobbingOperationInputData();
            inputData.ExpertModeCalculation = true;
            inputData.TurningSpeed = 150;
            inputData.FeedRatePerTooth = 0.15m;

            var expected = new GearHobbingCalculationResult()
            {
                MillingTime = 400M,
                GrossProcessTime = 406.001M,
                MachiningVolume = 150000M
            };

            var calculator = new MachiningCalculator.MachiningCalculator();
            var actual = calculator.CalculateGearHobbingOperation(inputData);

            Assert.AreEqual(expected.MillingTime, Math.Round(actual.MillingTime.Value, 4));
            Assert.AreEqual(expected.GrossProcessTime, Math.Round(actual.GrossProcessTime.Value, 4));
            Assert.AreEqual(expected.MachiningVolume, Math.Round(actual.MachiningVolume.Value, 4));
        }

        [TestMethod]
        public void CalculateGearHobbingOperation()
        {
            var inputData = CreateGearHobbingOperationInputData();

            var expected = new GearHobbingCalculationResult()
            {
                TurningSpeedOfMillingTool = 139.3312M,
                FeedRatePerTooth = 0.214M,
                MillingTime = 301.8425M,
                GrossProcessTime = 307.8435M,
                MachiningVolume = 150000M
            };

            var calculator = new MachiningCalculator.MachiningCalculator();
            var actual = calculator.CalculateGearHobbingOperation(inputData);

            Assert.AreEqual(expected.TurningSpeedOfMillingTool, Math.Round(actual.TurningSpeedOfMillingTool.Value, 4));
            Assert.AreEqual(expected.FeedRatePerTooth, Math.Round(actual.FeedRatePerTooth.Value, 4));
            Assert.AreEqual(expected.MillingTime, Math.Round(actual.MillingTime.Value, 4));
            Assert.AreEqual(expected.GrossProcessTime, Math.Round(actual.GrossProcessTime.Value, 4));
            Assert.AreEqual(expected.MachiningVolume, Math.Round(actual.MachiningVolume.Value, 4));
        }

        #endregion Gear Hobbing Calculation Tests

        #region Grinding Calculation Tests

        [TestMethod]
        public void CalculateGrindingOperationInExpertMode()
        {
            var inputData = CreateGrindingOperationInputData();
            inputData.ExpertModeCalculation = true;
            inputData.TurningSpeed = 110;
            inputData.FeedRate = 25.83m;

            var expected = new GrindingCalculationResult()
            {
                AverageGainDistance = 38M,
                HM = 0.1411M,
                GrindingTime = 8.508M,
                GrossProcessTime = 11.708M,
                MachiningVolume = 5000M
            };

            var calculator = new MachiningCalculator.MachiningCalculator();
            var actual = calculator.CalculateGrindingOperation(inputData);

            Assert.AreEqual(expected.AverageGainDistance, Math.Round(actual.AverageGainDistance.Value, 4));
            Assert.AreEqual(expected.HM, Math.Round(actual.HM.Value, 4));
            Assert.AreEqual(expected.GrindingTime, Math.Round(actual.GrindingTime.Value, 4));
            Assert.AreEqual(expected.GrossProcessTime, Math.Round(actual.GrossProcessTime.Value, 4));
            Assert.AreEqual(expected.MachiningVolume, Math.Round(actual.MachiningVolume.Value, 4));
        }

        [TestMethod]
        public void CalculateGrindingOperation_SurfaceGrindingColinear()
        {
            var inputData = CreateGrindingOperationInputData();
            inputData.OperationType = GrindingOperationType.SurfaceGrindingColinear;

            var expected = new GrindingCalculationResult()
            {
                TurningSpeedOfGrindingTool = 119.4268M,
                FeedRate = 21.1765M,
                AverageGainDistance = 38M,
                HM = 0.1065M,
                GrindingTime = 11.268M,
                GrossProcessTime = 14.468M,
                MachiningVolume = 5000M
            };

            var calculator = new MachiningCalculator.MachiningCalculator();
            var actual = calculator.CalculateGrindingOperation(inputData);
            VerifyResult(expected, actual);
        }

        [TestMethod]
        public void CalculateGrindingOperation_SurfaceGrindingPerpendicular()
        {
            var inputData = CreateGrindingOperationInputData();
            inputData.OperationType = GrindingOperationType.SurfaceGrindingPerpendicular;
            inputData.GrindingGrainSize = 80M;
            inputData.CuttingDepthPerCycle = 0.01M;

            var expected = new GrindingCalculationResult()
            {
                TurningSpeedOfGrindingTool = 119.4268M,
                FeedRate = 21.1765M,
                AverageGainDistance = 40M,
                HM = 0.0793M,
                GrindingTime = 30.288M,
                GrossProcessTime = 33.688M,
                MachiningVolume = 5000M
            };

            var calculator = new MachiningCalculator.MachiningCalculator();
            var actual = calculator.CalculateGrindingOperation(inputData);
            VerifyResult(expected, actual);
        }

        [TestMethod]
        public void CalculateGrindingOperation_CylindricalGrindingAxialOuter()
        {
            var inputData = CreateGrindingOperationInputData();
            inputData.OperationType = GrindingOperationType.CylindricalGrindingAxialOuter;
            inputData.GrindingGrainSize = 60M;
            inputData.CuttingDepthPerCycle = 0.006M;

            var expected = new GrindingCalculationResult()
            {
                TurningSpeedOfGrindingTool = 119.4268M,
                FeedRate = 21.1765M,
                AverageGainDistance = 36M,
                HM = 0.4975M,
                GrindingTime = 9.648M,
                GrossProcessTime = 13.448M,
                MachiningVolume = 5000M
            };

            var calculator = new MachiningCalculator.MachiningCalculator();
            var actual = calculator.CalculateGrindingOperation(inputData);
            VerifyResult(expected, actual);
        }

        [TestMethod]
        public void CalculateGrindingOperation_CylindricalGrindingRadialOuter()
        {
            var inputData = CreateGrindingOperationInputData();
            inputData.OperationType = GrindingOperationType.CylindricalGrindingRadialOuter;
            inputData.GrindingGrainSize = 120M;
            inputData.CuttingDepthPerCycle = 0.005M;

            var expected = new GrindingCalculationResult()
            {
                TurningSpeedOfGrindingTool = 119.4268M,
                FeedRate = 21.1765M,
                AverageGainDistance = 58M,
                HM = 0.7317M,
                GrindingTime = 6.576M,
                GrossProcessTime = 10.376M,
                MachiningVolume = 5000M
            };

            var calculator = new MachiningCalculator.MachiningCalculator();
            var actual = calculator.CalculateGrindingOperation(inputData);
            VerifyResult(expected, actual);
        }

        [TestMethod]
        public void CalculateGrindingOperation_CylindricalGrindingAxialInner()
        {
            var inputData = CreateGrindingOperationInputData();
            inputData.OperationType = GrindingOperationType.CylindricalGrindingAxialInner;
            inputData.GrindingGrainSize = 150M;
            inputData.CuttingDepthPerCycle = 0.03M;
            inputData.GrindingToolDiameter = 0.8M;

            var expected = new GrindingCalculationResult()
            {
                TurningSpeedOfGrindingTool = 11942.6752M,
                FeedRate = 21.1765M,
                AverageGainDistance = 40M,
                HM = 0.0061M,
                GrindingTime = 195.372M,
                GrossProcessTime = 198.572M,
                MachiningVolume = 5000M
            };

            var calculator = new MachiningCalculator.MachiningCalculator();
            var actual = calculator.CalculateGrindingOperation(inputData);
            VerifyResult(expected, actual);
        }

        [TestMethod]
        public void CalculateGrindingOperation_GrindingGrainSizeOutsideRange()
        {
            var inputData = CreateGrindingOperationInputData();
            inputData.OperationType = GrindingOperationType.CylindricalGrindingAxialInner;
            inputData.GrindingGrainSize = 59M;

            var calculator = new MachiningCalculator.MachiningCalculator();
            var actual = calculator.CalculateGrindingOperation(inputData);
            Assert.AreEqual(actual.GrindingTime, 0m);
            Assert.AreEqual(actual.GrossProcessTime, 3.2m);
        }

        [TestMethod]
        public void CalculateGrindingOperation_CuttingDepthPerCycleOutsideRange()
        {
            var inputData = CreateGrindingOperationInputData();
            inputData.OperationType = GrindingOperationType.CylindricalGrindingAxialInner;
            inputData.CuttingDepthPerCycle = 1M;

            var calculator = new MachiningCalculator.MachiningCalculator();
            var actual = calculator.CalculateGrindingOperation(inputData);
            Assert.AreEqual(actual.GrindingTime, 0m);
            Assert.AreEqual(actual.GrossProcessTime, 3.2m);
        }

        #endregion Grinding Calculation Tests

        #region Helpers

        private TurningCalculationInputData CreateTurningOperationTestData()
        {
            TurningCalculationInputData inputData = new TurningCalculationInputData()
            {
                MachiningType = TurningMachiningType.RoughChipping,
                TurningType = TurningType.Axial,
                TurningSituation = TurningSituation.ContinuousCut,
                MachineFeedSpeed = 20,
                CutDepth = 1,
                StartStopTimeForMachineFeed = 0.1m,
                MachineFeedLength = 1,
                TurningDiameter = 120,
                TurningLengh = 1,
                TurningDepth = 30,
                ToolChangeTime = 3
            };

            inputData.Material = new TurningMaterialToBeMachined();

            inputData.Material.MachiningParams.Add(
                TurningMachiningType.FinestFeed,
                new TurningMaterialToBeMachined.MachiningParameters()
                {
                    CuttingSpeed = 300,
                    CuttingFeed = 0.15m,
                    CuttingFeedVariable1 = -0.02m,
                    CuttingFeedVariable2 = 4
                });

            inputData.Material.MachiningParams.Add(
                TurningMachiningType.FineFeed,
                new TurningMaterialToBeMachined.MachiningParameters()
                {
                    CuttingSpeed = 250,
                    CuttingFeed = 0.25m,
                    CuttingFeedVariable1 = -0.02m,
                    CuttingFeedVariable2 = 4
                });

            inputData.Material.MachiningParams.Add(
                TurningMachiningType.Chipping,
                new TurningMaterialToBeMachined.MachiningParameters()
                {
                    CuttingSpeed = 220,
                    CuttingFeed = 0.45m,
                    CuttingFeedVariable1 = -0.02m,
                    CuttingFeedVariable2 = 4
                });

            inputData.Material.MachiningParams.Add(
                TurningMachiningType.RoughChipping,
                new TurningMaterialToBeMachined.MachiningParameters()
                {
                    CuttingSpeed = 180,
                    CuttingFeed = 0.6m,
                    CuttingFeedVariable1 = -0.02m,
                    CuttingFeedVariable2 = 4
                });

            return inputData;
        }

        private void VerifyResult(TurningCalculationResult expected, TurningCalculationResult actual)
        {
            Assert.AreEqual(expected.CalculatedFeedRate, Math.Round(actual.CalculatedFeedRate.Value, 4));
            Assert.AreEqual(expected.GrossProcessTime, Math.Round(actual.GrossProcessTime.Value, 4));
            Assert.AreEqual(expected.MachiningVolume, Math.Round(actual.MachiningVolume.Value, 4));
            Assert.AreEqual(expected.TurningSpeed, Math.Round(actual.TurningSpeed.Value, 4));
            Assert.AreEqual(expected.TurningTime, Math.Round(actual.TurningTime.Value, 4));
        }

        private MillingCalculationInputData CreateMillingOperationTestData()
        {
            MillingCalculationInputData inputData = new MillingCalculationInputData()
            {
                ToolType = MillingToolType.CylindricalCutter,
                MachineFeedSpeed = 20,
                MillingType = MillingType.RoughMachining,
                CutDepth = 2,
                TeethNumber = 5,
                MillingToolDiameter = 80,
                MillingToolWidth = 100,
                StartStopTimeForMachineFeed = 1,
                MachineFeedLength = 10,
                MillingLength = 400,
                MillingWidth = 300,
                MillingDepth = 5,
                ToolChangeTime = 3
            };

            inputData.Material = new MillingMaterialToBeMachined();

            inputData.Material.MachiningParams.Add(
                MillingToolType.CylindricalCutter,
                new MillingMaterialToBeMachined.MachiningParameters()
                {
                    ToolTypeFactor = 1,
                    CuttingSpeed = 28,
                    CuttingFeed = 0.09m
                });

            inputData.Material.MachiningParams.Add(
                MillingToolType.ShellEndMill,
                new MillingMaterialToBeMachined.MachiningParameters()
                {
                    ToolTypeFactor = 1,
                    CuttingSpeed = 28,
                    CuttingFeed = 0.08m
                });

            inputData.Material.MachiningParams.Add(
                MillingToolType.MillingHead,
                new MillingMaterialToBeMachined.MachiningParameters()
                {
                    ToolTypeFactor = 1,
                    CuttingSpeed = 250,
                    CuttingFeed = 0.08m
                });

            inputData.Material.MachiningParams.Add(
                MillingToolType.EndMillCutter,
                new MillingMaterialToBeMachined.MachiningParameters()
                {
                    ToolTypeFactor = 1,
                    CuttingSpeed = 80,
                    CuttingFeed = 0.11m
                });

            inputData.Material.MachiningParams.Add(
                MillingToolType.SideMillCutter,
                new MillingMaterialToBeMachined.MachiningParameters()
                {
                    ToolTypeFactor = 1,
                    CuttingSpeed = 120,
                    CuttingFeed = 0.05m
                });

            inputData.Material.MachiningParams.Add(
                MillingToolType.MillingSaw,
                new MillingMaterialToBeMachined.MachiningParameters()
                {
                    ToolTypeFactor = 1,
                    CuttingSpeed = 130,
                    CuttingFeed = 0.06m
                });

            return inputData;
        }

        private void VerifyResult(MillingCalculationResult expected, MillingCalculationResult actual)
        {
            Assert.AreEqual(expected.FeedRatePerTooth, Math.Round(actual.FeedRatePerTooth.Value, 4));
            Assert.AreEqual(expected.GrossProcessTime, Math.Round(actual.GrossProcessTime.Value, 4));
            Assert.AreEqual(expected.MachiningVolume, Math.Round(actual.MachiningVolume.Value, 4));
            Assert.AreEqual(expected.TurningSpeed, Math.Round(actual.TurningSpeed.Value, 4));
            Assert.AreEqual(expected.MillingTime, Math.Round(actual.MillingTime.Value, 4));
        }

        private GearHobbingCalculationInputData CreateGearHobbingOperationInputData()
        {
            var input = new GearHobbingCalculationInputData()
            {
                Material = new GearHobbingMaterialToBeMachined() { CuttingSpeed = 35M, CuttingFeed = 0.2M },
                MillingToolType = GearHobbingMillingToolType.Hob,
                MachineFeedSpeed = 10M,
                MillingType = MillingType.RoughMachining,
                CutDepth = 2M,
                TeethNumber = 10M,
                MillingToolDiameter = 80M,
                StartStopTimeForMachineFeed = 1M,
                MachineFeedLength = 10M,
                GearDiameter = 300M,
                GearWidth = 10M,
                GearModul = 5M,
                ToolChangeTime = 3M
            };

            return input;
        }

        private GrindingCalculationInputData CreateGrindingOperationInputData()
        {
            var inputData = new GrindingCalculationInputData()
            {
                MachineFeedSpeed = 5M,
                Material = new GrindingMaterial() { QBase = 85M, VCBase = 30M },
                OperationType = GrindingOperationType.SurfaceGrindingColinear,
                GrindingGrainSize = 100M,
                CuttingDepthPerCycle = 0.02M,
                GrindingToolDiameter = 80M,
                GrindingToolWidth = 60M,
                StartStopTimeForMachineFeed = 0.2M,
                MachineFeedLength = 10M,
                GrindingPartDiameter = 1M,
                GrindingLength = 100M,
                GrindingWidth = 50M,
                GrindingDepth = 0.02M,
                ToolChangeTime = 3M
            };

            return inputData;
        }

        private void VerifyResult(GrindingCalculationResult expected, GrindingCalculationResult actual)
        {
            Assert.AreEqual(expected.TurningSpeedOfGrindingTool, Math.Round(actual.TurningSpeedOfGrindingTool.Value, 4));
            Assert.AreEqual(expected.FeedRate, Math.Round(actual.FeedRate.Value, 4));
            Assert.AreEqual(expected.AverageGainDistance, Math.Round(actual.AverageGainDistance.Value, 4));
            Assert.AreEqual(expected.HM, Math.Round(actual.HM.Value, 4));
            Assert.AreEqual(expected.GrindingTime, Math.Round(actual.GrindingTime.Value, 4));
            Assert.AreEqual(expected.GrossProcessTime, Math.Round(actual.GrossProcessTime.Value, 4));
            Assert.AreEqual(expected.MachiningVolume, Math.Round(actual.MachiningVolume.Value, 4));
        }

        #endregion Helpers
    }
}
