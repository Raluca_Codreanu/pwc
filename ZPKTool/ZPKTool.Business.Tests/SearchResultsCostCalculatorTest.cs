﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Business.Search;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Common;

namespace ZPKTool.Business.Tests
{
    /// <summary>
    /// Tests the SearchResultsCostCalculator methods.
    /// </summary>
    [TestClass]
    public class SearchResultsCostCalculatorTest
    {
        /// <summary>
        /// The super assembly used in some tests. 
        /// </summary>
        private static Assembly testSuperAssembly;

        /// <summary>
        /// The test context.
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// The data source manager used by the tests to access the database.
        /// </summary>
        private static IDataSourceManager dataSourceManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="SearchResultsCostCalculatorTest"/> class.
        /// </summary>
        public SearchResultsCostCalculatorTest()
        {
        }

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes

        /// <summary>
        /// This method is running before running the first test in the class. 
        /// </summary>
        /// <param name="testContext">The test context.</param>
        [ClassInitialize()]
        public static void SearchResultsCalculatorTestInitialize(TestContext testContext)
        {
            BasicSetting settings = new BasicSetting()
            {
                DeprPeriod = 8,
                DeprRate = 0.06m,
                PartBatch = 12,
                ShiftsPerWeek = 15,
                HoursPerShift = 7.5m,
                ProdDays = 5,
                ProdWeeks = 48,
                AssetRate = 0.055m,
                LogisticCostRatio = 0.04m
            };

            var kg = new MeasurementUnit() { Name = "Kilogram", ConversionRate = 1, Type = MeasurementUnitType.Weight, ScaleID = MeasurementUnitScale.MetricWeightScale, ScaleFactor = 1 };
            var meter = new MeasurementUnit() { Name = "Meter", ConversionRate = 1, Type = MeasurementUnitType.Length, ScaleID = MeasurementUnitScale.MetricLengthScale, ScaleFactor = 1 };
            var squareMeter = new MeasurementUnit() { Name = "Square Meter", ConversionRate = 1, Type = MeasurementUnitType.Area, ScaleID = MeasurementUnitScale.MetricAreaScale, ScaleFactor = 1 };
            var cubicMeter = new MeasurementUnit() { Name = "Cubic Meter", ConversionRate = 1, Type = MeasurementUnitType.Volume, ScaleID = MeasurementUnitScale.MetricVolumeScale, ScaleFactor = 1 };

            var pound = new MeasurementUnit() { Name = "Pound", ConversionRate = 2.20462m, Type = MeasurementUnitType.Weight, ScaleID = MeasurementUnitScale.ImperialWeightScale, ScaleFactor = 1m };
            var foot = new MeasurementUnit() { Name = "Foot", ConversionRate = 3.28083m, Type = MeasurementUnitType.Length, ScaleID = MeasurementUnitScale.ImperialLengthScale, ScaleFactor = 1 };
            var squareFoot = new MeasurementUnit() { Name = "Square Foot", ConversionRate = 10.76390m, Type = MeasurementUnitType.Area, ScaleID = MeasurementUnitScale.ImperialAreaScale, ScaleFactor = 1 };
            var cubicFoot = new MeasurementUnit() { Name = "Cubic Foot", ConversionRate = 35.31466m, Type = MeasurementUnitType.Volume, ScaleID = MeasurementUnitScale.ImperialVolumeScale, ScaleFactor = 1 };

            List<MeasurementUnit> units = new List<MeasurementUnit>();
            units.Add(new MeasurementUnit() { Name = "Minute", ConversionRate = 60, Type = MeasurementUnitType.Time, ScaleID = MeasurementUnitScale.TimeScale, ScaleFactor = 60 });
            units.Add(new MeasurementUnit() { Name = "Hour", ConversionRate = 3600, Type = MeasurementUnitType.Time, ScaleID = MeasurementUnitScale.TimeScale, ScaleFactor = 3600 });
            units.Add(new MeasurementUnit() { Name = "Gram", ConversionRate = 0.001m, Type = MeasurementUnitType.Weight, ScaleID = MeasurementUnitScale.MetricWeightScale, ScaleFactor = 0.001m });
            units.Add(new MeasurementUnit() { Name = "Liter", ConversionRate = 1, Type = MeasurementUnitType.Volume2, ScaleID = MeasurementUnitScale.MetricLiquidsVolumeScale, ScaleFactor = 1 });
            units.Add(new MeasurementUnit() { Name = "Milliliter", ConversionRate = 0.001m, Type = MeasurementUnitType.Volume2, ScaleID = MeasurementUnitScale.MetricLiquidsVolumeScale, ScaleFactor = 0.001m });
            units.Add(new MeasurementUnit() { Name = "Pieces", ConversionRate = 1, Type = MeasurementUnitType.Pieces, ScaleID = MeasurementUnitScale.PiecesScale, ScaleFactor = 1 });
            units.Add(foot);
            units.Add(cubicFoot);
            units.Add(squareFoot);
            units.Add(pound);
            units.Add(new MeasurementUnit() { Name = "Second", ConversionRate = 1, Type = MeasurementUnitType.Time, ScaleID = MeasurementUnitScale.TimeScale, ScaleFactor = 1 });
            units.Add(kg);
            units.Add(meter);
            units.Add(squareMeter);
            units.Add(cubicMeter);

            LocalDataCache.Initialize(settings, units);

            // Load the super assembly
            Assembly importedAssembly = (Assembly)Utils.ImportEntity("BusinessTestAssy.assembly");

            // Clones the imported entity
            Utils.ConfigureDbAccess();
            dataSourceManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            CloneManager cloneManager = new CloneManager(dataSourceManager);

            Project parentProject = new Project();
            parentProject.Name = parentProject.Guid.ToString();
            parentProject.OverheadSettings = new OverheadSetting();
            parentProject.DepreciationPeriod = 8;
            parentProject.DepreciationRate = 0.06m;

            testSuperAssembly = cloneManager.Clone(importedAssembly);
            parentProject.Assemblies.Add(testSuperAssembly);

            // Save the project in db
            dataSourceManager.ProjectRepository.Add(parentProject);
            dataSourceManager.SaveChanges();
        }

        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion Additional test attributes

        #region Assembly Tests

        /// <summary>
        /// A test for CalculateCostOfSearchResultEntity(Assembly assembly, EntityContext entityContext) method using the following
        /// input data: a master data assembly.
        /// </summary>
        [TestMethod]
        public void CalculateAssemblyCostTest1()
        {
            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            assembly.CalculationAccuracy = PartCalculationAccuracy.Estimation;
            assembly.EstimatedCost = 1000;
            assembly.IsMasterData = true;

            IDataSourceManager assemblyContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            assemblyContext.AssemblyRepository.Add(assembly);
            assemblyContext.SaveChanges();

            decimal? result = SearchResultsCostCalculator.CalculateCostOfSearchResultEntity(assembly, DbIdentifier.LocalDatabase);
            Assert.IsNull(result, "A null cost should be returned for an assembly master data.");
        }

        /// <summary>
        /// A test for CalculateCostOfSearchResultEntity(Assembly assembly, EntityContext entityContext) method using the following
        /// input data: Assembly : "Havac Main Unit - 50-4 GBP"(a large assembly that drives the calculation through all possible code paths),
        /// Calculation Version: 1.0.
        /// </summary>
        [TestMethod]
        public void CalculateAssemblyCostTest2()
        {
            testSuperAssembly.SetCalculationVariant("1.0");
            dataSourceManager.SaveChanges();

            // Retrieve the super assembly from db without loading its references.
            IDataSourceManager assemblyContext = DataAccessFactory.CreateDataSourceManager(dataSourceManager.DatabaseId);
            Assembly assembly = dataSourceManager.AssemblyRepository.GetAssembly(testSuperAssembly.Guid);

            decimal result = SearchResultsCostCalculator.CalculateCostOfSearchResultEntity(assembly, assemblyContext.DatabaseId).GetValueOrDefault();
            decimal expectedCost = 224.4382m;
            Assert.AreEqual(expectedCost, Math.Round(result, 4), "Super assembly cost calculation result was wrong.");
        }

        #endregion Assembly Tests

        #region Part Tests

        /// <summary>
        /// A test for CalculateCostOfSearchResultEntity(Part part, EntityContext entityContext) method using the following data
        /// input data: a master data part.
        /// </summary>
        [TestMethod]
        public void CalculatePartCostTest1()
        {
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            part.CalculationAccuracy = PartCalculationAccuracy.Estimation;
            part.EstimatedCost = 1000;
            part.IsMasterData = true;

            IDataSourceManager partContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            partContext.PartRepository.Add(part);
            partContext.SaveChanges();

            decimal? result = SearchResultsCostCalculator.CalculateCostOfSearchResultEntity(part, partContext.DatabaseId);
            Assert.IsNull(result, "A null cost should be returned for a part master data.");
        }

        /// <summary>
        /// A test for CalculateCostOfSearchResultEntity(Part part, EntityContext entityContext) method using the following data
        /// input data-> Part: "Test Part" (a part which belongs to the testSuperAssembly).
        /// Calculation Version-> 1.1.1.
        /// </summary>
        [TestMethod]
        public void CalculatePartCostTest2()
        {
            testSuperAssembly.SetCalculationVariant("1.1.1");
            dataSourceManager.SaveChanges();

            // Retrieve the part from db without loading its references
            IDataSourceManager partContext = DataAccessFactory.CreateDataSourceManager(dataSourceManager.DatabaseId);
            Part part = partContext.PartRepository.GetPart(testSuperAssembly.Parts.FirstOrDefault(p => p.Name == "Test Part").Guid);

            decimal result = SearchResultsCostCalculator.CalculateCostOfSearchResultEntity(part, partContext.DatabaseId).GetValueOrDefault();
            decimal expectedCost = 6.6294m;
            Assert.AreEqual(expectedCost, Math.Round(result, 4), "Part cost calculation result was wrong.");
        }

        #endregion Part Tests

        #region Raw Material Tests

        /// <summary>
        /// A test for CalculateCostOfSearchResultEntity(RawMaterial material, EntityContext entityContext) method using the following data
        /// input data: a master data part.
        /// </summary>
        [TestMethod]
        public void CalculateRawMaterialCostTest1()
        {
            RawMaterial material = new RawMaterial();
            material.Name = material.Guid.ToString();
            material.IsMasterData = true;

            IDataSourceManager materialContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            materialContext.RawMaterialRepository.Add(material);
            materialContext.SaveChanges();

            decimal? result = SearchResultsCostCalculator.CalculateCostOfSearchResultEntity(material, materialContext.DatabaseId);
            Assert.IsNull(result, "A null cost should be returned for a raw material master data.");
        }

        /// <summary>
        /// A test for CalculateCostOfSearchResultEntity(Part part, EntityContext entityContext) method using the following data
        /// input data-> Raw Material: "C22" (a raw material which belongs to a part of the testSuperAssembly).
        /// Calculation Version-> 1.1.
        /// </summary>
        [TestMethod]
        public void CalculateRawMaterialCostTest2()
        {
            testSuperAssembly.SetCalculationVariant("1.1");
            dataSourceManager.SaveChanges();
            RawMaterial material = testSuperAssembly.Parts.FirstOrDefault(p => p.Name == "Test Part").RawMaterials.FirstOrDefault(m => m.Name == "C22");

            decimal result = SearchResultsCostCalculator.CalculateCostOfSearchResultEntity(material, dataSourceManager.DatabaseId).GetValueOrDefault();
            decimal expectedCost = 0.5575m;
            Assert.AreEqual(expectedCost, Math.Round(result, 4), "Raw Material cost calculation result was wrong.");
        }

        #endregion Raw Material Tests

        #region Commodity Tests

        /// <summary>
        /// A test for CalculateCostOfSearchResultEntity(Commodity commodity, EntityContext entityContext) method using the following data
        /// input data: a master data commodity.
        /// </summary>
        [TestMethod]
        public void CalculateCommodityTest1()
        {
            Commodity commodity = new Commodity();
            commodity.Name = commodity.Guid.ToString();
            commodity.Amount = 100;
            commodity.Price = 1200;
            commodity.IsMasterData = true;

            dataSourceManager.CommodityRepository.Add(commodity);
            dataSourceManager.SaveChanges();

            decimal? result = SearchResultsCostCalculator.CalculateCostOfSearchResultEntity(commodity, dataSourceManager.DatabaseId);
            Assert.IsNull(result, "A null cost should be returned for a commodity master data.");
        }

        /// <summary>
        /// A test for CalculateCostOfSearchResultEntity(Commodity commodity, EntityContext entityContext) method using the following data
        /// input data: a commodity with null amount.
        /// </summary>
        [TestMethod]
        public void CalculateCommodityTest2()
        {
            Commodity commodity = new Commodity();
            commodity.Name = commodity.Guid.ToString();
            commodity.Amount = null;
            commodity.Price = 1000;

            dataSourceManager.CommodityRepository.Add(commodity);
            dataSourceManager.SaveChanges();

            decimal? result = SearchResultsCostCalculator.CalculateCostOfSearchResultEntity(commodity, dataSourceManager.DatabaseId);
            Assert.IsNull(result, "The cost should be null for a commodity whose amount is null");
        }

        /// <summary>
        /// A test for CalculateCostOfSearchResultEntity(Commodity commodity, EntityContext entityContext) method using the following data
        /// input data: a commodity with null price.
        /// </summary>
        [TestMethod]
        public void CalculateCommodityTest3()
        {
            Commodity commodity = new Commodity();
            commodity.Name = commodity.Guid.ToString();
            commodity.Amount = 10;
            commodity.Price = null;

            dataSourceManager.CommodityRepository.Add(commodity);
            dataSourceManager.SaveChanges();

            decimal? result = SearchResultsCostCalculator.CalculateCostOfSearchResultEntity(commodity, dataSourceManager.DatabaseId);
            Assert.IsNull(result, "The cost should be null for a commodity whose price is null");
        }

        /// <summary>
        /// A test for CalculateCostOfSearchResultEntity(Commodity commodity, EntityContext entityContext) method using the following data
        /// input data: a valid commodity.
        /// </summary>
        [TestMethod]
        public void CalculateCostOfCommodityWithPartParent()
        {
            Commodity commodity = new Commodity();
            commodity.Name = commodity.Guid.ToString();
            commodity.Amount = 10;
            commodity.Price = 100;

            Part part = new Part();
            part.Name = EncryptionManager.Instance.GenerateRandomString(50, true);
            part.CountrySettings = new CountrySetting();
            part.OverheadSettings = new OverheadSetting();
            part.Commodities.Add(commodity);

            dataSourceManager.PartRepository.Add(part);
            dataSourceManager.SaveChanges();

            decimal result = SearchResultsCostCalculator.CalculateCostOfSearchResultEntity(commodity, dataSourceManager.DatabaseId).GetValueOrDefault();
            decimal expectedCost = 1000m;
            Assert.AreEqual(expectedCost, Math.Round(result, 4), "Commodity cost calculation result was wrong.");
        }

        [TestMethod]
        public void CalculateCostOfCommodityWithProcessStepParent()
        {
            Commodity commodity = new Commodity();
            commodity.Name = commodity.Guid.ToString();
            commodity.Amount = 10;
            commodity.Price = 100;

            Assembly assy = new Assembly();
            assy.Name = EncryptionManager.Instance.GenerateRandomString(50, true);
            assy.CountrySettings = new CountrySetting();
            assy.OverheadSettings = new OverheadSetting();
            assy.Process = new Process();

            var step = new AssemblyProcessStep();
            step.Name = EncryptionManager.Instance.GenerateRandomString(50, true);
            step.Commodities.Add(commodity);
            assy.Process.Steps.Add(step);

            dataSourceManager.AssemblyRepository.Add(assy);
            dataSourceManager.SaveChanges();

            decimal result = SearchResultsCostCalculator.CalculateCostOfSearchResultEntity(commodity, dataSourceManager.DatabaseId).GetValueOrDefault();
            decimal expectedCost = 1000m;
            Assert.AreEqual(expectedCost, Math.Round(result, 4), "Commodity cost calculation result was wrong.");
        }

        #endregion Commodity Tests

        #region Consumable Tests

        /// <summary>
        /// A test for CalculateCostOfSearchResultEntity(Consumable consumable, EntityContext entityContext) method using the following data
        /// input data: a master data consumable.
        /// </summary>
        [TestMethod]
        public void CalculateConsumableCostTest1()
        {
            Consumable consumable = new Consumable();
            consumable.Name = consumable.Guid.ToString();
            consumable.IsMasterData = true;
            consumable.Amount = 10;
            consumable.Price = 100;

            dataSourceManager.ConsumableRepository.Add(consumable);
            dataSourceManager.SaveChanges();

            decimal? result = SearchResultsCostCalculator.CalculateCostOfSearchResultEntity(consumable, dataSourceManager.DatabaseId);
            Assert.IsNull(result, "A null cost should be returned for a consumable master data.");
        }

        /// <summary>
        /// A test for CalculateCostOfSearchResultEntity(Consumable consumable, EntityContext entityContext) method using the following data
        /// input data: a valid consumable.
        /// </summary>
        [TestMethod]
        public void CalculateConsumableCostTest3()
        {
            Consumable consumable = new Consumable();
            consumable.Name = consumable.Guid.ToString();
            consumable.Amount = 10;
            consumable.Price = 100;

            dataSourceManager.ConsumableRepository.Add(consumable);
            dataSourceManager.SaveChanges();

            // TODO: uncomment when SearchResultsCostCalculator.CalculateCostOfSearchResultEntity is re-implemented.
            // decimal result = SearchResultsCostCalculator.CalculateCostOfSearchResultEntity(consumable, dataSourceManager.DatabaseId).GetValueOrDefault();
            // decimal expectedResult = 1000m;            
            // Assert.AreEqual(expectedResult, result, "Consumable cost calculation result was wrong.");
        }

        #endregion Consumable Tests

        #region Die Tests

        /// <summary>
        /// A test for CalculateCostOfSearchResultEntity(Die die, EntityContext entityContext) method using the following data
        /// input data: a master data die.
        /// </summary>
        [TestMethod]
        public void CalculateDieCostTest1()
        {
            Die die = new Die();
            die.Name = die.Guid.ToString();
            die.IsMasterData = true;

            dataSourceManager.DieRepository.Add(die);
            dataSourceManager.SaveChanges();

            decimal? result = SearchResultsCostCalculator.CalculateCostOfSearchResultEntity(die, dataSourceManager.DatabaseId);
            Assert.IsNull(result, "The cost should be null for die master data.");
        }

        /// <summary>
        /// A test for CalculateCostOfSearchResultEntity(Die die, EntityContext entityContext) method using the following data:
        /// Calculation Version-> 1.0 and the die belongs to an assembly.
        /// </summary>
        [TestMethod]
        public void CalculateDieCostTest2()
        {
            testSuperAssembly.SetCalculationVariant("1.0");
            dataSourceManager.SaveChanges();
            Die die = testSuperAssembly.Process.Steps.FirstOrDefault(s => s.Name == "Final-Assemblling Havac Main Unit").Dies.FirstOrDefault(d => d.Name == "Jig");

            decimal result = SearchResultsCostCalculator.CalculateCostOfSearchResultEntity(die, dataSourceManager.DatabaseId).GetValueOrDefault();
            decimal expectedResult = 0.0077m;
            Assert.AreEqual(expectedResult, Math.Round(result, 4), "Die cost calculation result was wrong.");
        }

        /// <summary>
        /// A test for CalculateCostOfSearchResultEntity(Die die, EntityContext entityContext) method using the following data:
        /// Calculation Version-> 1.1 and a die which belongs to a part.
        /// </summary>
        [TestMethod]
        public void CalculateDieCostTest3()
        {
            testSuperAssembly.SetCalculationVariant("1.1");
            dataSourceManager.SaveChanges();
            Die die = testSuperAssembly.Parts.FirstOrDefault(p => p.Name == "Test Part").Process.Steps.FirstOrDefault(s => s.Name == "Step 1").Dies.FirstOrDefault(d => d.Name == "Test Die 1");

            decimal result = SearchResultsCostCalculator.CalculateCostOfSearchResultEntity(die, dataSourceManager.DatabaseId).GetValueOrDefault();
            decimal expectedResult = 0.3750m;
            Assert.AreEqual(expectedResult, Math.Round(result, 4), "Die cost calculation result was wrong.");
        }

        /// <summary>
        /// A test for CalculateCostOfSearchResultEntity(Die die, EntityContext entityContext) method using the following data:
        /// Calculation Version-> 1.1.1 and a die which belongs to a part.
        /// </summary>
        [TestMethod]
        public void CalculateDieCostTest4()
        {
            testSuperAssembly.SetCalculationVariant("1.1.1");
            dataSourceManager.SaveChanges();
            Die die = testSuperAssembly.Parts.FirstOrDefault(p => p.Name == "Test Part").Process.Steps.FirstOrDefault(s => s.Name == "Step 1").Dies.FirstOrDefault(d => d.Name == "Test Die 1");

            decimal result = SearchResultsCostCalculator.CalculateCostOfSearchResultEntity(die, dataSourceManager.DatabaseId).GetValueOrDefault();
            decimal expectedResult = 0.3750m;
            Assert.AreEqual(expectedResult, Math.Round(result, 4), "Die cost calculation result was wrong");
        }

        /// <summary>
        /// A test for CalculateCostOfSearchResultEntity(Die die, EntityContext entityContext) method using the following data:
        /// Calculation Version-> 1.2 and a die which belongs to a part.
        /// </summary>
        [TestMethod]
        public void CalculateDieCostTest5()
        {
            testSuperAssembly.SetCalculationVariant("1.2");
            dataSourceManager.SaveChanges();
            Die die = testSuperAssembly.Parts.FirstOrDefault(p => p.Name == "Test Part").Process.Steps.FirstOrDefault(s => s.Name == "Step 1").Dies.FirstOrDefault(d => d.Name == "Test Die 1");

            decimal result = SearchResultsCostCalculator.CalculateCostOfSearchResultEntity(die, dataSourceManager.DatabaseId).GetValueOrDefault();
            decimal expectedResult = 0.3750m;
            Assert.AreEqual(expectedResult, Math.Round(result, 4), "Die cost calculation result was wrong");
        }

        #endregion Die Tests
    }
}
