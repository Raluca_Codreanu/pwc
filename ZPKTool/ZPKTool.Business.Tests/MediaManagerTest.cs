﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;

namespace ZPKTool.Business.Tests
{
    /// <summary>
    /// This is a test class for MediaManager and is intended
    /// to contain all MediaManager Unit Tests.
    /// </summary>
    [TestClass]
    public class MediaManagerTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MediaManagerTest"/> class.
        /// </summary>
        public MediaManagerTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }

        #endregion Additional test attributes

        #region Test Methods

        /// <summary>
        /// Tests GetPictureOrVideoMetaData(object entity) method.
        /// </summary>
        [TestMethod]
        public void GetPictureOrVideoMetaDataOfValidEntity()
        {
            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            image.Size = image.Content.Length;
            image.OriginalFileName = "image.txt";

            Media video = new Media();
            video.Type = MediaType.Video;
            video.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            video.Size = video.Content.Length;
            video.OriginalFileName = "video.txt";

            Media document = new Media();
            document.Type = MediaType.Document;
            document.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            document.Size = document.Content.Length;
            document.OriginalFileName = "document.txt";

            Die die = new Die();
            die.Name = "Die" + DateTime.Now.Ticks;
            die.Media = image;

            Machine machine = new Machine();
            Media machineImage = image.Copy();
            machine.Name = "Machine" + DateTime.Now.Ticks;
            machine.Media = machineImage;

            Commodity commodity = new Commodity();
            Media commodityImage = image.Copy();
            commodity.Name = "Commodity" + DateTime.Now.Ticks;
            commodity.Media = commodityImage;

            RawMaterial rawMaterial = new RawMaterial();
            Media materialImage = image.Copy();
            rawMaterial.Name = "Raw Material" + DateTime.Now.Ticks;
            rawMaterial.Media = materialImage;

            Part part = new Part();
            part.Name = "Part" + DateTime.Now.Ticks;
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            part.Media.Add(video);
            part.Media.Add(document);

            Process process = new Process();
            PartProcessStep step = new PartProcessStep();
            Media stepImage = image.Copy();
            step.Name = "Step" + DateTime.Now.Ticks;
            step.Media = stepImage;
            process.Steps.Add(step);
            part.Process = process;

            Assembly assembly = new Assembly();
            Media assemblyImage1 = image.Copy();
            Media assemblyImage2 = image.Copy();
            Media assemblyDocument = document.Copy();
            assembly.Name = "Assembly" + DateTime.Now.Ticks;
            assembly.Media.Add(assemblyImage1);
            assembly.Media.Add(assemblyImage2);
            assembly.Media.Add(assemblyDocument);

            Project project = new Project();
            Media projectVideo = video.Copy();
            Media projectDocument = document.Copy();
            project.Name = "Project" + DateTime.Now.Ticks;
            project.OverheadSettings = new OverheadSetting();
            project.Media.Add(projectVideo);
            project.Media.Add(projectDocument);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.DieRepository.Add(die);
            dataContext.MachineRepository.Add(machine);
            dataContext.CommodityRepository.Add(commodity);
            dataContext.RawMaterialRepository.Add(rawMaterial);
            dataContext.PartRepository.Add(part);
            dataContext.AssemblyRepository.Add(assembly);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            // Tests GetPictureOrVideoMetaData method for a Die entity
            MediaManager mediaManager = new MediaManager(dataContext);
            Collection<MediaMetaData> result1 = mediaManager.GetPictureOrVideoMetaData(die);

            Assert.IsTrue(
                result1.Count() >= 1,
                "GetPictureOrVideoMetaData(die)- No media metadata were returned for a die entity which has a media");

            Assert.IsNull(
                result1.Where(m => m.Type == MediaType.Document).FirstOrDefault(),
                "GetPictureOrVideoMetaData(die)- A media metadata belonging to a document was returned");

            Assert.IsNull(
                result1.Where(m => m.Guid != image.Guid).FirstOrDefault(),
                "GetPictureOrVideoMetaData(die)- A media metadata which doesn't belong to entity was returned");

            // Tests GetPictureOrVideoMetaData method for a Machine entity
            Collection<MediaMetaData> result2 = mediaManager.GetPictureOrVideoMetaData(machine);

            Assert.IsTrue(
                result2.Count() >= 1,
                "GetPictureOrVideoMetaData(machine)- No media metadata were returned for a machine entity which has a media");

            Assert.IsNull(
                result2.Where(m => m.Type == MediaType.Document).FirstOrDefault(),
                "GetPictureOrVideoMetaData(machine)- A media metadata belonging to a document was returned");

            Assert.IsNull(
                result2.Where(m => m.Guid != machineImage.Guid).FirstOrDefault(),
                "GetPictureOrVideoMetaData(machine)- A media metadata which doesn't belong to entity was returned");

            // Tests GetPictureOrVideoMetaData method for a Commodity entity
            Collection<MediaMetaData> result3 = mediaManager.GetPictureOrVideoMetaData(commodity);

            Assert.IsTrue(
                result3.Count() >= 1,
                "GetPictureOrVideoMetaData(commodity)- No media metadata were returned for a commodity entity which has a media");

            Assert.IsNull(
                result3.Where(m => m.Type == MediaType.Document).FirstOrDefault(),
                "GetPictureOrVideoMetaData(commodity)- A media metadata belonging to a document was returned");

            Assert.IsNull(
                result3.Where(m => m.Guid != commodityImage.Guid).FirstOrDefault(),
                "GetPictureOrVideoMetaData(commodity)- A media metadata which doesn't belong to entity was returned");

            // Tests GetPictureOrVideoMetaData method for a Raw Material entity
            Collection<MediaMetaData> result4 = mediaManager.GetPictureOrVideoMetaData(rawMaterial);

            Assert.IsTrue(
                result4.Count() >= 1,
                "GetPictureOrVideoMetaData(rawMaterial)- No media metadata were returned for a raw material entity which has a media");

            Assert.IsNull(
                result4.Where(m => m.Type == MediaType.Document).FirstOrDefault(),
                "GetPictureOrVideoMetaData(rawMaterial)- A media metadata belonging to a document was returned");

            Assert.IsNull(
                result4.Where(m => m.Guid != materialImage.Guid).FirstOrDefault(),
                "GetPictureOrVideoMetaData(rawMaterial)- A media metadata which doesn't belong to entity was returned");

            // Tests GetPictureOrVideoMetaData method for a Process Step entity
            Collection<MediaMetaData> result5 = mediaManager.GetPictureOrVideoMetaData(step);

            Assert.IsTrue(
                result5.Count() >= 1,
                "GetPictureOrVideoMetaData(step)- No media metadata were returned for a process step entity which has a media");

            Assert.IsNull(
                result5.Where(m => m.Type == MediaType.Document).FirstOrDefault(),
                "GetPictureOrVideoMetaData(step)- A media metadata belonging to a document was returned");

            Assert.IsNull(
                result5.Where(m => m.Guid != stepImage.Guid).FirstOrDefault(),
                "GetPictureOrVideoMetaData(step)- A media metadata which doesn't belong to entity was returned");

            // Tests GetPictureOrVideoMetaData method for a Part entity
            Collection<MediaMetaData> result6 = mediaManager.GetPictureOrVideoMetaData(part);

            Assert.IsTrue(
                result6.Count() >= 1,
                "GetPictureOrVideoMetaData(part)- Not all medias metadata were returned for a part entity which has medias");

            Assert.IsNull(
                result6.Where(m => m.Type == MediaType.Document).FirstOrDefault(),
                "GetPictureOrVideoMetaData(part)- A media metadata belonging to a document was returned");

            Assert.IsNull(
                result6.Where(m => m.Guid != video.Guid).FirstOrDefault(),
                "GetPictureOrVideoMetaData(part)- A media metadata which doesn't belong to entity was returned");

            // Tests GetPictureOrVideoMetaData method for an Assembly entity
            Collection<MediaMetaData> result7 = mediaManager.GetPictureOrVideoMetaData(assembly);

            Assert.IsTrue(
                result7.Count() >= 2,
                "GetPictureOrVideoMetaData(assembly)- Not all medias metadata were returned for an assembly entity which has medias");

            Assert.IsNull(
                result7.Where(m => m.Type == MediaType.Document).FirstOrDefault(),
                "GetPictureOrVideoMetaData(assembly)- A media metadata belonging to a document was returned");

            Assert.IsNull(
                result7.Where(m => m.Guid != assemblyImage1.Guid && m.Guid != assemblyImage2.Guid).FirstOrDefault(),
                "GetPictureOrVideoMetaData(assembly)- A media metadata which doesn't belong to entity was returned");

            // Tests GetPictureOrVideoMetaData method for a Project entity
            Collection<MediaMetaData> result8 = mediaManager.GetPictureOrVideoMetaData(project);

            Assert.IsTrue(
                result8.Count() >= 1,
                "GetPictureOrVideoMetaData(project)- Not all medias metadata were returned for a project entity which has medias");

            Assert.IsNull(
                result8.Where(m => m.Type == MediaType.Document).FirstOrDefault(),
                "GetPictureOrVideoMetaData(project)- A media metadata belonging to a document was returned");

            Assert.IsNull(
                result8.Where(m => m.Guid != projectVideo.Guid).FirstOrDefault(),
                "GetPictureOrVideoMetaData(project)- A media metadata which doesn't belong to entity was returned");
        }

        /// <summary>
        /// Tests GetPictureOrVideoMetaData(object entity) method using an entity which does not exist in database.
        /// </summary>
        [TestMethod]
        public void GetPictureOrVideoMetaDataOfInvalidEntity()
        {
            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            image.Size = image.Content.Length;
            image.OriginalFileName = "image.txt";

            Die die = new Die();
            die.Name = "Die" + DateTime.Now.Ticks;
            die.Media = image;

            IDataSourceManager changeSet = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MediaManager mediaManager = new MediaManager(changeSet);
            Collection<MediaMetaData> result = mediaManager.GetPictureOrVideoMetaData(die);

            Assert.IsTrue(result.Count == 0, "The method returned media meta data for an entity which does not exist in data base");
        }

        /// <summary>
        /// Tests GetPictureOrVideoMetaData(object entity) method using a null entity.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetPictureOrVideoMetaDataOfNullEntity()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MediaManager mediaManager = new MediaManager(dataContext);
            Collection<MediaMetaData> result = mediaManager.GetPictureOrVideoMetaData(null);
        }

        /// <summary>
        /// Tests GetPictureOrVideoMetaData(object entity) method using a valid context as input data.
        /// </summary>
        [TestMethod]
        public void GetPictureOrVideoMetaDataFromValidContext()
        {
            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            image.Size = image.Content.Length;
            image.OriginalFileName = "image.txt";

            Media video = new Media();
            video.Type = MediaType.Video;
            video.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            video.Size = video.Content.Length;
            video.OriginalFileName = "video.txt";

            Media document = new Media();
            document.Type = MediaType.Document;
            document.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            document.Size = document.Content.Length;
            document.OriginalFileName = "document.txt";

            Die die = new Die();
            die.Name = "Die" + DateTime.Now.Ticks;
            die.Media = image;

            Assembly assembly = new Assembly();
            assembly.Name = "Assembly" + DateTime.Now.Ticks;
            assembly.Media.Add(video);
            assembly.Media.Add(document);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.AssemblyRepository.Add(assembly);
            dataContext.SaveChanges();

            // Tests GetPictureOrVideoMetaData for an assembly which belongs to LocalDatabase
            MediaManager mediaManager = new MediaManager(dataContext);
            Collection<MediaMetaData> result1 = mediaManager.GetPictureOrVideoMetaData(assembly);

            Assert.IsTrue(result1.Count >= 1, "No media metadata was returned for an entity that has medias");
            Assert.IsNull(result1.FirstOrDefault(m => m.Type == MediaType.Document), "A media metadata belonging to a document was returned");
            Assert.IsNull(result1.FirstOrDefault(m => m.Guid != video.Guid), "A media metadata which doesn't belong to entity was returned");
        }

        /// <summary>
        /// Tests GetPictureOrVideoMetaData(object entity) method using a not set context as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(BusinessException))]
        public void GetPictureOrVideoMetaDataFromInvalidContext()
        {
            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            image.Size = image.Content.Length;
            image.OriginalFileName = "image.txt";

            Die die = new Die();
            die.Name = "Die" + DateTime.Now.Ticks;
            die.Media = image;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.DieRepository.Add(die);
            dataContext.SaveChanges();

            // Modify connParams of LocalDb to a wrong value
            DbConnectionInfo localConnParams = new DbConnectionInfo();
            localConnParams.InitialCatalog = "ZPKTool";
            localConnParams.DataSource = @"localhost\SQLEXPRESSDummy";
            localConnParams.UserId = "PROimpensUser";
            localConnParams.Password = "iRl9VE3o1n1GPJexKN6RjA==";
            localConnParams.PasswordEncryptionKey = ZPKTool.Common.Constants.PasswordEncryptionKey;
            ConnectionConfiguration.Initialize(localConnParams, new DbConnectionInfo());

            // Try to get the base measurement unit from a scale
            IDataSourceManager changeSet = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MediaManager mediaManager = new MediaManager(changeSet);
            Collection<MediaMetaData> result = mediaManager.GetPictureOrVideoMetaData(die);
        }

        /// <summary>
        /// Tests GetDocumentsMetaData(object entity) method using a null entity as input parameter.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetDocumentsMetadataOfNullEntity()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MediaManager mediaManager = new MediaManager(dataContext);
            mediaManager.GetDocumentsMetaData(null);
        }

        /// <summary>
        /// Tests GetDocumentsMetaData(object entity) method using a valid entity as input parameter.
        /// </summary>
        [TestMethod]
        public void GetDocumentsMetaDataOfValidEntity()
        {
            Project project = new Project();
            project.Name = "Project" + DateTime.Now.Ticks;
            project.OverheadSettings = new OverheadSetting();

            Media document1 = new Media();
            document1.Type = MediaType.Document;
            document1.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            document1.Size = document1.Content.Length;
            document1.OriginalFileName = "document1.txt";
            project.Media.Add(document1);

            Media document2 = new Media();
            document2.Type = MediaType.Document;
            document2.Content = new byte[] { 1, 1, 1, 1, 1, 1, 0 };
            document2.Size = document2.Content.Length;
            document2.OriginalFileName = "document2.txt";
            project.Media.Add(document2);

            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            image.Size = image.Content.Length;
            image.OriginalFileName = "image.txt";
            project.Media.Add(image);

            Media video = new Media();
            video.Type = MediaType.Video;
            video.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            video.Size = video.Content.Length;
            video.OriginalFileName = "video.txt";
            project.Media.Add(video);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            MediaManager mediaManager = new MediaManager(dataContext);
            Collection<MediaMetaData> documentsMetadata = mediaManager.GetDocumentsMetaData(project);
            Collection<Media> corespondingMedia = new Collection<Media>(dataContext.EntityContext.MediaSet.Where(m => m.Project != null && m.Project.Guid == project.Guid &&
                                                                                                           m.Type == MediaType.Document).ToList());

            Assert.AreEqual<int>(corespondingMedia.Count, documentsMetadata.Count, "The count of documents metadata is different than the expected one");
            foreach (Media media in corespondingMedia)
            {
                if (documentsMetadata.FirstOrDefault(m => m.Guid == media.Guid) == null)
                {
                    Assert.Fail("Failed to return all documents metadata");
                }
            }
        }

        /// <summary>
        /// Tests GetPictureOrVideo(object entity) method using a valid entity as input data.
        /// </summary>
        [TestMethod]
        public void GetPictureOrVideoOfValidEntity()
        {
            Media image1 = new Media();
            image1.Type = (short)MediaType.Image;
            image1.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            image1.Size = image1.Content.Length;
            image1.OriginalFileName = "image.txt";

            Media video = new Media();
            video.Type = MediaType.Video;
            video.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            video.Size = video.Content.Length;
            video.OriginalFileName = "video.txt";

            Media document1 = new Media();
            document1.Type = MediaType.Document;
            document1.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            document1.Size = document1.Content.Length;
            document1.OriginalFileName = "document.txt";

            Assembly assembly1 = new Assembly();
            Media image2 = image1.Copy();
            assembly1.Name = "Assembly1_" + DateTime.Now.Ticks;
            assembly1.Media.Add(image1);
            assembly1.Media.Add(image2);
            assembly1.Media.Add(document1);

            Assembly assembly2 = new Assembly();
            Media document2 = document1.Copy();
            assembly2.Name = "Assembly2_" + DateTime.Now.Ticks;
            assembly2.Media.Add(video);
            assembly2.Media.Add(document2);

            Media image3 = new Media();
            image3.Type = (short)MediaType.Image;
            image3.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            image3.Size = image3.Content.Length;
            image3.OriginalFileName = image3.Guid.ToString();

            Commodity commodity = new Commodity();
            commodity.Name = commodity.Guid.ToString();
            commodity.Media = image3;

            Die die = new Die();
            die.Name = "Die" + DateTime.Now.Ticks;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.AssemblyRepository.Add(assembly1);
            dataContext.AssemblyRepository.Add(assembly2);
            dataContext.CommodityRepository.Add(commodity);
            dataContext.DieRepository.Add(die);
            dataContext.SaveChanges();

            MediaManager mediaManager = new MediaManager(dataContext);
            Media result1 = mediaManager.GetPictureOrVideo(assembly1);
            Assert.IsNotNull(result1, "No media was returned for an entity which has more medias");
            Assert.IsTrue(result1.Guid == image1.Guid || result1.Guid == image2.Guid, "The returned media doesn't belong to entity");

            Media result2 = mediaManager.GetPictureOrVideo(assembly2);
            Assert.IsNotNull(result2, "No media was returned for an entity which has a media of video type");
            Assert.AreEqual<Guid>(video.Guid, result2.Guid, "The returned media doesn't belong to entity");

            Media result3 = mediaManager.GetPictureOrVideo(die);
            Assert.IsNull(result3, "A media was returned for an entity that doesn't have a media");

            // Delete the commodity's media 
            var commodityImage = dataContext.EntityContext.MediaSet.Find(image3.Guid);
            dataContext.MediaRepository.RemoveAll(commodityImage);

            // Retrieve a media that is in deleted state
            Media result4 = mediaManager.GetPictureOrVideo(commodity);
            Assert.IsNull(result4, "A media that is in deleted state shouldn't be returned");
        }

        /// <summary>
        /// Tests GetPictureOrVideo(object entity) method using an entity which doesn't exist in data base or a null entity.
        /// </summary>
        [TestMethod]
        public void GetPictureOrVideoOfInvalidEntity()
        {
            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            image.Size = image.Content.Length;
            image.OriginalFileName = "image.txt";

            Die die = new Die();
            die.Name = "Die" + DateTime.Now.Ticks;
            die.Media = image;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MediaManager mediaManager = new MediaManager(dataContext);
            Media result1 = mediaManager.GetPictureOrVideo(die);
            Assert.IsNull(result1, "The method returned a media for an entity that wasn't saved in data base");

            Media result2 = mediaManager.GetPictureOrVideo(null);
            Assert.IsNull(result1, "The method returned a media for a null entity");
        }

        /// <summary>
        /// Tests GetPictures(object entity, MediaMetaData metaData) method using a valid entity.
        /// </summary>
        [TestMethod]
        public void GetPicturesOfValidEntity()
        {
            Project project = new Project();
            project.Name = "Test Project" + DateTime.Now.Ticks;
            project.OverheadSettings = new OverheadSetting();

            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            image.Size = image.Content.Length;
            image.OriginalFileName = "image.txt";
            project.Media.Add(image);

            Media document = new Media();
            document.Type = MediaType.Document;
            document.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            document.Size = document.Content.Length;
            document.OriginalFileName = "document.txt";
            project.Media.Add(document);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            MediaManager mediaManager = new MediaManager(dataContext);
            MediaMetaData imageMetadata = mediaManager.GetPictureOrVideoMetaData(project).FirstOrDefault(m => m.Type == (short)MediaType.Image);

            // Tests the method using valid data.
            Collection<Media> result1 = mediaManager.GetPictures(project);
            Assert.IsTrue(result1.Count() >= 1, "Not all entity's pictures were returned");
            Assert.IsNull(result1.FirstOrDefault(m => m.Type == MediaType.Document || m.Type == MediaType.Video), "A media of a different type was returned");
            Assert.IsNull(result1.FirstOrDefault(m => m.Guid != image.Guid), "A media belonging to other entity was returned");

            // Tests the method using a null entity as input parameter.
            Collection<Media> result2 = mediaManager.GetPictures(null);
            Assert.IsNull(result2, "The result should be null for a null entity");

            // Tests the method using an entity whose pictures are in deleted state.            
            dataContext.MediaRepository.RemoveAll(dataContext.EntityContext.MediaSet.Find(image.Guid));

            MediaMetaData imageMetadata2 = new MediaMetaData();
            imageMetadata2.Guid = image.Guid;
            imageMetadata2.Type = image.Type;
            imageMetadata2.Size = image.Size;
            imageMetadata2.OriginalFileName = image.OriginalFileName;
            imageMetadata2.IsMasterData = image.IsMasterData;

            Collection<Media> result4 = mediaManager.GetPictures(project);
            Assert.IsTrue(result4.Count == 0, "The result should be an empty collection for an entity whose images are in deleted state.");
        }

        /// <summary>
        /// Tests GetPicture(object entity) method using valid data.
        /// </summary>
        [TestMethod]
        public void GetPictureOfValidEntity()
        {
            Project project = new Project();
            project.Name = "TestProject" + DateTime.Now.Ticks;
            project.OverheadSettings = new OverheadSetting();

            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            image.Size = image.Content.Length;
            image.OriginalFileName = "image.txt";
            project.Media.Add(image);

            Media document = new Media();
            document.Type = MediaType.Document;
            document.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            document.Size = document.Content.Length;
            document.OriginalFileName = "document.txt";
            project.Media.Add(document);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            // Tests the method using valid data.
            MediaManager mediaManager = new MediaManager(dataContext);
            Media result1 = mediaManager.GetPicture(project);
            Assert.IsNotNull(result1, "Failed to get picture of a project");
            Assert.IsTrue(result1.Type == (short)MediaType.Image, "The returned media is not a picture");
            Assert.IsTrue(result1.Guid == image.Guid, "The returned picture doesn't belong to input entity");

            // Tests the method using a null entity as input parameter.
            Media result2 = mediaManager.GetPicture(null);
            Assert.IsNull(result2, "A picture was returned for a null entity");

            // Tests the method using an entity whose pictures were deleted.            
            dataContext.MediaRepository.RemoveAll(dataContext.EntityContext.MediaSet.Find(image.Guid));
            dataContext.SaveChanges();

            Media result4 = mediaManager.GetPicture(project);
            Assert.IsNull(result4, "The result should be null for an entity whose images were deleted");
        }

        /// <summary>
        /// Tests GetAllMedia(object entity) method using a valid entity as input data.
        /// </summary>
        [TestMethod]
        public void GetAllMediaOfValidEntity()
        {
            Project project = new Project();
            project.Name = "TestProject" + DateTime.Now.Ticks;
            project.OverheadSettings = new OverheadSetting();

            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            image.Size = image.Content.Length;
            image.OriginalFileName = "image.txt";
            project.Media.Add(image);

            Media video = new Media();
            video.Type = MediaType.Video;
            video.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            video.Size = video.Content.Length;
            video.OriginalFileName = "video.txt";
            project.Media.Add(video);

            Media document = new Media();
            document.Type = MediaType.Document;
            document.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            document.Size = document.Content.Length;
            document.OriginalFileName = "document.txt";
            project.Media.Add(document);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            MediaManager mediaManager = new MediaManager(dataContext);
            Collection<Media> result = mediaManager.GetAllMedia(project);
            Collection<Media> expectedResults = new Collection<Media>(
                dataContext.EntityContext.MediaSet.Where(m => m.Project != null && m.Project.Guid == project.Guid &&
                                                  (m.Type == MediaType.Image ||
                                                   m.Type == MediaType.Video ||
                                                   m.Type == MediaType.Document)).ToList());

            Assert.IsTrue(result.Count() == expectedResults.Count(), "GetAllMedia method returned wrong results");
            foreach (Media media in expectedResults)
            {
                if (result.FirstOrDefault(m => m.Guid == media.Guid) == null)
                {
                    Assert.Fail("Not all entity's medias were returned");
                }
            }
        }

        /// <summary>
        /// Tests GetAllMedia(object entity) method using a null entity as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetAllMediaOfNullEntity()
        {
            IDataSourceManager changeSet = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MediaManager mediaManager = new MediaManager(changeSet);
            Collection<Media> result = mediaManager.GetAllMedia(null);
        }

        /// <summary>
        /// Tests GetPicturesForProcessSteps method using valid data.
        /// </summary>
        [TestMethod]
        public void GetPicturesForProcessStepsUsingValidSteps()
        {
            Process process = new Process();
            ProcessStep step1 = new AssemblyProcessStep();
            step1.Name = "Test" + DateTime.Now.Ticks;

            Media media1 = new Media();
            media1.Type = (short)MediaType.Image;
            media1.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            media1.Size = media1.Content.Length;
            media1.OriginalFileName = "test.txt";
            step1.Media = media1;
            process.Steps.Add(step1);

            ProcessStep step2 = new AssemblyProcessStep();
            step2.Name = "Test" + DateTime.Now.Ticks;

            Media media2 = new Media();
            media2.Type = MediaType.Document;
            media2.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            media2.Size = media2.Content.Length;
            media2.OriginalFileName = "test.txt";
            step2.Media = media2;
            process.Steps.Add(step2);

            ProcessStep step3 = new AssemblyProcessStep();
            step3.Name = "Test" + DateTime.Now.Ticks;
            process.Steps.Add(step3);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProcessRepository.Add(process);
            dataContext.SaveChanges();

            List<ProcessStep> stepList1 = new List<ProcessStep>();
            stepList1.Add(step1);
            stepList1.Add(step2);
            stepList1.Add(step3);

            MediaManager mediaManager = new MediaManager(dataContext);
            var result1 = mediaManager.GetPicturesForProcessSteps(stepList1);

            Assert.IsTrue(result1.Count >= 2, "Not all pairs process step -> media were returned");

            Media step3Media;
            bool step3MediaFound = result1.TryGetValue(step3.Guid, out step3Media);
            Assert.IsFalse(step3MediaFound, "The result contains a step that doesn't have media");

            List<ProcessStep> stepList2 = new List<ProcessStep>();
            var result2 = mediaManager.GetPicturesForProcessSteps(stepList2);

            Assert.IsTrue(result2.Count == 0, "The result should be null for an empty list");
        }

        /// <summary>
        /// Tests GetPicturesForProcessSteps method using a null list as input parameter.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetPicturesForProcessStepsUsingNullStepsList()
        {
            IDataSourceManager changeSet = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MediaManager mediaManager = new MediaManager(changeSet);
            var result = mediaManager.GetPicturesForProcessSteps(null);
        }

        /// <summary>
        /// Tests DeleteMedia(MediaMetaData metaData) method using a valid metadata as input parameter.
        /// </summary>
        [TestMethod]
        public void DeleteMediaUsingValidMetadata()
        {
            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            image.Size = image.Content.Length;
            image.OriginalFileName = "test.txt";

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.MediaRepository.Add(image);
            dataContext1.SaveChanges();

            MediaMetaData imageMetadata = new MediaMetaData();
            imageMetadata.Guid = image.Guid;
            imageMetadata.IsMasterData = image.IsMasterData;
            imageMetadata.OriginalFileName = image.OriginalFileName;
            imageMetadata.Size = image.Size;
            imageMetadata.Type = image.Type;

            MediaManager mediaManager = new MediaManager(dataContext1);
            mediaManager.DeleteMedia(imageMetadata);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Media result = dataContext2.EntityContext.MediaSet.Where(m => m.Guid == image.Guid).FirstOrDefault();

            Assert.IsNull(result, "Failed to delete a media");
        }

        /// <summary>
        /// Tests DeleteMedia(MediaMetaData metaData) method using a null metadata as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void DeleteMediaUsingNullMetadata()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MediaManager mediaManager = new MediaManager(dataContext);
            MediaMetaData mediaMetadata = null;
            mediaManager.DeleteMedia(mediaMetadata);
        }

        /// <summary>
        /// Tests DeleteMedia(Media media) using a null media as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void DeleteNullMedia()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MediaManager mediaManager = new MediaManager(dataContext);
            Media media = null;
            mediaManager.DeleteMedia(media);
        }

        /// <summary>
        /// Tests DeleteMedia(Media media) using a valid media as input data.
        /// </summary>
        [TestMethod]
        public void DeleteValidMedia()
        {
            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            image.Size = image.Content.Length;
            image.OriginalFileName = "test.txt";

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.MediaRepository.Add(image);
            dataContext1.SaveChanges();

            MediaManager mediaManager = new MediaManager(dataContext1);
            image = dataContext1.EntityContext.MediaSet.Find(image.Guid);
            mediaManager.DeleteMedia(image);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Media deletedMedia = dataContext2.EntityContext.MediaSet.FirstOrDefault(m => m.Guid == image.Guid);

            // Verifies that media was deleted
            Assert.IsNull(deletedMedia, "Failed to delete a media");
        }

        /// <summary>
        /// Tests DeleteAllImagesAndVideo(object entity) method using a valid entity as input data. 
        /// </summary>
        [TestMethod]
        public void DeleteAllImagesAndVideoOfValidEntity()
        {
            Project project = new Project();
            project.Name = "Test Project" + DateTime.Now.Ticks;

            OverheadSetting overheadSetting = new OverheadSetting();
            overheadSetting.MaterialOverhead = 1;
            overheadSetting.ConsumableOverhead = 1;
            overheadSetting.CommodityOverhead = 1;
            overheadSetting.ExternalWorkOverhead = 1;
            overheadSetting.ManufacturingOverhead = 1;
            overheadSetting.MaterialMargin = 1;
            overheadSetting.ConsumableMargin = 1;
            overheadSetting.CommodityMargin = 1;
            overheadSetting.ExternalWorkMargin = 1;
            overheadSetting.ManufacturingMargin = 1;
            overheadSetting.OtherCostOHValue = 1;
            overheadSetting.PackagingOHValue = 1;
            overheadSetting.LogisticOHValue = 1;
            overheadSetting.SalesAndAdministrationOHValue = 1;
            project.OverheadSettings = overheadSetting;

            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            image.Size = image.Content.Length;
            image.OriginalFileName = "test.txt";
            project.Media.Add(image);

            Media video = new Media();
            video.Type = MediaType.Video;
            video.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            video.Size = video.Content.Length;
            video.OriginalFileName = "test.txt";
            project.Media.Add(video);

            Media document = new Media();
            document.Type = MediaType.Document;
            document.Content = new byte[] { 1, 1, 1, 1, 1, 1, 1 };
            document.Size = document.Content.Length;
            document.OriginalFileName = "test.txt";
            project.Media.Add(document);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            MediaManager mediaManager = new MediaManager(dataContext);
            mediaManager.DeleteAllImagesAndVideo(project);
            dataContext.SaveChanges();

            Collection<Media> result = mediaManager.GetAllMedia(project);
            Assert.IsTrue(result.Where(m => m.Type == MediaType.Image || m.Type == MediaType.Video).Count() == 0, "Failed to delete all images and videos of entity");
            Assert.IsTrue(result.Where(m => m.Type == MediaType.Document).Count() >= 1, "A media which is not a video or image was deleted");
        }

        /// <summary>
        /// Tests DeleteAllImagesAndVideo(object entity) method using a null entity as input parameter.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void DeleteAllImagesAndVideoOfNullEntity()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MediaManager mediaManager = new MediaManager(dataContext);
            mediaManager.DeleteAllImagesAndVideo(null);
        }

        /// <summary>
        /// Tests DeleteBatchMedia using valid data as input parameters.
        /// </summary>
        [TestMethod]
        public void DeleteBatchMediaUsingValidDatas()
        {
            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            image.Size = image.Content.Length;
            image.OriginalFileName = "test.txt";

            Media video = new Media();
            video.Type = MediaType.Video;
            video.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            video.Size = video.Content.Length;
            video.OriginalFileName = "test.txt";

            Media document = new Media();
            document.Type = MediaType.Document;
            document.Content = new byte[] { 1, 1, 1, 1, 1, 1, 1 };
            document.Size = document.Content.Length;
            document.OriginalFileName = "test.txt";

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.MediaRepository.Add(image);
            dataContext1.MediaRepository.Add(video);
            dataContext1.MediaRepository.Add(document);
            dataContext1.SaveChanges();

            MediaMetaData imageMetadata = new MediaMetaData();
            imageMetadata.Guid = image.Guid;
            imageMetadata.IsMasterData = image.IsMasterData;
            imageMetadata.OriginalFileName = image.OriginalFileName;
            imageMetadata.Size = image.Size;
            imageMetadata.Type = image.Type;

            MediaMetaData videoMetadata = new MediaMetaData();
            videoMetadata.Guid = video.Guid;
            videoMetadata.IsMasterData = video.IsMasterData;
            videoMetadata.OriginalFileName = video.OriginalFileName;
            videoMetadata.Size = video.Size;
            videoMetadata.Type = video.Type;

            MediaMetaData documentMetadata = new MediaMetaData();
            documentMetadata.Guid = document.Guid;
            documentMetadata.IsMasterData = document.IsMasterData;
            documentMetadata.OriginalFileName = document.OriginalFileName;
            documentMetadata.Size = document.Size;
            documentMetadata.Type = document.Type;

            List<MediaMetaData> mediaList1 = new List<MediaMetaData>();
            mediaList1.Add(imageMetadata);
            mediaList1.Add(documentMetadata);

            MediaManager mediaManager = new MediaManager(dataContext1);
            mediaManager.DeleteBatchMedia(mediaList1);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Media imageDeleted = dataContext2.EntityContext.MediaSet.FirstOrDefault(m => m.Guid == imageMetadata.Guid);
            Media documentDeleted = dataContext2.EntityContext.MediaSet.FirstOrDefault(m => m.Guid == documentMetadata.Guid);
            Media videoDeleted = dataContext2.EntityContext.MediaSet.FirstOrDefault(m => m.Guid == videoMetadata.Guid);

            Assert.IsNull(imageDeleted, "Failed to delete all medias from the batch");
            Assert.IsNull(documentDeleted, " Failed to delete all medias from the batch");
            Assert.IsNotNull(videoDeleted, "A wrong media was deleted");
        }

        /// <summary>
        /// Tests DeleteBatchMedia method using a null list as input parameter.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void DeleteBatchMediaUsingNullList()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MediaManager mediaManager = new MediaManager(dataContext);
            mediaManager.DeleteBatchMedia(null);
        }

        /// <summary>
        /// Tests DeleteAllMedia(object entity) method using a null entity as input parameter.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void DeleteAllMediaOfNullEntity()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MediaManager mediaManager = new MediaManager(dataContext);
            mediaManager.DeleteAllMedia(null);
        }

        /// <summary>
        /// Tests DeleteAllMedia(object entity) method using a valid entity as input data.
        /// </summary>
        [TestMethod]
        public void DeleteAllMediaOfValidEntity()
        {
            Project project = new Project();
            project.Name = "Project" + DateTime.Now.Ticks;
            project.OverheadSettings = new OverheadSetting();

            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            image.Size = image.Content.Length;
            image.OriginalFileName = "image.txt";
            project.Media.Add(image);

            Media video = new Media();
            video.Type = MediaType.Video;
            video.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            video.Size = video.Content.Length;
            video.OriginalFileName = "video.txt";
            project.Media.Add(video);

            Media document = new Media();
            document.Type = MediaType.Document;
            document.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            document.Size = document.Content.Length;
            document.OriginalFileName = "document.txt";
            project.Media.Add(document);

            IDataSourceManager dataContext1 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext1.ProjectRepository.Add(project);
            dataContext1.SaveChanges();

            MediaManager mediaManager = new MediaManager(dataContext1);
            mediaManager.DeleteAllMedia(project);
            dataContext1.SaveChanges();

            IDataSourceManager dataContext2 = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            Media deletedImage = dataContext2.EntityContext.MediaSet.Where(i => i.Guid == image.Guid).FirstOrDefault();
            Media deletedVideo = dataContext2.EntityContext.MediaSet.Where(v => v.Guid == video.Guid).FirstOrDefault();
            Media deletedDocument = dataContext2.EntityContext.MediaSet.Where(d => d.Guid == document.Guid).FirstOrDefault();

            // Verifies that all project's media were delete
            Assert.IsNull(deletedImage, "Failed to deleted an image");
            Assert.IsNull(deletedVideo, "Failed to delete a video");
            Assert.IsNull(deletedDocument, "Failed to delete a document");
        }

        /// <summary>
        /// Tests AddEntityMedia(IEntityWithMedia entityObject, Media newMedia) method using valid data as input parameters.
        /// </summary>
        [TestMethod]
        public void AddSingleMediaToValidEntity()
        {
            Project project1 = new Project();
            project1.Name = "TestProject" + DateTime.Now.Ticks;
            project1.OverheadSettings = new OverheadSetting();

            Project project2 = new Project();
            project2.Name = project2.Guid.ToString();
            project2.OverheadSettings = new OverheadSetting();

            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            assembly.IsMasterData = true;

            User user = new User();
            user.Username = user.Guid.ToString();
            user.Name = user.Guid.ToString();
            user.Password = EncryptionManager.Instance.HashSHA256("Password.", user.Salt, user.Guid.ToString());
            project2.SetOwner(user);
            user.Roles = Role.Admin;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProjectRepository.Add(project1);
            dataContext.ProjectRepository.Add(project2);
            dataContext.AssemblyRepository.Add(assembly);
            dataContext.SaveChanges();

            Media image1 = new Media();
            image1.Type = (short)MediaType.Image;
            image1.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            image1.Size = image1.Content.Length;
            image1.OriginalFileName = image1.Guid.ToString();

            // Add a media to an entity that has null owner and is not a MasterData
            MediaManager mediaManager = new MediaManager(dataContext);
            mediaManager.AddEntityMedia(project1, new List<Media>() { image1 });

            Assert.IsNotNull(project1.Media.FirstOrDefault(m => m.Guid == image1.Guid), "Failed to add a single media to entity.");
            Assert.IsNull(image1.Owner, "Media doesn't have the same owner as the parent entity");
            Assert.IsFalse(image1.IsMasterData, "Failed to set IsMasterData flag");

            Media image2 = new Media();
            image2.Type = (short)MediaType.Image;
            image2.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            image2.Size = image2.Content.Length;
            image2.OriginalFileName = image2.Guid.ToString();

            // Add a media to an entity that has owner
            mediaManager.AddEntityMedia(project2, new List<Media>() { image2 });
            Assert.IsNotNull(project2.Media.FirstOrDefault(m => m.Guid == image2.Guid), "Failed to add a single media to entity.");
            Assert.IsTrue(image2.Owner != null && image2.Owner.Guid == project2.Owner.Guid, "Media doesn't have the same owner as the parent entity");

            Media image3 = new Media();
            image3.Type = (short)MediaType.Image;
            image3.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            image3.Size = image3.Content.Length;
            image3.OriginalFileName = image3.Guid.ToString();

            // Add a media to an entity that belongs to MasterData
            mediaManager.AddEntityMedia(assembly, new List<Media>() { image3 });
            Assert.IsNotNull(assembly.Media.FirstOrDefault(m => m.Guid == image3.Guid), "Failed to add a single media to entity.");
            Assert.IsTrue(image3.IsMasterData, "Failed to set IsMasterData flag");
        }

        /// <summary>
        /// Tests AddEntityMedia(IEntityWithMedia entityObject, Media newMedia) method using a null entity as input
        /// data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddSingleMediaToNullEntity()
        {
            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            image.Size = image.Content.Length;
            image.OriginalFileName = "test.txt";

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MediaManager mediaManager = new MediaManager(dataContext);
            mediaManager.AddEntityMedia(null, image);
        }

        /// <summary>
        /// Tests AddEntityMedia(IEntityWithMedia entityObject, Media newMedia) method using a null media as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddNullMediaToEntity()
        {
            Project project = new Project();
            project.Name = "TestProject" + DateTime.Now.Ticks;
            project.OverheadSettings = new OverheadSetting();

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            MediaManager mediaManager = new MediaManager(dataContext);
            mediaManager.AddEntityMedia(project, null);
        }

        /// <summary>
        /// Tests AddEntityMedia(IEntityWithMediaCollection entityObject, List[Media] newMedia) method using valid 
        /// data as input parameters.
        /// </summary>
        [TestMethod]
        public void AddMediaListToValidEntity()
        {
            Project project = new Project();
            project.Name = "TestProject" + DateTime.Now.Ticks;
            project.OverheadSettings = new OverheadSetting();

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            image.Size = image.Content.Length;
            image.OriginalFileName = "image.txt";

            Media video = new Media();
            video.Type = MediaType.Video;
            video.Content = new byte[] { 1, 1, 1, 1, 1, 1, 1 };
            video.Size = video.Content.Length;
            video.OriginalFileName = "video.txt";

            Media document = new Media();
            document.Type = MediaType.Document;
            document.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            document.Size = document.Content.Length;
            document.OriginalFileName = "document.txt";

            List<Media> mediaList1 = new List<Media>();
            mediaList1.Add(image);
            mediaList1.Add(video);
            mediaList1.Add(document);

            MediaManager mediaManager = new MediaManager(dataContext);
            mediaManager.AddEntityMedia(project, mediaList1);
            Assert.IsTrue(project.Media.Count() == mediaList1.Count(), "Not all medias were added to entity");
            foreach (Media media in mediaList1)
            {
                if (project.Media.FirstOrDefault(m => m.Guid == media.Guid) == null)
                {
                    Assert.Fail("Not all medias were added to entity");
                }
            }

            // Create a IMasterDataObject and add a media list to it
            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            assembly.IsMasterData = true;

            List<Media> mediaList2 = new List<Media>();
            mediaList2.Add(image.Copy());
            mediaList2.Add(document.Copy());
            mediaManager.AddEntityMedia(assembly, mediaList2);

            Assert.IsTrue(assembly.Media.Count() == mediaList2.Count(), "Not all medias were added to entity");
            foreach (Media media in mediaList2)
            {
                Media correspondingMedia = assembly.Media.FirstOrDefault(m => m.Guid == media.Guid);
                if (correspondingMedia == null)
                {
                    Assert.Fail("Not all medias were added to entity");
                }

                // Verifies that IsMasterData flag of media was set 
                Assert.AreEqual<bool>(assembly.IsMasterData, media.IsMasterData, "Failed to set IsMasterData flag");
            }
        }

        /// <summary>
        /// Tests AddEntityMedia(IEntityWithMediaCollection entityObject, List[Media] newMedia) using a null entity as
        /// input parameter.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddMediaListToNullEntity()
        {
            Media document = new Media();
            document.Type = MediaType.Document;
            document.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            document.Size = document.Content.Length;
            document.OriginalFileName = "document.txt";

            List<Media> mediaList = new List<Media>();
            mediaList.Add(document);

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MediaManager mediaManager = new MediaManager(dataContext);
            mediaManager.AddEntityMedia(null, mediaList);
        }

        /// <summary>
        /// Tests AddEntityMedia(IEntityWithMediaCollection entityObject, List[Media] newMedia) method using a null media
        /// as input parameter.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddNullMediaListToEntity()
        {
            Project project = new Project();
            project.Name = "TestProject" + DateTime.Now.Ticks;
            project.OverheadSettings = new OverheadSetting();

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            MediaManager mediaManager = new MediaManager(dataContext);
            mediaManager.AddEntityMedia(project, null);
        }

        /// <summary>
        /// A test for WriteMediaContentToFile(MediaMetaData mediaMetaData, string path) method 
        /// using valid data as input data.
        /// </summary>
        [TestMethod]
        public void WriteMediaContentToFile()
        {
            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 1, 1, 1, 1, 1 };
            image.Size = image.Content.Length;
            image.OriginalFileName = image.Guid.ToString();

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.MediaRepository.Add(image);
            dataContext.SaveChanges();

            MediaMetaData mediaMetadata = new MediaMetaData();
            mediaMetadata.Guid = image.Guid;
            mediaMetadata.Type = image.Type;
            mediaMetadata.Size = image.Size;
            mediaMetadata.IsMasterData = image.IsMasterData;
            mediaMetadata.OriginalFileName = image.OriginalFileName;

            MediaManager mediaManager = new MediaManager(dataContext);
            string path = Path.GetTempFileName();
            mediaManager.WriteMediaContentToFile(mediaMetadata, path);

            FileStream result = new FileStream(path, FileMode.Open);
            try
            {
                Assert.IsTrue(result.Length == image.Size, "Failed to write a media content to a file");
            }
            finally
            {
                // Delete the temp file
                result.Close();
                Utils.DeleteFile(path);
            }
        }

        /// <summary>
        /// A test for WriteMediaContentToFile(MediaMetaData mediaMetaData, string path) method 
        /// using an invalid file as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(BusinessException))]
        public void WriteMediaContentToInvalidFile()
        {
            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 1, 1, 1, 1, 1 };
            image.Size = image.Content.Length;
            image.OriginalFileName = image.Guid.ToString();

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.MediaRepository.Add(image);
            dataContext.SaveChanges();

            MediaMetaData mediaMetadata = new MediaMetaData();
            mediaMetadata.Guid = image.Guid;
            mediaMetadata.Type = image.Type;
            mediaMetadata.Size = image.Size;
            mediaMetadata.IsMasterData = image.IsMasterData;
            mediaMetadata.OriginalFileName = image.OriginalFileName;

            MediaManager mediaManager = new MediaManager(dataContext);
            string path = "MockPath\\";
            mediaManager.WriteMediaContentToFile(mediaMetadata, path);
        }

        #endregion Test Methods
    }
}
