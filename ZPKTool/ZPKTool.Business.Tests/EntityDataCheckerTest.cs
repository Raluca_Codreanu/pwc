﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Data;
using ZPKTool.Common;

namespace ZPKTool.Business.Tests
{
    /// <summary>
    /// This is a test class for EntityDataChecker and is intended
    /// to contain unit tests for all public methods from EntityDataChecker class.
    /// </summary>
    [TestClass]
    public class EntityDataCheckerTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EntityDataCheckerTest"/> class.
        /// </summary>
        public EntityDataCheckerTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }

        #endregion Additional test attributes

        #region Test methods

        /// <summary>
        /// Tests GetMissingPartData(Part part) method using a null part as input parameter.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetMissingPartDataOfNullPart()
        {
            EntityDataChecker.CheckPartData(null);
        }

        /// <summary>
        /// Tests GetMissingPartData(Part part) method using a valid part as input parameter -
        /// verifies that all missing data of part were returned.
        /// </summary>
        [TestMethod]
        public void GetMissingPartDataOfValidPart()
        {
            // Tests the method for a part having CalculationAccuracy set to FineCalculation and CalculateLogisticCost set to false
            Part part1 = new Part();
            part1.Name = "Test Part" + DateTime.Now.Ticks;
            part1.CalculationAccuracy = PartCalculationAccuracy.FineCalculation;
            part1.CalculateLogisticCost = false;

            var result1 = EntityDataChecker.CheckPartData(part1).FlattenErrors();
            EntityDataCheckResult part1CheckResult = result1.Where(m => m.Entity.Equals(part1)).FirstOrDefault();

            // Verifies that all part missing data were returned
            List<string> part1ExpectedNullValueProperties = new List<string>();
            part1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => part1.LifeTime));
            part1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => part1.OtherCost));
            part1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => part1.PackagingCost));
            part1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => part1.LogisticCost));
            part1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => part1.ProjectInvest));
            part1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => part1.DevelopmentCost));

            Assert.IsNotNull(part1CheckResult, "Fails to check missing data for a part");
            Assert.IsTrue(part1CheckResult.UnusedEntities.Count == 0, "Unused Entities list is not empty for a part entity");
            Assert.IsTrue(part1CheckResult.InvalidValueProperties.Count == 0, "Invalid value properties were detected for a part which has no invalid value property");
            Assert.IsTrue(part1CheckResult.NullValueProperties.Count == part1ExpectedNullValueProperties.Count, "Not all null value properties of a part were detected");
            foreach (string missingData in part1ExpectedNullValueProperties)
            {
                if (part1CheckResult.NullValueProperties.FirstOrDefault(m => string.Equals(m, missingData)) == null)
                {
                    Assert.Fail("Not all null value properties of a part were detected");
                }
            }

            // Tests the method for a part having CalculationAccuracy set to FineCalculation and CalculateLogisticCost set to true
            Part part2 = new Part();
            part2.Name = "Test Part" + DateTime.Now.Ticks;
            part2.CalculationAccuracy = PartCalculationAccuracy.FineCalculation;
            part2.CalculateLogisticCost = true;

            var result2 = EntityDataChecker.CheckPartData(part2).FlattenErrors();
            EntityDataCheckResult part2CheckResult = result2.Where(m => m.Entity.Equals(part2)).FirstOrDefault();

            // Verifies that all part missing data were returned
            List<string> part2ExpectedNullValueProperties = new List<string>();
            part2ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => part2.LifeTime));
            part2ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => part2.OtherCost));
            part2ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => part2.PackagingCost));
            part2ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => part2.ProjectInvest));
            part2ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => part2.DevelopmentCost));

            Assert.IsNotNull(part2CheckResult, "Fails to check missing data for a part");
            Assert.IsTrue(part2CheckResult.UnusedEntities.Count == 0, "Unused Entities list is not empty for a part entity");
            Assert.IsTrue(part2CheckResult.InvalidValueProperties.Count == 0, "Invalid value properties were detected for a part which has no invalid value property");
            Assert.IsTrue(part2CheckResult.NullValueProperties.Count == part2ExpectedNullValueProperties.Count, "Not all null value properties of a part were detected");
            foreach (string missingData in part2ExpectedNullValueProperties)
            {
                if (part2CheckResult.NullValueProperties.FirstOrDefault(m => string.Equals(m, missingData)) == null)
                {
                    Assert.Fail("Not all null value properties of a part were detected");
                }
            }

            // Tests the method for a part which has CalculationAccuracy set to FineCalculation and all required properties set to valid values 
            Part part3 = new Part();
            part3.Name = "Test Part" + DateTime.Now.Ticks;
            part3.CalculationAccuracy = PartCalculationAccuracy.FineCalculation;
            part3.CalculateLogisticCost = true;
            part3.OtherCost = 1;
            part3.PackagingCost = 1;
            part3.ProjectInvest = 1;
            part3.DevelopmentCost = 1;
            part3.LifeTime = 1;

            // Verifies that no missing data is returned for a part which has all required properties set to valid values 
            var result3 = EntityDataChecker.CheckPartData(part3).FlattenErrors();
            Assert.IsTrue(result3.Count == 0, "The result contains missing data for a part which has all required properties set to valid values");

            // Tests the method for a part having CalculationAccuracy set to Estimation and all required properties set to null values 
            Part part4 = new Part();
            part4.Name = "Test Part" + DateTime.Now.Ticks;
            part4.CalculationAccuracy = PartCalculationAccuracy.Estimation;

            var result4 = EntityDataChecker.CheckPartData(part4).FlattenErrors();
            EntityDataCheckResult part4CheckResult = result4.Where(m => m.Entity.Equals(part4)).FirstOrDefault();

            // Verifies that all part missing data were returned
            List<string> part4ExpectedNullValueProperties = new List<string>();
            part4ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => part4.EstimatedCost));

            Assert.IsNotNull(part4CheckResult, "Fails to check missing data for a part");
            Assert.IsTrue(part4CheckResult.UnusedEntities.Count == 0, "Unused Entities list is not empty for a part entity");
            Assert.IsTrue(part4CheckResult.InvalidValueProperties.Count == 0, "Invalid value properties were detected for a part which has no invalid value property");
            Assert.IsTrue(part4CheckResult.NullValueProperties.Count == part4ExpectedNullValueProperties.Count, "Not all null value properties of a part were detected");
            foreach (string missingData in part4ExpectedNullValueProperties)
            {
                if (part4CheckResult.NullValueProperties.FirstOrDefault(m => string.Equals(m, missingData)) == null)
                {
                    Assert.Fail("Not all null value properties of a part were detected");
                }
            }

            // Tests the method for a part which has CalculationAccuracy set to Estimation and all required properties set to valid values 
            Part part5 = new Part();
            part5.Name = "Test Part" + DateTime.Now.Ticks;
            part5.CalculationAccuracy = PartCalculationAccuracy.Estimation;
            part5.EstimatedCost = 1;

            // Verifies that no missing data is returned for a part which has all required properties set to valid values 
            var result5 = EntityDataChecker.CheckPartData(part3).FlattenErrors();
            Assert.IsTrue(result5.Count == 0, "The result contains missing data for a part which has CalculationAccuracy set to Estimation and EstimatedCost set to valid value");
        }

        /// <summary>
        /// Tests GetMissingPartData(Part part) method using a valid part with raw materials as input parameter -
        /// verifies that all missing data of raw materials were returned.
        /// </summary>
        [TestMethod]
        public void GetMissingPartDataOfPartWithRawMaterials()
        {
            // Creates a part which has CalculationAccuracy set to FineCalculation and all required properties set to valid values 
            Part part1 = new Part();
            part1.Name = "Test Part" + DateTime.Now;
            part1.CalculationAccuracy = PartCalculationAccuracy.FineCalculation;
            part1.CalculateLogisticCost = true;
            part1.OtherCost = 1;
            part1.PackagingCost = 1;
            part1.ProjectInvest = 1;
            part1.DevelopmentCost = 1;

            // Creates a Raw Material having all required properties set to null values 
            RawMaterial rawMaterial1 = new RawMaterial();
            rawMaterial1.Name = "Test Raw Material" + DateTime.Now.Ticks;
            rawMaterial1.Loss = null;
            rawMaterial1.ScrapRefundRatio = null;
            part1.RawMaterials.Add(rawMaterial1);

            // Creates a Raw Material having all required properties set to null values and IsDeleted flag set to true
            RawMaterial rawMaterial2 = new RawMaterial();
            rawMaterial2.Name = "Test Raw Material" + DateTime.Now.Ticks;
            rawMaterial2.IsDeleted = true;
            part1.RawMaterials.Add(rawMaterial2);

            // Creates a Raw Material having all required properties set to valid values
            RawMaterial rawMaterial3 = new RawMaterial();
            rawMaterial3.Name = "Test Raw Material" + DateTime.Now.Ticks;
            rawMaterial3.Price = 1;
            rawMaterial3.Quantity = 1;
            rawMaterial3.ParentWeight = 1;
            rawMaterial3.Loss = 1;
            rawMaterial3.ScrapRefundRatio = 1;
            rawMaterial3.RejectRatio = 1;
            rawMaterial3.StockKeeping = 1;
            rawMaterial3.RecyclingRatio = 1;
            part1.RawMaterials.Add(rawMaterial3);

            var result1 = EntityDataChecker.CheckPartData(part1).FlattenErrors();
            Assert.IsNotNull(result1, "Fails to check missing data for a part ");

            // Verifies returned missing data of raw material1
            EntityDataCheckResult rawMaterial1CheckResult = result1.Where(m => m.Entity.Equals(rawMaterial1)).FirstOrDefault();
            List<string> rawMaterial1ExpectedNullValueProperties = new List<string>();
            rawMaterial1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => rawMaterial1.Price));
            rawMaterial1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => rawMaterial1.Quantity));
            rawMaterial1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => rawMaterial1.ParentWeight));
            rawMaterial1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => rawMaterial1.Loss));
            rawMaterial1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => rawMaterial1.ScrapRefundRatio));
            rawMaterial1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => rawMaterial1.RejectRatio));
            rawMaterial1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => rawMaterial1.StockKeeping));
            rawMaterial1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => rawMaterial1.RecyclingRatio));

            Assert.IsNotNull(rawMaterial1CheckResult, "Fails to check missing data for a raw material");
            Assert.IsTrue(rawMaterial1CheckResult.UnusedEntities.Count == 0, "Unused Entities list is not empty for a a raw material entity");
            Assert.IsTrue(rawMaterial1CheckResult.InvalidValueProperties.Count == 0, "Invalid Value Properties list is not empty for a a raw material entity");
            Assert.IsTrue(rawMaterial1CheckResult.NullValueProperties.Count == rawMaterial1ExpectedNullValueProperties.Count, "Not all null value properties of a raw material were detected");
            foreach (string missingData in rawMaterial1ExpectedNullValueProperties)
            {
                if (rawMaterial1CheckResult.NullValueProperties.FirstOrDefault(m => string.Equals(m, missingData)) == null)
                {
                    Assert.Fail("Not all null value properties of a raw material were detected");
                }
            }

            // Verifies returned missing data of raw material2
            EntityDataCheckResult rawMaterial2CheckResult = result1.Where(m => m.Entity.Equals(rawMaterial2)).FirstOrDefault();
            Assert.IsNull(rawMaterial2CheckResult, "Some missing data were returned for a deleted raw material");

            // Verifies returned missing data of raw material3
            EntityDataCheckResult rawMaterial3CheckResult = result1.Where(m => m.Entity.Equals(rawMaterial3)).FirstOrDefault();
            Assert.IsNull(rawMaterial3CheckResult, "Some missing data were returned for a Raw Material which has no missing data");

            // Creates a part which has CalculationAccuracy set to Estimation and all required properties set to valid values 
            Part part2 = new Part();
            part2.Name = "Test Part" + DateTime.Now.Ticks;
            part2.CalculationAccuracy = PartCalculationAccuracy.Estimation;
            part2.EstimatedCost = 1;

            // Creates a Raw Material having all required properties set to null values 
            RawMaterial rawMaterial4 = new RawMaterial();
            rawMaterial4.Name = "Test Raw Material" + DateTime.Now.Ticks;
            rawMaterial4.Loss = null;
            rawMaterial4.ScrapRefundRatio = null;
            part2.RawMaterials.Add(rawMaterial4);

            // Verifies that no missing data of raw material was returned 
            var result2 = EntityDataChecker.CheckPartData(part2).FlattenErrors();
            EntityDataCheckResult rawMaterial4CheckResult = result2.Where(m => m.Entity.Equals(rawMaterial4)).FirstOrDefault();

            Assert.IsNull(rawMaterial4CheckResult, "Some missing data were returned for a raw material belonging to a part which has CalculationAccuracy set to Estimation");
        }

        /// <summary>
        /// Tests GetMissingPartData(Part part) method using a valid part with commodities as input parameter -
        /// verifies that all missing data of commodities were returned.
        /// </summary>
        [TestMethod]
        public void GetMissingPartDataOfPartWithCommodities()
        {
            // Creates a part which has CalculationAccuracy set to FineCalculation and all required properties set to valid values 
            Part part1 = new Part();
            part1.Name = "Test Part" + DateTime.Now;
            part1.CalculationAccuracy = PartCalculationAccuracy.FineCalculation;
            part1.CalculateLogisticCost = true;
            part1.OtherCost = 1;
            part1.PackagingCost = 1;
            part1.ProjectInvest = 1;
            part1.DevelopmentCost = 1;

            // Creates a Commodity having all required properties set to null values 
            Commodity commodity1 = new Commodity();
            commodity1.Name = "Test Commodity" + DateTime.Now.Ticks;
            part1.Commodities.Add(commodity1);

            // Creates a Commodity having all required properties set to null values and IsDeleted flag set to true
            Commodity commodity2 = new Commodity();
            commodity2.Name = "Test Commodity" + DateTime.Now.Ticks;
            commodity2.IsDeleted = true;
            part1.Commodities.Add(commodity2);

            // Creates a Commodity having all required properties set to valid values
            Commodity commodity3 = new Commodity();
            commodity3.Name = "Test Commodity" + DateTime.Now.Ticks;
            commodity3.Price = 1;
            commodity3.Amount = 1;

            var result1 = EntityDataChecker.CheckPartData(part1).FlattenErrors();
            Assert.IsNotNull(result1, "Fails to check missing data for a part");

            // Verifies returned missing data of commodity1
            EntityDataCheckResult commodity1CheckResult = result1.Where(m => m.Entity.Equals(commodity1)).FirstOrDefault();
            List<string> commodity1ExpectedNullValueProperties = new List<string>();
            commodity1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => commodity1.Price));
            commodity1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => commodity1.Amount));

            Assert.IsNotNull(commodity1CheckResult, "Fails to check missing data for a commodity");
            Assert.IsTrue(commodity1CheckResult.UnusedEntities.Count == 0, "Unused Entities list is not empty for a commodity entity");
            Assert.IsTrue(commodity1CheckResult.InvalidValueProperties.Count == 0, "Invalid Value Properties list is not empty for a commodity entity");
            Assert.IsTrue(commodity1CheckResult.NullValueProperties.Count == commodity1ExpectedNullValueProperties.Count, "Not all null value properties of a commodity were detected");
            foreach (string missingData in commodity1ExpectedNullValueProperties)
            {
                if (commodity1CheckResult.NullValueProperties.FirstOrDefault(m => string.Equals(m, missingData)) == null)
                {
                    Assert.Fail("Not all null value properties of a commodity were detected");
                }
            }

            // Verifies returned missing data of commodity2
            EntityDataCheckResult commodity2CheckResult = result1.Where(m => m.Entity.Equals(commodity2)).FirstOrDefault();
            Assert.IsNull(commodity2CheckResult, "Some missing data were returned for a deleted commodity");

            // Verifies returned missing data of commodity3
            EntityDataCheckResult commodity3CheckResult = result1.Where(m => m.Entity.Equals(commodity3)).FirstOrDefault();
            Assert.IsNull(commodity3CheckResult, "Some missing data were returned for a commodity which has no missing data");

            // Creates a part which has CalculationAccuracy set to Estimation and all required properties set to valid values 
            Part part2 = new Part();
            part2.Name = "Test Part" + DateTime.Now.Ticks;
            part2.CalculationAccuracy = PartCalculationAccuracy.Estimation;
            part2.EstimatedCost = 1;

            // Creates a Commodity having all required properties set to null values 
            Commodity commodity4 = new Commodity();
            commodity4.Name = "Test Commodity" + DateTime.Now.Ticks;
            part2.Commodities.Add(commodity4);

            // Verifies that no missing data of commodity was returned 
            var result2 = EntityDataChecker.CheckPartData(part2).FlattenErrors();
            EntityDataCheckResult commodity4CheckResult = result2.Where(m => m.Entity.Equals(commodity4)).FirstOrDefault();

            Assert.IsNull(commodity4CheckResult, "Some missing data were returned for a commodity belonging to a part which has CalculationAccuracy set to Estimation");
        }

        /// <summary>
        /// Tests GetMissingPartData(Part part) method using a valid part with process steps as input parameter -
        /// verifies that all missing data of process steps were returned.
        /// </summary>
        [TestMethod]
        public void GetMissingPartDataOfPartWithProcessSteps()
        {
            // Creates a part which has CalculationAccuracy set to FineCalculation and all required properties set to valid values 
            Part part1 = new Part();
            part1.Name = "Test Part" + DateTime.Now;
            part1.CalculationAccuracy = PartCalculationAccuracy.FineCalculation;
            part1.CalculateLogisticCost = true;
            part1.OtherCost = 1;
            part1.PackagingCost = 1;
            part1.ProjectInvest = 1;
            part1.DevelopmentCost = 1;

            Process process = new Process();
            part1.Process = process;

            // Creates a process step which has Accuracy set to Calculated and ExceedShiftCost set to true
            ProcessStep step1 = new PartProcessStep();
            step1.Name = "Test step" + DateTime.Now.Ticks;
            step1.Accuracy = ProcessCalculationAccuracy.Calculated;
            step1.ExceedShiftCost = true;
            step1.CycleTime = null;
            step1.BatchSize = 0;
            process.Steps.Add(step1);

            // Creates a process step which has CalculationAccuracy set to Calculated and ExceedShiftCost set to false
            ProcessStep step2 = new PartProcessStep();
            step2.Name = "Test step" + DateTime.Now.Ticks;
            step2.Accuracy = ProcessCalculationAccuracy.Calculated;
            step2.ExceedShiftCost = false;
            step2.CycleTime = 0;
            step2.BatchSize = null;
            process.Steps.Add(step2);

            // Creates a process step which has Accuracy set to Calculated and all required properties set to valid values
            ProcessStep step3 = new PartProcessStep();
            step3.Name = "Test step" + DateTime.Now.Ticks;
            step3.Accuracy = ProcessCalculationAccuracy.Calculated;
            step3.CycleTime = 1;
            step3.SetupsPerBatch = 1;
            step3.SetupTime = 1;
            step3.MaxDownTime = 1;
            step3.SetupUnskilledLabour = 1;
            step3.SetupSkilledLabour = 1;
            step3.SetupForeman = 1;
            step3.SetupTechnicians = 1;
            step3.SetupEngineers = 1;
            step3.BatchSize = 1;
            step3.ProcessTime = 1;
            step3.ProductionUnskilledLabour = 1;
            step3.ProductionSkilledLabour = 1;
            step3.ProductionForeman = 1;
            step3.ProductionTechnicians = 1;
            step3.ProductionEngineers = 1;
            step3.ScrapAmount = 1;
            step3.PartsPerCycle = 1;
            step3.ExtraShiftsNumber = 1;
            step3.ShiftCostExceedRatio = 1;
            process.Steps.Add(step3);

            // Creates a process step which has Accuracy set to Estimation and all required properties set to null values
            ProcessStep step5 = new PartProcessStep();
            step5.Name = "Test Step" + DateTime.Now.Ticks;
            step5.Accuracy = ProcessCalculationAccuracy.Estimated;
            process.Steps.Add(step5);

            // Creates a process step which has Accuracy set to Estimation and all required properties set to valid values
            ProcessStep step6 = new PartProcessStep();
            step6.Name = "Test Step" + DateTime.Now.Ticks;
            step6.Accuracy = ProcessCalculationAccuracy.Estimated;
            step6.Price = 1;
            process.Steps.Add(step6);

            var result1 = EntityDataChecker.CheckPartData(part1).FlattenErrors();
            Assert.IsNotNull(result1.Count >= 1, "Fails to check missing data for a part");

            // Verifies returned missing data of step1
            EntityDataCheckResult step1CheckResult = result1.Where(m => m.Entity.Equals(step1)).FirstOrDefault();
            List<string> step1ExpectedNullValueProperties = new List<string>();
            step1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step1.CycleTime));
            step1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step1.SetupsPerBatch));
            step1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step1.SetupTime));
            step1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step1.MaxDownTime));
            step1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step1.SetupUnskilledLabour));
            step1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step1.SetupSkilledLabour));
            step1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step1.SetupForeman));
            step1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step1.SetupTechnicians));
            step1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step1.SetupEngineers));
            step1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step1.ProcessTime));
            step1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step1.ProductionUnskilledLabour));
            step1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step1.ProductionSkilledLabour));
            step1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step1.ProductionForeman));
            step1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step1.ProductionTechnicians));
            step1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step1.ProductionEngineers));
            step1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step1.ScrapAmount));
            step1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step1.PartsPerCycle));
            step1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step1.ExtraShiftsNumber));
            step1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step1.ShiftCostExceedRatio));

            List<string> step1ExpectedInvalidValueProperties = new List<string>();
            step1ExpectedInvalidValueProperties.Add(ReflectionUtils.GetPropertyName(() => step1.BatchSize));

            Assert.IsNotNull(step1CheckResult, "Fails to check missing data for a process step");
            Assert.IsTrue(step1CheckResult.UnusedEntities.Count == 0, "Unused Entities list is not empty for a process step entity");
            Assert.IsTrue(step1CheckResult.InvalidValueProperties.Count == step1ExpectedInvalidValueProperties.Count, "Not all invalid value properties were detected for a process step");
            foreach (string missingData in step1ExpectedInvalidValueProperties)
            {
                if (step1CheckResult.InvalidValueProperties.FirstOrDefault(m => string.Equals(m, missingData)) == null)
                {
                    Assert.Fail("Not all invalid value properties of a process step were detected");
                }
            }

            Assert.IsTrue(step1CheckResult.NullValueProperties.Count == step1ExpectedNullValueProperties.Count, "Not all null value properties of a process step were detected");
            foreach (string missingData in step1ExpectedNullValueProperties)
            {
                if (step1CheckResult.NullValueProperties.FirstOrDefault(m => string.Equals(m, missingData)) == null)
                {
                    Assert.Fail("Not all null value properties of a process step were detected");
                }
            }

            // Verifies returned missing data of step2
            EntityDataCheckResult step2CheckResult = result1.Where(m => m.Entity.Equals(step2)).FirstOrDefault();
            List<string> step2ExpectedNullValueProperties = new List<string>();
            step2ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step2.BatchSize));
            step2ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step2.SetupsPerBatch));
            step2ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step2.SetupTime));
            step2ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step2.MaxDownTime));
            step2ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step2.SetupUnskilledLabour));
            step2ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step2.SetupSkilledLabour));
            step2ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step2.SetupForeman));
            step2ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step2.SetupTechnicians));
            step2ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step2.SetupEngineers));
            step2ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step2.ProcessTime));
            step2ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step2.ProductionUnskilledLabour));
            step2ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step2.ProductionSkilledLabour));
            step2ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step2.ProductionForeman));
            step2ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step2.ProductionTechnicians));
            step2ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step2.ProductionEngineers));
            step2ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step2.ScrapAmount));
            step2ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step2.PartsPerCycle));

            List<string> step2ExpectedInvalidValueProperties = new List<string>();
            step2ExpectedInvalidValueProperties.Add(ReflectionUtils.GetPropertyName(() => step1.CycleTime));

            Assert.IsNotNull(step2CheckResult, "Fails to check missing data for a process step");
            Assert.IsTrue(step2CheckResult.UnusedEntities.Count == 0, "Unused Entities list is not empty for a process step entity");
            Assert.IsTrue(step2CheckResult.InvalidValueProperties.Count == step2ExpectedInvalidValueProperties.Count, "Not all invalid value properties were detected for a process step");
            foreach (string missingData in step2ExpectedInvalidValueProperties)
            {
                if (step2CheckResult.InvalidValueProperties.FirstOrDefault(m => string.Equals(m, missingData)) == null)
                {
                    Assert.Fail("Not all invalid value properties of a process step were detected");
                }
            }

            Assert.IsTrue(step2CheckResult.NullValueProperties.Count == step2ExpectedNullValueProperties.Count, "Not all null value properties of a process step were detected");
            foreach (string missingData in step2ExpectedNullValueProperties)
            {
                if (step2CheckResult.NullValueProperties.FirstOrDefault(m => string.Equals(m, missingData)) == null)
                {
                    Assert.Fail("Not all null value properties of a process step were detected");
                }
            }

            // Verifies returned missing data of step4 - no missing data should be returned because the step has all required properties set to valid values
            EntityDataCheckResult step4CheckResult = result1.Where(m => m.Entity.Equals(step3)).FirstOrDefault();
            Assert.IsNull(step4CheckResult, "Some missing data were returned for process step which has all required properties set to valid values");

            // Verifies returned missing data of step5 - step with Accuracy set to Estimated
            EntityDataCheckResult step5CheckResult = result1.Where(m => m.Entity.Equals(step5)).FirstOrDefault();
            List<string> step5ExpectedNullValueProperties = new List<string>();
            step5ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step5.Price));

            Assert.IsNotNull(step5CheckResult, "Fails to check missing data for a process step");
            Assert.IsTrue(step5CheckResult.UnusedEntities.Count == 0, "Unused Entities list is not empty for a process step entity");
            Assert.IsTrue(step5CheckResult.InvalidValueProperties.Count == 0, "Invalid Value Properties list is not empty for a process step which has Accuracy set to Estimated");
            Assert.IsTrue(step5CheckResult.NullValueProperties.Count == step5ExpectedNullValueProperties.Count, "Not all null value properties of a process step were detected");
            foreach (string missingData in step5ExpectedNullValueProperties)
            {
                if (step5CheckResult.NullValueProperties.FirstOrDefault(m => string.Equals(m, missingData)) == null)
                {
                    Assert.Fail("Not all null value properties of a process step were detected");
                }
            }

            // Verifies returned missing data of step6 - step with Accuracy set to Estimated and all required properties set to valid values
            EntityDataCheckResult step6CheckResult = result1.Where(m => m.Entity.Equals(step6)).FirstOrDefault();
            Assert.IsNull(step6CheckResult, "Some missing data were returned for process step which has all required properties set to valid values");

            // Creates a part which has CalculationAccuracy set to Estimation and all required properties set to valid values 
            Part part2 = new Part();
            part2.Name = "Test Part" + DateTime.Now;
            part2.CalculationAccuracy = PartCalculationAccuracy.Estimation;
            part2.EstimatedCost = 1;

            // Creates a process step which has all required properties set to null
            Process process2 = new Process();
            ProcessStep step7 = new PartProcessStep();
            step7.Name = "Test Step" + DateTime.Now.Ticks;
            process2.Steps.Add(step7);
            part2.Process = process2;

            // Verifies returned missing data of step7 - no missing data should be returned because the part2 has CalculationAccuracy set to Estimation
            var result2 = EntityDataChecker.CheckPartData(part2).FlattenErrors();
            EntityDataCheckResult step7CheckResult = result2.Where(m => m.Entity.Equals(step7)).FirstOrDefault();
            Assert.IsNull(step7CheckResult, "Some missing data was returned for a process step belonging to a part which has CalculationAccuracy set to Estimation");
        }

        /// <summary>
        /// Tests GetMissingPartData(Part part) method using a valid part with machines as input parameter -
        /// verifies that all missing data of machines were returned.
        /// </summary>
        [TestMethod]
        public void GetMissingPartDataOfPartWithMachines()
        {
            // Creates a part which has CalculationAccuracy set to FineCalculation and all required properties set to valid values 
            Part part1 = new Part();
            part1.Name = "Test Part" + DateTime.Now;
            part1.CalculationAccuracy = PartCalculationAccuracy.FineCalculation;
            part1.CalculateLogisticCost = true;
            part1.OtherCost = 1;
            part1.PackagingCost = 1;
            part1.ProjectInvest = 1;
            part1.DevelopmentCost = 1;

            Process process = new Process();
            part1.Process = process;

            // Creates a process step which has Accuracy set to Calculated and all required properties set to valid values
            ProcessStep step1 = new PartProcessStep();
            step1.Name = "Test step" + DateTime.Now.Ticks;
            step1.Accuracy = (short)ProcessCalculationAccuracy.Calculated;
            step1.CycleTime = 1;
            step1.SetupsPerBatch = 1;
            step1.SetupTime = 1;
            step1.MaxDownTime = 1;
            step1.SetupUnskilledLabour = 1;
            step1.SetupSkilledLabour = 1;
            step1.SetupForeman = 1;
            step1.SetupTechnicians = 1;
            step1.SetupEngineers = 1;
            step1.BatchSize = 1;
            step1.ProcessTime = 1;
            step1.ProductionUnskilledLabour = 1;
            step1.ProductionSkilledLabour = 1;
            step1.ProductionForeman = 1;
            step1.ProductionTechnicians = 1;
            step1.ProductionEngineers = 1;
            step1.ScrapAmount = 1;
            step1.ExtraShiftsNumber = 1;
            step1.ShiftCostExceedRatio = 1;
            process.Steps.Add(step1);

            // Creates a machine which has 'CalculateWitkKValue' property set to true and all required properties set to null values
            Machine machine1 = new Machine();
            machine1.Name = "Test Machine" + DateTime.Now.Ticks;
            machine1.CalculateWithKValue = true;
            machine1.KValue = null;
            step1.Machines.Add(machine1);

            // Creates a machine which has 'CalculateWitkKValue' property set to false and all required properties set to null values
            Machine machine2 = new Machine();
            machine2.Name = "Test Machine" + DateTime.Now.Ticks;
            machine2.CalculateWithKValue = false;
            step1.Machines.Add(machine2);

            // Creates a machine which has 'IsDeleted' flag set to true
            Machine machine3 = new Machine();
            machine3.Name = "Test Machine" + DateTime.Now.Ticks;
            machine3.IsDeleted = true;
            step1.Machines.Add(machine3);

            // Creates a machine which has all required properties set to valid values
            Machine machine4 = new Machine();
            machine4.CalculateWithKValue = true;
            machine4.KValue = 1;
            machine4.MachineInvestment = 1;
            step1.Machines.Add(machine4);

            // Creates a process step which has 'Accuracy' set to 'Estimated'
            ProcessStep step2 = new PartProcessStep();
            step2.Name = "Test Step" + DateTime.Now.Ticks;
            step2.Accuracy = ProcessCalculationAccuracy.Estimated;
            step2.Price = 1;
            process.Steps.Add(step2);

            // Creates a machine which has all required properties set to null values
            Machine machine5 = new Machine();
            machine5.Name = "Test Machine" + DateTime.Now.Ticks;
            step2.Machines.Add(machine5);

            var result1 = EntityDataChecker.CheckPartData(part1).FlattenErrors();
            Assert.IsNotNull(result1, "Fails to check missing data for a part");

            // Verifies returned missing data of machine1
            EntityDataCheckResult machine1CheckResult = result1.Where(m => m.Entity.Equals(machine1)).FirstOrDefault();
            List<string> machine1ExpectedNullValueProperties = new List<string>();
            machine1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => machine1.KValue));
            machine1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => machine1.MachineInvestment));

            Assert.IsNotNull(machine1CheckResult, "Fails to check missing data for a machine");
            Assert.IsTrue(machine1CheckResult.UnusedEntities.Count == 0, "Unused Entities list is not empty for a machine entity");
            Assert.IsTrue(machine1CheckResult.InvalidValueProperties.Count == 0, "Invalid Value Properties list is not empty for a machine entity");
            Assert.IsTrue(machine1CheckResult.NullValueProperties.Count == machine1ExpectedNullValueProperties.Count, "Not all null value properties of a machine were detected");
            foreach (string missingData in machine1ExpectedNullValueProperties)
            {
                if (machine1CheckResult.NullValueProperties.FirstOrDefault(m => string.Equals(m, missingData)) == null)
                {
                    Assert.Fail("Not all null value properties of a machine were detected");
                }
            }

            // Verifies returned missing data of machine2
            EntityDataCheckResult machine2CheckResult = result1.Where(m => m.Entity.Equals(machine2)).FirstOrDefault();
            List<string> machine2ExpectedNullValueProperties = new List<string>();
            machine2ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => machine2.ConsumablesCost));
            machine2ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => machine2.RepairsCost));
            machine2ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => machine2.MaterialCost));
            machine2ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => machine2.ExternalWorkCost));

            Assert.IsNotNull(machine2CheckResult, "Fails to check missing data for a machine");
            Assert.IsTrue(machine2CheckResult.UnusedEntities.Count == 0, "Unused Entities list is not empty for a machine entity");
            Assert.IsTrue(machine2CheckResult.InvalidValueProperties.Count == 0, "Invalid Value Properties list is not empty for a machine entity");
            Assert.IsTrue(machine2CheckResult.NullValueProperties.Count == machine2ExpectedNullValueProperties.Count, "Not all null value properties of a machine were detected");
            foreach (string missingData in machine2ExpectedNullValueProperties)
            {
                if (machine2CheckResult.NullValueProperties.FirstOrDefault(m => string.Equals(m, missingData)) == null)
                {
                    Assert.Fail("Not all null value properties of a machine were detected");
                }
            }

            // Verifies returned missing data of machine3
            EntityDataCheckResult machine3CheckResult = result1.Where(m => m.Entity.Equals(machine3)).FirstOrDefault();
            Assert.IsNull(machine3CheckResult, "Some missing datas were returned for a deleted machine");

            // Verifies returned missing data of machine4
            EntityDataCheckResult machine4CheckResult = result1.Where(m => m.Entity.Equals(machine4)).FirstOrDefault();
            Assert.IsNull(machine4CheckResult, "Some missing datas were returned for a machine which has all required properties set to valid values");

            // Verifies returned missing data of machine5
            EntityDataCheckResult machine5CheckResult = result1.Where(m => m.Entity.Equals(machine5)).FirstOrDefault();
            Assert.IsNull(machine5CheckResult, "Some missing datas were returned for a machine belonging to a process step having 'Accuracy' set to Estimated");
        }

        /// <summary>
        /// Tests GetMissingPartData(Part part) method using a valid part with consumables as input parameter -
        /// verifies that all consumables missing datas were returned.
        /// </summary>
        [TestMethod]
        public void GetMissingPartDataOfPartWithConsumables()
        {
            // Creates a part which has CalculationAccuracy set to FineCalculation and all required properties set to valid values 
            Part part1 = new Part();
            part1.Name = "Test Part" + DateTime.Now;
            part1.CalculationAccuracy = PartCalculationAccuracy.FineCalculation;
            part1.CalculateLogisticCost = true;
            part1.OtherCost = 1;
            part1.PackagingCost = 1;
            part1.ProjectInvest = 1;
            part1.DevelopmentCost = 1;

            Process process = new Process();
            part1.Process = process;

            // Creates a process step which has Accuracy set to Calculated and all required properties set to valid values
            ProcessStep step1 = new PartProcessStep();
            step1.Name = "Test step" + DateTime.Now.Ticks;
            step1.Accuracy = ProcessCalculationAccuracy.Calculated;
            step1.CycleTime = 1;
            step1.SetupsPerBatch = 1;
            step1.SetupTime = 1;
            step1.MaxDownTime = 1;
            step1.SetupUnskilledLabour = 1;
            step1.SetupSkilledLabour = 1;
            step1.SetupForeman = 1;
            step1.SetupTechnicians = 1;
            step1.SetupEngineers = 1;
            step1.BatchSize = 1;
            step1.ProcessTime = 1;
            step1.ProductionUnskilledLabour = 1;
            step1.ProductionSkilledLabour = 1;
            step1.ProductionForeman = 1;
            step1.ProductionTechnicians = 1;
            step1.ProductionEngineers = 1;
            step1.ScrapAmount = 1;
            step1.ExtraShiftsNumber = 1;
            step1.ShiftCostExceedRatio = 1;
            process.Steps.Add(step1);

            // Creates a Consumable having all required properties set to null values 
            Consumable consumable1 = new Consumable();
            consumable1.Name = "Test Consumable" + DateTime.Now.Ticks;
            step1.Consumables.Add(consumable1);

            // Creates a Consumable having all required properties set to null values and IsDeleted flag set to true
            Consumable consumable2 = new Consumable();
            consumable2.Name = "Test Consumable" + DateTime.Now.Ticks;
            consumable2.IsDeleted = true;
            step1.Consumables.Add(consumable2);

            // Creates a Consumable having all required properties set to valid values
            Consumable consumable3 = new Consumable();
            consumable3.Name = "Test Consumable" + DateTime.Now.Ticks;
            consumable3.PriceUnitBase = new MeasurementUnit();
            consumable3.Amount = 1;
            consumable3.Price = 1;
            step1.Consumables.Add(consumable3);

            // Creates a process step which has 'Accuracy' set to 'Estimated'
            ProcessStep step2 = new PartProcessStep();
            step2.Name = "Test Step" + DateTime.Now.Ticks;
            step2.Accuracy = ProcessCalculationAccuracy.Estimated;
            step2.Price = 1;
            process.Steps.Add(step2);

            // Creates a Consumable which has all required properties set to null values
            Consumable consumable4 = new Consumable();
            consumable4.Name = "Test Consumable" + DateTime.Now.Ticks;
            step2.Consumables.Add(consumable4);

            var result1 = EntityDataChecker.CheckPartData(part1).FlattenErrors();
            Assert.IsNotNull(result1, "Fails to check missing data for a part");

            // Verifies returned missing data of consumable1
            EntityDataCheckResult consumable1CheckResult = result1.Where(m => m.Entity.Equals(consumable1)).FirstOrDefault();
            List<string> consumable1ExpectedNullValueProperties = new List<string>();
            consumable1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => consumable1.PriceUnitBase));
            consumable1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => consumable1.Amount));
            consumable1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => consumable1.Price));

            Assert.IsNotNull(consumable1CheckResult, "Fails to check missing data for a consumable");
            Assert.IsTrue(consumable1CheckResult.UnusedEntities.Count == 0, "Unused Entities list is not empty for a consumable entity");
            Assert.IsTrue(consumable1CheckResult.InvalidValueProperties.Count == 0, "Invalid Value Properties list is not empty for a consumable entity");
            Assert.IsTrue(consumable1CheckResult.NullValueProperties.Count == consumable1ExpectedNullValueProperties.Count, "Not all null value properties of a consumable were detected");
            foreach (string missingData in consumable1ExpectedNullValueProperties)
            {
                if (consumable1CheckResult.NullValueProperties.FirstOrDefault(m => string.Equals(m, missingData)) == null)
                {
                    Assert.Fail("Not all null value properties of a consumable were detected");
                }
            }

            // Verifies returned missing data of consumable2
            EntityDataCheckResult consumable2CheckResult = result1.Where(m => m.Entity.Equals(consumable2)).FirstOrDefault();
            Assert.IsNull(consumable2CheckResult, "Some missing datas were returned for a deleted consumable");

            // Verifies returned missing data of consumable3
            EntityDataCheckResult consumable3CheckResult = result1.Where(m => m.Entity.Equals(consumable3)).FirstOrDefault();
            Assert.IsNull(consumable3CheckResult, "Some missing datas were returned for a consumable which has all required properties set to valid values");

            // Verifies returned missing data of consumable4
            EntityDataCheckResult consumable4CheckResult = result1.Where(m => m.Entity.Equals(consumable4)).FirstOrDefault();
            Assert.IsNull(consumable4CheckResult, "Some missing datas were returned for a consumable belonging to a process step having 'Accuracy' set to Estimated");
        }

        /// <summary>
        /// Tests GetMissingPartData(Part part) method using a valid part with dies as input parameter -
        /// verifies that all dies missing datas were returned.
        /// </summary>
        [TestMethod]
        public void GetMissingPartDataOfPartWithDies()
        {
            // Creates a part which has CalculationAccuracy set to FineCalculation and all required properties set to valid values 
            Part part1 = new Part();
            part1.Name = "Test Part" + DateTime.Now;
            part1.CalculationAccuracy = PartCalculationAccuracy.FineCalculation;
            part1.CalculateLogisticCost = true;
            part1.OtherCost = 1;
            part1.PackagingCost = 1;
            part1.ProjectInvest = 1;
            part1.DevelopmentCost = 1;

            Process process = new Process();
            part1.Process = process;

            // Creates a process step which has Accuracy set to Calculated and all required properties set to valid values
            ProcessStep step1 = new PartProcessStep();
            step1.Name = "Test step" + DateTime.Now.Ticks;
            step1.Accuracy = ProcessCalculationAccuracy.Calculated;
            step1.CycleTime = 1;
            step1.SetupsPerBatch = 1;
            step1.SetupTime = 1;
            step1.MaxDownTime = 1;
            step1.SetupUnskilledLabour = 1;
            step1.SetupSkilledLabour = 1;
            step1.SetupForeman = 1;
            step1.SetupTechnicians = 1;
            step1.SetupEngineers = 1;
            step1.BatchSize = 1;
            step1.ProcessTime = 1;
            step1.ProductionUnskilledLabour = 1;
            step1.ProductionSkilledLabour = 1;
            step1.ProductionForeman = 1;
            step1.ProductionTechnicians = 1;
            step1.ProductionEngineers = 1;
            step1.ScrapAmount = 1;
            step1.ExtraShiftsNumber = 1;
            step1.ShiftCostExceedRatio = 1;
            process.Steps.Add(step1);

            // Creates a die which has 'CostAllocationBasedOnPartsPerLifeTime ' property set to true and all required properties set to null values
            Die die1 = new Die();
            die1.Name = "Test Die" + DateTime.Now.Ticks;
            die1.CostAllocationBasedOnPartsPerLifeTime = false;
            step1.Dies.Add(die1);

            // Creates a die which has 'CostAllocationBasedOnPartsPerLifeTime' property set to false and all required properties set to null values
            Die die2 = new Die();
            die2.Name = "Test Die" + DateTime.Now.Ticks;
            die2.CostAllocationBasedOnPartsPerLifeTime = false;
            step1.Dies.Add(die2);

            // Creates a die which has 'IsDeleted' flag set to true
            Die die3 = new Die();
            die3.Name = "Test Die" + DateTime.Now.Ticks;
            die3.IsDeleted = true;
            step1.Dies.Add(die3);

            // Creates a die which has all required properties set to valid values
            Die die4 = new Die();
            die4.CostAllocationBasedOnPartsPerLifeTime = false;
            die4.LifeTime = 1;
            die4.DiesetsNumberPaidByCustomer = 1;
            die4.Investment = 1;
            die4.AllocationRatio = 1;
            die4.Wear = 1;
            die4.Maintenance = 1;
            die4.CostAllocationNumberOfParts = 1;
            step1.Dies.Add(die4);

            // Creates a process step which has 'Accuracy' set to 'Estimated'
            ProcessStep step2 = new PartProcessStep();
            step2.Name = "Test Step" + DateTime.Now.Ticks;
            step2.Accuracy = ProcessCalculationAccuracy.Estimated;
            step2.Price = 1;
            process.Steps.Add(step2);

            // Creates a die which has all required properties set to null values
            Die die5 = new Die();
            die5.Name = "Test Die" + DateTime.Now.Ticks;
            step2.Dies.Add(die5);

            var result1 = EntityDataChecker.CheckPartData(part1).FlattenErrors();
            Assert.IsNotNull(result1, "Fails to check missing data for a part");

            // Verifies returned missing data of die1
            EntityDataCheckResult die1CheckResult = result1.Where(m => m.Entity.Equals(die1)).FirstOrDefault();
            List<string> die1ExpectedNullValueProperties = new List<string>();
            die1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => die1.LifeTime));
            die1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => die1.DiesetsNumberPaidByCustomer));
            die1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => die1.Investment));
            die1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => die1.AllocationRatio));
            die1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => die1.Wear));
            die1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => die1.Maintenance));
            die1ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => die1.CostAllocationNumberOfParts));

            Assert.IsNotNull(die1CheckResult, "Fails to check missing data for a die");
            Assert.IsTrue(die1CheckResult.UnusedEntities.Count == 0, "Unused Entities list is not empty for a die entity");
            Assert.IsTrue(die1CheckResult.InvalidValueProperties.Count == 0, "Invalid Value Properties list is not empty for a die entity");
            Assert.IsTrue(die1CheckResult.NullValueProperties.Count == die1ExpectedNullValueProperties.Count, "Not all null value properties of a die were detected");
            foreach (string missingData in die1ExpectedNullValueProperties)
            {
                if (die1CheckResult.NullValueProperties.FirstOrDefault(m => string.Equals(m, missingData)) == null)
                {
                    Assert.Fail("Not all null value properties of a die were detected");
                }
            }

            // Verifies returned missing data of die2
            EntityDataCheckResult die2CheckResult = result1.Where(m => m.Entity.Equals(die2)).FirstOrDefault();
            List<string> die2ExpectedNullValueProperties = new List<string>();
            die2ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => die2.LifeTime));
            die2ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => die2.DiesetsNumberPaidByCustomer));
            die2ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => die2.Investment));
            die2ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => die2.AllocationRatio));
            die2ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => die2.Wear));
            die2ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => die2.Maintenance));
            die2ExpectedNullValueProperties.Add(ReflectionUtils.GetPropertyName(() => die2.CostAllocationNumberOfParts));

            Assert.IsNotNull(die1CheckResult, "Fails to check missing data for a die");
            Assert.IsTrue(die2CheckResult.UnusedEntities.Count == 0, "Unused Entities list is not empty for a die entity");
            Assert.IsTrue(die2CheckResult.InvalidValueProperties.Count == 0, "Invalid Value Properties list is not empty for a die entity");
            Assert.IsTrue(die2CheckResult.NullValueProperties.Count == die2ExpectedNullValueProperties.Count, "Not all null value properties of a die were detected");
            foreach (string missingData in die2ExpectedNullValueProperties)
            {
                if (die2CheckResult.NullValueProperties.FirstOrDefault(m => string.Equals(m, missingData)) == null)
                {
                    Assert.Fail("Not all null value properties of a die were detected");
                }
            }

            // Verifies returned missing data of die3
            EntityDataCheckResult die3CheckResult = result1.Where(m => m.Entity.Equals(die3)).FirstOrDefault();
            Assert.IsNull(die3CheckResult, "Some missing data were returned for a deleted die");

            // Verifies returned missing data of die4
            EntityDataCheckResult die4CheckResult = result1.Where(m => m.Entity.Equals(die4)).FirstOrDefault();
            Assert.IsNull(die4CheckResult, "Some missing data were returned for a die which has all required properties set to valid values");

            // Verifies returned missing data of die4
            EntityDataCheckResult die5CheckResult = result1.Where(m => m.Entity.Equals(die5)).FirstOrDefault();
            Assert.IsNull(die5CheckResult, "Some missing data were returned for a die belonging to a process step having 'Accuracy' set to Estimated");
        }

        /// <summary>
        /// Tests GetMissingAssemblyData(Assembly assembly) method using a null assembly as input parameter.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void CheckAssemblyDataWithNullAssembly()
        {
            EntityDataChecker.CheckAssemblyData(null);
        }

        /// <summary>
        /// Tests GetMissingAssemblyData(Assembly assembly) method using a valid assembly as input parameter - 
        /// an assembly having all required properties values set to null.
        /// </summary>
        [TestMethod]
        public void CheckAssemblyDataWithValidAssembly1()
        {
            Assembly assembly = new Assembly();
            assembly.Name = "Test Assembly" + DateTime.Now.Ticks;
            assembly.CalculateLogisticCost = false;

            Process process = new Process();
            ProcessStep step = new AssemblyProcessStep();
            step.Name = "Test Step" + DateTime.Now.Ticks;
            step.ExceedShiftCost = true;
            step.CycleTime = 0;
            process.Steps.Add(step);
            assembly.Process = process;

            Machine machine = new Machine();
            machine.Name = "Test Machine" + DateTime.Now.Ticks;
            machine.CalculateWithKValue = false;
            step.Machines.Add(machine);

            Consumable consumable = new Consumable();
            consumable.Name = "Test Consumable" + DateTime.Now.Ticks;
            step.Consumables.Add(consumable);

            Commodity commodity = new Commodity();
            commodity.Name = "Test Commodity" + DateTime.Now.Ticks;
            step.Commodities.Add(commodity);

            Die die = new Die();
            die.Name = "Test Die" + DateTime.Now.Ticks;
            step.Dies.Add(die);

            Part part = new Part();
            part.Name = "Test Part" + DateTime.Now.Ticks;
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            part.CalculateLogisticCost = false;
            assembly.Parts.Add(part);

            Assembly subassembly = new Assembly();
            subassembly.Name = "Subassembly" + DateTime.Now.Ticks;
            subassembly.CalculateLogisticCost = false;
            assembly.Subassemblies.Add(subassembly);

            var result1 = EntityDataChecker.CheckAssemblyData(assembly).FlattenErrors();
            EntityDataCheckResult assemblyCheckResult = result1.Where(m => m.Entity.Equals(assembly)).FirstOrDefault();
            EntityDataCheckResult partCheckResult = result1.Where(m => m.Entity.Equals(part)).FirstOrDefault();
            EntityDataCheckResult subassemblyCheckResult = result1.Where(m => m.Entity.Equals(subassembly)).FirstOrDefault();
            EntityDataCheckResult processStepCheckResult = result1.Where(m => m.Entity.Equals(step)).FirstOrDefault();
            EntityDataCheckResult machineCheckResult = result1.Where(m => m.Entity.Equals(machine)).FirstOrDefault();
            EntityDataCheckResult consumableCheckResult = result1.Where(m => m.Entity.Equals(consumable)).FirstOrDefault();
            EntityDataCheckResult commodityCheckResult = result1.Where(m => m.Entity.Equals(commodity)).FirstOrDefault();
            EntityDataCheckResult dieCheckResult = result1.Where(m => m.Entity.Equals(die)).FirstOrDefault();

            // Verifies that all missing data of assembly were returned
            List<string> assemblyNullValuePropertiesExpected = new List<string>();
            assemblyNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => assembly.LifeTime));
            assemblyNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => assembly.OtherCost));
            assemblyNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => assembly.PackagingCost));
            assemblyNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => assembly.LogisticCost));
            assemblyNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => assembly.ProjectInvest));
            assemblyNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => assembly.DevelopmentCost));

            List<object> assemblyUnusedEntitiesExpected = new List<object>();
            assemblyUnusedEntitiesExpected.Add(part);
            assemblyUnusedEntitiesExpected.Add(subassembly);

            Assert.IsNotNull(assemblyCheckResult, "Fails to check missing data for an assembly");
            Assert.IsTrue(assemblyCheckResult.UnusedEntities.Count == assemblyUnusedEntitiesExpected.Count, "Not all unused entities of an assembly were returned");
            foreach (object unusedEntity in assemblyUnusedEntitiesExpected)
            {
                if (assemblyCheckResult.UnusedEntities.FirstOrDefault(e => e.Equals(part) || e.Equals(subassembly)) == null)
                {
                    Assert.Fail("Not all unused entities of an assembly were returned");
                }
            }

            Assert.IsTrue(assemblyCheckResult.InvalidValueProperties.Count == 0, "Invalid value properties were detected for an assembly which has no invalid value property");
            Assert.IsTrue(assemblyCheckResult.NullValueProperties.Count == assemblyNullValuePropertiesExpected.Count, "Not all null value properties of an assembly were detected");
            foreach (string missingData in assemblyNullValuePropertiesExpected)
            {
                if (assemblyCheckResult.NullValueProperties.FirstOrDefault(m => string.Equals(m, missingData)) == null)
                {
                    Assert.Fail("Not all null value properties of an assembly were detected");
                }
            }

            // Verifies that all missing datas of part were returned
            List<string> partNullValuePropertiesExpected = new List<string>();
            partNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => part.LifeTime));
            partNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => part.OtherCost));
            partNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => part.PackagingCost));
            partNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => part.LogisticCost));
            partNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => part.ProjectInvest));
            partNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => part.DevelopmentCost));

            Assert.IsNotNull(partCheckResult, "Fails to check missing data for a part");
            Assert.IsTrue(partCheckResult.UnusedEntities.Count == 0, "Unused Entities list is not empty for a part entity");
            Assert.IsTrue(partCheckResult.InvalidValueProperties.Count == 0, "Invalid value properties were detected for a part which has no invalid value property");
            Assert.IsTrue(partCheckResult.NullValueProperties.Count == partNullValuePropertiesExpected.Count, "Not all null value properties of a part were detected");
            foreach (string missingData in partNullValuePropertiesExpected)
            {
                if (partCheckResult.NullValueProperties.FirstOrDefault(m => string.Equals(m, missingData)) == null)
                {
                    Assert.Fail("Not all null value properties of a part were detected");
                }
            }

            // Verifies that all missing datas of subassembly were returned
            List<string> subassemblyNullValuePropertiesExpected = new List<string>();
            subassemblyNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => subassembly.LifeTime));
            subassemblyNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => subassembly.OtherCost));
            subassemblyNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => subassembly.PackagingCost));
            subassemblyNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => subassembly.LogisticCost));
            subassemblyNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => subassembly.ProjectInvest));
            subassemblyNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => subassembly.DevelopmentCost));

            Assert.IsNotNull(subassemblyCheckResult, "Fails to check missing data for a subassembly");
            Assert.IsTrue(subassemblyCheckResult.UnusedEntities.Count == 0, "Unused Entities list is not empty for a subassembly which has no unused entity");
            Assert.IsTrue(subassemblyCheckResult.InvalidValueProperties.Count == 0, "Invalid value properties were detected for a subassembly which has no invalid value property");
            Assert.IsTrue(subassemblyCheckResult.NullValueProperties.Count == subassemblyNullValuePropertiesExpected.Count, "Not all null value properties of a subassembly were detected");
            foreach (string missingData in subassemblyNullValuePropertiesExpected)
            {
                if (subassemblyCheckResult.NullValueProperties.FirstOrDefault(m => string.Equals(m, missingData)) == null)
                {
                    Assert.Fail("Not all null value properties of an assembly were detected");
                }
            }

            // Verifies that all missing datas of process step were returned
            List<string> stepInvalidValuePropertiesExpected = new List<string>();
            stepInvalidValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => step.CycleTime));

            List<string> stepNullValuePropertiesExpected = new List<string>();
            stepNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => step.BatchSize));
            stepNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => step.SetupsPerBatch));
            stepNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => step.SetupTime));
            stepNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => step.MaxDownTime));
            stepNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => step.SetupUnskilledLabour));
            stepNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => step.SetupSkilledLabour));
            stepNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => step.SetupForeman));
            stepNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => step.SetupTechnicians));
            stepNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => step.SetupEngineers));
            stepNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => step.ProcessTime));
            stepNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => step.ProductionUnskilledLabour));
            stepNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => step.ProductionSkilledLabour));
            stepNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => step.ProductionForeman));
            stepNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => step.ProductionTechnicians));
            stepNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => step.ProductionEngineers));
            stepNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => step.ScrapAmount));
            stepNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => step.ExtraShiftsNumber));
            stepNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => step.ShiftCostExceedRatio));
            stepNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => step.PartsPerCycle));

            Assert.IsNotNull(processStepCheckResult, "Fails to check missing data for a process step");
            Assert.IsTrue(processStepCheckResult.UnusedEntities.Count == 0, "Unused Entities list is not empty for a process step entity");
            Assert.IsTrue(processStepCheckResult.InvalidValueProperties.Count == stepInvalidValuePropertiesExpected.Count, "Not all invalid value properties were detected for a process step");
            foreach (string missingData in stepInvalidValuePropertiesExpected)
            {
                if (processStepCheckResult.InvalidValueProperties.FirstOrDefault(m => string.Equals(m, missingData)) == null)
                {
                    Assert.Fail("Not all invalid value properties of a process step were detected");
                }
            }

            Assert.IsTrue(processStepCheckResult.NullValueProperties.Count == stepNullValuePropertiesExpected.Count, "Not all null value properties of a process step were detected");
            foreach (string missingData in stepNullValuePropertiesExpected)
            {
                if (processStepCheckResult.NullValueProperties.FirstOrDefault(m => string.Equals(m, missingData)) == null)
                {
                    Assert.Fail("Not all null value properties of a process step were detected");
                }
            }

            // Verifies that all missing datas of commodity were returned
            List<string> commodityNullValuePropertiesExpected = new List<string>();
            commodityNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => commodity.Price));
            commodityNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => commodity.Amount));

            Assert.IsNotNull(commodityCheckResult, "Fails to check missing data for a commodity");
            Assert.IsTrue(commodityCheckResult.UnusedEntities.Count == 0, "Unused Entities list is not empty for a commodity entity");
            Assert.IsTrue(commodityCheckResult.InvalidValueProperties.Count == 0, "Invalid value properties were detected for a commodity which has no invalid value property");
            Assert.IsTrue(commodityCheckResult.NullValueProperties.Count == commodityNullValuePropertiesExpected.Count, "Not all null value properties of a commodity were detected");
            foreach (string missingData in commodityNullValuePropertiesExpected)
            {
                if (commodityCheckResult.NullValueProperties.FirstOrDefault(m => string.Equals(m, missingData)) == null)
                {
                    Assert.Fail("Not all null value properties of a commodity were detected");
                }
            }

            // Verifies that all missing datas of machine were returned
            List<string> machineNullValuePropertiesExpected = new List<string>();
            machineNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => machine.ConsumablesCost));
            machineNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => machine.RepairsCost));
            machineNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => machine.MaterialCost));
            machineNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => machine.ExternalWorkCost));

            Assert.IsNotNull(machineCheckResult, "Fails to check missing data for a machine");
            Assert.IsTrue(machineCheckResult.UnusedEntities.Count == 0, "Unused Entities list is not empty for a machine entity");
            Assert.IsTrue(machineCheckResult.InvalidValueProperties.Count == 0, "Invalid value properties were detected for a machine which has no invalid value property");
            Assert.IsTrue(machineCheckResult.NullValueProperties.Count == machineNullValuePropertiesExpected.Count, "Not all null value properties of a machine were detected");
            foreach (string missingData in machineNullValuePropertiesExpected)
            {
                if (machineCheckResult.NullValueProperties.FirstOrDefault(m => string.Equals(m, missingData)) == null)
                {
                    Assert.Fail("Not all null value properties of a machine were detected");
                }
            }

            // Verifies that all missing datas of  consumable were returned
            List<string> consumableNullValuePropertiesExpected = new List<string>();
            consumableNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => consumable.PriceUnitBase));
            consumableNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => consumable.Amount));
            consumableNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => consumable.Price));

            Assert.IsNotNull(consumableCheckResult, "Fails to check missing data for a consumable");
            Assert.IsTrue(consumableCheckResult.UnusedEntities.Count == 0, "Unused Entities list is not empty for a consumable entity");
            Assert.IsTrue(consumableCheckResult.InvalidValueProperties.Count == 0, "Invalid value properties were detected for a consumable which has no invalid value property");
            Assert.IsTrue(consumableCheckResult.NullValueProperties.Count == consumableNullValuePropertiesExpected.Count, "Not all null value properties of a consumable were detected");
            foreach (string missingData in consumableNullValuePropertiesExpected)
            {
                if (consumableCheckResult.NullValueProperties.FirstOrDefault(m => string.Equals(m, missingData)) == null)
                {
                    Assert.Fail("Not all null value properties of a consumable were detected");
                }
            }

            // Verifies that all missing datas of die were returned
            List<string> dieNullValuePropertiesExpected = new List<string>();
            dieNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => die.LifeTime));
            dieNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => die.DiesetsNumberPaidByCustomer));
            dieNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => die.Investment));
            dieNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => die.AllocationRatio));
            dieNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => die.Wear));
            dieNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => die.Maintenance));
            dieNullValuePropertiesExpected.Add(ReflectionUtils.GetPropertyName(() => die.CostAllocationNumberOfParts));

            Assert.IsNotNull(dieCheckResult, "Fails to check missing data for a die");
            Assert.IsTrue(dieCheckResult.UnusedEntities.Count == 0, "Unused Entities list is not empty for a die entity");
            Assert.IsTrue(dieCheckResult.InvalidValueProperties.Count == 0, "Invalid value properties were detected for a die which has no invalid value property");
            Assert.IsTrue(dieCheckResult.NullValueProperties.Count == dieNullValuePropertiesExpected.Count, "Not all null value properties of a die were detected");
            foreach (string missingData in dieNullValuePropertiesExpected)
            {
                if (dieCheckResult.NullValueProperties.FirstOrDefault(m => string.Equals(m, missingData)) == null)
                {
                    Assert.Fail("Not all null value properties of a die were detected");
                }
            }

            // Set CalculationAccuracy of assembly to Estimation
            assembly.CalculationAccuracy = PartCalculationAccuracy.Estimation;

            // Verifies that all assembly missing data were returned
            var result2 = EntityDataChecker.CheckAssemblyData(assembly).FlattenErrors();
            EntityDataCheckResult assemblyCheckResult2 = result2.Where(m => m.Entity.Equals(assembly)).FirstOrDefault();
            List<string> assemblyNullValuePropertiesExpected2 = new List<string>();
            assemblyNullValuePropertiesExpected2.Add(ReflectionUtils.GetPropertyName(() => assembly.EstimatedCost));

            Assert.IsNotNull(assemblyCheckResult2, "Fails to check missing data for an assembly which has CalculationAccuracy set to Estimation");
            Assert.IsTrue(assemblyCheckResult2.UnusedEntities.Count == 0, "Unused Entities list is not empty for an assembly which has CalculationAccuracy set to Estimation");
            Assert.IsTrue(assemblyCheckResult2.InvalidValueProperties.Count == 0, "Invalid value properties were detected for an assembly which has CalculationAccuracy set to Estimation");
            Assert.IsTrue(assemblyCheckResult2.NullValueProperties.Count == assemblyNullValuePropertiesExpected2.Count, "Not all null value properties of an assembly which has CalculationAccuracy set to Estimation were detected");
            foreach (string missingData in assemblyNullValuePropertiesExpected2)
            {
                if (assemblyCheckResult2.NullValueProperties.FirstOrDefault(m => string.Equals(m, missingData)) == null)
                {
                    Assert.Fail("Not all null value properties of an assembly were detected");
                }
            }
        }

        /// <summary>
        /// Tests GetMissingAssemblyData(Assembly assembly) method using a valid assembly as input parameter - 
        /// an assembly having all required properties values set to valid values.
        /// </summary>
        [TestMethod]
        public void CheckAssemblyDataWithValidAssembly2()
        {
            Assembly assembly1 = new Assembly();
            assembly1.Name = "Test Assembly" + DateTime.Now.Ticks;
            assembly1.CalculateLogisticCost = false;
            assembly1.LifeTime = 1;
            assembly1.OtherCost = 1;
            assembly1.PackagingCost = 1;
            assembly1.LogisticCost = 1;
            assembly1.ProjectInvest = 1;
            assembly1.DevelopmentCost = 1;

            Process process = new Process();
            ProcessStep step = new AssemblyProcessStep();
            step.Name = "Test Step" + DateTime.Now.Ticks;
            step.ExceedShiftCost = true;
            step.CycleTime = 1;
            step.SetupsPerBatch = 1;
            step.SetupTime = 1;
            step.MaxDownTime = 1;
            step.SetupUnskilledLabour = 1;
            step.SetupSkilledLabour = 1;
            step.SetupForeman = 1;
            step.SetupTechnicians = 1;
            step.SetupEngineers = 1;
            step.BatchSize = 1;
            step.ProcessTime = 1;
            step.ProductionUnskilledLabour = 1;
            step.ProductionSkilledLabour = 1;
            step.ProductionForeman = 1;
            step.ProductionTechnicians = 1;
            step.ProductionEngineers = 1;
            step.ScrapAmount = 1;
            step.ExtraShiftsNumber = 1;
            step.ShiftCostExceedRatio = 1;
            step.PartsPerCycle = 1;
            process.Steps.Add(step);
            assembly1.Process = process;

            Machine machine = new Machine();
            machine.Name = "Test Machine" + DateTime.Now.Ticks;
            machine.CalculateWithKValue = true;
            machine.KValue = 1;
            machine.MachineInvestment = 1;
            step.Machines.Add(machine);

            Consumable consumable = new Consumable();
            consumable.Name = "Test Consumable" + DateTime.Now.Ticks;
            consumable.PriceUnitBase = new MeasurementUnit();
            consumable.Amount = 1;
            consumable.Price = 1;
            step.Consumables.Add(consumable);

            Commodity commodity = new Commodity();
            commodity.Name = "Test Commodity" + DateTime.Now.Ticks;
            commodity.Price = 1;
            commodity.Amount = 1;
            step.Commodities.Add(commodity);

            Die die = new Die();
            die.Name = "Test Die" + DateTime.Now.Ticks;
            die.LifeTime = 1;
            die.DiesetsNumberPaidByCustomer = 1;
            die.Investment = 1;
            die.AllocationRatio = 1;
            die.Wear = 1;
            die.Maintenance = 1;
            die.CostAllocationBasedOnPartsPerLifeTime = false;
            die.CostAllocationNumberOfParts = 1;
            step.Dies.Add(die);

            Part part = new Part();
            part.Name = "Test Part" + DateTime.Now.Ticks;
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            part.CalculationAccuracy = PartCalculationAccuracy.Estimation;
            part.EstimatedCost = 1;
            assembly1.Parts.Add(part);

            ProcessStepPartAmount partAmount = new ProcessStepPartAmount();
            partAmount.Part = part;
            partAmount.Amount = 1;
            step.PartAmounts.Add(partAmount);

            Assembly subassembly = new Assembly();
            subassembly.Name = "Subassembly" + DateTime.Now.Ticks;
            subassembly.CalculationAccuracy = PartCalculationAccuracy.Estimation;
            subassembly.EstimatedCost = 1;
            assembly1.Subassemblies.Add(subassembly);

            ProcessStepAssemblyAmount assemblyAmount = new ProcessStepAssemblyAmount();
            assemblyAmount.Assembly = subassembly;
            assemblyAmount.Amount = 1;
            step.AssemblyAmounts.Add(assemblyAmount);

            var result1 = EntityDataChecker.CheckAssemblyData(assembly1).FlattenErrors();
            Assert.IsTrue(result1.Count == 0, "Some missing data were detected for an assembly which has no missing data");

            // Creates an assembly which has CalculationAccuracy set to Estimation and all required properties set to valid values
            Assembly assembly2 = new Assembly();
            assembly2.Name = "Test Assembly" + DateTime.Now.Ticks;
            assembly2.CalculationAccuracy = PartCalculationAccuracy.Estimation;
            assembly2.EstimatedCost = 1;

            var result2 = EntityDataChecker.CheckAssemblyData(assembly2).FlattenErrors();
            Assert.IsTrue(result2.Count == 0, "Some missing data were detected for an assembly which has no missing data");
        }

        [TestMethod]
        public void TestMachineOverloadCheckWithNullProcess()
        {
            Assembly assembly = CreateTestDataForMachineOverloadCheck();
            assembly.Process = null;

            var result = EntityDataChecker.CheckAssemblyData(assembly).FlattenErrors();
            result.Remove(result.FirstOrDefault(r => r.Entity == assembly));

            Assert.AreEqual(0, result.Count);
        }

        [TestMethod]
        public void TestMachineOverloadCheck()
        {
            Assembly assy = CreateTestDataForMachineOverloadCheck();
            assy.CalculationVariant = "1.2";

            var result = EntityDataChecker.CheckAssemblyData(assy).FlattenErrors();

            // Keep only the process step related errors
            var stepErrors = result.Where(r => r.Entity is ProcessStep);

            var step1Errors = stepErrors.FirstOrDefault(r => ((ProcessStep)r.Entity).Name == "Step1");
            if (step1Errors != null)
            {
                Assert.IsTrue(step1Errors.OverloadedMachines.Count == 0, "Step 1 should not have overloaded machines.");
            }

            // Check if the expected overloaded machines were returned.
            var step2Errors = stepErrors.FirstOrDefault(r => ((ProcessStep)r.Entity).Name == "Step2");
            Assert.IsTrue(step2Errors.OverloadedMachines.Count == 1, "Step 2 had the wrong number of overloaded machines.");
            Assert.IsTrue(step2Errors.OverloadedMachines[0].Name == "MachineC", "Machine overload check for step 2 returned the wrong machine.");

            var step3Errors = stepErrors.FirstOrDefault(r => ((ProcessStep)r.Entity).Name == "Step3");
            Assert.IsTrue(step3Errors.OverloadedMachines.Count == 1, "Step 3 had the wrong number of overloaded machines.");
            Assert.IsTrue(step3Errors.OverloadedMachines[0].Name == "MachineF", "Machine overload check for step 3 returned the wrong machine.");
        }

        [TestMethod]
        public void TestMachineOverloadCheckWithEstimatedProcessSteps()
        {
            Assembly assy = CreateTestDataForMachineOverloadCheck();
            assy.CalculationVariant = "1.2";

            var result = EntityDataChecker.CheckAssemblyData(assy).FlattenErrors();
            var stepErrors = result.Where(r => r.Entity is ProcessStep);

            // The overload machine check should not be performed on steps with estimated cost.
            var step4Errors = stepErrors.FirstOrDefault(r => ((ProcessStep)r.Entity).Name == "Step4");
            if (step4Errors != null)
            {
                Assert.IsTrue(step4Errors.OverloadedMachines.Count == 0, "Machine overload check was performed on machines in step 4, which has an estimated cost.");
            }
        }

        [TestMethod]
        public void TestMachineOverloadCheckWithDeletedMachine()
        {
            Assembly assy = CreateTestDataForMachineOverloadCheck();
            assy.CalculationVariant = "1.2";

            // Set MachineF as deleted and check to see if it appears in the overload check results.
            Machine machineF = assy.Process.Steps.FirstOrDefault(s => s.Name == "Step3").Machines.FirstOrDefault(m => m.Name == "MachineF");
            machineF.IsDeleted = true;

            var result = EntityDataChecker.CheckAssemblyData(assy).FlattenErrors();
            var stepErrors = result.Where(r => r.Entity is ProcessStep);

            var step3Errors = stepErrors.FirstOrDefault(r => ((ProcessStep)r.Entity).Name == "Step3");
            if (step3Errors != null)
            {
                Assert.IsTrue(step3Errors.OverloadedMachines.Count == 0, "A deleted overloaded machine was included in the check results for Step 3.");
            }
        }

        [TestMethod]
        public void TestMachineOverloadCheckPreCalculationVersion1_2()
        {
            Assembly assy = CreateTestDataForMachineOverloadCheck();

            // Check for v1.0
            assy.CalculationVariant = "1.0";
            var result = EntityDataChecker.CheckAssemblyData(assy).FlattenErrors();
            foreach (var stepError in result.Where(r => r.Entity is ProcessStep))
            {
                Assert.IsTrue(stepError.OverloadedMachines.Count == 0, "Machine overload check was performed for calculation version 1.0");
            }

            // Check for v1.1
            assy.CalculationVariant = "1.1";
            result = EntityDataChecker.CheckAssemblyData(assy).FlattenErrors();
            foreach (var stepError in result.Where(r => r.Entity is ProcessStep))
            {
                Assert.IsTrue(stepError.OverloadedMachines.Count == 0, "Machine overload check was performed for calculation version 1.1");
            }

            // Check for v1.1.1
            assy.CalculationVariant = "1.1.1";
            result = EntityDataChecker.CheckAssemblyData(assy).FlattenErrors();
            foreach (var stepError in result.Where(r => r.Entity is ProcessStep))
            {
                Assert.IsTrue(stepError.OverloadedMachines.Count == 0, "Machine overload check was performed for calculation version 1.1.1");
            }
        }

        #endregion Test methods

        #region Helpers

        /// <summary>
        /// Creates test data for testing the machine overload check.
        /// </summary>
        /// <returns>An Assembly containing the test data.</returns>
        private Assembly CreateTestDataForMachineOverloadCheck()
        {
            Assembly assy = new Assembly();
            assy.BatchSizePerYear = 24;
            assy.YearlyProductionQuantity = 100000;
            assy.LifeTime = 5;
            assy.Process = new Process();

            ProcessStep step1 = new AssemblyProcessStep()
            {
                Name = "Step1",
                CycleTime = 30,
                ProcessTime = 30,
                PartsPerCycle = 1,
                SetupsPerBatch = 1,
                SetupTime = 0,
                MaxDownTime = 60,
                BatchSize = 4167,
                ScrapAmount = 0.01m,
                Rework = 0,
                ShiftsPerWeek = 15,
                HoursPerShift = 7.5m,
                ProductionDaysPerWeek = 5,
                ProductionWeeksPerYear = 48
            };
            assy.Process.Steps.Add(step1);

            Machine machineA = new Machine() { Name = "MachineA", Availability = 0.96m };
            step1.Machines.Add(machineA);
            Machine machineB = new Machine() { Name = "MachineB", Availability = 0.9m };
            step1.Machines.Add(machineB);

            ProcessStep step2 = new AssemblyProcessStep()
            {
                Name = "Step2",
                CycleTime = 1200,
                ProcessTime = 1200,
                PartsPerCycle = 8,
                SetupsPerBatch = 1,
                SetupTime = 0,
                MaxDownTime = 20,
                BatchSize = 1000,
                ScrapAmount = 0.02m,
                Rework = 0,
                ShiftsPerWeek = 15,
                HoursPerShift = 7.5m,
                ProductionDaysPerWeek = 5,
                ProductionWeeksPerYear = 48
            };
            assy.Process.Steps.Add(step2);

            Machine machineC = new Machine() { Name = "MachineC", Availability = 0.7m };
            step2.Machines.Add(machineC);
            Machine machineD = new Machine() { Name = "MachineD", Availability = 0.9m };
            step2.Machines.Add(machineD);

            ProcessStep step3 = new AssemblyProcessStep()
            {
                Name = "Step3",
                CycleTime = 600,
                ProcessTime = 600,
                PartsPerCycle = 5,
                SetupsPerBatch = 1,
                SetupTime = 0,
                MaxDownTime = 60,
                BatchSize = 4167,
                ScrapAmount = 0.002m,
                Rework = 0,
                ShiftsPerWeek = 15,
                HoursPerShift = 7.5m,
                ProductionDaysPerWeek = 5,
                ProductionWeeksPerYear = 48
            };
            assy.Process.Steps.Add(step3);

            Machine machineE = new Machine() { Name = "MachineE", Availability = 0.85m };
            step3.Machines.Add(machineE);
            Machine machineF = new Machine() { Name = "MachineF", Availability = 0.6m };
            step3.Machines.Add(machineF);

            ProcessStep step4 = new AssemblyProcessStep()
            {
                Name = "Step4",
                Accuracy = ProcessCalculationAccuracy.Estimated,
                CycleTime = 600,
                ProcessTime = 600,
                PartsPerCycle = 5,
                SetupsPerBatch = 1,
                SetupTime = 0,
                MaxDownTime = 60,
                BatchSize = 4167,
                ScrapAmount = 0.002m,
                Rework = 0,
                ShiftsPerWeek = 15,
                HoursPerShift = 7.5m,
                ProductionDaysPerWeek = 5,
                ProductionWeeksPerYear = 48
            };
            assy.Process.Steps.Add(step4);

            Machine machineG = new Machine() { Name = "MachineG", Availability = 0.6m };
            step4.Machines.Add(machineG);

            return assy;
        }

        #endregion
    }
}
