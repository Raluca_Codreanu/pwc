﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZPKTool.Business.Export;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;

namespace ZPKTool.Business.Tests
{
    /// <summary>
    /// Tests the export functionality of application.
    /// </summary>
    [TestClass]
    public class ExportManagerTest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExportManagerTest"/> class.
        /// </summary>
        public ExportManagerTest()
        {
            Utils.ConfigureDbAccess();
        }

        #region Additional test attributes

        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }

        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }

        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }

        /// <summary>
        /// Use TestCleanup to run code after each test has run
        /// If a user is logged in the application, it should be logged out after the test was done otherwise will influence other tests.
        /// </summary>
        [TestCleanup]
        public void MyTestCleanup()
        {
            if (SecurityManager.Instance.CurrentUser != null)
            {
                SecurityManager.Instance.Logout();
            }
        }

        #endregion Additional test attributes

        #region Test methods

        /// <summary>
        /// Tests Export(object entity, ChangeSet changeSet, string path) method using a null entity as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ExportNullEntity()
        {
            string exportPath = Path.GetTempPath() + "die.die";
            ExportManager exportManager = new ExportManager();
            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            exportManager.Export(null, dataManager, exportPath, null, null);
        }

        /// <summary>
        /// Tests Export(object entity, ChangeSet changeSet, string path) method using an invalid path as input data.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ExportIntoInvalidPath()
        {
            Die die = new Die();
            die.Name = "Test Die" + DateTime.Now.Ticks;

            ExportManager exportManager = new ExportManager();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            exportManager.Export(die, dataContext, string.Empty, null, null);
        }

        /// <summary>
        /// Tests ImportEntity(string filePath, object objToImportInto, ChangeSet changeSet) method
        /// using an entity which is not supported for import operation.
        /// </summary>
        [TestMethod]
        public void ImportNotSupportedEntity()
        {
            ProcessStep step = new AssemblyProcessStep();
            step.Name = "Test Step" + DateTime.Now.Ticks;

            var defaultCurrency = CurrencyConversionManager.DefaultBaseCurrency;
            var currencies = new Collection<Currency>();
            currencies.Add(defaultCurrency);

            string exportPath = Path.GetTempPath() + step.Name + ".step";
            ExportManager exportManager = new ExportManager();
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            exportManager.Export(step, dataContext, exportPath, currencies, defaultCurrency);

            // Creates a Project -> Assembly in which the step will be imported
            Project project = new Project();
            project.Name = "Test Project" + DateTime.Now.Ticks;
            project.OverheadSettings = new OverheadSetting();

            Assembly assembly = new Assembly();
            assembly.Name = "Assembly" + DateTime.Now.Ticks;
            assembly.Process = new Process();
            project.Assemblies.Add(assembly);

            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            var importData = exportManager.ImportEntity<ProcessStep>(exportPath, assembly, dataContext, false);
            Assert.IsTrue(importData.Error is BusinessException, "The expected error was not generated.");
        }

        /// <summary>
        /// Tests ImportEntity(string filePath, object objToImportInto, ChangeSet changeSet) method using 
        /// an invalid path as input data.
        /// </summary>
        [TestMethod]
        public void ImportEntityFromInvalidPath()
        {
            Project project = new Project();
            project.Name = "Test Project" + DateTime.Now.Ticks;
            project.OverheadSettings = new OverheadSetting();

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            ExportManager exportManager = new ExportManager();
            var importData = exportManager.ImportEntity<Assembly>(string.Empty, project, dataContext, false);
            Assert.IsTrue(importData.Error is BusinessException, "The expected error was not generated.");
        }

        /// <summary>
        /// Tests ImportEntity(string filePath, object objToImportInto, ChangeSet changeSet) method using
        /// a null ChangeSet as input data
        /// </summary>
        [TestMethod]
        public void ImportEntityIntoNullDataManager()
        {
            Part part = new Part();
            part.Name = "Part" + DateTime.Now.Ticks;
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var defaultCurrency = CurrencyConversionManager.DefaultBaseCurrency;
            var currencies = new Collection<Currency>();
            currencies.Add(defaultCurrency);

            string exportPath = Path.GetTempPath() + part.Name + ".part";
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            ExportManager exportManager = new ExportManager();
            exportManager.Export(part, dataContext, exportPath, currencies, defaultCurrency);

            Project project = new Project();
            project.Name = "Project" + DateTime.Now.Ticks;
            project.OverheadSettings = new OverheadSetting();

            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            var importData = exportManager.ImportEntity<Part>(exportPath, project, null, false);

            Assert.IsTrue(importData.Error is BusinessException, "The expected error was not generated.");
        }

        /// <summary>
        /// Tests ImportEntity(string filePath, object objToImportInto, ChangeSet changeSet) method
        /// using an entity which has a different type than TEntity.
        /// </summary>
        [TestMethod]
        public void ImportNotTEntity()
        {
            Part part = new Part();
            part.Name = "Part" + DateTime.Now.Ticks;
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            var defaultCurrency = CurrencyConversionManager.DefaultBaseCurrency;
            var currencies = new Collection<Currency>();
            currencies.Add(defaultCurrency);

            string exportPath = Path.GetTempPath() + part.Name + ".part";
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            ExportManager exportManager = new ExportManager();
            exportManager.Export(part, dataContext, exportPath, currencies, defaultCurrency);

            Project project = new Project();
            project.Name = "Project" + DateTime.Now.Ticks;
            project.OverheadSettings = new OverheadSetting();

            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            var importData = exportManager.ImportEntity<Assembly>(exportPath, project, dataContext, false);

            Assert.IsTrue(importData.Error is BusinessException, "The expected error was not generated.");
        }

        /// <summary>
        /// Tests ImportEntity(string filePath, object objToImportInto, ChangeSet changeSet) method using
        /// a null objToImportInto as input data. The object which will be imported cannot be imported without a parent.
        /// </summary>
        [TestMethod]
        public void ImportEntityIntoNullEntity()
        {
            Die die = new Die();
            die.Name = "Test Die" + DateTime.Now.Ticks;

            var defaultCurrency = CurrencyConversionManager.DefaultBaseCurrency;
            var currencies = new Collection<Currency>();
            currencies.Add(defaultCurrency);

            string exportPath = Path.GetTempPath() + die.Name + ".die";
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            ExportManager exportManager = new ExportManager();
            exportManager.Export(die, dataContext, exportPath, currencies, defaultCurrency);

            var importData = exportManager.ImportEntity<Die>(exportPath, null, dataContext, false);
            Assert.IsTrue(importData.Error is BusinessException, "The expected error was not generated.");
        }

        /// <summary>
        /// Tests ImportEntity(string filePath, object objToImportInto, ChangeSet changeSet) method using
        /// a not supported objToImportInto as input parameter(e.g. import an assembly into a part).
        /// </summary>
        [TestMethod]
        public void ImportEntityIntoNotSupportedParent()
        {
            Assembly assembly = new Assembly();
            assembly.Name = "Test Assembly" + DateTime.Now.Ticks;
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();

            var defaultCurrency = CurrencyConversionManager.DefaultBaseCurrency;
            var currencies = new Collection<Currency>();
            currencies.Add(defaultCurrency);

            string exportPath = Path.GetTempPath() + assembly.Name + ".assembly";
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            ExportManager exportManager = new ExportManager();
            exportManager.Export(assembly, dataContext, exportPath, currencies, defaultCurrency);

            var project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();

            Part part = new Part();
            part.Name = "Test Part" + DateTime.Now.Ticks;
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            project.Parts.Add(part);

            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            var importData = exportManager.ImportEntity<Assembly>(exportPath, part, dataContext, false);
            Assert.IsTrue(importData.Error is BusinessException, "The expected error was not generated.");
        }

        /// <summary>
        /// Tests Export(object entity, ChangeSet changeSet, string path) method and ImportEntity(string filePath, object objToImportInto, ChangeSet changeSet)
        /// method using valid data - import/export a die. 
        /// </summary>
        [TestMethod]
        public void ExportImportValidDie()
        {
            Die die = new Die();
            die.Name = die.Guid.ToString();
            die.Remark = "Remark of Die";
            die.Type = DieType.MoldingDie;
            die.Investment = 100;
            die.ReusableInvest = 50;
            die.Wear = 90;
            die.IsWearInPercentage = true;
            die.Maintenance = 50;
            die.IsMaintenanceInPercentage = true;
            die.LifeTime = 60;
            die.AllocationRatio = 70;
            die.DiesetsNumberPaidByCustomer = 80;
            die.CostAllocationBasedOnPartsPerLifeTime = true;
            die.CostAllocationNumberOfParts = 1;

            // Add media to die.
            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 1, 2, 3, 4 };
            image.Size = image.Content.Length;
            image.OriginalFileName = image.Guid.ToString();
            die.Media = image;

            // Add a manufacturer to die.
            die.Manufacturer = new Manufacturer();
            die.Manufacturer.Name = die.Manufacturer.Guid.ToString();
            die.Manufacturer.Description = "Manufacturer Of Die" + DateTime.Now.Ticks;

            // Set an owner to die.
            die.Owner = new User();
            die.Owner.Name = die.Owner.Guid.ToString();
            die.Owner.Username = "Username" + DateTime.Now.Ticks;
            die.Owner.Password = EncryptionManager.Instance.HashSHA256("Password.", die.Owner.Salt, die.Owner.Guid.ToString());
            die.Owner.Roles = Role.Admin;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.DieRepository.Add(die);
            dataContext.SaveChanges();

            var defaultCurrency = CurrencyConversionManager.DefaultBaseCurrency;
            var currencies = new Collection<Currency>();
            currencies.Add(defaultCurrency);

            // Exports  created die. 
            string exportDirectory = Path.Combine(Path.GetTempPath() + die.Guid.ToString());
            string exportFilePath = Path.Combine(exportDirectory, die.Name + ".die");
            Directory.CreateDirectory(exportDirectory);
            ExportManager exportManager = new ExportManager();
            exportManager.Export(die, dataContext, exportFilePath, currencies, defaultCurrency);

            // Create a Project and add an Assembly -> ProcessStep and Part -> ProcessStep in which the die will be imported
            Project project = new Project();
            project.Name = "Test Project" + DateTime.Now.Ticks;
            project.OverheadSettings = new OverheadSetting();

            User user = new User();
            user.Username = "Username" + DateTime.Now.Ticks;
            user.Password = EncryptionManager.Instance.HashSHA256("Password", user.Salt, user.Guid.ToString());
            user.Name = "User" + DateTime.Now.Ticks;
            user.Roles = Role.Admin;

            Assembly assembly = new Assembly();
            assembly.Name = "Assembly" + DateTime.Now.Ticks;
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();

            ProcessStep step1 = new AssemblyProcessStep();
            step1.Name = "Step" + DateTime.Now.Ticks;
            assembly.Process = new Process();
            assembly.Process.Steps.Add(step1);
            project.Assemblies.Add(assembly);

            Part part = new Part();
            part.Name = "Part" + DateTime.Now.Ticks;
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            ProcessStep step2 = new PartProcessStep();
            step2.Name = "Step" + DateTime.Now.Ticks;
            part.Process = new Process();
            part.Process.Steps.Add(step2);
            project.Parts.Add(part);
            project.SetOwner(user);

            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            // Import the exported die into ProcessStep of Assembly
            var importedData1 = exportManager.ImportEntity<Die>(exportFilePath, step1, dataContext, true);
            Die importedDie1 = importedData1.Entity;
            bool result1 = AreEqual(die, dataContext, importedDie1, dataContext);

            Assert.IsTrue(result1, "Importing a die into an assembly process step doesn't work properly");
            Assert.AreEqual(step1.Guid, importedDie1.ProcessStep.Guid, "Failed to import a die into the specified entity");
            Assert.IsTrue(importedDie1.Owner != null && step1.Owner.Guid == importedDie1.Owner.Guid, "Fails to set the owner at die importing");

            // Import the exported die into ProcessStep of Part
            var importedData2 = exportManager.ImportEntity<Die>(exportFilePath, step2, dataContext, true);
            Die importedDie2 = importedData2.Entity;
            bool result2 = AreEqual(die, dataContext, importedDie2, dataContext);

            Assert.IsTrue(result2, "Importing a die into a part process step doesn't work properly");
            Assert.AreEqual<Guid>(step2.Guid, importedDie2.ProcessStep.Guid, "Failed to import a die into the specified entity");
            Assert.IsTrue(importedDie1.Owner != null && step2.Owner.Guid == importedDie2.Owner.Guid, "Failed to set the owner at die importing");
        }

        /// <summary>
        /// Tests Export(object entity, ChangeSet changeSet, string path) method and ImportEntity(string filePath, object objToImportInto, ChangeSet changeSet)
        /// method using valid data - import/export a commodity. 
        /// </summary>
        [TestMethod]
        public void ExportImportValidCommodity()
        {
            Commodity commodity = new Commodity();
            commodity.Name = commodity.Guid.ToString();
            commodity.Price = 1000;
            commodity.Weight = 10;
            commodity.RejectRatio = 100;
            commodity.Remarks = "commodity is used for test";
            commodity.Amount = 9;
            commodity.PartNumber = "90";

            // Adds a media to commodity.
            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            image.Size = image.Content.Length;
            image.OriginalFileName = image.Guid.ToString();
            commodity.Media = image;

            // Create a manufacturer for commodity.
            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = manufacturer.Guid.ToString();
            commodity.Manufacturer = manufacturer;

            // Set WeightUnitBase.
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            commodity.WeightUnitBase = dataContext.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Kilogram");

            // Set parent process step.
            commodity.ProcessStep = new AssemblyProcessStep();
            commodity.ProcessStep.Name = commodity.ProcessStep.Guid.ToString();
            commodity.ProcessStep.Process = new Process();

            // Set the owner.
            commodity.Owner = new User();
            commodity.Owner.Name = "User" + DateTime.Now.Ticks;
            commodity.Owner.Username = "Username" + DateTime.Now.Ticks;
            commodity.Owner.Password = EncryptionManager.Instance.HashSHA256("Password.", commodity.Owner.Salt, commodity.Owner.Guid.ToString());
            commodity.Owner.Roles = Role.Admin;

            dataContext.CommodityRepository.Add(commodity);
            dataContext.SaveChanges();

            var defaultCurrency = CurrencyConversionManager.DefaultBaseCurrency;
            var currencies = new Collection<Currency>();
            currencies.Add(defaultCurrency);

            string exportDirectoryPath = Path.Combine(Path.GetTempPath(), commodity.Guid.ToString());
            string exportFilePath = Path.Combine(exportDirectoryPath, commodity.Name + ".commodity");
            Directory.CreateDirectory(exportDirectoryPath);
            ExportManager exportManager = new ExportManager();
            exportManager.Export(commodity, dataContext, exportFilePath, currencies, defaultCurrency);

            User user = new User();
            user.Username = "Username" + DateTime.Now.Ticks;
            user.Password = EncryptionManager.Instance.HashSHA256("Password.", user.Salt, user.Guid.ToString());
            user.Name = "User" + DateTime.Now.Ticks;
            user.Roles = Role.Admin;

            // Create a project
            var project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();

            // Create a part in which will be imported the commodity
            Part part = new Part();
            part.Name = "Test Part" + DateTime.Now.Ticks;
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            part.SetOwner(user);
            project.Parts.Add(part);

            // Create an assembly in which will be imported the commodity
            Assembly assembly = new Assembly();
            assembly.Name = "Assembly" + DateTime.Now.Ticks;
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();
            project.Assemblies.Add(assembly);

            ProcessStep step = new AssemblyProcessStep();
            step.Name = "Step" + DateTime.Now.Ticks;
            assembly.Process = new Process();
            assembly.Process.Steps.Add(step);
            assembly.SetOwner(user);

            dataContext.ProjectRepository.Add(project);
            dataContext.AssemblyRepository.Add(assembly);
            dataContext.SaveChanges();

            // Imports the commodity into the part
            var importedData1 = exportManager.ImportEntity<Commodity>(exportFilePath, part, dataContext, true);
            Commodity importedCommodity1 = importedData1.Entity;
            bool result1 = AreEqual(commodity, dataContext, importedCommodity1, dataContext);

            Assert.IsTrue(result1, "Exporting a commodity entity into a part doesn't work properly");
            Assert.AreEqual<Guid>(part.Guid, importedCommodity1.Part.Guid, "Commodity wasn't imported into specified entity");
            Assert.IsTrue(importedCommodity1.Owner != null && part.Owner.Guid == importedCommodity1.Owner.Guid, "Fails to set the owner at commodity importing");

            // Imports the exported commodity into process step of assembly
            var importedData2 = exportManager.ImportEntity<Commodity>(exportFilePath, step, dataContext, true);
            Commodity importedCommodity2 = importedData2.Entity;
            bool result2 = AreEqual(commodity, dataContext, importedCommodity2, dataContext);

            Assert.IsTrue(result2, "Exporting a commodity entity into an assembly process step doesn't work properly");
            Assert.AreEqual<Guid>(step.Guid, importedCommodity2.ProcessStep.Guid, "Failed to import a commodity into the specified entity");
            Assert.IsTrue(importedCommodity2.Owner != null && assembly.Owner.Guid == importedCommodity2.Owner.Guid, "Failed to set the owner at commodity importing");
        }

        /// <summary>
        /// Tests Export(object entity, ChangeSet changeSet, string path) method and ImportEntity(string filePath, object objToImportInto, ChangeSet changeSet)
        /// method using valid data - import/export a consumable.
        /// </summary>
        [TestMethod]
        public void ExportImportValidConsumable()
        {
            Consumable consumable = new Consumable();
            consumable.Name = consumable.Guid.ToString();
            consumable.Description = "Description of Consumable";
            consumable.Price = 200;
            consumable.Amount = 100;

            // Adds a manufacturer to consumable.
            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = manufacturer.Guid.ToString();
            manufacturer.Description = EncryptionManager.Instance.GenerateRandomString(10, true);
            consumable.Manufacturer = manufacturer;

            // Set PriceUnitBase and AmountUnitBase.
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            consumable.PriceUnitBase = dataContext.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Kilogram");
            consumable.AmountUnitBase = dataContext.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Kilogram");

            dataContext.ConsumableRepository.Add(consumable);
            dataContext.SaveChanges();

            var defaultCurrency = CurrencyConversionManager.DefaultBaseCurrency;
            var currencies = new Collection<Currency>();
            currencies.Add(defaultCurrency);

            string exportDirectoryPath = Path.Combine(Path.GetTempPath(), consumable.Guid.ToString());
            string exportFilePath = Path.Combine(exportDirectoryPath, consumable.Name + ".consumable");
            Directory.CreateDirectory(exportDirectoryPath);
            ExportManager exportManager = new ExportManager();
            exportManager.Export(consumable, dataContext, exportFilePath, currencies, defaultCurrency);

            User user = new User();
            user.Username = "Username" + DateTime.Now.Ticks;
            user.Password = EncryptionManager.Instance.HashSHA256("Password.", user.Salt, user.Guid.ToString());
            user.Name = "User" + DateTime.Now.Ticks;
            user.Roles = Role.Admin;

            // Create a project
            var project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();

            // Create an Assembly-> Process -> Process Step in which will be imported the consumable
            Assembly assembly = new Assembly();
            assembly.Name = "Test Assembly" + DateTime.Now.Ticks;
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            project.Assemblies.Add(assembly);

            ProcessStep step1 = new AssemblyProcessStep();
            step1.Name = step1.Guid.ToString();
            assembly.Process = new Process();
            assembly.Process.Steps.Add(step1);
            assembly.SetOwner(user);

            // Create a Part -> Process -> Step in which will be imported the consumable
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            project.Parts.Add(part);

            ProcessStep step2 = new PartProcessStep();
            step2.Name = step2.Guid.ToString();
            part.Process = new Process();
            part.Process.Steps.Add(step2);
            part.SetOwner(user);

            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            // Import the exported consumable into a part process step
            var importedData1 = exportManager.ImportEntity<Consumable>(exportFilePath, step1, dataContext, true);
            Consumable importedConsumable1 = importedData1.Entity;
            bool result1 = AreEqual(consumable, importedConsumable1);

            Assert.IsTrue(result1, "Exporting a consumable doesn't work properly");
            Assert.AreEqual<Guid>(step1.Guid, importedConsumable1.ProcessStep.Guid, "Failed to import a consumable into the specified entity");
            Assert.IsTrue(importedConsumable1.Owner != null && step1.Owner.Guid == importedConsumable1.Owner.Guid, "Failed to set the owner at consumable importing");

            // Import the exported consumable into an assembly process step
            var importedData2 = exportManager.ImportEntity<Consumable>(exportFilePath, step2, dataContext, true);
            Consumable importedConsumable2 = importedData2.Entity;
            bool result2 = AreEqual(consumable, importedConsumable2);

            Assert.IsTrue(result2, "Exporting a consumable into an assembly process step doesn't work properly");
            Assert.AreEqual<Guid>(step2.Guid, importedConsumable2.ProcessStep.Guid, "Failed to import a consumable into the specified entity");
            Assert.IsTrue(importedConsumable2.Owner != null && step2.Owner.Guid == importedConsumable2.Owner.Guid, "Failed to set the owner at consumable importing");
        }

        /// <summary>
        /// Tests Export(object entity, ChangeSet changeSet, string path) method and ImportEntity(string filePath, object objToImportInto, ChangeSet changeSet) 
        /// method using valid data - import/export a raw material.
        /// </summary>
        [TestMethod]
        public void ExportImportValidRawMaterial()
        {
            RawMaterial material = new RawMaterial();
            material.Name = material.Guid.ToString();
            material.NormName = "Norm Name Of Raw Material";
            material.Price = 200;
            material.RejectRatio = 40;
            material.Remarks = "Qualitative material";
            material.ParentWeight = 100;
            material.Quantity = 200;
            material.ScrapRefundRatio = 300;
            material.Loss = 30;
            material.NameUK = "Plasterboard";
            material.NameUS = "Plasterboard9";
            material.Sprue = 300;
            material.ScrapCalculationType = ScrapCalculationType.Dispose;
            material.YieldStrength = "1";
            material.RuptureStrength = "1";
            material.Density = "1";
            material.MaxElongation = "1";
            material.GlassTransitionTemperature = "1";
            material.Rx = "1";
            material.Rm = "1";

            // Adds a media to raw material.
            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 1, 2, 3, 4, 5 };
            image.Size = image.Content.Length;
            image.OriginalFileName = image.Guid.ToString();
            material.Media = image;

            // Add a manufacturer to created raw material.
            material.Manufacturer = new Manufacturer();
            material.Manufacturer.Name = material.Manufacturer.Guid.ToString();
            material.Manufacturer.Description = "Manufacturer Of Raw Material";

            // Set PriceUnitBase,ParentWeightUnitBase and QuantityUnitBase.
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            material.PriceUnitBase = dataContext.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Kilogram");
            material.ParentWeightUnitBase = dataContext.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Kilogram");
            material.QuantityUnitBase = dataContext.MeasurementUnitRepository.GetAll().FirstOrDefault(m => m.Name == "Kilogram");

            // Set material classifications.
            material.MaterialsClassificationL1 = new MaterialsClassification();
            material.MaterialsClassificationL1.Name = material.MaterialsClassificationL1.Guid.ToString();
            material.MaterialsClassificationL2 = new MaterialsClassification();
            material.MaterialsClassificationL2.Name = material.MaterialsClassificationL2.Guid.ToString();
            material.MaterialsClassificationL3 = new MaterialsClassification();
            material.MaterialsClassificationL3.Name = material.MaterialsClassificationL3.Guid.ToString();
            material.MaterialsClassificationL4 = new MaterialsClassification();
            material.MaterialsClassificationL4.Name = material.MaterialsClassificationL4.Guid.ToString();

            // Set delivery type of raw material.
            material.DeliveryType = new RawMaterialDeliveryType();
            material.DeliveryType.Name = "Plates";

            dataContext.RawMaterialRepository.Add(material);
            dataContext.SaveChanges();

            var defaultCurrency = CurrencyConversionManager.DefaultBaseCurrency;
            var currencies = new Collection<Currency>();
            currencies.Add(defaultCurrency);

            string exportDirectoryPath = Path.Combine(Path.GetTempPath(), material.Guid.ToString());
            string exportFilePath = Path.Combine(exportDirectoryPath, material.Name + ".material");
            Directory.CreateDirectory(exportDirectoryPath);
            ExportManager exportManager = new ExportManager();
            exportManager.Export(material, dataContext, exportFilePath, currencies, defaultCurrency);

            User user = new User();
            user.Username = "User" + DateTime.Now.Ticks;
            user.Password = EncryptionManager.Instance.HashSHA256("Password.", user.Salt, user.Guid.ToString());
            user.Name = "User" + DateTime.Now.Ticks;
            user.Roles = Role.Admin;

            // Create a project
            var project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();

            // Create a Part in which will be imported the raw material
            Part part = new Part();
            part.Name = "Test Part" + DateTime.Now.Ticks;
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            part.SetOwner(user);
            project.Parts.Add(part);

            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            // Import the exported raw material into a part
            RawMaterial importedMaterial1 = ExportManager.Instance.ImportEntity<RawMaterial>(exportFilePath, part, dataContext, true).Entity;
            bool result1 = AreEqual(material, dataContext, importedMaterial1, dataContext);

            Assert.IsTrue(result1, "Exporting a raw material doesn't work properly");
            Assert.AreEqual<Guid>(part.Guid, importedMaterial1.Part.Guid, "Raw Material wasn't imported into the specified entity");
            Assert.IsTrue(importedMaterial1.Owner != null && part.Owner.Guid == importedMaterial1.Owner.Guid, "Fails to set the owner at raw material importing");
        }

        /// <summary>
        /// Tests Export(object entity, ChangeSet changeSet, string path) method and ImportEntity(string filePath, object objToImportInto, ChangeSet changeSet) 
        /// method using valid data - import/export a machine.
        /// </summary>
        [TestMethod]
        public void ExportImportValidMachine()
        {
            Machine machine = new Machine();
            machine.Name = machine.Guid.ToString();
            machine.ManufacturingYear = 2000;
            machine.RegistrationNumber = "1234567";
            machine.DescriptionOfFunction = "Grass Cutter";
            machine.FloorSize = 100;
            machine.WorkspaceArea = 90;
            machine.PowerConsumption = 105;
            machine.AirConsumption = 50;
            machine.WaterConsumption = 100;
            machine.FullLoadRate = 20;
            machine.MaxCapacity = 300;
            machine.OEE = 300;
            machine.Availability = 30;
            machine.MachineInvestment = 500;
            machine.SetupInvestment = 400;
            machine.AdditionalEquipmentInvestment = 600;
            machine.FundamentalSetupInvestment = 300;
            machine.InvestmentRemarks = "Good investment";
            machine.ExternalWorkCost = 2500;
            machine.MaterialCost = 200;
            machine.ConsumablesCost = 300;
            machine.RepairsCost = 500;
            machine.KValue = 20;
            machine.CalculateWithKValue = true;
            machine.ManualConsumableCostCalc = true;
            machine.ManualConsumableCostPercentage = 30;
            machine.IsProjectSpecific = true;
            machine.DepreciationPeriod = 20;
            machine.DepreciationRate = 40;
            machine.MountingCubeLength = "300";
            machine.MountingCubeWidth = "200";
            machine.MountingCubeHeight = "100";
            machine.MaxDiameterRodPerChuckMaxThickness = "30";
            machine.MaxPartsWeightPerCapacity = "40";
            machine.LockingForce = "300";
            machine.PressCapacity = "700";
            machine.MaximumSpeed = "200";
            machine.RapidFeedPerCuttingSpeed = "300";
            machine.StrokeRate = "9";
            machine.Other = "10";
            machine.IsScrambled = true;

            // Add media to machine.
            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 1, 9, 9, 9 };
            image.Size = image.Content.Length;
            image.OriginalFileName = image.Guid.ToString();
            machine.Media = image;

            // Creates a manufacturer for machine.
            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = manufacturer.Guid.ToString();
            manufacturer.Description = "Description" + DateTime.Now.Ticks;
            machine.Manufacturer = manufacturer;

            // Set machine classifications.
            machine.MainClassification = new MachinesClassification();
            machine.MainClassification.Name = machine.MainClassification.Guid.ToString();
            machine.TypeClassification = new MachinesClassification();
            machine.TypeClassification.Name = machine.TypeClassification.Guid.ToString();
            machine.SubClassification = new MachinesClassification();
            machine.SubClassification.Name = machine.SubClassification.Guid.ToString();
            machine.ClassificationLevel4 = new MachinesClassification();
            machine.ClassificationLevel4.Name = machine.ClassificationLevel4.Guid.ToString();

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            dataContext.MachineRepository.Add(machine);
            dataContext.SaveChanges();

            var defaultCurrency = CurrencyConversionManager.DefaultBaseCurrency;
            var currencies = new Collection<Currency>();
            currencies.Add(defaultCurrency);

            string exportDirectoryPath = Path.Combine(Path.GetTempPath(), machine.Guid.ToString());
            string exportPath = Path.Combine(exportDirectoryPath, machine.Name + ".machine");
            Directory.CreateDirectory(exportDirectoryPath);
            ExportManager exportManager = new ExportManager();
            exportManager.Export(machine, dataContext, exportPath, currencies, defaultCurrency);

            User user = new User();
            user.Username = "User" + DateTime.Now.Ticks;
            user.Password = EncryptionManager.Instance.HashSHA256("Password.", user.Salt, user.Guid.ToString());
            user.Name = "User" + DateTime.Now.Ticks;
            user.Roles = Role.Admin;

            // Create a project
            var project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();

            // Create an Assembly -> Process-> Process Step in which will be imported the machine
            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            project.Assemblies.Add(assembly);

            ProcessStep step1 = new AssemblyProcessStep();
            step1.Name = step1.Guid.ToString();
            assembly.Process = new Process();
            assembly.Process.Steps.Add(step1);
            assembly.SetOwner(user);

            // Create a Part -> Process -> ProcessStepin which will be imported the machine
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            project.Parts.Add(part);

            ProcessStep step2 = new PartProcessStep();
            step2.Name = step2.Guid.ToString();
            part.Process = new Process();
            part.Process.Steps.Add(step2);
            part.SetOwner(user);

            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            // Import the exported machine into assembly process step
            Machine importedMachine1 = exportManager.ImportEntity<Machine>(exportPath, step1, dataContext, false).Entity;
            bool result1 = AreEqual(machine, dataContext, importedMachine1, dataContext);

            Assert.IsTrue(result1, "Exporting a machine doesn't work properly");
            Assert.AreEqual<Guid>(step1.Guid, importedMachine1.ProcessStep.Guid, "Failed to import a machine into the specified entity");
            Assert.IsTrue(importedMachine1.Owner != null && step1.Owner.Guid == importedMachine1.Owner.Guid, "Failed to set the owner at machine importing");

            // Import the exported machine into part process step
            Machine importedMachine2 = exportManager.ImportEntity<Machine>(exportPath, step2, dataContext, false).Entity;
            bool result2 = AreEqual(machine, dataContext, importedMachine2, dataContext);

            Assert.IsTrue(result2, "Exporting a machine into a part process step doesn't work properly");
            Assert.AreEqual<Guid>(step2.Guid, importedMachine2.ProcessStep.Guid, "Failed to import a machine into the specified entity");
            Assert.IsTrue(importedMachine2.Owner != null && step2.Owner.Guid == importedMachine2.Owner.Guid, "Failed to set the owner at machine importing");
        }

        /// <summary>
        /// Tests Export(object entity, ChangeSet changeSet, string path) method and ImportEntity(string filePath, object objToImportInto, ChangeSet changeSet) 
        /// method using valid data - import/export a part.
        /// </summary>
        [TestMethod]
        public void ExportImportValidPart()
        {
            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.Number = EncryptionManager.Instance.GenerateRandomString(10, true);
            part.Version = 1;
            part.Description = "Description" + DateTime.Now.Ticks;
            part.CalculationStatus = PartCalculationStatus.Approved;
            part.CalculationAccuracy = PartCalculationAccuracy.FineCalculation;
            part.CalculationVariant = "9";
            part.BatchSizePerYear = 10;
            part.YearlyProductionQuantity = 90;
            part.LifeTime = 50;
            part.ManufacturingCountry = "Country" + DateTime.Now.Ticks;
            part.ManufacturingSupplier = "State" + DateTime.Now.Ticks;
            part.PurchasePrice = 1500;
            part.DelivertType = PartDeliveryType.CIF;
            part.AssetRate = 200;
            part.TargetPrice = 2000;
            part.SBMActive = true;
            part.IsExternal = true;
            part.AdditionalRemarks = "Remarks" + DateTime.Now.Ticks;
            part.AdditionalDescription = "Additional Description" + DateTime.Now.Ticks;
            part.ManufacturerDescription = "Manufacturer Description" + DateTime.Now.Ticks;
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();

            // Add media to part.
            Media video = new Media();
            video.Type = MediaType.Video;
            video.Content = new byte[] { 0, 0, 1, 0, 1, 1, 0 };
            video.Size = video.Content.Length;
            video.OriginalFileName = video.Guid.ToString();
            part.Media.Add(video);

            Media document = new Media();
            document.Type = MediaType.Document;
            document.Content = new byte[] { 1, 1, 1 };
            document.Size = document.Content.Length;
            document.OriginalFileName = document.Guid.ToString();
            part.Media.Add(document);

            User user1 = new User();
            user1.Name = user1.Guid.ToString();
            user1.Username = EncryptionManager.Instance.GenerateRandomString(5, true);
            user1.Password = EncryptionManager.Instance.HashSHA256("Password.", user1.Salt, user1.Guid.ToString());
            part.CalculatorUser = user1;
            user1.Roles = Role.Admin;

            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = manufacturer.Guid.ToString();
            manufacturer.Description = "Description" + DateTime.Now.Ticks;
            part.Manufacturer = manufacturer;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MeasurementUnit weightUnit = dataContext.EntityContext.MeasurementUnitSet.FirstOrDefault(m => m.Name.Equals("Gram") && !m.IsReleased);

            Commodity commodity = new Commodity();
            commodity.Name = commodity.Guid.ToString();
            commodity.PartNumber = EncryptionManager.Instance.GenerateRandomString(11, true);
            commodity.Price = 300;
            commodity.Amount = 2;
            commodity.RejectRatio = 1;
            commodity.Weight = 20;
            commodity.WeightUnitBase = weightUnit;
            commodity.Remarks = "Remark" + DateTime.Now.Ticks;
            commodity.Manufacturer = manufacturer.Copy();
            commodity.Manufacturer.Name = commodity.Manufacturer.Guid.ToString();

            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 1, 2, 9, 9, 9 };
            image.OriginalFileName = image.Guid.ToString();
            image.Size = image.Content.Length;
            commodity.Media = image;
            part.Commodities.Add(commodity);

            RawMaterial rawMaterial = new RawMaterial();
            rawMaterial.Name = rawMaterial.Guid.ToString();
            rawMaterial.NameUK = "UK" + DateTime.Now.Ticks;
            rawMaterial.NameUS = "US" + DateTime.Now.Ticks;
            rawMaterial.NormName = "Norm" + DateTime.Now.Ticks;
            rawMaterial.ScrapCalculationType = (short)ScrapCalculationType.Yield;
            rawMaterial.Price = 1200;
            rawMaterial.PriceUnitBase = weightUnit;
            rawMaterial.ParentWeight = 10;
            rawMaterial.ParentWeightUnitBase = weightUnit;
            rawMaterial.Sprue = 1;
            rawMaterial.Remarks = "Remark" + DateTime.Now.Ticks;
            rawMaterial.ScrapRefundRatio = 150;
            rawMaterial.RejectRatio = 1;
            rawMaterial.YieldStrength = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.RuptureStrength = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.Density = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.MaxElongation = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.GlassTransitionTemperature = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.Rx = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.Rm = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.Manufacturer = manufacturer.Copy();
            rawMaterial.Manufacturer.Name = rawMaterial.Manufacturer.Guid.ToString();
            rawMaterial.Media = image.Copy();
            rawMaterial.Media.OriginalFileName = rawMaterial.Media.Guid.ToString();
            part.RawMaterials.Add(rawMaterial);

            RawMaterialDeliveryType deliveryType = new RawMaterialDeliveryType();
            deliveryType.Name = deliveryType.Guid.ToString();
            rawMaterial.DeliveryType = deliveryType;

            MaterialsClassification materialClassificationLevel4 = new MaterialsClassification();
            materialClassificationLevel4.Name = materialClassificationLevel4.Guid.ToString();
            rawMaterial.MaterialsClassificationL4 = materialClassificationLevel4;

            MaterialsClassification materialClassificationLevel3 = new MaterialsClassification();
            materialClassificationLevel3.Name = materialClassificationLevel3.Guid.ToString();
            rawMaterial.MaterialsClassificationL3 = materialClassificationLevel3;

            MaterialsClassification materialClassificationLevel2 = new MaterialsClassification();
            materialClassificationLevel2.Name = materialClassificationLevel2.Guid.ToString();
            rawMaterial.MaterialsClassificationL2 = materialClassificationLevel2;

            MaterialsClassification materialClassificationLevel1 = new MaterialsClassification();
            materialClassificationLevel1.Name = materialClassificationLevel1.Guid.ToString();
            rawMaterial.MaterialsClassificationL1 = materialClassificationLevel1;

            MeasurementUnit timeUnit = dataContext.EntityContext.MeasurementUnitSet.FirstOrDefault(m => m.Name.Equals("Minute") && !m.IsReleased);

            ProcessStep step1 = new PartProcessStep();
            step1.Name = step1.Guid.ToString();
            step1.Accuracy = (short)ProcessCalculationAccuracy.Calculated;
            step1.ProcessTime = 10;
            step1.ProcessTimeUnit = timeUnit;
            step1.PartsPerCycle = 5;
            step1.ScrapAmount = 15;
            step1.Rework = 10;
            step1.ShiftsPerWeek = 12;
            step1.HoursPerShift = 9;
            step1.ProductionDaysPerWeek = 4;
            step1.ProductionWeeksPerYear = 250;
            step1.BatchSize = 100;
            step1.SetupsPerBatch = 25;
            step1.SetupTime = 19;
            step1.SetupTimeUnit = timeUnit;
            step1.MaxDownTime = 23;
            step1.MaxDownTimeUnit = timeUnit;
            step1.ProductionUnskilledLabour = 45;
            step1.ProductionSkilledLabour = 50;
            step1.ProductionForeman = 23;
            step1.ProductionTechnicians = 55;
            step1.ProductionEngineers = 123;
            step1.SetupUnskilledLabour = 235;
            step1.SetupSkilledLabour = 12;
            step1.SetupForeman = 355;
            step1.SetupTechnicians = 124;
            step1.SetupEngineers = 235;
            step1.IsExternal = true;
            step1.Price = 199;
            step1.ExceedShiftCost = true;
            step1.ExtraShiftsNumber = 700;
            step1.ShiftCostExceedRatio = 10;
            step1.Description = "Description" + DateTime.Now.Ticks;
            step1.Media = image.Copy();
            step1.Media.OriginalFileName = step1.Media.Guid.ToString();

            CycleTimeCalculation cycleTimeCalculation = new CycleTimeCalculation();
            cycleTimeCalculation.Description = cycleTimeCalculation.Guid.ToString();
            cycleTimeCalculation.RawPartDescription = "Description" + DateTime.Now.Ticks;
            cycleTimeCalculation.MachiningVolume = 10;
            cycleTimeCalculation.MachiningType = MachiningType.Boring;
            cycleTimeCalculation.ToleranceType = "Tolerance" + DateTime.Now.Ticks;
            cycleTimeCalculation.SurfaceType = "Surface" + DateTime.Now.Ticks;
            cycleTimeCalculation.TransportTimeFromBuffer = 70;
            cycleTimeCalculation.ToolChangeTime = 30;
            cycleTimeCalculation.TransportClampingTime = 15;
            cycleTimeCalculation.MachiningTime = 23;
            cycleTimeCalculation.TakeOffTime = 120;
            cycleTimeCalculation.MachiningCalculationData = new byte[] { 1, 2, 3, 4 };
            step1.CycleTimeCalculations.Add(cycleTimeCalculation);
            step1.CycleTimeUnit = timeUnit;
            step1.CycleTime = 1200;
            part.Process = new Process();
            part.Process.Steps.Add(step1);

            ProcessStepsClassification stepSubtype = new ProcessStepsClassification();
            stepSubtype.Name = stepSubtype.Guid.ToString();
            step1.SubType = stepSubtype;

            ProcessStepsClassification stepType = new ProcessStepsClassification();
            stepType.Name = stepType.Guid.ToString();
            step1.Type = stepType;

            Machine machine = new Machine();
            machine.Name = machine.Guid.ToString();
            machine.ManufacturingYear = 2000;
            machine.DepreciationPeriod = 20;
            machine.DepreciationRate = 10;
            machine.RegistrationNumber = EncryptionManager.Instance.GenerateRandomString(10, true);
            machine.DescriptionOfFunction = EncryptionManager.Instance.GenerateRandomString(15, true);
            machine.FloorSize = 10;
            machine.WorkspaceArea = 25;
            machine.PowerConsumption = 100;
            machine.AirConsumption = 230;
            machine.WaterConsumption = 300;
            machine.FullLoadRate = 50;
            machine.MaxCapacity = 1500;
            machine.OEE = 10;
            machine.Availability = 30;
            machine.MachineInvestment = 20;
            machine.SetupInvestment = 30;
            machine.AdditionalEquipmentInvestment = 230;
            machine.FundamentalSetupInvestment = 45;
            machine.InvestmentRemarks = "Remark" + DateTime.Now.Ticks;
            machine.ExternalWorkCost = 1230;
            machine.MaterialCost = 1000;
            machine.ConsumablesCost = 2300;
            machine.RepairsCost = 2345;
            machine.KValue = 1200;
            machine.CalculateWithKValue = true;
            machine.MountingCubeLength = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MountingCubeWidth = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MountingCubeHeight = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MaxDiameterRodPerChuckMaxThickness = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MaxPartsWeightPerCapacity = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.LockingForce = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.PressCapacity = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MaximumSpeed = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.RapidFeedPerCuttingSpeed = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.StrokeRate = EncryptionManager.Instance.GenerateRandomString(2, true);
            machine.Other = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.Manufacturer = manufacturer.Copy();
            machine.Manufacturer.Name = machine.Manufacturer.Guid.ToString();
            machine.Media = image.Copy();
            machine.Media.OriginalFileName = machine.Media.Guid.ToString();
            step1.Machines.Add(machine);

            // Add a classification level 4 to machine
            MachinesClassification machineClassificationLevel4 = new MachinesClassification();
            machineClassificationLevel4.Name = machineClassificationLevel4.Guid.ToString();
            machine.ClassificationLevel4 = machineClassificationLevel4;

            // Add subtype classification to machine
            MachinesClassification machineSubClassification = new MachinesClassification();
            machineSubClassification.Name = machineSubClassification.Guid.ToString();
            machine.SubClassification = machineSubClassification;

            // Add type classification to machine
            MachinesClassification machineTypeClassification = new MachinesClassification();
            machineTypeClassification.Name = machineTypeClassification.Guid.ToString();
            machine.TypeClassification = machineTypeClassification;

            // Add main classification to machine
            MachinesClassification machineMainClassification = new MachinesClassification();
            machineMainClassification.Name = machineMainClassification.Guid.ToString();
            machine.MainClassification = machineMainClassification;

            Consumable consumable = new Consumable();
            consumable.Name = consumable.Guid.ToString();
            consumable.Description = EncryptionManager.Instance.GenerateRandomString(17, true);
            consumable.Price = 1200;
            consumable.PriceUnitBase = weightUnit;
            consumable.Amount = 12;
            consumable.Manufacturer = manufacturer.Copy();
            consumable.Manufacturer.Name = consumable.Manufacturer.Guid.ToString();
            step1.Consumables.Add(consumable);

            Die die = new Die();
            die.Name = die.Guid.ToString();
            die.Type = (short)DieType.CastingDie;
            die.Investment = 100;
            die.ReusableInvest = 1200;
            die.Wear = 300;
            die.Maintenance = 1200;
            die.LifeTime = 300;
            die.AllocationRatio = 2300;
            die.DiesetsNumberPaidByCustomer = 1300;
            die.CostAllocationBasedOnPartsPerLifeTime = true;
            die.CostAllocationNumberOfParts = 234;
            die.Remark = EncryptionManager.Instance.GenerateRandomString(15, true);
            die.Manufacturer = manufacturer.Copy();
            die.Manufacturer.Name = die.Manufacturer.Guid.ToString();
            die.Media = image.Copy();
            die.Media.OriginalFileName = die.Media.Guid.ToString();
            step1.Dies.Add(die);
            part.SetOwner(user1);

            dataContext.PartRepository.Add(part);
            dataContext.SaveChanges();

            var defaultCurrency = new Currency();
            defaultCurrency.Symbol = "€";
            defaultCurrency.IsoCode = "EUR";
            defaultCurrency.ExchangeRate = 1;
            var currencies = new Collection<Currency>();
            currencies.Add(defaultCurrency);

            string exportDirectoryPath = Path.Combine(Path.GetTempPath(), part.Guid.ToString());
            string exportFilePath = Path.Combine(exportDirectoryPath, part.Name + ".part");
            Directory.CreateDirectory(exportDirectoryPath);
            ExportManager exportManager = new ExportManager();
            exportManager.Export(part, dataContext, exportFilePath, currencies, defaultCurrency);

            User user2 = new User();
            user2.Username = "User" + DateTime.Now.Ticks;
            user2.Password = EncryptionManager.Instance.HashSHA256("Password.", user2.Salt, user2.Guid.ToString()); ;
            user2.Name = "User" + DateTime.Now.Ticks;
            user2.Roles = Role.Admin;

            // Create a project in which will be imported the exported part
            Project project = new Project();
            project.Name = "Test Project" + DateTime.Now.Ticks;
            project.OverheadSettings = new OverheadSetting();
            project.SetOwner(user2);

            // Create an assembly in which will be imported the exported part
            Assembly assembly = new Assembly();
            assembly.Name = assembly.Guid.ToString();
            assembly.OverheadSettings = new OverheadSetting();
            assembly.CountrySettings = new CountrySetting();
            assembly.SetOwner(user2);
            project.Assemblies.Add(assembly);

            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            // Import the exported part into the project
            Part importedPart1 = exportManager.ImportEntity<Part>(exportFilePath, project, dataContext, false).Entity;
            bool result1 = AreEqual(part, dataContext, importedPart1, dataContext);

            Assert.IsTrue(result1, "Importing a part into a project doesn't work properly");
            Assert.AreEqual<Guid>(project.Guid, importedPart1.Project.Guid, "Part wasn't imported into specified entity");
            Assert.IsTrue(importedPart1.Owner != null && project.Owner.Guid == importedPart1.Owner.Guid, "Failed to set the owner at part importing");

            // Import the exported part into the assembly
            Part importedPart2 = exportManager.ImportEntity<Part>(exportFilePath, assembly, dataContext, false).Entity;
            bool result2 = AreEqual(part, dataContext, importedPart2, dataContext);

            Assert.IsTrue(result2, "Importing a part into an assembly doesn't work properly");
            Assert.AreEqual<Guid>(assembly.Guid, importedPart2.Assembly.Guid, "Failed to import a part into the specified entity");
            Assert.IsTrue(importedPart2.Owner != null && assembly.Owner.Guid == importedPart2.Owner.Guid, "Failed to set the owner at part importing");
        }

        /// <summary>
        /// A test for Export(object entity, ChangeSet changeSet, string path) method and ImportEntity(string filePath, object objToImportInto, ChangeSet changeSet)
        /// method using valid data - import/export an assembly
        /// </summary>
        [TestMethod]
        public void ExportImportValidAssembly()
        {
            Assembly assembly = new Assembly();
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();
            assembly.Name = assembly.Guid.ToString();
            assembly.Number = EncryptionManager.Instance.GenerateRandomString(10, true);
            assembly.AssemblingCountry = "Country" + DateTime.Now.Ticks;
            assembly.AssemblingSupplier = "State" + DateTime.Now.Ticks;
            assembly.Version = 2;
            assembly.CalculationAccuracy = PartCalculationAccuracy.FineCalculation;
            assembly.CalculationApproach = PartCalculationApproach.Brownfield;
            assembly.CalculationVariant = EncryptionManager.Instance.GenerateRandomString(7, true);
            assembly.BatchSizePerYear = 120;
            assembly.YearlyProductionQuantity = 1200;
            assembly.LifeTime = 50;
            assembly.PurchasePrice = 2300;
            assembly.DeliveryType = PartDeliveryType.CPT;
            assembly.AssetRate = 123;
            assembly.TargetPrice = 3400;
            assembly.SBMActive = true;
            assembly.IsExternal = true;

            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = manufacturer.Guid.ToString();
            manufacturer.Description = EncryptionManager.Instance.GenerateRandomString(10, true);
            assembly.Manufacturer = manufacturer;

            Media video = new Media();
            video.Type = MediaType.Video;
            video.Content = new byte[] { 1, 2, 3, 4, 5 };
            video.Size = video.Content.Length;
            video.OriginalFileName = video.Guid.ToString();
            assembly.Media.Add(video);

            Media document = new Media();
            document.Type = MediaType.Document;
            document.Content = new byte[] { 1, 2, 3, 4, 5, 6 };
            document.Size = document.Content.Length;
            document.OriginalFileName = document.Guid.ToString();
            assembly.Media.Add(document);

            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 1, 8, 6, 7, 9 };
            image.Size = image.Content.Length;
            image.OriginalFileName = image.Guid.ToString();

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MeasurementUnit timeUnit = dataContext.EntityContext.MeasurementUnitSet.FirstOrDefault(m => m.Name.Equals("Minute") && !m.IsReleased);
            MeasurementUnit weightUnit = dataContext.EntityContext.MeasurementUnitSet.FirstOrDefault(m => m.Name.Equals("Gram") && !m.IsReleased);

            ProcessStep step1 = new AssemblyProcessStep();
            step1.Name = step1.Guid.ToString();
            step1.Accuracy = (short)ProcessCalculationAccuracy.Calculated;
            step1.ProcessTime = 10;
            step1.ProcessTimeUnit = timeUnit;
            step1.PartsPerCycle = 5;
            step1.ScrapAmount = 15;
            step1.Rework = 10;
            step1.ShiftsPerWeek = 12;
            step1.HoursPerShift = 9;
            step1.ProductionDaysPerWeek = 4;
            step1.ProductionWeeksPerYear = 250;
            step1.BatchSize = 100;
            step1.SetupsPerBatch = 25;
            step1.SetupTime = 19;
            step1.SetupTimeUnit = timeUnit;
            step1.MaxDownTime = 23;
            step1.MaxDownTimeUnit = timeUnit;
            step1.ProductionUnskilledLabour = 45;
            step1.ProductionSkilledLabour = 50;
            step1.ProductionForeman = 23;
            step1.ProductionTechnicians = 55;
            step1.ProductionEngineers = 123;
            step1.SetupUnskilledLabour = 235;
            step1.SetupSkilledLabour = 12;
            step1.SetupForeman = 355;
            step1.SetupTechnicians = 124;
            step1.SetupEngineers = 235;
            step1.IsExternal = true;
            step1.Price = 199;
            step1.ExceedShiftCost = true;
            step1.ExtraShiftsNumber = 700;
            step1.ShiftCostExceedRatio = 10;
            step1.Description = "Description" + DateTime.Now.Ticks;
            step1.Media = image;

            CycleTimeCalculation cycleTimeCalculation = new CycleTimeCalculation();
            cycleTimeCalculation.Description = cycleTimeCalculation.Guid.ToString();
            cycleTimeCalculation.RawPartDescription = "Description" + DateTime.Now.Ticks;
            cycleTimeCalculation.MachiningVolume = 10;
            cycleTimeCalculation.MachiningType = MachiningType.Boring;
            cycleTimeCalculation.ToleranceType = "Tolerance" + DateTime.Now.Ticks;
            cycleTimeCalculation.SurfaceType = "Surface" + DateTime.Now.Ticks;
            cycleTimeCalculation.TransportTimeFromBuffer = 70;
            cycleTimeCalculation.ToolChangeTime = 30;
            cycleTimeCalculation.TransportClampingTime = 15;
            cycleTimeCalculation.MachiningTime = 23;
            cycleTimeCalculation.TakeOffTime = 120;
            cycleTimeCalculation.MachiningCalculationData = new byte[] { 1, 2, 3, 4 };
            step1.CycleTimeCalculations.Add(cycleTimeCalculation);
            step1.CycleTimeUnit = timeUnit;
            step1.CycleTime = 1200;
            assembly.Process = new Process();
            assembly.Process.Steps.Add(step1);

            ProcessStepsClassification stepSubtype = new ProcessStepsClassification();
            stepSubtype.Name = stepSubtype.Guid.ToString();
            step1.SubType = stepSubtype;

            ProcessStepsClassification stepType = new ProcessStepsClassification();
            stepType.Name = stepType.Guid.ToString();
            step1.Type = stepType;

            Machine machine = new Machine();
            machine.Name = machine.Guid.ToString();
            machine.ManufacturingYear = 2000;
            machine.DepreciationPeriod = 20;
            machine.DepreciationRate = 10;
            machine.RegistrationNumber = EncryptionManager.Instance.GenerateRandomString(10, true);
            machine.DescriptionOfFunction = EncryptionManager.Instance.GenerateRandomString(15, true);
            machine.FloorSize = 10;
            machine.WorkspaceArea = 25;
            machine.PowerConsumption = 100;
            machine.AirConsumption = 230;
            machine.WaterConsumption = 300;
            machine.FullLoadRate = 50;
            machine.MaxCapacity = 1500;
            machine.OEE = 10;
            machine.Availability = 30;
            machine.MachineInvestment = 20;
            machine.SetupInvestment = 30;
            machine.AdditionalEquipmentInvestment = 230;
            machine.FundamentalSetupInvestment = 45;
            machine.InvestmentRemarks = "Remark" + DateTime.Now.Ticks;
            machine.ExternalWorkCost = 1230;
            machine.MaterialCost = 1000;
            machine.ConsumablesCost = 2300;
            machine.RepairsCost = 2345;
            machine.KValue = 1200;
            machine.CalculateWithKValue = true;
            machine.MountingCubeLength = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MountingCubeWidth = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MountingCubeHeight = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MaxDiameterRodPerChuckMaxThickness = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MaxPartsWeightPerCapacity = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.LockingForce = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.PressCapacity = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MaximumSpeed = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.RapidFeedPerCuttingSpeed = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.StrokeRate = EncryptionManager.Instance.GenerateRandomString(2, true);
            machine.Other = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.Manufacturer = manufacturer.Copy();
            machine.Manufacturer.Name = machine.Manufacturer.Guid.ToString();
            machine.Media = image.Copy();
            machine.Media.OriginalFileName = machine.Media.Guid.ToString();
            step1.Machines.Add(machine);

            // Add a classification level 4 to machine
            MachinesClassification machineClassificationLevel4 = new MachinesClassification();
            machineClassificationLevel4.Name = machineClassificationLevel4.Guid.ToString();
            machine.ClassificationLevel4 = machineClassificationLevel4;

            // Add subtype classification to machine
            MachinesClassification machineSubClassification = new MachinesClassification();
            machineSubClassification.Name = machineSubClassification.Guid.ToString();
            machine.SubClassification = machineSubClassification;

            // Add type classification to machine
            MachinesClassification machineTypeClassification = new MachinesClassification();
            machineTypeClassification.Name = machineTypeClassification.Guid.ToString();
            machine.TypeClassification = machineTypeClassification;

            // Add main classification to machine
            MachinesClassification machineMainClassification = new MachinesClassification();
            machineMainClassification.Name = machineMainClassification.Guid.ToString();
            machine.MainClassification = machineMainClassification;

            Commodity commodity = new Commodity();
            commodity.Name = commodity.Guid.ToString();
            commodity.PartNumber = EncryptionManager.Instance.GenerateRandomString(11, true);
            commodity.Price = 300;
            commodity.Amount = 2;
            commodity.RejectRatio = 1;
            commodity.Weight = 20;
            commodity.WeightUnitBase = weightUnit;
            commodity.Remarks = "Remark" + DateTime.Now.Ticks;
            commodity.Manufacturer = manufacturer.Copy();
            commodity.Manufacturer.Name = commodity.Manufacturer.Guid.ToString();
            commodity.Media = image.Copy();
            commodity.Media.OriginalFileName = commodity.Media.Guid.ToString();
            step1.Commodities.Add(commodity);

            Consumable consumable = new Consumable();
            consumable.Name = consumable.Guid.ToString();
            consumable.Description = EncryptionManager.Instance.GenerateRandomString(17, true);
            consumable.Price = 1200;
            consumable.PriceUnitBase = weightUnit;
            consumable.Amount = 12;
            consumable.Manufacturer = manufacturer.Copy();
            consumable.Manufacturer.Name = consumable.Manufacturer.Guid.ToString();
            step1.Consumables.Add(consumable);

            Die die = new Die();
            die.Name = die.Guid.ToString();
            die.Type = (short)DieType.CastingDie;
            die.Investment = 100;
            die.ReusableInvest = 1200;
            die.Wear = 300;
            die.Maintenance = 1200;
            die.LifeTime = 300;
            die.AllocationRatio = 2300;
            die.DiesetsNumberPaidByCustomer = 1300;
            die.CostAllocationBasedOnPartsPerLifeTime = true;
            die.CostAllocationNumberOfParts = 234;
            die.Remark = EncryptionManager.Instance.GenerateRandomString(15, true);
            die.Manufacturer = manufacturer.Copy();
            die.Manufacturer.Name = die.Manufacturer.Guid.ToString();
            die.Media = image.Copy();
            die.Media.OriginalFileName = die.Media.Guid.ToString();
            step1.Dies.Add(die);

            Assembly assembly2 = new Assembly();
            assembly2.Name = assembly2.Guid.ToString();
            assembly2.Manufacturer = manufacturer.Copy();
            assembly2.Manufacturer.Name = assembly2.Manufacturer.Guid.ToString();
            assembly2.CountrySettings = new CountrySetting();
            assembly2.OverheadSettings = new OverheadSetting();
            assembly.Subassemblies.Add(assembly2);

            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.CountrySettings = new CountrySetting();
            part.OverheadSettings = new OverheadSetting();
            part.Manufacturer = manufacturer.Copy();
            part.Manufacturer.Name = part.Manufacturer.Guid.ToString();
            assembly.Parts.Add(part);

            ProcessStepPartAmount partAmount = new ProcessStepPartAmount();
            partAmount.Part = part;
            partAmount.Amount = 9;
            step1.PartAmounts.Add(partAmount);

            ProcessStepAssemblyAmount assemblyAmount = new ProcessStepAssemblyAmount();
            assemblyAmount.Assembly = assembly2;
            assemblyAmount.Amount = 3;
            step1.AssemblyAmounts.Add(assemblyAmount);

            User user1 = new User();
            user1.Username = user1.Guid.ToString();
            user1.Password = EncryptionManager.Instance.HashSHA256("Password.", user1.Salt, user1.Guid.ToString());
            user1.Name = user1.Guid.ToString();
            assembly.SetOwner(user1);
            user1.Roles = Role.Admin;

            dataContext.AssemblyRepository.Add(assembly);
            dataContext.SaveChanges();

            var defaultCurrency = CurrencyConversionManager.DefaultBaseCurrency;
            var currencies = new Collection<Currency>();
            currencies.Add(defaultCurrency);

            string exportDirectoryPath = Path.Combine(Path.GetTempPath(), assembly.Guid.ToString());
            string exportFilePath = Path.Combine(exportDirectoryPath, assembly.Name + ".assembly");
            Directory.CreateDirectory(exportDirectoryPath);
            ExportManager exportManager = new ExportManager();
            exportManager.Export(assembly, dataContext, exportFilePath, currencies, defaultCurrency);

            User user2 = new User();
            user2.Name = user2.Guid.ToString();
            user2.Username = EncryptionManager.Instance.GenerateRandomString(5, true);
            user2.Password = EncryptionManager.Instance.HashSHA256("Password", user2.Salt, user2.Guid.ToString());
            user2.Roles = Role.Admin;

            // Create a project in which will be imported the exported assembly
            Project project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();
            project.SetOwner(user2);

            // Create an assembly in which will be imported the exported assembly
            Assembly parentAssembly = new Assembly();
            parentAssembly.Name = parentAssembly.Guid.ToString();
            parentAssembly.OverheadSettings = new OverheadSetting();
            parentAssembly.CountrySettings = new CountrySetting();
            parentAssembly.SetOwner(user2);

            project.Assemblies.Add(parentAssembly);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            // Import the exported assembly into a project
            var importedData1 = exportManager.ImportEntity<Assembly>(exportFilePath, project, dataContext, true);
            Assembly importedAssembly1 = importedData1.Entity;
            bool result1 = AreEqual(assembly, dataContext, importedAssembly1, dataContext);

            Assert.IsTrue(result1, "Importing an assembly into a project doesn't work properly");
            Assert.AreEqual<Guid>(project.Guid, importedAssembly1.Project.Guid, "Failed to import an assembly into the specified entity");
            Assert.IsTrue(importedAssembly1.Owner != null && project.Owner.Guid == importedAssembly1.Owner.Guid, "Failed to set the owner for imported assembly");

            // Import the exported assembly into an assembly
            var importedData2 = exportManager.ImportEntity<Assembly>(exportFilePath, parentAssembly, dataContext, true);
            Assembly importedAssembly2 = importedData2.Entity;
            bool result2 = AreEqual(assembly, dataContext, importedAssembly2, dataContext);

            Assert.IsTrue(result2, "Importing an assembly into an assembly doesn't work properly");
            Assert.AreEqual<Guid>(parentAssembly.Guid, importedAssembly2.ParentAssembly.Guid, "Failed to import an assembly into the specifies entity");
            Assert.IsTrue(importedAssembly2.Owner != null && parentAssembly.Owner.Guid == importedAssembly2.Owner.Guid, "Failed to set the owner for imported assembly");
        }

        /// <summary>
        /// A test for Export(object entity, ChangeSet changeSet, string path) method and ImportEntity(string filePath, object objToImportInto, ChangeSet changeSet)
        /// method using valid data - import/export a project
        /// </summary>
        [TestMethod]
        public void ExportImpotValidProject()
        {
            User user1 = new User();
            user1.Username = user1.Guid.ToString();
            user1.Password = EncryptionManager.Instance.HashSHA256("Password.", user1.Salt, user1.Guid.ToString());
            user1.Name = user1.Guid.ToString();
            user1.Roles = Role.Admin;

            Project project = new Project();
            project.Name = project.Guid.ToString();
            project.Number = EncryptionManager.Instance.GenerateRandomString(10, true);
            project.StartDate = DateTime.Now;
            project.EndDate = DateTime.Now;
            project.Status = ProjectStatus.ValidatedInternal;
            project.Partner = "Partner" + DateTime.Now.Ticks;
            project.ProjectLeader = user1;
            project.ResponsibleCalculator = user1;
            project.Version = 1;
            project.ProductName = "Product" + DateTime.Now.Ticks;
            project.ProductNumber = EncryptionManager.Instance.GenerateRandomString(10, true);
            project.YearlyProductionQuantity = 100;
            project.LifeTime = 10;
            project.ProductDescription = "Description" + DateTime.Now.Ticks;
            project.DepreciationPeriod = 10;
            project.DepreciationRate = 20;
            project.LogisticCostRatio = 300;
            project.Remarks = "Remarks" + DateTime.Now.Ticks;
            project.CustomerDescription = "Customer Description" + DateTime.Now.Ticks;
            project.SourceOverheadSettingsLastUpdate = DateTime.Now;
            project.OverheadSettings = new OverheadSetting();

            Media video = new Media();
            video.Type = MediaType.Video;
            video.Content = new byte[] { 1, 2, 3, 4, 5, 6 };
            video.OriginalFileName = video.Guid.ToString();
            video.Size = video.Content.Length;
            project.Media.Add(video);

            Media document = new Media();
            document.Type = MediaType.Document;
            document.Content = new byte[] { 1, 2, 3, 3, 3, 9 };
            document.OriginalFileName = document.Guid.ToString();
            document.Size = document.Content.Length;
            project.Media.Add(document);

            Customer customer = new Customer();
            customer.Name = customer.Guid.ToString();
            customer.Description = "Description" + DateTime.Now.Ticks;
            customer.Type = (short)SupplierType.CorporateGroup;
            customer.Number = EncryptionManager.Instance.GenerateRandomString(9, true);
            customer.StreetAddress = "Street Address" + DateTime.Now.Ticks;
            customer.ZipCode = EncryptionManager.Instance.GenerateRandomString(5, true);
            customer.Country = "Country" + DateTime.Now.Ticks;
            customer.State = "State" + DateTime.Now.Ticks;
            customer.City = "City" + DateTime.Now.Ticks;
            customer.FirstContactName = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer.FirstContactPhone = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer.FirstContactMobile = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer.FirstContactWebAddress = "www.test.com";
            customer.FirstContactEmail = "test@gmail.com";
            customer.FirstContactFax = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer.SecondContactName = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer.SecondContactPhone = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer.SecondContactMobile = EncryptionManager.Instance.GenerateRandomString(10, true);
            customer.SecondContactWebAddress = "www.test.com";
            customer.SecondContactEmail = "test@gmail.com";
            customer.SecondContactFax = EncryptionManager.Instance.GenerateRandomString(10, true);
            project.Customer = customer;
            project.SetOwner(user1);

            Part part = new Part();
            part.Name = part.Guid.ToString();
            part.Number = EncryptionManager.Instance.GenerateRandomString(10, true);
            part.Version = 1;
            part.Description = "Description" + DateTime.Now.Ticks;
            part.CalculationStatus = PartCalculationStatus.Approved;
            part.CalculationAccuracy = PartCalculationAccuracy.FineCalculation;
            part.CalculationVariant = "9";
            part.CalculatorUser = user1;
            part.BatchSizePerYear = 10;
            part.YearlyProductionQuantity = 90;
            part.LifeTime = 50;
            part.Media.Add(video.Copy());
            part.Media.Add(document.Copy());
            part.ManufacturingCountry = "Country" + DateTime.Now.Ticks;
            part.ManufacturingSupplier = "State" + DateTime.Now.Ticks;
            part.PurchasePrice = 1500;
            part.DelivertType = PartDeliveryType.CIF;
            part.AssetRate = 200;
            part.TargetPrice = 2000;
            part.SBMActive = true;
            part.IsExternal = true;
            part.AdditionalRemarks = "Remarks" + DateTime.Now.Ticks;
            part.AdditionalDescription = "Additional Description" + DateTime.Now.Ticks;
            part.ManufacturerDescription = "Manufacturer Description" + DateTime.Now.Ticks;
            part.OverheadSettings = new OverheadSetting();
            part.CountrySettings = new CountrySetting();
            project.Parts.Add(part);

            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = "Manufacturer" + DateTime.Now.Ticks;
            manufacturer.Description = "Description" + DateTime.Now.Ticks;
            part.Manufacturer = manufacturer;

            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            MeasurementUnit weightUnit = dataContext.EntityContext.MeasurementUnitSet.FirstOrDefault(m => m.Name.Equals("Gram") && !m.IsReleased);

            Commodity commodity = new Commodity();
            commodity.Name = commodity.Guid.ToString();
            commodity.PartNumber = EncryptionManager.Instance.GenerateRandomString(11, true);
            commodity.Price = 300;
            commodity.Amount = 2;
            commodity.RejectRatio = 1;
            commodity.Weight = 20;
            commodity.WeightUnitBase = weightUnit;
            commodity.Remarks = "Remark" + DateTime.Now.Ticks;
            commodity.Manufacturer = manufacturer.Copy();
            commodity.Manufacturer.Name = commodity.Manufacturer.Guid.ToString();

            Media image = new Media();
            image.Type = (short)MediaType.Image;
            image.Content = new byte[] { 1, 2, 9, 9, 9 };
            image.OriginalFileName = image.Guid.ToString();
            image.Size = image.Content.Length;
            commodity.Media = image;
            part.Commodities.Add(commodity);

            RawMaterial rawMaterial = new RawMaterial();
            rawMaterial.Name = rawMaterial.Guid.ToString();
            rawMaterial.NameUK = "UK" + DateTime.Now.Ticks;
            rawMaterial.NameUS = "US" + DateTime.Now.Ticks;
            rawMaterial.NormName = "Norm" + DateTime.Now.Ticks;
            rawMaterial.ScrapCalculationType = (short)ScrapCalculationType.Yield;
            rawMaterial.Price = 1200;
            rawMaterial.PriceUnitBase = weightUnit;
            rawMaterial.ParentWeight = 10;
            rawMaterial.ParentWeightUnitBase = weightUnit;
            rawMaterial.Sprue = 1;
            rawMaterial.Remarks = "Remark" + DateTime.Now.Ticks;
            rawMaterial.ScrapRefundRatio = 150;
            rawMaterial.RejectRatio = 1;
            rawMaterial.YieldStrength = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.RuptureStrength = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.Density = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.MaxElongation = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.GlassTransitionTemperature = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.Rx = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.Rm = EncryptionManager.Instance.GenerateRandomString(5, true);
            rawMaterial.Manufacturer = manufacturer.Copy();
            rawMaterial.Manufacturer.Name = rawMaterial.Manufacturer.Guid.ToString();
            rawMaterial.Media = image.Copy();
            rawMaterial.Media.OriginalFileName = rawMaterial.Media.Guid.ToString();
            part.RawMaterials.Add(rawMaterial);

            RawMaterialDeliveryType deliveryType = new RawMaterialDeliveryType();
            deliveryType.Name = deliveryType.Guid.ToString();
            rawMaterial.DeliveryType = deliveryType;

            MaterialsClassification materialClassificationLevel4 = new MaterialsClassification();
            materialClassificationLevel4.Name = materialClassificationLevel4.Guid.ToString();
            rawMaterial.MaterialsClassificationL4 = materialClassificationLevel4;

            MaterialsClassification materialClassificationLevel3 = new MaterialsClassification();
            materialClassificationLevel3.Name = materialClassificationLevel3.Guid.ToString();
            rawMaterial.MaterialsClassificationL3 = materialClassificationLevel3;

            MaterialsClassification materialClassificationLevel2 = new MaterialsClassification();
            materialClassificationLevel2.Name = materialClassificationLevel2.Guid.ToString();
            rawMaterial.MaterialsClassificationL2 = materialClassificationLevel2;

            MaterialsClassification materialClassificationLevel1 = new MaterialsClassification();
            materialClassificationLevel1.Name = materialClassificationLevel1.Guid.ToString();
            rawMaterial.MaterialsClassificationL1 = materialClassificationLevel1;

            MeasurementUnit timeUnit = dataContext.EntityContext.MeasurementUnitSet.FirstOrDefault(m => m.Name.Equals("Minute") && !m.IsReleased);

            ProcessStep step1 = new PartProcessStep();
            step1.Name = step1.Guid.ToString();
            step1.Accuracy = (short)ProcessCalculationAccuracy.Calculated;
            step1.ProcessTime = 10;
            step1.ProcessTimeUnit = timeUnit;
            step1.PartsPerCycle = 5;
            step1.ScrapAmount = 15;
            step1.Rework = 10;
            step1.ShiftsPerWeek = 12;
            step1.HoursPerShift = 9;
            step1.ProductionDaysPerWeek = 4;
            step1.ProductionWeeksPerYear = 250;
            step1.BatchSize = 100;
            step1.SetupsPerBatch = 25;
            step1.SetupTime = 19;
            step1.SetupTimeUnit = timeUnit;
            step1.MaxDownTime = 23;
            step1.MaxDownTimeUnit = timeUnit;
            step1.ProductionUnskilledLabour = 45;
            step1.ProductionSkilledLabour = 50;
            step1.ProductionForeman = 23;
            step1.ProductionTechnicians = 55;
            step1.ProductionEngineers = 123;
            step1.SetupUnskilledLabour = 235;
            step1.SetupSkilledLabour = 12;
            step1.SetupForeman = 355;
            step1.SetupTechnicians = 124;
            step1.SetupEngineers = 235;
            step1.IsExternal = true;
            step1.Price = 199;
            step1.ExceedShiftCost = true;
            step1.ExtraShiftsNumber = 700;
            step1.ShiftCostExceedRatio = 10;
            step1.Description = "Description" + DateTime.Now.Ticks;
            step1.Media = image.Copy();
            step1.Media.OriginalFileName = step1.Media.Guid.ToString();

            CycleTimeCalculation cycleTimeCalculation = new CycleTimeCalculation();
            cycleTimeCalculation.Description = cycleTimeCalculation.Guid.ToString();
            cycleTimeCalculation.RawPartDescription = "Description" + DateTime.Now.Ticks;
            cycleTimeCalculation.MachiningVolume = 10;
            cycleTimeCalculation.MachiningType = MachiningType.Boring;
            cycleTimeCalculation.ToleranceType = "Tolerance" + DateTime.Now.Ticks;
            cycleTimeCalculation.SurfaceType = "Surface" + DateTime.Now.Ticks;
            cycleTimeCalculation.TransportTimeFromBuffer = 70;
            cycleTimeCalculation.ToolChangeTime = 30;
            cycleTimeCalculation.TransportClampingTime = 15;
            cycleTimeCalculation.MachiningTime = 23;
            cycleTimeCalculation.TakeOffTime = 120;
            cycleTimeCalculation.MachiningCalculationData = new byte[] { 1, 2, 3, 4 };
            step1.CycleTimeCalculations.Add(cycleTimeCalculation);
            step1.CycleTimeUnit = timeUnit;
            step1.CycleTime = 1200;
            part.Process = new Process();
            part.Process.Steps.Add(step1);

            ProcessStepsClassification stepSubtype = new ProcessStepsClassification();
            stepSubtype.Name = stepSubtype.Guid.ToString();
            step1.SubType = stepSubtype;

            ProcessStepsClassification stepType = new ProcessStepsClassification();
            stepType.Name = stepType.Guid.ToString();
            step1.Type = stepType;

            Machine machine = new Machine();
            machine.Name = machine.Guid.ToString();
            machine.ManufacturingYear = 2000;
            machine.DepreciationPeriod = 20;
            machine.DepreciationRate = 10;
            machine.RegistrationNumber = EncryptionManager.Instance.GenerateRandomString(10, true);
            machine.DescriptionOfFunction = EncryptionManager.Instance.GenerateRandomString(15, true);
            machine.FloorSize = 10;
            machine.WorkspaceArea = 25;
            machine.PowerConsumption = 100;
            machine.AirConsumption = 230;
            machine.WaterConsumption = 300;
            machine.FullLoadRate = 50;
            machine.MaxCapacity = 1500;
            machine.OEE = 10;
            machine.Availability = 30;
            machine.MachineInvestment = 20;
            machine.SetupInvestment = 30;
            machine.AdditionalEquipmentInvestment = 230;
            machine.FundamentalSetupInvestment = 45;
            machine.InvestmentRemarks = "Remark" + DateTime.Now.Ticks;
            machine.ExternalWorkCost = 1230;
            machine.MaterialCost = 1000;
            machine.ConsumablesCost = 2300;
            machine.RepairsCost = 2345;
            machine.KValue = 1200;
            machine.CalculateWithKValue = true;
            machine.MountingCubeLength = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MountingCubeWidth = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MountingCubeHeight = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MaxDiameterRodPerChuckMaxThickness = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MaxPartsWeightPerCapacity = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.LockingForce = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.PressCapacity = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.MaximumSpeed = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.RapidFeedPerCuttingSpeed = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.StrokeRate = EncryptionManager.Instance.GenerateRandomString(2, true);
            machine.Other = EncryptionManager.Instance.GenerateRandomString(4, true);
            machine.Manufacturer = manufacturer.Copy();
            machine.Manufacturer.Name = machine.Manufacturer.Guid.ToString();
            machine.Media = image.Copy();
            machine.Media.OriginalFileName = machine.Media.Guid.ToString();
            step1.Machines.Add(machine);

            // Add a classification level 4 to machine
            MachinesClassification machineClassificationLevel4 = new MachinesClassification();
            machineClassificationLevel4.Name = machineClassificationLevel4.Guid.ToString();
            machine.ClassificationLevel4 = machineClassificationLevel4;

            // Add subtype classification to machine
            MachinesClassification machineSubClassification = new MachinesClassification();
            machineSubClassification.Name = machineSubClassification.Guid.ToString();
            machine.SubClassification = machineSubClassification;

            // Add type classification to machine
            MachinesClassification machineTypeClassification = new MachinesClassification();
            machineTypeClassification.Name = machineTypeClassification.Guid.ToString();
            machine.TypeClassification = machineTypeClassification;

            // Add main classification to machine
            MachinesClassification machineMainClassification = new MachinesClassification();
            machineMainClassification.Name = machineMainClassification.Guid.ToString();
            machine.MainClassification = machineMainClassification;

            Consumable consumable = new Consumable();
            consumable.Name = consumable.Guid.ToString();
            consumable.Description = EncryptionManager.Instance.GenerateRandomString(17, true);
            consumable.Price = 1200;
            consumable.PriceUnitBase = weightUnit;
            consumable.Amount = 12;
            consumable.Manufacturer = manufacturer.Copy();
            consumable.Manufacturer.Name = consumable.Manufacturer.Guid.ToString();
            step1.Consumables.Add(consumable);

            Die die = new Die();
            die.Name = die.Guid.ToString();
            die.Type = (short)DieType.CastingDie;
            die.Investment = 100;
            die.ReusableInvest = 1200;
            die.Wear = 300;
            die.Maintenance = 1200;
            die.LifeTime = 300;
            die.AllocationRatio = 2300;
            die.DiesetsNumberPaidByCustomer = 1300;
            die.CostAllocationBasedOnPartsPerLifeTime = true;
            die.CostAllocationNumberOfParts = 234;
            die.Remark = EncryptionManager.Instance.GenerateRandomString(15, true);
            die.Manufacturer = manufacturer.Copy();
            die.Manufacturer.Name = die.Manufacturer.Guid.ToString();
            die.Media = image.Copy();
            die.Media.OriginalFileName = die.Media.Guid.ToString();
            step1.Dies.Add(die);

            Assembly assembly = new Assembly();
            assembly.CountrySettings = new CountrySetting();
            assembly.OverheadSettings = new OverheadSetting();
            assembly.Name = assembly.Guid.ToString();
            assembly.Number = EncryptionManager.Instance.GenerateRandomString(10, true);
            assembly.AssemblingCountry = "Country" + DateTime.Now.Ticks;
            assembly.AssemblingSupplier = "State" + DateTime.Now.Ticks;
            assembly.Version = 2;
            assembly.CalculationAccuracy = PartCalculationAccuracy.FineCalculation;
            assembly.CalculationApproach = PartCalculationApproach.Brownfield;
            assembly.CalculationVariant = EncryptionManager.Instance.GenerateRandomString(7, true);
            assembly.BatchSizePerYear = 120;
            assembly.YearlyProductionQuantity = 1200;
            assembly.LifeTime = 50;
            assembly.PurchasePrice = 2300;
            assembly.DeliveryType = PartDeliveryType.CPT;
            assembly.AssetRate = 123;
            assembly.TargetPrice = 3400;
            assembly.SBMActive = true;
            assembly.IsExternal = true;
            assembly.Manufacturer = manufacturer.Copy();
            assembly.Manufacturer.Name = assembly.Manufacturer.Guid.ToString();
            assembly.Media.Add(video.Copy());
            assembly.Media.Add(document.Copy());
            project.Assemblies.Add(assembly);

            ProcessStep step2 = new AssemblyProcessStep();
            step1.CopyValuesTo(step2);
            step2.Name = step2.Guid.ToString();
            step2.CycleTimeUnit = timeUnit;
            step2.SetupTimeUnit = timeUnit;
            step2.MaxDownTimeUnit = timeUnit;
            step2.Media = image.Copy();
            step2.Media.OriginalFileName = step2.Media.Guid.ToString();
            step2.Type = new ProcessStepsClassification();
            step2.Type.Name = step2.Type.Guid.ToString();
            step2.SubType = new ProcessStepsClassification();
            step2.SubType.Name = step2.SubType.Guid.ToString();
            step2.Type.Children.Add(step2.SubType);
            assembly.Process = new Process();
            assembly.Process.Steps.Add(step2);

            CycleTimeCalculation cycleTimeCalculation2 = new CycleTimeCalculation();
            cycleTimeCalculation.CopyValuesTo(cycleTimeCalculation2);
            step2.CycleTimeCalculations.Add(cycleTimeCalculation2);

            Machine machine2 = new Machine();
            machine.CopyValuesTo(machine2);
            machine2.Name = machine2.Guid.ToString();
            machine2.Manufacturer = manufacturer.Copy();
            machine2.Manufacturer.Name = machine2.Manufacturer.Guid.ToString();
            machine2.Media = image.Copy();
            machine2.Media.OriginalFileName = machine2.Media.Guid.ToString();
            step2.Machines.Add(machine2);

            Consumable consumable2 = new Consumable();
            consumable.CopyValuesTo(consumable2);
            consumable2.Name = consumable2.Guid.ToString();
            consumable2.PriceUnitBase = weightUnit;
            consumable2.Manufacturer = manufacturer.Copy();
            consumable2.Manufacturer.Name = consumable2.Manufacturer.Guid.ToString();
            step2.Consumables.Add(consumable2);

            Commodity commodity2 = new Commodity();
            commodity.CopyValuesTo(commodity2);
            commodity2.Name = commodity2.Guid.ToString();
            commodity2.WeightUnitBase = weightUnit;
            commodity2.Media = image.Copy();
            commodity2.Media.OriginalFileName = commodity2.Media.Guid.ToString();
            commodity2.Manufacturer = manufacturer.Copy();
            commodity2.Manufacturer.Name = commodity2.Manufacturer.Guid.ToString();
            step2.Commodities.Add(commodity2);

            Die die2 = new Die();
            die.CopyValuesTo(die2);
            die2.Name = die2.Guid.ToString();
            die2.Manufacturer = manufacturer.Copy();
            die2.Manufacturer.Name = die2.Manufacturer.Guid.ToString();
            step2.Dies.Add(die2);

            Assembly assembly2 = new Assembly();
            assembly2.Name = assembly2.Guid.ToString();
            assembly2.Manufacturer = manufacturer.Copy();
            assembly2.Manufacturer.Name = assembly2.Manufacturer.Guid.ToString();
            assembly2.CountrySettings = new CountrySetting();
            assembly2.OverheadSettings = new OverheadSetting();
            assembly.Subassemblies.Add(assembly2);

            Part part2 = new Part();
            part2.Name = part2.Guid.ToString();
            part2.CountrySettings = new CountrySetting();
            part2.OverheadSettings = new OverheadSetting();
            part2.Manufacturer = manufacturer.Copy();
            part2.Manufacturer.Name = part2.Manufacturer.Guid.ToString();
            assembly.Parts.Add(part2);

            ProcessStepPartAmount partAmount = new ProcessStepPartAmount();
            partAmount.Part = part2;
            partAmount.ProcessStep = step2;
            partAmount.Amount = 9;
            step2.PartAmounts.Add(partAmount);
            part2.ProcessStepPartAmounts.Add(partAmount);

            ProcessStepAssemblyAmount assemblyAmount = new ProcessStepAssemblyAmount();
            assemblyAmount.Assembly = assembly2;
            assembly2.ProcessStepAssemblyAmounts.Add(assemblyAmount);
            assemblyAmount.ProcessStep = step2;
            step2.AssemblyAmounts.Add(assemblyAmount);
            assemblyAmount.Amount = 3;
            project.SetOwner(user1);

            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            var defaultCurrency = CurrencyConversionManager.DefaultBaseCurrency;
            var currencies = new Collection<Currency>();
            currencies.Add(defaultCurrency);

            string exportDirectoryPath = Path.Combine(Path.GetTempPath(), project.Guid.ToString());
            string exportFilePath = Path.Combine(exportDirectoryPath, project.Name + ".project");
            Directory.CreateDirectory(exportDirectoryPath);
            ExportManager exportManager = new ExportManager();
            exportManager.Export(project, dataContext, exportFilePath, currencies, defaultCurrency);

            User user2 = new User();
            user2.Username = user2.Guid.ToString();
            user2.Name = user2.Guid.ToString();
            user2.Password = EncryptionManager.Instance.HashSHA256("Password.", user2.Salt, user2.Guid.ToString());
            user2.Roles = Role.Admin;

            // Create a Folder in which will be imported the project
            ProjectFolder folder = new ProjectFolder();
            folder.Name = folder.Guid.ToString();
            folder.SetOwner(user2);

            dataContext.ProjectFolderRepository.Add(folder);
            dataContext.SaveChanges();

            // Set the current user to 'user2' by logging in the application
            SecurityManager.Instance.Login(user2.Username, "Password.");

            Project importedProject1 = exportManager.ImportEntity<Project>(exportFilePath, folder, dataContext, true).Entity;
            bool result1 = AreEqual(project, dataContext, importedProject1, dataContext);

            Assert.IsTrue(result1, "Importing a project into a folder doesn't work properly");
            Assert.AreEqual<Guid>(folder.Guid, importedProject1.ProjectFolder.Guid, "Failed to import a project into the specified entity");
            Assert.IsTrue(importedProject1.Owner != null && importedProject1.Owner.Guid == folder.Owner.Guid, "Failed to set the Owner of imported project");

            Project importedProject2 = exportManager.ImportEntity<Project>(exportFilePath, null, dataContext, true).Entity;
            bool result2 = AreEqual(project, dataContext, importedProject2, dataContext);

            Assert.IsTrue(result2, "Importing a project in 'My Projects' doesn't work properly");
        }

        /// <summary>
        /// A test for Export(object entity, ChangeSet changeSet, string path) method and ImportEntity(string filePath, object objToImportInto, ChangeSet changeSet)
        /// method using valid data - import/export an assembly
        /// </summary>
        [TestMethod]
        public void ImportWithConfirmationValidAssembly()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var filePath = Environment.CurrentDirectory + "\\Steering Column L320 Manual ROW_1.3_version.assembly";
            bool fileExists = File.Exists(filePath);

            ExportManager exportManager = new ExportManager();

            User user2 = new User();
            user2.Name = user2.Guid.ToString();
            user2.Username = EncryptionManager.Instance.GenerateRandomString(5, true);
            user2.Password = EncryptionManager.Instance.HashSHA256("Password.", user2.Salt, user2.Guid.ToString());
            user2.Roles = Role.Admin;

            // Create a project in which will be imported the assembly
            Project project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();
            project.SetOwner(user2);

            // Create an assembly in which will be imported the exported assembly
            Assembly parentAssembly = new Assembly();
            parentAssembly.Name = parentAssembly.Guid.ToString();
            parentAssembly.OverheadSettings = new OverheadSetting();
            parentAssembly.CountrySettings = new CountrySetting();
            parentAssembly.SetOwner(user2);

            dataContext.ProjectRepository.Add(project);
            dataContext.AssemblyRepository.Add(parentAssembly);
            dataContext.SaveChanges();

            Func<object, bool> func = new Func<object, bool>((o) =>
                {
                    return true;
                });

            // Import the assembly into a project
            var importedData = exportManager.ImportEntity<Assembly>(filePath, project, dataContext, true, func);

            Assembly importedAssembly = importedData.Entity;

            var exist = dataContext.AssemblyRepository.CheckIfExists(importedAssembly.Guid);
            Assert.IsTrue(exist, "Failed to import the assembly");
        }

        /// <summary>
        /// A test for Export(object entity, ChangeSet changeSet, string path) method and ImportEntity(string filePath, object objToImportInto, ChangeSet changeSet)
        /// method using valid data - import/export an assembly
        /// </summary>
        [TestMethod]
        public void ImportWithConfirmationCanceledValidAssembly()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var filePath = Environment.CurrentDirectory + "\\Steering Column L320 Manual ROW.assembly";
            bool fileExists = File.Exists(filePath);

            ExportManager exportManager = new ExportManager();

            User user2 = new User();
            user2.Name = user2.Guid.ToString();
            user2.Username = EncryptionManager.Instance.GenerateRandomString(5, true);
            user2.Password = EncryptionManager.Instance.HashSHA256("Password.", user2.Salt, user2.Guid.ToString());
            user2.Roles = Role.Admin;

            // Create a project in which will be imported the assembly
            Project project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();
            project.SetOwner(user2);

            // Create an assembly in which will be imported the exported assembly
            Assembly parentAssembly = new Assembly();
            parentAssembly.Name = parentAssembly.Guid.ToString();
            parentAssembly.OverheadSettings = new OverheadSetting();
            parentAssembly.CountrySettings = new CountrySetting();
            parentAssembly.SetOwner(user2);

            dataContext.ProjectRepository.Add(project);
            dataContext.AssemblyRepository.Add(parentAssembly);
            dataContext.SaveChanges();

            Func<object, bool> func = new Func<object, bool>((o) =>
            {
                return false;
            });

            // Import the assembly into a project
            var importedData = exportManager.ImportEntity<Assembly>(filePath, project, dataContext, true, func);

            Assembly importedAssembly = importedData.Entity;

            Assert.IsTrue(importedAssembly == null, "The assembly was imported");
        }

        /// <summary>
        /// A test for ImportEntity(string filePath, object objToImportInto, ChangeSet changeSet) for the 1.3 version
        /// method using valid data - import/export an assembly
        /// </summary>
        [TestMethod]
        public void Import1_3VersionWithConfirmationValidAssembly()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var filePath = Environment.CurrentDirectory + "\\Steering Column L320 Manual ROW_1.3_version.assembly";
            bool fileExists = File.Exists(filePath);

            ExportManager exportManager = new ExportManager();

            User user2 = new User();
            user2.Name = user2.Guid.ToString();
            user2.Username = EncryptionManager.Instance.GenerateRandomString(5, true);
            user2.Password = EncryptionManager.Instance.HashSHA256("Password.", user2.Salt, user2.Guid.ToString());
            user2.Roles = Role.Admin;

            // Create an assembly in which will be imported the exported assembly
            var project = new Project();
            project.Name = project.Guid.ToString();
            project.OverheadSettings = new OverheadSetting();

            Assembly parentAssembly = new Assembly();
            parentAssembly.Name = parentAssembly.Guid.ToString();
            parentAssembly.OverheadSettings = new OverheadSetting();
            parentAssembly.CountrySettings = new CountrySetting();
            parentAssembly.SetOwner(user2);

            project.Assemblies.Add(parentAssembly);
            dataContext.ProjectRepository.Add(project);
            dataContext.SaveChanges();

            Func<object, bool> func = new Func<object, bool>((o) =>
            {
                return true;
            });

            // Import the assembly into a project
            var importedData = exportManager.ImportEntity<Assembly>(filePath, parentAssembly, dataContext, true, func);

            Assembly importedAssembly = importedData.Entity;

            var entityExist = dataContext.AssemblyRepository.CheckIfExists(importedAssembly.Guid);

            // Get the first subassembly from parentAssembly that have same id as the imported assembly
            var parentSubassembly = parentAssembly.Subassemblies.Where(a => a.Guid == importedAssembly.Guid).FirstOrDefault();

            Assert.IsTrue(entityExist, "Failed to save to database the assembly");
            Assert.IsTrue(parentSubassembly != null, "Failed to associate assembly with its parent");
        }

        /// <summary>
        /// A test for ImportEntity(string filePath, object objToImportInto, ChangeSet changeSet) for the 1.3 version
        /// method using valid data - import/export an assembly
        /// </summary>
        [TestMethod]
        public void Import1_3VersionWithConfirmationCanceledValidAssembly()
        {
            IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var filePath = Environment.CurrentDirectory + "\\Steering Column L320 Manual ROW_1.3_version.assembly";
            bool fileExists = File.Exists(filePath);

            ExportManager exportManager = new ExportManager();

            User user2 = new User();
            user2.Name = user2.Guid.ToString();
            user2.Username = EncryptionManager.Instance.GenerateRandomString(5, true);
            user2.Password = EncryptionManager.Instance.HashSHA256("Password.", user2.Salt, user2.Guid.ToString());
            user2.Roles = Role.Admin;

            // Create an assembly in which will be imported the exported assembly
            Assembly parentAssembly = new Assembly();
            parentAssembly.Name = parentAssembly.Guid.ToString();
            parentAssembly.OverheadSettings = new OverheadSetting();
            parentAssembly.CountrySettings = new CountrySetting();
            parentAssembly.SetOwner(user2);

            dataContext.AssemblyRepository.Add(parentAssembly);
            dataContext.SaveChanges();

            Func<object, bool> func = new Func<object, bool>((o) =>
            {
                return false;
            });

            // Count the assemblies stored in database before import
            int assembliesNrBefore = dataContext.AssemblyRepository.GetAllMasterData().Count;

            // Import the assembly into a project
            var importedData = exportManager.ImportEntity<Assembly>(filePath, parentAssembly, dataContext, true, func);

            // Count the assemblies stored in database after import
            int assembliesNrAfter = dataContext.AssemblyRepository.GetAllMasterData().Count;

            Assembly importedAssembly = importedData.Entity;

            Assert.IsTrue(importedAssembly == null && assembliesNrBefore == assembliesNrAfter, "The assembly was imported");
        }

        #endregion Test methods

        #region Helpers

        /// <summary>
        /// Compares two projects.
        /// </summary>
        /// <param name="project">The compared project</param>
        /// <param name="projectDataManager">The 'IDataSourceManager' of compared project</param>
        /// <param name="comparedProject">The project to compare with</param>
        /// <param name="comparedProjectDataManager">The 'IDataSourceManager' of project to compare with</param>
        /// <returns>True if projects are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: IsReleased, Owner, Guid, LastChangeTimestamp, IsDeleted.
        /// </remarks>
        private bool AreEqual(
            Project project,
            IDataSourceManager projectDataManager,
            Project comparedProject,
            IDataSourceManager comparedProjectDataManager)
        {
            if (project == null && comparedProject == null)
            {
                return true;
            }

            if ((project == null && comparedProject != null) || (project != null && comparedProject == null))
            {
                return false;
            }

            bool result = string.Equals(project.Name, comparedProject.Name) &&
                          string.Equals(project.Number, comparedProject.Number) &&
                          string.Equals(project.Partner, comparedProject.Partner) &&
                          string.Equals(project.ProductName, comparedProject.ProductName) &&
                          string.Equals(project.ProductNumber, comparedProject.ProductNumber) &&
                          string.Equals(project.ProductDescription, comparedProject.ProductDescription) &&
                          string.Equals(project.CustomerDescription, comparedProject.CustomerDescription) &&
                          string.Equals(project.Remarks, comparedProject.Remarks) &&
                          string.Equals(project.StartDate.GetValueOrDefault().ToShortDateString(), comparedProject.StartDate.GetValueOrDefault().ToShortDateString()) &&
                          string.Equals(project.EndDate.GetValueOrDefault().ToShortDateString(), comparedProject.EndDate.GetValueOrDefault().ToShortDateString()) &&
                          string.Equals(project.SourceOverheadSettingsLastUpdate.GetValueOrDefault().ToShortDateString(), comparedProject.SourceOverheadSettingsLastUpdate.GetValueOrDefault().ToShortDateString()) &&
                          project.Status == comparedProject.Status &&
                          project.YearlyProductionQuantity == comparedProject.YearlyProductionQuantity &&
                          project.LifeTime == comparedProject.LifeTime &&
                          project.DepreciationPeriod == comparedProject.DepreciationPeriod &&
                          project.DepreciationRate == comparedProject.DepreciationRate &&
                          project.LogisticCostRatio == comparedProject.LogisticCostRatio &&
                          project.Version == comparedProject.Version;

            // If projects are not equal => return false.
            if (!result)
            {
                return false;
            }

            // Retrieve the media of project
            Collection<Media> projectMedia = new Collection<Media>(project.Media.ToList());
            if (projectMedia.Count == 0)
            {
                MediaManager mediaManager1 = new MediaManager(projectDataManager);
                projectMedia = mediaManager1.GetAllMedia(project);
            }

            // Retrieve the media of compared project
            Collection<Media> comparedProjectMedia = new Collection<Media>(comparedProject.Media.ToList());
            if (comparedProjectMedia.Count == 0)
            {
                MediaManager mediaManager2 = new MediaManager(comparedProjectDataManager);
                comparedProjectMedia = mediaManager2.GetAllMedia(comparedProject);
            }

            // Verifies medias of projects are equal
            foreach (Media media in projectMedia)
            {
                Media correspondingMedia = comparedProjectMedia.Where(m => string.Equals(m.OriginalFileName, media.OriginalFileName) && m.Content.SequenceEqual(media.Content)).FirstOrDefault();

                result = AreEqual(media, correspondingMedia);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if project leaders are equal.
            result = AreEqual(project.ProjectLeader, comparedProject.ProjectLeader);
            if (!result)
            {
                return false;
            }

            // Verifies if ResponsibleCalculators of projects are equal.
            result = AreEqual(project.ResponsibleCalculator, comparedProject.ResponsibleCalculator);
            if (!result)
            {
                return false;
            }

            // Verifies if Customers of projects are equal.
            result = AreEqual(project.Customer, comparedProject.Customer);
            if (!result)
            {
                return false;
            }

            // Verifies if Overhead settings of projects are equal.
            result = AreEqual(project.OverheadSettings, comparedProject.OverheadSettings);
            if (!result)
            {
                return false;
            }

            // Verifies if parts of projects are equal.
            foreach (Part part in project.Parts)
            {
                Part correspondingPart = comparedProject.Parts.Where(p => string.Equals(p.Name, part.Name)).FirstOrDefault();

                result = AreEqual(part, projectDataManager, correspondingPart, comparedProjectDataManager);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if subassemblies of projects are equal.
            foreach (Assembly assembly in project.Assemblies)
            {
                Assembly correspondingSubassembly = comparedProject.Assemblies.Where(a => string.Equals(a.Name, assembly.Name)).FirstOrDefault();

                result = AreEqual(assembly, projectDataManager, correspondingSubassembly, comparedProjectDataManager);
                if (!result)
                {
                    return false;
                }
            }

            return result;
        }

        /// <summary>
        /// Compares two dies
        /// </summary>
        /// <param name="die">The compared die</param>
        /// <param name="dieChangeSet">The 'ChangeSet' of die</param>
        /// <param name="comparedDie">The die to compare with</param>
        /// <param name="comparedDieChangeSet">The 'ChangeSet' of die to compare with</param>
        /// <returns>True if dies are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner,Guid, IsMasterData and IsDeleted. 
        /// </remarks>
        private bool AreEqual(
            Die die,
            IDataSourceManager dieChangeSet,
            Die comparedDie,
            IDataSourceManager comparedDieChangeSet)
        {
            if (die == null && comparedDie == null)
            {
                return true;
            }

            if ((die == null && comparedDie != null) || (die != null && comparedDie == null))
            {
                return false;
            }

            bool result = string.Equals(die.Name, comparedDie.Name) &&
                          string.Equals(die.Remark, comparedDie.Remark) &&
                          die.Type == comparedDie.Type &&
                          die.Investment == comparedDie.Investment &&
                          die.ReusableInvest == comparedDie.ReusableInvest &&
                          die.Wear == comparedDie.Wear &&
                          die.IsWearInPercentage == comparedDie.IsWearInPercentage &&
                          die.Maintenance == comparedDie.Maintenance &&
                          die.IsMaintenanceInPercentage == comparedDie.IsMaintenanceInPercentage &&
                          die.LifeTime == comparedDie.LifeTime &&
                          die.AllocationRatio == comparedDie.AllocationRatio &&
                          die.DiesetsNumberPaidByCustomer == comparedDie.DiesetsNumberPaidByCustomer &&
                          die.CostAllocationBasedOnPartsPerLifeTime == comparedDie.CostAllocationBasedOnPartsPerLifeTime &&
                          die.CostAllocationNumberOfParts == comparedDie.CostAllocationNumberOfParts;

            // If entities are not equal -> return false.
            if (!result)
            {
                return false;
            }

            // Verifies if manufacturers of dies are equal.
            result = AreEqual(die.Manufacturer, comparedDie.Manufacturer);
            if (!result)
            {
                return false;
            }

            // Retrieve the media of die
            Media dieMedia = die.Media;
            if (dieMedia == null)
            {
                MediaManager mediaManager1 = new MediaManager(dieChangeSet);
                dieMedia = mediaManager1.GetPicture(die);
            }

            // Retrieve the media of compared die
            Media comparedDieMedia = comparedDie.Media;
            if (comparedDieMedia == null)
            {
                MediaManager mediaManager2 = new MediaManager(comparedDieChangeSet);
                comparedDieMedia = mediaManager2.GetPicture(comparedDie);
            }

            // Verifies if medias of dies are equal.
            result = AreEqual(dieMedia, comparedDieMedia);

            return result;
        }

        /// <summary>
        /// Compares two manufacturers
        /// </summary>
        /// <param name="manufacturer">The compared manufacturer</param>
        /// <param name="manufacturerToCompareWith">The manufacturer to compare with</param>
        /// <returns>True if manufacturers are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid.
        /// </remarks>
        private bool AreEqual(Manufacturer manufacturer, Manufacturer manufacturerToCompareWith)
        {
            if (manufacturer == null && manufacturerToCompareWith == null)
            {
                return true;
            }

            if ((manufacturer == null && manufacturerToCompareWith != null) || (manufacturer != null && manufacturerToCompareWith == null))
            {
                return false;
            }

            bool result = string.Equals(manufacturer.Name, manufacturerToCompareWith.Name) &&
                          string.Equals(manufacturer.Description, manufacturerToCompareWith.Description) &&
                          manufacturer.IsMasterData == manufacturerToCompareWith.IsMasterData;

            return result;
        }

        /// <summary>
        /// Compares two medias
        /// </summary>
        /// <param name="media">The compared media</param>
        /// <param name="mediaToCompareWith">The media to compare with</param>
        /// <returns>True if medias are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, IsMasterData.
        /// </remarks>
        private bool AreEqual(Media media, Media mediaToCompareWith)
        {
            if (media == null && mediaToCompareWith == null)
            {
                return true;
            }

            if ((media == null && mediaToCompareWith != null) || (media != null && mediaToCompareWith == null))
            {
                return false;
            }

            bool result = media.Type == mediaToCompareWith.Type &&
                          media.Content.SequenceEqual(mediaToCompareWith.Content) &&
                          media.Size == mediaToCompareWith.Size &&
                          string.Equals(media.OriginalFileName, mediaToCompareWith.OriginalFileName);

            return result;
        }

        /// <summary>
        /// Compares two commodities
        /// </summary>
        /// <param name="commodity">The compared commodity</param>
        /// <param name="commodityChangeSet">The 'ChangeSet' of commodity</param>
        /// <param name="comparedCommodity">The commodity to compare with</param>
        /// <param name="comparedCommodityChangeSet">The 'ChangeSet' of commodity to compare with</param>
        /// <returns>True if commodities are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner,Guid, IsDeleted, IsMasterData.
        /// </remarks>
        private bool AreEqual(
            Commodity commodity,
            IDataSourceManager commodityChangeSet,
            Commodity comparedCommodity,
            IDataSourceManager comparedCommodityChangeSet)
        {
            if (commodity == null && comparedCommodity == null)
            {
                return true;
            }

            if ((commodity == null && comparedCommodity != null) || (commodity != null && comparedCommodity == null))
            {
                return false;
            }

            bool result = string.Equals(commodity.Name, comparedCommodity.Name) &&
                          string.Equals(commodity.Remarks, comparedCommodity.Remarks) &&
                          string.Equals(commodity.PartNumber, comparedCommodity.PartNumber) &&
                          commodity.Price == comparedCommodity.Price &&
                          commodity.Weight == comparedCommodity.Weight &&
                          commodity.RejectRatio == comparedCommodity.RejectRatio &&
                          commodity.Amount == comparedCommodity.Amount;

            // If commodities are not equal -> return false;
            if (!result)
            {
                return false;
            }

            // Verifies if Manufacturers of commodities are equal.
            result = AreEqual(commodity.Manufacturer, comparedCommodity.Manufacturer);
            if (!result)
            {
                return false;
            }

            // Retrieve the commodity's media
            Media commodityMedia = commodity.Media;
            if (commodityMedia == null)
            {
                MediaManager mediaManager1 = new MediaManager(commodityChangeSet);
                commodityMedia = mediaManager1.GetPicture(commodity);
            }

            // Retrieve the compared commodity's media
            Media comparedCommodityMedia = comparedCommodity.Media;
            if (comparedCommodityMedia == null)
            {
                MediaManager mediaManager2 = new MediaManager(comparedCommodityChangeSet);
                comparedCommodityMedia = mediaManager2.GetPicture(comparedCommodity);
            }

            // Verifies if Medias of commodities are equal.
            result = AreEqual(commodityMedia, comparedCommodityMedia);
            if (!result)
            {
                return false;
            }

            // Verifies if WeightUnits of commodities are equal.
            result = AreEqual(commodity.WeightUnitBase, comparedCommodity.WeightUnitBase);

            return result;
        }

        /// <summary>
        /// Compares two consumables
        /// </summary>
        /// <param name="consumable">The compared consumable</param>
        /// <param name="consumableToCompareWith">The consumable to compare with</param>
        /// <returns>True if consumables are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, IsMasterData, IsDeleted.
        /// </remarks>
        private bool AreEqual(Consumable consumable, Consumable consumableToCompareWith)
        {
            if (consumable == null && consumableToCompareWith == null)
            {
                return true;
            }

            if ((consumable == null && consumableToCompareWith != null) || (consumable != null && consumableToCompareWith == null))
            {
                return false;
            }

            bool result = string.Equals(consumable.Name, consumableToCompareWith.Name) &&
                          string.Equals(consumable.Description, consumableToCompareWith.Description) &&
                          consumable.Price == consumableToCompareWith.Price &&
                          consumable.Amount == consumableToCompareWith.Amount;

            // If consumables are not equal -> return false.
            if (!result)
            {
                return false;
            }

            // Verifies if Manufacturers are equal.
            result = AreEqual(consumable.Manufacturer, consumableToCompareWith.Manufacturer);
            if (!result)
            {
                return false;
            }

            // Verifies if PriceUnits are equal.
            result = AreEqual(consumable.PriceUnitBase, consumableToCompareWith.PriceUnitBase);
            if (!result)
            {
                return false;
            }

            // Verifies if AmountUnits are equal.
            result = AreEqual(consumable.AmountUnitBase, consumableToCompareWith.AmountUnitBase);

            return result;
        }

        /// <summary>
        /// Compares two raw materials 
        /// </summary>
        /// <param name="material">The compared raw material</param>
        /// <param name="materialChangeSet">The 'ChangeSet' of raw material</param>
        /// <param name="comparedMaterial">The raw material to compare with</param>
        /// <param name="comparedMaterialChangeSet">The 'ChangeSet' of raw material to compare with</param>
        /// <returns>True if raw materials are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, IsDeleted, IsMasterData.
        /// </remarks>
        private bool AreEqual(
            RawMaterial material,
            IDataSourceManager materialChangeSet,
            RawMaterial comparedMaterial,
            IDataSourceManager comparedMaterialChangeSet)
        {
            if (material == null && comparedMaterial == null)
            {
                return true;
            }

            if ((material == null && comparedMaterial != null) || (material != null && comparedMaterial == null))
            {
                return false;
            }

            bool result = string.Equals(material.Name, comparedMaterial.Name) &&
                          string.Equals(material.NormName, comparedMaterial.NormName) &&
                          string.Equals(material.Remarks, comparedMaterial.Remarks) &&
                          string.Equals(material.NameUK, comparedMaterial.NameUK) &&
                          string.Equals(material.NameUS, comparedMaterial.NameUS) &&
                          material.Price == comparedMaterial.Price &&
                          material.RejectRatio == comparedMaterial.RejectRatio &&
                          material.Remarks == comparedMaterial.Remarks &&
                          material.ParentWeight == comparedMaterial.ParentWeight &&
                          material.Quantity == comparedMaterial.Quantity &&
                          material.ScrapRefundRatio == comparedMaterial.ScrapRefundRatio &&
                          material.Loss == comparedMaterial.Loss &&
                          material.IsScrambled == comparedMaterial.IsScrambled &&
                          material.Sprue == comparedMaterial.Sprue &&
                          material.ScrapCalculationType == comparedMaterial.ScrapCalculationType &&
                          string.Equals(material.YieldStrength, comparedMaterial.YieldStrength) &&
                          string.Equals(material.RuptureStrength, comparedMaterial.RuptureStrength) &&
                          string.Equals(material.Density, comparedMaterial.Density) &&
                          string.Equals(material.MaxElongation, comparedMaterial.MaxElongation) &&
                          string.Equals(material.GlassTransitionTemperature, comparedMaterial.GlassTransitionTemperature) &&
                          string.Equals(material.Rx, comparedMaterial.Rx) &&
                          string.Equals(material.Rm, comparedMaterial.Rm);

            // If raw materials are not equal => return false.
            if (!result)
            {
                return false;
            }

            // Retrieve the material's media
            Media materialMedia = material.Media;
            if (materialMedia == null)
            {
                MediaManager mediaManager1 = new MediaManager(materialChangeSet);
                materialMedia = mediaManager1.GetPicture(material);
            }

            // Retrieve the compared material's media
            Media comparedMaterialMedia = comparedMaterial.Media;
            if (comparedMaterialMedia == null)
            {
                MediaManager mediaManager2 = new MediaManager(comparedMaterialChangeSet);
                comparedMaterialMedia = mediaManager2.GetPicture(comparedMaterial);
            }

            // Verifies if Medias of materials are equal.
            result = AreEqual(materialMedia, comparedMaterialMedia);
            if (!result)
            {
                return false;
            }

            // Verifies if Manufacturers of materials are equal.
            result = AreEqual(material.Manufacturer, comparedMaterial.Manufacturer);
            if (!result)
            {
                return false;
            }

            // Verifies if ClassificationLevel1 of materials are equal.
            result = AreEqual(material.MaterialsClassificationL1, comparedMaterial.MaterialsClassificationL1);
            if (!result)
            {
                return false;
            }

            // Verifies if ClassificationLevel2 of materials are equal.
            result = AreEqual(material.MaterialsClassificationL2, comparedMaterial.MaterialsClassificationL2);
            if (!result)
            {
                return false;
            }

            // Verifies if ClassificationLevel3 of materials are equal.
            result = AreEqual(material.MaterialsClassificationL3, comparedMaterial.MaterialsClassificationL3);
            if (!result)
            {
                return false;
            }

            // Verifies if ClassificationLevel4 of materials are equal.
            result = AreEqual(material.MaterialsClassificationL4, comparedMaterial.MaterialsClassificationL4);
            if (!result)
            {
                return false;
            }

            // Verifies if PriceUnitBase of materials are equal.
            result = AreEqual(material.PriceUnitBase, comparedMaterial.PriceUnitBase);
            if (!result)
            {
                return false;
            }

            // Verifies if ParentWeightUnitBase of materials are equal.
            result = AreEqual(material.ParentWeightUnitBase, comparedMaterial.ParentWeightUnitBase);
            if (!result)
            {
                return false;
            }

            // Verifies if QuantityUnitBase of materials are equal.
            result = AreEqual(material.QuantityUnitBase, comparedMaterial.QuantityUnitBase);
            if (!result)
            {
                return false;
            }

            // Verifies if DeliveryTypes of raw materials are equal.
            result = AreEqual(material.DeliveryType, comparedMaterial.DeliveryType);

            return result;
        }

        /// <summary>
        /// Compares two parts 
        /// </summary>
        /// <param name="part">The compared part</param>
        /// <param name="partChangeSet">The "ChangeSet" of compared part.</param>
        /// <param name="comparedPart">The part to compare with</param>
        /// <param name="comparedPartChangeSet">The 'ChangeSet' of part to compare with</param>
        /// <returns>True if parts are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, IsDeleted, IsMasterData.
        /// </remarks>
        private bool AreEqual(
            Part part,
            IDataSourceManager partChangeSet,
            Part comparedPart,
            IDataSourceManager comparedPartChangeSet)
        {
            if (part == null && comparedPart == null)
            {
                return true;
            }

            if ((part == null && comparedPart != null) || (part != null && comparedPart == null))
            {
                return false;
            }

            bool result = string.Equals(part.Name, comparedPart.Name) &&
                          string.Equals(part.Number, comparedPart.Number) &&
                          string.Equals(part.Description, comparedPart.Description) &&
                          string.Equals(part.AdditionalDescription, comparedPart.AdditionalDescription) &&
                          string.Equals(part.ManufacturerDescription, comparedPart.ManufacturerDescription) &&
                          string.Equals(part.AdditionalRemarks, comparedPart.AdditionalRemarks) &&
                          string.Equals(part.CalculationVariant, comparedPart.CalculationVariant) &&
                          string.Equals(part.ManufacturingCountry, comparedPart.ManufacturingCountry) &&
                          string.Equals(part.ManufacturingSupplier, comparedPart.ManufacturingSupplier) &&
                          part.CalculationStatus == comparedPart.CalculationStatus &&
                          part.CalculationAccuracy == comparedPart.CalculationAccuracy &&
                          part.CalculationApproach == comparedPart.CalculationApproach &&
                          part.BatchSizePerYear == comparedPart.BatchSizePerYear &&
                          part.PurchasePrice == comparedPart.PurchasePrice &&
                          part.TargetPrice == comparedPart.TargetPrice &&
                          part.DelivertType == comparedPart.DelivertType &&
                          part.AssetRate == comparedPart.AssetRate &&
                          part.SBMActive == comparedPart.SBMActive &&
                          part.IsExternal == comparedPart.IsExternal &&
                          part.DevelopmentCost == comparedPart.DevelopmentCost &&
                          part.ProjectInvest == comparedPart.ProjectInvest &&
                          part.OtherCost == comparedPart.OtherCost &&
                          part.PackagingCost == comparedPart.PackagingCost &&
                          part.CalculateLogisticCost == comparedPart.CalculateLogisticCost &&
                          part.LogisticCost == comparedPart.LogisticCost &&                         
                          part.EstimatedCost == comparedPart.EstimatedCost &&
                          part.YearlyProductionQuantity == comparedPart.YearlyProductionQuantity &&
                          part.LifeTime == comparedPart.LifeTime &&
                          part.PaymentTerms == comparedPart.PaymentTerms &&
                          part.Version == comparedPart.Version &&
                          part.Reusable == comparedPart.Reusable &&
                          part.ManufacturingRatio == comparedPart.ManufacturingRatio;

            // If parts are not equal => return false.
            if (!result)
            {
                return false;
            }

            // Retrieve the part's media
            Collection<Media> partMedia = new Collection<Media>(part.Media.ToList());
            if (partMedia.Count == 0)
            {
                MediaManager mediaManager1 = new MediaManager(partChangeSet);
                partMedia = mediaManager1.GetAllMedia(part);
            }

            // Retrieve the comparedPart's media
            Collection<Media> comparedPartMedia = new Collection<Media>(comparedPart.Media.ToList());
            if (comparedPartMedia.Count == 0)
            {
                MediaManager mediaManager2 = new MediaManager(comparedPartChangeSet);
                comparedPartMedia = mediaManager2.GetAllMedia(comparedPart);
            }

            // Verifies if Medias of parts are equal.
            foreach (Media media in partMedia)
            {
                Media correspondingMedia = comparedPartMedia.Where(m => string.Equals(m.OriginalFileName, media.OriginalFileName) && m.Content.SequenceEqual(media.Content)).FirstOrDefault();

                result = AreEqual(media, correspondingMedia);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if commodities of parts are equal.
            foreach (Commodity commodity in part.Commodities)
            {
                Commodity correspondingCommodity = comparedPart.Commodities.Where(c => string.Equals(c.Name, commodity.Name)).FirstOrDefault();

                result = AreEqual(commodity, partChangeSet, correspondingCommodity, comparedPartChangeSet);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if raw materials of parts are equal.
            foreach (RawMaterial material in part.RawMaterials)
            {
                RawMaterial correspondingMaterial = comparedPart.RawMaterials.Where(m => string.Equals(m.Name, material.Name)).FirstOrDefault();

                result = AreEqual(material, partChangeSet, correspondingMaterial, comparedPartChangeSet);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if processes of parts are equals.
            result = AreEqual(part.Process, partChangeSet, comparedPart.Process, comparedPartChangeSet);
            if (!result)
            {
                return false;
            }

            // Verifies if manufacturers of parts are equals.
            result = AreEqual(part.Manufacturer, comparedPart.Manufacturer);
            if (!result)
            {
                return false;
            }

            // Verifies if overhead settings of parts are equals.
            result = AreEqual(part.OverheadSettings, comparedPart.OverheadSettings);
            if (!result)
            {
                return false;
            }

            // Verifies if country settings of parts are equal.
            result = AreEqual(part.CountrySettings, comparedPart.CountrySettings);
            if (!result)
            {
                return false;
            }

            // Verifies if calculators of parts are equal.
            result = AreEqual(part.CalculatorUser, comparedPart.CalculatorUser);

            return result;
        }

        /// <summary>
        /// Compares two assemblies
        /// </summary>
        /// <param name="assembly">The compared assembly</param>
        /// <param name="assemblyChangeSet">The 'ChangeSet' of assembly</param>
        /// <param name="comparedAssembly">The assembly to compare with</param>
        /// <param name="comparedAssemblyChangeSet">The 'ChangeSet' of compared assembly</param>
        /// <returns>True if assemblies are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, LastChangeTimestamp, IsMasterData, IsDeleted.
        /// </remarks>
        private bool AreEqual(
            Assembly assembly,
            IDataSourceManager assemblyChangeSet,
            Assembly comparedAssembly,
            IDataSourceManager comparedAssemblyChangeSet)
        {
            if (assembly == null && comparedAssembly == null)
            {
                return true;
            }

            if ((assembly == null && comparedAssembly != null) || (assembly != null && comparedAssembly == null))
            {
                return false;
            }

            bool result = string.Equals(assembly.Name, comparedAssembly.Name) &&
                          string.Equals(assembly.Number, comparedAssembly.Number) &&
                          string.Equals(assembly.CalculationVariant, comparedAssembly.CalculationVariant) &&
                          string.Equals(assembly.AssemblingCountry, comparedAssembly.AssemblingCountry) &&
                          string.Equals(assembly.AssemblingSupplier, comparedAssembly.AssemblingSupplier) &&
                          assembly.DevelopmentCost == comparedAssembly.DevelopmentCost &&
                          assembly.ProjectInvest == comparedAssembly.ProjectInvest &&
                          assembly.OtherCost == comparedAssembly.OtherCost &&
                          assembly.PackagingCost == comparedAssembly.PackagingCost &&
                          assembly.CalculateLogisticCost == comparedAssembly.CalculateLogisticCost &&
                          assembly.LogisticCost == comparedAssembly.LogisticCost &&                         
                          assembly.BatchSizePerYear == comparedAssembly.BatchSizePerYear &&
                          assembly.YearlyProductionQuantity == comparedAssembly.YearlyProductionQuantity &&
                          assembly.LifeTime == comparedAssembly.LifeTime &&
                          assembly.PaymentTerms == comparedAssembly.PaymentTerms &&
                          assembly.PurchasePrice == comparedAssembly.PurchasePrice &&
                          assembly.TargetPrice == comparedAssembly.TargetPrice &&
                          assembly.DeliveryType == comparedAssembly.DeliveryType &&
                          assembly.SBMActive == comparedAssembly.SBMActive &&
                          assembly.AssetRate == comparedAssembly.AssetRate &&
                          assembly.IsExternal == comparedAssembly.IsExternal &&
                          assembly.CalculationAccuracy == comparedAssembly.CalculationAccuracy &&
                          assembly.EstimatedCost == comparedAssembly.EstimatedCost &&
                          assembly.Version == comparedAssembly.Version &&
                          assembly.Reusable == comparedAssembly.Reusable &&
                          assembly.CalculationApproach == comparedAssembly.CalculationApproach;

            // If assemblies are not equal => return false.
            if (!result)
            {
                return false;
            }

            // Retrieve the assembly's media
            Collection<Media> assemblyMedia = new Collection<Media>(assembly.Media.ToList());
            if (assemblyMedia.Count == 0)
            {
                MediaManager mediaManager1 = new MediaManager(assemblyChangeSet);
                assemblyMedia = mediaManager1.GetAllMedia(assembly);
            }

            // Retrieve the compared assembly's media
            Collection<Media> comparedAssemblyMedia = new Collection<Media>(comparedAssembly.Media.ToList());
            if (comparedAssemblyMedia.Count == 0)
            {
                MediaManager mediaManager2 = new MediaManager(comparedAssemblyChangeSet);
                comparedAssemblyMedia = mediaManager2.GetAllMedia(comparedAssembly);
            }

            // Verifies if Medias of assemblies are equal.
            foreach (Media media in assemblyMedia)
            {
                Media correspondingMedia = comparedAssemblyMedia.Where(m => string.Equals(m.OriginalFileName, media.OriginalFileName) && m.Content.SequenceEqual(media.Content)).FirstOrDefault();

                result = AreEqual(media, correspondingMedia);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if processes of assemblies are equals.
            result = AreEqual(assembly.Process, assemblyChangeSet, comparedAssembly.Process, comparedAssemblyChangeSet);
            if (!result)
            {
                return false;
            }

            // Verifies if manufacturers of assemblies are equals.
            result = AreEqual(assembly.Manufacturer, comparedAssembly.Manufacturer);
            if (!result)
            {
                return false;
            }

            // Verifies if overhead settings of assemblies are equals.
            result = AreEqual(assembly.OverheadSettings, comparedAssembly.OverheadSettings);
            if (!result)
            {
                return false;
            }

            // Verifies if country settings of assemblies are equal.
            result = AreEqual(assembly.CountrySettings, comparedAssembly.CountrySettings);
            if (!result)
            {
                return false;
            }

            // Verifies if parts of assemblies are equal.
            foreach (Part part in assembly.Parts)
            {
                Part correspondingPart = comparedAssembly.Parts.Where(p => string.Equals(p.Name, part.Name)).FirstOrDefault();

                result = AreEqual(part, assemblyChangeSet, correspondingPart, comparedAssemblyChangeSet);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if subassemblies of assemblies are equal.
            foreach (Assembly subassembly in assembly.Subassemblies)
            {
                Assembly correspondingSubassembly = comparedAssembly.Subassemblies.Where(s => string.Equals(s.Name, subassembly.Name)).FirstOrDefault();

                result = AreEqual(subassembly, assemblyChangeSet, correspondingSubassembly, comparedAssemblyChangeSet);
                if (!result)
                {
                    return false;
                }
            }

            return result;
        }

        /// <summary>
        /// Compares two processes
        /// </summary>
        /// <param name="process">The compared process</param>
        /// <param name="processChangeSet">The 'ChangeSet' of compared process</param>
        /// <param name="comparedProcess">The process to compare with</param>
        /// <param name="comparedProcessChangeSet">The 'ChangeSet' of process to compare with</param>
        /// <returns>True if processes are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, IsMasterData.
        /// </remarks>
        private bool AreEqual(
            Process process,
            IDataSourceManager processChangeSet,
            Process comparedProcess,
            IDataSourceManager comparedProcessChangeSet)
        {
            if (process == null && comparedProcess == null)
            {
                return true;
            }

            if ((process == null && comparedProcess != null) || (process != null && comparedProcess == null))
            {
                return false;
            }

            // Verifies if steps of processes are equal.
            bool result = true;
            foreach (ProcessStep step in process.Steps)
            {
                ProcessStep correspondingStep = comparedProcess.Steps.Where(s => string.Equals(s.Name, step.Name)).FirstOrDefault();

                result = AreEqual(step, processChangeSet, correspondingStep, comparedProcessChangeSet);
                if (!result)
                {
                    return false;
                }
            }

            return result;
        }

        /// <summary>
        /// Compares two process steps
        /// </summary>
        /// <param name="step">The compared step</param>
        /// <param name="stepChangeSet">The 'ChangeSet' of compared step</param>
        /// <param name="comparedStep">The step to compare with</param>
        /// <param name="comparedStepChangeSet">The 'ChangeSet' of step to compare with</param>
        /// <returns>True if process steps are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, IsMasterData.
        /// </remarks>
        private bool AreEqual(
            ProcessStep step,
            IDataSourceManager stepChangeSet,
            ProcessStep comparedStep,
            IDataSourceManager comparedStepChangeSet)
        {
            if (step == null && comparedStep == null)
            {
                return true;
            }

            if ((step == null && comparedStep != null) || (step != null && comparedStep == null))
            {
                return false;
            }

            bool result = string.Equals(step.Name, comparedStep.Name) &&
                          string.Equals(step.Description, comparedStep.Description) &&
                          step.Accuracy == comparedStep.Accuracy &&
                          step.CycleTime == comparedStep.CycleTime &&
                          step.ProcessTime == comparedStep.ProcessTime &&
                          step.PartsPerCycle == comparedStep.PartsPerCycle &&
                          step.ScrapAmount == comparedStep.ScrapAmount &&
                          step.Rework == comparedStep.Rework &&
                          step.ShiftsPerWeek == comparedStep.ShiftsPerWeek &&
                          step.HoursPerShift == comparedStep.HoursPerShift &&
                          step.ProductionDaysPerWeek == comparedStep.ProductionDaysPerWeek &&
                          step.ProductionWeeksPerYear == comparedStep.ProductionWeeksPerYear &&
                          step.BatchSize == comparedStep.BatchSize &&
                          step.SetupsPerBatch == comparedStep.SetupsPerBatch &&
                          step.SetupTime == comparedStep.SetupTime &&
                          step.MaxDownTime == comparedStep.MaxDownTime &&
                          step.ProductionUnskilledLabour == comparedStep.ProductionUnskilledLabour &&
                          step.ProductionSkilledLabour == comparedStep.ProductionSkilledLabour &&
                          step.ProductionForeman == comparedStep.ProductionForeman &&
                          step.ProductionTechnicians == comparedStep.ProductionTechnicians &&
                          step.ProductionEngineers == comparedStep.ProductionEngineers &&
                          step.SetupUnskilledLabour == comparedStep.SetupUnskilledLabour &&
                          step.SetupSkilledLabour == comparedStep.SetupSkilledLabour &&
                          step.SetupForeman == comparedStep.SetupForeman &&
                          step.SetupTechnicians == comparedStep.SetupTechnicians &&
                          step.SetupEngineers == comparedStep.SetupEngineers &&
                          step.IsExternal == comparedStep.IsExternal &&
                          step.Price == comparedStep.Price &&
                          step.Index == comparedStep.Index &&
                          step.ExceedShiftCost == comparedStep.ExceedShiftCost &&
                          step.ExtraShiftsNumber == comparedStep.ExtraShiftsNumber &&
                          step.ShiftCostExceedRatio == comparedStep.ShiftCostExceedRatio;

            // If steps are not equal => return false;
            if (!result)
            {
                return false;
            }

            // Verifies if Types of steps are equal.
            result = AreEqual(step.Type, comparedStep.Type);
            if (!result)
            {
                return false;
            }

            // Verifies if SubTypes of steps are equal.
            result = AreEqual(step.SubType, comparedStep.SubType);
            if (!result)
            {
                return false;
            }

            // Retrieve the media of step
            Media stepMedia = step.Media;
            if (stepMedia == null)
            {
                MediaManager mediaManager1 = new MediaManager(stepChangeSet);
                stepMedia = mediaManager1.GetPicture(step);
            }

            // Retrieve the media of comparedStep
            Media comparedStepMedia = comparedStep.Media;
            if (comparedStepMedia == null)
            {
                MediaManager mediaManager2 = new MediaManager(comparedStepChangeSet);
                comparedStepMedia = mediaManager2.GetPicture(comparedStep);
            }

            // Verifies if Medias of steps are equal.
            result = AreEqual(stepMedia, comparedStepMedia);
            if (!result)
            {
                return false;
            }

            // Verifies if CycleTimeUnits of steps are equals.
            result = AreEqual(step.CycleTimeUnit, comparedStep.CycleTimeUnit);
            if (!result)
            {
                return false;
            }

            // Verifies if ProcessStepPartAmounts are equals.
            foreach (ProcessStepPartAmount partAmount in step.PartAmounts)
            {
                ProcessStepPartAmount correspondingPartAmount = comparedStep.PartAmounts.Where(p => string.Equals(p.Part.Name, partAmount.Part.Name)).FirstOrDefault();

                result = AreEqual(partAmount, correspondingPartAmount);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if ProcessStepAssemblyAmount are equals.
            foreach (ProcessStepAssemblyAmount assemblyAmount in step.AssemblyAmounts)
            {
                ProcessStepAssemblyAmount correspondingAssemblyAmount = comparedStep.AssemblyAmounts.Where(p => string.Equals(p.Assembly.Name, assemblyAmount.Assembly.Name)).FirstOrDefault();

                result = AreEqual(assemblyAmount, correspondingAssemblyAmount);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if ProcessTimeUnits are equal.
            result = AreEqual(step.ProcessTimeUnit, comparedStep.ProcessTimeUnit);
            if (!result)
            {
                return false;
            }

            // Verifies if SetupTimeUnits are equals.
            result = AreEqual(step.SetupTimeUnit, comparedStep.SetupTimeUnit);
            if (!result)
            {
                return false;
            }

            // Verifies if MaxDownTimeUnits are equals.
            result = AreEqual(step.MaxDownTimeUnit, comparedStep.MaxDownTimeUnit);
            if (!result)
            {
                return false;
            }

            // Verifies if cycle time calculations are equals.
            foreach (CycleTimeCalculation calculation in step.CycleTimeCalculations)
            {
                CycleTimeCalculation correspondingCalculation = comparedStep.CycleTimeCalculations.Where(c => string.Equals(c.Description, calculation.Description)).FirstOrDefault();

                result = AreEqual(calculation, correspondingCalculation);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if commodities of  steps are equal.
            foreach (Commodity commodity in step.Commodities)
            {
                Commodity correspondingCommodity = comparedStep.Commodities.Where(c => string.Equals(c.Name, commodity.Name)).FirstOrDefault();

                result = AreEqual(commodity, stepChangeSet, correspondingCommodity, comparedStepChangeSet);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if consumables of  steps are equal.
            foreach (Consumable consumable in step.Consumables)
            {
                Consumable correspondingConsumable = comparedStep.Consumables.Where(c => string.Equals(c.Name, consumable.Name)).FirstOrDefault();

                result = AreEqual(consumable, correspondingConsumable);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if machines of  steps are equal.
            foreach (Machine machine in step.Machines)
            {
                Machine correspondingMachine = comparedStep.Machines.Where(c => string.Equals(c.Name, machine.Name)).FirstOrDefault();

                result = AreEqual(machine, stepChangeSet, correspondingMachine, comparedStepChangeSet);
                if (!result)
                {
                    return false;
                }
            }

            // Verifies if dies of  steps are equal.
            foreach (Die die in step.Dies)
            {
                Die correspondingDie = comparedStep.Dies.Where(d => string.Equals(d.Name, die.Name)).FirstOrDefault();

                result = AreEqual(die, stepChangeSet, correspondingDie, comparedStepChangeSet);
                if (!result)
                {
                    return false;
                }
            }

            return result;
        }

        /// <summary>
        /// Compares two cycle time calculations
        /// </summary>
        /// <param name="calculation">The compared cycle time calculation</param>
        /// <param name="calculationToCompareWith">The cycle time calculation to compare with</param>
        /// <returns>True if cycle time calculations are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, IsMasterData.
        /// </remarks>
        private bool AreEqual(CycleTimeCalculation calculation, CycleTimeCalculation calculationToCompareWith)
        {
            if (calculation == null && calculationToCompareWith == null)
            {
                return true;
            }

            if ((calculation == null && calculationToCompareWith != null) || (calculation != null && calculationToCompareWith == null))
            {
                return false;
            }

            bool result = string.Equals(calculation.Description, calculationToCompareWith.Description) &&
                          string.Equals(calculation.RawPartDescription, calculationToCompareWith.RawPartDescription) &&
                          string.Equals(calculation.ToleranceType, calculationToCompareWith.ToleranceType) &&
                          string.Equals(calculation.SurfaceType, calculationToCompareWith.SurfaceType) &&
                          calculation.MachiningVolume == calculationToCompareWith.MachiningVolume &&
                          calculation.MachiningType == calculationToCompareWith.MachiningType &&
                          calculation.TransportTimeFromBuffer == calculationToCompareWith.TransportTimeFromBuffer &&
                          calculation.ToolChangeTime == calculationToCompareWith.ToolChangeTime &&
                          calculation.TransportClampingTime == calculationToCompareWith.TransportClampingTime &&
                          calculation.MachiningTime == calculationToCompareWith.MachiningTime &&
                          calculation.TakeOffTime == calculationToCompareWith.TakeOffTime &&
                          calculation.Index == calculationToCompareWith.Index &&
                          calculation.MachiningCalculationData.SequenceEqual(calculationToCompareWith.MachiningCalculationData);

            return result;
        }

        /// <summary>
        /// Compares two process step part amounts
        /// </summary>
        /// <param name="partAmount">The compared process step part amount</param>
        /// <param name="partAmountToComapareWith">The process step part amount to compare with</param>
        /// <returns>True if process step part amounts are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, IsMasterData.
        /// </remarks>
        private bool AreEqual(ProcessStepPartAmount partAmount, ProcessStepPartAmount partAmountToComapareWith)
        {
            if (partAmount == null && partAmountToComapareWith == null)
            {
                return true;
            }

            if ((partAmount == null && partAmountToComapareWith != null) || (partAmount != null && partAmountToComapareWith == null))
            {
                return false;
            }

            bool result = partAmount.Amount == partAmountToComapareWith.Amount &&
                          string.Equals(partAmount.ProcessStep.Name, partAmountToComapareWith.ProcessStep.Name) &&
                          string.Equals(partAmount.Part.Name, partAmountToComapareWith.Part.Name);

            return result;
        }

        /// <summary>
        /// Compares two process steps classifications
        /// </summary>
        /// <param name="classification">The compared process step classification</param>
        /// <param name="classificationToCompareWith">The process step classification to compare with</param>
        /// <returns>True if process step classifications are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: IsReleased, Guid.
        /// </remarks>
        private bool AreEqual(ProcessStepsClassification classification, ProcessStepsClassification classificationToCompareWith)
        {
            if (classification == null && classificationToCompareWith == null)
            {
                return true;
            }

            if ((classification == null && classificationToCompareWith != null) || (classification != null && classificationToCompareWith == null))
            {
                return false;
            }

            bool result = string.Equals(classification.Name, classificationToCompareWith.Name);

            return result;
        }

        /// <summary>
        /// Compares two process step assembly amounts
        /// </summary>
        /// <param name="assemblyAmount">The compared process step assembly amount</param>
        /// <param name="assemblyAmountToComapareWith">The process step assembly amount to compare with</param>
        /// <returns>True if process step assembly amounts are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, IsMasterData.
        /// </remarks>
        private bool AreEqual(ProcessStepAssemblyAmount assemblyAmount, ProcessStepAssemblyAmount assemblyAmountToComapareWith)
        {
            if (assemblyAmount == null && assemblyAmountToComapareWith == null)
            {
                return true;
            }

            if ((assemblyAmount == null && assemblyAmountToComapareWith != null) || (assemblyAmount != null && assemblyAmountToComapareWith == null))
            {
                return false;
            }

            bool result = assemblyAmount.Amount == assemblyAmountToComapareWith.Amount &&
                          string.Equals(assemblyAmount.ProcessStep.Name, assemblyAmountToComapareWith.ProcessStep.Name) &&
                          string.Equals(assemblyAmount.Assembly.Name, assemblyAmountToComapareWith.Assembly.Name);

            return result;
        }

        /// <summary>
        /// Compares two overhead settings
        /// </summary>
        /// <param name="setting">The compared overhead setting</param>
        /// <param name="settingToCompareWith">The overhead setting to compare with</param>
        /// <returns>True if overhead settings are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, LastUpdateTime, IsMasterData.
        /// </remarks>
        private bool AreEqual(OverheadSetting setting, OverheadSetting settingToCompareWith)
        {
            if (setting == null && settingToCompareWith == null)
            {
                return true;
            }

            if ((setting == null && settingToCompareWith != null) || (setting != null && settingToCompareWith == null))
            {
                return false;
            }

            bool result = setting.MaterialOverhead == settingToCompareWith.MaterialOverhead &&
                          setting.ConsumableOverhead == settingToCompareWith.ConsumableOverhead &&
                          setting.CommodityOverhead == settingToCompareWith.CommodityOverhead &&
                          setting.ExternalWorkOverhead == settingToCompareWith.ExternalWorkOverhead &&
                          setting.ManufacturingOverhead == settingToCompareWith.ManufacturingOverhead &&
                          setting.MaterialMargin == settingToCompareWith.MaterialMargin &&
                          setting.ConsumableMargin == settingToCompareWith.ConsumableMargin &&
                          setting.CommodityMargin == settingToCompareWith.CommodityMargin &&
                          setting.ExternalWorkMargin == settingToCompareWith.ExternalWorkMargin &&
                          setting.ManufacturingMargin == settingToCompareWith.ManufacturingMargin &&
                          setting.OtherCostOHValue == settingToCompareWith.OtherCostOHValue &&
                          setting.PackagingOHValue == settingToCompareWith.PackagingOHValue &&
                          setting.LogisticOHValue == settingToCompareWith.LogisticOHValue &&
                          setting.SalesAndAdministrationOHValue == settingToCompareWith.SalesAndAdministrationOHValue;

            return result;
        }

        /// <summary>
        /// Compares two country settings
        /// </summary>
        /// <param name="countrySetting">The compared country setting</param>
        /// <param name="countrySettingToCompareWith">The country setting to compare with</param>
        /// <returns>True if country settings are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid.
        /// </remarks>
        private bool AreEqual(CountrySetting countrySetting, CountrySetting countrySettingToCompareWith)
        {
            if (countrySetting == null && countrySettingToCompareWith == null)
            {
                return true;
            }

            if ((countrySetting == null && countrySettingToCompareWith != null) || (countrySetting != null && countrySettingToCompareWith == null))
            {
                return false;
            }

            bool result = countrySetting.UnskilledLaborCost == countrySettingToCompareWith.UnskilledLaborCost &&
                          countrySetting.SkilledLaborCost == countrySettingToCompareWith.SkilledLaborCost &&
                          countrySetting.ForemanCost == countrySettingToCompareWith.ForemanCost &&
                          countrySetting.TechnicianCost == countrySettingToCompareWith.TechnicianCost &&
                          countrySetting.EngineerCost == countrySettingToCompareWith.EngineerCost &&
                          countrySetting.EnergyCost == countrySettingToCompareWith.EnergyCost &&
                          countrySetting.AirCost == countrySettingToCompareWith.AirCost &&
                          countrySetting.WaterCost == countrySettingToCompareWith.WaterCost &&
                          countrySetting.ProductionAreaRentalCost == countrySettingToCompareWith.ProductionAreaRentalCost &&
                          countrySetting.OfficeAreaRentalCost == countrySettingToCompareWith.OfficeAreaRentalCost &&
                          countrySetting.InterestRate == countrySettingToCompareWith.InterestRate &&
                          countrySetting.ShiftCharge1ShiftModel == countrySettingToCompareWith.ShiftCharge1ShiftModel &&
                          countrySetting.ShiftCharge2ShiftModel == countrySettingToCompareWith.ShiftCharge2ShiftModel &&
                          countrySetting.ShiftCharge3ShiftModel == countrySettingToCompareWith.ShiftCharge3ShiftModel &&
                          countrySetting.LaborAvailability == countrySettingToCompareWith.LaborAvailability &&
                          countrySetting.IsScrambled == countrySettingToCompareWith.IsScrambled;

            return result;
        }

        /// <summary>
        /// Compares two countries
        /// </summary>
        /// <param name="country">The compared country</param>
        /// <param name="countryToCompareWith">The country to compare with</param>
        /// <returns>True if countries are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: IsReleased, Guid.
        /// </remarks>
        private bool AreEqual(Country country, Country countryToCompareWith)
        {
            if (country == null && countryToCompareWith == null)
            {
                return true;
            }

            if ((country == null && countryToCompareWith != null) || (country != null && countryToCompareWith == null))
            {
                return false;
            }

            bool result = string.Equals(country.Name, countryToCompareWith.Name) &&
                          AreEqual(country.Currency, countryToCompareWith.Currency) &&
                          AreEqual(country.WeightMeasurementUnit, countryToCompareWith.WeightMeasurementUnit) &&
                          AreEqual(country.LengthMeasurementUnit, countryToCompareWith.LengthMeasurementUnit) &&
                          AreEqual(country.VolumeMeasurementUnit, country.VolumeMeasurementUnit) &&
                          AreEqual(country.TimeMeasurementUnit, countryToCompareWith.TimeMeasurementUnit) &&
                          AreEqual(country.FloorMeasurementUnit, countryToCompareWith.FloorMeasurementUnit) &&
                          AreEqual(country.CountrySetting, countryToCompareWith.CountrySetting);

            return result;
        }

        /// <summary>
        /// Compares two users
        /// </summary>
        /// <param name="user">The compared user</param>
        /// <param name="userToCompareWith">The user to compare with</param>
        /// <returns>True if medias are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: IsReleased, Guid.
        /// </remarks>
        private bool AreEqual(User user, User userToCompareWith)
        {
            if (user == null && userToCompareWith == null)
            {
                return true;
            }

            if ((user == null && userToCompareWith != null) || (user != null && userToCompareWith == null))
            {
                return false;
            }

            bool result = string.Equals(user.Name, userToCompareWith.Name) &&
                          string.Equals(user.Description, userToCompareWith.Description) &&
                          string.Equals(user.Username, userToCompareWith.Username) &&
                          string.Equals(user.Password, userToCompareWith.Password) &&
                          user.Disabled == userToCompareWith.Disabled;

            // If entities are not equal -> return false.
            if (!result)
            {
                return false;
            }

            result = AreEqual(user.UICurrency, userToCompareWith.UICurrency);

            return result;
        }

        /// <summary>
        /// Compares two customers
        /// </summary>
        /// <param name="customer">The compared customer</param>
        /// <param name="customerToCompareWith">The customer to compare with</param>
        /// <returns>True if customers are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, IsMasterData, Guid.
        /// </remarks>
        private bool AreEqual(Customer customer, Customer customerToCompareWith)
        {
            if (customer == null && customerToCompareWith == null)
            {
                return true;
            }

            if ((customer == null && customerToCompareWith != null) || (customer != null && customerToCompareWith == null))
            {
                return false;
            }

            bool result = string.Equals(customer.Name, customerToCompareWith.Name) &&
                          string.Equals(customer.Description, customerToCompareWith.Description) &&
                          string.Equals(customer.Number, customerToCompareWith.Number) &&
                          string.Equals(customer.StreetAddress, customerToCompareWith.StreetAddress) &&
                          string.Equals(customer.ZipCode, customerToCompareWith.ZipCode) &&
                          string.Equals(customer.City, customerToCompareWith.City) &&
                          string.Equals(customer.FirstContactName, customerToCompareWith.FirstContactName) &&
                          string.Equals(customer.FirstContactEmail, customerToCompareWith.FirstContactEmail) &&
                          string.Equals(customer.FirstContactPhone, customerToCompareWith.FirstContactPhone) &&
                          string.Equals(customer.FirstContactFax, customerToCompareWith.FirstContactFax) &&
                          string.Equals(customer.FirstContactMobile, customerToCompareWith.FirstContactMobile) &&
                          string.Equals(customer.FirstContactWebAddress, customerToCompareWith.SecondContactWebAddress) &&
                          string.Equals(customer.SecondContactName, customerToCompareWith.SecondContactName) &&
                          string.Equals(customer.SecondContactEmail, customerToCompareWith.SecondContactEmail) &&
                          string.Equals(customer.SecondContactPhone, customerToCompareWith.SecondContactPhone) &&
                          string.Equals(customer.SecondContactFax, customerToCompareWith.SecondContactFax) &&
                          string.Equals(customer.SecondContactMobile, customerToCompareWith.SecondContactMobile) &&
                          string.Equals(customer.SecondContactWebAddress, customerToCompareWith.SecondContactWebAddress) &&
                          string.Equals(customer.Country, customerToCompareWith.Country) &&
                          string.Equals(customer.State, customerToCompareWith.State) &&
                          customer.Type == customerToCompareWith.Type;

            return result;
        }

        /// <summary>
        /// compares two measurement units
        /// </summary>
        /// <param name="measurementUnit">The compared measurement unit</param>
        /// <param name="measurementUnitToCompareWith">The measurement unit to compare with</param>
        /// <returns>True if measurement units are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: IsReleased, Guid.
        /// </remarks>
        private bool AreEqual(MeasurementUnit measurementUnit, MeasurementUnit measurementUnitToCompareWith)
        {
            if (measurementUnit == null && measurementUnitToCompareWith == null)
            {
                return true;
            }

            if ((measurementUnit == null && measurementUnitToCompareWith != null) || (measurementUnit != null && measurementUnitToCompareWith == null))
            {
                return false;
            }

            bool result = string.Equals(measurementUnit.Name, measurementUnitToCompareWith.Name) &&
                          string.Equals(measurementUnit.Symbol, measurementUnitToCompareWith.Symbol) &&
                          measurementUnit.ConversionRate == measurementUnitToCompareWith.ConversionRate &&
                          measurementUnit.Type == measurementUnitToCompareWith.Type &&
                          measurementUnit.ScaleID == measurementUnitToCompareWith.ScaleID &&
                          measurementUnit.ScaleFactor == measurementUnitToCompareWith.ScaleFactor;

            return result;
        }

        /// <summary>
        /// Compares two materials classifications 
        /// </summary>
        /// <param name="classification">The compared material classification</param>
        /// <param name="classificationToCompareWith">The material classification to compare with</param>
        /// <returns>True if materials classifications are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: IsReleased, Guid.
        /// </remarks>
        private bool AreEqual(MaterialsClassification classification, MaterialsClassification classificationToCompareWith)
        {
            if (classification == null && classificationToCompareWith == null)
            {
                return true;
            }

            if ((classification == null && classificationToCompareWith != null) || (classification != null && classificationToCompareWith == null))
            {
                return false;
            }

            bool result = string.Equals(classification.Name, classificationToCompareWith.Name);

            return result;
        }

        /// <summary>
        /// Compares two raw material delivery types
        /// </summary>
        /// <param name="deliveryType">The compared raw material delivery type</param>
        /// <param name="deliveryTypeToCompareWith">The raw material delivery type to compare with</param>
        /// <returns>True if raw material delivery types are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: IsReleased, Guid.
        /// </remarks>
        private bool AreEqual(RawMaterialDeliveryType deliveryType, RawMaterialDeliveryType deliveryTypeToCompareWith)
        {
            if (deliveryType == null && deliveryTypeToCompareWith == null)
            {
                return true;
            }

            if ((deliveryType == null && deliveryTypeToCompareWith != null) || (deliveryType != null && deliveryTypeToCompareWith == null))
            {
                return false;
            }

            bool result = string.Equals(deliveryType.Name, deliveryTypeToCompareWith.Name);

            return result;
        }

        /// <summary>
        /// Compares two machines
        /// </summary>
        /// <param name="machine">The compared machine</param>
        /// <param name="machineChangeSet">The 'ChangeSet' of the machine</param>
        /// <param name="comparedMachine">The machine to compare with</param>
        /// <param name="comparedMachineChangeSet">The 'ChangeSet' of machine to compare with</param>
        /// <returns>True if machines are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: Owner, Guid, Index, IsDeleted, IsMasterData.
        /// </remarks>
        private bool AreEqual(
            Machine machine,
            IDataSourceManager machineChangeSet,
            Machine comparedMachine,
            IDataSourceManager comparedMachineChangeSet)
        {
            if (machine == null && comparedMachine == null)
            {
                return true;
            }

            if ((machine == null && comparedMachine != null) || (machine != null && comparedMachine == null))
            {
                return false;
            }

            bool result = string.Equals(machine.Name, comparedMachine.Name) &&
                          string.Equals(machine.RegistrationNumber, comparedMachine.RegistrationNumber) &&
                          string.Equals(machine.DescriptionOfFunction, comparedMachine.DescriptionOfFunction) &&
                          string.Equals(machine.InvestmentRemarks, comparedMachine.InvestmentRemarks) &&
                          string.Equals(machine.MountingCubeLength, comparedMachine.MountingCubeLength) &&
                          string.Equals(machine.MountingCubeWidth, comparedMachine.MountingCubeWidth) &&
                          string.Equals(machine.MountingCubeHeight, comparedMachine.MountingCubeHeight) &&
                          string.Equals(machine.MaxDiameterRodPerChuckMaxThickness, comparedMachine.MaxDiameterRodPerChuckMaxThickness) &&
                          string.Equals(machine.MaxPartsWeightPerCapacity, comparedMachine.MaxPartsWeightPerCapacity) &&
                          string.Equals(machine.LockingForce, comparedMachine.LockingForce) &&
                          string.Equals(machine.PressCapacity, comparedMachine.PressCapacity) &&
                          string.Equals(machine.MaximumSpeed, comparedMachine.MaximumSpeed) &&
                          string.Equals(machine.RapidFeedPerCuttingSpeed, comparedMachine.RapidFeedPerCuttingSpeed) &&
                          string.Equals(machine.StrokeRate, comparedMachine.StrokeRate) &&
                          string.Equals(machine.Other, comparedMachine.Other) &&
                          machine.ManufacturingYear == comparedMachine.ManufacturingYear &&
                          machine.FloorSize == comparedMachine.FloorSize &&
                          machine.WorkspaceArea == comparedMachine.WorkspaceArea &&
                          machine.PowerConsumption == comparedMachine.PowerConsumption &&
                          machine.AirConsumption == comparedMachine.AirConsumption &&
                          machine.WaterConsumption == comparedMachine.WaterConsumption &&
                          machine.FullLoadRate == comparedMachine.FullLoadRate &&
                          machine.MaxCapacity == comparedMachine.MaxCapacity &&
                          machine.OEE == comparedMachine.OEE &&
                          machine.Availability == comparedMachine.Availability &&
                          machine.MachineInvestment == comparedMachine.MachineInvestment &&
                          machine.SetupInvestment == comparedMachine.SetupInvestment &&
                          machine.AdditionalEquipmentInvestment == comparedMachine.AdditionalEquipmentInvestment &&
                          machine.FundamentalSetupInvestment == comparedMachine.FundamentalSetupInvestment &&
                          machine.ExternalWorkCost == comparedMachine.ExternalWorkCost &&
                          machine.MaterialCost == comparedMachine.MaterialCost &&
                          machine.ConsumablesCost == comparedMachine.ConsumablesCost &&
                          machine.RepairsCost == comparedMachine.RepairsCost &&
                          machine.KValue == comparedMachine.KValue &&
                          machine.CalculateWithKValue == comparedMachine.CalculateWithKValue &&
                          machine.IsScrambled == comparedMachine.IsScrambled &&
                          machine.ManualConsumableCostCalc == comparedMachine.ManualConsumableCostCalc &&
                          machine.ManualConsumableCostPercentage == comparedMachine.ManualConsumableCostPercentage &&
                          machine.IsProjectSpecific == comparedMachine.IsProjectSpecific &&
                          machine.DepreciationPeriod == comparedMachine.DepreciationPeriod &&
                          machine.DepreciationRate == comparedMachine.DepreciationRate;

            // If machines are not equal -> return false.
            if (!result)
            {
                return false;
            }

            // Retrieve the media of machine
            Media machineMedia = machine.Media;
            if (machineMedia == null)
            {
                MediaManager mediaManager1 = new MediaManager(machineChangeSet);
                machineMedia = mediaManager1.GetPictureOrVideo(machine);
            }

            // Retrieve the media of the machine to compare with 
            Media comparedMachineMedia = comparedMachine.Media;
            if (comparedMachineMedia == null)
            {
                MediaManager mediaManager2 = new MediaManager(comparedMachineChangeSet);
                comparedMachineMedia = mediaManager2.GetPictureOrVideo(comparedMachine);
            }

            // Verifies if Medias are equal.
            result = AreEqual(machineMedia, comparedMachineMedia);
            if (!result)
            {
                return false;
            }

            // Verifies if Manufacturers are equal.
            result = AreEqual(machine.Manufacturer, comparedMachine.Manufacturer);
            if (!result)
            {
                return false;
            }

            // Verifies if main classifications are equal
            result = AreEqual(machine.MainClassification, comparedMachine.MainClassification);
            if (!result)
            {
                return false;
            }

            // Verifies if type classifications are equal.
            result = AreEqual(machine.TypeClassification, comparedMachine.TypeClassification);
            if (!result)
            {
                return false;
            }

            // Verifies if sub classifications are equal.
            result = AreEqual(machine.SubClassification, comparedMachine.SubClassification);
            if (!result)
            {
                return false;
            }

            // Verifies if classifications level4 are equal.
            result = AreEqual(machine.ClassificationLevel4, comparedMachine.ClassificationLevel4);

            return result;
        }

        /// <summary>
        /// Compares two machines classifications
        /// </summary>
        /// <param name="classification">The compared machine classification</param>
        /// <param name="classificationToCompareWith">The machine classification to compare with</param>
        /// <returns>True if machines classifications are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: IsReleased, Guid.
        /// </remarks>
        private bool AreEqual(MachinesClassification classification, MachinesClassification classificationToCompareWith)
        {
            if (classification == null && classificationToCompareWith == null)
            {
                return true;
            }

            if ((classification == null && classificationToCompareWith != null) || (classification != null && classificationToCompareWith == null))
            {
                return false;
            }

            bool result = string.Equals(classification.Name, classificationToCompareWith.Name);

            return result;
        }

        /// <summary>
        /// Compares two currencies
        /// </summary>
        /// <param name="currency">The compared currency</param>
        /// <param name="currencyToCompareWith">The currency to compare with</param>
        /// <returns>True if currencies are equal, false otherwise</returns>
        /// <remarks>
        /// The following properties are not included in comparison: IsReleased, Guid.
        /// </remarks>
        private bool AreEqual(Currency currency, Currency currencyToCompareWith)
        {
            if (currency == null && currencyToCompareWith == null)
            {
                return true;
            }

            if ((currency == null && currencyToCompareWith != null) || (currency != null && currencyToCompareWith == null))
            {
                return false;
            }

            bool result = currency.Name == currencyToCompareWith.Name
                && currency.Symbol == currencyToCompareWith.Symbol
                && currency.ExchangeRate == currencyToCompareWith.ExchangeRate
                && currency.IsoCode == currencyToCompareWith.IsoCode;

            return result;
        }

        #endregion Helpers
    }
}
