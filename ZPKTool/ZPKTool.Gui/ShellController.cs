using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Globalization;
using System.Windows;
using ZPKTool.Common;
using ZPKTool.Controls;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.ViewModels;
using ZPKTool.Gui.Views;
using ZPKTool.LicenseValidator;
using ZPKTool.MvvmCore.Services;
using ZPKTool.Updater;

namespace ZPKTool.Gui
{
    /// <summary>
    /// The application controller used to glue together the UI components
    /// </summary>    
    //// TODO: consider eliminating this class.
    [Export]
    public class ShellController
    {
        /// <summary>
        /// The composition container that contains all UI parts.
        /// </summary>
        private CompositionContainer container;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The view model of the app shell.
        /// </summary>
        private ShellViewModel shellViewModel;

        /// <summary>
        /// The application's version;
        /// </summary>
        private Version appVersion;

        /// <summary>
        /// Initializes a new instance of the <see cref="ShellController"/> class.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="windowService">The window service.</param>
        [ImportingConstructor]
        public ShellController(CompositionContainer container, IWindowService windowService)
        {
            this.windowService = windowService;
            if (container == null)
            {
                throw new ArgumentNullException("container");
            }

            this.container = container;
            appVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
            string currentVersion = appVersion.ToString();

            // TODO: maybe we should write a service wrapper for the update manager, in case it is needed in other places.
            this.UpdateManager = new UpdateManager(currentVersion);
            this.UpdateManager.UpdateFound += (s, e) =>
                {
                    IMessenger messenger = container.GetExportedValue<IMessenger>();
                    messenger.Send<NotificationMessage>(new NotificationMessage(Notification.ShowUpdateAvailable));
                };
        }

        /// <summary>
        /// Gets or sets the update manager.
        /// </summary>        
        public UpdateManager UpdateManager { get; set; }

        /// <summary>
        /// Set UI language.
        /// </summary>
        /// <param name="cultureName">The ISO code of the language.</param>
        public static void SetUILanguage(string cultureName)
        {
            System.Globalization.CultureInfo culture = null;
            if (!string.IsNullOrWhiteSpace(cultureName))
            {
                culture = new System.Globalization.CultureInfo(cultureName);
            }
            else
            {
                culture = System.Globalization.CultureInfo.InvariantCulture;
            }

            // Set the culture for UI thread
            Application.Current.Dispatcher.Thread.CurrentUICulture = culture;

            // set the culture for the language resource to the specific culture because it may be used from a thread which is not the UI thread
            ZPKTool.Gui.Resources.LocalizedResources.Culture = culture;
            ZPKTool.Business.Resources.LocalizedResources.Culture = culture;
        }

        /// <summary>
        /// Initializes the application and displays the shell.
        /// </summary>
        /// <param name="modelInfo">The information for the model viewer startup.</param>
        public void Run(ModelViewerStartupInfo modelInfo)
        {
            if (modelInfo.StartInViewerMode)
            {
                var modelBrowserViewModel = this.container.GetExportedValue<ModelBrowserViewModel>();
                if (!string.IsNullOrEmpty(modelInfo.ModelFilePath))
                {
                    modelBrowserViewModel.ModelFilePathToBrowse = modelInfo.ModelFilePath;
                }

                this.windowService.ShowViewInWindow(modelBrowserViewModel, handleEscKey: false, singleInstance: true);

                return;
            }

            LicenseInfo license = null;
            LicenseValidation validator = new LicenseValidation(
                ZPKTool.Business.SecurityManager.LicensePublicKey,
                ZPKTool.Business.SecurityManager.LicenseFilePath);

            try
            {
                // Perform the license check before showing the shell             
                license = validator.ValidateLicense(appVersion);
            }
            catch (ZPKTool.LicenseValidator.LicenseException ex)
            {
                if (ex.ErrorCode == LicenseErrorCodes.NoNetworkAdapter)
                {
                    this.windowService.MessageDialogService.Show(ex);
                    Application.Current.Shutdown();
                    return;
                }
            }

            // If the application is not registered yet, show the register view.
            if (license == null)
            {
                var viewModel = this.container.GetExportedValue<RegisterViewModel>();
                var windowService = this.container.GetExportedValue<IWindowService>();
                windowService.ShowViewInDialog(viewModel);

                // Perform the license check again to see if it was registered above.
                try
                {
                    license = validator.ValidateLicense(appVersion);
                }
                catch (ZPKTool.LicenseValidator.LicenseException)
                {
                }
            }

            // If the application is registered show the shell
            if (license != null)
            {
                this.shellViewModel = this.container.GetExportedValue<ShellViewModel>();
                ShellView view = new ShellView();
                Application.Current.MainWindow = view;
                view.DataContext = shellViewModel;
                view.Show();

                // Use it only one time at the application startup.
                this.BackgroundLicenseCheck();

                // Check if a warning for license expiration should be displayed, and show an info message for it.
                this.CheckLicenseExpirationDate(license);
            }
            else if (Application.Current != null)
            {
                Application.Current.Shutdown();
            }
        }

        /// <summary>
        /// Performs a license check in the background.
        /// This check obtains the current date and time from time servers, and the execution time depends on which time server from the list is up and running.
        /// Used only one time at the application startup.
        /// </summary>
        private void BackgroundLicenseCheck()
        {
            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += (s, e) =>
            {
                ZPKTool.LicenseValidator.LicenseValidation.RefreshCurrentDateTime();
            };

            worker.RunWorkerCompleted += (s, e) =>
            {
                try
                {
                    LicenseValidation validator = new LicenseValidation(
                        ZPKTool.Business.SecurityManager.LicensePublicKey,
                        ZPKTool.Business.SecurityManager.LicenseFilePath);
                    validator.ValidateLicense(appVersion);
                }
                catch (ZPKTool.LicenseValidator.LicenseException)
                {
                    // If the application is not registered yet show the register view
                    var viewModel = this.container.GetExportedValue<RegisterViewModel>();
                    this.windowService.ShowViewInDialog(viewModel);
                }
            };

            worker.RunWorkerAsync();
        }

        /// <summary>
        /// Displays an info message when the days remaining until the license expiration date are less then 14.
        /// </summary>
        /// <param name="license">The license to check.</param>
        private void CheckLicenseExpirationDate(LicenseInfo license)
        {
            var now = DateTime.Now.Date;
            double daysRemaining = (license.ExpirationDate.Date - now).Days;

            if (daysRemaining <= 14)
            {
                double daysSinceLastWarning = (now - UserSettingsManager.Instance.LastLicenseExpirationWarningDate.Date).Days;
                if (daysSinceLastWarning > 0)
                {
                    string messageToDisplay = string.Format(
                        CultureInfo.CurrentCulture,
                        LocalizedResources.License_WarningBeforeExpiration,
                        license.ExpirationDate.ToShortDateString());

                    // If the license will expire in 2 weeks, display a warning only once, in that week.
                    if (daysRemaining > 7)
                    {
                        if (daysSinceLastWarning >= 7)
                        {
                            UserSettingsManager.Instance.LastLicenseExpirationWarningDate = now;
                            UserSettingsManager.Instance.Save();

                            this.ShowLicenseExpiringWarningMessage(messageToDisplay, license.SerialNumber);
                        }
                    }
                    else
                    {
                        // If the license will expire in 1 week, display the warning daily.
                        if (daysSinceLastWarning != 0)
                        {
                            UserSettingsManager.Instance.LastLicenseExpirationWarningDate = now;
                            UserSettingsManager.Instance.Save();
                            this.ShowLicenseExpiringWarningMessage(messageToDisplay, license.SerialNumber);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Shows the license expiring warning message dialog to user
        /// </summary>
        /// <param name="messageToDisplay">The message to display</param>
        /// <param name="licenseSerialNumber">The license serial number for which to request a new license, if the user chooses to request a new license.</param>
        private void ShowLicenseExpiringWarningMessage(string messageToDisplay, string licenseSerialNumber)
        {
            var itemsCollection = new Collection<MessageDialogActionableItem>();
            var action = new Action(() =>
            {
                LicenseRequestViewModel requestLicenseViewModel = new LicenseRequestViewModel(this.windowService, licenseSerialNumber);
                this.windowService.ShowViewInDialog(requestLicenseViewModel);
            });

            itemsCollection.Add(new MessageDialogActionableItem(LocalizedResources.License_RequestNewLicense, action, null, MessageDialogActionableItemType.Button));
            itemsCollection.Add(new MessageDialogActionableItem(LocalizedResources.General_Close, null, null, MessageDialogActionableItemType.Button));

            MessageDialogInfo dialogInfo = new MessageDialogInfo(LocalizedResources.License_ExpireSoon, messageToDisplay, Images.WarningIconKey, itemsCollection);
            this.windowService.MessageDialogService.Show(dialogInfo);
        }
    }
}
