﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.Notifications
{
    /// <summary>
    /// A message requesting a master data entity to be edited.
    /// </summary>
    public class EditMasterDataEntityMessage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EditMasterDataEntityMessage"/> class.
        /// </summary>
        /// <param name="entity">The master data entity which is loaded</param>
        public EditMasterDataEntityMessage(object entity)
        {
            this.Entity = entity;
        }

        /// <summary>
        /// Gets the entity.
        /// </summary>        
        public object Entity { get; private set; }
    }
}
