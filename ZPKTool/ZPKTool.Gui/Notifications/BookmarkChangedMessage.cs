﻿namespace ZPKTool.Gui.Notifications
{
    /// <summary>
    /// The types of notifications sent to signal bookmark related changes.
    /// </summary>
    public enum BookmarkChangeType
    {
        /// <summary>
        /// A bookmark was created.
        /// </summary>
        BookmarkCreated = 0,

        /// <summary>
        /// A bookmark was deleted.
        /// </summary>
        BookmarkDeleted = 1,
    }

    /// <summary>
    /// A message signaling that a bookmark from the bookmarks tree has changed.
    /// </summary>
    public class BookmarkChangedMessage : ZPKTool.MvvmCore.Services.MessageBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BookmarkChangedMessage"/> class.
        /// </summary>
        public BookmarkChangedMessage()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BookmarkChangedMessage"/> class.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="changeType">Type of the change.</param>
        public BookmarkChangedMessage(object entity, BookmarkChangeType changeType)
        {
            this.Entity = entity;
            this.ChangeType = changeType;
        }

        /// <summary>
        /// Gets or sets the entity that changed.
        /// </summary>        
        public object Entity { get; set; }

        /// <summary>
        /// Gets or sets the type of the change.
        /// </summary>        
        public BookmarkChangeType ChangeType { get; set; }
    }
}