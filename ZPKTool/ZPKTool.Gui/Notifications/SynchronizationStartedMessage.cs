﻿namespace ZPKTool.Gui.Notifications
{
    /// <summary>
    /// A message signaling that the synchronization has started.
    /// </summary>
    public class SynchronizationStartedMessage
    {
    }
}
