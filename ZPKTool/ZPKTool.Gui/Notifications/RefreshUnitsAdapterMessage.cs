﻿using ZPKTool.DataAccess;

namespace ZPKTool.Gui.Notifications
{
    /// <summary>
    /// A message singaling to refresh the units adapter using the specified data source manager.
    /// </summary>
    public class RefreshUnitsAdapterMessage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RefreshUnitsAdapterMessage" /> class.
        /// </summary>
        /// <param name="dsm">The data source manager.</param>
        public RefreshUnitsAdapterMessage(IDataSourceManager dsm)
        {
            this.DataSourceManager = dsm;
        }

        /// <summary>
        /// Gets the data source manager.
        /// </summary>        
        public IDataSourceManager DataSourceManager { get; private set; }
    }
}