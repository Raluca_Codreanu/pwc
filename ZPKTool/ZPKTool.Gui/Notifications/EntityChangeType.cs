﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui
{
    /// <summary>
    /// The types of notifications sent between the UI components.
    /// </summary>
    public enum EntityChangeType
    {
        /// <summary>
        /// Unknown change.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// An entity was created.
        /// </summary>
        EntityCreated = 1,

        /// <summary>
        /// An entity was updated (had some of its properties changed).
        /// </summary>
        EntityUpdated = 2,

        /// <summary>
        /// An entity was deleted.
        /// </summary>
        EntityDeleted = 3,

        /// <summary>
        /// An entity was imported.
        /// </summary>
        EntityImported = 4,

        /// <summary>
        /// An entity was moved.
        /// </summary>
        EntityMoved = 5,

        /// <summary>
        /// An entity was recovered from the trash bin.
        /// </summary>
        EntityRecovered = 6,

        /// <summary>
        /// An entity was permanently deleted from the database.
        /// </summary>
        EntityPermanentlyDeleted = 7,

        /// <summary>
        /// The overhead settings or a project were updated.
        /// </summary>        
        OverheadSettingsUpdated = 8,

        /// <summary>
        /// This change type is sent when data must be refreshed from its associated DataContext because it was changed using a different DataContext instance.
        /// </summary>        
        RefreshData = 9,

        /// <summary>
        /// An entity was reordered.
        /// </summary>
        EntityReordered = 10
    }
}
