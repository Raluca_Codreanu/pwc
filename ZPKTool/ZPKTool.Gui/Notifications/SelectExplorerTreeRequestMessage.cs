﻿using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Notifications
{
    /// <summary>
    /// A message requesting a specified projects explorer tree to be selected (Projects Tree for example).
    /// </summary>
    public class SelectExplorerTreeRequestMessage : MessageBase
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="SelectExplorerTreeRequestMessage"/> class from being created.
        /// </summary>
        private SelectExplorerTreeRequestMessage()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SelectExplorerTreeRequestMessage"/> class.
        /// </summary>
        /// <param name="treeToSelect">A value indicating which explorer tree to select.</param>
        public SelectExplorerTreeRequestMessage(ExplorerTree treeToSelect)
        {
            this.TreeToSelect = treeToSelect;
        }

        /// <summary>
        /// Gets a value indicating what explorer tree to be selected.
        /// </summary>        
        public ExplorerTree TreeToSelect { get; private set; }
    }
}