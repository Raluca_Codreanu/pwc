﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.DataAccess;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Notifications
{
    /// <summary>
    /// A message signaling that an entity has been imported.
    /// </summary>
    public class EntityImportedMessage : NotificationMessage
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityImportedMessage"/> class.
        /// </summary>
        /// <param name="entityType">The type of the imported entity.</param>
        public EntityImportedMessage(ImportedEntityType entityType)
            : base(string.Empty)
        {
            this.EntityType = entityType;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the type of the imported entity.
        /// </summary>
        public ImportedEntityType EntityType { get; set; }

        #endregion
    }
}
