﻿namespace ZPKTool.Gui.Notifications
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Business;

    /// <summary>
    /// A message that signals when the online state of the central database has changed.
    /// </summary>
    public class OnlineStateChangedMessage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OnlineStateChangedMessage"/> class.
        /// </summary>
        /// <param name="newState">The new new online state of the central database (true - online, false - offline) </param>
        public OnlineStateChangedMessage(bool newState)
        {
            IsOnline = newState;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OnlineStateChangedMessage"/> class.
        /// </summary>
        /// <param name="checkResult">The online check result.</param>
        public OnlineStateChangedMessage(DbOnlineCheckResult checkResult)
        {
            if (checkResult != null)
            {
                IsOnline = checkResult.IsOnline;
                OfflineReason = checkResult.OfflineReason;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the central database is online.
        /// </summary>        
        public bool IsOnline { get; private set; }

        /// <summary>
        /// Gets or sets the the reason why the database is offline, when <see cref="IsOnline"/> is false.
        /// </summary>        
        public DbOfflineReason OfflineReason { get; set; }
    }
}
