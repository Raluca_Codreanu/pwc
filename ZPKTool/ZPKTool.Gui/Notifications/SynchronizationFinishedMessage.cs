﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Synchronization;

namespace ZPKTool.Gui.Notifications
{
    /// <summary>
    /// A message signaling that the synchronization has finished.
    /// </summary>
    public class SynchronizationFinishedMessage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SynchronizationFinishedMessage"/> class.
        /// </summary>
        /// <param name="completedTasks">The completed tasks.</param>
        public SynchronizationFinishedMessage(IEnumerable<SynchronizationTask> completedTasks)
        {
            if (completedTasks == null)
            {
                this.CompletedTasks = new List<SynchronizationTask>();
            }
            else
            {
                this.CompletedTasks = new List<SynchronizationTask>(completedTasks);
            }
        }

        /// <summary>
        /// Gets the completed synchronization tasks.
        /// </summary>        
        public IEnumerable<SynchronizationTask> CompletedTasks { get; private set; }
    }
}
