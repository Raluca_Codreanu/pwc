﻿namespace ZPKTool.Gui.Notifications
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using ZPKTool.Data;

    /// <summary>
    /// A message signaling that an entity media collection has changed.
    /// </summary>
    public class MediaChangedMessage : ZPKTool.MvvmCore.Services.NotificationMessage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MediaChangedMessage"/> class.
        /// </summary>
        public MediaChangedMessage()
            : base(null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MediaChangedMessage"/> class.
        /// </summary>
        /// <param name="notification">A string containing any arbitrary message to be passed to recipient(s)</param>
        public MediaChangedMessage(string notification)
            : base(notification)
        {
        }

        /// <summary>
        /// Gets or sets the entity changed.
        /// </summary>
        public object Entity { get; set; }

        /// <summary>
        /// Gets or sets the current media list.
        /// </summary>
        public List<object> Media { get; set; }

        /// <summary>
        /// Gets or sets the type of the change.
        /// </summary>        
        public MediaChangeType ChangeType { get; set; }
    }
}
