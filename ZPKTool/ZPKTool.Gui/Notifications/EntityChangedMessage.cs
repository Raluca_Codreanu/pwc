﻿using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Notifications
{
    /// <summary>
    /// A message signaling that an entity from the main tree has changed
    /// </summary>
    public class EntityChangedMessage : NotificationMessage
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityChangedMessage"/> class.
        /// </summary>
        public EntityChangedMessage()
            : base(null)
        {
            this.SelectEntity = true;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityChangedMessage"/> class.
        /// </summary>
        /// <param name="notification">A string containing any arbitrary message to be passed to recipient(s)</param>
        public EntityChangedMessage(string notification)
            : base(notification)
        {
            this.SelectEntity = true;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityChangedMessage"/> class.
        /// </summary>
        /// <param name="notification">A string containing any arbitrary message to be passed to recipient(s)</param>
        /// <param name="entity">The entity.</param>
        /// <param name="parent">The entity's parent.</param>
        /// <param name="changeType">Type of the change.</param>
        public EntityChangedMessage(string notification, object entity, object parent, EntityChangeType changeType)
            : this(notification)
        {
            this.Entity = entity;
            this.Parent = parent;
            this.ChangeType = changeType;
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Gets or sets the entity that changed.
        /// </summary>        
        public object Entity { get; set; }

        /// <summary>
        /// Gets or sets the parent of the entity.
        /// </summary>        
        public object Parent { get; set; }

        /// <summary>
        /// Gets or sets the type of the change.
        /// </summary>        
        public EntityChangeType ChangeType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the entity should be selected by the handlers of the message.
        /// <para />
        /// What "entity is selected" means depends on each handler of the message.
        /// </summary>        
        public bool SelectEntity { get; set; }

        #endregion Properties
    }
}
