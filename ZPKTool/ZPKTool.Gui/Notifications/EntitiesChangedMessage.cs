﻿namespace ZPKTool.Gui.Notifications
{
    using System.Collections.Generic;

    /// <summary>
    /// A message signaling that multiple entities from the main tree have changed.
    /// </summary>
    public class EntitiesChangedMessage
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="EntitiesChangedMessage"/> class.
        /// </summary>
        /// <param name="changes">The EntityChanged messages list.</param>
        public EntitiesChangedMessage(IEnumerable<EntityChangedMessage> changes)
        {
            this.Changes = changes;
        }

        #endregion Constructor

        #region Properties

        /// <summary>
        /// Gets the EntityChanged messages.
        /// </summary>
        public IEnumerable<EntityChangedMessage> Changes { get; private set; }

        #endregion Properties
    }
}
