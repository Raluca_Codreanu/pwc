﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui
{
    /// <summary>
    /// The types of notifications sent when Process Step changed.
    /// </summary>
    public enum ProcessStepChangeType
    {
        /// <summary>
        /// Unknown change.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Process step name change.
        /// </summary>
        NameUpdated = 1,

        /// <summary>
        /// Sub-entity name updated.
        /// </summary>
        SubEntityNameUpdated = 2,

        /// <summary>
        /// Sub-entity added.
        /// </summary>
        SubEntityAdded = 3,

        /// <summary>
        /// Sub-entity deleted.
        /// </summary>
        SubEntityDeleted = 4,

        /// <summary>
        /// Step item updated.
        /// </summary>
        StepItemUpdated = 5,

        /// <summary>
        /// Sub-entity moved.
        /// </summary>
        SubEntityMoved = 6
    }
}
