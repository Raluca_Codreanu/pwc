﻿namespace ZPKTool.Gui.Notifications
{
    /// <summary>
    /// A message requesting the update of a the ReadonlyDocumentContent tab from the application MainDocuments avalon dock pane.
    /// </summary>
    public class UpdateReadonlyDocumentContentMessage
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateReadonlyDocumentContentMessage"/> class.
        /// </summary>
        /// <param name="content">The MainDocuments ReadonlyDocument Tab content.</param>
        public UpdateReadonlyDocumentContentMessage(object content)
        {
            this.Content = content;
        }

        #endregion Constructor

        #region Properties

        /// <summary>
        /// Gets the content of the ReadonlyDocumentContent tab.
        /// </summary>        
        public object Content { get; private set; }

        /// <summary>
        /// Gets or sets the ReadonlyDocumentContent tab title.
        /// </summary>        
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets some supplementary data. 
        /// </summary>
        public object AdditionalData { get; set; }

        #endregion Properties
    }
}
