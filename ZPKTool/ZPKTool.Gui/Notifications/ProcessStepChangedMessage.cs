﻿namespace ZPKTool.Gui.Notifications
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;    
    using ZPKTool.Data;

    /// <summary>
    /// A message signaling that a process step data data has changed.
    /// </summary>
    public class ProcessStepChangedMessage : ZPKTool.MvvmCore.Services.NotificationMessage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessStepChangedMessage"/> class.
        /// </summary>
        public ProcessStepChangedMessage()
            : base(null)
        {
            this.AdditionalEntities = new List<object>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessStepChangedMessage"/> class.
        /// </summary>
        /// <param name="notification">A string containing any arbitrary message to be passed to recipient(s)</param>
        public ProcessStepChangedMessage(string notification)
            : base(notification)
        {
            this.AdditionalEntities = new List<object>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessStepChangedMessage"/> class.
        /// </summary>
        /// <param name="sender">The message's sender.</param>
        public ProcessStepChangedMessage(object sender)
            : base(sender, string.Empty)
        {
            this.AdditionalEntities = new List<object>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessStepChangedMessage"/> class.
        /// </summary>
        /// <param name="sender">The message's sender.</param>
        /// <param name="target">The message's intended target. This parameter can be used
        /// to give an indication as to whom the message was intended for. Of course
        /// this is only an indication, amd may be null.</param>
        public ProcessStepChangedMessage(object sender, object target)
            : base(sender, target, string.Empty)
        {
            this.AdditionalEntities = new List<object>();
        }

        /// <summary>
        /// Gets or sets the process step Guid.
        /// </summary>
        public Guid StepID { get; set; }

        /// <summary>
        /// Gets or sets the process step sub-entity that has changed.
        /// </summary>
        public object SubEntity { get; set; }

        /// <summary>
        /// Gets or sets the updated property value.
        /// </summary>
        public object Value { get; set; }

        /// <summary>
        /// Gets or sets the type of the change.
        /// </summary>        
        public ProcessStepChangeType ChangeType { get; set; }

        /// <summary>
        /// Gets or sets a collection containing the entities also changed besides the <see cref="Entity"/> object that have the same parent.
        /// </summary>
        public ICollection<object> AdditionalEntities { get; set; }
    }
}
