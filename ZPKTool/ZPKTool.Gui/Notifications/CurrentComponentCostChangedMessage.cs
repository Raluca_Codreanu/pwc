﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Notifications
{
    /// <summary>
    /// A message notifying that the cost of the component (part, assembly, ...) currently selected in the main view has changed.
    /// </summary>
    public class CurrentComponentCostChangedMessage : GenericMessage<CalculationResult>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CurrentComponentCostChangedMessage"/> class.
        /// </summary>
        /// <param name="result">The result.</param>
        public CurrentComponentCostChangedMessage(CalculationResult result)
            : base(result)
        {
        }
    }
}
