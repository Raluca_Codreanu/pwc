﻿namespace ZPKTool.Gui.Notifications
{
    /// <summary>
    /// The messages sent by the Messenger.
    /// </summary>
    public static class Notification
    {
        /// <summary>
        /// Signals that the user has logged into the application.
        /// </summary>
        public static readonly string LoggedIn = "UserLoggedIn";

        /// <summary>
        /// Signals that the user has logged out.
        /// </summary>
        public static readonly string LoggedOut = "UserLoggedOut";

        /// <summary>
        /// Signals that the shell view should be disabled.
        /// </summary>
        public static readonly string DisableShellView = "DisableShellView";

        /// <summary>
        /// Signals that the shell view should be enabled.
        /// </summary>
        public static readonly string EnableShellView = "EnableShellView";

        /// <summary>
        /// Signals that the Update available popup needs to be shown
        /// </summary>
        public static readonly string ShowUpdateAvailable = "ShowUpdateAvailable";

        /// <summary>
        /// Signals that the Update available popup needs to be shown
        /// </summary>
        public static readonly string HideUpdateAvailable = "HideUpdateAvailable";

        /// <summary>
        /// Signals the Shell to shutdown the application.
        /// </summary>
        public static readonly string ShutdownApplication = "ShutdownApplication";

        /// <summary>
        /// Signals that the trash bin has been emptied.
        /// </summary>
        public static readonly string TrashBinEmptied = "TrashBinEmptied";

        /// <summary>
        /// Signals that a background task has started work.
        /// </summary>
        public static readonly string BackgroundTaskStarted = "BackgroundTaskStarted";

        /// <summary>
        /// Signals that a background task has finished work.
        /// </summary>
        public static readonly string BackgroundTaskFinished = "BackgroundTaskFinished";

        /// <summary>
        /// Signals that an entity from MyProjects part of the main tree has changed.
        /// </summary>
        public static readonly string MyProjectsEntityChanged = "MyProjectsEntityChanged";

        /// <summary>
        /// Signals that an entity from Master Data part of the main tree has changed.
        /// </summary>
        public static readonly string MasterDataEntityChanged = "MasterDataEntityChanged";

        /// <summary>
        /// Signals that the update buttons was clicked in the overhead settings screen and the project needs to be saved (if it is possible)
        /// </summary>
        public static readonly string RequestUpdateOverheadSettings = "RequestUpdateOverheadSettings";

        /// <summary>
        /// Signals that the compare view must be displayed.
        /// </summary>
        public static readonly string DisplayCompareView = "DisplayCompareView";

        /// <summary>
        /// Cancel Sync reminder.
        /// </summary>
        public static readonly string CancelSyncReminder = "CancelSyncReminder";

        /// <summary>
        /// Signals that a project's owner has changed.
        /// </summary>
        public static readonly string ProjectOwnerChanged = "ProjectOwnerChanged";

        /// <summary>
        /// Signals to navigate to the entity in process screen not in tree
        /// </summary>
        public static readonly string NavigateToEntityIntoProcessScreen = "NavigateToEntityIntoProcessScreen";

        /// <summary>
        /// Signals that the process step changed
        /// </summary>
        public static readonly string ProcessStepChanged = "ProcessStepChanged";

        /// <summary>
        /// Signals that the Model Viewer must load a specified model.
        /// </summary>
        public static readonly string LoadModelInViewer = "LoadModelInViewer";

        /// <summary>
        /// Signals that the Next Step button from ProcessStep screen was clicked and the next ProcessStepTreeItem needs to be selected.
        /// </summary>
        public static readonly string SelectNextProcessStep = "SelectNextProcessStep";

        /// <summary>
        /// Signals that the Previous Step button from ProcessStep screen was clicked and the previous ProcessStepTreeItem needs to be selected.
        /// </summary>
        public static readonly string SelectPreviousProcessStep = "SelectPreviousProcessStep";

        /// <summary>
        /// Signals the Shell to restart the application.
        /// </summary>
        public static readonly string RestartApplication = "RestartApplication";

        /// <summary>
        /// Signals that the additional costs from result details have been updated.
        /// </summary>
        public static readonly string ResultDetailsAdditionalCostsUpdated = "ResultDetailsAdditionalCostsUpdated";

        /// <summary>
        /// Signals that the current chart mode from result details has been changed.
        /// </summary>
        public static readonly string ResultDetailsChartModeChanged = "ResultDetailsChartModeChanged";

        /// <summary>
        /// Signals the synchronization view model to start synchronizing the conflicts.
        /// </summary>
        public static readonly string StartConflictsSynchronization = "StartConflictsSynchronization";

        #region Main View Related Notifications

        /// <summary>
        /// Signals the main view model to reload the main view's docking manager layout.
        /// </summary>
        public static readonly string ReloadMainViewLayout = "ReloadMainViewLayout";

        /// <summary>
        /// Signals that the main view content must be reloaded due to changes in the model
        /// </summary>
        public static readonly string ReloadMainViewCurrentContent = "ReloadMainViewCurrentContent";

        /// <summary>
        /// Signals the main view to trigger the unloading process for the content of the main document.
        /// The message carrying this notification must be an instance of <see cref="NotificationMessageWithAction{TCallbackParam}"/>.
        /// </summary>
        public static readonly string MainViewNotifyContentToUnload = "MainViewNotifyContentToUnload";

        /// <summary>
        /// Signals the main view to return the content of the main document to the sender of this notification.
        /// The message carrying this notification must be an instance of <see cref="NotificationMessageWithAction&lt;object&gt;"/>.
        /// </summary>
        public static readonly string MainViewGetContent = "MainViewGetContent";

        /// <summary>
        /// Signals the main view to show the main document content pane
        /// </summary>
        public static readonly string MainViewShowDocumentContent = "MainViewShowDocumentContent";

        /// <summary>
        /// Signals that the master data starts to import.
        /// </summary>
        public static readonly string ImportMasterData = "ImportMasterData";

        #endregion Main View Related Notifications

        #region Media related notifications

        /// <summary>
        /// Signals the media view model to start the process of adding a new media (browse for it).
        /// </summary>
        public static readonly string AddMedia = "AddMedia";

        /// <summary>
        /// Signals the media view model that a media was deleted.
        /// </summary>
        public static readonly string MediaDeleted = "MediaDeleted";

        #endregion
    }
}
