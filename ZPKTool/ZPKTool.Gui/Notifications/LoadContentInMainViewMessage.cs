﻿namespace ZPKTool.Gui.Notifications
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// A message requesting the main view to load the specified content.
    /// </summary>
    public class LoadContentInMainViewMessage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoadContentInMainViewMessage"/> class.
        /// </summary>
        /// <param name="content">The content.</param>
        /// <param name="title">The title.</param>
        public LoadContentInMainViewMessage(object content, string title)
        {
            this.Content = content;
            this.Title = title;
            this.AllowVerticalScrolling = true;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LoadContentInMainViewMessage"/> class.
        /// </summary>
        /// <param name="content">The content.</param>
        /// <param name="title">The title.</param>
        /// <param name="allowVerticalScrolling">if set to true allows vertical scrolling of the content.</param>
        public LoadContentInMainViewMessage(object content, string title, bool allowVerticalScrolling)
            : this(content, title)
        {
            this.AllowVerticalScrolling = allowVerticalScrolling;
        }

        /// <summary>
        /// Gets or sets the content to load.
        /// </summary>        
        public object Content { get; set; }

        /// <summary>
        /// Gets or sets the title of the content to display on the main view.        
        /// </summary>        
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether vertical scrolling should be enabled for the content.
        /// The default value is true.
        /// Setting this property to false will constrain the content to the height of the main view;
        /// otherwise the content will be placed in a scroll viewer with vertical scrolling enabled.
        /// </summary>        
        public bool AllowVerticalScrolling { get; set; }
    }
}
