﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.Gui.Notifications
{
    /// <summary>
    /// A message requesting the main view to navigate to a specified entity.
    /// </summary>
    public class NavigateToEntityMessage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NavigateToEntityMessage"/> class.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="databaseId">The entity's source database.</param>
        public NavigateToEntityMessage(object entity, ZPKTool.DataAccess.DbIdentifier databaseId)
        {
            this.Entity = entity;
            this.DatabaseId = databaseId;
        }

        /// <summary>
        /// Gets the entity.
        /// </summary>        
        public object Entity { get; private set; }

        /// <summary>
        /// Gets the entity's source database.
        /// </summary>
        public ZPKTool.DataAccess.DbIdentifier DatabaseId { get; private set; }

        /// <summary>
        /// Gets or sets some supplementary data for navigation 
        /// </summary>
        public object AdditionalData { get; set; }
    }
}
