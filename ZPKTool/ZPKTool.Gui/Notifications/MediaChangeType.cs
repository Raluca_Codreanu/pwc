﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.Notifications
{
    /// <summary>
    /// The types of notifications sent when Media changed.
    /// </summary>
    public enum MediaChangeType
    {
        /// <summary>
        /// Unknown change.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Media added.
        /// </summary>
        MediaAdded = 1,

        /// <summary>
        /// Media deleted.
        /// </summary>
        MediaDeleted = 2,

        /// <summary>
        /// Media updated.
        /// </summary>
        MediaUpdated = 3,
    }
}
