﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using ZPKTool.Common;

namespace ZPKTool.Gui.Resources
{
    /// <summary>
    /// The images from the global image dictionary are accessed through this class.
    /// </summary>
    public static class Images
    {
        /// <summary>
        /// The dictionary containing the application's images.
        /// </summary>
        private static readonly ResourceDictionary ImageResources;

        /// <summary>
        /// Initializes static members of the <see cref="Images"/> class.
        /// </summary>
        static Images()
        {
            if (Application.Current != null)
            {
                // The image resource dictionary is merged in the application's resources.
                ImageResources = Application.Current.Resources;
            }
            else
            {
                // In unit tests, when the Application instance is not created, explicitly create the image resources dictionary.
                ImageResources = new ResourceDictionary();
                ImageResources.Source = new Uri(@"pack://application:,,,/PCM;component/Resources/Images.xaml");
            }

            Backgrounds = new List<Tuple<int, string, object>>();
            Backgrounds.Add(new Tuple<int, string, object>(0, LocalizedResources.General_Yellow, Images.BackgroundYellowBrushKey));
            Backgrounds.Add(new Tuple<int, string, object>(1, LocalizedResources.General_LightBlue, Images.BackgroundBlueBrushKey));
            Backgrounds.Add(new Tuple<int, string, object>(2, LocalizedResources.General_Silver, Images.BackgroundSilverBrushKey));

            DefaultApplicationLogo = ApplicationLogo;
        }

        /// <summary>
        /// Gets the available backgrounds, identified as follows.
        /// Item1 = Background index.
        /// Item2 = Background name.
        /// Item3 = Background brush object.
        /// </summary>
        public static List<Tuple<int, string, object>> Backgrounds { get; private set; }

        /// <summary>
        /// Gets the calculator icon.
        /// </summary>
        public static ImageSource CalculatorIcon
        {
            get { return ImageResources[CalculatorIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the calculator checked icon.
        /// </summary>
        public static ImageSource CalculatorCheckedIcon
        {
            get { return ImageResources[CalculatorCheckedIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the Part icon
        /// </summary>
        public static ImageSource PartIcon
        {
            get { return ImageResources[PartIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the Raw Part icon.
        /// </summary>
        public static ImageSource RawPartIcon
        {
            get { return ImageResources[RawPartIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the Parts icon
        /// </summary>
        public static ImageSource PartsIcon
        {
            get { return ImageResources[PartsIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the Master data icon
        /// </summary>
        public static ImageSource MasterDataIcon
        {
            get { return ImageResources[MasterDataIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the Custom Master data icon
        /// </summary>
        public static ImageSource CustomMasterDataIcon
        {
            get { return ImageResources[CustomMasterDataIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the MyProjectsNodeIcon
        /// </summary>
        public static ImageSource MyProjectsIcon
        {
            get { return ImageResources[MyProjectsIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the Raw Material icon
        /// </summary>
        public static ImageSource RawMaterialIcon
        {
            get { return ImageResources[RawMaterialIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the Other users projects node icon
        /// </summary>
        public static ImageSource OtherUsersProjectsIcon
        {
            get { return ImageResources[OtherUsersProjectsIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the Process node icon
        /// </summary>
        public static ImageSource ProcessIcon
        {
            get { return ImageResources[ProcessIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the Project icon.
        /// </summary>
        public static ImageSource ProjectIcon
        {
            get { return ImageResources[ProjectIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the Result Details icon.
        /// </summary>
        public static ImageSource ResultDetailsIcon
        {
            get { return ImageResources[ResultDetailsIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the icon for the trash bin.
        /// </summary>
        public static ImageSource TrashBinIcon
        {
            get { return ImageResources[TrashBinIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the icon for the empty trash bin.
        /// </summary>
        public static ImageSource TrashBinEmptyIcon
        {
            get { return ImageResources[TrashBinEmptyIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the MachineClassification icon
        /// </summary>
        public static ImageSource MachineClassificationIcon
        {
            get { return ImageResources[MachineClassificationIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the MaterialClassification icon
        /// </summary>
        public static ImageSource MaterialClassificationIcon
        {
            get { return ImageResources[MaterialClassificationIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the process steps classification icon.
        /// </summary>
        public static ImageSource ProcessStepsClassificationIcon
        {
            get { return ImageResources[ProcessStepsClassificationIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the Assembly icon
        /// </summary>
        public static ImageSource AssemblyIcon
        {
            get { return ImageResources[AssemblyIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the Assemblies icon
        /// </summary>
        public static ImageSource AssembliesIcon
        {
            get { return ImageResources[AssembliesIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the Consumable Icon
        /// </summary>
        public static ImageSource ConsumableIcon
        {
            get { return ImageResources[ConsumableIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the Die icon
        /// </summary>
        public static ImageSource DieIcon
        {
            get { return ImageResources[DieIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the Delete icon.
        /// </summary>
        public static ImageSource DeleteIcon
        {
            get { return ImageResources["DeleteIcon"] as ImageSource; }
        }

        /// <summary>
        /// Gets the Edit icon.
        /// </summary>
        public static ImageSource EditIcon
        {
            get { return ImageResources[EditIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the Check icon.
        /// </summary>
        public static ImageSource CheckIcon
        {
            get { return ImageResources["CheckIcon"] as ImageSource; }
        }

        /// <summary>
        /// Gets the Machine Icon
        /// </summary>
        public static ImageSource MachineIcon
        {
            get { return ImageResources[MachineIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the Play icon
        /// </summary>
        public static ImageSource PlayIcon
        {
            get { return ImageResources["PlayImage"] as ImageSource; }
        }

        /// <summary>
        /// Gets the Pause icon
        /// </summary>
        public static ImageSource PauseIcon
        {
            get { return ImageResources["PauseImage"] as ImageSource; }
        }

        /// <summary>
        /// Gets the Speaker icon
        /// </summary>
        public static ImageSource NoSpeakerIcon
        {
            get { return ImageResources["NoSpeakerImage"] as ImageSource; }
        }

        /// <summary>
        /// Gets the Speaker icon
        /// </summary>
        public static ImageSource SpeakerIcon
        {
            get { return ImageResources["SpeakerImage"] as ImageSource; }
        }

        /// <summary>
        /// Gets the Hand cursor
        /// </summary>
        public static Cursor HandCursor
        {
            get { return ImageResources["HandCursor"] as Cursor; }
        }

        /// <summary>
        /// Gets the Catch-Hand cursor
        /// </summary>
        public static Cursor CatchHandCursor
        {
            get { return ImageResources["Hand2Cursor"] as Cursor; }
        }

        /// <summary>
        /// Gets the icon for the Admin node of the projects tree.
        /// </summary>
        public static ImageSource AdminIcon
        {
            get { return ImageResources[AdminIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the icon for the ManageUsers node.
        /// </summary>
        public static ImageSource ManageUsersIcon
        {
            get { return ImageResources[ManageUsersIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the icon for Commodity.
        /// </summary>
        public static ImageSource CommodityIcon
        {
            get { return ImageResources[CommodityIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the icon for the Basic settings node
        /// </summary>
        public static ImageSource BasicSettingsIcon
        {
            get { return ImageResources[BasicSettingsIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the overhead settings icon.
        /// </summary>
        /// <value>The overhead settings icon.</value>
        public static ImageSource OverheadSettingsIcon
        {
            get { return ImageResources[OverheadSettingsIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the countries icon.
        /// </summary>
        /// <value>The countries icon.</value>
        public static ImageSource CountriesIcon
        {
            get { return ImageResources[CountriesIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the currency icon.
        /// </summary>
        /// <value>The currency icon.</value>
        public static ImageSource CurrencyIcon
        {
            get { return ImageResources[CurrencyIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the manufacturer icon.
        /// </summary>
        /// <value>The manufacturer icon.</value>
        public static ImageSource ManufacturerIcon
        {
            get { return ImageResources[ManufacturerIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the supplier icon.
        /// </summary>
        public static ImageSource SupplierIcon
        {
            get { return ImageResources[SupplierIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the online icon.
        /// </summary>
        public static ImageSource OnlineIcon
        {
            get { return ImageResources["ConnectedImage"] as ImageSource; }
        }

        /// <summary>
        /// Gets the offline icon.
        /// </summary>
        public static ImageSource OfflineIcon
        {
            get { return ImageResources["DisconnectedImage"] as ImageSource; }
        }

        /// <summary>
        /// Gets the wrench icon (used for settings, among other).
        /// </summary>
        public static ImageSource WrenchIcon
        {
            get { return ImageResources[WrenchIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the application logo.
        /// </summary>
        public static ImageSource ApplicationLogo
        {
            get { return ImageResources[ApplicationLogoKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the default application logo.
        /// </summary>
        public static readonly ImageSource DefaultApplicationLogo;

        /// <summary>
        /// Gets the user icon.
        /// </summary>
        public static ImageSource UserIcon
        {
            get { return ImageResources[UserIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the process step icon brush.
        /// </summary>
        public static ImageSource ProcessStepIcon
        {
            get { return ImageResources[ProcessStepIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the folder icon.
        /// </summary>    
        public static ImageSource FolderIcon
        {
            get { return ImageResources[FolderIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the isExternal icon.
        /// </summary>
        public static ImageSource IsExternalIcon
        {
            get { return ImageResources["IsExternalImage"] as ImageSource; }
        }

        /// <summary>
        /// Gets the icon representing the Estimation cost calculation accuracy.
        /// </summary>        
        public static ImageSource EstimationIcon
        {
            get { return ImageResources["EstimationIcon"] as ImageSource; }
        }

        /// <summary>
        /// Gets the icon representing the Rough Calculation cost calculation accuracy.
        /// </summary>        
        public static ImageSource RoughCalculationIcon
        {
            get { return ImageResources["RoughCalculationIcon"] as ImageSource; }
        }

        /// <summary>
        /// Gets the icon representing the Offer / External Calculation cost calculation accuracy.
        /// </summary>        
        public static ImageSource OfferExternalCalculationIcon
        {
            get { return ImageResources["OfferExternalCalculationIcon"] as ImageSource; }
        }

        /// <summary>
        /// Gets the icon representing an offline item.
        /// </summary>
        public static ImageSource OfflineItemIcon
        {
            get { return ImageResources["OfflineItemIcon"] as ImageSource; }
        }

        /// <summary>
        /// Gets the manage projects icon.
        /// </summary>
        public static ImageSource ManageProjectsIcon
        {
            get { return ImageResources[ManageProjectsIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the bookmarks icon.
        /// </summary>
        public static ImageSource BookmarksIcon
        {
            get { return ImageResources[BookmarksIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the search icon.
        /// </summary>
        public static ImageSource SearchIcon
        {
            get { return ImageResources[SearchIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the waterfall - pie switch icon.
        /// </summary>
        public static ImageSource WaterfallPieSwitchIcon
        {
            get { return ImageResources[WaterfallPieSwitchIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the information icon.
        /// </summary>
        public static ImageSource InfoIcon
        {
            get { return ImageResources[InfoIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the warning icon.
        /// </summary>
        public static ImageSource WarningIcon
        {
            get { return ImageResources[WarningIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the error icon.
        /// </summary>
        public static ImageSource ErrorIcon
        {
            get { return ImageResources[ErrorIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the question image.
        /// </summary>
        public static ImageSource QuestionImage
        {
            get { return ImageResources[QuestionImageKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the undo icon.
        /// </summary>
        public static ImageSource UndoIcon
        {
            get { return ImageResources[UndoIconKey] as ImageSource; }
        }

        /// <summary>
        /// Gets the redo icon.
        /// </summary>
        public static ImageSource RedoIcon
        {
            get { return ImageResources[RedoIconKey] as ImageSource; }
        }

        /// <summary>
        /// The resource key for the Admin icon.
        /// </summary>
        public static readonly object AdminIconKey = "AdminIcon";

        /// <summary>
        /// The resource key for the Basic Settings icon.
        /// </summary>
        public static readonly object BasicSettingsIconKey = "AdminIcon";

        /// <summary>
        /// The resource key for the Countries icon.
        /// </summary>
        public static readonly object CountriesIconKey = "AdminIcon";

        /// <summary>
        /// The resource key for the Currency icon.
        /// </summary>
        public static readonly object CurrencyIconKey = "AdminIcon";

        /// <summary>
        /// The resource key for the Overhead Settings icon.
        /// </summary>
        public static readonly object OverheadSettingsIconKey = "AdminIcon";

        /// <summary>
        /// The resource key for the waterfall - pie switch icon.
        /// </summary>
        public static readonly object WaterfallPieSwitchIconKey = "WaterfallPieSwitch";

        /// <summary>
        /// The resource key for the bookmarks icon.
        /// </summary>
        public static readonly object BookmarksIconKey = "BookmarksIcon";

        /// <summary>
        /// The resource key for the search icon.
        /// </summary>
        public static readonly object SearchIconKey = "SearchIcon";

        /// <summary>
        /// The resource key for the Part icon.
        /// </summary>
        public static readonly object PartIconKey = "PartIcon";

        /// <summary>
        /// The resource key for the Raw Part icon.
        /// </summary>
        public static readonly object RawPartIconKey = "RawPartIcon";

        /// <summary>
        /// The resource key for the Result Details icon.
        /// </summary>
        public static readonly object ResultDetailsIconKey = "ResultDetailsIcon";

        /// <summary>
        /// The resource key for the Parts icon.
        /// </summary>
        public static readonly object PartsIconKey = "PartsIcon";

        /// <summary>
        /// The resource key for the Master Data icon.
        /// </summary>
        public static readonly object MasterDataIconKey = "MasterdataIcon";

        /// <summary>
        /// The resource key for the Custom Master Data icon.
        /// </summary>
        public static readonly object CustomMasterDataIconKey = "CustomMasterdataIcon";

        /// <summary>
        /// The resource key for the MyProjects icon.
        /// </summary>
        public static readonly object MyProjectsIconKey = "MyProjectsIcon";

        /// <summary>
        /// The resource key for the Raw Material icon.
        /// </summary>
        public static readonly object RawMaterialIconKey = "MaterialIcon";

        /// <summary>
        /// The resource key for the Project icon.
        /// </summary>
        public static readonly object ProjectIconKey = "ProjectIcon";

        /// <summary>
        /// The resource key for the Assembly icon.
        /// </summary>
        public static readonly object AssemblyIconKey = "AssemblyIcon";

        /// <summary>
        /// The resource key for the Assemblies image.
        /// </summary>
        public static readonly object AssembliesIconKey = "AssembliesImage";

        /// <summary>
        /// The resource key for the Material Classification icon.
        /// </summary>
        public static readonly object MaterialClassificationIconKey = "AssemblyIcon";

        /// <summary>
        /// The resource key for the Machine Classification icon.
        /// </summary>
        public static readonly object MachineClassificationIconKey = "AssemblyIcon";

        /// <summary>
        /// The resource key for the Process Steps Classification icon.
        /// </summary>
        public static readonly object ProcessStepsClassificationIconKey = "AssemblyIcon";

        /// <summary>
        /// The resource key for the Consumable icon.
        /// </summary>
        public static readonly object ConsumableIconKey = "ConsumableIcon";

        /// <summary>
        /// The resource key for the Die icon.
        /// </summary>
        public static readonly object DieIconKey = "DieIcon";

        /// <summary>
        /// The resource key for the Machine icon.
        /// </summary>
        public static readonly object MachineIconKey = "MachineIcon";

        /// <summary>
        /// The resource key for the Commodity icon.
        /// </summary>
        public static readonly object CommodityIconKey = "CommodityIcon";

        /// <summary>
        /// The resource key for the Manufacturer icon.
        /// </summary>
        public static readonly object ManufacturerIconKey = "FactoryIcon";

        /// <summary>
        /// The resource for the Other Users Projects icon.
        /// </summary>
        public static readonly object OtherUsersProjectsIconKey = "OtherUsersProjectsIcon";

        /// <summary>
        /// The resource key for the User icon.
        /// </summary>
        public static readonly object UserIconKey = "UserIcon";

        /// <summary>
        /// The resource key for the Supplier icon.
        /// </summary>
        public static readonly object SupplierIconKey = "SupplierIcon";

        /// <summary>
        /// The resource key for the Manage Users icon.
        /// </summary>
        public static readonly object ManageUsersIconKey = "ManageUsersIcon";

        /// <summary>
        /// The resource key for the Manage Projects icon.
        /// </summary>
        public static readonly object ManageProjectsIconKey = "ManageProjectsIcon";

        /// <summary>
        /// The resource key for the ProcessStep icon.
        /// </summary>
        public static readonly object ProcessStepIconKey = "ProcessStepIcon";

        /// <summary>
        /// The resource key to the Checkmark icon.
        /// </summary>
        public static readonly object CheckmarkIconKey = "CheckmarkIcon";

        /// <summary>
        /// The resource key to the Error icon.
        /// </summary>
        public static readonly object ErrorIconKey = "ErrorIcon";

        /// <summary>
        /// The resource key to the brush containing the background used on windows and screens.
        /// </summary>
        public static readonly object BackgroundBrushKey = "BackgroundImageBrush";

        /// <summary>
        /// The resource key to the yellow brush background.
        /// </summary>
        public static readonly object BackgroundYellowBrushKey = "BackgroundYellowImageBrush";

        /// <summary>
        /// The resource key to the blue brush background.
        /// </summary>
        public static readonly object BackgroundBlueBrushKey = "BackgroundBlueImageBrush";

        /// <summary>
        /// The resource key to the silver brush background.
        /// </summary>
        public static readonly object BackgroundSilverBrushKey = "BackgroundSilverImageBrush";

        /// <summary>
        /// The resource key for the Down (arrow) icon.
        /// </summary>        
        public static readonly object DownIconKey = "DownIcon";

        /// <summary>
        /// The resource key for the Up (arrow) icon.
        /// </summary>
        public static readonly object UpIconKey = "UpIcon";

        /// <summary>
        /// The resource key for the Copy icon.
        /// </summary>
        public static readonly object CopyIconKey = "CopyIcon";

        /// <summary>
        /// The resource key for the Paste icon.
        /// </summary>
        public static readonly object PasteIconKey = "PasteIcon";

        /// <summary>
        /// The resource key for the Delete icon.
        /// </summary>
        public static readonly object DeleteIconKey = "DeleteIcon";

        /// <summary>
        /// The resource key for the Trash Bin icon.
        /// </summary>
        public static readonly object TrashBinIconKey = "TrashBinIcon";

        /// <summary>
        /// The resource key for the Trash Bin Empty icon.
        /// </summary>
        public static readonly object TrashBinEmptyIconKey = "TrashBinEmptyIcon";

        /// <summary>
        /// The resource key for the Edit icon.
        /// </summary>
        public static readonly object EditIconKey = "EditIcon";

        /// <summary>
        /// The resource key for the Folder icon.
        /// </summary>
        public static readonly object FolderIconKey = "FolderIcon";

        /// <summary>
        /// The resource key for the Cut icon.
        /// </summary>
        public static readonly object CutIconKey = "CutIcon";

        /// <summary>
        /// The resource key for the new project folder icon.
        /// </summary>
        public static readonly object ProjectFolderNewIconKey = "ProjectFolderNewIcon";

        /// <summary>
        /// The resource key for the Import icon.
        /// </summary>
        public static readonly object ImportIconKey = "ImportIcon";

        /// <summary>
        /// The resource key for the Export icon.
        /// </summary>
        public static readonly object ExportIconKey = "ExportIcon";

        /// <summary>
        /// The resource key for the Compare icon.
        /// </summary>
        public static readonly object CompareIconKey = "CompareIcon";

        /// <summary>
        /// The resource key for the Mass Data Update icon.
        /// </summary>
        public static readonly object MassDataUpdateIconKey = "MassDataUpdateIcon";

        /// <summary>
        /// The resource key for the Calculator icon.
        /// </summary>
        public static readonly object CalculatorIconKey = "CalculatorIcon";

        /// <summary>
        /// The resource key for the Calculator icon with a checkmark in the lower right corner.
        /// </summary>
        public static readonly object CalculatorCheckedIconKey = "CalculatorCheckedIcon";

        /// <summary>
        /// The resource key for the Synchronize icon.
        /// </summary>
        public static readonly object SyncIconKey = "SyncIcon";

        /// <summary>
        /// The resource key for the Batch Reporting icon.
        /// </summary>
        public static readonly object BatchReportingIconKey = "ReportIcon";

        /// <summary>
        /// The resource key for the Report Generator icon.
        /// </summary>
        public static readonly object ReportGeneratorIconKey = "ReportGeneratorIcon";

        /// <summary>
        /// The resource key for the Add Bookmark icon.
        /// </summary>
        public static readonly object AddBookmarkIconKey = "AddBookmarkIcon";

        /// <summary>
        /// The resource key for the Remove Bookmark icon.
        /// </summary>
        public static readonly object RemoveBookmarkIconKey = "RemoveBookmarkIcon";

        /// <summary>
        /// The resource key for the Information icon.
        /// </summary>
        public static readonly object InfoIconKey = "InfoIcon";

        /// <summary>
        /// The resource key for the Warning icon.
        /// </summary>
        public static readonly object WarningIconKey = "WarningIcon";

        /// <summary>
        /// The resource key for the Question image.
        /// </summary>
        public static readonly object QuestionImageKey = "QuestionImage";

        /// <summary>
        /// The resource key for the Go To icon.
        /// </summary>
        public static readonly object GoToIconKey = "GoToIcon";

        /// <summary>
        /// The resource key for the ArrowUpBlack image.
        /// </summary>
        public static readonly object ArrowUpBlackKey = "ArrowUpBlack";

        /// <summary>
        /// The resource key for the application logo.
        /// </summary>
        public static readonly object ApplicationLogoKey = "ApplicationLogo";

        /// <summary>
        /// The resource key to the Undo icon.
        /// </summary>
        public static readonly object UndoIconKey = "UndoIcon";

        /// <summary>
        /// The resource key to the Redo icon.
        /// </summary>
        public static readonly object RedoIconKey = "RedoIcon";

        /// <summary>
        /// The resource key for the Wrench icon.
        /// </summary>
        public static readonly object WrenchIconKey = "WrenchIcon";

        /// <summary>
        /// The resource key for the Process icon.
        /// </summary>
        public static readonly object ProcessIconKey = "ProcessIcon";

        /// <summary>
        /// Set The UI background brush color.
        /// </summary>
        /// <param name="backgroundID">The background id, corresponding to the existing BackgroundBrush.</param>
        public static void SetBackground(int backgroundID)
        {
            var selectedBackground = Backgrounds.FirstOrDefault(b => b.Item1 == backgroundID);
            if (selectedBackground != null)
            {
                var brush = (RadialGradientBrush)ImageResources[BackgroundBrushKey];
                if (brush != null
                    && brush.IsFrozen)
                {
                    // Because the brush is frozen, we must create a copy, modify it, and then apply it. T
                    // This requires the consumer of the resource to be dynamic.
                    RadialGradientBrush newBrush = brush.Clone();
                    if (selectedBackground.Item3 == BackgroundYellowBrushKey)
                    {
                        newBrush = (RadialGradientBrush)ImageResources[BackgroundYellowBrushKey];
                    }
                    else if (selectedBackground.Item3 == BackgroundBlueBrushKey)
                    {
                        newBrush = (RadialGradientBrush)ImageResources[BackgroundBlueBrushKey];
                    }
                    else if (selectedBackground.Item3 == BackgroundSilverBrushKey)
                    {
                        newBrush = (RadialGradientBrush)ImageResources[BackgroundSilverBrushKey];
                    }

                    ImageResources[BackgroundBrushKey] = newBrush;
                }
            }
        }

        /// <summary>
        /// Generate a RadialGradientBrush with the color passed as parameter and apply it as the UI background brush.
        /// </summary>
        /// <param name="color">The RadialGradientBrush maximum value color. / The minimum color value is White.</param>
        private static void ApplyBackgroundColor(Color color)
        {
            var brush = (RadialGradientBrush)ImageResources[BackgroundBrushKey];
            if (brush != null
                && brush.IsFrozen)
            {
                // Because the brush is frozen, we must create a copy, modify it, and then apply it. T
                // This requires the consumer of the resource to be dynamic.
                RadialGradientBrush newBrush = brush.Clone();
                newBrush.GradientOrigin = new Point(0.5, 0);
                newBrush.Center = new Point(0.5, 0);
                newBrush.RadiusX = 1;
                newBrush.RadiusY = 1.1;

                int minR = Colors.White.R;
                int maxR = color.R;
                int minG = Colors.White.G;
                int maxG = color.G;
                int minB = Colors.White.B;
                int maxB = color.B;

                var offsets = new List<double>() { 0.0, 0.4, 0.5, 1.0 };

                for (int i = 0; i < 4; i++)
                {
                    var averageR = (byte)(minR + (int)((maxR - minR) * offsets[i]));
                    var averageG = (byte)(minG + (int)((maxG - minG) * offsets[i]));
                    var averageB = (byte)(minB + (int)((maxB - minB) * offsets[i]));

                    newBrush.GradientStops[i] = new GradientStop() { Color = Color.FromRgb(averageR, averageG, averageB), Offset = offsets[i] };
                }

                ImageResources[BackgroundBrushKey] = newBrush;
            }
        }

        /// <summary>
        /// Loads the application logo from the custom logo image or as the default one from resources.
        /// </summary>
        public static void LoadCustomApplicationLogo()
        {
            if (File.Exists(Path.Combine(Constants.ApplicationDataFolderPath, UserSettingsManager.Instance.CustomLogoPath)))
            {
                try
                {
                    // Loads the custom logo from the path saved into settings; this method prevents the file from being locked.
                    var customLogo = new BitmapImage();
                    customLogo.BeginInit();
                    customLogo.CacheOption = BitmapCacheOption.OnLoad;
                    customLogo.CreateOptions = BitmapCreateOptions.IgnoreImageCache;
                    customLogo.UriSource = new Uri(Path.Combine(Constants.ApplicationDataFolderPath, UserSettingsManager.Instance.CustomLogoPath));
                    customLogo.EndInit();

                    ImageResources[ApplicationLogoKey] = customLogo;
                }
                catch
                {
                    // If setting the custom logo throws an error, the default application logo is set.
                    ImageResources[ApplicationLogoKey] = DefaultApplicationLogo;
                }
            }
            else if (ApplicationLogo != DefaultApplicationLogo)
            {
                // If no custom logo path is saved into settings, the default logo is set.
                ImageResources[ApplicationLogoKey] = DefaultApplicationLogo;
            }
        }

        /// <summary>
        /// Sets the application background and loads the application logo.
        /// </summary>
        public static void InitializeImageResources()
        {
            // Set the application background.
            SetBackground(UserSettingsManager.Instance.BackgroundColorID);

            // Set the application logo.
            LoadCustomApplicationLogo();
        }
    }
}
