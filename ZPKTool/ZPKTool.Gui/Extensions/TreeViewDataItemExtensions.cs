﻿namespace System.Collections.Generic
{
    using System.Linq;
    using ZPKTool.Gui.Controls;

    /// <summary>
    /// Extensions for the <see cref="TreeViewDataItem"/> class.
    /// </summary>
    public static class TreeViewDataItemExtensions
    {
        /// <summary>
        /// Determines whether an element with a specified data object is in the IEnumerable<see cref="TreeViewDataItem"/> collection.
        /// </summary>
        /// <param name="collection">The IEnumerable<see cref="TreeViewDataItem"/> collection.</param>
        /// <param name="dataObject">The data object.</param>
        /// <returns>true if item is found in the collection; otherwise, false.</returns>
        public static bool ContainsItemWithDataObject(this IEnumerable<TreeViewDataItem> collection, object dataObject)
        {
            return collection.Any(it => it.FindItemForDataObject(dataObject) != null);
        }
    }
}
