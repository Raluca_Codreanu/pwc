﻿namespace System.Windows.Controls
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Extensions for the <see cref="System.Windows.Controls.TreeViewItem"/> class.
    /// </summary>
    public static class TreeViewItemExtensions
    {
        /// <summary>
        /// The {DisconnectedItem} sentinel object instance.
        /// </summary>
        private static readonly object disconnectedItem;

        /// <summary>
        /// Initializes static members of the <see cref="TreeViewItemExtensions"/> class.
        /// </summary>
        static TreeViewItemExtensions()
        {
            disconnectedItem = typeof(System.Windows.Data.BindingExpressionBase)
                .GetField("DisconnectedItem", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic)
                .GetValue(null);
            if (disconnectedItem == null)
            {
                throw new InvalidOperationException("Failed to get the DisconnectedItem sentinel object. This probably means that the project was upgraded to a new .NET framework version. Check if the new framework version has a way of checking for DisconnectingItem.");
            }
        }

        /// <summary>
        /// Determines whether the specified item is disconnected ("{DisconnectedItem}"), that is, if the object's data context is a sentinel object instance.
        /// This may happen if the item is used in a virtualized tree view.
        /// </summary>
        /// <param name="item">The item to check.</param>
        /// <returns>
        /// true if the specified item is disconnected; otherwise, false.
        /// </returns>
        public static bool IsDisconnectedItem(this TreeViewItem item)
        {
            // TODO: this method is a workaround; checking if an item is the sentinel item should be available in the next .net framework (4.5)
            // so remove this method when upgrading the project.
            if (item == null)
            {
                throw new ArgumentNullException("item", "The item can not be null.");
            }

            if (item.Header == disconnectedItem || item.DataContext == disconnectedItem)
            {
                return true;
            }

            return false;
        }
    }
}
