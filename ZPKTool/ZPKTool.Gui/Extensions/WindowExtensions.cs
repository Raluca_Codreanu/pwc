﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Interop;
using ZPKTool.Gui;

namespace System.Windows
{
    /// <summary>
    /// Extensions for the <see cref="System.Windows.Window"/> class.
    /// </summary>
    public static class WindowExtensions
    {
        /// <summary>
        /// Brings to specified window to top.
        /// This method should be used to uncover any window that is partially or completely obscured by any overlapping windows.
        /// </summary>
        /// <param name="window">The window to bring to top.</param>
        /// <exception cref="ArgumentNullException">The <paramref name="window"/> is null.</exception>
        public static void BringToTop(this Window window)
        {
            if (window == null)
            {
                throw new ArgumentNullException("Argument window can't be null.", "window");
            }

            WindowInteropHelper interopHelper = new WindowInteropHelper(window);
            NativeMethods.BringWindowToTop(interopHelper.Handle);
        }

        /// <summary>
        /// Activates, focuses and switches input to the specified window.
        /// </summary>
        /// <param name="window">The window.</param>
        /// <exception cref="ArgumentNullException">The <paramref name="window"/> is null.</exception>
        public static void BringToForeground(this Window window)
        {
            if (window == null)
            {
                throw new ArgumentNullException("Argument window can't be null.", "window");
            }

            WindowInteropHelper interopHelper = new WindowInteropHelper(window);
            NativeMethods.SetForegroundWindow(interopHelper.Handle);
        }

        /// <summary>
        /// Sends the specified message to the specified window.
        /// </summary>
        /// <param name="windowHandle">The window handle.</param>
        /// <param name="messageID">The ID of the message to send.</param>
        /// <param name="message">The string message to send.</param>
        public static void SendStringMessage(IntPtr windowHandle, int messageID, string message)
        {
            byte[] messageBytes = System.Text.Encoding.Default.GetBytes(message);
            var dataStuct = new NativeMethods.COPYDATASTRUCT();
            dataStuct.dwData = (IntPtr)100;
            dataStuct.lpData = message;
            dataStuct.cbData = messageBytes.Length + 1;
            NativeMethods.SendMessage(windowHandle, NativeMethods.WM_COPYDATA, messageID, ref dataStuct);
        }

        /// <summary>
        /// Activates and displays the window.
        /// </summary>
        /// <param name="windowHandle">The window.</param>
        public static void RestoreWindow(IntPtr windowHandle)
        {
            var placement = new NativeMethods.WINDOWPLACEMENT();
            NativeMethods.GetWindowPlacement(windowHandle, ref placement);
            switch (placement.showCmd)
            {
                case 2:
                    {
                        // The window is minimized.
                        if ((placement.flags & NativeMethods.WPF_RESTORETOMAXIMIZED) == NativeMethods.WPF_RESTORETOMAXIMIZED)
                        {
                            NativeMethods.ShowWindow(windowHandle, NativeMethods.SW_SHOWMAXIMIZED);
                        }
                        else
                        {
                            NativeMethods.ShowWindow(windowHandle, NativeMethods.SW_RESTORE);
                        }

                        break;
                    }

                case 3:
                    {
                        // The window is maximized.
                        NativeMethods.ShowWindow(windowHandle, NativeMethods.SW_SHOWMAXIMIZED);
                        break;
                    }

                default:
                    {
                        // The window is neither minimized or maximized.
                        NativeMethods.ShowWindow(windowHandle, NativeMethods.SW_RESTORE);
                        break;
                    }
            }
        }
    }
}
