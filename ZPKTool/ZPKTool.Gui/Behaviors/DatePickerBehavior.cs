﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace ZPKTool.Gui.Behaviors
{
    /// <summary>
    /// A behavior that enables a date picker to work in year mode.
    /// </summary>
    public static class DataPickerBehavior
    {
        #region Dependency Properties

        /// <summary>
        /// Using a DependencyProperty as the backing store for YearMode.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty YearModeProperty =
            DependencyProperty.RegisterAttached("YearMode", typeof(bool), typeof(DataPickerBehavior), new PropertyMetadata(false, OnYearModeChanged));

        /// <summary>
        /// Gets a value indicating whether the date pickers year mode is enabled.
        /// </summary>
        /// <param name="obj">The dependency object.</param>
        /// <returns>The value.</returns>
        public static bool GetYearMode(DependencyObject obj)
        {
            return (bool)obj.GetValue(YearModeProperty);
        }

        /// <summary>
        /// Sets a value indicating whether the date pickers year mode is enabled.
        /// </summary>
        /// <param name="obj">The dependency object.</param>
        /// <param name="value">The value.</param>
        public static void SetYearMode(DependencyObject obj, bool value)
        {
            obj.SetValue(YearModeProperty, value);
        }

        #endregion Dependency Properties

        /// <summary>
        /// Called when the YearMode property is changed.
        /// </summary>
        /// <param name="d">The dependency object.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnYearModeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var picker = d as DatePicker;
            if (picker == null)
            {
                throw new InvalidOperationException("Year mode is supported only for DatePicker.");
            }

            bool enabled = (bool)e.NewValue;
            if (enabled)
            {
                picker.CalendarOpened += OnCalendarOpened;
                picker.CalendarClosed += OnCalendarClosed;
            }
            else
            {
                picker.CalendarOpened -= OnCalendarOpened;
                picker.CalendarClosed -= OnCalendarClosed;
            }
        }

        /// <summary>
        /// Called when the date pickers calendar has opened.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs" /> instance containing the event data.</param>
        private static void OnCalendarOpened(object sender, RoutedEventArgs e)
        {
            var picker = sender as DatePicker;
            if (picker != null)
            {
                var popupPicker = picker.Template.FindName("PART_Popup", picker) as Popup;
                if (popupPicker != null)
                {
                    var calendarPicker = popupPicker.Child as Calendar;
                    if (calendarPicker != null)
                    {
                        calendarPicker.DisplayMode = CalendarMode.Decade;
                        calendarPicker.DisplayModeChanged += new EventHandler<CalendarModeChangedEventArgs>(Calendar_DisplayModeChanged);
                    }
                }
            }
        }

        /// <summary>
        /// Called when the date pickers calendar has closed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs" /> instance containing the event data.</param>
        private static void OnCalendarClosed(object sender, RoutedEventArgs e)
        {
            var picker = sender as DatePicker;
            if (picker != null)
            {
                var popupPicker = picker.Template.FindName("PART_Popup", picker) as Popup;
                if (popupPicker != null)
                {
                    var calendar = popupPicker.Child as Calendar;
                    if (calendar != null)
                    {
                        calendar.DisplayModeChanged -= new EventHandler<CalendarModeChangedEventArgs>(Calendar_DisplayModeChanged);
                    }
                }
            }
        }

        /// <summary>
        /// Handles the DisplayModeChanged event of the Calendar control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="CalendarModeChangedEventArgs" /> instance containing the event data.</param>
        private static void Calendar_DisplayModeChanged(object sender, CalendarModeChangedEventArgs e)
        {
            var calendarPicker = sender as Calendar;
            if (calendarPicker != null)
            {
                calendarPicker.DisplayMode = CalendarMode.Year;
                var popupPicker = calendarPicker.Parent as Popup;
                if (popupPicker != null)
                {
                    var picker = popupPicker.TemplatedParent as DatePicker;
                    if (picker != null
                        && picker.IsDropDownOpen)
                    {
                        picker.SelectedDate = new DateTime(calendarPicker.DisplayDate.Year, 1, 1);
                        picker.IsDropDownOpen = false;
                        calendarPicker.DisplayModeChanged -= new EventHandler<CalendarModeChangedEventArgs>(Calendar_DisplayModeChanged);
                    }
                }
            }
        }
    }
}