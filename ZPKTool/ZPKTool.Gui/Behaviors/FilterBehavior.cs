﻿namespace ZPKTool.Gui.Behaviors
{
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Threading;

    /// <summary>
    /// A behavior that facilitates the filter function on a ICollectionView instance.
    /// </summary>
    public static class FilterBehavior
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// A list containing the associations between the objects on which the filtering is performed and the timers used to delay the Filter trigger action.
        /// </summary>
        private static List<Tuple<DependencyObject, DispatcherTimer>> filterDelayTimers = new List<Tuple<DependencyObject, DispatcherTimer>>();

        /// <summary>
        /// The Filter Trigger refresh timer's and current event's handler list.
        /// </summary>
        private static List<Tuple<DispatcherTimer, EventHandler>> filterTriggerHandlers = new List<Tuple<DispatcherTimer, EventHandler>>();

        #endregion Attributes

        #region Attached dependency properties

        /// <summary>
        /// Using a DependencyProperty as the backing store for Filter.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty FilterProperty = DependencyProperty.RegisterAttached(
            "Filter",
            typeof(Predicate<object>),
            typeof(FilterBehavior),
            new UIPropertyMetadata(null, OnFilterChanged));

        /// <summary>
        /// Using a DependencyProperty as the backing store for FilterTrigger.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty FilterTriggerProperty = DependencyProperty.RegisterAttached(
            "FilterTrigger",
            typeof(object),
            typeof(FilterBehavior),
            new UIPropertyMetadata(null, OnFilterTriggerChanged));

        #endregion Attached dependency properties

        #region Getters and setters of the attached properties

        /// <summary>
        /// Gets a value representing the Filter Predicate.
        /// </summary>
        /// <param name="obj">The attached ItemsControl object.</param>
        /// <returns>The Filter Predicate.</returns>
        public static Predicate<object> GetFilter(DependencyObject obj)
        {
            return (Predicate<object>)obj.GetValue(FilterProperty);
        }

        /// <summary>
        /// Sets a value a value representing the Filter Predicate.
        /// </summary>
        /// <param name="obj">The attached ItemsControl object.</param>
        /// <param name="value">The Filter Predicate value.</param>
        public static void SetFilter(DependencyObject obj, Predicate<object> value)
        {
            obj.SetValue(FilterProperty, value);
        }

        /// <summary>
        /// Gets a value indicating to trigger the colletcion filter.
        /// </summary>
        /// <param name="obj">The attached ItemsControl object.</param>
        /// <returns>The modified object that triggers the filter.</returns>
        public static object GetFilterTrigger(DependencyObject obj)
        {
            return (object)obj.GetValue(FilterTriggerProperty);
        }

        /// <summary>
        /// Sets a value indicating to trigger the colletcion filter.
        /// </summary>
        /// <param name="obj">The attached ItemsControl object.</param>
        /// <param name="value">The modified object that triggers the filter.</param>
        public static void SetFilterTrigger(DependencyObject obj, object value)
        {
            obj.SetValue(FilterTriggerProperty, value);
        }

        #endregion Getters and setters of the attached properties

        #region Event Handler

        /// <summary>
        /// Called when Filter property changed.
        /// </summary>
        /// <param name="depObj">The dependency object for which the property changed.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnFilterChanged(DependencyObject depObj, DependencyPropertyChangedEventArgs e)
        {
            var itemsControl = depObj as ItemsControl;
            if (itemsControl != null)
            {
                itemsControl.Items.Filter = e.NewValue as Predicate<object>;
                if (filterDelayTimers.Find(t => t.Item1 == depObj) == null)
                {
                    var filterDelayTimer = new DispatcherTimer();
                    filterDelayTimers.Add(new Tuple<DependencyObject, DispatcherTimer>(depObj, filterDelayTimer));
                }
            }
        }

        /// <summary>
        /// Called when FilterTrigger property changed.
        /// </summary>
        /// <param name="depObj">The dependency object for which the property changed.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnFilterTriggerChanged(DependencyObject depObj, DependencyPropertyChangedEventArgs e)
        {
            var filterTimer = filterDelayTimers.Find(f => f.Item1 == depObj);
            if (filterTimer != null)
            {
                var timer = filterTimer.Item2 ?? new DispatcherTimer();
                var filterTriggerHandler = filterTriggerHandlers.Find(t => t.Item1 == timer);
                var handler = filterTriggerHandler != null ? filterTriggerHandler.Item2 : null;

                timer.Stop();
                timer.Tick -= handler;
                timer.Interval = new TimeSpan(0, 0, 0, 0, 300);
                handler = (object sender, EventArgs ev) =>
                {
                    var itemsControl = depObj as ItemsControl;
                    if (itemsControl != null)
                    {
                        var view = System.Windows.Data.CollectionViewSource.GetDefaultView(itemsControl.ItemsSource);
                        if (view != null)
                        {
                            view.Refresh();
                        }
                    }

                    timer.Stop();
                };

                timer.Tick += handler;
                filterTriggerHandlers.Remove(filterTriggerHandler);
                filterTriggerHandlers.Add(new Tuple<DispatcherTimer, EventHandler>(timer, handler));
                timer.Start();
            }
        }

        #endregion Event Handler
    }
}
