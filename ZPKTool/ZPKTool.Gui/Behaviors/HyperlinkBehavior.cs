﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Navigation;

namespace ZPKTool.Gui.Behaviors
{
    /// <summary>
    /// A behavior that adds the IsExternalLink property to Hyperlink control, to allow specifying if the control must perform the navigation to link by opening it in a browser.
    /// </summary>
    public static class HyperlinkBehavior
    {
        /// <summary>
        /// Using a DependencyProperty as the backing store for IsExternalLinkProperty.
        /// </summary>
        public static readonly DependencyProperty IsExternalLinkProperty =
            DependencyProperty.RegisterAttached("IsExternalLink", typeof(bool), typeof(HyperlinkBehavior), new PropertyMetadata(false, OnIsExternalLinkChanged));

        /// <summary>
        /// Gets the is external link property value.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns>True if link is external, false otherwise.</returns>
        public static bool GetIsExternalLink(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsExternalLinkProperty);
        }

        /// <summary>
        /// Sets the is external link property.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <param name="value">if set to <c>true</c> [value].</param>
        public static void SetIsExternalLink(DependencyObject obj, bool value)
        {
            obj.SetValue(IsExternalLinkProperty, value);
        }

        /// <summary>
        /// Called when the IsExternalLink property value changed.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        /// <exception cref="System.InvalidOperationException">The IsExternalLink behavior can be attached only to Hyperlink objects.</exception>
        private static void OnIsExternalLinkChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var sender = obj as Hyperlink;
            if (sender == null)
            {
                throw new InvalidOperationException("The IsExternalLink behavior can be attached only to Hyperlink objects.");
            }

            if ((bool)e.NewValue)
            {
                sender.RequestNavigate += HyperlinkControl_RequestNavigate;
            }
            else
            {
                sender.RequestNavigate -= HyperlinkControl_RequestNavigate;
            }
        }

        /// <summary>
        /// Handles the RequestNavigate event of the Hyperlink control, to open the URL from the control in a browser.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RequestNavigateEventArgs"/> instance containing the event data.</param>
        private static void HyperlinkControl_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Uri.AbsoluteUri))
            {
                Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            }
        }
    }
}
