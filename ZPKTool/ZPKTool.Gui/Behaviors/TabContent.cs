﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Markup;

namespace ZPKTool.Gui.Behaviors
{
    /// <summary>
    /// Attached properties for persistent tab control
    /// </summary>
    /// <remarks>By default WPF TabControl bound to an ItemsSource destroys visual state of invisible tabs. 
    /// Set Behaviors:TabContent.IsCached="True" to preserve visual state of each tab.
    /// </remarks>
    public static class TabContent
    {
        #region Attached dependency properties

        /// <summary>
        /// Controls whether tab content is cached or not
        /// </summary>
        /// <remarks>When TabContent.IsCached is true, visual state of each tab is preserved (cached), even when the tab is hidden</remarks>
        public static readonly DependencyProperty IsCachedProperty =
            DependencyProperty.RegisterAttached("IsCached", typeof(bool), typeof(TabContent), new UIPropertyMetadata(false, OnIsCachedChanged));

        /// <summary>
        /// Used instead of TabControl.ContentTemplate for cached tabs.
        /// </summary>
        public static readonly DependencyProperty TemplateProperty =
            DependencyProperty.RegisterAttached("Template", typeof(DataTemplate), typeof(TabContent), new UIPropertyMetadata(null));

        /// <summary>
        /// Used instead of TabControl.ContentTemplateSelector for cached tabs
        /// </summary>
        public static readonly DependencyProperty TemplateSelectorProperty =
            DependencyProperty.RegisterAttached("TemplateSelector", typeof(DataTemplateSelector), typeof(TabContent), new UIPropertyMetadata(null));

        /// <summary>
        /// Using a DependencyProperty as the backing store for InternalTabControl.  This enables animation, styling, binding, etc...
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static readonly DependencyProperty InternalTabControlProperty =
            DependencyProperty.RegisterAttached("InternalTabControl", typeof(TabControl), typeof(TabContent), new UIPropertyMetadata(null, OnInternalTabControlChanged));

        /// <summary>
        /// Using a DependencyProperty as the backing store for InternalCachedContent.  This enables animation, styling, binding, etc.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static readonly DependencyProperty InternalCachedContentProperty =
            DependencyProperty.RegisterAttached("InternalCachedContent", typeof(ContentControl), typeof(TabContent), new UIPropertyMetadata(null));

        /// <summary>
        ///  Using a DependencyProperty as the backing store for InternalContentManager.  This enables animation, styling, binding, etc.
        /// </summary>
        public static readonly DependencyProperty InternalContentManagerProperty =
            DependencyProperty.RegisterAttached("InternalContentManager", typeof(object), typeof(TabContent), new UIPropertyMetadata(null));

        #endregion Getters and setters of the attached properties

        #region Getters and setters of the attached properties

        /// <summary>
        /// Gets a value representing whether the tab content is cached or not.
        /// </summary>
        /// <param name="obj">The attached tab content object.</param>
        /// <returns>A bool value indicating whether the tab content is cached or not.</returns>
        public static bool GetIsCached(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsCachedProperty);
        }

        /// <summary>
        /// Sets the IsCached property, representing whether the tab content is cached or not.
        /// </summary>
        /// <param name="obj">The attached tab content object.</param>
        /// <param name="value">A bool value indicating whether the tab content is cached or not.</param>
        public static void SetIsCached(DependencyObject obj, bool value)
        {
            obj.SetValue(IsCachedProperty, value);
        }

        /// <summary>
        /// Gets a value representing the tab Content Template for cached tabs.
        /// </summary>
        /// <param name="obj">The attached tab content object.</param>
        /// <returns>The content template.</returns>
        public static DataTemplate GetTemplate(DependencyObject obj)
        {
            return (DataTemplate)obj.GetValue(TemplateProperty);
        }

        /// <summary>
        /// Gets the content template, used instead of TabControl.ContentTemplate for cached tabs.
        /// </summary>
        /// <param name="obj">The attached tab content object.</param>
        /// <param name="value">The content template.</param>
        public static void SetTemplate(DependencyObject obj, DataTemplate value)
        {
            obj.SetValue(TemplateProperty, value);
        }

        /// <summary>
        /// Gets the tab control content TemplateSelector.
        /// </summary>
        /// <param name="obj">The attached tab content object.</param>
        /// <returns>The tab control content TemplateSelector.</returns>
        public static DataTemplateSelector GetTemplateSelector(DependencyObject obj)
        {
            return (DataTemplateSelector)obj.GetValue(TemplateSelectorProperty);
        }

        /// <summary>
        /// Sets the tab control content TemplateSelector.
        /// </summary>
        /// <param name="obj">The attached tab content object.</param>
        /// <param name="value">The tab control content TemplateSelector.</param>
        public static void SetTemplateSelector(DependencyObject obj, DataTemplateSelector value)
        {
            obj.SetValue(TemplateSelectorProperty, value);
        }

        /// <summary>
        /// Gets the internal tab control.
        /// </summary>
        /// <param name="obj">The tab content object.</param>
        /// <returns>The backing store for the internal tab control object.</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static TabControl GetInternalTabControl(DependencyObject obj)
        {
            return (TabControl)obj.GetValue(InternalTabControlProperty);
        }

        /// <summary>
        /// Gets the internal tab control.
        /// </summary>
        /// <param name="obj">The tab content object.</param>
        /// <param name="value">The backing store for the internal tab control object..</param>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static void SetInternalTabControl(DependencyObject obj, TabControl value)
        {
            obj.SetValue(InternalTabControlProperty, value);
        }

        /// <summary>
        /// Gets the internal cached content control.
        /// </summary>
        /// <param name="obj">The tab content object.</param>
        /// <returns>The backing store for the internal cached content object.</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static ContentControl GetInternalCachedContent(DependencyObject obj)
        {
            return (ContentControl)obj.GetValue(InternalCachedContentProperty);
        }

        /// <summary>
        /// Sets the internal cached content control.
        /// </summary>
        /// <param name="obj">The tab content object.</param>
        /// <param name="value">The backing store for the internal cached content object.</param>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static void SetInternalCachedContent(DependencyObject obj, ContentControl value)
        {
            obj.SetValue(InternalCachedContentProperty, value);
        }

        /// <summary>
        /// Gets the internal content manager.
        /// </summary>
        /// <param name="obj">The tab content object.</param>
        /// <returns>The backing store for the internal content manager.</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static object GetInternalContentManager(DependencyObject obj)
        {
            return (object)obj.GetValue(InternalContentManagerProperty);
        }

        /// <summary>
        /// Sets the internal content manager.
        /// </summary>
        /// <param name="obj">The tab content object.</param>
        /// <param name="value">The backing store for the internal content manager.</param>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static void SetInternalContentManager(DependencyObject obj, object value)
        {
            obj.SetValue(InternalContentManagerProperty, value);
        }

        #endregion Getters and setters of the attached properties

        #region Event Handler

        /// <summary>
        /// Called when IsCached property changed.
        /// </summary>
        /// <param name="obj">The dependency object for which the property changed.</param>
        /// <param name="args">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnIsCachedChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            if (obj == null)
            {
                return;
            }

            var tabControl = obj as TabControl;
            if (tabControl == null)
            {
                throw new InvalidOperationException("Cannot set TabContent.IsCached on object of type " + args.NewValue.GetType().Name +
                    ". Only objects of type TabControl can have TabContent.IsCached property.");
            }

            var newValue = (bool)args.NewValue;
            if (!newValue)
            {
                if (args.OldValue != null && ((bool)args.OldValue))
                {
                    throw new NotImplementedException("Cannot change TabContent.IsCached from True to False. Turning tab caching off is not implemented");
                }

                return;
            }

            // Ensure the content template is null.
            if (tabControl.ContentTemplate != null)
            {
                throw new InvalidOperationException("TabControl.ContentTemplate value is not null. If TabContent.IsCached is True, use TabContent.Template instead of ContentTemplate");
            }

            // Create a data template and assign it to TabControl.ContentTemplate.
            tabControl.ContentTemplate = CreateContentTemplate();
        }

        /// <summary>
        /// Called when InternalTabControl property changed.
        /// </summary>
        /// <param name="obj">The dependency object for which the property changed.</param>
        /// <param name="args">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnInternalTabControlChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            if (obj == null)
            {
                return;
            }

            var container = obj as Decorator;
            if (container == null)
            {
                var message = "Cannot set TabContent.InternalTabControl on object of type " + obj.GetType().Name +
                    ". Only controls that derive from Decorator, such as Border can have a TabContent.InternalTabControl.";
                throw new InvalidOperationException(message);
            }

            if (args.NewValue == null)
            {
                return;
            }

            if (!(args.NewValue is TabControl))
            {
                throw new InvalidOperationException("Value of TabContent.InternalTabControl cannot be of type " + args.NewValue.GetType().Name + ", it must be of type TabControl");
            }

            var tabControl = (TabControl)args.NewValue;
            var contentManager = GetContentManager(tabControl, container);
            contentManager.UpdateSelectedTab();
        }

        #endregion Event Handler

        #region Private static methods

        /// <summary>
        /// Create a new DataTemplate.
        /// </summary>
        /// <returns>The created Data template.</returns>
        private static DataTemplate CreateContentTemplate()
        {
            const string Xaml =
                "<DataTemplate><Border b:TabContent.InternalTabControl=\"{Binding RelativeSource={RelativeSource AncestorType=TabControl}}\" /></DataTemplate>";

            var context = new ParserContext();

            context.XamlTypeMapper = new XamlTypeMapper(new string[0]);
            context.XamlTypeMapper.AddMappingProcessingInstruction("b", typeof(TabContent).Namespace, typeof(TabContent).Assembly.FullName);

            context.XmlnsDictionary.Add(string.Empty, "http://schemas.microsoft.com/winfx/2006/xaml/presentation");
            context.XmlnsDictionary.Add("b", "b");

            var template = (DataTemplate)XamlReader.Parse(Xaml, context);
            return template;
        }

        /// <summary>
        /// Obtain the ContentManager for the specific tab control.
        /// </summary>
        /// <param name="tabControl">The tab control.</param>
        /// <param name="container">THe decorator</param>
        /// <returns>The ContentManager generated for the tab control passed as parameter.</returns>
        private static ContentManager GetContentManager(TabControl tabControl, Decorator container)
        {
            var contentManager = (ContentManager)GetInternalContentManager(tabControl);
            if (contentManager != null)
            {
                // Content manager already exists for the tab control. This means that tab content template is applied 
                // again, and new instance of the Border control (container) has been created. The old container 
                // referenced by the content manager is no longer visible and needs to be replaced.
                contentManager.ReplaceContainer(container);
            }
            else
            {
                // Create a new content manager.
                contentManager = new ContentManager(tabControl, container);
                SetInternalContentManager(tabControl, contentManager);
            }

            return contentManager;
        }

        #endregion Private static methods

        #region Nested Classes

        /// <summary>
        /// The TabControl ContentManager class.
        /// </summary>
        public class ContentManager
        {
            /// <summary>
            /// The tab control.
            /// </summary>
            private TabControl tabControl;

            /// <summary>
            /// The border element.
            /// </summary>
            private Decorator border;

            /// <summary>
            /// Initializes a new instance of the <see cref="ContentManager" /> class.
            /// </summary>
            /// <param name="tabControl">The owner tab control.</param>
            /// <param name="border">The container border.</param>
            public ContentManager(TabControl tabControl, Decorator border)
            {
                this.tabControl = tabControl;
                this.border = border;
                this.tabControl.SelectionChanged += (sender, args) => UpdateSelectedTab();
            }

            /// <summary>
            /// Replace the border container for the current ContentManager.
            /// </summary>
            /// <param name="newBorder">The new container border.</param>
            public void ReplaceContainer(Decorator newBorder)
            {
                if (object.ReferenceEquals(border, newBorder))
                {
                    return;
                }

                border.Child = null; // detach any tab content that old border may hold
                border = newBorder;
            }

            /// <summary>
            /// Update the new selected tab content.
            /// </summary>
            public void UpdateSelectedTab()
            {
                border.Child = GetCurrentContent();
            }

            /// <summary>
            /// Obtain the cached content for the tab control selected tabitem.
            /// </summary>
            /// <returns>The tab item content control.</returns>
            private ContentControl GetCurrentContent()
            {
                var item = tabControl.SelectedItem;
                if (item == null)
                {
                    return null;
                }

                var tabItem = tabControl.ItemContainerGenerator.ContainerFromItem(item);
                if (tabItem == null)
                {
                    return null;
                }

                var cachedContent = TabContent.GetInternalCachedContent(tabItem);
                if (cachedContent == null)
                {
                    cachedContent = new ContentControl
                    {
                        DataContext = item,
                        ContentTemplate = TabContent.GetTemplate(tabControl),
                        ContentTemplateSelector = TabContent.GetTemplateSelector(tabControl)
                    };

                    cachedContent.SetBinding(ContentControl.ContentProperty, new Binding());
                    TabContent.SetInternalCachedContent(tabItem, cachedContent);
                }

                return cachedContent;
            }
        }

        #endregion Nested Classes
    }
}