﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Interactivity;
using System.Xml;
using ZPKTool.Common;
using ZPKTool.Gui.Utils;
using ZPKTool.Gui.ViewModels;

namespace ZPKTool.Gui.Behaviors
{
    /// <summary>
    /// A behavior that saves each data grid layout into a file, to persist the columns order and width.
    /// </summary>
    /// <remarks>
    /// The try-catch statements are used to prevent any possible error that might appear during grid layout's saving or restoring from disturbing the user.
    /// </remarks>
    public static class DataGridBehavior
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The path for the data grid layout configuration file.
        /// </summary>
        private static readonly string layoutConfigFilePath = Path.Combine(ZPKTool.Common.Constants.ApplicationDataFolderPath, "DataGridLayoutConfig.xml");

        /// <summary>
        /// The dictionary that saves the display index for each column of a grid.
        /// </summary>
        private static Dictionary<DataGrid, List<Tuple<DataGridColumn, int>>> dataGridColumnIndexes = new Dictionary<DataGrid, List<Tuple<DataGridColumn, int>>>();

        /// <summary>
        /// An object used to lock the file while reading / writing.
        /// </summary>
        private static object lockObject = new object();

        #region Dependency Properties

        /// <summary>
        /// Using a DependencyProperty as the backing store for PersistLayout.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty PersistLayoutProperty =
            DependencyProperty.RegisterAttached("PersistLayout", typeof(bool), typeof(DataGridBehavior), new PropertyMetadata(false, OnPersistLayoutChanged));

        /// <summary>
        /// Using a DependencyProperty as the backing store for PersistId.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty PersistIdProperty =
            DependencyProperty.RegisterAttached("PersistId", typeof(string), typeof(DataGridBehavior), new PropertyMetadata(null));

        /// <summary>
        /// Using a DependencyProperty as the backing store for BringIntoViewWhenSelected.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty BringIntoViewWhenSelectedProperty =
            DependencyProperty.RegisterAttached("BringIntoViewWhenSelected", typeof(bool), typeof(DataGridBehavior), new PropertyMetadata(false, OnBringIntoViewWhenSelectedChanged));

        /// <summary>
        /// Using a DependencyProperty as the backing store for CustomSortEnabled.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty CustomSortEnabledProperty =
            DependencyProperty.RegisterAttached("CustomSortEnabled", typeof(bool), typeof(DataGridBehavior), new PropertyMetadata(false, OnCustomSortEnabledChanged));

        /// <summary>
        /// Gets a value indicating whether the persist layout is enabled.
        /// </summary>
        /// <param name="obj">The dependency object.</param>
        /// <returns>The value.</returns>
        public static bool GetPersistLayout(DependencyObject obj)
        {
            return (bool)obj.GetValue(PersistLayoutProperty);
        }

        /// <summary>
        /// Sets a value indicating whether the persist layout is enabled.
        /// </summary>
        /// <param name="obj">The dependency object.</param>
        /// <param name="value">The value.</param>
        public static void SetPersistLayout(DependencyObject obj, bool value)
        {
            obj.SetValue(PersistLayoutProperty, value);
        }

        /// <summary>
        /// Gets the persist id.
        /// </summary>
        /// <param name="obj">The dependency object.</param>
        /// <returns>The value.</returns>
        public static string GetPersistId(DependencyObject obj)
        {
            return (string)obj.GetValue(PersistIdProperty);
        }

        /// <summary>
        /// Sets the persist id.
        /// </summary>
        /// <param name="obj">The dependency object.</param>
        /// <param name="value">The value.</param>
        public static void SetPersistId(DependencyObject obj, string value)
        {
            obj.SetValue(PersistIdProperty, value);
        }

        /// <summary>
        /// Gets the BringIntoViewWhenSelected value.
        /// </summary>
        /// <param name="obj">The dependency object.</param>
        /// <returns>The value.</returns>
        public static bool GetBringIntoViewWhenSelected(DependencyObject obj)
        {
            return (bool)obj.GetValue(BringIntoViewWhenSelectedProperty);
        }

        /// <summary>
        /// Sets a value indicating whether the BringIntoViewWhenSelected is enabled.
        /// </summary>
        /// <param name="obj">The dependency object.</param>
        /// <param name="value">The value.</param>
        public static void SetBringIntoViewWhenSelected(DependencyObject obj, bool value)
        {
            obj.SetValue(BringIntoViewWhenSelectedProperty, value);
        }

        /// <summary>
        /// Gets the CustomSortEnabled value.
        /// </summary>
        /// <param name="obj">The dependency object.</param>
        /// <returns>The value.</returns>
        public static bool GetCustomSortEnabled(DependencyObject obj)
        {
            return (bool)obj.GetValue(CustomSortEnabledProperty);
        }

        /// <summary>
        /// Sets a value indicating whether the CustomSortEnabled is enabled.
        /// </summary>
        /// <param name="obj">The dependency object.</param>
        /// <param name="value">The value.</param>
        public static void SetCustomSortEnabled(DependencyObject obj, bool value)
        {
            obj.SetValue(CustomSortEnabledProperty, value);
        }

        #endregion Dependency Properties

        /// <summary>
        /// Called when the BringIntoViewWhenSelected property is changed.
        /// </summary>
        /// <param name="d">The dependency object.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnBringIntoViewWhenSelectedChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var datagrid = d as DataGrid;
            if (datagrid == null)
            {
                throw new InvalidOperationException("BringIntoView is supported only for DataGrid.");
            }

            if ((bool)e.NewValue)
            {
                datagrid.SelectionChanged += Datagrid_SelectionChanged;
            }
            else
            {
                datagrid.SelectionChanged -= Datagrid_SelectionChanged;
            }
        }

        /// <summary>
        /// Called when the CustomSortEnabled property is changed.
        /// </summary>
        /// <param name="d">The dependency object.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        /// <exception cref="System.InvalidOperationException">Custom Sort is supported only for DataGrid.</exception>
        private static void OnCustomSortEnabledChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            DataGrid dataGrid = d as DataGrid;
            if (dataGrid == null)
            {
                throw new InvalidOperationException("Custom Sort is supported only for DataGrid.");
            }

            if ((bool)e.NewValue)
            {
                dataGrid.Sorting += DataGrid_Sorting;
            }
            else
            {
                dataGrid.Sorting -= DataGrid_Sorting;
            }
        }

        #region DataGrid layout

        /// <summary>
        /// Handles the SelectionChanged event of a DataGrid.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private static void Datagrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender is DataGrid)
            {
                DataGrid grid = sender as DataGrid;
                if (grid.SelectedItem != null)
                {
                    grid.ScrollIntoView(grid.SelectedItem, null);
                }
            }
        }

        /// <summary>
        /// Called when the PersistLayout property is changed.
        /// </summary>
        /// <param name="d">The dependency object.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnPersistLayoutChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var dataGrid = d as DataGrid;
            if (dataGrid == null)
            {
                throw new InvalidOperationException("Layout persistence is supported only for DataGrid.");
            }

            bool enabled = (bool)e.NewValue;
            if (enabled)
            {
                dataGrid.Loaded += DataGridLoaded;
                dataGrid.Unloaded += DataGridUnloaded;
                dataGrid.ColumnReordered += DataGridColumnReordered;
            }
            else
            {
                dataGrid.Loaded -= DataGridLoaded;
                dataGrid.Unloaded -= DataGridUnloaded;
                dataGrid.ColumnReordered -= DataGridColumnReordered;
            }
        }

        /// <summary>
        /// Handles the ColumnReordered event of a DataGrid.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.Controls.DataGridColumnEventArgs"/> instance containing the event data.</param>
        private static void DataGridColumnReordered(object sender, DataGridColumnEventArgs e)
        {
            try
            {
                var dataGrid = (DataGrid)sender;
                var newColIndexes = new List<Tuple<DataGridColumn, int>>();
                foreach (var column in dataGrid.Columns)
                {
                    newColIndexes.Add(new Tuple<DataGridColumn, int>(column, column.DisplayIndex));
                }

                dataGridColumnIndexes[dataGrid] = newColIndexes;
            }
            catch (Exception ex)
            {
                log.WarnException("The ColumnReordered event could not be handled.", ex);
            }
        }

        /// <summary>
        /// Handles the Loaded event of a DataGrid.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private static void DataGridLoaded(object sender, RoutedEventArgs e)
        {
            try
            {
                var dataGrid = (DataGrid)sender;
                RestoreDataGridLayout(dataGrid);

                var columnIndexes = new List<Tuple<DataGridColumn, int>>();
                foreach (var column in dataGrid.Columns)
                {
                    columnIndexes.Add(new Tuple<DataGridColumn, int>(column, column.DisplayIndex));
                }

                dataGridColumnIndexes[dataGrid] = columnIndexes;
            }
            catch (Exception ex)
            {
                log.WarnException("The Loaded event could not be handled.", ex);
            }
        }

        /// <summary>
        /// Handles the Unloaded event of a DataGrid.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private static void DataGridUnloaded(object sender, RoutedEventArgs e)
        {
            try
            {
                var dataGrid = (DataGrid)sender;
                SaveDataGridLayout(dataGrid, dataGridColumnIndexes);

                dataGridColumnIndexes.Remove(dataGrid);
            }
            catch (Exception ex)
            {
                log.WarnException("The Unloaded event could not be handled.", ex);
            }
        }

        /// <summary>
        /// Restores the data grid layout by reading the values from the xml file.
        /// </summary>
        /// <param name="dataGrid">The DataGrid</param>
        public static void RestoreDataGridLayout(DataGrid dataGrid)
        {
            if (dataGrid == null || string.IsNullOrEmpty(GetPersistId(dataGrid)))
            {
                return;
            }

            if (DesignerProperties.GetIsInDesignMode(dataGrid))
            {
                // If the application runs in design mode, a process overwrites the xml document, so the persistence is irrelevant.
                return;
            }

            lock (lockObject)
            {
                try
                {
                    if (File.Exists(layoutConfigFilePath))
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.Load(layoutConfigFilePath);

                        // Select the root of the document.
                        XmlNode root = doc.SelectSingleNode("GridLayout");
                        if (root == null)
                        {
                            throw new XmlException("The document's root element is missing.");
                        }

                        // Select the Grid element that has the same name as the current data grid.
                        string gridQuery = string.Format("Grid[@Name='{0}']", GetPersistId(dataGrid));
                        XmlNode gridNode = root.SelectSingleNode(gridQuery);

                        if (gridNode != null)
                        {
                            // Create a list with all the data grid column ids.
                            List<string> dataGridColumnIds = new List<string>();
                            foreach (var column in dataGrid.Columns)
                            {
                                dataGridColumnIds.Add(GetColumnId(column));
                            }

                            // Read data from the column nodes into a list for each column.
                            List<ColumnLayoutConfig> columnsLayout = new List<ColumnLayoutConfig>();
                            foreach (XmlNode columnNode in gridNode.ChildNodes)
                            {
                                ColumnLayoutConfig columnLayoutInfo = new ColumnLayoutConfig()
                                {
                                    Uid = columnNode.Attributes["Uid"].Value,
                                    DisplayIndex = int.Parse(columnNode.Attributes["DisplayIndex"].Value, CultureInfo.InvariantCulture),
                                    Width = double.Parse(columnNode.Attributes["Width"].Value, CultureInfo.InvariantCulture),
                                    WidthType = columnNode.Attributes["WidthType"].Value.ToString()
                                };

                                if (dataGridColumnIds.Contains(columnLayoutInfo.Uid))
                                {
                                    // Add to column layout list only the columns that have matching id with the data grid columns.
                                    columnsLayout.Add(columnLayoutInfo);
                                }
                            }

                            columnsLayout.Sort(d => d.DisplayIndex);
                            for (int i = 0; i < columnsLayout.Count; i++)
                            {
                                // Arrange the columns by ordered display indexes, starting from 0.
                                columnsLayout[i].DisplayIndex = i;
                            }

                            // Search for new columns added later into grid (or columns with a changed id) and save them into the list.
                            ColumnLayoutConfig gridColumnConfig = null;
                            foreach (DataGridColumn gridColumn in dataGrid.Columns)
                            {
                                // Save the grid columns configuration only if the grid is initialized.
                                if (gridColumn.DisplayIndex >= 0)
                                {
                                    gridColumnConfig = columnsLayout.FirstOrDefault(d => d.Uid == GetColumnId(gridColumn));
                                    if (gridColumnConfig == null)
                                    {
                                        gridColumnConfig = new ColumnLayoutConfig()
                                        {
                                            Uid = GetColumnId(gridColumn),
                                            DisplayIndex = gridColumn.DisplayIndex,
                                            Width = gridColumn.Width.DisplayValue,
                                            WidthType = gridColumn.Width.UnitType.ToString()
                                        };

                                        // All columns' display indexes grater than the new column's display index are incremented.
                                        foreach (var item in columnsLayout.Where(i => i.DisplayIndex >= gridColumnConfig.DisplayIndex))
                                        {
                                            item.DisplayIndex++;
                                        }

                                        columnsLayout.Add(gridColumnConfig);
                                    }

                                    // Restore the layout settings.
                                    gridColumn.DisplayIndex = gridColumnConfig.DisplayIndex;
                                    gridColumn.Width = new DataGridLength(gridColumnConfig.Width, (DataGridLengthUnitType)Enum.Parse(typeof(DataGridLengthUnitType), gridColumnConfig.WidthType));
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    string message = string.Format("The layout could not be restored for data grid with the id '{0}'.", GetPersistId(dataGrid));
                    log.WarnException(message, ex);
                }
            }
        }

        /// <summary>
        /// Save the data grid layout into the xml file.
        /// </summary>
        /// <param name="dataGrid">The DataGrid.</param>
        /// <param name="dataGridColumnIndexes">The dictionary containing the column indexes.</param>
        public static void SaveDataGridLayout(DataGrid dataGrid, Dictionary<DataGrid, List<Tuple<DataGridColumn, int>>> dataGridColumnIndexes)
        {
            if (dataGrid == null || string.IsNullOrEmpty(GetPersistId(dataGrid)))
            {
                return;
            }

            if (DesignerProperties.GetIsInDesignMode(dataGrid))
            {
                // If the application runs in design mode, a process overwrites the xml document, so the persistence is irrelevant.
                return;
            }

            lock (lockObject)
            {
                if (!File.Exists(layoutConfigFilePath))
                {
                    // If the file does not exist for some reason, recreate it.
                    CreateDataGridLayoutConfigFile();
                }

                try
                {
                    // Load the file to save the new values.
                    XmlDocument doc = new XmlDocument();
                    doc.Load(layoutConfigFilePath);

                    XmlElement root = doc.DocumentElement;
                    if (root == null)
                    {
                        // Throw XmlException so the xml document is re-created in the catch at the end.
                        throw new XmlException("The document's root element is missing.");
                    }

                    string gridQuery = string.Format("Grid[@Name='{0}']", GetPersistId(dataGrid));
                    XmlNode gridNode = root.SelectSingleNode(gridQuery);

                    // If the layout for this grid is not saved in the XML document, the grid with the corresponding columns will be created.
                    if (gridNode == null)
                    {
                        gridNode = doc.CreateElement("Grid");
                        XmlAttribute gridAttribute = doc.CreateAttribute("Name");
                        gridAttribute.Value = GetPersistId(dataGrid);
                        root.AppendChild(gridNode);
                        gridNode.Attributes.Append(gridAttribute);
                    }

                    List<string> columnIds = new List<string>();
                    foreach (DataGridColumn column in dataGrid.Columns)
                    {
                        var colPersistId = GetColumnId(column);
                        columnIds.Add(colPersistId);
                        if (dataGridColumnIndexes.ContainsKey(dataGrid) && dataGridColumnIndexes[dataGrid].FirstOrDefault(c => c.Item1 == column) != null)
                        {
                            var displayIndex = dataGridColumnIndexes[dataGrid].FirstOrDefault(c => c.Item1 == column).Item2;

                            // If the column has an Uid, save it in the XML file. If an Uid does not exist, the column is not saved because it can not be identified.
                            // Also, if the display index of the column is not in the range, the layout is not saved.
                            if (!string.IsNullOrWhiteSpace(colPersistId) && displayIndex >= 0 && displayIndex < dataGrid.Columns.Count)
                            {
                                var columnNode = gridNode.SelectSingleNode(string.Format("Column[@Uid='{0}']", colPersistId));
                                if (columnNode == null)
                                {
                                    columnNode = doc.CreateElement("Column");
                                    gridNode.AppendChild(columnNode);
                                }

                                var headerAttr = columnNode.Attributes["Uid"];
                                if (headerAttr == null)
                                {
                                    headerAttr = doc.CreateAttribute("Uid");
                                    columnNode.Attributes.Append(headerAttr);
                                }

                                headerAttr.Value = colPersistId;

                                var widthAttr = columnNode.Attributes["Width"];
                                if (widthAttr == null)
                                {
                                    widthAttr = doc.CreateAttribute("Width");
                                    columnNode.Attributes.Append(widthAttr);
                                }

                                widthAttr.Value = column.Width.DisplayValue.ToString(CultureInfo.InvariantCulture);

                                var widthTypeAttr = columnNode.Attributes["WidthType"];
                                if (widthTypeAttr == null)
                                {
                                    widthTypeAttr = doc.CreateAttribute("WidthType");
                                    columnNode.Attributes.Append(widthTypeAttr);
                                }

                                widthTypeAttr.Value = column.Width.UnitType.ToString();

                                var displayIndexAttr = columnNode.Attributes["DisplayIndex"];
                                if (displayIndexAttr == null)
                                {
                                    displayIndexAttr = doc.CreateAttribute("DisplayIndex");
                                    columnNode.Attributes.Append(displayIndexAttr);
                                }

                                displayIndexAttr.Value = displayIndex.ToString(CultureInfo.InvariantCulture);
                            }
                        }
                    }

                    List<XmlNode> nodesToRemove = new List<XmlNode>();
                    foreach (XmlNode columnNode in gridNode.ChildNodes)
                    {
                        var columnNodeId = columnNode.Attributes["Uid"].Value;
                        if (!columnIds.Contains(columnNodeId))
                        {
                            // Add to the list of nodes to remove the ones that are no longer into data grid.
                            nodesToRemove.Add(columnNode);
                        }
                    }

                    foreach (XmlNode node in nodesToRemove)
                    {
                        // Remove from file the nodes.
                        gridNode.RemoveChild(node);
                    }

                    if (gridNode.ChildNodes.Count == 0)
                    {
                        // Remove the grid node from file if it is empty.
                        root.RemoveChild(gridNode);
                    }

                    doc.Save(layoutConfigFilePath);
                }
                catch (Exception ex)
                {
                    string message = string.Format("The layout could not be saved for data grid with the id '{0}'.", GetPersistId(dataGrid));
                    log.WarnException(message, ex);
                }
            }
        }

        /// <summary>
        /// Creates the file that contains the data grid layout configuration. It writes only the root element, which is GridLayout.
        /// </summary>
        private static void CreateDataGridLayoutConfigFile()
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                XmlElement rootElem = doc.CreateElement("GridLayout");
                doc.AppendChild(rootElem);
                doc.Save(layoutConfigFilePath);
            }
            catch (Exception ex)
            {
                log.WarnException("The data grid layout file could not be created.", ex);
            }
        }

        /// <summary>
        /// Get the Uid for the column using the persist ID, the header or the text block from an UI Element.
        /// </summary>
        /// <param name="gridColumn">The column.</param>
        /// <returns>The column's Uid.</returns>
        private static string GetColumnId(DataGridColumn gridColumn)
        {
            var colPersistId = GetPersistId(gridColumn);
            if (string.IsNullOrWhiteSpace(colPersistId))
            {
                var header = gridColumn.Header as string;
                if (string.IsNullOrWhiteSpace(header))
                {
                    var headerElement = gridColumn.Header as UIElement;
                    if (headerElement != null)
                    {
                        var headerTextElement = UIHelper.FindChildren<TextBlock>(headerElement).FirstOrDefault();
                        if (headerTextElement != null)
                        {
                            header = headerTextElement.Text;
                        }
                        else
                        {
                            // The data grid columns must have a persist id or header to be identified.
                            log.Error("The data grid layout persistence cannot be applied for columns without persist id or header.");
                        }
                    }
                }

                colPersistId = header;
            }

            return colPersistId;
        }

        #endregion

        #region DataGrid sorting

        /// <summary>
        /// Handles the Sorting event of the DataGrid control.
        /// A custom sort is used for the columns that are bound to Enum values.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DataGridSortingEventArgs"/> instance containing the event data.</param>
        private static void DataGrid_Sorting(object sender, DataGridSortingEventArgs e)
        {
            DataGrid dataGrid = sender as DataGrid;
            if (dataGrid != null && dataGrid.ItemsSource != null)
            {
                // Check the binding for grids that contain items.
                var boundColumn = e.Column as DataGridBoundColumn;
                if (boundColumn != null)
                {
                    // Get the binding from the column to sort.
                    Binding binding = boundColumn.Binding as Binding;
                    if (binding != null && binding.Converter != null
                        && typeof(Converters.EnumToStringConverter) == binding.Converter.GetType())
                    {
                        // If the binding for column uses an enum converter, check its converter parameter.
                        if (binding.ConverterParameter != null)
                        {
                            // If the converter parameter, check its type.
                            var type = binding.ConverterParameter as Type;
                            if (type.IsEnum)
                            {
                                // Apply the sorting algorithm only if the column's items are Enums.
                                var dataView = CollectionViewSource.GetDefaultView(dataGrid.ItemsSource) as ListCollectionView;
                                if (dataView != null)
                                {
                                    // Manually set the sort direction.
                                    e.Column.SortDirection = (e.Column.SortDirection == null || e.Column.SortDirection == ListSortDirection.Descending) ? ListSortDirection.Ascending : ListSortDirection.Descending;

                                    // The custom sort can be used only for ListCollectionView.
                                    dataView.CustomSort = new EnumValueComparer(e.Column.SortDirection.Value, binding);
                                }

                                // Prevent the grid's default sorting to be applied again.
                                e.Handled = true;
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region private classes

        /// <summary>
        /// A private class that holds the information about columns' layout configuration.
        /// </summary>
        private class ColumnLayoutConfig
        {
            /// <summary>
            /// Gets or sets the unique id.
            /// </summary>
            public string Uid { get; set; }

            /// <summary>
            /// Gets or sets the display index.
            /// </summary>
            public int DisplayIndex { get; set; }

            /// <summary>
            /// Gets or sets the width.
            /// </summary>
            public double Width { get; set; }

            /// <summary>
            /// Gets or sets the width type.
            /// </summary>
            public string WidthType { get; set; }
        }

        /// <summary>
        /// A private class that implements a custom comparer for enum values.
        /// </summary>
        private class EnumValueComparer : IComparer
        {
            /// <summary>
            /// The sort direction.
            /// </summary>,
            private ListSortDirection sortDirection;

            /// <summary>
            /// The property's binding.
            /// </summary>
            private Binding binding;

            /// <summary>
            /// Initializes a new instance of the <see cref="EnumValueComparer"/> class.
            /// </summary>
            /// <param name="direction">The sort direction.</param>
            /// <param name="binding">The binding.</param>
            public EnumValueComparer(ListSortDirection direction, Binding binding)
            {
                this.sortDirection = direction;
                this.binding = binding;
            }

            /// <summary>
            /// Compares the string value of the specified objects.
            /// </summary>
            /// <param name="firstObject">The first object.</param>
            /// <param name="secondObject">The second object.</param>
            /// <returns>An integer indicating the objects relative position in the sort order.</returns>
            public int Compare(object firstObject, object secondObject)
            {
                string firstObjectValue = this.GetRowValueFromBinding(firstObject);
                string secondObjectValue = this.GetRowValueFromBinding(secondObject);

                if (string.IsNullOrEmpty(firstObjectValue) && string.IsNullOrEmpty(secondObjectValue))
                {
                    // If both values are null or empty, it returns 0.
                    return 0;
                }

                // Get the result to return depending on the sort direction.
                int result = sortDirection == ListSortDirection.Ascending ? 1 : -1;

                if (string.IsNullOrEmpty(firstObjectValue))
                {
                    return -result;
                }

                if (string.IsNullOrEmpty(secondObjectValue))
                {
                    return result;
                }

                // The values for both objects are compared using the current culture. The returned value depends on the sorting direction.
                return result * string.Compare(firstObjectValue, secondObjectValue, StringComparison.CurrentCulture);
            }

            /// <summary>
            /// Gets the value for each row, using the binding path.
            /// </summary>
            /// <param name="value">The value.</param>
            /// <returns>The string value of the row.</returns>
            private string GetRowValueFromBinding(object value)
            {
                object rowValue = ReflectionUtils.GetComplexPropertyValue(value, binding.Path.Path);

                var bindingConverter = binding.Converter;
                if (bindingConverter != null)
                {
                    // If the enum value is displayed using a converter, then the row value calls it.
                    rowValue = bindingConverter.Convert(rowValue, null, binding.ConverterParameter, System.Globalization.CultureInfo.CurrentCulture);
                }

                if (rowValue != null)
                {
                    return rowValue.ToString();
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        #endregion
    }
}