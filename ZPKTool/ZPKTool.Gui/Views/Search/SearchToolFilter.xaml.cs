﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using ZPKTool.Business.Search;
using ZPKTool.Gui.Resources;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for SearchToolFilter.xaml
    /// </summary>
    public partial class SearchToolFilter : Popup
    {
        /// <summary>
        /// Selection property registered as a DP to support data binding
        /// </summary>
        public static readonly DependencyProperty SelectionProperty = DependencyProperty.Register("Selection", typeof(SearchFilter), typeof(SearchToolFilter));

        /// <summary>
        /// Initializes a new instance of the <see cref="SearchToolFilter"/> class.
        /// </summary>
        public SearchToolFilter()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(SearchToolFilter_Loaded);
        }

        /// <summary>
        /// Gets or sets the Selection Property
        /// </summary>
        public SearchFilter Selection
        {
            get { return (SearchFilter)GetValue(SelectionProperty); }
            set { SetValue(SelectionProperty, value); }
        }

        /// <summary>
        /// Handler for the Loaded event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs" /> instance containing the event data.</param>
        private void SearchToolFilter_Loaded(object sender, RoutedEventArgs e)
        {
            if ((this.Selection & SearchFilter.Projects) != 0)
            {
                this.ProjectsCheckBox.IsChecked = true;
            }

            if ((this.Selection & SearchFilter.Assemblies) != 0)
            {
                this.AssembliesCheckBox.IsChecked = true;
            }

            if ((this.Selection & SearchFilter.Parts) != 0)
            {
                this.PartsCheckBox.IsChecked = true;
            }

            if ((this.Selection & SearchFilter.ProcessSteps) != 0)
            {
                this.ProcessStepsCheckBox.IsChecked = true;
            }

            if ((this.Selection & SearchFilter.Machines) != 0)
            {
                this.MachinesCheckBox.IsChecked = true;
            }

            if ((this.Selection & SearchFilter.RawMaterials) != 0)
            {
                this.RawMaterialsCheckBox.IsChecked = true;
            }

            if ((this.Selection & SearchFilter.Commodities) != 0)
            {
                this.CommodityCheckBox.IsChecked = true;
            }

            if ((this.Selection & SearchFilter.Consumables) != 0)
            {
                this.ConsumableCheckBox.IsChecked = true;
            }

            if ((this.Selection & SearchFilter.Dies) != 0)
            {
                this.DiesCheckBox.IsChecked = true;
            }

            if ((this.Selection & SearchFilter.RawParts) != 0)
            {
                this.RawPartsCheckBox.IsChecked = true;
            }

            AttachHandlers();

            if (this.Selection == (SearchFilter.Projects | SearchFilter.Assemblies | SearchFilter.Parts |
                                   SearchFilter.Machines | SearchFilter.RawMaterials | SearchFilter.ProcessSteps |
                                   SearchFilter.Commodities | SearchFilter.Consumables | SearchFilter.Dies | SearchFilter.RawParts))
            {
                this.SelectAllCheckbox.IsChecked = true;
                this.SelectAllCheckbox.Content = LocalizedResources.General_UncheckAll;
            }
        }

        /// <summary>
        /// Methods which refreshes the selection property
        /// </summary>
        private void Refresh()
        {
            SearchFilter filter = SearchFilter.None;

            if (this.ProjectsCheckBox.IsChecked == true)
            {
                filter |= SearchFilter.Projects;
            }

            if (this.AssembliesCheckBox.IsChecked == true)
            {
                filter |= SearchFilter.Assemblies;
            }

            if (this.PartsCheckBox.IsChecked == true)
            {
                filter |= SearchFilter.Parts;
            }

            if (this.RawMaterialsCheckBox.IsChecked == true)
            {
                filter |= SearchFilter.RawMaterials;
            }

            if (this.CommodityCheckBox.IsChecked == true)
            {
                filter |= SearchFilter.Commodities;
            }

            if (this.MachinesCheckBox.IsChecked == true)
            {
                filter |= SearchFilter.Machines;
            }

            if (this.DiesCheckBox.IsChecked == true)
            {
                filter |= SearchFilter.Dies;
            }

            if (this.ConsumableCheckBox.IsChecked == true)
            {
                filter |= SearchFilter.Consumables;
            }

            if (this.ProcessStepsCheckBox.IsChecked == true)
            {
                filter |= SearchFilter.ProcessSteps;
            }

            if (this.RawPartsCheckBox.IsChecked == true)
            {
                filter |= SearchFilter.RawParts;
            }

            this.Selection = filter;
        }

        /// <summary>
        /// Handles the Checked event of the SelectAllCheckbox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void SelectAllCheckbox_Checked(object sender, RoutedEventArgs e)
        {
            DetachHandlers();
            ProjectsCheckBox.IsChecked = true;
            PartsCheckBox.IsChecked = true;
            AssembliesCheckBox.IsChecked = true;
            MachinesCheckBox.IsChecked = true;
            RawMaterialsCheckBox.IsChecked = true;
            CommodityCheckBox.IsChecked = true;
            ProcessStepsCheckBox.IsChecked = true;
            ConsumableCheckBox.IsChecked = true;
            DiesCheckBox.IsChecked = true;
            RawPartsCheckBox.IsChecked = true;
            SelectAllCheckbox.Content = LocalizedResources.General_UncheckAll;
            Refresh();
            AttachHandlers();
        }

        /// <summary>
        /// Handles the Unchecked event of the SelectAllCheckbox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void SelectAllCheckbox_Unchecked(object sender, RoutedEventArgs e)
        {
            DetachHandlers();
            ProjectsCheckBox.IsChecked = false;
            PartsCheckBox.IsChecked = false;
            AssembliesCheckBox.IsChecked = false;
            MachinesCheckBox.IsChecked = false;
            RawMaterialsCheckBox.IsChecked = false;
            CommodityCheckBox.IsChecked = false;
            ProcessStepsCheckBox.IsChecked = false;
            ConsumableCheckBox.IsChecked = false;
            DiesCheckBox.IsChecked = false;
            RawPartsCheckBox.IsChecked = false;
            SelectAllCheckbox.Content = LocalizedResources.General_CheckAll;
            Refresh();
            AttachHandlers();
        }

        /// <summary>
        /// Handles the Unchecked event of the checkboxes
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            Refresh();
        }

        /// <summary>
        /// Handles the Checked event of the checkboxes
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            Refresh();
        }

        /// <summary>
        /// Handles the Click event of the CloseLookInButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void CloseFiltersButton_Click(object sender, RoutedEventArgs e)
        {
            this.IsOpen = false;
        }

        /// <summary>
        /// Attaches the Checked and Unchecked handlers
        /// </summary>
        private void AttachHandlers()
        {
            ProjectsCheckBox.Checked += new RoutedEventHandler(CheckBox_Checked);
            ProjectsCheckBox.Unchecked += new RoutedEventHandler(CheckBox_Unchecked);
            AssembliesCheckBox.Checked += new RoutedEventHandler(CheckBox_Checked);
            AssembliesCheckBox.Unchecked += new RoutedEventHandler(CheckBox_Unchecked);
            PartsCheckBox.Checked += new RoutedEventHandler(CheckBox_Checked);
            PartsCheckBox.Unchecked += new RoutedEventHandler(CheckBox_Unchecked);
            MachinesCheckBox.Checked += new RoutedEventHandler(CheckBox_Checked);
            MachinesCheckBox.Unchecked += new RoutedEventHandler(CheckBox_Unchecked);
            RawMaterialsCheckBox.Checked += new RoutedEventHandler(CheckBox_Checked);
            RawMaterialsCheckBox.Unchecked += new RoutedEventHandler(CheckBox_Unchecked);
            CommodityCheckBox.Checked += new RoutedEventHandler(CheckBox_Checked);
            CommodityCheckBox.Unchecked += new RoutedEventHandler(CheckBox_Unchecked);
            ConsumableCheckBox.Checked += new RoutedEventHandler(CheckBox_Checked);
            ConsumableCheckBox.Unchecked += new RoutedEventHandler(CheckBox_Unchecked);
            DiesCheckBox.Checked += new RoutedEventHandler(CheckBox_Checked);
            DiesCheckBox.Unchecked += new RoutedEventHandler(CheckBox_Unchecked);
            ProcessStepsCheckBox.Checked += new RoutedEventHandler(CheckBox_Checked);
            ProcessStepsCheckBox.Unchecked += new RoutedEventHandler(CheckBox_Unchecked);
            RawPartsCheckBox.Checked += new RoutedEventHandler(CheckBox_Unchecked);
            RawPartsCheckBox.Unchecked += new RoutedEventHandler(CheckBox_Unchecked);
        }

        /// <summary>
        /// Detaches the Checked and Unchecked handlers
        /// </summary>
        private void DetachHandlers()
        {
            ProjectsCheckBox.Checked -= new RoutedEventHandler(CheckBox_Checked);
            ProjectsCheckBox.Unchecked -= new RoutedEventHandler(CheckBox_Unchecked);
            AssembliesCheckBox.Checked -= new RoutedEventHandler(CheckBox_Checked);
            AssembliesCheckBox.Unchecked -= new RoutedEventHandler(CheckBox_Unchecked);
            PartsCheckBox.Checked -= new RoutedEventHandler(CheckBox_Checked);
            PartsCheckBox.Unchecked -= new RoutedEventHandler(CheckBox_Unchecked);
            MachinesCheckBox.Checked -= new RoutedEventHandler(CheckBox_Checked);
            MachinesCheckBox.Unchecked -= new RoutedEventHandler(CheckBox_Unchecked);
            RawMaterialsCheckBox.Checked -= new RoutedEventHandler(CheckBox_Checked);
            RawMaterialsCheckBox.Unchecked -= new RoutedEventHandler(CheckBox_Unchecked);
            CommodityCheckBox.Checked -= new RoutedEventHandler(CheckBox_Checked);
            CommodityCheckBox.Unchecked -= new RoutedEventHandler(CheckBox_Unchecked);
            ConsumableCheckBox.Checked -= new RoutedEventHandler(CheckBox_Checked);
            ConsumableCheckBox.Unchecked -= new RoutedEventHandler(CheckBox_Unchecked);
            DiesCheckBox.Checked -= new RoutedEventHandler(CheckBox_Checked);
            DiesCheckBox.Unchecked -= new RoutedEventHandler(CheckBox_Unchecked);
            ProcessStepsCheckBox.Checked -= new RoutedEventHandler(CheckBox_Checked);
            ProcessStepsCheckBox.Unchecked -= new RoutedEventHandler(CheckBox_Unchecked);
            RawPartsCheckBox.Checked -= new RoutedEventHandler(CheckBox_Unchecked);
            RawPartsCheckBox.Unchecked -= new RoutedEventHandler(CheckBox_Unchecked);
        }
    }
}
