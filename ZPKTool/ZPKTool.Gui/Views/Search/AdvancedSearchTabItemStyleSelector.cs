﻿using System.Windows;
using System.Windows.Controls;
using ZPKTool.Gui.ViewModels;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Selects the tab item style to use in advanced search.
    /// </summary>
    public class AdvancedSearchTabItemStyleSelector : StyleSelector
    {
        /// <summary>
        /// When overridden in a derived class, returns a <see cref="T:System.Windows.Style" /> based on custom logic.
        /// </summary>
        /// <param name="item">The content.</param>
        /// <param name="container">The element to which the style will be applied.</param>
        /// <returns>
        /// Returns an application-specific style to apply; otherwise, null.
        /// </returns>
        public override Style SelectStyle(object item, DependencyObject container)
        {
            var itemsControl = ItemsControl.ItemsControlFromItemContainer(container);

            if (item is AdvancedSearchConfigurationViewModel)
            {
                return (Style)itemsControl.FindResource("ConfigurationItemStyle");
            }
            else if (item is AdvancedSearchResultViewModel)
            {
                return (Style)itemsControl.FindResource("ResultItemStyle");
            }

            return base.SelectStyle(item, container);
        }
    }
}