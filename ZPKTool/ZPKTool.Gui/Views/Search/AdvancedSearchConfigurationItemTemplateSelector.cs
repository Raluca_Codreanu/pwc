﻿using System.Windows;
using System.Windows.Controls;
using ZPKTool.Gui.ViewModels;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Selects the template to use for the advanced search items.
    /// </summary>
    public class AdvancedSearchConfigurationItemTemplateSelector : DataTemplateSelector
    {
        /// <summary>
        /// Gets or sets the empty template.
        /// </summary>
        public DataTemplate EmptyTemplate { get; set; }

        /// <summary>
        /// Gets or sets the text extended text box template.
        /// </summary>
        public DataTemplate TextExtendedTextBoxTemplate { get; set; }

        /// <summary>
        /// Gets or sets the number extended text box template.
        /// </summary>
        public DataTemplate NumberExtendedTextBoxTemplate { get; set; }

        /// <summary>
        /// Gets or sets the range of extended text boxes template.
        /// </summary>
        public DataTemplate RangeExtendedTextBoxTemplate { get; set; }

        /// <summary>
        /// Gets or sets the check box template.
        /// </summary>
        public DataTemplate CheckBoxTemplate { get; set; }

        /// <summary>
        /// Gets or sets the year picker template.
        /// </summary>
        public DataTemplate YearPickerTemplate { get; set; }

        /// <summary>
        /// Gets or sets the date picker template.
        /// </summary>
        public DataTemplate DatePickerTemplate { get; set; }

        /// <summary>
        /// Gets or sets the users combo box template.
        /// </summary>
        public DataTemplate UsersComboBoxTemplate { get; set; }

        /// <summary>
        /// Gets or sets the calculation variant combo box template.
        /// </summary>
        public DataTemplate CalculationVariantComboBoxTemplate { get; set; }

        /// <summary>
        /// Gets or sets the raw material delivery type combo box template.
        /// </summary>
        public DataTemplate RawMaterialDeliveryTypeComboBoxTemplate { get; set; }

        /// <summary>
        /// Gets or sets the project status combo box template.
        /// </summary>
        public DataTemplate ProjectStatusComboBoxTemplate { get; set; }

        /// <summary>
        /// Gets or sets the supplier type combo box template.
        /// </summary>
        public DataTemplate SupplierTypeComboBoxTemplate { get; set; }

        /// <summary>
        /// Gets or sets the calculation accuracy combo box template.
        /// </summary>
        public DataTemplate CalculationAccuracyComboBoxTemplate { get; set; }

        /// <summary>
        /// Gets or sets the calculation approach combo box template.
        /// </summary>
        public DataTemplate CalculationApproachComboBoxTemplate { get; set; }

        /// <summary>
        /// Gets or sets the calculation status combo box template.
        /// </summary>
        public DataTemplate CalculationStatusComboBoxTemplate { get; set; }

        /// <summary>
        /// Gets or sets the assembly/part delivery type combo box template.
        /// </summary>
        public DataTemplate AssemblyPartDeliveryTypeComboBoxTemplate { get; set; }

        /// <summary>
        /// Gets or sets the process accuracy combo box template.
        /// </summary>
        public DataTemplate ProcessAccuracyComboBoxTemplate { get; set; }

        /// <summary>
        /// Gets or sets the die type combo box template.
        /// </summary>
        public DataTemplate DieTypeComboBoxTemplate { get; set; }

        /// <summary>
        /// Gets or sets the scrap calculation type template.
        /// </summary>
        public DataTemplate ScrapCalculationTypeTemplate { get; set; }

        /// <summary>
        /// Gets or sets the consumable amount combo box template.
        /// </summary>
        public DataTemplate ConsumableAmountComboBoxTemplate { get; set; }

        /// <summary>
        /// Gets or sets the country template.
        /// </summary>
        public DataTemplate CountryTemplate { get; set; }

        /// <summary>
        /// Gets or sets the supplier template.
        /// </summary>
        public DataTemplate SupplierTemplate { get; set; }

        /// <summary>
        /// Gets or sets the classification selector template.
        /// </summary>
        public DataTemplate ClassificationSelectorTemplate { get; set; }

        /// <summary>
        /// When overridden in a derived class, returns a <see cref="T:System.Windows.DataTemplate" /> based on custom logic.
        /// </summary>
        /// <param name="item">The data object for which to select the template.</param>
        /// <param name="container">The data-bound object.</param>
        /// <returns>
        /// Returns a <see cref="T:System.Windows.DataTemplate" /> or null. The default value is null.
        /// </returns>
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var it = (ValueControlTypes)item;
            DataTemplate selectedDataTemplate = null;
            switch (it)
            {
                case ValueControlTypes.TextExtendedTextBox:
                    selectedDataTemplate = this.TextExtendedTextBoxTemplate;
                    break;
                case ValueControlTypes.NumberExtendedTextBox:
                    selectedDataTemplate = this.NumberExtendedTextBoxTemplate;
                    break;
                case ValueControlTypes.RangeExtendedTextBox:
                    selectedDataTemplate = this.RangeExtendedTextBoxTemplate;
                    break;
                case ValueControlTypes.CheckBox:
                    selectedDataTemplate = this.CheckBoxTemplate;
                    break;
                case ValueControlTypes.YearPicker:
                    selectedDataTemplate = this.YearPickerTemplate;
                    break;
                case ValueControlTypes.DatePicker:
                    selectedDataTemplate = this.DatePickerTemplate;
                    break;
                case ValueControlTypes.UsersComboBox:
                    selectedDataTemplate = this.UsersComboBoxTemplate;
                    break;
                case ValueControlTypes.CalculationVariantComboBox:
                    selectedDataTemplate = this.CalculationVariantComboBoxTemplate;
                    break;
                case ValueControlTypes.RawMaterialDeliveryTypeComboBox:
                    selectedDataTemplate = this.RawMaterialDeliveryTypeComboBoxTemplate;
                    break;
                case ValueControlTypes.ProjectStatusComboBox:
                    selectedDataTemplate = this.ProjectStatusComboBoxTemplate;
                    break;
                case ValueControlTypes.SupplierTypeComboBox:
                    selectedDataTemplate = this.SupplierTypeComboBoxTemplate;
                    break;
                case ValueControlTypes.CalculationAccuracyComboBox:
                    selectedDataTemplate = this.CalculationAccuracyComboBoxTemplate;
                    break;
                case ValueControlTypes.CalculationApproachComboBox:
                    selectedDataTemplate = this.CalculationApproachComboBoxTemplate;
                    break;
                case ValueControlTypes.CalculationStatusComboBox:
                    selectedDataTemplate = this.CalculationStatusComboBoxTemplate;
                    break;
                case ValueControlTypes.AssemblyPartDeliveryTypeComboBox:
                    selectedDataTemplate = this.AssemblyPartDeliveryTypeComboBoxTemplate;
                    break;
                case ValueControlTypes.ProcessAccuracyComboBox:
                    selectedDataTemplate = this.ProcessAccuracyComboBoxTemplate;
                    break;
                case ValueControlTypes.DieType:
                    selectedDataTemplate = this.DieTypeComboBoxTemplate;
                    break;
                case ValueControlTypes.ScrapCalculationType:
                    selectedDataTemplate = this.ScrapCalculationTypeTemplate;
                    break;
                case ValueControlTypes.ConsumableAmountComboBox:
                    selectedDataTemplate = this.ConsumableAmountComboBoxTemplate;
                    break;
                case ValueControlTypes.Country:
                    selectedDataTemplate = this.CountryTemplate;
                    break;
                case ValueControlTypes.Supplier:
                    selectedDataTemplate = this.SupplierTemplate;
                    break;
                case ValueControlTypes.ClassificationSelector:
                    selectedDataTemplate = this.ClassificationSelectorTemplate;
                    break;
                default:
                    selectedDataTemplate = EmptyTemplate;
                    break;
            }

            return selectedDataTemplate;
        }
    }
}