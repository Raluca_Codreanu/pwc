﻿using System.Windows;
using System.Windows.Controls.Primitives;
using ZPKTool.Business.Search;
using ZPKTool.Data;
using ZPKTool.Gui.Resources;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for SearchToolSearchScope.xaml
    /// </summary>
    public partial class SearchToolScope : Popup
    {
        /// <summary>
        /// The Selection property registered as a DP in order to support 
        /// </summary>
        public static readonly DependencyProperty SelectionProperty = DependencyProperty.Register("Selection", typeof(SearchScopeFilter), typeof(SearchToolScope), new UIPropertyMetadata(SelectionChangedCallback));

        /// <summary>
        /// Initializes a new instance of the <see cref="SearchToolScope" /> class.
        /// </summary>
        public SearchToolScope()
        {
            this.InitializeComponent();
            this.SetCheckBoxes();
        }

        /// <summary>
        /// Gets or sets the Selection property.
        /// </summary>
        public SearchScopeFilter Selection
        {
            get { return (SearchScopeFilter)GetValue(SelectionProperty); }
            set { SetValue(SelectionProperty, value); }
        }

        /// <summary>
        /// Selections the changed callback.
        /// </summary>
        /// <param name="depObj">The dep obj.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs" /> instance containing the event data.</param>
        private static void SelectionChangedCallback(DependencyObject depObj, DependencyPropertyChangedEventArgs e)
        {
            var searchScope = depObj as SearchToolScope;
            if (searchScope != null)
            {
                searchScope.SetCheckBoxes();
            }
        }

        /// <summary>
        /// Sets the check boxes.
        /// </summary>
        private void SetCheckBoxes()
        {
            this.DetachHandlers();

            if ((this.Selection & SearchScopeFilter.MasterData) != 0)
            {
                this.MasterDataCheckBox.IsChecked = true;
            }
            else
            {
                this.MasterDataCheckBox.IsChecked = false;
            }

            if ((this.Selection & SearchScopeFilter.MyProjects) != 0)
            {
                this.MyProjectsCheckBox.IsChecked = true;
            }
            else
            {
                this.MyProjectsCheckBox.IsChecked = false;
            }

            if ((this.Selection & SearchScopeFilter.OtherUsersProjects) != 0)
            {
                this.OtherUserProjectsCheckBox.IsChecked = true;
            }
            else
            {
                this.OtherUserProjectsCheckBox.IsChecked = false;
            }

            if ((this.Selection & SearchScopeFilter.ReleasedProjects) != 0)
            {
                this.ReleasedProjectsCheckBox.IsChecked = true;
            }
            else
            {
                this.ReleasedProjectsCheckBox.IsChecked = false;
            }

            this.AttachHandlers();

            if (this.Selection == (SearchScopeFilter.MasterData | SearchScopeFilter.MyProjects | SearchScopeFilter.OtherUsersProjects | SearchScopeFilter.ReleasedProjects))
            {
                this.SelectAllCheckbox.IsChecked = true;
                this.SelectAllCheckbox.Content = LocalizedResources.General_UncheckAll;
            }
        }

        /// <summary>
        /// Handles the unloaded event of the search scope control
        /// </summary>
        private void Refresh()
        {
            SearchScopeFilter scope = SearchScopeFilter.None;

            if (this.MasterDataCheckBox.IsChecked == true)
            {
                scope |= SearchScopeFilter.MasterData;
            }

            if (this.ReleasedProjectsCheckBox.IsChecked == true)
            {
                scope |= SearchScopeFilter.ReleasedProjects;
            }

            if (this.MyProjectsCheckBox.IsChecked == true)
            {
                scope |= SearchScopeFilter.MyProjects;
            }

            if (this.OtherUserProjectsCheckBox.IsChecked == true)
            {
                scope |= SearchScopeFilter.OtherUsersProjects;
            }

            this.Selection = scope;
        }

        /// <summary>
        /// Handles the Checked event of the SelectAllCheckbox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs" /> instance containing the event data.</param>
        private void SelectAllCheckbox_Checked(object sender, RoutedEventArgs e)
        {
            this.DetachHandlers();
            MasterDataCheckBox.IsChecked = true;
            ReleasedProjectsCheckBox.IsChecked = true;
            MyProjectsCheckBox.IsChecked = true;
            OtherUserProjectsCheckBox.IsChecked = true;
            SelectAllCheckbox.Content = LocalizedResources.General_UncheckAll;
            this.Refresh();
            this.AttachHandlers();
        }

        /// <summary>
        /// Handles the Unchecked event of the SelectAllCheckbox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs" /> instance containing the event data.</param>
        private void SelectAllCheckbox_Unchecked(object sender, RoutedEventArgs e)
        {
            this.DetachHandlers();
            MasterDataCheckBox.IsChecked = false;
            ReleasedProjectsCheckBox.IsChecked = false;
            MyProjectsCheckBox.IsChecked = false;
            OtherUserProjectsCheckBox.IsChecked = false;
            SelectAllCheckbox.Content = LocalizedResources.General_CheckAll;
            this.Refresh();
            this.AttachHandlers();
        }

        /// <summary>
        /// Handles the Check event on the checkboxes
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs" /> instance containing the event data.</param>
        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            this.Refresh();
        }

        /// <summary>
        /// Handles the UnCheck event on the checkboxes
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs" /> instance containing the event data.</param>
        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            this.Refresh();
        }

        /// <summary>
        /// Handles the Click event of the CloseSearchScopeButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void SearchScopeButton_Click(object sender, RoutedEventArgs e)
        {
            this.IsOpen = false;
        }

        /// <summary>
        /// Detaches the checked and unchecked handlers
        /// </summary>
        private void DetachHandlers()
        {
            this.MasterDataCheckBox.Checked -= new RoutedEventHandler(CheckBox_Checked);
            this.MasterDataCheckBox.Unchecked -= new RoutedEventHandler(CheckBox_Unchecked);
            this.MyProjectsCheckBox.Checked -= new RoutedEventHandler(CheckBox_Checked);
            this.MyProjectsCheckBox.Unchecked -= new RoutedEventHandler(CheckBox_Unchecked);
            this.ReleasedProjectsCheckBox.Checked -= new RoutedEventHandler(CheckBox_Checked);
            this.ReleasedProjectsCheckBox.Unchecked -= new RoutedEventHandler(CheckBox_Unchecked);
            this.OtherUserProjectsCheckBox.Checked -= new RoutedEventHandler(CheckBox_Checked);
            this.OtherUserProjectsCheckBox.Unchecked -= new RoutedEventHandler(CheckBox_Unchecked);
        }

        /// <summary>
        /// Attaches the checked and unchecked handlers
        /// </summary>
        private void AttachHandlers()
        {
            this.MasterDataCheckBox.Checked += new RoutedEventHandler(CheckBox_Checked);
            this.MasterDataCheckBox.Unchecked += new RoutedEventHandler(CheckBox_Unchecked);
            this.MyProjectsCheckBox.Checked += new RoutedEventHandler(CheckBox_Checked);
            this.MyProjectsCheckBox.Unchecked += new RoutedEventHandler(CheckBox_Unchecked);
            this.ReleasedProjectsCheckBox.Checked += new RoutedEventHandler(CheckBox_Checked);
            this.ReleasedProjectsCheckBox.Unchecked += new RoutedEventHandler(CheckBox_Unchecked);
            this.OtherUserProjectsCheckBox.Checked += new RoutedEventHandler(CheckBox_Checked);
            this.OtherUserProjectsCheckBox.Unchecked += new RoutedEventHandler(CheckBox_Unchecked);
        }
    }
}