﻿<UserControl x:Class="ZPKTool.Gui.Views.AdvancedSearchView"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
             xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
             mc:Ignorable="d"
             xmlns:controls="clr-namespace:ZPKTool.Controls;assembly=ZPKTool.Controls"
             xmlns:resources="clr-namespace:ZPKTool.Gui.Resources"
             xmlns:converters="clr-namespace:ZPKTool.Gui.Converters"
             xmlns:views="clr-namespace:ZPKTool.Gui.Views"
             xmlns:viewModels="clr-namespace:ZPKTool.Gui.ViewModels"
             xmlns:mvvmcore="http://zpktool.com/mvvmcore"
             xmlns:guiControls="clr-namespace:ZPKTool.Gui.Controls"
             xmlns:behaviors="clr-namespace:ZPKTool.Gui.Behaviors"
             xmlns:i="clr-namespace:System.Windows.Interactivity;assembly=System.Windows.Interactivity"
             Name="AdvancedSearch">

    <UserControl.Resources>
        <views:AdvancedSearchTabItemStyleSelector x:Key="AdvancedSearchTabItemStyleSelector" />

        <converters:InverseBooleanConverter x:Key="InverseBoolConverter" />

        <mvvmcore:CommandReference x:Key="CloseSearchTabCommand"
                                   Command="{Binding CloseSearchTabCommand}" />

        <!--The item and control styles-->
        <Style x:Key="ConfigurationItemStyle"
               TargetType="TabItem">
            <Setter Property="Template">
                <Setter.Value>
                    <ControlTemplate TargetType="{x:Type TabItem}">
                        <Grid>
                            <Border Name="Border"
                                    Margin="1,0,0,0"
                                    Background="#999999"
                                    BorderBrush="#999999"
                                    BorderThickness="1,1,1,1"
                                    CornerRadius="4,4,0,0">
                                <ContentPresenter VerticalAlignment="Center"
                                                  HorizontalAlignment="Center"
                                                  ContentSource="Header"
                                                  Margin="12,2,12,2"
                                                  RecognizesAccessKey="True" />
                            </Border>
                        </Grid>
                        <ControlTemplate.Triggers>
                            <Trigger Property="IsSelected"
                                     Value="True">
                                <Setter TargetName="Border"
                                        Property="Background"
                                        Value="#666666" />
                            </Trigger>
                        </ControlTemplate.Triggers>
                    </ControlTemplate>
                </Setter.Value>
            </Setter>
        </Style>

        <Style x:Key="ResultItemStyle"
               TargetType="TabItem">
            <Setter Property="Template">
                <Setter.Value>
                    <ControlTemplate TargetType="{x:Type TabItem}">
                        <Grid>
                            <Border Name="Border"
                                    Margin="1,0,0,0"
                                    Background="#79a6d2"
                                    BorderBrush="#79a6d2"
                                    BorderThickness="1,1,1,1"
                                    CornerRadius="8,8,0,0">
                                <ContentPresenter VerticalAlignment="Center"
                                                  HorizontalAlignment="Center"
                                                  ContentSource="Header"
                                                  Margin="12,2,12,2"
                                                  RecognizesAccessKey="True" />
                            </Border>
                        </Grid>
                        <ControlTemplate.Triggers>
                            <Trigger Property="IsSelected"
                                     Value="True">
                                <Setter TargetName="Border"
                                        Property="Background"
                                        Value="#336699" />
                            </Trigger>
                        </ControlTemplate.Triggers>
                    </ControlTemplate>
                </Setter.Value>
            </Setter>
        </Style>

        <Style x:Key="TabControlStyle"
               TargetType="TabControl"
               BasedOn="{StaticResource {x:Type TabControl}}">

            <Setter Property="Control.Template">
                <Setter.Value>
                    <ControlTemplate TargetType="TabControl">
                        <Grid ClipToBounds="True"
                              SnapsToDevicePixels="True"
                              KeyboardNavigation.TabNavigation="Local">
                            <Grid.ColumnDefinitions>
                                <ColumnDefinition />
                            </Grid.ColumnDefinitions>
                            <Grid.RowDefinitions>
                                <RowDefinition Height="Auto" />
                                <RowDefinition Height="*" />
                            </Grid.RowDefinitions>

                            <Grid Margin="2,2,2,0"
                                  Panel.ZIndex="1">
                                <Grid.ColumnDefinitions>
                                    <ColumnDefinition Width="Auto" />
                                    <ColumnDefinition Width="*" />
                                </Grid.ColumnDefinitions>

                                <controls:Button Content="+"
                                                 ToolTip="{x:Static resources:LocalizedResources.General_AddTab}"
                                                 Command="{Binding AddSearchTabCommand}"
                                                 VerticalAlignment="Center"
                                                 HorizontalAlignment="Left" />
                                <TabPanel IsItemsHost="True"
                                          Name="HeaderPanel"
                                          KeyboardNavigation.TabIndex="1"
                                          Margin="5,0,0,0"
                                          Grid.Column="1" />
                            </Grid>

                            <Border BorderThickness="{TemplateBinding Border.BorderThickness}"
                                    BorderBrush="{TemplateBinding Border.BorderBrush}"
                                    Background="{TemplateBinding Panel.Background}"
                                    Name="ContentPanel"
                                    KeyboardNavigation.TabIndex="2"
                                    KeyboardNavigation.TabNavigation="Local"
                                    KeyboardNavigation.DirectionalNavigation="Contained"
                                    Grid.Row="1">
                                <ContentPresenter Content="{TemplateBinding TabControl.SelectedContent}"
                                                  ContentTemplate="{TemplateBinding TabControl.SelectedContentTemplate}"
                                                  ContentStringFormat="{TemplateBinding TabControl.SelectedContentStringFormat}"
                                                  ContentTemplateSelector="{TemplateBinding TabControl.ContentTemplateSelector}"
                                                  ContentSource="SelectedContent"
                                                  Name="PART_SelectedContentHost"
                                                  Margin="{TemplateBinding Control.Padding}"
                                                  SnapsToDevicePixels="{TemplateBinding UIElement.SnapsToDevicePixels}" />
                            </Border>
                        </Grid>
                    </ControlTemplate>
                </Setter.Value>
            </Setter>
        </Style>

        <!--The tab control and tab item data templates-->
        <DataTemplate x:Key="TabConfigurationsTemplate">
            <Grid>
                <Grid.ColumnDefinitions>
                    <ColumnDefinition Width="Auto" />
                    <ColumnDefinition Width="Auto" />
                </Grid.ColumnDefinitions>

                <TextBox Name="TitleTextBoxControl"
                         Text="{Binding SearchConfiguration.Title}"
                         FontWeight="Bold"
                         ToolTip="{x:Static resources:LocalizedResources.General_DoubleClickToEditName}"
                         TextAlignment="Center"
                         IsReadOnly="True"
                         MaxLength="100"
                         Background="Transparent"
                         BorderThickness="0"
                         Cursor="Arrow"
                         PreviewMouseDown="TitleTextBoxControl_PreviewMouseDown"
                         MouseDoubleClick="TitleTextBoxControl_MouseDoubleClick"
                         LostFocus="TitleTextBoxControl_LostFocus" />

                <controls:AnimatedImageButton Width="11"
                                              Height="11"
                                              Grid.Column="1"
                                              ToolTip="{x:Static resources:LocalizedResources.General_CloseTab}"
                                              ImageSource="{DynamicResource CloseIcon}"
                                              Margin="8,0,0,0"
                                              Command="{StaticResource CloseSearchTabCommand}"
                                              CommandParameter="{Binding}" />
            </Grid>
        </DataTemplate>

        <DataTemplate x:Key="TabResultsTemplate">
            <Grid>
                <Grid.ColumnDefinitions>
                    <ColumnDefinition Width="Auto" />
                    <ColumnDefinition Width="Auto" />
                </Grid.ColumnDefinitions>

                <TextBox Name="TitleTextBoxControl"
                         Text="{Binding Title}"
                         FontWeight="Bold"
                         ToolTip="{x:Static resources:LocalizedResources.General_DoubleClickToEditName}"
                         TextAlignment="Center"
                         IsReadOnly="True"
                         MaxLength="100"
                         Background="Transparent"
                         BorderThickness="0"
                         Cursor="Arrow"
                         PreviewMouseDown="TitleTextBoxControl_PreviewMouseDown"
                         MouseDoubleClick="TitleTextBoxControl_MouseDoubleClick"
                         LostFocus="TitleTextBoxControl_LostFocus" />

                <controls:AnimatedImageButton Width="11"
                                              Height="11"
                                              Grid.Column="1"
                                              ToolTip="{x:Static resources:LocalizedResources.General_CloseTab}"
                                              ImageSource="{DynamicResource CloseIcon}"
                                              Margin="8,0,0,0"
                                              Command="{StaticResource CloseSearchTabCommand}"
                                              CommandParameter="{Binding}" />
            </Grid>
        </DataTemplate>

        <!--The view-model data templates-->
        <DataTemplate DataType="{x:Type viewModels:AdvancedSearchConfigurationViewModel}">
            <views:AdvancedSearchConfigurationView />
        </DataTemplate>

        <DataTemplate DataType="{x:Type viewModels:AdvancedSearchResultViewModel}">
            <views:AdvancedSearchResultView />
        </DataTemplate>
    </UserControl.Resources>

    <Grid x:Name="LayoutRoot"
          Margin="10,10,10,10">
        <Grid.RowDefinitions>
            <RowDefinition Height="*" />
            <RowDefinition Height="Auto" />
        </Grid.RowDefinitions>

        <TabControl Name="TabControl"
                    Margin="0,5,0,0"
                    ItemsSource="{Binding AdvancedSearchVMs}"
                    Style="{StaticResource TabControlStyle}"
                    SelectedItem="{Binding SelectedItem}"
                    Initialized="TabControl_Initialized"
                    ItemContainerStyleSelector="{StaticResource AdvancedSearchTabItemStyleSelector}"
                    behaviors:TabContent.IsCached="True">
            <TabControl.ItemTemplateSelector>
                <views:AdvancedSearchItemTemplateSelector TabConfigurationsTemplate="{StaticResource TabConfigurationsTemplate}"
                                                          TabResultsTemplate="{StaticResource TabResultsTemplate}" />
            </TabControl.ItemTemplateSelector>
        </TabControl>

        <controls:Button x:Name="SearchButton"
                         VerticalAlignment="Bottom"
                         HorizontalAlignment="Right"
                         Content="{x:Static resources:LocalizedResources.General_Search}"
                         MinWidth="62"
                         Command="{Binding SearchCommand}"
                         CommandParameter="{Binding Path=SelectedItem, ElementName=TabControl}"
                         IsEnabled="{Binding IsSearchInProgress,Converter={StaticResource InverseBoolConverter}}"
                         Margin="5,5,0,0"
                         Grid.Row="1" />
    </Grid>

    <i:Interaction.Triggers>
        <i:EventTrigger EventName="KeyDown">
            <mvvmcore:EventToCommand Command="{Binding Path=TextBoxKeyDownCommand}"
                                     PassEventArgsToCommand="True" />
        </i:EventTrigger>
    </i:Interaction.Triggers>
</UserControl>