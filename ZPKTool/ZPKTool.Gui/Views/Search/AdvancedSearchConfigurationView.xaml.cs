﻿using System.Collections;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using ZPKTool.Gui.Utils;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for AdvancedSearchConfigurationView.xaml
    /// </summary>
    public partial class AdvancedSearchConfigurationView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AdvancedSearchConfigurationView" /> class.
        /// </summary>
        public AdvancedSearchConfigurationView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handles the SelectedDateChanged event from the first date picker.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Controls.SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void DatePicker1_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedDatePicker = sender as DatePicker;
            if (selectedDatePicker != null)
            {
                var secondDatePicker = this.GetSecondDatePicker(selectedDatePicker);
                if (secondDatePicker != null && secondDatePicker.Visibility == Visibility.Visible && selectedDatePicker.SelectedDate > secondDatePicker.SelectedDate)
                {
                    secondDatePicker.SelectedDate = selectedDatePicker.SelectedDate;
                }
            }
        }

        /// <summary>
        /// Handles the SelectedDateChanged event from the second date picker.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Controls.SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void DatePicker2_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedDatePicker = sender as DatePicker;
            if (selectedDatePicker != null)
            {
                var secondDatePicker = this.GetSecondDatePicker(selectedDatePicker);
                if (secondDatePicker != null && secondDatePicker.Visibility == Visibility.Visible && selectedDatePicker.SelectedDate < secondDatePicker.SelectedDate)
                {
                    secondDatePicker.SelectedDate = selectedDatePicker.SelectedDate;
                }
            }
        }

        /// <summary>
        /// Gets the second date picker used for defining the range in search 
        /// </summary>
        /// <param name="datePicker">The date picker that changed</param>
        /// <returns>The second date picker used for defining the range in search or null</returns>
        private DatePicker GetSecondDatePicker(DatePicker datePicker)
        {
            if (datePicker != null)
            {
                var parent = datePicker.Parent as StackPanel;
                if (parent != null)
                {
                    var allDatePickers = UIHelper.FindChildren<DatePicker>(parent);
                    var secondDatePicker = allDatePickers.FirstOrDefault(d => d != datePicker);
                    if (secondDatePicker != null)
                    {
                        return secondDatePicker;
                    }
                }
            }

            return null;
        }
    }
}