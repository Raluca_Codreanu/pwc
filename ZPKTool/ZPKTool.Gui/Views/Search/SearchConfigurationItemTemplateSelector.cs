﻿using System.Windows;
using System.Windows.Controls;
using PROimpens.Gui.ViewModels;

namespace PROimpens.Gui.Views
{
    public class SearchConfigurationItemTemplateSelector : DataTemplateSelector
    {
        /// <summary>
        /// Gets or sets the empty template.
        /// </summary>
        /// <value>
        /// The empty template.
        /// </value>
        public DataTemplate EmptyTemplate { get; set; }

        /// <summary>
        /// Gets or sets the text box template.
        /// </summary>
        /// <value>
        /// The text box template.
        /// </value>
        public DataTemplate TextBoxTemplate { get; set; }

        /// <summary>
        /// Gets or sets the extended text box template.
        /// </summary>
        /// <value>
        /// The extended text box template.
        /// </value>
        public DataTemplate ExtendedTextBoxTemplate { get; set; }

        /// <summary>
        /// Gets or sets the combo box template.
        /// </summary>
        /// <value>
        /// The combo box template.
        /// </value>
        public DataTemplate ComboBoxTemplate { get; set; }

        /// <summary>
        /// Gets or sets the check box template.
        /// </summary>
        /// <value>
        /// The check box template.
        /// </value>
        public DataTemplate CheckBoxTemplate { get; set; }

        /// <summary>
        /// Gets or sets the date picker template.
        /// </summary>
        /// <value>
        /// The date picker template.
        /// </value>
        public DataTemplate DatePickerTemplate { get; set; }

        /// <summary>
        /// When overridden in a derived class, returns a <see cref="T:System.Windows.DataTemplate" /> based on custom logic.
        /// </summary>
        /// <param name="item">The data object for which to select the template.</param>
        /// <param name="container">The data-bound object.</param>
        /// <returns>
        /// Returns a <see cref="T:System.Windows.DataTemplate" /> or null. The default value is null.
        /// </returns>
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var it = (ValueControlTypes)item;
            DataTemplate selectedDataTemplate = null;
            switch (it)
            {
                case ValueControlTypes.TextBox:
                    selectedDataTemplate = TextBoxTemplate;
                    break;
                case ValueControlTypes.ExtendedTextBox:
                    selectedDataTemplate = ExtendedTextBoxTemplate;
                    break;
                case ValueControlTypes.ComboBox:
                    selectedDataTemplate = ComboBoxTemplate;
                    break;
                case ValueControlTypes.CheckBox:
                    selectedDataTemplate = CheckBoxTemplate;
                    break;
                case ValueControlTypes.DatePicker:
                    selectedDataTemplate = DatePickerTemplate;
                    break;
                default:
                    selectedDataTemplate = EmptyTemplate;
                    break;
            }

            return selectedDataTemplate;
        }
    }
}