﻿using System;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using ZPKTool.Gui.Utils;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for AdvancedSearchView.xaml
    /// </summary>
    public partial class AdvancedSearchView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AdvancedSearchView" /> class.
        /// </summary>
        public AdvancedSearchView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handles the Initialized event of the TabControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void TabControl_Initialized(object sender, EventArgs e)
        {
            // Select the created tab item.
            var tabControl = sender as TabControl;
            if (tabControl != null)
            {
                var view = CollectionViewSource.GetDefaultView(tabControl.Items);
                view.CollectionChanged += (o, i) =>
                {
                    if (i.NewItems != null && i.NewItems.Count != 0)
                    {
                        var newItem = i.NewItems[0];
                        if (newItem != null)
                        {
                            tabControl.SelectedItem = newItem;
                        }
                    }
                };
            }
        }

        /// <summary>
        /// Handles the PreviewMouseDown event of the TitleTextBoxControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseButtonEventArgs" /> instance containing the event data.</param>
        private void TitleTextBoxControl_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var tb = sender as TextBox;
            if (tb != null)
            {
                var ti = UIHelper.FindParent<TabItem>(tb);
                if (ti != null)
                {
                    ti.IsSelected = true;
                }
            }
        }

        /// <summary>
        /// Handles the MouseDoubleClick event of the TitleTextBoxControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseButtonEventArgs" /> instance containing the event data.</param>
        private void TitleTextBoxControl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Right)
            {
                return;
            }

            var tb = sender as TextBox;
            if (tb != null)
            {
                tb.IsReadOnly = false;
                tb.Cursor = Cursors.IBeam;
                tb.Background = Brushes.White;
            }
        }

        /// <summary>
        /// Handles the LostFocus event of the TitleTextBoxControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs" /> instance containing the event data.</param>
        private void TitleTextBoxControl_LostFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            var tb = sender as TextBox;
            if (tb != null)
            {
                tb.IsReadOnly = true;
                tb.Cursor = Cursors.Arrow;
                tb.Background = Brushes.Transparent;
            }
        }
    }
}