﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using ZPKTool.Gui.Controls;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for SearchToolView.xaml
    /// </summary>
    public partial class SearchToolView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SearchToolView"/> class.
        /// </summary>
        public SearchToolView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Called when the expander header's border has been loaded.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void OnExpanderHeaderBorderLoaded(object sender, RoutedEventArgs e)
        {
            // Workaround to stretch the expander header content
            Border border = sender as Border;
            if (border != null)
            {
                ContentPresenter presenter = border.TemplatedParent as ContentPresenter;
                if (presenter != null)
                {
                    presenter.HorizontalAlignment = HorizontalAlignment.Stretch;
                }
            }
        }

        /// <summary>
        /// Handles the MouseRightButtonDown event of the items in the SearchResults Tree.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void SearchResultsTreeItem_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            TreeViewItem item = sender as TreeViewItem;
            if (item != null)
            {
                item.IsSelected = true;
                e.Handled = true;
            }
        }
    }
}