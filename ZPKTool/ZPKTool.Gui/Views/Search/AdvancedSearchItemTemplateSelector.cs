﻿using System.Windows;
using System.Windows.Controls;
using ZPKTool.Gui.ViewModels;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Selects the tab template to use for the advanced search.
    /// </summary>
    public class AdvancedSearchItemTemplateSelector : DataTemplateSelector
    {
        /// <summary>
        /// Gets or sets the tab configurations template.
        /// </summary>
        public DataTemplate TabConfigurationsTemplate { get; set; }

        /// <summary>
        /// Gets or sets the tab results template.
        /// </summary>
        public DataTemplate TabResultsTemplate { get; set; }

        /// <summary>
        /// When overridden in a derived class, returns a <see cref="T:System.Windows.DataTemplate"/> based on custom logic.
        /// </summary>
        /// <param name="item">The data object for which to select the template.</param>
        /// <param name="container">The data-bound object.</param>
        /// <returns>
        /// Returns a <see cref="T:System.Windows.DataTemplate"/> or null. The default value is null.
        /// </returns>
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item is AdvancedSearchConfigurationViewModel)
            {
                return TabConfigurationsTemplate;
            }
            else if (item is AdvancedSearchResultViewModel)
            {
                return TabResultsTemplate;
            }

            return null;
        }
    }
}
