﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Utils;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for AdvancedSearchResultView.xaml
    /// </summary>
    public partial class AdvancedSearchResultView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AdvancedSearchResultView"/> class.
        /// </summary>
        public AdvancedSearchResultView()
        {
            InitializeComponent();
        }
    }
}