﻿using System;
using System.Windows.Data;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Utils;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// This class converts an int value representing an enumeration member to string.
    /// </summary>
    [ValueConversion(typeof(int), typeof(string))]
    public class IntToEnumConverter : IValueConverter
    {
        #region IValueConverter Members

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var index = value as int?;
            var valueType = parameter as Type;
            if (value != null && valueType != null)
            {
                string[] names = Enum.GetNames(valueType);
                if (index >= 0 && index < names.Length)
                {
                    return names[index.Value];
                }
            }

            return null;
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is Enum)
            {
                var entype = value.GetType();
                var undertype = Enum.GetUnderlyingType(entype);
                return System.Convert.ChangeType(value, undertype);
            }

            return null;
        }

        #endregion
    }
}