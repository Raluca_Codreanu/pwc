﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using ZPKTool.Data;
using ZPKTool.Gui.Resources;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for BomImporterSelectDataAdapter.xaml
    /// </summary>
    public partial class BomImporterConfigureView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BomImporterConfigureView"/> class.
        /// </summary>
        public BomImporterConfigureView()
        {
            InitializeComponent();
        }
    }
}