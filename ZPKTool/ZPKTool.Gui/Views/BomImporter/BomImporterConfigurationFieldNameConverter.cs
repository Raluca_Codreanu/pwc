﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.ViewModels;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Configuration field name converter.
    /// </summary>
    [ValueConversion(typeof(List<PairKeyValue<string, string>>), typeof(string))]
    public class BomImporterConfigurationFieldNameConverter : IValueConverter
    {
        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value", "The value to convert was null");
            }

            string displayCategory = string.Empty;

            List<PairKeyValue<string, string>> pairs = value as List<PairKeyValue<string, string>>;

            if (pairs == null)
            {
                throw new ArgumentNullException("value", "The value is not a list of categories");
            }

            foreach (var pair in pairs)
            {
                string resourceKey = pair.First + "_" + pair.Second;
                string result = LocalizedResources.ResourceManager.GetString(resourceKey);

                if (string.IsNullOrWhiteSpace(displayCategory) && !string.IsNullOrWhiteSpace(result))
                {
                    displayCategory = result;
                }
            }

            return displayCategory;
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
