﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.ViewModels;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for BomImporterConfigurationView.xaml
    /// </summary>
    public partial class BomImporterConfigurationView
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BomImporterConfigurationView"/> class.
        /// </summary>
        public BomImporterConfigurationView()
        {
            InitializeComponent();
        }
    }
}
