﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Selects the template to use for the column name.
    /// </summary>
    public class BomImporterColumnNameTemplateSelector : DataTemplateSelector
    {
        /// <summary>
        /// Gets or sets the text box template.
        /// </summary>
        /// <value>
        /// The text box template.
        /// </value>
        public DataTemplate TextBoxTemplate { get; set; }

        /// <summary>
        /// Gets or sets the combo box template.
        /// </summary>
        /// <value>
        /// The combo box template.
        /// </value>
        public DataTemplate ComboBoxTemplate { get; set; }

        /// <summary>
        /// When overridden in a derived class, returns a <see cref="T:System.Windows.DataTemplate"/> based on custom logic.
        /// </summary>
        /// <param name="item">The data object for which to select the template.</param>
        /// <param name="container">The data-bound object.</param>
        /// <returns>
        /// Returns a <see cref="T:System.Windows.DataTemplate"/> or null. The default value is null.
        /// </returns>
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item != null)
            {
                var pi = item.GetType().GetProperty("ColumnNames");
                object value = pi.GetValue(item, null);

                object value2 = null;

                if (value != null)
                {
                    var pi2 = value.GetType().GetProperty("Count");
                    value2 = pi2.GetValue(value, null);
                }

                if ((value == null) ||
                    (value2 != null && (int)value2 == 0))
                {
                    return TextBoxTemplate;
                }
                else
                {
                    return ComboBoxTemplate;
                }
            }

            return TextBoxTemplate;
        }
    }
}
