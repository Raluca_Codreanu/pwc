﻿using ZPKTool.Common;
using ZPKTool.Gui.ViewModels;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// WeakEventManager implementation for the RefreshPreviewMode event of the MediaHandlingViewModelBase.
    /// </summary>
    public class MediaHandlerRefreshEventManager : WeakEventManagerBase<MediaHandlerRefreshEventManager, MediaHandlingViewModelBase>
    {
        /// <summary>
        /// Attaches the event handler.
        /// </summary>
        /// <param name="source">The source to which to attach.</param>
        protected override void StartListening(MediaHandlingViewModelBase source)
        {
            source.RefreshPreviewMode += DeliverEvent;
        }

        /// <summary>
        /// Detaches the event handler.
        /// </summary>
        /// <param name="source">The source from which to detach.</param>
        protected override void StopListening(MediaHandlingViewModelBase source)
        {
            source.RefreshPreviewMode -= DeliverEvent;
        }
    }
}