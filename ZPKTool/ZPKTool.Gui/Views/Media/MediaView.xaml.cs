﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using ZPKTool.Controls;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore.Commands;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for MediaView.xaml
    /// </summary>    
    public partial class MediaView
    {
        /// <summary>
        /// Using a DependencyProperty as the backing store for IsChangingScreenMode. This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty IsChangingScreenModeProperty = DependencyProperty.Register(
            "IsChangingScreenMode",
            typeof(bool),
            typeof(MediaView),
            new FrameworkPropertyMetadata(false));

        /// <summary>
        /// Using a DependencyProperty as the backing store for IsInFullScreenMode. This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty IsInFullScreenModeProperty = DependencyProperty.Register(
            "IsInFullScreenMode",
            typeof(bool),
            typeof(MediaView),
            new FrameworkPropertyMetadata(false));

        /// <summary>
        /// Using a DependencyProperty as the backing store for IsEmpty. This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty IsEmptyProperty = DependencyProperty.Register(
            "IsEmpty",
            typeof(bool),
            typeof(MediaView),
            new FrameworkPropertyMetadata(false));

        /// <summary>
        ///  Using a DependencyProperty as the backing store for ViewLoadedCommand.  This enables animation, styling, binding, etc...
        /// </summary>
        private static readonly DependencyProperty ViewLoadedCommandProperty =
            DependencyProperty.Register("ViewLoadedCommand", typeof(ICommand), typeof(MediaView), new PropertyMetadata(null, OnViewLoadedCommandChanged));

        /// <summary>
        /// The parent control of this.
        /// </summary>
        private DependencyObject parentControl;

        /// <summary>
        /// Represents the control which should be moved into the full screen grid and back to the location where this control is displayed in normal size.
        /// </summary>
        private UIElement controlToMove;

        /// <summary>
        /// Reference to the border which holds the dark transparent background of the full screen mode.
        /// </summary>
        private Border fullScreenContent;

        /// <summary>
        /// Reference to the grid which is added in the window in the full screen mode.
        /// </summary>
        private Grid fullScreenGrid;

        /// <summary>
        /// Height of the media control before the full screen mode
        /// </summary>
        private double mediaControlBackupHeight;

        /// <summary>
        /// Width of the media control before the full screen mode
        /// </summary>
        private double mediaControlBackupWidth;

        /// <summary>
        /// Initializes a new instance of the <see cref="MediaView"/> class.
        /// </summary>
        public MediaView()
        {
            this.InitializeBindings();
            this.InitializeComponent();
            this.InitializeCommands();
        }

        /// <summary>
        /// Gets the toggle full screen command.
        /// </summary>
        public ICommand ToggleFullScreenCommand { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether the screen mode is currently changing or not.
        /// </summary>
        public bool IsChangingScreenMode
        {
            get { return (bool)GetValue(IsChangingScreenModeProperty); }
            set { SetValue(IsChangingScreenModeProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether control is in full screen mode or not.
        /// </summary>
        public bool IsInFullScreenMode
        {
            get { return (bool)GetValue(IsInFullScreenModeProperty); }
            set { SetValue(IsInFullScreenModeProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the Media box is empty or not.
        /// </summary>
        public bool IsEmpty
        {
            get { return (bool)GetValue(IsEmptyProperty); }
            set { SetValue(IsEmptyProperty, value); }
        }

        /// <summary>
        ///  Gets or sets a command that informs the data context that the view is loaded.
        /// </summary>
        private ICommand ViewLoadedCommand
        {
            get { return (ICommand)GetValue(ViewLoadedCommandProperty); }
            set { SetValue(ViewLoadedCommandProperty, value); }
        }

        /// <summary>
        /// Invoked when the OnViewLoadedCommand property value has changed.
        /// </summary>
        /// <param name="d">The dependency object.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnViewLoadedCommandChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var mediaView = d as MediaView;
            if (mediaView != null)
            {
                if (mediaView.ViewLoadedCommand != null && mediaView.ViewLoadedCommand.CanExecute(null))
                {
                    mediaView.ViewLoadedCommand.Execute(true);
                }
            }
        }

        /// <summary>
        /// Initializes the bindings.
        /// </summary>
        private void InitializeBindings()
        {
            Binding isChangingScreenModeBinding = new Binding("MediaHandler.IsTogglingFullScreen");
            isChangingScreenModeBinding.Mode = BindingMode.TwoWay;
            SetBinding(IsChangingScreenModeProperty, isChangingScreenModeBinding);

            Binding viewLoadedCmdBinding = new Binding("ViewLoadedCommand");
            viewLoadedCmdBinding.Mode = BindingMode.OneWay;
            SetBinding(ViewLoadedCommandProperty, viewLoadedCmdBinding);

            Binding isMediaEmpty = new Binding("MediaHandler.IsEmpty");
            isMediaEmpty.Mode = BindingMode.OneWay;
            isMediaEmpty.FallbackValue = true;
            SetBinding(IsEmptyProperty, isMediaEmpty);
        }

        /// <summary>
        /// Initializes the commands.
        /// </summary>
        private void InitializeCommands()
        {
            this.ToggleFullScreenCommand = new DelegateCommand(this.ToggleFullScreen, this.CanToggleFullScreen);
        }

        /// <summary>
        /// Determines whether this instance can execute the ToggleFullScreen command.
        /// </summary>
        /// <returns>
        /// true if the command can be executed; otherwise returns false.
        /// </returns>
        private bool CanToggleFullScreen()
        {
            return this.IsInFullScreenMode || !this.IsEmpty;
        }

        /// <summary>
        /// Toggles the full screen mode on and off.
        /// </summary>
        private void ToggleFullScreen()
        {
            if (this.IsInFullScreenMode)
            {
                this.SetToNormalScreen();
            }
            else
            {
                this.SetToFullScreen();
            }
        }

        /// <summary>
        /// Determines and stores the parent control of this instance.
        /// </summary>
        /// <returns><c>true</c>, if the parent of this control could be determined; otherwise <c>false</c>.</returns>
        private bool DetermineParent()
        {
            this.parentControl = null;
            this.controlToMove = this;

            // todo: consider removing this branch. It's not removed yet, because the MediaView might be used directly in xaml (in this case it has Parent) 
            // and not only via its view model (it has TemplatedParent)
            if (this.Parent is Panel || this.Parent is ContentControl || this.Parent is ContentPresenter)
            {
                this.parentControl = this.Parent;
            }
            else
            {
                FrameworkElement templatedParent = this.TemplatedParent as FrameworkElement;
                if (templatedParent != null && (templatedParent.Parent is Panel || templatedParent.Parent is ContentControl || templatedParent.Parent is ContentPresenter))
                {
                    this.parentControl = templatedParent.Parent;
                    this.controlToMove = templatedParent;
                }
            }

            return this.parentControl != null;
        }

        /// <summary>
        /// Sets to full screen.
        /// </summary>
        private void SetToFullScreen()
        {
            if (!this.DetermineParent())
            {
                return;
            }

            // Find the window in which the media control is placed
            var parentWindow = UIHelper.FindParent<Window>(this);

            // Initialize the background of the full screen mode
            this.fullScreenContent = new Border();

            // Set row and column span to a large value to assure that the grid is stretching on the whole content
            Grid.SetRowSpan(fullScreenContent, 100);
            Grid.SetColumnSpan(fullScreenContent, 100);
            this.fullScreenContent.HorizontalAlignment = HorizontalAlignment.Stretch;
            this.fullScreenContent.VerticalAlignment = VerticalAlignment.Stretch;
            this.fullScreenContent.SetResourceReference(BackgroundProperty, "DarkTransparentBackground");

            this.IsChangingScreenMode = true;
            this.RemoveFromParent();

            // Add the control to the full screen transparent border
            this.fullScreenContent.Child = controlToMove;
            this.fullScreenGrid = UIHelper.GetVisualChild<Grid>(parentWindow);

            // Store the width and height and set the full screen mode flag
            this.IsInFullScreenMode = true;
            this.ToggleFullScreenButton.SetResourceReference(AnimatedImageButton.ImageSourceProperty, "FullScreenOffIcon");
            this.mediaControlBackupHeight = this.Height;
            this.mediaControlBackupWidth = this.Width;
            this.Height = double.NaN;
            this.Width = double.NaN;

            // Add an event handle all the key events to prevent key and tab navigation under the full screen.
            foreach (UIElement child in this.fullScreenGrid.Children)
            {
                child.PreviewKeyDown += FullScreenContentChildPreviewKeyDown;
            }

            // Set the negative margin to cover the initial grid margin.
            Thickness parentMargin = this.fullScreenGrid.Margin;
            this.fullScreenContent.Margin = new Thickness(-parentMargin.Left, -parentMargin.Top, -parentMargin.Right, -parentMargin.Bottom);
            this.fullScreenGrid.Children.Add(fullScreenContent);

            // Todo: verify if the BeginInvoke is needed or not.
            Action action = delegate { this.IsChangingScreenMode = false; };
            this.Dispatcher.BeginInvoke(action, DispatcherPriority.Input);
        }

        /// <summary>
        /// Sets to normal screen.
        /// </summary>
        private void SetToNormalScreen()
        {
            this.IsChangingScreenMode = true;

            // Remove the media control from the full screen content
            this.fullScreenContent.Child = null;
            this.fullScreenGrid.Children.Remove(fullScreenContent);
            this.AddControlToParent();

            // Restore the width and height and set the full screen mode off
            this.IsInFullScreenMode = false;
            this.ToggleFullScreenButton.SetResourceReference(AnimatedImageButton.ImageSourceProperty, "FullScreenIcon");
            this.Height = this.mediaControlBackupHeight;
            this.Width = this.mediaControlBackupWidth;

            foreach (UIElement element in this.fullScreenGrid.Children)
            {
                element.PreviewKeyDown -= FullScreenContentChildPreviewKeyDown;
            }

            Action action = delegate { this.IsChangingScreenMode = false; };
            this.Dispatcher.BeginInvoke(action, DispatcherPriority.Input);
        }

        /// <summary>
        /// Handles the PreviewKeyDown event on the children elements of the full screen content.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.Input.KeyEventArgs"/> instance containing the event data.</param>
        private void FullScreenContentChildPreviewKeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = true;
        }

        /// <summary>
        /// Adds the control to parent.
        /// </summary>
        private void AddControlToParent()
        {
            if (!(this.parentControl is ContentControl || this.parentControl is Panel || this.parentControl is ContentPresenter))
            {
                return;
            }

            // Remove the media control from its parent
            if (this.parentControl is ContentControl)
            {
                (this.parentControl as ContentControl).Content = controlToMove;
            }
            else if (this.parentControl is ContentPresenter)
            {
                (this.parentControl as ContentPresenter).Content = controlToMove;
            }
            else
            {
                (this.parentControl as Panel).Children.Add(controlToMove);
            }
        }

        /// <summary>
        /// Removes this instance of media view from its parent.
        /// </summary>
        private void RemoveFromParent()
        {
            if (!(this.parentControl is ContentControl || this.parentControl is Panel || this.parentControl is ContentPresenter))
            {
                return;
            }

            // Remove the media control from its parent
            if (this.parentControl is ContentControl)
            {
                (this.parentControl as ContentControl).Content = null;
            }
            else if (this.parentControl is ContentPresenter)
            {
                (this.parentControl as ContentPresenter).Content = null;
            }
            else
            {
                (this.parentControl as Panel).Children.Remove(controlToMove);
            }
        }
    }
}
