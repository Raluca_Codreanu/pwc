﻿namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for VideoView.xaml
    /// </summary>    
    public partial class VideoView
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VideoView"/> class.
        /// </summary>
        public VideoView()
        {
            InitializeComponent();
        }
    }
}
