﻿using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using ZPKTool.Common;
using ZPKTool.Gui.ViewModels;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for PicturesView.xaml
    /// </summary>    
    public partial class PicturesView
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Weak event listener for the PropertyChanged notification.
        /// </summary>
        private static WeakEventListener<PropertyChangedEventArgs> mediaHandlerPropertyChangedListener;

        /// <summary>
        /// Weak event listener for the MediaHandler refresh notification.
        /// </summary>
        private static WeakEventListener<EventArgs> mediaHandlerRefreshListener;

        /// <summary>
        /// The index of the selected image in preview mode.
        /// </summary>
        private int selectedPictureIndex;

        /// <summary>
        /// Using a DependencyProperty as the backing store for NumberOfColumns.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty NumberOfColumnsProperty =
            DependencyProperty.Register("NumberOfColumns", typeof(int), typeof(PicturesView), new PropertyMetadata(0));

        /// <summary>
        /// Using a DependencyProperty as the backing store for ViewModelBase.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty ViewModelBaseProperty =
            DependencyProperty.Register("ViewModelBase", typeof(MediaHandlingViewModelBase), typeof(PicturesView), new PropertyMetadata(null, OnViewModelBaseChanged));

        /// <summary>
        ///  Using a DependencyProperty as the backing store for UpdateLayoutFlag.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty UpdateLayoutFlagProperty =
            DependencyProperty.Register("UpdateLayoutFlag", typeof(bool), typeof(PicturesView), new PropertyMetadata(false));

        /// <summary>
        /// Initializes a new instance of the <see cref="PicturesView"/> class.
        /// </summary>
        public PicturesView()
        {
            this.InitializeComponent();
            this.Loaded += this.OnLoaded;
            this.DataContextChanged += this.OnDataContextChanged;
            this.MouseLeftButtonDown += this.ImageMouseClick;
        }

        /// <summary>
        /// Gets or sets the number of columns.
        /// </summary>
        /// <value>
        /// The number of columns.
        /// </value>
        public int NumberOfColumns
        {
            get { return (int)GetValue(NumberOfColumnsProperty); }
            set { SetValue(NumberOfColumnsProperty, value); }
        }

        /// <summary>
        /// Gets or sets the view model base.
        /// </summary>
        /// <value>
        /// The view model base.
        /// </value>
        public MediaHandlingViewModelBase ViewModelBase
        {
            get { return (MediaHandlingViewModelBase)GetValue(ViewModelBaseProperty); }
            set { SetValue(ViewModelBaseProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to refresh the PicturesView Image stretch state, when switching from one state to another in media box.
        /// </summary>
        public bool UpdateLayoutFlag
        {
            get { return (bool)GetValue(UpdateLayoutFlagProperty); }
            set { SetValue(UpdateLayoutFlagProperty, value); }
        }

        /// <summary>
        /// Called when [view model base changed].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs" /> instance containing the event data.</param>
        private static void OnViewModelBaseChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var picturesView = sender as PicturesView;
            if (picturesView == null)
            {
                return;
            }

            var oldViewModel = e.NewValue as MediaHandlingViewModelBase;
            if (oldViewModel != null)
            {
                if (mediaHandlerPropertyChangedListener != null)
                {
                    PropertyChangedEventManager.RemoveListener(
                        oldViewModel,
                        mediaHandlerPropertyChangedListener,
                        string.Empty);
                }

                if (mediaHandlerRefreshListener != null)
                {
                    MediaHandlerRefreshEventManager.RemoveListener(oldViewModel, mediaHandlerRefreshListener);
                }
            }

            var newViewModel = e.NewValue as MediaHandlingViewModelBase;
            if (newViewModel != null)
            {
                mediaHandlerPropertyChangedListener = new WeakEventListener<PropertyChangedEventArgs>(picturesView.ViewModelOnPropertyChanged);
                PropertyChangedEventManager.AddListener(newViewModel, mediaHandlerPropertyChangedListener, string.Empty);

                mediaHandlerRefreshListener = new WeakEventListener<EventArgs>(picturesView.RefreshPreviewMode);
                MediaHandlerRefreshEventManager.AddListener(newViewModel, mediaHandlerRefreshListener);
            }
        }

        /// <summary>
        /// Called when [data context changed].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="dependencyPropertyChangedEventArgs">The <see cref="DependencyPropertyChangedEventArgs" /> instance containing the event data.</param>
        private void OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            if (this.DataContext == null)
            {
                return;
            }

            this.ViewModelBase = this.DataContext as MediaHandlingViewModelBase;
            if (this.ViewModelBase == null)
            {
                Log.Error("The DataContext is not a MediaHandlingViewModelBase");
            }
        }

        /// <summary>
        /// Refreshes the preview mode.
        /// </summary>
        /// <param name="sender">Object that originated the event.</param>
        /// <param name="e">Event data.</param>
        private void RefreshPreviewMode(object sender, EventArgs e)
        {
            this.Dispatcher.Invoke(new Action(this.SetPreviewModeOn));
        }

        /// <summary>
        /// Views the model on property changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="propertyChangedEventArgs">The <see cref="PropertyChangedEventArgs" /> instance containing the event data.</param>
        private void ViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (propertyChangedEventArgs.PropertyName == ReflectionUtils.GetPropertyName(() => this.ViewModelBase.IsPreviewModeOn))
            {
                this.Dispatcher.BeginInvoke(new Action(() => this.TogglePreviewMode(this.ViewModelBase.IsPreviewModeOn)));
            }
            else if (propertyChangedEventArgs.PropertyName == "CurrentMediaIndex")
            {
                this.selectedPictureIndex = this.ViewModelBase.CurrentMediaIndex;
            }
        }

        /// <summary>
        /// Called when [loaded].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="routedEventArgs">The <see cref="RoutedEventArgs" /> instance containing the event data.</param>
        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            if (ViewModelBase != null)
            {
                TogglePreviewMode(ViewModelBase.IsPreviewModeOn);
            }
        }

        /// <summary>
        /// Toggles the preview mode.
        /// </summary>
        /// <param name="setToPreviewMode">if set to <c>true</c> [set to preview mode].</param>
        private void TogglePreviewMode(bool setToPreviewMode)
        {
            this.UpdateLayoutFlag = !this.UpdateLayoutFlag;
            if (setToPreviewMode)
            {
                this.SetPreviewModeOn();
            }
            else
            {
                this.SetPreviewModeOff();
            }
        }

        /// <summary>
        /// Creates the image grid for preview mode
        /// </summary>
        private void CreateImageGrid()
        {
            var picturesVm = this.ViewModelBase as PicturesViewModel;
            if (picturesVm == null)
            {
                return;
            }

            this.ViewModelBase.ValidateMediaToDisplay();

            // If there is a single source in Media, displays it without drawing the grid used for multiple images.
            if (this.ViewModelBase.MediaSource.AllSources.Count == 1)
            {
                ViewModelBase.DisplayMediaAtIndex(0);
                return;
            }

            if (this.ViewModelBase.MediaSource.AllSources.Count <= 4)
            {
                ThirdColumn.Width = new GridLength(0);
                this.NumberOfColumns = 2;
            }
            else
            {
                ThirdColumn.Width = SecondColumn.Width;
                this.NumberOfColumns = 3;
            }

            var viewPictures = picturesVm.GetAllImageSources();
            for (var rowIndex = 0; rowIndex < viewPictures.Count; rowIndex++)
            {
                PreviewImagesGrid.RowDefinitions.Add(new RowDefinition());

                for (var columnIndex = 0; columnIndex < this.NumberOfColumns; columnIndex++)
                {
                    if ((rowIndex * this.NumberOfColumns) + columnIndex >= viewPictures.Count)
                    {
                        rowIndex = viewPictures.Count;
                        break;
                    }

                    var source = viewPictures[(rowIndex * this.NumberOfColumns) + columnIndex];
                    if (source != null)
                    {
                        var image = new Image { Source = source, Margin = new Thickness(3, 3, 3, 3) };

                        image.MouseEnter += this.ImageMouseEnter;
                        image.MouseLeave += this.ImageMouseLeave;
                        image.MouseLeftButtonDown += this.ImageMouseClick;
                        image.RenderTransformOrigin = new Point(0.5, 0.5);
                        image.RenderTransform = new ScaleTransform(1, 1);
                        image.VerticalAlignment = VerticalAlignment.Top;

                        Grid.SetRow(image, rowIndex);
                        Grid.SetColumn(image, columnIndex);
                        PreviewImagesGrid.Children.Add(image);
                    }
                }
            }
        }

        /// <summary>
        /// Handles the MouseClick event of the Image control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void ImageMouseClick(object sender, RoutedEventArgs e)
        {
            var image = sender as Image;
            if (image == null)
            {
                return;
            }

            this.selectedPictureIndex = (Grid.GetRow(image) * this.NumberOfColumns) + Grid.GetColumn(image);

            // If media object is a path and the corresponding image on disk does not exist anymore do nothing.
            var selectedMediapath = this.ViewModelBase.MediaSource.AllSources.ElementAt(this.selectedPictureIndex) as string;
            if (selectedMediapath != null && !File.Exists(selectedMediapath))
            {
                return;
            }

            this.ViewModelBase.DisplayMediaAtIndex(this.selectedPictureIndex);
        }

        /// <summary>
        /// Handles the MouseLeave event of the Image control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseEventArgs"/> instance containing the event data.</param>
        private void ImageMouseLeave(object sender, MouseEventArgs e)
        {
            var image = sender as Image;
            if (!this.IsLoaded || image == null)
            {
                return;
            }

            image.Effect = null;
            this.RegisterName("CurrentImage", image);

            var mouseLeave = Resources["MouseLeave"] as Storyboard;
            if (mouseLeave != null)
            {
                mouseLeave.Begin();
            }

            this.UnregisterName("CurrentImage");

            var zindex = Panel.GetZIndex(image);
            Panel.SetZIndex(image, zindex - 1);
        }

        /// <summary>
        /// Handles the MouseEnter event of the Image control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseEventArgs"/> instance containing the event data.</param>
        private void ImageMouseEnter(object sender, MouseEventArgs e)
        {
            var image = sender as Image;
            if (image == null)
            {
                return;
            }

            var shadow = new DropShadowEffect { Color = Colors.Black, ShadowDepth = 0, BlurRadius = 5 };
            image.Effect = shadow;
            this.RegisterName("CurrentImage", image);

            var mouseEnter = Resources["MouseEnter"] as Storyboard;
            if (mouseEnter != null)
            {
                mouseEnter.Begin();
            }

            this.UnregisterName("CurrentImage");

            var zindex = Panel.GetZIndex(image);
            Panel.SetZIndex(image, zindex + 1);
        }

        /// <summary>
        /// Sets the preview mode on.
        /// </summary>
        public void SetPreviewModeOn()
        {
            PreviewImagesGrid.Children.RemoveRange(0, PreviewImagesGrid.Children.Count);
            if (PreviewImagesGrid.RowDefinitions.Count > 0)
            {
                PreviewImagesGrid.RowDefinitions.RemoveRange(0, PreviewImagesGrid.RowDefinitions.Count);
            }

            this.CreateImageGrid();
        }

        /// <summary>
        /// Sets the preview mode off.
        /// </summary>
        private void SetPreviewModeOff()
        {
            PreviewImagesGrid.Children.RemoveRange(0, PreviewImagesGrid.Children.Count);
            if (PreviewImagesGrid.RowDefinitions.Count > 0)
            {
                PreviewImagesGrid.RowDefinitions.RemoveRange(0, PreviewImagesGrid.RowDefinitions.Count);
            }
        }
    }
}
