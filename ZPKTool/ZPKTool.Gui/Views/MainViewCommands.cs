﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using ZPKTool.Gui.Resources;
using ZPKTool.MvvmCore;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Provides the set of commands for the MainView.
    /// </summary>
    public static class MainViewCommands
    {
        /// <summary>
        /// Initializes static members of the <see cref="MainViewCommands"/> class.
        /// </summary>
        static MainViewCommands()
        {
            InitializeCommands();
        }

        #region Commands

        /// <summary>
        /// Gets a value representing the Undo command.
        /// </summary>
        public static RoutedUICommand Undo { get; private set; }

        /// <summary>
        /// Gets a value representing the Cut command.
        /// </summary>
        public static RoutedUICommand Cut { get; private set; }

        /// <summary>
        /// Gets a value representing the Copy command.
        /// </summary>
        public static RoutedUICommand Copy { get; private set; }

        /// <summary>
        /// Gets a value representing the Paste command.
        /// </summary>
        public static RoutedUICommand Paste { get; private set; }

        /// <summary>
        /// Gets a value representing the Delete command.
        /// </summary>
        public static RoutedUICommand Delete { get; private set; }

        /// <summary>
        /// Gets a value representing the Edit command.
        /// </summary>
        public static RoutedUICommand Edit { get; private set; }

        /// <summary>
        /// Gets a value representing the Rename command.
        /// </summary>
        public static RoutedUICommand Rename { get; private set; }

        /// <summary>
        /// Gets a value representing the Open Readonly command.
        /// </summary>
        public static RoutedUICommand OpenReadonly { get; private set; }

        /// <summary>
        /// Gets a value representing the Create Project Folder command.
        /// </summary>        
        public static RoutedUICommand CreateProjectFolder { get; private set; }

        /// <summary>
        /// Gets a value representing the Create Project command.
        /// </summary>        
        public static RoutedUICommand CreateProject { get; private set; }

        /// <summary>
        /// Gets a value representing the Create Assembly command.
        /// </summary>        
        public static RoutedUICommand CreateAssembly { get; private set; }

        /// <summary>
        /// Gets a value representing the Create Part command.
        /// </summary>        
        public static RoutedUICommand CreatePart { get; private set; }

        /// <summary>
        /// Gets a value representing the Import command.
        /// </summary>
        public static RoutedUICommand Import { get; private set; }

        /// <summary>
        /// Gets a value representing the Import BOM command.
        /// </summary>
        public static RoutedUICommand BomImport { get; private set; }

        /// <summary>
        /// Gets a value representing the Export command.
        /// </summary>        
        public static RoutedUICommand Export { get; private set; }

        /// <summary>
        /// Gets a value representing the Release Project command.
        /// </summary>        
        public static RoutedUICommand ReleaseProject { get; private set; }

        /// <summary>
        /// Gets a value representing the Un-release Project command.
        /// </summary>        
        public static RoutedUICommand UnreleaseProject { get; private set; }

        /// <summary>
        /// Gets a value representing the Un-release Project Folder command.
        /// </summary>        
        public static RoutedUICommand UnreleaseProjectFolder { get; private set; }

        /// <summary>
        /// Gets a value representing the Batch Reporting command.
        /// </summary>        
        public static RoutedUICommand BatchReporting { get; private set; }

        /// <summary>
        /// Gets a value representing the Report Generator command.
        /// </summary>
        public static RoutedUICommand ReportGenerator { get; private set; }

        /// <summary>
        /// Gets a value representing the Mass Data Update command.
        /// </summary>
        public static RoutedUICommand MassDataUpdate { get; private set; }

        /// <summary>
        /// Gets a value representing the Compare command.
        /// </summary>
        public static RoutedUICommand Compare { get; private set; }

        /// <summary>
        /// Gets a value representing the Paste and Search command.
        /// </summary>
        //// TODO: this command should be moved into the SearchTool scope
        public static RoutedUICommand PasteAndSearch { get; private set; }

        /// <summary>
        /// Gets a value representing the "Copy to Master Data" command.
        /// </summary>        
        public static RoutedUICommand CopyToMasterData { get; private set; }

        /// <summary>
        /// Gets a value representing the "Create Raw Material" command.
        /// </summary>        
        public static RoutedUICommand CreateRawMaterial { get; private set; }

        /// <summary>
        /// Gets a value representing the "Create Raw Part" command.
        /// </summary>        
        public static RoutedUICommand CreateRawPart { get; private set; }

        /// <summary>
        /// Gets a value representing the "Create Consumable" command.
        /// </summary>        
        public static RoutedUICommand CreateConsumable { get; private set; }

        /// <summary>
        /// Gets a value representing the "Create Commodity" command.
        /// </summary>        
        public static RoutedUICommand CreateCommodity { get; private set; }

        /// <summary>
        /// Gets a value representing the "Create Machine" command.
        /// </summary>        
        public static RoutedUICommand CreateMachine { get; private set; }

        /// <summary>
        /// Gets a value representing the "Create Die" command.
        /// </summary>        
        public static RoutedUICommand CreateDie { get; private set; }

        /// <summary>
        /// Gets a value representing the Synchronize command.
        /// </summary>        
        public static RoutedUICommand Synchronize { get; private set; }

        /// <summary>
        /// Gets a value representing the Selective Sync command.
        /// </summary>
        public static RoutedUICommand SelectiveSync { get; private set; }

        /// <summary>
        /// Gets a value representing the Add Bookmark command.
        /// </summary>
        public static RoutedUICommand AddBookmark { get; private set; }

        /// <summary>
        /// Gets a value representing the Remove Bookmark command.
        /// </summary>
        public static RoutedUICommand RemoveBookmark { get; private set; }

        /// <summary>
        /// Gets a value representing the show tool pane command.
        /// </summary>
        public static RoutedUICommand ShowToolPane { get; private set; }

        /// <summary>
        /// Gets a value representing the advanced search pane command.
        /// </summary>
        public static RoutedUICommand AdvancedSearch { get; private set; }
       
        #endregion Commands

        /// <summary>
        /// Initializes the commands.
        /// </summary>
        private static void InitializeCommands()
        {
            Undo = new RoutedUICommand(
               LocalizedResources.General_Undo,
               "Undo",
               typeof(MainViewCommands),
               new InputGestureCollection(new[] { new KeyGesture(Key.Z, ModifierKeys.Control) }));
            Cut = new RoutedUICommand(
                LocalizedResources.General_Cut,
                "Cut",
                typeof(MainViewCommands),
                new InputGestureCollection(new[] { new KeyGesture(Key.X, ModifierKeys.Control) }));
            Copy = new RoutedUICommand(
                LocalizedResources.General_Copy,
                "Copy",
                typeof(MainViewCommands),
                new InputGestureCollection(new[] { new KeyGesture(Key.C, ModifierKeys.Control) }));
            Paste = new RoutedUICommand(
                LocalizedResources.General_Paste,
                "Paste",
                typeof(MainViewCommands),
                new InputGestureCollection(new[] { new KeyGesture(Key.V, ModifierKeys.Control) }));
            Delete = new RoutedUICommand(
                LocalizedResources.General_Delete,
                "Delete",
                typeof(MainViewCommands),
                new InputGestureCollection(new[] { new KeyGesture(Key.Delete), new KeyGesture(Key.Delete, ModifierKeys.Shift) }));

            OpenReadonly = new RoutedUICommand(LocalizedResources.OpenReadonly, "OpenReadonly", typeof(MainViewCommands));
            Rename = new RoutedUICommand(LocalizedResources.Rename, "Rename", typeof(MainViewCommands));
            Edit = new RoutedUICommand(LocalizedResources.General_Edit, "Edit", typeof(MainViewCommands));

            ReleaseProject = new RoutedUICommand(LocalizedResources.General_Release, "ReleaseProject", typeof(MainViewCommands));
            UnreleaseProject = new RoutedUICommand(LocalizedResources.General_Unrelease, "UnreleaseProject", typeof(MainViewCommands));
            UnreleaseProjectFolder = new RoutedUICommand(LocalizedResources.General_Unrelease, "UnreleaseProjectFolder", typeof(MainViewCommands));
            Import = new RoutedUICommand(LocalizedResources.General_Import, "Import", typeof(MainViewCommands));
            BomImport = new RoutedUICommand(LocalizedResources.General_ImportBOM, "ImportBOM", typeof(MainViewCommands));
            Export = new RoutedUICommand(LocalizedResources.General_Export, "Export", typeof(MainViewCommands));
            BatchReporting = new RoutedUICommand(LocalizedResources.General_BatchReporting, "BatchReporting", typeof(MainViewCommands));
            ReportGenerator = new RoutedUICommand(LocalizedResources.General_ReportGenerator, "ReprotGenerator", typeof(MainViewCommands));
            MassDataUpdate = new RoutedUICommand(LocalizedResources.General_MassDataUpdate, "MassDataUpdate", typeof(MainViewCommands));
            Compare = new RoutedUICommand(LocalizedResources.General_Compare, "Compare", typeof(MainViewCommands));
            PasteAndSearch = new RoutedUICommand(LocalizedResources.General_PasteAndSearch, "PasteAndSearch", typeof(MainViewCommands));
            CopyToMasterData = new RoutedUICommand(LocalizedResources.General_CopyToMasterData, "CopyToMasterData ", typeof(MainViewCommands));

            CreateProjectFolder = new RoutedUICommand(LocalizedResources.General_CreateFolder, "CreateFolder", typeof(MainViewCommands));
            CreateProject = new RoutedUICommand(LocalizedResources.General_CreateProject, "CreateProject", typeof(MainViewCommands));
            CreateAssembly = new RoutedUICommand(LocalizedResources.General_CreateAssembly, "CreateAssembly", typeof(MainViewCommands));
            CreatePart = new RoutedUICommand(LocalizedResources.General_CreatePart, "CreatePart", typeof(MainViewCommands));
            CreateRawMaterial = new RoutedUICommand(LocalizedResources.General_CreateRawMaterial, "CreateRawMaterial", typeof(MainViewCommands));
            CreateRawPart = new RoutedUICommand(LocalizedResources.General_CreateRawPart, "CreateRawPart", typeof(MainViewCommands));
            CreateConsumable = new RoutedUICommand(LocalizedResources.General_CreateConsumable, "CreateConsumable", typeof(MainViewCommands));
            CreateCommodity = new RoutedUICommand(LocalizedResources.General_CreateCommodity, "CreateCommodity", typeof(MainViewCommands));
            CreateMachine = new RoutedUICommand(LocalizedResources.General_CreateMachine, "CreateMachine", typeof(MainViewCommands));
            CreateDie = new RoutedUICommand(LocalizedResources.General_CreateDie, "CreateDie", typeof(MainViewCommands));
            Synchronize = new RoutedUICommand(LocalizedResources.General_Synchronize, "Synchronize", typeof(MainViewCommands));
            SelectiveSync = new RoutedUICommand(LocalizedResources.Synchronization_SelectiveSync, "SelectiveSync", typeof(MainViewCommands));
            AdvancedSearch = new RoutedUICommand(LocalizedResources.General_AdvancedSearch, "AdvancedSearch", typeof(MainViewCommands));

            AddBookmark = new RoutedUICommand(LocalizedResources.General_AddBookmark, "AddBookmark", typeof(MainViewCommands));
            RemoveBookmark = new RoutedUICommand(LocalizedResources.General_RemoveBookmark, "RemoveBookmark", typeof(MainViewCommands));

            ShowToolPane = new RoutedUICommand(string.Empty, "ShowToolPane", typeof(MainViewCommands));
        }
    }
}
