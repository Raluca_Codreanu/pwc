﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// The interface used for the projects explorer trees instances in order to set focus on it.
    /// </summary>
    public interface IFocusDispatcher
    {
        /// <summary>
        /// Sets the focus in the control content.
        /// </summary>
        void FocusContent();
    }
}
