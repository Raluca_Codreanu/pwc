﻿<UserControl x:Class="ZPKTool.Gui.Views.ChangePasswordView"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
             xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
             xmlns:resources="clr-namespace:ZPKTool.Gui.Resources"
             xmlns:data="clr-namespace:ZPKTool.Data;assembly=Data"
             xmlns:mvvmcore="http://zpktool.com/mvvmcore"
             xmlns:i="clr-namespace:System.Windows.Interactivity;assembly=System.Windows.Interactivity"
             xmlns:guiControls="clr-namespace:ZPKTool.Gui.Controls"
             xmlns:controls="clr-namespace:ZPKTool.Controls;assembly=ZPKTool.Controls"
             xmlns:common="clr-namespace:ZPKTool.Common;assembly=ZPKTool.Common"
             xmlns:view="clr-namespace:ZPKTool.Gui.Views"
             xmlns:converters="clr-namespace:ZPKTool.Gui.Converters"
             mc:Ignorable="d">

    <UserControl.Resources>
        <converters:BoolToPasswordPolicyStatusIcon x:Key="BoolToPasswordPolicyStatusIcon" />
        <converters:BoolToVisibilityConverter x:Key="BoolToVisibilityConverter" />
        <converters:BoolToVisibilityConverter x:Key="InvertedBoolToVisibilityConverter"
                                              Inverted="True" />
    </UserControl.Resources>

    <StackPanel>
        <Grid x:Name="LayoutRoot"
          Margin="10" >
            <Grid.RowDefinitions>
                <RowDefinition Height="Auto" />
                <RowDefinition Height="Auto" />
                <RowDefinition Height="Auto" />
                <RowDefinition Height="Auto" />
                <RowDefinition Height="Auto" />
            </Grid.RowDefinitions>

            <Grid.ColumnDefinitions>
                <ColumnDefinition Width="*" />
                <ColumnDefinition Width="1.5*" />
            </Grid.ColumnDefinitions>

            <controls:LabelControl Text="{x:Static resources:LocalizedResources.Password_ChangeMandatory}"
                               HorizontalAlignment="Center"
                               VerticalAlignment="Bottom"
                               Margin="0,0,0,10"
                               Grid.ColumnSpan="2"
                               Visibility="{Binding Path=IsPasswordChangeMandatory, Converter={StaticResource BoolToVisibilityConverter}}"/>

            <controls:LabelControl Text="{x:Static resources:LocalizedResources.General_CurrentPassword}"
                               HorizontalAlignment="Right"
                               VerticalAlignment="Bottom"
                               Grid.Row="1"
                               Margin="0,0,0,5" />
            <PasswordBox x:Name="OldPassword"
                     HorizontalAlignment="Left"
                     VerticalAlignment="Bottom"
                     Width="175"
                     Margin="10,0,0,5"
                     Grid.Column="1"
                     Grid.Row="1"
                     controls:PasswordBoxAssistant.BindPassword="True"
                     controls:PasswordBoxAssistant.Necessity="Mandatory"
                     controls:PasswordBoxAssistant.Password="{Binding Path=OldPassword,Mode=TwoWay,UpdateSourceTrigger=PropertyChanged,ValidatesOnDataErrors=True}" />

            <controls:LabelControl Text="{x:Static resources:LocalizedResources.General_NewPassword}"
                               HorizontalAlignment="Right"
                               VerticalAlignment="Bottom"
                               Margin="0,0,0,5"
                               Grid.Row="2" />
            <StackPanel Grid.Row="2"
                    Grid.Column="1"
                    Orientation="Horizontal"
                    Margin="10,0,0,5">
                <PasswordBox x:Name="NewPassword"  
                         VerticalAlignment="Center"
                         Width="175"                                         
                         controls:PasswordBoxAssistant.BindPassword="True"
                         controls:PasswordBoxAssistant.Necessity="Mandatory"
                         controls:PasswordBoxAssistant.ValidatePassword="True"
                         controls:PasswordBoxAssistant.ValidatePasswordForDuplicateAndSequenceOfCharacters="True"
                         controls:PasswordBoxAssistant.PasswordValidationExpression="{x:Static common:Constants.PasswordValidationExpression}"
                         controls:PasswordBoxAssistant.IsPasswordValid="{Binding Path=IsPasswordValid, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged}"
                         controls:PasswordBoxAssistant.Password="{Binding Path=NewPassword,Mode=TwoWay,UpdateSourceTrigger=PropertyChanged,ValidatesOnDataErrors=True}" />
                <Image Source="{Binding Path=IsPasswordValid, Converter={StaticResource BoolToPasswordPolicyStatusIcon}}" 
                   Height="16"  
                   Width="16"
                   HorizontalAlignment="Right" 
                   Margin="5,0,0,0">
                    <Image.ToolTip>
                        <ToolTip Visibility="{Binding Path=IsPasswordValid, Converter={StaticResource InvertedBoolToVisibilityConverter}}">
                            <StackPanel>
                                <controls:LabelControl Text="{x:Static resources:LocalizedResources.Password_PolicyRequirements}" />
                            </StackPanel>
                        </ToolTip>
                    </Image.ToolTip>
                </Image>
            </StackPanel>
            <controls:LabelControl Text="{x:Static resources:LocalizedResources.ChangePassword_ConfirmNew}"
                               HorizontalAlignment="Right"
                               VerticalAlignment="Bottom"
                               Margin="0,0,0,5"
                               Grid.Row="3" />
            <StackPanel Grid.Row="3"
                    Grid.Column="1"
                    Orientation="Horizontal"
                    Margin="10,0,0,5">
                <PasswordBox x:Name="ConfirmNewPassword"
                     HorizontalAlignment="Left"
                     VerticalAlignment="Bottom"
                     Width="175"
                     controls:PasswordBoxAssistant.BindPassword="True"
                     controls:PasswordBoxAssistant.Necessity="Mandatory"
                     controls:PasswordBoxAssistant.Password="{Binding Path=ConfirmNewPassword,Mode=TwoWay,UpdateSourceTrigger=PropertyChanged, ValidatesOnDataErrors=True}" />
                <Image Source="{Binding Path=PasswordsMatch, Converter={StaticResource BoolToPasswordPolicyStatusIcon}}"                                  
                   Height="16"  
                   Width="16"
                   HorizontalAlignment="Right" 
                   Margin="5,0,0,0">
                    <Image.ToolTip>
                        <ToolTip Visibility="{Binding Path=PasswordsMatch, Converter={StaticResource InvertedBoolToVisibilityConverter}}"
                                 Content="{x:Static resources:LocalizedResources.Password_PasswordsDoNotMatch}" />
                    </Image.ToolTip>
                </Image>
            </StackPanel>

            <!-- Change/Cancel buttons-->
            <StackPanel Orientation="Horizontal"
                    HorizontalAlignment="Right"
                    VerticalAlignment="Bottom"
                    Margin="30,10,8,0"
                    Grid.Row="4"
                    Grid.Column="1">
                <controls:Button x:Name="ChangeButton"
                             Content="{x:Static resources:LocalizedResources.General_Change}"
                             Command="{Binding ChangePasswordCommand}"
                             Margin="0,0,5,0" />
                <controls:Button x:Name="CancelButton"
                             Content="{x:Static resources:LocalizedResources.General_Cancel}"
                             Command="{Binding CancelCommand}"
                             Margin="5,0,0,0" />
            </StackPanel>
        </Grid>
    </StackPanel>
</UserControl>
