﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for WelcomeView.xaml
    /// </summary>
    public partial class WelcomeView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WelcomeView"/> class.
        /// </summary>
        public WelcomeView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handles the Loaded event of the Carousel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void Carousel_Loaded(object sender, RoutedEventArgs e)
        {
            this.Carousel.Focus();
            this.Carousel.SelectedIndex = 0;
        }
    }    
}
