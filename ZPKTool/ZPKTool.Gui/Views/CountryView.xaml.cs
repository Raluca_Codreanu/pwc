﻿using System.Windows;
using System.Windows.Controls;
using ZPKTool.Gui.ViewModels;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for CountryView.xaml
    /// </summary>
    public partial class CountryView : UserControl
    {
        #region Attributes

        /// <summary>
        /// The Undo Highlight manager, used to highlight view controls when undo is performed.
        /// </summary>
        private UndoHighlightManager viewManager;

        #endregion Attributes

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="CountryView"/> class.
        /// </summary>
        public CountryView()
        {
            InitializeComponent();
            this.DataContextChanged += this.OnDataContextChanged;
        }

        #endregion Constructor

        #region Event Handlers

        /// <summary>
        /// Called when [data context changed].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="dependencyPropertyChangedEventArgs">The <see cref="DependencyPropertyChangedEventArgs" /> instance containing the event data.</param>
        private void OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var countryVM = dependencyPropertyChangedEventArgs.NewValue as CountryViewModel;
            if (countryVM != null)
            {
                if (this.IsLoaded)
                {
                    this.viewManager = new UndoHighlightManager(countryVM.UndoManager, this.MainTabControl, this.DataContext);
                }
                else
                {
                    RoutedEventHandler initOnLoad = null;
                    initOnLoad = (s, e) =>
                    {
                        this.viewManager = new UndoHighlightManager(countryVM.UndoManager, this.MainTabControl, this.DataContext);
                        this.Loaded -= initOnLoad;
                    };
                    this.Loaded += initOnLoad;
                }
            }
            else
            {
                this.viewManager = null;
            }
        }

        #endregion Event Handlers
    }
}
