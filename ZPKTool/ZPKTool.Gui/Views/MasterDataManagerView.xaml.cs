﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ZPKTool.Gui.Behaviors;
using ZPKTool.Gui.ViewModels;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for MasterDataManagerView.xaml
    /// </summary>
    public partial class MasterDataManagerView : UserControl
    {
        /// <summary>
        /// The dictionary that contains the columns and their index, for each data grid.
        /// </summary>
        private static Dictionary<DataGrid, List<Tuple<DataGridColumn, int>>> dataGridColumnIndexes = new Dictionary<DataGrid, List<Tuple<DataGridColumn, int>>>();

        /// <summary>
        /// Initializes a new instance of the <see cref="MasterDataManagerView"/> class.
        /// </summary>
        public MasterDataManagerView()
        {
            InitializeComponent();
            this.EntitiesDataGrid.Loaded += EntitiesDataGrid_Loaded;
            this.EntitiesDataGrid.Unloaded += EntitiesDataGrid_Unloaded;
            this.EntitiesDataGrid.ColumnReordered += EntitiesDataGrid_ColumnReordered;
        }

        #region Dependency Properties

        /// <summary>
        /// Using a DependencyProperty as the backing store for SourceMasterDataType.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty SourceMasterDataTypeProperty =
            DependencyProperty.Register("SourceMasterDataType", typeof(Type), typeof(MasterDataManagerView), new PropertyMetadata(null, new PropertyChangedCallback(OnSourceMasterDataTypeChanged)));

        /// <summary>
        /// Using a DependencyProperty as the backing store for Loading.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty LoadedVMProperty =
            DependencyProperty.Register("LoadedVM", typeof(bool), typeof(MasterDataManagerView), new PropertyMetadata(false, new PropertyChangedCallback(OnLoadedVMChanged)));
        
        /// <summary>
        /// Gets or sets the source of the master data type.
        /// </summary>
        public Type SourceMasterDataType
        {
            get { return (Type)GetValue(SourceMasterDataTypeProperty); }
            set { SetValue(SourceMasterDataTypeProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the view model is loaded.
        /// </summary>
        public bool LoadedVM
        {
            get { return (bool)GetValue(LoadedVMProperty); }
            set { SetValue(LoadedVMProperty, value); }
        }

        #endregion Dependency Properties

        /// <summary>
        /// Called when the SourceMasterDataType property is changed.
        /// </summary>
        /// <param name="d">The dependency object.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnSourceMasterDataTypeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if ((e.NewValue != null && e.OldValue != null) || e.NewValue == null)
            {
                // If the user switches between the master data types, or lives the grid, the layout is saved.
                var view = d as MasterDataManagerView;
                var dataGrid = view.EntitiesDataGrid;

                var columnIndexes = new List<Tuple<DataGridColumn, int>>();
                foreach (var column in dataGrid.Columns)
                {
                    columnIndexes.Add(new Tuple<DataGridColumn, int>(column, column.DisplayIndex));
                }

                dataGridColumnIndexes[dataGrid] = columnIndexes;

                DataGridBehavior.SaveDataGridLayout(dataGrid, dataGridColumnIndexes);
                dataGridColumnIndexes.Remove(dataGrid);
            }
        }

        /// <summary>
        /// Called when the LoadedVM property is changed.
        /// </summary>
        /// <param name="d">The dependency object.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnLoadedVMChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue == true)
            {
                // When a new view model is loaded, the layout will be restored for it's grid.
                var view = d as MasterDataManagerView;
                var dataGrid = view.EntitiesDataGrid;

                DataGridBehavior.RestoreDataGridLayout(dataGrid);
            }
        }

        /// <summary>
        /// Handles the Loaded event of EntitiesDataGrid.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void EntitiesDataGrid_Loaded(object sender, RoutedEventArgs e)
        {
            // Only after the data grid is loaded, the restore method can get it's persist id.  
            var dataGrid = (DataGrid)sender;
            DataGridBehavior.RestoreDataGridLayout(dataGrid);
        }

        /// <summary>
        /// Handles the Unloaded event of EntitiesDataGrid.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void EntitiesDataGrid_Unloaded(object sender, RoutedEventArgs e)
        {
            var dataGrid = (DataGrid)sender;
            var newColIndexes = new List<Tuple<DataGridColumn, int>>();
            foreach (var column in dataGrid.Columns)
            {
                newColIndexes.Add(new Tuple<DataGridColumn, int>(column, column.DisplayIndex));
            }

            dataGridColumnIndexes[dataGrid] = newColIndexes;
            DataGridBehavior.SaveDataGridLayout(dataGrid, dataGridColumnIndexes);
        }

        /// <summary>
        /// Handles the ColumnReordered event of EntitiesDataGrid.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.Controls.DataGridColumnEventArgs"/> instance containing the event data.</param>
        private void EntitiesDataGrid_ColumnReordered(object sender, DataGridColumnEventArgs e)
        {
            var dataGrid = (DataGrid)sender;
            var newColIndexes = new List<Tuple<DataGridColumn, int>>();
            foreach (var column in dataGrid.Columns)
            {
                newColIndexes.Add(new Tuple<DataGridColumn, int>(column, column.DisplayIndex));
            }

            dataGridColumnIndexes[dataGrid] = newColIndexes;
        }

        /// <summary>
        /// Handles the SelectionChanged event of the EntitiesDataGrid control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Controls.SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void EntitiesDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Bring the selected item into view because the view model has no way of doing it.
            if (e.AddedItems.Count > 0)
            {
                this.EntitiesDataGrid.ScrollIntoView(e.AddedItems[0]);
            }
        }
    }
}
