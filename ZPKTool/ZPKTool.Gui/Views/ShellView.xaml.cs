﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for ShellView.xaml
    /// </summary>
    public partial class ShellView
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ShellView"/> class.
        /// </summary>
        public ShellView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handles the KetDown event for the zooming combo box. Used to update the data source when ENTER is pressed.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> instance containing the event data.</param>
        private void ZoomLevelsComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return || e.Key == Key.Enter)
            {
                ComboBox comboBox = sender as ComboBox;
                UpdateDataSource(comboBox, ComboBox.TextProperty);
                e.Handled = true;
            }
        }

        /// <summary>
        /// Updates the data source for the specified element.
        /// </summary>
        /// <param name="element">The target framework element.</param>
        /// <param name="dp">The target DependencyProperty to get the binding from.</param>
        private void UpdateDataSource(FrameworkElement element, DependencyProperty dp)
        {
            if (element != null)
            {
                BindingExpression expression = element.GetBindingExpression(dp);
                if (expression != null)
                {
                    expression.UpdateSource();
                }                      
            }
        }
    }
}
