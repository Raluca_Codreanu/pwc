﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for MaterialsView.xaml
    /// </summary>
    public partial class MaterialsView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MaterialsView" /> class.
        /// </summary>
        public MaterialsView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handles the SelectionChanged event of the MaterialsDataGrid control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Controls.SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void MaterialsDataGridSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Bring the selected item into view because the view model has no way of doing it.
            if (e.AddedItems.Count > 0)
            {
                this.MaterialsDataGrid.UpdateLayout();
                this.MaterialsDataGrid.ScrollIntoView(e.AddedItems[0]);
            }
        }

        /// <summary>
        /// Handles the MouseDown event of the MaterialsDataGrid control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void MaterialsDataGrid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Right)
            {
                // When right clicked, the context menu opens but the data grid does not get focus, causing the context click menu's commands not to
                // be routed to the materialsView.
                this.MaterialsDataGrid.Focus();
            }
        }
    }
}
