﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for RoleSelectionView.xaml
    /// </summary>
    public partial class RoleSelectionView
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RoleSelectionView"/> class.
        /// </summary>
        public RoleSelectionView()
        {
            this.InitializeComponent();
        }
    }
}