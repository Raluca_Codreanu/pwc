﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Data;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// A converter that determines the price to be displayed in the Materials view data grid.
    /// The first value represents the object.
    /// The second value represents the units adapter that is used to get the current measurement weight unit.
    /// The third value represents the weight unit, by using this, the converter will be re-evaluated when the weight changes.
    /// </summary>
    internal class MaterialsViewPriceConverter : IMultiValueConverter
    {
        #region IValueConverter Members

        /// <summary>
        /// Converts source values to a value for the binding target. The data binding engine calls this method when it propagates the values from source bindings to the binding target.
        /// </summary>
        /// <param name="values">The array of values that the source bindings in the <see cref="T:System.Windows.Data.MultiBinding" /> produces. The value <see cref="F:System.Windows.DependencyProperty.UnsetValue" /> indicates that the source binding has no value to provide for conversion.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value.If the method returns null, the valid null value is used.A return value of <see cref="T:System.Windows.DependencyProperty" />.<see cref="F:System.Windows.DependencyProperty.UnsetValue" /> indicates that the converter did not produce a value, and that the binding will use the <see cref="P:System.Windows.Data.BindingBase.FallbackValue" /> if it is available, or else will use the default value.A return value of <see cref="T:System.Windows.Data.Binding" />.<see cref="F:System.Windows.Data.Binding.DoNothing" /> indicates that the binding does not transfer the value or use the <see cref="P:System.Windows.Data.BindingBase.FallbackValue" /> or the default value.
        /// </returns>
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var material = values[0] as RawMaterial;
            var adapter = values[1] as UnitsAdapter;
            if (material != null
                && adapter != null)
            {
                var priceUnit = material.QuantityUnitBase != null ? material.QuantityUnitBase : material.ParentWeightUnitBase;
                var currentWeightUnit = adapter.UIWeight.ToMeasurementUnit();

                return CostCalculationHelper.AdjustRawMaterialPriceToNewWeightUnit(material.Price ?? 0m, priceUnit, currentWeightUnit);
            }

            var commodity = values[0] as Commodity;
            if (commodity != null)
            {
                return commodity.Price;
            }

            return null;
        }

        /// <summary>
        /// Converts a binding target value to the source binding values.
        /// </summary>
        /// <param name="value">The value that the binding target produces.</param>
        /// <param name="targetTypes">The array of types to convert to. The array length indicates the number and types of values that are suggested for the method to return.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// An array of values that have been converted from the target value back to the source values.
        /// </returns>
        /// <exception cref="System.NotImplementedException">The method is not implemented.</exception>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
