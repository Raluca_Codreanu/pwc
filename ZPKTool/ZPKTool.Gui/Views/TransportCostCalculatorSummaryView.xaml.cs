﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for TransportCostCalculatorSummaryView.xaml
    /// </summary>
    public partial class TransportCostCalculatorSummaryView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TransportCostCalculatorSummaryView"/> class.
        /// </summary>
        public TransportCostCalculatorSummaryView()
        {
            InitializeComponent();
            this.Loaded += TransportCostCalculatorSummaryView_Loaded;
        }

        /// <summary>
        /// Handles the Loaded event of the TransportCostCalculatorSummaryView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void TransportCostCalculatorSummaryView_Loaded(object sender, RoutedEventArgs e)
        {
            var transportCostBinding = this.TransportCostErrorImage.GetBindingExpression(Image.VisibilityProperty);
            transportCostBinding.UpdateTarget();

            var packagingCostBinding = this.PackagingCostErrorImage.GetBindingExpression(Image.VisibilityProperty);
            packagingCostBinding.UpdateTarget();
        }
    }
}
