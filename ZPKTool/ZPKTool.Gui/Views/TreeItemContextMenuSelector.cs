﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// This converter selects the appropriate context menu for tree items.
    /// The source values provided must be:
    ///     - the 1st must be the data item on which the tree hierarchical data template is applied.
    ///     - the 2nd must be the TreeView instance that uses the converter instance and defines in its resources the context menus from which to select.
    /// </summary>
    public class TreeItemContextMenuSelector : IMultiValueConverter
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Converts source values to a value for the binding target. The data binding engine calls this method when it propagates the values from source bindings to the binding target.
        /// </summary>
        /// <param name="values">The array of values that the source bindings in the <see cref="T:System.Windows.Data.MultiBinding"/> produces. The value <see cref="F:System.Windows.DependencyProperty.UnsetValue"/> indicates that the source binding has no value to provide for conversion.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value.If the method returns null, the valid null value is used.A return value of <see cref="T:System.Windows.DependencyProperty"/>.<see cref="F:System.Windows.DependencyProperty.UnsetValue"/> indicates that the converter did not produce a value, and that the binding will use the <see cref="P:System.Windows.Data.BindingBase.FallbackValue"/> if it is available, or else will use the default value.A return value of <see cref="T:System.Windows.Data.Binding"/>.<see cref="F:System.Windows.Data.Binding.DoNothing"/> indicates that the binding does not transfer the value or use the <see cref="P:System.Windows.Data.BindingBase.FallbackValue"/> or the default value.
        /// </returns>
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values == DependencyProperty.UnsetValue)
            {
                return null;
            }

            if (values.Length != 2)
            {
                throw new ArgumentException("Wrong number of values provided.");
            }

            if (values[0] == null || values[0] == DependencyProperty.UnsetValue ||
                values[1] == null || values[1] == DependencyProperty.UnsetValue)
            {
                return null;
            }

            FrameworkElement view = values[1] as FrameworkElement;
            if (view == null)
            {
                throw new ArgumentException("The 2nd value provided was not a FrameworkElement instance.");
            }

            // The ResourceKey of an item's context menu template is by convention the item's type name followed by "ContextMenuTemplate". ex: ProjectTreeItemContextMenuTemplate
            ContextMenu contextMenu = null;
            string type = values[0].GetType().Name;
            string key = type + "ContextMenuTemplate";
            DataTemplate contextMenuTemplate = view.Resources[key] as DataTemplate;
            if (contextMenuTemplate != null)
            {
                contextMenu = contextMenuTemplate.LoadContent() as ContextMenu;
                if (contextMenu == null)
                {
                    throw new InvalidOperationException("The context menu template for " + type + "did not contain an instance of ContentMenu as the root element.");
                }

                // Set the data item as the DataContext of the menu to enable binding.
                contextMenu.DataContext = values[0];
            }

            return contextMenu;
        }

        /// <summary>
        /// Converts a binding target value to the source binding values.
        /// </summary>
        /// <param name="value">The value that the binding target produces.</param>
        /// <param name="targetTypes">The array of types to convert to. The array length indicates the number and types of values that are suggested for the method to return.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// An array of values that have been converted from the target value back to the source values.
        /// </returns>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
