﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// A converter used by the shell view's menu items to obtain their icon based on the resource key stored in their source <see cref="SystemMenuItem"/> instance.
    /// </summary>
    public class ShellMenuItemIconConverter : IValueConverter
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return null;
            }

            var stringKey = value as string;
            if (stringKey != null && string.IsNullOrWhiteSpace(stringKey))
            {
                // The icon resource key is an empty or whitespace string.
                return null;
            }

            var resource = Application.Current.TryFindResource(value);
            if (resource == null)
            {
                log.Warn("The resource with the key '{0}' was not found.", value);
                return null;
            }

            var icon = resource as ImageSource;
            if (icon == null)
            {
                log.Warn("The resource with the key '{0}' was not an image (its type was {1}).", value, icon.GetType());
                return null;
            }

            Image image = new Image();
            image.Width = 20d;
            image.Height = 20d;
            image.Source = icon;

            return image;
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        /// <exception cref="System.NotSupportedException">This method is not supported.</exception>        
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
