﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Data;
using ZPKTool.Common;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Utils;

namespace ZPKTool.Gui.Converters
{
    /// <summary>
    /// Converter used for Version formatting.
    /// Converts the DateTime value to short format and returns the Version / DateTime in a string.
    /// </summary>
    [ValueConversion(typeof(object), typeof(string))]
    public class VersionDateConverter : IMultiValueConverter
    {
        #region IValueConverter Members

        /// <summary>
        /// Converts source values to a value for the binding target. The data binding engine calls this method when it propagates the values from source bindings to the binding target.
        /// </summary>
        /// <param name="values">The array of values that the source bindings in the <see cref="T:System.Windows.Data.MultiBinding"/> produces. The value <see cref="F:System.Windows.DependencyProperty.UnsetValue"/> indicates that the source binding has no value to provide for conversion.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <remarks>The input values must contain 3 items: The IsSelected property, the target TreeView and the source TreeViewDataItem.</remarks>
        /// <returns>
        /// A converted value.If the method returns null, the valid null value is used.A return value of <see cref="T:System.Windows.DependencyProperty"/>.<see cref="F:System.Windows.DependencyProperty.UnsetValue"/> indicates that the converter did not produce a value, and that the binding will use the <see cref="P:System.Windows.Data.BindingBase.FallbackValue"/> if it is available, or else will use the default value.A return value of <see cref="T:System.Windows.Data.Binding"/>.<see cref="F:System.Windows.Data.Binding.DoNothing"/> indicates that the binding does not transfer the value or use the <see cref="P:System.Windows.Data.BindingBase.FallbackValue"/> or the default value.
        /// </returns>
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var version = values[0] as decimal?;
            var versionDate = values[1] as DateTime?;
            if (versionDate != null)
            {
                var shortDate = string.Empty;
                string format = parameter as string;
                DateTime date = (DateTime)versionDate;

                if (!string.IsNullOrWhiteSpace(format))
                {
                    shortDate = date.ToString(format, culture);
                }
                else
                {
                    shortDate = date.ToString(culture);
                }

                if (version != null)
                {
                    return Formatter.FormatNumber(version) + " / " + shortDate;
                }
                else
                {
                    return shortDate;
                }
            }
            else if (version != null)
            {
                return Formatter.FormatNumber(version);
            }

            return null;
        }

        /// <summary>
        /// Converts a binding target value to the source binding values.
        /// </summary>
        /// <param name="value">The value that the binding target produces.</param>
        /// <param name="targetTypes">The array of types to convert to. The array length indicates the number and types of values that are suggested for the method to return.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// An array of values that have been converted from the target value back to the source values.
        /// </returns>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
