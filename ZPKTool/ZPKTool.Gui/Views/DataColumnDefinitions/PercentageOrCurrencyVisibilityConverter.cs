﻿using System;
using System.Windows;
using System.Windows.Data;

namespace ZPKTool.Gui.Converters
{
    /// <summary>
    /// Converter used for returning the visibility, depending if the field represents a percentage or a currency.
    /// If the value is true the field represents a percentage and the converter returns visible, else returns collapsed.
    /// By setting Inverted true the logic is inverted.
    /// </summary>
    [ValueConversion(typeof(bool), typeof(Visibility))]
    public class PercentageOrCurrencyVisibilityConverter : IValueConverter
    {
        /// <summary>
        /// Gets or sets a value indicating whether the conversion logic is inverted, that is 'true' is converted to 'Visible' and 'false' to 'Collapsed'.
        /// Default value is false.
        /// </summary>
        public bool Inverted { get; set; }

        #region IValueConverter Members

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool isWearInPercentage = (bool)value;
            if ((isWearInPercentage && !this.Inverted)
                || (!isWearInPercentage && this.Inverted))
            {
                return Visibility.Collapsed;
            }

            return Visibility.Visible;
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}