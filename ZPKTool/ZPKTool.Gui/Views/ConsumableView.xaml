﻿<UserControl x:Class="ZPKTool.Gui.Views.ConsumableView"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
             xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
             mc:Ignorable="d"
             xmlns:s="clr-namespace:System;assembly=mscorlib"
             xmlns:i="clr-namespace:System.Windows.Interactivity;assembly=System.Windows.Interactivity"
             xmlns:mvvmcore="http://zpktool.com/mvvmcore"
             xmlns:views="clr-namespace:ZPKTool.Gui.Views"
             xmlns:controls="clr-namespace:ZPKTool.Controls;assembly=ZPKTool.Controls"
             xmlns:guiControls="clr-namespace:ZPKTool.Gui.Controls"
             xmlns:resources="clr-namespace:ZPKTool.Gui.Resources"
             xmlns:converters="clr-namespace:ZPKTool.Gui.Converters"
             FontFamily="Levenim MT">

    <UserControl.Resources>
        <converters:InverseBooleanConverter x:Key="BooleanInverter" />
        <converters:BoolToVisibilityConverter x:Key="ReverseBoolVisibilityConverter"
                                              Inverted="True" />

        <!-- ## Command references for command bindings -->
        <mvvmcore:CommandReference x:Key="UndoCommand"
                                   Command="{Binding UndoCommand}" />
    </UserControl.Resources>

    <UserControl.CommandBindings>
        <mvvmcore:DelegateCommandBinding Command="{x:Static views:MainViewCommands.Undo}"
                                         CommandReference="{StaticResource UndoCommand}" />
    </UserControl.CommandBindings>

    <Grid x:Name="LayoutRoot"
          VerticalAlignment="Top">
        <Grid.RowDefinitions>
            <RowDefinition Height="*" />
            <RowDefinition Height="Auto" />
        </Grid.RowDefinitions>
        <TabControl Name="MainTabControl">
            <guiControls:FormTabItem x:Name="GeneralTabItem"
                                     Header="{x:Static resources:LocalizedResources.General_General}"
                                     InvalidInputMessage="{Binding Error}">
                <GroupBox Visibility="Visible"
                              BorderBrush="{DynamicResource CommonBackgroundColor}"
                              Header="{x:Static resources:LocalizedResources.General_BasicInformation}"
                              x:Name="ConsumableGroupBox">
                    <Grid Margin="5">

                        <Grid.RowDefinitions>
                            <RowDefinition Height="Auto" />
                            <RowDefinition Height="Auto" />
                            <RowDefinition Height="Auto" />
                            <RowDefinition Height="Auto" />
                        </Grid.RowDefinitions>

                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width="Auto" />
                            <ColumnDefinition Width="*" />
                            <ColumnDefinition Width="Auto" />
                            <ColumnDefinition Width="*" />
                        </Grid.ColumnDefinitions>

                        <controls:LabelControl HorizontalAlignment="Left"
                                                   VerticalAlignment="Center"
                                                   Text="{x:Static resources:LocalizedResources.Consumable_Name}" />
                        <StackPanel Grid.Column="1"
                                        Orientation="Horizontal">
                            <controls:TextBoxControl x:Name="NameTextBox"
                                                         Margin="10,0,0,0"
                                                         Necessity="Mandatory"
                                                         Width="150"
                                                         HorizontalAlignment="Left"
                                                         VerticalAlignment="Center"
                                                         Text="{Binding Path=Name.Value,Mode=TwoWay,UpdateSourceTrigger=PropertyChanged,ValidatesOnDataErrors=True}"
                                                         IsReadOnly="{Binding IsReadOnly}" />
                            <controls:AnimatedImageButton x:Name="BrowseConsumableMasterData"
                                                              Height="24"
                                                              ImageSource="{DynamicResource MasterdataIcon}"
                                                              DisableEnlargingEffect="True"
                                                              VerticalAlignment="Center"
                                                              HorizontalAlignment="Left"
                                                              Margin="2,0,0,0"
                                                              Command="{Binding BrowseMasterDataCommand}"
                                                              ToolTip="{x:Static resources:LocalizedResources.MasterData_ButtonTooltip}"
                                                              IsEnabled="{Binding Path=IsReadOnly,Converter={StaticResource BooleanInverter}}" />
                        </StackPanel>
                        <controls:LabelControl Grid.Column="2"
                                                   HorizontalAlignment="Left"
                                                   Margin="20,0,0,0"
                                                   Text="{x:Static resources:LocalizedResources.General_PriceUnitBase}"
                                                   VerticalAlignment="Center" />
                        <controls:ComboBoxControl x:Name="PriceUnitBaseComboBox"
                                                      Grid.Column="3"
                                                      Margin="10,0,0,0"
                                                      VerticalAlignment="Center"
                                                      HorizontalAlignment="Left"
                                                      Width="150"
                                                      Necessity="Mandatory"
                                                      ItemsSource="{Binding Path=PriceUnitBaseItems}"
                                                      DisplayMemberPath="Name"
                                                      SelectedValue="{Binding Path=PriceUnit.Value,Mode=TwoWay}"
                                                      IsEnabled="{Binding Path=IsReadOnly, Converter={StaticResource BooleanInverter}}" />
                        <controls:LabelControl Grid.Row="1"
                                                   Margin="0,10,0,0"
                                                   Text="{x:Static resources:LocalizedResources.Consumable_Amount}"
                                                   HorizontalAlignment="Left"
                                                   VerticalAlignment="Center" />
                        <guiControls:TextBoxWithUnitSelector x:Name="AmountTextBox"
                                                                 Margin="10,10,0,0"
                                                                 Grid.ColumnSpan="2"
                                                                 Grid.Row="1"
                                                                 Grid.Column="1"
                                                                 HorizontalAlignment="Left"
                                                                 VerticalAlignment="Center"
                                                                 InputType="Decimal"
                                                                 MaxDecimalPlaces="4"
                                                                 Necessity="Mandatory"
                                                                 SelectorUnits="{Binding Path=AmountUnits.Value,Mode=OneWay}"
                                                                 SelectedUnit="{Binding Path=AmountUnit.Value,Mode=TwoWay}"
                                                                 BaseValue="{Binding Path=Amount.Value,Mode=TwoWay,UpdateSourceTrigger=PropertyChanged,ValidatesOnDataErrors=True}"
                                                                 IsReadOnly="{Binding IsReadOnly}" />
                        <controls:LabelControl Grid.Row="1"
                                                   Grid.Column="2"
                                                   HorizontalAlignment="Left"
                                                   VerticalAlignment="Center"
                                                   Margin="20,10,0,0"
                                                   Text="{x:Static resources:LocalizedResources.Consumable_Price}" />
                        <guiControls:ExtendedTextBoxControl x:Name="PriceTextBox"
                                                                BaseCurrency="{Binding Path=MeasurementUnitsAdapter.BaseCurrency, Mode=OneWay}"
                                                                UIUnit="{Binding Path=MeasurementUnitsAdapter.UICurrency, Mode=OneWay}"
                                                                InputType="Money"
                                                                Necessity="Recommended"
                                                                Margin="10,10,0,0"
                                                                Grid.Column="3"
                                                                Grid.Row="1"
                                                                HorizontalAlignment="Left"
                                                                VerticalAlignment="Center"
                                                                BaseValue="{Binding Path=Price.Value,Mode=TwoWay,UpdateSourceTrigger=PropertyChanged,ValidatesOnDataErrors=True,TargetNullValue={x:Static s:String.Empty}}"
                                                                IsReadOnly="{Binding IsReadOnly}" />
                        <controls:LabelControl Grid.Row="2"
                                                   Grid.Column="2"
                                                   Margin="20,10,0,0"
                                                   Text="{x:Static resources:LocalizedResources.General_Cost}"
                                                   VerticalAlignment="Center"
                                                   HorizontalAlignment="Left" />
                        <guiControls:ExtendedLabelControl BaseCurrency="{Binding Path=MeasurementUnitsAdapter.BaseCurrency, Mode=OneWay}"
                                                              UIUnit="{Binding Path=MeasurementUnitsAdapter.UICurrency, Mode=OneWay}"
                                                              HorizontalAlignment="Left"
                                                              VerticalAlignment="Center"
                                                              Margin="10,10,0,0"
                                                              Grid.Column="3"
                                                              Grid.Row="2"
                                                              BaseValue="{Binding ConsumableCost,Mode=OneWay,UpdateSourceTrigger=PropertyChanged}" />
                        <controls:LabelControl Grid.Row="3"
                                                   Margin="0,10,0,0"
                                                   Text="{x:Static resources:LocalizedResources.General_Description}"
                                                   VerticalAlignment="Top"
                                                   HorizontalAlignment="Left" />
                        <controls:TextBoxControl x:Name="DescriptionTextBox"
                                                     Grid.Row="3"
                                                     Grid.Column="1"
                                                     Grid.ColumnSpan="3"
                                                     Margin="10,10,0,0"
                                                     Height="75"
                                                     VerticalAlignment="Top"
                                                     HorizontalAlignment="Stretch"
                                                     IsTextArea="True"
                                                     InputType="Text2000"
                                                     Text="{Binding Path=Description.Value,Mode=TwoWay,UpdateSourceTrigger=PropertyChanged,ValidatesOnDataErrors=True}"
                                                     IsReadOnly="{Binding IsReadOnly}" />
                    </Grid>
                </GroupBox>
            </guiControls:FormTabItem>
            <guiControls:FormTabItem x:Name="ManufacturerTabItem"
                                     Header="{x:Static resources:LocalizedResources.General_Manufacturer}"
                                     InvalidInputMessage="{Binding ManufacturerViewModel.Error}">
                <ContentPresenter Content="{Binding ManufacturerViewModel}" />
            </guiControls:FormTabItem>
        </TabControl>

        <!-- Save/Cancel buttons-->
        <StackPanel Orientation="Horizontal"
                    HorizontalAlignment="Right"
                    Margin="0,10,10,0"
                    Grid.Row="1"
                    Visibility="{Binding Path=IsReadOnly,Converter={StaticResource ReverseBoolVisibilityConverter}}">
            <controls:Button Content="{x:Static resources:LocalizedResources.General_Undo}"
                             Command="{Binding UndoCommand}" />
            <controls:Button x:Name="SaveButton"
                             Content="{x:Static resources:LocalizedResources.General_Save}"
                             Command="{Binding SaveCommand}"
                             Margin="5,0,0,0" />
            <controls:Button x:Name="CancelButton"
                             Content="{x:Static resources:LocalizedResources.General_Cancel}"
                             Command="{Binding CancelCommand}"
                             Margin="5,0,0,0" />
        </StackPanel>
    </Grid>

</UserControl>