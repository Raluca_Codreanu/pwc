﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for ManageReleasedProjectsView.xaml
    /// </summary>
    public partial class ManageReleasedProjectsView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ManageReleasedProjectsView"/> class.
        /// </summary>
        public ManageReleasedProjectsView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handles the MouseRightButtonDown event of the items in the Manage Released Projects Tree.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void ManageReleasedProjectsTreeItem_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            TreeViewItem item = sender as TreeViewItem;
            if (item != null)
            {
                item.IsSelected = true;
                e.Handled = true;
            }
        }
    }
}
