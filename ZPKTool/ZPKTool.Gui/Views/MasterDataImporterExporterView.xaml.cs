﻿using System.Windows.Controls;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for MasterDataImporterExporterView.xaml
    /// </summary>
    public partial class MasterDataImporterExporterView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MasterDataImporterExporterView"/> class.
        /// </summary>
        public MasterDataImporterExporterView()
        {
            InitializeComponent();
        }
    }
}