﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using ZPKTool.Data;
using ZPKTool.Gui.ViewModels;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Returns the command parameters for importing into a materials tree item, depending on the parent part or raw part.
    /// </summary>
    public class MaterialsTreeItemImportCommandParameterSelector : IValueConverter
    {
        /// <summary>
        /// Gets or sets the list of command parameters for assembly process step import.
        /// </summary>
        public IEnumerable<Type> ParentPartImportCommandParameter { get; set; }

        /// <summary>
        /// Gets or sets the list of command parameters for part process step import.
        /// </summary>
        public IEnumerable<Type> ParentRawPartImportCommandParameter { get; set; }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        /// <exception cref="System.InvalidOperationException">The converter ProcessStepTreeItemImportCommandParameterSelector must be used only for ProcessStepTreeItem.</exception>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var materialsItem = value as MaterialsTreeItem;
            if (materialsItem == null)
            {
                throw new InvalidOperationException("The converter MaterialsTreeItemImportCommandParameterSelector must be used only for MaterialsTreeItem.");
            }

            if (materialsItem.Parent == null)
            {
                throw new InvalidOperationException("The parent of the materials item was null.");
            }

            if (materialsItem.Parent.GetType() == typeof(PartTreeItem))
            {
                return ParentPartImportCommandParameter;
            }
            else if (materialsItem.Parent.GetType() == typeof(RawPartTreeItem))
            {
                return ParentRawPartImportCommandParameter;
            }
            else
            {
                throw new InvalidOperationException("The materials item's parent type is not supported.");
            }
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
