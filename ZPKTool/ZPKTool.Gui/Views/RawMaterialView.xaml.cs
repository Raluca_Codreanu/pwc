﻿using System.Windows;
using System.Windows.Controls;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for RawMaterialView.xaml
    /// </summary>    
    public partial class RawMaterialView : UserControl
    {
        #region Attributes

        /// <summary>
        /// The Undo Highlight manager, used to highlight view controls when undo is performed.
        /// </summary>
        private UndoHighlightManager viewManager;

        #endregion Attributes

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="RawMaterialView"/> class.
        /// </summary>
        public RawMaterialView()
        {
            InitializeComponent();
            this.DataContextChanged += this.OnDataContextChanged;
        }

        #endregion Constructor

        #region Event Handlers

        /// <summary>
        /// Called when [data context changed].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="dependencyPropertyChangedEventArgs">The <see cref="DependencyPropertyChangedEventArgs" /> instance containing the event data.</param>
        private void OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var undoableData = dependencyPropertyChangedEventArgs.NewValue as IUndoable;
            if (undoableData != null)
            {
                if (this.IsLoaded)
                {
                    this.viewManager = new UndoHighlightManager(undoableData.UndoManager, this.MainTabControl, this.DataContext);
                }
                else
                {
                    RoutedEventHandler initOnLoad = null;
                    initOnLoad = (s, e) =>
                        {
                            this.viewManager = new UndoHighlightManager(undoableData.UndoManager, this.MainTabControl, this.DataContext);
                            this.Loaded -= initOnLoad;
                        };
                    this.Loaded += initOnLoad;
                }
            }
            else
            {
                this.viewManager = null;
            }
        }

        /// <summary>
        /// Handles the SelectionChanged event of the MainTabControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Controls.SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void MainTabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count <= 0)
            {
                return;
            }

            var selectedTab = e.AddedItems[0] as TabItem;
            if (selectedTab == null)
            {
                return;
            }

            // Focus the 1st text box in the selected tab.
            var tabContent = selectedTab.Content as UIElement;
            if (tabContent != null)
            {
                UIUtils.FocusFirstTextbox(tabContent);
            }
        }

        #endregion Event Handlers
    }
}