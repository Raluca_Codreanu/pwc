﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using ZPKTool.Data;
using ZPKTool.Gui.ViewModels;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Returns the command parameters for importing into a process step tree item, depending on the process step's type: assembly or part process step.
    /// </summary>
    public class ProcessStepTreeItemImportCommandParameterSelector : IValueConverter
    {
        /// <summary>
        /// Gets or sets the list of command parameters for assembly process step import.
        /// </summary>
        public IEnumerable<Type> AssyStepImportCommandParameter { get; set; }

        /// <summary>
        /// Gets or sets the list of command parameters for part process step import.
        /// </summary>
        public IEnumerable<Type> PartStepImportCommandParameter { get; set; }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        /// <exception cref="System.InvalidOperationException">The converter ProcessStepTreeItemImportCommandParameterSelector must be used only for ProcessStepTreeItem.</exception>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var stepItem = value as ProcessStepTreeItem;
            if (stepItem == null)
            {
                throw new InvalidOperationException("The converter ProcessStepTreeItemImportCommandParameterSelector must be used only for ProcessStepTreeItem.");
            }

            if (stepItem.DataObject == null)
            {
                throw new InvalidOperationException("The data object for the step item was null.");
            }

            if (stepItem.DataObject.GetType() == typeof(AssemblyProcessStep))
            {
                return AssyStepImportCommandParameter;
            }
            else if (stepItem.DataObject.GetType() == typeof(PartProcessStep))
            {
                return PartStepImportCommandParameter;
            }
            else
            {
                throw new InvalidOperationException("The step item's type is not supported.");
            }
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
