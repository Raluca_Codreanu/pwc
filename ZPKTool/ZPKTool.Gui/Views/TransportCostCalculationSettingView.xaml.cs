﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for TransportCostCalculationSettingView.xaml
    /// </summary>
    public partial class TransportCostCalculationSettingView : UserControl
    {
        /// <summary>
        /// The Undo Highlight manager, used to highlight view controls when undo is performed.
        /// </summary>
        private UndoHighlightManager viewManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="TransportCostCalculationSettingView"/> class.
        /// </summary>
        public TransportCostCalculationSettingView()
        {
            InitializeComponent();
            this.DataContextChanged += this.OnDataContextChanged;
        }

        /// <summary>
        /// Called when the data context changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="dependencyPropertyChangedEventArgs">The <see cref="DependencyPropertyChangedEventArgs" /> instance containing the event data.</param>
        private void OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var undoableData = dependencyPropertyChangedEventArgs.NewValue as IUndoable;
            if (undoableData != null)
            {
                if (this.IsLoaded)
                {
                    this.viewManager = new UndoHighlightManager(undoableData.UndoManager, this, this.DataContext);
                }
                else
                {
                    RoutedEventHandler initOnLoad = null;
                    initOnLoad = (s, e) =>
                    {
                        this.viewManager = new UndoHighlightManager(undoableData.UndoManager, this, this.DataContext);
                        this.Loaded -= initOnLoad;
                    };
                    this.Loaded += initOnLoad;
                }
            }
            else
            {
                this.viewManager = null;
            }
        }
    }
}
