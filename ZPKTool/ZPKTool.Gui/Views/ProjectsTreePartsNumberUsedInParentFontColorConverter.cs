﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Media;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// This converter is used to set the font color of the label that displays the number of parts used in parent assembly.
    /// This label is placed in the lower right corner of the part and assembly tree items.
    /// </summary>
    public class ProjectsTreePartsNumberUsedInParentFontColorConverter : IValueConverter
    {
        /// <summary>
        /// Converts a Boolean value to SolidColorBrush
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// The color black if the value is True and Red if the value is False 
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            // If the number of parts is zero return Red, otherwise return blue.
            if (value is int)
            {
                int numberOfParts = (int)value;
                if (numberOfParts == 0)
                {
                    return new SolidColorBrush(Colors.Red);
                }
            }

            return new SolidColorBrush(Colors.Black);
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
