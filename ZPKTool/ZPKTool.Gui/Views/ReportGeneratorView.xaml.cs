﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ZPKTool.Gui.ViewModels;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for ReportGeneratorView.xaml
    /// </summary>
    public partial class ReportGeneratorView : ZPKTool.Controls.ZpkToolWindow
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// A value indicating whether the name cell for a row will get the focus and begin editing.
        /// </summary>
        private bool beginEditCell = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportGeneratorView"/> class.
        /// </summary>
        public ReportGeneratorView()
        {
            InitializeComponent();

            var itemCollection = CollectionViewSource.GetDefaultView(ConfigurationsDataGrid.Items) as System.Collections.Specialized.INotifyCollectionChanged;
            if (itemCollection != null)
            {
                itemCollection.CollectionChanged += ItemCollection_CollectionChanged;
            }
        }

        /// <summary>
        /// Handles the CollectionChanged event of the ItemCollection control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Collections.Specialized.NotifyCollectionChangedEventArgs" /> instance containing the event data.</param>
        private void ItemCollection_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add && e.NewItems.Count == 1)
            {
                // If the item collection is changed by adding a new item, set the begin edit cell flag to true.
                beginEditCell = true;
            }
        }

        /// <summary>
        /// Handles the SelectionChanged event of the EntitiesDataGrid control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Controls.SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void ConfigurationsDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                // Bring the selected item into view because the view model has no way of doing it.
                this.ConfigurationsDataGrid.ScrollIntoView(e.AddedItems[0]);

                if (beginEditCell)
                {
                    try
                    {
                        // If the selected item is newly added, get the name cell of it's row.
                        var row = this.ConfigurationsDataGrid.ItemContainerGenerator.ContainerFromItem(e.AddedItems[0]) as DataGridRow;
                        var nameCell = this.ConfigurationsDataGrid.GetCell(row, 0);

                        // Focus on the name cell for the added item and begin edit.
                        nameCell.Focus();
                        this.ConfigurationsDataGrid.BeginEdit();
                    }
                    catch (Exception ex)
                    {
                        log.ErrorException("An error occurred while trying to set focus and begin edit on a cell.", ex);
                    }
                    finally
                    {
                        beginEditCell = false;
                    }
                }
            }
        }

        /// <summary>
        /// Handles the SelectionChanged event of the MainTab control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SelectionChangedEventArgs" /> instance containing the event data.</param>
        private void MainTab_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.MainTab.SelectedItem == this.ConfigurationsTab)
            {
                if (this.ConfigurationsDataGrid.SelectedItem != null)
                {
                    // Scroll into view the selected item when the configurations tab is selected.
                    this.ConfigurationsDataGrid.ScrollIntoView(this.ConfigurationsDataGrid.SelectedItem);
                }
            }
        }
    }
}