﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AvalonDock;
using ZPKTool.Common;
using ZPKTool.Gui.Utils;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for MainView.xaml
    /// </summary>    
    public partial class MainView : UserControl
    {
        /// <summary>
        /// The minimum width of the main document content that does not require horizontal scrolling to be enabled.
        /// When its width drops below this value the horizontal scrolling is set to Auto..
        /// </summary>
        public const double MainDocContentWidthWithoutScroll = 746d;

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The max with of the content presenter that displays the main document's content. It is uses to avoid giving infinite horizontal space to the content.
        /// </summary>
        private double maxContentWidthWithScroll = System.Windows.SystemParameters.PrimaryScreenWidth;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainView"/> class.
        /// </summary>
        public MainView()
        {
            InitializeComponent();

            CommandBinding[] commandBindingCollection = this.Resources["CommandBindingsCollection"] as CommandBinding[];
            if (commandBindingCollection == null)
            {
                throw new InvalidOperationException("Could not get the command binding collection.");
            }

            this.CommandBindings.AddRange(commandBindingCollection);
            this.MainTreeContent.CommandBindings.AddRange(commandBindingCollection);
            this.BookmarksTreeContent.CommandBindings.AddRange(commandBindingCollection);
        }

        /// <summary>
        /// Show tool panels if they are hidden
        /// </summary>
        /// <param name="sender">The object</param>
        /// <param name="e">The Executed Routed Event Arguments</param>
        private void ShowToolPaneCommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var name = string.Empty;
            if (e.Parameter != null)
            {
                name = e.Parameter.ToString();
            }

            var content = this.DockingManager.DockableContents.FirstOrDefault(dc => dc.Name == name);

            if (content == null)
            {
                return;
            }
            else if (content.State == DockableContentState.Hidden)
            {
                content.Show();
            }
            else
            {
                content.Activate();
            }
        }

        /// <summary>
        /// Handles the SizeChanged event of the MainView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SizeChangedEventArgs" /> instance containing the event data.</param>
        private void MainView_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            this.maxContentWidthWithScroll = e.NewSize.Width;
        }

        /// <summary>
        /// Handles the SizeChanged event of the MainDocumentContentScrollViewer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SizeChangedEventArgs" /> instance containing the event data.</param>
        private void MainDocumentContentScrollViewer_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            // The size of the Main Document's scroll viewer has changed -> re-evaluate whether the horizontal scrolling should be enabled 
            // and the re-calculate the max width of its content.
            // This action is not also performed in MainView_SizeChanged because if the MainView size changes the size of MainDocumentContentScrollViewer will also change.
            double newWidth = e.NewSize.Width;

            if (newWidth < MainView.MainDocContentWidthWithoutScroll)
            {
                this.MainDocumentContentScrollViewer.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
                if (this.MainDocumentContentPresenter != null)
                {
                    this.MainDocumentContentPresenter.MaxWidth = this.maxContentWidthWithScroll;
                }
            }
            else
            {
                this.MainDocumentContentScrollViewer.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
                if (this.MainDocumentContentPresenter != null)
                {
                    this.MainDocumentContentPresenter.MaxWidth = double.PositiveInfinity;
                }
            }
        }

        /// <summary>
        /// Handles the ActiveContentChanged event of the DockingManager control.
        /// It sets the focus on the current active content (ex:needed for keyboard navigation in the tree).
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void DockingManager_ActiveContentChanged(object sender, EventArgs e)
        {
            var manager = sender as DockingManager;
            if (manager != null && manager.ActiveContent != null)
            {
                var contentPresenter = manager.ActiveContent.Content as ContentPresenter;
                if (contentPresenter != null && !contentPresenter.IsKeyboardFocusWithin)
                {
                    RoutedEventHandler focusHandler = null;
                    focusHandler = (s1, e1) =>
                    {
                        var childrenCount = VisualTreeHelper.GetChildrenCount(contentPresenter);
                        if (childrenCount > 0)
                        {
                            var content = VisualTreeHelper.GetChild(contentPresenter, 0) as IFocusDispatcher;
                            if (content != null)
                            {
                                content.FocusContent();
                            }
                        }

                        contentPresenter.Loaded -= focusHandler;
                    };

                    if (!contentPresenter.IsLoaded)
                    {
                        contentPresenter.Loaded += focusHandler;
                    }
                    else
                    {
                        focusHandler.Invoke(this, null);
                    }
                }
            }
            else
            {
                UIUtils.FocusElement(this.LayoutRoot);
            }
        }

        /// <summary>
        /// Handles the Loaded event of the <see cref="MainView"/> control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void MainViewControl_Loaded(object sender, RoutedEventArgs e)
        {
            // Set the focus on the main view control
            // This will make the menu commands active
            UIUtils.FocusElement(this.LayoutRoot);
        }
    }
}
