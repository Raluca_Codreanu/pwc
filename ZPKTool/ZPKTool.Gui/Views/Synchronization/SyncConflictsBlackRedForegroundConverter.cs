﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Media;
using ZPKTool.Gui.ViewModels;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Converts a foreground to red if the values are different and to black if the values are equal. 
    /// </summary>
    [ValueConversion(typeof(string), typeof(Brush))]
    public class SyncConflictsBlackRedForegroundConverter : IValueConverter
    {
        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var detailedConflict = value as ConflictDetails;
            if (detailedConflict != null && detailedConflict.CentralColumnValue != null && detailedConflict.LocalColumnValue != null)
            {
                if (detailedConflict.CentralColumnValue.Trim().Equals(detailedConflict.LocalColumnValue.Trim()))
                {
                    return new SolidColorBrush(Colors.Black);
                }
                else
                {
                    return new SolidColorBrush(Colors.Red);
                }
            }

            return new SolidColorBrush(Colors.Red);
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var detailedConflict = value as ConflictDetails;
            if (detailedConflict != null && detailedConflict.CentralColumnValue != null && detailedConflict.LocalColumnValue != null)
            {
                if (detailedConflict.CentralColumnValue.Trim().Equals(detailedConflict.LocalColumnValue.Trim()))
                {
                    return new SolidColorBrush(Colors.Red);
                }
                else
                {
                    return new SolidColorBrush(Colors.Black);
                }
            }

            return new SolidColorBrush(Colors.Black);
        }
    }
}
