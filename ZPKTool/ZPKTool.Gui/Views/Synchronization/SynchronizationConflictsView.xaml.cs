﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for SynchronizationConflictsView.xaml
    /// </summary>
    public partial class SynchronizationConflictsView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SynchronizationConflictsView"/> class.
        /// </summary>
        public SynchronizationConflictsView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handles the PreviewMouseLeftButtonDown event of the cells from the ConflictsGrid control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void ConflictsGridCell_PreviewMouseLeftButtonDown(object sender, RoutedEventArgs e)
        {
            // Enables editing on single click
            var cell = sender as DataGridCell;
            if (cell != null && !cell.IsEditing)
            {
                if (!cell.IsFocused)
                {
                    cell.Focus();
                }

                if (!cell.IsSelected)
                {
                    cell.IsSelected = true;
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the ConflictDetailsButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void ConflictDetailsButton_Click(object sender, RoutedEventArgs e)
        {
            // Popup with conflict details must be placed near the conflict for which is displayed 
            this.ConflictDetailsPopup.PlacementTarget = sender as UIElement;
        }
    }
}
