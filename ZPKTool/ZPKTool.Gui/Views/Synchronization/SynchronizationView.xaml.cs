﻿namespace ZPKTool.Gui.Views
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for SynchronizationView.xaml
    /// </summary>
    public partial class SynchronizationView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SynchronizationView"/> class.
        /// </summary>
        public SynchronizationView()
        {
            InitializeComponent();
        }
    }
}
