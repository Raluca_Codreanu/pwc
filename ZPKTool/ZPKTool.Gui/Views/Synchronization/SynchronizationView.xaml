﻿<UserControl x:Class="ZPKTool.Gui.Views.SynchronizationView"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
             xmlns:i="clr-namespace:System.Windows.Interactivity;assembly=System.Windows.Interactivity"
             xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
             xmlns:mvvmcore="http://zpktool.com/mvvmcore"
             xmlns:resources="clr-namespace:ZPKTool.Gui.Resources"
             xmlns:controls="clr-namespace:ZPKTool.Controls;assembly=ZPKTool.Controls"
             xmlns:converters="clr-namespace:ZPKTool.Gui.Converters"
             mc:Ignorable="d">

    <UserControl.Resources>
        <converters:BoolToVisibilityConverter x:Key="BoolToVisibilityConverter" />
        <converters:InverseBooleanConverter x:Key="BoolInverterConverter" />
    </UserControl.Resources>

    <Grid>
        <Grid x:Name="OfflineContainer"
              Visibility="{Binding Path=IsCentralOffline.Value, Converter={StaticResource BoolToVisibilityConverter}}"
              Background="{DynamicResource ResourceKey=DarkTransparentBackground}"
              Panel.ZIndex="1"
              Grid.ColumnSpan="100"
              Grid.RowSpan="100">
            <Border Background="{DynamicResource BackgroundImageBrush}"
                    HorizontalAlignment="Center"
                    VerticalAlignment="Center"
                    CornerRadius="15"
                    BorderThickness="2"
                    BorderBrush="Black"
                    Width="250"
                    Height="240">
                <StackPanel HorizontalAlignment="Center"
                            VerticalAlignment="Center"
                            Margin="5,5,0,0">
                    <Image x:Name="OfflineImage"
                           Source="{DynamicResource OfflineIcon}"
                           HorizontalAlignment="Stretch"
                           VerticalAlignment="Stretch"
                           Margin="10,10,10,5"
                           Height="90"
                           Width="90" />
                    <controls:LabelControl x:Name="OfflineMessage"
                                           Text="{x:Static resources:LocalizedResources.Synchronization_OfflineMessage}"
                                           TextAlignment="Center"
                                           HorizontalAlignment="Center"
                                           VerticalAlignment="Center"
                                           Margin="0,5,0,0"
                                           FontSize="15"
                                           FontWeight="Bold" />
                    <controls:Button x:Name="CheckAgain"
                                     Content="{x:Static resources:LocalizedResources.Synchronization_CheckAgain}"
                                     Command="{Binding CheckAgainCommand}"
                                     HorizontalAlignment="Center"
                                     VerticalAlignment="Center"
                                     Margin="10,0,0,0"
                                     IsEnabled="True" />
                    <controls:Button x:Name="CloseButtonOfflineState"
                                     Content="{x:Static resources:LocalizedResources.General_Close}"
                                     Command="{Binding CloseCommand}"
                                     HorizontalAlignment="Right"
                                     VerticalAlignment="Bottom"
                                     IsEnabled="True"
                                     Margin="150,30,0,0" />
                </StackPanel>
            </Border>
        </Grid>
        <Grid x:Name="LayoutRoute">
            <Grid.RowDefinitions>
                <RowDefinition Height="Auto" />
                <RowDefinition Height="Auto" />
                <RowDefinition Height="Auto" />
                <RowDefinition Height="*" />
            </Grid.RowDefinitions>

            <GroupBox Header="{x:Static resources:LocalizedResources.Synchronization_OptionsTitle}"
                      BorderBrush="{DynamicResource CommonBackgroundColor}"
                      VerticalAlignment="Top"
                      FontSize="14.667"
                      Margin="10,10,10,0">
                <Grid Margin="5,10">
                    <Grid.RowDefinitions>
                        <RowDefinition Height="Auto" />
                        <RowDefinition Height="Auto" />
                        <RowDefinition Height="Auto" />
                        <RowDefinition Height="Auto" />
                        <RowDefinition Height="Auto" />
                    </Grid.RowDefinitions>
                    <CheckBox x:Name="StaticDataAndSettingsCheckBox"
                              Content="{x:Static resources:LocalizedResources.Synchronization_StaticDataAndSettingsOption}"
                              IsChecked="{Binding SynchronizeStaticDataAndSettings.Value}"
                              IsEnabled="{Binding Path=IsSyncInProgress.Value, Converter={StaticResource BoolInverterConverter}}"
                              HorizontalAlignment="Left"
                              VerticalAlignment="Top"
                              FontSize="13.333" />
                    <CheckBox x:Name="ReleasedProjectsCheckBox"
                              Content="{x:Static resources:LocalizedResources.General_ReleasedProjects}"
                              IsChecked="{Binding SynchronizeReleasedProjects.Value}"
                              IsEnabled="{Binding Path=IsSyncInProgress.Value, Converter={StaticResource BoolInverterConverter}}"
                              HorizontalAlignment="Left"
                              VerticalAlignment="Top"
                              FontSize="13.333"
                              Grid.Row="1"
                              Margin="0,5,0,0" />
                    <CheckBox x:Name="MasterDataCheckBox"
                              Content="{x:Static resources:LocalizedResources.General_IsMasterData}"
                              IsChecked="{Binding SynchronizeMasterData.Value}"
                              IsEnabled="{Binding Path=IsSyncInProgress.Value, Converter={StaticResource BoolInverterConverter}}"
                              Visibility="{Binding Path=SynchronizeMasterDataAvailability.Value, Converter={StaticResource BoolToVisibilityConverter}}"
                              HorizontalAlignment="Left"
                              VerticalAlignment="Top"
                              Grid.Row="2"
                              Margin="0,5,0,0"
                              FontSize="13.333" />
                    <CheckBox x:Name="MyProjectsCheckBox"
                              Content="{x:Static resources:LocalizedResources.General_MyProjects}"
                              IsChecked="{Binding SynchronizeMyProjects.Value}"
                              IsEnabled="{Binding Path=IsSyncInProgress.Value, Converter={StaticResource BoolInverterConverter}}"
                              Visibility="{Binding SynchronizeMyProjectsAvailability.Value, Converter={StaticResource BoolToVisibilityConverter}}"
                              HorizontalAlignment="Left"
                              VerticalAlignment="Top"
                              Grid.Row="3"
                              Margin="0,5,0,0"
                              FontSize="13.333" />
                    <StackPanel Orientation="Horizontal"
                                VerticalAlignment="Bottom"
                                Margin="0,10,0,0"
                                Grid.Row="4">
                        <controls:Button x:Name="SynchronizeButton"
                                         Content="{x:Static resources:LocalizedResources.Synchronization_SynchButtonLabel}"
                                         Command="{Binding SynchronizationCommand}"
                                         IsEnabled="{Binding Path=IsSyncInProgress.Value, Converter={StaticResource BoolInverterConverter}}"
                                         HorizontalAlignment="Left" />
                        <controls:Button x:Name="AbortButton"
                                         Content="{Binding AbortMessage.Value}"
                                         Command="{Binding AbortCommand}"
                                         IsEnabled="{Binding AbortAvailability.Value}"
                                         HorizontalAlignment="Left"
                                         Margin="10,0,0,0" />
                    </StackPanel>
                </Grid>
            </GroupBox>

            <GroupBox Header="{x:Static resources:LocalizedResources.Synchronization_ProgressTitle}"
                      BorderBrush="{DynamicResource CommonBackgroundColor}"
                      VerticalAlignment="Top"
                      FontSize="14.667"
                      Grid.Row="1"
                      Margin="10,5,10,0">
                <Grid Margin="5">
                    <Grid.RowDefinitions>
                        <RowDefinition Height="Auto" />
                        <RowDefinition Height="Auto" />
                        <RowDefinition Height="Auto" />
                        <RowDefinition Height="Auto" />
                    </Grid.RowDefinitions>
                    <StackPanel x:Name="CurrentTaskContainer"
                                HorizontalAlignment="Left"
                                VerticalAlignment="Top"
                                Orientation="Horizontal">
                        <controls:LabelControl x:Name="CurrentTaskLabel"
                                               Text="{Binding CurrentTask.Value}"
                                               FontSize="13.333"
                                               HorizontalAlignment="Left"
                                               VerticalAlignment="Center"
                                               Grid.Row="0"
                                               TextWrapping="Wrap" />
                        <controls:LabelControl x:Name="CurrentTaskStatusLabel"
                                               Text="{Binding CurrentTaskStatus.Value}"
                                               FontSize="13.333"
                                               HorizontalAlignment="Left"
                                               VerticalAlignment="Center"
                                               Grid.Row="0"
                                               TextWrapping="Wrap" />
                    </StackPanel>
                    <ProgressBar x:Name="CurrentProgress"
                                 Value="{Binding CurrentProgress.Value}"
                                 IsIndeterminate="{Binding CurrentProgressIsIndeterminate.Value}"
                                 Maximum="{Binding CurrentProgressMaximum.Value}"
                                 VerticalAlignment="Top"
                                 Height="22"
                                 Grid.Row="1" />
                    <Grid Grid.Row="2"
                          Margin="0,15,0,0">
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width="Auto"></ColumnDefinition>
                            <ColumnDefinition></ColumnDefinition>
                        </Grid.ColumnDefinitions>
                        <controls:LabelControl Text="{x:Static resources:LocalizedResources.Synchronization_TotalProgress}"
                                               FontSize="13.333"
                                               HorizontalAlignment="Left"
                                               VerticalAlignment="Top"
                                               Grid.Column="0" />
                        <controls:LabelControl  x:Name="SynchronizationDurationLabel"
                                                Text="{Binding SynchronizationDuration.Value}"
                                                Visibility="{Binding Path=SynchronizationDurationVisibility.Value, Converter={StaticResource BoolToVisibilityConverter}}"
                                                Grid.Column="1"
                                                Margin="5,0,0,0"
                                                FontSize="12"
                                                HorizontalAlignment="Left"
                                                VerticalAlignment="Center" />
                    </Grid>
                    <ProgressBar x:Name="TotalProgress"
                                 Value="{Binding TotalProgress.Value}"
                                 Maximum="{Binding TotalProgressMaximum.Value}"
                                 VerticalAlignment="Top"
                                 Height="22"
                                 Grid.Row="3" />
                </Grid>
            </GroupBox>

            <GroupBox x:Name="SynchSummaryGroupBox"
                      Header="{x:Static resources:LocalizedResources.Synchronization_SummaryTitle}"
                      Visibility="{Binding Path=DisplaySyncSummary.Value, Converter={StaticResource BoolToVisibilityConverter}}"
                      BorderBrush="{DynamicResource CommonBackgroundColor}"
                      VerticalAlignment="Top"
                      FontSize="14.667"
                      Grid.Row="2"
                      Margin="10,5,10,0">
                <Grid Margin="5,5,5,0">
                    <Grid.RowDefinitions>
                        <RowDefinition Height="Auto" />
                        <RowDefinition Height="Auto" />
                        <RowDefinition Height="Auto" />
                        <RowDefinition Height="Auto" />
                    </Grid.RowDefinitions>
                    <StackPanel HorizontalAlignment="Left"
                                VerticalAlignment="Bottom"
                                Orientation="Horizontal">
                        <controls:LabelControl Text="{x:Static resources:LocalizedResources.Synchronization_TotalItems}"
                                               FontSize="12"
                                               HorizontalAlignment="Left"
                                               VerticalAlignment="Top"
                                               Margin="0" />
                        <controls:LabelControl x:Name="SummaryTotalItemsLabel"
                                               Text="{Binding SummaryTotalItems.Value}"
                                               FontSize="12"
                                               HorizontalAlignment="Left"
                                               VerticalAlignment="Top"
                                               Margin="5,0,0,0" />
                    </StackPanel>
                    <StackPanel HorizontalAlignment="Left"
                                VerticalAlignment="Top"
                                Grid.Row="1"
                                Orientation="Horizontal">
                        <controls:LabelControl Text="{x:Static resources:LocalizedResources.Synchronization_SuccessfulItems}"
                                               FontSize="12"
                                               Foreground="#FF07D220"
                                               Margin="0" />
                        <controls:LabelControl x:Name="SummarySuccessesLabel"
                                               Text="{Binding SummarySuccessesCount.Value}"
                                               FontSize="12"
                                               Foreground="#FF07D220"
                                               Margin="5,0,0,0" />
                    </StackPanel>
                    <StackPanel HorizontalAlignment="Left"
                                VerticalAlignment="Top"
                                Grid.Row="2"
                                Orientation="Horizontal">
                        <controls:LabelControl Text="{x:Static resources:LocalizedResources.Synchronization_Conflicts}"
                                               FontSize="12"
                                               Foreground="#FFFF6A00"
                                               HorizontalAlignment="Left"
                                               VerticalAlignment="Center"
                                               Margin="0" />
                        <controls:LabelControl x:Name="SummaryConflictsLabel"
                                               Text="{Binding SummaryConflictsCount.Value}"
                                               FontSize="12"
                                               Foreground="#FFFF6A00"
                                               HorizontalAlignment="Left"
                                               VerticalAlignment="Center"
                                               Margin="5,0,0,0" />
                        <controls:AnimatedImageButton x:Name="ViewConflicts"
                                                      ToolTip="{x:Static resources:LocalizedResources.Synchronization_HandleConflicts}"
                                                      Command="{Binding ViewConflictsCommand}"
                                                      IsEnabled="{Binding IsAnyConflict.Value}"
                                                      ImageSource="{DynamicResource EditIcon}"
                                                      Margin="5,0,0,0"
                                                      Width="18"
                                                      Height="18" />
                    </StackPanel>
                </Grid>
            </GroupBox>
            <StackPanel Orientation="Horizontal"
                        VerticalAlignment="Bottom"
                        HorizontalAlignment="Right"
                        Margin="10,10,10,10"
                        Grid.Row="3">
                <controls:Button x:Name="RunInBackgroundButton"
                                 Content="{x:Static resources:LocalizedResources.General_RunInBackground}"
                                 Command="{Binding RunInBackgroundCommand}"
                                 Visibility="{Binding IsSyncInProgress.Value, Converter={StaticResource BoolToVisibilityConverter}}"
                                 IsEnabled="{Binding Path=IsSyncInProgress.Value}" />
                <controls:Button x:Name="CloseButton"
                                 Content="{x:Static resources:LocalizedResources.General_Close}"
                                 Command="{Binding CloseCommand}"
                                 IsEnabled="{Binding Path=IsSyncInProgress.Value, Converter={StaticResource BoolInverterConverter}}"
                                 Margin="10,0,0,0" />
            </StackPanel>
        </Grid>
    </Grid>
</UserControl>
