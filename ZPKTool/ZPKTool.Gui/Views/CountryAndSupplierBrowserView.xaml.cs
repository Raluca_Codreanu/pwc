﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ZPKTool.MvvmCore;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for MasterDataCountryBrowserView.xaml
    /// </summary>
    public partial class CountryAndSupplierBrowserView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CountryAndSupplierBrowserView"/> class.
        /// </summary>
        public CountryAndSupplierBrowserView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handles the Loaded event of the UserControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            this.MasterDataTree.Focus();
        }
    }
}
