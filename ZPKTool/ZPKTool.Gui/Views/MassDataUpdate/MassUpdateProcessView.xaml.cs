﻿using System.Windows.Controls;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for MassUpdateProcessView.xaml
    /// </summary>
    public partial class MassUpdateProcessView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MassUpdateProcessView"/> class.
        /// </summary>
        public MassUpdateProcessView()
        {
            InitializeComponent();
        }
    }
}
