﻿namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for MassUpdateGeneralView.xaml
    /// </summary>
    public partial class MassUpdateGeneralView
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MassUpdateGeneralView"/> class.
        /// </summary>
        public MassUpdateGeneralView()
        {
            InitializeComponent();
        }
    }
}
