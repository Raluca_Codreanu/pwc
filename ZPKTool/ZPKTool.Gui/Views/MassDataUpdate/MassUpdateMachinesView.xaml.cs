﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for MassUpdateMachinesView.xaml
    /// </summary>
    public partial class MassUpdateMachinesView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MassUpdateMachinesView" /> class.
        /// </summary>
        public MassUpdateMachinesView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handles the Loaded event of the MassUpdateMachines control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void MassUpdateMachines_Loaded(object sender, RoutedEventArgs e)
        {
            // Hide the date picker's textbox because we'll use our own text box to display just the selected year.
            TextBox tb = this.ManufacturingYearPicker.Template.FindName("PART_TextBox", this.ManufacturingYearPicker) as TextBox;
            if (tb != null)
            {
                tb.Visibility = Visibility.Collapsed;
            }
        }
    }
}
