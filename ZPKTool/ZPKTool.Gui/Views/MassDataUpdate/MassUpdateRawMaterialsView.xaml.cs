﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for MassUpdateRawMaterialsView.xaml
    /// </summary>
    public partial class MassUpdateRawMaterialsView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MassUpdateRawMaterialsView" /> class.
        /// </summary>
        public MassUpdateRawMaterialsView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Selects all click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void SelectAllClick(object sender, RoutedEventArgs e)
        {
            RawMaterialsListBox.SelectAll();
        }

        /// <summary>
        /// Deselects all click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void DeselectAllClick(object sender, RoutedEventArgs e)
        {
            RawMaterialsListBox.UnselectAll();
        }
    }
}
