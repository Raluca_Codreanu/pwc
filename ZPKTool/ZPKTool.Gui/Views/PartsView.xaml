﻿<UserControl x:Class="ZPKTool.Gui.Views.PartsView"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
             xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
             xmlns:i="clr-namespace:System.Windows.Interactivity;assembly=System.Windows.Interactivity"
             xmlns:mvvmcore="http://zpktool.com/mvvmcore"
             xmlns:controls="clr-namespace:ZPKTool.Controls;assembly=ZPKTool.Controls"
             xmlns:guiControls="clr-namespace:ZPKTool.Gui.Controls"
             xmlns:resources="clr-namespace:ZPKTool.Gui.Resources"
             xmlns:converters="clr-namespace:ZPKTool.Gui.Converters"
             xmlns:data="clr-namespace:ZPKTool.Data;assembly=Data"
             xmlns:business="clr-namespace:ZPKTool.Business;assembly=ZPKTool.Business"
             xmlns:views="clr-namespace:ZPKTool.Gui.Views"
             xmlns:behaviors="clr-namespace:ZPKTool.Gui.Behaviors"
             mc:Ignorable="d">
    <i:Interaction.Triggers>
        <mvvmcore:WindowServiceTrigger WindowService="{Binding WindowService}" />
    </i:Interaction.Triggers>

    <UserControl.Resources>
        <converters:BoolToVisibilityConverter x:Key="BoolVisibilityConverter" />
        <converters:EnumToStringConverter x:Key="EnumToStringConverter" />
        <converters:BoolToVisibilityConverter x:Key="InvertedBoolVisibilityConverter"
                                              Inverted="True" />

        <!-- ## Command references for command bindings -->
        <mvvmcore:CommandReference x:Key="AddCommand"
                                   Command="{Binding AddCommand}" />
        <mvvmcore:CommandReference x:Key="EditCommand"
                                   Command="{Binding EditCommand}" />
        <mvvmcore:CommandReference x:Key="ImportCommand"
                                   Command="{Binding ImportCommand}" />
        <mvvmcore:CommandReference x:Key="ExportCommand"
                                   Command="{Binding ExportCommand}" />

        <mvvmcore:BindingProxy x:Key="DataContextProxy"
                               Data="{Binding}" />

        <!-- Checks if the current user has the right to edit master data -->
        <ObjectDataProvider x:Key="EditMasterDataAccessCheck"
                            ObjectInstance="{x:Static business:SecurityManager.Instance}"
                            MethodName="CurrentUserHasRight">
            <ObjectDataProvider.MethodParameters>
                <data:Right>EditMasterData</data:Right>
            </ObjectDataProvider.MethodParameters>
        </ObjectDataProvider>

        <ContextMenu x:Key="DataGridRowContextMenu">
            <MenuItem Header="{x:Static resources:LocalizedResources.General_Edit}"
                      Command="{StaticResource EditCommand}">
                <MenuItem.Icon>
                    <Image Source="{DynamicResource EditIcon}" />
                </MenuItem.Icon>
            </MenuItem>
            <MenuItem Header="{x:Static resources:LocalizedResources.General_Copy}"
                      Command="{x:Static views:MainViewCommands.Copy}">
                <MenuItem.Icon>
                    <Image Source="{DynamicResource CopyIcon}" />
                </MenuItem.Icon>
            </MenuItem>
            <MenuItem Header="{x:Static resources:LocalizedResources.General_Delete}"
                      Command="{x:Static views:MainViewCommands.Delete}">
                <MenuItem.Icon>
                    <Image Source="{DynamicResource DeleteIcon}" />
                </MenuItem.Icon>
            </MenuItem>
            <Separator />

            <MenuItem Header="{x:Static resources:LocalizedResources.General_Export}"
                      Command="{x:Static views:MainViewCommands.Export}">
                <MenuItem.Icon>
                    <Image Source="{DynamicResource ExportIcon}" />
                </MenuItem.Icon>
            </MenuItem>
            <Separator Visibility="{Binding Source={StaticResource EditMasterDataAccessCheck},Converter={StaticResource BoolVisibilityConverter}}" />
            <MenuItem Header="{x:Static resources:LocalizedResources.General_CopyToMasterData}"
                      Command="{x:Static views:MainViewCommands.CopyToMasterData}"
                      CommandParameter="{Binding Path=Data.SelectedEntitiesInfo, Source={StaticResource DataContextProxy}}"
                      Visibility="{Binding Source={StaticResource EditMasterDataAccessCheck},Converter={StaticResource BoolVisibilityConverter}}" />
        </ContextMenu>

        <!-- The style for data grid rows that adds the above context menu to them. -->
        <Style x:Key="DataGridRowStyle"
               TargetType="{x:Type DataGridRow}"
               BasedOn="{StaticResource {x:Type DataGridRow}}">
            <Setter Property="ContextMenu"
                    Value="{StaticResource DataGridRowContextMenu}" />
            <Setter Property="ContextMenu.DataContext"
                    Value="{Binding}" />
        </Style>

        <!--## The context menus of data grids applied when not clicking a row -->
        <ContextMenu x:Key="PartsDataGridContextMenu">
            <MenuItem Header="{x:Static resources:LocalizedResources.General_CreatePart}"
                      Command="{x:Static views:MainViewCommands.CreatePart}"
                      CommandParameter="{x:Type data:Part}">
                <MenuItem.Icon>
                    <Image Source="{DynamicResource PartIcon}" />
                </MenuItem.Icon>
            </MenuItem>
            <MenuItem Header="{x:Static resources:LocalizedResources.General_Paste}"
                      Command="{x:Static views:MainViewCommands.Paste}"
                      CommandParameter="{x:Type data:Part}">
                <MenuItem.Icon>
                    <Image Source="{DynamicResource PasteIcon}" />
                </MenuItem.Icon>
            </MenuItem>
            <MenuItem Header="{x:Static resources:LocalizedResources.General_Import}"
                      Command="{x:Static views:MainViewCommands.Import}"
                      CommandParameter="{x:Type data:Part}">
                <MenuItem.Icon>
                    <Image Source="{DynamicResource ImportIcon}" />
                </MenuItem.Icon>
            </MenuItem>
        </ContextMenu>

    </UserControl.Resources>

    <UserControl.CommandBindings>
        <mvvmcore:DelegateCommandBinding Command="{x:Static views:MainViewCommands.CreatePart}"
                                         CommandReference="{StaticResource AddCommand}" />
        <mvvmcore:DelegateCommandBinding Command="{x:Static views:MainViewCommands.Import}"
                                         CommandReference="{StaticResource ImportCommand}" />
        <mvvmcore:DelegateCommandBinding Command="{x:Static views:MainViewCommands.Export}"
                                         CommandReference="{StaticResource ExportCommand}" />
    </UserControl.CommandBindings>

    <Grid x:Name="LayoutRoot">
        <Grid.RowDefinitions>
            <RowDefinition Height="Auto" />
            <RowDefinition Height="Auto" />
            <RowDefinition Height="*" />
        </Grid.RowDefinitions>

        <guiControls:ExtendedDataGrid x:Name="Parts"
                                      Height="300"
                                      IsReadOnly="True"
                                      AllowDrop="True"
                                      AutoGenerateColumns="False"
                                      SelectionMode="Extended"
                                      SelectedItems="{Binding SelectedItems, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged}"
                                      ItemsSource="{Binding Parts}"
                                      EnableExporting="True"
                                      SelectionChanged="EntitiesDataGrid_SelectionChanged"
                                      FilterMode="Contains"
                                      AdditionalReportInformation="{Binding AdditionalReportInformation}"
                                      RowStyle="{StaticResource DataGridRowStyle}"
                                      ContextMenu="{StaticResource PartsDataGridContextMenu}"
                                      behaviors:DataGridBehavior.CustomSortEnabled="True"
                                      behaviors:DataGridBehavior.PersistLayout="True"
                                      behaviors:DataGridBehavior.PersistId="24DE3E41-3A6F-4B54-A2A2-2E21B86983BC">

            <guiControls:ExtendedDataGrid.Resources>
                <mvvmcore:CommandReference x:Key="AddCommand"
                                           Command="{Binding AddCommand}" />
                <mvvmcore:CommandReference x:Key="CopyCommand"
                                           Command="{Binding CopyCommand}" />
                <mvvmcore:CommandReference x:Key="PasteCommand"
                                           Command="{Binding PasteCommand}" />
                <mvvmcore:CommandReference x:Key="DeleteCommand"
                                           Command="{Binding DeleteCommand}" />
            </guiControls:ExtendedDataGrid.Resources>

            <guiControls:ExtendedDataGrid.CommandBindings>
                <mvvmcore:DelegateCommandBinding Command="{x:Static views:MainViewCommands.CreatePart}"
                                                 CommandReference="{StaticResource AddCommand}" />
                <mvvmcore:DelegateCommandBinding Command="{x:Static views:MainViewCommands.Copy}"
                                                 CommandReference="{StaticResource CopyCommand}" />
                <mvvmcore:DelegateCommandBinding Command="{x:Static views:MainViewCommands.Paste}"
                                                 CommandReference="{StaticResource PasteCommand}" />
                <mvvmcore:DelegateCommandBinding Command="{x:Static views:MainViewCommands.Delete}"
                                                 CommandReference="{StaticResource DeleteCommand}" />
            </guiControls:ExtendedDataGrid.CommandBindings>

            <i:Interaction.Triggers>
                <i:EventTrigger EventName="MouseDoubleClick">
                    <mvvmcore:EventToCommand Command="{Binding MouseDoubleClickCommand}"
                                             PassEventArgsToCommand="True" />
                </i:EventTrigger>

                <i:EventTrigger EventName="DragEnter">
                    <mvvmcore:EventToCommand Command="{Binding DragEnterCommand}"
                                             PassEventArgsToCommand="True" />
                </i:EventTrigger>

                <i:EventTrigger EventName="DragOver">
                    <mvvmcore:EventToCommand Command="{Binding DragOverCommand}"
                                             PassEventArgsToCommand="True" />
                </i:EventTrigger>

                <i:EventTrigger EventName="Drop">
                    <mvvmcore:EventToCommand Command="{Binding DropCommand}"
                                             PassEventArgsToCommand="True" />
                </i:EventTrigger>

            </i:Interaction.Triggers>
            <guiControls:ExtendedDataGrid.Columns>
                <DataGridTextColumn Width="200"
                                    Header="{x:Static resources:LocalizedResources.General_Name}"
                                    Binding="{Binding Path=Part.Name}"
                                    behaviors:DataGridBehavior.PersistId="B72B9CB1-25FD-4580-AE1F-71F25F9482E8" />
                <DataGridTextColumn Width="100"
                                    Header="{x:Static resources:LocalizedResources.General_Number}"
                                    Binding="{Binding Path=Part.Number}"
                                    behaviors:DataGridBehavior.PersistId="5DC79BC4-8606-4EDC-A40E-B9649FB7F18A" />
                <DataGridTextColumn Width="100"
                                    Header="{x:Static resources:LocalizedResources.General_Status}"
                                    Binding="{Binding Path=Part.CalculationStatus, Converter={StaticResource EnumToStringConverter}, ConverterParameter={x:Type data:PartCalculationStatus}}"
                                    behaviors:DataGridBehavior.PersistId="A8926231-40F3-4BD9-B6D1-57DA00F6D578" />
                <DataGridTextColumn Width="100"
                                    Header="{x:Static resources:LocalizedResources.General_Calculator}"
                                    Binding="{Binding Path=Part.CalculatorUser.Name}"
                                    behaviors:DataGridBehavior.PersistId="C260FA57-99EC-471D-B7A9-CB9227555E19" />
                <DataGridTextColumn Width="100"
                                    Header="{x:Static resources:LocalizedResources.General_Manufacturer}"
                                    Binding="{Binding Path=Part.Manufacturer.Name}"
                                    behaviors:DataGridBehavior.PersistId="7ED2B09F-7BFC-427E-9408-ECD341332A78" />

                <DataGridTemplateColumn Width="100"
                                        SortMemberPath="PartCost"
                                        Header="{x:Static resources:LocalizedResources.General_Cost}"
                                        behaviors:DataGridBehavior.PersistId="BD7A50B1-68A9-4E7C-AA20-E37C9E4AF455"
                                        Visibility="{Binding Data.ParentAssembly.IsMasterData,Source={StaticResource DataContextProxy},Converter={StaticResource InvertedBoolVisibilityConverter}}">
                    <DataGridTemplateColumn.CellTemplate>
                        <DataTemplate>
                            <guiControls:ExtendedLabelControl BaseCurrency="{Binding Path=Data.MeasurementUnitsAdapter.BaseCurrency, Mode=OneWay, Source={StaticResource DataContextProxy}}"
                                                              UIUnit="{Binding Path=Data.MeasurementUnitsAdapter.UICurrency, Mode=OneWay, Source={StaticResource DataContextProxy}}"
                                                              BaseValue="{Binding Path=PartCost}" />

                        </DataTemplate>
                    </DataGridTemplateColumn.CellTemplate>
                </DataGridTemplateColumn>
            </guiControls:ExtendedDataGrid.Columns>
        </guiControls:ExtendedDataGrid>

        <StackPanel Grid.Row="1"
                    HorizontalAlignment="Left"
                    Margin="5"
                    Orientation="Horizontal">
            <controls:Button x:Name="AddButton"
                             Content="{x:Static resources:LocalizedResources.General_Add}"
                             VerticalAlignment="Top"
                             Visibility="Visible"
                             Margin="0,0,0,0"
                             Command="{Binding AddCommand}" />
            <controls:Button x:Name="EditButton"
                             Content="{x:Static resources:LocalizedResources.General_Edit}"
                             VerticalAlignment="Top"
                             Visibility="Visible"
                             Margin="10,0,0,0"
                             Command="{Binding EditCommand}" />
            <controls:Button x:Name="DeleteButton"
                             Content="{x:Static resources:LocalizedResources.General_Delete}"
                             VerticalAlignment="Top"
                             Visibility="Visible"
                             Margin="10,0,0,0"
                             Command="{Binding DeleteCommand}" />
        </StackPanel>

        <ScrollViewer Grid.Row="2"
                      HorizontalScrollBarVisibility="Disabled"
                      VerticalScrollBarVisibility="Auto"
                      Margin="0,5,0,0">
            <ContentPresenter Content="{Binding PartDetails}" />
        </ScrollViewer>
    </Grid>
</UserControl>
