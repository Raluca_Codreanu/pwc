﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for ProcessBreakdownView.xaml
    /// </summary>
    public partial class ProcessBreakdownView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessBreakdownView"/> class.
        /// </summary>
        public ProcessBreakdownView()
        {
            InitializeComponent();
        }
    }
}
