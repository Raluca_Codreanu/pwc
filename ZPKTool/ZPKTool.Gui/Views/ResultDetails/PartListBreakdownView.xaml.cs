﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Utils;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for PartListBreakdownView.xaml
    /// </summary>
    public partial class PartListBreakdownView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PartListBreakdownView"/> class.
        /// </summary>
        public PartListBreakdownView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handles the Click event of the DetailsButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void DetailsButton_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            if (button == null)
            {
                return;
            }

            var depObj = sender as DependencyObject;
            if (depObj == null)
            {
                return;
            }

            DataGridRow parentRow = UIHelper.FindParent<DataGridRow>(depObj);
            if (parentRow == null)
            {
                return;
            }

            this.PartListDataGrid.RowDetailsTemplate = this.Resources["RowDetailsTemplate"] as DataTemplate;
            ScrollViewer.SetCanContentScroll(this.PartListDataGrid, false);

            var buttonImage = button.Content as Image;
            if (buttonImage != null)
            {
                if (parentRow.DetailsVisibility != Visibility.Visible)
                {
                    buttonImage.SetResourceReference(Image.SourceProperty, Images.UpIconKey);
                    parentRow.DetailsVisibility = Visibility.Visible;
                }
                else
                {
                    buttonImage.SetResourceReference(Image.SourceProperty, Images.DownIconKey);
                    parentRow.DetailsVisibility = Visibility.Collapsed;
                }
            }
        }

        /// <summary>
        /// Handles the PreviewMouseDoubleClick event of the TreeView control, to prevent the MouseDoubleClick event bubbling from the source (tree view) to its parent (data grid).
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        /// <returns>The parameter of event (selected item from tree).</returns>
        private object TreeView_PreviewMouseDoubleClick(object sender, RoutedEventArgs e)
        {
            // Prevent the MouseDoubleClick event to be handled by the parent (data grid)
            e.Handled = true;

            var treeView = e.Source as TreeViewEx;
            if (treeView != null)
            {
                return treeView.SelectedItem;
            }

            return null;
        }
    }
}
