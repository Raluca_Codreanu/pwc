﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for ResultDetailsView.xaml
    /// </summary>
    public partial class ResultDetailsView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ResultDetailsView"/> class.
        /// </summary>
        public ResultDetailsView()
        {
            InitializeComponent();
            foreach (TabItem tab in this.BreakdownsTabControl.Items)
            {
                tab.IsVisibleChanged += TabVisibilityChanged;
            }
        }

        /// <summary>
        /// Handles the tab visibility changed event.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private void TabVisibilityChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var tabItem = sender as TabItem;
            if (tabItem != null && tabItem.Visibility != Visibility.Visible && this.BreakdownsTabControl.SelectedItem == tabItem)
            {
                this.BreakdownsTabControl.SelectedIndex = 0;
            }
        }
    }
}
