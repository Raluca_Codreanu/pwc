﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Media;
using ZPKTool.Data;

namespace ZPKTool.Gui.Views.ResultDetails
{
    /// <summary>
    /// Converts an int (which represents the AmountPerAssembly ) to a foreground color. 
    /// If the value needed to be converted is 0, the foreground for the item containing that amount will be red; else, the foreground is black.
    /// </summary>  
    public class PartListAmountToForegroundConverter : IValueConverter
    {
        #region IValueConverter Members

        /// <summary>
        /// Receives an int parameter which represents the AmountPerAssembly and returns the Foreground for the item containing that amount.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Brush foreground = Brushes.Black;

            var amount = value as int?;
            if (amount == null)
            {
                return null;
            }
            else if (amount == 0)
            {
                foreground = Brushes.Red;
            }

            return foreground;
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion IValueConverter Members
    }
}
