﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using ZPKTool.Data;

namespace ZPKTool.Gui.Views.ResultDetails
{
    /// <summary>
    /// Converts an entity type (AssembledPartCost, AssemblyCost, Assembly, Part) to Visibility type (Visible, Hidden or Collapsed). 
    /// If the value needed to be converted is Assembly or AssemblyCost the visibility is set Visible, else to Hidden.
    /// </summary>  
    public class PartListEntityToVisibilityConverter : IValueConverter
    {
        #region IValueConverter Members

        /// <summary>
        /// Receives an Object parameter and returns the Visibility associated with it.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Visibility visibility = Visibility.Hidden;
            var assy = value as Assembly;
            if (assy != null)
            {
                if (assy.Subassemblies.Where(a => !a.IsDeleted).Count() > 0
                    || assy.Parts.Where(p => !p.IsDeleted).Count() > 0)
                {
                    visibility = Visibility.Visible;
                }
            }

            return visibility;
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
