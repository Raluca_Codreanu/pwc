﻿<UserControl x:Class="ZPKTool.Gui.Views.InvestmentBreakdownView"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
             xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
             mc:Ignorable="d"
             xmlns:guiControls="clr-namespace:ZPKTool.Gui.Controls"
             xmlns:resources="clr-namespace:ZPKTool.Gui.Resources"
             xmlns:behaviors="clr-namespace:ZPKTool.Gui.Behaviors"
             xmlns:converters="clr-namespace:ZPKTool.Gui.Converters"
             xmlns:i="clr-namespace:System.Windows.Interactivity;assembly=System.Windows.Interactivity"
             xmlns:mvvmcore="http://zpktool.com/mvvmcore"
             xmlns:controls="clr-namespace:ZPKTool.Controls;assembly=ZPKTool.Controls">
    <UserControl.Resources>
        <converters:BoolToVisibilityConverter x:Key="BoolToVisibilityConverter" />
        <mvvmcore:BindingProxy x:Key="DataContextProxy"
                               Data="{Binding}" />
    </UserControl.Resources>
    
    <Grid>
        <Grid.RowDefinitions>
            <RowDefinition Height="*" />
            <RowDefinition Height="*" />
        </Grid.RowDefinitions>
        <guiControls:Chart x:Name="InvestmentChart"
                           VerticalAlignment="Stretch"
                           ChartMode="Both"
                           CurrentChartMode="{Binding Path=DataContext.ChartMode.Value, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged, RelativeSource={RelativeSource Mode=FindAncestor, AncestorType={x:Type UserControl}}}"
                           AxesMaxDigits="0"
                           BreakdownName="{x:Static resources:LocalizedResources.General_Investment}"
                           Entity="{Binding Path=DataContext.Entity, RelativeSource={RelativeSource Mode=FindAncestor, AncestorType={x:Type UserControl}}}"
                           ItemsSource="{Binding Path=DataContext.ChartItems.Value, RelativeSource={RelativeSource Mode=FindAncestor, AncestorType={x:Type UserControl}}}"
                           MeasurementUnitsAdapter="{Binding Path=DataContext.MeasurementUnitsAdapter, RelativeSource={RelativeSource Mode=FindAncestor, AncestorType={x:Type UserControl}}}"
                           ChartUiUnit="{Binding Path=DataContext.MeasurementUnitsAdapter.UICurrency, RelativeSource={RelativeSource Mode=FindAncestor, AncestorType={x:Type UserControl}}}"
                           TotalValue="{Binding Path=DataContext.TotalCost.Value, RelativeSource={RelativeSource Mode=FindAncestor, AncestorType={x:Type UserControl}}}"
                           TotalValueLabel="{x:Static resources:LocalizedResources.ResultDetails_TotalInvestment}" />
        
        <guiControls:ExtendedDataGrid x:Name="InvestmentDataGrid"
                                      IsReadOnly="True"
                                      ItemsSource="{Binding Path=DataGridItems.Value}"
                                      SelectedItem="{Binding Path=SelectedDataGridItem.Value, Mode=TwoWay}"
                                      ExportAuthor="{Binding Path=CurrentUserName}"
                                      AdditionalReportInformation="{Binding Path=AdditionalReportInformation}"
                                      Grid.Row="1"
                                      Margin="0,5,0,0"
                                      behaviors:DataGridBehavior.PersistLayout="True"
                                      behaviors:DataGridBehavior.PersistId="0F61DC5B-1CA3-4ECC-A01E-EBEFF5859BD4">
            <guiControls:ExtendedDataGrid.ExportDefaultFileName>
                <MultiBinding StringFormat="{}{0}_{1}">
                    <Binding Path="Entity.Name" />
                    <Binding Source="{x:Static resources:LocalizedResources.General_Investment}" />
                </MultiBinding>
            </guiControls:ExtendedDataGrid.ExportDefaultFileName>

            <i:Interaction.Triggers>
                <i:EventTrigger EventName="MouseDoubleClick">
                    <mvvmcore:EventToCommand Command="{Binding MouseDoubleClickCommand}"
                                             PassEventArgsToCommand="False" />
                </i:EventTrigger>
            </i:Interaction.Triggers>


            <guiControls:ExtendedDataGrid.Columns>
                <DataGridTemplateColumn Header="{x:Static resources:LocalizedResources.General_Type}"
                                        CanUserResize="False"
                                        SortMemberPath="TypeName"
                                        behaviors:DataGridBehavior.PersistId="12EDA03A-B41B-4A99-9369-C513173EAB95">
                    <DataGridTemplateColumn.CellTemplate>
                        <DataTemplate>
                            <!--The Tag property specifies the value to be exported to xls/pdf instead of the image-->
                            <Image Height="20"
                                   Source="{Binding Path=TypeIcon}"
                                   Tag="{Binding Path=TypeName}"
                                   ToolTip="{Binding Path=TypeName}" />
                        </DataTemplate>
                    </DataGridTemplateColumn.CellTemplate>
                </DataGridTemplateColumn>

                <DataGridTextColumn x:Name="InvestmentDataGridParentNameColumn"
                                    Header="{x:Static resources:LocalizedResources.General_ParentName}"
                                    Binding="{Binding Path=ParentName, Mode=OneWay}"
                                    behaviors:DataGridBehavior.PersistId="B7BE0040-5B41-4C75-A1E2-85D5D00E444B" />

                <DataGridTextColumn Header="{x:Static resources:LocalizedResources.General_Name}"
                                    Binding="{Binding Path=Name, Mode=OneWay}"
                                    behaviors:DataGridBehavior.PersistId="3FDA91DE-7292-4AC9-80BE-6233653AE399" />

                <DataGridTemplateColumn Header="{x:Static resources:LocalizedResources.General_Investment}"
                                        SortMemberPath="Investment"
                                        behaviors:DataGridBehavior.PersistId="AB1C6320-CCAE-4E3B-9AB5-A5B1691CFB07">
                    <DataGridTemplateColumn.CellTemplate>
                        <DataTemplate>
                            <guiControls:ExtendedLabelControl BaseCurrency="{Binding Path=Data.MeasurementUnitsAdapter.BaseCurrency, Mode=OneWay, Source={StaticResource DataContextProxy}}"
                                                              UIUnit="{Binding Path=Data.MeasurementUnitsAdapter.UICurrency, Mode=OneWay, Source={StaticResource DataContextProxy}}"
                                                              BaseValue="{Binding Path=Investment, Mode=OneWay}" />
                        </DataTemplate>
                    </DataGridTemplateColumn.CellTemplate>
                </DataGridTemplateColumn>

                <DataGridTextColumn Header="{x:Static resources:LocalizedResources.ResultDetails_NeededAmount}"
                                    Binding="{Binding Path=NeededAmount, Mode=OneWay}"
                                    behaviors:DataGridBehavior.PersistId="06158AA1-24CE-485A-85E6-E8232F33CC60" />

                <DataGridTemplateColumn Header="{x:Static resources:LocalizedResources.ResultDetails_TotalInvestment}"
                                        SortMemberPath="TotalInvestment"
                                        behaviors:DataGridBehavior.PersistId="41EE7F5E-A224-4174-A859-3EC97BCA04B8">
                    <DataGridTemplateColumn.CellTemplate>
                        <DataTemplate>
                            <guiControls:ExtendedLabelControl BaseCurrency="{Binding Path=Data.MeasurementUnitsAdapter.BaseCurrency, Mode=OneWay, Source={StaticResource DataContextProxy}}"
                                                              UIUnit="{Binding Path=Data.MeasurementUnitsAdapter.UICurrency, Mode=OneWay, Source={StaticResource DataContextProxy}}"
                                                              BaseValue="{Binding Path=TotalInvestment, Mode=OneWay}" />
                        </DataTemplate>
                    </DataGridTemplateColumn.CellTemplate>
                </DataGridTemplateColumn>

                <DataGridTemplateColumn Header="{x:Static resources:LocalizedResources.General_CapacityUtilization}"
                                        SortMemberPath="CapacityUtilization"
                                        behaviors:DataGridBehavior.PersistId="95LE7F5E-A345-4174-A859-3EC97BCA5417">
                    <DataGridTemplateColumn.CellTemplate>
                        <DataTemplate>
                            <guiControls:ExtendedLabelControl UIUnit="{Binding Path=Data.MeasurementUnitsAdapter.UIPercentage, Mode=OneWay, Source={StaticResource DataContextProxy}}"
                                                              BaseValue="{Binding Path=CapacityUtilization, Mode=OneWay}"
                                                              DefaultText="{x:Static resources:LocalizedResources.General_NotAvailable}" />
                        </DataTemplate>
                    </DataGridTemplateColumn.CellTemplate>
                </DataGridTemplateColumn>
            </guiControls:ExtendedDataGrid.Columns>
        </guiControls:ExtendedDataGrid>
    </Grid>
</UserControl>
