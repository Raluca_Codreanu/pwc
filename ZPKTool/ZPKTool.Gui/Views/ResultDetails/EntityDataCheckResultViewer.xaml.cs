﻿using System;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using ZPKTool.Business;
using ZPKTool.Controls;
using ZPKTool.Data;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// The visual panel containing the entity data errors.
    /// </summary>
    public partial class EntityDataCheckResultViewer
    {
        /// <summary>
        ///  Using a DependencyProperty as the backing store for IsInViewerMode.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty IsInViewerModeProperty =
            DependencyProperty.Register("IsInViewerMode", typeof(bool), typeof(EntityDataCheckResultViewer), new PropertyMetadata(false));

        /// <summary>
        ///  Using a DependencyProperty as the backing store for CheckedEntitySourceDb.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty CheckedEntitySourceDbProperty = DependencyProperty.Register(
            "CheckedEntitySourceDb",
            typeof(ZPKTool.DataAccess.DbIdentifier),
            typeof(EntityDataCheckResultViewer),
            new PropertyMetadata(ZPKTool.DataAccess.DbIdentifier.NotSet));

        /// <summary>
        ///  Using a DependencyProperty as the backing store for NavigateIntoProcessStep.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty NavigateIntoProcessStepProperty =
            DependencyProperty.Register("NavigateIntoProcessStep", typeof(bool), typeof(EntityDataCheckResultViewer), new PropertyMetadata(false));

        /// <summary>
        ///  Using a DependencyProperty as the backing store for DataCheckResult.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty DataCheckResultProperty =
            DependencyProperty.Register("DataCheckResult", typeof(EntityDataCheckResult), typeof(EntityDataCheckResultViewer), new PropertyMetadata(null, DataCheckResultChanged));

        /// <summary>
        ///  Using a DependencyProperty as the backing store for DataCheckResult.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty ShowDataCheckResultProperty =
            DependencyProperty.Register("ShowDataCheckResult", typeof(bool), typeof(EntityDataCheckResultViewer), new PropertyMetadata(false, ShowDataCheckResultChanged));

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityDataCheckResultViewer"/> class.
        /// </summary>
        public EntityDataCheckResultViewer()
        {
            this.InitializeComponent();
            this.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// Gets or sets the data check result to display.
        /// </summary>
        public EntityDataCheckResult DataCheckResult
        {
            get { return (EntityDataCheckResult)GetValue(DataCheckResultProperty); }
            set { SetValue(DataCheckResultProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is in viewer mode.
        /// </summary>        
        public bool IsInViewerMode
        {
            get { return (bool)GetValue(IsInViewerModeProperty); }
            set { SetValue(IsInViewerModeProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating the the source database of the checked entity(ies).
        /// </summary>
        /// <remarks>
        /// It is necessary for navigating to entities with errors in the Projects Tree.
        /// </remarks>
        public ZPKTool.DataAccess.DbIdentifier CheckedEntitySourceDb
        {
            get { return (ZPKTool.DataAccess.DbIdentifier)GetValue(CheckedEntitySourceDbProperty); }
            set { SetValue(CheckedEntitySourceDbProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the navigation should be made inside the process step of on tree
        /// By default the navigation is made on tree
        /// </summary>
        public bool NavigateIntoProcessStep
        {
            get { return (bool)GetValue(NavigateIntoProcessStepProperty); }
            set { SetValue(NavigateIntoProcessStepProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the data check results should be displayed or not. It opens or closes the viewer containing the results.
        /// </summary>
        public bool ShowDataCheckResult
        {
            get { return (bool)GetValue(ShowDataCheckResultProperty); }
            set { SetValue(ShowDataCheckResultProperty, value); }
        }

        /// <summary>
        /// Called when the DataCheckResult property is changed.
        /// </summary>
        /// <param name="d">The dependency object.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void DataCheckResultChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var resultViewer = d as EntityDataCheckResultViewer;
            if (resultViewer != null)
            {
                resultViewer.Refresh();
            }
        }

        /// <summary>
        /// Called when the ShowDataCheckResult property is changed.
        /// </summary>
        /// <param name="d">The d.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void ShowDataCheckResultChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var resultViewer = d as EntityDataCheckResultViewer;
            if (resultViewer != null)
            {
                if ((bool)e.NewValue)
                {
                    resultViewer.Visibility = Visibility.Visible;
                    Storyboard showPanel = resultViewer.Resources["ShowPanel"] as Storyboard;
                    showPanel.Begin();
                }
                else
                {
                    resultViewer.Visibility = Visibility.Collapsed;
                }
            }
        }

        /// <summary>
        /// Refreshes the UI by rebuilding it from the check results list.
        /// </summary>
        private void Refresh()
        {
            this.ErrorsContainer.Children.Clear();

            if (this.DataCheckResult == null)
            {
                return;
            }

            var errors = this.DataCheckResult.FlattenErrors();
            if (errors.Count > 0)
            {
                this.ErrorsContainer.Visibility = Visibility.Visible;
                foreach (EntityDataCheckResult result in errors.Where(md => md.HasErrors))
                {
                    Grid currentError = GetCheckResultUI(result);
                    this.ErrorsContainer.Children.Add(currentError);
                    currentError.Margin = new Thickness(8, 10, 0, 0);
                }
            }
            else
            {
                // If having no errors and the data check result viewer is visible close it because is no longer needed
                if (this.Visibility == Visibility.Visible)
                {
                    this.ShowDataCheckResult = false;
                }
            }
        }

        /// <summary>
        /// Gets the UI that displays the data contained in the specified <see cref="EntityDataCheckResult"/>.
        /// </summary>
        /// <param name="result">The check result.</param>
        /// <returns>A Grid containing the UI.</returns>
        private Grid GetCheckResultUI(EntityDataCheckResult result)
        {
            Grid checkResultUI = null;

            // appending the type and name of the referenced object
            if (result.Entity.GetType() == typeof(Part))
            {
                checkResultUI = CreateCheckResultUI(LocalizedResources.General_Part, (result.Entity as Part).Name, result);
                return checkResultUI;
            }

            if (result.Entity.GetType() == typeof(RawPart))
            {
                checkResultUI = CreateCheckResultUI(LocalizedResources.General_RawPart, (result.Entity as RawPart).Name, result);
                return checkResultUI;
            }

            if (result.Entity is Assembly)
            {
                checkResultUI = CreateCheckResultUI(LocalizedResources.General_Assembly, (result.Entity as Assembly).Name, result);
                return checkResultUI;
            }

            if (result.Entity is Project)
            {
                checkResultUI = CreateCheckResultUI(LocalizedResources.General_Project, (result.Entity as Project).Name, result);
                return checkResultUI;
            }

            if (result.Entity is RawMaterial)
            {
                checkResultUI = CreateCheckResultUI(LocalizedResources.General_RawMaterial, (result.Entity as RawMaterial).Name, result);
                return checkResultUI;
            }

            if (result.Entity is Commodity)
            {
                checkResultUI = CreateCheckResultUI(LocalizedResources.General_Commodity, (result.Entity as Commodity).Name, result);
                return checkResultUI;
            }

            if (result.Entity is Consumable)
            {
                checkResultUI = CreateCheckResultUI(LocalizedResources.General_Consumable, (result.Entity as Consumable).Name, result);
                return checkResultUI;
            }

            if (result.Entity is ProcessStep)
            {
                checkResultUI = CreateCheckResultUI(LocalizedResources.General_ProcessStep, (result.Entity as ProcessStep).Name, result);
                return checkResultUI;
            }

            if (result.Entity is Machine)
            {
                checkResultUI = CreateCheckResultUI(LocalizedResources.General_Machine, (result.Entity as Machine).Name, result);
                return checkResultUI;
            }

            if (result.Entity is Die)
            {
                checkResultUI = CreateCheckResultUI(LocalizedResources.General_Die, (result.Entity as Die).Name, result);
                return checkResultUI;
            }

            return null;
        }

        /// <summary>
        /// Creates the UI that displays the data contained in the specified <see cref="EntityDataCheckResult"/>.
        /// </summary>
        /// <param name="entityType">The type name of the entity for which to create the check result UI.</param>
        /// <param name="entityName">Name of the entity for which to create the check result UI.</param>
        /// <param name="result">The check result.</param>
        /// <returns>
        /// A Grid containing the UI.
        /// </returns>
        private Grid CreateCheckResultUI(string entityType, string entityName, EntityDataCheckResult result)
        {
            // creating the grid
            Grid checkResultUI = new Grid();
            checkResultUI.VerticalAlignment = VerticalAlignment.Top;
            checkResultUI.HorizontalAlignment = HorizontalAlignment.Left;

            ColumnDefinition colDef = new ColumnDefinition();
            colDef.Width = new GridLength(1, GridUnitType.Star);
            checkResultUI.ColumnDefinitions.Add(colDef);

            // Create the 1st line (explanation line)            
            string[] parts = LocalizedResources.ResultDetails_EntityErrors.Split(new string[] { "{1}" }, StringSplitOptions.None);
            LabelControl firstLineLbl = new LabelControl();
            firstLineLbl.Inlines.Add(string.Format(parts[0], entityType));
            Hyperlink entityNameLink = ConstructHyperlinkForEntityError(entityName, result.Entity);

            firstLineLbl.Inlines.Add(entityNameLink);
            firstLineLbl.Inlines.Add(parts[1]);

            checkResultUI.RowDefinitions.Add(new RowDefinition());
            checkResultUI.Children.Add(firstLineLbl);

            // Create the 2nd line (missing data)
            if (result.NullValueProperties.Count > 0)
            {
                LabelControl secondLinelabel = new LabelControl();
                secondLinelabel.Margin = new Thickness(10, 0, 0, 0);
                secondLinelabel.TextWrapping = TextWrapping.Wrap;

                checkResultUI.RowDefinitions.Add(new RowDefinition());
                checkResultUI.Children.Add(secondLinelabel);
                Grid.SetRow(secondLinelabel, 1);

                StringBuilder sb = new StringBuilder();
                sb.Append("- ");
                sb.Append(LocalizedResources.General_EmptyFields);
                sb.Append(" ");
                foreach (string field in result.NullValueProperties)
                {
                    sb.Append(GetUIStringForPropertyName(field));
                    sb.Append(", ");
                }

                secondLinelabel.Text = sb.ToString(0, sb.Length - 2);
            }

            // Create the 3rd line (invalid data)
            if (result.InvalidValueProperties.Count > 0)
            {
                LabelControl thirdLineLabel = new LabelControl();
                thirdLineLabel.Margin = new Thickness(10, 0, 0, 0);
                thirdLineLabel.TextWrapping = TextWrapping.Wrap;

                checkResultUI.RowDefinitions.Add(new RowDefinition());
                checkResultUI.Children.Add(thirdLineLabel);
                Grid.SetRow(thirdLineLabel, checkResultUI.Children.Count - 1);

                StringBuilder sb = new StringBuilder();
                sb.Append("- ");
                sb.Append(LocalizedResources.General_InvalidFields);
                sb.Append(" ");
                foreach (string field in result.InvalidValueProperties)
                {
                    sb.Append(GetUIStringForPropertyName(field));
                    sb.Append(", ");
                }

                thirdLineLabel.Text = sb.ToString(0, sb.Length - 2);
            }

            // Create the 4th line, containing the overloaded machines
            if (result.OverloadedMachines.Count > 0)
            {
                LabelControl text = new LabelControl();
                text.Margin = new Thickness(10, 0, 0, 0);
                text.TextWrapping = TextWrapping.Wrap;

                checkResultUI.RowDefinitions.Add(new RowDefinition());
                checkResultUI.Children.Add(text);
                Grid.SetRow(text, checkResultUI.Children.Count - 1);

                text.Inlines.Add("- " + LocalizedResources.DataCheck_OverloadedMachines + ": ");
                foreach (Machine machine in result.OverloadedMachines)
                {
                    Hyperlink link = ConstructHyperlinkForEntityError(machine.Name, machine);
                    text.Inlines.Add(link);
                    text.Inlines.Add(" , ");
                }

                text.Inlines.Remove(text.Inlines.LastInline);
            }

            // Create the 5th and 6th line (unused entities).
            if (result.UnusedEntities.Count > 0)
            {
                Assembly unusedEntitiesParent = result.Entity as Assembly;
                LabelControl unusedSubassembliesLabel = new LabelControl();
                unusedSubassembliesLabel.Margin = new Thickness(10, 0, 0, 0);
                unusedSubassembliesLabel.TextWrapping = TextWrapping.Wrap;
                unusedSubassembliesLabel.Inlines.Add("- ");
                unusedSubassembliesLabel.Inlines.Add(LocalizedResources.ResultDetails_UnusedSubassemblies + " ");

                LabelControl unusedPartsLabel = new LabelControl();
                unusedPartsLabel.Margin = new Thickness(10, 0, 0, 0);
                unusedPartsLabel.TextWrapping = TextWrapping.Wrap;
                unusedPartsLabel.Inlines.Add("- ");
                unusedPartsLabel.Inlines.Add(LocalizedResources.ResultDetails_UnusedParts + " ");

                foreach (object entity in result.UnusedEntities)
                {
                    var name = EntityUtils.GetEntityName(entity);
                    var unusedEntityLink = this.ConstructHyperlinkForEntityError(name, unusedEntitiesParent.Process);
                    unusedPartsLabel.Inlines.Add(unusedEntityLink);
                    unusedPartsLabel.Inlines.Add(", ");
                }

                // Verify if exists unused parts, delete the last comma and add to the Grid.
                Run lastInlineOfUnusedPartsLabel = unusedPartsLabel.Inlines.LastInline as Run;
                if (lastInlineOfUnusedPartsLabel != null && lastInlineOfUnusedPartsLabel.Text == ", ")
                {
                    unusedPartsLabel.Inlines.Remove(lastInlineOfUnusedPartsLabel);
                    checkResultUI.RowDefinitions.Add(new RowDefinition());
                    checkResultUI.Children.Add(unusedPartsLabel);
                    Grid.SetRow(unusedPartsLabel, checkResultUI.Children.Count - 1);
                }

                // Verify if exists unused assemblies, delete the last comma and add to the Grid.  
                Run lastInlineOfUnusedSubassembliesLabel = unusedSubassembliesLabel.Inlines.LastInline as Run;
                if (lastInlineOfUnusedSubassembliesLabel != null && lastInlineOfUnusedSubassembliesLabel.Text == ", ")
                {
                    unusedSubassembliesLabel.Inlines.Remove(lastInlineOfUnusedSubassembliesLabel);
                    checkResultUI.RowDefinitions.Add(new RowDefinition());
                    checkResultUI.Children.Add(unusedSubassembliesLabel);
                    Grid.SetRow(unusedSubassembliesLabel, checkResultUI.Children.Count - 1);
                }
            }

            // Create the 7rd line (yearly production qty errors)
            foreach (var error in result.YearlyProductionQtyErrors)
            {
                LabelControl yearlyProdQtyErrorsLabel = new LabelControl();
                yearlyProdQtyErrorsLabel.Margin = new Thickness(10, 0, 0, 0);
                yearlyProdQtyErrorsLabel.TextWrapping = TextWrapping.Wrap;

                // Create the hyperlink to the sub entity.
                Hyperlink subentityNameLink = ConstructHyperlinkForEntityError(error.EntityName, error.Entity);

                // Set the last message parts that indicates whether there are not enough or too many parts in the entity.
                string lastMessagePart = string.Empty;
                if (error.IsValueSmaller)
                {
                    lastMessagePart = LocalizedResources.ResultDetails_NotEnoughParts;
                }
                else
                {
                    lastMessagePart = LocalizedResources.ResultDetails_TooManyParts;
                }

                yearlyProdQtyErrorsLabel.Inlines.Add("- " + LocalizedResources.ResultDetails_YearlyProductionQty + " ");
                yearlyProdQtyErrorsLabel.Inlines.Add(subentityNameLink);
                yearlyProdQtyErrorsLabel.Inlines.Add(" (" + lastMessagePart + ")");

                checkResultUI.RowDefinitions.Add(new RowDefinition());
                checkResultUI.Children.Add(yearlyProdQtyErrorsLabel);
                Grid.SetRow(yearlyProdQtyErrorsLabel, checkResultUI.Children.Count - 1);
            }

            return checkResultUI;
        }

        /// <summary>
        /// Constructs a hyperlink.
        /// </summary>
        /// <param name="displayedString">The displayed string.</param>
        /// <param name="underlyingHiperlinkObject">The underlying hyperlink object.</param>
        /// <returns>A hyperlink.</returns>
        private Hyperlink ConstructHyperlinkForEntityError(string displayedString, object underlyingHiperlinkObject)
        {
            if (string.IsNullOrEmpty(displayedString))
            {
                displayedString = string.Format("<{0}>", LocalizedResources.General_Unknown);
            }

            Hyperlink link = new Hyperlink();
            link.Foreground = new SolidColorBrush(Colors.Red);
            link.Inlines.Add(displayedString);
            if (underlyingHiperlinkObject == null)
            {
                return link;
            }

            var processStepObject = underlyingHiperlinkObject as ProcessStep;
            if (processStepObject == null || !processStepObject.IsMasterData)
            {
                link.NavigateUri = new Uri("http://www.dummyUri.com");
                link.RequestNavigate += delegate(object sender, RequestNavigateEventArgs e)
                {
                    var messenger = new Messenger();
                    if (this.NavigateIntoProcessStep)
                    {
                        var navMessage = new NotificationMessage<object>(Notification.NavigateToEntityIntoProcessScreen, underlyingHiperlinkObject);
                        messenger.Send(navMessage, IsInViewerMode ? GlobalMessengerTokens.ModelBrowserTargetToken : GlobalMessengerTokens.MainViewTargetToken);
                        this.ShowDataCheckResult = false;
                    }
                    else
                    {
                        NavigateToEntityMessage navMessage = new NavigateToEntityMessage(underlyingHiperlinkObject, this.CheckedEntitySourceDb);
                        messenger.Send(navMessage, IsInViewerMode ? GlobalMessengerTokens.ModelBrowserTargetToken : GlobalMessengerTokens.MainViewTargetToken);
                    }
                };
            }

            return link;
        }

        /// <summary>
        /// Gets the UI string for property of an object.
        /// </summary>
        /// <param name="field">The field.</param>
        /// <returns>Returns the UI string for the given property.</returns>
        private string GetUIStringForPropertyName(string field)
        {
            if (field == "Sprue")
            {
                return LocalizedResources.General_Sprue;
            }

            if (field == "Price")
            {
                return LocalizedResources.General_Price;
            }

            if (field == "ScrapRefundRatio")
            {
                return LocalizedResources.General_ScrapRefundRatio;
            }

            if (field == "Quantity")
            {
                return LocalizedResources.General_MaterialWeight;
            }

            if (field == "ParentWeight")
            {
                return LocalizedResources.General_PartWeight;
            }

            if (field == "Cost")
            {
                return LocalizedResources.General_Cost;
            }

            if (field == "CycleTime")
            {
                return LocalizedResources.General_CycleTime;
            }

            if (field == "SetupsPerBatch")
            {
                return LocalizedResources.General_SetupsPerBatch;
            }

            if (field == "SetupTime")
            {
                return LocalizedResources.General_SetupTime;
            }

            if (field == "MaxDownTime")
            {
                return LocalizedResources.General_MaxDowntime;
            }

            if (field == "SetupUnskilledLabour")
            {
                return LocalizedResources.Labour_UnskilledLabourSetup;
            }

            if (field == "SetupSkilledLabour")
            {
                return LocalizedResources.Labour_SkilledLabourSetup;
            }

            if (field == "SetupForeman")
            {
                return LocalizedResources.Labour_ForemanSetup;
            }

            if (field == "SetupTechnicians")
            {
                return LocalizedResources.Labour_TechniciansSetup;
            }

            if (field == "SetupEngineers")
            {
                return LocalizedResources.Labour_EngineersSetup;
            }

            if (field == "BatchSize")
            {
                return LocalizedResources.General_PartBatch;
            }

            if (field == "ProcessTime")
            {
                return LocalizedResources.General_ProcessTime;
            }

            if (field == "ProductionUnskilledLabour")
            {
                return LocalizedResources.Labour_UnskilledLabourProduction;
            }

            if (field == "ProductionSkilledLabour")
            {
                return LocalizedResources.Labour_SkilledLabourProduction;
            }

            if (field == "ProductionForeman")
            {
                return LocalizedResources.Labour_ForemanProduction;
            }

            if (field == "ProductionTechnicians")
            {
                return LocalizedResources.Labour_TechniciansProduction;
            }

            if (field == "ProductionEngineers")
            {
                return LocalizedResources.Labour_EngineersProduction;
            }

            if (field == "ScrapAmount")
            {
                return LocalizedResources.General_Reject;
            }

            if (field == "KValue")
            {
                return LocalizedResources.General_KValue;
            }

            if (field == "MachineInvestment" || field == "Investment")
            {
                return LocalizedResources.General_Investment;
            }

            if (field == "ConsumablesCost")
            {
                return LocalizedResources.General_ConsumablesCost;
            }

            if (field == "RepairsCost")
            {
                return LocalizedResources.General_RepairCost;
            }

            if (field == "OtherCost")
            {
                return LocalizedResources.General_OtherCost;
            }

            if (field == "PackagingCost")
            {
                return LocalizedResources.General_PackagingCost;
            }

            if (field == "LogisticCost")
            {
                return LocalizedResources.General_LogisticCost;
            }

            if (field == "ProjectInvest")
            {
                return LocalizedResources.General_ProjectInvest;
            }

            if (field == "DevelopmentCost")
            {
                return LocalizedResources.General_DevelopmentCost;
            }

            if (field == "YearlyProductionQuantity")
            {
                return LocalizedResources.General_YearlyProductionQuantity;
            }

            if (field == "EstimatedCost")
            {
                return LocalizedResources.General_EstimatedCost;
            }

            if (field == "ExtraShiftsNumber")
            {
                return LocalizedResources.ProcessStep_ShiftNoExceedingCost;
            }

            if (field == "ExceedRatio")
            {
                return LocalizedResources.ProcessStep_ShiftCostExceedRatio;
            }

            if (field == "Loss")
            {
                return LocalizedResources.General_Loss;
            }

            if (field == "LifeTime")
            {
                return LocalizedResources.Die_LifeTime;
            }

            if (field == "DiesetsNumberPaidByCustomer")
            {
                return LocalizedResources.Die_DiesetsNumberPaidByCustomer;
            }

            if (field == "AllocationRatio")
            {
                return LocalizedResources.Die_AllocationRatio;
            }

            if (field == "Wear")
            {
                return LocalizedResources.Die_Wear;
            }

            if (field == "Maintenance")
            {
                return LocalizedResources.General_Maintenance;
            }

            if (field == "CostAllocationNumberOfParts")
            {
                return LocalizedResources.Die_NumberOfParts;
            }

            if (field == "PriceUnitBase")
            {
                return LocalizedResources.General_PriceUnitBase;
            }

            if (field == "Amount")
            {
                return LocalizedResources.General_Amount;
            }

            if (field == "RejectRatio")
            {
                return LocalizedResources.RawMaterial_RejectRatio;
            }

            return field;
        }

        /// <summary>
        /// Handles the Click event of the CloseErrorsPanelButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void CloseErrorsPanelButton_Click(object sender, RoutedEventArgs e)
        {
            this.ShowDataCheckResult = false;
        }
    }
}