﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.ViewModels;
using ZPKTool.Gui.Views;

namespace ZPKTool.Gui.Views
{
    #region Sort Direction Enum

    /// <summary>
    /// The sort direction enum.
    /// </summary>
    public enum SortDirection
    {
        /// <summary>
        /// Ascending sort direction.
        /// </summary>
        Ascending = 0,

        /// <summary>
        /// Descending sort direction.
        /// </summary>
        Descending = 1
    }

    #endregion Sort Direction Enum

    /// <summary>
    /// Interaction logic for ComparisonView.xaml
    /// </summary>
    public partial class CompareView : UserControl
    {
        #region Fields

        /// <summary>
        /// Value changed notifier.
        /// </summary>
        private DependencyPropertyChangeNotifier valueChangedNotifier;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="CompareView"/> class.
        /// </summary>
        public CompareView()
        {
            InitializeComponent();

            this.valueChangedNotifier = new DependencyPropertyChangeNotifier(this.ComparisonDataGrid, DataGrid.ItemsSourceProperty);
            this.valueChangedNotifier.ValueChanged += new EventHandler(CompareCollectionViewValueChanged);
        }

        #endregion Constructor

        #region Properties

        /// <summary>
        /// Gets or sets the sorted data grid header.
        /// </summary>
        /// <value>
        /// The sorted data grid header.
        /// </value>
        private DataGridRowHeader SortedDataGridHeader { get; set; }

        #endregion Properties

        #region Collection changed

        /// <summary>
        /// Handles the ValueChanged event of the ValueChangedNotifier control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void CompareCollectionViewValueChanged(object sender, EventArgs e)
        {
            this.ComparisonDataGrid.Columns.Clear();
            CollectionViewSource headers = this.Resources["ColumnHeadersCollectionContainer"] as CollectionViewSource;

            if (headers.View != null)
            {
                foreach (object header in headers.View.SourceCollection)
                {
                    DataGridTemplateColumn columnTemplate = new DataGridTemplateColumn();

                    var prop = header.GetType().GetProperty("Header");
                    if (prop == null)
                    {
                        throw new InvalidOperationException("Header property not found on header");
                    }

                    columnTemplate.Header = prop.GetValue(header, null);
                    ComparisonDataGridCellTemplateSelector cellTemplateSelector = new ComparisonDataGridCellTemplateSelector();
                    columnTemplate.CellTemplateSelector = cellTemplateSelector;
                    this.ComparisonDataGrid.Columns.Add(columnTemplate);
                }
            }
        }

        #endregion Collection changed

        #region Sorting

        /// <summary>
        /// Handles the PreviewMouseLeftButtonDown event of the ComparisonDataGrid control.
        /// Data Grid Sort feature is handled in this event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void ComparisonDataGrid_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            // DataGrid RowHeader Sort (Ascending/ Descending).
            DependencyObject dep = (DependencyObject)e.OriginalSource;

            // iteratively traverse the visual tree
            while ((dep != null) &&
                    !(dep is DataGridRowHeader))
            {
                dep = VisualTreeHelper.GetParent(dep);
            }

            if (dep == null)
            {
                return;
            }

            DataGridRowHeader rowHeader = dep as DataGridRowHeader;
            if (rowHeader != null)
            {
                SortDataGridByRowHeader(rowHeader);
            }
        }

        /// <summary>
        /// Sorts the data grid row header.
        /// </summary>
        /// <param name="rowHeader">The row header.</param>
        private void SortDataGridByRowHeader(DataGridRowHeader rowHeader)
        {
            DependencyObject dep = rowHeader as DependencyObject;

            // navigate further up the tree and find the DataGridRow
            while ((dep != null) && !(dep is DataGridRow))
            {
                dep = VisualTreeHelper.GetParent(dep);
            }

            DataGridRow row = dep as DataGridRow;
            if (row != null)
            {
                row.IsSelected = true;

                CompareRowPropertyItem comparedRowItem = row.Item as CompareRowPropertyItem;
                if (comparedRowItem != null)
                {
                    Type itemsType = comparedRowItem.Type;
                    bool allItemsAreComparable = true;

                    for (int index = 0; index < comparedRowItem.ComparedRowItems.Count(); index++)
                    {
                        // check if item is IComparable.
                        if (comparedRowItem.ComparedRowItems[index] != null && !(comparedRowItem.ComparedRowItems[index] is IComparable))
                        {
                            allItemsAreComparable = false;
                        }
                    }

                    if (allItemsAreComparable)
                    {
                        if (comparedRowItem.IsSortedAscending.HasValue == false || comparedRowItem.IsSortedAscending.Value == false)
                        {
                            List<CompareRowPropertyItem> dataGridItems = this.ComparisonDataGrid.ItemsSource.OfType<CompareRowPropertyItem>().ToList();

                            // set row headers to unsorted state.
                            foreach (CompareRowPropertyItem item in dataGridItems)
                            {
                                item.IsSortedAscending = null;
                            }

                            this.SortDataGrid(SortDirection.Ascending, comparedRowItem, dataGridItems, itemsType);

                            if (SortedDataGridHeader != null)
                            {
                                VisualStateManager.GoToState(SortedDataGridHeader, "Unsorted", false);
                            }

                            VisualStateManager.GoToState(rowHeader, "SortAscending", false);
                            comparedRowItem.IsSortedAscending = true;
                            SortedDataGridHeader = rowHeader;
                        }
                        else if (comparedRowItem.IsSortedAscending.Value == true)
                        {
                            List<CompareRowPropertyItem> dataGridItems = this.ComparisonDataGrid.ItemsSource.OfType<CompareRowPropertyItem>().ToList();

                            // set row headers to unsorted state.
                            foreach (CompareRowPropertyItem item in dataGridItems)
                            {
                                item.IsSortedAscending = null;
                            }

                            this.SortDataGrid(SortDirection.Descending, comparedRowItem, dataGridItems, itemsType);

                            if (SortedDataGridHeader != null)
                            {
                                VisualStateManager.GoToState(SortedDataGridHeader, "Unsorted", false);
                            }

                            VisualStateManager.GoToState(rowHeader, "SortDescending", false);
                            comparedRowItem.IsSortedAscending = false;
                            SortedDataGridHeader = rowHeader;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Sorts the data grid items ascending.
        /// </summary>
        /// <param name="sortDirection">The sort direction.</param>
        /// <param name="comparedRowItem">The compared row item.</param>
        /// <param name="dataGridItems">The data grid items.</param>
        /// <param name="itemsType">Type of the items.</param>
        private void SortDataGrid(SortDirection sortDirection, CompareRowPropertyItem comparedRowItem, List<CompareRowPropertyItem> dataGridItems, Type itemsType)
        {
            for (int indexi = 0; indexi < comparedRowItem.ComparedRowItems.Count(); indexi++)
            {
                for (int indexj = indexi + 1; indexj < comparedRowItem.ComparedRowItems.Count(); indexj++)
                {
                    if (itemsType == typeof(decimal?) || itemsType == typeof(decimal))
                    {
                        decimal? elementOfIndexi = comparedRowItem.ComparedRowItems[indexi] as decimal?;
                        decimal? elementOfIndexj = comparedRowItem.ComparedRowItems[indexj] as decimal?;

                        if (elementOfIndexi == null)
                        {
                            elementOfIndexi = decimal.MinValue;
                        }

                        if (elementOfIndexj == null)
                        {
                            elementOfIndexj = decimal.MinValue;
                        }

                        if (sortDirection == SortDirection.Ascending)
                        {
                            if (elementOfIndexi > elementOfIndexj)
                            {
                                this.InterChangeItems(comparedRowItem, dataGridItems, indexi, indexj);
                            }
                        }
                        else if (sortDirection == SortDirection.Descending)
                        {
                            if (elementOfIndexi < elementOfIndexj)
                            {
                                this.InterChangeItems(comparedRowItem, dataGridItems, indexi, indexj);
                            }
                        }
                    }
                    else if (itemsType == typeof(DateTime))
                    {
                        var date1 = comparedRowItem.ComparedRowItems[indexi] as DateTime?;
                        var date2 = comparedRowItem.ComparedRowItems[indexj] as DateTime?;
                        if (!date1.HasValue)
                        {
                            date1 = DateTime.MinValue;
                        }

                        if (!date2.HasValue)
                        {
                            date2 = DateTime.MinValue;
                        }

                        if (sortDirection == SortDirection.Ascending)
                        {
                            if (date1 > date2)
                            {
                                this.InterChangeItems(comparedRowItem, dataGridItems, indexi, indexj);
                            }
                        }
                        else if (sortDirection == SortDirection.Descending)
                        {
                            if (date1 < date2)
                            {
                                this.InterChangeItems(comparedRowItem, dataGridItems, indexi, indexj);
                            }
                        }
                    }
                    else if (itemsType == typeof(string))
                    {
                        string elementOfIndexi = comparedRowItem.ComparedRowItems[indexi] as string;
                        string elementOfIndexj = comparedRowItem.ComparedRowItems[indexj] as string;

                        if (elementOfIndexi == null)
                        {
                            elementOfIndexi = string.Empty;
                        }

                        if (elementOfIndexj == null)
                        {
                            elementOfIndexj = string.Empty;
                        }

                        if (sortDirection == SortDirection.Ascending)
                        {
                            if (string.Compare(elementOfIndexi, elementOfIndexj) > 0)
                            {
                                this.InterChangeItems(comparedRowItem, dataGridItems, indexi, indexj);
                            }
                        }
                        else if (sortDirection == SortDirection.Descending)
                        {
                            if (string.Compare(elementOfIndexi, elementOfIndexj) < 0)
                            {
                                this.InterChangeItems(comparedRowItem, dataGridItems, indexi, indexj);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Interchanges the dataGrid items.
        /// </summary>
        /// <param name="comparedRowItem">The compared row item.</param>
        /// <param name="dataGridItems">The data grid items.</param>
        /// <param name="indexi">The index i.</param>
        /// <param name="indexj">The index j.</param>
        private void InterChangeItems(CompareRowPropertyItem comparedRowItem, List<CompareRowPropertyItem> dataGridItems, int indexi, int indexj)
        {
            var auxHeader = this.ComparisonDataGrid.Columns[indexi].Header;
            this.ComparisonDataGrid.Columns[indexi].Header = this.ComparisonDataGrid.Columns[indexj].Header;
            this.ComparisonDataGrid.Columns[indexj].Header = auxHeader;

            // interchange the items that are compared, the compared row.
            var aux = comparedRowItem.ComparedRowItems[indexi];
            comparedRowItem.ComparedRowItems[indexi] = comparedRowItem.ComparedRowItems[indexj];
            comparedRowItem.ComparedRowItems[indexj] = aux;

            var head = comparedRowItem.ColumnHeaders[indexi];
            comparedRowItem.ColumnHeaders[indexi] = comparedRowItem.ColumnHeaders[indexj];
            comparedRowItem.ColumnHeaders[indexj] = head;

            var tempMeasurementUnits = comparedRowItem.MeasurementUnitsAdapters[indexi];
            comparedRowItem.MeasurementUnitsAdapters[indexi] = comparedRowItem.MeasurementUnitsAdapters[indexj];
            comparedRowItem.MeasurementUnitsAdapters[indexj] = tempMeasurementUnits;

            // interchange all the related items to the compared row item.
            for (int indexk = 0; indexk < dataGridItems.Count(); indexk++)
            {
                if (dataGridItems[indexk] != comparedRowItem)
                {
                    var auxiliar = dataGridItems[indexk].ComparedRowItems[indexi];
                    dataGridItems[indexk].ComparedRowItems[indexi] = dataGridItems[indexk].ComparedRowItems[indexj];
                    dataGridItems[indexk].ComparedRowItems[indexj] = auxiliar;

                    var header = dataGridItems[indexk].ColumnHeaders[indexi];
                    dataGridItems[indexk].ColumnHeaders[indexi] = dataGridItems[indexk].ColumnHeaders[indexj];
                    dataGridItems[indexk].ColumnHeaders[indexj] = header;
                }
            }
        }

        #endregion Sorting

        /// <summary>
        /// Handles the ConvertEventArgs event of the <see cref="EventToCommand" /> that routes the MouseDoubleClick event of the ComparisonDataGrid to a command.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="args">The event arguments of the MouseDoubleClick event.</param>
        /// <returns>
        /// The data item of the clicked data grid cell and the column number
        /// </returns>
        private object MouseDoubleClickCommand_ConvertEventArgs(object sender, RoutedEventArgs args)
        {
            args.Handled = true;
            DependencyObject source = (DependencyObject)args.OriginalSource;
            DataGridRow row = ZPKTool.Gui.Utils.UIHelper.FindParent<DataGridRow>(source);
            var cell = ZPKTool.Gui.Utils.UIHelper.FindParent<DataGridCell>(source);

            if (cell != null && row != null)
            {
                object item = row.Item;
                int columnIndex = cell.Column.DisplayIndex;

                return new Tuple<object, int>(item, columnIndex);
            }

            return null;
        }
    }
}