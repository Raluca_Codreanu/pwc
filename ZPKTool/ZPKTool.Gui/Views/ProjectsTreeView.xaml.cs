﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for ProjectsTreeView.xaml
    /// </summary>    
    public partial class ProjectsTreeView : UserControl, IFocusDispatcher
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectsTreeView"/> class.
        /// </summary>
        public ProjectsTreeView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handles the MouseRightButtonDown event of the items in the Projects Tree.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void ProjectsTreeItem_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            TreeViewItem item = sender as TreeViewItem;
            if (item != null)
            {
                if (!item.IsSelected)
                {
                    item.IsSelected = true;
                }

                e.Handled = true;
            }
        }

        /// <summary>
        /// Handles the SelectedItemChanged event of the Projects Tree.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedPropertyChangedEventArgs"/> instance containing the event data.</param>
        private void ProjectsTree_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            // In some cases after selection the keyboard focus in not on the selected tree item 
            // so is needed to bring the focus on the tree or anywhere nearby
            this.ProjectsTree.Focus();
        }

        /// <summary>
        /// Sets the focus on the projects tree.
        /// This way the navigation using keyboard in the tree will work.
        /// </summary>
        public void FocusContent()
        {
            if (this.ProjectsTree != null)
            {
                var isFocused = this.ProjectsTree.Focus();
                if (!isFocused)
                {
                    RoutedEventHandler treeLoadedHandler = null;
                    treeLoadedHandler = delegate
                    {
                        this.ProjectsTree.Focus();
                        this.ProjectsTree.Loaded -= treeLoadedHandler;
                    };

                    this.ProjectsTree.Loaded += treeLoadedHandler;
                }
            }
        }
    }
}
