﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Converters;
using ZPKTool.Gui.Managers;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore.Commands;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// DataGrid Cell Data Template Selector.
    /// </summary>
    public class ComparisonDataGridCellTemplateSelector : DataTemplateSelector
    {
        /// <summary>
        /// When overridden in a derived class, returns a <see cref="T:System.Windows.DataTemplate"/> based on custom logic.
        /// </summary>
        /// <param name="item">The data object for which to select the template.</param>
        /// <param name="container">The data-bound object.</param>
        /// <returns>
        /// Returns a <see cref="T:System.Windows.DataTemplate"/> or null. The default value is null.
        /// </returns>
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            FrameworkElement element = container as FrameworkElement;
            CompareRowPropertyItem comparisonItem = item as CompareRowPropertyItem;

            if (container != null && element != null)
            {
                element.VerticalAlignment = VerticalAlignment.Stretch;
                element.HorizontalAlignment = HorizontalAlignment.Stretch;

                DataGridCell cell = Utils.UIHelper.FindParent<DataGridCell>(container);

                DataGridColumn column = cell.Column;
                column.Width = 200;
                int index = column.DisplayIndex;
                string bindingPath = string.Format("ComparedRowItems[{0}]", index);

                if (comparisonItem != null)
                {
                    DataTemplate template = new DataTemplate();
                    template.DataType = typeof(CompareRowPropertyItem);
                    FrameworkElementFactory gridFactory = new FrameworkElementFactory(typeof(Grid));
                    gridFactory.SetValue(Grid.HorizontalAlignmentProperty, HorizontalAlignment.Stretch);
                    gridFactory.SetValue(Grid.VerticalAlignmentProperty, VerticalAlignment.Stretch);

                    if (comparisonItem.DisplayTemplateType == DisplayTemplateType.TextBlock)
                    {
                        FrameworkElementFactory textBlockFactory = new FrameworkElementFactory(typeof(TextBlock));
                        Binding binding = new Binding(bindingPath);
                        binding.Mode = BindingMode.OneWay;

                        if (!string.IsNullOrWhiteSpace(comparisonItem.SymbolFormat))
                        {
                            binding.StringFormat = comparisonItem.SymbolFormat;
                        }

                        textBlockFactory.SetBinding(TextBlock.TextProperty, binding);
                        textBlockFactory.SetValue(TextBlock.TextWrappingProperty, TextWrapping.Wrap);
                        textBlockFactory.SetValue(TextBlock.HorizontalAlignmentProperty, HorizontalAlignment.Center);
                        textBlockFactory.SetValue(TextBlock.VerticalAlignmentProperty, VerticalAlignment.Center);

                        gridFactory.AppendChild(textBlockFactory);
                        template.VisualTree = gridFactory;
                    }
                    else if (comparisonItem.DisplayTemplateType == DisplayTemplateType.ExtendedLabelControl)
                    {
                        FrameworkElementFactory extendedLabelControlFactory = new FrameworkElementFactory(typeof(ExtendedLabelControl));
                        Binding binding = new Binding(bindingPath);
                        binding.Mode = BindingMode.OneWay;

                        extendedLabelControlFactory.SetBinding(ExtendedLabelControl.BaseValueProperty, binding);

                        if (comparisonItem.MaxDigitsToDisplay != null)
                        {
                            extendedLabelControlFactory.SetValue(ExtendedLabelControl.MaxDigitsToDisplayProperty, comparisonItem.MaxDigitsToDisplay);
                        }

                        if (!string.IsNullOrWhiteSpace(comparisonItem.SymbolFormat))
                        {
                            extendedLabelControlFactory.SetValue(ExtendedLabelControl.StringFormatProperty, comparisonItem.SymbolFormat);
                        }

                        var unitsAdapter = comparisonItem.MeasurementUnitsAdapters.ElementAt(index);
                        var unit = unitsAdapter.GetCurrentUnit(comparisonItem.UnitType);

                        // Get the measurement units and base currency and use it to set the ExtendedLabelControl
                        string unitsBindingPath = string.Empty;
                        switch (comparisonItem.UnitType)
                        {
                            case UnitType.Percentage:
                                unitsBindingPath = string.Format("MeasurementUnitsAdapters[{0}].UIPercentage", index);
                                break;

                            case UnitType.Currency:
                                unitsBindingPath = string.Format("MeasurementUnitsAdapters[{0}].UICurrency", index);
                                string baseCurrencyBindingPath = string.Format("MeasurementUnitsAdapters[{0}].BaseCurrency", index);
                                Binding baseCurrencyBinding = new Binding(baseCurrencyBindingPath);
                                baseCurrencyBinding.Mode = BindingMode.OneWay;
                                extendedLabelControlFactory.SetBinding(ExtendedLabelControl.BaseCurrencyProperty, baseCurrencyBinding);
                                break;
                        }

                        if (!string.IsNullOrWhiteSpace(unitsBindingPath))
                        {
                            Binding unitsBinding = new Binding(unitsBindingPath);
                            unitsBinding.Mode = BindingMode.OneWay;
                            extendedLabelControlFactory.SetBinding(ExtendedLabelControl.UIUnitProperty, unitsBinding);
                        }

                        if (comparisonItem.Symbol != null)
                        {
                            extendedLabelControlFactory.SetValue(ExtendedLabelControl.SymbolProperty, comparisonItem.Symbol);
                        }

                        extendedLabelControlFactory.SetValue(ExtendedLabelControl.HorizontalAlignmentProperty, HorizontalAlignment.Center);
                        extendedLabelControlFactory.SetValue(ExtendedLabelControl.VerticalAlignmentProperty, VerticalAlignment.Center);

                        gridFactory.AppendChild(extendedLabelControlFactory);
                        template.VisualTree = gridFactory;
                    }
                    else if (comparisonItem.DisplayTemplateType == DisplayTemplateType.List)
                    {
                        bindingPath += ".Key";
                        DataGridRow row = Utils.UIHelper.FindParent<DataGridRow>(container);
                        if (row != null)
                        {
                            row.Selected += new RoutedEventHandler(Row_Selected);
                        }

                        cell.VerticalAlignment = VerticalAlignment.Top;
                        cell.HorizontalAlignment = HorizontalAlignment.Stretch;

                        FrameworkElementFactory listBoxFactory = new FrameworkElementFactory(typeof(ListBox));
                        listBoxFactory.SetValue(ListBox.HorizontalAlignmentProperty, HorizontalAlignment.Stretch);
                        listBoxFactory.SetValue(ListBox.VerticalAlignmentProperty, VerticalAlignment.Stretch);
                        listBoxFactory.SetValue(ListBox.VerticalContentAlignmentProperty, VerticalAlignment.Stretch);
                        listBoxFactory.SetBinding(ListBox.ItemsSourceProperty, new Binding(bindingPath));
                        listBoxFactory.SetValue(ListBox.SelectionModeProperty, SelectionMode.Single);
                        listBoxFactory.SetValue(ListBox.DisplayMemberPathProperty, "Name");
                        listBoxFactory.SetValue(ListBox.BorderThicknessProperty, new Thickness(0));

                        listBoxFactory.AddHandler(ListBox.ContextMenuOpeningEvent, new ContextMenuEventHandler(CompareMenu_ContextMenuOpening));
                        listBoxFactory.AddHandler(ListBox.MouseDoubleClickEvent, new MouseButtonEventHandler(ListBox_MouseDoubleClick));
                        ContextMenu contextMenu = new ContextMenu();
                        listBoxFactory.SetValue(ListBox.ContextMenuProperty, contextMenu);

                        gridFactory.AppendChild(listBoxFactory);
                        template.VisualTree = gridFactory;
                    }
                    else if (comparisonItem.DisplayTemplateType == DisplayTemplateType.CheckBox)
                    {
                        FrameworkElementFactory checkBoxControlFactory = new FrameworkElementFactory(typeof(CheckBox));
                        checkBoxControlFactory.SetValue(CheckBox.IsHitTestVisibleProperty, false);
                        checkBoxControlFactory.SetValue(CheckBox.FocusableProperty, false);
                        Binding binding = new Binding(bindingPath);
                        binding.Mode = BindingMode.OneWay;

                        checkBoxControlFactory.SetBinding(CheckBox.IsCheckedProperty, binding);
                        checkBoxControlFactory.SetValue(CheckBox.HorizontalAlignmentProperty, HorizontalAlignment.Center);
                        checkBoxControlFactory.SetValue(CheckBox.VerticalAlignmentProperty, VerticalAlignment.Center);

                        gridFactory.AppendChild(checkBoxControlFactory);
                        template.VisualTree = gridFactory;
                    }
                    else if (comparisonItem.DisplayTemplateType == DisplayTemplateType.Date)
                    {
                        FrameworkElementFactory textBlockFactory = new FrameworkElementFactory(typeof(TextBlock));
                        Binding binding = new Binding(bindingPath);
                        binding.Mode = BindingMode.OneWay;
                        binding.Converter = new DateTimeToShortDateConverter();
                        textBlockFactory.SetBinding(TextBlock.TextProperty, binding);
                        textBlockFactory.SetValue(TextBlock.TextWrappingProperty, TextWrapping.Wrap);
                        textBlockFactory.SetValue(TextBlock.HorizontalAlignmentProperty, HorizontalAlignment.Center);
                        textBlockFactory.SetValue(TextBlock.VerticalAlignmentProperty, VerticalAlignment.Center);

                        gridFactory.AppendChild(textBlockFactory);
                        template.VisualTree = gridFactory;
                    }

                    return template;
                }
            }

            return null;
        }

        /// <summary>
        /// Handles the ContextMenuOpening event of the CompareMenu control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Controls.ContextMenuEventArgs"/> instance containing the event data.</param>
        private void CompareMenu_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            ListBox listBox = sender as ListBox;

            if (listBox != null)
            {
                ContextMenu contextMenu = listBox.ContextMenu;
                MenuItem compareMenu = new MenuItem();
                compareMenu.Command = MainViewCommands.Compare;

                Image compareImage = new Image();
                compareImage.Width = 20d;
                compareImage.Height = 20d;
                compareImage.SetResourceReference(Image.SourceProperty, Images.CompareIconKey);
                compareMenu.Icon = compareImage;

                MenuItem goToMenu = new MenuItem();
                goToMenu.Header = LocalizedResources.General_GoTo;

                Image goToImage = new Image();
                goToImage.Width = 20d;
                goToImage.Height = 20d;
                goToImage.SetResourceReference(Image.SourceProperty, Images.GoToIconKey);
                goToMenu.Icon = goToImage;
                goToMenu.Header = LocalizedResources.General_GoTo;

                List<MenuItem> menuList = new List<MenuItem>();
                menuList.Add(compareMenu);
                menuList.Add(goToMenu);
                contextMenu.ItemsSource = menuList;

                var selectedItem = listBox.SelectedItem;

                CompareRowPropertyItem compareRowPropertyItem = listBox.DataContext as CompareRowPropertyItem;
                var context = DbIdentifier.LocalDatabase;

                if (compareRowPropertyItem != null)
                {
                    var result = compareRowPropertyItem.ComparedRowItems.FirstOrDefault(item => ((KeyValuePair<object, DbIdentifier>)item).Key == listBox.ItemsSource);

                    if (result != null)
                    {
                        context = ((KeyValuePair<object, DbIdentifier>)result).Value;
                    }

                    goToMenu.Command = compareRowPropertyItem.GoToCommand;
                }

                var param = new List<EntityInfo>() { new EntityInfo(selectedItem, context) };

                compareMenu.CommandParameter = param;
                goToMenu.CommandParameter = selectedItem;
            }
        }

        /// <summary>
        /// Handles the Selected event of the Row control.
        /// Deselect the row thats contains a list in it.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void Row_Selected(object sender, RoutedEventArgs e)
        {
            DataGridRow row = sender as DataGridRow;
            row.IsSelected = false;
            e.Handled = true;
        }

        /// <summary>
        /// Handles the List box mouse double click event and navigates to the selected entity
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void ListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListBox listBox = sender as ListBox;
            if (listBox != null)
            {
                var selectedItem = listBox.SelectedItem;
                CompareRowPropertyItem compareRowPropertyItem = listBox.DataContext as CompareRowPropertyItem;

                if (compareRowPropertyItem != null)
                {
                    compareRowPropertyItem.GoToCommand.Execute(selectedItem);
                }
            }
        }

        #region DateTimeToShortDateConverter class

        /// <summary>
        /// Converts the value of a System.DateTime object to its equivalent short date string representation.
        /// </summary>
        [ValueConversion(typeof(DateTime), typeof(string))]
        private class DateTimeToShortDateConverter : IValueConverter
        {
            /// <summary>
            /// Converts the specified DateTime to ShortDateString.
            /// </summary>
            /// <param name="value">The value.</param>
            /// <param name="targetType">Type of the target.</param>
            /// <param name="parameter">The parameter.</param>
            /// <param name="culture">The culture.</param>
            /// <returns>
            /// A converted value. If the method returns null, the valid null value is used.
            /// </returns>
            public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
            {
                if (value is DateTime)
                {
                    DateTime date = (DateTime)value;
                    return date.ToShortDateString();
                }
                else
                {
                    return string.Empty;
                }
            }

            /// <summary>
            /// Converts a value.
            /// </summary>
            /// <param name="value">The value that is produced by the binding target.</param>
            /// <param name="targetType">The type to convert to.</param>
            /// <param name="parameter">The converter parameter to use.</param>
            /// <param name="culture">The culture to use in the converter.</param>
            /// <returns>
            /// A converted value. If the method returns null, the valid null value is used.
            /// </returns>
            public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
            {
                throw new NotImplementedException();
            }
        }

        #endregion
    }
}
