﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using ZPKTool.Data;
using ZPKTool.Gui.Utils;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// A converter that joins the roles of a user in a string, separated by comma.
    /// </summary>
    [ValueConversion(typeof(Role), typeof(string))]
    public class JoinUserRolesConverter : IValueConverter
    {
        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (Enum.IsDefined(typeof(Role), value))
            {
                var role = (Role)value;
                var separatedRoles = new List<string>();
                foreach (Role currentRole in Enum.GetValues(role.GetType()))
                {
                    if (role.HasFlag(currentRole)
                        && currentRole != Role.None)
                    {
                        separatedRoles.Add(UIUtils.GetEnumValueLabel(currentRole));
                    }
                }

                return string.Join(", ", separatedRoles);
            }

            return null;
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
