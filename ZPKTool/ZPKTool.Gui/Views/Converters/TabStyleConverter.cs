﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Converter used to set the tabs from ProcessStep View style, based on a bool value representing whether the specified tab contains data or not.
    /// </summary>
    public class TabStyleConverter : IValueConverter
    {
        /// <summary>
        /// Gets or sets the style applied for tabs without data.
        /// </summary>
        public Style TabWithNoData { get; set; }

        /// <summary>
        /// Gets or sets the style applied for tabs with no data.
        /// </summary>
        public Style TabWithData { get; set; }

        /// <summary>
        /// Converts a bool value into a style. Used to set the tabs style from ProcessStep View.
        /// </summary>
        /// <param name="value">TThe value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value specifying the style used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null && value is bool)
            {
                return (bool)value ? TabWithData : TabWithNoData;
            }

            return TabWithNoData;
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
