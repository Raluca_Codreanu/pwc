﻿using System;
using System.Windows.Data;
using System.Windows.Media;
using ZPKTool.Calculations.CO2Emissions;

namespace ZPKTool.Gui.Converters
{
    /// <summary>
    /// A converter that sets a color on data grid item depending on the CO2 emission type.
    /// Is used to difference process steps and machines items.
    /// </summary>
    public class CO2EmissionToColorConverter : IValueConverter
    {
        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is CO2EmissionsProcessStep)
            {
                return new SolidColorBrush((Color)ColorConverter.ConvertFromString("#F2F2F2"));
            }

            return new SolidColorBrush(Colors.White);
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
