﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ZPKTool.Gui.Utils;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for ManageCurrenciesView.xaml
    /// </summary>
    public partial class ManageCurrenciesView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ManageCurrenciesView"/> class.
        /// </summary>
        public ManageCurrenciesView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handles the ConvertEventArgs event of the <see cref="EventToCommand" /> that routes the PreviewKeyDown event of the CurrenciesDataGrid to a command.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="args">The event arguments of the PreviewKeyDown event.</param>
        /// <returns>
        /// The data item of the clicked data grid row.
        /// </returns>
        private object CurrenciesDataGrid_PreviewKeyDownCommand_ConvertEventArgs(object sender, RoutedEventArgs args)
        {
            var keyArgs = args as KeyEventArgs;
            if (!UIUtils.IsCausingDataGridSelectionChange(keyArgs.Key))
            {
                return null;
            }

            // Try to get the clicked item.
            DependencyObject source = (DependencyObject)args.OriginalSource;
            var row = UIHelper.FindParent<DataGridRow>(source);

            if (row == null)
            {
                return null;
            }

            // If the user actually clicked on a row, extract its underlying data item.
            return row.Item;
        }

        /// <summary>
        /// Handles the ConvertEventArgs event of the <see cref="EventToCommand" /> that routes the PreviewMouseDown event of the CurrenciesDataGrid to a command.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="args">The event arguments of the PreviewMouseDown event.</param>
        /// <returns>
        /// The data item of the clicked data grid row.
        /// </returns>
        private object CurrenciesDataGrid_PreviewMouseDownCommand_ConvertEventArgs(object sender, RoutedEventArgs args)
        {
            // Try to get the clicked item.
            DependencyObject source = (DependencyObject)args.OriginalSource;
            var row = UIHelper.FindParent<DataGridRow>(source);

            if (row == null)
            {
                return null;
            }

            // If the user actually clicked on a row, extract its underlying data item.
            return row.Item;
        }
    }
}
