﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using ZPKTool.Gui.Utils;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for ModelBrowserView.xaml
    /// </summary>
    public partial class ModelBrowserView : UserControl
    {
        /// <summary>
        /// The max with of the content presenter that displays the main document's content. It is uses to avoid giving infinite horizontal space to the content.
        /// </summary>
        private double maxContentWidthWithScroll = System.Windows.SystemParameters.PrimaryScreenWidth;

        /// <summary>
        /// Initializes a new instance of the <see cref="ModelBrowserView"/> class.
        /// </summary>
        public ModelBrowserView()
        {
            InitializeComponent();

            Loaded += (sender, args) => UIUtils.FocusElement(this.ModelBrowserTree);
        }

        /// <summary>
        /// Handles the KetDown event for the zooming combo box. Used to update the data source when ENTER is pressed.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> instance containing the event data.</param>
        private void ZoomLevelsComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return || e.Key == Key.Enter)
            {
                ComboBox comboBox = sender as ComboBox;
                UpdateDataSource(comboBox, ComboBox.TextProperty);
                e.Handled = true;
            }
        }

        /// <summary>
        /// Updates the data source for the specified element.
        /// </summary>
        /// <param name="element">The target framework element.</param>
        /// <param name="dp">The target DependencyProperty to get the binding from.</param>
        private void UpdateDataSource(FrameworkElement element, DependencyProperty dp)
        {
            if (element != null)
            {
                BindingExpression expression = element.GetBindingExpression(dp);
                if (expression != null)
                {
                    expression.UpdateSource();
                }
            }
        }

        /// <summary>
        /// Handles the SizeChanged event of the ModelBrowser control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SizeChangedEventArgs" /> instance containing the event data.</param>
        private void ModelBrowser_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            this.maxContentWidthWithScroll = e.NewSize.Width;
        }

        /// <summary>
        /// Handles the SizeChanged event of the MainDocumentContentScrollViewer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SizeChangedEventArgs" /> instance containing the event data.</param>
        private void MainDocumentContentScrollViewer_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            // The size of the Main Document's scroll viewer has changed -> re-evaluate whether the horizontal scrolling should be enabled 
            // and the re-calculate the max width of its content.
            // This action is not also performed in ModelBrowser_SizeChanged because if the Model Browser size changes the size of MainDocumentContentScrollViewer will also change.
            double newWidth = e.NewSize.Width;

            if (newWidth < MainView.MainDocContentWidthWithoutScroll)
            {
                this.MainDocumentContentScrollViewer.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
                if (this.MainDocumentContentPresenter != null)
                {
                    this.MainDocumentContentPresenter.MaxWidth = this.maxContentWidthWithScroll;
                }
            }
            else
            {
                this.MainDocumentContentScrollViewer.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
                if (this.MainDocumentContentPresenter != null)
                {
                    this.MainDocumentContentPresenter.MaxWidth = double.PositiveInfinity;
                }
            }
        }
    }
}
