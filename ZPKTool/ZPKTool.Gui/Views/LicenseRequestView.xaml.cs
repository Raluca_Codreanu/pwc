﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for LicenseRequestView.xaml
    /// </summary>
    public partial class LicenseRequestView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LicenseRequestView"/> class.
        /// </summary>
        public LicenseRequestView()
        {
            InitializeComponent();         
        }

        /// <summary>
        /// Handles the PreviewLostKeyboardFocus event of the ErrorsPanel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.KeyboardFocusChangedEventArgs"/> instance containing the event data.</param>
        private void ErrorsPanel_PreviewLostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            Grid grid = sender as Grid;
            if (grid != null && grid.Visibility == Visibility.Visible)
            {
                grid.Visibility = Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Handles the IsVisibleChanged event of the ErrorsPanel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private void ErrorsPanel_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Grid grid = sender as Grid;
            if (grid != null)
            {
                grid.Focus();
            }
        }
    }
}
