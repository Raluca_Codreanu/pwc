﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Media.Animation;
using ZPKTool.Common;
using ZPKTool.Controls;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Utils;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// The class responsible with the UI elements highlighting during the undo operations.
    /// </summary>
    public class UndoHighlightManager
    {
        #region Constants

        /// <summary>
        /// The UIElement types supported by Undo function.
        /// </summary>
        public static readonly List<Type> HandledUIElementTypes = new List<Type>()
        {
            typeof(TextBox),
            typeof(TextBoxControl),
            typeof(ExtendedTextBoxControl),
            typeof(ComboBox),
            typeof(ComboBoxControl),
            typeof(ExtendedDataGrid),
            typeof(DataGrid),
            typeof(CheckBox),
            typeof(DatePicker),
            typeof(RadioButton),
            typeof(ItemsControl),
            typeof(TextBoxWithUnitSelector),
            typeof(PasswordBox),
            typeof(LabelControl)
        };

        #endregion Constants

        #region Attributes

        /// <summary>
        /// The list of UI elements. Contains the controls, the bindings and the tab items which must be selected.
        /// </summary>
        private List<UIElementData> uiItemsList;

        /// <summary>
        /// The view source DataContext.
        /// </summary>
        private object viewDataContext;

        /// <summary>
        /// Weak event listener for the PropertyChanging notification.
        /// </summary>
        private WeakEventListener<UndoPerformedEventArgs> undoPerformedListener;

        #endregion Attributes

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="UndoHighlightManager"/> class.
        /// </summary>
        /// <param name="undoManager">The undo manager.</param>
        /// <param name="root">The UIElement root; it can be the View, a tab control or any other UIElement.</param>
        /// <param name="dataContext">The view data context.</param>
        public UndoHighlightManager(UndoManager undoManager, UIElement root, object dataContext)
        {
            this.undoPerformedListener = new WeakEventListener<UndoPerformedEventArgs>(this.UndoManager_UndoPerformed);
            UndoPerformedEventManager.AddListener(undoManager, undoPerformedListener);

            this.uiItemsList = this.GenerateUndoElementsList(root);
            this.viewDataContext = dataContext;
        }

        #endregion Constructor

        #region Properties

        /// <summary>
        /// Gets or sets the default TabItem control for the current view instance.
        /// </summary>
        public TabItem DefaultTabItem { get; set; }

        #endregion Properties

        #region Undo handling

        /// <summary>
        /// Handles the UndoPerformed event of UndoManager.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The <see cref="UndoPerformedEventArgs"/> instance containing the event data.</param>
        private void UndoManager_UndoPerformed(object sender, UndoPerformedEventArgs e)
        {
            if (e.Item == null || uiItemsList == null || !uiItemsList.Any())
            {
                return;
            }

            var undoableItem = e.Item;
            var navigateToItem = e.NavigateToItem;

            if (undoableItem.ViewModelInstance != null && this.viewDataContext.GetType() == undoableItem.ViewModelInstance.GetType()
                && this.viewDataContext != undoableItem.ViewModelInstance)
            {
                return;
            }

            var tabSelected = false;

            // Select the tabs that contain view-models. (This operation is executed for the parent view-model of the element on which the undo command was performed.)
            if (undoableItem.ViewModelInstance != null)
            {
                var viewModelControl = this.uiItemsList.FirstOrDefault(p => p.ViewModelType == undoableItem.ViewModelInstance.GetType());
                if (viewModelControl != null)
                {
                    var tabItem = viewModelControl.TabItem as TabItem;
                    if (tabItem != null && navigateToItem)
                    {
                        tabItem.IsSelected = true;
                        tabSelected = true;
                    }

                    if (viewModelControl.ViewModelType == typeof(MediaViewModel)
                        || viewModelControl.ViewModelType == typeof(ClassificationSelectorViewModel)
                        || viewModelControl.ViewModelType == typeof(EntityDocumentsViewModel))
                    {
                        this.HighlightUIElement(viewModelControl.Element);
                    }
                }
            }

            if (!tabSelected)
            {
                var undoableControl = this.uiItemsList.FirstOrDefault(p => p.Bindings != null && p.Bindings.Any(binding => binding != null &&
                    (binding.Path.Path == string.Format("{0}.{1}", undoableItem.ActionParentName, undoableItem.ActionName) || binding.Path.Path == undoableItem.ActionName)));
                if (undoableControl != null)
                {
                    if (navigateToItem)
                    {
                        this.BringUndoableControlIntoView(undoableControl);
                    }

                    // Obtain the UIElement to select and highlight it.
                    var undoUIElement = this.GetElementToSelect(undoableControl.Element, undoableItem.Instance);
                    this.HighlightUIElement(undoUIElement);
                }
                else
                {
                    // Handle the undo selection and highlights, when undo is performed on collections.
                    undoableControl = this.uiItemsList.FirstOrDefault(p => p.Bindings != null && p.Bindings.Any(binding => binding != null &&
                    binding.Path.Path == undoableItem.ActionParentName));
                    if (undoableControl != null)
                    {
                        var undoList = undoableItem.Instance as System.Collections.IList;
                        if (undoList != null)
                        {
                            if (navigateToItem)
                            {
                                this.BringUndoableControlIntoView(undoableControl);
                            }

                            switch (undoableItem.ActionType)
                            {
                                case UndoActionType.Insert:
                                    {
                                        foreach (var item in undoableItem.Values)
                                        {
                                            // Obtain the UIElement to select and highlight it.
                                            var undoUIElement = this.GetElementToSelect(undoableControl.Element, item);
                                            this.HighlightUIElement(undoUIElement);
                                        }

                                        break;
                                    }

                                case UndoActionType.Delete:
                                    {
                                        this.HighlightUIElement(undoableControl.Element);
                                        break;
                                    }

                                case UndoActionType.Move:
                                    {
                                        this.HighlightUIElement(undoableControl.Element);
                                        break;
                                    }

                                default:
                                    break;
                            }
                        }
                    }
                }
            }

            // Special case, navigation to a view property, using item Instance property. (ex. ClassificationSelector View-Model)
            if (undoableItem.Instance != null)
            {
                var rootControl = this.uiItemsList.FirstOrDefault(p => p.ViewModelType == undoableItem.Instance.GetType());
                if (rootControl != null)
                {
                    var tabItem = rootControl.TabItem as TabItem;
                    if (tabItem != null && navigateToItem)
                    {
                        tabItem.IsSelected = true;
                        tabSelected = true;
                    }
                }
            }

            if (!tabSelected && this.DefaultTabItem != null)
            {
                // Select the default tab item of the view.
                this.DefaultTabItem.IsSelected = true;
            }
        }

        /// <summary>
        /// Get the UIElement item whose binding value has changed and which must be selected.
        /// </summary>
        /// <param name="uiElement">The undoable UIElement associate with the binding property on which undo was performed.</param>
        /// <param name="itemData">The undo item data object.</param>
        /// <returns>The item which must be selected.</returns>
        private UIElement GetElementToSelect(UIElement uiElement, object itemData)
        {
            if (uiElement == null || itemData == null)
            {
                return uiElement;
            }

            // If the element to select is a DataGrid object, check if the item data represents an DataGridRow.
            var dataGridElement = uiElement as DataGrid;
            if (dataGridElement != null)
            {
                var datagridRow = dataGridElement.ItemContainerGenerator.ContainerFromItem(itemData) as DataGridRow;
                if (datagridRow != null)
                {
                    return datagridRow;
                }
            }

            return uiElement;
        }

        #endregion Undo handling

        #region View UIElements handling

        /// <summary>
        /// Add a new item to which the Undo must navigate.
        /// </summary>
        /// <param name="uiElement">The item UI element.</param>
        /// <param name="elementBinding">The item binding.</param>
        /// <param name="tabItem">The parent tab item.</param>
        public void AddUndoElement(UIElement uiElement, string elementBinding, UIElement tabItem)
        {
            if (uiElement == null && tabItem as TabItem == null && string.IsNullOrEmpty(elementBinding))
            {
                return;
            }

            var binding = new Binding();
            binding.Path = new PropertyPath(elementBinding);

            this.uiItemsList.Add(new UIElementData { Element = uiElement, TabItem = tabItem, Bindings = new List<Binding>() { binding } });
        }

        /// <summary>
        /// Create the undo UIElements list, based on UIElement root.
        /// </summary>
        /// <param name="root">The root UIElement.</param>
        /// <returns>The generated UI elements list.</returns>
        private List<UIElementData> GenerateUndoElementsList(UIElement root)
        {
            var uiElements = new List<UIElementData>();
            var rootTabControl = root as TabControl;
            if (rootTabControl != null)
            {
                foreach (var tabItem in rootTabControl.Items.OfType<TabItem>())
                {
                    this.GetUIElementsRecursive(tabItem, tabItem, uiElements);
                }
            }
            else
            {
                this.GetUIElementsRecursive(null, root, uiElements);
            }

            return uiElements;
        }

        /// <summary>
        /// Get all the UIElements that support undo from the root object.
        /// </summary>
        /// <param name="tabItem">The tab item, if it exists.</param>
        /// <param name="rootObj">The root dependency object.</param>
        /// <param name="uiElements">The UIElements list that will add elements.</param>
        private void GetUIElementsRecursive(UIElement tabItem, DependencyObject rootObj, List<UIElementData> uiElements)
        {
            if (rootObj != null)
            {
                foreach (var item in LogicalTreeHelper.GetChildren(rootObj))
                {
                    var uiElement = item as UIElement;
                    if (uiElement != null && HandledUIElementTypes.Contains(item.GetType()))
                    {
                        var itemBinding = this.GetElementBindings(uiElement);
                        if (itemBinding != null)
                        {
                            uiElements.Add(new UIElementData { Element = uiElement, TabItem = tabItem, Bindings = itemBinding });
                        }
                    }
                    else if (uiElement != null && uiElement is ContentPresenter)
                    {
                        var elementContent = (uiElement as ContentPresenter).Content;
                        if (elementContent is ViewModel)
                        {
                            uiElements.Add(new UIElementData { Element = uiElement, TabItem = tabItem, ViewModelType = elementContent.GetType() });
                        }
                    }

                    this.GetUIElementsRecursive(tabItem, item as DependencyObject, uiElements);
                }
            }
        }

        /// <summary>
        /// Get an UIElement binding.
        /// </summary>
        /// <param name="item">The UIElement.</param>
        /// <returns>The associated binding.</returns>
        private List<Binding> GetElementBindings(UIElement item)
        {
            if (item == null)
            {
                return null;
            }

            List<Binding> bindings = new List<Binding>();
            var itemType = item.GetType();

            if (itemType == typeof(TextBox))
            {
                bindings.Add(BindingOperations.GetBinding((TextBox)item, TextBox.TextProperty));
            }
            else if (itemType == typeof(TextBoxControl))
            {
                bindings.Add(BindingOperations.GetBinding((TextBoxControl)item, TextBoxControl.TextProperty));
            }
            else if (itemType == typeof(ExtendedTextBoxControl))
            {
                var textBinding = BindingOperations.GetBinding((ExtendedTextBoxControl)item, ExtendedTextBoxControl.TextProperty);
                if (textBinding != null)
                {
                    bindings.Add(textBinding);
                }

                var baseValueBinding = BindingOperations.GetBinding((ExtendedTextBoxControl)item, ExtendedTextBoxControl.BaseValueProperty);
                if (baseValueBinding != null)
                {
                    bindings.Add(baseValueBinding);
                }

                var visibilityBinding = BindingOperations.GetBinding((ExtendedTextBoxControl)item, ExtendedTextBoxControl.VisibilityProperty);
                if (visibilityBinding != null)
                {
                    bindings.Add(visibilityBinding);
                }
            }
            else if (itemType == typeof(ComboBox))
            {
                var selectedItemBinding = BindingOperations.GetBinding((ComboBox)item, ComboBox.SelectedItemProperty);
                if (selectedItemBinding != null)
                {
                    bindings.Add(selectedItemBinding);
                }

                var selectedValueBinding = BindingOperations.GetBinding((ComboBox)item, ComboBox.SelectedValueProperty);
                if (selectedValueBinding != null)
                {
                    bindings.Add(selectedValueBinding);
                }

                var itemSourceBinding = BindingOperations.GetBinding((ComboBox)item, ComboBox.ItemsSourceProperty);
                if (itemSourceBinding != null)
                {
                    bindings.Add(itemSourceBinding);
                }
            }
            else if (itemType == typeof(ComboBoxControl))
            {
                var selectedItemBinding = BindingOperations.GetBinding((ComboBoxControl)item, ComboBoxControl.SelectedItemProperty);
                if (selectedItemBinding != null)
                {
                    bindings.Add(selectedItemBinding);
                }

                var selectedValueBinding = BindingOperations.GetBinding((ComboBoxControl)item, ComboBoxControl.SelectedValueProperty);
                if (selectedValueBinding != null)
                {
                    bindings.Add(selectedValueBinding);
                }

                var itemSourceBinding = BindingOperations.GetBinding((ComboBoxControl)item, ComboBoxControl.ItemsSourceProperty);
                if (itemSourceBinding != null)
                {
                    bindings.Add(itemSourceBinding);
                }
            }
            else if (itemType == typeof(CheckBox))
            {
                bindings.Add(BindingOperations.GetBinding((CheckBox)item, CheckBox.IsCheckedProperty));
            }
            else if (itemType == typeof(RadioButton))
            {
                bindings.Add(BindingOperations.GetBinding((RadioButton)item, RadioButton.IsCheckedProperty));
            }
            else if (itemType == typeof(DatePicker))
            {
                bindings.Add(BindingOperations.GetBinding((DatePicker)item, DatePicker.SelectedDateProperty));
            }
            else if (itemType == typeof(ItemsControl))
            {
                bindings.Add(BindingOperations.GetBinding((ItemsControl)item, ItemsControl.ItemsSourceProperty));
            }
            else if (itemType == typeof(TextBoxWithUnitSelector))
            {
                bindings.Add(BindingOperations.GetBinding(item, TextBoxWithUnitSelector.BaseValueProperty));
                bindings.Add(BindingOperations.GetBinding(item, TextBoxWithUnitSelector.SelectedUnitProperty));
            }
            else if (itemType == typeof(PasswordBox))
            {
                bindings.Add(BindingOperations.GetBinding(item, PasswordBoxAssistant.Password));
            }
            else if (itemType == typeof(LabelControl))
            {
                bindings.Add(BindingOperations.GetBinding(item, LabelControl.TextProperty));
                bindings.Add(BindingOperations.GetBinding(item, LabelControl.VisibilityProperty));
            }
            else if (itemType == typeof(DataGrid))
            {
                bindings.Add(BindingOperations.GetBinding(item, DataGrid.ItemsSourceProperty));
            }

            return bindings;
        }

        #endregion View UIElements handling

        #region Show UndoAdorner

        /// <summary>
        /// Bring into view the control over which the undo function is performed.
        /// </summary>
        /// <param name="undoableControl">The control on which the undo function is performed</param>
        private void BringUndoableControlIntoView(UIElementData undoableControl)
        {
            // If the control, which has the binding for the property on which the undo command was performed, has an TabItem associate, select it.
            var tabItem = undoableControl.TabItem as TabItem;
            if (tabItem != null && tabItem.IsEnabled)
            {
                tabItem.IsSelected = true;
            }

            // If the control, which has the binding for the property on which the undo command was performed, is inside an Expander control, expand it.
            var expanderParent = UIHelper.FindParent<Expander>(undoableControl.Element);
            if (expanderParent != null && expanderParent.IsEnabled)
            {
                expanderParent.IsExpanded = true;
            }
        }

        /// <summary>
        /// Highlight the UIElement passed as parameter.
        /// </summary>
        /// <param name="element">The UIElement.</param>
        private void HighlightUIElement(UIElement element)
        {
            if (element == null)
            {
                return;
            }

            element.Focus();
            var adornerLayer = AdornerLayer.GetAdornerLayer(element);

            // Check if the adorner layer already contains an UndoAdorner for the specified element.
            var undoAdorners = adornerLayer.FindChildren<UndoAdorner>();
            foreach (var adorner in undoAdorners)
            {
                if (adorner.AdornedElement == element)
                {
                    return;
                }
            }

            var undoAdorner = new UndoAdorner(element);
            if (adornerLayer != null)
            {
                adornerLayer.Add(undoAdorner);
            }

            var undoAnimation = new DoubleAnimation();
            undoAnimation.From = 0.0;
            undoAnimation.To = 1.0;
            undoAnimation.Duration = TimeSpan.FromSeconds(1);
            undoAnimation.Completed += (s, e) => this.RemoveUndoHighlightAdorner(adornerLayer, undoAdorner);
            element.BeginAnimation(UIElement.OpacityProperty, undoAnimation);
            ((FrameworkElement)element).Unloaded += (s, e) => this.RemoveUndoHighlightAdorner(adornerLayer, undoAdorner);
        }

        /// <summary>
        /// Handles the completion of  the undo animation.
        /// </summary>
        /// <param name="elementLayer">The adorner layer.</param>
        /// <param name="undoAdorner">The adorner.</param>
        private void RemoveUndoHighlightAdorner(AdornerLayer elementLayer, Adorner undoAdorner)
        {
            if (elementLayer != null)
            {
                elementLayer.Remove(undoAdorner);
            }
        }

        #endregion Show UndoAdorner

        #region Inner classes

        /// <summary>
        /// Class that contains the necessary data to perform the controls selection, on which the undo function has been applied.
        /// </summary>
        private class UIElementData
        {
            #region Properties

            /// <summary>
            /// Gets or sets the UI element on which the Adorner will be applied.
            /// </summary>
            public UIElement Element { get; set; }

            /// <summary>
            /// Gets or sets the UI element TabItem parent.
            /// </summary>
            public UIElement TabItem { get; set; }

            /// <summary>
            /// Gets or sets the UI element binding.
            /// </summary>
            public List<Binding> Bindings { get; set; }

            /// <summary>
            /// Gets or sets the UIElement view-model parent type.
            /// </summary>
            public Type ViewModelType { get; set; }

            #endregion Properties
        }

        #endregion Inner classes
    }
}
