﻿using System.Windows;
using System.Windows.Controls;
using ZPKTool.Common;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for ProjectView.xaml
    /// </summary>
    public partial class ProjectView : UserControl
    {
        #region Attributes

        /// <summary>
        /// The Undo Highlight manager, used to highlight view controls when undo is performed.
        /// </summary>
        private UndoHighlightManager viewManager;

        #endregion Attributes

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectView"/> class.
        /// </summary>
        public ProjectView()
        {
            InitializeComponent();
            this.DataContextChanged += this.OnDataContextChanged;
        }

        #endregion Constructor

        #region Event Handlers

        /// <summary>
        /// Called when [data context changed].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="dependencyPropertyChangedEventArgs">The <see cref="DependencyPropertyChangedEventArgs" /> instance containing the event data.</param>
        private void OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var undoableData = dependencyPropertyChangedEventArgs.NewValue as IUndoable;
            if (undoableData != null)
            {
                if (this.IsLoaded)
                {
                    this.viewManager = new UndoHighlightManager(undoableData.UndoManager, this.MainTabControl, this.DataContext);
                }
                else
                {
                    RoutedEventHandler initOnLoad = null;
                    initOnLoad = (s, e) =>
                    {
                        this.viewManager = new UndoHighlightManager(undoableData.UndoManager, this.MainTabControl, this.DataContext);
                        this.Loaded -= initOnLoad;
                    };
                    this.Loaded += initOnLoad;
                }
            }
            else
            {
                this.viewManager = null;
            }
        }

        /// <summary>
        /// Handles the SelectionChanged event of the MainTabControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Controls.SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void MainTabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count <= 0)
            {
                return;
            }

            var selectedTab = e.AddedItems[0] as TabItem;
            if (selectedTab == null)
            {
                return;
            }

            if (selectedTab.Content is UIElement)
            {
                UIUtils.FocusFirstTextbox(selectedTab.Content);
            }
        }

        /// <summary>
        /// ShiftsperWeekTextBox GotFocus Routed Event Handler
        /// </summary>
        /// <param name="sender">the control that triggered the event</param>
        /// <param name="e">the event args</param>
        private void ShiftsPerWeekTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            decimal val = 0;

            if (decimal.TryParse(HoursPerShiftTextBox.Text, out val) &&
                val > Constants.MinShiftInfoValue &&
                val <= Constants.MaxTotalHoursAndShifts)
            {
                ShiftsPerWeekTextBox.MaxContentLimit = Constants.MaxTotalHoursAndShifts / val;
            }
            else
            {
                ShiftsPerWeekTextBox.MaxContentLimit = Constants.MaxTotalHoursAndShifts;
            }
        }

        /// <summary>
        /// HoursPerShiftTextBox GotFocus Routed Event Handler
        /// </summary>
        /// <param name="sender">the control that triggered the event</param>
        /// <param name="e">the event args</param>
        private void HoursPerShiftTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            decimal val = 0;

            if (decimal.TryParse(ShiftsPerWeekTextBox.Text, out val) &&
                val > Constants.MinShiftInfoValue &&
                val <= Constants.MaxTotalHoursAndShifts)
            {
                HoursPerShiftTextBox.MaxContentLimit = Constants.MaxTotalHoursAndShifts / val;
            }
            else
            {
                HoursPerShiftTextBox.MaxContentLimit = Constants.MaxTotalHoursAndShifts;
            }
        }

        #endregion Event Handlers
    }
}