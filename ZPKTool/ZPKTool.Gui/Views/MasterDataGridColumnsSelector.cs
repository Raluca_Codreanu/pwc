﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Class used to define attached properties.
    /// </summary>
    public static class GridColumnsSelector
    {
        /// <summary>
        /// Using a DependencyProperty as the backing store for ColumnSet.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty ColumnSetProperty = DependencyProperty.RegisterAttached(
            "ColumnSet",
            typeof(Type),
            typeof(GridColumnsSelector),
            new UIPropertyMetadata(OnColumnSetChanged));

        /// <summary>
        /// Gets the column set.
        /// </summary>
        /// <param name="obj">The dependency objects whose property value to return.</param>
        /// <returns>The value of the ColumnSet dependency property.</returns>
        public static Type GetColumnSet(DependencyObject obj)
        {
            return (Type)obj.GetValue(ColumnSetProperty);
        }

        /// <summary>
        /// Sets the column set dependency property.
        /// </summary>
        /// <param name="obj">The dependency objects whose ColumnSet dependency property to set.</param>
        /// <param name="value">The value to set.</param>
        public static void SetColumnSet(DependencyObject obj, Type value)
        {
            obj.SetValue(ColumnSetProperty, value);
        }

        /// <summary>
        /// Called when the ColumnSet property has changed.
        /// </summary>
        /// <param name="depObj">The dependency object whose property changed.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs" /> instance containing the event data.</param>
        private static void OnColumnSetChanged(DependencyObject depObj, DependencyPropertyChangedEventArgs e)
        {
            DataGrid grid = depObj as DataGrid;
            if (grid == null)
            {
                return;
            }

            Type newType = e.NewValue as Type;
            if (newType == null)
            {
                return;
            }
                        
            // load the grid columns from the resources of the data grid based on the type of entity used
            grid.Columns.Clear();
            var columns = grid.Resources[newType] as DataGridColumn[];

            foreach (DataGridColumn column in columns)
            {
                grid.Columns.Add(column);
            }
        }
    }
}
