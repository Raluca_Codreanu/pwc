﻿namespace ZPKTool.Gui.Views
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Windows.Data;

    /// <summary>
    /// Extracts the data (domain) objects from the data source items of the data grids on the ProcessStep view.
    /// </summary>
    public class ProcessStepDataGridSourceItemConverter : IValueConverter
    {
        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var machineDataGridItem = value as ZPKTool.Gui.ViewModels.ProcessInfrastructure.MachineDataGridItem;
            if (machineDataGridItem != null)
            {
                return machineDataGridItem.Machine;
            }

            var dieDataGridItem = value as ZPKTool.Gui.ViewModels.ProcessInfrastructure.DieDataGridItem;
            if (dieDataGridItem != null)
            {
                return dieDataGridItem.Die;
            }

            var consumableDataGridItem = value as ZPKTool.Gui.ViewModels.ProcessInfrastructure.ConsumableDataGridItem;
            if (consumableDataGridItem != null)
            {
                return consumableDataGridItem.Consumable;
            }

            var commodityDataGridItem = value as ZPKTool.Gui.ViewModels.ProcessInfrastructure.CommodityDataGridItem;
            if (commodityDataGridItem != null)
            {
                return commodityDataGridItem.Commodity;
            }

            return value;
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
