﻿using System.Windows;
using System.Windows.Controls;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for MachiningCalculatorView.xaml
    /// </summary>
    public partial class MachiningCalculatorView : UserControl
    {
        #region Attributes

        /// <summary>
        /// The Undo Highlight manager, used to highlight view controls when undo is performed.
        /// </summary>
        private UndoHighlightManager viewManager;

        #endregion Attributes

        /// <summary>
        /// Initializes a new instance of the <see cref="MachiningCalculatorView"/> class.
        /// </summary>
        public MachiningCalculatorView()
        {
            InitializeComponent();
            this.DataContextChanged += OnDataContextChanged;
        }

        /// <summary>
        /// Called when [data context changed].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="dependencyPropertyChangedEventArgs">The <see cref="DependencyPropertyChangedEventArgs" /> instance containing the event data.</param>
        private void OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var undoableData = dependencyPropertyChangedEventArgs.NewValue as IUndoable;
            if (undoableData != null)
            {
                if (this.IsLoaded)
                {
                    this.viewManager = new UndoHighlightManager(undoableData.UndoManager, this.MainTab, this.DataContext);
                }
                else
                {
                    RoutedEventHandler initOnLoad = null;
                    initOnLoad = (s, e) =>
                    {
                        this.viewManager = new UndoHighlightManager(undoableData.UndoManager, this.MainTab, this.DataContext);
                        this.Loaded -= initOnLoad;
                    };
                    this.Loaded += initOnLoad;
                }
            }
            else
            {
                this.viewManager = null;
            }
        }
    }
}
