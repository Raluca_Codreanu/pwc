﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for ProcessView.xaml
    /// </summary>
    public partial class ProcessView : UserControl
    {
        /// <summary>
        /// The minimum scale ratio available for zoom actions
        /// </summary>
        private const double MinimumScaleRatio = 0.7;

        /// <summary>
        /// The maximum scale ratio available for zoom actions
        /// </summary>
        private const double MaximumScaleRatio = 1.5;

        /// <summary>
        /// User to scroll the steps to the right while the left mouse buttons is kept pressed.
        /// </summary>
        private DispatcherTimer scrollRightTimer;

        /// <summary>
        /// User to scroll the steps to the left while the left mouse buttons is kept pressed.
        /// </summary>
        private DispatcherTimer scrollLeftTimer;

        /// <summary>
        /// The last horizontal scroll offset of the <see cref="MainProcessStepsWidgetScrollViewer"/>
        /// In some cases if the scroll viewer contains elements with margins, it consider the margins as not representing visual data
        /// and automatically scrolls until it find the first visual element, so needs to be scrolled back to it's last position 
        /// </summary>
        private double lastHorizontalScrollOffset;

        /// <summary>
        /// The Undo Highlight manager, used to highlight view controls when undo is performed.
        /// </summary>
        private UndoHighlightManager viewManager;

        /// <summary>
        /// Using a DependencyProperty as the backing store for SelectedWidget.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty SelectedWidgetProperty =
            DependencyProperty.Register("SelectedWidget", typeof(object), typeof(ProcessView), new PropertyMetadata(null, SelectedWidgetChanged));

        /// <summary>
        /// Using a DependencyProperty as the backing store for IsReadOnly.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty IsReadOnlyProperty =
            DependencyProperty.Register("IsReadOnly", typeof(bool), typeof(ProcessView), new PropertyMetadata(false));

        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessView"/> class.
        /// </summary>
        public ProcessView()
        {
            InitializeComponent();
            this.DataContextChanged += this.OnDataContextChanged;
            this.Co2EmissionsChart.TotalLabelControl.Symbol = "g";
            ScaleRatio = 1.0;
            this.lastHorizontalScrollOffset = 0;

            this.InitializeTimers();

            var selectedWidgetBinding = new Binding("SelectedProcessStepWidget");
            selectedWidgetBinding.Mode = BindingMode.TwoWay;
            BindingOperations.SetBinding(this, SelectedWidgetProperty, selectedWidgetBinding);

            var isReadOnlyBinding = new Binding("IsReadOnly");
            isReadOnlyBinding.Mode = BindingMode.OneWay;
            BindingOperations.SetBinding(this, IsReadOnlyProperty, isReadOnlyBinding);
        }

        /// <summary>
        /// Gets or sets the scale of the arrows and widgets
        /// </summary>
        public static double ScaleRatio { get; set; }

        /// <summary>
        /// Gets or sets the point used to determine the move direction and distance
        /// </summary>
        private Point OriginalPoint { get; set; }

        /// <summary>
        /// Gets or sets the widget that is selected
        /// </summary>
        public object SelectedWidget
        {
            get { return (object)GetValue(SelectedWidgetProperty); }
            set { SetValue(SelectedWidgetProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the view is read only or not
        /// </summary>
        public bool IsReadOnly
        {
            get { return (bool)GetValue(IsReadOnlyProperty); }
            set { SetValue(IsReadOnlyProperty, value); }
        }

        /// <summary>
        /// Called when the SelectedWidget property is changed.
        /// </summary>
        /// <param name="d">The dependency object.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void SelectedWidgetChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var process = d as ProcessView;
            if (process != null)
            {
                var widget = process.ProcessStepWidgetItemsControl.FindChildren<ProcessStepWidgetView>().FirstOrDefault((w) => w.DataContext == process.SelectedWidget);
                process.BringIntoViewSelectedWidget();
            }
        }

        /// <summary>
        /// Initialize the timers used for scrolling.
        /// </summary>
        private void InitializeTimers()
        {
            scrollLeftTimer = new DispatcherTimer();
            scrollLeftTimer.Interval = new TimeSpan(0, 0, 0, 0, 45);
            scrollLeftTimer.Tick += new EventHandler(ScrollLeftTimer_Tick);

            scrollRightTimer = new DispatcherTimer();
            scrollRightTimer.Interval = new TimeSpan(0, 0, 0, 0, 45);
            scrollRightTimer.Tick += new EventHandler(ScrollRightTimer_Tick);
        }

        /// <summary>
        /// Called when [data context changed].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="dependencyPropertyChangedEventArgs">The <see cref="DependencyPropertyChangedEventArgs" /> instance containing the event data.</param>
        private void OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var undoableData = dependencyPropertyChangedEventArgs.NewValue as IUndoable;
            if (undoableData != null)
            {
                if (this.IsLoaded)
                {
                    this.viewManager = new UndoHighlightManager(undoableData.UndoManager, this.MainTabControl, this.DataContext);
                    viewManager.AddUndoElement(this.MainProcessStepsWidgetRoot, "ProcessStepViewModelItems", this.ProcessViewTab);
                    viewManager.DefaultTabItem = this.ProcessViewTab;
                }
                else
                {
                    RoutedEventHandler initOnLoad = null;
                    initOnLoad = (s, e) =>
                    {
                        this.viewManager = new UndoHighlightManager(undoableData.UndoManager, this.MainTabControl, this.DataContext);
                        viewManager.AddUndoElement(this.MainProcessStepsWidgetRoot, "ProcessStepViewModelItems", this.ProcessViewTab);
                        viewManager.DefaultTabItem = this.ProcessViewTab;
                        this.Loaded -= initOnLoad;
                    };
                    this.Loaded += initOnLoad;
                }
            }
            else
            {
                this.viewManager = null;
            }
        }

        #region Process view

        /// <summary>
        /// Scrolls the process step controls a fixed distance to the right.
        /// </summary>
        public void ScrollToRight()
        {
            this.MainProcessStepsWidgetScrollViewer.LineLeft();
        }

        /// <summary>
        /// Scrolls the process step controls a fixed distance to the left.
        /// </summary>
        public void ScrollToLeft()
        {
            this.MainProcessStepsWidgetScrollViewer.LineRight();
        }

        /// <summary>
        /// Handles the Click event of the ZoomOutButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void ZoomOutButton_Click(object sender, RoutedEventArgs e)
        {
            TryToZoomOut();
        }

        /// <summary>
        /// Handles the Click event of the ZoomInButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void ZoomInButton_Click(object sender, RoutedEventArgs e)
        {
            TryToZoomIn();
        }

        /// <summary>
        /// Zooming out if needed
        /// </summary>
        private void TryToZoomOut()
        {
            if (ScaleRatio > MinimumScaleRatio)
            {
                ScaleRatio -= 0.1;
                Resize(ScaleRatio); // send old scale ratio as a parameter

                if (ScaleRatio >= 1)
                {
                    this.ProcessStepWidgetRoot.MinWidth = (this.ProcessStepWidgetRoot.ActualWidth / (ScaleRatio + 0.1)) * ScaleRatio;
                }

                this.BringIntoViewSelectedWidget();
            }

            this.RefreshZoomButtonsAvailability();
        }

        /// <summary>
        /// Zooming in if needed
        /// </summary>
        private void TryToZoomIn()
        {
            if (ScaleRatio < MaximumScaleRatio)
            {
                ScaleRatio += 0.1;
                Resize(ScaleRatio); // send old scale ratio as a parameter

                if (ScaleRatio > 1)
                {
                    this.ProcessStepWidgetRoot.MinWidth = (this.ProcessStepWidgetRoot.ActualWidth / (ScaleRatio - 0.1)) * ScaleRatio;
                }

                this.BringIntoViewSelectedWidget();
            }

            this.RefreshZoomButtonsAvailability();
        }

        /// <summary>
        /// Refresh the zoom buttons availability regarding the global scale ratio of the widgets
        /// </summary>
        private void RefreshZoomButtonsAvailability()
        {
            if (ScaleRatio < MinimumScaleRatio)
            {
                // If scale ratio is less then the minimum ratio scale the zoom out button is disabled
                this.ZoomOutButton.IsEnabled = false;
                this.ZoomInButton.IsEnabled = true;
            }
            else if (ScaleRatio > MaximumScaleRatio)
            {
                // If scale ratio is less then the maximum ratio scale the zoom in button is disabled
                this.ZoomInButton.IsEnabled = false;
                this.ZoomOutButton.IsEnabled = true;
            }
            else
            {
                // If scale ratio is between then the minimum and maximum ratio scale all the zoom buttons must be enabled
                this.ZoomInButton.IsEnabled = true;
                this.ZoomOutButton.IsEnabled = true;
            }
        }

        /// <summary>
        /// Resize the whole process step list
        /// </summary>
        /// <param name="oldScaleRatio">The old scale ratio.</param>
        private void Resize(double oldScaleRatio)
        {
            this.ProcessStepWidgetItemsControl.RenderTransform = new ScaleTransform(oldScaleRatio, oldScaleRatio);
        }

        /// <summary>
        /// Handles the SizeChanged event of the items control that contains the process stepWidget
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SizeChangedEventArgs" /> instance containing the event data.</param>
        private void ProcessStepWidgetItemsControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (ScaleRatio > 1)
            {
                this.ProcessStepWidgetRoot.MinWidth = this.ProcessStepWidgetItemsControl.ActualWidth * ScaleRatio;
            }
        }

        /// <summary>
        /// Handles the PreviewMouseWheel event of the ProcessGrid control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseWheelEventArgs"/> instance containing the event data.</param>
        private void ProcessGrid_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (e.Delta > 0)
            {   // if the wheel is scrolled upwards we try to zoom in
                TryToZoomIn();
            }
            else
            {   // if the wheel is scrolled downwards we try to zoom out
                TryToZoomOut();
            }
        }

        /// <summary>
        /// Handles the PreviewMouseRightButtonUp event of the ProcessGrid control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void ProcessGrid_PreviewMouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            // Set the cursor to normal arrow
            this.ProcessGrid.Cursor = Cursors.Arrow;
        }

        /// <summary>
        /// Handles the PreviewMouseRightButtonDown event of the ProcessGrid control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void ProcessGrid_PreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            // Set the cursor to hand
            this.ProcessGrid.Cursor = Images.HandCursor;
        }

        /// <summary>
        /// Handles the MouseMove event of the ProcessGrid control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseEventArgs"/> instance containing the event data.</param>
        private void ProcessGrid_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.RightButton == MouseButtonState.Pressed)
            {
                // set the catch cursor
                this.ProcessGrid.Cursor = Images.CatchHandCursor;

                // get the new point
                double newPointX = e.GetPosition(this).X;

                // if the distance is long enough between the two points (original point and newPoint)
                if (Math.Abs(OriginalPoint.X - newPointX) > 0)
                {
                    // get the direction of movement
                    var offsetX = newPointX - OriginalPoint.X;
                    this.MainProcessStepsWidgetScrollViewer.ScrollToHorizontalOffset(this.MainProcessStepsWidgetScrollViewer.HorizontalOffset - offsetX);
                    this.lastHorizontalScrollOffset = this.MainProcessStepsWidgetScrollViewer.HorizontalOffset;

                    // set the new point as the original point for later moves
                    OriginalPoint = e.GetPosition(this);
                }
            }
            else
            {   // if the right mouse button isn't pressed just store the point where the mouse is right now
                OriginalPoint = e.GetPosition(this);
            }
        }

        /// <summary>
        /// Handles the Tick event of the ScrollRightTimer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ScrollRightTimer_Tick(object sender, EventArgs e)
        {
            this.MainProcessStepsWidgetScrollViewer.LineLeft();
            this.lastHorizontalScrollOffset = this.MainProcessStepsWidgetScrollViewer.HorizontalOffset;
        }

        /// <summary>
        /// Handles the Tick event of the ScrollLeftTimer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ScrollLeftTimer_Tick(object sender, EventArgs e)
        {
            this.MainProcessStepsWidgetScrollViewer.LineRight();
            this.lastHorizontalScrollOffset = this.MainProcessStepsWidgetScrollViewer.HorizontalOffset;
        }

        /// <summary>
        /// Handles the MouseLeftButtonDown event of the ProcessScrollLeftArrow control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void ProcessScrollLeftArrow_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (null != scrollLeftTimer)
            {
                scrollLeftTimer.Start();
            }
        }

        /// <summary>
        /// Handles the MouseLeftButtonUp event of the ProcessScrollLeftArrow control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void ProcessScrollLeftArrow_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (null != scrollLeftTimer)
            {
                scrollLeftTimer.Stop();
            }
        }

        /// <summary>
        /// Handles the MouseLeave event of the ProcessScrollLeftArrow control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void ProcessScrollLeftArrow_MouseLeave(object sender, MouseEventArgs e)
        {
            if (null != scrollLeftTimer)
            {
                scrollLeftTimer.Stop();
            }
        }

        /// <summary>
        /// Handles the MouseLeftButtonDown event of the ProcessScrollRightArrow control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void ProcessScrollRightArrow_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (null != scrollRightTimer)
            {
                scrollRightTimer.Start();
            }
        }

        /// <summary>
        /// Handles the MouseLeftButtonUp event of the ProcessScrollRightArrow control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void ProcessScrollRightArrow_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (null != scrollRightTimer)
            {
                scrollRightTimer.Stop();
            }
        }

        /// <summary>
        /// Handles the MouseLeave event of the ProcessScrollRightArrow control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void ProcessScrollRightArrow_MouseLeave(object sender, MouseEventArgs e)
        {
            if (null != scrollRightTimer)
            {
                scrollRightTimer.Stop();
            }
        }

        /// <summary>
        /// Handles the Loaded event of the MainProcessStepsWidgetScrollViewer control.
        /// In some cases if the scroll viewer contains elements with margins, it consider the margins as not representing visual data
        /// and automatically scrolls until it find the first visual element, so needs to be scrolled back to it's last position 
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.RoutedEventArgs"/> instance containing the event data.</param>
        private void MainProcessStepsWidgetScrollViewer_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.lastHorizontalScrollOffset != this.MainProcessStepsWidgetScrollViewer.HorizontalOffset)
            {
                this.MainProcessStepsWidgetScrollViewer.ScrollToHorizontalOffset(this.lastHorizontalScrollOffset);
            }
        }

        /// <summary>
        /// Handles the MouseLeftButtonUp event of the ProcessStepWidgetView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void ProcessStepWidgetView_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            object selectedWidget = null;
            FrameworkElement sourceItemContainer = this.ProcessStepWidgetItemsControl.ContainerFromElement((DependencyObject)e.OriginalSource) as FrameworkElement;
            if (sourceItemContainer != null)
            {
                selectedWidget = this.ProcessStepWidgetItemsControl.ItemContainerGenerator.ItemFromContainer(sourceItemContainer);
            }

            this.SelectedWidget = selectedWidget;
        }

        /// <summary>
        /// Tries to bring into view the current process step widget
        /// </summary>
        private void BringIntoViewSelectedWidget()
        {
            var widget = this.ProcessStepWidgetItemsControl.FindChildren<ProcessStepWidgetView>().FirstOrDefault((w) => w.DataContext == this.SelectedWidget);
            if (widget != null)
            {
                var widgetPanel = this.ProcessStepWidgetItemsControl.FindChildren<StackPanel>().FirstOrDefault((s) => s.Children.Contains(widget));
                if (widgetPanel != null)
                {
                    widgetPanel.BringIntoView();
                }
            }
        }

        /// <summary>
        /// Handles the MouseLeave event of the ProcessGrid.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void ProcessGrid_MouseLeave(object sender, MouseEventArgs e)
        {
            // In some cases this event is triggered after the view is unloaded
            // and in this case we don't have any visual elements anymore to apply the storyboard to
            if (!this.IsLoaded)
            {
                return;
            }

            // Search the storyboard to hide the arrows on process
            var storyboard = this.ProcessGrid.FindResource("HideProcessScrollArrows") as Storyboard;
            if (storyboard != null)
            {
                storyboard.Begin();
            }
        }

        #endregion Process view

        #region Process view Drag&Drop

        /// <summary>
        /// Handles the Loaded event of the process arrow widget.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void ProcessStepWidgetView_Loaded(object sender, RoutedEventArgs e)
        {
            var element = sender as UIElement;
            if (element != null)
            {
                // If the widget is the selected item try to select it
                var widget = sender as ProcessStepWidgetView;
                if (widget != null && widget.DataContext == this.SelectedWidget)
                {
                    this.BringIntoViewSelectedWidget();
                }

                if (!this.IsReadOnly)
                {
                    var dragHelper = new ProcessStepDragHelper(element, this);
                }
            }
        }

        /// <summary>
        /// Handles the DragLeave event of the RightDropGrid.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        private void RightDropGrid_DragLeave(object sender, DragEventArgs e)
        {
            var arrowRoot = sender as Grid;
            if (arrowRoot != null)
            {
                var widget = arrowRoot.Parent.FindChildren<ProcessStepWidgetView>().FirstOrDefault();
                if (widget != null)
                {
                    if (widget.IsLastWidget)
                    {
                        arrowRoot.Background = new SolidColorBrush(Colors.Transparent);
                    }
                }
            }

            if (arrowRoot != null)
            {
                var storyboard = arrowRoot.TryFindResource("ProcessArrow_MouseLeave") as Storyboard;
                if (storyboard != null)
                {
                    storyboard.Begin();
                }
            }
        }

        /// <summary>
        /// Checks the drop target
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        private void RightDropGrid_CheckDropTarget(object sender, DragEventArgs e)
        {
            e.Handled = true;
            var arrowRoot = sender as Grid;
            if (arrowRoot != null)
            {
                var widget = arrowRoot.Parent.FindChildren<ProcessStepWidgetView>().FirstOrDefault();
                if (widget != null)
                {
                    if (widget.IsLastWidget)
                    {
                        arrowRoot.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF808080"));
                        arrowRoot.Background.Opacity = 0.25d;
                    }
                }
            }

            if (e.Data.GetData(typeof(UIElement)) is ProcessStepWidgetView)
            {
                e.Effects = DragDropEffects.Move;

                if (arrowRoot != null)
                {
                    var storyboard = arrowRoot.TryFindResource("ProcessArrow_MouseOver") as Storyboard;
                    if (storyboard != null)
                    {
                        storyboard.Begin();
                    }
                }
            }
            else
            {
                e.Effects = DragDropEffects.None;
            }
        }

        /// <summary>
        /// Handles the ConvertEventArgs event of the <see cref="EventToCommand" /> that routes the Drop event of the ProcessArrowGrid to a command.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="args">The event arguments of the PreviewKeyDown event.</param>
        /// <returns>
        /// The data item of the clicked data grid row.
        /// </returns>
        private object ProcessStepDropCommand_ConvertEventArgs(object sender, RoutedEventArgs args)
        {
            object sourceWidget = null;
            object targetWidget = null;

            FrameworkElement targetItemContainer = this.ProcessStepWidgetItemsControl.ContainerFromElement((DependencyObject)args.OriginalSource) as FrameworkElement;
            if (targetItemContainer != null)
            {
                targetWidget = this.ProcessStepWidgetItemsControl.ItemContainerGenerator.ItemFromContainer(targetItemContainer);
            }

            var dragArgs = args as DragEventArgs;
            if (dragArgs != null)
            {
                var data = dragArgs.Data.GetData(typeof(UIElement));
                if (data != null)
                {
                    FrameworkElement sourceItemContainer = this.ProcessStepWidgetItemsControl.ContainerFromElement((DependencyObject)data) as FrameworkElement;
                    if (sourceItemContainer != null)
                    {
                        sourceWidget = this.ProcessStepWidgetItemsControl.ItemContainerGenerator.ItemFromContainer(sourceItemContainer);
                    }
                }
            }

            return new Tuple<object, object>(sourceWidget, targetWidget);
        }

        /// <summary>
        /// Checks the drop target
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        private void LeftDropGrid_CheckDropTarget(object sender, DragEventArgs e)
        {
            e.Handled = true;
            var arrowRoot = sender as Grid;
            if (arrowRoot != null)
            {
                var widget = arrowRoot.Parent.FindChildren<ProcessStepWidgetView>().FirstOrDefault();
                if (widget != null)
                {
                    if (widget.IsFirstWidget)
                    {
                        arrowRoot.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF808080"));
                        arrowRoot.Background.Opacity = 0.25d;
                    }
                }
            }

            if (e.Data.GetData(typeof(UIElement)) is ProcessStepWidgetView)
            {
                e.Effects = DragDropEffects.Move;
            }
            else
            {
                e.Effects = DragDropEffects.None;
            }
        }

        /// <summary>
        /// Handles the DragLeave event of the LeftDropGrid.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        private void LeftDropGrid_DragLeave(object sender, DragEventArgs e)
        {
            var arrowRoot = sender as Grid;
            if (arrowRoot != null)
            {
                var widget = arrowRoot.Parent.FindChildren<ProcessStepWidgetView>().FirstOrDefault();
                if (widget != null)
                {
                    if (widget.IsFirstWidget)
                    {
                        arrowRoot.Background = new SolidColorBrush(Colors.Transparent);
                    }
                }
            }
        }

        #endregion Process view Drag&Drop

        #region CO2 Emissions

        /// <summary>
        /// Handles the SelectionChanged event of the CO2 emission by process chart.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.Controls.SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void CO2ChartStackColumnSeries_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // If the chart is not loaded and the chart Selected Item property is set, due a wpf toolkit bug it will generate an exception
            // To solve this problem the selected item value is stored locally and the selected item property is reset
            // After the chart is loaded the stored selected value is restored to the Selected Item property from chart
            if (!this.CO2ChartStackColumnSeries.IsLoaded)
            {
                if (this.CO2ChartStackColumnSeries.SelectedItem != null)
                {
                    var selectedItem = this.CO2ChartStackColumnSeries.SelectedItem;
                    this.CO2ChartStackColumnSeries.SelectedItem = null;

                    var co2ChartLoaded = new RoutedEventHandler((s, arg) =>
                    {
                        this.CO2ChartStackColumnSeries.SelectedItem = selectedItem;
                    });

                    this.CO2ChartStackColumnSeries.Loaded += co2ChartLoaded;
                    this.CO2ChartStackColumnSeries.Unloaded -= co2ChartLoaded;
                }
            }
        }

        /// <summary>
        /// Handles the SelectionChanged event of the CO2 emission data grid.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.Controls.SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void Co2EmissionsDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.Co2EmissionsDataGrid.SelectedItem != null)
            {
                this.Co2EmissionsDataGrid.ScrollIntoView(this.Co2EmissionsDataGrid.SelectedItem);
            }
        }

        #endregion CO2 Emissions

        #region Scrap view

        /// <summary>
        /// Handles the IsVisibleChanged event of the ScrapView tab item on process view.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private void ScrapViewTab_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            // If the scrap view tab is not visible and it was previously selected needs to reset the tab control selection
            // by selecting the first tab ( process view tab )
            var tabItem = sender as TabItem;
            if (tabItem != null && tabItem.Visibility != Visibility.Visible && this.MainTabControl.SelectedItem == tabItem)
            {
                this.MainTabControl.SelectedIndex = 0;
            }
        }

        #endregion Scrap view
    }
}
