﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// DragHelperClass. The class in which are implemented the event handlers for a draggable control.
    /// </summary>
    public class ProcessStepDragHelper : DependencyObject
    {
        /// <summary>
        /// The adorner associated with drag scope.
        /// </summary>
        private ProcessStepDragAdorner adorner;

        /// <summary>
        /// Represents a "timer" which shows when to execute the moving of process steps.
        /// </summary>
        private int dragOverTriggeredCount = 0;

        /// <summary>
        /// The adorner layer.
        /// </summary>
        private AdornerLayer layer;

        /// <summary>
        /// Start point for dragging.
        /// </summary>
        private Point startPoint;

        /// <summary>
        /// The bool var which shows if the drag source has left the drag scope.
        /// </summary>
        private bool dragHasLeftScope = false;

        /// <summary>
        /// The scroll left rectangle.
        /// </summary>
        private Rectangle leftRectangle = new Rectangle();

        /// <summary>
        /// The scroll right rectangle.
        /// </summary>
        private Rectangle rightRectangle = new Rectangle();

        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessStepDragHelper"/> class.
        /// </summary>
        /// <param name="source">The source element</param>
        /// <param name="control">The process control.</param>
        public ProcessStepDragHelper(UIElement source, ProcessView control)
        {
            this.Control = control;
            this.DragScope = control.MainProcessStepsWidgetRoot;
            this.DragSource = source;
            WireEvents(source, this.DragScope);
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is dragging.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is dragging; otherwise, <c>false</c>.
        /// </value>
        public bool IsDragging { get; set; }

        /// <summary>
        /// Gets or sets the drag source.
        /// </summary>
        /// <value>The drag source.</value>
        public UIElement DragSource { get; set; }

        /// <summary>
        /// Gets or sets the drag scope which represents the area in which the drag source can be moved.
        /// </summary>
        /// <value>The drag scope.</value>
        public UIElement DragScope { get; set; }

        /// <summary>
        /// Gets or sets the ProcessControl which instantiate a DragHelper. 
        /// </summary>
        public ProcessView Control { get; set; }

        /// <summary>
        /// Wires the events for drag source.
        /// </summary>
        /// <param name="uie">The drag source.</param>
        /// <param name="scope">The drag scope.</param>
        private void WireEvents(UIElement uie, Visual scope)
        {
            if (uie != null)
            {
                uie.PreviewMouseLeftButtonDown += new MouseButtonEventHandler(DragSource_PreviewMouseLeftButtonDown);
                uie.PreviewMouseMove += new System.Windows.Input.MouseEventHandler(DragSource_PreviewMouseMove);
            }
        }

        /// <summary>
        /// Handles the PreviewMouseLeftButtonDown event of the DragSource control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void DragSource_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            startPoint = e.GetPosition(null);
        }

        /// <summary>
        /// Handles the PreviewMouseMove event of the DragSource control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseEventArgs"/> instance containing the event data.</param>
        private void DragSource_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed && !IsDragging)
            {
                Point currentMousePosition = e.GetPosition(null);
                double moveDeltaX = Math.Abs(currentMousePosition.X - this.startPoint.X);
                double moveDeltaY = Math.Abs(currentMousePosition.Y - this.startPoint.Y);
                if (moveDeltaX <= SystemParameters.MinimumHorizontalDragDistance + 8d
                    && moveDeltaY <= SystemParameters.MinimumVerticalDragDistance + 8d)
                {
                    return;
                }

                if (true)
                {
                    StartDrag(e);
                }
            }
        }

        /// <summary>
        /// Translates a html hexadecimal definition of a color into a .NET Framework Color.
        /// The input string contains 6 hexadecimal digits. The digits A-F are not case sensitive.
        /// If the conversion was not successful the color Gray will be returned.
        /// </summary>
        /// <param name="hexString"> html hexadecimal </param>
        /// <returns>converted color. </returns>
        public static Color HexToColor(string hexString)
        {
            Color actColor = Colors.Gray;

            byte a = System.Convert.ToByte(hexString.Substring(0, 2), 16);
            byte r = System.Convert.ToByte(hexString.Substring(2, 2), 16);
            byte g = System.Convert.ToByte(hexString.Substring(4, 2), 16);
            byte b = System.Convert.ToByte(hexString.Substring(6, 2), 16);

            actColor = Color.FromArgb(a, r, g, b);

            return actColor;
        }

        /// <summary>
        /// Starts the drag of the control.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.Input.MouseEventArgs"/> instance containing the event data.</param>
        private void StartDrag(MouseEventArgs e)
        {
            IDataObject data = null;
            QueryContinueDragEventHandler queryhandler = null;
            GiveFeedbackEventHandler feedbackhandler = null;
            DragEventHandler draghandler = null;
            DragEventHandler dragleavehandler = null;

            leftRectangle.Width = 30;
            leftRectangle.Fill = new SolidColorBrush(HexToColor("80808080"));
            leftRectangle.Height = this.Control.MainProcessStepsWidgetRoot.ActualHeight;
            rightRectangle.Width = 30;
            rightRectangle.Fill = new SolidColorBrush(HexToColor("80808080"));
            rightRectangle.Height = this.Control.MainProcessStepsWidgetRoot.ActualHeight;
            leftRectangle.Visibility = Visibility.Hidden;
            leftRectangle.HorizontalAlignment = HorizontalAlignment.Left;
            rightRectangle.Visibility = Visibility.Hidden;
            rightRectangle.HorizontalAlignment = HorizontalAlignment.Right;

            double gridOffest = this.Control.MainProcessStepsWidgetRoot.ActualWidth - leftRectangle.Width;
            Canvas.SetLeft(rightRectangle, gridOffest);
            Canvas.SetRight(leftRectangle, gridOffest);
            Canvas.SetZIndex(leftRectangle, 5);
            Canvas.SetZIndex(rightRectangle, 5);

            if (!this.Control.MainProcessStepsWidgetRoot.Children.Contains(rightRectangle))
            {
                this.Control.MainProcessStepsWidgetRoot.Children.Add(rightRectangle);
            }

            if (!this.Control.MainProcessStepsWidgetRoot.Children.Contains(leftRectangle))
            {
                this.Control.MainProcessStepsWidgetRoot.Children.Add(leftRectangle);
            }

            // Enable Drag & Drop in the scope (this allows us to get DragOver) 
            bool previousDrop = DragScope.AllowDrop;
            this.DragScope.AllowDrop = true;

            // Wire the events. 
            // GiveFeedback just tells it to use no standard cursors.
            feedbackhandler = new GiveFeedbackEventHandler(DragSource_GiveFeedback);
            this.DragSource.GiveFeedback += feedbackhandler;

            // The DragOver event . 
            draghandler = new DragEventHandler(DragScope_DragOver);
            this.DragScope.PreviewDragOver += draghandler;

            // Drag Leave event. 
            dragleavehandler = new DragEventHandler(DragScope_DragLeave);
            this.DragScope.DragLeave += dragleavehandler;

            // QueryContinue Drag goes with drag leave. 
            queryhandler = new QueryContinueDragEventHandler(DragScope_QueryContinueDrag);
            this.DragScope.QueryContinueDrag += queryhandler;

            // Create the  adorner.
            adorner = new ProcessStepDragAdorner(DragScope, (UIElement)this.DragSource, true, 0.5);
            layer = AdornerLayer.GetAdornerLayer(DragScope as Visual);
            layer.Add(adorner);

            IsDragging = true;
            dragHasLeftScope = false;
            data = new DataObject(typeof(UIElement).ToString(), this.DragSource);
            DragDropEffects de = DragDrop.DoDragDrop(this.DragSource, data, DragDropEffects.Move);

            // Clean up after the drag source was dragged.
            DragScope.AllowDrop = previousDrop;
            AdornerLayer.GetAdornerLayer(DragScope).Remove(adorner);
            adorner = null;
            if (this.Control.MainProcessStepsWidgetRoot.Children.Contains(leftRectangle))
            {
                this.Control.MainProcessStepsWidgetRoot.Children.Remove(leftRectangle);
            }

            if (this.Control.MainProcessStepsWidgetRoot.Children.Contains(rightRectangle))
            {
                this.Control.MainProcessStepsWidgetRoot.Children.Remove(rightRectangle);
            }

            DragSource.GiveFeedback -= feedbackhandler;
            DragScope.PreviewDragOver -= draghandler;
            DragScope.DragLeave -= dragleavehandler;
            DragScope.QueryContinueDrag -= queryhandler;
            IsDragging = false;
        }

        /// <summary>
        /// Handles the GiveFeedback event of the DragSource control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.GiveFeedbackEventArgs"/> instance containing the event data.</param>
        private void DragSource_GiveFeedback(object sender, GiveFeedbackEventArgs e)
        {
            e.UseDefaultCursors = true;
            e.Handled = true;
        }

        /// <summary>
        /// Handles the DragOver event of the DragScope control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="args">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        private void DragScope_DragOver(object sender, DragEventArgs args)
        {
            if (adorner != null)
            {
                adorner.LeftOffset = args.GetPosition(DragScope).X;
                adorner.TopOffset = args.GetPosition(DragScope).Y;
            }

            // Execute the moving of steps only the second time this event is triggered
            this.dragOverTriggeredCount++;
            if (this.dragOverTriggeredCount < 2)
            {
                return;
            }
            else
            {
                this.dragOverTriggeredCount = 0;
            }

            Point mousePoint = args.GetPosition(this.Control.MainProcessStepsWidgetRoot);
            double leftGridOffest = this.Control.MainProcessStepsWidgetRoot.ActualWidth - Canvas.GetRight(leftRectangle);
            double rightGridOffset = Canvas.GetLeft(rightRectangle);

            if (mousePoint.X >= rightGridOffset)
            {
                rightRectangle.Visibility = Visibility.Visible;
                this.Control.ScrollToLeft();
            }
            else if (mousePoint.X <= leftGridOffest)
            {
                leftRectangle.Visibility = Visibility.Visible;
                this.Control.ScrollToRight();
            }

            if (rightGridOffset > mousePoint.X && mousePoint.X > leftGridOffest)
            {
                if (rightRectangle.Visibility == Visibility.Visible)
                {
                    rightRectangle.Visibility = Visibility.Hidden;
                }
                else
                {
                    leftRectangle.Visibility = Visibility.Hidden;
                }
            }
        }

        /// <summary>
        /// Handles the DragLeave event of the DragScope control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        private void DragScope_DragLeave(object sender, DragEventArgs e)
        {
            if (e.OriginalSource == DragScope)
            {
                Point p = e.GetPosition(DragScope);
                Rect r = VisualTreeHelper.GetContentBounds(DragScope);

                if (!r.Contains(p))
                {
                    this.dragHasLeftScope = true;
                    e.Handled = true;
                }
            }
        }

        /// <summary>
        /// Handles the QueryContinueDrag event of the DragScope control. If dragging is leaved, in another place than drop target, 
        /// the drag action is canceled.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.QueryContinueDragEventArgs"/> instance containing the event data.</param>
        private void DragScope_QueryContinueDrag(object sender, QueryContinueDragEventArgs e)
        {
            if (this.dragHasLeftScope)
            {
                e.Action = DragAction.Cancel;
                e.Handled = true;
            }
        }
    }
}
