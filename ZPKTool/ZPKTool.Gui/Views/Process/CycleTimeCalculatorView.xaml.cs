﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for CycleTimeCalculatorView.xaml
    /// </summary>
    public partial class CycleTimeCalculatorView : UserControl
    {
        #region Attributes

        /// <summary>
        /// The Undo Highlight manager, used to highlight view controls when undo is performed.
        /// </summary>
        private UndoHighlightManager viewManager;

        #endregion Attributes

        /// <summary>
        /// Initializes a new instance of the <see cref="CycleTimeCalculatorView"/> class.
        /// </summary>
        public CycleTimeCalculatorView()
        {
            InitializeComponent();
            this.DataContextChanged += this.OnDataContextChanged;
        }

        /// <summary>
        /// Called when the data context changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="dependencyPropertyChangedEventArgs">The <see cref="DependencyPropertyChangedEventArgs" /> instance containing the event data.</param>
        private void OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var undoableData = dependencyPropertyChangedEventArgs.NewValue as IUndoable;
            if (undoableData != null)
            {
                if (this.IsLoaded)
                {
                    this.viewManager = new UndoHighlightManager(undoableData.UndoManager, this, this.DataContext);
                }
                else
                {
                    RoutedEventHandler initOnLoad = null;
                    initOnLoad = (s, e) =>
                    {
                        this.viewManager = new UndoHighlightManager(undoableData.UndoManager, this, this.DataContext);
                        this.Loaded -= initOnLoad;
                    };
                    this.Loaded += initOnLoad;
                }
            }
            else
            {
                this.viewManager = null;
            }
        }

        /// <summary>
        /// Handles the Initialized event of the ItemsControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void ItemsControl_Initialized(object sender, System.EventArgs e)
        {
            var control = sender as ItemsControl;
            if (control != null)
            {
                var view = CollectionViewSource.GetDefaultView(control.Items);
                view.CollectionChanged += (o, i) =>
                {
                    if (i.NewItems != null && i.NewItems.Count != 0)
                    {
                        // If a new item is added, scroll to bottom of the list.
                        this.ContentScrollViewer.ScrollToBottom();
                    }
                };
            }
        }
    }
}
