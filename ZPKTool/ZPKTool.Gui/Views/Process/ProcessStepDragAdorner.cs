﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Shapes;
using ZPKTool.Gui.Views;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// The ProcessStepDragAdorner which inherits the abstract base class Adorner.
    /// </summary>
    public class ProcessStepDragAdorner : Adorner
    {
        /// <summary>
        /// Represents the visual child of adorner.
        /// </summary>
        private UIElement child;

        /// <summary>
        /// Represents the owner of adorned element.
        /// </summary>
        private UIElement owner;

        /// <summary>
        /// Represents the XCenter of adorned element.
        /// </summary>
        private double centerX;

        /// <summary>
        /// Represents the YCenter of adorned element.
        /// </summary>
        private double centerY;

        /// <summary>
        /// Represents the top offset of adorned element.
        /// </summary>
        private double topOffset;

        /// <summary>
        ///  Represents the left offset of adorned element.
        /// </summary>
        private double leftOffset;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessStepDragAdorner"/> class. Represents the base class constructor.
        /// </summary>
        /// <param name="owner">The owner.</param>
        public ProcessStepDragAdorner(UIElement owner)
            : base(owner)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessStepDragAdorner"/> class.
        /// </summary>
        /// <param name="owner">The owner.</param>
        /// <param name="adornElement">The adorn element.</param>
        /// <param name="useVisualBrush">if set to <c>true</c> use visual brush of adornElement.</param>
        /// <param name="opacity">The opacity of adornElement.</param>
        public ProcessStepDragAdorner(UIElement owner, UIElement adornElement, bool useVisualBrush, double opacity)
            : base(owner)
        {
            this.owner = owner;
            if (useVisualBrush)
            {
                VisualBrush brush = new VisualBrush(adornElement);
                brush.Opacity = opacity;
                Rectangle r = new Rectangle();
                r.RadiusX = 3;
                r.RadiusY = 3;

                var rectBounds = VisualTreeHelper.GetDescendantBounds(adornElement);
                if (rectBounds != null)
                {
                    r.Width = rectBounds.Width * ProcessView.ScaleRatio;
                    r.Height = rectBounds.Height * ProcessView.ScaleRatio;
                    centerX = rectBounds.Width / 2;
                    centerY = rectBounds.Height / 2;
                }
                else
                {
                    r.Width = adornElement.DesiredSize.Width * ProcessView.ScaleRatio;
                    r.Height = adornElement.DesiredSize.Height * ProcessView.ScaleRatio;
                    centerX = adornElement.DesiredSize.Width / 2;
                    centerY = adornElement.DesiredSize.Height / 2;
                }

                r.Fill = brush;
                child = r;
            }
            else
            {
                child = adornElement;
            }
        }

        /// <summary>
        /// Gets or sets the left offset of AdornedElement.
        /// </summary>
        /// <value>The left offset.</value>
        public double LeftOffset
        {
            get
            {
                return leftOffset;
            }

            set
            {
                leftOffset = value - centerX;
                UpdatePosition();
            }
        }

        /// <summary>
        /// Gets or sets the top offset of AdornedElement.
        /// </summary>
        /// <value>The top offset.</value>
        public double TopOffset
        {
            get
            {
                return topOffset;
            }

            set
            {
                topOffset = value - centerY;

                UpdatePosition();
            }
        }

        /// <summary>
        /// Gets the number of visual child elements within this element.
        /// </summary>
        /// <value></value>
        /// <returns>
        /// The number of visual child elements for this element.
        /// </returns>
        protected override int VisualChildrenCount
        {
            get
            {
                return 1;
            }
        }

        /// <summary>
        /// Returns a <see cref="T:System.Windows.Media.Transform"/> for the adorner, based on the transform that is currently applied to the adorned element.
        /// </summary>
        /// <param name="transform">The transform that is currently applied to the adorned element.</param>
        /// <returns>A transform to apply to the adorner.</returns>
        public override GeneralTransform GetDesiredTransform(GeneralTransform transform)
        {
            GeneralTransformGroup result = new GeneralTransformGroup();

            result.Children.Add(base.GetDesiredTransform(transform));
            result.Children.Add(new TranslateTransform(leftOffset, topOffset));
            return result;
        }

        /// <summary>
        /// Overrides <see cref="M:System.Windows.Media.Visual.GetVisualChild(System.Int32)"/>, and returns a child at the specified index from a collection of child elements.
        /// </summary>
        /// <param name="index">The zero-based index of the requested child element in the collection.</param>
        /// <returns>
        /// The requested child element. This should not return null; if the provided index is out of range, an exception is thrown.
        /// </returns>
        protected override Visual GetVisualChild(int index)
        {
            return child;
        }

        /// <summary>
        /// Updates the DesiredSize of child.
        /// </summary>
        /// <param name="finalSize">The final size.</param>
        /// <returns> The desired size.</returns>
        protected override Size MeasureOverride(Size finalSize)
        {
            child.Measure(finalSize);
            return child.DesiredSize;
        }

        /// <summary>
        /// When overridden in a derived class, positions child elements and determines a size for a <see cref="T:System.Windows.FrameworkElement"/> derived class.
        /// </summary>
        /// <param name="finalSize">The final area within the parent that this element should use to arrange itself and its children.</param>
        /// <returns>The actual size used.</returns>
        protected override Size ArrangeOverride(Size finalSize)
        {
            child.Arrange(new Rect(child.DesiredSize));
            return finalSize;
        }

        /// <summary>
        /// Updates the position of AdornedElement.
        /// </summary>
        private void UpdatePosition()
        {
            AdornerLayer adorner = (AdornerLayer)this.Parent;
            if (adorner != null)
            {
                adorner.Update(this.AdornedElement);
            }
        }
    }
}
