﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ZPKTool.Data;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for ProcessStepWidgetView.xaml
    /// </summary>
    public partial class ProcessStepWidgetView : UserControl
    {
        /// <summary>
        ///  Using a DependencyProperty as the backing store for IsLastWidget.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty IsLastWidgetProperty =
            DependencyProperty.Register("IsLastWidget", typeof(bool), typeof(ProcessStepWidgetView), new PropertyMetadata(false));

        /// <summary>
        /// Using a DependencyProperty as the backing store for IsFirstWidget.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty IsFirstWidgetProperty =
            DependencyProperty.Register("IsFirstWidget", typeof(bool), typeof(ProcessStepWidgetView), new PropertyMetadata(false));

        /// <summary>
        ///  Using a DependencyProperty as the backing store for IsSelected.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty IsSelectedProperty =
            DependencyProperty.Register("IsSelected", typeof(bool), typeof(ProcessStepWidgetView), new PropertyMetadata(false));

        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessStepWidgetView"/> class.
        /// </summary>
        public ProcessStepWidgetView()
        {
            InitializeComponent();

            var isFirstBinding = new Binding("IsFirstWidget");
            isFirstBinding.Mode = BindingMode.OneWay;
            BindingOperations.SetBinding(this, IsFirstWidgetProperty, isFirstBinding);

            var isLastBinding = new Binding("IsLastWidget");
            isLastBinding.Mode = BindingMode.OneWay;
            BindingOperations.SetBinding(this, IsLastWidgetProperty, isLastBinding);

            var isSelectedBinding = new Binding("IsSelected");
            isSelectedBinding.Mode = BindingMode.OneWay;
            BindingOperations.SetBinding(this, IsSelectedProperty, isSelectedBinding);
        }

        /// <summary>
        /// Gets or sets a value indicating whether the widget is the first one on view 
        /// </summary>
        public bool IsFirstWidget
        {
            get { return (bool)GetValue(IsFirstWidgetProperty); }
            set { SetValue(IsFirstWidgetProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the widget is the last one on view 
        /// </summary>
        public bool IsLastWidget
        {
            get { return (bool)GetValue(IsLastWidgetProperty); }
            set { SetValue(IsLastWidgetProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the widget is selected or not 
        /// </summary>
        public bool IsSelected
        {
            get { return (bool)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }

        /// <summary>
        /// Checks the drop target for the dropped entity.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        private void LayoutRoot_CheckDropTarget(object sender, DragEventArgs e)
        {
            e.Handled = true;
            bool isValid = false;

            // obtaining the dropped item
            var dragDropData = e.Data.GetData(typeof(DragAndDropData)) as DragAndDropData;
            if (dragDropData != null && dragDropData.DataObject != null)
            {
                // TODO: the view should not "know" of Model types
                var droppedEntityType = dragDropData.DataObject.GetType();
                isValid = droppedEntityType == typeof(Machine) || droppedEntityType == typeof(Consumable) || droppedEntityType == typeof(Commodity) || droppedEntityType == typeof(Die);
            }

            if (isValid)
            {
                e.Effects = DragDropEffects.Copy;
            }
            else
            {
                e.Effects = DragDropEffects.None;
            }
        }

        /// <summary>
        /// Handles the ConvertEventArgs event of the <see cref="DropCommand" /> that routes the Drop event of the <see cref="ProcessStepWidgetView" /> to a command.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="args">The event arguments of the Drop event.</param>
        /// <returns>
        /// The data context and the dropped entity.
        /// </returns>
        private object DropCommand_ConvertEventArgs(object sender, RoutedEventArgs args)
        {
            var dragEventArgs = args as DragEventArgs;
            object droppedData = null;

            var dragDropData = dragEventArgs.Data.GetData(typeof(DragAndDropData)) as DragAndDropData;
            if (dragDropData != null && dragDropData.DataObject != null)
            {
                droppedData = dragDropData.DataObject;
            }

            return new Tuple<object, object>(this.DataContext, droppedData);
        }
    }
}
