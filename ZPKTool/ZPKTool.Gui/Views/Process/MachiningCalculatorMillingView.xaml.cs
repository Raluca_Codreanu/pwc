﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Effects;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for MachiningCalculatorMillingView.xaml
    /// </summary>
    public partial class MachiningCalculatorMillingView : UserControl
    {
        #region Attributes

        /// <summary>
        /// The Undo Highlight manager, used to highlight view controls when undo is performed.
        /// </summary>
        private UndoHighlightManager viewManager;

        #endregion Attributes

        /// <summary>
        /// Initializes a new instance of the <see cref="MachiningCalculatorMillingView"/> class.
        /// </summary>
        public MachiningCalculatorMillingView()
        {
            InitializeComponent();
            this.DataContextChanged += this.OnDataContextChanged;
            this.MillingToolTypeCombo.SelectionChanged += MillingToolTypeCombo_SelectionChanged;
        }

        /// <summary>
        /// Called when the data context changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="dependencyPropertyChangedEventArgs">The <see cref="DependencyPropertyChangedEventArgs" /> instance containing the event data.</param>
        private void OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var undoableData = dependencyPropertyChangedEventArgs.NewValue as IUndoable;
            if (undoableData != null)
            {
                if (this.IsLoaded)
                {
                    this.viewManager = new UndoHighlightManager(undoableData.UndoManager, this, this.DataContext);
                }
                else
                {
                    RoutedEventHandler initOnLoad = null;
                    initOnLoad = (s, e) =>
                    {
                        this.viewManager = new UndoHighlightManager(undoableData.UndoManager, this, this.DataContext);
                        this.Loaded -= initOnLoad;
                    };
                    this.Loaded += initOnLoad;
                }
            }
            else
            {
                this.viewManager = null;
            }
        }

        /// <summary>
        /// Handles the SelectionChanged event of the MillingToolTypeCombo control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void MillingToolTypeCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int selectedIndex = this.MillingToolTypeCombo.SelectedIndex;
            for (int i = 0; i < this.MillingToolTypesContainer.Children.Count; i++)
            {
                UIElement element = this.MillingToolTypesContainer.Children[i] as UIElement;
                if (element != null)
                {
                    if (i == selectedIndex)
                    {
                        element.Effect = this.Resources["MillingTypeImageSelectionEffect"] as Effect;
                    }
                    else
                    {
                        element.Effect = null;
                    }
                }
            }
        }
    }
}
