﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using ZPKTool.Gui.Resources;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Provides the set of commands for the MainView.
    /// </summary>
    public static class ProcessStepCommands
    {
        /// <summary>
        /// Initializes static members of the <see cref="ProcessStepCommands"/> class.
        /// </summary>
        static ProcessStepCommands()
        {
            InitializeCommands();
        }

        #region Commands

        /// <summary>
        /// Gets a value representing the Edit command.
        /// </summary>
        public static RoutedUICommand Edit { get; private set; }

        /// <summary>
        /// Gets a value representing the Cut command.
        /// </summary>
        public static RoutedUICommand Cut { get; private set; }

        /// <summary>
        /// Gets a value representing the Copy command.
        /// </summary>
        public static RoutedUICommand Copy { get; private set; }

        /// <summary>
        /// Gets a value representing the Paste command.
        /// </summary>
        public static RoutedUICommand Paste { get; private set; }

        /// <summary>
        /// Gets a value representing the Delete command.
        /// </summary>
        public static RoutedUICommand Delete { get; private set; }

        /// <summary>
        /// Gets a value representing the Import command.
        /// </summary>
        public static RoutedUICommand Import { get; private set; }

        /// <summary>
        /// Gets a value representing the Export command.
        /// </summary>        
        public static RoutedUICommand Export { get; private set; }

        #endregion Commands

        /// <summary>
        /// Initializes the commands.
        /// </summary>
        private static void InitializeCommands()
        {
            Edit = new RoutedUICommand(LocalizedResources.General_Edit, "Edit", typeof(ProcessStepCommands));
            Cut = new RoutedUICommand(
                LocalizedResources.General_Cut,
                "Cut",
                typeof(ProcessStepCommands),
                new InputGestureCollection(new[] { new KeyGesture(Key.X, ModifierKeys.Control) }));
            Copy = new RoutedUICommand(
                LocalizedResources.General_Copy,
                "Copy",
                typeof(ProcessStepCommands),
                new InputGestureCollection(new[] { new KeyGesture(Key.C, ModifierKeys.Control) }));
            Paste = new RoutedUICommand(
                LocalizedResources.General_Paste,
                "Paste",
                typeof(ProcessStepCommands),
                new InputGestureCollection(new[] { new KeyGesture(Key.V, ModifierKeys.Control) }));
            Delete = new RoutedUICommand(
                LocalizedResources.General_Delete,
                "Delete",
                typeof(ProcessStepCommands),
                new InputGestureCollection(new[] { new KeyGesture(Key.Delete), new KeyGesture(Key.Delete, ModifierKeys.Shift) }));

            Import = new RoutedUICommand(LocalizedResources.General_Import, "Import", typeof(ProcessStepCommands));
            Export = new RoutedUICommand(LocalizedResources.General_Export, "Export", typeof(ProcessStepCommands));
        }
    }
}
