﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using ZPKTool.Common;
using ZPKTool.Controls;
using ZPKTool.Gui.ViewModels.ProcessInfrastructure;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for ProcessStepView.xaml
    /// </summary>
    public partial class ProcessStepView : UserControl
    {
        #region Attributes

        /// <summary>
        /// Reference to the PartsAmount text property changed notification.
        /// </summary>
        private DependencyPropertyChangeNotifier partsAmountTextChangedNotifier;

        /// <summary>
        /// The Undo Highlight manager, used to highlight view controls when undo is performed.
        /// </summary>
        private UndoHighlightManager viewManager;

        #endregion Attributes

        /// <summary>
        /// Using a DependencyProperty as the backing store for MyString. This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty IsReadOnlyProperty =
         DependencyProperty.Register("IsReadOnly", typeof(bool), typeof(ProcessStepView), new UIPropertyMetadata(false));

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessStepView"/> class.
        /// </summary>
        public ProcessStepView()
        {
            InitializeComponent();
            this.DataContextChanged += this.OnDataContextChanged;

            var binding = new Binding("IsReadOnly") { Mode = BindingMode.OneWay };
            this.SetBinding(IsReadOnlyProperty, binding);
        }

        #endregion Constructor

        /// <summary>
        /// Gets or sets a value indicating whether the current view should be read only.
        /// </summary>
        /// <value>
        /// true if the view should read only; otherwise, false.
        /// </value>
        public bool IsReadOnly
        {
            get { return (bool)GetValue(IsReadOnlyProperty); }
            set { SetValue(IsReadOnlyProperty, value); }
        }

        #region Event Handlers

        /// <summary>
        /// Called when [data context changed].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="dependencyPropertyChangedEventArgs">The <see cref="DependencyPropertyChangedEventArgs" /> instance containing the event data.</param>
        private void OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var undoableData = dependencyPropertyChangedEventArgs.NewValue as IUndoable;
            if (undoableData != null)
            {
                if (this.IsLoaded)
                {
                    this.viewManager = new UndoHighlightManager(undoableData.UndoManager, this.MainTabControl, this.DataContext);
                    viewManager.AddUndoElement(this.PartsDataGrid, "Amount", this.PartsTabItem);
                    viewManager.AddUndoElement(this.PartsDataGrid, "IsExternal", this.PartsTabItem);
                    viewManager.AddUndoElement(this.PartsDataGrid, "HasExternalSGA", this.PartsTabItem);
                    viewManager.AddUndoElement(this.MachinesDataGrid, "MachineAmount", this.MachinesListTab);
                    viewManager.AddUndoElement(this.MachinesDataGrid, "IsProjectSpecific", this.MachinesListTab);
                }
                else
                {
                    RoutedEventHandler initOnLoad = null;
                    initOnLoad = (s, e) =>
                    {
                        this.viewManager = new UndoHighlightManager(undoableData.UndoManager, this.MainTabControl, this.DataContext);
                        viewManager.AddUndoElement(this.PartsDataGrid, "Amount", this.PartsTabItem);
                        viewManager.AddUndoElement(this.PartsDataGrid, "IsExternal", this.PartsTabItem);
                        viewManager.AddUndoElement(this.PartsDataGrid, "HasExternalSGA", this.PartsTabItem);
                        viewManager.AddUndoElement(this.MachinesDataGrid, "MachineAmount", this.MachinesListTab);
                        viewManager.AddUndoElement(this.MachinesDataGrid, "IsProjectSpecific", this.MachinesListTab);
                        this.Loaded -= initOnLoad;
                    };
                    this.Loaded += initOnLoad;
                }
            }
            else
            {
                this.viewManager = null;
            }
        }

        /// <summary>
        /// ShiftsperWeekTextBox GotFocus Routed Event Handler
        /// </summary>
        /// <param name="sender">the control that triggered the event</param>
        /// <param name="e">the event args</param>
        private void ShiftsPerWeekTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            decimal val = 0;

            if (decimal.TryParse(HoursPerShiftTextBox.Text, out val) &&
                val > Constants.MinShiftInfoValue &&
                val <= Constants.MaxTotalHoursAndShifts)
            {
                ShiftsPerWeekTextBox.MaxContentLimit = Constants.MaxTotalHoursAndShifts / val;
            }
            else
            {
                ShiftsPerWeekTextBox.MaxContentLimit = Constants.MaxTotalHoursAndShifts;
            }
        }

        /// <summary>
        /// HoursPerShiftTextBox GotFocus Routed Event Handler
        /// </summary>
        /// <param name="sender">the control that triggered the event</param>
        /// <param name="e">the event args</param>
        private void HoursPerShiftTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            decimal val = 0;

            if (decimal.TryParse(ShiftsPerWeekTextBox.Text, out val) &&
                val > Constants.MinShiftInfoValue &&
                val <= Constants.MaxTotalHoursAndShifts)
            {
                HoursPerShiftTextBox.MaxContentLimit = Constants.MaxTotalHoursAndShifts / val;
            }
            else
            {
                HoursPerShiftTextBox.MaxContentLimit = Constants.MaxTotalHoursAndShifts;
            }
        }

        /// <summary>
        /// Handles the PreviewMouseRightButtonDown event of a datagrid control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void DataGrid_PreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            var datagrid = sender as DataGrid;
            if (datagrid != null)
            {
                datagrid.Focus();
            }
        }

        /// <summary>
        /// Handles the GotFocus event of the PartsAmountExtendedTextBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void PartsAmountExtendedTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            var textBox = sender as TextBox;
            if (textBox == null)
            {
                return;
            }

            this.partsAmountTextChangedNotifier = new DependencyPropertyChangeNotifier(textBox, TextBox.TextProperty);
            this.partsAmountTextChangedNotifier.ValueChanged += OnTextBoxTextChanged;
        }

        /// <summary>
        /// Called when the PartsAmount's text has changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void OnTextBoxTextChanged(object sender, EventArgs args)
        {
            var changeNotifier = sender as DependencyPropertyChangeNotifier;
            if (changeNotifier == null)
            {
                return;
            }

            var textBox = changeNotifier.PropertySource as TextBox;
            if (textBox == null)
            {
                return;
            }

            textBox.SelectAll();

            this.partsAmountTextChangedNotifier.ValueChanged -= OnTextBoxTextChanged;
            this.partsAmountTextChangedNotifier = null;
        }

        #endregion Event Handlers

        #region Machine Data Grid Drag and Drop

        /// <summary>
        /// Handles the PreviewMouseLeftButtonDown event of the MachinesDataGrid control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void MachinesDataGrid_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DataGrid dataGrid = sender as DataGrid;

            if (dataGrid != null)
            {
                ListCollectionView listCollectionView = (ListCollectionView)CollectionViewSource.GetDefaultView(dataGrid.ItemsSource);

                if (!this.IsReadOnly && !listCollectionView.IsEditingItem)
                {
                    DragAndDropHelper.DataGrid_PreviewMouseLeftButtonDown(dataGrid, e);
                }
            }
        }

        /// <summary>
        /// Handles the PreviewMouseMove event of the MachinesDataGrid control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseEventArgs"/> instance containing the event data.</param>
        private void MachinesDataGrid_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            DataGrid dataGrid = sender as DataGrid;

            if (dataGrid != null)
            {
                ListCollectionView listCollectionView = (ListCollectionView)CollectionViewSource.GetDefaultView(dataGrid.ItemsSource);

                if (!this.IsReadOnly && !listCollectionView.IsEditingItem)
                {
                    DragAndDropHelper.DataGrid_PreviewMouseMove(typeof(MachineDataGridItem), dataGrid, e);
                }
            }
        }

        /// <summary>
        /// Handles the PreviewMouseLeftButtonUp event of the MachinesDataGrid control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void MachinesDataGrid_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            DataGrid dataGrid = sender as DataGrid;
            if (dataGrid != null)
            {
                ListCollectionView listCollectionView = (ListCollectionView)CollectionViewSource.GetDefaultView(dataGrid.ItemsSource);
                if (!this.IsReadOnly && !listCollectionView.IsEditingItem)
                {
                    DragAndDropHelper.ResetState();
                }
            }
        }

        /// <summary>
        /// Handles the DragEnter event of the MachinesDataGrid control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        private void MachinesDataGrid_DragEnter(object sender, DragEventArgs e)
        {
            DragAndDropHelper.DataGrid_DragEnter(typeof(MachineDataGridItem), sender as DataGrid, e);
        }

        /// <summary>
        /// Handles the DragLeave event of the MachinesDataGrid control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        private void MachinesDataGrid_DragLeave(object sender, DragEventArgs e)
        {
            DragAndDropHelper.DataGrid_DragLeave(typeof(MachineDataGridItem), e);
        }

        /// <summary>
        /// Handles the DragOver event of the MachinesDataGrid control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        private void MachinesDataGrid_DragOver(object sender, DragEventArgs e)
        {
            DragAndDropHelper.DataGrid_DragOver(typeof(MachineDataGridItem), sender as DataGrid, e);
        }

        /// <summary>
        /// Handles the Drop event of the MachinesDataGrid control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        private void MachinesDataGrid_Drop(object sender, DragEventArgs e)
        {
            DragAndDropHelper.DataGrid_Drop<MachineDataGridItem>(sender as DataGrid, e);
        }

        /// <summary>
        /// Handles the QueryContinueDrag event of the MachinesDataGrid control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.QueryContinueDragEventArgs"/> instance containing the event data.</param>
        private void MachinesDataGrid_QueryContinueDrag(object sender, QueryContinueDragEventArgs e)
        {
            DragAndDropHelper.QueryContinueDrag(e);
        }

        #endregion Machine Data Grid Drag and Drop
    }
}
