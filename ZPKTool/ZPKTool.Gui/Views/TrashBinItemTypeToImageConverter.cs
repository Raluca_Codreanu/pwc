﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Media;
using ZPKTool.Data;
using ZPKTool.Gui.Managers;
using ZPKTool.Gui.Resources;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Converts a short int to the associated picture path of the TrashBinItem that it represents.
    /// </summary>
    [ValueConversion(typeof(short), typeof(ImageSource))]
    public class TrashBinItemTypeToImageConverter : IValueConverter
    {
        #region IValueConverter Members

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var type = value as TrashBinItemType?;
            if (type.HasValue)
            {
                switch (type.Value)
                {
                    case TrashBinItemType.ProjectFolder:
                        return Images.FolderIcon;

                    case TrashBinItemType.Project:
                        return Images.ProjectIcon;

                    case TrashBinItemType.Assembly:
                        return Images.AssemblyIcon;

                    case TrashBinItemType.Part:
                        return Images.PartIcon;

                    case TrashBinItemType.RawMaterial:
                        return Images.RawMaterialIcon;

                    case TrashBinItemType.Commodity:
                        return Images.CommodityIcon;

                    case TrashBinItemType.Machine:
                        return Images.MachineIcon;

                    case TrashBinItemType.Consumable:
                        return Images.ConsumableIcon;

                    case TrashBinItemType.Die:
                        return Images.DieIcon;

                    case TrashBinItemType.RawPart:
                        return Images.RawPartIcon;
                }
            }

            return null;
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
