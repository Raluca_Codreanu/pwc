﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ZPKTool.Controls;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.ViewModels;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for BatchReportingView.xaml
    /// </summary>    
    public partial class BatchReportingView : ZpkToolWindow
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BatchReportingView"/> class.
        /// </summary>
        public BatchReportingView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handles the KeyDown event of the DataTreeView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.KeyEventArgs"/> instance containing the event data.</param>
        private void DataTreeView_KeyDown(object sender, KeyEventArgs e)
        {
            // Toggle the check box of the selected tree item when pressing space.
            if (e.Key == Key.Space)
            {
                TreeViewEx treeView = sender as TreeViewEx;
                if (treeView != null)
                {
                    BatchReportingDataSourceItem selectedItem = treeView.SelectedItem as BatchReportingDataSourceItem;
                    if (selectedItem != null)
                    {
                        selectedItem.IsChecked = !selectedItem.IsChecked;
                    }
                }
            }
        }
    }
}
