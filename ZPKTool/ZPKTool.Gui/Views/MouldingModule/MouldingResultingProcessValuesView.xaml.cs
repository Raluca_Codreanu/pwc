﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for MouldingResultingProcessValuesView.xaml
    /// </summary>    
    public partial class MouldingResultingProcessValuesView
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MouldingResultingProcessValuesView"/> class.
        /// </summary>
        public MouldingResultingProcessValuesView()
        {
            InitializeComponent();
        }
    }
}