﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for PartsView.xaml
    /// </summary>
    public partial class PartsView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PartsView"/> class.
        /// </summary>
        public PartsView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handles the SelectionChanged event of the EntitiesDataGrid control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Controls.SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void EntitiesDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Bring the selected item into view because the view model has no way of doing it.
            if (e.AddedItems.Count > 0)
            {
                this.Parts.UpdateLayout();
                this.Parts.ScrollIntoView(e.AddedItems[0]);
            }
        }
    }
}
