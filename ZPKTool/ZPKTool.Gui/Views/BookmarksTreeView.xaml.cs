﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ZPKTool.Gui.Utils;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for BookmarksTreeView.xaml
    /// </summary>    
    public partial class BookmarksTreeView : UserControl, IFocusDispatcher
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BookmarksTreeView"/> class.
        /// </summary>
        public BookmarksTreeView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handles the MouseRightButtonDown event of the items in the Bookmarks Tree.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void BookmarksTreeItem_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            TreeViewItem item = sender as TreeViewItem;
            if (item != null)
            {
                if (!item.IsSelected)
                {
                    item.IsSelected = true;
                }

                e.Handled = true;
            }
        }

        /// <summary>
        /// Handles the SelectedItemChanged event of the Bookmarks Tree.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedPropertyChangedEventArgs"/> instance containing the event data.</param>
        private void BookmarksTree_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            // In some cases after selection the keyboard focus in not on the selected tree item 
            // so is needed to bring the focus on the tree or anywhere nearby
            this.BookmarksTree.Focus();
        }

        /// <summary>
        /// Sets the focus on the bookmarks tree.
        /// This way the navigation using keyboard in the tree will work.
        /// </summary>
        public void FocusContent()
        {
            if (this.BookmarksTree != null)
            {
                var isFocused = this.BookmarksTree.Focus();
                if (!isFocused)
                {
                    RoutedEventHandler treeLoadedHandler = null;
                    treeLoadedHandler = delegate
                    {
                        this.BookmarksTree.Focus();
                        this.BookmarksTree.Loaded -= treeLoadedHandler;
                    };

                    this.BookmarksTree.Loaded += treeLoadedHandler;
                }
            }
        }
    }
}