﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ZPKTool.Gui.Utils;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Interaction logic for TrashBinView.xaml
    /// </summary>    
    public partial class TrashBinView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TrashBinView"/> class.
        /// </summary>
        public TrashBinView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handles the ContextMenuOpening event of the TrashBin control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Controls.ContextMenuEventArgs"/> instance containing the event data.</param>
        private void TrashBinItemsDataGrid_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            DependencyObject depObj = e.OriginalSource as DependencyObject;            
            DataGridCell cell = UIHelper.FindParent<DataGridCell>(depObj);
            if (cell == null)
            {
                e.Handled = true;
            }
        }
    }
}
