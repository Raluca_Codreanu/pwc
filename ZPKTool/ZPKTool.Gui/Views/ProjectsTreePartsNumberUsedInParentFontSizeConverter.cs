﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// This converter is used to adjust the font size of the label that displays the number of parts used in parent assembly.
    /// This label is placed in the lower right corner of the part and assembly tree items.
    /// </summary>
    public class ProjectsTreePartsNumberUsedInParentFontSizeConverter : IValueConverter
    {
        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is int)
            {
                // If the number of parts has 1-2 digits return font size 10, else return 9.
                int numberOfParts = (int)value;
                var absoluteValue = Math.Abs(numberOfParts);
                if (absoluteValue >= 100)
                {
                    return 9;
                }
                else
                {
                    return 10;
                }
            }
            else
            {
                return Binding.DoNothing;
            }
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        /// <exception cref="System.NotImplementedException">The method is not implemented.</exception>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
