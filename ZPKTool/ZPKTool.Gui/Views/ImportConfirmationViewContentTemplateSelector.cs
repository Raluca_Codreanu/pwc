﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// Selects the template to use for import confirmation screen
    /// </summary>
    public class ImportConfirmationViewContentTemplateSelector : DataTemplateSelector
    {
        /// <summary>
        /// Returns a <see cref="T:System.Windows.DataTemplate" /> depending the item type.
        /// </summary>
        /// <param name="item">The data object for which to select the template.</param>
        /// <param name="container">The data-bound object.</param>
        /// <returns> Returns a <see cref="T:System.Windows.DataTemplate" /> or null. The default value is null. </returns>
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item != null && item is ZPKTool.MvvmCore.ViewModel)
            {
                return (DataTemplate)((FrameworkElement)container).TryFindResource("ViewModelDisplayTemplate");
            }
            else if (item is IEnumerable<object>)
            {
                return (DataTemplate)((FrameworkElement)container).TryFindResource("FolderContentDisplayTemplate");
            }
            else
            {
                return base.SelectTemplate(item, container);
            }
        }
    }
}
