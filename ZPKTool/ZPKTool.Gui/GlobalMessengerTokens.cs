﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui
{
    /// <summary>
    /// A helper for the Projects Tree and Bookmarks Tree.
    /// </summary>
    public class GlobalMessengerTokens
    {
        /// <summary>
        /// The messageTokenToUse used to target the MainView when sending messages.
        /// </summary>
        public const string MainViewTargetToken = "a4e5ad86-30ce-42cc-b0f1-ad81b09755bb";

        /// <summary>
        /// The messageTokenToUse used to target the ModelBrowser when sending messages.
        /// </summary>
        public const string ModelBrowserTargetToken = "15fb4ac0-e476-4c96-afe6-0d3d711b31c1";
    }
}
