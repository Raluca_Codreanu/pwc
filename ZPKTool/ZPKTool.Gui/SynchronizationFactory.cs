﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using ZPKTool.Gui.Screens;
using ZPKTool.Synchronization;

namespace ZPKTool.Gui
{
    /// <summary>
    /// Creates synchronization managers.
    /// </summary>
    public static class SynchronizationFactory
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Initializes the static members of the <see cref="SynchronizationFactory"/> class.
        /// </summary>
        /// <returns>Synchronization Manager.</returns>
        public static SynchronizationManager CreateSynchronizationManager()
        {
            try
            {
               return new SynchronizationManager();
            }
            catch (FileNotFoundException ex)
            {
                log.ErrorException("Sync framework initialization error. The sync framework redistributable package may not be installed correctly.", ex);
                throw new UIException(SyncErrorCodes.SyncFrameworkNotInstalled, ex);
            }
        }     
    }
}
