﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Managers
{
    /// <summary>
    /// This class offers support for copy/paste operations on assemblies, parts and other objects.
    /// Implemented as singleton.
    /// </summary>
    public sealed class ClipboardManager
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The only instance of this class.
        /// </summary>
        private static readonly ClipboardManager instance = new ClipboardManager();

        /// <summary>
        /// The objects stored in clipboard and available for paste.
        /// TODO: consider using a weak reference so that the object is released if it was put on clipboard but not used; or use a timer to release the object after a while.
        /// </summary>
        private List<ClipboardObject> clipboardObjects = new List<ClipboardObject>();

        /// <summary>
        /// Indicates if the object from clipboard is fully loaded and do not need to be loaded from the database on paste.
        /// </summary>
        private bool areClipboardObjectsFullyLoaded;

        #endregion Attributes

        #region Constructor

        /// <summary>
        /// Prevents a default instance of the <see cref="ClipboardManager" /> class from being created.
        /// </summary>
        private ClipboardManager()
        {
        }

        #endregion Constructor

        #region Enums

        /// <summary>
        /// Clipboard objects state. (used to validate clipboard data)
        /// </summary>
        private enum ClipboardObjectsState
        {
            /// <summary>
            /// The clipboard is empty.
            /// </summary>
            Empty = 0,

            /// <summary>
            /// The clipboard data is in a valid state.
            /// </summary>
            Valid = 1,

            /// <summary>
            /// The clipboard contains some deleted objects.
            /// </summary>
            Deleted = 2,

            /// <summary>
            /// The clipboard contains data not supported by the selected entity.
            /// </summary>
            NotSupported = 3
        }

        #endregion Enums

        #region Properties

        /// <summary>
        /// Gets the only instance of this class.
        /// </summary>        
        public static ClipboardManager Instance
        {
            get { return instance; }
        }

        /// <summary>
        /// Gets the types of the objects available for paste.
        /// </summary>
        /// <returns>The type of the copied object. Returns null if no object is available for paste.</returns>
        public IEnumerable<Type> PeekTypes
        {
            get
            {
                return this.clipboardObjects.Select(obj => obj.Entity.GetType());
            }
        }

        /// <summary>
        /// Gets the peek clipboard objects.
        /// </summary>
        /// <value>The peek clipboard object.</value>
        public IEnumerable<object> PeekClipboardObjects
        {
            get
            {
                return this.clipboardObjects.Select(obj => obj.Entity);
            }
        }

        /// <summary>
        /// Gets the operation that will be performed on the current clipboard object.
        /// </summary>
        public ClipboardOperation Operation { get; private set; }

        #endregion Properties

        #region Copy/Cut

        /// <summary>
        /// Store a reference to an object or to a collection of objects to be pasted later.
        /// </summary>
        /// <param name="obj">The object or the collection of objects to be copied.</param>
        /// <param name="objectSource">The source database of <paramref name="obj"/> where the object(s) are located.</param>
        public void Copy(object obj, DbIdentifier objectSource)
        {
            this.Copy(obj, objectSource, false);
        }

        /// <summary>
        /// Store a reference to an object or to a collection of objects to be pasted later.
        /// </summary>
        /// <param name="obj">The object or the collection of objects to be copied.</param>
        /// <param name="objectSource">The source database of <paramref name="obj"/> where the object(s) are located.</param>
        /// <param name="areObjFullyLoaded">A value indicating whether the object to be copied is fully loaded from database or not</param>
        public void Copy(object obj, DbIdentifier objectSource, bool areObjFullyLoaded)
        {
            this.UpdateClipboard(obj, objectSource);
            this.Operation = ClipboardOperation.Copy;
            this.areClipboardObjectsFullyLoaded = areObjFullyLoaded;
        }

        /// <summary>
        /// Store a collection of <see cref="ClipboardObject"/> objects to be pasted later.
        /// </summary>
        /// <param name="objectsToCopy">The collection of <see cref="ClipboardObject"/> objects to be copied</param>
        public void Copy(IEnumerable<ClipboardObject> objectsToCopy)
        {
            this.Operation = ClipboardOperation.Copy;
            this.clipboardObjects.Clear();
            if (objectsToCopy != null)
            {
                this.clipboardObjects = objectsToCopy.ToList();
            }
        }

        /// <summary>
        /// Cuts the specified object or collection of objects.
        /// </summary>
        /// <param name="obj">The object or the collection of objects to be cut.</param>
        /// <param name="objectSource">The source database of <paramref name="obj"/> where the object(s) are located.</param>
        public void Cut(object obj, DbIdentifier objectSource)
        {
            this.UpdateClipboard(obj, objectSource);
            this.Operation = ClipboardOperation.Cut;
        }

        /// <summary>
        /// Cuts a collection of <see cref="ClipboardObject"/> objects to be pasted later.
        /// </summary>
        /// <param name="objectsToCut">The object or the collection of objects to be cut.</param>
        public void Cut(IEnumerable<ClipboardObject> objectsToCut)
        {
            this.Operation = ClipboardOperation.Cut;
            this.clipboardObjects.Clear();
            if (objectsToCut != null)
            {
                this.clipboardObjects = objectsToCut.ToList();
            }
        }

        /// <summary>
        /// Update clipboard object list.
        /// </summary>
        /// <param name="obj">The object('s) to be added to the clipboard.</param>
        /// <param name="objectSource">The source database of <paramref name="obj"/>.</param>
        private void UpdateClipboard(object obj, DbIdentifier objectSource)
        {
            this.clipboardObjects.Clear();
            var multipleObjects = obj as IEnumerable<object>;
            if (multipleObjects != null)
            {
                foreach (var item in multipleObjects)
                {
                    clipboardObjects.Add(new ClipboardObject() { Entity = item, DbSource = objectSource });
                }
            }
            else
            {
                this.clipboardObjects.Add(new ClipboardObject() { Entity = obj, DbSource = objectSource });
            }
        }

        #endregion Copy/Cut

        #region Clipboard checks

        /// <summary>
        /// Checks if an entity accepts for pasting the current clipboard objects.
        /// If one clipboard object is Deleted it returns false.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <returns>
        /// True if the clipboard object is accepted, false otherwise.
        /// </returns>
        public bool CheckIfEntityAcceptsClipboardObjects(Type entityType)
        {
            switch (this.GetClipboardObjectsState(entityType))
            {
                case ClipboardObjectsState.Valid:
                    return true;
                case ClipboardObjectsState.Empty:
                case ClipboardObjectsState.Deleted:
                case ClipboardObjectsState.NotSupported:
                    return false;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Checks if an entity accepts for pasting the current clipboard object.
        /// If the clipboard object is Deleted it returns false.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <returns>
        /// True if the clipboard object is accepted, false otherwise.
        /// </returns>
        private ClipboardObjectsState GetClipboardObjectsState(Type entityType)
        {
            if (this.clipboardObjects.Count == 0)
            {
                return ClipboardObjectsState.Empty;
            }

            foreach (var clipboardObj in this.clipboardObjects)
            {
                if (!this.CheckIfEntityIsDeleted(clipboardObj))
                {
                    if (!this.CheckIfEntityAcceptsObject(entityType, clipboardObj.Entity.GetType()))
                    {
                        return ClipboardObjectsState.NotSupported;
                    }
                }
                else
                {
                    this.clipboardObjects.Clear();
                    return ClipboardObjectsState.Deleted;
                }
            }

            return ClipboardObjectsState.Valid;
        }

        /// <summary>
        /// Checks if an entity accepts for pasting/importing a specified type of objects.
        /// </summary>
        /// <param name="entityType">Type of the entity for which to check.</param>
        /// <param name="objectType">Type of the object to check.</param>
        /// <returns>
        /// True if the object is accepted, false otherwise.
        /// </returns>
        public bool CheckIfEntityAcceptsObject(Type entityType, Type objectType)
        {
            if (entityType == null)
            {
                throw new ArgumentNullException("entityType", "The entity type was null.");
            }

            if (objectType == null)
            {
                throw new ArgumentNullException("objectType", "The object type was null.");
            }

            if (entityType == typeof(ProjectFolder) && objectType == typeof(ProjectFolder))
            {
                return true;
            }

            if (entityType == typeof(ProjectFolder) && objectType == typeof(Project))
            {
                return true;
            }

            if (entityType == typeof(MyProjectsTreeItem) && objectType == typeof(ProjectFolder))
            {
                return true;
            }

            if (entityType == typeof(MyProjectsTreeItem) && objectType == typeof(Project))
            {
                return true;
            }

            if (entityType == typeof(Project) &&
                (objectType == typeof(Assembly) || objectType == typeof(Part)))
            {
                return true;
            }

            if (entityType == typeof(RawPart) &&
                (objectType == typeof(RawMaterial) || objectType == typeof(Commodity)))
            {
                // Part accepts Raw Material, Commodity
                return true;
            }

            if (entityType == typeof(Part) &&
                (objectType == typeof(RawMaterial) || objectType == typeof(Commodity) || objectType == typeof(RawPart)))
            {
                // Part accepts Raw Material, Commodity, RawPart
                return true;
            }

            if (entityType == typeof(Assembly) &&
                (objectType == typeof(Assembly) || objectType == typeof(Part)))
            {
                // Assembly accepts Assembly and Part
                return true;
            }

            if ((entityType == typeof(PartProcessStep) || entityType == typeof(AssemblyProcessStep))
                && (objectType == typeof(Machine)
                    || objectType == typeof(Consumable)
                    || objectType == typeof(CycleTimeCalculation)
                    || objectType == typeof(Die)
                    || (objectType == typeof(Commodity) && entityType == typeof(AssemblyProcessStep))))
            {
                // ProcessStep accepts Machine, Consumable, Die, Commodity
                return true;
            }

            if (entityType == typeof(Process) &&
                objectType.IsSubclassOf(typeof(ProcessStep)))
            {
                // Process accepts any kind of Process Step
                return true;
            }

            if ((entityType == typeof(ManageReleasedProjectsTreeItem) && objectType == typeof(ProjectFolder))
                || (entityType == typeof(ManageReleasedProjectsTreeItem) && objectType == typeof(Project)))
            {
                return true;
            }

            if (entityType == typeof(SubAssembliesTreeItem) && objectType == typeof(Assembly))
            {
                return true;
            }

            if (entityType == typeof(SubPartsTreeItem) && objectType == typeof(Part))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Checks if the clipboard object entity is deleted.
        /// </summary>
        /// <param name="obj">The clipboard object.</param>
        /// <returns>true if the clipboard object is deleted, false otherwise</returns>
        private bool CheckIfEntityIsDeleted(ClipboardObject obj)
        {
            var trashableEntity = obj.Entity as ITrashable;
            if (trashableEntity != null)
            {
                return trashableEntity.IsDeleted;
            }

            var processStep = obj.Entity as ProcessStep;
            if (processStep != null)
            {
                if (processStep.Process == null)
                {
                    // This check is for the situation when the process step was deleted
                    // after being added to the clipboard.
                    var dataManager = DataAccessFactory.CreateDataSourceManager(obj.DbSource);
                    return !dataManager.ProcessStepRepository.CheckIfExists(processStep.Guid);
                }

                var parentAssy = processStep.Process.Assemblies.FirstOrDefault();
                if (parentAssy != null)
                {
                    return parentAssy.IsDeleted;
                }

                var parentPart = processStep.Process.Parts.FirstOrDefault();
                if (parentPart != null)
                {
                    return parentPart.IsDeleted;
                }
            }

            return false;
        }

        #endregion Clipboard checks

        #region Paste

        /// <summary>
        /// Pastes the clipboard objects into a given object.
        /// </summary>
        /// <param name="recipient">The entity in which the clipboard objects will be pasted.</param>
        /// <param name="dataManager">The data context associated with the recipient.</param>
        /// <returns>A list of pasted data (containing each pasted entity and its error - if exists).</returns>
        public IList<PastedData> Paste(object recipient, IDataSourceManager dataManager)
        {
            return Paste(recipient, dataManager, false, false);
        }

        /// <summary>
        /// Pastes the clipboard objects into a given object.
        /// </summary>
        /// <param name="recipient">The entity in which the clipboard objects will be pasted.</param>
        /// <param name="dataManager">The data context associated with the recipient.</param>
        /// <param name="updateToRecipientCalculationVersion">
        /// If set to true, the calculation version of the pasted objects (if it has one) is upgraded (if older)
        /// to the calculation version of the recipient object.
        /// </param>
        /// <param name="addToContext">Attach the pasted entities to recipient context.</param>
        /// <returns>A list of pasted data (containing each pasted entity and its error - if exists).</returns>
        public IList<PastedData> Paste(object recipient, IDataSourceManager dataManager, bool updateToRecipientCalculationVersion, bool addToContext)
        {
            var pastedObjects = new List<PastedData>();
            try
            {
                if (!this.clipboardObjects.Any())
                {
                    throw new ArgumentException("clipboardObjects", "There is no object to paste.");
                }

                if (recipient == null)
                {
                    throw new ArgumentNullException("recipient", "The recipient object of paste was null.");
                }

                var clipboardState = GetClipboardObjectsState(recipient.GetType());
                switch (clipboardState)
                {
                    case ClipboardObjectsState.Valid:
                        {
                            foreach (var clipboardObject in this.clipboardObjects)
                            {
                                var pastedEntity = new PastedData();
                                try
                                {
                                    var clipboardFolder = clipboardObject.Entity as ProjectFolder;
                                    if (clipboardFolder != null)
                                    {
                                        var pastedProjectFolder = PasteProjectFolder(clipboardFolder, recipient as ProjectFolder, dataManager);
                                        pastedEntity.Entity = pastedProjectFolder;
                                        pastedObjects.Add(pastedEntity);
                                        continue;
                                    }

                                    var clipboardProject = clipboardObject.Entity as Project;
                                    if (clipboardProject != null)
                                    {
                                        var pastedProject = PasteProject(clipboardObject, recipient as ProjectFolder, dataManager);
                                        pastedEntity.Entity = pastedProject;
                                        pastedObjects.Add(pastedEntity);
                                        continue;
                                    }

                                    var recipientProject = recipient as Project;
                                    if (recipientProject != null)
                                    {
                                        var pastedProjectChild = PasteIntoProject(clipboardObject, recipientProject, dataManager);
                                        pastedEntity.Entity = pastedProjectChild;
                                        pastedObjects.Add(pastedEntity);
                                        continue;
                                    }

                                    var recipientPart = recipient as Part;
                                    if (recipientPart != null)
                                    {
                                        if (clipboardObject.Entity.GetType() == typeof(RawPart) && recipientPart.RawPart != null)
                                        {
                                            if (recipientPart.RawPart.IsDeleted)
                                            {
                                                throw new BusinessException(ErrorCodes.PartAlreadyContainsDeletedRawPart);
                                            }

                                            throw new BusinessException(ErrorCodes.PartAlreadyContainsRawPart);
                                        }

                                        var pastedPartChild = PasteIntoPart(clipboardObject, recipientPart, dataManager);
                                        pastedEntity.Entity = pastedPartChild;
                                        pastedObjects.Add(pastedEntity);
                                        continue;
                                    }

                                    var recipientAssembly = recipient as Assembly;
                                    if (recipientAssembly != null)
                                    {
                                        var pastedAssemblyChild = PasteIntoAssembly(clipboardObject, recipientAssembly, dataManager, updateToRecipientCalculationVersion);
                                        pastedEntity.Entity = pastedAssemblyChild;
                                        pastedObjects.Add(pastedEntity);
                                        continue;
                                    }

                                    var recipientStep = recipient as ProcessStep;
                                    if (recipientStep != null)
                                    {
                                        var pastedStepChild = PasteIntoProcessStep(clipboardObject, recipientStep, dataManager, addToContext);
                                        pastedEntity.Entity = pastedStepChild;
                                        pastedObjects.Add(pastedEntity);
                                        continue;
                                    }

                                    var recipientProcess = recipient as Process;
                                    if (recipientProcess != null)
                                    {
                                        var pastedProcessChild = PasteIntoProcess(dataManager, clipboardObject, recipientProcess);
                                        pastedEntity.Entity = pastedProcessChild;
                                        pastedObjects.Add(pastedEntity);
                                        continue;
                                    }

                                    throw new BusinessException(ErrorCodes.PasteRecipientNotSupported);
                                }
                                catch (Exception ex)
                                {
                                    pastedEntity.Entity = clipboardObject.Entity;
                                    pastedEntity.Error = ex;
                                    pastedObjects.Add(pastedEntity);

                                    log.Error("An error occurred while executing the paste operation.", ex);
                                }
                            }

                            break;
                        }

                    case ClipboardObjectsState.Deleted:
                        throw new BusinessException(ErrorCodes.PasteClipboardObjectDeleted);
                    case ClipboardObjectsState.NotSupported:
                        throw new BusinessException(ErrorCodes.PastedObjectNotSupported);
                }
            }
            finally
            {
                this.ClearClipboard();
            }

            return pastedObjects;
        }

        /// <summary>
        /// Clears the clipboard
        /// </summary>
        private void ClearClipboard()
        {
            this.clipboardObjects.Clear();
        }

        #endregion Paste

        #region Paste methods

        /// <summary>
        /// Pastes the specified project folder in another project folder.
        /// </summary>
        /// <param name="folderToPaste">The folder to paste.</param>
        /// <param name="recipient">The folder to paste into.</param>
        /// <param name="dataManager">The recipient's data context.</param>
        /// <returns>The pasted object.</returns>
        private object PasteProjectFolder(ProjectFolder folderToPaste, ProjectFolder recipient, IDataSourceManager dataManager)
        {
            // Check if folderToPaste is in the parent tree of the recipient to avoid pasting into children and creating a circular reference
            if (dataManager.ProjectFolderRepository.CheckIfParentOf(folderToPaste, recipient))
            {
                throw new BusinessException(ErrorCodes.ProjectFolderMoveInChildError);
            }

            var folder = dataManager.ProjectFolderRepository.GetFolderWithParent(folderToPaste.Guid);
            if (folder == null)
            {
                throw new BusinessException(ErrorCodes.PastedObjectNotAvailable);
            }

            // Don't do anything if the recipient is already the parent of the folder
            if ((folder.ParentProjectFolder != null && recipient != null && folder.ParentProjectFolder.Guid == recipient.Guid)
                || (folder.ParentProjectFolder == null && recipient == null))
            {
                return null;
            }

            if (recipient != null)
            {
                recipient.ChildrenProjectFolders.Add(folder);
            }
            else
            {
                folder.ParentProjectFolder = null;
            }

            dataManager.ProjectFolderRepository.Save(folder);
            dataManager.SaveChanges();

            return folder;
        }

        /// <summary>
        /// Pastes the project into a folder.
        /// </summary>
        /// <param name="clipboardObjectToPaste">The clipboard object to paste.</param>
        /// <param name="parentFolder">The folder in which to paste the project.</param>
        /// <param name="projectDataManager">The project data context.</param>
        /// <returns>
        /// The pasted object.
        /// </returns>
        private object PasteProject(ClipboardObject clipboardObjectToPaste, ProjectFolder parentFolder, IDataSourceManager projectDataManager)
        {
            if (clipboardObjectToPaste == null)
            {
                throw new ArgumentNullException("clipboardObjectToPaste", "The clipboard object to paste was null.");
            }

            var projectToPaste = clipboardObjectToPaste.Entity as Project;
            if (clipboardObjectToPaste == null)
            {
                throw new ArgumentNullException("projectToPaste", "The project to paste was null.");
            }

            if (this.Operation == ClipboardOperation.Cut)
            {
                var project = projectDataManager.ProjectRepository.GetProjectForProjectsTree(projectToPaste.Guid);
                if (project == null)
                {
                    throw new BusinessException(ErrorCodes.PastedObjectNotAvailable);
                }

                if (parentFolder != null)
                {
                    parentFolder.Projects.Add(project);
                }
                else
                {
                    project.ProjectFolder = null;
                }

                projectDataManager.ProjectRepository.Save(project);
                projectDataManager.SaveChanges();

                return projectToPaste;
            }
            else
            {
                object pastedObject = null;
                var projectClone = CloneObject(projectToPaste, clipboardObjectToPaste.DbSource, projectDataManager);
                this.SetCopyName(projectClone, projectToPaste, parentFolder, projectDataManager);
                projectClone.SetIsMasterData(false);
                var crtUser = projectDataManager.UserRepository.GetById(SecurityManager.Instance.CurrentUser.Guid, false);
                projectClone.SetOwner(crtUser);
                projectClone.ProjectFolder = parentFolder;
                projectDataManager.ProjectRepository.Save(projectClone);
                projectDataManager.SaveChanges();
                pastedObject = projectClone;

                // If the project assemblies and parts are not loaded, load them.               
                foreach (var assembly in projectToPaste.Assemblies)
                {
                    using (var dataManager = DataAccessFactory.CreateDataSourceManager(projectDataManager.DatabaseId))
                    {
                        var currentUser = dataManager.UserRepository.GetById(SecurityManager.Instance.CurrentUser.Guid, false);
                        var assemblyClone = CloneObject(assembly, clipboardObjectToPaste.DbSource, dataManager);
                        assemblyClone.SetIsMasterData(false);
                        assemblyClone.SetOwner(currentUser);
                        dataManager.AssemblyRepository.Add(assemblyClone);
                        dataManager.SaveChanges();

                        // TODO: refresh the clone's assemblies collection, don't get the new assembly and insert it in the clone (even though that works)
                        var newAssembly = projectDataManager.AssemblyRepository.GetAssembly(assemblyClone.Guid, false);
                        if (newAssembly != null)
                        {
                            projectClone.Assemblies.Add(newAssembly);
                            projectDataManager.ProjectRepository.Save(projectClone);
                            projectDataManager.SaveChanges();
                        }
                    }
                }

                foreach (var part in projectToPaste.Parts)
                {
                    using (var dataManager = DataAccessFactory.CreateDataSourceManager(projectDataManager.DatabaseId))
                    {
                        var currentUser = dataManager.UserRepository.GetById(SecurityManager.Instance.CurrentUser.Guid, false);
                        var partClone = CloneObject(part, clipboardObjectToPaste.DbSource, dataManager);
                        partClone.SetIsMasterData(false);
                        partClone.SetOwner(currentUser);
                        dataManager.PartRepository.Add(partClone);
                        dataManager.SaveChanges();

                        // TODO: refresh the clone's assemblies collection, don't get the new part and insert it in the clone (even though that works)
                        var newPart = projectDataManager.PartRepository.GetPart(partClone.Guid, false);
                        if (newPart != null)
                        {
                            projectClone.Parts.Add(newPart);
                            projectDataManager.ProjectRepository.Save(projectClone);
                            projectDataManager.SaveChanges();
                        }
                    }
                }

                return pastedObject;
            }
        }

        /// <summary>
        /// Called by PasteCopiedObject for pasting an object into a project.
        /// </summary>
        /// <param name="clipboardObjectToPaste">The clipboard object to paste into Project.</param>
        /// <param name="project">The project into which to paste the clipboard object.</param>
        /// <param name="projectDataManager">The project's data context.</param>
        /// <returns>
        /// The pasted object.
        /// </returns>
        private object PasteIntoProject(ClipboardObject clipboardObjectToPaste, Project project, IDataSourceManager projectDataManager)
        {
            if (clipboardObjectToPaste == null)
            {
                throw new ArgumentNullException("clipboardObjectToPaste", "The clipboard object to paste was null.");
            }

            // The project is usually detached so we have to get it.
            var freshProject = projectDataManager.ProjectRepository.GetProjectIncludingTopLevelChildren(project.Guid);
            if (freshProject == null)
            {
                throw new BusinessException(ErrorCodes.ItemNotFound);
            }

            object pastedObject = null;
            var assembly = clipboardObjectToPaste.Entity as Assembly;
            if (assembly != null)
            {
                var messages = new List<EntityChangedMessage>();
                Assembly assemblyToPaste = null;
                if (this.Operation == ClipboardOperation.Cut)
                {
                    // Check if the item parent is the same as the recipient
                    if (assembly.Project == null || assembly.Project.Guid != project.Guid)
                    {
                        // Load the full assembly because it is needed for currency conversions
                        assemblyToPaste = projectDataManager.AssemblyRepository.GetAssemblyFull(assembly.Guid);

                        // Load the assembly parent in the context before set a new value because when loading the assembly it does not load its parent assembly too
                        projectDataManager.AssemblyRepository.GetParentAssembly(assemblyToPaste, false);
                        assemblyToPaste.ParentAssembly = null;

                        // Remove the process step assembly amounts and generate the messages to refresh the tree
                        messages = this.RemoveProcessStepAssemblyAmount(projectDataManager, assemblyToPaste);
                    }
                    else
                    {
                        // If the item parent is the same as the recipient throw an exception
                        throw new InvalidOperationException("An object cannot be moved into its own parent");
                    }
                }
                else
                {
                    assemblyToPaste = CloneObject(assembly, clipboardObjectToPaste.DbSource, projectDataManager);
                    this.SetCopyName(assemblyToPaste, assembly, project, projectDataManager);
                    assemblyToPaste.SetIsMasterData(false);
                    assemblyToPaste.SetOwner(freshProject.Owner);
                }

                CurrencyConversionManager.ConvertCurrency(project, clipboardObjectToPaste.Entity, assemblyToPaste, projectDataManager.DatabaseId, clipboardObjectToPaste.DbSource);

                freshProject.Assemblies.Add(assemblyToPaste);
                assemblyToPaste.Index = freshProject.Assemblies.Max(a => a.Index) + 1 ?? 0;
                projectDataManager.ProjectRepository.Save(freshProject);
                projectDataManager.SaveChanges();

                // Send the messages to refresh the tree
                this.SendEntityChangedMessages(messages);

                pastedObject = assemblyToPaste;
            }
            else
            {
                var part = clipboardObjectToPaste.Entity as Part;
                if (part != null)
                {
                    var messages = new List<EntityChangedMessage>();
                    Part partToPaste = null;
                    if (this.Operation == ClipboardOperation.Cut)
                    {
                        // Check if the item parent is the same as the recipient
                        if (part.Project == null || part.Project.Guid != project.Guid)
                        {
                            // Load the full part because it is needed for currency conversions
                            partToPaste = projectDataManager.PartRepository.GetPartFull(part.Guid);

                            // Load the assembly parent in the context before set e new value because when loading the part it does not load its parent assembly too
                            projectDataManager.AssemblyRepository.GetParentAssembly(partToPaste, false);
                            partToPaste.Assembly = null;

                            // Remove the process step part amounts and generate the messages to refresh the tree
                            messages = this.RemoveProcessStepPartAmount(projectDataManager, partToPaste);
                        }
                        else
                        {
                            // If the item parent is the same as the recipient throw an exception
                            throw new InvalidOperationException("An object cannot be moved into its own parent");
                        }
                    }
                    else
                    {
                        partToPaste = CloneObject(part, clipboardObjectToPaste.DbSource, projectDataManager);
                        this.SetCopyName(partToPaste, part, project, projectDataManager);
                        partToPaste.SetIsMasterData(false);
                        partToPaste.SetOwner(freshProject.Owner);
                    }

                    CurrencyConversionManager.ConvertCurrency(project, clipboardObjectToPaste.Entity, partToPaste, projectDataManager.DatabaseId, clipboardObjectToPaste.DbSource);

                    freshProject.Parts.Add(partToPaste);
                    partToPaste.Index = freshProject.Parts.Max(p => p.Index) + 1 ?? 0;
                    projectDataManager.ProjectRepository.Save(freshProject);
                    projectDataManager.SaveChanges();

                    // Send the messages to refresh the tree
                    this.SendEntityChangedMessages(messages);

                    pastedObject = partToPaste;
                }
                else
                {
                    log.Error("Pasted object is not accepted by a project ({0})", clipboardObjectToPaste.Entity.GetType().FullName);
                }
            }

            return pastedObject;
        }

        /// <summary>
        /// Pastes the clipboard object into assembly.
        /// </summary>
        /// <param name="clipboardObjectToPaste">The clipboard object to paste into Assembly.</param>
        /// <param name="assembly">The assembly.</param>
        /// <param name="assemblyDataManager">The assembly data manager.</param>
        /// <param name="updateToRecipientCalculationVersion">If set to true, the calculation version of the pasted object (if it has one)
        /// is upgraded (if older) to the calculation version of the recipient object.</param>
        /// <returns>
        /// The pasted object.
        /// </returns>
        private object PasteIntoAssembly(
            ClipboardObject clipboardObjectToPaste,
            Assembly assembly,
            IDataSourceManager assemblyDataManager,
            bool updateToRecipientCalculationVersion)
        {
            if (clipboardObjectToPaste == null)
            {
                throw new ArgumentNullException("clipboardObjectToPaste", "The clipboard object to paste was null.");
            }

            object pastedObj = null;
            var assy = clipboardObjectToPaste.Entity as Assembly;

            if (this.Operation == ClipboardOperation.Cut && this.CheckAssemblyIfParentOf(assy, assembly))
            {
                throw new BusinessException(ErrorCodes.AssemblyMoveInChildError);
            }

            if (assy != null)
            {
                var messages = new List<EntityChangedMessage>();
                Assembly assemblyToPaste = null;
                if (this.Operation == ClipboardOperation.Cut)
                {
                    // Check if the item parent is the same as the recipient
                    if (assy.ParentAssembly == null || assy.ParentAssembly.Guid != assembly.Guid)
                    {
                        // Load the full assembly because it is needed for currency conversions
                        assemblyToPaste = assemblyDataManager.AssemblyRepository.GetAssemblyFull(assy.Guid);

                        // Load the project parent before set e new value because when loading the assembly it does not load its parent project too
                        assemblyDataManager.ProjectRepository.GetParentProject(assemblyToPaste);
                        assemblyToPaste.Project = null;

                        // Remove the process step assembly amounts and generate the messages to refresh the tree
                        messages = this.RemoveProcessStepAssemblyAmount(assemblyDataManager, assemblyToPaste);
                    }
                    else
                    {
                        // If the item parent is the same as the recipient throw an exception
                        throw new InvalidOperationException("An object cannot be moved into its own parent");
                    }
                }
                else
                {
                    assemblyToPaste = CloneObject(assy, clipboardObjectToPaste.DbSource, assemblyDataManager);
                    this.SetCopyName(assemblyToPaste, assy, assembly, assemblyDataManager);
                    assemblyToPaste.SetIsMasterData(assembly.IsMasterData);
                    assemblyToPaste.SetOwner(assembly.Owner);
                }

                if (updateToRecipientCalculationVersion)
                {
                    assemblyToPaste.SetCalculationVariant(assembly.CalculationVariant);
                }

                CurrencyConversionManager.ConvertCurrency(assembly, clipboardObjectToPaste.Entity, assemblyToPaste, assemblyDataManager.DatabaseId, clipboardObjectToPaste.DbSource);

                assembly.Subassemblies.Add(assemblyToPaste);
                assemblyToPaste.Index = assembly.Subassemblies.Max(a => a.Index) + 1 ?? 0;
                assemblyDataManager.AssemblyRepository.Save(assembly);
                assemblyDataManager.SaveChanges();

                // Send the messages to refresh the tree
                this.SendEntityChangedMessages(messages);

                pastedObj = assemblyToPaste;
            }
            else
            {
                var part = clipboardObjectToPaste.Entity as Part;
                if (part != null)
                {
                    Part partToPaste = null;
                    var messages = new List<EntityChangedMessage>();
                    if (this.Operation == ClipboardOperation.Cut)
                    {
                        // Check if the item parent is the same as the recipient
                        if (part.Assembly == null || part.Assembly.Guid != assembly.Guid)
                        {
                            // Load the full part because it is needed for currency conversions
                            partToPaste = assemblyDataManager.PartRepository.GetPartFull(part.Guid);

                            // Load the project parent before set e new value because when loading the part it does not load its parent project too
                            assemblyDataManager.ProjectRepository.GetParentProject(partToPaste);
                            partToPaste.Project = null;

                            // Remove the process step part amounts and generate the messages to refresh the tree
                            messages = this.RemoveProcessStepPartAmount(assemblyDataManager, partToPaste);
                        }
                        else
                        {
                            // If the item parent is the same as the recipient throw an exception
                            throw new InvalidOperationException("An object cannot be moved into its own parent");
                        }
                    }
                    else
                    {
                        partToPaste = CloneObject(part, clipboardObjectToPaste.DbSource, assemblyDataManager);
                        this.SetCopyName(partToPaste, part, assembly, assemblyDataManager);
                        partToPaste.SetIsMasterData(assembly.IsMasterData);
                        partToPaste.SetOwner(assembly.Owner);
                    }

                    if (updateToRecipientCalculationVersion)
                    {
                        partToPaste.CalculationVariant = assembly.CalculationVariant;
                    }

                    CurrencyConversionManager.ConvertCurrency(assembly, clipboardObjectToPaste.Entity, partToPaste, assemblyDataManager.DatabaseId, clipboardObjectToPaste.DbSource);

                    assembly.Parts.Add(partToPaste);
                    partToPaste.Index = assembly.Parts.Max(p => p.Index) + 1 ?? 0;
                    assemblyDataManager.AssemblyRepository.Save(assembly);
                    assemblyDataManager.SaveChanges();

                    // Send the messages to refresh the tree
                    this.SendEntityChangedMessages(messages);

                    pastedObj = partToPaste;
                }
                else
                {
                    log.Error("Pasted object is not accepted by an Assembly ({0})", clipboardObjectToPaste.Entity.GetType().FullName);
                }
            }

            return pastedObj;
        }

        /// <summary>
        /// Pastes the clipboard object into a part.
        /// </summary>
        /// <param name="clipboardObjectToPaste">The clipboard object to paste into the part.</param>
        /// <param name="part">The part in which to paste.</param>
        /// <param name="partDataManager">The change set associated with the part.</param>
        /// <returns>The pasted object.</returns>
        private object PasteIntoPart(ClipboardObject clipboardObjectToPaste, Part part, IDataSourceManager partDataManager)
        {
            if (clipboardObjectToPaste == null)
            {
                throw new ArgumentNullException("clipboardObjectToPaste", "The clipboard object to paste was null.");
            }

            object pastedObj = null;
            var rawMaterial = clipboardObjectToPaste.Entity as RawMaterial;
            if (rawMaterial != null)
            {
                RawMaterial rawMaterialPasted = null;
                if (this.Operation == ClipboardOperation.Cut)
                {
                    // Check if the item parent is the same as the recipient
                    var rawMaterialParent = rawMaterial.Part;
                    if (rawMaterialParent.Guid != part.Guid)
                    {
                        // Load the full raw material because it is needed for currency conversions
                        rawMaterialPasted = partDataManager.RawMaterialRepository.GetById(rawMaterial.Guid);
                        rawMaterialPasted.Part = null;
                    }
                    else
                    {
                        // If the item parent is the same as the recipient throw an exception
                        throw new InvalidOperationException("An object cannot be moved into its own parent");
                    }
                }
                else
                {
                    rawMaterialPasted = CloneObject(rawMaterial, clipboardObjectToPaste.DbSource, partDataManager);
                    this.SetCopyName(rawMaterialPasted, rawMaterial, part, partDataManager);
                    rawMaterialPasted.SetIsMasterData(part.IsMasterData);
                    rawMaterialPasted.SetOwner(part.Owner);
                }

                CurrencyConversionManager.ConvertCurrency(part, clipboardObjectToPaste.Entity, rawMaterialPasted, partDataManager.DatabaseId, clipboardObjectToPaste.DbSource);

                part.RawMaterials.Add(rawMaterialPasted);
                partDataManager.PartRepository.Save(part);
                partDataManager.SaveChanges();

                pastedObj = rawMaterialPasted;
            }
            else
            {
                var commodity = clipboardObjectToPaste.Entity as Commodity;
                if (commodity != null)
                {
                    Commodity commodityPasted = null;
                    if (this.Operation == ClipboardOperation.Cut)
                    {
                        // Check if the item parent is the same as the recipient
                        var commodityParent = commodity.Part;
                        if (commodityParent.Guid != part.Guid)
                        {
                            // Load the full commodity because it is needed for currency conversions
                            commodityPasted = partDataManager.CommodityRepository.GetById(commodity.Guid);
                            commodityPasted.Part = null;
                        }
                        else
                        {
                            // If the item parent is the same as the recipient throw an exception
                            throw new InvalidOperationException("An object cannot be moved into its own parent");
                        }
                    }
                    else
                    {
                        commodityPasted = CloneObject(commodity, clipboardObjectToPaste.DbSource, partDataManager);
                        this.SetCopyName(commodityPasted, commodity, part, partDataManager);
                        commodityPasted.SetIsMasterData(part.IsMasterData);
                        commodityPasted.SetOwner(part.Owner);
                    }

                    CurrencyConversionManager.ConvertCurrency(part, clipboardObjectToPaste.Entity, commodityPasted, partDataManager.DatabaseId, clipboardObjectToPaste.DbSource);

                    part.Commodities.Add(commodityPasted);
                    partDataManager.PartRepository.Save(part);
                    partDataManager.SaveChanges();

                    pastedObj = commodityPasted;
                }
                else
                {
                    var rawPart = clipboardObjectToPaste.Entity as RawPart;
                    if (rawPart != null)
                    {
                        RawPart rawPartPasted = null;
                        if (this.Operation == ClipboardOperation.Cut)
                        {
                            // Check if the item parent is the same as the recipient
                            var rawPartParent = rawPart.ParentOfRawPart.First();
                            if (rawPartParent.Guid != part.Guid)
                            {
                                // Load the full raw part because it is needed for currency conversions
                                rawPartPasted = (RawPart)partDataManager.PartRepository.GetPartFull(rawPart.Guid);

                                // Load the part parent before set e new value because when loading the raw part it does not load its parent part too
                                partDataManager.PartRepository.GetPartFull(rawPartParent.Guid);
                                rawPartPasted.ParentOfRawPart.Clear();
                            }
                            else
                            {
                                // If the item parent is the same as the recipient throw an exception
                                throw new InvalidOperationException("An object cannot be moved into its own parent");
                            }
                        }
                        else
                        {
                            rawPartPasted = (RawPart)CloneObject(rawPart, clipboardObjectToPaste.DbSource, partDataManager);
                            this.SetCopyName(rawPartPasted, rawPart, part, partDataManager);
                            rawPartPasted.SetIsMasterData(part.IsMasterData);
                            rawPartPasted.SetOwner(part.Owner);
                        }

                        part.RawPart = rawPartPasted;
                        CurrencyConversionManager.ConvertCurrency(part, clipboardObjectToPaste.Entity, rawPartPasted, partDataManager.DatabaseId, clipboardObjectToPaste.DbSource);
                        partDataManager.PartRepository.Save(part);
                        partDataManager.SaveChanges();

                        pastedObj = rawPartPasted;
                    }
                    else
                    {
                        log.Error("Pasted object is not accepted by a part ({0})", clipboardObjectToPaste.Entity.GetType().FullName);
                    }
                }
            }

            return pastedObj;
        }

        /// <summary>
        /// Pastes the clipboard object into a process step.
        /// </summary>
        /// <param name="clipboardObjectToPaste">The clipboard object to paste into the process step.</param>
        /// <param name="processStep">The process step.</param>
        /// <param name="stepDataManager">The step data context.</param>
        /// <param name="addToContext">Attach the pasted entity to recipient context.</param>
        /// <returns>
        /// The pasted object.
        /// </returns>
        private object PasteIntoProcessStep(ClipboardObject clipboardObjectToPaste, ProcessStep processStep, IDataSourceManager stepDataManager, bool addToContext)
        {
            var isMasterData = processStep.IsMasterData;
            var owner = processStep.Owner;

            // Pasting into a process step does not trigger a save.
            var machine = clipboardObjectToPaste.Entity as Machine;
            if (machine != null)
            {
                var machineIndex = processStep.Machines.Max(m => m.Index) + 1;
                Machine machinePasted = null;

                if (this.Operation == ClipboardOperation.Cut)
                {
                    // Check if the process step parent is the same as the recipient
                    var canPaste = false;
                    var machineParent = machine.ProcessStep;
                    canPaste = machineParent == null || machineParent.Guid != processStep.Guid;
                    if (canPaste)
                    {
                        // Load the full machine because it is needed for currency conversions
                        machinePasted = stepDataManager.MachineRepository.GetById(machine.Guid);
                        machinePasted.ProcessStep = null;
                    }
                    else
                    {
                        // If the item parent is the same as the recipient throw an exception
                        throw new InvalidOperationException("An object cannot be moved into its own parent");
                    }
                }
                else
                {
                    machinePasted = CloneObject(machine, clipboardObjectToPaste.DbSource, stepDataManager);
                    this.SetCopyName(machinePasted, machine, processStep, stepDataManager);
                    machinePasted.SetIsMasterData(isMasterData);
                    machinePasted.SetOwner(owner);
                }

                CurrencyConversionManager.ConvertCurrency(processStep, clipboardObjectToPaste.Entity, machinePasted, stepDataManager.DatabaseId, clipboardObjectToPaste.DbSource);
                machinePasted.Index = machineIndex;
                machinePasted.ProcessStep = processStep;
                if (addToContext)
                {
                    processStep.Machines.Add(machinePasted);
                }

                return machinePasted;
            }

            var consumable = clipboardObjectToPaste.Entity as Consumable;
            if (consumable != null)
            {
                Consumable consumablePasted = null;
                if (this.Operation == ClipboardOperation.Cut)
                {
                    // Check if the process step parent is the same as the recipient
                    var canPaste = false;
                    var consumableParent = consumable.ProcessStep;
                    canPaste = consumableParent == null || consumableParent.Guid != processStep.Guid;
                    if (canPaste)
                    {
                        // Load the full consumable because it is needed for currency conversions
                        consumablePasted = stepDataManager.ConsumableRepository.GetById(consumable.Guid);
                        consumablePasted.ProcessStep = null;
                    }
                    else
                    {
                        // If the item parent is the same as the recipient throw an exception
                        throw new InvalidOperationException("An object cannot be moved into its own parent");
                    }
                }
                else
                {
                    consumablePasted = CloneObject(consumable, clipboardObjectToPaste.DbSource, stepDataManager);
                    this.SetCopyName(consumablePasted, consumable, processStep, stepDataManager);
                    consumablePasted.SetIsMasterData(isMasterData);
                    consumablePasted.SetOwner(owner);
                }

                CurrencyConversionManager.ConvertCurrency(processStep, clipboardObjectToPaste.Entity, consumablePasted, stepDataManager.DatabaseId, clipboardObjectToPaste.DbSource);
                consumablePasted.ProcessStep = processStep;
                if (addToContext)
                {
                    processStep.Consumables.Add(consumablePasted);
                }

                return consumablePasted;
            }

            var commodity = clipboardObjectToPaste.Entity as Commodity;
            if (commodity != null)
            {
                Commodity commodityPasted = null;
                if (this.Operation == ClipboardOperation.Cut)
                {
                    // Check if the process step parent is the same as the recipient
                    bool canPaste = false;
                    var commodityParent = commodity.ProcessStep;

                    canPaste = commodityParent == null || commodityParent.Guid != processStep.Guid;
                    if (canPaste)
                    {
                        // Load the full commodity because it is needed for currency conversions
                        commodityPasted = stepDataManager.CommodityRepository.GetById(commodity.Guid);
                        commodityPasted.Part = null;
                        commodityPasted.ProcessStep = null;
                    }
                    else
                    {
                        // If the item parent is the same as the recipient throw an exception
                        throw new InvalidOperationException("An object cannot be moved into its own parent");
                    }
                }
                else
                {
                    commodityPasted = CloneObject(commodity, clipboardObjectToPaste.DbSource, stepDataManager);
                    this.SetCopyName(commodityPasted, commodity, processStep, stepDataManager);
                    commodityPasted.SetIsMasterData(isMasterData);
                    commodityPasted.SetOwner(owner);
                }

                CurrencyConversionManager.ConvertCurrency(processStep, clipboardObjectToPaste.Entity, commodityPasted, stepDataManager.DatabaseId, clipboardObjectToPaste.DbSource);

                commodityPasted.ProcessStep = processStep;
                if (addToContext)
                {
                    processStep.Commodities.Add(commodityPasted);
                }

                return commodityPasted;
            }

            var die = clipboardObjectToPaste.Entity as Die;
            if (die != null)
            {
                Die diePasted = null;
                if (this.Operation == ClipboardOperation.Cut)
                {
                    // Check if the process step parent is the same as the recipient
                    var canPaste = false;
                    var dieParent = die.ProcessStep;

                    canPaste = dieParent == null || dieParent.Guid != processStep.Guid;
                    if (canPaste)
                    {
                        // Load the full die because it is needed for currency conversions
                        diePasted = stepDataManager.DieRepository.GetById(die.Guid);
                        diePasted.ProcessStep = null;
                    }
                    else
                    {
                        // If the item parent is the same as the recipient throw an exception
                        throw new InvalidOperationException("An object cannot be moved into its own parent");
                    }
                }
                else
                {
                    diePasted = CloneObject(die, clipboardObjectToPaste.DbSource, stepDataManager);
                    this.SetCopyName(diePasted, die, processStep, stepDataManager);
                    diePasted.SetIsMasterData(isMasterData);
                    diePasted.SetOwner(owner);
                }

                CurrencyConversionManager.ConvertCurrency(processStep, clipboardObjectToPaste.Entity, diePasted, stepDataManager.DatabaseId, clipboardObjectToPaste.DbSource);

                diePasted.ProcessStep = processStep;
                if (addToContext)
                {
                    processStep.Dies.Add(diePasted);
                }

                return diePasted;
            }

            return null;
        }

        /// <summary>
        /// Pastes the clipboard objects into a process.
        /// </summary>
        /// <param name="processDataManager">The process data manager.</param>
        /// <param name="clipboardObjectToPaste">The clipboard object to paste into the process.</param>
        /// <param name="process">The process into which to paste.</param>
        /// <returns>
        /// The pasted object.
        /// </returns>
        /// <exception cref="BusinessException">The clipboard object was not fully loaded and couldn't be found in the source database.</exception>
        /// <exception cref="BusinessException">Could not determine the database in which process resides.</exception>
        /// <exception cref="BusinessException">Cloning the clipboard object failed.</exception>
        private object PasteIntoProcess(
            IDataSourceManager processDataManager,
            ClipboardObject clipboardObjectToPaste,
            Process process)
        {
            var clipboardStep = clipboardObjectToPaste.Entity as ProcessStep;
            if (clipboardStep == null)
            {
                return null;
            }

            return (this.Operation == ClipboardOperation.Cut) ?
                this.HandleProcessStepCut(processDataManager, clipboardObjectToPaste.DataManager, clipboardObjectToPaste.DbSource, clipboardStep, process) :
                this.HandleProcessStepCopy(processDataManager, clipboardObjectToPaste.DbSource, clipboardStep, process);
        }

        /// <summary>
        /// Handle process step copy.
        /// </summary>
        /// <param name="processDataManager">The process data manager.</param>
        /// <param name="databaseSource">The database source.</param>
        /// <param name="processStep">The process step.</param>
        /// <param name="process">The process.</param>
        /// <returns>The copied new process step.</returns>
        private ProcessStep HandleProcessStepCopy(
            IDataSourceManager processDataManager,
            DbIdentifier databaseSource,
            ProcessStep processStep,
            Process process)
        {
            IDataSourceManager sourceChangeSet = null;
            ProcessStep stepToPaste = null;

            // If the cloned source is not an entity stored in the database, directly clone the object source.
            // Otherwise fully load the entity from the database.
            if (databaseSource == DbIdentifier.NotSet)
            {
                stepToPaste = processStep;
            }
            else
            {
                // Load the step from the database.
                // TODO: improvement - determine if the step comes from processChangeSet and get it from there (it will improve the copy/paste time inside the same part/assembly.
                sourceChangeSet = DataAccessFactory.CreateDataSourceManager(databaseSource);
                stepToPaste = sourceChangeSet.ProcessStepRepository.GetProcessStepFull(processStep.Guid);
                if (stepToPaste == null)
                {
                    log.Error("Could not load a process step from the database for a paste operation.");
                    throw new BusinessException(ErrorCodes.PastedObjectNotAvailable);
                }
            }

            var stepCopy = this.GetProcessStepClone(sourceChangeSet, processDataManager, stepToPaste, process);

            CurrencyConversionManager.ConvertCurrency(
                process,
                processStep,
                stepCopy,
                processDataManager.DatabaseId,
                databaseSource);

            process.Steps.Add(stepCopy);
            processDataManager.ProcessRepository.Save(process);
            processDataManager.SaveChanges();

            return stepCopy;
        }

        /// <summary>
        /// Handle process step cut operation.
        /// </summary>
        /// <param name="processDataManager">The process data manager.</param>
        /// <param name="sourceDataManager">The source entity data manager.</param>
        /// <param name="databaseSource">The database source.</param>
        /// <param name="processStep">The process step.</param>
        /// <param name="process">The process.</param>
        /// <returns> The moved process step.</returns>
        private ProcessStep HandleProcessStepCut(
            IDataSourceManager processDataManager,
            IDataSourceManager sourceDataManager,
            DbIdentifier databaseSource,
            ProcessStep processStep,
            Process process)
        {
            var stepProcess = processStep.Process;
            stepProcess.Steps.Remove(processStep);
            processStep.Process = null;

            var pastedStep = sourceDataManager.ProcessStepRepository.GetProcessStepFull(processStep.Guid);
            var pastedStepProcess = sourceDataManager.ProcessRepository.GetProcessWithSteps(stepProcess.Guid);

            // Remove the process step from the old process.
            pastedStepProcess.Steps.Remove(pastedStep);
            pastedStep.Process = null;

            // Create a clone for the process step and made the connections with the new process.
            var processStepClone = this.GetProcessStepClone(processDataManager, processDataManager, pastedStep, process);
            processStepClone.Process = process;
            process.Steps.Add(processStepClone);

            CurrencyConversionManager.ConvertCurrency(
                    process,
                    processStep,
                    processStepClone,
                    processDataManager.DatabaseId,
                    databaseSource);

            sourceDataManager.ProcessStepRepository.RemoveAll(pastedStep);
            sourceDataManager.SaveChanges();

            processDataManager.ProcessRepository.Save(process);
            processDataManager.SaveChanges();

            var deleteStepMsg = new EntityChangedMessage(Notification.MyProjectsEntityChanged)
            {
                Entity = processStep,
                Parent = stepProcess,
                ChangeType = EntityChangeType.EntityDeleted
            };

            // Notify the projects tree that the old process step was deleted.
            this.SendEntityChangedMessages(new List<EntityChangedMessage> { deleteStepMsg });

            return processStepClone;
        }

        /// <summary>
        /// The get process step clone.
        /// </summary>
        /// <param name="sourceDataManager">The source change manager.</param>
        /// <param name="targetDataManager">The target data manager.</param>
        /// <param name="processStep">The process step.</param>
        /// <param name="process">The process.</param>
        /// <returns>The process step created clone.</returns>
        private ProcessStep GetProcessStepClone(
            IDataSourceManager sourceDataManager,
            IDataSourceManager targetDataManager,
            ProcessStep processStep,
            Process process)
        {
            ProcessStep stepClone = null;

            // Clone the step and add it in the process
            Type clonedStepType = null;
            object processParent = null;
            if (process.Assemblies.Count == 1)
            {
                clonedStepType = typeof(AssemblyProcessStep);
                processParent = process.Assemblies.First();
            }
            else if (process.Parts.Count == 1)
            {
                clonedStepType = typeof(PartProcessStep);
                processParent = process.Parts.First();
            }

            var cloneManager = new CloneManager(sourceDataManager, targetDataManager);
            stepClone = cloneManager.Clone(processStep, clonedStepType);
            if (stepClone != null)
            {
                this.SetCopyName(stepClone, processStep, process, targetDataManager);

                // Set the process step's index to max index + 1
                stepClone.Index = 0;
                if (process.Steps.Count > 0)
                {
                    stepClone.Index = process.Steps.Max(ps => ps.Index) + 1;
                }

                // Set the process' owner and master data flag on the process step and its sub-objects
                stepClone.SetOwner(process.Owner);
                stepClone.SetIsMasterData(process.IsMasterData);

                // Update the pated step's batch size to match the default batch size for the part/assembly into whose process is pasted
                var batchSizePerYear = 0;
                var yearlyProdQty = 0;

                var part = processParent as Part;
                if (part != null)
                {
                    batchSizePerYear = part.BatchSizePerYear.GetValueOrDefault(0);
                    yearlyProdQty = part.YearlyProductionQuantity.GetValueOrDefault(0);
                }
                else
                {
                    var assy = processParent as Assembly;
                    if (assy != null)
                    {
                        batchSizePerYear = assy.BatchSizePerYear.GetValueOrDefault(0);
                        yearlyProdQty = assy.YearlyProductionQuantity.GetValueOrDefault(0);
                    }
                }

                if (batchSizePerYear != 0)
                {
                    double batchSize = Math.Ceiling(Convert.ToDouble(yearlyProdQty) / Convert.ToDouble(batchSizePerYear));
                    if (batchSize < int.MaxValue)
                    {
                        stepClone.BatchSize = Convert.ToInt32(batchSize);
                    }
                    else
                    {
                        log.Warn(
                            "The batch size of a process step calculated during a copy step operation was greater than the max integer value.");
                    }
                }
            }

            return stepClone;
        }

        /// <summary>
        /// Modifies the name of a copied object according to some conventions.
        /// </summary>
        /// <param name="copiedObject">The object whose name to change.</param>
        /// <param name="source">The object whose copy is <paramref name="copiedObject"/>.</param>
        /// <param name="target">The object into which the copy is pasted.</param>
        /// <param name="dataSourceManager">The data source manager used by the copy operation.</param>
        private void SetCopyName(INameable copiedObject, object source, IIdentifiable target, IDataSourceManager dataSourceManager)
        {
            if (this.Operation != ClipboardOperation.Copy || copiedObject == null)
            {
                return;
            }

            // After the source type is found, the source's parent is compared with the target;
            // if their id is equal, the object's name is modified according to the conventions.
            bool changeName = false;

            var assySource = source as Assembly;
            if (assySource != null)
            {
                if (dataSourceManager.AssemblyRepository.GetParentAssemblyId(assySource.Guid) == target.Guid ||
                    dataSourceManager.AssemblyRepository.GetParentProjectId(assySource, true) == target.Guid)
                {
                    changeName = true;
                }
            }
            else
            {
                var rawPartSource = source as RawPart;
                if (rawPartSource != null)
                {
                    var parentPart = rawPartSource.ParentOfRawPart.FirstOrDefault();
                    var parentPartId = parentPart != null ? parentPart.Guid : Guid.Empty;

                    if (parentPart == null)
                    {
                        parentPartId = dataSourceManager.PartRepository.GetParentOfRawPartId(rawPartSource.Guid);
                    }

                    if (parentPartId == target.Guid)
                    {
                        changeName = true;
                    }
                }
                else
                {
                    var partSource = source as Part;
                    if (partSource != null)
                    {
                        if (dataSourceManager.PartRepository.GetParentAssemblyId(partSource) == target.Guid ||
                            dataSourceManager.PartRepository.GetParentProjectId(partSource, true) == target.Guid)
                        {
                            changeName = true;
                        }
                    }
                    else
                    {
                        var projectSource = source as Project;
                        if (projectSource != null)
                        {
                            var crtParentFolderId = dataSourceManager.ProjectRepository.GetParentFolderId(projectSource);
                            var targetFolderId = target != null ? target.Guid : Guid.Empty;
                            if (crtParentFolderId == targetFolderId)
                            {
                                changeName = true;
                            }
                        }
                        else
                        {
                            var rawMaterialSource = source as RawMaterial;
                            if (rawMaterialSource != null)
                            {
                                var parentPart = rawMaterialSource.Part;
                                var parentPartId = parentPart != null ? parentPart.Guid : Guid.Empty;

                                if (parentPart == null)
                                {
                                    parentPartId = dataSourceManager.RawMaterialRepository.GetParentPartId(rawMaterialSource);
                                }

                                if (parentPartId == target.Guid)
                                {
                                    changeName = true;
                                }
                            }
                            else
                            {
                                var machineSource = source as Machine;
                                if (machineSource != null)
                                {
                                    var parentProcessStep = machineSource.ProcessStep;
                                    var parentProcessStepId = parentProcessStep != null ? parentProcessStep.Guid : Guid.Empty;

                                    if (parentProcessStep == null)
                                    {
                                        parentProcessStepId = dataSourceManager.MachineRepository.GetParentProcessStepId(machineSource);
                                    }

                                    if (parentProcessStepId == target.Guid)
                                    {
                                        changeName = true;
                                    }
                                }
                                else
                                {
                                    var consumableSource = source as Consumable;
                                    if (consumableSource != null)
                                    {
                                        var parentProcessStep = consumableSource.ProcessStep;
                                        var parentProcessStepId = parentProcessStep != null ? parentProcessStep.Guid : Guid.Empty;

                                        if (parentProcessStep == null)
                                        {
                                            parentProcessStepId = dataSourceManager.ConsumableRepository.GetParentProcessStepId(consumableSource);
                                        }

                                        if (parentProcessStepId == target.Guid)
                                        {
                                            changeName = true;
                                        }
                                    }
                                    else
                                    {
                                        var commoditySource = source as Commodity;
                                        if (commoditySource != null)
                                        {
                                            var parentId = Guid.Empty;
                                            var parentPart = commoditySource.Part;
                                            var parentProcessStep = commoditySource.ProcessStep;

                                            if (parentPart != null)
                                            {
                                                parentId = parentPart.Guid;
                                            }
                                            else if (parentProcessStep != null)
                                            {
                                                parentId = parentProcessStep.Guid;
                                            }
                                            else
                                            {
                                                parentId = dataSourceManager.CommodityRepository.GetParentPartId(commoditySource);
                                                if (parentId == Guid.Empty)
                                                {
                                                    parentId = dataSourceManager.CommodityRepository.GetParentProcessStepId(commoditySource);
                                                }
                                            }

                                            if (parentId == target.Guid)
                                            {
                                                changeName = true;
                                            }
                                        }
                                        else
                                        {
                                            var dieSource = source as Die;
                                            if (dieSource != null)
                                            {
                                                var parentProcessStep = dieSource.ProcessStep;
                                                var parentProcessStepId = parentProcessStep != null ? parentProcessStep.Guid : Guid.Empty;

                                                if (parentProcessStep == null)
                                                {
                                                    parentProcessStepId = dataSourceManager.DieRepository.GetParentProcessStepId(dieSource);
                                                }

                                                if (parentProcessStepId == target.Guid)
                                                {
                                                    changeName = true;
                                                }
                                            }
                                            else
                                            {
                                                var processStepSource = source as ProcessStep;
                                                if (processStepSource != null)
                                                {
                                                    var parentProcess = processStepSource.Process;
                                                    var parentProcessId = parentProcess != null ? parentProcess.Guid : Guid.Empty;

                                                    if (parentProcess == null)
                                                    {
                                                        parentProcessId = dataSourceManager.ProcessStepRepository.GetParentProcessId(processStepSource);
                                                    }

                                                    if (parentProcessId == target.Guid)
                                                    {
                                                        changeName = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (changeName)
            {
                var name = ZPKTool.Gui.Resources.LocalizedResources.General_CopyOf + " " + copiedObject.Name;
                if (name.Length > 100)
                {
                    name = name.Substring(0, 100);
                }

                copiedObject.Name = name;
            }
        }

        /// <summary>
        /// Checks if a specified assembly is the parent of another assembly, directly or indirectly (parent of parent ...).
        /// </summary>
        /// <param name="parent">The assembly to check if it is the parent.</param>
        /// <param name="assembly">The assembly supposed to be the child.</param>
        /// <returns>true if <paramref name="parent"/> is the parent folder of <paramref name="assembly"/>; otherwise false.</returns>
        public bool CheckAssemblyIfParentOf(Assembly parent, Assembly assembly)
        {
            if (assembly == null || parent == null)
            {
                return false;
            }

            bool isParent = false;
            while (assembly != null)
            {
                //// TODO: what is the point of this? that is not the way to load data...
                ////if (!assembly.ParentAssemblyReference.IsLoaded)
                ////{
                ////    assembly.ParentAssemblyReference.Load();
                ////}

                if (assembly.ParentAssembly != null && assembly.ParentAssembly.Guid == parent.Guid)
                {
                    isParent = true;
                    break;
                }

                assembly = assembly.ParentAssembly;
            }

            return isParent;
        }

        /// <summary>
        /// Removes all process step assembly amount references for the provided assembly entity
        /// </summary>
        /// <param name="dataSourceManager">The data source manager</param>
        /// <param name="assembly">The assembly to remove the process step amount from</param>
        /// <returns>
        /// A list of <see cref="EntityChangedMessage"/> for every process step amount removed
        /// Those messages are used to refresh the tree
        /// </returns>
        private List<EntityChangedMessage> RemoveProcessStepAssemblyAmount(IDataSourceManager dataSourceManager, Assembly assembly)
        {
            var messages = new List<EntityChangedMessage>();
            var assyAmountRepository = dataSourceManager.Repository(typeof(ProcessStepAssemblyAmount));
            foreach (var amount in assembly.ProcessStepAssemblyAmounts.ToList())
            {
                assyAmountRepository.Remove(amount);
                var step = dataSourceManager.ProcessStepRepository.GetByKey(amount.ProcessStepGuid);

                string notification = assembly.IsMasterData ? Notification.MasterDataEntityChanged : Notification.MyProjectsEntityChanged;
                EntityChangedMessage msg = new EntityChangedMessage(notification)
                {
                    Entity = amount,
                    Parent = step,
                    ChangeType = EntityChangeType.EntityDeleted
                };
                messages.Add(msg);
            }

            return messages;
        }

        /// <summary>
        /// Removes all process step part amount references for the provided part entity
        /// </summary>
        /// <param name="dataSourceManager">The data source manager</param>
        /// <param name="part">The part to remove the process step amount from</param>
        /// <returns>
        /// A list of <see cref="EntityChangedMessage"/> for every process step amount removed
        /// Those messages are used to refresh the tree
        /// </returns>
        private List<EntityChangedMessage> RemoveProcessStepPartAmount(IDataSourceManager dataSourceManager, Part part)
        {
            var messages = new List<EntityChangedMessage>();
            var partAmountRepository = dataSourceManager.Repository(typeof(ProcessStepPartAmount));
            foreach (var amount in part.ProcessStepPartAmounts.ToList())
            {
                partAmountRepository.Remove(amount);
                var step = dataSourceManager.ProcessStepRepository.GetByKey(amount.ProcessStepGuid);

                string notification = part.IsMasterData ? Notification.MasterDataEntityChanged : Notification.MyProjectsEntityChanged;
                EntityChangedMessage msg = new EntityChangedMessage(notification)
                {
                    Entity = amount,
                    Parent = step,
                    ChangeType = EntityChangeType.EntityDeleted
                };
                messages.Add(msg);
            }

            return messages;
        }

        /// <summary>
        /// Sends a list of <see cref="EntityChangedMessage"/> using the application messenger
        /// </summary>
        /// <param name="messages">The messages to send</param>
        private void SendEntityChangedMessages(List<EntityChangedMessage> messages)
        {
            var messenger = new Messenger();
            foreach (var message in messages)
            {
                messenger.Send(message);
            }
        }

        #endregion Paste methods

        #region Cloning methods

        /// <summary>
        /// Clones an object. The cloned object is used for pasting.
        /// </summary>
        /// <param name="objectToClone">The object to clone.</param>
        /// <param name="objectToCloneSource">The persistence source of the object to clone.</param>
        /// <param name="cloneDataManager">The clone data context.</param>
        /// <returns>
        /// The cloned object
        /// </returns>
        /// <exception cref="BusinessException">Thrown when <paramref name="objectToClone"/> is not fully loaded and
        /// the database no longer contains it.</exception>
        /// <exception cref="BusinessException">Thrown when <paramref name="recipient"/> isn't part of any context.</exception>
        private Project CloneObject(Project objectToClone, DbIdentifier objectToCloneSource, IDataSourceManager cloneDataManager)
        {
            Project proj = null;
            IDataSourceManager sourceChangeSet = null;

            // If the cloned source is not an entity stored in the database, directly clone the object source.
            // Otherwise fully load the entity from the database.
            if (objectToCloneSource == DbIdentifier.NotSet)
            {
                proj = objectToClone;
            }
            else
            {
                sourceChangeSet = DataAccessFactory.CreateDataSourceManager(objectToCloneSource);
                proj = sourceChangeSet.ProjectRepository.GetProjectForEdit(objectToClone.Guid);
                if (proj == null)
                {
                    log.Error("Could not load an project from the database for a paste operation.");
                    throw new BusinessException(ErrorCodes.PastedObjectNotAvailable);
                }
            }

            CloneManager cloneManager = new CloneManager(sourceChangeSet, cloneDataManager);
            Project projClone = cloneManager.Clone(proj);

            return projClone;
        }

        /// <summary>
        /// Clones an object. The cloned object is used for pasting.
        /// </summary>
        /// <param name="objectToClone">The object to clone.</param>
        /// <param name="objectToCloneSource">The persistence source of the object to clone.</param>
        /// <param name="cloneDataContext">The clone data context.</param>
        /// <returns>
        /// The cloned object
        /// </returns>
        /// <exception cref="BusinessException">Thrown when <paramref name="objectToClone"/> is not fully loaded and
        /// the database no longer contains it.</exception>
        /// <exception cref="BusinessException">Thrown when <paramref name="recipient"/> isn't part of any context.</exception>
        private Assembly CloneObject(Assembly objectToClone, DbIdentifier objectToCloneSource, IDataSourceManager cloneDataContext)
        {
            Assembly assy = null;
            IDataSourceManager sourceChangeSet = null;

            // If the cloned source is not an entity stored in the database, directly clone the object source.
            // Otherwise fully load the entity from the database.
            if (objectToCloneSource == DbIdentifier.NotSet)
            {
                assy = objectToClone;
            }
            else
            {
                // Load the object to clone form its source db context.
                sourceChangeSet = DataAccessFactory.CreateDataSourceManager(objectToCloneSource);
                assy = sourceChangeSet.AssemblyRepository.GetAssemblyFull(objectToClone.Guid);
                if (assy == null)
                {
                    log.Error("Could not load an assembly from the database for a paste operation.");
                    throw new BusinessException(ErrorCodes.PastedObjectNotAvailable);
                }
            }

            CloneManager cloneManager = new CloneManager(sourceChangeSet, cloneDataContext);
            Assembly assyClone = cloneManager.Clone(assy);

            return assyClone;
        }

        /// <summary>
        /// Clones an object. The cloned object is used for pasting.
        /// </summary>
        /// <param name="objectToClone">The object to clone.</param>
        /// <param name="objectToCloneSource">The persistence source of the object to clone.</param>
        /// <param name="cloneDataContext">The clone data context.</param>
        /// <returns>
        /// The cloned object
        /// </returns>
        /// <exception cref="BusinessException">Thrown when <paramref name="objectToClone"/> is not fully loaded and
        /// the database no longer contains it.</exception>
        /// <exception cref="BusinessException">Thrown when <paramref name="recipient"/> could not be found in any context.</exception>
        private Part CloneObject(Part objectToClone, DbIdentifier objectToCloneSource, IDataSourceManager cloneDataContext)
        {
            Part part = null;
            IDataSourceManager sourceChangeSet = null;

            // If the cloned source is not an entity stored in the database, directly clone the object source.
            // Otherwise fully load the entity from the database.
            if (objectToCloneSource == DbIdentifier.NotSet)
            {
                part = objectToClone;
            }
            else
            {
                sourceChangeSet = DataAccessFactory.CreateDataSourceManager(objectToCloneSource);
                part = sourceChangeSet.PartRepository.GetPartFull(objectToClone.Guid);
                if (part == null)
                {
                    log.Error("Could not load a part from the database for a paste operation.");
                    throw new BusinessException(ErrorCodes.PastedObjectNotAvailable);
                }
            }

            CloneManager cloneManager = new CloneManager(sourceChangeSet, cloneDataContext);
            Part partClone = cloneManager.Clone(part);

            return partClone;
        }

        /// <summary>
        /// Clones an object. The cloned object is used for pasting.
        /// </summary>
        /// <param name="objectToClone">The object to clone.</param>
        /// <param name="objectToCloneSource">The persistence source of the object to clone.</param>
        /// <param name="cloneDataContext">The clone data context.</param>
        /// <returns>
        /// The cloned object
        /// </returns>
        /// <exception cref="BusinessException">Thrown when <paramref name="objectToClone"/> is not fully loaded and
        /// the database no longer contains it.</exception>
        /// <exception cref="BusinessException">Thrown when <paramref name="recipient"/> can not be found in any context.</exception>
        private RawMaterial CloneObject(RawMaterial objectToClone, DbIdentifier objectToCloneSource, IDataSourceManager cloneDataContext)
        {
            RawMaterial rawMaterial = null;
            IDataSourceManager sourceChangeSet = null;

            // If the cloned source is not an entity stored in the database, directly clone the object source.
            // Otherwise fully load the entity from the database.
            if (objectToCloneSource == DbIdentifier.NotSet)
            {
                rawMaterial = objectToClone;
            }
            else
            {
                sourceChangeSet = DataAccessFactory.CreateDataSourceManager(objectToCloneSource);
                rawMaterial = sourceChangeSet.RawMaterialRepository.GetById(objectToClone.Guid);
                if (rawMaterial == null)
                {
                    log.Error("Could not load a raw material from the database for a paste operation.");
                    throw new BusinessException(ErrorCodes.PastedObjectNotAvailable);
                }
            }

            CloneManager cloneManager = new CloneManager(sourceChangeSet, cloneDataContext);
            RawMaterial materialClone = cloneManager.Clone(rawMaterial);
            return materialClone;
        }

        /// <summary>
        /// Clones an object. The cloned object is used for pasting.
        /// </summary>
        /// <param name="objectToClone">The object to clone.</param>
        /// <param name="objectToCloneSource">The persistence source of the object to clone.</param>
        /// <param name="cloneDataContext">The clone data context.</param>
        /// <returns>
        /// The cloned object
        /// </returns>
        /// <exception cref="BusinessException">Thrown when <paramref name="objectToClone"/> is not fully loaded and
        /// the database no longer contains it.</exception>
        /// <exception cref="BusinessException">Thrown when <paramref name="objectToClone"/> couldn't be found in any context.</exception>
        private Commodity CloneObject(Commodity objectToClone, DbIdentifier objectToCloneSource, IDataSourceManager cloneDataContext)
        {
            IDataSourceManager sourceChangeSet = null;
            Commodity commodity = null;

            // If the cloned source is not an entity stored in the database, directly clone the object source.
            // Otherwise fully load the entity from the database.
            if (this.areClipboardObjectsFullyLoaded)
            {
                commodity = objectToClone;
                sourceChangeSet = cloneDataContext;
            }
            else
            {
                sourceChangeSet = DataAccessFactory.CreateDataSourceManager(objectToCloneSource);
                commodity = sourceChangeSet.CommodityRepository.GetById(objectToClone.Guid);
                if (commodity == null)
                {
                    log.Error("Could not load a commodity from the database for a paste operation.");
                    throw new BusinessException(ErrorCodes.PastedObjectNotAvailable);
                }
            }

            CloneManager cloneManager = new CloneManager(sourceChangeSet, cloneDataContext);
            Commodity commodityCopy = cloneManager.Clone(commodity);
            return commodityCopy;
        }

        /// <summary>
        /// Clones an object. The cloned object is used for pasting.
        /// </summary>
        /// <param name="objectToClone">The object to clone.</param>
        /// <param name="objectToCloneSource">The persistence source of the object to clone.</param>
        /// <param name="cloneDataContext">The clone data context.</param>
        /// <returns>
        /// The cloned object
        /// </returns>
        /// <exception cref="BusinessException">Thrown when <paramref name="objectToClone"/> is not fully loaded and the database no longer contains it.</exception>
        /// <exception cref="BusinessException">Thrown when <paramref name="recipient"/> could not be found in any context.</exception>
        private Machine CloneObject(Machine objectToClone, DbIdentifier objectToCloneSource, IDataSourceManager cloneDataContext)
        {
            IDataSourceManager sourceChangeSet = null;
            Machine machine = null;

            // If the cloned source is not an entity stored in the database, directly clone the object source.
            // Otherwise fully load the entity from the database.
            if (this.areClipboardObjectsFullyLoaded)
            {
                machine = objectToClone;
                sourceChangeSet = cloneDataContext;
            }
            else
            {
                sourceChangeSet = DataAccessFactory.CreateDataSourceManager(objectToCloneSource);
                machine = sourceChangeSet.MachineRepository.GetById(objectToClone.Guid);
                if (machine == null)
                {
                    log.Error("Could not load a machine from the database for a paste operation.");
                    throw new BusinessException(ErrorCodes.PastedObjectNotAvailable);
                }
            }

            CloneManager cloneManager = new CloneManager(sourceChangeSet, cloneDataContext);
            Machine machineCopy = cloneManager.Clone(machine);
            return machineCopy;
        }

        /// <summary>
        /// Clones an object. The cloned object is used for pasting.
        /// </summary>
        /// <param name="objectToClone">The object to clone.</param>
        /// <param name="objectToCloneSource">The persistence source of the object to clone.</param>
        /// <param name="cloneDataContext">The clone data context.</param>
        /// <returns>
        /// The cloned object
        /// </returns>
        /// <exception cref="BusinessException">Thrown when <paramref name="objectToClone"/> is not fully loaded and
        /// the database no longer contains it.</exception>
        /// <exception cref="BusinessException">Thrown when <paramref name="recipient"/> couldn't be found in any of the contexts.</exception>
        private Consumable CloneObject(Consumable objectToClone, DbIdentifier objectToCloneSource, IDataSourceManager cloneDataContext)
        {
            IDataSourceManager sourceChangeSet = null;
            Consumable consumable = null;

            // If the cloned source is not an entity stored in the database, directly clone the object source.
            // Otherwise fully load the entity from the database.
            if (this.areClipboardObjectsFullyLoaded)
            {
                consumable = objectToClone;
                sourceChangeSet = cloneDataContext;
            }
            else
            {
                sourceChangeSet = DataAccessFactory.CreateDataSourceManager(objectToCloneSource);
                consumable = sourceChangeSet.ConsumableRepository.GetById(objectToClone.Guid);
                if (consumable == null)
                {
                    log.Error("Could not load an assembly from the database for a paste operation.");
                    throw new BusinessException(ErrorCodes.PastedObjectNotAvailable);
                }
            }

            CloneManager cloneManager = new CloneManager(sourceChangeSet, cloneDataContext);
            Consumable consumableCopy = cloneManager.Clone(consumable);
            return consumableCopy;
        }

        /// <summary>
        /// Clones an object. The cloned object is used for pasting.
        /// </summary>
        /// <param name="objectToClone">The object to clone.</param>
        /// <param name="objectToCloneSource">The persistence source of the object to clone.</param>
        /// <param name="cloneDataContext">The clone data context.</param>
        /// <returns>
        /// The cloned object
        /// </returns>
        /// <exception cref="BusinessException">Thrown when <paramref name="objectToClone"/> is not fully loaded and
        /// the database no longer contains it.</exception>
        /// <exception cref="BusinessException">Thrown when <paramref name="recipient"/> could not be found in any of the contexts.</exception>
        private Die CloneObject(Die objectToClone, DbIdentifier objectToCloneSource, IDataSourceManager cloneDataContext)
        {
            IDataSourceManager sourceChangeSet = null;
            Die die = null;

            // If the cloned source is not an entity stored in the database, directly clone the object source.
            // Otherwise fully load the entity from the database.
            if (this.areClipboardObjectsFullyLoaded)
            {
                die = objectToClone;
                sourceChangeSet = cloneDataContext;
            }
            else
            {
                sourceChangeSet = DataAccessFactory.CreateDataSourceManager(objectToCloneSource);
                die = sourceChangeSet.DieRepository.GetById(objectToClone.Guid);
                if (die == null)
                {
                    log.Error("Could not load an assembly from the database for a paste operation.");
                    throw new BusinessException(ErrorCodes.PastedObjectNotAvailable);
                }
            }

            CloneManager cloneManager = new CloneManager(sourceChangeSet, cloneDataContext);
            Die dieCopy = cloneManager.Clone(die);
            return dieCopy;
        }

        #endregion Cloning methods

        #region Helpers

        /// <summary>
        /// Gets the error message that will be displayed for the pasted objects that have an error (it concatenates multiple error messages).
        /// </summary>
        /// <param name="pastedObjects">The pasted objects for which the error message will be set.</param>
        /// <returns>The string containing the error messages.</returns>
        public string GetErrorMessageToDisplay(ICollection<PastedData> pastedObjects)
        {
            string displayedMessage = string.Empty;
            var windowService = new WindowService(new MessageDialogService(new DispatcherService()), new DispatcherService());

            var errorsNo = pastedObjects.Count(o => o.Error != null);
            if (errorsNo == 1)
            {
                displayedMessage = windowService.MessageDialogService.GetErrorMessage(pastedObjects.FirstOrDefault(o => o.Error != null).Error);
            }
            else if (errorsNo > 1)
            {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.AppendLine(LocalizedResources.General_PasteErrors);
                foreach (var data in pastedObjects.Where(o => o.Error != null))
                {
                    string entityName = data.Entity is INameable ? ((INameable)data.Entity).Name : string.Empty;
                    errorMessage.AppendLine("- \"" + entityName + "\": " + windowService.MessageDialogService.GetErrorMessage(data.Error));
                }

                // Return all errors into a single message.
                displayedMessage = errorMessage.ToString();
            }

            return displayedMessage;
        }

        #endregion Helpers
    }
}