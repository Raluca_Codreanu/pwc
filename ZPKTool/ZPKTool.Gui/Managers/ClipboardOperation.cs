﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.Managers
{
    /// <summary>
    /// The possible operations on the clipboard object.
    /// </summary>
    public enum ClipboardOperation
    {
        /// <summary>
        /// Copy the clipboard object.
        /// </summary>
        Copy = 0,

        /// <summary>
        /// Move the clipboard object.
        /// </summary>
        Cut = 1
    }
}
