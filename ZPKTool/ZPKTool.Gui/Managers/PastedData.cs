﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.Managers
{
    /// <summary>
    /// Contains the data for a pasted object (the entity and an error message, if an error occurred during the paste operation).
    /// </summary>
    public class PastedData
    {
        /// <summary>
        /// Gets or sets the pasted entity.
        /// </summary>
        public object Entity { get; set; }

        /// <summary>
        /// Gets or sets the error occurred during the paste operation for the entity (if exists).
        /// </summary>
        public Exception Error { get; set; }
    }
}
