﻿using ZPKTool.DataAccess;

namespace ZPKTool.Gui.Managers
{
    /// <summary>
    /// A class that keeps the data of an object saved to clipboard.
    /// </summary>
    public class ClipboardObject
    {
        /// <summary>
        /// Gets or sets the entity stored in clipboard and available for paste.
        /// </summary>
        public object Entity { get; set; }

        /// <summary>
        /// Gets or sets the source database of the clipboard object.
        /// </summary>
        public DbIdentifier DbSource { get; set; }

        /// <summary>
        /// Gets or sets the entity stored in clipboard and available for paste.
        /// </summary>
        public IDataSourceManager DataManager { get; set; }
    }
}
