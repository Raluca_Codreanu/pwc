﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Linq;
using System.Windows.Input;
using ZPKTool.Business;
using ZPKTool.Business.Export;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Utils;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Managers
{
    /// <summary>
    /// The command manager for the application project explorer (ex:Projects,Bookmarks Tree)
    /// </summary>
    public class ProjectsExplorerCommandManager
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Represents a collection of entities and their bookmark state.
        /// The key represents the entity's id and the value, if true means that the entity has a bookmark and if false it does not.
        /// </summary>
        private static Dictionary<Guid, bool> bookmarksStateCache = new Dictionary<Guid, bool>();

        /// <summary>
        /// The messenger service.
        /// </summary>
        private readonly IMessenger messenger;

        /// <summary>
        /// The window service.
        /// </summary>
        private readonly IWindowService windowService;

        /// <summary>
        /// The composition container.
        /// </summary>
        private readonly CompositionContainer compositionContainer;

        /// <summary>
        /// The PleaseWaitService reference.
        /// </summary>
        private readonly IPleaseWaitService pleaseWaitService;

        /// <summary>
        /// The ImportService reference.
        /// </summary>
        private readonly IImportService importService;

        /// <summary>
        /// The view model that the commands manager refers to
        /// </summary>
        private ProjectsExplorerBaseViewModel projectsExplorerBase;

        /// <summary>
        /// The service that provides measurement units and currency data.
        /// </summary>
        private IUnitsService unitsService;

        #endregion Attributes

        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectsExplorerCommandManager" /> class.
        /// </summary>
        /// <param name="projectsExplorerBase">The view model that the command manager refer to</param>
        /// <param name="compositionContainer">The composition container.</param>
        /// <param name="messenger">The messenger.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="pleaseWaitService">The please wait service.</param>
        /// <param name="importService">The import service.</param>
        /// <param name="unitsService">The service that provides measurement units and currency data.</param>
        public ProjectsExplorerCommandManager(
            ProjectsExplorerBaseViewModel projectsExplorerBase,
            CompositionContainer compositionContainer,
            IMessenger messenger,
            IWindowService windowService,
            IPleaseWaitService pleaseWaitService,
            IImportService importService,
            IUnitsService unitsService)
        {
            Argument.IsNotNull("projectsExplorerBase", projectsExplorerBase);
            this.projectsExplorerBase = projectsExplorerBase;

            this.messenger = messenger;
            this.windowService = windowService;
            this.compositionContainer = compositionContainer;
            this.pleaseWaitService = pleaseWaitService;
            this.importService = importService;
            this.unitsService = unitsService;

            this.InitializeCommands();
        }

        #region Commands

        /// <summary>
        /// Gets the Copy command.
        /// </summary>
        public ICommand CopyCommand { get; private set; }

        /// <summary>
        /// Gets the cut command.
        /// </summary>
        public ICommand CutCommand { get; private set; }

        /// <summary>
        /// Gets the paste command.
        /// </summary>
        public ICommand PasteCommand { get; private set; }

        /// <summary>
        /// Gets the Delete command.
        /// </summary>
        public ICommand DeleteCommand { get; private set; }

        /// <summary>
        /// Gets the rename command.
        /// </summary>
        public ICommand RenameCommand { get; private set; }

        /// <summary>
        /// Gets the Open Readonly command.
        /// </summary>
        public ICommand OpenReadonlyCommand { get; private set; }

        /// <summary>
        /// Gets the open batch reporting command.
        /// </summary>
        public ICommand OpenBatchReportingCommand { get; private set; }

        /// <summary>
        /// Gets the open report generator command.
        /// </summary>
        public ICommand OpenReportGeneratorCommand { get; private set; }

        /// <summary>
        /// Gets the mass data update command.
        /// </summary>
        public ICommand MassDataUpdateCommand { get; private set; }

        /// <summary>
        /// Gets the Import command.
        /// </summary>
        public ICommand ImportCommand { get; private set; }

        /// <summary>
        /// Gets the Export command.
        /// </summary>
        public ICommand ExportCommand { get; private set; }

        /// <summary>
        /// Gets the Release Project command.
        /// </summary>
        public ICommand ReleaseProjectCommand { get; private set; }

        /// <summary>
        /// Gets the Un-release Project command.
        /// </summary>
        public ICommand UnreleaseProjectCommand { get; private set; }

        /// <summary>
        /// Gets the create project folder command.
        /// </summary>
        public ICommand CreateProjectFolderCommand { get; private set; }

        /// <summary>
        /// Gets the create project command.
        /// </summary>
        public ICommand CreateProjectCommand { get; private set; }

        /// <summary>
        /// Gets the Create Assembly command.
        /// </summary>
        public ICommand CreateAssemblyCommand { get; private set; }

        /// <summary>
        /// Gets the Create Part command.
        /// </summary>
        public ICommand CreatePartCommand { get; private set; }

        /// <summary>
        /// Gets the Create Raw Material command.
        /// </summary>
        public ICommand CreateRawMaterialCommand { get; private set; }

        /// <summary>
        /// Gets the Create Commodity command.
        /// </summary>
        public ICommand CreateCommodityCommand { get; private set; }

        /// <summary>
        /// Gets the Create Consumable command.
        /// </summary>
        public ICommand CreateConsumableCommand { get; private set; }

        /// <summary>
        /// Gets the Create Machine command.
        /// </summary>
        public ICommand CreateMachineCommand { get; private set; }

        /// <summary>
        /// Gets the Create Die command.
        /// </summary>
        public ICommand CreateDieCommand { get; private set; }

        /// <summary>
        /// Gets the selective sync command.
        /// </summary>
        public ICommand SelectiveSyncCommand { get; private set; }

        /// <summary>
        /// Gets the BOM import command.
        /// </summary>
        public ICommand BomImportCommand { get; private set; }

        /// <summary>
        /// Gets the create Raw Part command.
        /// </summary>
        public ICommand CreateRawPartCommand { get; private set; }

        /// <summary>
        /// Gets the AddBookmark command.
        /// </summary>
        public ICommand AddBookmarkCommand { get; private set; }

        /// <summary>
        /// Gets the RemoveBookmark command.
        /// </summary>
        public ICommand RemoveBookmarkCommand { get; private set; }

        #endregion Commands

        /// <summary>
        /// Initializes the Project Explorer commands
        /// </summary>
        private void InitializeCommands()
        {
            this.CopyCommand = new DelegateCommand<object>(this.Copy, this.CanCopy);
            this.CutCommand = new DelegateCommand<object>(this.Cut, this.CanCut);
            this.PasteCommand = new DelegateCommand<object>(this.Paste, this.CanPaste);
            this.DeleteCommand = new DelegateCommand<object>(this.Delete, this.CanDelete);
            this.RenameCommand = new DelegateCommand<object>(this.Rename, this.CanRename);
            this.OpenReadonlyCommand = new DelegateCommand<object>(this.OpenReadonly, this.CanOpenReadonly);
            this.OpenBatchReportingCommand = new DelegateCommand<object>(this.OpenBatchReporting, this.CanOpenBatchReporting);
            this.OpenReportGeneratorCommand = new DelegateCommand<object>(this.OpenReportGenerator, this.CanOpenReportGenerator);
            this.MassDataUpdateCommand = new DelegateCommand<object>(this.OpenMassDataUpdate, this.CanOpenMassDataUpdate);
            this.ImportCommand = new DelegateCommand<object>(this.Import, this.CanImport);
            this.ExportCommand = new DelegateCommand<object>(this.Export, this.CanExport);
            this.ReleaseProjectCommand = new DelegateCommand<object>(this.ReleaseProject, this.CanReleaseProject);
            this.UnreleaseProjectCommand = new DelegateCommand<object>(this.UnreleaseProject, this.CanUnreleaseProject);
            this.CreateProjectFolderCommand = new DelegateCommand<object>(this.CreateProjectFolder, this.CanCreateProjectFolder);
            this.CreateProjectCommand = new DelegateCommand<object>(this.CreateProject, this.CanCreateProject);
            this.CreateAssemblyCommand = new DelegateCommand<object>(this.CreateAssembly, this.CanCreateAssembly);
            this.CreatePartCommand = new DelegateCommand<object>(this.CreatePart, this.CanCreatePart);
            this.CreateRawMaterialCommand = new DelegateCommand<object>(this.CreateRawMaterial, this.CanCreateRawMaterial);
            this.CreateCommodityCommand = new DelegateCommand<object>(this.CreateCommodity, this.CanCreateCommodity);
            this.CreateConsumableCommand = new DelegateCommand<object>(this.CreateConsumable, this.CanCreateConsumable);
            this.CreateMachineCommand = new DelegateCommand<object>(this.CreateMachine, this.CanCreateMachine);
            this.CreateDieCommand = new DelegateCommand<object>(this.CreateDie, this.CanCreateDie);
            this.SelectiveSyncCommand = new DelegateCommand<object>(this.SelectiveSync, this.CanSelectiveSync);
            this.BomImportCommand = new DelegateCommand<object>(this.BomImport, this.CanBomImport);
            this.CreateRawPartCommand = new DelegateCommand<object>(this.CreateRawPart, this.CanCreateRawPart);
            this.AddBookmarkCommand = new DelegateCommand<object>(this.AddBookmark, this.CanAddBookmark);
            this.RemoveBookmarkCommand = new DelegateCommand<object>(this.RemoveBookmark, this.CanRemoveBookmark);
        }

        #region Commands Handling

        /// <summary>
        /// Determines whether the Copy command can be execute.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        /// true if the command can be executed, false otherwise.
        /// </returns>
        private bool CanCopy(object parameter)
        {
            var selectedTreeItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedTreeItems == null
                || selectedTreeItems.Count == 0
                || !SecurityManager.Instance.CurrentUserHasRight(Right.ProjectsAndCalculate))
            {
                return false;
            }

            foreach (var item in selectedTreeItems)
            {
                var treeItem = item as TreeViewDataItem;
                if (treeItem == null
                    || !(treeItem.DataObject is Project
                    || treeItem.DataObject is Part
                    || treeItem.DataObject is Assembly
                    || treeItem.DataObject is RawMaterial
                    || treeItem.DataObject is Commodity
                    || treeItem.DataObject is ProcessStep))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Performs the logic associated with the Copy command.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void Copy(object parameter)
        {
            var selectedTreeItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedTreeItems == null || selectedTreeItems.Count == 0)
            {
                return;
            }

            var itemsToCopy = selectedTreeItems
                .OfType<TreeViewDataItem>()
                .Select(item => new ClipboardObject() { Entity = item.DataObject, DbSource = item.DataObjectSource });

            ClipboardManager.Instance.Copy(itemsToCopy);
        }

        /// <summary>
        /// Determines whether the Cut command can be execute.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        /// true if the command can be executed, false otherwise.
        /// </returns>
        private bool CanCut(object parameter)
        {
            var selectedTreeItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedTreeItems == null
                || selectedTreeItems.Count == 0)
            {
                return false;
            }

            foreach (var item in selectedTreeItems)
            {
                // Can not move a part or an assembly from master data
                var treeItem = item as TreeViewDataItem;
                if (treeItem == null
                    || treeItem.ReadOnly
                    || !(treeItem.DataObject is ProjectFolder
                    || treeItem.DataObject is Project
                    || (treeItem.DataObject is Assembly && !EntityUtils.IsMasterData(treeItem.DataObject))
                    || (treeItem.DataObject is Part && !EntityUtils.IsMasterData(treeItem.DataObject))
                    || (treeItem.DataObject is RawMaterial && !EntityUtils.IsMasterData(treeItem.DataObject))
                    || (treeItem.DataObject is ProcessStep && !EntityUtils.IsMasterData(treeItem.DataObject))
                    || (treeItem.DataObject is Machine && !EntityUtils.IsMasterData(treeItem.DataObject))
                    || (treeItem.DataObject is Die && !EntityUtils.IsMasterData(treeItem.DataObject))
                    || (treeItem.DataObject is Consumable && !EntityUtils.IsMasterData(treeItem.DataObject))
                    || (treeItem.DataObject is Commodity && !EntityUtils.IsMasterData(treeItem.DataObject))))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Performs the logic associated with the Cut command.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void Cut(object parameter)
        {
            var selectedTreeItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedTreeItems == null
                || selectedTreeItems.Count == 0)
            {
                return;
            }

            var itemsToMove = selectedTreeItems
                .OfType<TreeViewDataItem>()
                .Select(item => new ClipboardObject() { Entity = item.DataObject, DbSource = item.DataObjectSource, DataManager = item.DataObjectContext });

            ClipboardManager.Instance.Cut(itemsToMove);
        }

        /// <summary>
        /// Determines whether the Paste command can be execute.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        /// true if the command can be executed, false otherwise.
        /// </returns>
        private bool CanPaste(object parameter)
        {
            var selectedTreeItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedTreeItems == null
                || selectedTreeItems.Count != 1)
            {
                return false;
            }

            /*
             * Check the selected item (or it's parent) accuracy, and return false 
             * if it is other than FineCalculation.
             * Also returns false if the selected item is a Bookmark or Master Data root item. 
             * [Paste command has no logic in these items]
             */
            var selectedItem = selectedTreeItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null
                || selectedItem.ReadOnly
                || !IsFineCalculationAccuracy(selectedItem)
                || selectedItem is ProjectsBookmarksTreeItem
                || selectedItem is AssembliesBookmarksTreeItem
                || selectedItem is PartsBookmarksTreeItem
                || selectedItem is SimpleProjectsTreeItem)
            {
                return false;
            }

            Type targetType = null;
            if (selectedItem is MyProjectsTreeItem || selectedItem is SubAssembliesTreeItem || selectedItem is SubPartsTreeItem)
            {
                targetType = selectedItem.GetType();
            }
            else if (selectedItem.DataObject != null)
            {
                targetType = selectedItem.DataObject.GetType();
            }
            else if (selectedItem.Parent != null && selectedItem.Parent.DataObject != null)
            {
                targetType = selectedItem.Parent.DataObject.GetType();
            }

            if (targetType != null && !ClipboardManager.Instance.CheckIfEntityAcceptsClipboardObjects(targetType))
            {
                return false;
            }

            if (ClipboardManager.Instance.Operation == ClipboardOperation.Cut)
            {
                object selectedDataObject = selectedItem.DataObject == null && selectedItem.Parent != null
                    ? selectedItem.Parent.DataObject
                    : selectedItem.DataObject;

                var clipboardObjects = ClipboardManager.Instance.PeekClipboardObjects;
                foreach (var clpObject in clipboardObjects)
                {
                    // Released objects can not be moved  
                    var releaseableObj = clpObject as IReleasable;
                    if (releaseableObj != null && releaseableObj.IsReleased)
                    {
                        return false;
                    }

                    // Should not be able to move a folder into itself or into the same parent folder.
                    var clipboardProjectFolder = clpObject as ProjectFolder;
                    if (clipboardProjectFolder != null)
                    {
                        ProjectFolder folder = selectedItem.DataObject as ProjectFolder;
                        if (folder != null)
                        {
                            if (folder.Guid == clipboardProjectFolder.Guid
                                || selectedItem.HasChildForObject(clipboardProjectFolder))
                            {
                                return false;
                            }
                        }
                    }

                    // An item can not be moved into a master data item.
                    // A master data item can not be moved anywhere
                    if (EntityUtils.IsMasterData(selectedDataObject) || EntityUtils.IsMasterData(clpObject))
                    {
                        return false;
                    }

                    // An item can not be move into the same parent (but can be moved into the same parent's sub-tree).
                    var clipboardEntity = clpObject as IIdentifiable;
                    if (clipboardEntity != null)
                    {
                        // Check if the entity from clipboard is already the child of the selected tree item
                        if (selectedItem.HasChildForObject(clipboardEntity))
                        {
                            return false;
                        }

                        IIdentifiable selectedEntity = selectedDataObject as IIdentifiable;
                        if (selectedEntity != null)
                        {
                            // Verify if the item from the clipboard is the same as the item recipient.
                            if (selectedEntity.Guid == clipboardEntity.Guid)
                            {
                                return false;
                            }

                            // If the selected tree item is an assembly then is needed to search on its subassemblies and subparts list 
                            Assembly assembly = selectedEntity as Assembly;
                            if (assembly != null)
                            {
                                // Verify if the target assembly is not a child of the assembly from clipboard to not create loops
                                var assyToPaste = clpObject as Assembly;
                                if (assyToPaste != null && ClipboardManager.Instance.CheckAssemblyIfParentOf(assyToPaste, assembly))
                                {
                                    return false;
                                }
                                else
                                {
                                    var assy = assembly.Subassemblies.FirstOrDefault(a => a.Guid == clipboardEntity.Guid);
                                    var part = assembly.Parts.FirstOrDefault(p => p.Guid == clipboardEntity.Guid);
                                    if (assy != null || part != null)
                                    {
                                        return false;
                                    }
                                }
                            }

                            // If the selected tree item is a part then is needed to search on its materials list and raw part property.
                            var selectedPart = selectedEntity as Part;
                            if (selectedPart != null)
                            {
                                var rawMaterial = selectedPart.RawMaterials.FirstOrDefault(m => m.Guid == clipboardEntity.Guid);
                                var commodity = selectedPart.Commodities.FirstOrDefault(c => c.Guid == clipboardEntity.Guid);
                                if ((selectedPart.RawPart != null
                                    && selectedPart.RawPart.Guid == clipboardEntity.Guid)
                                    || rawMaterial != null
                                    || commodity != null)
                                {
                                    return false;
                                }
                            }

                            var selectedProcessStep = selectedEntity as ProcessStep;
                            if (selectedProcessStep != null)
                            {
                                // Verify if the target process step does not contain already the item to paste.
                                var commodity = selectedProcessStep.Commodities.FirstOrDefault(c => c.Guid == clipboardEntity.Guid);
                                if (commodity != null)
                                {
                                    return false;
                                }

                                var machine = selectedProcessStep.Machines.FirstOrDefault(m => m.Guid == clipboardEntity.Guid);
                                if (machine != null)
                                {
                                    return false;
                                }

                                var die = selectedProcessStep.Dies.FirstOrDefault(d => d.Guid == clipboardEntity.Guid);
                                if (die != null)
                                {
                                    return false;
                                }

                                var consumable = selectedProcessStep.Consumables.FirstOrDefault(c => c.Guid == clipboardEntity.Guid);
                                if (consumable != null)
                                {
                                    return false;
                                }
                            }
                        }
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Performs the logic associated with the Paste command.
        /// </summary>
        /// <param name="parameter">The command parameter.</param>
        private void Paste(object parameter)
        {
            var selectedTreeItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedTreeItems == null
                || selectedTreeItems.Count != 1)
            {
                return;
            }

            // The selected tree item is the target for the paste operation
            var selectedItem = selectedTreeItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null || selectedItem.ReadOnly)
            {
                return;
            }

            object pasteRecipient;
            var dataContext = selectedItem.DataObjectContext;
            if (selectedItem is MyProjectsTreeItem)
            {
                pasteRecipient = selectedItem;
            }
            else if (selectedItem is SubAssembliesTreeItem
                || selectedItem is SubPartsTreeItem
                || selectedItem is MaterialsTreeItem)
            {
                // The recipient of the paste operation if the selected item is SubAssembliesTreeItem, SubPartsTreeItem or MaterialsTreeItem
                // Is the underlying data object of its parent item.
                pasteRecipient = selectedItem.Parent.DataObject;
            }
            else
            {
                pasteRecipient = selectedItem.DataObject;
            }

            var operation = ClipboardManager.Instance.Operation;
            var clipboardObjects = ClipboardManager.Instance.PeekClipboardObjects.ToList();
            var displayedMessage = string.Empty;

            var updateToRecipientCalculationVersion = this.importService.CanUpdateToRecipientCalculationVersion(pasteRecipient, clipboardObjects);

            // If the operation is Cut, the message displayed is "Moving object(s)..."; else, the operation is Copy, and the message displayed is "Copying object(s)..."
            if (clipboardObjects.Count == 1)
            {
                var objName = EntityUtils.GetEntityName(clipboardObjects.First());
                if (!string.IsNullOrWhiteSpace(objName))
                {
                    objName = "\"" + objName + "\"";
                }

                displayedMessage = string.Format(operation == ClipboardOperation.Cut ? LocalizedResources.General_MovingItem : LocalizedResources.General_CopyingItem, objName);
            }
            else
            {
                displayedMessage = operation == ClipboardOperation.Cut ? LocalizedResources.General_MovingObjects : LocalizedResources.General_CopyingObjects;
            }

            var pastedObjects = new List<PastedData>();
            Action<PleaseWaitService.WorkParams> work = (workParams) =>
            {
                pastedObjects = ClipboardManager.Instance.Paste(pasteRecipient, dataContext, updateToRecipientCalculationVersion, false).ToList();
            };

            Action<PleaseWaitService.WorkParams> workCompleted = (workparams) =>
            {
                if (workparams.Error != null)
                {
                    log.Error("An error occurred while executing the paste operation.", workparams.Error);
                    this.windowService.MessageDialogService.Show(workparams.Error);
                }
                else
                {
                    // Display errors appeared during the paste process.
                    string errorMessage = ClipboardManager.Instance.GetErrorMessageToDisplay(pastedObjects);
                    if (!string.IsNullOrWhiteSpace(errorMessage))
                    {
                        this.windowService.MessageDialogService.Show(errorMessage, MessageDialogType.Error);
                    }

                    var messages = new Collection<EntityChangedMessage>();
                    foreach (var pastedObj in pastedObjects.Where(o => o.Error == null))
                    {
                        if (pastedObj != null)
                        {
                            var selectedPastedObject = pastedObj.Entity;
                            var stepRecipient = pasteRecipient as ProcessStep;
                            if (stepRecipient != null)
                            {
                                // Send a message notifying a sub-entity of process step was created.
                                var stepMsg = new ProcessStepChangedMessage(this, stepRecipient)
                                {
                                    StepID = stepRecipient.Guid,
                                    SubEntity = selectedPastedObject,
                                    ChangeType = operation == ClipboardOperation.Cut
                                        ? ProcessStepChangeType.SubEntityMoved
                                        : ProcessStepChangeType.SubEntityAdded
                                };

                                this.messenger.Send(stepMsg, GlobalMessengerTokens.MainViewTargetToken);
                            }

                            // Notify the projects tree that an object has changed.
                            // Check if the object is part of the master data node or part of my projects node.
                            var msg =
                                new EntityChangedMessage(
                                    EntityUtils.IsMasterData(selectedPastedObject)
                                        ? Notification.MasterDataEntityChanged
                                        : Notification.MyProjectsEntityChanged);

                            msg.Entity = selectedPastedObject;
                            msg.Parent = pasteRecipient;
                            msg.ChangeType = operation == ClipboardOperation.Cut
                                                 ? EntityChangeType.EntityMoved
                                                 : EntityChangeType.EntityCreated;

                            messages.Add(msg);
                        }
                    }

                    if (messages.Count == 1)
                    {
                        this.messenger.Send(messages.FirstOrDefault());
                    }
                    else if (messages.Any())
                    {
                        var message = new EntitiesChangedMessage(messages);
                        this.messenger.Send(message);
                    }
                }
            };

            this.pleaseWaitService.Show(displayedMessage, work, workCompleted);
        }

        /// <summary>
        /// Determines whether the Delete command can be execute.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        /// true if the command can be executed, false otherwise.
        /// </returns>
        private bool CanDelete(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null || selectedItems.Count == 0)
            {
                return false;
            }

            foreach (var item in selectedItems)
            {
                var treeItem = item as TreeViewDataItem;
                if (treeItem == null
                    || treeItem.ReadOnly
                    || !(treeItem is ProjectFolderTreeItem
                    || treeItem is ProjectTreeItem
                    || treeItem is PartTreeItem
                    || treeItem is AssemblyTreeItem
                    || treeItem is RawMaterialTreeItem
                    || treeItem is CommodityTreeItem
                    || treeItem is ProcessStepTreeItem))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Performs the logic associated with the Delete command.
        /// </summary>
        /// <param name="parameter">The command parameter.</param>
        private void Delete(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null
                || selectedItems.Count == 0
                || selectedItems.Any(item => !(item is TreeViewDataItem) || ((TreeViewDataItem)item).ReadOnly))
            {
                return;
            }

            // If the Shift modifier is not pressed then the deletion should be made to the trash bin
            // else should be deleted permanently
            var deletePermanently = Keyboard.Modifiers == ModifierKeys.Shift;

            // If deleting items to trash bin and all items are not trash able or all items are master data the deletion is permanently
            if (!deletePermanently)
            {
                deletePermanently = selectedItems.All(item =>
                    !(((TreeViewDataItem)item).DataObject is ITrashable)
                    || EntityUtils.IsMasterData(((TreeViewDataItem)item).DataObject));
            }

            var message = string.Empty;
            if (deletePermanently)
            {
                message = LocalizedResources.Question_PermanentlyDeleteSelectedItems;
            }
            else
            {
                message = LocalizedResources.Question_DeleteSelectedObjects;
            }

            var result = this.windowService.MessageDialogService.Show(message, MessageDialogType.YesNo);
            if (result != MessageDialogResult.Yes)
            {
                return;
            }

            Action<PleaseWaitService.WorkParams> work = (workParams) =>
            {
                var objectIndex = 1;
                foreach (var item in selectedItems)
                {
                    var waitMessage = string.Empty;
                    if (selectedItems.Count == 1)
                    {
                        waitMessage = LocalizedResources.General_WaitItemDelete;
                    }
                    else
                    {
                        waitMessage = string.Format(LocalizedResources.General_WaitDeleteObjects, objectIndex, selectedItems.Count);
                    }

                    this.pleaseWaitService.Message = waitMessage;
                    objectIndex++;

                    var treeItem = item as TreeViewDataItem;
                    var dataManager = treeItem.DataObjectContext;

                    // Delete the object.
                    TrashManager trashManager = new TrashManager(dataManager);
                    if (deletePermanently || EntityUtils.IsMasterData(treeItem.DataObject) || !(treeItem.DataObject is ITrashable))
                    {
                        trashManager.DeleteByStoreProcedure(treeItem.DataObject);
                    }
                    else
                    {
                        trashManager.DeleteToTrashBin(treeItem.DataObject);
                        dataManager.SaveChanges();
                    }
                }
            };

            Action<PleaseWaitService.WorkParams> workCompleted = (workParams) =>
            {
                if (workParams.Error != null)
                {
                    this.windowService.MessageDialogService.Show(workParams.Error);
                }
                else
                {
                    foreach (var item in selectedItems)
                    {
                        var treeItem = item as TreeViewDataItem;
                        TreeViewDataItem parentItem = treeItem.Parent;
                        if (parentItem is SubPartsTreeItem || parentItem is SubAssembliesTreeItem || parentItem is MaterialsTreeItem)
                        {
                            parentItem = parentItem.Parent;
                        }

                        // Notify the projects tree that an object has changed.
                        // Check if the object is part of the master data node or part of my projects node.
                        EntityChangedMessage msg = null;
                        if (EntityUtils.IsMasterData(treeItem.DataObject))
                        {
                            msg = new EntityChangedMessage(Notification.MasterDataEntityChanged);
                        }
                        else
                        {
                            msg = new EntityChangedMessage(Notification.MyProjectsEntityChanged);
                        }

                        msg.Entity = treeItem.DataObject;
                        msg.Parent = parentItem != null ? parentItem.DataObject : null;
                        msg.ChangeType = EntityChangeType.EntityDeleted;
                        this.messenger.Send(msg);
                    }
                }
            };

            this.pleaseWaitService.Show(string.Empty, work, workCompleted);
        }

        /// <summary>
        /// Determines whether the Rename command can be executed.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        /// true if the command can be executed; false otherwise.
        /// </returns>
        private bool CanRename(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null || selectedItems.Count != 1)
            {
                return false;
            }

            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;

            return selectedItem != null
                && !selectedItem.ReadOnly
                && selectedItem.DataObject is ProjectFolder;
        }

        /// <summary>
        /// Executes the logic associated with the Rename command.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void Rename(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null || selectedItems.Count != 1)
            {
                return;
            }

            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem.ReadOnly)
            {
                return;
            }

            ProjectFolder folder = selectedItem.DataObject as ProjectFolder;
            if (folder == null)
            {
                return;
            }

            ProjectFolderViewModel viewModel = this.compositionContainer.GetExportedValue<ProjectFolderViewModel>();
            viewModel.EditMode = ViewModelEditMode.Edit;
            viewModel.Title = LocalizedResources.General_RenameFolder;
            viewModel.DataSourceManager = selectedItem.DataObjectContext;
            viewModel.Model = folder;
            this.windowService.ShowViewInDialog(viewModel);
        }

        /// <summary>
        /// Determines whether the entity can be opened in read-only mode or not.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        ///   <c>true</c> if this parameter [can be opened in read-only mode]; otherwise, <c>false</c>.
        /// </returns>
        private bool CanOpenReadonly(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null || selectedItems.Count != 1)
            {
                return false;
            }

            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null || selectedItem.ReadOnly)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Open the entity in read-only mode.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void OpenReadonly(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null || selectedItems.Count != 1)
            {
                return;
            }

            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null)
            {
                return;
            }

            var parentProjectItem = this.FindParentProjectItem(selectedItem);
            var parentProject = parentProjectItem.DataObject as Project ??
                                selectedItem.DataObjectContext.ProjectRepository.GetParentProject(
                                    selectedItem.DataObject);

            var project = selectedItem.DataObject as Project;
            if (project != null)
            {
                var projectVm = this.compositionContainer.GetExportedValue<ProjectViewModel>();
                projectVm.EditMode = ViewModelEditMode.Edit;
                projectVm.IsReadOnly = true;
                projectVm.IsInViewerMode = false;
                projectVm.DataSourceManager = selectedItem.DataObjectContext;
                projectVm.Model = project;

                this.RequiredReadonlyDocumentContentChanging(projectVm);
                return;
            }

            var assembly = selectedItem.DataObject as Assembly;
            if (assembly != null && parentProject != null)
            {
                var assyVm = this.compositionContainer.GetExportedValue<AssemblyViewModel>();
                assyVm.IsReadOnly = true;
                assyVm.IsInViewerMode = false;
                assyVm.EditMode = ViewModelEditMode.Edit;
                assyVm.ParentProject = parentProject;
                assyVm.DataSourceManager = selectedItem.DataObjectContext;
                assyVm.Model = assembly;

                this.RequiredReadonlyDocumentContentChanging(assyVm);
                return;
            }

            var part = selectedItem.DataObject as Part;
            if (part != null && parentProject != null)
            {
                var partVm = this.compositionContainer.GetExportedValue<PartViewModel>();
                partVm.IsReadOnly = true;
                partVm.IsInViewerMode = false;
                partVm.EditMode = ViewModelEditMode.Edit;
                partVm.ParentProject = parentProject;
                partVm.DataSourceManager = selectedItem.DataObjectContext;
                partVm.Model = selectedItem.DataObject as RawPart ?? part;

                this.RequiredReadonlyDocumentContentChanging(partVm);
                return;
            }

            var process = selectedItem.DataObject as Process;
            if (process != null && parentProject != null)
            {
                var processVm = this.compositionContainer.GetExportedValue<ProcessViewModel>();
                processVm.ParentProject = parentProject;
                processVm.Parent = selectedItem.Parent.DataObject;
                processVm.DataSourceManager = selectedItem.DataObjectContext;
                processVm.IsInViewerMode = false;
                processVm.IsReadOnly = true;
                processVm.EditMode = ViewModelEditMode.Edit;
                processVm.Model = process;

                this.RequiredReadonlyDocumentContentChanging(processVm);
                return;
            }

            var step = selectedItem.DataObject as ProcessStep;
            if (step != null)
            {
                var processStepVm = this.compositionContainer.GetExportedValue<ProcessStepEditorViewModel>();
                processStepVm.DataSourceManager = selectedItem.Parent.DataObjectContext;
                processStepVm.IsInViewerMode = false;
                processStepVm.IsReadOnly = true;
                processStepVm.ParentProject = parentProject;
                processStepVm.ProcessParent = selectedItem.Parent.Parent.DataObject;
                processStepVm.Process = selectedItem.Parent.DataObject as Process;
                processStepVm.Model = step;

                this.RequiredReadonlyDocumentContentChanging(processStepVm);
                return;
            }

            var rawMaterial = selectedItem.DataObject as RawMaterial;
            if (rawMaterial != null)
            {
                string calculationVersion = null;
                if (selectedItem.Parent != null && selectedItem.Parent.Parent != null && selectedItem.Parent.Parent.DataObject is Part)
                {
                    calculationVersion = ((Part)selectedItem.Parent.Parent.DataObject).CalculationVariant;
                }

                var rawMaterialVm = this.compositionContainer.GetExportedValue<RawMaterialViewModel>();
                rawMaterialVm.EditMode = ViewModelEditMode.Edit;
                rawMaterialVm.DataSourceManager = selectedItem.DataObjectContext;
                rawMaterialVm.IsReadOnly = true;
                rawMaterialVm.ParentProject = parentProject;
                rawMaterialVm.IsInViewerMode = false;
                rawMaterialVm.CostCalculationVersion = calculationVersion;
                rawMaterialVm.Model = rawMaterial;

                this.RequiredReadonlyDocumentContentChanging(rawMaterialVm);
                return;
            }

            var commodity = selectedItem.DataObject as Commodity;
            if (commodity != null)
            {
                // Find the commodity's parent Assembly/Part.
                var currentParentItem = selectedItem.Parent;
                object parentEntity = null;
                while (currentParentItem != null)
                {
                    if (currentParentItem.DataObject is Assembly
                        || currentParentItem.DataObject is Part)
                    {
                        parentEntity = currentParentItem.DataObject;
                        break;
                    }

                    currentParentItem = currentParentItem.Parent;
                }

                var commodityVm = this.compositionContainer.GetExportedValue<CommodityViewModel>();
                commodityVm.IsReadOnly = true;
                commodityVm.IsInViewerMode = false;
                commodityVm.EditMode = ViewModelEditMode.Edit;
                commodityVm.ParentProject = parentProject;
                commodityVm.CommodityParent = parentEntity;
                commodityVm.DataSourceManager = selectedItem.DataObjectContext;
                commodityVm.Model = commodity;

                this.RequiredReadonlyDocumentContentChanging(commodityVm);
                return;
            }

            var subassembliesItem = selectedItem as SubAssembliesTreeItem;
            if (subassembliesItem != null && subassembliesItem.Parent != null)
            {
                var subassembliesViewModel = this.compositionContainer.GetExportedValue<SubassembliesViewModel>();
                subassembliesViewModel.AssemblyDataContext = selectedItem.DataObjectContext;
                subassembliesViewModel.ParentAssembly = subassembliesItem.Parent.DataObject as Assembly;
                subassembliesViewModel.IsReadOnly = true;
                subassembliesViewModel.IsInViewerMode = false;

                this.RequiredReadonlyDocumentContentChanging(subassembliesViewModel);
                return;
            }

            var subPartsItem = selectedItem as SubPartsTreeItem;
            if (subPartsItem != null && subPartsItem.Parent != null)
            {
                var partsViewModel = this.compositionContainer.GetExportedValue<PartsViewModel>();
                partsViewModel.PartDataContext = subPartsItem.DataObjectContext;
                partsViewModel.ParentAssembly = subPartsItem.Parent.DataObject as Assembly;
                partsViewModel.IsReadOnly = true;
                partsViewModel.IsInViewerMode = false;

                this.RequiredReadonlyDocumentContentChanging(partsViewModel);
                return;
            }

            var materialsItem = selectedItem as MaterialsTreeItem;
            if (materialsItem != null && materialsItem.Parent != null)
            {
                var materialsVm = this.compositionContainer.GetExportedValue<MaterialsViewModel>();
                materialsVm.PartDataContext = materialsItem.Parent.DataObjectContext;
                materialsVm.ParentPart = materialsItem.Parent.DataObject as Part;
                materialsVm.IsInViewerMode = false;
                materialsVm.IsReadOnly = true;

                this.RequiredReadonlyDocumentContentChanging(materialsVm);
                return;
            }

            var resultDetailsItem = selectedItem as ResultDetailsTreeItem;
            if (resultDetailsItem != null && resultDetailsItem.Parent != null)
            {
                var resultDetailsVM = this.compositionContainer.GetExportedValue<ResultDetailsViewModel>();
                resultDetailsVM.Entity = resultDetailsItem.Parent.DataObject;
                resultDetailsVM.ParentProject = parentProject;
                resultDetailsVM.DataSourceManager = resultDetailsItem.DataObjectContext;
                resultDetailsVM.IsReadOnly = true;
                resultDetailsVM.IsInViewerMode = false;

                this.RequiredReadonlyDocumentContentChanging(resultDetailsVM);
                return;
            }
        }

        /// <summary>
        /// Opens the report generator.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        ///   <c>true</c> if this instance [can open report generator] the specified parameter; otherwise, <c>false</c>.
        /// </returns>
        private bool CanOpenReportGenerator(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null
                || selectedItems.Count != 1)
            {
                return false;
            }

            // Note: Right now only the assemblies are supported.            
            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem != null)
            {
                IMasterDataObject masterObj = selectedItem.DataObject as IMasterDataObject;
                if (masterObj != null && masterObj.IsMasterData)
                {
                    return false;
                }

                if (selectedItem.DataObject is Assembly)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Determines whether the report generator feature is available at this time.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void OpenReportGenerator(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null
                || selectedItems.Count != 1)
            {
                return;
            }

            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null)
            {
                return;
            }

            object entity = null;
            Project parentProject = null;
            DbIdentifier databaseId = DbIdentifier.NotSet;

            // The selected item is an Assembly.
            if (selectedItem.DataObject is Assembly)
            {
                entity = selectedItem.DataObject;
                databaseId = selectedItem.DataObjectSource;
            }
            else if (selectedItem.Parent != null
                && selectedItem.Parent.DataObject is Assembly)
            {
                // The selected item may be a child of an Assembly so check its parent
                entity = selectedItem.Parent.DataObject;
                databaseId = selectedItem.Parent.DataObjectSource;
            }

            if (entity is Assembly)
            {
                // Find the parent project of the assembly. If it is not found get the parent project from the database.
                // If it is not found set the entity to null so the batch reports window does not open.
                IDataSourceManager dataManager = selectedItem.DataObjectContext;
                var projectItem = this.FindParentProjectItem(selectedItem);
                parentProject = projectItem != null ? projectItem.DataObject as Project : dataManager.ProjectRepository.GetParentProject(entity);
                if (parentProject == null)
                {
                    entity = null;
                }
            }

            if (entity != null)
            {
                var viewModel = this.compositionContainer.GetExportedValue<ReportGeneratorViewModel>();
                viewModel.Entity = entity;
                viewModel.EntityParent = parentProject;
                this.windowService.ShowViewInDialog(viewModel);
            }
        }

        /// <summary>
        /// Determines whether the batch reporting feature is available at this time.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        /// true if the feature is available, false otherwise.
        /// </returns>
        private bool CanOpenBatchReporting(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null
                || selectedItems.Count != 1)
            {
                return false;
            }

            // The Batch Reporting can be opened if a project item, an assembly item or one of the assembly items's direct children is selected.
            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem != null)
            {
                IMasterDataObject masterObj = selectedItem.DataObject as IMasterDataObject;
                if (masterObj != null && masterObj.IsMasterData)
                {
                    return false;
                }

                if (selectedItem.DataObject is Assembly || selectedItem.DataObject is Project)
                {
                    return true;
                }
                else if (selectedItem.Parent != null && selectedItem.Parent.DataObject is Assembly)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Opens the batch reporting.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void OpenBatchReporting(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null
                || selectedItems.Count != 1)
            {
                return;
            }

            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null)
            {
                return;
            }

            object entity = null;
            Project parentProject = null;
            DbIdentifier databaseId = DbIdentifier.NotSet;

            // The selected item is an Assembly or Project.
            if (selectedItem.DataObject is Assembly
                || selectedItem.DataObject is Project)
            {
                entity = selectedItem.DataObject;
                databaseId = selectedItem.DataObjectSource;
            }
            else if (selectedItem.Parent != null
                && selectedItem.Parent.DataObject is Assembly)
            {
                // The selected item may be a child of an Assembly so check its parent
                entity = selectedItem.Parent.DataObject;
                databaseId = selectedItem.Parent.DataObjectSource;
            }

            if (entity is Assembly)
            {
                // Find the parent project of the assembly. If it is not found get the parent project from the database.
                // If it is not found set the entity to null so the batch reports window does not open.
                IDataSourceManager dataContext = selectedItem.DataObjectContext;
                var projectItem = this.FindParentProjectItem(selectedItem);
                parentProject = projectItem != null ? projectItem.DataObject as Project : dataContext.ProjectRepository.GetParentProject(entity);
                if (parentProject == null)
                {
                    entity = null;
                }
            }

            if (entity != null)
            {
                var viewModel = compositionContainer.GetExportedValue<BatchReportingViewModel>();
                viewModel.Entity = entity;
                viewModel.EntitySourceDb = databaseId;
                viewModel.EntityParent = parentProject;
                this.windowService.ShowViewInDialog(viewModel);
            }
        }

        /// <summary>
        /// Determines whether the selected item can have mass data update.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        /// <c>true</c> if it can; otherwise, <c>false</c>.
        /// </returns>
        private bool CanOpenMassDataUpdate(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null
                || selectedItems.Count != 1)
            {
                return false;
            }

            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null || selectedItem.ReadOnly)
            {
                return false;
            }

            object dataObject = selectedItem.DataObject;
            if (dataObject is Project
                || dataObject is Assembly
                || dataObject is Part
                || dataObject is Process
                || dataObject is Machine
                || dataObject is RawMaterial)
            {
                return true;
            }
            else
            {
                var processStep = dataObject as ProcessStep;
                if (processStep != null && this.IsProcessStepLoadedAndAccuracyCalculated())
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Contains the logic associated with the Mass Data Update command.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void OpenMassDataUpdate(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null
                || selectedItems.Count != 1)
            {
                return;
            }

            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem.ReadOnly)
            {
                return;
            }

            MassDataUpdateViewModel viewModel = this.compositionContainer.GetExportedValue<MassDataUpdateViewModel>();
            viewModel.CurrentObject = selectedItem.DataObject;
            viewModel.DataContext = selectedItem.DataObjectContext;
            this.windowService.ShowViewInDialog(viewModel);
        }

        /// <summary>
        /// Determines whether the Import command can execute.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        /// true if it can execute; otherwise, <c>false</c>.
        /// </returns>
        private bool CanImport(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null
                || selectedItems.Count != 1
                || !SecurityManager.Instance.CurrentUserHasRight(Right.ProjectsAndCalculate))
            {
                return false;
            }

            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null || selectedItem.ReadOnly)
            {
                return false;
            }

            if (!this.IsFineCalculationAccuracy(selectedItem))
            {
                return false;
            }

            Type entityType = null;
            if (selectedItem is MyProjectsTreeItem)
            {
                entityType = typeof(MyProjectsTreeItem);
            }
            else if ((selectedItem is MaterialsTreeItem || selectedItem is SubAssembliesTreeItem || selectedItem is SubPartsTreeItem)
                && selectedItem.Parent != null
                && selectedItem.Parent.DataObject != null)
            {
                entityType = selectedItem.Parent.DataObject.GetType();
            }
            else if (selectedItem.DataObject != null)
            {
                entityType = selectedItem.DataObject.GetType();
            }

            if (entityType == null)
            {
                return false;
            }

            // Get the list of types to import from the command's parameter.
            List<Type> typesToImport = new List<Type>();
            var parameterAsType = parameter as Type;
            if (parameterAsType != null)
            {
                typesToImport.Add(parameterAsType);
            }
            else
            {
                var parameterAsEnumerable = parameter as IEnumerable<Type>;
                if (parameterAsEnumerable != null)
                {
                    typesToImport.AddRange(parameterAsEnumerable);
                }
            }

            foreach (var typeToCheck in typesToImport)
            {
                if (!ClipboardManager.Instance.CheckIfEntityAcceptsObject(entityType, typeToCheck))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Contains the logic associated with the Import command.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void Import(object parameter)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException("parameter", "The parameter for Import was null.");
            }

            // Get the types of the entities to import.
            List<Type> types = new List<Type>();
            var parameterAsType = parameter as Type;
            if (parameterAsType != null)
            {
                types.Add(parameterAsType);
            }
            else
            {
                var parameterAsEnumerable = parameter as IEnumerable<Type>;
                if (parameterAsEnumerable != null)
                {
                    types.AddRange(parameterAsEnumerable);
                }
            }

            if (types.Count == 0)
            {
                throw new ArgumentException("The parameter for Import must be an instance of the Type class or a list of Type objects.", "parameter");
            }

            // The import will be performed in the currently selected projects tree item.
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null
                || selectedItems.Count != 1)
            {
                return;
            }

            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem.ReadOnly)
            {
                return;
            }

            // If the selected projects tree item is Materials, SubAssemblies or SubParts, the import will be performed in its parent Assembly/Part item.
            object importParent = null;
            IDataSourceManager dataContext = null;
            if (selectedItem is MaterialsTreeItem
                || selectedItem is SubAssembliesTreeItem
                || selectedItem is SubPartsTreeItem)
            {
                importParent = selectedItem.Parent.DataObject;
                dataContext = selectedItem.Parent.DataObjectContext;
            }
            else if (selectedItem is MyProjectsTreeItem
                || selectedItem is ProjectFolderTreeItem
                || selectedItem is ProjectTreeItem)
            {
                // Create a new data context to import the entity
                // After the import finishes then a reference to the imported entity will be added to its parent data context
                dataContext = DataAccessFactory.CreateDataSourceManager(selectedItem.DataObjectSource);
                if (selectedItem.DataObject != null)
                {
                    if (selectedItem is ProjectTreeItem)
                    {
                        importParent = dataContext.ProjectRepository.GetProjectIncludingTopLevelChildren((selectedItem.DataObject as Project).Guid);
                    }
                    else if (selectedItem is ProjectFolderTreeItem)
                    {
                        importParent = dataContext.ProjectFolderRepository.GetFolderWithParent((selectedItem.DataObject as ProjectFolder).Guid);
                    }
                }
                else
                {
                    importParent = null;
                }
            }
            else
            {
                importParent = selectedItem.DataObject;
                dataContext = selectedItem.DataObjectContext;
            }

            bool saveChanges = true;
            if (importParent is ProcessStep)
            {
                saveChanges = false;
            }

            this.ImportEntities(types, importParent, dataContext, saveChanges);
        }

        /// <summary>
        /// Determines whether the Export command can execute.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        /// true if it can execute; otherwise, <c>false</c>.
        /// </returns>
        private bool CanExport(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null || selectedItems.Count == 0)
            {
                return false;
            }

            foreach (var item in selectedItems)
            {
                var treeItem = item as TreeViewDataItem;
                if (treeItem == null
                    || treeItem.DataObject == null
                    || treeItem.DataObject is Process
                    || treeItem.DataObject is ProcessStep)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Contains the logic associated with the Export command.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void Export(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null || selectedItems.Count == 0)
            {
                return;
            }

            foreach (var item in selectedItems)
            {
                var treeItem = item as TreeViewDataItem;
                if (treeItem == null || treeItem.DataObjectContext == null)
                {
                    return;
                }
            }

            string filePath = null;
            if (selectedItems.Count == 1)
            {
                var treeItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
                string defaultFileName = UIUtils.GetExportObjectFileName(treeItem.DataObject);

                var fileDialogService = this.compositionContainer.GetExportedValue<IFileDialogService>();
                fileDialogService.FileName = defaultFileName;
                fileDialogService.Filter = UIUtils.GetDialogFilterForExportImport(treeItem.DataObject.GetType());

                bool? result = fileDialogService.ShowSaveFileDialog();
                if (result == true)
                {
                    filePath = fileDialogService.FileName;
                }
            }
            else
            {
                var folderBrowseService = this.compositionContainer.GetExportedValue<IFolderBrowserService>();
                folderBrowseService.Description = LocalizedResources.General_ExportFolderBrowserDescription;
                var result = folderBrowseService.ShowFolderBrowserDialog();
                if (result == true)
                {
                    filePath = folderBrowseService.SelectedPath;
                }
            }

            if (string.IsNullOrEmpty(filePath))
            {
                return;
            }

            Action<PleaseWaitService.WorkParams> work = (workParams) =>
            {
                var objectIndex = 1;
                foreach (var item in selectedItems)
                {
                    var pathToExport = filePath;
                    var treeItem = item as TreeViewDataItem;
                    var waitMessage = string.Empty;
                    if (selectedItems.Count == 1)
                    {
                        waitMessage = UIUtils.GetExportWaitMessage(treeItem.DataObject);
                    }
                    else
                    {
                        waitMessage = string.Format(LocalizedResources.General_WaitExportObjects, objectIndex, selectedItems.Count);
                    }

                    this.pleaseWaitService.Message = waitMessage;
                    objectIndex++;

                    var objectToExport = treeItem.DataObject;
                    var dataContext = treeItem.DataObjectContext;

                    // Projects and Project Folders must be loaded before exporting them because they are not fully loaded,
                    // Assemblies and Parts need to be fully loaded because there is a chance that they are not fully loaded, for example when using multi select.
                    // Also, they are loaded using a new IDataSourceManager instance which can be disposed after this operation because full projects and project folders are not used for anything and they just occupy memory.
                    var projectFolder = objectToExport as ProjectFolder;
                    if (projectFolder != null)
                    {
                        dataContext = DataAccessFactory.CreateDataSourceManager(treeItem.DataObjectSource);
                        objectToExport = dataContext.ProjectFolderRepository.GetProjectFolderFull(projectFolder.Guid);
                        if (objectToExport == null)
                        {
                            throw new UIException(ErrorCodes.ItemNotFound);
                        }
                    }

                    var project = objectToExport as Project;
                    if (project != null)
                    {
                        dataContext = DataAccessFactory.CreateDataSourceManager(treeItem.DataObjectSource);
                        objectToExport = dataContext.ProjectRepository.GetProjectFull(project.Guid);
                        if (objectToExport == null)
                        {
                            throw new UIException(ErrorCodes.ItemNotFound);
                        }
                    }

                    var assembly = objectToExport as Assembly;
                    if (assembly != null)
                    {
                        dataContext = DataAccessFactory.CreateDataSourceManager(treeItem.DataObjectSource);
                        objectToExport = dataContext.AssemblyRepository.GetAssemblyFull(assembly.Guid);
                        if (objectToExport == null)
                        {
                            throw new UIException(ErrorCodes.ItemNotFound);
                        }
                    }

                    var part = objectToExport as Part;
                    if (part != null)
                    {
                        dataContext = DataAccessFactory.CreateDataSourceManager(treeItem.DataObjectSource);
                        objectToExport = dataContext.PartRepository.GetPartFull(part.Guid);
                        if (objectToExport == null)
                        {
                            throw new UIException(ErrorCodes.ItemNotFound);
                        }
                    }

                    // If more then one object to export create for each object the path where it will be exported
                    // Must check for file name collisions before export
                    if (selectedItems.Count > 1)
                    {
                        pathToExport = Path.Combine(pathToExport, UIUtils.GetExportObjectFileName(objectToExport));
                        pathToExport = PathUtils.FixFilePathForCollisions(pathToExport);
                    }

                    ExportManager.Instance.Export(objectToExport, dataContext, pathToExport, this.unitsService.Currencies, this.unitsService.BaseCurrency);
                }
            };

            Action<PleaseWaitService.WorkParams> workCompleted = (workParams) =>
            {
                if (workParams.Error != null)
                {
                    this.windowService.MessageDialogService.Show(workParams.Error);
                }
                else
                {
                    this.windowService.MessageDialogService.Show(LocalizedResources.ExportImport_ExportSuccess, MessageDialogType.Info);
                }
            };

            this.pleaseWaitService.Show(string.Empty, work, workCompleted);
        }

        /// <summary>
        /// Determines whether the Release Project command can be executed.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        /// true if the command can be executed; otherwise, false.
        /// </returns>
        private bool CanReleaseProject(object parameter)
        {
            if (!ZPKTool.Business.SecurityManager.Instance.CurrentUserHasRight(Right.AdminTasks)
                    && !ZPKTool.Business.SecurityManager.Instance.CurrentUserHasRight(Right.ReleaseProject))
            {
                return false;
            }

            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null
                || selectedItems.Count != 1)
            {
                return false;
            }

            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null)
            {
                return false;
            }

            Project project = selectedItem.DataObject as Project;
            if (project == null || project.IsReleased)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Contains the logic associated with the Release Project command.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void ReleaseProject(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null
                || selectedItems.Count != 1)
            {
                return;
            }

            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null)
            {
                return;
            }

            Project project = selectedItem.DataObject as Project;

            if (project.Status == (short)ProjectStatus.Working)
            {
                this.windowService.MessageDialogService.Show(LocalizedResources.Error_CantReleaseWorkingProject, MessageDialogType.Error);
                return;
            }

            var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_ReleaseProject, MessageDialogType.YesNo);
            if (result != MessageDialogResult.Yes)
            {
                return;
            }

            Action<PleaseWaitService.WorkParams> work = (workParams) =>
            {
                ReleaseManager.Instance.ReleaseProject(project.Guid, selectedItem.DataObjectSource);
            };

            Action<PleaseWaitService.WorkParams> workCompleted = (workParams) =>
            {
                if (workParams.Error != null)
                {
                    this.windowService.MessageDialogService.Show(workParams.Error);
                }
            };

            this.pleaseWaitService.Show(LocalizedResources.General_WaitReleaseProject, work, workCompleted);
        }

        /// <summary>
        /// Determines whether the Un-release Project command can be executed.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        /// true if the command can be executed; otherwise, false.
        /// </returns>
        private bool CanUnreleaseProject(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null
                || selectedItems.Count != 1
                || !ZPKTool.Business.SecurityManager.Instance.CurrentUserHasRight(Right.AdminTasks))
            {
                return false;
            }

            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null)
            {
                return false;
            }

            Project project = selectedItem.DataObject as Project;
            if (project == null || !project.IsReleased)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Contains the logic associated with the Un-release Project command.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void UnreleaseProject(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null
                || selectedItems.Count != 1)
            {
                return;
            }

            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null)
            {
                return;
            }

            Project project = selectedItem.DataObject as Project;

            var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_UnreleaseProject, MessageDialogType.YesNo);
            if (result != MessageDialogResult.Yes)
            {
                return;
            }

            Action<PleaseWaitService.WorkParams> work = (workParams) =>
            {
                ReleaseManager.Instance.UnReleaseProject(project.Guid);
            };

            Action<PleaseWaitService.WorkParams> workCompleted = (workParams) =>
            {
                if (workParams.Error != null)
                {
                    var zpkException = workParams.Error as ZPKException;
                    if (zpkException != null &&
                       (zpkException.ErrorCode == DatabaseErrorCode.Deadlock
                        || zpkException.ErrorCode == DatabaseErrorCode.OptimisticConcurrency))
                    {
                        var retry = this.windowService.MessageDialogService.Show(LocalizedResources.General_DeadlockYesNoQuestion, MessageDialogType.YesNo);
                        if (retry == MessageDialogResult.Yes)
                        {
                            work(null);
                        }
                    }
                    else
                    {
                        throw workParams.Error;
                    }
                }
                else
                {
                    this.windowService.MessageDialogService.Show(LocalizedResources.UnReleaseProject_Success, MessageDialogType.Info);
                }
            };

            this.pleaseWaitService.Show(LocalizedResources.General_WaitUnreleaseProject, work, workCompleted);
        }

        /// <summary>
        /// Determines whether a project folder can be created.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        /// true if a folder can be created at this time; otherwise false.
        /// </returns>
        private bool CanCreateProjectFolder(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null
                || selectedItems.Count != 1
                || !SecurityManager.Instance.CurrentUserHasRight(Right.ProjectsAndCalculate))
            {
                return false;
            }

            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null || selectedItem.ReadOnly)
            {
                return false;
            }

            while (selectedItem != null)
            {
                if (selectedItem is MyProjectsTreeItem)
                {
                    return true;
                }

                selectedItem = selectedItem.Parent;
            }

            return false;
        }

        /// <summary>
        /// Action that creates a project folder.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void CreateProjectFolder(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null || selectedItems.Count != 1)
            {
                return;
            }

            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null || selectedItem.ReadOnly)
            {
                return;
            }

            // Determine the parent folder for the folder that will be created.
            ProjectFolder parent = selectedItem.DataObject as ProjectFolder;
            IDataSourceManager dataManager = selectedItem.DataObjectContext;
            if (parent == null)
            {
                // The selected item may be a project in a sub-folder so look at its parent to see if it is a folder, and if it is it will become the parent
                // for the new folder.
                if (selectedItem.Parent != null && !selectedItem.Parent.ReadOnly)
                {
                    parent = selectedItem.Parent.DataObject as ProjectFolder;
                    dataManager = selectedItem.Parent.DataObjectContext;
                }
            }

            ProjectFolderViewModel viewModel = this.compositionContainer.GetExportedValue<ProjectFolderViewModel>();
            viewModel.Title = LocalizedResources.General_CreateFolder;
            viewModel.DataSourceManager = dataManager;
            viewModel.InitializeForCreation(parent);

            this.windowService.ShowViewInDialog(viewModel);
        }

        /// <summary>
        /// Determines whether a project can be created.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        /// true if a project can be created at this time; otherwise false.
        /// </returns>
        private bool CanCreateProject(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null
                || selectedItems.Count != 1
                || !SecurityManager.Instance.CurrentUserHasRight(Right.ProjectsAndCalculate))
            {
                return false;
            }

            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null || selectedItem.ReadOnly)
            {
                return false;
            }

            while (selectedItem != null)
            {
                if (selectedItem is MyProjectsTreeItem)
                {
                    return true;
                }

                selectedItem = selectedItem.Parent;
            }

            return false;
        }

        /// <summary>
        /// Action that creates a project.
        /// </summary>
        /// <param name="parameter">The command parameter.</param>
        private void CreateProject(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null
                || selectedItems.Count != 1)
            {
                return;
            }

            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null || selectedItem.ReadOnly)
            {
                return;
            }

            // Determine the parent folder for the project that will be created.
            ProjectFolder parent = selectedItem.DataObject as ProjectFolder;
            IDataSourceManager dataManager = selectedItem.DataObjectContext;
            if (parent == null)
            {
                // The selected item may be an item in a folder so look at its parent to see if it is a folder.
                if (selectedItem.Parent != null && !selectedItem.Parent.ReadOnly)
                {
                    parent = selectedItem.Parent.DataObject as ProjectFolder;
                    dataManager = selectedItem.Parent.DataObjectContext;
                }
            }

            // Initialize the Project view-model and display the Project view.
            ProjectViewModel viewModel = this.compositionContainer.GetExportedValue<ProjectViewModel>();
            if (dataManager == null)
            {
                dataManager = DataAccessFactory.CreateDataSourceManager(selectedItem.DataObjectSource);
            }

            viewModel.DataSourceManager = dataManager;
            viewModel.InitializeForProjectCreation(parent);

            this.windowService.ShowViewInDialog(viewModel, "CreateProjectView");
        }

        /// <summary>
        /// Determines whether the Create Assembly command can be executed.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        /// true if the command can be executed; otherwise, false.
        /// </returns>
        private bool CanCreateAssembly(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null
                || selectedItems.Count != 1
                || !SecurityManager.Instance.CurrentUserHasRight(Right.ProjectsAndCalculate))
            {
                return false;
            }

            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null || selectedItem.ReadOnly)
            {
                return false;
            }

            // Check the selected item (or it's parent) accuracy, and return false if it is other than FineCalculation
            if (!this.IsFineCalculationAccuracy(selectedItem))
            {
                return false;
            }

            if (selectedItem.DataObject is Project
                || selectedItem.DataObject is Assembly
                || (selectedItem.Parent != null && selectedItem.Parent.DataObject is Assembly))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Contains the logic associated with the Create Assembly command.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void CreateAssembly(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null
                || selectedItems.Count != 1)
            {
                return;
            }

            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null)
            {
                return;
            }

            object assemblyParent = null;
            IDataSourceManager dataManager = null;

            if (selectedItem.DataObject is Project
                || selectedItem.DataObject is Assembly)
            {
                assemblyParent = selectedItem.DataObject;
                dataManager = selectedItem.DataObjectContext;
            }
            else if (selectedItem.Parent != null && selectedItem.Parent.DataObject is Assembly)
            {
                assemblyParent = selectedItem.Parent.DataObject;
                dataManager = selectedItem.Parent.DataObjectContext;
            }

            if (assemblyParent != null)
            {
                AssemblyViewModel assyVM = this.compositionContainer.GetExportedValue<AssemblyViewModel>();
                assyVM.DataSourceManager = dataManager;
                assyVM.InitializeForAssemblyCreation(false, assemblyParent);

                this.windowService.ShowViewInDialog(assyVM, "CreateAssemblyViewTemplate");
            }
            else
            {
                log.Error("Failed to determine the parent for creating an assembly.");
            }
        }

        /// <summary>
        /// Determines whether the Create Part command can be executed.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        /// true if the command can be executed; otherwise, false.
        /// </returns>
        private bool CanCreatePart(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null
                || selectedItems.Count != 1
                || !SecurityManager.Instance.CurrentUserHasRight(Right.ProjectsAndCalculate))
            {
                return false;
            }

            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null || selectedItem.ReadOnly)
            {
                return false;
            }

            // Check the selected item (or it's parent) accuracy, and return false if it is other than FineCalculation
            if (!this.IsFineCalculationAccuracy(selectedItem))
            {
                return false;
            }

            if (selectedItem.DataObject is Project
                || selectedItem.DataObject is Assembly
                || (selectedItem.Parent != null && selectedItem.Parent.DataObject is Assembly))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// The logic associated with the Create Part command.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void CreatePart(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null
                || selectedItems.Count != 1)
            {
                return;
            }

            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null)
            {
                return;
            }

            object partParent = null;
            IDataSourceManager dataManager = null;

            if (selectedItem.DataObject is Project
                || selectedItem.DataObject is Assembly)
            {
                partParent = selectedItem.DataObject;
                dataManager = selectedItem.DataObjectContext;
            }
            else if (selectedItem.Parent != null && selectedItem.Parent.DataObject is Assembly)
            {
                partParent = selectedItem.Parent.DataObject;
                dataManager = selectedItem.Parent.DataObjectContext;
            }

            if (partParent != null)
            {
                PartViewModel partVM = this.compositionContainer.GetExportedValue<PartViewModel>();
                partVM.DataSourceManager = dataManager;
                partVM.InitializeForCreation(false, partParent);
                this.windowService.ShowViewInDialog(partVM, "PartWindowViewTemplate");
            }
            else
            {
                log.Error("Failed to determine the parent for creating a part.");
            }
        }

        /// <summary>
        /// Determines whether the Create Raw Material command can be executed.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        /// true if the command can be executed; otherwise, false.
        /// </returns>
        private bool CanCreateRawMaterial(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null
                || selectedItems.Count != 1
                || !SecurityManager.Instance.CurrentUserHasRight(Right.ProjectsAndCalculate))
            {
                return false;
            }

            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null || selectedItem.ReadOnly)
            {
                return false;
            }

            // Check the selected item (or it's parent) accuracy, and return false if it is other than FineCalculation
            if (!this.IsFineCalculationAccuracy(selectedItem))
            {
                return false;
            }

            if (selectedItem is PartTreeItem
                || (selectedItem is MaterialsTreeItem && selectedItem.Parent is PartTreeItem))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// The logic associated with the Create Raw Material command.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void CreateRawMaterial(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null
                || selectedItems.Count != 1)
            {
                return;
            }

            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null)
            {
                return;
            }

            Part parent = null;
            if (selectedItem is MaterialsTreeItem)
            {
                parent = selectedItem.Parent.DataObject as Part;
            }
            else
            {
                parent = selectedItem.DataObject as Part;
            }

            // Check if the parent entity is supported
            if (parent == null)
            {
                log.Error("The selected item's data object is not supported as parent of a Raw Material.");
                return;
            }

            // Create the material, associate it with the parent entity
            IDataSourceManager dataManager = selectedItem.DataObjectContext;

            RawMaterialViewModel rawMaterialVM = this.compositionContainer.GetExportedValue<RawMaterialViewModel>();
            rawMaterialVM.DataSourceManager = dataManager;
            rawMaterialVM.InitializeForCreation(false, parent, parent.CalculationVariant);
            rawMaterialVM.Title = LocalizedResources.General_CreateRawMaterial;
            this.windowService.ShowViewInDialog(rawMaterialVM, "RawMaterialWindowViewTemplate");
        }

        /// <summary>
        /// Determines whether the Create Raw Part command can be executed.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        /// true if the command can be executed; otherwise, false.
        /// </returns>
        private bool CanCreateRawPart(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null
                || selectedItems.Count != 1
                || !SecurityManager.Instance.CurrentUserHasRight(Right.ProjectsAndCalculate))
            {
                return false;
            }

            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null || selectedItem.ReadOnly)
            {
                return false;
            }

            // Check the selected item (or it's parent) accuracy, and return false if it is other than FineCalculation
            if (!this.IsFineCalculationAccuracy(selectedItem))
            {
                return false;
            }

            if (selectedItem.GetType() == typeof(PartTreeItem)
                || (selectedItem is MaterialsTreeItem && selectedItem.Parent.GetType() == typeof(PartTreeItem)))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// The logic associated with the Create Raw Part command.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void CreateRawPart(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null
                || selectedItems.Count != 1)
            {
                return;
            }

            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null)
            {
                return;
            }

            Part parent = null;
            if (selectedItem is MaterialsTreeItem)
            {
                parent = selectedItem.Parent.DataObject as Part;
            }
            else
            {
                parent = selectedItem.DataObject as Part;
            }

            // Check if the parent entity is supported
            if (parent == null)
            {
                log.Error("The selected item's data object is not supported as parent of a Raw Part.");
                return;
            }

            if (parent.RawPart != null)
            {
                if (parent.RawPart.IsDeleted)
                {
                    this.windowService.MessageDialogService.Show(LocalizedResources.Error_PartAlreadyContainsDeletedRawPart, MessageDialogType.Error);
                }
                else
                {
                    this.windowService.MessageDialogService.Show(LocalizedResources.Error_PartAlreadyContainsRawPart, MessageDialogType.Error);
                }

                return;
            }

            // Create the Part, associate it with the parent entity
            IDataSourceManager dataManager = selectedItem.DataObjectContext;

            if (parent != null)
            {
                PartViewModel partVM = this.compositionContainer.GetExportedValue<PartViewModel>();
                partVM.DataSourceManager = dataManager;
                partVM.InitializeForCreation(false, parent, true);
                this.windowService.ShowViewInDialog(partVM, "RawPartWindowViewTemplate");
            }
            else
            {
                log.Error("Failed to determine the parent for creating a part.");
            }
        }

        /// <summary>
        /// Determines whether the Create Commodity command can be executed.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        /// true if the command can be executed; otherwise, false.
        /// </returns>
        private bool CanCreateCommodity(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null
                || selectedItems.Count != 1
                || !SecurityManager.Instance.CurrentUserHasRight(Right.ProjectsAndCalculate))
            {
                return false;
            }

            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null || selectedItem.ReadOnly)
            {
                return false;
            }

            // Check the selected item (or it's parent) accuracy, and return false if it is other than FineCalculation
            if (!this.IsFineCalculationAccuracy(selectedItem))
            {
                return false;
            }

            if (selectedItem is PartTreeItem
                || selectedItem is MaterialsTreeItem
                || (selectedItem is ProcessStepTreeItem && selectedItem.DataObject is AssemblyProcessStep))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// The logic associated with the Create Commodity command.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void CreateCommodity(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null
                || selectedItems.Count != 1)
            {
                return;
            }

            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null)
            {
                return;
            }

            object parentEntity = null;
            if (selectedItem is MaterialsTreeItem)
            {
                parentEntity = selectedItem.Parent.DataObject;
            }
            else
            {
                parentEntity = selectedItem.DataObject;
            }

            if (!(parentEntity is Part) && !(parentEntity is AssemblyProcessStep))
            {
                log.Error("Entity {0} is not supported as parent of a Commodity.", parentEntity.GetType().FullName);
                return;
            }

            var commodityVM = this.compositionContainer.GetExportedValue<CommodityViewModel>();
            commodityVM.DataSourceManager = selectedItem.DataObjectContext;
            commodityVM.SavesToDataSource = parentEntity is Part ? true : false;
            commodityVM.CommodityParent = parentEntity;
            commodityVM.InitializeForCreation(false, parentEntity);
            commodityVM.Title = LocalizedResources.General_CreateCommodity;

            this.windowService.ShowViewInDialog(commodityVM, "CommodityWindowViewTemplate");

            var commodity = commodityVM.Model;
            var parentStep = parentEntity as AssemblyProcessStep;
            if (commodity != null
                && parentStep != null
                && commodityVM.Saved)
            {
                // Send a message notifying that a sub-entity of process step was created.
                var stepMsg = new ProcessStepChangedMessage(this, parentStep)
                {
                    StepID = parentStep.Guid,
                    SubEntity = commodity,
                    ChangeType = ProcessStepChangeType.SubEntityAdded
                };
                this.messenger.Send(stepMsg, GlobalMessengerTokens.MainViewTargetToken);

                // Send a message notifying of the creation
                string notification = commodity.IsMasterData ? Notification.MasterDataEntityChanged : Notification.MyProjectsEntityChanged;
                EntityChangedMessage msg = new EntityChangedMessage(notification);
                msg.Entity = commodity;
                msg.Parent = parentStep;
                msg.ChangeType = EntityChangeType.EntityCreated;
                this.messenger.Send(msg);
            }
        }

        /// <summary>
        /// Determines whether the Create Consumable command can be executed.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        /// true if the command can be executed; otherwise, false.
        /// </returns>
        private bool CanCreateConsumable(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null
                || selectedItems.Count != 1
                || !SecurityManager.Instance.CurrentUserHasRight(Right.ProjectsAndCalculate))
            {
                return false;
            }

            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null || selectedItem.ReadOnly)
            {
                return false;
            }

            // Check the selected item (or it's parent) accuracy, and return false if it is other than FineCalculation
            if (!this.IsFineCalculationAccuracy(selectedItem))
            {
                return false;
            }

            if (selectedItem is ProcessStepTreeItem)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// The logic associated with the Create Consumable command.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void CreateConsumable(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null
                || selectedItems.Count != 1)
            {
                return;
            }

            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null)
            {
                return;
            }

            ProcessStep parentStep = (ProcessStep)selectedItem.DataObject;
            if (parentStep == null)
            {
                log.Error("Entity {0} is not supported as parent of a Consumable.", parentStep.GetType().FullName);
                return;
            }

            object parent = null;
            Process process = parentStep.Process;

            if (process != null)
            {
                Assembly parentAssembly = process.Assemblies.FirstOrDefault();
                parent = parentAssembly;
                if (parentAssembly == null)
                {
                    Part parentPart = process.Parts.FirstOrDefault();
                    parent = parentPart;
                }
            }

            var consumableVM = this.compositionContainer.GetExportedValue<ConsumableViewModel>();
            consumableVM.DataSourceManager = selectedItem.DataObjectContext;
            consumableVM.SavesToDataSource = false;
            consumableVM.ConsumableParent = parent;
            consumableVM.InitializeForCreation(false, parentStep);
            consumableVM.Title = LocalizedResources.General_CreateConsumable;

            this.windowService.ShowViewInDialog(consumableVM, "ConsumableWindowViewTemplate");

            var consumable = consumableVM.Model;
            if (consumable != null && consumableVM.Saved)
            {
                // Send a message notifying that a sub-entity of process step was created.
                var stepMsg = new ProcessStepChangedMessage(this, parentStep)
                {
                    StepID = parentStep.Guid,
                    SubEntity = consumable,
                    ChangeType = ProcessStepChangeType.SubEntityAdded
                };
                this.messenger.Send(stepMsg, GlobalMessengerTokens.MainViewTargetToken);

                // Send a message notifying of the creation
                string notification = consumable.IsMasterData ? Notification.MasterDataEntityChanged : Notification.MyProjectsEntityChanged;
                EntityChangedMessage msg = new EntityChangedMessage(notification);
                msg.Entity = consumable;
                msg.Parent = parentStep;
                msg.ChangeType = EntityChangeType.EntityCreated;
                this.messenger.Send(msg);
            }
        }

        /// <summary>
        /// Determines whether the Create Machine command can be executed.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        /// true if the command can be executed; otherwise, false.
        /// </returns>
        private bool CanCreateMachine(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null
                || selectedItems.Count != 1
                || !SecurityManager.Instance.CurrentUserHasRight(Right.ProjectsAndCalculate))
            {
                return false;
            }

            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null || selectedItem.ReadOnly)
            {
                return false;
            }

            // Check the selected item (or it's parent) accuracy, and return false if it is other than FineCalculation
            if (!this.IsFineCalculationAccuracy(selectedItem))
            {
                return false;
            }

            if (selectedItem is ProcessStepTreeItem)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// The logic associated with the Create Machine command.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void CreateMachine(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null
                || selectedItems.Count != 1)
            {
                return;
            }

            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null)
            {
                return;
            }

            ProcessStep parentStep = (ProcessStep)selectedItem.DataObject;

            Project parentProject = null;
            var parentProjectItem = this.FindParentProjectItem(selectedItem);
            if (parentProjectItem != null)
            {
                parentProject = parentProjectItem.DataObject as Project;
            }

            object parentEntity = null;
            if (selectedItem.Parent != null && selectedItem.Parent.Parent != null)
            {
                parentEntity = selectedItem.Parent.Parent.DataObject;
            }

            IDataSourceManager dataManager = selectedItem.DataObjectContext;

            // Extract some data needed for the creation of the machine
            var machineCalcParams = new MachineCostCalculationParameters();
            string costCalculationVersion = null;

            Assembly assembly = parentEntity as Assembly;
            if (assembly != null)
            {
                costCalculationVersion = assembly.CalculationVariant;
                if (assembly.CountrySettings != null)
                {
                    machineCalcParams.CountrySettingsAirCost = assembly.CountrySettings.AirCost;
                    machineCalcParams.CountrySettingsEnergyCost = assembly.CountrySettings.EnergyCost;
                    machineCalcParams.CountrySettingsWaterCost = assembly.CountrySettings.WaterCost;
                }

                if (parentProject != null)
                {
                    machineCalcParams.ProjectCreateDate =
                        CostCalculationHelper.GetProjectCreateDate(parentProject);
                }
            }
            else
            {
                Part part = parentEntity as Part;
                if (part != null)
                {
                    costCalculationVersion = part.CalculationVariant;
                    if (part.CountrySettings != null)
                    {
                        machineCalcParams.CountrySettingsAirCost = part.CountrySettings.AirCost;
                        machineCalcParams.CountrySettingsEnergyCost = part.CountrySettings.EnergyCost;
                        machineCalcParams.CountrySettingsWaterCost = part.CountrySettings.WaterCost;
                    }
                }
            }

            if (parentProject != null)
            {
                machineCalcParams.ProjectDepreciationPeriod = parentProject.DepreciationPeriod.GetValueOrDefault();
                machineCalcParams.ProjectDepreciationRate = parentProject.DepreciationRate.GetValueOrDefault();
            }

            machineCalcParams.ProcessStepHoursPerShift = parentStep.HoursPerShift.GetValueOrDefault();
            machineCalcParams.ProcessStepProductionWeeksPerYear = parentStep.ProductionWeeksPerYear.GetValueOrDefault();
            machineCalcParams.ProcessStepShiftsPerWeek = parentStep.ShiftsPerWeek.GetValueOrDefault();
            machineCalcParams.ProcessStepExtraShiftsPerWeek = parentStep.ExtraShiftsNumber.GetValueOrDefault();

            // Create the machine and associate it with the process step
            var machineVM = this.compositionContainer.GetExportedValue<MachineViewModel>();
            machineVM.Title = LocalizedResources.General_CreateMachine;
            machineVM.DataSourceManager = dataManager;
            machineVM.SavesToDataSource = false;
            machineVM.MachineCalculationParams = machineCalcParams;
            machineVM.CostCalculationVersion = costCalculationVersion;
            machineVM.InitializeForCreation(false, parentStep);
            this.windowService.ShowViewInDialog(machineVM, "MachineWindowViewTemplate");

            Machine machine = machineVM.Model;
            if (machine != null && machineVM.Saved)
            {
                // Send a message notifying that a sub-entity of process step was created.
                var stepMsg = new ProcessStepChangedMessage(this, parentStep)
                {
                    StepID = parentStep.Guid,
                    SubEntity = machine,
                    ChangeType = ProcessStepChangeType.SubEntityAdded
                };
                this.messenger.Send(stepMsg, GlobalMessengerTokens.MainViewTargetToken);

                // Send a message notifying of the creation
                string notification = machine.IsMasterData ? Notification.MasterDataEntityChanged : Notification.MyProjectsEntityChanged;
                EntityChangedMessage msg = new EntityChangedMessage(notification);
                msg.Entity = machine;
                msg.Parent = parentStep;
                msg.ChangeType = EntityChangeType.EntityCreated;
                this.messenger.Send(msg);
            }
        }

        /// <summary>
        /// Determines whether the Create Die command can be executed.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        /// true if the command can be executed; otherwise, false.
        /// </returns>
        private bool CanCreateDie(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null
                || selectedItems.Count != 1
                || !SecurityManager.Instance.CurrentUserHasRight(Right.ProjectsAndCalculate))
            {
                return false;
            }

            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null || selectedItem.ReadOnly)
            {
                return false;
            }

            // Check the selected item (or it's parent) accuracy, and return false if it is other than FineCalculation
            if (!this.IsFineCalculationAccuracy(selectedItem))
            {
                return false;
            }

            if (selectedItem is ProcessStepTreeItem)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// The logic associated with the Create Die command.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void CreateDie(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null
                || selectedItems.Count != 1)
            {
                return;
            }

            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null)
            {
                return;
            }

            ProcessStep parentStep = (ProcessStep)selectedItem.DataObject;
            IDataSourceManager dataManager = selectedItem.DataObjectContext;

            object parent = null;
            Process process = parentStep.Process;

            if (process != null)
            {
                Assembly parentAssembly = process.Assemblies.FirstOrDefault();
                parent = parentAssembly;
                if (parentAssembly == null)
                {
                    Part parentPart = process.Parts.FirstOrDefault();
                    parent = parentPart;
                }
            }

            DieViewModel dieVM = this.compositionContainer.GetExportedValue<DieViewModel>();
            dieVM.DataSourceManager = dataManager;
            dieVM.SavesToDataSource = false;
            dieVM.DieParent = parent;
            dieVM.InitializeForCreation(false, parentStep);
            dieVM.Title = LocalizedResources.General_CreateDie;
            this.windowService.ShowViewInDialog(dieVM, "DieViewTemplate");

            Die newDie = dieVM.Model;
            if (newDie != null && dieVM.Saved)
            {
                // Send a message notifying that a sub-entity of process step was created.
                var stepMsg = new ProcessStepChangedMessage(this, parentStep)
                {
                    StepID = parentStep.Guid,
                    SubEntity = newDie,
                    ChangeType = ProcessStepChangeType.SubEntityAdded
                };
                this.messenger.Send(stepMsg, GlobalMessengerTokens.MainViewTargetToken);

                // Send a message notifying of the creation
                string notification = newDie.IsMasterData ? Notification.MasterDataEntityChanged : Notification.MyProjectsEntityChanged;
                EntityChangedMessage msg = new EntityChangedMessage(notification);
                msg.Entity = newDie;
                msg.Parent = parentStep;
                msg.ChangeType = EntityChangeType.EntityCreated;
                this.messenger.Send(msg);
            }
        }

        /// <summary>
        /// Determines whether the Selective Sync command can be executed.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        ///   <c>true</c> if this instance [can selective sync] the specified parameter; otherwise, <c>false</c>.
        /// </returns>
        private bool CanSelectiveSync(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null
                || selectedItems.Count != 1)
            {
                return false;
            }

            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null || selectedItem.ReadOnly)
            {
                return false;
            }

            if (!SecurityManager.Instance.CurrentUserHasRight(Right.ProjectsAndCalculate))
            {
                return false;
            }

            if (selectedItem is ProjectFolderTreeItem)
            {
                return true;
            }
            else if (selectedItem is ProjectTreeItem)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// The logic associated with the Selective Sync command.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void SelectiveSync(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null
                || selectedItems.Count != 1)
            {
                return;
            }

            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null)
            {
                return;
            }

            IDataSourceManager dataManager = selectedItem.DataObjectContext;
            if (dataManager == null)
            {
                throw new ArgumentException("The data context associated with the selected projects tree item was null.", "dataContext");
            }

            var selectedDataObject = selectedItem.DataObject;
            if (selectedDataObject == null)
            {
                return;
            }

            ProjectFolder folder = selectedDataObject as ProjectFolder;
            if (folder != null)
            {
                folder.IsOffline = !folder.IsOffline;
                dataManager.SaveChanges();

                this.SetFolderChildrenOfflineState(selectedItem, folder.IsOffline);
            }
            else
            {
                Project project = selectedDataObject as Project;
                if (project != null)
                {
                    project.IsOffline = !project.IsOffline;
                    dataManager.SaveChanges();
                }
            }

            selectedItem.Refresh();

            var msg = new EntityChangedMessage(Notification.MyProjectsEntityChanged, selectedItem.DataObject, null, EntityChangeType.EntityUpdated);
            this.messenger.Send(msg);
        }

        /// <summary>
        /// Determines whether the BomImportCommand can execute.
        /// </summary>
        /// <param name="parameter">The command parameter.</param>
        /// <returns>
        /// true if the command can execute with the specified parameter; otherwise, false.
        /// </returns>
        private bool CanBomImport(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null
                || selectedItems.Count != 1)
            {
                return false;
            }

            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem != null)
            {
                // Check the selected item (or it's parent) accuracy, and return false if it is other than FineCalculation
                if (!this.IsFineCalculationAccuracy(selectedItem))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Implements the logic associated with the BomImportCommand.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void BomImport(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null
                || selectedItems.Count != 1)
            {
                return;
            }

            var selectedItem = selectedItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null)
            {
                return;
            }

            BomImporterWizardViewModel bomimportWindow = this.compositionContainer.GetExportedValue<BomImporterWizardViewModel>();
            bomimportWindow.DataContext = selectedItem.DataObjectContext;
            bomimportWindow.Parent = selectedItem.DataObject;

            this.windowService.ShowViewInDialog(bomimportWindow);
        }

        /// <summary>
        /// Determines whether the Add Bookmark command can execute.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns> True if the command can be executed; otherwise, false. </returns>
        /// <remarks>It returns true if at least one object is not bookmarked yet.</remarks>
        private bool CanAddBookmark(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null || selectedItems.Count == 0)
            {
                return false;
            }

            var existsNotBookmarkedObjects = false;
            foreach (var item in selectedItems)
            {
                var treeItem = item as TreeViewDataItem;
                if (treeItem == null
                    || treeItem.ReadOnly
                    || (!(treeItem.DataObject is Project) && !(treeItem.DataObject is Assembly) && !(treeItem.DataObject is Part))
                    || EntityUtils.IsMasterData(treeItem.DataObject))
                {
                    return false;
                }

                var identifiableObj = treeItem.DataObject as IIdentifiable;
                if (identifiableObj != null)
                {
                    if (!this.IsObjectBookmarked(treeItem.DataObjectContext, identifiableObj.Guid))
                    {
                        existsNotBookmarkedObjects = true;
                    }
                }
            }

            return existsNotBookmarkedObjects;
        }

        /// <summary>
        /// The logic associated with the Add Bookmark command.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void AddBookmark(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null || selectedItems.Count == 0)
            {
                return;
            }

            var dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            object freshEntity = null;
            var itemType = BookmarkedItemType.None;
            var owner = dataManager.UserRepository.GetById(SecurityManager.Instance.CurrentUser.Guid, false);
            var messages = new List<BookmarkChangedMessage>();

            foreach (var item in selectedItems)
            {
                var treeItem = item as TreeViewDataItem;
                var identifiableObject = treeItem.DataObject as IIdentifiable;
                if (identifiableObject != null)
                {
                    var itemId = identifiableObject.Guid;
                    if (!this.IsObjectBookmarked(dataManager, itemId))
                    {
                        Project project = treeItem.DataObject as Project;
                        if (project != null)
                        {
                            freshEntity = dataManager.ProjectRepository.GetProjectForProjectsTree(project.Guid);
                            itemType = BookmarkedItemType.Project;
                        }
                        else
                        {
                            Assembly assembly = treeItem.DataObject as Assembly;
                            if (assembly != null)
                            {
                                freshEntity = dataManager.AssemblyRepository.GetAssembly(assembly.Guid);
                                itemType = BookmarkedItemType.Assembly;
                            }
                            else
                            {
                                Part part = treeItem.DataObject as Part;
                                if (part != null)
                                {
                                    freshEntity = dataManager.PartRepository.GetPart(part.Guid);
                                    itemType = BookmarkedItemType.Part;
                                }
                            }
                        }

                        if (freshEntity != null && itemType != BookmarkedItemType.None && itemId != Guid.Empty)
                        {
                            Bookmark bookmark = new Bookmark() { EntityTypeId = itemType, EntityId = itemId, Owner = owner };
                            dataManager.BookmarkRepository.Add(bookmark);
                            dataManager.SaveChanges();

                            bookmarksStateCache[itemId] = true;

                            messages.Add(new BookmarkChangedMessage() { Entity = freshEntity, ChangeType = BookmarkChangeType.BookmarkCreated });
                        }
                    }
                }
            }

            // Send all messages to refresh bookmarks
            foreach (var msg in messages)
            {
                this.messenger.Send(msg);
            }
        }

        /// <summary>
        /// Determines whether the Remove Bookmark command can execute.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns> True if the command can be executed; otherwise, false. </returns>
        private bool CanRemoveBookmark(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null || selectedItems.Count == 0)
            {
                return false;
            }

            var dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var existsBookmarkedObjects = false;
            foreach (var item in selectedItems)
            {
                var treeItem = item as TreeViewDataItem;
                if (treeItem == null
                    || treeItem.ReadOnly
                    || EntityUtils.IsMasterData(treeItem.DataObject))
                {
                    return false;
                }

                var identifiableObj = treeItem.DataObject as IIdentifiable;
                if (identifiableObj != null)
                {
                    if (this.IsObjectBookmarked(dataManager, identifiableObj.Guid))
                    {
                        existsBookmarkedObjects = true;
                    }
                }
            }

            return existsBookmarkedObjects;
        }

        /// <summary>
        /// The logic associated with the Remove Bookmark command.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void RemoveBookmark(object parameter)
        {
            var selectedItems = this.projectsExplorerBase.SelectedTreeItems;
            if (selectedItems == null || selectedItems.Count == 0)
            {
                return;
            }

            var dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var messages = new List<BookmarkChangedMessage>();

            foreach (var item in selectedItems)
            {
                var treeItem = item as TreeViewDataItem;
                var identifiableObject = treeItem.DataObject as IIdentifiable;
                if (identifiableObject != null)
                {
                    if (this.IsObjectBookmarked(dataManager, identifiableObject.Guid))
                    {
                        dataManager.BookmarkRepository.DeleteBookmark(treeItem.DataObject);
                        dataManager.SaveChanges();

                        bookmarksStateCache[identifiableObject.Guid] = false;

                        messages.Add(new BookmarkChangedMessage() { Entity = treeItem.DataObject, ChangeType = BookmarkChangeType.BookmarkDeleted });
                    }
                }
            }

            // Send all messages to refresh bookmarks
            foreach (var msg in messages)
            {
                this.messenger.Send(msg);
            }
        }

        #endregion Commands Handling

        #region Import / Export Helpers

        /// <summary>
        /// Imports the selected entities.
        /// </summary>
        /// <param name="supportedTypes">The supported types of the entities to import.</param>
        /// <param name="parent">The parent into which to import the entities.</param>
        /// <param name="dataContext">The data context in which to import.</param>
        /// <param name="saveChanges">If set to true, saves changes to dataContext, else adds the entity to the dataContext.</param>
        private void ImportEntities(List<Type> supportedTypes, object parent, IDataSourceManager dataContext, bool saveChanges)
        {
            var fileDialogService = this.compositionContainer.GetExportedValue<IFileDialogService>();
            fileDialogService.Filter = UIUtils.GetDialogFilterForMultipleImportedTypes(supportedTypes);
            fileDialogService.Multiselect = true;
            if (fileDialogService.ShowOpenFileDialog() != true)
            {
                return;
            }

            var filePaths = new List<string>(fileDialogService.FileNames);

            Action<PleaseWaitService.WorkParams, IEnumerable<ImportedData<object>>> importCompleted = (workParams, importedData) =>
            {
                // Perform action after the import is completed.
                foreach (var entityData in importedData.Where(imp => imp.Entity != null))
                {
                    var parentStep = parent as ProcessStep;
                    if (parentStep != null)
                    {
                        // Send a message notifying a sub-entity of process step was created.
                        var stepMsg = new ProcessStepChangedMessage(this, parentStep)
                        {
                            StepID = parentStep.Guid,
                            SubEntity = entityData.Entity,
                            ChangeType = ProcessStepChangeType.SubEntityAdded
                        };
                        this.messenger.Send(stepMsg, GlobalMessengerTokens.MainViewTargetToken);
                    }

                    bool importedObjectIsMasterData = EntityUtils.IsMasterData(entityData.Entity);
                    string notification = importedObjectIsMasterData ? Notification.MasterDataEntityChanged : Notification.MyProjectsEntityChanged;

                    // The last imported item will be selected in the tree.
                    bool selectItem = entityData == importedData.LastOrDefault(i => i.Entity != null) ? true : false;

                    EntityChangedMessage msg = new EntityChangedMessage(notification)
                    {
                        // Create a message to notify that a new item has been imported.
                        Entity = entityData.Entity,
                        Parent = parent,
                        ChangeType = EntityChangeType.EntityImported,
                        SelectEntity = selectItem
                    };
                    this.messenger.Send(msg);
                }
            };

            this.importService.Import(filePaths, parent, dataContext, saveChanges, importCompleted);
        }

        #endregion Import / Export Helpers

        #region Helpers

        /// <summary>
        /// Finds the Project item in whose sub-tree a specified item is located.
        /// </summary>
        /// <param name="item">The item for which to search for the parent project item.</param>
        /// <returns>
        /// The project item or null if it doesn't exist.
        /// </returns>
        private ProjectTreeItem FindParentProjectItem(TreeViewDataItem item)
        {
            ProjectTreeItem projectItem = item as ProjectTreeItem;
            if (projectItem != null)
            {
                return projectItem;
            }
            else if (item.Parent != null)
            {
                return FindParentProjectItem(item.Parent);
            }

            return null;
        }

        /// <summary>
        /// Checks if the calculation accuracy of an assembly or part is FineCalculation.
        /// It also checks for the item's parent, if it is an assembly or part.
        /// </summary>
        /// <param name="item">The item to check for.</param>
        /// <returns>True if the accuracy is FineCalculation, False otherwise.</returns>
        private bool IsFineCalculationAccuracy(TreeViewDataItem item)
        {
            var assembly = item.DataObject as Assembly;
            if (assembly != null)
            {
                var accuracy = EntityUtils.ConvertToEnum(assembly.CalculationAccuracy, PartCalculationAccuracy.FineCalculation);
                if (accuracy != PartCalculationAccuracy.FineCalculation)
                {
                    return false;
                }

                return true;
            }

            var part = item.DataObject as Part;
            if (part != null)
            {
                var accuracy = EntityUtils.ConvertToEnum(part.CalculationAccuracy, PartCalculationAccuracy.FineCalculation);
                if (accuracy != PartCalculationAccuracy.FineCalculation)
                {
                    return false;
                }

                return true;
            }

            var processStep = item.DataObject as ProcessStep;
            if (processStep != null)
            {
                if (!IsProcessStepLoadedAndAccuracyCalculated())
                {
                    return false;
                }

                return true;
            }

            if (item.Parent != null)
            {
                // Check the item's parent if is an assembly
                var assemblyParent = item.Parent.DataObject as Assembly;
                if (assemblyParent != null)
                {
                    var accuracy = EntityUtils.ConvertToEnum(assemblyParent.CalculationAccuracy, PartCalculationAccuracy.FineCalculation);
                    if (accuracy != PartCalculationAccuracy.FineCalculation)
                    {
                        return false;
                    }

                    return true;
                }

                // Check the item's parent if is a part
                var partParent = item.Parent.DataObject as Part;
                if (partParent != null)
                {
                    var accuracy = EntityUtils.ConvertToEnum(partParent.CalculationAccuracy, PartCalculationAccuracy.FineCalculation);
                    if (accuracy != PartCalculationAccuracy.FineCalculation)
                    {
                        return false;
                    }

                    return true;
                }
            }

            return true;
        }

        /// <summary>
        /// Checks if the loaded view model is a <see cref="ProcessStepEditorViewModel"/> and the process step contained has its accuracy Calculated
        /// </summary>
        /// <returns>True is the process step has accuracy <see cref="ProcessCalculationAccuracy.Calculated"/> and false otherwise</returns>        
        private bool IsProcessStepLoadedAndAccuracyCalculated()
        {
            // TODO: use local messenger after the all command handling is moved from MainViewModel here
            var messenger = new Messenger();

            object mainViewContent = null;

            // This is the callback for the message that requests the main view's content;
            var message = new NotificationMessageWithAction<object>(Notification.MainViewGetContent, (content) => mainViewContent = content);
            messenger.Send(message, GlobalMessengerTokens.MainViewTargetToken);

            var processStepEditor = mainViewContent as ProcessStepEditorViewModel;
            if (processStepEditor != null
                && processStepEditor.EditedStep.Value != null
                && processStepEditor.EditedStep.Value.Accuracy.Value == ProcessCalculationAccuracy.Calculated)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Sets the offline state (IsOffline flag) of the specified folder's children hierarchy to the specified value.
        /// </summary>
        /// <param name="folderItem">The folder item whose children to update.</param>
        /// <param name="isOffline">if set to true set the children to offline; otherwise set it to online.</param>
        private void SetFolderChildrenOfflineState(TreeViewDataItem folderItem, bool isOffline)
        {
            // The data context used for setting the offline state of the folder's children that are not loaded.
            IDataSourceManager dc = DataAccessFactory.CreateDataSourceManager(folderItem.DataObjectContext.DatabaseId);

            foreach (var child in folderItem.Children)
            {
                IDataSourceManager dataManager = child.DataObjectContext;

                ProjectFolder folder = child.DataObject as ProjectFolder;
                if (folder != null)
                {
                    folder.IsOffline = isOffline;
                    dataManager.SaveChanges();
                    child.Refresh();

                    if (child.AreChildrenLoaded)
                    {
                        this.SetFolderChildrenOfflineState(child, isOffline);
                    }
                    else
                    {
                        this.SetFolderChildrenOfflineState(folder.Guid, isOffline, dc);
                        dc.SaveChanges();
                    }
                }
                else
                {
                    Project project = child.DataObject as Project;
                    if (project != null)
                    {
                        project.IsOffline = isOffline;
                        dataManager.SaveChanges();
                        child.Refresh();
                    }
                }
            }
        }

        /// <summary>
        /// Sets the offline state of the children hierarchy of the folder specified by <paramref name="folderId" /> to the value specified by <paramref name="isOffline" />,
        /// using the specified data context to load the children. The data context changes are not saved.
        /// </summary>
        /// <param name="folderId">The id of the folder.</param>
        /// <param name="isOffline">if set to true set the children to offline; otherwise set it to online.</param>
        /// <param name="dataContext">The data context to use for loading the children.</param>
        private void SetFolderChildrenOfflineState(Guid folderId, bool isOffline, IDataSourceManager dataContext)
        {
            Guid userId = SecurityManager.Instance.CurrentUser.Guid;
            var folders = dataContext.ProjectFolderRepository.GetSubFolders(folderId, userId);
            foreach (var folder in folders)
            {
                folder.IsOffline = isOffline;
                this.SetFolderChildrenOfflineState(folder.Guid, isOffline, dataContext);
            }

            var projects = dataContext.ProjectRepository.GetProjectsOfFolder(folderId);
            foreach (var project in projects)
            {
                project.IsOffline = isOffline;
            }
        }

        /// <summary>
        /// Checks if the provided entity is already added to bookmarks.
        /// Also it refreshes the bookmarks state cache in order to minimize the access to the database.
        /// </summary>
        /// <param name="dataManager">The data source manager.</param>
        /// <param name="objectId">The id of the object to check.</param>
        /// <returns>True if the object is bookmarked or false otherwise.</returns>
        private bool IsObjectBookmarked(IDataSourceManager dataManager, Guid objectId)
        {
            var isBookmarked = false;
            if (bookmarksStateCache.ContainsKey(objectId))
            {
                isBookmarked = bookmarksStateCache[objectId];
            }
            else
            {
                isBookmarked = dataManager.BookmarkRepository.HasBookmark(objectId);
                if (isBookmarked)
                {
                    bookmarksStateCache.Add(objectId, true);
                }
                else
                {
                    bookmarksStateCache.Add(objectId, false);
                }
            }

            return isBookmarked;
        }

        /// <summary>
        /// Request the creation of a new Read-Only tab into the application MainDocuments container.
        /// </summary>
        /// <param name="content">The new tab content.</param>
        private void RequiredReadonlyDocumentContentChanging(object content)
        {
            var navMessage = new UpdateReadonlyDocumentContentMessage(content);
            messenger.Send(navMessage, GlobalMessengerTokens.MainViewTargetToken);
        }

        #endregion Helpers
    }
}
