﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;
using ZPKTool.Gui.Services;
using ZPKTool.MvvmCore;

namespace ZPKTool.Gui
{
    /// <summary>
    /// Configure and initialize the UI composition framework.
    /// </summary>
    public class Bootstrapper : MefBootstrapper
    {
        /// <summary>
        /// Adds all necessary modules into the module catalog.
        /// </summary>
        protected override void ConfigureModuleCatalog()
        {
            this.ModuleCatalog.Catalogs.Add(new AssemblyCatalog(System.Reflection.Assembly.GetExecutingAssembly()));
        }

        /// <summary>
        /// Starts the GUI services that have startup logic.
        /// </summary>
        public void StartServices()
        {
            if (!this.IsInitialized)
            {
                throw new InvalidOperationException("The bootstrapper has not been initialized. Call the Run method before using the bootstrapper.");
            }

            var onlineChecker = this.CompositionContainer.GetExportedValue<IOnlineCheckService>();
            onlineChecker.Start(ZPKTool.Common.UserSettingsManager.Instance.CentralServerOnlineCheckInterval);
            onlineChecker.CheckAsync();
        }
    }
}
