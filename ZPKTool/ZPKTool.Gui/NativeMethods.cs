﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace ZPKTool.Gui
{
    /// <summary>
    /// Contains imports of native Win32 methods.
    /// </summary>
    internal static class NativeMethods
    {
        /// <summary>
        /// Activates the window and displays it as a maximized window.
        /// </summary>
        internal const int SW_SHOWMAXIMIZED = 3;

        /// <summary>
        /// Activates and displays the window. If the window is minimized or maximized, the system restores it to its original size and position.
        /// An application should specify this flag when restoring a minimized window.
        /// </summary>
        internal const int SW_RESTORE = 9;

        /// <summary>
        /// The restored window will be maximized, regardless of whether it was maximized before it was minimized.
        /// </summary>
        internal const int WPF_RESTORETOMAXIMIZED = 0x2;

        /// <summary>
        /// An application sends the WM_COPYDATA message to pass data to another application.
        /// </summary>
        internal const int WM_COPYDATA = 0x4A;

        /// <summary>
        /// Brings the specified window to the top of the z-order. If the window is a top-level window, it is activated.
        /// If the window is a child window, the top-level parent window associated with the child window is activated.
        /// </summary>
        /// <param name="windowHandle">The handle of the window.</param>
        /// <returns>
        /// True for success, false for failure.
        /// </returns>
        [DllImport("user32.dll")]
        internal static extern bool BringWindowToTop(IntPtr windowHandle);

        /// <summary>
        /// Activates, focuses and switches input to a specified window.
        /// </summary>
        /// <param name="windowHandle">The handle of the window.</param>
        /// <returns>
        /// True if the window was activated, false otherwise.
        /// </returns>
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool SetForegroundWindow(IntPtr windowHandle);

        /// <summary>
        /// Sets the specified window's show state.
        /// </summary>
        /// <param name="windowHandle">The window handle.</param>
        /// <param name="showCommand">Controls how the window is to be shown.</param>
        /// <returns>
        /// If the window was previously visible, the return value is nonzero, otherwise if the window was previously hidden, the return value is zero.
        /// </returns>
        [DllImport("user32.dll")]
        internal static extern bool ShowWindow(IntPtr windowHandle, int showCommand);

        /// <summary>
        /// Sends the specified message to a window or windows.
        /// It calls the window procedure for the specified window and does not return until the window procedure has processed the message.
        /// </summary>
        /// <param name="windowHandle">The window handle.</param>
        /// <param name="messageID">The message ID.</param>
        /// <param name="wParam">The message's wParam value.</param>
        /// <param name="lParam">The message's lParam value.</param>
        /// <returns>The return value specifies the result of the message processing; it depends on the message sent.</returns>
        [DllImport("User32.dll", EntryPoint = "SendMessage")]
        internal static extern int SendMessage(IntPtr windowHandle, int messageID, int wParam, ref COPYDATASTRUCT lParam);

        /// <summary>
        /// Retrieves the show state and the restored, minimized, and maximized positions of the specified window.
        /// </summary>
        /// <param name="windowHandle">The window handle.</param>
        /// <param name="placementInfo">Will contain the window placement information.</param>
        /// <returns>Non-zero value if the call succeeded; otherwise, zero.</returns>
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool GetWindowPlacement(IntPtr windowHandle, ref WINDOWPLACEMENT placementInfo);

        /// <summary>
        /// Retrieves a handle to an icon from the specified executable file, DLL, or icon file.
        /// </summary>
        /// <param name="hInst">A handle to the instance of the application calling the function.</param>
        /// <param name="lpszExeFileName">The name of an executable file, DLL, or icon file.</param>
        /// <param name="nIconIndex">The zero-based index of the icon to retrieve. If this value is -1, the function returns the total number of icons in the specified file.</param>
        /// <returns>
        /// A handle to an icon. If the file specified was not an executable file, DLL, or icon file, the return is 1. If no icons were found in the file, the return value is NULL.
        /// </returns>
        [DllImport("shell32.dll")]
        internal static extern IntPtr ExtractIcon(IntPtr hInst, string lpszExeFileName, int nIconIndex);

        /// <summary>
        /// Contains data to be passed to another application by the WM_COPYDATA message. 
        /// </summary>
        internal struct COPYDATASTRUCT
        {
            /// <summary>
            /// The data to be passed to the receiving application.
            /// </summary>
            public IntPtr dwData;

            /// <summary>
            /// The size, in bytes, of the data pointed to by the lpData member.
            /// </summary>
            public int cbData;

            /// <summary>
            /// The data to be passed to the receiving application. This member can be NULL.
            /// </summary>
            [MarshalAs(UnmanagedType.LPStr)]
            public string lpData;
        }

        /// <summary>
        /// Contains information about the placement of a window on the screen.
        /// </summary>
        internal struct WINDOWPLACEMENT
        {
            /// <summary>
            /// The length of the structure, in bytes.
            /// </summary>
            public int length;

            /// <summary>
            /// The flags that control the position of the minimized window and the method by which the window is restored. 
            /// This member can be one or more of the following values:
            /// WPF_ASYNCWINDOWPLACEMENT - 0x0004 | WPF_RESTORETOMAXIMIZED - 0x0002 | WPF_SETMINPOSITION - 0x0001.
            /// </summary>
            public int flags;

            /// <summary>
            /// The current show state of the window. 
            /// </summary>
            public int showCmd;

            /// <summary>
            /// The coordinates of the window's upper-left corner when the window is minimized.
            /// </summary>
            public System.Drawing.Point ptMinPosition;

            /// <summary>
            /// The coordinates of the window's upper-left corner when the window is maximized.
            /// </summary>
            public System.Drawing.Point ptMaxPosition;

            /// <summary>
            /// The window's coordinates when the window is in the restored position.
            /// </summary>
            public System.Drawing.Rectangle rcNormalPosition;
        }
    }
}
