﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.DataAccess;

namespace ZPKTool.Gui
{
    /// <summary>
    /// Contains the information about an entity, like the database identifier where it will be or is saved, etc.
    /// </summary>
    public class EntityInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EntityInfo"/> class.
        /// </summary>
        /// <param name="entity">The entity object.</param>
        /// <param name="dbIdentifier">The database identifier.</param>
        public EntityInfo(object entity, DbIdentifier dbIdentifier)
        {
            this.Entity = entity;
            this.DbIdentifier = dbIdentifier;
        }

        /// <summary>
        /// Gets or sets the entity object.
        /// </summary>
        public object Entity { get; set; }

        /// <summary>
        /// Gets or sets the database identifier.
        /// </summary>
        public DbIdentifier DbIdentifier { get; set; }
    }
}
