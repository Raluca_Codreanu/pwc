﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Common;

namespace ZPKTool.Gui
{
    /// <summary>
    /// The exception thrown by UI controls and components.
    /// </summary>
    public class UIException : ZPKException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UIException"/> class.
        /// </summary>
        public UIException()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UIException"/> class.
        /// </summary>
        /// <param name="errorCode">The code of the error</param>
        public UIException(string errorCode)
            : base(errorCode)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UIException"/> class.
        /// </summary>
        /// <param name="errorCode">The error code.</param>
        /// <param name="ex">The exception wrapped by this instance.</param>
        public UIException(string errorCode, Exception ex)
            : base(errorCode, ex)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UIException"/> class.
        /// </summary>
        /// <param name="errorCode">The error code.</param>
        /// <param name="errorMessage">The error message.</param>
        /// <param name="ex">The base exception</param>
        public UIException(string errorCode, string errorMessage, Exception ex)
            : base(errorCode, errorMessage, ex)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UIException"/> class.
        /// </summary>
        /// <param name="errorCode">The code of the error</param>
        /// <param name="errorMessage">The error message</param>
        public UIException(string errorCode, string errorMessage)
            : base(errorCode, errorMessage)
        {
        }
    }
}
