﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.Reporting
{
    /// <summary>
    /// The data used to fill a row of an enhanced assembly report cost tables.
    /// </summary>
    public class EnhancedAssyReportRowData
    {
        /// <summary>
        /// Gets or sets the "Pos" column value.
        /// </summary>
        public string Position { get; set; }

        /// <summary>
        /// Gets or sets the "PartNo" column value.
        /// </summary>
        public string PartNumber { get; set; }

        /// <summary>
        /// Gets or sets the "Amount" column value.
        /// </summary>
        public int Amount { get; set; }

        /// <summary>
        /// Gets or sets the "Name" column value.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the "Remark" column value.
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// Gets or sets the "Est / Calc" column value.
        /// </summary>
        [SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1623:PropertySummaryDocumentationMustMatchAccessors", Justification = "The form of the documentation is intentional.")]
        public bool Estimated { get; set; }

        /// <summary>
        /// Gets or sets the "Int / Ext" column value.
        /// </summary>
        [SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1623:PropertySummaryDocumentationMustMatchAccessors", Justification = "The form of the documentation is intentional.")]
        public bool External { get; set; }

        /// <summary>
        /// Gets or sets the "External Part Cost (Commodities)" column value.
        /// </summary>
        public decimal ExternalPartsAndCommoditiesCost { get; set; }

        /// <summary>
        /// Gets or sets the "OH on External Parts" column value.
        /// </summary>
        public decimal ExternalPartsAndCommoditiesOH { get; set; }

        /// <summary>
        /// Gets or sets the "Raw Material" column value.
        /// </summary>
        public decimal RawMaterial { get; set; }

        /// <summary>
        /// Gets or sets the "Raw Material OH" column value.
        /// </summary>
        public decimal RawMaterialOH { get; set; }

        /// <summary>
        /// Gets or sets the "Consumables" column value.
        /// </summary>
        public decimal Consumables { get; set; }

        /// <summary>
        /// Gets or sets the "Consumables OH" column value.
        /// </summary>
        public decimal ConsumablesOH { get; set; }

        /// <summary>
        /// Gets or sets the "Estimated Part Cost" column value.
        /// </summary>
        public decimal EstimatedPartCost { get; set; }

        /// <summary>
        /// Gets or sets the "Manufacturing Cost incl. Scrap" column value.
        /// </summary>
        public decimal ManufacturingCostIncludingScrap { get; set; }

        /// <summary>
        /// Gets or sets the "Manufacturing OH" column value.
        /// </summary>
        public decimal ManufacturingOH { get; set; }

        /// <summary>
        /// Gets or sets the "n-Tier Transport Cost" column value.
        /// </summary>
        public decimal ManufacturingTransport { get; set; }

        /// <summary>
        /// Gets or sets the "Dev't Cost" column value.
        /// </summary>
        public decimal DevelopmentCost { get; set; }

        /// <summary>
        /// Gets or sets the "Proj. Cost" column value.
        /// </summary>
        public decimal ProjectInvest { get; set; }

        /// <summary>
        /// Gets or sets the "Other Cost incl OH" column value.
        /// </summary>
        public decimal OtherCostIncludingOH { get; set; }

        /// <summary>
        /// Gets or sets the "Pkg Cost incl OH" column value.
        /// </summary>
        public decimal PackagingCostIncludingOH { get; set; }

        /// <summary>
        /// Gets or sets the "Logistics incl OH" column value.
        /// </summary>
        public decimal LogisticsIncludingOH { get; set; }

        /// <summary>
        /// Gets or sets the "Transport" column value.
        /// </summary>        
        public decimal Transport { get; set; }

        /// <summary>
        /// Gets or sets the "SG&amp;A" column value.
        /// </summary>
        public decimal SalesAndAdministrationOH { get; set; }

        /// <summary>
        /// Gets or sets the "Company Surcharge" column value.
        /// </summary>
        public decimal CompanySurchargeOH { get; set; }

        /// <summary>
        /// Gets or sets the "External Work OH" column value.
        /// </summary>
        public decimal ExternalWorkOH { get; set; }

        /// <summary>
        /// Gets or sets the "Margin on Material/Consumables/Commodities" column value.
        /// </summary>
        public decimal MarginOnMaterialConsumablesAndCommodities { get; set; }

        /// <summary>
        /// Gets or sets the "Margin on Manufacturing and Assembly" column value.
        /// </summary>
        public decimal MarginOnManufacturingAndAssembly { get; set; }

        /// <summary>
        /// Gets or sets the "Payment Cost" column value.
        /// </summary>
        public decimal PaymentCost { get; set; }

        /// <summary>
        /// Gets the "Piece Price" column value.
        /// </summary>
        public decimal PiecePrice
        {
            get
            {
                return this.ExternalPartsAndCommoditiesCost + this.ExternalPartsAndCommoditiesOH + this.RawMaterial + this.RawMaterialOH
                    + this.Consumables + this.ConsumablesOH + this.EstimatedPartCost + this.ManufacturingCostIncludingScrap + this.ManufacturingOH
                    + this.ManufacturingTransport + this.DevelopmentCost + this.ProjectInvest + this.OtherCostIncludingOH + this.PackagingCostIncludingOH
                    + this.LogisticsIncludingOH + this.Transport + this.SalesAndAdministrationOH + this.CompanySurchargeOH + this.ExternalWorkOH
                    + this.MarginOnMaterialConsumablesAndCommodities + this.MarginOnManufacturingAndAssembly + this.PaymentCost;
            }
        }

        /// <summary>
        /// Gets the "Total Price per Assy" column value.
        /// </summary>
        public virtual decimal TotalPricePerAssy
        {
            get { return this.PiecePrice * this.Amount; }
        }

        /// <summary>
        /// Copies the data to a specified instance.
        /// </summary>
        /// <param name="destination">The destination for the copying.</param>
        public void CopyData(EnhancedAssyReportRowData destination)
        {
            if (destination == null)
            {
                throw new ArgumentNullException("destination", "The 'destination' parameter was null.");
            }

            destination.Amount = this.Amount;
            destination.Consumables = this.Consumables;
            destination.ConsumablesOH = this.ConsumablesOH;
            destination.DevelopmentCost = this.DevelopmentCost;
            destination.Estimated = this.Estimated;
            destination.EstimatedPartCost = this.EstimatedPartCost;
            destination.External = this.External;
            destination.ExternalPartsAndCommoditiesCost = this.ExternalPartsAndCommoditiesCost;
            destination.ExternalPartsAndCommoditiesOH = this.ExternalPartsAndCommoditiesOH;
            destination.ManufacturingCostIncludingScrap = this.ManufacturingCostIncludingScrap;
            destination.ManufacturingOH = this.ManufacturingOH;
            destination.MarginOnManufacturingAndAssembly = this.MarginOnManufacturingAndAssembly;
            destination.MarginOnMaterialConsumablesAndCommodities = this.MarginOnMaterialConsumablesAndCommodities;
            destination.Name = this.Name;
            destination.OtherCostIncludingOH = this.OtherCostIncludingOH;
            destination.PackagingCostIncludingOH = this.PackagingCostIncludingOH;
            destination.PartNumber = this.PartNumber;
            destination.PaymentCost = this.PaymentCost;
            destination.Position = this.Position;
            destination.ProjectInvest = this.ProjectInvest;
            destination.RawMaterial = this.RawMaterial;
            destination.RawMaterialOH = this.RawMaterialOH;
            destination.Remark = this.Remark;
            destination.SalesAndAdministrationOH = this.SalesAndAdministrationOH;
            destination.CompanySurchargeOH = this.CompanySurchargeOH;
            destination.LogisticsIncludingOH = this.LogisticsIncludingOH;
            destination.Transport = this.Transport;
            destination.ManufacturingTransport = this.ManufacturingTransport;
            destination.ExternalWorkOH = this.ExternalWorkOH;
        }
    }
}
