﻿namespace ZPKTool.Gui.Reporting
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Windows;
    using System.Windows.Media.Imaging;
    using NPOI.HSSF.UserModel;
    using NPOI.SS.UserModel;
    using ZPKTool.Calculations.CostCalculation;
    using ZPKTool.Common;
    using ZPKTool.Data;
    using ZPKTool.Gui.Resources;
    using ZPKTool.Gui.Utils;

    /// <summary>
    /// Contains helper methods for reports generating logic.
    /// </summary>
    public static class ReportsHelper
    {
        /// <summary>
        /// Generates the file name of the report for a given entity
        /// </summary>
        /// <param name="entity">The entity for which to generate the report file name.</param>
        /// <param name="enhanced">A flag specifying whether the report is enhanced.</param>
        /// <returns>A string containing a valid file name that can be assigned to a report.</returns>
        public static string GenerateReportFileName(object entity, bool enhanced)
        {
            string fileName = string.Empty;
            Assembly assy = entity as Assembly;
            if (assy != null)
            {
                fileName = LocalizedResources.General_Assembly + "_" + assy.Name + (!string.IsNullOrEmpty(assy.Number) ? "_" + assy.Number : null) + (assy.Version != null ? "_v" + ZPKTool.Common.Formatter.FormatNumber(assy.Version) : null);
                if (enhanced)
                {
                    fileName = LocalizedResources.Reports_EReport + "_" + fileName;
                }
            }

            Part part = entity as Part;
            if (part != null)
            {
                fileName = LocalizedResources.General_Part + "_" + part.Name + (!string.IsNullOrEmpty(part.Number) ? "_" + part.Number : null) + (part.Version != null ? "_v" + ZPKTool.Common.Formatter.FormatNumber(part.Version) : null);
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                fileName = ZPKTool.Common.PathUtils.SanitizeFileName(fileName);
            }

            return fileName;
        }

        /// <summary>
        /// Checks if the name of the worksheet is valid with respect to what Excel expects to contain and fixes it by
        /// trimming it down to 31 characters and replacing the invalid characters with underscores.        
        /// </summary>
        /// <remarks>
        /// The worksheet name should have at most 31 characters and should not contain \ / ? * [ ]
        /// </remarks>
        /// <param name="sheetName">Name of the sheet.</param>
        /// <returns>The fixed worksheet name; if no fix was made it returns the input string.</returns>
        public static string ValidateWorksheetName(string sheetName)
        {
            if (string.IsNullOrEmpty(sheetName))
            {
                return sheetName;
            }

            string fixedName = sheetName;
            if (sheetName.Length > 31)
            {
                fixedName = sheetName.Substring(0, 31);
            }

            List<char> invalidChars = new List<char>() { '\\', '/', '?', '*', '[', ']', ':', '\'' };
            foreach (char invalidChar in invalidChars)
            {
                if (fixedName.Contains(invalidChar))
                {
                    fixedName = fixedName.Replace(invalidChar, '_');
                }
            }

            return fixedName;
        }

        /// <summary>
        /// Creates an enhanced cost breakdown by aggregating on the top-level calculation result all the data in the contained calculation result objects.
        /// For example, if the result is of an assembly it aggregates on the top the cost results of all sub-assemblies and parts in the assembly's graph.
        /// </summary>
        /// <param name="result">The calculation result from which to build the enhanced breakdown.</param>
        /// <returns>
        /// A new calculation result which contains the enhanced (aggregated) breakdown.
        /// Note that the returned calculation result retains the sub-structure of CalculationResult objects contained in the input result.
        /// </returns>
        public static CalculationResult CreateEnhancedCostBreakdown(CalculationResult result)
        {
            CalculationResult enhancedResult = result.Clone();
            enhancedResult.OverheadCost.OverheadSettings = null;
            enhancedResult.Summary.SubpartsAndSubassembliesCost = 0m;

            // Order the process step costs according to step's index.
            enhancedResult.ProcessCost.SortStepCosts(s => s.StepIndex);

            foreach (PartCost partCost in result.PartsCost.PartCosts)
            {
                AggregateCalculationResults(partCost.FullCalculationResult, enhancedResult, partCost.AmountPerAssembly);
            }

            foreach (AssemblyCost assyCost in result.AssembliesCost.AssemblyCosts)
            {
                CalculationResult assyEnhancedResult = CreateEnhancedCostBreakdown(assyCost.FullCalculationResult);
                AggregateCalculationResults(assyEnhancedResult, enhancedResult, assyCost.AmountPerAssembly);
            }

            return enhancedResult;
        }

        /// <summary>
        /// Aggregates the data from a calculation result into another calculation result.
        /// </summary>
        /// <param name="inputResult">The result to aggregate.</param>
        /// <param name="aggregator">The result into which to aggregate.</param>
        /// <param name="amount">The "amount" of inputResult that must be added to aggregator.</param>
        private static void AggregateCalculationResults(CalculationResult inputResult, CalculationResult aggregator, int amount)
        {
            // Aggregate the consumables, commodities, materials and investment
            aggregator.CommoditiesCost.Add(inputResult.CommoditiesCost);
            aggregator.ConsumablesCost.Add(inputResult.ConsumablesCost);
            aggregator.RawMaterialsCost.Add(inputResult.RawMaterialsCost);
            aggregator.InvestmentCost.Add(inputResult.InvestmentCost);

            // Aggregate the sub-assemblies and sub-parts costs
            aggregator.AssembliesCost.AddCosts(inputResult.AssembliesCost.AssemblyCosts);
            aggregator.PartsCost.AddCosts(inputResult.PartsCost.PartCosts.Where(c => !c.IsRawPart));

            // Aggregate the process data
            aggregator.ProcessCost.CommoditiesCost.Add(inputResult.ProcessCost.CommoditiesCost);
            aggregator.ProcessCost.ConsumablesCost.Add(inputResult.ProcessCost.ConsumablesCost);
            aggregator.ProcessCost.InvestmentCost.Add(inputResult.ProcessCost.InvestmentCost);

            int maxIndex = 0;
            if (inputResult.ProcessCost.StepCosts.Count > 0)
            {
                maxIndex = inputResult.ProcessCost.StepCosts.Max(c => c.StepIndex);
            }

            foreach (ProcessStepCost cost in inputResult.ProcessCost.StepCosts.OrderBy(c => c.StepIndex))
            {
                ProcessStepCost clone = cost.Clone();
                clone.StepIndex = ++maxIndex;
                aggregator.ProcessCost.AddStepCost(clone);
            }

            // External parts have their cost added to the sub-parts/sub-assemblies cost
            if (inputResult.External)
            {
                aggregator.Summary.SubpartsAndSubassembliesCost += inputResult.TotalCost * amount;
            }
            else
            {
                decimal targetCost = aggregator.Summary.TargetCost;

                // When a part's cost estimation is "Offer/External Calculation", the estimated cost is added directly into the production cost
                // and not spread over its breakdown components. The production cost is not displayed on the Result Details charts, which causes
                // this not to be displayed. To fix that we put the estimated cost into a separate component that is displayed on the chart.
                if (inputResult.CalculationAccuracy == ZPKTool.Data.PartCalculationAccuracy.OfferOrExternalCalculation)
                {
                    aggregator.Summary.OfferExternalCalcPartsCost += inputResult.Summary.ProductionCost * amount;
                }

                // Aggregate the summary data
                aggregator.Summary.Add(inputResult.Summary, amount);

                // Aggregate the overheads
                aggregator.OverheadCost.Add(inputResult.OverheadCost, amount);

                // The total cost is not aggregated
                aggregator.Summary.TargetCost = targetCost;
            }
        }

        /// <summary>
        /// Adds the bottom logo to part report.
        /// </summary>
        /// <param name="workbook">The workbook.</param>
        /// <param name="sheet">The sheet.</param>
        /// <param name="insertionColNum">The column where to insert the logo.</param>
        /// <param name="insertionRowNum">The row where to insert the logo.</param>
        public static void AddReportBottomLogo(IWorkbook workbook, ISheet sheet, int insertionColNum, int insertionRowNum)
        {
            try
            {
                byte[] pictureData;
                string fileExtension;
                GetReportLogo(out pictureData, out fileExtension);

                if (pictureData == null)
                {
                    return;
                }

                IRow row = sheet.GetRow(insertionRowNum);
                if (row != null)
                {
                    // Create logo picture in Excel row.
                    HSSFPatriarch patriarch = null;
                    if (sheet.DrawingPatriarch != null)
                    {
                        patriarch = (HSSFPatriarch)sheet.DrawingPatriarch;
                    }
                    else
                    {
                        patriarch = (HSSFPatriarch)sheet.CreateDrawingPatriarch();
                    }

                    // Set the picture type, depending on the file extension.
                    PictureType pictureType = PictureType.PNG;
                    if (fileExtension.Equals(".jpg", StringComparison.InvariantCultureIgnoreCase) ||
                        fileExtension.Equals(".jpeg", StringComparison.InvariantCultureIgnoreCase))
                    {
                        pictureType = PictureType.JPEG;
                    }

                    HSSFClientAnchor anchor =
                        new HSSFClientAnchor(0, 0, 0, 0, insertionColNum, insertionRowNum, insertionColNum, insertionRowNum);
                    anchor.AnchorType = 2;
                    int logoIndex = workbook.AddPicture(pictureData, pictureType);
                    HSSFPicture logo = (HSSFPicture)patriarch.CreatePicture(anchor, logoIndex);
                    logo.Resize();
                }
            }
            catch
            {
                // Ignore error because failure to add the logo should not break the report generation.
            }
        }

        /// <summary>
        /// Gets the report logo image read from the custom logo file path or, if it does not exist, from the default logo resource.
        /// </summary>
        /// <param name="pictureData">The picture data.</param>
        /// <param name="fileExtension">The extension of the image file.</param>
        public static void GetReportLogo(out byte[] pictureData, out string fileExtension)
        {
            Stream logoStream = null;
            pictureData = null;
            fileExtension = string.Empty;
            try
            {
                if (!string.IsNullOrWhiteSpace(UserSettingsManager.Instance.CustomLogoPath))
                {
                    // If the custom logo path is found, the stream is read from that file.
                    if (File.Exists(Path.Combine(ZPKTool.Common.Constants.ApplicationDataFolderPath, UserSettingsManager.Instance.CustomLogoPath)))
                    {
                        logoStream = File.Open(Path.Combine(ZPKTool.Common.Constants.ApplicationDataFolderPath, UserSettingsManager.Instance.CustomLogoPath), FileMode.Open);
                        fileExtension = Path.GetExtension(UserSettingsManager.Instance.CustomLogoPath);
                    }
                }
            }
            catch
            {
            }

            if (logoStream == null)
            {
                // If there is no custom logo file path found, the stream is read from resources.
                logoStream = Application.GetResourceStream(new Uri("Resources/Images/AppLogo.png", UriKind.Relative)).Stream;
                fileExtension = ".png";
            }

            if (logoStream != null && logoStream.Length > 0)
            {
                // Read the logo stream into a logo byte array.
                byte[] logoBytes = new byte[logoStream.Length];
                logoStream.Read(logoBytes, 0, logoBytes.Length);

                // Create a bitmap image from the logo bytes, resized to a desired logo width.
                BitmapImage bitmap = MediaUtils.CreateImageForDisplay(logoBytes, 100);

                using (MemoryStream outStream = new MemoryStream())
                {
                    // Create a memory stream from the bitmap image, to read the bytes from it into picture data.
                    BitmapEncoder encoder = new BmpBitmapEncoder();
                    encoder.Frames.Add(BitmapFrame.Create(bitmap));
                    encoder.Save(outStream);
                    pictureData = outStream.ToArray();
                }
            }

            if (logoStream != null)
            {
                logoStream.Dispose();
            }
        }
    }
}
