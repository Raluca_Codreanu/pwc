﻿using System;
using NPOI.SS.UserModel;
using ZPKTool.Data;

namespace ZPKTool.Gui.Reporting
{
    /// <summary>
    /// Extensions for the NPOI lib.
    /// </summary>
    public static class NPOIExtensions
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Sets the cell value.
        /// </summary>
        /// <param name="cell">The cell whose value will be set.</param>
        /// <param name="value">The value to set.</param>
        public static void SetCellValue(this ICell cell, decimal value)
        {
            cell.SetCellValue(Convert.ToDouble(value));
        }

        /// <summary>
        /// Sets the cell value.
        /// </summary>
        /// <param name="cell">The cell whose value will be set.</param>
        /// <param name="value">The value to set.</param>
        public static void SetCellValue(this ICell cell, decimal? value)
        {
            if (value.HasValue)
            {
                cell.SetCellValue(value.Value);
            }
        }

        /// <summary>
        /// Sets the cell value.
        /// </summary>
        /// <param name="cell">The cell whose value will be set.</param>
        /// <param name="value">The value to set.</param>
        public static void SetCellValue(this ICell cell, short? value)
        {
            if (value.HasValue)
            {
                cell.SetCellValue(value.Value);
            }
        }

        /// <summary>
        /// Sets the cell value.
        /// </summary>
        /// <param name="cell">The cell whose value will be set.</param>
        /// <param name="value">The value to set.</param>
        public static void SetCellValue(this ICell cell, int? value)
        {
            if (value.HasValue)
            {
                cell.SetCellValue(value.Value);
            }
        }

        /// <summary>
        /// Sets the cell value.
        /// </summary>
        /// <param name="cell">The cell whose value will be set.</param>
        /// <param name="value">The value to set.</param>
        public static void SetCellValue(this ICell cell, long? value)
        {
            if (value.HasValue)
            {
                cell.SetCellValue(value.Value);
            }
        }

        /// <summary>
        /// Sets the cell value.
        /// </summary>
        /// <param name="cell">The cell to set its value.</param>
        /// <param name="value">The value to set.</param>
        /// <remarks>Although an <see cref="object"/> is accepted as parameter, note that this will be converted to one
        /// of the already supported types and treated as is.</remarks>
        public static void SetCellValue(this ICell cell, object value)
        {
            if (value is bool)
            {
                cell.SetCellValue((bool)value);
                return;
            }

            if (value is DateTime)
            {
                cell.SetCellValue((DateTime)value);
                return;
            }

            if (value is decimal?)
            {
                cell.SetCellValue((decimal?)value);
                return;
            }

            if (value is double)
            {
                cell.SetCellValue((double)value);
                return;
            }

            if (value is short?)
            {
                cell.SetCellValue((short?)value);
                return;
            }

            if (value is int?)
            {
                cell.SetCellValue((int?)value);
                return;
            }

            if (value is long?)
            {
                cell.SetCellValue((long?)value);
                return;
            }

            if (value is Guid)
            {
                cell.SetCellValue(value.ToString());
                return;
            }

            IRichTextString richTextVal = value as IRichTextString;
            if (richTextVal != null)
            {
                cell.SetCellValue(richTextVal);
                return;
            }

            string stringVal = value as string;
            if (stringVal != null)
            {
                cell.SetCellValue(stringVal);
                return;
            }

            RawMaterialDeliveryType deliveryTypeVal = value as RawMaterialDeliveryType;
            if (deliveryTypeVal != null)
            {
                cell.SetCellValue(deliveryTypeVal.Name);
                return;
            }

            if (value != null)
            {
                log.Warn("Not supported type was passed to the Cell.SetCellValue() method. Type: {0}.", value.GetType().Name);
            }
        }
    }
}
