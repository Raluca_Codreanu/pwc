﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.Reporting
{
    /// <summary>
    /// The data used to fill an assembly table in the enhanced assembly report.
    /// </summary>
    public class EnhancedAssyReportTableData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EnhancedAssyReportTableData"/> class.
        /// </summary>
        public EnhancedAssyReportTableData()
        {
            this.SubAssembliesData = new Collection<EnhancedAssyReportRowData>();
            this.PartsAndCommoditiesData = new Collection<EnhancedAssyReportRowData>();
        }

        /// <summary>
        /// Gets or sets the level of the assembly whose data is stored in this table in an assembly tree.
        /// </summary>
        public int Level { get; set; }

        /// <summary>
        /// Gets or sets the data to fill the assembly table's header row (summary row, 1st row).
        /// </summary>
        public EnhancedAssyReportHeaderRowData HeaderData { get; set; }

        /// <summary>
        /// Gets or sets the data to fill the assembly table's assembly row (2nd row).
        /// </summary>
        public EnhancedAssyReportRowData AssyData { get; set; }

        /// <summary>
        /// Gets the data to fill the assembly table's sub-assemblies rows (from 3rd row on).
        /// </summary>        
        public Collection<EnhancedAssyReportRowData> SubAssembliesData { get; private set; }

        /// <summary>
        /// Gets the data to fill the assembly table's parts and commodities rows (from 3rd row on).
        /// </summary>
        public Collection<EnhancedAssyReportRowData> PartsAndCommoditiesData { get; private set; }
    }
}
