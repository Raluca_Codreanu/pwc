﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.Reporting
{
    /// <summary>
    /// The data used to fill the header row of an enhanced assembly report cost tables.
    /// </summary>
    public class EnhancedAssyReportHeaderRowData : EnhancedAssyReportRowData
    {
        /// <summary>
        /// Gets the "Total Price per Assy" column value.
        /// </summary>
        public override decimal TotalPricePerAssy
        {
            get { return this.PiecePrice; }
        }
    }
}
