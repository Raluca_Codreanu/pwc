﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Resources;
using iTextSharp.text;
using iTextSharp.text.pdf;
using NPOI.HPSF;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using ZPKTool.Business;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Utils;

namespace ZPKTool.Gui.Reporting
{
    /// <summary>
    /// Creates XLS and PDF reports.
    /// </summary>
    //// TODO: the methods of this class should not throw BusinessException, it should throw maybe a ReportingException (derived from UIException)
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.OrderingRules", "SA1204:StaticElementsMustAppearBeforeInstanceElements", Justification = "This is a large class so the methods are organized in regions by their purpose.")]
    public class ReportsManager
    {
        #region Private Members

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// A color for the headers in the regular report.
        /// </summary>
        private readonly Color pdfReportHeaderColor = new Color(157, 180, 213);

        /// <summary>
        /// A color for the headers in the regular report.
        /// </summary>
        private readonly Color pdfReportHeaderColorDark = new Color(67, 94, 139);

        /// <summary>
        /// A color for the headers in the enhanced report.
        /// </summary>
        private readonly Color pdfEnhancedReportHeaderColor = new Color(149, 179, 215);

        /// <summary>
        /// A color for the headers in the enhanced report.
        /// </summary>
        private readonly Color pdfEnhancedReportHeaderColorLight = new Color(184, 204, 228);

        /// <summary>
        /// A color for the row/cells representing an assembly in the enhanced report.
        /// </summary>
        private readonly Color pdfEnhancedReportSubassyColor = new Color(191, 191, 191);

        /// <summary>
        /// A color for the row/cells representing an assembly in the enhanced report.
        /// </summary>
        private readonly Color pdfEnhancedReportSubassyColorLight = new Color(216, 216, 216);

        /// <summary>
        /// The grey color used as cell background in some pdf reports.
        /// </summary>
        private readonly Color pdfGreyCellBackground = new Color(216, 216, 216);

        /// <summary>
        /// Small font for pdf reports.
        /// </summary>
        private readonly iTextSharp.text.Font pdfReportFontSmall = FontFactory.GetFont(ZPKTool.Common.Constants.PdfReportsFont, 8, iTextSharp.text.Font.NORMAL);

        /// <summary>
        /// Small and bold font for pdf reports.
        /// </summary>
        private readonly iTextSharp.text.Font pdfReportFontSmallBold = FontFactory.GetFont(ZPKTool.Common.Constants.PdfReportsFont, 8, iTextSharp.text.Font.BOLD);

        /// <summary>
        /// Normal font for pdf reports.
        /// </summary>
        private readonly iTextSharp.text.Font pdfReportFont = FontFactory.GetFont(ZPKTool.Common.Constants.PdfReportsFont, 10, iTextSharp.text.Font.NORMAL);

        /// <summary>
        /// Normal bold font for pdf reports.
        /// </summary>
        private readonly iTextSharp.text.Font pdfReportFontBold = FontFactory.GetFont(ZPKTool.Common.Constants.PdfReportsFont, 10, iTextSharp.text.Font.BOLD);

        /// <summary>
        /// Font for column headers in pdf reports.
        /// </summary>
        private readonly iTextSharp.text.Font pdfReportFontColumnHeader = FontFactory.GetFont(ZPKTool.Common.Constants.PdfReportsFont, 10, iTextSharp.text.Font.NORMAL, Color.WHITE);

        /// <summary>
        /// Bold font for column headers in pdf reports.
        /// </summary>
        private readonly iTextSharp.text.Font pdfReportFontColumnHeaderBold = FontFactory.GetFont(ZPKTool.Common.Constants.PdfReportsFont, 10, iTextSharp.text.Font.BOLD, Color.WHITE);

        /// <summary>
        /// Large bold font for pdf reports.
        /// </summary>
        private readonly iTextSharp.text.Font pdfReportFontLargeBold = FontFactory.GetFont(ZPKTool.Common.Constants.PdfReportsFont, 16, iTextSharp.text.Font.BOLD);

        /// <summary>
        /// The default symbol to display in an empty cell.
        /// </summary>
        private readonly string emptyCellSymbol = "-";

        /// <summary>
        /// A value indicating the database that will be used by the manager to retrieve the pictures of exported objects. 
        /// </summary>
        private DbIdentifier databaseId;

        /// <summary>
        /// A value indicating the model browser helper that will be used by the manager to retrieve the pictures of exported objects. 
        /// </summary>
        private IModelBrowserHelperService modelBrowserHelper;

        /// <summary>
        /// The measurement units adapter.
        /// </summary>
        private UnitsAdapter measurementUnitsAdapter;

        #endregion Private Members

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportsManager"/> class.
        /// </summary>
        public ReportsManager()
        {
            this.databaseId = DbIdentifier.NotSet;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportsManager"/> class (when the user is not in viewer mode).
        /// </summary>
        /// <param name="databaseId">A value indicating the database that will be used by the manager to retrieve the pictures of exported objects.</param>
        /// <param name="measurementUnitsAdapter">The measurement units adapter</param>
        public ReportsManager(DbIdentifier databaseId, UnitsAdapter measurementUnitsAdapter)
        {
            this.databaseId = databaseId;
            this.measurementUnitsAdapter = measurementUnitsAdapter;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportsManager"/> class (when the user is in viewer mode).
        /// </summary>
        /// <param name="modelBrowserHelper">A value indicating the model browser helper that will be used by the manager to retrieve the pictures of exported objects.</param>
        /// <param name="measurementUnitsAdapter">The measurement units adapter</param>
        public ReportsManager(IModelBrowserHelperService modelBrowserHelper, UnitsAdapter measurementUnitsAdapter)
        {
            this.modelBrowserHelper = modelBrowserHelper;
            this.databaseId = DbIdentifier.NotSet;
            this.measurementUnitsAdapter = measurementUnitsAdapter;
        }

        #region Generic Reports

        /// <summary>
        /// Exports generic data to an excel worksheet.
        /// </summary>
        /// <param name="columnHeaders">The column headers.</param>
        /// <param name="data">The data to export.</param>
        /// <param name="filePath">The path to the xls file.</param>
        /// <param name="author">The string to which to set the Author property of the xls document that will be created.</param>
        /// <param name="additionalReportInformation">The additional report information.</param>
        public void GenericExportToXls(
            StringCollection columnHeaders,
            List<StringCollection> data,
            string filePath,
            string author,
            ExtendedDataGridAdditionalReportInformation additionalReportInformation)
        {
            try
            {
                // Add the author, date and currency to additionalReportInformation
                additionalReportInformation.Author = LocalizedResources.Report_Author + ": " + (SecurityManager.Instance.CurrentUser != null ? SecurityManager.Instance.CurrentUser.Name : string.Empty);
                additionalReportInformation.Date = LocalizedResources.Report_Date + " " + DateTime.Now;
                additionalReportInformation.Currency = LocalizedResources.Report_Currency + " " + this.GetUICurrencySymbol();

                // Create the xls workbook an 1 sheet
                HSSFWorkbook workbook = new HSSFWorkbook();

                if (!string.IsNullOrWhiteSpace(author))
                {
                    // Create a entry of SummaryInformation
                    SummaryInformation summaryInfo = PropertySetFactory.CreateSummaryInformation();
                    summaryInfo.Author = author;
                    workbook.SummaryInformation = summaryInfo;
                }

                ISheet sheet = workbook.CreateSheet();
                int rowIndex = 0;
                IRow crtRow;
                IFont cellFont = workbook.CreateFont();
                cellFont.Boldweight = (short)FontBoldWeight.BOLD;
                ICellStyle cellStyle = workbook.CreateCellStyle();
                cellStyle.SetFont(cellFont);

                // Add empty rows in the sheet to later add the additional report information, after setting the columns width.
                Type additionalReportInformationType = additionalReportInformation.GetType();
                foreach (System.Reflection.PropertyInfo property in additionalReportInformationType.GetProperties())
                {
                    crtRow = sheet.CreateRow(rowIndex);
                    rowIndex++;
                }

                // Add the headers in the sheet
                int columnNo = 0;
                rowIndex++;
                crtRow = sheet.CreateRow(rowIndex);
                foreach (string header in columnHeaders)
                {
                    ICell cell = crtRow.CreateCell(columnNo);
                    cell.CellStyle = cellStyle;
                    cell.SetCellValue(header);
                    columnNo++;
                }

                // Add the data in the sheet
                rowIndex++;
                foreach (StringCollection rowData in data)
                {
                    int columnIndex = 0;
                    crtRow = sheet.CreateRow(rowIndex);
                    foreach (string cellContent in rowData)
                    {
                        ICell cell = crtRow.CreateCell(columnIndex);

                        decimal number;
                        if (decimal.TryParse(cellContent, out number))
                        {
                            cell.SetCellValue(number);
                        }
                        else
                        {
                            cell.SetCellValue(cellContent);
                        }

                        columnIndex++;
                    }

                    rowIndex++;
                }

                // Set the width of the sheet's columns to auto
                for (int i = 0; i < columnNo; i++)
                {
                    sheet.AutoSizeColumn(i);
                }

                // Add the additional report information at the beginning of the sheet.
                rowIndex = 0;
                foreach (System.Reflection.PropertyInfo property in additionalReportInformationType.GetProperties())
                {
                    crtRow = sheet.CreateRow(rowIndex);
                    ICell cell = crtRow.CreateCell(0);
                    cell.SetCellValue(property.GetValue(additionalReportInformation, null).ToString());
                    rowIndex++;
                }

                using (FileStream file = new FileStream(filePath, FileMode.Create))
                {
                    workbook.Write(file);
                }
            }
            catch (Exception ex)
            {
                log.ErrorException("Error while exporting generic data to Excel.", ex);
                throw new BusinessException(ErrorCodes.ExcelReportGenerationUnknownError, ex);
            }
        }

        /// <summary>
        /// Exports generic data to a pdf document.
        /// </summary>
        /// <param name="columnHeaders">The column headers.</param>
        /// <param name="data">The data to export.</param>
        /// <param name="path">The path where to save the pdf file.</param>
        /// <param name="author">The string to which to set the Author property of the xls document that will be created.</param>
        /// <param name="additionalReportInformation">The additional report information.</param>
        public void GenericExportToPdf(
            StringCollection columnHeaders,
            List<StringCollection> data,
            string path,
            string author,
            ExtendedDataGridAdditionalReportInformation additionalReportInformation)
        {
            // Add the author, date and currency to additionalReportInformation
            additionalReportInformation.Author = LocalizedResources.Report_Author + ": " + (SecurityManager.Instance.CurrentUser != null ? SecurityManager.Instance.CurrentUser.Name : string.Empty);
            additionalReportInformation.Date = LocalizedResources.Report_Date + " " + DateTime.Now;
            additionalReportInformation.Currency = LocalizedResources.Report_Currency + " " + this.GetUICurrencySymbol();

            // Create a document-object
            Document document = new Document();

            try
            {
                // Create a writer that listens to the document and directs a PDF-stream to a file.
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(path, FileMode.Create));
                document.Open();
                document.AddAuthor(author);

                PdfPTable pdfTable = new PdfPTable(columnHeaders.Count);
                pdfTable.WidthPercentage = 100;

                // Add the additional report information in the document
                Type additionalReportInformationType = additionalReportInformation.GetType();
                foreach (System.Reflection.PropertyInfo property in additionalReportInformationType.GetProperties())
                {
                    Phrase phrase = new Phrase(property.GetValue(additionalReportInformation, null).ToString() + Environment.NewLine, pdfReportFontSmall);
                    document.Add(phrase);
                }

                // Add Header of the pdf table  
                foreach (string header in columnHeaders)
                {
                    PdfPCell pdfPCell = new PdfPCell(new Phrase(header, pdfReportFontSmallBold));
                    pdfTable.AddCell(pdfPCell);
                }

                // Add the data in the table
                foreach (StringCollection rowData in data)
                {
                    if (rowData.Count > 1 || columnHeaders.Count == 1)
                    {
                        foreach (string cellContent in rowData)
                        {
                            PdfPCell pdfPCell = new PdfPCell(new Phrase(cellContent, pdfReportFontSmall));
                            pdfTable.AddCell(pdfPCell);
                        }
                    }
                    else if (rowData.Count == 1)
                    {
                        pdfTable.AddCell(new PdfPCell(new Phrase(rowData[0].ToString(), pdfReportFontSmall))
                        {
                            Colspan = 3,
                            Border = Table.NO_BORDER
                        });
                        pdfTable.DefaultCell.Border = Table.NO_BORDER;
                        pdfTable.CompleteRow();
                        pdfTable.DefaultCell.Border = Table.BOTTOM_BORDER | Table.LEFT_BORDER | Table.RIGHT_BORDER | Table.TOP_BORDER;
                    }
                    else
                    {
                        pdfTable.DefaultCell.Border = Table.NO_BORDER;
                        pdfTable.AddCell(new Phrase(" "));
                        pdfTable.CompleteRow();
                        pdfTable.DefaultCell.Border = Table.BOTTOM_BORDER | Table.LEFT_BORDER | Table.RIGHT_BORDER | Table.TOP_BORDER;
                    }
                }

                // Add pdf table to the document  
                document.Add(pdfTable);
            }
            catch (DocumentException docEx)
            {
                log.ErrorException("Error while exporting data grid to PDF.", docEx);
                throw new BusinessException(ErrorCodes.PdfReportGenerationUnknownError, docEx);
            }
            catch (IOException ex)
            {
                log.ErrorException("IO exception while exporting a pdf report.", ex);
                throw new BusinessException(ErrorCodes.PdfReportGenerationUnknownError, ex);
            }
            catch (Exception ex)
            {
                log.ErrorException("Error while exporting data grid to PDF.", ex);
                throw new BusinessException(ErrorCodes.PdfReportGenerationUnknownError, ex);
            }
            finally
            {
                // Close document and writer  
                document.Close();
            }
        }

        #endregion Generic Reports

        #region Part Report Xls

        /// <summary>
        /// Create a part report in xls format.
        /// </summary>
        /// <param name="part">The part to export.</param>
        /// <param name="result">The cost of the part.</param>
        /// <param name="path">The path where to save the excel file on the disk.</param>
        /// <exception cref="ArgumentNullException">the part or result parameters is null.</exception>
        /// <exception cref="ArgumentException">The path is null or empty</exception>
        public void CreatePartReportXls(
            Part part,
            CalculationResult result,
            string path)
        {
            if (part == null)
            {
                throw new ArgumentNullException("part", "The part was null.");
            }

            if (result == null)
            {
                throw new ArgumentNullException("result", "The part cost calculation result was null.");
            }

            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentException("The file path was null or empty", "path");
            }

            try
            {
                HSSFWorkbook workbook = null;
                using (Stream templateStream = new MemoryStream(LocalizedResources.RegularPartReportTemplate))
                {
                    if (templateStream == null)
                    {
                        log.Error("The part report xls template was null.");
                    }

                    workbook = new HSSFWorkbook(templateStream);
                }

                ISheet sheet = workbook.GetSheetAt(0);
                if (sheet != null)
                {
                    workbook.SetSheetName(0, ReportsHelper.ValidateWorksheetName(part.Name));

                    // Fill the generic info (calculator, date, etc.) and the Part Data section
                    FillPartReportGenericAndPartData(sheet, part, result);

                    // Fill part picture      
                    Media picture = this.GetPictureForReport(part);
                    if (picture != null)
                    {
                        AddReportPicture(workbook, sheet, picture, 4, 2, 19, 5);
                    }

                    // Fill the Weight column header for bill of materials
                    var weightLabel = LocalizedResources.General_Weight;

                    // Append the symbol to the weight label if have any
                    if (this.measurementUnitsAdapter != null && !string.IsNullOrWhiteSpace(this.measurementUnitsAdapter.UIWeight.Symbol))
                    {
                        weightLabel = string.Format("{0} ({1})", weightLabel, this.measurementUnitsAdapter.UIWeight.Symbol);
                    }

                    sheet.GetRow(4).GetCell(23).SetCellValue(weightLabel);

                    // Fill the Bill of Materials section
                    int bomLastInsertedRow = FillPartReportBOM(sheet, part, result);

                    // Create the process steps icons/shapes
                    int stepIconsInsertionRowNum = bomLastInsertedRow == -1 ? 24 : bomLastInsertedRow + 3;
                    int stepsIconsLastInsertedRow = FillPartReportManufacturingStepsTable(workbook, sheet, part, stepIconsInsertionRowNum);

                    // Fill the process steps data table
                    int stepDataFirstInsertionRowNum = stepsIconsLastInsertedRow + 3;
                    FillPartReportManufacturingSummaryTable(sheet, part, result, stepDataFirstInsertionRowNum);

                    // Add the bottom logo
                    ReportsHelper.AddReportBottomLogo(workbook, sheet, 9, sheet.LastRowNum - 5);
                }

                // Save the report
                using (FileStream file = new FileStream(path, FileMode.Create))
                {
                    workbook.Write(file);
                }
            }
            catch (IOException ex)
            {
                throw new BusinessException(ErrorCodes.ReportGenerationIOError, ex);
            }
            catch (UnauthorizedAccessException ex)
            {
                throw new BusinessException(FileErrorCode.UnauthorizedAccess, ex);
            }
            catch (Exception ex)
            {
                log.ErrorException("Error while creating the part xls report.", ex);
                throw new BusinessException(ErrorCodes.ExcelReportGenerationUnknownError, ex);
            }
        }

        /// <summary>
        /// Gets the media picture from an entity used in reports
        /// </summary>
        /// <param name="entity">The entity</param>
        /// <returns>The media picture if exists or null if not</returns>
        private Media GetPictureForReport(IEntityWithMediaCollection entity)
        {
            if (entity == null)
            {
                return null;
            }

            Media picture = null;
            if (this.databaseId != DbIdentifier.NotSet)
            {
                MediaManager mediaManager = new MediaManager(DataAccessFactory.CreateDataSourceManager(this.databaseId));
                picture = mediaManager.GetPicture(entity);
            }
            else if (this.modelBrowserHelper != null)
            {
                var mediaFromEntity = modelBrowserHelper.GetMediaForEntity(entity);
                var pictureImported = mediaFromEntity.FirstOrDefault(item => (MediaType)item.Type == MediaType.Image);
                if (pictureImported != null)
                {
                    try
                    {
                        if (File.Exists(pictureImported.CachedFilePath))
                        {
                            picture = new Media();
                            picture.Type = (short)MediaType.Image;
                            picture.Content = File.ReadAllBytes(pictureImported.CachedFilePath);
                        }
                    }
                    catch (IOException ex)
                    {
                        log.ErrorException("Error while reading the picture from disk", ex);
                        picture = null;
                    }
                }
            }
            else
            {
                picture = entity.Media.FirstOrDefault(m => m.Type == (short)MediaType.Image);
            }

            return picture;
        }

        /// <summary>
        /// Fill the sheet with generic information (Date, current user, currency) and fill the Part Data section.
        /// </summary>
        /// <param name="sheet">The excel sheet to fill.</param>
        /// <param name="part">The part for which the report is being created.</param>
        /// <param name="result">The cost calculation result for the part.</param>
        private void FillPartReportGenericAndPartData(ISheet sheet, Part part, CalculationResult result)
        {
            // Fill the calculator name
            if (SecurityManager.Instance.CurrentUser != null)
            {
                sheet.GetRow(1).GetCell(14).SetCellValue(SecurityManager.Instance.CurrentUser.Name);
            }

            // Fill the version
            sheet.GetRow(1).GetCell(22).SetCellValue(part.CalculationVariant);

            // Fill the report generation date
            sheet.GetRow(1).GetCell(27).SetCellValue(DateTime.Now.ToString("dd MMMM yyyy"));

            // Fill the used currency
            sheet.GetRow(48).GetCell(1).SetCellValue(this.GetUICurrencySymbol());

            // Fill the Part Data section, by column, from right to left
            sheet.GetRow(4).GetCell(8).SetCellValue(part.Name);

            string location = (part.ManufacturingCountry + ", " + part.ManufacturingSupplier).TrimEnd(',', ' ');
            sheet.GetRow(6).GetCell(8).SetCellValue(location);

            sheet.GetRow(8).GetCell(8).SetCellValue(part.YearlyProductionQuantity);
            sheet.GetRow(9).GetCell(8).SetCellValue(part.LifeTime);

            ICostCalculator calculator = CostCalculatorFactory.GetCalculator(part.CalculationVariant);
            var lifetimeProdQty = calculator.CalculateNetLifetimeProductionQuantity(part);
            sheet.GetRow(10).GetCell(8).SetCellValue(lifetimeProdQty);

            sheet.GetRow(12).GetCell(8).SetCellValue(result.ProcessCost.MachineCostSum);
            sheet.GetRow(13).GetCell(8).SetCellValue(result.ProcessCost.SetupCostSum);
            sheet.GetRow(14).GetCell(8).SetCellValue(result.ProcessCost.DirectLabourCostSum);
            sheet.GetRow(15).GetCell(8).SetCellValue(result.ProcessCost.ToolAndDieCostSum);
            sheet.GetRow(16).GetCell(8).SetCellValue(result.ProcessCost.DiesMaintenanceCostSum);
            sheet.GetRow(17).GetCell(8).SetCellValue(result.ProcessCost.ManufacturingOverheadSum);
            sheet.GetRow(18).GetCell(8).SetCellValue(result.ProcessCost.RejectCostSum);
            sheet.GetRow(19).GetCell(8).SetCellValue(result.ProcessCost.ManufacturingTransportCost);
            sheet.GetRow(21).GetCell(8).SetCellValue(result.ProcessCost.TotalManufacturingCostSum);

            PartCalculationAccuracy calcAccuracy =
                (PartCalculationAccuracy)part.CalculationAccuracy.GetValueOrDefault(PartCalculationAccuracy.FineCalculation);
            sheet.GetRow(6).GetCell(16).SetCellValue(UIUtils.GetEnumValueLabel(calcAccuracy));

            // Fill calculated information
            sheet.GetRow(8).GetCell(16).SetCellValue(result.Summary.RawMaterialCost);
            sheet.GetRow(9).GetCell(16).SetCellValue(result.Summary.CommoditiesCost);
            sheet.GetRow(10).GetCell(16).SetCellValue(result.Summary.ConsumableCost);
            sheet.GetRow(11).GetCell(16).SetCellValue(result.Summary.TotalManufacturingCost);
            sheet.GetRow(12).GetCell(16).SetCellValue(result.Summary.ManufacturingRejectCost);
            sheet.GetRow(13).GetCell(16).SetCellValue(result.Summary.ManufacturingTransportCost);
            sheet.GetRow(14).GetCell(16).SetCellValue(result.Summary.DevelopmentCost + result.Summary.ProjectInvest);
            sheet.GetRow(15).GetCell(16).SetCellValue(result.Summary.OtherCost);
            sheet.GetRow(16).GetCell(16).SetCellValue(result.Summary.PackagingCost);
            sheet.GetRow(17).GetCell(16).SetCellValue(result.Summary.TransportCost + result.Summary.LogisticCost);
            sheet.GetRow(18).GetCell(16).SetCellValue(result.Summary.OverheadAndMarginCost);
            sheet.GetRow(19).GetCell(16).SetCellValue(result.Summary.PaymentCost);
            sheet.GetRow(21).GetCell(16).SetCellValue(result.Summary.TargetCost);

            // Fill the cost data in the bottom right of the report sheet, below all tables
            var targetPrice = CurrencyConversionManager.ConvertBaseValueToDisplayValue(part.TargetPrice.GetValueOrDefault(), this.measurementUnitsAdapter.BaseCurrency.ExchangeRate, this.measurementUnitsAdapter.UICurrency.ConversionFactor);
            sheet.GetRow(50).GetCell(29).SetCellValue(targetPrice);
            sheet.GetRow(52).GetCell(29).SetCellValue(result.Summary.TargetCost);
        }

        /// <summary>
        /// Fill Bill of Materials table for the regular part report.
        /// </summary>
        /// <param name="sheet">The excel sheet to fill.</param>
        /// <param name="part">The Part object to get the data from.</param>
        /// <param name="partCalculationResult">The cost calculation result for the part.</param>
        /// <returns>The number of the last row inserted in the sheet by this method or -1 if no row was inserted.</returns>
        private int FillPartReportBOM(ISheet sheet, Part part, CalculationResult partCalculationResult)
        {
            // Create a list of homogeneous anonymous data objects by extracting the data that needs to be put in the Bill Of Materials report section
            // from the different types of cost objects.
            var bomData = partCalculationResult.CommoditiesCost.CommodityCosts.OrderBy(c => c.Name)
                .Select(cost => new
                {
                    Name = cost.Name,
                    Weight = cost.Weight,
                    Price = (decimal?)cost.Price,
                    Scrap = (decimal?)null,
                    ScrapRefund = (decimal?)null,
                    Loss = (decimal?)null,
                    NetCost = (decimal?)cost.Cost,
                    Overhead = (decimal?)cost.Overhead,
                    TotalCost = (decimal?)cost.TotalCost
                }).ToList();

            foreach (var cost in partCalculationResult.RawMaterialsCost.RawMaterialCosts.OrderBy(c => c.Name))
            {
                // Adjust the price of the raw material according to its weight if possible
                decimal price = this.AdjustRawMaterialPriceToWeightUnit(part, cost);

                var materialData = new
                    {
                        Name = cost.Name,
                        Weight = (decimal?)cost.PartWeight,
                        Price = (decimal?)price,
                        Scrap = (decimal?)cost.ScrapCost,
                        ScrapRefund = (decimal?)cost.ScrapRefund,
                        Loss = (decimal?)cost.LossCost,
                        NetCost = (decimal?)cost.NetCost,
                        Overhead = (decimal?)cost.Overhead,
                        TotalCost = (decimal?)cost.TotalCost
                    };

                bomData.Add(materialData);
            }

            var consumableData = partCalculationResult.ConsumablesCost.ConsumableCosts.OrderBy(c => c.Name)
                .Select(cost => new
                {
                    Name = cost.Name,
                    Weight = (decimal?)null,
                    Price = (decimal?)cost.Price,
                    Scrap = (decimal?)null,
                    ScrapRefund = (decimal?)null,
                    Loss = (decimal?)null,
                    NetCost = (decimal?)cost.Cost,
                    Overhead = (decimal?)cost.Overhead,
                    TotalCost = (decimal?)cost.TotalCost
                });
            bomData.AddRange(consumableData);

            // The row number of the last row in the BOM table. After this row is filled new rows have to be inserted.
            int bomTableLastRowNum = 21;

            // The current row to fill with a material, commodity or consumable. The initial value is the row at which the BOM table starts.
            int rowNum = 5;

            // The number of the last row inserted in the sheet.
            int lastInsertedRowNum = -1;

            // Add the collected BOM data into the report
            foreach (var data in bomData)
            {
                // If there are empty rows in the BOM table select the next one, otherwise insert a new one
                IRow currentRow = null;
                if (rowNum > bomTableLastRowNum)
                {
                    // If the BOM table is full insert a new row under the last one and copy its style.
                    currentRow = CopyRow(sheet, bomTableLastRowNum, 18, 32, sheet, rowNum, false);
                    currentRow.CreateCell(1).CellStyle = sheet.GetRow(bomTableLastRowNum).GetCell(1).CellStyle;
                    currentRow.Height = sheet.GetRow(bomTableLastRowNum).Height;
                    lastInsertedRowNum = rowNum;
                }
                else
                {
                    currentRow = sheet.GetRow(rowNum);
                }

                currentRow.GetCell(20).SetCellValue(data.Name);
                currentRow.GetCell(23).SetCellValue(data.Weight.HasValue ? (object)data.Weight.Value : this.emptyCellSymbol);
                currentRow.GetCell(24).SetCellValue(data.Price.HasValue ? (object)data.Price.Value : this.emptyCellSymbol);
                currentRow.GetCell(25).SetCellValue(data.Scrap.HasValue ? (object)data.Scrap.Value : this.emptyCellSymbol);
                currentRow.GetCell(26).SetCellValue(data.ScrapRefund.HasValue ? (object)data.ScrapRefund.Value : this.emptyCellSymbol);
                currentRow.GetCell(27).SetCellValue(data.Loss.HasValue ? (object)data.Loss.Value : this.emptyCellSymbol);
                currentRow.GetCell(28).SetCellValue(data.NetCost.HasValue ? (object)data.NetCost.Value : this.emptyCellSymbol);
                currentRow.GetCell(29).SetCellValue(data.Overhead.HasValue ? (object)data.Overhead.Value : this.emptyCellSymbol);
                currentRow.GetCell(30).SetCellValue(data.TotalCost.HasValue ? (object)data.TotalCost.Value : this.emptyCellSymbol);

                rowNum++;
            }

            return lastInsertedRowNum;
        }

        /// <summary>
        /// Fill the Manufacturing Steps table of the part report.
        /// </summary>
        /// <param name="workbook">The sheet's workbook.</param>
        /// <param name="sheet">The excel sheet to fill.</param>
        /// <param name="part">The Part object to get the data from.</param>
        /// <param name="rowNum">The row num.</param>
        /// <returns>The number of the last row inserted in the sheet by this method or -1 if no row was inserted.</returns>
        private int FillPartReportManufacturingStepsTable(IWorkbook workbook, ISheet sheet, Part part, int rowNum)
        {
            int lastInsertedRowNum = rowNum;

            var partCalcAccuracy = (PartCalculationAccuracy)part.CalculationAccuracy.GetValueOrDefault(PartCalculationAccuracy.FineCalculation);
            if (part.Process.Steps.Count == 0 ||
                 partCalcAccuracy != PartCalculationAccuracy.FineCalculation)
            {
                return lastInsertedRowNum;
            }

            HSSFPatriarch patriarch = null;
            if (sheet.DrawingPatriarch != null)
            {
                patriarch = (HSSFPatriarch)sheet.DrawingPatriarch;
            }
            else
            {
                patriarch = (HSSFPatriarch)sheet.CreateDrawingPatriarch();
            }

            // The font used for the step name and index
            IFont font = workbook.CreateFont();
            font.FontHeightInPoints = 8;

            // The anchor and shape group for the initial steps row
            HSSFClientAnchor anchor = new HSSFClientAnchor(0, 0, 1023, 255, 2, rowNum, 30, rowNum);
            HSSFShapeGroup mainGroup = patriarch.CreateGroup(anchor);
            mainGroup.SetCoordinates(0, -25, 1023, 230);

            int stepShapeWidth = 80;
            int maxStepShapesPerRow = 11;
            int currentStepNumber = 1;
            int shapeOffsetX = 0;
            foreach (ProcessStep step in part.Process.Steps.OrderBy(ps => ps.Index))
            {
                if (currentStepNumber > 1 && currentStepNumber % maxStepShapesPerRow == 1)
                {
                    // Add a new row for step shapes if the current step doesn't fit in the step shapes row anymore
                    CopyRow(sheet, rowNum, sheet, ++rowNum, false);
                    IRow newRow = sheet.GetRow(rowNum);
                    newRow.Height = sheet.GetRow(rowNum - 1).Height;

                    lastInsertedRowNum = rowNum;
                    shapeOffsetX = 0;

                    // Create an anchor and shape group for the new row
                    anchor = new HSSFClientAnchor(0, 0, 1023, 255, 2, rowNum, 30, rowNum);
                    mainGroup = patriarch.CreateGroup(anchor);
                    mainGroup.SetCoordinates(0, -25, 1023, 230);
                }

                CreateProcessStepShape(mainGroup, stepShapeWidth, shapeOffsetX, step.Name, currentStepNumber.ToString(), font);

                shapeOffsetX += stepShapeWidth + 10;
                currentStepNumber++;
            }

            return lastInsertedRowNum;
        }

        /// <summary>
        /// Fill part of the sheet with Manufacturing summary
        /// </summary>
        /// <param name="sheet">The excel sheet to fill.</param>
        /// <param name="part">The Part object to get the data from.</param>
        /// <param name="result">The part calculation result.</param>
        /// <param name="rowNum">The number of the row where to start inserting step data.</param>
        /// <returns>The number of the last row inserted in the sheet by this method or -1 if no row was inserted.</returns>
        private int FillPartReportManufacturingSummaryTable(ISheet sheet, Part part, CalculationResult result, int rowNum)
        {
            if (part.Process.Steps.Count == 0)
            {
                return -1;
            }

            // The number of rows available for steps by default. New rows should be added if the number of steps is larger than this.
            int defaultRowsNumber = 20;

            int lastInsertedRowNum = -1;
            int stepNumber = 1;
            foreach (ProcessStepCost stepCost in result.ProcessCost.StepCosts.OrderBy(cost => cost.StepIndex))
            {
                if (stepNumber > defaultRowsNumber)
                {
                    // Add a new row if the default ones have been occupied
                    CopyRow(sheet, rowNum - 1, sheet, rowNum, false);
                    lastInsertedRowNum = rowNum;
                }

                IRow currentRow = sheet.GetRow(rowNum);

                // Fill process step information
                currentRow.GetCell(1).SetCellValue(stepNumber);
                currentRow.GetCell(3).SetCellValue(stepCost.StepName);
                currentRow.GetCell(27).SetCellValue(stepCost.IsExternal ? LocalizedResources.General_Yes : LocalizedResources.General_No);

                // Fill process step cost info
                currentRow.GetCell(8).SetCellValue(stepCost.MachineCost);
                currentRow.GetCell(13).SetCellValue(stepCost.SetupCost);
                currentRow.GetCell(16).SetCellValue(stepCost.DirectLabourCost);
                currentRow.GetCell(17).SetCellValue(stepCost.ToolAndDieCost);
                currentRow.GetCell(20).SetCellValue(stepCost.DiesMaintenanceCost);
                currentRow.GetCell(21).SetCellValue(stepCost.ManufacturingCost);
                currentRow.GetCell(22).SetCellValue(stepCost.ManufacturingOverheadCost);
                currentRow.GetCell(24).SetCellValue(stepCost.RejectCost);
                currentRow.GetCell(26).SetCellValue(stepCost.TransportCost);
                currentRow.GetCell(30).SetCellValue(stepCost.TotalManufacturingCost);

                ProcessStep step = part.Process.Steps.FirstOrDefault(ps => ps.Guid == stepCost.StepId);
                if (step != null)
                {
                    currentRow.GetCell(28).SetCellValue(step.CycleTime);
                    currentRow.GetCell(29).SetCellValue(step.PartsPerCycle);

                    var machines = step.Machines.Where(m => !m.IsDeleted).OrderBy(m => m.Index);
                    int machinesCount = machines.Count();
                    for (int i = 0; i < machinesCount; i++)
                    {
                        if (i > 0)
                        {
                            // Add a row for each machine following the 1st one.
                            CopyRow(sheet, rowNum, sheet, ++rowNum, false);
                            lastInsertedRowNum = rowNum;
                        }

                        Machine machine = machines.ElementAt(i);
                        sheet.GetRow(rowNum).GetCell(5).SetCellValue(machine.Name);
                        sheet.GetRow(rowNum).GetCell(9).SetCellValue(machine.Availability);
                        sheet.GetRow(rowNum).GetCell(11).SetCellValue(machine.FullLoadRate);
                    }
                }

                stepNumber++;
                rowNum++;
            }

            return lastInsertedRowNum;
        }

        #endregion Part Report Xls

        #region Part Report Pdf

        /// <summary>
        /// Generate the Part Report in the PDF form template.
        /// </summary>
        /// <param name="part">The part object to export.</param>
        /// <param name="result">The cost calculation to export.</param>
        /// <param name="path">The path where to save the excel file on the disk.</param>
        public void CreatePartReportPdf(
            Part part,
            CalculationResult result,
            string path)
        {
            MemoryStream memoryStream = null;
            try
            {
                memoryStream = new MemoryStream();
                float pageMargin = 10f;
                Document document = new Document(new Rectangle(1355, 897), pageMargin, pageMargin, pageMargin, pageMargin);
                PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
                document.Open();

                // fill generic part info
                FillPDFReportGenericInformation(document, part);

                // fill part data and bill of materials
                FillPartPDFReportPartDataAndBOM(document, part, result);

                // fill process steps
                PdfPTable processStepsTable = new PdfPTable(1);
                processStepsTable.WidthPercentage = 100;
                processStepsTable.AddCell(new PdfPCell(new Phrase(LocalizedResources.Report_ManufacturingSteps, pdfReportFontColumnHeaderBold))
                {
                    Colspan = processStepsTable.NumberOfColumns,
                    BackgroundColor = pdfReportHeaderColor,
                    HorizontalAlignment = Table.ALIGN_CENTER
                });
                processStepsTable.DefaultCell.Padding = 0;
                processStepsTable.DefaultCell.Border = Table.NO_BORDER;

                var partCalcAccuracy = (PartCalculationAccuracy)part.CalculationAccuracy.GetValueOrDefault(PartCalculationAccuracy.FineCalculation);
                if (partCalcAccuracy == PartCalculationAccuracy.FineCalculation)
                {
                    FillPDFReportProcessSteps(processStepsTable, part.Process, part.GetType(), 10);
                }
                else
                {
                    // add empty row (adding whitespace so that the table row has height)
                    processStepsTable.AddCell(new PdfPCell(new Phrase(" "))
                    {
                        Border = Table.LEFT_BORDER | Table.RIGHT_BORDER
                    });
                }

                document.Add(processStepsTable);

                // fill manufacturing summary
                FillPartPDFReportManufacturingSummary(document, part, result);

                // fill price data
                FillPDFReportPriceData(document, part.TargetPrice, result);

                document.Close();
                File.WriteAllBytes(path, memoryStream.GetBuffer());
            }
            catch (Exception ex)
            {
                log.ErrorException("Error while exporting Part report to PDF.", ex);
            }
            finally
            {
                if (memoryStream != null)
                {
                    memoryStream.Close();
                }
            }
        }

        /// <summary>
        /// Fill part of the pdf document with Part Data calculation information and with Bill of Commodities and RawMaterials information
        /// </summary>
        /// <param name="document">The PDF document to fill.</param>
        /// <param name="part">The part for which to fill the report.</param>
        /// <param name="partCalculationResult">Calculation result for the part.</param>
        private void FillPartPDFReportPartDataAndBOM(Document document, Part part, CalculationResult partCalculationResult)
        {
            PdfPTable table = new PdfPTable(new float[] { 58, 42 });
            table.WidthPercentage = 100;
            table.DefaultCell.Padding = 10;
            table.DefaultCell.HorizontalAlignment = Table.ALIGN_CENTER;

            table.DefaultCell.DisableBorderSide(Table.BOTTOM_BORDER);
            table.DefaultCell.VerticalAlignment = Table.ALIGN_BOTTOM;
            table.AddCell(new Phrase(LocalizedResources.Report_PartData, pdfReportFontLargeBold));
            table.AddCell(new Phrase(LocalizedResources.Report_BOM, pdfReportFontLargeBold));
            table.DefaultCell.DisableBorderSide(Table.TOP_BORDER);
            table.DefaultCell.VerticalAlignment = Table.ALIGN_TOP;

            // fill part data
            FillPartPDFReportPartData(table, part, partCalculationResult);

            // fill bill of materials
            FillPartPDFReportBOM(table, part, partCalculationResult);

            document.Add(table);
        }

        /// <summary>
        /// Fill part of the pdf document with Part Data calculation information
        /// </summary>
        /// <param name="parentTable">The PDF table to fill.</param>
        /// <param name="part">The part for which to fill the report.</param>
        /// <param name="partCalculationResult">Calculation result for the part.</param>
        private void FillPartPDFReportPartData(PdfPTable parentTable, Part part, CalculationResult partCalculationResult)
        {
            PdfPTable containerTable = new PdfPTable(new float[] { 20, 80 });
            containerTable.DefaultCell.Border = Table.NO_BORDER;
            containerTable.HorizontalAlignment = Table.ALIGN_CENTER;

            PdfCellLayoutEvent cellSpacingLayoutEvent = new PdfCellLayoutEvent(new Thickness(5, 0, 5, 0), Color.WHITE);
            PdfCellLayoutEvent cellSpacingLayoutEventColored = new PdfCellLayoutEvent(new Thickness(5, 0, 5, 0), this.pdfReportHeaderColor);
            PdfCellLayoutEvent cellLayoutGrey = new PdfCellLayoutEvent(new Thickness(5, 0, 5, 0), this.pdfGreyCellBackground);

            // fill part picture
            PdfPTable pictureTable = new PdfPTable(1);
            pictureTable.WidthPercentage = 90;

            // workaround for cell spacing top
            pictureTable.DefaultCell.BorderColor = Color.WHITE;
            pictureTable.AddCell(string.Empty);
            pictureTable.DefaultCell.BorderColor = Color.BLACK;

            Media picture = this.GetPictureForReport(part);
            if (picture != null)
            {
                FillPDFReportPicture(pictureTable, picture);
            }
            else
            {
                pictureTable.AddCell(" ");
            }

            // workaround for the size of the cell containing the image
            pictureTable.DefaultCell.DisableBorderSide(Table.LEFT_BORDER);
            pictureTable.DefaultCell.DisableBorderSide(Table.RIGHT_BORDER);
            pictureTable.DefaultCell.DisableBorderSide(Table.BOTTOM_BORDER);
            pictureTable.AddCell(string.Empty);

            containerTable.AddCell(pictureTable);

            // fill the info and costs table
            PdfPTable table = new PdfPTable(new float[] { 25, 25, 25, 25 });
            table.WidthPercentage = 100;
            table.DefaultCell.Border = Table.NO_BORDER;
            table.DefaultCell.PaddingLeft = 7;
            table.DefaultCell.PaddingRight = 7;
            table.DefaultCell.HorizontalAlignment = Table.ALIGN_LEFT;
            table.DefaultCell.CellEvent = cellSpacingLayoutEventColored;

            // Part name
            table.AddCell(new Phrase(LocalizedResources.Report_Name, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_CENTER,
                Phrase = new Phrase(part.Name, pdfReportFontSmall),
                Colspan = 3
            });

            // Empty row
            table.AddCell(new PdfPCell(table.DefaultCell) { Colspan = table.NumberOfColumns, CellEvent = null, Phrase = new Phrase(" ", pdfReportFontSmall) });

            // Location
            string location = (part.ManufacturingCountry + ", " + part.ManufacturingSupplier).TrimEnd(',', ' ');
            table.AddCell(new Phrase(LocalizedResources.General_Location, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_CENTER,
                Phrase = new Phrase(location, pdfReportFontSmall)
            });

            // Calc. Accuracy
            var partCalcAccuracy = (PartCalculationAccuracy)part.CalculationAccuracy.GetValueOrDefault(PartCalculationAccuracy.FineCalculation);
            table.AddCell(new Phrase(LocalizedResources.Report_CalcAccuracy, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_CENTER,
                Phrase = new Phrase(UIUtils.GetEnumValueLabel(partCalcAccuracy), pdfReportFontSmall)
            });

            // Empty row
            table.AddCell(new PdfPCell(table.DefaultCell) { Colspan = table.NumberOfColumns, CellEvent = null, Phrase = new Phrase(" ", pdfReportFontSmall) });

            // Volume / year (RFQ)
            table.AddCell(new Phrase(LocalizedResources.Report_VolumePerYear, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(part.YearlyProductionQuantity != null ? Formatter.FormatInteger(part.YearlyProductionQuantity) : this.emptyCellSymbol, pdfReportFontSmall)
            });

            // Raw Materials
            table.AddCell(new Phrase(LocalizedResources.Report_RawMaterials, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(partCalculationResult.Summary.RawMaterialCost), pdfReportFontSmall)
            });

            // Life Expectancy (years)
            table.AddCell(new Phrase(LocalizedResources.Report_LifeExpectancy, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(part.LifeTime != null ? Formatter.FormatInteger(part.LifeTime) : this.emptyCellSymbol, pdfReportFontSmall)
            });

            // Commodity Cost
            table.AddCell(new Phrase(LocalizedResources.Report_CommodityCost, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(partCalculationResult.Summary.CommoditiesCost), pdfReportFontSmall)
            });

            // Life-time build
            ICostCalculator calculator = CostCalculatorFactory.GetCalculator(part.CalculationVariant);
            var lifetimeProdQty = calculator.CalculateNetLifetimeProductionQuantity(part);

            table.AddCell(new Phrase(LocalizedResources.Report_LifeTimeBuild, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatInteger(lifetimeProdQty), pdfReportFontSmall)
            });

            // Consumable Cost
            table.AddCell(new Phrase(LocalizedResources.Report_ConsumablesCost, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(partCalculationResult.Summary.ConsumableCost), pdfReportFontSmall)
            });

            // Next 2 cells are empty
            table.AddCell(new PdfPCell(table.DefaultCell) { Colspan = 2, CellEvent = null });

            // Manufacturing Cost
            table.AddCell(new Phrase(LocalizedResources.Report_ManufacturingCost, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(partCalculationResult.Summary.TotalManufacturingCost), pdfReportFontSmall)
            });

            // Machine Cost
            table.AddCell(new Phrase(LocalizedResources.Report_MaterialCost, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(partCalculationResult.ProcessCost.MachineCostSum), pdfReportFontSmall)
            });

            // Reject Cost
            table.AddCell(new Phrase(LocalizedResources.Report_RejectCost, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(partCalculationResult.ProcessCost.RejectCostSum), pdfReportFontSmall)
            });

            // Setup Cost
            table.AddCell(new Phrase(LocalizedResources.Report_SetupCost, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(partCalculationResult.ProcessCost.SetupCostSum), pdfReportFontSmall)
            });

            // Transport Cost (from manufacturing process)
            table.AddCell(new Phrase(LocalizedResources.Report_nTierTransportCost, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(partCalculationResult.Summary.ManufacturingTransportCost), pdfReportFontSmall)
            });

            // Direct Labor Cost
            table.AddCell(new Phrase(LocalizedResources.Report_DirectLaborCost, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(partCalculationResult.ProcessCost.DirectLabourCostSum), pdfReportFontSmall)
            });

            // Development & Invest
            decimal devAndInvestCost = partCalculationResult.DevelopmentCost + partCalculationResult.ProjectInvest;
            table.AddCell(new Phrase(LocalizedResources.Report_DevelopmentAndInvest, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(devAndInvestCost), pdfReportFontSmall)
            });

            // Tool/Die Cost
            table.AddCell(new Phrase(LocalizedResources.Report_ToolOrDieCost, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(partCalculationResult.ProcessCost.ToolAndDieCostSum), pdfReportFontSmall)
            });

            // Other
            table.AddCell(new Phrase(LocalizedResources.Report_Other, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(partCalculationResult.OtherCost), pdfReportFontSmall)
            });

            // Maintenance Tools/Dies
            table.AddCell(new Phrase(LocalizedResources.Report_MaintenanceToolsOrDies, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(partCalculationResult.ProcessCost.DiesMaintenanceCostSum), pdfReportFontSmall)
            });

            // Packaging
            table.AddCell(new Phrase(LocalizedResources.Report_Packaging, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(partCalculationResult.PackagingCost), pdfReportFontSmall)
            });

            // OH Manufacturing
            table.AddCell(new Phrase(LocalizedResources.Report_OHManufacturing, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(partCalculationResult.ProcessCost.ManufacturingOverheadSum), pdfReportFontSmall)
            });

            // Transport & Logistic
            decimal transportAndLogCost = partCalculationResult.TransportCost + partCalculationResult.LogisticCost;
            table.AddCell(new Phrase(LocalizedResources.Report_TransportAndLogistic, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(transportAndLogCost), pdfReportFontSmall)
            });

            // Reject Cost
            table.AddCell(new Phrase(LocalizedResources.Report_RejectCost, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(partCalculationResult.ProcessCost.RejectCostSum), pdfReportFontSmall)
            });

            // Overhead & Margin
            table.AddCell(new Phrase(LocalizedResources.Report_OverheadAndMargin, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(partCalculationResult.OverheadCost.TotalSumOverheadAndMargin), pdfReportFontSmall)
            });

            // Transport Cost (from process)
            table.AddCell(new Phrase(LocalizedResources.Report_nTierTransportCost, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(partCalculationResult.ProcessCost.ManufacturingTransportCost), pdfReportFontSmall)
            });

            // Payment Cost
            table.AddCell(new Phrase(LocalizedResources.Report_PaymentCost, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(partCalculationResult.PaymentCost), pdfReportFontSmall)
            });

            // Empty row
            table.AddCell(new PdfPCell(table.DefaultCell) { Colspan = table.NumberOfColumns, CellEvent = null, Phrase = new Phrase(" ", pdfReportFontSmall) });

            // Total Manufacturing Cost
            table.AddCell(new Phrase(LocalizedResources.Report_TotalManufacturingCost, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellLayoutGrey,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(partCalculationResult.ProcessCost.TotalManufacturingCostSum), pdfReportFontSmallBold)
            });

            // Target Sales Price
            table.AddCell(new Phrase(LocalizedResources.Report_TargetSalesPrice, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellLayoutGrey,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(partCalculationResult.TotalCost), pdfReportFontSmallBold)
            });

            // add empty row - workaround to maintain correct height for previous row
            table.AddCell(new PdfPCell(table.DefaultCell) { Colspan = table.NumberOfColumns, CellEvent = null });

            containerTable.AddCell(table);

            parentTable.AddCell(containerTable);
        }

        /// <summary>
        /// Fill pdf report with part's picture. 
        /// </summary>
        /// <param name="table">The PDF table to fill.</param>
        /// <param name="picture">The media that contains the image. Usually it comes form a domain object.</param>
        private void FillPDFReportPicture(PdfPTable table, Media picture)
        {
            MediaType type = (MediaType)picture.Type;
            if (type == MediaType.Image)
            {
                byte[] imageBytes = MediaUtils.GetMediaJPEGImageBytes(picture.Content);
                Image instanceImg = Image.GetInstance(imageBytes);
                table.AddCell(Image.GetInstance(instanceImg));
            }
        }

        /// <summary>
        /// Fill part of the pdf document with Bill of Commodities and RawMaterials information
        /// </summary>
        /// <param name="parentTable">The PDF table to fill.</param>
        /// <param name="part">The Part object to get the data from.</param>
        /// <param name="partCalculationResult">The part calculation result.</param>
        private void FillPartPDFReportBOM(PdfPTable parentTable, Part part, CalculationResult partCalculationResult)
        {
            bool isEmptyTable = false;

            // If the part cost is estimated the the BOM is left empty.
            // This check is necessary because a part with estimated cost may have some materials defined but they should be ignored.
            var partCalcAccuracy = (PartCalculationAccuracy)part.CalculationAccuracy.GetValueOrDefault(PartCalculationAccuracy.FineCalculation);
            if (partCalcAccuracy == PartCalculationAccuracy.Estimation)
            {
                isEmptyTable = true;
            }

            // create the PDF table
            PdfPTable table = new PdfPTable(new float[] { 20, 12, 8, 8, 13, 9, 10, 10, 10 });
            table.WidthPercentage = 100;
            table.DefaultCell.HorizontalAlignment = Table.ALIGN_CENTER;
            table.DefaultCell.VerticalAlignment = Table.ALIGN_MIDDLE;

            // workaround for cell spacing top
            table.DefaultCell.BorderColor = Color.WHITE;
            table.AddCell(string.Empty);
            table.CompleteRow();
            table.DefaultCell.BorderColor = Color.BLACK;

            // add header row to the PDF table
            table.DefaultCell.BackgroundColor = pdfReportHeaderColor;
            table.AddCell(new Phrase(LocalizedResources.Report_Name, pdfReportFontColumnHeader));

            var weightLabel = LocalizedResources.General_Weight;

            // Append the symbol to the weight label if have any
            if (this.measurementUnitsAdapter != null && !string.IsNullOrWhiteSpace(this.measurementUnitsAdapter.UIWeight.Symbol))
            {
                weightLabel = string.Format("{0} ({1})", weightLabel, this.measurementUnitsAdapter.UIWeight.Symbol);
            }

            table.AddCell(new Phrase(weightLabel, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_Price, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_Scrap, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_ScrapRefund, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_Loss, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_NetCost, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_Overhead, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_TotalCost, pdfReportFontColumnHeader));

            // fill calculated information
            table.DefaultCell.BackgroundColor = Color.WHITE;
            if (!isEmptyTable && partCalculationResult.Summary != null)
            {
                FillPartPDFReportCommodities(table, partCalculationResult, part);
                FillPartPDFReportRawMaterial(table, partCalculationResult, part);
                FillPartPDFReportConsumables(table, partCalculationResult, part);
            }

            // if no data has been filled - add an empty row
            if (table.Rows.Count <= 2)
            {
                table.DefaultCell.BackgroundColor = Color.WHITE;
                table.AddCell(" ");
                table.CompleteRow();
            }

            // add a dummy row without borders to fill the remaining height
            table.DefaultCell.Border = Table.NO_BORDER;
            table.AddCell(string.Empty);
            table.CompleteRow();

            parentTable.AddCell(table);
        }

        /// <summary>
        /// Fill Raw Material information in the Part export report.
        /// </summary>
        /// <param name="table">The PDF table to be filled.</param>
        /// <param name="partCalculationResult">The part calculation result.</param>
        /// <param name="part">The part from where the materials are taken.</param>
        private void FillPartPDFReportCommodities(PdfPTable table, CalculationResult partCalculationResult, Part part)
        {
            // fill commodities required information                            
            foreach (var cost in partCalculationResult.CommoditiesCost.CommodityCosts.OrderBy(c => c.Name))
            {
                table.DefaultCell.HorizontalAlignment = Table.ALIGN_CENTER;
                table.AddCell(new Phrase(cost.Name, pdfReportFont));

                table.DefaultCell.HorizontalAlignment = Table.ALIGN_RIGHT;
                table.AddCell(new Phrase(cost.Weight.HasValue ? Formatter.FormatNumber(cost.Weight.Value, 4) : this.emptyCellSymbol, pdfReportFont));
                table.AddCell(new Phrase(Formatter.FormatMoney(cost.Price), pdfReportFont));
                table.AddCell(this.emptyCellSymbol);
                table.AddCell(this.emptyCellSymbol);
                table.AddCell(this.emptyCellSymbol);
                table.AddCell(new Phrase(Formatter.FormatMoney(cost.Cost), pdfReportFont));
                table.AddCell(new Phrase(Formatter.FormatMoney(cost.Overhead), pdfReportFont));
                table.AddCell(new PdfPCell(table.DefaultCell)
                    {
                        Phrase = new Phrase(Formatter.FormatMoney(cost.TotalCost), pdfReportFontBold),
                        BackgroundColor = this.pdfGreyCellBackground
                    });
            }
        }

        /// <summary>
        /// Fill Raw Material information in the Bill Of Material section
        /// </summary>
        /// <param name="table">The PDF table to be filled.</param>
        /// <param name="partCalculationResult">The part calculation result.</param>
        /// <param name="part">The part from which the materials are taken.</param>
        private void FillPartPDFReportRawMaterial(PdfPTable table, CalculationResult partCalculationResult, Part part)
        {
            // fill raw materials required information              
            foreach (var cost in partCalculationResult.RawMaterialsCost.RawMaterialCosts.OrderBy(c => c.Name))
            {
                table.DefaultCell.HorizontalAlignment = Table.ALIGN_CENTER;
                table.AddCell(new Phrase(cost.Name, pdfReportFont));

                table.DefaultCell.HorizontalAlignment = Table.ALIGN_RIGHT;
                table.AddCell(new Phrase(Formatter.FormatNumber(cost.PartWeight, 4), pdfReportFont));

                // Adjust the price of the raw material according to its weight if possible
                decimal price = this.AdjustRawMaterialPriceToWeightUnit(part, cost);

                table.AddCell(new Phrase(Formatter.FormatMoney(price), pdfReportFont));
                table.AddCell(new Phrase(Formatter.FormatMoney(cost.ScrapCost), pdfReportFont));
                table.AddCell(new Phrase(Formatter.FormatMoney(cost.ScrapRefund), pdfReportFont));
                table.AddCell(new Phrase(Formatter.FormatMoney(cost.LossCost), pdfReportFont));
                table.AddCell(new Phrase(Formatter.FormatMoney(cost.NetCost), pdfReportFont));
                table.AddCell(new Phrase(Formatter.FormatMoney(cost.Overhead), pdfReportFont));
                table.AddCell(new PdfPCell(table.DefaultCell)
                {
                    Phrase = new Phrase(Formatter.FormatMoney(cost.TotalCost), pdfReportFontBold),
                    BackgroundColor = this.pdfGreyCellBackground
                });
            }
        }

        /// <summary>
        /// Fill Part Consumables in the pdf form.
        /// </summary>
        /// <param name="table">The PDF table to be filled.</param>
        /// <param name="partCalculationResult">The part calculation result.</param>
        /// <param name="part">The part from where the consumable are taken.</param>
        private void FillPartPDFReportConsumables(PdfPTable table, CalculationResult partCalculationResult, Part part)
        {
            // fill Consumables required information
            foreach (var cost in partCalculationResult.ConsumablesCost.ConsumableCosts.OrderBy(c => c.Name))
            {
                table.DefaultCell.HorizontalAlignment = Table.ALIGN_CENTER;
                table.AddCell(new Phrase(cost.Name, pdfReportFont));

                table.DefaultCell.HorizontalAlignment = Table.ALIGN_RIGHT;
                table.AddCell(this.emptyCellSymbol);
                table.AddCell(new Phrase(Formatter.FormatMoney(cost.Price), pdfReportFont));
                table.AddCell(this.emptyCellSymbol);
                table.AddCell(this.emptyCellSymbol);
                table.AddCell(this.emptyCellSymbol);
                table.AddCell(new Phrase(Formatter.FormatMoney(cost.Cost), pdfReportFont));
                table.AddCell(new Phrase(Formatter.FormatMoney(cost.Overhead), pdfReportFont));
                table.AddCell(new PdfPCell(table.DefaultCell)
                {
                    Phrase = new Phrase(Formatter.FormatMoney(cost.TotalCost), pdfReportFontBold),
                    BackgroundColor = this.pdfGreyCellBackground
                });
            }
        }

        /// <summary>
        /// Fill part of the pdf document with Manufacturing summary.
        /// </summary>
        /// <param name="document">The PDF document to fill.</param>
        /// <param name="part">The Part object to get the data from.</param>
        /// <param name="partCalculationResult">The part calculation result.</param>
        private void FillPartPDFReportManufacturingSummary(Document document, Part part, CalculationResult partCalculationResult)
        {
            bool isEmptyTable = false;

            // If the part cost is estimated the manufacturing summary is left empty.
            // This check is necessary because a part with estimated cost may have process steps defined but they should be ignored.
            var partCalcAccuracy = (PartCalculationAccuracy)part.CalculationAccuracy.GetValueOrDefault(PartCalculationAccuracy.FineCalculation);
            if (partCalcAccuracy != PartCalculationAccuracy.FineCalculation)
            {
                isEmptyTable = true;
            }

            if (document == null ||
                part == null || part.Process == null || part.Process.Steps == null ||
                partCalculationResult == null ||
                partCalculationResult.ProcessCost == null ||
                partCalculationResult.ProcessCost.StepCosts == null)
            {
                isEmptyTable = true;
            }

            PdfPTable table = new PdfPTable(new float[] { 3.5f, 15.5f, 12.5f, 6.5f, 3f, 3f, 5f, 5f, 4f, 5f, 5f, 5.5f, 5f, 4.5f, 4.5f, 4.5f, 4f, 4f });
            table.WidthPercentage = 100;
            table.DefaultCell.HorizontalAlignment = Table.ALIGN_CENTER;
            table.DefaultCell.VerticalAlignment = Table.ALIGN_MIDDLE;
            table.DefaultCell.BackgroundColor = pdfReportHeaderColor;
            table.AddCell(new PdfPCell(new Phrase(LocalizedResources.Report_ManufacturingSummary, pdfReportFontColumnHeaderBold))
                {
                    Colspan = table.NumberOfColumns,
                    HorizontalAlignment = Table.ALIGN_CENTER,
                    VerticalAlignment = Table.ALIGN_MIDDLE,
                    BackgroundColor = pdfReportHeaderColor
                });

            table.AddCell(new Phrase(LocalizedResources.Report_StepNo, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_OperationalDescription, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_MachineName, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_MachineOrEquipmentCost, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_Availability, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_LoadRate, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_SetupCost, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_DirectLabor, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_ToolsAndDieCost, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_MaintenanceDieOrTooling, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_ManufacturingCost, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_ManufacturingOH, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_RejectCost, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_nTierTransportCost, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_ExternalProcess, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_CycleTime, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_PartsPerCycle, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_TotalManufacturingCost, pdfReportFontColumnHeader));
            table.DefaultCell.BackgroundColor = Color.WHITE;

            if (isEmptyTable || part.Process.Steps.Count == 0)
            {
                // add empty row and return
                table.AddCell(" ");
                table.CompleteRow();
                document.Add(table);
                return;
            }

            int stepNo = 0;
            foreach (ProcessStep step in part.Process.Steps.OrderBy(ps => ps.Index))
            {
                stepNo++;

                // get process step cost by ID
                ProcessStepCost cost = partCalculationResult.ProcessCost.StepCosts.FirstOrDefault(c => c.StepId == step.Guid);

                // get machines information - there will be one row for each machine
                List<Machine> machines = step.Machines.Where(m => !m.IsDeleted).ToList();
                Machine firstMachine = machines.FirstOrDefault();
                machines.Remove(firstMachine);

                table.DefaultCell.HorizontalAlignment = Table.ALIGN_CENTER;
                table.AddCell(new Phrase(stepNo.ToString(), pdfReportFont)); // "Step No" 1
                table.AddCell(new Phrase(step.Name, pdfReportFont)); // "Operational Description" 2
                table.AddCell(new Phrase(firstMachine != null ? firstMachine.Name : this.emptyCellSymbol, pdfReportFont)); // "Machine Name" 3

                table.DefaultCell.HorizontalAlignment = Table.ALIGN_RIGHT;
                table.AddCell(new Phrase(cost != null ? Formatter.FormatMoney(cost.MachineCost) : string.Empty, pdfReportFont)); // "Machine / Equipment Cost" 4
                table.AddCell(new Phrase(firstMachine != null ? Formatter.FormatNumber(firstMachine.Availability * 100, 2) + " %" : this.emptyCellSymbol, pdfReportFont));  // "Availability" 5
                table.AddCell(new Phrase(firstMachine != null ? Formatter.FormatNumber(firstMachine.FullLoadRate * 100, 2) + " %" : this.emptyCellSymbol, pdfReportFont));  // "Load-rate" 6
                table.AddCell(new Phrase(cost != null ? Formatter.FormatMoney(cost.SetupCost) : string.Empty, pdfReportFont)); // "Setup Cost" 7
                table.AddCell(new Phrase(cost != null ? Formatter.FormatMoney(cost.DirectLabourCost) : string.Empty, pdfReportFont)); // "Direct Labor" 8
                table.AddCell(new Phrase(cost != null ? Formatter.FormatMoney(cost.ToolAndDieCost) : string.Empty, pdfReportFont)); // "Tools and Die Cost" 9
                table.AddCell(new Phrase(cost != null ? Formatter.FormatMoney(cost.DiesMaintenanceCost) : string.Empty, pdfReportFont)); // "Maintenance DiesTools" 10
                table.AddCell(new Phrase(cost != null ? Formatter.FormatMoney(cost.ManufacturingCost) : string.Empty, pdfReportFont)); // "Manufacturing Cost" 11
                table.AddCell(new Phrase(cost != null ? Formatter.FormatMoney(cost.ManufacturingOverheadCost) : string.Empty, pdfReportFont)); // "Manufacturing OH" 12
                table.AddCell(new Phrase(cost != null ? Formatter.FormatMoney(cost.RejectCost) : string.Empty, pdfReportFont)); // "ScrapCost" 13
                table.AddCell(new Phrase(cost != null ? Formatter.FormatMoney(cost.TransportCost) : string.Empty, pdfReportFont)); // "Transport Cost" 14
                table.AddCell(new Phrase(step.IsExternal == true ? LocalizedResources.General_Yes : LocalizedResources.General_No, pdfReportFont)); // "External Process" 15
                table.AddCell(new Phrase(step.CycleTime.HasValue ? Formatter.FormatNumber(step.CycleTime.Value, 4) : this.emptyCellSymbol, pdfReportFont)); // "Cycle Time" 16
                table.AddCell(new Phrase(step.PartsPerCycle.HasValue ? Formatter.FormatInteger(step.PartsPerCycle.Value) : this.emptyCellSymbol, pdfReportFont)); // "Parts Per Cycle" 17
                table.AddCell(new PdfPCell(table.DefaultCell)
                    {
                        Phrase = new Phrase(cost != null ? Formatter.FormatMoney(cost.TotalManufacturingCost) : string.Empty, pdfReportFontBold),
                        BackgroundColor = this.pdfGreyCellBackground
                    });

                // Add the rest of machines on new rows
                foreach (var machine in machines)
                {
                    table.DefaultCell.HorizontalAlignment = Table.ALIGN_CENTER;
                    table.AddCell(string.Empty);
                    table.AddCell(string.Empty);
                    table.AddCell(new Phrase(machine.Name, pdfReportFont)); // "Machine Name" 3
                    table.AddCell(string.Empty);

                    table.DefaultCell.HorizontalAlignment = Table.ALIGN_RIGHT;
                    table.AddCell(new Phrase(Formatter.FormatNumber(machine.Availability * 100, 2) + " %", pdfReportFont)); // "Availability" 5
                    table.AddCell(new Phrase(Formatter.FormatNumber(machine.FullLoadRate * 100, 2) + " %", pdfReportFont)); // "Load-rate" 6
                    table.CompleteRow();
                }
            }

            document.Add(table);
        }

        #endregion Part Report Pdf

        #region Assembly Report Xls

        /// <summary>
        /// Create the assembly report in xls format.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <param name="result">The cost calculation of the assembly.</param>
        /// <param name="path">The path where to save the excel file on the disk.</param>
        /// <exception cref="ArgumentNullException">The assembly or result parameter is null</exception>
        /// <exception cref="ArgumentException">The path is null or empty</exception>
        public void CreateAssemblyReportXls(
            Assembly assembly,
            CalculationResult result,
            string path)
        {
            if (assembly == null)
            {
                throw new ArgumentNullException("assembly", "The assembly was null.");
            }

            if (result == null)
            {
                throw new ArgumentNullException("result", "The assembly cost calculation result was null.");
            }

            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentException("The file path was null or empty", "path");
            }

            try
            {
                HSSFWorkbook workbook = null;
                using (Stream templateStream = new MemoryStream(LocalizedResources.RegularAssemblyReportTemplate))
                {
                    if (templateStream == null)
                    {
                        log.Error("The xls assembly report template was null.");
                    }

                    workbook = new HSSFWorkbook(templateStream);
                }

                ISheet sheet = workbook.GetSheetAt(0);
                if (sheet != null)
                {
                    workbook.SetSheetName(0, ReportsHelper.ValidateWorksheetName(assembly.Name));

                    // Fill some generic info and the Assembly Data section
                    FillAssemblyReportGenericAndAssemblyData(sheet, assembly, result);

                    // Fill part picture
                    Media picture = this.GetPictureForReport(assembly);
                    if (picture != null)
                    {
                        AddReportPicture(workbook, sheet, picture, 5, 2, 22, 5);
                    }

                    int partsListTableStartRowNum;
                    int stepsTableStartRowNum;
                    int assyListTableStartRowNum;
                    int stepDetailsTableStartRowNum;
                    int otherCommoditiesTableStartRowNum;

                    // Insert the necessary rows to accommodate all the content in the "Part List" table, "Steps" table, "Assembly / Fixing & Bought in Parts" table, "Step Details" table and "Other Commodities" table.                    
                    AdjustRowsOfAssemblyReport(
                        sheet,
                        assembly,
                        result,
                        out partsListTableStartRowNum,
                        out stepsTableStartRowNum,
                        out assyListTableStartRowNum,
                        out stepDetailsTableStartRowNum,
                        out otherCommoditiesTableStartRowNum);

                    // Fill the "Part List" table
                    FillAssemblyReportPartListTable(sheet, result, partsListTableStartRowNum);

                    // Fill the "Steps" table
                    FillAssemblyReportStepsTable(workbook, sheet, assembly, stepsTableStartRowNum);

                    // Fill the "Assembly / Fixing & Bought in Parts" table
                    FillAssemblyReportAssemblyFixingAndBoughtinPartsTable(sheet, assembly, result, assyListTableStartRowNum);

                    // Fill the "Step Details" table
                    FillAssemblyReportStepDetailsTable(sheet, assembly, result, stepDetailsTableStartRowNum);

                    // Fill the "Other Commodities" table
                    FillAssemblyReportOtherCommodities(sheet, result, otherCommoditiesTableStartRowNum);

                    // Add the bottom logo
                    ReportsHelper.AddReportBottomLogo(workbook, sheet, 9, sheet.LastRowNum - 5);
                }

                // Save the report
                using (FileStream file = new FileStream(path, FileMode.Create))
                {
                    workbook.Write(file);
                }
            }
            catch (IOException ex)
            {
                throw new BusinessException(ErrorCodes.ReportGenerationIOError, ex);
            }
            catch (UnauthorizedAccessException ex)
            {
                throw new BusinessException(FileErrorCode.UnauthorizedAccess, ex);
            }
            catch (Exception ex)
            {
                log.ErrorException("Error while creating the xls assembly report.", ex);
                throw new BusinessException(ErrorCodes.ExcelReportGenerationUnknownError, ex);
            }
        }

        /// <summary>
        /// Fill in the generic information (date, current user, currency) and the Assembly Data section of the template sheet.
        /// </summary>
        /// <param name="sheet">The excel sheet to fill.</param>
        /// <param name="assembly">The assembly.</param>
        /// <param name="result">The assembly's cost calculation result.</param>
        private void FillAssemblyReportGenericAndAssemblyData(ISheet sheet, Assembly assembly, CalculationResult result)
        {
            // Create the enhanced cost breakdown from which to get the raw material related costs.
            var enhancedResult = ReportsHelper.CreateEnhancedCostBreakdown(result);

            // Add username
            if (SecurityManager.Instance.CurrentUser != null)
            {
                sheet.GetRow(1).GetCell(15).SetCellValue(SecurityManager.Instance.CurrentUser.Name);
            }

            sheet.GetRow(1).GetCell(22).SetCellValue(assembly.CalculationVariant);
            sheet.GetRow(1).GetCell(28).SetCellValue(DateTime.Now.ToString("dd MMMM yyyy"));

            // Add currency used
            sheet.GetRow(82).GetCell(1).SetCellValue(this.GetUICurrencySymbol());

            // Add some assembly information
            sheet.GetRow(5).GetCell(8).SetCellValue(assembly.Name);

            string location = (assembly.AssemblingCountry + ", " + assembly.AssemblingSupplier).TrimEnd(',', ' ');
            sheet.GetRow(7).GetCell(8).SetCellValue(location);

            sheet.GetRow(7).GetCell(17).SetCellValue(UIUtils.GetEnumValueLabel(result.CalculationAccuracy));

            sheet.GetRow(11).GetCell(8).SetCellValue(enhancedResult.Summary.RawMaterialCost);
            sheet.GetRow(12).GetCell(8).SetCellValue(enhancedResult.OverheadCost.RawMaterialOverhead);
            sheet.GetRow(14).GetCell(8).SetCellValue(result.ProcessCost.MachineCostSum);
            sheet.GetRow(15).GetCell(8).SetCellValue(result.ProcessCost.SetupCostSum);
            sheet.GetRow(16).GetCell(8).SetCellValue(result.ProcessCost.DirectLabourCostSum);
            sheet.GetRow(17).GetCell(8).SetCellValue(result.ProcessCost.ToolAndDieCostSum);
            sheet.GetRow(18).GetCell(8).SetCellValue(result.ProcessCost.ManufacturingOverheadSum);
            sheet.GetRow(19).GetCell(8).SetCellValue(result.ProcessCost.RejectCostSum);
            sheet.GetRow(20).GetCell(8).SetCellValue(result.ProcessCost.ManufacturingTransportCost);
            sheet.GetRow(22).GetCell(8).SetCellValue(result.ProcessCost.TotalManufacturingCostSum);

            sheet.GetRow(11).GetCell(17).SetCellValue(result.Summary.ConsumableCost + result.Summary.CommoditiesCost);
            sheet.GetRow(12).GetCell(17).SetCellValue(result.Summary.TotalManufacturingCost);
            sheet.GetRow(13).GetCell(17).SetCellValue(result.Summary.ManufacturingRejectCost);
            sheet.GetRow(14).GetCell(17).SetCellValue(result.Summary.SubpartsAndSubassembliesCost);
            sheet.GetRow(15).GetCell(17).SetCellValue(result.Summary.DevelopmentCost + result.Summary.ProjectInvest);
            sheet.GetRow(16).GetCell(17).SetCellValue(result.Summary.OtherCost);
            sheet.GetRow(17).GetCell(17).SetCellValue(result.Summary.PackagingCost);
            sheet.GetRow(18).GetCell(17).SetCellValue(result.Summary.TransportCost + result.Summary.LogisticCost);
            sheet.GetRow(19).GetCell(17).SetCellValue(result.OverheadCost.TotalSumOverheadAndMargin);
            sheet.GetRow(20).GetCell(17).SetCellValue(result.Summary.PaymentCost);
            sheet.GetRow(22).GetCell(17).SetCellValue(result.Summary.TargetCost);

            // The Price Data in the lower right corner, below all tables
            sheet.GetRow(84).GetCell(29).SetCellValue(assembly.TargetPrice);
            sheet.GetRow(86).GetCell(29).SetCellValue(result.Summary.TargetCost);
        }

        /// <summary>
        /// Insert the necessary rows to accommodate all the content in the "Steps" area, "Part List" table, "Assembly / Fixing and Bought in Parts" table,
        /// "Other Commodities" table and "Step Details" table
        /// </summary>
        /// <param name="sheet">The sheet.</param>
        /// <param name="assembly">The assembly.</param>
        /// <param name="result">The assembly's cost calculation result.</param>
        /// <param name="partsListStartRowNum">The row number where the "Parts List" table starts.</param>
        /// <param name="stepsStartRowNum">The row number where the "Steps" table starts.</param>
        /// <param name="assyListStartRowNum">The row number where the "Assembly / Fixing and Bought in Parts" table starts.</param>
        /// <param name="stepDetailsStartRowNum">The row number where the "Step Details" table starts.</param>
        /// <param name="otherCommoditiesStartRowNum">The row number where the "Other Commodities" table starts.</param>
        private void AdjustRowsOfAssemblyReport(
            ISheet sheet,
            Assembly assembly,
            CalculationResult result,
            out int partsListStartRowNum,
            out int stepsStartRowNum,
            out int assyListStartRowNum,
            out int stepDetailsStartRowNum,
            out int otherCommoditiesStartRowNum)
        {
            // The number of the row where each table starts. The default values are from the unchanged template; they are updated when new rows are inserted.
            partsListStartRowNum = 6;
            stepsStartRowNum = 25;
            assyListStartRowNum = 37;
            stepDetailsStartRowNum = 32;
            otherCommoditiesStartRowNum = 56;

            // The number of rows available by default are: 27 rows for "Part List", 15 rows for "Assembly / Fixing & Bought in Parts", 24 rows for "Other Commodities"
            // and 49 rows in "Step Details": 15 are shared with "Assembly / Fixing & Bought in Parts", 24 are shared with "Other Commodities"
            // 2 are shared width "Part List"
            int extraPartListRowsNo = 0;
            int extraAssyListRowsNo = 0;
            int extraOtherCommoditiesRowsNo = 0;
            int extraStepDetailsRowsNo = 0;
            int extraStepsRowsNo = 0;

            if (result.PartsCost.PartCosts.Count > 27)
            {
                extraPartListRowsNo = result.PartsCost.PartCosts.Count - 27;
            }

            int assyListEntriesNo = result.AssembliesCost.AssemblyCosts.Count + result.CommoditiesCost.CommodityCosts.Count;
            if (assyListEntriesNo > 15)
            {
                extraAssyListRowsNo = assyListEntriesNo - 15;
            }

            if (result.ConsumablesCost.ConsumableCosts.Count > 24)
            {
                extraOtherCommoditiesRowsNo = result.ConsumablesCost.ConsumableCosts.Count - 24;
            }

            if (result.ProcessCost.StepCosts.Count > 49)
            {
                extraStepDetailsRowsNo = result.ProcessCost.StepCosts.Count - 49;
            }

            // For each process step, each machine starting with the 2nd must be added on a new row
            foreach (ProcessStep step in assembly.Process.Steps)
            {
                if (step.Machines.Count > 1)
                {
                    extraStepDetailsRowsNo += step.Machines.Count - 1;
                }
            }

            // In the "Steps" table we can fit 9 step icons on every 3 rows and we have 9 rows available by default => we can fit 9 step icons in the space available by default
            if (result.ProcessCost.StepCosts.Count > 9)
            {
                double extraSteps = result.ProcessCost.StepCosts.Count - 9;

                // Compute how many extra groups of 9 steps we have and multiply by 3 (3 is the number of rows needed to display a group of 9 step icons).
                extraStepsRowsNo = Convert.ToInt32(Math.Ceiling(extraSteps / 9d)) * 3;
            }

            // The number of rows indirectly added to the "Part List" table (When adding a row to the "Step" the row is also added to the "Part List" table).
            int addedPartListNo = 0;

            // The number of rows indirectly added to the "Assembly / Fixing & Bought in Parts" table (When adding a row to the step icons area it is also added in this table)
            int addedAssyListRowsNo = 0;

            // The number of rows indirectly added to the "Step Details" table (When adding a row to the "Assembly / Fixing & Bought in Parts" or "Other Commodities"
            // the row is also added to the "Step Details" table).
            int addedStepDetailsRowsNo = 0;

            // The number of rows indirectly added to the "Other Commodities" table (When adding a row to the "Step Details" table, it is also added in this table).
            int addedOtherCommoditiesRowsNo = 0;

            // Add the extra necessary rows for the step icons area. These rows will be added in the part of the area that is common with the "Assembly / Fixing & Bought in Parts" table.                
            for (int i = 0; i < extraStepsRowsNo; i++)
            {
                CopyRow(sheet, stepsStartRowNum, sheet, stepsStartRowNum + 1, false);
                addedPartListNo++;

                // When a row in the "Steps" table, the "Step Details" and "Other Commodities" tables are shifted down
                stepDetailsStartRowNum++;
                otherCommoditiesStartRowNum++;
                assyListStartRowNum++;
            }

            // Add the necessary extra rows to the "Part List" table
            for (int i = 0; i < extraPartListRowsNo - addedPartListNo; i++)
            {
                CopyRow(sheet, stepDetailsStartRowNum, sheet, stepDetailsStartRowNum + 1, false);
                addedStepDetailsRowsNo++;

                // When a row in the "Part List" table, all tables except "Steps" are shifted down.
                assyListStartRowNum++;
                otherCommoditiesStartRowNum++;
            }

            // Add the extra necessary rows to "Assembly / Fixing & Bought in Parts". These rows will be added in the part of the area that is common with the "Step Details" table.
            for (int i = 0; i < extraAssyListRowsNo - addedAssyListRowsNo; i++)
            {
                CopyRow(sheet, assyListStartRowNum, sheet, assyListStartRowNum + 1, false);
                addedStepDetailsRowsNo++;

                otherCommoditiesStartRowNum++;
            }

            // Add the extra necessary rows to the "Other Commodities" table
            for (int i = 0; i < extraOtherCommoditiesRowsNo - addedOtherCommoditiesRowsNo; i++)
            {
                CopyRow(sheet, otherCommoditiesStartRowNum + 1, sheet, otherCommoditiesStartRowNum + 2, false);
                addedStepDetailsRowsNo++;
            }

            // Add the extra necessary rows to Step Details" table. These rows will be added in the part of the area that is common with the "Other Commodities" table.
            for (int i = 0; i < extraStepDetailsRowsNo - addedStepDetailsRowsNo; i++)
            {
                CopyRow(sheet, otherCommoditiesStartRowNum + 1, sheet, otherCommoditiesStartRowNum + 2, false);
                addedOtherCommoditiesRowsNo++;
            }
        }

        /// <summary>
        /// Fill the "Part List" table in the assembly report.
        /// </summary>
        /// <param name="sheet">The excel sheet to fill.</param>
        /// <param name="result">The assembly cost calculation result.</param>
        /// <param name="rowNum">The number of the row where to start inserting data.</param>
        private void FillAssemblyReportPartListTable(ISheet sheet, CalculationResult result, int rowNum)
        {
            foreach (PartCost partCost in result.PartsCost.PartCosts.OrderBy(c => c.PartIndex))
            {
                IRow crtRow = sheet.GetRow(rowNum);
                crtRow.GetCell(21).SetCellValue(partCost.Name);
                crtRow.GetCell(22).SetCellValue(partCost.Description);
                crtRow.GetCell(28).SetCellValue(partCost.AmountPerAssembly);
                crtRow.GetCell(31).SetCellValue(partCost.TargetCost);
                crtRow.GetCell(32).SetCellValue(partCost.AmountPerAssembly * partCost.TargetCost);
                rowNum++;
            }
        }

        /// <summary>
        /// Fills the assembly report "Steps" table with shapes denoting steps.
        /// </summary>
        /// <param name="workbook">The workbook.</param>
        /// <param name="sheet">The sheet.</param>
        /// <param name="assy">The assembly.</param>
        /// <param name="rowNum">The row number where the insertion area of the "Steps" table begins.</param>
        private void FillAssemblyReportStepsTable(IWorkbook workbook, ISheet sheet, Assembly assy, int rowNum)
        {
            var assyCalcAccuracy = (PartCalculationAccuracy)assy.CalculationAccuracy.GetValueOrDefault(PartCalculationAccuracy.FineCalculation);
            if (assy.Process.Steps.Count == 0 || assyCalcAccuracy != PartCalculationAccuracy.FineCalculation)
            {
                return;
            }

            HSSFPatriarch patriarch = null;
            if (sheet.DrawingPatriarch != null)
            {
                patriarch = (HSSFPatriarch)sheet.DrawingPatriarch;
            }
            else
            {
                patriarch = (HSSFPatriarch)sheet.CreateDrawingPatriarch();
            }

            // The font used for the step name and index
            IFont font = workbook.CreateFont();
            font.FontHeightInPoints = 7;

            HSSFClientAnchor anchor = new HSSFClientAnchor(0, 0, 1023, 255, 2, rowNum, 19, rowNum + 2);
            HSSFShapeGroup mainGroup = patriarch.CreateGroup(anchor);
            mainGroup.SetCoordinates(0, -35, 1023, 220);
            rowNum += 3;

            int stepShapeWidth = 100;
            int maxStepShapesPerRow = 9;
            int currentStepNumber = 1;
            int shapeOffsetX = 0;
            foreach (ProcessStep step in assy.Process.Steps.OrderBy(ps => ps.Index))
            {
                if (currentStepNumber > 1 && currentStepNumber % maxStepShapesPerRow == 1)
                {
                    shapeOffsetX = 0;

                    // Create an new anchor and shape group 3 rows down for a new row of shapes
                    anchor = new HSSFClientAnchor(0, 0, 1023, 255, 2, rowNum, 19, rowNum + 2);
                    mainGroup = patriarch.CreateGroup(anchor);
                    mainGroup.SetCoordinates(0, -35, 1023, 220);
                    rowNum += 3;
                }

                CreateProcessStepShape(mainGroup, stepShapeWidth, shapeOffsetX, step.Name, currentStepNumber.ToString(), font);

                shapeOffsetX += stepShapeWidth + 10;
                currentStepNumber++;
            }
        }

        /// <summary>
        /// Fills the "Assembly / Fixing and Bought in Parts" table of an assembly report.
        /// </summary>
        /// <param name="sheet">The sheet.</param>
        /// <param name="assy">The assembly.</param>
        /// <param name="result">The assembly's cost calculation result.</param>
        /// <param name="rowNum">The number of the row where to start inserting data.</param>
        private void FillAssemblyReportAssemblyFixingAndBoughtinPartsTable(ISheet sheet, Assembly assy, CalculationResult result, int rowNum)
        {
            // Add the sub-assemblies first                        
            foreach (AssemblyCost cost in result.AssembliesCost.AssemblyCosts.OrderBy(c => c.AssemblyIndex))
            {
                IRow crtRow = sheet.GetRow(rowNum);
                crtRow.GetCell(21).SetCellValue(cost.Name);
                crtRow.GetCell(28).SetCellValue(cost.AmountPerAssembly);
                crtRow.GetCell(31).SetCellValue(cost.TargetCost);
                crtRow.GetCell(32).SetCellValue(cost.TargetCost * cost.AmountPerAssembly);

                Assembly subAssy = assy.Subassemblies.FirstOrDefault(a => a.Guid == cost.AssemblyId);
                if (subAssy != null)
                {
                    crtRow.GetCell(22).SetCellValue(subAssy.Description);

                    if (subAssy.Manufacturer != null)
                    {
                        crtRow.GetCell(26).SetCellValue(subAssy.Manufacturer.Name);
                    }
                }

                rowNum++;
            }

            // Then add the commodities
            foreach (CommodityCost cost in result.CommoditiesCost.CommodityCosts.OrderBy(c => c.Name))
            {
                IRow crtRow = sheet.GetRow(rowNum);
                crtRow.GetCell(21).SetCellValue(cost.Name);
                crtRow.GetCell(22).SetCellValue(cost.Remarks);
                crtRow.GetCell(26).SetCellValue(cost.Manufacturer);
                crtRow.GetCell(28).SetCellValue(cost.Amount);
                crtRow.GetCell(31).SetCellValue(cost.Price);
                crtRow.GetCell(32).SetCellValue(cost.TotalCost);
                rowNum++;
            }
        }

        /// <summary>
        /// Fills the "Step Details" table of an assembly report.
        /// </summary>
        /// <param name="sheet">The worksheet.</param>
        /// <param name="assembly">The assembly.</param>
        /// <param name="result">The assembly's cost calculation result.</param>
        /// <param name="rowNum">The number of the row where to start inserting data.</param>
        private void FillAssemblyReportStepDetailsTable(ISheet sheet, Assembly assembly, CalculationResult result, int rowNum)
        {
            var assyCalcAccuracy = (PartCalculationAccuracy)assembly.CalculationAccuracy.GetValueOrDefault(PartCalculationAccuracy.FineCalculation);
            if (assyCalcAccuracy != PartCalculationAccuracy.FineCalculation)
            {
                return;
            }

            int crtStepNumber = 1;
            foreach (ProcessStep step in assembly.Process.Steps.OrderBy(ps => ps.Index))
            {
                IRow crtRow = sheet.GetRow(rowNum);

                // Fill process step information
                crtRow.GetCell(2).SetCellValue(crtStepNumber);
                crtRow.GetCell(3).SetCellValue(step.Name);
                crtRow.GetCell(15).SetCellValue(step.CycleTime);
                crtRow.GetCell(17).SetCellValue(step.PartsPerCycle);

                ProcessStepCost stepCost = result.ProcessCost.StepCosts.FirstOrDefault(c => c.StepId == step.Guid);
                if (stepCost != null)
                {
                    // Fill calculation result info
                    crtRow.GetCell(7).SetCellValue(stepCost.MachineCost);
                    crtRow.GetCell(8).SetCellValue(stepCost.SetupCost);
                    crtRow.GetCell(9).SetCellValue(stepCost.DirectLabourCost);
                    crtRow.GetCell(12).SetCellValue(stepCost.RejectCost);
                    crtRow.GetCell(14).SetCellValue(stepCost.ManufacturingCost);
                    crtRow.GetCell(18).SetCellValue(stepCost.TotalManufacturingCost);
                }

                // Fill the machines information. The info of each machine after the 1st is added on a new row.                
                var machines = step.Machines.Where(m => !m.IsDeleted).OrderBy(m => m.Index);
                int machinesCount = machines.Count();
                for (int i = 0; i < machinesCount; i++)
                {
                    if (i > 0)
                    {
                        rowNum++;
                    }

                    IRow crtMachineRow = sheet.GetRow(rowNum);
                    Machine machine = machines.ElementAt(i);

                    crtMachineRow.GetCell(4).SetCellValue(machine.Name);
                }

                rowNum++;
                crtStepNumber++;
            }
        }

        /// <summary>
        /// Fills the "Other Commodities" table of an assembly report.
        /// </summary>
        /// <param name="sheet">The excel sheet to fill.</param>
        /// <param name="result">The assembly's cost calculation result.</param>
        /// <param name="rowNum">The number of the row where to start inserting data.</param>
        private void FillAssemblyReportOtherCommodities(ISheet sheet, CalculationResult result, int rowNum)
        {
            // Add the consumables to the table
            foreach (ConsumableCost cost in result.ConsumablesCost.ConsumableCosts.OrderBy(c => c.Name))
            {
                IRow crtRow = sheet.GetRow(rowNum);
                crtRow.GetCell(21).SetCellValue(cost.Name);
                crtRow.GetCell(22).SetCellValue(cost.Description);
                crtRow.GetCell(26).SetCellValue(cost.Manufacturer);
                crtRow.GetCell(28).SetCellValue(cost.Amount);
                crtRow.GetCell(30).SetCellValue(cost.AmountUnitBaseSymbol);
                crtRow.GetCell(31).SetCellValue(cost.Price);
                crtRow.GetCell(32).SetCellValue(cost.TotalCost);

                rowNum++;
            }
        }

        #endregion Assembly Report Xls

        #region Assembly Report Pdf

        /// <summary>
        /// Generate the Assembly Report in the PDF template
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <param name="result">The cost calculation to export.</param>
        /// <param name="path">The path where to save the excel file on the disk.</param>
        public void CreateAssemblyReportPdf(
            Assembly assembly,
            CalculationResult result,
            string path)
        {
            MemoryStream memoryStream = null;

            try
            {
                memoryStream = new MemoryStream();
                float pageMargin = 10f;
                Document document = new Document(new Rectangle(1348, 1014), pageMargin, pageMargin, pageMargin, pageMargin);
                PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
                document.Open();

                // fill generic Assembly info
                FillPDFReportGenericInformation(document, assembly);

                // create PDF layout structure
                PdfPTable mainTable = new PdfPTable(new float[] { 57, 43 });
                mainTable.WidthPercentage = 100;
                mainTable.DefaultCell.Padding = 0;
                PdfPTable sideTable = new PdfPTable(1);
                sideTable.DefaultCell.Border = Table.NO_BORDER;
                sideTable.DefaultCell.Padding = 0;

                // fill assembly data
                FillAssemblyPDFReportAssemblyData(sideTable, assembly, result);

                // fill Assembly process steps
                sideTable.AddCell(new PdfPCell(new Phrase(LocalizedResources.Report_Steps, pdfReportFontColumnHeaderBold))
                {
                    Colspan = sideTable.NumberOfColumns,
                    BackgroundColor = pdfReportHeaderColorDark,
                    HorizontalAlignment = Table.ALIGN_CENTER
                });
                PdfPTable processStepsTable = new PdfPTable(1);
                processStepsTable.DefaultCell.Padding = 0;
                processStepsTable.DefaultCell.Border = Table.NO_BORDER;

                var assyCalcAccuracy = (PartCalculationAccuracy)assembly.CalculationAccuracy.GetValueOrDefault(PartCalculationAccuracy.FineCalculation);
                if (assyCalcAccuracy == PartCalculationAccuracy.FineCalculation)
                {
                    FillPDFReportProcessSteps(processStepsTable, assembly.Process, assembly.GetType(), 9);
                }
                else
                {
                    processStepsTable.AddCell(" ");
                }

                sideTable.DefaultCell.Padding = 0;
                sideTable.AddCell(processStepsTable);

                // fill Assembly process steps details
                FillAssemblyPDFReportStepDetails(sideTable, assembly, result);

                sideTable.AddCell(string.Empty);
                mainTable.AddCell(sideTable);
                sideTable = new PdfPTable(1);
                sideTable.DefaultCell.Border = Table.NO_BORDER;
                sideTable.DefaultCell.Padding = 0;

                // fill part list
                FillAssemblyPDFReportPartList(sideTable, result);

                // fill subassemblies and commodities
                FillAssemblyPDFReportSubassembliesAndCommodities(sideTable, assembly, result);

                // fill Assembly consumables
                FillAssemblyPDFReportConsumables(sideTable, result);

                sideTable.AddCell(string.Empty);
                mainTable.AddCell(sideTable);

                document.Add(mainTable);

                // fill price data
                FillPDFReportPriceData(document, assembly.TargetPrice, result);

                document.Close();
                File.WriteAllBytes(path, memoryStream.GetBuffer());
            }
            catch (Exception ex)
            {
                log.ErrorException("Error while exporting Assembly report to PDF.", ex);
            }
            finally
            {
                if (memoryStream != null)
                {
                    memoryStream.Close();
                }
            }
        }

        /// <summary>
        /// Fill Assembly Data in the pdf document.
        /// </summary>
        /// <param name="parentTable">The PDF table to fill.</param>
        /// <param name="assembly">The Assembly object.</param>
        /// <param name="assemblyCalculationResult">The calculation result containing data.</param>
        private void FillAssemblyPDFReportAssemblyData(PdfPTable parentTable, Assembly assembly, CalculationResult assemblyCalculationResult)
        {
            PdfCellLayoutEvent cellSpacingLayoutEvent = new PdfCellLayoutEvent(new Thickness(5d, 0d, 5d, 0d), Color.WHITE);
            PdfCellLayoutEvent cellSpacingLayoutEventColored = new PdfCellLayoutEvent(new Thickness(5d, 0d, 5d, 0d), pdfReportHeaderColor);

            // add table header
            parentTable.AddCell(new PdfPCell(new Phrase(LocalizedResources.Report_AssemblyData, pdfReportFontColumnHeaderBold))
                {
                    Colspan = parentTable.NumberOfColumns,
                    BackgroundColor = pdfReportHeaderColorDark,
                    HorizontalAlignment = Table.ALIGN_CENTER
                });

            PdfPTable containerTable = new PdfPTable(new float[] { 20f, 80f });
            containerTable.DefaultCell.Border = Table.NO_BORDER;
            containerTable.HorizontalAlignment = Table.ALIGN_CENTER;

            // fill assembly picture
            PdfPTable pictureTable = new PdfPTable(1);
            pictureTable.WidthPercentage = 90;

            // workaround for cell spacing top
            pictureTable.DefaultCell.BorderColor = Color.WHITE;
            pictureTable.AddCell(string.Empty);
            pictureTable.DefaultCell.BorderColor = Color.BLACK;

            Media picture = this.GetPictureForReport(assembly);
            if (picture != null)
            {
                FillPDFReportPicture(pictureTable, picture);
            }
            else
            {
                pictureTable.AddCell(" ");
            }

            // workaround for the size of the cell containing the image
            pictureTable.DefaultCell.DisableBorderSide(Table.LEFT_BORDER);
            pictureTable.DefaultCell.DisableBorderSide(Table.RIGHT_BORDER);
            pictureTable.DefaultCell.DisableBorderSide(Table.BOTTOM_BORDER);
            pictureTable.AddCell(string.Empty);

            containerTable.AddCell(pictureTable);

            // Create the enhanced cost breakdown from which to get the raw material related costs.
            var enhancedResult = ReportsHelper.CreateEnhancedCostBreakdown(assemblyCalculationResult);

            // Fill the info and costs table
            PdfPTable table = new PdfPTable(new float[] { 25f, 25f, 30f, 20f });
            table.WidthPercentage = 100;
            table.SpacingAfter = 10;
            table.DefaultCell.Border = Table.NO_BORDER;
            table.DefaultCell.PaddingLeft = 7;
            table.DefaultCell.PaddingRight = 7;
            table.DefaultCell.HorizontalAlignment = Table.ALIGN_LEFT;
            table.DefaultCell.VerticalAlignment = Table.ALIGN_MIDDLE;
            table.DefaultCell.CellEvent = cellSpacingLayoutEventColored;

            // Empty row for padding
            table.AddCell(new PdfPCell(table.DefaultCell) { Colspan = table.NumberOfColumns, CellEvent = null });

            table.AddCell(new Phrase(LocalizedResources.Report_AssemblyName, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_CENTER,
                Colspan = 3,
                Phrase = new Phrase(assembly.Name, pdfReportFontSmall)
            });

            // Empty row
            table.AddCell(new PdfPCell(table.DefaultCell) { Colspan = table.NumberOfColumns, CellEvent = null, Phrase = new Phrase(" ", pdfReportFontSmall) });

            table.AddCell(new Phrase(LocalizedResources.General_Location, pdfReportFontColumnHeader));
            string location = (assembly.AssemblingCountry + ", " + assembly.AssemblingSupplier).TrimEnd(',', ' ');
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_CENTER,
                Phrase = new Phrase(location, pdfReportFontSmall)
            });

            var calcAccuracy = (PartCalculationAccuracy)assembly.CalculationAccuracy.GetValueOrDefault(PartCalculationAccuracy.FineCalculation);
            table.AddCell(new Phrase(LocalizedResources.Report_CalcAccuracy, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_CENTER,
                Phrase = new Phrase(UIUtils.GetEnumValueLabel(calcAccuracy), pdfReportFontSmall)
            });

            // Empty row
            table.AddCell(new PdfPCell(table.DefaultCell) { Colspan = table.NumberOfColumns, CellEvent = null, Phrase = new Phrase(" ", pdfReportFontSmall) });

            table.AddCell(new Phrase(LocalizedResources.Report_MaterialCost, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(enhancedResult.Summary.RawMaterialCost), pdfReportFontSmall)
            });

            table.AddCell(new Phrase(LocalizedResources.Report_ConsumableOrCommodityCost, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(assemblyCalculationResult.Summary.ConsumableCost + assemblyCalculationResult.Summary.CommoditiesCost), pdfReportFontSmall)
            });

            table.AddCell(new Phrase(LocalizedResources.Report_MaterialOHCost, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(enhancedResult.OverheadCost.RawMaterialOverhead), pdfReportFontSmall)
            });

            table.AddCell(new Phrase(LocalizedResources.Report_AssemblingCost, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(assemblyCalculationResult.Summary.TotalManufacturingCost), pdfReportFontSmall)
            });

            // Next 2 cells are empty.
            table.AddCell(new PdfPCell(table.DefaultCell) { Colspan = 2, CellEvent = null });

            table.AddCell(new Phrase(LocalizedResources.Report_RejectCost, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(assemblyCalculationResult.Summary.ManufacturingRejectCost), pdfReportFontSmall)
            });

            table.AddCell(new Phrase(LocalizedResources.Report_MachineCost, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(assemblyCalculationResult.ProcessCost.MachineCostSum), pdfReportFontSmall)
            });

            table.AddCell(new Phrase(LocalizedResources.Report_SubPartOrAssemblyCost, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(assemblyCalculationResult.Summary.SubpartsAndSubassembliesCost), pdfReportFontSmall)
            });

            table.AddCell(new Phrase(LocalizedResources.Report_SetupCost, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(assemblyCalculationResult.ProcessCost.SetupCostSum), pdfReportFontSmall)
            });

            table.AddCell(new Phrase(LocalizedResources.Report_DevelopmentAndInvest, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(assemblyCalculationResult.Summary.DevelopmentCost + assemblyCalculationResult.Summary.ProjectInvest), pdfReportFontSmall)
            });

            table.AddCell(new Phrase(LocalizedResources.Report_DirectLaborCost, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(assemblyCalculationResult.ProcessCost.DirectLabourCostSum), pdfReportFontSmall)
            });

            table.AddCell(new Phrase(LocalizedResources.Report_OtherCost, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(assemblyCalculationResult.Summary.OtherCost), pdfReportFontSmall)
            });

            table.AddCell(new Phrase(LocalizedResources.Report_ToolOrDieCost, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(assemblyCalculationResult.ProcessCost.ToolAndDieCostSum), pdfReportFontSmall)
            });

            table.AddCell(new Phrase(LocalizedResources.Report_PackagingCost, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(assemblyCalculationResult.Summary.PackagingCost), pdfReportFontSmall)
            });

            table.AddCell(new Phrase(LocalizedResources.Report_OHAssembling, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(assemblyCalculationResult.ProcessCost.ManufacturingOverheadSum), pdfReportFontSmall)
            });

            table.AddCell(new Phrase(LocalizedResources.Report_TransportAndLogisticCost, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(assemblyCalculationResult.Summary.TransportCost + assemblyCalculationResult.Summary.LogisticCost), pdfReportFontSmall)
            });

            table.AddCell(new Phrase(LocalizedResources.Report_RejectCost, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(assemblyCalculationResult.ProcessCost.RejectCostSum), pdfReportFontSmall)
            });

            table.AddCell(new Phrase(LocalizedResources.Report_OverheadAndMarginCost, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(assemblyCalculationResult.OverheadCost.TotalSumOverheadAndMargin), pdfReportFontSmall)
            });

            table.AddCell(new Phrase(LocalizedResources.Report_nTierTransportCost, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(assemblyCalculationResult.ProcessCost.ManufacturingTransportCost), pdfReportFontSmall)
            });

            table.AddCell(new Phrase(LocalizedResources.Report_PaymentCost, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(assemblyCalculationResult.Summary.PaymentCost), pdfReportFontSmall)
            });

            // Empty row
            table.AddCell(new PdfPCell(table.DefaultCell) { Colspan = table.NumberOfColumns, CellEvent = null, Phrase = new Phrase(" ", pdfReportFontSmall) });

            table.AddCell(new Phrase(LocalizedResources.Report_TotalAssemblingCost, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(assemblyCalculationResult.ProcessCost.TotalManufacturingCostSum), pdfReportFontSmall)
            });

            table.AddCell(new Phrase(LocalizedResources.Report_TargetSalesPrice, pdfReportFontColumnHeader));
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                CellEvent = cellSpacingLayoutEvent,
                HorizontalAlignment = Table.ALIGN_RIGHT,
                Phrase = new Phrase(Formatter.FormatMoney(assemblyCalculationResult.Summary.TargetCost), pdfReportFontSmall)
            });

            table.DefaultCell.CellEvent = null;
            table.AddCell(string.Empty);
            table.CompleteRow();

            containerTable.AddCell(table);
            parentTable.AddCell(containerTable);
        }

        /// <summary>
        /// Fill Assembly Parts list in the pdf document.
        /// </summary>
        /// <param name="parentTable">The PDF table to fill.</param>
        /// <param name="assemblyCalculationResult">The assemblyCalculationResult object containing values.</param>
        private void FillAssemblyPDFReportPartList(PdfPTable parentTable, CalculationResult assemblyCalculationResult)
        {
            // add table header
            parentTable.AddCell(new PdfPCell(new Phrase(LocalizedResources.Report_PartList, pdfReportFontColumnHeaderBold))
            {
                Colspan = parentTable.NumberOfColumns,
                BackgroundColor = pdfReportHeaderColorDark,
                HorizontalAlignment = Table.ALIGN_CENTER
            });

            PdfPTable table = new PdfPTable(new float[5] { 21, 40, 13, 13, 13 });
            table.DefaultCell.Border = Table.NO_BORDER;
            table.HorizontalAlignment = Table.ALIGN_LEFT;

            // add column headers
            table.DefaultCell.BackgroundColor = pdfReportHeaderColor;
            table.DefaultCell.Border = Table.NO_BORDER;
            table.AddCell(new Phrase(LocalizedResources.Report_Name, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_Description, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_Amount, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_Cost, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_TotalCost, pdfReportFontColumnHeaderBold));

            // fill parts required information
            table.DefaultCell.BackgroundColor = Color.WHITE;
            table.DefaultCell.BorderColor = Color.LIGHT_GRAY;
            table.DefaultCell.Border = Table.RIGHT_BORDER | Table.BOTTOM_BORDER;
            foreach (PartCost partCost in assemblyCalculationResult.PartsCost.PartCosts.OrderBy(c => c.PartIndex))
            {
                table.DefaultCell.EnableBorderSide(Table.RIGHT_BORDER);
                table.DefaultCell.HorizontalAlignment = Table.ALIGN_LEFT;

                table.AddCell(new Phrase(partCost.Name, pdfReportFontSmall));
                table.AddCell(new Phrase(!string.IsNullOrEmpty(partCost.Description) ? partCost.Description : this.emptyCellSymbol, pdfReportFontSmall));

                table.DefaultCell.HorizontalAlignment = Table.ALIGN_RIGHT;
                table.AddCell(new Phrase(Formatter.FormatInteger(partCost.AmountPerAssembly), pdfReportFontSmall));
                table.AddCell(new Phrase(Formatter.FormatMoney(partCost.TargetCost), pdfReportFontSmall));
                table.DefaultCell.DisableBorderSide(Table.RIGHT_BORDER);
                table.AddCell(new PdfPCell(table.DefaultCell)
                    {
                        Phrase = new Phrase(Formatter.FormatMoney(partCost.TargetCost * partCost.AmountPerAssembly), pdfReportFontSmallBold),
                        BackgroundColor = this.pdfGreyCellBackground
                    });
            }

            parentTable.DefaultCell.Padding = 5;
            parentTable.AddCell(table);
        }

        /// <summary>
        /// Fill Assembly / Fixing and Bought in Parts with subassemblies and commodities in the pdf document.
        /// </summary>
        /// <param name="parentTable">The root table.</param>
        /// <param name="assy">The assembly.</param>
        /// <param name="assemblyCalculationResult">The assemblyCalculationResult object containing values.</param>
        private void FillAssemblyPDFReportSubassembliesAndCommodities(PdfPTable parentTable, Assembly assy, CalculationResult assemblyCalculationResult)
        {
            // add table header
            parentTable.AddCell(new PdfPCell(new Phrase(LocalizedResources.Report_AssemblyOrFixingAndBoughtInParts, pdfReportFontColumnHeaderBold))
            {
                Colspan = parentTable.NumberOfColumns,
                BackgroundColor = pdfReportHeaderColorDark,
                HorizontalAlignment = Table.ALIGN_CENTER,
            });

            PdfPTable table = new PdfPTable(new float[6] { 21, 30, 13, 12, 12, 12 });
            table.DefaultCell.Border = Table.NO_BORDER;
            table.HorizontalAlignment = Table.ALIGN_LEFT;

            // add column headers
            table.DefaultCell.BackgroundColor = pdfReportHeaderColor;
            table.DefaultCell.Border = Table.NO_BORDER;
            table.AddCell(new Phrase(LocalizedResources.Report_Name, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_Description, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_Manufacturer, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_Amount, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_Cost, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_TotalCost, pdfReportFontColumnHeaderBold));

            // fill parts required information
            table.DefaultCell.BackgroundColor = Color.WHITE;
            table.DefaultCell.BorderColor = Color.LIGHT_GRAY;
            table.DefaultCell.Border = Table.RIGHT_BORDER | Table.BOTTOM_BORDER;
            foreach (AssemblyCost assemblyCost in assemblyCalculationResult.AssembliesCost.AssemblyCosts.OrderBy(c => c.AssemblyIndex))
            {
                Assembly subAssy = assy.Subassemblies.FirstOrDefault(s => s.Guid == assemblyCost.AssemblyId);

                table.DefaultCell.EnableBorderSide(Table.RIGHT_BORDER);
                table.DefaultCell.HorizontalAlignment = Table.ALIGN_LEFT;

                table.AddCell(new Phrase(assemblyCost.Name, pdfReportFontSmall));
                table.AddCell(new Phrase(subAssy != null ? subAssy.Description : this.emptyCellSymbol, pdfReportFontSmall));
                table.AddCell(new Phrase(subAssy != null && subAssy.Manufacturer != null ? subAssy.Manufacturer.Name : this.emptyCellSymbol, pdfReportFontSmall));

                table.DefaultCell.HorizontalAlignment = Table.ALIGN_RIGHT;
                table.AddCell(new Phrase(Formatter.FormatInteger(assemblyCost.AmountPerAssembly), pdfReportFontSmall));
                table.AddCell(new Phrase(Formatter.FormatMoney(assemblyCost.TargetCost), pdfReportFontSmall));
                table.DefaultCell.DisableBorderSide(Table.RIGHT_BORDER);
                table.AddCell(new PdfPCell(table.DefaultCell)
                    {
                        Phrase = new Phrase(Formatter.FormatMoney(assemblyCost.TargetCost * assemblyCost.AmountPerAssembly), pdfReportFontSmallBold),
                        BackgroundColor = this.pdfGreyCellBackground
                    });
            }

            // fill commodities required information
            foreach (CommodityCost commodityCost in assemblyCalculationResult.CommoditiesCost.CommodityCosts.OrderBy(c => c.Name))
            {
                table.DefaultCell.EnableBorderSide(Table.RIGHT_BORDER);
                table.DefaultCell.HorizontalAlignment = Table.ALIGN_LEFT;

                table.AddCell(new Phrase(commodityCost.Name, pdfReportFontSmall));
                table.AddCell(new Phrase(commodityCost.Remarks, pdfReportFontSmall));
                table.AddCell(new Phrase(commodityCost.Manufacturer, pdfReportFontSmall));

                table.DefaultCell.HorizontalAlignment = Table.ALIGN_RIGHT;
                table.AddCell(new Phrase(Formatter.FormatInteger(commodityCost.Amount), pdfReportFontSmall));
                table.AddCell(new Phrase(Formatter.FormatMoney(commodityCost.Price), pdfReportFontSmall));
                table.DefaultCell.DisableBorderSide(Table.RIGHT_BORDER);
                table.AddCell(new PdfPCell(table.DefaultCell)
                    {
                        Phrase = new Phrase(Formatter.FormatMoney(commodityCost.TotalCost), pdfReportFontSmallBold),
                        BackgroundColor = pdfGreyCellBackground
                    });
            }

            parentTable.DefaultCell.Padding = 5;
            parentTable.AddCell(table);
        }

        /// <summary>
        /// Fill Assembly Other Consumables in the pdf document.
        /// </summary>
        /// <param name="parentTable">The PDF table to fill.</param>
        /// <param name="assemblyCalculationResult">The assemblyCalculationResult object containing values.</param>
        private void FillAssemblyPDFReportConsumables(PdfPTable parentTable, CalculationResult assemblyCalculationResult)
        {
            // add table header
            parentTable.AddCell(new PdfPCell(new Phrase(LocalizedResources.Report_OtherCommodities, pdfReportFontColumnHeaderBold))
            {
                Colspan = parentTable.NumberOfColumns,
                BackgroundColor = pdfReportHeaderColorDark,
                HorizontalAlignment = Table.ALIGN_CENTER,
            });

            PdfPTable table = new PdfPTable(new float[7] { 21, 30, 13, 10, 6, 10, 10 });
            table.DefaultCell.Border = Table.NO_BORDER;
            table.HorizontalAlignment = Table.ALIGN_LEFT;

            // add column headers
            table.DefaultCell.BackgroundColor = pdfReportHeaderColor;
            table.DefaultCell.Border = Table.NO_BORDER;
            table.AddCell(new Phrase(LocalizedResources.Report_Name, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_Description, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_Manufacturer, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_Amount, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_Unit, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_Price, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_TotalCost, pdfReportFontColumnHeaderBold));

            // fill Consumables required information
            table.DefaultCell.BackgroundColor = Color.WHITE;
            table.DefaultCell.BorderColor = Color.LIGHT_GRAY;
            table.DefaultCell.Border = Table.RIGHT_BORDER | Table.BOTTOM_BORDER;
            foreach (ConsumableCost consumableCost in assemblyCalculationResult.ConsumablesCost.ConsumableCosts)
            {
                table.DefaultCell.EnableBorderSide(Table.RIGHT_BORDER);
                table.DefaultCell.HorizontalAlignment = Table.ALIGN_LEFT;

                table.AddCell(new Phrase(consumableCost.Name, pdfReportFontSmall));
                table.AddCell(new Phrase(consumableCost.Description, pdfReportFontSmall));
                table.AddCell(new Phrase(consumableCost.Manufacturer, pdfReportFontSmall));

                table.DefaultCell.HorizontalAlignment = Table.ALIGN_RIGHT;
                table.AddCell(new Phrase(Formatter.FormatNumber(consumableCost.Amount), pdfReportFontSmall));

                table.DefaultCell.HorizontalAlignment = Table.ALIGN_LEFT;
                table.AddCell(new Phrase(consumableCost.AmountUnitBaseSymbol, pdfReportFontSmall));

                table.DefaultCell.HorizontalAlignment = Table.ALIGN_RIGHT;
                table.AddCell(new Phrase(Formatter.FormatMoney(consumableCost.Price), pdfReportFontSmall));
                table.DefaultCell.DisableBorderSide(Table.RIGHT_BORDER);
                table.AddCell(new PdfPCell(table.DefaultCell)
                    {
                        Phrase = new Phrase(Formatter.FormatMoney(consumableCost.TotalCost), pdfReportFontSmallBold),
                        BackgroundColor = this.pdfGreyCellBackground
                    });
            }

            parentTable.DefaultCell.Padding = 5;
            parentTable.AddCell(table);
        }

        /// <summary>
        /// Fill Assembly Step Details in the pdf document.
        /// </summary>
        /// <param name="parentTable">The PDF table to fill.</param>
        /// <param name="assembly">The assembly object.</param>
        /// <param name="assemblyCalculationResult">The assembly calculation result.</param>
        private void FillAssemblyPDFReportStepDetails(PdfPTable parentTable, Assembly assembly, CalculationResult assemblyCalculationResult)
        {
            if (parentTable == null ||
                assembly == null || assembly.Process == null || assembly.Process.Steps == null ||
                assemblyCalculationResult == null ||
                assemblyCalculationResult.ProcessCost == null ||
                assemblyCalculationResult.ProcessCost.StepCosts == null)
            {
                return;
            }

            // add table header
            parentTable.AddCell(new PdfPCell(new Phrase(LocalizedResources.Report_StepDetails, pdfReportFontColumnHeaderBold))
            {
                Colspan = parentTable.NumberOfColumns,
                BackgroundColor = pdfReportHeaderColorDark,
                HorizontalAlignment = Table.ALIGN_CENTER
            });

            // add step details table
            PdfPTable table = new PdfPTable(new float[] { 4f, 20f, 19f, 6f, 8f, 8f, 6f, 10f, 7f, 6f, 7f });
            table.DefaultCell.Border = Table.NO_BORDER;
            table.DefaultCell.BackgroundColor = pdfReportHeaderColor;
            table.AddCell(new Phrase(LocalizedResources.Report_Step, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_NameOrDescription, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_MachineOrEqName, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_MachineCost, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_SetupCost, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_DirectLabor, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_RejectCost, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_ManufacturingCost, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_CycleTime, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_PartsOrCycle, pdfReportFontColumnHeader));
            table.AddCell(new Phrase(LocalizedResources.Report_TotalCost, pdfReportFontColumnHeaderBold));
            table.DefaultCell.BorderColor = Color.LIGHT_GRAY;
            table.DefaultCell.Border = Table.BOTTOM_BORDER | Table.RIGHT_BORDER;
            table.DefaultCell.BackgroundColor = Color.WHITE;

            var assyCalcAccuracy = (PartCalculationAccuracy)assembly.CalculationAccuracy.GetValueOrDefault(PartCalculationAccuracy.FineCalculation);
            if (assyCalcAccuracy != PartCalculationAccuracy.FineCalculation)
            {
                parentTable.DefaultCell.Padding = 5;
                parentTable.AddCell(table);
                return;
            }

            int stepNo = 0;
            foreach (ProcessStep step in assembly.Process.Steps.OrderBy(ps => ps.Index))
            {
                stepNo++;

                // get process step calculation from ID
                ProcessStepCost currentMpsc = assemblyCalculationResult.ProcessCost.StepCosts.FirstOrDefault(c => c.StepId == step.Guid);

                // get machines information - there will be one row for each machine
                List<Machine> machines = step.Machines.Where(m => !m.IsDeleted).ToList();
                Machine firstMachine = machines.Any() ? machines[0] : null;

                table.DefaultCell.EnableBorderSide(Table.RIGHT_BORDER);

                table.DefaultCell.HorizontalAlignment = Table.ALIGN_CENTER;
                table.AddCell(new Phrase(stepNo.ToString(), pdfReportFontSmall));

                table.DefaultCell.HorizontalAlignment = Table.ALIGN_LEFT;
                table.AddCell(new Phrase(step.Name, pdfReportFontSmall));
                table.AddCell(new Phrase(firstMachine != null ? firstMachine.Name : this.emptyCellSymbol, pdfReportFontSmall));

                table.DefaultCell.HorizontalAlignment = Table.ALIGN_RIGHT;
                if (currentMpsc != null)
                {
                    table.AddCell(new Phrase(Formatter.FormatMoney(currentMpsc.MachineCost), pdfReportFontSmall));
                    table.AddCell(new Phrase(Formatter.FormatMoney(currentMpsc.SetupCost), pdfReportFontSmall));
                    table.AddCell(new Phrase(Formatter.FormatMoney(currentMpsc.DirectLabourCost), pdfReportFontSmall));
                    table.AddCell(new Phrase(Formatter.FormatNumber(currentMpsc.RejectCost, 2), pdfReportFontSmall));
                    table.AddCell(new Phrase(Formatter.FormatMoney(currentMpsc.ManufacturingCost), pdfReportFontSmall));
                }
                else
                {
                    table.AddCell(string.Empty);
                    table.AddCell(string.Empty);
                    table.AddCell(string.Empty);
                    table.AddCell(string.Empty);
                    table.AddCell(string.Empty);
                }

                table.AddCell(new Phrase(Formatter.FormatNumber(step.CycleTime, 4), pdfReportFontSmall));

                table.DefaultCell.DisableBorderSide(Table.RIGHT_BORDER);
                table.AddCell(new Phrase(Formatter.FormatInteger(step.PartsPerCycle), pdfReportFontSmall));

                if (currentMpsc != null)
                {
                    table.AddCell(new PdfPCell(table.DefaultCell)
                        {
                            Phrase = new Phrase(Formatter.FormatMoney(currentMpsc.TotalManufacturingCost), pdfReportFontSmallBold),
                            BackgroundColor = this.pdfGreyCellBackground
                        });
                }
                else
                {
                    table.AddCell(new PdfPCell(table.DefaultCell) { BackgroundColor = this.pdfGreyCellBackground });
                }

                for (int mi = 1; mi < machines.Count; mi++)
                {
                    // get current machine
                    Machine machine = machines[mi];

                    table.DefaultCell.EnableBorderSide(Table.RIGHT_BORDER);
                    table.DefaultCell.HorizontalAlignment = Table.ALIGN_LEFT;

                    table.AddCell(string.Empty);
                    table.AddCell(string.Empty);
                    table.AddCell(new Phrase(machine.Name, pdfReportFontSmall));
                    table.AddCell(string.Empty);
                    table.AddCell(string.Empty);
                    table.AddCell(string.Empty);
                    table.AddCell(string.Empty);
                    table.AddCell(string.Empty);
                    table.AddCell(string.Empty);
                    table.AddCell(string.Empty);
                    table.DefaultCell.DisableBorderSide(Table.RIGHT_BORDER);
                    table.AddCell(new PdfPCell(table.DefaultCell) { BackgroundColor = this.pdfGreyCellBackground });
                }
            }

            parentTable.DefaultCell.Padding = 5;
            parentTable.AddCell(table);
        }

        #endregion Assembly Report Pdf

        #region Common Pdf Report Methods

        /// <summary>
        /// Fill part of the sheet with generic information: date, current user.
        /// </summary>
        /// <param name="document">The PDF document to fill.</param>
        /// <param name="entity">The entity from which to get data.</param>
        private void FillPDFReportGenericInformation(Document document, object entity)
        {
            string title = null;
            string calcVersion = null;

            Assembly assy = entity as Assembly;
            if (assy != null)
            {
                title = LocalizedResources.Report_AssemblyDataSummaryTitle;
                calcVersion = assy.CalculationVariant;
            }
            else
            {
                Part part = entity as Part;
                if (part != null)
                {
                    title = LocalizedResources.Report_PartDataSummaryTitle;
                    calcVersion = part.CalculationVariant;
                }
            }

            // create PDF table
            PdfPTable table = new PdfPTable(new float[] { 42, 10, 19, 10, 10, 10, 19 });
            table.WidthPercentage = 100;
            table.SpacingAfter = 10;
            table.DefaultCell.VerticalAlignment = Table.ALIGN_MIDDLE;

            // add cell 1
            table.DefaultCell.HorizontalAlignment = Table.ALIGN_CENTER;
            table.AddCell(new Phrase(title, pdfReportFontBold));

            // add cell 2
            table.DefaultCell.HorizontalAlignment = Table.ALIGN_LEFT;
            table.AddCell(new Phrase(LocalizedResources.Report_CompletedBy, pdfReportFontBold));

            // add username (cell 3)
            table.AddCell(new Phrase(SecurityManager.Instance.CurrentUser == null ? string.Empty : SecurityManager.Instance.CurrentUser.Name, pdfReportFont));

            // Cell 4 - version label
            table.AddCell(new Phrase(LocalizedResources.Report_Version, pdfReportFontBold));

            // Cell 5 - the calculation version number
            table.DefaultCell.HorizontalAlignment = Table.ALIGN_CENTER;
            table.AddCell(new Phrase(calcVersion, pdfReportFontBold));

            // add cell 6
            table.DefaultCell.HorizontalAlignment = Table.ALIGN_LEFT;
            table.AddCell(new Phrase(LocalizedResources.Report_Date, pdfReportFontBold));

            // add date (cell 7)
            table.DefaultCell.HorizontalAlignment = Table.ALIGN_CENTER;
            DateTime dt = DateTime.Now;
            table.AddCell(new Phrase(string.Format("{0} {1} {2}", dt.Day, dt.ToString("MMMM"), dt.Year), pdfReportFont));

            // add the table to the PDF document
            document.Add(table);
        }

        /// <summary>
        /// Fill pdf report with process steps.
        /// Create the pictures for every step and add them to the pdf report.
        /// </summary>
        /// <param name="parentTable">The PDF table to fill.</param>
        /// <param name="process">The process steps instance.</param>
        /// <param name="entityType">The type of the entity (Assembly or Part) for which to draw the steps.</param>
        /// <param name="maxStepsPerRow">Indicates the maximum number of process steps that can be displayed per line.</param>
        private void FillPDFReportProcessSteps(PdfPTable parentTable, Process process, Type entityType, int maxStepsPerRow)
        {
            // check if the part has process steps to display
            if (process == null || process.Steps.Count == 0)
            {
                parentTable.DefaultCell.Border = Table.LEFT_BORDER | Table.BOTTOM_BORDER | Table.RIGHT_BORDER;
                parentTable.AddCell(" ");
                return;
            }

            PdfPTable table = new PdfPTable(maxStepsPerRow);
            table.WidthPercentage = 100;

            // Get process step image
            string imageResourceName = null;
            if (entityType == typeof(Assembly) || entityType.IsSubclassOf(typeof(Assembly)))
            {
                imageResourceName = "AssemblyProcessStep.png";
            }
            else if (entityType == typeof(Part) || entityType.IsSubclassOf(typeof(Part)))
            {
                imageResourceName = "PartProcessStep.png";
            }
            else
            {
                log.Error("Entity type '{0}' does not have a process step widget image associated to it.", entityType != null ? entityType.Name : string.Empty);
            }

            Image stepWidgetImage = GetImageForPDFReport(imageResourceName);

            // Iterate process steps and display info
            IEnumerable<ProcessStep> processSteps = process.Steps.OrderBy(ps => ps.Index);
            int i = 0;
            foreach (ProcessStep step in processSteps)
            {
                i++;
                PdfPTable stepTable = new PdfPTable(new float[] { 18, 82 });
                stepTable.TableEvent = new PdfTableLayoutEvent(stepWidgetImage);
                stepTable.DefaultCell.Border = iTextSharp.text.Cell.NO_BORDER;

                // format step name
                int maxRowsPerStepImage = (int)stepWidgetImage.Height / 14;
                int maxChractersPerRow = (int)((stepWidgetImage.Width - 18f) / 6.2f);
                string stepNameFormatted = string.Empty;
                int j;
                for (j = 0; j < step.Name.Length / maxChractersPerRow && j < maxRowsPerStepImage; j++)
                {
                    stepNameFormatted += step.Name.Substring(j * maxChractersPerRow, maxChractersPerRow) + "\r\n";
                }

                if (j < maxRowsPerStepImage)
                {
                    stepNameFormatted += step.Name.Substring(j * maxChractersPerRow, step.Name.Length - (j * maxChractersPerRow));
                    j++;
                }

                // compensate padding (and implicitly cell size and text positioning) to match processStepImage size
                float stepNumberPadding = i > 9 ? 3f : 6f;
                float stepNamePadding = (maxRowsPerStepImage - j) * 5;
                stepTable.DefaultCell.PaddingLeft = stepNumberPadding;
                stepTable.AddCell(new Phrase(Formatter.FormatInteger(i), pdfReportFontSmall));
                stepTable.DefaultCell.PaddingLeft = 0;
                stepTable.DefaultCell.PaddingTop = stepNamePadding + 3.7f;
                stepTable.DefaultCell.PaddingBottom = stepNamePadding + 5;
                stepTable.AddCell(new Phrase(stepNameFormatted, pdfReportFontSmall));

                table.DefaultCell.DisableBorderSide(PdfPCell.TOP_BORDER);
                table.DefaultCell.DisableBorderSide(PdfPCell.LEFT_BORDER);
                table.DefaultCell.DisableBorderSide(PdfPCell.RIGHT_BORDER);
                table.DefaultCell.DisableBorderSide(PdfPCell.BOTTOM_BORDER);

                // if first step in the row
                if (i % maxStepsPerRow == 1)
                {
                    table.DefaultCell.EnableBorderSide(PdfPCell.LEFT_BORDER);
                }

                // if last step in the row
                if (i % maxStepsPerRow == 0)
                {
                    table.DefaultCell.EnableBorderSide(PdfPCell.RIGHT_BORDER);
                }

                // if last row
                if (i / maxStepsPerRow == processSteps.Count() / maxStepsPerRow && i % maxStepsPerRow > 0)
                {
                    table.DefaultCell.EnableBorderSide(PdfPCell.BOTTOM_BORDER);
                }

                table.AddCell(stepTable);
            }

            // complete last row
            table.DefaultCell.DisableBorderSide(PdfPCell.LEFT_BORDER);
            for (int j = i; j % maxStepsPerRow < maxStepsPerRow - 1; j++)
            {
                table.AddCell(string.Empty);
            }

            if (i % maxStepsPerRow < maxStepsPerRow)
            {
                table.DefaultCell.EnableBorderSide(PdfPCell.RIGHT_BORDER);
                table.AddCell(string.Empty);
            }

            parentTable.AddCell(table);
        }

        /// <summary>
        /// Fill the pdf document with Manufacturing summary.
        /// </summary>
        /// <param name="document">The PDF document to fill.</param>
        /// <param name="targetPrice">The target price to be written in the document.</param>
        /// <param name="calculationResult">The (part or assembly) calculation result.</param>
        private void FillPDFReportPriceData(Document document, decimal? targetPrice, CalculationResult calculationResult)
        {
            PdfPTable containerTable = new PdfPTable(new float[] { 12, 19, 38, 31 });
            containerTable.WidthPercentage = 100;
            containerTable.DefaultCell.BorderColor = Color.WHITE;
            containerTable.DefaultCell.Padding = 0;
            containerTable.DefaultCell.PaddingRight = 1;
            containerTable.SpacingBefore = 20;

            // add currency used
            PdfPTable table = new PdfPTable(new float[] { 35, 65 });
            table.WidthPercentage = 100;
            table.DefaultCell.PaddingTop = 0;
            table.DefaultCell.PaddingBottom = 3;
            table.DefaultCell.HorizontalAlignment = Table.ALIGN_CENTER;
            table.AddCell(new Phrase(this.GetUICurrencySymbol(), pdfReportFontBold));
            table.DefaultCell.HorizontalAlignment = Table.ALIGN_LEFT;
            table.AddCell(new Phrase(LocalizedResources.Report_CurrencyUsed, pdfReportFont));

            // workaround to keep "Currency Used" row to correct height
            table.DefaultCell.Border = Table.NO_BORDER;
            table.AddCell(string.Empty);
            table.AddCell(string.Empty);

            containerTable.AddCell(table);

            // add empty cell to keep the logo image in the center of the page
            containerTable.AddCell(string.Empty);

            // add logo image
            table = new PdfPTable(1);
            table.DefaultCell.Border = Table.NO_BORDER;
            Image logo = GetReportLogoForPDFReport();
            table.AddCell(logo);
            table.TotalWidth = 70f;
            table.LockedWidth = true;
            containerTable.AddCell(table);

            // add benchmark price and validated price
            containerTable.DefaultCell.BorderColor = Color.BLACK;
            containerTable.DefaultCell.Padding = 10;

            table = new PdfPTable(2);
            table.WidthPercentage = 100;
            table.DefaultCell.PaddingTop = 0;

            // add "Price Data" header
            table.DefaultCell.Colspan = table.NumberOfColumns;
            table.DefaultCell.PaddingBottom = 10;
            table.DefaultCell.HorizontalAlignment = Table.ALIGN_CENTER;
            table.DefaultCell.BorderColor = Color.WHITE;
            table.AddCell(new Phrase(LocalizedResources.Report_PriceData, pdfReportFontBold));
            table.DefaultCell.Colspan = 1;
            table.DefaultCell.PaddingBottom = 3;
            table.DefaultCell.BorderColor = Color.BLACK;

            decimal? benchmarkPrice = null;
            if (targetPrice.HasValue)
            {
                benchmarkPrice = CurrencyConversionManager.ConvertBaseValueToDisplayValue(targetPrice.Value, this.measurementUnitsAdapter.BaseCurrency.ExchangeRate, this.measurementUnitsAdapter.UICurrency.ConversionFactor);
            }

            table.DefaultCell.HorizontalAlignment = Table.ALIGN_LEFT;
            table.DefaultCell.BackgroundColor = pdfReportHeaderColor;
            table.AddCell(new Phrase(LocalizedResources.Report_ShouldCostBenchmarkPrice, pdfReportFontColumnHeaderBold));
            table.DefaultCell.HorizontalAlignment = Table.ALIGN_RIGHT;
            table.DefaultCell.BackgroundColor = Color.WHITE;
            table.AddCell(new Phrase(benchmarkPrice != null ? Formatter.FormatMoney(benchmarkPrice) : this.emptyCellSymbol, pdfReportFont));

            // add spacing between price rows
            table.DefaultCell.DisableBorderSide(Table.LEFT_BORDER);
            table.DefaultCell.DisableBorderSide(Table.RIGHT_BORDER);
            table.DefaultCell.PaddingBottom = 10;
            table.AddCell(string.Empty);
            table.AddCell(string.Empty);
            table.DefaultCell.EnableBorderSide(Table.LEFT_BORDER);
            table.DefaultCell.EnableBorderSide(Table.RIGHT_BORDER);
            table.DefaultCell.PaddingBottom = 3;

            table.DefaultCell.HorizontalAlignment = Table.ALIGN_LEFT;
            table.DefaultCell.BackgroundColor = pdfReportHeaderColor;
            table.AddCell(new Phrase(LocalizedResources.Report_ShouldCostValidatedPrice, pdfReportFontColumnHeaderBold));
            table.DefaultCell.HorizontalAlignment = Table.ALIGN_RIGHT;
            table.DefaultCell.BackgroundColor = Color.WHITE;
            table.AddCell(new Phrase(Formatter.FormatMoney(calculationResult.Summary.TargetCost), pdfReportFont));

            containerTable.AddCell(table);

            document.Add(containerTable);
        }

        /// <summary>
        /// Obtains the specified image as an <see cref="iTextSharp.text.Image"/> object.
        /// </summary>
        /// <param name="resourceName">The name of the resource which identifies the image to be returned (must include file extension).</param>
        /// <returns>Returns the specified resource as an <see cref="iTextSharp.text.Image"/> object;
        /// if the resource is not found - returns a blank 100x50 <see cref="iTextSharp.text.Image"/> object.</returns>
        private Image GetImageForPDFReport(string resourceName)
        {
            StreamResourceInfo streamInfo = null;
            byte[] pictureData = null;

            try
            {
                streamInfo = Application.GetResourceStream(new Uri(string.Format("Resources/Images/{0}", resourceName), UriKind.Relative));
            }
            catch (Exception ex)
            {
                log.ErrorException("Exception while obtaining an image resource for the PDF report.", ex);
            }

            if (streamInfo != null && streamInfo.Stream != null && streamInfo.Stream.Length > 0)
            {
                pictureData = new byte[streamInfo.Stream.Length];
                streamInfo.Stream.Read(pictureData, 0, (int)streamInfo.Stream.Length);
            }

            return pictureData != null ?
                Image.GetInstance(pictureData) :
                Image.GetInstance(new System.Drawing.Bitmap(100, 50), Color.WHITE);
        }

        /// <summary>
        /// Gets the report logo for the PDF reports.
        /// </summary>
        /// <returns>The image containing the report logo.</returns>
        private Image GetReportLogoForPDFReport()
        {
            byte[] pictureData = null;
            string fileExtension;

            try
            {
                ReportsHelper.GetReportLogo(out pictureData, out fileExtension);
            }
            catch (Exception ex)
            {
                log.ErrorException("Exception while obtaining an image resource for the PDF report.", ex);
            }

            // Return the logo picture if exists, or a default empty image.
            return pictureData != null ?
                Image.GetInstance(pictureData) :
                Image.GetInstance(new System.Drawing.Bitmap(100, 50), Color.WHITE);
        }

        #endregion Common Pdf Report Methods

        #region Enhanced Assembly Report Xls

        /// <summary>
        /// Creates the enhanced assembly report in xls format.
        /// </summary>
        /// <param name="assemblyCostCalculation">The assembly cost calculation.</param>
        /// <param name="reportFilePath">The report file path.</param>
        public void CreateEnhancedAssemblyReportXls(CalculationResult assemblyCostCalculation, string reportFilePath)
        {
            if (assemblyCostCalculation == null)
            {
                throw new ArgumentNullException("assemblyCostCalculation", "The cost calculation was null.");
            }

            if (string.IsNullOrEmpty(reportFilePath))
            {
                throw new ArgumentException("The file path was null or empty", "reportFilePath");
            }

            try
            {
                HSSFWorkbook workbook = null;
                using (Stream templateStream = new MemoryStream(LocalizedResources.EnhancedAssemblyReport))
                {
                    if (templateStream == null)
                    {
                        log.Error("The enhanced xls assembly report template was null.");
                    }

                    workbook = new HSSFWorkbook(templateStream);
                }

                // The 1st sheet is the sheet to fill with the report data.
                ISheet worksheet = workbook.GetSheetAt(0);

                // The 3rd sheet contains the template data
                ISheet templateSheet = workbook.GetSheetAt(2);

                // Determine the report sheet name.
                string reportSheetName = null;
                if (!string.IsNullOrEmpty(assemblyCostCalculation.PartNumber))
                {
                    reportSheetName = ReportsHelper.ValidateWorksheetName(assemblyCostCalculation.PartNumber);
                }
                else if (!string.IsNullOrEmpty(assemblyCostCalculation.Name))
                {
                    reportSheetName = ReportsHelper.ValidateWorksheetName(assemblyCostCalculation.Name);
                }

                if (!string.IsNullOrEmpty(reportSheetName))
                {
                    workbook.SetSheetName(0, reportSheetName);
                }
                else
                {
                    reportSheetName = workbook.GetSheetName(0);
                }

                // Fill the report
                FillEnhancedAssyReport(assemblyCostCalculation, workbook, worksheet, templateSheet);

                // Add the bottom logo
                worksheet.CreateRow(worksheet.LastRowNum + 1);
                worksheet.CreateRow(worksheet.LastRowNum + 1);
                ReportsHelper.AddReportBottomLogo(workbook, worksheet, 4, worksheet.LastRowNum);

                // Force the recalculation of formulas on the summary sheet.
                ISheet summarySheet = workbook.GetSheetAt(1);
                summarySheet.ForceFormulaRecalculation = true;

                // Delete the template sheet as it is no longer necessary.
                workbook.RemoveSheetAt(2);

                // Save the report
                using (FileStream file = new FileStream(reportFilePath, FileMode.Create))
                {
                    workbook.Write(file);
                }
            }
            catch (IOException ex)
            {
                throw new BusinessException(ErrorCodes.ReportGenerationIOError, ex);
            }
            catch (UnauthorizedAccessException ex)
            {
                throw new BusinessException(FileErrorCode.UnauthorizedAccess, ex);
            }
            catch (Exception ex)
            {
                log.ErrorException("Error while creating the enhanced xls assembly report.", ex);
                throw new BusinessException(ErrorCodes.ExcelReportGenerationUnknownError, ex);
            }
        }

        /// <summary>
        /// Fills the enhanced assembly report sheet.
        /// </summary>
        /// <param name="assyCostCalculation">The assembly cost calculation.</param>
        /// <param name="workbook">The workbook.</param>
        /// <param name="reportSheet">The report sheet.</param>
        /// <param name="templateSheet">The template sheet.</param>
        private void FillEnhancedAssyReport(
            CalculationResult assyCostCalculation,
            HSSFWorkbook workbook,
            ISheet reportSheet,
            ISheet templateSheet)
        {
            List<EnhancedAssyReportTableData> assembliesData = CompileEnhancedAssyReportData(assyCostCalculation, 1);

            // Fill header info
            if (SecurityManager.Instance.CurrentUser != null)
            {
                reportSheet.GetRow(2).GetCell(2).SetCellValue(SecurityManager.Instance.CurrentUser.Name);
            }

            reportSheet.GetRow(2).GetCell(5).SetCellValue(DateTime.Now.ToString("dd MMMM yyyy"));

            // Fill the version
            reportSheet.GetRow(4).GetCell(2).SetCellValue(assyCostCalculation.CalculatorVersion);

            // Fill the used currency
            reportSheet.GetRow(4).GetCell(5).SetCellValue(this.GetUICurrencySymbol());

            int crtRowNum = 8;
            int position = 0;

            // Add the assembly tables to the report
            foreach (EnhancedAssyReportTableData data in assembliesData)
            {
                int subPosition = 1;

                // Add and fill the summary row (1st row)
                IRow row1 = CopyRow(templateSheet, 3, reportSheet, crtRowNum);
                crtRowNum++;
                data.HeaderData.Position = position.ToString();
                FillEnhancedAssyXlsReportRow(row1, data.HeaderData);

                // Add and fill the assembly row (2nd row)
                IRow row2 = null;
                if (data.SubAssembliesData.Count == 0 && data.PartsAndCommoditiesData.Count == 0)
                {
                    row2 = CopyRow(templateSheet, 15, reportSheet, crtRowNum);
                }
                else
                {
                    row2 = CopyRow(templateSheet, 4, reportSheet, crtRowNum);
                }

                crtRowNum++;
                data.AssyData.Position = position.ToString();
                FillEnhancedAssyXlsReportRow(row2, data.AssyData);

                // Add rows for sub-assemblies
                for (int i = 0; i < data.SubAssembliesData.Count; i++)
                {
                    EnhancedAssyReportRowData rowData = data.SubAssembliesData[i];
                    rowData.Position = position > 0 ? position + "." + subPosition : subPosition.ToString();

                    IRow row = null;
                    if (i < data.SubAssembliesData.Count - 1 || data.PartsAndCommoditiesData.Count > 0)
                    {
                        if (rowData.External)
                        {
                            row = CopyRow(templateSheet, 9, reportSheet, crtRowNum);
                        }
                        else
                        {
                            row = CopyRow(templateSheet, 8, reportSheet, crtRowNum);
                        }
                    }
                    else
                    {
                        if (rowData.External)
                        {
                            row = CopyRow(templateSheet, 13, reportSheet, crtRowNum);
                        }
                        else
                        {
                            row = CopyRow(templateSheet, 11, reportSheet, crtRowNum);
                        }
                    }

                    FillEnhancedAssyXlsReportRow(row, rowData);
                    crtRowNum++;
                    subPosition++;
                }

                // Add rows for parts and commodities
                for (int i = 0; i < data.PartsAndCommoditiesData.Count; i++)
                {
                    EnhancedAssyReportRowData rowData = data.PartsAndCommoditiesData[i];
                    rowData.Position = position > 0 ? position + "." + subPosition : subPosition.ToString();

                    IRow row = null;
                    if (i < data.PartsAndCommoditiesData.Count - 1)
                    {
                        row = CopyRow(templateSheet, 5, reportSheet, crtRowNum);
                    }
                    else
                    {
                        row = CopyRow(templateSheet, 6, reportSheet, crtRowNum);
                    }

                    FillEnhancedAssyXlsReportRow(row, rowData);
                    crtRowNum++;
                    subPosition++;
                }

                crtRowNum++;
                position++;
            }
        }

        /// <summary>
        /// Fills an enhanced assembly XLS report row with the specified data.
        /// </summary>
        /// <param name="row">The row to fill.</param>
        /// <param name="data">The data to fill the row with.</param>
        private void FillEnhancedAssyXlsReportRow(IRow row, EnhancedAssyReportRowData data)
        {
            // Pos
            row.GetCell(0).SetCellValue(data.Position);

            // PartNo
            row.GetCell(1).SetCellValue(data.PartNumber);

            // Amount            
            if (!(data is EnhancedAssyReportHeaderRowData))
            {
                // The amount must not appear in the header row.
                row.GetCell(2).SetCellValue(data.Amount);
            }

            // Name
            row.GetCell(3).SetCellValue(data.Name);

            // Remark
            row.GetCell(4).SetCellValue(data.Remark);

            // Est / Calc
            if (data.Estimated)
            {
                row.GetCell(5).SetCellValue("Est");
            }
            else
            {
                row.GetCell(5).SetCellValue("Calc");
            }

            // Int / Ext
            if (data.External)
            {
                row.GetCell(6).SetCellValue("Ext");
            }
            else
            {
                row.GetCell(6).SetCellValue("Int");
            }

            // External Part Cost (Commodities)
            row.GetCell(7).SetCellValue(TruncateTo4DecimalPlaces(data.ExternalPartsAndCommoditiesCost));

            // OH on External Parts
            row.GetCell(8).SetCellValue(TruncateTo4DecimalPlaces(data.ExternalPartsAndCommoditiesOH));

            // Raw Material
            row.GetCell(9).SetCellValue(TruncateTo4DecimalPlaces(data.RawMaterial));

            // Raw Material OH
            row.GetCell(10).SetCellValue(TruncateTo4DecimalPlaces(data.RawMaterialOH));

            // Consumables
            row.GetCell(11).SetCellValue(TruncateTo4DecimalPlaces(data.Consumables));

            // Consumables OH
            row.GetCell(12).SetCellValue(TruncateTo4DecimalPlaces(data.ConsumablesOH));

            // Estimated Part Cost
            row.GetCell(13).SetCellValue(TruncateTo4DecimalPlaces(data.EstimatedPartCost));

            // Manufacturing Cost incl. Scrap
            row.GetCell(14).SetCellValue(TruncateTo4DecimalPlaces(data.ManufacturingCostIncludingScrap));

            // Manufacturing OH
            row.GetCell(15).SetCellValue(TruncateTo4DecimalPlaces(data.ManufacturingOH));

            // Manufacturing Transport
            row.GetCell(16).SetCellValue(TruncateTo4DecimalPlaces(data.ManufacturingTransport));

            // External Work OH
            row.GetCell(17).SetCellValue(TruncateTo4DecimalPlaces(data.ExternalWorkOH));

            // Dev't Cost
            row.GetCell(18).SetCellValue(TruncateTo4DecimalPlaces(data.DevelopmentCost));

            // Proj. Cost
            row.GetCell(19).SetCellValue(TruncateTo4DecimalPlaces(data.ProjectInvest));

            // Other Cost incl OH
            row.GetCell(20).SetCellValue(TruncateTo4DecimalPlaces(data.OtherCostIncludingOH));

            // Pkg Cost
            row.GetCell(21).SetCellValue(TruncateTo4DecimalPlaces(data.PackagingCostIncludingOH));

            // Logistics incl OH
            row.GetCell(22).SetCellValue(TruncateTo4DecimalPlaces(data.LogisticsIncludingOH));

            // Transport
            row.GetCell(23).SetCellValue(TruncateTo4DecimalPlaces(data.Transport));

            // SG&A
            row.GetCell(24).SetCellValue(TruncateTo4DecimalPlaces(data.SalesAndAdministrationOH));

            // Company Surcharge
            row.GetCell(25).SetCellValue(TruncateTo4DecimalPlaces(data.CompanySurchargeOH));

            // Margin on Material/Consumables/Commodities
            row.GetCell(26).SetCellValue(TruncateTo4DecimalPlaces(data.MarginOnMaterialConsumablesAndCommodities));

            // Margin on Manufacturing and Assembly
            row.GetCell(27).SetCellValue(TruncateTo4DecimalPlaces(data.MarginOnManufacturingAndAssembly));

            // Payment Cost
            row.GetCell(28).SetCellValue(TruncateTo4DecimalPlaces(data.PaymentCost));

            // Piece Price
            row.GetCell(29).SetCellValue(TruncateTo4DecimalPlaces(data.PiecePrice));

            // Total Price per Assy
            row.GetCell(30).SetCellValue(TruncateTo4DecimalPlaces(data.TotalPricePerAssy));
        }

        /// <summary>
        /// Compiles the enhanced assembly report data from the assembly's cost calculation result.
        /// The report data consists of some data for the top-level assembly and all sub-assemblies in its tree; each data in the result will be saved
        /// in the report as a separate table. The order of the data (thus tables) is decided by traversing the sub-assemblies tree in breadth-first order.
        /// </summary>
        /// <param name="assyCalcResult">The assembly's cost calculation result.</param>        
        /// <param name="level">The start value for counting the assembly's sub-tree levels during recursive calls. Set it to 1 (any integer is fine).</param>        
        /// <returns>
        /// The list of data for each assembly in the assembly's tree to be added in the report.
        /// The 1st entry in the list is for the assembly whose report is being generated.
        /// </returns>
        private List<EnhancedAssyReportTableData> CompileEnhancedAssyReportData(CalculationResult assyCalcResult, int level)
        {
            // Initialize the data for the assembly whose cost calculation is the input. This data is always on the 1st position of the data list returned.
            EnhancedAssyReportTableData assyTableData = new EnhancedAssyReportTableData();

            List<EnhancedAssyReportTableData> firstLevelSubAssysData = new List<EnhancedAssyReportTableData>();
            List<EnhancedAssyReportTableData> lowerLevelSubAssysData = new List<EnhancedAssyReportTableData>();
            foreach (AssemblyCost cost in assyCalcResult.AssembliesCost.AssemblyCosts.OrderBy(a => a.Name))
            {
                List<EnhancedAssyReportTableData> subAssyTableData = CompileEnhancedAssyReportData(cost.FullCalculationResult, level + 1);

                if (subAssyTableData.Count > 0)
                {
                    EnhancedAssyReportTableData tableData = subAssyTableData.ElementAt(0);
                    firstLevelSubAssysData.Add(tableData);
                    subAssyTableData.RemoveAt(0);
                    lowerLevelSubAssysData.AddRange(subAssyTableData);

                    // For each 1st level sub-assembly, add a row in the parent assembly's table
                    EnhancedAssyReportRowData rowData = new EnhancedAssyReportRowData();

                    if (!tableData.AssyData.External)
                    {
                        // Non-external sub-assy data is copied from its table header (summary) row
                        tableData.HeaderData.CopyData(rowData);
                    }
                    else
                    {
                        // External sub-assemblies are treated as external parts.
                        rowData.PartNumber = tableData.AssyData.PartNumber;
                        rowData.Estimated = tableData.AssyData.Estimated;
                        rowData.External = tableData.AssyData.External;

                        rowData.ExternalPartsAndCommoditiesCost = cost.TargetCost;
                        rowData.ExternalPartsAndCommoditiesOH = cost.ExternalOverhead;
                        rowData.MarginOnMaterialConsumablesAndCommodities = cost.ExternalMargin;
                        rowData.SalesAndAdministrationOH = cost.ExternalSGA;
                    }

                    rowData.Amount = cost.AmountPerAssembly;
                    rowData.Name = tableData.AssyData.Name;
                    rowData.Remark = "Subassembly";

                    assyTableData.SubAssembliesData.Add(rowData);
                }
            }

            List<EnhancedAssyReportTableData> assembliesData = new List<EnhancedAssyReportTableData>();
            assembliesData.Add(assyTableData);
            assembliesData.AddRange(firstLevelSubAssysData);

            // For assemblies situated 2 levels or lower under the current assembly, the data is added after the last assembly on the same level.
            // At the end of the recursive calls the data will be in breadth-first order.
            foreach (EnhancedAssyReportTableData data in lowerLevelSubAssysData)
            {
                EnhancedAssyReportTableData last = assembliesData.Where(d => d.Level == data.Level).LastOrDefault();
                if (last != null)
                {
                    int index = assembliesData.IndexOf(last) + 1;
                    assembliesData.Insert(index, data);
                }
                else
                {
                    assembliesData.Add(data);
                }
            }

            // Set the parts and commodities data (from 3rd row on)
            var partsAndCommData = this.CompileSubPartsAndCommoditiesDataForEnhancedAssyReport(assyCalcResult);
            assyTableData.PartsAndCommoditiesData.AddRange(partsAndCommData);
            assyTableData.Level = level;

            /* Set the assembly data (2nd row) */
            EnhancedAssyReportRowData assyRowData = new EnhancedAssyReportRowData();
            assyTableData.AssyData = assyRowData;

            assyRowData.PartNumber = assyCalcResult.PartNumber;
            assyRowData.Amount = 1;
            assyRowData.Name = assyCalcResult.Name + " Assy";
            assyRowData.Remark = assyCalcResult.Remark;
            assyRowData.Estimated = assyCalcResult.Estimated;
            assyRowData.External = assyCalcResult.External;

            assyRowData.ExternalPartsAndCommoditiesCost = 0;
            assyRowData.ExternalPartsAndCommoditiesOH = 0;
            assyRowData.MarginOnMaterialConsumablesAndCommodities = 0;

            assyRowData.RawMaterial = assyCalcResult.RawMaterialsCost.NetCostSum;
            assyRowData.RawMaterialOH = assyCalcResult.OverheadCost.RawMaterialOverhead;
            assyRowData.Consumables = assyCalcResult.ConsumablesCost.CostsSum;
            assyRowData.ConsumablesOH = assyCalcResult.OverheadCost.ConsumableOverhead;

            // In calculation version "1.0" the full estimated cost was put in the "Manufacturing Cost" column 
            if (assyCalcResult.CalculatorVersion == "1.0")
            {
                if (assyCalcResult.Estimated)
                {
                    assyRowData.EstimatedPartCost = assyCalcResult.Summary.ProductionCost;
                }
                else
                {
                    assyRowData.ManufacturingCostIncludingScrap = assyCalcResult.ProcessCost.ManufacturingCostSum + assyCalcResult.ProcessCost.RejectCostSum;
                }
            }
            else
            {
                // In version "1.1" the estimated cost was split between "Raw Material Cost" and "Manufacturing Cost" for the "Rough Calculation" and
                // "Estimation" accuracies. For the "Offer / External Calculation" accuracy the production cost is put in the "Manufacturing Cost" column, like in "1.0".
                if (assyCalcResult.CalculationAccuracy == PartCalculationAccuracy.OfferOrExternalCalculation)
                {
                    assyRowData.EstimatedPartCost = assyCalcResult.Summary.ProductionCost;
                }
                else
                {
                    assyRowData.ManufacturingCostIncludingScrap = assyCalcResult.ProcessCost.ManufacturingCostSum + assyCalcResult.ProcessCost.RejectCostSum;
                }
            }

            assyRowData.ManufacturingOH = assyCalcResult.OverheadCost.ManufacturingOverhead;
            assyRowData.ManufacturingTransport = assyCalcResult.ProcessCost.ManufacturingTransportCost;
            assyRowData.ExternalWorkOH = assyCalcResult.OverheadCost.ExternalWorkOverhead;
            assyRowData.DevelopmentCost = assyCalcResult.Summary.DevelopmentCost;
            assyRowData.ProjectInvest = assyCalcResult.Summary.ProjectInvest;
            assyRowData.OtherCostIncludingOH = assyCalcResult.Summary.OtherCost + assyCalcResult.OverheadCost.OtherCostOverhead;
            assyRowData.PackagingCostIncludingOH = assyCalcResult.Summary.PackagingCost + assyCalcResult.OverheadCost.PackagingOverhead;
            assyRowData.LogisticsIncludingOH = assyCalcResult.Summary.LogisticCost + assyCalcResult.OverheadCost.LogisticOverhead;
            assyRowData.Transport = assyCalcResult.Summary.TransportCost;

            // Remove the SGA of the external sub-parts and sub-assemblies from the total SGA because it is included at the sub-part/sub-assembly level.
            assyRowData.SalesAndAdministrationOH = assyCalcResult.OverheadCost.SalesAndAdministrationOverhead
              - assyCalcResult.AssembliesCost.AssemblyCosts.Sum(a => a.ExternalSGA * a.AmountPerAssembly)
              - assyCalcResult.PartsCost.PartCosts.Sum(p => p.ExternalSGA * p.AmountPerAssembly);

            assyRowData.CompanySurchargeOH = assyCalcResult.OverheadCost.CompanySurchargeOverhead;
            assyRowData.MarginOnMaterialConsumablesAndCommodities += assyCalcResult.OverheadCost.RawMaterialMargin + assyCalcResult.OverheadCost.ConsumableMargin;
            assyRowData.MarginOnManufacturingAndAssembly = assyCalcResult.OverheadCost.ManufacturingMargin + assyCalcResult.OverheadCost.ExternalWorkMargin;
            assyRowData.PaymentCost = assyCalcResult.Summary.PaymentCost;
            /************************************/

            /* Set the data in the summary row (1st row). */
            EnhancedAssyReportHeaderRowData headerRowData = new EnhancedAssyReportHeaderRowData();
            assyTableData.HeaderData = headerRowData;

            headerRowData.PartNumber = assyCalcResult.PartNumber;
            headerRowData.Name = assyCalcResult.Name.Trim() + " SUMMARY RESULT";
            headerRowData.Remark = assyCalcResult.Remark;
            headerRowData.Estimated = assyCalcResult.Estimated;
            headerRowData.External = assyCalcResult.External;

            headerRowData.ExternalPartsAndCommoditiesCost = (assyRowData.ExternalPartsAndCommoditiesCost * assyRowData.Amount) +
                assyTableData.PartsAndCommoditiesData.Sum(selector => selector.ExternalPartsAndCommoditiesCost * selector.Amount) +
                assyTableData.SubAssembliesData.Sum(selector => selector.ExternalPartsAndCommoditiesCost * selector.Amount);

            headerRowData.ExternalPartsAndCommoditiesOH = (assyRowData.ExternalPartsAndCommoditiesOH * assyRowData.Amount) +
                assyTableData.PartsAndCommoditiesData.Sum(selector => selector.ExternalPartsAndCommoditiesOH * selector.Amount) +
                assyTableData.SubAssembliesData.Sum(selector => selector.ExternalPartsAndCommoditiesOH * selector.Amount);

            headerRowData.RawMaterial = (assyRowData.RawMaterial * assyRowData.Amount) +
                assyTableData.PartsAndCommoditiesData.Sum(selector => selector.RawMaterial * selector.Amount) +
                assyTableData.SubAssembliesData.Sum(selector => selector.RawMaterial * selector.Amount);

            headerRowData.RawMaterialOH = (assyRowData.RawMaterialOH * assyRowData.Amount) +
                assyTableData.PartsAndCommoditiesData.Sum(selector => selector.RawMaterialOH * selector.Amount) +
                assyTableData.SubAssembliesData.Sum(selector => selector.RawMaterialOH * selector.Amount);

            headerRowData.Consumables = (assyRowData.Consumables * assyRowData.Amount) +
                assyTableData.PartsAndCommoditiesData.Sum(selector => selector.Consumables * selector.Amount) +
                assyTableData.SubAssembliesData.Sum(selector => selector.Consumables * selector.Amount);

            headerRowData.ConsumablesOH = (assyRowData.ConsumablesOH * assyRowData.Amount) +
                assyTableData.PartsAndCommoditiesData.Sum(selector => selector.ConsumablesOH * selector.Amount) +
                assyTableData.SubAssembliesData.Sum(selector => selector.ConsumablesOH * selector.Amount);

            headerRowData.EstimatedPartCost = (assyRowData.EstimatedPartCost * assyRowData.Amount) +
                assyTableData.PartsAndCommoditiesData.Sum(selector => selector.EstimatedPartCost * selector.Amount) +
                assyTableData.SubAssembliesData.Sum(selector => selector.EstimatedPartCost * selector.Amount);

            headerRowData.ManufacturingCostIncludingScrap = (assyRowData.ManufacturingCostIncludingScrap * assyRowData.Amount) +
                assyTableData.PartsAndCommoditiesData.Sum(selector => selector.ManufacturingCostIncludingScrap * selector.Amount) +
                assyTableData.SubAssembliesData.Sum(selector => selector.ManufacturingCostIncludingScrap * selector.Amount);

            headerRowData.ManufacturingOH = (assyRowData.ManufacturingOH * assyRowData.Amount) +
                assyTableData.PartsAndCommoditiesData.Sum(selector => selector.ManufacturingOH * selector.Amount) +
                assyTableData.SubAssembliesData.Sum(selector => selector.ManufacturingOH * selector.Amount);

            headerRowData.ManufacturingTransport = (assyRowData.ManufacturingTransport * assyRowData.Amount) +
                assyTableData.PartsAndCommoditiesData.Sum(selector => selector.ManufacturingTransport * selector.Amount) +
                assyTableData.SubAssembliesData.Sum(selector => selector.ManufacturingTransport * selector.Amount);

            headerRowData.ExternalWorkOH = (assyRowData.ExternalWorkOH * assyRowData.Amount) +
                assyTableData.PartsAndCommoditiesData.Sum(selector => selector.ExternalWorkOH * selector.Amount) +
                assyTableData.SubAssembliesData.Sum(selector => selector.ExternalWorkOH * selector.Amount);

            headerRowData.DevelopmentCost = (assyRowData.DevelopmentCost * assyRowData.Amount) +
                assyTableData.PartsAndCommoditiesData.Sum(selector => selector.DevelopmentCost * selector.Amount) +
                assyTableData.SubAssembliesData.Sum(selector => selector.DevelopmentCost * selector.Amount);

            headerRowData.ProjectInvest = (assyRowData.ProjectInvest * assyRowData.Amount) +
                assyTableData.PartsAndCommoditiesData.Sum(selector => selector.ProjectInvest * selector.Amount) +
                assyTableData.SubAssembliesData.Sum(selector => selector.ProjectInvest * selector.Amount);

            headerRowData.OtherCostIncludingOH = (assyRowData.OtherCostIncludingOH * assyRowData.Amount) +
                assyTableData.PartsAndCommoditiesData.Sum(selector => selector.OtherCostIncludingOH * selector.Amount) +
                assyTableData.SubAssembliesData.Sum(selector => selector.OtherCostIncludingOH * selector.Amount);

            headerRowData.PackagingCostIncludingOH = (assyRowData.PackagingCostIncludingOH * assyRowData.Amount) +
                assyTableData.PartsAndCommoditiesData.Sum(selector => selector.PackagingCostIncludingOH * selector.Amount) +
                assyTableData.SubAssembliesData.Sum(selector => selector.PackagingCostIncludingOH * selector.Amount);

            headerRowData.LogisticsIncludingOH = (assyRowData.LogisticsIncludingOH * assyRowData.Amount) +
                assyTableData.PartsAndCommoditiesData.Sum(selector => selector.LogisticsIncludingOH * selector.Amount) +
                assyTableData.SubAssembliesData.Sum(selector => selector.LogisticsIncludingOH * selector.Amount);

            headerRowData.Transport = (assyRowData.Transport * assyRowData.Amount) +
                assyTableData.PartsAndCommoditiesData.Sum(selector => selector.Transport * selector.Amount) +
                assyTableData.SubAssembliesData.Sum(selector => selector.Transport * selector.Amount);

            headerRowData.SalesAndAdministrationOH = (assyRowData.SalesAndAdministrationOH * assyRowData.Amount) +
                assyTableData.PartsAndCommoditiesData.Sum(selector => selector.SalesAndAdministrationOH * selector.Amount) +
                assyTableData.SubAssembliesData.Sum(selector => selector.SalesAndAdministrationOH * selector.Amount);

            headerRowData.CompanySurchargeOH = (assyRowData.CompanySurchargeOH * assyRowData.Amount) +
                assyTableData.PartsAndCommoditiesData.Sum(selector => selector.CompanySurchargeOH * selector.Amount) +
                assyTableData.SubAssembliesData.Sum(selector => selector.CompanySurchargeOH * selector.Amount);

            headerRowData.MarginOnMaterialConsumablesAndCommodities = (assyRowData.MarginOnMaterialConsumablesAndCommodities * assyRowData.Amount) +
                assyTableData.PartsAndCommoditiesData.Sum(selector => selector.MarginOnMaterialConsumablesAndCommodities * selector.Amount) +
                assyTableData.SubAssembliesData.Sum(selector => selector.MarginOnMaterialConsumablesAndCommodities * selector.Amount);

            headerRowData.MarginOnManufacturingAndAssembly = (assyRowData.MarginOnManufacturingAndAssembly * assyRowData.Amount) +
                assyTableData.PartsAndCommoditiesData.Sum(selector => selector.MarginOnManufacturingAndAssembly * selector.Amount) +
                assyTableData.SubAssembliesData.Sum(selector => selector.MarginOnManufacturingAndAssembly * selector.Amount);

            headerRowData.PaymentCost = (assyRowData.PaymentCost * assyRowData.Amount) +
                assyTableData.PartsAndCommoditiesData.Sum(selector => selector.PaymentCost * selector.Amount) +
                assyTableData.SubAssembliesData.Sum(selector => selector.PaymentCost * selector.Amount);
            /*************************************/

            return assembliesData;
        }

        /// <summary>
        /// Compiles the data corresponding to each part and commodity in an assembly. The data is meant to be used for generating an enhanced assembly report.
        /// Does not include data form sub-assemblies.
        /// </summary>
        /// <param name="assyCalcResult">The assembly's cost calculation.</param>
        /// <returns>
        /// A list of data entries corresponding to rows in an assembly report table
        /// </returns>
        private List<EnhancedAssyReportRowData> CompileSubPartsAndCommoditiesDataForEnhancedAssyReport(CalculationResult assyCalcResult)
        {
            List<EnhancedAssyReportRowData> rowsData = new List<EnhancedAssyReportRowData>();
            int subPosition = 1;

            /* Add the commodities cost info */
            foreach (CommodityCost commodityCost in assyCalcResult.CommoditiesCost.CommodityCosts.OrderBy(selector => selector.Name))
            {
                EnhancedAssyReportRowData data = CompileCommodityDataForEnhancedAssyReport(commodityCost);
                rowsData.Add(data);
                subPosition++;
            }

            /**********************************/

            /* Add the sub-parts cost info */
            foreach (PartCost partCost in assyCalcResult.PartsCost.PartCosts.OrderBy(selector => selector.Name))
            {
                // Add the part cost info
                EnhancedAssyReportRowData partRowData = new EnhancedAssyReportRowData();
                partRowData.PartNumber = partCost.FullCalculationResult.PartNumber;
                partRowData.Amount = partCost.AmountPerAssembly;
                partRowData.Name = partCost.FullCalculationResult.Name + " Part";
                partRowData.Remark = partCost.FullCalculationResult.Remark;
                partRowData.Estimated = partCost.FullCalculationResult.Estimated;
                partRowData.External = partCost.FullCalculationResult.External;

                // External parts have 0 in all fields except in the "External Part Cost (Commodities)" field where the total cost is put,
                // the "OH on External Parts" field where the overhead is put and the "Margin on Material/Consumables/Commodities" field where the margin is put.
                if (partCost.FullCalculationResult.External)
                {
                    partRowData.ExternalPartsAndCommoditiesCost =
                        partCost.FullCalculationResult.TotalCost
                        - partCost.FullCalculationResult.CommoditiesCost.CostsSum
                        - partCost.FullCalculationResult.CommoditiesCost.OverheadSum
                        - partCost.FullCalculationResult.CommoditiesCost.MarginSum;

                    partRowData.ExternalPartsAndCommoditiesOH = partCost.ExternalOverhead;
                    partRowData.MarginOnMaterialConsumablesAndCommodities = partCost.ExternalMargin;
                    partRowData.SalesAndAdministrationOH = partCost.ExternalSGA;
                }
                else
                {
                    partRowData.RawMaterial = partCost.FullCalculationResult.RawMaterialsCost.NetCostSum;
                    partRowData.RawMaterialOH = partCost.FullCalculationResult.OverheadCost.RawMaterialOverhead;
                    partRowData.Consumables = partCost.FullCalculationResult.ConsumablesCost.CostsSum;
                    partRowData.ConsumablesOH = partCost.FullCalculationResult.OverheadCost.ConsumableOverhead;

                    // In calculation version "1.0" the full estimated cost was put in the "Manufacturing Cost" column 
                    if (partCost.FullCalculationResult.CalculatorVersion == "1.0")
                    {
                        if (partCost.FullCalculationResult.Estimated)
                        {
                            partRowData.EstimatedPartCost = partCost.FullCalculationResult.Summary.ProductionCost;
                        }
                        else
                        {
                            partRowData.ManufacturingCostIncludingScrap = partCost.FullCalculationResult.ProcessCost.ManufacturingCostSum + partCost.FullCalculationResult.ProcessCost.RejectCostSum;
                        }
                    }
                    else
                    {
                        // In version "1.1" the estimated cost was split between "Raw Material Cost" and "Manufacturing Cost" fort the "Rough Calculation" and
                        // "Estimation" accuracies. For the "Offer / External Calculation" accuracy the production cost is put in the "Manufacturing Cost" column, like in "1.0".
                        if (partCost.FullCalculationResult.CalculationAccuracy == PartCalculationAccuracy.OfferOrExternalCalculation)
                        {
                            partRowData.EstimatedPartCost = partCost.FullCalculationResult.Summary.ProductionCost;
                        }
                        else
                        {
                            partRowData.ManufacturingCostIncludingScrap = partCost.FullCalculationResult.ProcessCost.ManufacturingCostSum + partCost.FullCalculationResult.ProcessCost.RejectCostSum;
                        }
                    }

                    partRowData.ManufacturingOH = partCost.FullCalculationResult.OverheadCost.ManufacturingOverhead;
                    partRowData.ManufacturingTransport = partCost.FullCalculationResult.ProcessCost.ManufacturingTransportCost;
                    partRowData.ExternalWorkOH = partCost.FullCalculationResult.OverheadCost.ExternalWorkOverhead;
                    partRowData.DevelopmentCost = partCost.FullCalculationResult.Summary.DevelopmentCost;
                    partRowData.ProjectInvest = partCost.FullCalculationResult.Summary.ProjectInvest;
                    partRowData.OtherCostIncludingOH = partCost.FullCalculationResult.Summary.OtherCost + partCost.FullCalculationResult.OverheadCost.OtherCostOverhead;
                    partRowData.PackagingCostIncludingOH = partCost.FullCalculationResult.Summary.PackagingCost + partCost.FullCalculationResult.OverheadCost.PackagingOverhead;
                    partRowData.LogisticsIncludingOH = partCost.FullCalculationResult.Summary.LogisticCost + partCost.FullCalculationResult.OverheadCost.LogisticOverhead;
                    partRowData.Transport = partCost.FullCalculationResult.Summary.TransportCost;
                    partRowData.SalesAndAdministrationOH = partCost.FullCalculationResult.OverheadCost.SalesAndAdministrationOverhead;
                    partRowData.CompanySurchargeOH = partCost.FullCalculationResult.OverheadCost.CompanySurchargeOverhead;

                    partRowData.MarginOnMaterialConsumablesAndCommodities = partCost.FullCalculationResult.OverheadCost.RawMaterialMargin
                        + partCost.FullCalculationResult.OverheadCost.ConsumableMargin;

                    partRowData.MarginOnManufacturingAndAssembly = partCost.FullCalculationResult.OverheadCost.ManufacturingMargin
                        + partCost.FullCalculationResult.OverheadCost.ExternalWorkMargin;

                    partRowData.PaymentCost = partCost.FullCalculationResult.Summary.PaymentCost;
                }

                rowsData.Add(partRowData);
                subPosition++;

                // Add the cost info for each commodity in the part
                foreach (CommodityCost commodityCost in partCost.FullCalculationResult.CommoditiesCost.CommodityCosts.OrderBy(selector => selector.Name))
                {
                    EnhancedAssyReportRowData commodityRowData = CompileCommodityDataForEnhancedAssyReport(commodityCost);
                    commodityRowData.Amount *= partCost.AmountPerAssembly;
                    rowsData.Add(commodityRowData);
                    subPosition++;
                }
            }

            /*******************************/

            return rowsData;
        }

        /// <summary>
        /// Compiles the commodity data for enhanced assembly report.
        /// </summary>
        /// <param name="commodityCost">The commodity cost from which to compile the data.</param>
        /// <returns>The compiled data.</returns>
        private EnhancedAssyReportRowData CompileCommodityDataForEnhancedAssyReport(CommodityCost commodityCost)
        {
            EnhancedAssyReportRowData data = new EnhancedAssyReportRowData();
            data.Amount = commodityCost.Amount;
            data.Name = commodityCost.Name;
            data.Remark = "Commodity";
            data.External = true;
            if (commodityCost.Amount != 0)
            {
                data.ExternalPartsAndCommoditiesCost = commodityCost.Cost / commodityCost.Amount;
                data.ExternalPartsAndCommoditiesOH = commodityCost.Overhead / commodityCost.Amount;
                data.MarginOnMaterialConsumablesAndCommodities = commodityCost.Margin / commodityCost.Amount;
            }

            return data;
        }

        #endregion Enhanced Assembly Report Xls

        #region Enhanced Assembly Report Pdf

        /// <summary>
        /// Generate the Enhances Assembly Report in the PDF format
        /// </summary>
        /// <param name="calculationResult">The assembly cost calculation result to export.</param>
        /// <param name="path">The path where to save the PDF file on the disk.</param>
        public void CreateEnhancedAssemblyReportPdf(CalculationResult calculationResult, string path)
        {
            try
            {
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    float pageMargin = 10f;
                    Document document = new Document(new Rectangle(1450f, 1014f), pageMargin, pageMargin, pageMargin, pageMargin);
                    PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
                    document.Open();

                    // Create and populate the top table containing the extra information.
                    PdfPTable reportInfoTable = new PdfPTable(new float[] { 20f, 40f, 15f, 25f });
                    reportInfoTable.WidthPercentage = 50f;
                    reportInfoTable.SpacingAfter = 10f;
                    reportInfoTable.HorizontalAlignment = Table.ALIGN_LEFT;
                    reportInfoTable.DefaultCell.HorizontalAlignment = Table.ALIGN_LEFT;
                    reportInfoTable.DefaultCell.VerticalAlignment = Table.ALIGN_MIDDLE;
                    reportInfoTable.DefaultCell.Border = Table.NO_BORDER;
                    reportInfoTable.DefaultCell.PaddingLeft = 7f;
                    reportInfoTable.DefaultCell.PaddingRight = 7f;
                    reportInfoTable.DefaultCell.PaddingTop = 3.5f;
                    reportInfoTable.DefaultCell.PaddingBottom = 3.5f;

                    PdfCellLayoutEvent whiteCellEvent = new PdfCellLayoutEvent(new Thickness(5d, 2.5d, 5d, 2.5d), Color.WHITE);
                    PdfCellLayoutEvent blueCellEvent = new PdfCellLayoutEvent(new Thickness(5d, 2.5d, 5d, 2.5d), new Color(79, 129, 189));
                    iTextSharp.text.Font titleFont = FontFactory.GetFont(ZPKTool.Common.Constants.PdfReportsFont, 12, iTextSharp.text.Font.NORMAL, Color.WHITE);
                    Color titleColor = new Color(55, 91, 161);

                    reportInfoTable.AddCell(new PdfPCell(reportInfoTable.DefaultCell)
                        {
                            Phrase = new Phrase(LocalizedResources.Report_EnhancedReportTitle, titleFont),
                            Colspan = reportInfoTable.NumberOfColumns,
                            CellEvent = new PdfCellLayoutEvent(new Thickness(5d, 2.5d, 5d, 2.5d), titleColor)
                        });
                    reportInfoTable.AddCell(new PdfPCell(reportInfoTable.DefaultCell)
                        {
                            Phrase = new Phrase(LocalizedResources.Report_CompletedBy, this.pdfReportFontColumnHeader),
                            CellEvent = blueCellEvent
                        });

                    string user = SecurityManager.Instance.CurrentUser != null ? SecurityManager.Instance.CurrentUser.Name : this.emptyCellSymbol;
                    reportInfoTable.AddCell(new PdfPCell(reportInfoTable.DefaultCell)
                        {
                            Phrase = new Phrase(user, this.pdfReportFontBold),
                            HorizontalAlignment = Table.ALIGN_CENTER,
                            CellEvent = whiteCellEvent
                        });

                    reportInfoTable.AddCell(new PdfPCell(reportInfoTable.DefaultCell)
                    {
                        Phrase = new Phrase(LocalizedResources.Report_Date, this.pdfReportFontColumnHeader),
                        CellEvent = blueCellEvent
                    });
                    reportInfoTable.AddCell(new PdfPCell(reportInfoTable.DefaultCell)
                    {
                        Phrase = new Phrase(DateTime.Now.ToString("dd MMMM yyyy"), this.pdfReportFontBold),
                        HorizontalAlignment = Table.ALIGN_CENTER,
                        CellEvent = whiteCellEvent
                    });

                    reportInfoTable.AddCell(new PdfPCell(reportInfoTable.DefaultCell)
                    {
                        Phrase = new Phrase(LocalizedResources.Report_Version, this.pdfReportFontColumnHeader),
                        CellEvent = blueCellEvent
                    });
                    reportInfoTable.AddCell(new PdfPCell(reportInfoTable.DefaultCell)
                    {
                        Phrase = new Phrase(calculationResult.CalculatorVersion, this.pdfReportFontBold),
                        HorizontalAlignment = Table.ALIGN_CENTER,
                        CellEvent = whiteCellEvent
                    });

                    reportInfoTable.AddCell(new PdfPCell(reportInfoTable.DefaultCell)
                    {
                        Phrase = new Phrase(LocalizedResources.Report_Currency, this.pdfReportFontColumnHeader),
                        CellEvent = blueCellEvent
                    });

                    reportInfoTable.AddCell(new PdfPCell(reportInfoTable.DefaultCell)
                    {
                        Phrase = new Phrase(this.GetUICurrencySymbol(), this.pdfReportFontBold),
                        HorizontalAlignment = Table.ALIGN_CENTER,
                        CellEvent = whiteCellEvent
                    });

                    document.Add(reportInfoTable);

                    // Create the table containing the column headers and add it to the document
                    float[] columnWidths = new float[]
                    {
                        1.8f, 4.0f, 3.3f, 11.3f, 7.0f, 3.3f, 3.3f, 3.3f, 3.3f, 3.3f, 
                        3.3f, 3.3f, 3.3f, 3.3f, 3.3f, 3.3f, 3.3f, 3.3f, 3.3f, 3.3f,
                        3.3f, 3.3f, 3.3f, 3.3f, 3.3f, 3.3f, 3.3f, 3.3f, 3.3f, 3.3f, 3.3f
                    };
                    PdfPTable headerTable = new PdfPTable(columnWidths);
                    headerTable.WidthPercentage = 100;
                    headerTable.DefaultCell.HorizontalAlignment = Table.ALIGN_LEFT;
                    headerTable.DefaultCell.VerticalAlignment = Table.ALIGN_MIDDLE;
                    headerTable.DefaultCell.CellEvent = new PdfCellLayoutEvent() { DrawCellBorder = true };

                    this.FillEnhancedAssyPdfReportTableHeader(headerTable);
                    document.Add(headerTable);

                    // Compile the report data and add it into new tables, below the table containing the headers.
                    List<EnhancedAssyReportTableData> assembliesData = this.CompileEnhancedAssyReportData(calculationResult, 1);
                    int position = 0;
                    foreach (EnhancedAssyReportTableData data in assembliesData)
                    {
                        int subPosition = 1;

                        Color backgroundColor = Color.WHITE;

                        PdfPTable table = new PdfPTable(columnWidths);
                        table.WidthPercentage = 100;
                        table.DefaultCell.HorizontalAlignment = Table.ALIGN_LEFT;
                        table.DefaultCell.VerticalAlignment = Table.ALIGN_MIDDLE;
                        table.DefaultCell.BorderWidth = 1.0f;
                        table.DefaultCell.Border = Table.NO_BORDER;
                        table.DefaultCell.CellEvent = new PdfCellLayoutEvent() { DrawCellBorder = true };

                        // Add and fill the summary row (1st row)
                        data.HeaderData.Position = position.ToString();
                        FillEnhancedAssyPdfReportHeaderRow(table, data.HeaderData);

                        // Add and fill the assembly row (2nd row)
                        backgroundColor = pdfEnhancedReportHeaderColorLight;
                        data.AssyData.Position = position.ToString();
                        FillEnhancedAssyPdfReportRow(table, data.AssyData, backgroundColor);

                        // Add rows for sub-assemblies
                        for (int i = 0; i < data.SubAssembliesData.Count; i++)
                        {
                            EnhancedAssyReportRowData rowData = data.SubAssembliesData[i];
                            rowData.Position = position > 0 ? position + "." + subPosition : subPosition.ToString();

                            backgroundColor = rowData.External ? pdfEnhancedReportSubassyColor : pdfEnhancedReportSubassyColorLight;
                            FillEnhancedAssyPdfReportRow(table, rowData, backgroundColor);
                            subPosition++;
                        }

                        // Add rows for parts and commodities
                        backgroundColor = pdfEnhancedReportHeaderColor;
                        for (int i = 0; i < data.PartsAndCommoditiesData.Count; i++)
                        {
                            EnhancedAssyReportRowData rowData = data.PartsAndCommoditiesData[i];
                            rowData.Position = position > 0 ? position + "." + subPosition : subPosition.ToString();

                            FillEnhancedAssyPdfReportRow(table, rowData, backgroundColor);
                            subPosition++;
                        }

                        // add table to a container to have a table border
                        PdfPTable containerTable = new PdfPTable(1);
                        containerTable.WidthPercentage = 100;
                        containerTable.DefaultCell.Padding = 0;
                        containerTable.DefaultCell.BorderWidth = 1.0f;
                        containerTable.AddCell(table);

                        // add space between tables
                        containerTable.DefaultCell.Border = Table.NO_BORDER;
                        containerTable.AddCell(" ");

                        document.Add(containerTable);

                        position++;
                    }

                    // Create the Summary table at the end of the report
                    PdfPTable summaryTable = new PdfPTable(new float[] { 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 20f, 10f });
                    summaryTable.SpacingBefore = 20f;
                    summaryTable.WidthPercentage = 60f;
                    summaryTable.HorizontalAlignment = Table.ALIGN_LEFT;
                    summaryTable.DefaultCell.HorizontalAlignment = Table.ALIGN_CENTER;
                    summaryTable.DefaultCell.VerticalAlignment = Table.ALIGN_BOTTOM;
                    summaryTable.DefaultCell.Border = Table.BOX;
                    summaryTable.DefaultCell.PaddingLeft = 3f;
                    summaryTable.DefaultCell.PaddingRight = 3f;
                    summaryTable.DefaultCell.PaddingTop = 5f;
                    summaryTable.DefaultCell.PaddingBottom = 3f;

                    summaryTable.AddCell(new PdfPCell(summaryTable.DefaultCell)
                    {
                        Phrase = new Phrase(LocalizedResources.Report_EnhancedReportSummary, titleFont),
                        Colspan = summaryTable.NumberOfColumns,
                        BackgroundColor = titleColor,
                        HorizontalAlignment = Table.ALIGN_LEFT
                    });

                    summaryTable.DefaultCell.Border = Table.NO_BORDER;
                    summaryTable.AddCell(string.Empty);
                    summaryTable.CompleteRow();

                    summaryTable.DefaultCell.Border = Table.BOX;
                    summaryTable.DefaultCell.BackgroundColor = new Color(219, 229, 241);

                    summaryTable.AddCell(new Phrase(LocalizedResources.Report_MaterialCost, this.pdfReportFont));
                    summaryTable.AddCell(new Phrase(LocalizedResources.Report_MaterialOverhead, this.pdfReportFont));
                    summaryTable.AddCell(new Phrase(LocalizedResources.Report_ManufacturingCost, this.pdfReportFont));
                    summaryTable.AddCell(new Phrase(LocalizedResources.Report_ManufacturingOH, this.pdfReportFont));
                    summaryTable.AddCell(new Phrase(LocalizedResources.Report_SGAndA, this.pdfReportFont));
                    summaryTable.AddCell(new Phrase(LocalizedResources.Report_CompanySurcharge, this.pdfReportFont));
                    summaryTable.AddCell(new Phrase(LocalizedResources.Report_Margin, this.pdfReportFont));
                    summaryTable.AddCell(new Phrase(LocalizedResources.Report_TransportAndLogistic, this.pdfReportFont));
                    summaryTable.AddCell(new Phrase(LocalizedResources.Report_Rest, this.pdfReportFont));

                    summaryTable.DefaultCell.BackgroundColor = new Color(216, 216, 216);

                    summaryTable.AddCell(new Phrase(LocalizedResources.Report_Total, this.pdfReportFontBold));

                    summaryTable.DefaultCell.VerticalAlignment = Table.ALIGN_MIDDLE;
                    summaryTable.DefaultCell.PaddingTop = 2f;
                    summaryTable.DefaultCell.PaddingBottom = 3f;

                    var mainAssyTotals = assembliesData.First().HeaderData;

                    decimal materialCost = mainAssyTotals.ExternalPartsAndCommoditiesCost + mainAssyTotals.RawMaterial
                        + mainAssyTotals.Consumables + mainAssyTotals.EstimatedPartCost;
                    summaryTable.AddCell(new Phrase(Formatter.FormatMoney(materialCost), this.pdfReportFont));

                    decimal materialOH = mainAssyTotals.ExternalPartsAndCommoditiesOH + mainAssyTotals.RawMaterialOH + mainAssyTotals.ConsumablesOH;
                    summaryTable.AddCell(new Phrase(Formatter.FormatMoney(materialOH), this.pdfReportFont));

                    decimal manufacturingCost = mainAssyTotals.ManufacturingCostIncludingScrap + mainAssyTotals.ManufacturingTransport;
                    summaryTable.AddCell(new Phrase(Formatter.FormatMoney(manufacturingCost), this.pdfReportFont));

                    decimal manufacturingOH = mainAssyTotals.ManufacturingOH + mainAssyTotals.ExternalWorkOH;
                    summaryTable.AddCell(new Phrase(Formatter.FormatMoney(manufacturingOH), this.pdfReportFont));

                    decimal sga = mainAssyTotals.SalesAndAdministrationOH;
                    summaryTable.AddCell(new Phrase(Formatter.FormatMoney(sga), this.pdfReportFont));

                    decimal companySurcharge = mainAssyTotals.CompanySurchargeOH;
                    summaryTable.AddCell(new Phrase(Formatter.FormatMoney(companySurcharge), this.pdfReportFont));

                    decimal margin = mainAssyTotals.MarginOnMaterialConsumablesAndCommodities + mainAssyTotals.MarginOnManufacturingAndAssembly;
                    summaryTable.AddCell(new Phrase(Formatter.FormatMoney(margin), this.pdfReportFont));

                    decimal logisticAndTransport = mainAssyTotals.LogisticsIncludingOH + mainAssyTotals.Transport;
                    summaryTable.AddCell(new Phrase(Formatter.FormatMoney(logisticAndTransport), this.pdfReportFont));

                    decimal restOfCosts = mainAssyTotals.DevelopmentCost + mainAssyTotals.ProjectInvest + mainAssyTotals.OtherCostIncludingOH
                        + mainAssyTotals.PackagingCostIncludingOH + mainAssyTotals.PaymentCost;
                    summaryTable.AddCell(new Phrase(Formatter.FormatMoney(restOfCosts), this.pdfReportFont));

                    decimal total = materialCost + materialOH + manufacturingCost + manufacturingOH
                        + sga + companySurcharge + margin + logisticAndTransport + restOfCosts;
                    summaryTable.AddCell(new Phrase(Formatter.FormatMoney(total), this.pdfReportFontBold));

                    summaryTable.CompleteRow();
                    document.Add(summaryTable);

                    // add logo image
                    PdfPTable logoTable = new PdfPTable(1);
                    logoTable.DefaultCell.Border = Table.NO_BORDER;
                    logoTable.SpacingBefore = 10f;
                    Image logo = GetReportLogoForPDFReport();
                    logoTable.AddCell(logo);
                    logoTable.TotalWidth = 70f;
                    logoTable.LockedWidth = true;
                    document.Add(logoTable);

                    document.Close();
                    File.WriteAllBytes(path, memoryStream.GetBuffer());
                }
            }
            catch (Exception ex)
            {
                log.ErrorException("Error while exporting enhanced Assembly report to PDF.", ex);
            }
        }

        /// <summary>
        /// Adds the table header for the assembly enhanced PDF report.
        /// </summary>
        /// <param name="table">The PDF table to add the header to.</param>
        private void FillEnhancedAssyPdfReportTableHeader(PdfPTable table)
        {
            table.DefaultCell.Border = Table.NO_BORDER;
            table.DefaultCell.BorderWidth = 1f;

            table.AddCell(new PdfPCell(table.DefaultCell) { Colspan = 7, CellEvent = null });
            table.AddCell(new PdfPCell(table.DefaultCell)
                {
                    Phrase = new Phrase(LocalizedResources.Report_MaterialCostPlusMaterialOH, pdfReportFontSmallBold),
                    HorizontalAlignment = Table.ALIGN_CENTER,
                    Border = Table.LEFT_BORDER | Table.TOP_BORDER | Table.RIGHT_BORDER | Table.BOTTOM_BORDER,
                    PaddingTop = 10,
                    PaddingBottom = 2,
                    Colspan = 7
                });
            table.AddCell(new PdfPCell(table.DefaultCell)
            {
                Phrase = new Phrase(LocalizedResources.Report_ManufacturingCostPlusManufacturingOH, pdfReportFontSmallBold),
                HorizontalAlignment = Table.ALIGN_CENTER,
                Border = Table.LEFT_BORDER | Table.TOP_BORDER | Table.RIGHT_BORDER | Table.BOTTOM_BORDER,
                PaddingTop = 10,
                PaddingBottom = 2,
                Colspan = 4
            });

            // Add an empty cell spanning all remaining cells in the row.
            table.AddCell(new PdfPCell(table.DefaultCell) { Colspan = table.NumberOfColumns - 18, CellEvent = null });
            table.CompleteRow();

            table.AddCell(new Phrase(LocalizedResources.Report_Pos, pdfReportFontSmall));
            table.AddCell(new Phrase(LocalizedResources.Report_PartNo, pdfReportFontSmall));
            table.AddCell(new Phrase(LocalizedResources.Report_Amount, pdfReportFontSmall));
            table.AddCell(new Phrase(LocalizedResources.Report_Name, pdfReportFontSmall));
            table.AddCell(new Phrase(LocalizedResources.Report_Remark, pdfReportFontSmall));
            table.AddCell(new Phrase(LocalizedResources.Report_EstOrCalc, pdfReportFontSmall));
            table.AddCell(new Phrase(LocalizedResources.Report_IntOrExt, pdfReportFontSmall));
            table.DefaultCell.Border = Table.TOP_BORDER | Table.LEFT_BORDER;
            table.AddCell(new Phrase(LocalizedResources.Report_ExternalPartCostOfCommodities, pdfReportFontSmallBold));
            table.DefaultCell.Border = Table.TOP_BORDER | Table.RIGHT_BORDER;
            table.AddCell(new Phrase(LocalizedResources.Report_OHOnExternalParts, pdfReportFontSmall));
            table.DefaultCell.Border = Table.TOP_BORDER;
            table.AddCell(new Phrase(LocalizedResources.Report_RawMaterial, pdfReportFontSmall));
            table.DefaultCell.Border = Table.TOP_BORDER | Table.RIGHT_BORDER;
            table.AddCell(new Phrase(LocalizedResources.Report_RawMaterialOH, pdfReportFontSmall));
            table.DefaultCell.Border = Table.TOP_BORDER;
            table.AddCell(new Phrase(LocalizedResources.Report_Consumables, pdfReportFontSmall));
            table.DefaultCell.Border = Table.TOP_BORDER | Table.RIGHT_BORDER;
            table.AddCell(new Phrase(LocalizedResources.Report_ConsumablesOH, pdfReportFontSmall));
            table.DefaultCell.Border = Table.TOP_BORDER | Table.RIGHT_BORDER;
            table.AddCell(new Phrase(LocalizedResources.Report_EstimatedPartCost, pdfReportFontSmall));
            table.DefaultCell.Border = Table.TOP_BORDER;
            table.AddCell(new Phrase(LocalizedResources.Report_ManufacturingCostInclScrap, pdfReportFontSmall));
            table.AddCell(new Phrase(LocalizedResources.Report_ManufacturingOH, pdfReportFontSmall));
            table.AddCell(new Phrase(LocalizedResources.Report_nTierTransportCost, pdfReportFontSmall));
            table.DefaultCell.Border = Table.TOP_BORDER | Table.RIGHT_BORDER;
            table.AddCell(new Phrase(LocalizedResources.Report_ExternalWorkOH, pdfReportFontSmall));
            table.DefaultCell.Border = Table.NO_BORDER;
            table.AddCell(new Phrase(LocalizedResources.Report_DevelopmentCost, pdfReportFontSmall));
            table.AddCell(new Phrase(LocalizedResources.Report_ProjCost, pdfReportFontSmall));
            table.AddCell(new Phrase(LocalizedResources.Report_OtherCostInclOH, pdfReportFontSmall));
            table.AddCell(new Phrase(LocalizedResources.Report_PackagingCostInclOH, pdfReportFontSmall));
            table.AddCell(new Phrase(LocalizedResources.Report_LogisticsInclOH, pdfReportFontSmall));
            table.AddCell(new Phrase(LocalizedResources.Report_Transport, pdfReportFontSmall));
            table.AddCell(new Phrase(LocalizedResources.Report_SGAndA, pdfReportFontSmall));
            table.AddCell(new Phrase(LocalizedResources.Report_CompanySurcharge, pdfReportFontSmall));
            table.AddCell(new Phrase(LocalizedResources.Report_MarginOnMaterialOrConsumablesOrCommodities, pdfReportFontSmall));
            table.AddCell(new Phrase(LocalizedResources.Report_MarginOnManufacturingAndAssembly, pdfReportFontSmall));
            table.AddCell(new Phrase(LocalizedResources.Report_PaymentCost, pdfReportFontSmall));
            table.AddCell(new Phrase(LocalizedResources.Report_PiecePrice, pdfReportFontSmallBold));
            table.AddCell(new Phrase(LocalizedResources.Report_TotalPricePerAssembly, pdfReportFontSmallBold));
        }

        /// <summary>
        /// Fills the header row of an enhanced assembly PDF report table with the specified data.
        /// </summary>
        /// <param name="table">The PDF table to fill.</param>
        /// <param name="data">The data to fill the row with.</param>
        private void FillEnhancedAssyPdfReportHeaderRow(PdfPTable table, EnhancedAssyReportHeaderRowData data)
        {
            int innitialBorder = table.DefaultCell.Border;
            table.DefaultCell.BackgroundColor = pdfEnhancedReportHeaderColor;
            table.DefaultCell.HorizontalAlignment = Table.ALIGN_LEFT;
            table.DefaultCell.Border = Table.TOP_BORDER | Table.BOTTOM_BORDER | Table.LEFT_BORDER;
            table.AddCell(new Phrase(data.Position, pdfReportFontSmallBold));
            table.DefaultCell.Border = Table.TOP_BORDER | Table.BOTTOM_BORDER;
            table.AddCell(new Phrase(data.PartNumber, pdfReportFontSmallBold));
            table.DefaultCell.HorizontalAlignment = Table.ALIGN_CENTER;
            table.AddCell(new Phrase(string.Empty, pdfReportFontSmallBold));
            table.DefaultCell.HorizontalAlignment = Table.ALIGN_LEFT;
            table.AddCell(new Phrase(data.Name, pdfReportFontSmallBold));
            table.AddCell(new Phrase(data.Remark, pdfReportFontSmallBold));
            table.AddCell(new Phrase(data.Estimated ? "Est" : "Calc", pdfReportFontSmallBold));
            table.AddCell(new Phrase(data.External ? "Ext" : "Int", pdfReportFontSmallBold));
            table.DefaultCell.HorizontalAlignment = Table.ALIGN_RIGHT;
            table.DefaultCell.Border += Table.LEFT_BORDER;
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.ExternalPartsAndCommoditiesCost).ToString(), pdfReportFontSmallBold));
            table.DefaultCell.Border += Table.RIGHT_BORDER - Table.LEFT_BORDER;
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.ExternalPartsAndCommoditiesOH).ToString(), pdfReportFontSmallBold));
            table.DefaultCell.Border += Table.LEFT_BORDER - Table.RIGHT_BORDER;
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.RawMaterial).ToString(), pdfReportFontSmallBold));
            table.DefaultCell.Border += Table.RIGHT_BORDER - Table.LEFT_BORDER;
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.RawMaterialOH).ToString(), pdfReportFontSmallBold));
            table.DefaultCell.Border += Table.LEFT_BORDER - Table.RIGHT_BORDER;
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.Consumables).ToString(), pdfReportFontSmallBold));
            table.DefaultCell.Border += Table.RIGHT_BORDER - Table.LEFT_BORDER;
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.ConsumablesOH).ToString(), pdfReportFontSmallBold));
            table.DefaultCell.Border += Table.LEFT_BORDER;
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.EstimatedPartCost).ToString(), pdfReportFontSmallBold));
            table.DefaultCell.Border += Table.RIGHT_BORDER - Table.LEFT_BORDER;
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.ManufacturingCostIncludingScrap).ToString(), pdfReportFontSmallBold));
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.ManufacturingOH).ToString(), pdfReportFontSmallBold));
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.ManufacturingTransport).ToString(), pdfReportFontSmallBold));
            table.DefaultCell.Border = Table.TOP_BORDER | Table.BOTTOM_BORDER | Table.RIGHT_BORDER;
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.ExternalWorkOH).ToString(), pdfReportFontSmallBold));
            table.DefaultCell.Border = Table.TOP_BORDER | Table.BOTTOM_BORDER;
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.DevelopmentCost).ToString(), pdfReportFontSmallBold));
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.ProjectInvest).ToString(), pdfReportFontSmallBold));
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.OtherCostIncludingOH).ToString(), pdfReportFontSmallBold));
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.PackagingCostIncludingOH).ToString(), pdfReportFontSmallBold));
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.LogisticsIncludingOH).ToString(), pdfReportFontSmallBold));
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.Transport).ToString(), pdfReportFontSmallBold));
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.SalesAndAdministrationOH).ToString(), pdfReportFontSmallBold));
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.CompanySurchargeOH).ToString(), pdfReportFontSmallBold));
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.MarginOnMaterialConsumablesAndCommodities).ToString(), pdfReportFontSmallBold));
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.MarginOnManufacturingAndAssembly).ToString(), pdfReportFontSmallBold));
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.PaymentCost).ToString(), pdfReportFontSmallBold));
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.PiecePrice).ToString(), pdfReportFontSmallBold));
            table.DefaultCell.Border = Table.TOP_BORDER | Table.BOTTOM_BORDER | Table.RIGHT_BORDER;
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.TotalPricePerAssy).ToString(), pdfReportFontSmallBold));

            table.DefaultCell.Border = innitialBorder;
        }

        /// <summary>
        /// Fills an enhanced assembly PDF report row with the specified data.
        /// </summary>
        /// <param name="table">The PDF table to fill.</param>
        /// <param name="data">The data to fill the row with.</param>
        /// <param name="backgroundColor">The background color of the first columns in each row.</param>
        private void FillEnhancedAssyPdfReportRow(PdfPTable table, EnhancedAssyReportRowData data, Color backgroundColor)
        {
            table.DefaultCell.BackgroundColor = backgroundColor;
            table.DefaultCell.HorizontalAlignment = Table.ALIGN_LEFT;
            table.AddCell(new Phrase(data.Position, pdfReportFontSmall));
            table.AddCell(new Phrase(data.PartNumber, pdfReportFontSmall));
            table.DefaultCell.HorizontalAlignment = Table.ALIGN_CENTER;
            table.AddCell(new Phrase(data.Amount.ToString(), pdfReportFontSmall));
            table.DefaultCell.HorizontalAlignment = Table.ALIGN_LEFT;
            table.AddCell(new Phrase(data.Name, pdfReportFontSmall));
            table.AddCell(new Phrase(data.Remark, pdfReportFontSmall));
            table.AddCell(new Phrase(data.Estimated ? "Est" : "Calc", pdfReportFontSmall));
            table.AddCell(new Phrase(data.External ? "Ext" : "Int", pdfReportFontSmall));

            table.DefaultCell.BackgroundColor = Color.WHITE;
            table.DefaultCell.HorizontalAlignment = Table.ALIGN_RIGHT;
            table.DefaultCell.Border = Table.LEFT_BORDER;
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.ExternalPartsAndCommoditiesCost).ToString(), pdfReportFontSmallBold));
            table.DefaultCell.Border = Table.RIGHT_BORDER;
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.ExternalPartsAndCommoditiesOH).ToString(), pdfReportFontSmall));
            table.DefaultCell.Border = Table.LEFT_BORDER;
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.RawMaterial).ToString(), pdfReportFontSmall));
            table.DefaultCell.Border = Table.RIGHT_BORDER;
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.RawMaterialOH).ToString(), pdfReportFontSmall));
            table.DefaultCell.Border = Table.LEFT_BORDER;
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.Consumables).ToString(), pdfReportFontSmall));
            table.DefaultCell.Border = Table.RIGHT_BORDER;
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.ConsumablesOH).ToString(), pdfReportFontSmall));
            table.DefaultCell.Border = Table.LEFT_BORDER | Table.RIGHT_BORDER;
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.EstimatedPartCost).ToString(), pdfReportFontSmall));
            table.DefaultCell.Border = Table.LEFT_BORDER;
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.ManufacturingCostIncludingScrap).ToString(), pdfReportFontSmall));
            table.DefaultCell.Border = Table.NO_BORDER;
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.ManufacturingOH).ToString(), pdfReportFontSmall));
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.ManufacturingTransport).ToString(), pdfReportFontSmall));
            table.DefaultCell.Border = Table.RIGHT_BORDER;
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.ExternalWorkOH).ToString(), pdfReportFontSmall));
            table.DefaultCell.Border = Table.NO_BORDER;
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.DevelopmentCost).ToString(), pdfReportFontSmall));
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.ProjectInvest).ToString(), pdfReportFontSmall));
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.OtherCostIncludingOH).ToString(), pdfReportFontSmall));
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.PackagingCostIncludingOH).ToString(), pdfReportFontSmall));
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.LogisticsIncludingOH).ToString(), pdfReportFontSmall));
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.Transport).ToString(), pdfReportFontSmall));
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.SalesAndAdministrationOH).ToString(), pdfReportFontSmall));
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.CompanySurchargeOH).ToString(), pdfReportFontSmall));
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.MarginOnMaterialConsumablesAndCommodities).ToString(), pdfReportFontSmall));
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.MarginOnManufacturingAndAssembly).ToString(), pdfReportFontSmall));
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.PaymentCost).ToString(), pdfReportFontSmall));
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.PiecePrice).ToString(), pdfReportFontSmallBold));
            table.AddCell(new Phrase(TruncateTo4DecimalPlaces(data.TotalPricePerAssy).ToString(), pdfReportFontSmallBold));
        }

        #endregion Enhanced Assembly Report Pdf

        #region Helpers

        /// <summary>
        /// Copies an xls table row and inserts it into a new row; will automatically push down any existing rows.
        /// Copy is done cell by cell and tries to copy all properties available (style, merged cells, values, etc...)
        /// </summary>
        /// <param name="sourceWorksheet">WorkSheet containing rows to be copied</param>
        /// <param name="sourceRowNum">Source Row Number</param>
        /// <param name="destinationWorksheet">The worksheet where to copy the row.</param>
        /// <param name="destinationRowNum">Destination Row Number</param>
        /// <returns>The new row created by copy.</returns>
        private IRow CopyRow(ISheet sourceWorksheet, int sourceRowNum, ISheet destinationWorksheet, int destinationRowNum)
        {
            IRow sourceRow = sourceWorksheet.GetRow(sourceRowNum);
            return CopyRow(sourceWorksheet, sourceRowNum, 0, sourceRow.LastCellNum, destinationWorksheet, destinationRowNum, true);
        }

        /// <summary>
        /// Copies an xls table row and inserts it into a new row; will automatically push down any existing rows.
        /// Copy is done cell by cell and tries to copy all properties available (style, merged cells, values, etc...)
        /// </summary>
        /// <param name="sourceWorksheet">WorkSheet containing rows to be copied</param>
        /// <param name="sourceRowNum">Source Row Number</param>
        /// <param name="destinationWorksheet">The worksheet where to copy the row.</param>
        /// <param name="destinationRowNum">Destination Row Number</param>
        /// <param name="copyCellValues">if set to true copy the values in the cells, otherwise don't copy them.</param>
        /// <returns>The new row created by copy.</returns>
        private IRow CopyRow(ISheet sourceWorksheet, int sourceRowNum, ISheet destinationWorksheet, int destinationRowNum, bool copyCellValues)
        {
            IRow sourceRow = sourceWorksheet.GetRow(sourceRowNum);
            return CopyRow(sourceWorksheet, sourceRowNum, 0, sourceRow.LastCellNum, destinationWorksheet, destinationRowNum, copyCellValues);
        }

        /// <summary>
        /// Copies a range of cells from an xls table row and inserts them into a new row on the same position; will automatically push down any existing rows.
        /// Copy is done cell by cell and tries to copy all properties available (style, merged cells, values, etc...)
        /// </summary>
        /// <param name="sourceWorksheet">WorkSheet containing rows to be copied</param>
        /// <param name="sourceRowNum">Source Row Number</param>
        /// <param name="sourceRowCellNumStart">The cell number in the source row where to start copying.</param>
        /// <param name="sourceRowCellNumEnd">The cell number in the source row where to stop copying (the cell with this number is not copied).</param>
        /// <param name="destinationWorksheet">The worksheet where to copy the row.</param>
        /// <param name="destinationRowNum">Destination Row Number</param>
        /// <param name="copyCellValues">if set to true copy the values in the cells, otherwise don't copy them.</param>
        /// <returns>The new row created by copy.</returns>
        private IRow CopyRow(
            ISheet sourceWorksheet,
            int sourceRowNum,
            int sourceRowCellNumStart,
            int sourceRowCellNumEnd,
            ISheet destinationWorksheet,
            int destinationRowNum,
            bool copyCellValues)
        {
            // Get the source / new row
            IRow sourceRow = sourceWorksheet.GetRow(sourceRowNum);
            IRow newRow = destinationWorksheet.GetRow(destinationRowNum);

            // If the row exist in destination, push down all rows by 1 else create a new row
            if (newRow != null)
            {
                destinationWorksheet.ShiftRows(destinationRowNum, destinationWorksheet.LastRowNum, 1, true, false);
            }
            else
            {
                newRow = destinationWorksheet.CreateRow(destinationRowNum);
            }

            // Loop through source columns to add to new row
            for (int i = sourceRowCellNumStart; i < sourceRowCellNumEnd; i++)
            {
                // Grab a copy of the old/new cell
                ICell oldCell = sourceRow.GetCell(i);
                ICell newCell = newRow.CreateCell(i);

                // If the old cell is null jump to next cell
                if (oldCell == null)
                {
                    newCell = null;
                    continue;
                }

                // Copy style from old cell and apply to new cell                
                newCell.CellStyle = oldCell.CellStyle;

                // If there is a cell comment, copy
                if (newCell.CellComment != null)
                {
                    newCell.CellComment = oldCell.CellComment;
                }

                // If there is a cell hyperlink, copy
                if (oldCell.Hyperlink != null)
                {
                    newCell.Hyperlink = oldCell.Hyperlink;
                }

                // Set the cell data value
                if (copyCellValues)
                {
                    // Set the cell data type
                    newCell.SetCellType(oldCell.CellType);

                    switch (oldCell.CellType)
                    {
                        case CellType.BLANK:
                            newCell.SetCellValue(oldCell.StringCellValue);
                            break;
                        case CellType.BOOLEAN:
                            newCell.SetCellValue(oldCell.BooleanCellValue);
                            break;
                        case CellType.ERROR:
                            newCell.SetCellErrorValue(oldCell.ErrorCellValue);
                            break;
                        case CellType.FORMULA:
                            newCell.CellFormula = oldCell.CellFormula;
                            break;
                        case CellType.NUMERIC:
                            newCell.SetCellValue(oldCell.NumericCellValue);
                            break;
                        case CellType.STRING:
                            newCell.SetCellValue(oldCell.RichStringCellValue);
                            break;
                        case CellType.Unknown:
                            newCell.SetCellValue(oldCell.StringCellValue);
                            break;
                    }
                }
            }

            // If there are are any merged regions in the source row, copy to new row
            for (int i = 0; i < sourceWorksheet.NumMergedRegions; i++)
            {
                CellRangeAddress cellRangeAddress = sourceWorksheet.GetMergedRegion(i);
                if (cellRangeAddress.FirstRow == sourceRow.RowNum)
                {
                    CellRangeAddress newCellRangeAddress = new CellRangeAddress(
                        newRow.RowNum,
                        newRow.RowNum + (cellRangeAddress.FirstRow - cellRangeAddress.LastRow),
                        cellRangeAddress.FirstColumn,
                        cellRangeAddress.LastColumn);
                    destinationWorksheet.AddMergedRegion(newCellRangeAddress);
                }
            }

            return newRow;
        }

        /// <summary>
        /// Truncates a decimal number to 4 decimal places and converts it to double.
        /// If an error occurs it returns double.NaN.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <returns>The number truncated and converted to double.</returns>
        private double TruncateTo4DecimalPlaces(decimal number)
        {
            double convertedNumber = 0;

            string numberStrg = Formatter.FormatNumber(number, 4);
            if (!double.TryParse(numberStrg, out convertedNumber))
            {
                // Try to truncate by multiplying with 10000, discarding all decimal places and then dividing by 10000.
                // This may not be possible if the number is close to the max double value.
                if (decimal.MaxValue / 10000m >= number)
                {
                    decimal truncatedNumber = decimal.Truncate(number * 10000m) / 10000m;
                    convertedNumber = Convert.ToDouble(truncatedNumber);
                }
                else
                {
                    // The number could not be truncated by the above method so convert it to string and keep at most 4 decimals
                    string numberAsString = number.ToString();
                    string decimalSeparatorSymbol = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
                    int decimalSeparatorIndex = numberAsString.IndexOf(decimalSeparatorSymbol);
                    if (decimalSeparatorIndex > 0 && numberAsString.Length > 0)
                    {
                        int numberOfDecimalPlaces = numberAsString.Length - 1 - decimalSeparatorIndex;
                        if (numberOfDecimalPlaces > 4)
                        {
                            numberAsString = numberAsString.Substring(0, decimalSeparatorIndex + 5);
                        }
                    }

                    if (!double.TryParse(numberAsString, out convertedNumber))
                    {
                        log.Error("Failed to convert the string \"{0}\" to double.", numberAsString);
                        convertedNumber = double.NaN;
                    }
                }
            }

            return convertedNumber;
        }

        /// <summary>
        /// Adds a picture in a worksheet in an area defined by a top left cell and a bottom right cell number.
        /// </summary>
        /// <param name="workbook">The workbook to which the sheet belongs.</param>
        /// <param name="sheet">The excel sheet to fill.</param>
        /// <param name="picture">The media that contains the image.</param>
        /// <param name="topLeftRowNum">The row index of the top left cell.</param>
        /// <param name="topLeftColNum">The column index of the top left cell.</param>
        /// <param name="bottomRightRowNum">The row index of the bottom right cell.</param>
        /// <param name="bottomRightColNum">The column index of the bottom right cell.</param>
        private void AddReportPicture(
            IWorkbook workbook,
            ISheet sheet,
            Media picture,
            int topLeftRowNum,
            int topLeftColNum,
            int bottomRightRowNum,
            int bottomRightColNum)
        {
            if (sheet == null || picture == null)
            {
                return;
            }

            if ((MediaType)picture.Type == MediaType.Image && picture.Content.Length > 0)
            {
                HSSFPatriarch patriarch = null;
                if (sheet.DrawingPatriarch != null)
                {
                    patriarch = (HSSFPatriarch)sheet.DrawingPatriarch;
                }
                else
                {
                    patriarch = (HSSFPatriarch)sheet.CreateDrawingPatriarch();
                }

                HSSFClientAnchor anchor = new HSSFClientAnchor(0, 0, 0, 0, topLeftColNum, topLeftRowNum, bottomRightColNum, bottomRightRowNum);
                anchor.AnchorType = 2;
                int picIndex = workbook.AddPicture(MediaUtils.GetMediaJPEGImageBytes(picture.Content), PictureType.JPEG);
                HSSFPicture pic = (HSSFPicture)patriarch.CreatePicture(anchor, picIndex);
            }
        }

        /// <summary>
        /// Creates a shape for a process step and adds it in the specified shape group according to the input parameters.
        /// </summary>
        /// <param name="parentGroup">The group in which to add the shape.</param>
        /// <param name="shapeWidth">Width of the shape.</param>
        /// <param name="offsetX">The horizontal offset of the shape inside the parent group.</param>
        /// <param name="stepName">Name of the step.</param>
        /// <param name="stepIndex">Index of the step.</param>
        /// <param name="font">The font used to display the step name and step index.</param>
        private void CreateProcessStepShape(HSSFShapeGroup parentGroup, int shapeWidth, int offsetX, string stepName, string stepIndex, IFont font)
        {
            int shapeX1 = offsetX;
            int shapeY1 = 0;
            int shapeX2 = offsetX + shapeWidth;
            int shapeY2 = parentGroup.Y2 - 10;

            // Create a shape group to hold the shapes that will form the step shape
            HSSFShapeGroup shapeGroup = parentGroup.CreateGroup(new HSSFChildAnchor(shapeX1, shapeY1, shapeX2, shapeY2));
            shapeGroup.SetCoordinates(shapeX1, shapeY1, shapeX2, shapeY2);

            // Create the background shape
            HSSFPolygon poly = shapeGroup.CreatePolygon(new HSSFChildAnchor(shapeX1, shapeY1, shapeX2, shapeY2));
            poly.SetPolygonDrawArea(shapeWidth, shapeY2);
            poly.SetPoints(new int[] { 0, shapeWidth - 5, shapeWidth, shapeWidth - 5, 0, 5 }, new int[] { 0, 0, shapeY2 / 2, shapeY2, shapeY2, shapeY2 / 2 });
            poly.SetFillColor(155, 173, 208);

            // Create the step name label and place it in the center of the background shape
            HSSFTextbox descriptionTextBox = shapeGroup.CreateTextbox(new HSSFChildAnchor(shapeX1 + 10, shapeY1, shapeX2 - 10, shapeY2));
            descriptionTextBox.IsNoFill = true;
            descriptionTextBox.HorizontalAlignment = HSSFTextbox.HORIZONTAL_ALIGNMENT_CENTERED;
            descriptionTextBox.VerticalAlignment = HSSFTextbox.VERTICAL_ALIGNMENT_CENTER;
            descriptionTextBox.LineStyle = LineStyle.None;
            descriptionTextBox.String = new HSSFRichTextString(stepName);
            if (font != null)
            {
                descriptionTextBox.String.ApplyFont(font);
            }

            // Create a shape group for the index circle and label
            HSSFShapeGroup indexGroup = shapeGroup.CreateGroup(new HSSFChildAnchor(shapeX1 - 5, shapeY1 - 20, shapeX2, shapeY2));

            // Create the index circle
            HSSFSimpleShape circle = indexGroup.CreateShape(new HSSFChildAnchor(0, 0, 180, 90));
            circle.ShapeType = HSSFSimpleShape.OBJECT_TYPE_OVAL;
            circle.SetFillColor(250, 203, 72);
            circle.LineStyle = LineStyle.None;

            // Create the index label and place it in the center of the circle
            HSSFTextbox numberTextBox = indexGroup.CreateTextbox(new HSSFChildAnchor(0, 0, 180, 90));
            numberTextBox.IsNoFill = true;
            numberTextBox.HorizontalAlignment = HSSFTextbox.HORIZONTAL_ALIGNMENT_CENTERED;
            numberTextBox.VerticalAlignment = HSSFTextbox.VERTICAL_ALIGNMENT_CENTER;
            numberTextBox.LineStyle = LineStyle.None;
            numberTextBox.String = new HSSFRichTextString(stepIndex);
            if (font != null)
            {
                numberTextBox.String.ApplyFont(font);
            }
        }

        /// <summary>
        /// Sets the acroFields field.
        /// </summary>
        /// <param name="acroFields">The acro fields.</param>
        /// <param name="name">The name of field which has to be set.</param>
        /// <param name="value">The value of filed.</param>
        private void SetField(AcroFields acroFields, string name, string value)
        {
            try
            {
                acroFields.SetField(name, !string.IsNullOrEmpty(value) ? value : string.Empty);
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Gets the symbol of the selected UI currency.
        /// </summary>
        /// <returns>The UI currency symbol.</returns>
        private string GetUICurrencySymbol()
        {
            if (this.modelBrowserHelper != null || GlobalSettingsManager.Instance.StartInViewerMode)
            {
                // If the app is in viewer mode, the currency symbol is read from ViewerUICurrencyIsoCode setting.
                return UserSettingsManager.Instance.ViewerUICurrencyIsoCode;
            }
            else
            {
                // If the app is not in viewer mode, the currency symbol is symbol of the UICurrency.
                return SecurityManager.Instance.UICurrency != null ? SecurityManager.Instance.UICurrency.Symbol : string.Empty;
            }
        }

        /// <summary>
        /// Adjusts the raw material price for the new weight unit
        /// The price on a raw material is expressed on his weight unit and needs an adjustment to the weight unit on the UI
        /// </summary>
        /// <param name="part">The parent Part of the entity</param>
        /// <param name="cost">The cost of the Raw Material</param>
        /// <returns>The new Raw Material Price if it can be adjusted or the old price</returns>
        private decimal AdjustRawMaterialPriceToWeightUnit(Part part, RawMaterialCost cost)
        {
            var material = part.RawMaterials.FirstOrDefault(m => m.Guid == cost.RawMaterialId);
            if (this.measurementUnitsAdapter == null || material == null)
            {
                return cost.Price;
            }

            MeasurementUnit priceUnit = null;
            var weightUnit = this.measurementUnitsAdapter.GetBaseMeasurementUnit(MeasurementUnitType.Weight);
            priceUnit = material.QuantityUnitBase ?? material.ParentWeightUnitBase;

            return CostCalculationHelper.AdjustRawMaterialPriceToNewWeightUnit(cost.Price, priceUnit, weightUnit);
        }

        #endregion Helpers

        #region Private Classes

        /// <summary>
        /// Custom PdfPCellEvent implementation
        /// </summary>
        private class PdfCellLayoutEvent : IPdfPCellEvent
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="PdfCellLayoutEvent"/> class.
            /// </summary>
            public PdfCellLayoutEvent()
            {
                CellSpacing = new Thickness(0d);
                FillColor = Color.WHITE;
                DrawCellBorder = false;
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="PdfCellLayoutEvent"/> class.
            /// </summary>
            /// <param name="cellSpacing">The spacing between table cells.</param>
            public PdfCellLayoutEvent(Thickness cellSpacing)
            {
                CellSpacing = cellSpacing;
                FillColor = Color.WHITE;
                DrawCellBorder = false;
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="PdfCellLayoutEvent"/> class.
            /// </summary>
            /// <param name="cellSpacing">The space between table cells.</param>
            /// <param name="fillColor">The color to be used to fill the cells.</param>
            public PdfCellLayoutEvent(Thickness cellSpacing, Color fillColor)
            {
                CellSpacing = cellSpacing;
                FillColor = fillColor;
                DrawCellBorder = false;
            }

            /// <summary>
            /// Gets or sets a value indicating whether to draw the cell borders.
            /// </summary>
            public bool DrawCellBorder { get; set; }

            /// <summary>
            /// Gets or sets the space between cells.
            /// </summary>
            public Thickness CellSpacing { get; set; }

            /// <summary>
            /// Gets or sets the color to use for filling the cell.
            /// </summary>
            public Color FillColor { get; set; }

            /// <summary>
            /// This method is called at the end of the cell rendering.
            /// The text or graphics are added to one of the 4 PdfContentByte instances contained in <paramref name="canvases"/>.
            /// </summary>
            /// <param name="cell">The rendered cell.</param>
            /// <param name="position">The cell position.</param>
            /// <param name="canvases">The rendering canvases.</param>
            public void CellLayout(PdfPCell cell, Rectangle position, PdfContentByte[] canvases)
            {
                if (this.DrawCellBorder)
                {
                    float x1 = position.Left;
                    float y1 = position.Top;
                    float x2 = position.Right;
                    float y2 = position.Bottom;

                    PdfContentByte canvas = canvases[PdfPTable.BACKGROUNDCANVAS];
                    canvas.Rectangle(x1, y1, x2 - x1, y2 - y1);
                    canvas.SetLineWidth(0.25f);
                    canvas.SetColorStroke(Color.DARK_GRAY);
                    canvas.Stroke();
                }

                if (this.CellSpacing != new Thickness(0d))
                {
                    // simulate cell spacing using canvas
                    float x1 = position.Left + Convert.ToSingle(CellSpacing.Left);
                    float y1 = position.Top - Convert.ToSingle(CellSpacing.Top) - 1;
                    float x2 = position.Right - Convert.ToSingle(CellSpacing.Right);
                    float y2 = position.Bottom + Convert.ToSingle(CellSpacing.Bottom) - 1;

                    PdfContentByte canvas = canvases[PdfPTable.BACKGROUNDCANVAS];
                    canvas.Rectangle(x1, y1, x2 - x1, y2 - y1);
                    canvas.SetLineWidth(0.5f);
                    canvas.SetColorStroke(Color.BLACK);
                    canvas.SetColorFill(FillColor);
                    canvas.FillStroke();
                }
            }
        }

        /// <summary>
        /// Custom PdfPTableEvent implementation
        /// </summary>
        private class PdfTableLayoutEvent : IPdfPTableEvent
        {
            /// <summary>
            /// The image to be used as a background for the table.
            /// </summary>
            private Image image;

            /// <summary>
            /// Compensation factor between <see cref="Image"/> size and image real size.
            /// </summary>
            private float imageSizeCompensationFactor = 1.4f;

            /// <summary>
            /// Initializes a new instance of the <see cref="PdfTableLayoutEvent"/> class.
            /// </summary>
            /// <param name="image">The background image to use for the table.</param>
            public PdfTableLayoutEvent(Image image)
            {
                this.image = image;
            }

            /// <summary>
            /// This method is called at the end of the table rendering.
            /// The text or graphics are added to one of the 4 PdfContentByte instances contained in <paramref name="canvases"/>.
            /// </summary>
            /// <param name="table">The table.</param>
            /// <param name="widths">An array of arrays with the cells' x positions. It has the length of the number of rows.</param>
            /// <param name="heights">An array with the cells' y positions. It has a length of the number of rows + 1.</param>
            /// <param name="headerRows">The number of rows defined for the header.</param>
            /// <param name="rowStart">The first row number after the header.</param>
            /// <param name="canvases">An array of PdfContentByte.</param>
            public void TableLayout(PdfPTable table, float[][] widths, float[] heights, int headerRows, int rowStart, PdfContentByte[] canvases)
            {
                if (image == null)
                {
                    return;
                }

                try
                {
                    this.image.SetAbsolutePosition(widths[0][0], heights[0] - (this.image.Height / this.imageSizeCompensationFactor));
                }
                catch
                {
                    this.image.SetAbsolutePosition(0, 0);
                }

                this.image.ScaleAbsolute(this.image.Width / this.imageSizeCompensationFactor, this.image.Height / this.imageSizeCompensationFactor);

                PdfContentByte canvas = canvases[PdfPTable.BACKGROUNDCANVAS];
                canvas.AddImage(this.image);
                canvas.Fill();
            }
        }

        #endregion Private Classes
    }
}
