﻿using System;
using ZPKTool.Data;

namespace ZPKTool.Gui
{
    /// <summary>
    /// Represents a unit of measurement used to display a value on the UI. It handles as one type the MeasurementUnit from database, Currency and other
    /// measurement units defined in code.
    /// </summary>
    public class Unit
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Unit"/> class.
        /// </summary>
        public Unit()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Unit"/> class.
        /// </summary>
        /// <param name="type">The type of the unit.</param>
        /// <param name="symbol">The symbol of the unit.</param>
        /// <param name="conversionFactor">The conversion factor.</param>
        public Unit(UnitType type, string symbol, decimal conversionFactor)
        {
            this.Type = type;
            this.Symbol = symbol;
            this.ConversionFactor = conversionFactor;
            this.Scale = MeasurementUnitScale.None;
            this.ScaleFactor = 1m;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Unit"/> class.
        /// </summary>
        /// <param name="type">The type of the unit.</param>
        /// <param name="symbol">The symbol of the unit.</param>
        /// <param name="conversionFactor">The conversion factor.</param>
        /// <param name="scale">The scale.</param>
        public Unit(UnitType type, string symbol, decimal conversionFactor, MeasurementUnitScale scale)
            : this(type, symbol, conversionFactor)
        {
            this.Scale = scale;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Unit"/> class.
        /// </summary>
        /// <param name="currency">The currency wrapped by this instance.</param>
        public Unit(Currency currency)
            : this(UnitType.Currency, currency.Symbol, currency.ExchangeRate)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Unit"/> class.
        /// </summary>
        /// <param name="measurementUnit">The measurement unit wrapped by this instance.</param>
        public Unit(MeasurementUnit measurementUnit)
        {
            this.Type = Unit.ConvertUnitType((MeasurementUnitType)measurementUnit.Type);
            this.Symbol = measurementUnit.Symbol;
            this.ConversionFactor = measurementUnit.ConversionRate;
            if (measurementUnit.ScaleID.HasValue)
            {
                this.Scale = (MeasurementUnitScale)measurementUnit.ScaleID;
            }
            else
            {
                this.Scale = MeasurementUnitScale.None;
            }

            this.ScaleFactor = measurementUnit.ScaleFactor;
        }

        #endregion Constructor

        #region Properties

        /// <summary>
        /// Gets the type of the unit.
        /// </summary>
        public UnitType Type { get; private set; }

        /// <summary>
        /// Gets the symbol.
        /// </summary>
        public string Symbol { get; private set; }

        /// <summary>
        /// Gets the conversion factor used to convert from this unit into the base unit in which values are stored in the database.
        /// </summary>
        public decimal ConversionFactor { get; private set; }

        /// <summary>
        /// Gets the scale.
        /// </summary>        
        public MeasurementUnitScale Scale { get; private set; }

        /// <summary>
        /// Gets the scale factor, which is similar to the conversion factor, but used to convert to the base value of a scale, not the base value of db.
        /// </summary>        
        public decimal ScaleFactor { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this unit is a base unit.
        /// </summary>
        public bool IsBaseUnit
        {
            get
            {
                return this.ScaleFactor == 1m;
            }
        }

        #endregion Properties

        /// <summary>
        /// Converts between MeasurementUnitType and UnitType.
        /// </summary>
        /// <param name="unitType">Type of the measurement unit to convert.</param>
        /// <returns>The corresponding UnitType.</returns>
        public static UnitType ConvertUnitType(MeasurementUnitType unitType)
        {
            UnitType type;
            switch (unitType)
            {
                case MeasurementUnitType.Area:
                    type = UnitType.Area;
                    break;
                case MeasurementUnitType.Length:
                    type = UnitType.Length;
                    break;
                case MeasurementUnitType.Volume:
                    type = UnitType.Volume;
                    break;
                case MeasurementUnitType.Weight:
                    type = UnitType.Weight;
                    break;
                case MeasurementUnitType.Time:
                    type = UnitType.Time;
                    break;
                case MeasurementUnitType.Pieces:
                    type = UnitType.Pieces;
                    break;
                case MeasurementUnitType.Volume2:
                    type = UnitType.VolumeLiquids;
                    break;
                default:
                    string msg = string.Format("The conversion from the MeasurementUnitType '{0}' to Unit is not implemented.", unitType);
                    throw new NotImplementedException(msg);
            }

            return type;
        }

        /// <summary>
        /// Converts between MeasurementUnitType and UnitType.
        /// </summary>
        /// <param name="unitType">Type of the UI unit to convert.</param>
        /// <returns>The corresponding MeasurementUnitType.</returns>
        public static MeasurementUnitType ConvertUnitType(UnitType unitType)
        {
            MeasurementUnitType type;
            switch (unitType)
            {
                case UnitType.Area:
                    type = MeasurementUnitType.Area;
                    break;
                case UnitType.Length:
                    type = MeasurementUnitType.Length;
                    break;
                case UnitType.Volume:
                    type = MeasurementUnitType.Volume;
                    break;
                case UnitType.Weight:
                    type = MeasurementUnitType.Weight;
                    break;
                case UnitType.Time:
                    type = MeasurementUnitType.Time;
                    break;
                case UnitType.Pieces:
                    type = MeasurementUnitType.Pieces;
                    break;
                case UnitType.VolumeLiquids:
                    type = MeasurementUnitType.Volume2;
                    break;
                default:
                    string msg = string.Format("The conversion of the UnitType '{0}' to MeasurementUnitType is not implemented.", unitType);
                    throw new NotImplementedException(msg);
            }

            return type;
        }

        /// <summary>
        /// Converts the value from a unit into another unit.
        /// The <paramref name="oldUnit"/> and <paramref name="newUnit"/> must have the same type.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="oldUnit">The unit to convert from.</param>
        /// <param name="newUnit">The unit to convert to.</param>
        /// <returns>The converted value.</returns>
        /// <exception cref="ArgumentNullException">A parameter was null.</exception>
        /// <exception cref="InvalidOperationException"><paramref name="oldUnit"/> and <paramref name="newUnit"/> did not have the same type.</exception>
        public static decimal ConvertValue(decimal value, MeasurementUnit oldUnit, MeasurementUnit newUnit)
        {
            if (oldUnit == null)
            {
                throw new ArgumentNullException("oldUnit", "The old unit can't be null");
            }

            if (newUnit == null)
            {
                throw new ArgumentNullException("newUnit", "The new unit can't be null");
            }

            return ConvertValue(value, new Unit(oldUnit), new Unit(newUnit));
        }

        /// <summary>
        /// Converts the value from a unit into another unit.
        /// The <paramref name="oldUnit"/> and <paramref name="newUnit"/> must have the same type.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="oldUnit">The unit to convert from.</param>
        /// <param name="newUnit">The unit to convert to.</param>
        /// <returns>The converted value.</returns>
        /// <exception cref="ArgumentNullException">A parameter was null.</exception>
        /// <exception cref="InvalidOperationException"><paramref name="oldUnit"/> and <paramref name="newUnit"/> did not have the same type.</exception>
        public static decimal ConvertValue(decimal value, Unit oldUnit, Unit newUnit)
        {
            if (oldUnit == null)
            {
                throw new ArgumentNullException("oldUnit", "The old unit can't be null");
            }

            if (newUnit == null)
            {
                throw new ArgumentNullException("newUnit", "The new unit can't be null");
            }

            if (oldUnit.Type != newUnit.Type)
            {
                // No conversion can be made between different types of units (meters to kilograms, for example).                
                throw new InvalidOperationException(string.Format(
                    "Can not convert a value to an unit of different type than the source unit. Attempted to convert '{0}' to '{1}'.",
                    oldUnit.Symbol,
                    newUnit.Symbol));
            }

            if (oldUnit.CompareTo(newUnit))
            {
                // The old and new unit instances represent the same unit.
                return value;
            }

            decimal convertedValue = 0m;
            if (oldUnit.Scale == MeasurementUnitScale.None && newUnit.Scale == MeasurementUnitScale.None)
            {
                // This is the case of units that are not measurement units, like currency and percentage;
                // the conversion logic is different than for measurement units of the same scale (actually, the logic is inverted).
                convertedValue = (value / oldUnit.ConversionFactor) * newUnit.ConversionFactor;
            }
            else if (oldUnit.Scale == newUnit.Scale)
            {
                // The old and new unit are from the same measurement scale (for example, kilogram and gram).
                convertedValue = (value * oldUnit.ScaleFactor) / newUnit.ScaleFactor;
            }
            else
            {
                // The old and new unit are from different measurement scales of the same unit type (kilogram and pound, for example).
                convertedValue = (value * oldUnit.ConversionFactor) / newUnit.ConversionFactor;
            }

            return convertedValue;
        }

        /// <summary>
        /// Compares the specified object with this instance. 
        /// The comparison is done member wise and fails if at least one member does not have the same value.
        /// </summary>
        /// <param name="obj">The object to compare to.</param>
        /// <returns>True if the objects contain the same data, otherwise false. False if <paramref name="obj"/> is null.</returns>
        public bool CompareTo(Unit obj)
        {
            if (obj == null)
            {
                return false;
            }

            if (this.Type == obj.Type
                && this.ConversionFactor == obj.ConversionFactor
                && this.Symbol == obj.Symbol
                && this.Scale == obj.Scale
                && this.ScaleFactor == obj.ScaleFactor)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Copies the data to another instance.
        /// </summary>
        /// <param name="dest">The copy destination instance.</param>
        public void CopyTo(Unit dest)
        {
            if (dest == null)
            {
                return;
            }

            dest.ConversionFactor = this.ConversionFactor;
            dest.Symbol = this.Symbol;
            dest.Type = this.Type;
            dest.Scale = this.Scale;
            dest.ScaleFactor = this.ScaleFactor;
        }

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns>A deep copy of this instance.</returns>
        public Unit Clone()
        {
            var clone = new Unit();
            this.CopyTo(clone);
            return clone;
        }

        /// <summary>
        /// Puts the information in this instance into a new MeasurementUnit instance.
        /// It works only if this instance represents a MeasurementUnit (or was built from one).
        /// </summary>
        /// <returns>A measurement unit instance.</returns>
        public MeasurementUnit ToMeasurementUnit()
        {
            MeasurementUnit measurementUnit = new MeasurementUnit();
            measurementUnit.Type = Unit.ConvertUnitType(this.Type);
            measurementUnit.Symbol = this.Symbol;
            measurementUnit.ConversionRate = this.ConversionFactor;
            measurementUnit.ScaleID = this.Scale != MeasurementUnitScale.None ? (MeasurementUnitScale?)this.Scale : null;
            measurementUnit.ScaleFactor = this.ScaleFactor;
            return measurementUnit;
        }        
    }
}