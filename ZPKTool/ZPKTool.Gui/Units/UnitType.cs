﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui
{
    /// <summary>
    /// The categories of units which can be displayed on the UI.
    /// </summary>
    public enum UnitType
    {
        /// <summary>
        /// Represents the lack of a unit.
        /// </summary>
        None = 0,

        /// <summary>
        /// The unit is a currency. The corresponding database base unit is 1 euro.
        /// </summary>e
        Currency = 1,

        /// <summary>
        /// This unit is for measuring the length. The corresponding database base unit is 1 meter.
        /// </summary>
        Length = 2,

        /// <summary>
        /// This unit is for measuring the weight. The corresponding database base unit is 1 kg.
        /// </summary>
        Weight = 3,

        /// <summary>
        /// This unit is for measuring the volume. The corresponding database base unit is 1 cubic meter.
        /// </summary>
        Volume = 4,

        /// <summary>
        /// This unit is for measuring the area. The corresponding database base unit is 1 square meter.
        /// </summary>
        Area = 5,

        /// <summary>
        /// This unit represents a percent. The corresponding database base unit is "value divided by 100".
        /// </summary>
        Percentage = 6,

        /// <summary>
        /// This unit is for measuring time. The corresponding database base unit is 1 second.
        /// </summary>
        Time = 7,

        /// <summary>
        /// This type is for units that measure liquids. The corresponding database base unit is 1 liter.
        /// </summary>
        VolumeLiquids = 8,

        /// <summary>
        /// This type is for units that represent a number of pieces. The database base unit is 1 piece (pcs).
        /// </summary>
        Pieces = 9
    }
}