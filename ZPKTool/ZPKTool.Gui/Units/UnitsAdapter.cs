﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using ZPKTool.Business;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Resources;

namespace ZPKTool.Gui
{
    /// <summary>
    /// This class has the role of managing the UI units and base currency. It handles the UI currency and system of units changes.
    /// </summary>
    public class UnitsAdapter : ObservableObject
    {
        #region Fields

        /// <summary>
        /// The UI currency changed listener.
        /// </summary>
        private readonly WeakEventListener<UICurrencyChangedArgs> uiCurrencyChangedListener;

        /// <summary>
        /// The viewer UI currency changed listener.
        /// </summary>
        private readonly WeakEventListener<PropertyChangedEventArgs> viewerUICurrencyChangedListener;

        /// <summary>
        /// The units system changed listener.
        /// </summary>
        private readonly WeakEventListener<PropertyChangedEventArgs> unitsSystemChangedListener;

        /// <summary>
        /// Gets or sets the data manager weak reference.
        /// </summary>
        private WeakReference dataManagerWeakReference;

        /// <summary>
        /// The currenct units system.
        /// </summary>
        private UnitsSystem currentUnitsSystem;

        /// <summary>
        /// All the measurement units.
        /// </summary>
        private Collection<MeasurementUnit> allMeasurementUnits;

        /// <summary>
        /// The measurement units.
        /// </summary>
        private Collection<MeasurementUnit> measurementUnits;

        /// <summary>
        /// The previous measurement units.
        /// </summary>
        private Collection<MeasurementUnit> previousMeasurementUnits;

        /// <summary>
        /// The currencies.
        /// </summary>
        private ICollection<Currency> currencies;

        /// <summary>
        /// The base currency.
        /// </summary>
        private Currency baseCurrency;

        /// <summary>
        /// The UI currency unit.
        /// </summary>
        private Unit uiCurrency;

        /// <summary>
        /// The UI length unit.
        /// </summary>
        private Unit uiLength;

        /// <summary>
        /// The UI weight unit.
        /// </summary>
        private Unit uiWeight;

        /// <summary>
        /// The UI volume unit.
        /// </summary>
        private Unit uiVolume;

        /// <summary>
        /// The UI area unit.
        /// </summary>
        private Unit uiArea;

        /// <summary>
        /// The UI time unit.
        /// </summary>
        private Unit uiTime;

        /// <summary>
        /// The UI volume liquids unit.
        /// </summary>
        private Unit uiVolumeLiquids;

        /// <summary>
        /// The UI pieces unit.
        /// </summary>
        private Unit uiPieces;

        /// <summary>
        /// The UI percentage unit.
        /// </summary>
        private Unit uiPercentage;

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitsAdapter" /> class.
        /// If the data source manager is null, the units adapter is used in viewer mode.
        /// </summary>
        /// <param name="dsm">The data source manager.</param>
        public UnitsAdapter(IDataSourceManager dsm)
        {
            if (dsm != null)
            {
                this.dataManagerWeakReference = new WeakReference(dsm);
                this.allMeasurementUnits = dsm.MeasurementUnitRepository.GetAll();

                this.uiCurrencyChangedListener = new WeakEventListener<UICurrencyChangedArgs>(this.HandleUICurrencyChanged);
                UICurrencyChangedEventManager.AddListener(SecurityManager.Instance, uiCurrencyChangedListener);
            }
            else
            {
                this.InitializeUnitsForViewerMode();

                this.viewerUICurrencyChangedListener = new WeakEventListener<PropertyChangedEventArgs>(this.HandleViewerUICurrencyChanged);
                PropertyChangedEventManager.AddListener(
                    UserSettingsManager.Instance,
                    viewerUICurrencyChangedListener,
                    ReflectionUtils.GetPropertyName(() => UserSettingsManager.Instance.ViewerUICurrencyIsoCode));
            }

            this.unitsSystemChangedListener = new WeakEventListener<PropertyChangedEventArgs>(this.HandleUnitsSystemChanged);
            PropertyChangedEventManager.AddListener(
                UserSettingsManager.Instance,
                unitsSystemChangedListener,
                ReflectionUtils.GetPropertyName(() => UserSettingsManager.Instance.UnitsSystem));

            // Get the current units system.
            this.currentUnitsSystem = UserSettingsManager.Instance.UnitsSystem;

            this.previousMeasurementUnits = new Collection<MeasurementUnit>();
            this.MeasurementUnits = new Collection<MeasurementUnit>();
            this.MeasurementUnits.AddRange(this.allMeasurementUnits);
            this.RefreshMeasurementUnitsOfCurrentUnitsSystem();

            // Initialize the UI units, some units are static and some depend on the system of units used.
            this.UIPercentage = new Unit(UnitType.Percentage, LocalizedResources.General_PercentSymbol, 100m);
            this.UITime = new Unit(UnitType.Time, "s", 1m, MeasurementUnitScale.TimeScale);
            this.UIVolumeLiquids = new Unit(UnitType.VolumeLiquids, "l", 1m, MeasurementUnitScale.MetricLiquidsVolumeScale);
            this.UIPieces = new Unit(UnitType.Pieces, "pcs", 1m, MeasurementUnitScale.PiecesScale);
            this.RefreshUIUnitsOfCurrentUnitsSystem();
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Gets the data manager.
        /// </summary>
        public IDataSourceManager DataManager
        {
            get
            {
                return this.dataManagerWeakReference != null && this.dataManagerWeakReference.IsAlive ? (IDataSourceManager)this.dataManagerWeakReference.Target : null;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the data manager is alive.
        /// </summary>
        public bool IsDataManagerAlive
        {
            get
            {
                if (this.dataManagerWeakReference == null)
                {
                    return true;
                }
                else
                {
                    return this.dataManagerWeakReference.IsAlive;
                }
            }
        }

        /// <summary>
        /// Gets or sets the currencies.
        /// </summary>
        public ICollection<Currency> Currencies
        {
            get
            {
                return this.currencies;
            }

            set
            {
                if (this.currencies != value)
                {
                    this.OnPropertyChanging(() => this.Currencies);
                    this.currencies = value;

                    if (this.DataManager != null)
                    {
                        this.RefreshUICurrencyUnit(SecurityManager.Instance.UICurrency.IsoCode);
                    }
                    else
                    {
                        this.RefreshUICurrencyUnit(UserSettingsManager.Instance.ViewerUICurrencyIsoCode);
                    }

                    this.OnPropertyChanged(() => this.Currencies);
                }
            }
        }

        /// <summary>
        /// Gets the measurement units.
        /// </summary>
        public Collection<MeasurementUnit> MeasurementUnits
        {
            get { return this.measurementUnits; }
            private set { this.SetProperty(ref this.measurementUnits, value, () => this.MeasurementUnits); }
        }

        /// <summary>
        /// Gets or sets the base currency.
        /// </summary>
        public Currency BaseCurrency
        {
            get { return this.baseCurrency; }
            set { this.SetProperty(ref this.baseCurrency, value, () => this.BaseCurrency); }
        }

        /// <summary>
        /// Gets the UI currency unit.
        /// </summary>
        public Unit UICurrency
        {
            get { return this.uiCurrency; }
            private set { this.SetProperty(ref this.uiCurrency, value, () => this.UICurrency); }
        }

        /// <summary>
        /// Gets the UI length unit.
        /// </summary>
        public Unit UILength
        {
            get { return this.uiLength; }
            private set { this.SetProperty(ref this.uiLength, value, () => this.UILength); }
        }

        /// <summary>
        /// Gets the UI weight unit.
        /// </summary>
        public Unit UIWeight
        {
            get { return this.uiWeight; }
            private set { this.SetProperty(ref this.uiWeight, value, () => this.UIWeight); }
        }

        /// <summary>
        /// Gets the UI volume unit.
        /// </summary>
        public Unit UIVolume
        {
            get { return this.uiVolume; }
            private set { this.SetProperty(ref this.uiVolume, value, () => this.UIVolume); }
        }

        /// <summary>
        /// Gets the UI area unit.
        /// </summary>
        public Unit UIArea
        {
            get { return this.uiArea; }
            private set { this.SetProperty(ref this.uiArea, value, () => this.UIArea); }
        }

        /// <summary>
        /// Gets the UI time unit.
        /// </summary>
        public Unit UITime
        {
            get { return this.uiTime; }
            private set { this.SetProperty(ref this.uiTime, value, () => this.UITime); }
        }

        /// <summary>
        /// Gets the UI volume liquids unit.
        /// </summary>
        public Unit UIVolumeLiquids
        {
            get { return this.uiVolumeLiquids; }
            private set { this.SetProperty(ref this.uiVolumeLiquids, value, () => this.UIVolumeLiquids); }
        }

        /// <summary>
        /// Gets the UI pieces unit.
        /// </summary>
        public Unit UIPieces
        {
            get { return this.uiPieces; }
            private set { this.SetProperty(ref this.uiPieces, value, () => this.UIPieces); }
        }

        /// <summary>
        /// Gets the UI percentage unit.
        /// </summary>
        public Unit UIPercentage
        {
            get { return this.uiPercentage; }
            private set { this.SetProperty(ref this.uiPercentage, value, () => this.UIPercentage); }
        }

        #endregion Properties

        /// <summary>
        /// Handles the UI currency change.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void HandleUICurrencyChanged(object sender, UICurrencyChangedArgs e)
        {
            if (e.NewCurrency != null
                && e.NewCurrency != e.OldCurrency)
            {
                this.RefreshUICurrencyUnit(e.NewCurrency.IsoCode);
            }
        }

        /// <summary>
        /// Handles the system units changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void HandleViewerUICurrencyChanged(object sender, PropertyChangedEventArgs e)
        {
            this.RefreshUICurrencyUnit(UserSettingsManager.Instance.ViewerUICurrencyIsoCode);
        }

        /// <summary>
        /// Handles the units system changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void HandleUnitsSystemChanged(object sender, PropertyChangedEventArgs e)
        {
            this.previousMeasurementUnits.Clear();
            this.previousMeasurementUnits.AddRange(this.measurementUnits);
            this.currentUnitsSystem = UserSettingsManager.Instance.UnitsSystem;
            this.RefreshUIUnitsOfCurrentUnitsSystem();
            this.RefreshMeasurementUnitsOfCurrentUnitsSystem();
        }

        /// <summary>
        /// Initializes the units for viewer mode.
        /// </summary>
        private void InitializeUnitsForViewerMode()
        {
            this.allMeasurementUnits = new Collection<MeasurementUnit>();
            this.allMeasurementUnits.Add(new MeasurementUnit() { Name = "Minute", Type = MeasurementUnitType.Time, Symbol = "m", ConversionRate = 60m, ScaleID = MeasurementUnitScale.TimeScale, ScaleFactor = 60m });
            this.allMeasurementUnits.Add(new MeasurementUnit() { Name = "Hour", Type = MeasurementUnitType.Time, Symbol = "h", ConversionRate = 3600m, ScaleID = MeasurementUnitScale.TimeScale, ScaleFactor = 3600m });
            this.allMeasurementUnits.Add(new MeasurementUnit() { Name = "Second", Type = MeasurementUnitType.Time, Symbol = "s", ConversionRate = 1m, ScaleID = MeasurementUnitScale.TimeScale, ScaleFactor = 1m });
            this.allMeasurementUnits.Add(new MeasurementUnit() { Name = "Liter", Type = MeasurementUnitType.Volume2, Symbol = "l", ConversionRate = 1m, ScaleID = MeasurementUnitScale.MetricLiquidsVolumeScale, ScaleFactor = 1m });
            this.allMeasurementUnits.Add(new MeasurementUnit() { Name = "Milliliter", Type = MeasurementUnitType.Volume2, Symbol = "ml", ConversionRate = 0.001m, ScaleID = MeasurementUnitScale.MetricLiquidsVolumeScale, ScaleFactor = 0.001m });
            this.allMeasurementUnits.Add(new MeasurementUnit() { Name = "Pieces", Type = MeasurementUnitType.Pieces, Symbol = "pcs", ConversionRate = 1m, ScaleID = MeasurementUnitScale.PiecesScale, ScaleFactor = 1m });
            this.allMeasurementUnits.Add(new MeasurementUnit() { Name = "Kilogram", Type = MeasurementUnitType.Weight, Symbol = "kg", ConversionRate = 1m, ScaleID = MeasurementUnitScale.MetricWeightScale, ScaleFactor = 1m });
            this.allMeasurementUnits.Add(new MeasurementUnit() { Name = "Gram", Type = MeasurementUnitType.Weight, Symbol = "g", ConversionRate = 0.001m, ScaleID = MeasurementUnitScale.MetricWeightScale, ScaleFactor = 0.001m });
            this.allMeasurementUnits.Add(new MeasurementUnit() { Name = "Pound", Type = MeasurementUnitType.Weight, Symbol = "lb", ConversionRate = 0.45359237m, ScaleID = MeasurementUnitScale.ImperialWeightScale, ScaleFactor = 1m });
            this.allMeasurementUnits.Add(new MeasurementUnit() { Name = "Ounce", Type = MeasurementUnitType.Weight, Symbol = "oz", ConversionRate = 0.028349523125m, ScaleID = MeasurementUnitScale.ImperialWeightScale, ScaleFactor = 0.0625m });
            this.allMeasurementUnits.Add(new MeasurementUnit() { Name = "Meter", Type = MeasurementUnitType.Length, Symbol = "m", ConversionRate = 1m, ScaleID = MeasurementUnitScale.MetricLengthScale, ScaleFactor = 1m });
            this.allMeasurementUnits.Add(new MeasurementUnit() { Name = "Foot", Type = MeasurementUnitType.Length, Symbol = "ft", ConversionRate = 0.3048m, ScaleID = MeasurementUnitScale.ImperialLengthScale, ScaleFactor = 1m });
            this.allMeasurementUnits.Add(new MeasurementUnit() { Name = "Square Meter", Type = MeasurementUnitType.Area, Symbol = "m²", ConversionRate = 1m, ScaleID = MeasurementUnitScale.MetricAreaScale, ScaleFactor = 1m });
            this.allMeasurementUnits.Add(new MeasurementUnit() { Name = "Square Foot", Type = MeasurementUnitType.Area, Symbol = "ft²", ConversionRate = 0.09290304m, ScaleID = MeasurementUnitScale.ImperialAreaScale, ScaleFactor = 1m });
            this.allMeasurementUnits.Add(new MeasurementUnit() { Name = "Cubic Meter", Type = MeasurementUnitType.Volume, Symbol = "m³", ConversionRate = 1m, ScaleID = MeasurementUnitScale.MetricVolumeScale, ScaleFactor = 1m });
            this.allMeasurementUnits.Add(new MeasurementUnit() { Name = "Cubic Foot", Type = MeasurementUnitType.Volume, Symbol = "ft³", ConversionRate = 0.0283168466m, ScaleID = MeasurementUnitScale.ImperialVolumeScale, ScaleFactor = 1m });
        }

        /// <summary>
        /// Refreshes the measurement units of current systemof units.
        /// </summary>
        private void RefreshMeasurementUnitsOfCurrentUnitsSystem()
        {
            var unitsToRemove = new Collection<MeasurementUnit>(this.MeasurementUnits.Where(u => u.Type == MeasurementUnitType.Area
                                                                                            || u.Type == MeasurementUnitType.Length
                                                                                            || u.Type == MeasurementUnitType.Volume
                                                                                            || u.Type == MeasurementUnitType.Weight).ToList());

            foreach (var unit in unitsToRemove)
            {
                this.MeasurementUnits.Remove(unit);
            }

            if (this.currentUnitsSystem == UnitsSystem.Metric)
            {
                this.MeasurementUnits.AddRange(this.allMeasurementUnits.Where(u => u.ScaleID == MeasurementUnitScale.MetricAreaScale
                                                                                || u.ScaleID == MeasurementUnitScale.MetricLengthScale
                                                                                || u.ScaleID == MeasurementUnitScale.MetricVolumeScale
                                                                                || u.ScaleID == MeasurementUnitScale.MetricWeightScale));
            }
            else if (this.currentUnitsSystem == UnitsSystem.Imperial)
            {
                this.MeasurementUnits.AddRange(this.allMeasurementUnits.Where(u => u.ScaleID == MeasurementUnitScale.ImperialAreaScale
                                                                                || u.ScaleID == MeasurementUnitScale.ImperialLengthScale
                                                                                || u.ScaleID == MeasurementUnitScale.ImperialVolumeScale
                                                                                || u.ScaleID == MeasurementUnitScale.ImperialWeightScale));
            }

            this.MeasurementUnits = new Collection<MeasurementUnit>(this.MeasurementUnits);
        }

        /// <summary>
        /// Refreshes the UI units of current system of units.
        /// </summary>
        private void RefreshUIUnitsOfCurrentUnitsSystem()
        {
            MeasurementUnit area = null;
            MeasurementUnit length = null;
            MeasurementUnit volume = null;
            MeasurementUnit weight = null;

            if (this.currentUnitsSystem == UnitsSystem.Metric)
            {
                area = this.allMeasurementUnits.FirstOrDefault(u => u.ScaleID == MeasurementUnitScale.MetricAreaScale);
                length = this.allMeasurementUnits.FirstOrDefault(u => u.ScaleID == MeasurementUnitScale.MetricLengthScale);
                volume = this.allMeasurementUnits.FirstOrDefault(u => u.ScaleID == MeasurementUnitScale.MetricVolumeScale);
                weight = this.allMeasurementUnits.FirstOrDefault(u => u.ScaleID == MeasurementUnitScale.MetricWeightScale);
            }
            else if (this.currentUnitsSystem == UnitsSystem.Imperial)
            {
                area = this.allMeasurementUnits.FirstOrDefault(u => u.ScaleID == MeasurementUnitScale.ImperialAreaScale);
                length = this.allMeasurementUnits.FirstOrDefault(u => u.ScaleID == MeasurementUnitScale.ImperialLengthScale);
                volume = this.allMeasurementUnits.FirstOrDefault(u => u.ScaleID == MeasurementUnitScale.ImperialVolumeScale);
                weight = this.allMeasurementUnits.FirstOrDefault(u => u.ScaleID == MeasurementUnitScale.ImperialWeightScale);
            }

            this.UIArea = new Unit(area);
            this.UILength = new Unit(length);
            this.UIVolume = new Unit(volume);
            this.UIWeight = new Unit(weight);
        }

        /// <summary>
        /// Refreshes the UI currency unit.
        /// </summary>
        /// <param name="currencyCode">The currency code.</param>
        private void RefreshUICurrencyUnit(string currencyCode)
        {
            if (!string.IsNullOrWhiteSpace(currencyCode)
                && this.Currencies != null)
            {
                var newUICurrency = this.Currencies.FirstOrDefault(c => c.IsoCode == currencyCode);
                if (newUICurrency != null)
                {
                    this.UICurrency = new Unit(newUICurrency);
                }
            }
        }

        /// <summary>
        /// Gets the unit conversion factors.
        /// </summary>
        /// <returns>The units conversion factors.</returns>
        public UnitConversionFactors GetUnitConversionFactors()
        {
            var conversionFactors = new UnitConversionFactors();
            conversionFactors.AreaFactor = this.MeasurementUnits.FirstOrDefault(u => u.Type == MeasurementUnitType.Area && u.IsBaseUnit).ConversionRate;
            conversionFactors.LengthFactor = this.MeasurementUnits.FirstOrDefault(u => u.Type == MeasurementUnitType.Length && u.IsBaseUnit).ConversionRate;
            conversionFactors.VolumeFactor = this.MeasurementUnits.FirstOrDefault(u => u.Type == MeasurementUnitType.Volume && u.IsBaseUnit).ConversionRate;
            conversionFactors.WeightFactor = this.MeasurementUnits.FirstOrDefault(u => u.Type == MeasurementUnitType.Weight && u.IsBaseUnit).ConversionRate;
            conversionFactors.CurrencyFactor = this.UICurrency.ConversionFactor / this.BaseCurrency.ExchangeRate;

            return conversionFactors;
        }

        /// <summary>
        /// Gets the current unit of the specified unit type.
        /// </summary>
        /// <param name="unitType">Type of the unit.</param>
        /// <returns>A unit</returns>
        public Unit GetCurrentUnit(UnitType unitType)
        {
            Unit unit = null;
            switch (unitType)
            {
                case UnitType.Currency:
                    unit = this.UICurrency;
                    break;
                case UnitType.Length:
                    unit = this.UILength;
                    break;
                case UnitType.Weight:
                    unit = this.UIWeight;
                    break;
                case UnitType.Volume:
                    unit = this.UIVolume;
                    break;
                case UnitType.Area:
                    unit = this.UIArea;
                    break;
                case UnitType.Percentage:
                    unit = this.UIPercentage;
                    break;
                case UnitType.Time:
                    unit = this.UITime;
                    break;
                case UnitType.VolumeLiquids:
                    unit = this.UIVolumeLiquids;
                    break;
                case UnitType.Pieces:
                    unit = this.UIPercentage;
                    break;
            }

            return unit;
        }

        /// <summary>
        /// Gets the base measurement unit of the specified unit type.
        /// </summary>
        /// <param name="unitType">Type of the unit.</param>
        /// <returns>A measurement unit.</returns>
        public MeasurementUnit GetBaseMeasurementUnit(MeasurementUnitType unitType)
        {
            return this.MeasurementUnits.FirstOrDefault(u => u.Type == unitType && u.IsBaseUnit);
        }

        /// <summary>
        /// Gets the base measurement unit from the previous units system (currenct metric -> imperial, current imperial -> metric).
        /// </summary>
        /// <param name="unitType">Type of the unit.</param>
        /// <returns>A measurement unit.</returns>
        public MeasurementUnit GetPreviousBaseMeasurementUnit(MeasurementUnitType unitType)
        {
            return this.previousMeasurementUnits.FirstOrDefault(u => u.Type == unitType && u.IsBaseUnit);
        }

        /// <summary>
        /// Gets the measurement units of the specified unit type.
        /// </summary>
        /// <param name="unitType">Type of the unit.</param>
        /// <returns>A list of measurement units.</returns>
        public Collection<MeasurementUnit> GetMeasurementUnits(MeasurementUnitType unitType)
        {
            return new Collection<MeasurementUnit>(this.MeasurementUnits.Where(u => u.Type == unitType).ToList());
        }
    }
}