﻿using System;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.ServiceProcess;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Threading;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.ViewModels;
using ZPKTool.LicenseValidator;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui
{
    /// <summary>
    /// The application's Entry-Point class.
    /// </summary>
    public partial class App : System.Windows.Application
    {
        /// <summary>
        /// Used for identifying if the Win32 window message sent to the running viewer process to signal that it has to open a new model in the Viewer.
        /// This message is sent by a 2nd viewer process, before shutting down (because only one viewer process can run at a time).
        /// </summary>
        private const int LoadModelMessageId = 1735;

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The UI composition framework bootstrapped.
        /// </summary>
        private static Bootstrapper bootstrapper;

        /// <summary>
        /// Gets the composition container from the UI composition framework.        
        /// </summary>
        private static CompositionContainer CompositionContainer
        {
            get
            {
                if (bootstrapper != null)
                {
                    return bootstrapper.CompositionContainer;
                }

                return null;
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Application.Startup"/> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.StartupEventArgs"/> that contains the event data.</param>
        protected override void OnStartup(StartupEventArgs e)
        {
            log.Info(
                "--- Starting PCM v{0}, OS: {1} ---",
                System.Reflection.Assembly.GetExecutingAssembly().GetName().Version,
                this.GetOSVersion());

            // Loads the application settings, initialize the UI language and only then show the splash screen.
            // This order is dictated by dependencies between the 3 operations.
            UserSettingsManager.Instance.Load();
            ShellController.SetUILanguage(UserSettingsManager.Instance.UILanguage);

            // Initialize the global settings.
            GlobalSettingsManager.Instance.Load();
            Images.InitializeImageResources();

            var modelBrowserStartInfo = new ModelViewerStartupInfo();
            modelBrowserStartInfo.StartInViewerMode = GlobalSettingsManager.Instance.StartInViewerMode;

            var currentProcess = System.Diagnostics.Process.GetCurrentProcess();
            var runningProcesses = System.Diagnostics.Process.GetProcessesByName(currentProcess.ProcessName);
            if (!GlobalSettingsManager.Instance.StartInViewerMode || runningProcesses.Length < 2)
            {
                this.LoadSplashScreen();
            }

            if (e.Args.Length > 0 && File.Exists(e.Args[0]))
            {
                modelBrowserStartInfo.StartInViewerMode = true;
                modelBrowserStartInfo.ModelFilePath = e.Args[0];
            }

            // The app doesn't start in the Guest mode due to lack of permissions but the Model Viewer works.
            if (!modelBrowserStartInfo.StartInViewerMode)
            {
                var crtUser = System.Security.Principal.WindowsIdentity.GetCurrent();
                if (crtUser != null && crtUser.IsGuest)
                {
                    MessageWindow.Show(LocalizedResources.General_AppCantRunInGuestMode, MessageDialogType.Error);
                    Application.Current.Shutdown();
                }
            }

            // Do not allow more than one instance of the application to run.
            if (runningProcesses.Length > 1)
            {
                var otherProcessWindowhandle = runningProcesses.FirstOrDefault(proc => proc.Id != currentProcess.Id).MainWindowHandle;
                if (modelBrowserStartInfo.StartInViewerMode && e.Args.Length > 0)
                {
                    // Restore & focus the window of the running app instance and notify the Viewer to reload the new model from argument path.
                    WindowExtensions.RestoreWindow(otherProcessWindowhandle);
                    NativeMethods.SetForegroundWindow(otherProcessWindowhandle);
                    WindowExtensions.SendStringMessage(otherProcessWindowhandle, LoadModelMessageId, e.Args[0]);
                }
                else
                {
                    MessageWindow.Show(LocalizedResources.General_AppAlreadyRunning, MessageDialogType.Info);

                    // Focus the window of the running app instance.
                    NativeMethods.SetForegroundWindow(otherProcessWindowhandle);
                }

                // Close this instance.
                Application.Current.Shutdown();
                return;
            }

            // Initialize the MEF container, on which the whole MVVM framework depends.
            bootstrapper = new Bootstrapper();
            bootstrapper.Run();

            this.CleanCacheFolders();

            if (!modelBrowserStartInfo.StartInViewerMode)
            {
                this.ConfigureDbAccess();
                LicenseValidation.Initialize();
                this.CheckIfLocalServerIsRunning();
            }

            // Create a dummy basic settings, required when the application is running in viewer mode.
            var dummyBasicSettings = new BasicSetting();
            ZPKTool.Calculations.CostCalculation.CostCalculatorFactory.SetBasicSettings(dummyBasicSettings);

            bootstrapper.StartServices();

            var shellController = bootstrapper.CompositionContainer.GetExportedValue<ShellController>();
            shellController.Run(modelBrowserStartInfo);

            if (Application.Current != null && Application.Current.MainWindow != null)
            {
                // the following code focuses the main window of the application when the application starts (after the loading screen)
                var window = Application.Current.MainWindow;
                if (window != null)
                {
                    window.BringToForeground();
                }
            }

            if (this.MainWindow != null)
            {
                IntPtr mainWindowHandle = new WindowInteropHelper(this.MainWindow).Handle;
                HwndSource mainWindowSource = HwndSource.FromHwnd(mainWindowHandle);
                mainWindowSource.AddHook(MainWindowMessagesHandler);
            }

            // This call just raises the Startup event so it should be called last, after everything is initialized.
            // If it is moved up in the method you should document the reason with a comment.
            base.OnStartup(e);
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Application.Exit"/> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.Windows.ExitEventArgs"/> that contains the event data.</param>
        protected override void OnExit(ExitEventArgs e)
        {
            log.Info("--- Shutting down PCM ---");

            base.OnExit(e);
        }

        /// <summary>
        /// Checks if the local server is running and tries to start it if it's not.
        /// </summary>
        private void CheckIfLocalServerIsRunning()
        {
            try
            {
                string localConnectionDataSource = GlobalSettingsManager.Instance.LocalConnectionDataSource;
                string serverInstanceName = localConnectionDataSource.Split('\\').LastOrDefault();
                string machineName = Environment.MachineName;

                string displayNameTemplate = "SQL Server ({0})";
                string displayName = string.Format(displayNameTemplate, serverInstanceName.ToUpper());

                ServiceController[] services = ServiceController.GetServices(machineName);
                var service = services.FirstOrDefault(s => s.DisplayName == displayName);
                if (service != null
                    && (service.Status == ServiceControllerStatus.Paused || service.Status == ServiceControllerStatus.Stopped))
                {
                    service.Start();
                    service.WaitForStatus(ServiceControllerStatus.Running, new TimeSpan(0, 0, 10));
                }
            }
            catch (Exception ex)
            {
                log.ErrorException("An error occurred while checking if the local server is running!", ex);
            }
        }

        #region Configure Database Access

        /// <summary>
        /// Configures the database access.
        /// The order of the method calls cannot change: first the connection strings need to be initialized, then the context
        /// </summary>
        private void ConfigureDbAccess()
        {
            DbConnectionInfo localConnParams = new DbConnectionInfo();
            localConnParams.InitialCatalog = GlobalSettingsManager.Instance.LocalConnectionInitialCatalog;
            localConnParams.DataSource = GlobalSettingsManager.Instance.LocalConnectionDataSource;
            localConnParams.UserId = GlobalSettingsManager.Instance.LocalConnectionUser;
            localConnParams.Password = GlobalSettingsManager.Instance.LocalConnectionPasswordEncrypted;
            localConnParams.PasswordEncryptionKey = Constants.PasswordEncryptionKey;
            localConnParams.UseWindowsAuthentication = GlobalSettingsManager.Instance.UseWindowsAuthForLocalDb;

            DbConnectionInfo centralConnParams = new DbConnectionInfo();
            centralConnParams.InitialCatalog = GlobalSettingsManager.Instance.CentralConnectionInitialCatalog;
            centralConnParams.DataSource = GlobalSettingsManager.Instance.CentralConnectionDataSource;
            centralConnParams.UserId = GlobalSettingsManager.Instance.CentralConnectionUser;
            centralConnParams.Password = GlobalSettingsManager.Instance.CentralConnectionPasswordEncrypted;
            centralConnParams.PasswordEncryptionKey = Constants.PasswordEncryptionKey;
            centralConnParams.UseWindowsAuthentication = GlobalSettingsManager.Instance.UseWindowsAuthForCentralDb;
            centralConnParams.UseSSLSupport = GlobalSettingsManager.Instance.UseSSLSupportForCentralDb;

            ZPKTool.DataAccess.ConnectionConfiguration.Initialize(localConnParams, centralConnParams);

            DbConnectionInfo syncLocalConnParams = new DbConnectionInfo();
            syncLocalConnParams.InitialCatalog = GlobalSettingsManager.Instance.LocalConnectionInitialCatalog;
            syncLocalConnParams.DataSource = GlobalSettingsManager.Instance.LocalConnectionDataSource;
            syncLocalConnParams.UserId = GlobalSettingsManager.Instance.LocalSyncConnectionUser;
            syncLocalConnParams.Password = GlobalSettingsManager.Instance.LocalConnectionPasswordEncrypted;
            syncLocalConnParams.PasswordEncryptionKey = Constants.PasswordEncryptionKey;
            syncLocalConnParams.UseWindowsAuthentication = GlobalSettingsManager.Instance.UseWindowsAuthForLocalDbSync;

            DbConnectionInfo syncCentralConnParams = new DbConnectionInfo();
            syncCentralConnParams.InitialCatalog = GlobalSettingsManager.Instance.CentralConnectionInitialCatalog;
            syncCentralConnParams.DataSource = GlobalSettingsManager.Instance.CentralConnectionDataSource;
            syncCentralConnParams.UserId = GlobalSettingsManager.Instance.CentralSyncConnectionUser;
            syncCentralConnParams.Password = GlobalSettingsManager.Instance.CentralConnectionPasswordEncrypted;
            syncCentralConnParams.PasswordEncryptionKey = Constants.PasswordEncryptionKey;
            syncCentralConnParams.UseWindowsAuthentication = GlobalSettingsManager.Instance.UseWindowsAuthForCentralDbSync;
            syncCentralConnParams.UseSSLSupport = GlobalSettingsManager.Instance.UseSSLSupportForCentralDb;

            ZPKTool.Synchronization.Configuration.InitializeDBAccess(syncLocalConnParams, syncCentralConnParams);
        }

        #endregion Initialization

        /// <summary>
        /// Remove the content of the cache folders.
        /// </summary>
        private void CleanCacheFolders()
        {
            try
            {
                if (Directory.Exists(Constants.VideoCacheFolderPath))
                {
                    Directory.Delete(Constants.VideoCacheFolderPath, true);
                }

                if (Directory.Exists(Constants.ViewerModeCacheFolderPath))
                {
                    Directory.Delete(Constants.ViewerModeCacheFolderPath, true);
                }

                Directory.CreateDirectory(Constants.VideoCacheFolderPath);
                Directory.CreateDirectory(Constants.ViewerModeCacheFolderPath);
            }
            catch (Exception ex)
            {
                log.WarnException("Error occurred while clearing the video cache folder.", ex);
            }
        }

        /// <summary>
        /// Loads a splash screen image based on the UILanguage
        /// </summary>
        private void LoadSplashScreen()
        {
            SplashScreen splash = new SplashScreen(LocalizedResources.SplashScreenImage);
            splash.Show(true, true);
        }

        /// <summary>
        /// Handles all Win32 window messages received by the main window.
        /// </summary>
        /// <param name="windowHandle">The window handle.</param>
        /// <param name="messageID">The message ID.</param>
        /// <param name="wparam">The message's wParam value.</param>
        /// <param name="lparam">The message's lParam value.</param>
        /// <param name="handled">A value that indicates whether the message was handled. Set the value to
        /// true if the message was handled; otherwise, false.</param>
        /// <returns>
        /// The appropriate return value depends on the particular message. See the message
        /// documentation details for the Win32 message being handled.
        /// </returns>
        private IntPtr MainWindowMessagesHandler(IntPtr windowHandle, int messageID, IntPtr wparam, IntPtr lparam, ref bool handled)
        {
            if (messageID == NativeMethods.WM_COPYDATA)
            {
                int firstParamVal = 0;

                try
                {
                    firstParamVal = wparam.ToInt32();
                }
                catch (OverflowException)
                {
                    return windowHandle;
                }

                if (firstParamVal == App.LoadModelMessageId)
                {
                    // If the application didn't start in viewer mode do nothing.
                    if (GlobalSettingsManager.Instance != null && !GlobalSettingsManager.Instance.StartInViewerMode)
                    {
                        MessageWindow.Show(LocalizedResources.General_AppAlreadyRunning, MessageDialogType.Info);
                        return windowHandle;
                    }

                    var receivedData = (NativeMethods.COPYDATASTRUCT)Marshal.PtrToStructure(lparam, typeof(NativeMethods.COPYDATASTRUCT));
                    var messenger = CompositionContainer.GetExportedValue<IMessenger>();
                    messenger.Send<NotificationMessage>(new NotificationMessage<string>(Notification.LoadModelInViewer, receivedData.lpData));
                }
            }

            return windowHandle;
        }

        /// <summary>
        /// Handles the DispatcherUnhandledException event of the Application.
        /// This event is triggered when an application exception is not handled.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Threading.DispatcherUnhandledExceptionEventArgs"/> instance containing the event data.</param>
        private void Application_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = true;

            Exception exception = e.Exception;
            this.LogException(exception);
            MessageWindow.Show(exception);

            // TODO: check if is an exception related to file operations

            // if the user account is disabled or deleted, he can't continue using the application so he is logged out. 
            PISecurityException securityException = exception as PISecurityException;
            if (securityException != null &&
                (securityException.ErrorCode == ErrorCodes.UserDeleted
                || securityException.ErrorCode == ErrorCodes.UserDisabled))
            {
                CompositionContainer.GetExportedValue<ShellViewModel>().LogoutCommand.Execute(null);
            }
        }

        /// <summary>
        /// Log the specified exception error.
        /// </summary>
        /// <param name="exception">The exception.</param>
        private void LogException(Exception exception)
        {
            var compositionEx = exception as CompositionException;
            if (compositionEx != null)
            {
                this.LogContentOfCompositionException(compositionEx);
            }

            log.ErrorException("An unhandled exception reached the UI dispatcher.", exception);
            LoggingManager.Log(LogEventType.Error, exception);
        }

        /// <summary>
        /// Log the content (inner exception) of all <see cref="ComposablePartException"/> instances in the specified composition error exception.
        /// </summary>
        /// <param name="compositionException">The composition exception.</param>
        private void LogContentOfCompositionException(CompositionException compositionException)
        {
            // A CompositionException can appear not only because of issues in composing MEF parts (which usually is due to bugs on our side),
            // but also if a part's constructor throws an exception. For example, a view-model is exported as a part and in the constructor makes a db call;
            // if the call fails, usually an exception that identifies the problem is thrown. The CompositionException hides the app specific exceptions so we
            // have to extract them.
            foreach (var compositionError in compositionException.Errors)
            {
                var compositionEx = compositionError.Exception as CompositionException;
                if (compositionEx != null)
                {
                    this.LogContentOfCompositionException(compositionEx);
                }
                else
                {
                    var composablePartEx = compositionError.Exception as ComposablePartException;
                    if (composablePartEx != null)
                    {
                        // The ComposablePartException should contain the actual application exception in the InnerException property.
                        // If it doesn't, the recursive call will fall back to internal error.
                        if (composablePartEx.InnerException != null)
                        {
                            log.ErrorException("The exception was wrapped in ComposablePartException.", composablePartEx.InnerException);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets the user friendly version name of the operating system running the app.
        /// </summary>
        /// <returns>A string containing the OS version name.</returns>
        private string GetOSVersion()
        {
            string versionFormat = "{0}";
            var osversion = System.Environment.OSVersion;
            if (osversion.Version.Major <= 4
                || (osversion.Version.Major == 5 && osversion.Version.Minor == 0))
            {
                versionFormat = "Pre-XP ({0})";
            }
            else if (osversion.Version.Major == 5 && osversion.Version.Minor == 1)
            {
                versionFormat = "Windows XP ({0})";
            }
            else if (osversion.Version.Major == 5 && osversion.Version.Minor == 2)
            {
                versionFormat = "Windows 2003 ({0})";
            }
            else if (osversion.Version.Major == 6 && osversion.Version.Minor == 0)
            {
                versionFormat = "Windows Vista/2008 ({0})";
            }
            else if (osversion.Version.Major == 6 && osversion.Version.Minor == 1)
            {
                versionFormat = "Windows 7/2008 R2 ({0})";
            }
            else if (osversion.Version.Major == 6 && osversion.Version.Minor == 2)
            {
                versionFormat = "Windows 8 ({0})";
            }

            /* otherwise show the raw version number */

            return string.Format(System.Globalization.CultureInfo.InvariantCulture, versionFormat, osversion.ToString());
        }
    }
}
