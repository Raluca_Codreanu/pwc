﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Input;
using ZPKTool.Business;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the IMyView view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class MachineViewModel : ViewModel<Machine, IDataSourceManager>
    {
        #region Attributes

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The messenger.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// A value indicating whether the ViewModel consumables cost is enabled or not.
        /// </summary>
        private bool consumablesCostIsEnabled;

        /// <summary>
        /// The model browser helper service.
        /// </summary>
        private IModelBrowserHelperService modelBrowserHelperService;

        /// <summary>
        /// The Machine ClassificationSelectorViewModel.
        /// </summary>
        private ClassificationSelectorViewModel machineClassificationsView;

        /// <summary>
        /// The machine costs calculation parameters.
        /// </summary>
        private MachineCostCalculationParameters machineCalculationParams;

        /// <summary>
        /// The cost calculation version to use when calculating for display the machine's cost.
        /// </summary>
        private string costCalculationVersion;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        /// <summary>
        /// A bool value indicating whether the model is loading or not.
        /// </summary>
        private bool isModelLoading = false;

        /// <summary>
        /// The UnitsAdapter handler, used to perform certain operations when some UnitsAdapter properties are updated.
        /// </summary>
        private UnitsAdapterUpdateHandler unitsAdapterHandler;

        #endregion Attributes

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MachineViewModel"/> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        /// <param name="messenger">The messenger.</param>
        /// <param name="modelBrowserHelperService">The model browser helper service.</param>
        /// <param name="costRecalculationCloneManager">The cost recalculation clone manager.</param>
        /// <param name="manufacturerViewModel">The manufacturer view model.</param>
        /// <param name="masterDataBrowser">The master data browser.</param>
        /// <param name="mediaViewModel">The media view model.</param>
        /// <param name="classificationSelectorVM">The classification selector view model.</param>
        /// <param name="unitsService">The units service.</param>
        [ImportingConstructor]
        public MachineViewModel(
            IWindowService windowService,
            IMessenger messenger,
            IModelBrowserHelperService modelBrowserHelperService,
            ICostRecalculationCloneManager costRecalculationCloneManager,
            ManufacturerViewModel manufacturerViewModel,
            MasterDataBrowserViewModel masterDataBrowser,
            MediaViewModel mediaViewModel,
            ClassificationSelectorViewModel classificationSelectorVM,
            IUnitsService unitsService)
        {
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("manufacturerViewModel", manufacturerViewModel);
            Argument.IsNotNull("masterDataBrowser", masterDataBrowser);
            Argument.IsNotNull("mediaViewModel", mediaViewModel);
            Argument.IsNotNull("classificationSelectorVM", classificationSelectorVM);
            Argument.IsNotNull("unitsService", unitsService);
            Argument.IsNotNull("costRecalculationCloneManager", costRecalculationCloneManager);

            this.windowService = windowService;
            this.messenger = messenger;
            this.modelBrowserHelperService = modelBrowserHelperService;
            this.ManufacturerViewModel = manufacturerViewModel;
            this.MasterDataBrowser = masterDataBrowser;
            this.MediaViewModel = mediaViewModel;
            this.MediaViewModel.Mode = MediaControlMode.SingleImage;
            this.MediaViewModel.IsChild = true;
            this.MachineClassificationsView = classificationSelectorVM;
            this.MachineClassificationsView.ClassificationType = typeof(MachinesClassification);
            this.unitsService = unitsService;
            this.CloneManager = costRecalculationCloneManager;

            // Initialize the manufacturer view-model
            this.ManufacturerViewModel.ShowSaveControls = false;
            this.ManufacturerViewModel.IsChild = true;

            // Initialize the commands
            this.SaveNestedViewModels = new CompositeCommand();
            this.SaveNestedViewModels.RegisterCommand(this.ManufacturerViewModel.SaveToModelCommand);
            this.SaveNestedViewModels.RegisterCommand(this.MediaViewModel.SaveToModelCommand);
            this.CancelNestedViewModels = new CompositeCommand();
            this.CancelNestedViewModels.RegisterCommand(this.ManufacturerViewModel.CancelCommand);
            this.CancelNestedViewModels.RegisterCommand(this.MediaViewModel.CancelCommand);
            this.CancelNestedViewModels.RegisterCommand(this.MachineClassificationsView.CancelCommand);
            this.BrowseMasterDataCommand = new DelegateCommand(BrowseMasterData);

            this.InitializeProperties();
            this.InitializeUndoManager();
        }

        #endregion Constructor

        #region Commands

        /// <summary>
        /// Gets the browse master data command.
        /// </summary>
        public ICommand BrowseMasterDataCommand { get; private set; }

        /// <summary>
        /// Gets or sets the command that save all changes in the nested view models back into their models.
        /// This command aggregates the SaveToModel commands of the nested view models.
        /// </summary>
        private CompositeCommand SaveNestedViewModels { get; set; }

        /// <summary>
        /// Gets or sets the command that cancels all changes of the nested view models.
        /// This command aggregates the Cancel commands of the nested view models.
        /// </summary>
        private CompositeCommand CancelNestedViewModels { get; set; }

        #endregion Commands

        #region Model related properties

        /// <summary>
        /// Gets the name of the machine.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Name", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Name")]
        public DataProperty<string> Name { get; private set; }

        /// <summary>
        /// Gets the machine manufacturing year.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("ManufacturingYear")]
        public DataProperty<short?> ManufacturingYear { get; private set; }

        /// <summary>
        /// Gets the machine depreciation period.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("DepreciationPeriod", AffectsCost = true)]
        public DataProperty<int?> DepreciationPeriod { get; private set; }

        /// <summary>
        /// Gets the machine depreciation rate.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("DepreciationRate", AffectsCost = true)]
        public DataProperty<decimal?> DepreciationRate { get; private set; }

        /// <summary>
        /// Gets the machine registration number.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("RegistrationNumber")]
        public DataProperty<string> RegistrationNumber { get; private set; }

        /// <summary>
        /// Gets the machine description of function.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("DescriptionOfFunction")]
        public DataProperty<string> DescriptionOfFunction { get; private set; }

        /// <summary>
        /// Gets the machine floor size.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("FloorSize", AffectsCost = true)]
        public DataProperty<decimal?> FloorSize { get; private set; }

        /// <summary>
        /// Gets the machine workspace area.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("WorkspaceArea", AffectsCost = true)]
        public DataProperty<decimal?> WorkspaceArea { get; private set; }

        /// <summary>
        /// Gets the machine power consumption.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("PowerConsumption", AffectsCost = true)]
        public DataProperty<decimal?> PowerConsumption { get; private set; }

        /// <summary>
        /// Gets the fossil energy consumption ratio.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("FossilEnergyConsumptionRatio", AffectsCost = true)]
        public DataProperty<decimal?> FossilEnergyConsumptionRatio { get; private set; }

        /// <summary>
        /// Gets the machine air consumption.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("AirConsumption", AffectsCost = true)]
        public DataProperty<decimal?> AirConsumption { get; private set; }

        /// <summary>
        /// Gets the machine water consumption.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("WaterConsumption", AffectsCost = true)]
        public DataProperty<decimal?> WaterConsumption { get; private set; }

        /// <summary>
        /// Gets the machine full load rate.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("FullLoadRate", AffectsCost = true)]
        public DataProperty<decimal?> FullLoadRate { get; private set; }

        /// <summary>
        /// Gets the machine max capacity.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("MaxCapacity", AffectsCost = true)]
        public DataProperty<int?> MaxCapacity { get; private set; }

        /// <summary>
        /// Gets the machine OEE.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("OEE", AffectsCost = true)]
        public DataProperty<decimal?> OEE { get; private set; }

        /// <summary>
        /// Gets the machine availability.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Availability", AffectsCost = true)]
        public DataProperty<decimal?> Availability { get; private set; }

        /// <summary>
        /// Gets the machine investment.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("MachineInvestment", AffectsCost = true)]
        public DataProperty<decimal?> MachineInvestment { get; private set; }

        /// <summary>
        /// Gets the lease costs for the machine.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("MachineLeaseCosts", AffectsCost = true)]
        public DataProperty<decimal?> MachineLeaseCosts { get; private set; }

        /// <summary>
        /// Gets the machine setup investment.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("SetupInvestment", AffectsCost = true)]
        public DataProperty<decimal?> SetupInvestment { get; private set; }

        /// <summary>
        /// Gets the machine additional equipment investment.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("AdditionalEquipmentInvestment", AffectsCost = true)]
        public DataProperty<decimal?> AdditionalEquipmentInvestment { get; private set; }

        /// <summary>
        /// Gets the machine fundamental setup investment.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("FundamentalSetupInvestment", AffectsCost = true)]
        public DataProperty<decimal?> FundamentalSetupInvestment { get; private set; }

        /// <summary>
        /// Gets the machine investment remarks.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("InvestmentRemarks")]
        public DataProperty<string> InvestmentRemarks { get; private set; }

        /// <summary>
        /// Gets the machine external work cost.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("ExternalWorkCost", AffectsCost = true)]
        public DataProperty<decimal?> ExternalWorkCost { get; private set; }

        /// <summary>
        /// Gets the machine material cost.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("MaterialCost", AffectsCost = true)]
        public DataProperty<decimal?> MaterialCost { get; private set; }

        /// <summary>
        /// Gets the machine consumables cost.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("ConsumablesCost", AffectsCost = true)]
        public DataProperty<decimal?> ConsumablesCost { get; private set; }

        /// <summary>
        /// Gets the machine ManualConsumableCostCalc.
        /// </summary>
        [ExposesModelProperty("ManualConsumableCostCalc", AffectsCost = true)]
        [UndoableProperty]
        public DataProperty<bool> ManualConsumableCostCalc { get; private set; }

        /// <summary>
        /// Gets the machine manual consumable cost percentage.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("ManualConsumableCostPercentage", AffectsCost = true)]
        public DataProperty<decimal?> ManualConsumableCostPercentage { get; private set; }

        /// <summary>
        /// Gets the machine repairs cost.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("RepairsCost", AffectsCost = true)]
        public DataProperty<decimal?> RepairsCost { get; private set; }

        /// <summary>
        /// Gets the machine KValue.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("KValue", AffectsCost = true)]
        public DataProperty<decimal?> KValue { get; private set; }

        /// <summary>
        /// Gets the machine CalculateWithKValue.
        /// </summary>
        [ExposesModelProperty("CalculateWithKValue", AffectsCost = true)]
        [UndoableProperty]
        public DataProperty<bool?> CalculateWithKValue { get; private set; }

        /// <summary>
        /// Gets the machine mounting cube length.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("MountingCubeLength")]
        public DataProperty<string> MountingCubeLength { get; private set; }

        /// <summary>
        /// Gets the machine mounting cube width.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("MountingCubeWidth")]
        public DataProperty<string> MountingCubeWidth { get; private set; }

        /// <summary>
        /// Gets the machine mounting cube height.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("MountingCubeHeight")]
        public DataProperty<string> MountingCubeHeight { get; private set; }

        /// <summary>
        /// Gets the machine max diameter RodPerChuck max thickness.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("MaxDiameterRodPerChuckMaxThickness")]
        public DataProperty<string> MaxDiameterRodPerChuckMaxThickness { get; private set; }

        /// <summary>
        /// Gets the machine max parts weight per capacity.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("MaxPartsWeightPerCapacity")]
        public DataProperty<string> MaxPartsWeightPerCapacity { get; private set; }

        /// <summary>
        /// Gets the machine locking force.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("LockingForce")]
        public DataProperty<string> LockingForce { get; private set; }

        /// <summary>
        /// Gets the machine press capacity.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("PressCapacity")]
        public DataProperty<string> PressCapacity { get; private set; }

        /// <summary>
        /// Gets the machine maximum speed.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("MaximumSpeed")]
        public DataProperty<string> MaximumSpeed { get; private set; }

        /// <summary>
        /// Gets the machine rapid feed per cutting speed.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("RapidFeedPerCuttingSpeed")]
        public DataProperty<string> RapidFeedPerCuttingSpeed { get; private set; }

        /// <summary>
        /// Gets the machine stroke rate.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("StrokeRate")]
        public DataProperty<string> StrokeRate { get; private set; }

        /// <summary>
        /// Gets the machine other data.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Other")]
        public DataProperty<string> Other { get; private set; }

        /// <summary>
        /// Gets the machine MaintenanceNotes.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("MaintenanceNotes")]
        public DataProperty<string> MaintenanceNotes { get; private set; }

        #endregion Model related properties

        #region Other Properties

        /// <summary>
        /// Gets the fossil energy consumption ratio.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public VMProperty<decimal?> RenewableEnergyConsumptionRatio { get; private set; }

        /// <summary>
        /// Gets the depreciation per hour.      
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public VMProperty<decimal> DepreciationPerHour { get; private set; }

        /// <summary>
        /// Gets the maintenance per hour.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public VMProperty<decimal> MaintenancePerHour { get; private set; }

        /// <summary>
        /// Gets the energy cost per hour.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public VMProperty<decimal> EnergyCostPerHour { get; private set; }

        /// <summary>
        /// Gets the floor cost.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public VMProperty<decimal> FloorCost { get; private set; }

        /// <summary>
        /// Gets the full cost per hour.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public VMProperty<decimal> FullCostPerHour { get; private set; }

        /// <summary>
        /// Gets the manufacturer view model.
        /// </summary>
        public ManufacturerViewModel ManufacturerViewModel { get; private set; }

        /// <summary>
        /// Gets the master data browser.
        /// </summary>
        public MasterDataBrowserViewModel MasterDataBrowser { get; private set; }

        /// <summary>
        /// Gets the media view model.
        /// </summary>
        public MediaViewModel MediaViewModel { get; private set; }

        /// <summary>
        /// Gets or sets the RawMaterial Classifications view-model.
        /// </summary>
        public ClassificationSelectorViewModel MachineClassificationsView
        {
            get { return this.machineClassificationsView; }
            set { this.SetProperty(ref this.machineClassificationsView, value, () => this.MachineClassificationsView); }
        }

        /// <summary>
        /// Gets or sets the machine costs calculation parameters.
        /// </summary>
        public MachineCostCalculationParameters MachineCalculationParams
        {
            get { return this.machineCalculationParams; }
            set { this.SetProperty(ref this.machineCalculationParams, value, () => this.MachineCalculationParams); }
        }

        /// <summary>
        /// Gets or sets the cost calculation version to use when calculating for display the machine's cost.
        /// </summary>
        public string CostCalculationVersion
        {
            get { return this.costCalculationVersion; }
            set { this.SetProperty(ref this.costCalculationVersion, value, () => this.CostCalculationVersion); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the ViewModel consumables cost is enabled or not.
        /// </summary>
        public bool ConsumablesCostIsEnabled
        {
            get { return this.consumablesCostIsEnabled; }
            set { this.SetProperty(ref this.consumablesCostIsEnabled, value, () => this.ConsumablesCostIsEnabled); }
        }

        /// <summary>
        /// Gets a value indicating whether the machine lease costs are enabled (otherwise the machine investment is enabled).
        /// </summary>
        [UndoableProperty]
        public VMProperty<bool> IsMachineLeaseCostsEnabled { get; private set; }

        /// <summary>
        /// Gets or sets the machine parent.
        /// </summary>
        public object MachineParent { get; set; }

        /// <summary>
        /// Gets or sets the project to which the Model belongs.
        /// This property is needed during cost calculations so it must be set when editing (it can be omitted when creating).
        /// </summary>
        public Project ParentProject { get; set; }

        /// <summary>
        /// Gets or sets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter { get; set; }

        /// <summary>
        /// Gets or sets the message token to be used when sending messages with token 
        /// </summary>
        public string MessageToken { get; set; }

        #endregion Other Properties

        #region Initialization

        /// <summary>
        /// Initializes the properties of this instance.
        /// </summary>
        private void InitializeProperties()
        {
            this.FossilEnergyConsumptionRatio.ValueChanged += (s, e) => this.OnFossilEnergyConsumptionRatioChanged();
            this.RenewableEnergyConsumptionRatio.ValueChanged += (s, e) => this.OnRenewableEnergyConsumptionRatioChanged();
            this.CalculateWithKValue.ValueChanged += (s, e) => this.OnCalculateWithKValueChanged();
            this.ManualConsumableCostCalc.ValueChanged += (s, e) => this.OnManualCosumableCostCalcChanged();

            this.DepreciationPeriod.ValueChanged += (s, e) => this.OnFieldInvolvedInCostCalculationChanged();
            this.DepreciationRate.ValueChanged += (s, e) => this.OnFieldInvolvedInCostCalculationChanged();
            this.FloorSize.ValueChanged += (s, e) => this.OnFieldInvolvedInCostCalculationChanged();
            this.WorkspaceArea.ValueChanged += (s, e) => this.OnFieldInvolvedInCostCalculationChanged();
            this.PowerConsumption.ValueChanged += (s, e) => this.OnFieldInvolvedInCostCalculationChanged();
            this.AirConsumption.ValueChanged += (s, e) => this.OnFieldInvolvedInCostCalculationChanged();
            this.WaterConsumption.ValueChanged += (s, e) => this.OnFieldInvolvedInCostCalculationChanged();
            this.FullLoadRate.ValueChanged += (s, e) => this.OnFieldInvolvedInCostCalculationChanged();
            this.Availability.ValueChanged += (s, e) => this.OnFieldInvolvedInCostCalculationChanged();
            this.MachineInvestment.ValueChanged += (s, e) => this.OnFieldInvolvedInCostCalculationChanged();
            this.MachineLeaseCosts.ValueChanged += (s, e) => this.OnFieldInvolvedInCostCalculationChanged();
            this.SetupInvestment.ValueChanged += (s, e) => this.OnFieldInvolvedInCostCalculationChanged();
            this.AdditionalEquipmentInvestment.ValueChanged += (s, e) => this.OnFieldInvolvedInCostCalculationChanged();
            this.FundamentalSetupInvestment.ValueChanged += (s, e) => this.OnFieldInvolvedInCostCalculationChanged();
            this.ExternalWorkCost.ValueChanged += (s, e) => this.OnFieldInvolvedInCostCalculationChanged();
            this.MaterialCost.ValueChanged += (s, e) => this.OnFieldInvolvedInCostCalculationChanged();
            this.ConsumablesCost.ValueChanged += (s, e) => this.OnFieldInvolvedInCostCalculationChanged();
            this.RepairsCost.ValueChanged += (s, e) => this.OnFieldInvolvedInCostCalculationChanged();
            this.KValue.ValueChanged += (s, e) => this.OnFieldInvolvedInCostCalculationChanged();
            this.ManualConsumableCostPercentage.ValueChanged += (s, e) => this.OnFieldInvolvedInCostCalculationChanged();
            this.CalculateWithKValue.ValueChanged += (s, e) => this.OnFieldInvolvedInCostCalculationChanged();
            this.ManualConsumableCostCalc.ValueChanged += (s, e) => this.OnFieldInvolvedInCostCalculationChanged();

            this.IsMachineLeaseCostsEnabled.ValueChanged += (s, e) =>
                {
                    // When the lease costs for the machine is enabled disable the machine investment input and vice versa.
                    this.StopRecalculationNotifications();
                    using (this.UndoManager.StartBatch(includePreviousItem: true, navigateToBatchControls: true))
                    {
                        if (this.IsMachineLeaseCostsEnabled.Value)
                        {
                            this.MachineInvestment.Value = null;
                        }
                        else
                        {
                            this.MachineLeaseCosts.Value = null;
                        }
                    }

                    this.ResumeRecalculationNotifications(true);
                };
        }

        /// <summary>
        /// Initializes the view-model for the creation of a new machine.
        /// The ModelDataContext property must be set to a valid value before this call.
        /// </summary>
        /// <param name="isMasterData">If set to true the machine will be created in master data; in this case the parent should be set to null.</param>
        /// <param name="parent">
        /// The parent object of the machine that will be created. If this is set, the value of the <paramref name="isMasterData"/> argument is ignored and
        /// the parent's IsMasterData flag is used instead.
        /// </param>
        /// <remarks>
        /// This is a helper method for populating the model with default data for creation. It is not mandatory to use it; you can set the model to a new
        /// instance set up however you want.
        /// </remarks>
        /// <exception cref="ArgumentException">
        /// <paramref name="parent"/> is null and <paramref name="isMasterData"/> is false, or the type of<paramref name="parent"/> is not supported.
        /// </exception>
        /// <exception cref="InvalidOperationException">The provided <paramref name="parent"/> object type is not supported.</exception>
        public void InitializeForCreation(bool isMasterData, object parent)
        {
            this.CheckDataSource();
            this.EditMode = ViewModelEditMode.Create;

            Machine newMachine = new Machine();

            if (!isMasterData)
            {
                if (parent == null)
                {
                    throw new ArgumentException("A non master data machine cannot be created without a parent", "parent");
                }

                var stepParent = parent as ProcessStep;
                if (stepParent != null)
                {
                    newMachine.ProcessStep = stepParent;
                    isMasterData = stepParent.IsMasterData;
                }
            }

            newMachine.MachineInvestment = 0m;
            newMachine.SetupInvestment = 0m;
            newMachine.AdditionalEquipmentInvestment = 0m;
            newMachine.FundamentalSetupInvestment = 0m;

            newMachine.ExternalWorkCost = 0m;
            newMachine.MaterialCost = 0m;
            newMachine.ConsumablesCost = 0m;
            newMachine.ManualConsumableCostCalc = false;
            newMachine.RepairsCost = 0m;
            newMachine.CalculateWithKValue = true;
            newMachine.ManufacturingYear = (short)DateTime.Now.Year;

            newMachine.DepreciationPeriod = this.MachineCalculationParams != null ? this.MachineCalculationParams.ProjectDepreciationPeriod : 0;
            newMachine.DepreciationRate = this.MachineCalculationParams != null ? this.MachineCalculationParams.ProjectDepreciationRate : 0;

            newMachine.Manufacturer = new Manufacturer();

            // Set the master data flag and the owner at the end so they are applied to all sub-objects.
            newMachine.SetIsMasterData(isMasterData);
            if (!newMachine.IsMasterData)
            {
                User owner = this.DataSourceManager.UserRepository.GetById(SecurityManager.Instance.CurrentUser.Guid, false);
                newMachine.SetOwner(owner);
            }

            this.Model = newMachine;
        }

        /// <summary>
        /// Initialize the undo manager.
        /// </summary>
        private void InitializeUndoManager()
        {
            this.ManufacturerViewModel.UndoManager = this.UndoManager;
            this.MediaViewModel.UndoManager = this.UndoManager;
            this.MachineClassificationsView.UndoManager = this.UndoManager;
        }

        #endregion Initialization

        #region Model handling

        /// <summary>
        /// Loads the data from the model.
        /// Each view-model property decorated with the ExposedModelProperty attribute is loaded from the associated Model property.
        /// </summary>
        /// <param name="model">The model instance.</param>
        public override void LoadDataFromModel(Machine model)
        {
            try
            {
                this.isModelLoading = true;
                base.LoadDataFromModel(model);
                this.LoadMachineClassifications(model);
                this.MachineClassificationsView.IsChanged = false;

                if (CostCalculatorFactory.IsOlder(this.CostCalculationVersion, "1.4"))
                {
                    // The lease cost must not be handled if the calculation variant is older than 1.4 (a machine with lease cost can be created in 1.4 and imported in 1.3).
                    this.IsMachineLeaseCostsEnabled.Value = false;
                    this.MachineLeaseCosts.Value = null;
                }
                else
                {
                    this.IsMachineLeaseCostsEnabled.Value = this.Model.MachineLeaseCosts.HasValue;
                }

                this.CalculateMachineCosts();
            }
            finally
            {
                this.isModelLoading = false;
            }
        }

        /// <summary>
        /// Called when the Model has changed.
        /// </summary>
        protected override void OnModelChanged()
        {
            this.CheckDataSource();
            base.OnModelChanged();

            if (this.IsInViewerMode)
            {
                this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(null);
            }

            this.RenewableEnergyConsumptionRatio.Value = this.Model.FossilEnergyConsumptionRatio.HasValue ? 1m - this.Model.FossilEnergyConsumptionRatio : 0;

            this.ManufacturerViewModel.DataSourceManager = this.DataSourceManager;
            if (this.Model.Manufacturer != null)
            {
                this.ManufacturerViewModel.Model = this.Model.Manufacturer;
            }
            else
            {
                this.ManufacturerViewModel.Model = new Manufacturer();
            }

            this.MediaViewModel.DataSourceManager = this.DataSourceManager;
            this.MediaViewModel.Model = this.Model;

            // Mark the view-model as unchanged because the custom logic above has unintentionally modified its changed state.
            this.IsChanged = false;

            this.UndoManager.Start();

            if (!this.IsChild)
            {
                this.CloneManager.Clone(this);
            }
        }

        /// <summary>
        /// Called when DataSourceManager has changed.
        /// </summary>
        protected override void OnDataSourceManagerChanged()
        {
            base.OnDataSourceManagerChanged();

            this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(this.DataSourceManager);
            this.unitsAdapterHandler = new UnitsAdapterUpdateHandler(this.MeasurementUnitsAdapter);
            this.unitsAdapterHandler.PauseUndoOnUnitsAdapterUpdate(this.UndoManager);
            this.MachineClassificationsView.DataAccessContext = this.DataSourceManager;
        }

        #endregion Model handling

        #region Property change handlers

        /// <summary>
        /// Called when the PartWeight property has changed.
        /// </summary>
        private void OnFossilEnergyConsumptionRatioChanged()
        {
            if (!this.isModelLoading)
            {
                using (this.UndoManager.Pause())
                {
                    if (this.FossilEnergyConsumptionRatio != null)
                    {
                        this.RenewableEnergyConsumptionRatio.Value = 1m - this.FossilEnergyConsumptionRatio.Value;
                    }
                }
            }
        }

        /// <summary>
        /// Called when the PartWeight property has changed.
        /// </summary>
        private void OnRenewableEnergyConsumptionRatioChanged()
        {
            if (!this.isModelLoading)
            {
                using (this.UndoManager.Pause())
                {
                    if (this.RenewableEnergyConsumptionRatio != null)
                    {
                        this.FossilEnergyConsumptionRatio.Value = 1m - this.RenewableEnergyConsumptionRatio.Value;
                    }
                }
            }
        }

        /// <summary>
        /// Called when the CalculateWithKValue property has changed.
        /// </summary>
        private void OnCalculateWithKValueChanged()
        {
            this.SetConsumablesCostIsEnabled();
        }

        /// <summary>
        /// Called when the ManualCosumableCostCalc property has changed.
        /// </summary>
        private void OnManualCosumableCostCalcChanged()
        {
            this.SetConsumablesCostIsEnabled();
        }

        /// <summary>
        /// Called when a property involved in cost calculation has changed.
        /// </summary>
        private void OnFieldInvolvedInCostCalculationChanged()
        {
            if (!this.isModelLoading)
            {
                this.CalculateMachineCosts();
            }
        }

        #endregion Property change handlers

        #region Other private methods

        /// <summary>
        /// Load Machine classifications.
        /// </summary>
        /// <param name="machine">Machine model instance.</param>
        private void LoadMachineClassifications(Machine machine)
        {
            // Clear all classifications because new ones will be loaded
            while (this.MachineClassificationsView.ClassificationLevels.Count > 0)
            {
                this.MachineClassificationsView.ClassificationLevels.RemoveAt(0);
            }

            if (machine.MainClassification != null)
            {
                if (this.IsInViewerMode)
                {
                    this.MachineClassificationsView.ClassificationLevels.Add(machine.MainClassification);
                }
                else
                {
                    var classificationLevel1 = this.DataSourceManager.MachinesClassificationRepository.GetByName(machine.MainClassification.Name);
                    if (classificationLevel1 != null)
                    {
                        this.MachineClassificationsView.ClassificationLevels.Add(classificationLevel1);
                    }
                }
            }

            if (machine.TypeClassification != null)
            {
                if (this.IsInViewerMode)
                {
                    this.MachineClassificationsView.ClassificationLevels.Add(machine.TypeClassification);
                }
                else
                {
                    var classificationLevel2 = this.DataSourceManager.MachinesClassificationRepository.GetByName(machine.TypeClassification.Name);
                    if (classificationLevel2 != null)
                    {
                        this.MachineClassificationsView.ClassificationLevels.Add(classificationLevel2);
                    }
                }
            }

            if (machine.SubClassification != null)
            {
                if (this.IsInViewerMode)
                {
                    this.MachineClassificationsView.ClassificationLevels.Add(machine.SubClassification);
                }
                else
                {
                    var classificationLevel3 = this.DataSourceManager.MachinesClassificationRepository.GetByName(machine.SubClassification.Name);
                    if (classificationLevel3 != null)
                    {
                        this.MachineClassificationsView.ClassificationLevels.Add(classificationLevel3);
                    }
                }
            }

            if (machine.ClassificationLevel4 != null)
            {
                if (this.IsInViewerMode)
                {
                    this.MachineClassificationsView.ClassificationLevels.Add(machine.ClassificationLevel4);
                }
                else
                {
                    var classificationLevel4 = this.DataSourceManager.MachinesClassificationRepository.GetByName(machine.ClassificationLevel4.Name);
                    if (classificationLevel4 != null)
                    {
                        this.MachineClassificationsView.ClassificationLevels.Add(classificationLevel4);
                    }
                }
            }
        }

        /// <summary>
        /// Sets the machine classifications.
        /// </summary>
        /// <param name="machine">The raw material.</param>
        private void SetMachineClassifications(Machine machine)
        {
            if (MachineClassificationsView != null)
            {
                var classifications = MachineClassificationsView.ClassificationLevels;

                if (classifications != null)
                {
                    if (classifications.Count > 0)
                    {
                        MachinesClassification cls = classifications[0] as MachinesClassification;
                        if (machine.MainClassification != cls)
                        {
                            machine.MainClassification = cls;
                        }
                    }
                    else
                    {
                        if (machine.MainClassification != null)
                        {
                            machine.MainClassification = null;
                        }
                    }

                    if (classifications.Count > 1)
                    {
                        MachinesClassification cls = classifications[1] as MachinesClassification;
                        if (machine.TypeClassification != cls)
                        {
                            machine.TypeClassification = cls;
                        }
                    }
                    else
                    {
                        if (machine.TypeClassification != null)
                        {
                            machine.TypeClassification = null;
                        }
                    }

                    if (classifications.Count > 2)
                    {
                        MachinesClassification cls = classifications[2] as MachinesClassification;
                        if (machine.SubClassification != cls)
                        {
                            machine.SubClassification = cls;
                        }
                    }
                    else
                    {
                        if (machine.SubClassification != null)
                        {
                            machine.SubClassification = null;
                        }
                    }

                    if (classifications.Count > 3)
                    {
                        MachinesClassification cls = classifications[3] as MachinesClassification;
                        if (machine.ClassificationLevel4 != cls)
                        {
                            machine.ClassificationLevel4 = cls;
                        }
                    }
                    else
                    {
                        if (machine.ClassificationLevel4 != null)
                        {
                            machine.ClassificationLevel4 = null;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Set ConsumablesCostIsEnabled state, depending on CalculateWithKValue and ManualConsumableCostCalc.
        /// </summary>
        private void SetConsumablesCostIsEnabled()
        {
            this.ConsumablesCostIsEnabled = !((this.CalculateWithKValue.Value ?? false) || this.ManualConsumableCostCalc.Value);
        }

        #endregion Other private methods

        #region Master Data Browse handling

        /// <summary>
        /// Opens the master data browser to select a machine.
        /// </summary>
        private void BrowseMasterData()
        {
            this.MasterDataBrowser.MasterDataType = typeof(Machine);
            this.MasterDataBrowser.MasterDataSelected += this.OnMasterDataMachineSelected;
            this.windowService.ShowViewInDialog(this.MasterDataBrowser);
            this.MasterDataBrowser.MasterDataSelected -= this.OnMasterDataMachineSelected;
        }

        /// <summary>
        /// Called when a master data machine is selected from the master data browser.
        /// </summary>
        /// <param name="masterDataEntity">The master data entity selected in the browser.</param>
        /// <param name="databaseId">The entity context of the selected master data.</param>
        private void OnMasterDataMachineSelected(object masterDataEntity, DbIdentifier databaseId)
        {
            Machine masterMachine = masterDataEntity as Machine;
            if (masterMachine == null)
            {
                return;
            }

            this.StopRecalculationNotifications();
            using (this.UndoManager.StartBatch(undoEachBatch: true))
            {
                // Retrieve the media of the machine master data.
                var dataManager = DataAccessFactory.CreateDataSourceManager(databaseId);
                MediaManager mediaManager = new MediaManager(dataManager);
                var mediaList = new List<Media>();
                var media = mediaManager.GetPictureOrVideo(masterDataEntity);
                if (media != null && (MediaType)media.Type != MediaType.Document)
                {
                    mediaList.Add(media.Copy());
                }

                // Load the machine master data media in Media view-model.
                this.MediaViewModel.LoadMedia(mediaList);

                // Convert the master data entity currency values.
                var currencies = dataManager.CurrencyRepository.GetBaseCurrencies();
                var baseCurrencyFromMD = currencies.FirstOrDefault(c => c.IsSameAs(this.MeasurementUnitsAdapter.BaseCurrency));
                CurrencyConversionManager.ConvertObject(masterMachine, baseCurrencyFromMD, CurrencyConversionManager.DefaultBaseCurrency);

                // Load the data from the master machine and sub-objects in the corresponding view-models. Use empty data for null sub-objects.
                this.LoadDataFromModel(masterMachine);

                // Some machines from master data don't have a fossil energy consumption ratio value, meaning that all the consumption energy is fossil
                if (!masterMachine.FossilEnergyConsumptionRatio.HasValue)
                {
                    this.FossilEnergyConsumptionRatio.Value = 1m;
                }

                this.RenewableEnergyConsumptionRatio.Value = 1m - this.FossilEnergyConsumptionRatio.Value;

                if (masterMachine.Manufacturer != null)
                {
                    this.ManufacturerViewModel.LoadDataFromModel(masterMachine.Manufacturer);
                }
                else
                {
                    this.ManufacturerViewModel.LoadDataFromModel(new Manufacturer());
                }
            }

            this.ResumeRecalculationNotifications(true);
        }

        #endregion Master Data Browse handling

        #region Cost handling

        /// <summary>
        /// Calculates the costs of the machine that is being created or edited.
        /// </summary>
        private void CalculateMachineCosts()
        {
            Machine tmpMachine = new Machine();

            tmpMachine.DepreciationPeriod = this.DepreciationPeriod.Value;
            tmpMachine.DepreciationRate = this.DepreciationRate.Value;
            tmpMachine.MachineInvestment = this.MachineInvestment.Value;
            tmpMachine.MachineLeaseCosts = this.MachineLeaseCosts.Value;
            tmpMachine.SetupInvestment = this.SetupInvestment.Value;
            tmpMachine.AdditionalEquipmentInvestment = this.AdditionalEquipmentInvestment.Value;
            tmpMachine.FundamentalSetupInvestment = this.FundamentalSetupInvestment.Value;
            tmpMachine.ExternalWorkCost = this.ExternalWorkCost.Value;
            tmpMachine.MaterialCost = this.MaterialCost.Value;
            tmpMachine.ConsumablesCost = this.ConsumablesCost.Value;
            tmpMachine.RepairsCost = this.RepairsCost.Value;
            tmpMachine.KValue = this.KValue.Value;
            tmpMachine.CalculateWithKValue = this.CalculateWithKValue.Value;
            tmpMachine.PowerConsumption = this.PowerConsumption.Value;
            tmpMachine.AirConsumption = this.AirConsumption.Value;
            tmpMachine.WaterConsumption = this.WaterConsumption.Value;
            tmpMachine.FloorSize = this.FloorSize.Value;
            tmpMachine.WorkspaceArea = this.WorkspaceArea.Value;
            tmpMachine.FullLoadRate = this.FullLoadRate.Value;
            tmpMachine.ManualConsumableCostCalc = this.ManualConsumableCostCalc.Value;
            tmpMachine.ManualConsumableCostPercentage = this.ManualConsumableCostPercentage.Value;
            tmpMachine.Availability = this.Availability.Value;
            tmpMachine.ManufacturingYear = this.ManufacturingYear.Value;

            if (!string.IsNullOrEmpty(this.CostCalculationVersion) && this.MachineCalculationParams != null)
            {
                ICostCalculator calculator = CostCalculatorFactory.GetCalculator(this.CostCalculationVersion);
                MachineCost cost = calculator.CalculateMachineCost(tmpMachine, this.MachineCalculationParams);

                if (cost != null)
                {
                    using (this.UndoManager.Pause())
                    {
                        this.DepreciationPerHour.Value = cost.DepreciationPerHour;
                        this.MaintenancePerHour.Value = cost.MaintenancePerHour;
                        this.EnergyCostPerHour.Value = cost.EnergyCostPerHour;
                        this.FloorCost.Value = cost.FloorCost;
                        this.FullCostPerHour.Value = cost.FullCostPerHour;
                    }

                    if (!this.ConsumablesCostIsEnabled
                        && this.ConsumablesCost.Value != cost.CalculatedConsumablesCost)
                    {
                        using (this.UndoManager.StartBatch(includePreviousItem: true, reverseUndoOrder: true, navigateToBatchControls: true))
                        {
                            this.ConsumablesCost.Value = cost.CalculatedConsumablesCost;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Refreshes the machine parent's cost and notifies listeners.
        /// </summary>        
        private void RefreshParentCost()
        {
            this.RefreshCalculations(this.MachineParent);
        }

        #endregion Cost handling

        #region Save/Cancel

        /// <summary>
        /// Saves all changes back into the model.
        /// </summary>
        protected override void SaveToModel()
        {
            this.CheckModel();
            base.SaveToModel();

            var manufacturer = this.ManufacturerViewModel.Model;
            if (manufacturer != null)
            {
                this.Model.Manufacturer = manufacturer;
            }
        }

        /// <summary>
        /// Determines whether this instance can perform the save operation. Executed by the SaveCommand.
        /// The default implementation allows the save to be performed if the input is valid.
        /// </summary>
        /// <returns>
        /// true if the save operation can be performed, false otherwise.
        /// </returns>
        protected override bool CanSave()
        {
            return base.CanSave() && this.SaveNestedViewModels.CanExecute(null);
        }

        /// <summary>
        /// Performs the save operation. Executed by the SaveCommand.
        /// </summary>
        protected override void Save()
        {
            this.CheckModelAndDataSource();

            this.SetMachineClassifications(this.Model);

            // Save all changes back into the Model objects. The validity check of this operation is performed by the CanSave method.
            this.SaveToModel();
            this.SaveNestedViewModels.Execute(null);

            if (this.SavesToDataSource)
            {
                // Update machine
                this.DataSourceManager.MachineRepository.Save(this.Model);
                this.DataSourceManager.SaveChanges();

                // Notify the other components that the machine was created/updated.
                EntityChangedMessage message = this.Model.IsMasterData ?
                    new EntityChangedMessage(Notification.MasterDataEntityChanged) :
                    new EntityChangedMessage(Notification.MyProjectsEntityChanged);

                message.ChangeType = this.EditMode == ViewModelEditMode.Create ? EntityChangeType.EntityCreated : EntityChangeType.EntityUpdated;
                message.Entity = this.Model;

                this.SendMessage(message, false);
            }

            this.RefreshParentCost();
            this.MachineClassificationsView.IsChanged = false;

            // Close the view-model when is displayed in a window.            
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Determines whether the Cancel operation can be performed. Executed by the CancelCommand.
        /// </summary>
        /// <returns>
        /// true if the changes can be canceled, false otherwise.
        /// </returns>
        protected override bool CanCancel()
        {
            return base.CanCancel() && this.CancelNestedViewModels.CanExecute(null);
        }

        /// <summary>
        /// Cancels all changes. Executed by the CancelCommand.
        /// </summary>
        protected override void Cancel()
        {
            if (!this.CanCancel())
            {
                return;
            }

            if (this.IsChanged)
            {
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_CancelChanges, MessageDialogType.YesNo);
                if (result != MessageDialogResult.Yes)
                {
                    // Don't cancel the changes and also don't close the view-model.
                    return;
                }
                else
                {
                    using (this.UndoManager.StartBatch())
                    {
                        // Cancel all changes
                        base.Cancel();
                        this.CancelNestedViewModels.Execute(null);
                    }
                }
            }

            // Close the view-model when is displayed in a window.            
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Called before unloading the view from its parent. Returning false will cancel the view's unloading.
        /// </summary>
        /// <returns>
        /// True if the unloading process should continue and false if it should be canceled.
        /// </returns>
        public override bool OnUnloading()
        {
            // Do nothing if the view-model is read-only, in viewer mode, it was not changed or the model was deleted.
            if (this.IsReadOnly || this.IsInViewerMode || !this.IsChanged || this.Model.IsDeleted)
            {
                return true;
            }

            if (this.EditMode == ViewModelEditMode.Create)
            {
                // Ask the user to confirm quitting
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_Quit, MessageDialogType.YesNo);
                if (result != MessageDialogResult.Yes)
                {
                    // The user chose to stay on the screen; return false to stop the screen unloading.
                    return false;
                }
                else
                {
                    this.IsChanged = false;
                }
            }
            else if (this.EditMode == ViewModelEditMode.Edit)
            {
                // Ask the user if he wants to save
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_UnsavedDataOnQuit, MessageDialogType.YesNoCancel);
                if (result == MessageDialogResult.Yes)
                {
                    // The user whishes to save but it is not possible because the input is not valid. Return false to stop the view-model from unloading.
                    if (!this.CanSave())
                    {
                        return false;
                    }

                    this.Save();
                }
                else if (result == MessageDialogResult.No)
                {
                    // The user does not want to save.                    
                    this.IsChanged = false;
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Called after one or more model properties value changed in order to refresh the calculations.
        /// </summary>
        protected override void RefreshCalculation()
        {
            if (this.EditMode == ViewModelEditMode.Create)
            {
                return;
            }

            var parenPart = this.ModelParentClone as Part;
            if (parenPart != null)
            {
                this.RefreshCalculations(parenPart);
            }
            else
            {
                var parentAssembly = this.ModelParentClone as Assembly;
                if (parentAssembly != null)
                {
                    this.RefreshCalculations(parentAssembly);
                }
            }
        }

        #endregion Save/Cancel

        /// <summary>
        /// Calculates the parent and sends a message with the result.
        /// </summary>
        /// <param name="entityParent">The entity parent.</param>
        private void RefreshCalculations(object entityParent)
        {
            if (entityParent == null)
            {
                return;
            }

            var parentPart = entityParent as Part;
            if (parentPart != null)
            {
                PartCostCalculationParameters calculationParams = null;
                if (this.ParentProject != null)
                {
                    calculationParams = CostCalculationHelper.CreatePartParamsFromProject(this.ParentProject);
                }
                else if (IsInViewerMode)
                {
                    // Note: theoretically this part is never reached because it's not possible to save in view mode.                    
                    calculationParams = this.modelBrowserHelperService.GetPartCostCalculationParameters();
                }

                if (calculationParams != null)
                {
                    var calculator = CostCalculatorFactory.GetCalculator(parentPart.CalculationVariant);
                    var result = calculator.CalculatePartCost(parentPart, calculationParams);
                    this.SendMessage(new CurrentComponentCostChangedMessage(result), true);
                }
            }
            else
            {
                var parentAssembly = entityParent as Assembly;
                if (parentAssembly != null)
                {
                    AssemblyCostCalculationParameters calculationParams = null;
                    if (this.ParentProject != null)
                    {
                        calculationParams = CostCalculationHelper.CreateAssemblyParamsFromProject(this.ParentProject);
                    }
                    else if (IsInViewerMode)
                    {
                        // Note: theoretically this part is never reached because it's not possible to save in view mode.
                        calculationParams = this.modelBrowserHelperService.GetAssemblyCostCalculationParameters();
                    }

                    if (calculationParams != null)
                    {
                        var calculator = CostCalculatorFactory.GetCalculator(parentAssembly.CalculationVariant);
                        var result = calculator.CalculateAssemblyCost(parentAssembly, calculationParams);
                        this.SendMessage(new CurrentComponentCostChangedMessage(result), true);
                    }
                }
            }
        }

        /// <summary>
        /// Sends a message with or without a token
        /// </summary>
        /// <typeparam name="TMessage">The message type</typeparam>
        /// <param name="message">The message to send</param>
        /// <param name="sendWithToken">A value indicating whether to send the message with a token or send it globally without one</param>
        private void SendMessage<TMessage>(TMessage message, bool sendWithToken)
        {
            if (sendWithToken)
            {
                if (this.MessageToken == null)
                {
                    this.messenger.Send(message, this.IsInViewerMode ? GlobalMessengerTokens.ModelBrowserTargetToken : GlobalMessengerTokens.MainViewTargetToken);
                }
                else
                {
                    this.messenger.Send(message, this.MessageToken);
                }
            }
            else
            {
                this.messenger.Send(message);
            }
        }
    }
}