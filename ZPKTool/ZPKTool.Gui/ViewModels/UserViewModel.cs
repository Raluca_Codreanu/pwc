﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Controls;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for the <see cref="UserView"/>.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class UserViewModel : ViewModel<User, IDataSourceManager>
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        #endregion Attributes

        /// <summary>
        /// Initializes a new instance of the <see cref="UserViewModel"/> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        [ImportingConstructor]
        public UserViewModel(IWindowService windowService)
        {
            Argument.IsNotNull("windowService", windowService);

            this.windowService = windowService;
            this.ShowCancelMessageFlag = true;

            this.AddRoleCommand = new DelegateCommand(AddRole);
            this.RemoveRoleCommand = new DelegateCommand(RemoveRole);
        }

        #region Properties

        /// <summary>
        /// Gets the username.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Username", ErrorMessageResourceType = typeof(LocalizedResources))]
        [ExposesModelProperty("Username")]
        public DataProperty<string> Username { get; private set; }

        /// <summary>
        /// Gets the name.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Name", ErrorMessageResourceType = typeof(LocalizedResources))]
        [ExposesModelProperty("Name")]
        public DataProperty<string> Name { get; private set; }

        /// <summary>
        /// Gets the description.
        /// </summary>
        [ExposesModelProperty("Description")]
        public DataProperty<string> Description { get; private set; }

        /// <summary>
        /// Gets the disabled flag value.
        /// </summary>
        [ExposesModelProperty("Disabled")]
        public DataProperty<bool> IsDisabled { get; private set; }

        /// <summary>
        /// Gets or sets the password inputted by the user.
        /// </summary>
        [PasswordValidator]
        public DataProperty<string> Password { get; set; }

        /// <summary>
        /// Gets the validation for the password inputted by the user.
        /// </summary>
        public DataProperty<bool?> IsPasswordValid { get; private set; }

        /// <summary>
        /// Gets or sets the password necessity.
        /// </summary>
        public TextBoxControlNecessityLevel PasswordNecessity { get; set; }

        /// <summary>
        /// Gets or sets the available roles list.
        /// </summary>
        public VMProperty<EnumerationDescription<Role>> AvailableRoles { get; set; }

        /// <summary>
        /// Gets or sets the current roles list.
        /// </summary>
        public VMProperty<EnumerationDescription<Role>> CurrentRoles { get; set; }

        /// <summary>
        /// Gets or sets the selected role from a list.
        /// </summary>
        public Role SelectedAvailableRole { get; set; }

        /// <summary>
        /// Gets or sets the selected role from a list.
        /// </summary>
        public Role SelectedCurrentRole { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the show cancel message will appear.
        /// </summary>
        public bool ShowCancelMessageFlag { get; set; }

        #endregion Properties

        #region Commands

        /// <summary>
        /// Gets the add role command.
        /// </summary>        
        public ICommand AddRoleCommand { get; private set; }

        /// <summary>
        /// Gets the remove role command.
        /// </summary>        
        public ICommand RemoveRoleCommand { get; private set; }

        #endregion Commands

        /// <summary>
        /// Called when the Model has changed.
        /// </summary>
        protected override void OnModelChanged()
        {
            this.CheckDataSource();
            base.OnModelChanged();

            this.AvailableRoles.Value = new EnumerationDescription<Role>();
            this.CurrentRoles.Value = new EnumerationDescription<Role>();

            // Add the roles that the user has in the current roles list, and the rest of the roles in the available roles list.
            foreach (Role currentRole in Enum.GetValues(typeof(Role)))
            {
                if (currentRole == Role.None)
                {
                    var role = AvailableRoles.Value.FirstOrDefault(r => r.Value == currentRole);
                    AvailableRoles.Value.Remove(role);

                    role = CurrentRoles.Value.FirstOrDefault(r => r.Value == currentRole);
                    CurrentRoles.Value.Remove(role);
                }
                else
                {
                    if (this.Model.Roles.HasFlag(currentRole))
                    {
                        var role = AvailableRoles.Value.FirstOrDefault(r => r.Value == currentRole);
                        AvailableRoles.Value.Remove(role);
                    }
                    else
                    {
                        var role = CurrentRoles.Value.FirstOrDefault(r => r.Value == currentRole);
                        CurrentRoles.Value.Remove(role);
                    }
                }
            }

            if (this.EditMode == ViewModelEditMode.Edit)
            {
                this.Title = LocalizedResources.User_Edit;
                this.PasswordNecessity = TextBoxControlNecessityLevel.Optional;
            }
            else if (this.EditMode == ViewModelEditMode.Create)
            {
                this.Title = LocalizedResources.User_Create;
                this.PasswordNecessity = TextBoxControlNecessityLevel.Mandatory;
            }
        }

        /// <summary>
        /// Add a role from the available roles list to the current roles list.
        /// </summary>
        private void AddRole()
        {
            if (AvailableRoles.Value.Any(r => r.Value == SelectedAvailableRole))
            {
                var role = AvailableRoles.Value.FirstOrDefault(r => r.Value == SelectedAvailableRole);
                AvailableRoles.Value.Remove(role);
                CurrentRoles.Value.Add(role);
                this.IsChanged = true;
            }
        }

        /// <summary>
        /// Remove a role from the current roles list and add it back to the available roles list.
        /// </summary>
        private void RemoveRole()
        {
            if (CurrentRoles.Value.Any(r => r.Value == SelectedCurrentRole))
            {
                var role = CurrentRoles.Value.FirstOrDefault(r => r.Value == SelectedCurrentRole);
                CurrentRoles.Value.Remove(role);
                AvailableRoles.Value.Add(role);
                this.IsChanged = true;
            }
        }

        /// <summary>
        /// Check if a user has a given role.
        /// </summary>
        /// <param name="user">The user for which to check.</param>
        /// <param name="role">The given role.</param>
        /// <returns>True if the user has the role, false otherwise.</returns>
        private bool UserHasRole(User user, Role role)
        {
            return RoleRights.HasRole(user.Roles, role);
        }

        #region Save/Cancel

        /// <summary>
        /// Saves all changes back into the model.
        /// </summary>
        protected override void SaveToModel()
        {
            this.CheckModel();
            base.SaveToModel();

            var userToSave = this.Model;
            if (this.EditMode == ViewModelEditMode.Create
                || !string.IsNullOrWhiteSpace(this.Password.Value))
            {
                SecurityManager.Instance.ValidatePassword(this.Password.Value);
                userToSave.Password = EncryptionManager.Instance.HashSHA256(this.Password.Value, userToSave.Salt, userToSave.Guid.ToString());
                userToSave.IsPasswordSetByUser = false;
            }

            var newRoles = Role.None;
            foreach (Role currentRole in this.CurrentRoles.Value.Select(r => r.Value))
            {
                newRoles = newRoles | currentRole;
            }

            userToSave.Roles = newRoles;
        }

        /// <summary>
        /// Determines whether this instance can save.
        /// </summary>
        /// <returns>
        /// True if this instance can save; otherwise, false.
        /// </returns>
        protected override bool CanSave()
        {
            if (this.EditMode == ViewModelEditMode.Create)
            {
                return base.CanSave()
                    && this.IsPasswordValid.Value.GetValueOrDefault();
            }

            return base.CanSave()
                && (string.IsNullOrWhiteSpace(this.Password.Value)
                    || (!string.IsNullOrWhiteSpace(this.Password.Value)
                        && this.IsPasswordValid.Value.GetValueOrDefault()));
        }

        /// <summary>
        /// Performs the save operation. Executed by the SaveCommand.
        /// </summary>
        protected override void Save()
        {
            // If there is no current role added, the user can not be saved.
            if (CurrentRoles.Value.Count == 0)
            {
                this.windowService.MessageDialogService.Show(LocalizedResources.RequiredField_Roles, MessageDialogType.Error);
                return;
            }

            this.CheckModelAndDataSource();

            // Save all changes back into the Model objects. The validity check of this operation is performed by the CanSave method.
            this.SaveToModel();

            this.DataSourceManager.UserRepository.Save(this.Model);
            this.DataSourceManager.SaveChanges();

            if (this.EditMode == ViewModelEditMode.Edit)
            {
                this.windowService.MessageDialogService.Show(LocalizedResources.User_Updated, MessageDialogType.Info);
                LoggingManager.Log(LogEventType.EditedUser, Username.Value);
            }
            else
            {
                this.windowService.MessageDialogService.Show(LocalizedResources.User_Created, MessageDialogType.Info);
                LoggingManager.Log(LogEventType.CreatedUser, Username.Value);
            }

            // Close the view-model when is displayed in a window.
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Cancels all changes. Executed by the CancelCommand.
        /// </summary>
        protected override void Cancel()
        {
            if (!this.CanCancel())
            {
                return;
            }

            if (this.IsChanged)
            {
                if (this.ShowCancelMessageFlag)
                {
                    var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_CancelChanges, MessageDialogType.YesNo);
                    if (result != MessageDialogResult.Yes)
                    {
                        // Don't cancel the changes and also don't close the view-model.
                        return;
                    }
                }

                // Cancel all changes
                base.Cancel();
            }

            // Close the view-model when is displayed in a window.
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Called before unloading the view from its parent. Returning false will cancel the view's unloading.
        /// </summary>
        /// <returns>
        /// True if the unloading process should continue and false if it should be canceled.
        /// </returns>
        public override bool OnUnloading()
        {
            // Do nothing if the view-model is read-only, in viewer mode or it was not changed .
            if (this.IsReadOnly || this.IsInViewerMode || !this.IsChanged)
            {
                return true;
            }

            if (this.EditMode == ViewModelEditMode.Create)
            {
                // Ask the user to confirm quitting
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_Quit, MessageDialogType.YesNo);
                if (result != MessageDialogResult.Yes)
                {
                    // The user chose to stay on the screen; return false to stop the screen unloading.
                    return false;
                }
                else
                {
                    this.IsChanged = false;
                }
            }
            else if (this.EditMode == ViewModelEditMode.Edit)
            {
                // Ask the user if he wants to save
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_UnsavedDataOnQuit, MessageDialogType.YesNoCancel);
                if (result == MessageDialogResult.Yes)
                {
                    // The user whishes to save but it is not possible because the input is not valid. Return false to stop the view-model from unloading.
                    if (!this.CanSave())
                    {
                        return false;
                    }

                    this.Save();
                }
                else if (result == MessageDialogResult.No)
                {
                    // The user does not want to save.                    
                    this.IsChanged = false;
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        #endregion Save/Cancel

        #region Inner Classes

        /// <summary>
        /// Validation attribute for the password field.
        /// </summary>
        private class PasswordValidator : ValidationAttribute
        {
            /// <summary>
            /// Validates the specified value with respect to the current validation attribute.
            /// </summary>
            /// <param name="value">The value to validate.</param>
            /// <param name="validationContext">The context information about the validation operation.</param>
            /// <returns>
            /// An instance of the <see cref="T:System.ComponentModel.DataAnnotations.ValidationResult" /> class.
            /// </returns>
            protected override ValidationResult IsValid(object value, ValidationContext validationContext)
            {
                object viewModel;
                validationContext.Items.TryGetValue("ViewModel", out viewModel);

                bool isValid = true;
                var password = value as string;
                var userViewModel = viewModel as UserViewModel;

                if (userViewModel != null)
                {
                    if (string.IsNullOrEmpty(password) && userViewModel.PasswordNecessity == TextBoxControlNecessityLevel.Mandatory)
                    {
                        // If the password necessity is mandatory and the password is null, the field is not valid.
                        isValid = false;
                    }
                }

                return isValid ? ValidationResult.Success : new ValidationResult(LocalizedResources.General_RequiredField);
            }
        }

        #endregion Inner Classes
    }
}
