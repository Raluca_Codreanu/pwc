﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations;
using System.Windows.Input;
using ZPKTool.Business.TransportCostCalculator;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the TransportCostCalculationSetting view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class TransportCostCalculationSettingViewModel : ViewModel<TransportCostCalculationSetting, IDataSourceManager>
    {
        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        /// <summary>
        /// Initializes a new instance of the <see cref="TransportCostCalculationSettingViewModel"/> class.
        /// </summary>
        /// <param name="unitsService">The units service.</param>
        [ImportingConstructor]
        public TransportCostCalculationSettingViewModel(IUnitsService unitsService)
        {
            Argument.IsNotNull("unitsService", unitsService);
            this.unitsService = unitsService;

            this.Transporters = TransportCostCalculator.GetTransporters();
        }

        #region Commands

        /// <summary>
        /// Gets or sets the command that closes the popup with the additional settings.
        /// </summary>
        public ICommand CloseSettingsPopupCommand { get; set; }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the days worked in an year.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_DaysPerYear", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("DaysPerYear")]
        public DataProperty<int?> DaysPerYear { get; private set; }

        /// <summary>
        /// Gets the average load target in percentage.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_AverageLoadTarget", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("AverageLoadTarget")]
        public DataProperty<decimal?> AverageLoadTarget { get; private set; }

        /// <summary>
        /// Gets the cost per km for a Truck-Trailer transporter.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_TruckTrailerCost", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("TruckTrailerCost")]
        public DataProperty<decimal?> TruckTrailerCost { get; private set; }

        /// <summary>
        /// Gets the cost per km for a Mega-Trailer transporter.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_MegaTrailerCost", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("MegaTrailerCost")]
        public DataProperty<decimal?> MegaTrailerCost { get; private set; }

        /// <summary>
        /// Gets the cost per km for a 7t Truck transporter.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Truck7tCost", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Truck7tCost")]
        public DataProperty<decimal?> Truck7tCost { get; private set; }

        /// <summary>
        /// Gets the cost per km for a 3,5t Truck transporter.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Truck3_5tCost", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Truck3_5tCost")]
        public DataProperty<decimal?> Truck3_5tCost { get; private set; }

        /// <summary>
        /// Gets the cost per km for a Van-Type transporter.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_VanTypeCost", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("VanTypeCost")]
        public DataProperty<decimal?> VanTypeCost { get; private set; }

        /// <summary>
        /// Gets the timestamp for the setting's creation.
        /// </summary>
        [ExposesModelProperty("Timestamp")]
        public DataProperty<DateTime> Timestamp { get; private set; }

        /// <summary>
        /// Gets the list of transporters.
        /// </summary>
        public ICollection<Transporter> Transporters { get; private set; }

        /// <summary>
        /// Gets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter { get; private set; }

        #endregion

        /// <summary>
        /// Called when the data source manager has changed.
        /// </summary>
        protected override void OnDataSourceManagerChanged()
        {
            base.OnDataSourceManagerChanged();

            this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(this.DataSourceManager);
        }

        /// <summary>
        /// Performs the save operation. Executed by the SaveCommand.
        /// </summary>
        protected override void Save()
        {
            // If the settings have been changed, save the current date time into Timestamp.
            this.Timestamp.Value = DateTime.Now;
            this.CheckModelAndDataSource();

            // Save all changes back into the Model objects. The validity check of this operation is performed by the CanSave method.
            this.SaveToModel();
        }
    }
}
