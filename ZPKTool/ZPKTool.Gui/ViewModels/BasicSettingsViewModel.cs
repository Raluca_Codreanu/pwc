﻿using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the Basic Settings view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]

    public class BasicSettingsViewModel : ViewModel<BasicSetting, IDataSourceManager>
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        #endregion Attributes

        /// <summary>
        /// Initializes a new instance of the <see cref="BasicSettingsViewModel"/> class.
        /// </summary>
        /// <param name="messenger">The messenger.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="unitsService">The units service.</param>
        [ImportingConstructor]
        public BasicSettingsViewModel(
            IMessenger messenger,
            IWindowService windowService,
            IUnitsService unitsService)
        {
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("unitsService", unitsService);

            this.messenger = messenger;
            this.windowService = windowService;
            this.unitsService = unitsService;
        }

        #region Properties

        /// <summary>
        /// Gets the depreciation period.
        /// </summary>
        [Required(ErrorMessageResourceName = "BasicSettings_DepreciationPeriodValidationError", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("DeprPeriod")]
        public DataProperty<int?> DeprPeriod { get; private set; }

        /// <summary>
        /// Gets the batch size.
        /// </summary>
        [Required(ErrorMessageResourceName = "BasicSettings_BatchSizeValidationError", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("PartBatch")]
        public DataProperty<int?> PartBatch { get; private set; }

        /// <summary>
        /// Gets the hours per shift.
        /// </summary>
        [Required(ErrorMessageResourceName = "BasicSettings_HoursPerShiftValidationError", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("HoursPerShift")]
        public DataProperty<decimal?> HoursPerShift { get; private set; }

        /// <summary>
        /// Gets the production weeks.
        /// </summary>
        [Required(ErrorMessageResourceName = "BasicSettings_ProductionWeeksValidationError", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("ProdWeeks")]
        public DataProperty<decimal?> ProdWeeks { get; private set; }

        /// <summary>
        /// Gets the logic cost ratio.
        /// </summary>
        [Required(ErrorMessageResourceName = "BasicSettings_LogisticCostRatioValidationError", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("LogisticCostRatio")]
        public DataProperty<decimal?> LogisticCostRatio { get; private set; }

        /// <summary>
        /// Gets the depreciation rate.
        /// </summary>
        [Required(ErrorMessageResourceName = "BasicSettings_DepreciationRateValidationError", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("DeprRate")]
        public DataProperty<decimal?> DeprRate { get; private set; }

        /// <summary>
        /// Gets the shifts per week.
        /// </summary>
        [Required(ErrorMessageResourceName = "BasicSettings_ShiftsPerWeekValidationError", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("ShiftsPerWeek")]
        public DataProperty<decimal?> ShiftsPerWeek { get; private set; }

        /// <summary>
        /// Gets the production days.
        /// </summary>
        [Required(ErrorMessageResourceName = "BasicSettings_ProductionDaysValidationError", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("ProdDays")]
        public DataProperty<decimal?> ProdDays { get; private set; }

        /// <summary>
        /// Gets the asset rate.
        /// </summary>
        [Required(ErrorMessageResourceName = "BasicSettings_AssetRateValidationError", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("AssetRate")]
        public DataProperty<decimal?> AssetRate { get; private set; }

        /// <summary>
        /// Gets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter { get; private set; }

        #endregion Properties

        /// <summary>
        /// Called when the Model property has changed.
        /// </summary>
        protected override void OnModelChanged()
        {
            this.CheckDataSource();
            this.LoadDataFromModel();
            this.UndoManager.Start();
        }

        /// <summary>
        /// Called when DataSourceManager has changed.
        /// </summary>
        protected override void OnDataSourceManagerChanged()
        {
            base.OnDataSourceManagerChanged();

            this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(this.DataSourceManager);
        }

        /// <summary>
        /// Performs the save operation. Executed by the SaveCommand.
        /// </summary>
        protected override void Save()
        {
            this.CheckModelAndDataSource();
            this.SaveToModel();
            this.DataSourceManager.BasicSettingsRepository.Save(this.Model);
            this.DataSourceManager.SaveChanges();
        }

        /// <summary>
        /// Cancels all changes. Executed by the CancelCommand.
        /// </summary>
        protected override void Cancel()
        {
            if (this.IsChanged)
            {
                MessageDialogResult result = MessageDialogResult.Yes;
                if (!this.IsChild)
                {
                    result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_CancelChanges, MessageDialogType.YesNo);
                }

                if (result != MessageDialogResult.Yes)
                {
                    // Don't cancel the changes and also don't close the view-model.
                    return;
                }
                else
                {
                    // Cancel all changes
                    base.Cancel();
                }
            }

            // Close the view-model when is displayed in a window.
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Called before unloading the view from its parent. Returning false will cancel the view's unloading.
        /// </summary>
        /// <returns>
        /// True if the unloading process should continue and false if it should be canceled.
        /// </returns>
        public override bool OnUnloading()
        {
            // Do nothing if the view-model is read-only, in viewer mode or it was not changed.
            if (this.IsReadOnly || this.IsInViewerMode || !this.IsChanged)
            {
                return true;
            }

            // Ask the user if he wants to save
            var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_UnsavedDataOnQuit, MessageDialogType.YesNoCancel);
            if (result == MessageDialogResult.Yes)
            {
                // The user whishes to save but it is not possible because the input is not valid. Return false to stop the view-model from unloading.
                if (!this.CanSave())
                {
                    return false;
                }

                this.Save();
            }
            else if (result == MessageDialogResult.No)
            {
                // The user does not want to save.
                this.IsChanged = false;
            }
            else
            {
                return false;
            }

            return true;
        }
    }
}