﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Controls;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Managers;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for the <see cref="ManageReleasedProjectsView"/>.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ManageReleasedProjectsViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The delay to display the loading screen when getting the projects and users.
        /// </summary>
        private const int DisplayDelay = 1000;

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The PleaseWaitService reference.
        /// </summary>
        private IPleaseWaitService pleaseWaitService;

        /// <summary>
        /// The composition container.
        /// </summary>
        private CompositionContainer container;

        /// <summary>
        /// The data manager to work with.
        /// </summary>
        private IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

        /// <summary>
        /// The screen coordinates where the current drag operation has started.
        /// </summary>
        private Point? dragStartPosition;

        /// <summary>
        /// The selected projects tree item.
        /// </summary>
        private TreeViewDataItem selectedTreeItem;

        #endregion Attributes

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ManageReleasedProjectsViewModel"/> class.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="pleaseWaitService">The please wait service.</param>
        [ImportingConstructor]
        public ManageReleasedProjectsViewModel(
            CompositionContainer container,
            IWindowService windowService,
            IPleaseWaitService pleaseWaitService)
        {
            this.container = container;

            this.messenger = new Messenger();
            this.WindowService = windowService;
            this.pleaseWaitService = pleaseWaitService;

            // Initialize commands.
            InitializeCommands();

            // Initialize tree.
            this.InitializeManageReleasedProjectsTree();

            // Register the message handlers.
            this.RegisterMessageHandlers();
        }

        #endregion

        #region Poperties

        /// <summary>
        /// Gets the projects and folder projects.
        /// </summary>
        public VMProperty<ObservableCollection<TreeViewDataItem>> Items { get; private set; }

        /// <summary>
        /// Gets the manage released projects item.
        /// </summary>
        public ManageReleasedProjectsTreeItem ManageReleasedProjectsItem { get; private set; }

        /// <summary>
        /// Gets the selected projects tree item.
        /// </summary>        
        public TreeViewDataItem SelectedTreeItem
        {
            get { return this.selectedTreeItem; }
            private set { this.SetProperty(ref this.selectedTreeItem, value, () => this.SelectedTreeItem); }
        }

        /// <summary>
        /// Gets or sets the window service.
        /// </summary>
        public IWindowService WindowService
        {
            get
            {
                return this.windowService;
            }

            set
            {
                if (this.windowService != value)
                {
                    this.windowService = value;
                    OnPropertyChanged(() => this.WindowService);
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected tree items.
        /// </summary>
        public IList<object> SelectedTreeItems { get; set; }

        #endregion Propeties

        #region Commands

        /// <summary>
        /// Gets the command executed when the selected item of the manage released projects tree view has changed.
        /// </summary>        
        public ICommand ManageReleasedProjectsTreeSelectedItemChangedCommand { get; private set; }

        /// <summary>
        /// Gets the command executed after the selected items collection is changed.
        /// </summary>
        public ICommand SelectedItemsChangedCommand { get; private set; }

        /// <summary>
        /// Gets the rename command.
        /// </summary>
        public ICommand RenameCommand { get; private set; }

        /// <summary>
        /// Gets the cut command.
        /// </summary>
        public ICommand CutCommand { get; private set; }

        /// <summary>
        /// Gets the paste command.
        /// </summary>
        public ICommand PasteCommand { get; private set; }

        /// <summary>
        /// Gets the create project folder command.
        /// </summary>        
        public ICommand CreateProjectFolderCommand { get; private set; }

        /// <summary>
        /// Gets the Un-release Project command.
        /// </summary>
        public ICommand UnreleaseProjectCommand { get; private set; }

        /// <summary>
        /// Gets the Un-release Project Folder command.
        /// </summary>
        public ICommand UnreleaseProjectFolderCommand { get; private set; }

        /// <summary>
        /// Gets the command executed for the PreviewMouseDown event of the Manage Released Projects Tree.
        /// </summary>        
        public ICommand PreviewMouseDownCommand { get; private set; }

        /// <summary>
        /// Gets the command executed for the PreviewMouseUp event of the Manage Released Projects Tree.
        /// </summary>
        public ICommand PreviewMouseUpCommand { get; private set; }

        /// <summary>
        /// Gets the command executed for the PreviewMouseMove event of the Manage Released Projects Tree.
        /// </summary>
        public ICommand PreviewMouseMoveCommand { get; private set; }

        /// <summary>
        /// Gets the command executed for the DragEnter event of the Manage Released Projects Tree.
        /// </summary>        
        public ICommand DragEnterCommand { get; private set; }

        /// <summary>
        /// Gets the command executed for the DragLeave event of the Manage Released Projects Tree.
        /// </summary>        
        public ICommand DragLeaveCommand { get; private set; }

        /// <summary>
        /// Gets the command executed for the DragOver event of the Manage Released Projects Tree.
        /// </summary>        
        public ICommand DragOverCommand { get; private set; }

        /// <summary>
        /// Gets the command executed for the QueryContinueDrag event of the Manage Released Projects Tree.
        /// </summary>
        public ICommand QueryContinueDragCommand { get; private set; }

        /// <summary>
        /// Gets the command executed for the Drop event of the Manage Released Projects Tree.
        /// </summary>        
        public ICommand DropCommand { get; private set; }

        #endregion Commands

        #region Initialization

        /// <summary>
        /// Initialize commands
        /// </summary>
        private void InitializeCommands()
        {
            this.RenameCommand = new DelegateCommand<object>(this.Rename, this.CanRename);
            this.CutCommand = new DelegateCommand<object>(this.ExecuteCut, this.CanCut);
            this.PasteCommand = new DelegateCommand<object>(this.ExecutePaste, this.CanPaste);
            this.CreateProjectFolderCommand = new DelegateCommand<object>(this.CreateProjectFolder, this.CanCreateProjectFolder);
            this.UnreleaseProjectCommand = new DelegateCommand<object>(this.UnreleaseProject, this.CanUnreleaseProject);
            this.UnreleaseProjectFolderCommand = new DelegateCommand<object>(this.UnreleaseProjectFolder, this.CanUnreleaseProjectFolder);

            this.ManageReleasedProjectsTreeSelectedItemChangedCommand = new DelegateCommand<RoutedPropertyChangedEventArgs<object>>(this.SelectedTreeItemChangedAction);
            this.PreviewMouseDownCommand = new DelegateCommand<MouseButtonEventArgs>(this.HandleManageReleasedProjectsTreePreviewMouseDown);
            this.PreviewMouseUpCommand = new DelegateCommand<MouseButtonEventArgs>(this.HandleManageReleasedProjectsTreePreviewMouseUp);
            this.PreviewMouseMoveCommand = new DelegateCommand<MouseEventArgs>(this.HandleManageReleasedProjectsTreePreviewMouseMove);
            this.SelectedItemsChangedCommand = new DelegateCommand<SelectedItemsChangedEventArgs>(this.OnSelectedTreeItemsChanged);

            this.DragEnterCommand = new DelegateCommand<DragEventArgs>(this.HandleManageReleasedProjectsTreeDragEnter);
            this.DragOverCommand = new DelegateCommand<DragEventArgs>(this.HandleManageReleasedProjectsTreeDragOver);
            this.DragLeaveCommand = new DelegateCommand<DragEventArgs>(this.HandleManageReleasedProjectsTreeDragLeave);
            this.QueryContinueDragCommand = new DelegateCommand<QueryContinueDragEventArgs>(this.HandleManageReleasedProjectsTreeQueryContinueDrag);
            this.DropCommand = new DelegateCommand<DragEventArgs>(this.HandleManageReleasedProjectsTreeTreeDrop);
        }

        /// <summary>
        /// Initialize the tree, add the root item.
        /// </summary>
        private void InitializeManageReleasedProjectsTree()
        {
            this.Items.Value = new ObservableCollection<TreeViewDataItem>();

            // Initialize Manage Released Projects Root Item
            this.ManageReleasedProjectsItem = new ManageReleasedProjectsTreeItem()
            {
                Label = LocalizedResources.General_ReleasedProjects,
                IconResourceKey = Images.ProjectIconKey,
                IsExpanded = true,
                Uid = ProjectsTreeItemUid.ManageReleasedProjectsRoot,
                AllowChildrenMultipleSelection = true
            };

            this.Items.Value.Add(this.ManageReleasedProjectsItem);
        }

        /// <summary>
        /// Registers the necessary message handlers with the IMessenger service.
        /// </summary>
        private void RegisterMessageHandlers()
        {
            this.messenger.Register<EntityChangedMessage>(this.HandleEntityChangedMessage);
            this.messenger.Register<EntitiesChangedMessage>(this.HandleEntitiesChangedMessage);
        }

        #endregion Initialization

        /// <summary>
        /// Method called after the view is loaded.
        /// Populate the manage released projects tree.
        /// </summary>
        public override void OnLoaded()
        {
            List<object> foldersAndProjects = new List<object>();

            Action<PleaseWaitService.WorkParams> work = (workParams) =>
            {
                foldersAndProjects.AddRange(this.dataManager.ProjectFolderRepository.GetReleasedProjectFolders().ToList());
                foldersAndProjects.AddRange(this.dataManager.ProjectRepository.GetReleasedProjects().ToList());
            };

            Action<PleaseWaitService.WorkParams> workCompleted = (workParams) =>
            {
                if (workParams.Error != null)
                {
                    windowService.MessageDialogService.Show(workParams.Error);
                }
                else
                {
                    this.PopulateManageReleasedProjects(foldersAndProjects, this.ManageReleasedProjectsItem, null);
                }
            };

            this.pleaseWaitService.Show(LocalizedResources.General_LoadingManageReleasedProjects, DisplayDelay, work, workCompleted);
        }

        /// <summary>
        /// Recursively populate de manage released projects tree with the released projects and released folders.
        /// </summary>
        /// <param name="foldersAndProjects">A list containing all the released projects and folders.</param>
        /// <param name="treeItem">The tree item to add the children.</param>
        /// <param name="parentGuid">The parent Guid.</param>
        private void PopulateManageReleasedProjects(List<object> foldersAndProjects, TreeViewDataItem treeItem, Guid? parentGuid)
        {
            List<object> foldersProjects = new List<object>();

            foreach (var obj in foldersAndProjects)
            {
                ProjectFolder folder = obj as ProjectFolder;
                if (folder != null)
                {
                    if (parentGuid == null)
                    {
                        if (folder.ParentProjectFolder == null)
                        {
                            foldersProjects.Add(folder);
                        }
                    }
                    else
                    {
                        if (folder.ParentProjectFolder != null
                            && folder.ParentProjectFolder.Guid == parentGuid)
                        {
                            foldersProjects.Add(folder);
                        }
                    }
                }
                else
                {
                    Project proj = obj as Project;
                    if (proj != null)
                    {
                        if (parentGuid == null)
                        {
                            if (proj.ProjectFolder == null)
                            {
                                foldersProjects.Add(proj);
                            }
                        }
                        else
                        {
                            if (proj.ProjectFolder != null
                                && proj.ProjectFolder.Guid == parentGuid)
                            {
                                foldersProjects.Add(proj);
                            }
                        }
                    }
                }
            }

            foreach (var obj in foldersProjects)
            {
                var releasedItem = new ManageReleasedProjectsTreeItem();
                Guid? guid = null;

                var folder = obj as ProjectFolder;
                if (folder != null)
                {
                    guid = folder.Guid;
                    releasedItem.Label = folder.Name;
                    releasedItem.IconResourceKey = Images.FolderIconKey;
                    releasedItem.Uid = ProjectsTreeItemUid.ManageReleasedProjectsReleasedFolder;
                    releasedItem.GroupId = 0;
                    releasedItem.AllowChildrenMultipleSelection = true;
                }
                else
                {
                    var proj = obj as Project;
                    if (proj != null)
                    {
                        guid = proj.Guid;
                        releasedItem.Label = proj.Name;
                        releasedItem.IconResourceKey = Images.ProjectIconKey;
                        releasedItem.Uid = ProjectsTreeItemUid.ManageReleasedProjectsReleasedProject;
                        releasedItem.GroupId = 1;
                    }
                }

                releasedItem.DataObject = obj;
                releasedItem.AllowMultipleSelection = true;

                treeItem.Children.Add(releasedItem);
                foldersAndProjects.Remove(obj);

                this.PopulateManageReleasedProjects(foldersAndProjects, releasedItem, guid);
            }
        }

        #region Selected items handling

        /// <summary>
        /// The action performed when the SelectedTreeItemChanged command is executed.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.RoutedPropertyChangedEventArgs&lt;System.Object&gt;"/> instance containing the event data.</param>
        private void SelectedTreeItemChangedAction(RoutedPropertyChangedEventArgs<object> e)
        {
            this.SelectedTreeItem = e.NewValue as TreeViewDataItem;
        }

        /// <summary>
        /// The action performed when the SelectedItemsChanged command is executed.
        /// </summary>
        /// <param name="e">The <see cref="object"/> instance containing the event data.</param>
        private void OnSelectedTreeItemsChanged(SelectedItemsChangedEventArgs e)
        {
            this.SelectedTreeItems = e.SelectedItems;
        }

        #endregion Selected items handling

        #region Menu commands handling

        /// <summary>
        /// Determines whether the Rename command can be executed.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        /// true if the command can be executed; false otherwise.
        /// </returns>
        private bool CanRename(object parameter)
        {
            if (this.SelectedTreeItems == null
               || this.SelectedTreeItems.Count != 1)
            {
                return false;
            }

            var selectedItem = this.SelectedTreeItems.FirstOrDefault() as TreeViewDataItem;
            return selectedItem != null
                && !selectedItem.ReadOnly &&
                (selectedItem.DataObject is ProjectFolder);
        }

        /// <summary>
        /// Executes the logic associated with the Rename command.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void Rename(object parameter)
        {
            if (this.SelectedTreeItems == null
              || this.SelectedTreeItems.Count != 1)
            {
                return;
            }

            var selectedItem = this.SelectedTreeItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem.ReadOnly)
            {
                return;
            }

            ProjectFolder folder = selectedItem.DataObject as ProjectFolder;
            if (folder == null)
            {
                return;
            }

            ProjectFolderViewModel viewModel = new ProjectFolderViewModel(this.messenger, this.WindowService);
            viewModel.EditMode = ViewModelEditMode.Edit;
            viewModel.Title = LocalizedResources.General_RenameFolder;
            viewModel.DataSourceManager = this.dataManager;
            viewModel.Model = folder;

            this.windowService.ShowViewInDialog(viewModel);
        }

        /// <summary>
        /// Determines whether the Cut command can be execute.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        /// true if the command can be executed, false otherwise.
        /// </returns>
        private bool CanCut(object parameter)
        {
            if (this.SelectedTreeItems == null || this.SelectedTreeItems.Count == 0)
            {
                return false;
            }

            foreach (var item in this.SelectedTreeItems)
            {
                var treeItem = item as TreeViewDataItem;
                if (treeItem == null
                    || treeItem.ReadOnly
                    || !(treeItem.DataObject is ProjectFolder
                    || treeItem.DataObject is Project))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Performs the logic associated with the Cut command.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void ExecuteCut(object parameter)
        {
            if (this.SelectedTreeItems == null || this.SelectedTreeItems.Count == 0)
            {
                return;
            }

            var itemsToMove = new List<ClipboardObject>();
            foreach (var item in this.SelectedTreeItems)
            {
                var treeItem = item as TreeViewDataItem;
                if (treeItem != null)
                {
                    itemsToMove.Add(new ClipboardObject() { Entity = treeItem.DataObject, DbSource = treeItem.DataObjectSource });
                }
            }

            ClipboardManager.Instance.Cut(itemsToMove);
        }

        /// <summary>
        /// Determines whether the Paste command can be execute.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        /// true if the command can be executed, false otherwise.
        /// </returns>
        private bool CanPaste(object parameter)
        {
            if (this.SelectedTreeItems == null
                || this.SelectedTreeItems.Count != 1)
            {
                return false;
            }

            var selectedItem = this.SelectedTreeItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null
                || selectedItem.ReadOnly
                || ClipboardManager.Instance.Operation == ClipboardOperation.Copy
                || !ClipboardManager.Instance.PeekClipboardObjects.Any())
            {
                return false;
            }

            var clipboardObjects = ClipboardManager.Instance.PeekClipboardObjects;
            foreach (var obj in clipboardObjects)
            {
                // Verify if the item from the clipboard is the same as the item recipient and if it was released.
                var releasableObject = obj as IReleasable;
                if (releasableObject != null && !releasableObject.IsReleased)
                {
                    return false;
                }

                var identifObject = obj as IIdentifiable;
                var identifSelectedItem = selectedItem.DataObject as IIdentifiable;
                if (identifObject != null && identifSelectedItem != null)
                {
                    if ((identifObject.Guid == identifSelectedItem.Guid)
                        || selectedItem.HasChildForObject(identifObject)
                        || this.CheckIfChild(selectedItem, identifObject))
                    {
                        return false;
                    }
                }
            }

            var rootManageReleasedProject = selectedItem as ManageReleasedProjectsTreeItem;
            if (rootManageReleasedProject != null)
            {
                var clipboardObjectTypes = ClipboardManager.Instance.PeekTypes.ToList();
                if (clipboardObjectTypes.Count > 0)
                {
                    return clipboardObjectTypes.All(type => type == typeof(ProjectFolder) || type == typeof(Project));
                }
            }

            return false;
        }

        /// <summary>
        /// Performs the logic associated with the Paste command.
        /// </summary>
        /// <param name="parameter">The command parameter.</param>
        private void ExecutePaste(object parameter)
        {
            if (this.SelectedTreeItems == null
               || this.SelectedTreeItems.Count != 1)
            {
                return;
            }

            // The selected tree item is the target for the paste operation
            var selectedItem = this.SelectedTreeItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null
                || selectedItem.ReadOnly
                || ClipboardManager.Instance.Operation == ClipboardOperation.Copy)
            {
                return;
            }

            try
            {
                var pasteRecipient =
                    ((ManageReleasedProjectsTreeItem)selectedItem).Uid == ProjectsTreeItemUid.ManageReleasedProjectsRoot
                        ? selectedItem
                        : selectedItem.DataObject;

                var pastedObjects = ClipboardManager.Instance.Paste(pasteRecipient, this.dataManager).ToList();

                // Display errors appeared during the paste process.
                string errorMessage = ClipboardManager.Instance.GetErrorMessageToDisplay(pastedObjects);
                if (!string.IsNullOrWhiteSpace(errorMessage))
                {
                    this.windowService.MessageDialogService.Show(errorMessage, MessageDialogType.Error);
                }

                foreach (var item in pastedObjects.Where(o => o.Error == null))
                {
                    // Handle changes to Project Folders.
                    var changedFolder = item.Entity as ProjectFolder;
                    if (changedFolder != null)
                    {
                        this.HandleProjectFolderChange(changedFolder, pasteRecipient as ProjectFolder, EntityChangeType.EntityMoved);
                    }

                    // Handle changes to Projects.
                    var changedProject = item.Entity as Project;
                    if (changedProject != null)
                    {
                        this.HandleProjectChange(changedProject, pasteRecipient as ProjectFolder, EntityChangeType.EntityMoved);
                    }
                }

                return;
            }
            catch (ZPKException ex)
            {
                log.ErrorException("Caught an exception during Paste operation.", ex);
                this.WindowService.MessageDialogService.Show(ex);
            }
        }

        /// <summary>
        /// Determines whether a project folder can be created.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        /// true if a project folder can be created at this time; otherwise false.
        /// </returns>
        private bool CanCreateProjectFolder(object parameter)
        {
            if (this.SelectedTreeItems == null
               || this.SelectedTreeItems.Count != 1)
            {
                return false;
            }

            var selectedItem = this.SelectedTreeItems.FirstOrDefault() as TreeViewDataItem;
            return selectedItem != null && !selectedItem.ReadOnly;
        }

        /// <summary>
        /// Contains the logic associated with the Create Project Folder command.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void CreateProjectFolder(object parameter)
        {
            if (this.SelectedTreeItems == null
               || this.SelectedTreeItems.Count != 1)
            {
                return;
            }

            var selectedItem = this.SelectedTreeItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null || selectedItem.ReadOnly)
            {
                return;
            }

            // Determine the parent folder for the folder that will be created.
            ProjectFolder parent = selectedItem.DataObject as ProjectFolder;
            if (parent == null)
            {
                // The selected item may be a project in a sub-folder so look at its parent to see if it is a folder, and if it is it will become the parent
                // for the new folder.                
                if (selectedItem.Parent != null && !selectedItem.Parent.ReadOnly)
                {
                    parent = selectedItem.Parent.DataObject as ProjectFolder;
                }
            }

            ProjectFolderViewModel viewModel = new ProjectFolderViewModel(this.messenger, this.WindowService);
            viewModel.Title = LocalizedResources.General_CreateFolder;
            viewModel.DataSourceManager = this.dataManager;
            viewModel.InitializeForCreation(parent, true);

            this.WindowService.ShowViewInDialog(viewModel);
        }

        /// <summary>
        /// Determines whether the Un-release Project command can be executed.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        /// true if the command can be executed; otherwise, false.
        /// </returns>
        private bool CanUnreleaseProject(object parameter)
        {
            if (!ZPKTool.Business.SecurityManager.Instance.CurrentUserHasRight(Right.AdminTasks)
               || this.SelectedTreeItems == null
               || this.SelectedTreeItems.Count != 1)
            {
                return false;
            }

            var selectedItem = this.SelectedTreeItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null)
            {
                return false;
            }

            Project project = selectedItem.DataObject as Project;
            if (project == null || !project.IsReleased)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Contains the logic associated with the Un-release Project command.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void UnreleaseProject(object parameter)
        {
            if (this.SelectedTreeItems == null
                || this.SelectedTreeItems.Count != 1)
            {
                return;
            }

            var selectedItem = this.SelectedTreeItems.FirstOrDefault() as TreeViewDataItem;
            Project project = selectedItem.DataObject as Project;

            var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_UnreleaseProject, MessageDialogType.YesNo);
            if (result != MessageDialogResult.Yes)
            {
                return;
            }

            Action<PleaseWaitService.WorkParams> work = (workParams) =>
            {
                ReleaseManager.Instance.UnReleaseProject(project.Guid, this.dataManager);
            };

            Action<PleaseWaitService.WorkParams> workCompleted = (workParams) =>
            {
                if (workParams.Error != null)
                {
                    ZPKException zpkException = workParams.Error as ZPKException;
                    if (zpkException != null &&
                       (zpkException.ErrorCode == DatabaseErrorCode.Deadlock
                        || zpkException.ErrorCode == DatabaseErrorCode.OptimisticConcurrency))
                    {
                        var retry = this.windowService.MessageDialogService.Show(LocalizedResources.General_DeadlockYesNoQuestion, MessageDialogType.YesNo);
                        if (retry == MessageDialogResult.Yes)
                        {
                            work(null);
                        }
                    }
                    else
                    {
                        throw workParams.Error;
                    }
                }
                else
                {
                    // Refresh the manage released projects tree
                    EntityChangedMessage message = new EntityChangedMessage(Notification.MyProjectsEntityChanged);
                    message.Entity = selectedItem.DataObject;
                    message.ChangeType = EntityChangeType.EntityDeleted;
                    message.Parent = selectedItem.Parent.DataObject;
                    HandleManageReleasedProjectsChangeMessage(message);

                    this.windowService.MessageDialogService.Show(LocalizedResources.UnReleaseProject_Success, MessageDialogType.Info);
                }
            };

            this.pleaseWaitService.Show(LocalizedResources.General_WaitUnreleaseProject, work, workCompleted);
        }

        /// <summary>
        /// Determines whether the Un-release Project Folder command can be executed.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        /// true if the command can be executed; otherwise, false.
        /// </returns>
        private bool CanUnreleaseProjectFolder(object parameter)
        {
            if (!ZPKTool.Business.SecurityManager.Instance.CurrentUserHasRight(Right.AdminTasks)
                || this.SelectedTreeItems == null
                || this.SelectedTreeItems.Count != 1)
            {
                return false;
            }

            var selectedItem = this.SelectedTreeItems.FirstOrDefault() as TreeViewDataItem;
            if (selectedItem == null)
            {
                return false;
            }

            ProjectFolder projectFolder = selectedItem.DataObject as ProjectFolder;
            if (projectFolder == null || !projectFolder.IsReleased)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Contains the logic associated with the Un-release Project Folder command.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void UnreleaseProjectFolder(object parameter)
        {
            if (this.SelectedTreeItems == null
               || this.SelectedTreeItems.Count != 1)
            {
                return;
            }

            var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_UnreleaseProjectFolder, MessageDialogType.YesNo);
            if (result != MessageDialogResult.Yes)
            {
                return;
            }

            var selectedItem = this.SelectedTreeItems.FirstOrDefault() as TreeViewDataItem;
            var folderToUnrelease = selectedItem.DataObject as ProjectFolder;
            if (folderToUnrelease == null)
            {
                throw new InvalidOperationException("The folder to un-release was null.");
            }

            Action<PleaseWaitService.WorkParams> work = (workParams) =>
            {
                this.UnreleaseProjectsInFolder(selectedItem);
                this.dataManager.ProjectFolderRepository.DeleteByStoredProcedure(folderToUnrelease);
            };

            Action<PleaseWaitService.WorkParams> workCompleted = (workParams) =>
            {
                if (workParams.Error != null)
                {
                    windowService.MessageDialogService.Show(workParams.Error);
                }
                else
                {
                    // Refresh the manage released projects tree.
                    EntityChangedMessage message = new EntityChangedMessage(Notification.MyProjectsEntityChanged);
                    message.Entity = selectedItem.DataObject;
                    message.ChangeType = EntityChangeType.EntityDeleted;
                    message.Parent = selectedItem.Parent.DataObject;
                    HandleManageReleasedProjectsChangeMessage(message);

                    this.windowService.MessageDialogService.Show(LocalizedResources.UnReleaseProjectFolder_Success, MessageDialogType.Info);
                }
            };

            this.pleaseWaitService.Show(LocalizedResources.General_WaitUnreleaseProjectFolder, work, workCompleted);
        }

        /// <summary>
        /// Recursively un-release all the projects that the selected project folder contains.
        /// </summary>
        /// <param name="folderTreeItem">The parameter.</param>
        private void UnreleaseProjectsInFolder(TreeViewDataItem folderTreeItem)
        {
            foreach (var item in folderTreeItem.Children)
            {
                Project project = item.DataObject as Project;
                if (project != null)
                {
                    ReleaseManager.Instance.UnReleaseProject(project.Guid, this.dataManager);
                }

                this.UnreleaseProjectsInFolder(item);
            }
        }

        #endregion Menu commands handling

        #region EntityChangedMessage handling

        /// <summary>
        /// Dispatches the entity change messages for the manage released projects tree to the appropriate handlers.
        /// </summary>
        /// <param name="message">The message.</param>
        private void HandleEntityChangedMessage(EntityChangedMessage message)
        {
            if (message.Notification == Notifications.Notification.MyProjectsEntityChanged)
            {
                this.HandleManageReleasedProjectsChangeMessage(message);
            }
        }

        /// <summary>
        /// Handles the entities change messages.
        /// </summary>
        /// <param name="message">The message.</param>  
        private void HandleEntitiesChangedMessage(EntitiesChangedMessage message)
        {
            if (message == null)
            {
                return;
            }

            foreach (var msgChange in message.Changes)
            {
                this.HandleEntityChangedMessage(msgChange);
            }
        }

        /// <summary>
        /// Handles an entity change message for Manage Released Projects.
        /// </summary>
        /// <param name="message">The message.</param>
        private void HandleManageReleasedProjectsChangeMessage(EntityChangedMessage message)
        {
            if (message.Notification != Notifications.Notification.MyProjectsEntityChanged)
            {
                return;
            }

            // The EntityDeleted message is handled the same for all entities.
            if (message.ChangeType == EntityChangeType.EntityDeleted)
            {
                ManageReleasedProjectsTreeItem manageReleasedProjectsItem = this.Items.Value.FirstOrDefault(it => it is ManageReleasedProjectsTreeItem) as ManageReleasedProjectsTreeItem;
                var item = manageReleasedProjectsItem.FindItemForDataObject(message.Entity);
                if (item != null)
                {
                    TreeViewDataItem parentItem = null;
                    if (message.Parent == null
                        && (message.Entity is Project || message.Entity is ProjectFolder))
                    {
                        parentItem = manageReleasedProjectsItem;
                    }
                    else
                    {
                        parentItem = manageReleasedProjectsItem.FindItemForDataObject(message.Parent);
                    }

                    if (parentItem != null)
                    {
                        parentItem.Children.Remove(item);
                    }
                }

                return;
            }

            // Handle changes to Project Folders.
            ProjectFolder changedFolder = message.Entity as ProjectFolder;
            if (changedFolder != null)
            {
                HandleProjectFolderChange(changedFolder, message.Parent as ProjectFolder, message.ChangeType);
                return;
            }

            // Handle changes to Projects.
            Project changedProject = message.Entity as Project;
            if (changedProject != null)
            {
                HandleProjectChange(changedProject, message.Parent as ProjectFolder, message.ChangeType);
                return;
            }
        }

        /// <summary>
        /// Handles a change made to a project folder in the manage released projects tree.
        /// </summary>
        /// <param name="changedFolder">The changed folder.</param>
        /// <param name="parentFolder">The parent of the changed folder.</param>
        /// <param name="changeType">Type of the change.</param>
        private void HandleProjectFolderChange(ProjectFolder changedFolder, ProjectFolder parentFolder, EntityChangeType changeType)
        {
            var myProjectFolderItem = this.Items.Value.FirstOrDefault();
            TreeViewDataItem folderItem = null;

            if (changeType == EntityChangeType.EntityCreated
                || changeType == EntityChangeType.EntityMoved)
            {
                folderItem = myProjectFolderItem.FindItemForDataObject(changedFolder);

                if (changeType == EntityChangeType.EntityMoved)
                {
                    // Remove the changed folder item from its old parent.
                    if (folderItem != null && folderItem.Parent != null)
                    {
                        // also we refresh the node in order to visually remove the pasted node from his old place
                        TreeViewDataItem parentFolderItem = folderItem.Parent;
                        parentFolderItem.Children.Remove(folderItem);
                    }
                }

                TreeViewDataItem parentItem = null;
                if (parentFolder == null)
                {
                    parentItem = myProjectFolderItem;
                }
                else
                {
                    parentItem = myProjectFolderItem.FindItemForDataObject(parentFolder);
                }

                if (folderItem != null)
                {
                    parentItem.Children.Add(folderItem);
                    folderItem.Select();
                }
                else
                {
                    var createdFolderTreeItem = new ManageReleasedProjectsTreeItem(changedFolder);
                    createdFolderTreeItem.Label = changedFolder.Name;
                    createdFolderTreeItem.IconResourceKey = Images.FolderIconKey;
                    createdFolderTreeItem.Uid = ProjectsTreeItemUid.ManageReleasedProjectsReleasedFolder;
                    createdFolderTreeItem.AllowMultipleSelection = true;
                    createdFolderTreeItem.AllowChildrenMultipleSelection = true;

                    parentItem.Children.Add(createdFolderTreeItem);
                    createdFolderTreeItem.Select();
                }
            }
            else if (changeType == EntityChangeType.EntityUpdated)
            {
                var item = myProjectFolderItem.FindItemForDataObject(changedFolder);
                this.SelectedTreeItem.Label = ((ProjectFolder)item.DataObject).Name;
            }
        }

        /// <summary>
        /// Handles a change made to a project in the manage released projects tree.
        /// </summary>
        /// <param name="changedProject">The changed project.</param>
        /// <param name="parentFolder">The parent of the changed folder.</param>
        /// <param name="changeType">Type of the change.</param>
        private void HandleProjectChange(Project changedProject, ProjectFolder parentFolder, EntityChangeType changeType)
        {
            var myProjectItem = this.Items.Value.FirstOrDefault();
            TreeViewDataItem projectItem = null;

            if (changeType == EntityChangeType.EntityCreated
                || changeType == EntityChangeType.EntityMoved)
            {
                projectItem = myProjectItem.FindItemForDataObject(changedProject);

                if (changeType == EntityChangeType.EntityMoved)
                {
                    // Remove the changed project item from its old parent.
                    if (projectItem != null && projectItem.Parent != null)
                    {
                        // also we refresh the node in order to visually remove the pasted node from his old place
                        TreeViewDataItem parentProjectItem = projectItem.Parent;
                        parentProjectItem.Children.Remove(projectItem);
                    }
                }

                TreeViewDataItem parentItem = null;
                if (parentFolder == null)
                {
                    parentItem = myProjectItem;
                }
                else
                {
                    parentItem = myProjectItem.FindItemForDataObject(parentFolder);
                }

                if (projectItem != null)
                {
                    parentItem.Children.Add(projectItem);
                    projectItem.Select();
                }
            }
        }

        #endregion EntityChangedMessage handling

        #region ManageReleasedProjectsTree events handling

        /// <summary>
        /// Handles the PreviewMouseDown event of the Manage Released Projects Tree.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        public void HandleManageReleasedProjectsTreePreviewMouseDown(MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed && e.ClickCount == 1)
            {
                if (this.SelectedTreeItem != null)
                {
                    this.dragStartPosition = e.GetPosition(null);
                }
            }
            else if (e.ClickCount == 2)
            {
                this.dragStartPosition = null;
            }

            DependencyObject source = e.OriginalSource as DependencyObject;
            if (source != null)
            {
                ToggleButton expander = UIHelper.FindParent<ToggleButton>(source);
                if (expander != null && expander.Name == "Expander")
                {
                    return;
                }
            }

            // Find the clicked tree item.
            TreeViewDataItem clickedItem = null;
            TreeViewItem parentItem = UIHelper.FindParent<TreeViewItem>(e.OriginalSource as DependencyObject);
            if (parentItem != null)
            {
                clickedItem = parentItem.DataContext as TreeViewDataItem;
            }
        }

        /// <summary>
        /// Handles the PreviewMouseUp event of the Manage Released Projects Tree.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        public void HandleManageReleasedProjectsTreePreviewMouseUp(MouseButtonEventArgs e)
        {
            this.dragStartPosition = null;
        }

        /// <summary>
        /// Handles the PreviewMouseMove event of the Manage Released Projects Tree.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.Input.MouseEventArgs"/> instance containing the event data.</param>
        public void HandleManageReleasedProjectsTreePreviewMouseMove(MouseEventArgs e)
        {
            this.TryStartDragOperation(e);
        }

        #endregion ManageReleasedProjectsTree events handling

        #region Drag and Drop

        /// <summary>
        /// Starts the drag operation for the tree item under the mouse, if it is possible or necessary.
        /// </summary>
        /// <param name="mouseMoveArgs">The <see cref="System.Windows.Input.MouseEventArgs"/> instance containing the event data from the PreviewMouseMove event.</param>
        private void TryStartDragOperation(MouseEventArgs mouseMoveArgs)
        {
            // Check for the conditions necessary in order to start dragging the current manage released projects tree item.

            // The left button must be pressed.
            if (mouseMoveArgs.LeftButton != MouseButtonState.Pressed)
            {
                return;
            }

            if (!this.dragStartPosition.HasValue)
            {
                return;
            }

            // The mouse must over a tree item, not other part of the tree view like a scroll bar.
            TreeViewItem parentItem = UIHelper.FindParent<TreeViewItem>(mouseMoveArgs.OriginalSource as DependencyObject);
            if (parentItem == null)
            {
                return;
            }

            // The selected item must be valid, not be read-only and its children must be loaded.
            if (this.SelectedTreeItem == null
                || this.SelectedTreeItem.DataObject == null
                || !this.SelectedTreeItem.AreChildrenLoaded)
            {
                return;
            }

            // A minimum movement distance is required before starting the drag in order to avoid starting it when moving the mouse a few pixels during double click.
            Point currentMousePosition = mouseMoveArgs.GetPosition(null);
            double moveDeltaX = Math.Abs(currentMousePosition.X - this.dragStartPosition.Value.X);
            double moveDeltaY = Math.Abs(currentMousePosition.Y - this.dragStartPosition.Value.Y);
            if (moveDeltaX <= SystemParameters.MinimumHorizontalDragDistance + 8d
                && moveDeltaY <= SystemParameters.MinimumVerticalDragDistance + 8d)
            {
                return;
            }

            if (this.SelectedTreeItem.DataObject is ProjectFolder
                || this.SelectedTreeItem.DataObject is Project)
            {
                // Start the drag & drop action.
                DragAndDropManageReleasedProjectsTreeItemData dragData = new DragAndDropManageReleasedProjectsTreeItemData(this.SelectedTreeItem);
                DataObject data = new DataObject(typeof(DragAndDropData), dragData);
                DragDrop.DoDragDrop(parentItem, data, DragDropEffects.All);
            }
        }

        /// <summary>
        /// Handles the manage released projects tree DragEnter event.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        private void HandleManageReleasedProjectsTreeDragEnter(DragEventArgs e)
        {
            DragDropEffects operationEffects = DragDropEffects.None;
            bool eventHandled = true;

            DragAndDropData dataItem = e.Data.GetData(typeof(DragAndDropData)) as DragAndDropData;
            if (dataItem != null)
            {
                bool dropAccepted = this.ValidateDataDragOperation(dataItem, e);
                if (dropAccepted)
                {
                    operationEffects = DragDropEffects.Move;
                }
            }

            e.Effects = operationEffects;
            e.Handled = eventHandled;
        }

        /// <summary>
        /// Handles the projects tree DragOver event.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        private void HandleManageReleasedProjectsTreeDragOver(DragEventArgs e)
        {
            DragDropEffects operationEffects = DragDropEffects.None;

            DragAndDropData dataItem = e.Data.GetData(typeof(DragAndDropData)) as DragAndDropData;
            if (dataItem != null)
            {
                bool dropAccepted = this.ValidateDataDragOperation(dataItem, e);
                if (dropAccepted)
                {
                    operationEffects = DragDropEffects.Move;
                }
            }

            e.Effects = operationEffects;
            e.Handled = true;
        }

        /// <summary>
        /// Handles the projects tree DragLeave event.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        private void HandleManageReleasedProjectsTreeDragLeave(DragEventArgs e)
        {
            e.Effects = DragDropEffects.None;
            e.Handled = true;
        }

        /// <summary>
        /// Handles the manage released projects tree QueryContinueDrag event.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.QueryContinueDragEventArgs"/> instance containing the event data.</param>
        private void HandleManageReleasedProjectsTreeQueryContinueDrag(QueryContinueDragEventArgs e)
        {
            DragAndDropHelper.QueryContinueDrag(e);
        }

        /// <summary>
        /// Handles the Drop of an item on the Manage Released Projects Tree (the Drop event).
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        private void HandleManageReleasedProjectsTreeTreeDrop(DragEventArgs e)
        {
            e.Effects = DragDropEffects.None;
            e.Handled = true;

            // Find the tree view item that received the drop, and its corresponding Manage Released Projects Tree item.
            TreeViewItem targetTreeViewItem = UIHelper.FindParent<TreeViewItem>(e.OriginalSource as DependencyObject);
            if (targetTreeViewItem == null)
            {
                return;
            }

            TreeViewDataItem targetTreeItem = targetTreeViewItem.DataContext as TreeViewDataItem;
            if (targetTreeItem == null || targetTreeItem.ReadOnly)
            {
                return;
            }

            if (targetTreeItem == this.SelectedTreeItem)
            {
                return;
            }

            DragAndDropData droppedData = (DragAndDropData)e.Data.GetData(typeof(DragAndDropData));
            if (droppedData != null)
            {
                this.HandleDataObjectDrop(targetTreeItem, droppedData);
            }
        }

        /// <summary>
        /// Validates the drag operation of a generic a data item.
        /// </summary>
        /// <param name="dataItem">The data item.</param>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the drag event data.</param>
        /// <returns>True if the drag operation is valid (the data item is accepted); otherwise, false.</returns>
        private bool ValidateDataDragOperation(DragAndDropData dataItem, DragEventArgs e)
        {
            if (dataItem == null || dataItem.DataObject == null)
            {
                return false;
            }

            if (!(dataItem is DragAndDropManageReleasedProjectsTreeItemData))
            {
                return false;
            }

            TreeViewItem treeItem = UIHelper.FindParent<TreeViewItem>(e.OriginalSource as DependencyObject);
            if (treeItem == null)
            {
                return false;
            }

            TreeViewDataItem manageReleasedProjectsTreeItem = treeItem.DataContext as TreeViewDataItem;
            if (manageReleasedProjectsTreeItem == null || manageReleasedProjectsTreeItem.ReadOnly)
            {
                return false;
            }

            if (manageReleasedProjectsTreeItem.DataObject != null)
            {
                // Check if the tree item over which the drag is accepts the dragged data object.
                return ClipboardManager.Instance.CheckIfEntityAcceptsObject(
                    manageReleasedProjectsTreeItem.DataObject.GetType(),
                    dataItem.DataObject.GetType());
            }
            else
            {
                Type dataObjType = dataItem.DataObject.GetType();
                if (manageReleasedProjectsTreeItem is ManageReleasedProjectsTreeItem)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Handles the drop of a data object on a manage released projects tree item.
        /// </summary>
        /// <param name="targetTreeItem">The target tree item.</param>
        /// <param name="droppedData">The dropped data.</param>
        private void HandleDataObjectDrop(TreeViewDataItem targetTreeItem, DragAndDropData droppedData)
        {
            if (targetTreeItem == null || droppedData == null)
            {
                return;
            }

            // Determine the data object that will receive the dropped data.
            object recipient = null;
            if ((targetTreeItem as ManageReleasedProjectsTreeItem).Uid == ProjectsTreeItemUid.ManageReleasedProjectsRoot)
            {
                recipient = targetTreeItem;
            }
            else
            {
                recipient = targetTreeItem.DataObject;
            }

            if (recipient == null)
            {
                log.Error("Failed to determine the recipient data object for a Manage Released Projects Tree data drop operation.");
                return;
            }

            // Add the dropped object to the recipient through the Cut->Paste system.
            // Whether the dropped item is accepted by the recipient is decided when the drag entered the item so it is not necessary to check that here.              
            ClipboardManager.Instance.Cut(droppedData.DataObject, droppedData.DataObjectContext);

            var pastedObject = ClipboardManager.Instance.Paste(recipient, this.dataManager).First();
            if (pastedObject.Error != null)
            {
                this.windowService.MessageDialogService.Show(pastedObject.Error);
            }
            else
            {
                // Handle changes to Project Folders.
                var changedFolder = pastedObject.Entity as ProjectFolder;
                if (changedFolder != null)
                {
                    HandleProjectFolderChange(changedFolder, recipient as ProjectFolder, EntityChangeType.EntityMoved);
                    return;
                }

                // Handle changes to Projects.
                var changedProject = pastedObject.Entity as Project;
                if (changedProject != null)
                {
                    HandleProjectChange(changedProject, recipient as ProjectFolder, EntityChangeType.EntityMoved);
                    return;
                }
            }
        }

        #endregion Drag and Drop

        #region Helpers

        /// <summary>
        /// Searches in the entire Manage Released Projects Tree for the item corresponding to a specified data object.
        /// </summary>
        /// <param name="dataObject">The data object.</param>
        /// <returns>The item or null if not found.</returns>
        private TreeViewDataItem FindItemOfDataObject(object dataObject)
        {
            TreeViewDataItem result = null;

            // Search in Manage Released Projects first because it is most often the target for searches.
            var manageReleasedProjectsItem = this.Items.Value.FirstOrDefault(it => it is ManageReleasedProjectsTreeItem);
            result = manageReleasedProjectsItem.FindItemForDataObject(dataObject);

            // Search in the other tree items if  the Manage Released Projects search yielded no result.
            if (result == null)
            {
                foreach (var item in this.Items.Value.Where(it => !(it is ManageReleasedProjectsTreeItem)))
                {
                    result = item.FindItemForDataObject(dataObject);
                    if (result != null)
                    {
                        break;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Searches for the visual container (TreeViewItem) of a specified projects tree item in the manage released projects tree.
        /// </summary>
        /// <param name="manageReleasedProjectsTree">The manage released projects tree.</param>
        /// <param name="item">The tree view data item.</param>
        /// <returns>
        /// The container of the item or null if it is not found.
        /// </returns>
        private TreeViewItem FindContainerOfProjectsTreeItem(TreeView manageReleasedProjectsTree, TreeViewDataItem item)
        {
            // List the parents of the items in hierarchical order up to the root.
            List<TreeViewDataItem> parentHierarchy = new List<TreeViewDataItem>();
            TreeViewDataItem walker = item.Parent;
            while (walker != null)
            {
                parentHierarchy.Insert(0, walker);
                walker = walker.Parent;
            }

            // Search the item's container from the root down by finding the container of each parent and continuing the search in that container.
            ItemsControl root = manageReleasedProjectsTree;
            while (parentHierarchy.Count > 0 && root != null)
            {
                root = root.ItemContainerGenerator.ContainerFromItem(parentHierarchy[0]) as ItemsControl;
                parentHierarchy.RemoveAt(0);
            }

            if (root != null)
            {
                return root.ItemContainerGenerator.ContainerFromItem(item) as TreeViewItem;
            }

            return null;
        }

        /// <summary>
        /// Checks if a <see cref="TreeViewDataItem"/> node is the child of an object.
        /// </summary>
        /// <param name="childTreeNode">The tree node to check.</param>
        /// <param name="parent">The parent object to check with.</param>
        /// <returns>True if the object is the parent of the tree node or false otherwise.</returns>
        private bool CheckIfChild(TreeViewDataItem childTreeNode, IIdentifiable parent)
        {
            var treeNode = childTreeNode;
            var isChild = false;
            while (treeNode.Parent != null)
            {
                var item = treeNode.Parent.DataObject as IIdentifiable;
                if (item != null && item.Guid == parent.Guid)
                {
                    return true;
                }

                treeNode = treeNode.Parent;
            }

            return isChild;
        }

        #endregion Helpers

        #region DragAndDropManageReleasedProjectsTreeItemData inner class

        /// <summary>
        /// The drag and drop data for an item dragged to the Manage Released Projects Tree.
        /// </summary>
        private class DragAndDropManageReleasedProjectsTreeItemData : DragAndDropData
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="DragAndDropManageReleasedProjectsTreeItemData"/> class.
            /// </summary>
            public DragAndDropManageReleasedProjectsTreeItemData()
                : base()
            {
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="DragAndDropManageReleasedProjectsTreeItemData"/> class.
            /// </summary>
            /// <param name="draggedItem">The dragged item.</param>
            public DragAndDropManageReleasedProjectsTreeItemData(TreeViewDataItem draggedItem)
            {
                if (draggedItem == null)
                {
                    throw new ArgumentNullException("draggedItem", "The dragged item was null.");
                }

                this.DraggedTreeItem = draggedItem;
                this.DataObject = draggedItem.DataObject;
                this.DataObjectContext = draggedItem.DataObjectSource;
            }

            /// <summary>
            /// Gets the dragged manage released projects tree item.
            /// </summary>
            public TreeViewDataItem DraggedTreeItem { get; private set; }
        }

        #endregion DragAndDropManageReleasedProjectsTreeItemData inner class
    }
}