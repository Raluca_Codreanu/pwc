﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Class used to add items in the parts view model, where a part object is needed and the cost obtained from calculation also.
    /// </summary>    
    public class PartItem
    {
        // @coderev: Add a public constructor that takes the part as parameter and call CalculateCost in the constructor.
        // @coderev: Make CalculateCost private. Derive the class from ObservableObject and send notification when PartCost changes.

        /// <summary>
        /// Gets the part cost.
        /// </summary>
        public decimal? PartCost { get; private set; }

        /// <summary>
        /// Gets or sets the part.
        /// </summary>        
        public Part Part { get; set; }

        /// <summary>
        /// Calculates the cost of the part set
        /// </summary>
        /// <param name="parentProject">The parent project.</param>
        /// <param name="isInViewerMode">indicates whether this instance is used in the model viewer</param>
        public void CalculateCost(Project parentProject, bool isInViewerMode)
        {
            if (this.Part != null)
            {
                PartCostCalculationParameters partCostCalculationParameters = null;
                if (parentProject != null)
                {
                    partCostCalculationParameters = CostCalculationHelper.CreatePartParamsFromProject(parentProject);
                }
                else if (isInViewerMode)
                {
                    var modelBrowserHelperService = new ModelBrowserHelperService();
                    partCostCalculationParameters = modelBrowserHelperService.GetPartCostCalculationParameters();
                }

                if (partCostCalculationParameters != null)
                {
                    ICostCalculator calculator = CostCalculatorFactory.GetCalculator(this.Part.CalculationVariant);
                    var result = calculator.CalculatePartCost(this.Part, partCostCalculationParameters);
                    this.PartCost = result.TotalCost;
                }
            }
        }
    }
}
