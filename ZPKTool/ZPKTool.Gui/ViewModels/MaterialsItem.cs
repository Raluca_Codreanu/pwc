﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using ZPKTool.Data;
using ZPKTool.Gui.Resources;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Class used in the <see cref="MaterialsViewModel"/> as a wrapper for the its items.
    /// It supplementary provides the entity icon an type name.
    /// </summary>
    public class MaterialsItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MaterialsItem"/> class.
        /// </summary>
        /// <param name="entity">The entity object.</param>
        public MaterialsItem(object entity)
        {
            this.Entity = entity;
            var entityType = entity.GetType();
            this.TypeName = this.GetEntityTypeName(entityType);
            this.Icon = this.GetEntityIcon(entityType);
        }

        /// <summary>
        /// Gets the entity object
        /// </summary>
        public object Entity { get; private set; }

        /// <summary>
        /// Gets the name of the entity type
        /// </summary>
        public string TypeName { get; private set; }

        /// <summary>
        /// Gets the icon of the entity
        /// </summary>
        public ImageSource Icon { get; private set; }

        /// <summary>
        /// Gets the icon for an entity type
        /// </summary>
        /// <param name="entityType">The type of the entity</param>
        /// <returns>The icon for the entity type provided.</returns>
        private ImageSource GetEntityIcon(Type entityType)
        {
            if (entityType == typeof(RawMaterial))
            {
                return Images.RawMaterialIcon;
            }
            else if (entityType == typeof(Commodity))
            {
                return Images.CommodityIcon;
            }
            else if (entityType == typeof(RawPart))
            {
                return Images.RawPartIcon;
            }

            return null;
        }

        /// <summary>
        /// Gets the name for an entity type
        /// </summary>
        /// <param name="entityType">The type of the entity</param>
        /// <returns>The name of the entity type provided.</returns>
        private string GetEntityTypeName(Type entityType)
        {
            if (entityType == typeof(RawMaterial))
            {
                return LocalizedResources.General_RawMaterial;
            }
            else if (entityType == typeof(Commodity))
            {
                return LocalizedResources.General_Commodity;
            }
            else if (entityType == typeof(RawPart))
            {
                return LocalizedResources.General_RawPart;
            }

            return string.Empty;
        }
    }
}
