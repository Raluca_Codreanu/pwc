﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the IMyView view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ProjectViewModel : ViewModel<Project, IDataSourceManager>
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The PleaseWaitService reference.
        /// </summary>
        private IPleaseWaitService pleaseWaitService;

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The list of users to display on the UI.
        /// </summary>
        private Collection<User> users;

        /// <summary>
        /// A weak event listener for changes in the "DisplayVersionAndTimestamp" application setting.
        /// </summary>
        private WeakEventListener<PropertyChangedEventArgs> timestampVisibilitySettingChangedListener;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        /// <summary>
        /// The clone manager.
        /// </summary>
        private CloneManager cloneManager;

        /// <summary>
        /// The project currencies.
        /// </summary>
        private ObservableCollection<Currency> currencies;

        /// <summary>
        /// The outdated currencies.
        /// </summary>
        private ObservableCollection<CurrencyComparisonData> outdatedCurrencies;

        /// <summary>
        /// The UnitsAdapter handler, used to perform certain operations when some UnitsAdapter properties are updated.
        /// </summary>
        private UnitsAdapterUpdateHandler unitsAdapterHandler;

        /// <summary>
        /// A value indicating whether the list of currencies has been updated or not. This is set to true if the user presses the Update button for currencies.
        /// </summary>
        private bool areCurrenciesUpdated;

        #endregion Attributes

        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectViewModel"/> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        /// <param name="pleaseWaitService">The please wait service.</param>
        /// <param name="messenger">The messenger service.</param>
        /// <param name="supplierViewModel">The supplier view model.</param>
        /// <param name="documentsViewModel">The documents view model.</param>
        /// <param name="overheadSettingsViewModel">The overhead settings view model.</param>
        /// <param name="mediaViewModel">The media view model.</param>
        /// <param name="unitsService">The units service.</param>
        [ImportingConstructor]
        public ProjectViewModel(
            IWindowService windowService,
            IPleaseWaitService pleaseWaitService,
            IMessenger messenger,
            SupplierViewModel supplierViewModel,
            EntityDocumentsViewModel documentsViewModel,
            OverheadSettingsViewModel overheadSettingsViewModel,
            MediaViewModel mediaViewModel,
            IUnitsService unitsService)
        {
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("supplierViewModel", supplierViewModel);
            Argument.IsNotNull("documentsViewModel", documentsViewModel);
            Argument.IsNotNull("overheadSettingsViewModel", overheadSettingsViewModel);
            Argument.IsNotNull("mediaViewModel", mediaViewModel);
            Argument.IsNotNull("unitsService", unitsService);

            this.windowService = windowService;
            this.pleaseWaitService = pleaseWaitService;
            this.messenger = messenger;
            this.SupplierViewModel = supplierViewModel;
            this.SupplierViewModel.IsChild = true;
            this.DocumentsViewModel = documentsViewModel;
            this.OverheadSettingsViewModel = overheadSettingsViewModel;
            this.OverheadSettingsViewModel.ShowSaveControls = false;
            this.OverheadSettingsViewModel.IsChild = true;
            this.MediaViewModel = mediaViewModel;
            this.MediaViewModel.Mode = MediaControlMode.MultipleImagesOrVideo;
            this.MediaViewModel.IsChild = true;
            this.unitsService = unitsService;

            this.StartDate.Value = DateTime.Now;
            this.EndDate.Value = this.StartDate.Value;
            this.Status.Value = ProjectStatus.Working;
            this.IsChanged = false;

            this.InitializeUndoManager();

            this.Currencies = new ObservableCollection<Currency>();

            // Last modification date
            this.TimestampVisibility.Value = UserSettingsManager.Instance.DisplayVersionAndTimestamp ? Visibility.Visible : Visibility.Collapsed;
            timestampVisibilitySettingChangedListener = new WeakEventListener<PropertyChangedEventArgs>((s, e) =>
            {
                this.TimestampVisibility.Value = UserSettingsManager.Instance.DisplayVersionAndTimestamp ? Visibility.Visible : Visibility.Collapsed;
            });
            PropertyChangedEventManager.AddListener(UserSettingsManager.Instance, timestampVisibilitySettingChangedListener, ReflectionUtils.GetPropertyName(() => UserSettingsManager.Instance.DisplayVersionAndTimestamp));

            this.messenger.Register<NotificationMessage>(this.NotificationsHandler);

            this.InitializeCommands();
            this.YearlyProductionQuantity.ValueChanged += (s, e) => this.CalculateLifetimeProductionQuantity();
            this.LifeTime.ValueChanged += (s, e) => this.CalculateLifetimeProductionQuantity();

            this.OutdatedCurrencies = new ObservableCollection<CurrencyComparisonData>();
        }

        #region Commands

        /// <summary>
        /// Gets the command that updates the currencies from the project with the master data currencies.
        /// </summary>
        public ICommand UpdateCurrenciesCommand { get; private set; }

        /// <summary>
        /// Gets or sets the command that save all changes in the nested view models back into their models.
        /// This command aggregates the SaveToModel commands of the nested view models.
        /// </summary>
        private CompositeCommand SaveNestedViewModels { get; set; }

        /// <summary>
        /// Gets or sets the command that cancels all changes of the nested view models.
        /// This command aggregates the Cancel commands of the nested view models.
        /// </summary>
        private CompositeCommand CancelNestedViewModels { get; set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets the supplier view model.
        /// </summary>
        public SupplierViewModel SupplierViewModel { get; private set; }

        /// <summary>
        /// Gets the documents view model.
        /// </summary>
        public EntityDocumentsViewModel DocumentsViewModel { get; private set; }

        /// <summary>
        /// Gets the overhead settings view model.
        /// </summary>
        public OverheadSettingsViewModel OverheadSettingsViewModel { get; private set; }

        /// <summary>
        /// Gets the media view model.
        /// </summary>
        public MediaViewModel MediaViewModel { get; private set; }

        /// <summary>
        /// Gets the project name.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Name", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Name")]
        public DataProperty<string> Name { get; private set; }

        /// <summary>
        /// Gets the project number.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Number")]
        public DataProperty<string> Number { get; private set; }

        /// <summary>
        /// Gets the project's start date.
        /// </summary>
        [UndoableProperty]
        [ExposesModelProperty("StartDate")]
        public DataProperty<DateTime?> StartDate { get; private set; }

        /// <summary>
        /// Gets the project's end date.
        /// </summary>
        [UndoableProperty]
        [ExposesModelProperty("EndDate")]
        public DataProperty<DateTime?> EndDate { get; private set; }

        /// <summary>
        /// Gets the project's status.
        /// </summary>
        [UndoableProperty]
        [ExposesModelProperty("Status")]
        public DataProperty<ProjectStatus?> Status { get; private set; }

        /// <summary>
        /// Gets the project partner.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Partner")]
        public DataProperty<string> Partner { get; private set; }

        /// <summary>
        /// Gets the project leader.
        /// </summary>
        [UndoableProperty]
        [ExposesModelProperty("ProjectLeader")]
        public DataProperty<User> ProjectLeader { get; private set; }

        /// <summary>
        /// Gets the project's responsible calculator.
        /// </summary>
        [UndoableProperty]
        [ExposesModelProperty("ResponsibleCalculator")]
        public DataProperty<User> ResponsibleCalculator { get; private set; }

        /// <summary>
        /// Gets the project's version.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Version")]
        public DataProperty<decimal?> Version { get; private set; }

        /// <summary>
        /// Gets the project's version date.
        /// </summary>
        [ExposesModelProperty("VersionDate")]
        [UndoableProperty]
        public DataProperty<DateTime?> VersionDate { get; private set; }

        /// <summary>
        /// Gets the name of the product.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("ProductName")]
        public DataProperty<string> ProductName { get; private set; }

        /// <summary>
        /// Gets the product number.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("ProductNumber")]
        public DataProperty<string> ProductNumber { get; private set; }

        /// <summary>
        /// Gets the yearly production quantity of the product.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("YearlyProductionQuantity")]
        public DataProperty<int?> YearlyProductionQuantity { get; private set; }

        /// <summary>
        /// Gets the product lifetime.
        /// </summary>
        /// <value>The lifetime.</value>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("LifeTime")]
        public DataProperty<int?> LifeTime { get; private set; }

        /// <summary>
        /// Gets the product description.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("ProductDescription")]
        public DataProperty<string> ProductDescription { get; private set; }

        /// <summary>
        /// Gets the supplier description.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("CustomerDescription")]
        public DataProperty<string> SupplierDescription { get; private set; }

        /// <summary>
        /// Gets the remarks.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Remarks")]
        public DataProperty<string> Remarks { get; private set; }

        /// <summary>
        /// Gets the timestamp when the last change in project has occurred.
        /// </summary>
        /// <value>The last change timestamp.</value>
        [ExposesModelProperty("LastChangeTimestamp")]
        public DataProperty<DateTime?> LastChangeTimestamp { get; private set; }

        /// <summary>
        /// Gets the project's depreciation period.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("DepreciationPeriod")]
        public DataProperty<int?> DepreciationPeriod { get; private set; }

        /// <summary>
        /// Gets the project's depreciation rate.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("DepreciationRate")]
        public DataProperty<decimal?> DepreciationRate { get; private set; }

        /// <summary>
        /// Gets the project's logistic cost ratio.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("LogisticCostRatio")]
        public DataProperty<decimal?> LogisticCostRatio { get; private set; }

        /// <summary>
        /// Gets the project's Shifts Per Week.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("ShiftsPerWeek")]
        public DataProperty<decimal?> ShiftsPerWeek { get; private set; }

        /// <summary>
        /// Gets the project's Hours Per Shift.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("HoursPerShift")]
        public DataProperty<decimal?> HoursPerShift { get; private set; }

        /// <summary>
        /// Gets the project's Production Weeks Per Year.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("ProductionWeeksPerYear")]
        public DataProperty<decimal?> ProductionWeeksPerYear { get; private set; }

        /// <summary>
        /// Gets the project's Production Days Per Week.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("ProductionDaysPerWeek")]
        public DataProperty<decimal?> ProductionDaysPerWeek { get; private set; }

        /// <summary>
        /// Gets the visibility of the project's last change date/time.
        /// </summary>
        public VMProperty<Visibility> TimestampVisibility { get; private set; }

        /// <summary>
        /// Gets the product's lifetime production quantity.
        /// </summary>
        public VMProperty<decimal?> LifetimeProductionQuantity { get; private set; }

        /// <summary>
        /// Gets the list of users to display on the UI.
        /// </summary>
        public Collection<User> Users
        {
            get { return this.users; }
            private set { this.SetProperty(ref this.users, value, () => Users); }
        }

        /// <summary>
        /// Gets the project's Base Currency.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_BaseCurrency", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty]
        [ExposesModelProperty("BaseCurrency")]
        public DataProperty<Currency> BaseCurrency { get; private set; }

        /// <summary>
        /// Gets or sets the currencies.
        /// </summary>
        [UndoableProperty]
        public ObservableCollection<Currency> Currencies
        {
            get
            {
                return this.currencies;
            }

            set
            {
                if (this.currencies != value)
                {
                    this.currencies = value;
                    OnPropertyChanged(() => this.Currencies);
                }
            }
        }

        /// <summary>
        /// Gets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter { get; private set; }

        /// <summary>
        /// Gets or sets the outdated currencies.
        /// </summary>
        [UndoableProperty]
        public ObservableCollection<CurrencyComparisonData> OutdatedCurrencies
        {
            get
            {
                return this.outdatedCurrencies;
            }

            set
            {
                if (this.outdatedCurrencies != value)
                {
                    this.outdatedCurrencies = value;
                    OnPropertyChanged(() => this.OutdatedCurrencies);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating if an update to the project currencies exists.
        /// </summary>
        [UndoableProperty]
        public VMProperty<bool> CurrenciesUpdateExists { get; set; }

        #endregion Properties

        #region Initialization

        /// <summary>
        /// Initializes the view-model for the creation of a new project.
        /// The ModelDataContext property must be set to a valid value before this call.
        /// </summary>
        /// <param name="parentFolder">The parent folder for the project to be created. This instance should belong to the data context set in the ModelDataContext property.</param>
        /// <remarks>
        /// This is a helper method for populating the model with default data for creation. It is not mandatory to use it; you can set the model to a new
        /// instance set up however you want.
        /// </remarks>
        public void InitializeForProjectCreation(ProjectFolder parentFolder)
        {
            this.CheckDataSource();
            this.EditMode = ViewModelEditMode.Create;

            Project newProject = new Project();

            newProject.Status = (short)ProjectStatus.Working;
            newProject.StartDate = DateTime.Now;
            newProject.EndDate = newProject.StartDate;
            newProject.Customer = new Customer();
            newProject.ProjectFolder = parentFolder;
            if (parentFolder != null)
            {
                newProject.IsOffline = parentFolder.IsOffline;
            }

            try
            {
                // For a new project, use the master data overhead settings as defaults
                OverheadSetting overheads = this.DataSourceManager.OverheadSettingsRepository.GetMasterData();
                newProject.OverheadSettings = overheads.Copy();
                newProject.SourceOverheadSettingsLastUpdate = overheads.LastUpdateTime;
            }
            catch (DataAccessException ex)
            {
                log.ErrorException("Failed to load the master data overhead settings.", ex);
                newProject.OverheadSettings = new OverheadSetting();
            }

            try
            {
                BasicSetting settings = this.DataSourceManager.BasicSettingsRepository.GetBasicSettings();
                newProject.DepreciationPeriod = settings.DeprPeriod;
                newProject.DepreciationRate = settings.DeprRate;
                newProject.LogisticCostRatio = settings.LogisticCostRatio;
                newProject.ShiftsPerWeek = settings.ShiftsPerWeek;
                newProject.HoursPerShift = settings.HoursPerShift;
                newProject.ProductionWeeksPerYear = settings.ProdWeeks;
                newProject.ProductionDaysPerWeek = settings.ProdDays;
            }
            catch (DataAccessException ex)
            {
                log.ErrorException("Failed to load the basic settings.", ex);
            }

            // Set the master data flag and the owner at the end so they are applied to all sub-objects.
            newProject.SetIsMasterData(false);
            User crtUser = this.DataSourceManager.UserRepository.GetByKey(SecurityManager.Instance.CurrentUser.Guid);
            newProject.SetOwner(crtUser);

            newProject.ResponsibleCalculator = crtUser;

            // Populate the project currencies list with master data currencies and set base currency as the default currency.
            var baseCurrencies = this.DataSourceManager.CurrencyRepository.GetBaseCurrencies();
            foreach (var currency in baseCurrencies)
            {
                var currencyClone = this.cloneManager.Clone(currency);
                if (currencyClone != null)
                {
                    currencyClone.IsMasterData = false;
                    newProject.Currencies.Add(currencyClone);
                }
            }

            this.BaseCurrency.Value = newProject.Currencies.FirstOrDefault(c => c.IsoCode == Constants.DefaultCurrencyIsoCode);
            this.Model = newProject;
        }

        /// <summary>
        /// Initializes the view-model's commands.
        /// </summary>
        private void InitializeCommands()
        {
            this.UpdateCurrenciesCommand = new DelegateCommand(
                () => this.UpdateCurrencies(),
                () => !this.IsReadOnly);

            this.SaveNestedViewModels = new CompositeCommand();
            this.SaveNestedViewModels.RegisterCommand(this.SupplierViewModel.SaveToModelCommand);
            this.SaveNestedViewModels.RegisterCommand(this.DocumentsViewModel.SaveCommand);
            this.SaveNestedViewModels.RegisterCommand(this.OverheadSettingsViewModel.SaveToModelCommand);

            this.CancelNestedViewModels = new CompositeCommand();
            this.CancelNestedViewModels.RegisterCommand(this.SupplierViewModel.CancelCommand);
            this.CancelNestedViewModels.RegisterCommand(this.DocumentsViewModel.CancelCommand);
            this.CancelNestedViewModels.RegisterCommand(this.OverheadSettingsViewModel.CancelCommand);
            this.CancelNestedViewModels.RegisterCommand(this.MediaViewModel.CancelCommand);
        }

        /// <summary>
        /// Initialize the undo manager.
        /// </summary>
        private void InitializeUndoManager()
        {
            this.MediaViewModel.UndoManager = this.UndoManager;
            this.DocumentsViewModel.UndoManager = this.UndoManager;
            this.SupplierViewModel.UndoManager = this.UndoManager;
            this.OverheadSettingsViewModel.UndoManager = this.UndoManager;
        }

        /// <summary>
        /// Loads the currencies from the model.
        /// </summary>
        private void LoadCurrenciesFromModel()
        {
            var orderedCurrencies = this.Model.Currencies.OrderBy(c => c.Name);

            while (this.Currencies.Count() > 0)
            {
                this.Currencies.RemoveAt(0);
            }

            if (this.IsInViewerMode)
            {
                foreach (var currency in orderedCurrencies)
                {
                    this.Currencies.Add(currency);
                }
            }
            else
            {
                // The currencies need to be cloned or else when updating the view-model currencies the ones from the model will also be modified because they would reference the same currency.
                foreach (var currency in orderedCurrencies)
                {
                    var currencyClone = cloneManager.Clone(currency);
                    this.Currencies.Add(currencyClone);
                }
            }

            var baseCurrencyCode = this.Model.BaseCurrency != null ? this.Model.BaseCurrency.IsoCode : Constants.DefaultCurrencyIsoCode;
            this.BaseCurrency.Value = this.Currencies.FirstOrDefault(c => c.IsoCode == baseCurrencyCode);

            if (!this.IsInViewerMode)
            {
                this.CheckForCurrenciesChanges();
            }

            this.IsChanged = false;
        }

        #endregion Initialization

        /// <summary>
        /// Handles the messages received by the view model
        /// </summary>
        /// <param name="msg">The message received</param>
        private void NotificationsHandler(NotificationMessage msg)
        {
            // If the request for save has been made, check if the command can be executed and send the message back to the overhead settings view-model.
            if (msg.Notification == Notification.RequestUpdateOverheadSettings
                && msg.Sender == this.OverheadSettingsViewModel)
            {
                if (this.SaveCommand.CanExecute(null))
                {
                    this.SaveCommand.Execute(null);

                    // Update the OH on a different thread.
                    Action<PleaseWaitService.WorkParams> work = (workParams) =>
                    {
                        // Update the oh on the current context, the oh will be refreshed later on the projects tree if necessary.
                        // TODO: The OH update loads all assemblies and parts of the project hierarchy in the DataSourceManager, which is probably the project's context
                        // in the project tree; the data loaded by the operation is never used but remains in memory creating a leak.
                        foreach (var part in this.Model.Parts)
                        {
                            this.DataSourceManager.PartRepository.UpdateOverheadSettings(part.Guid, this.Model.OverheadSettings);
                        }

                        foreach (var assembly in this.Model.Assemblies)
                        {
                            this.DataSourceManager.AssemblyRepository.UpdateOverheadSettings(assembly.Guid, this.Model.OverheadSettings);
                        }

                        this.DataSourceManager.SaveChanges();
                    };

                    Action<PleaseWaitService.WorkParams> workCompleted = (workParams) =>
                    {
                        if (workParams.Error != null)
                        {
                            this.windowService.MessageDialogService.Show(workParams.Error);
                        }
                        else
                        {
                            this.OverheadSettingsViewModel.UpdateApplied = true;

                            var message = new EntityChangedMessage(Notification.MyProjectsEntityChanged);
                            message.ChangeType = EntityChangeType.OverheadSettingsUpdated;
                            message.Entity = this.Model;
                            this.messenger.Send(message);
                        }
                    };

                    this.pleaseWaitService.Show(LocalizedResources.General_UpdatingOH, work, workCompleted);
                }
            }
        }

        /// <summary>
        /// Called after the view has been unloaded from the parent.
        /// </summary>
        public override void OnUnloaded()
        {
            base.OnUnloaded();
            this.messenger.Unregister(this);
        }

        /// <summary>
        /// Called when the Model property has changed.
        /// </summary>
        protected override void OnModelChanged()
        {
            this.CheckDataSource();
            this.LoadDataFromModel();

            if (this.IsInViewerMode)
            {
                this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(null);
            }

            this.LoadCurrenciesFromModel();

            // if the screen is read-only, the project leader and the responsible calculator are added to the users list in order to be selected in the combo boxes.
            if (this.IsReadOnly)
            {
                if (this.Users == null)
                {
                    this.Users = new Collection<User>();
                }

                if (this.Model.ProjectLeader != null)
                {
                    this.Users.Add(this.Model.ProjectLeader);
                }

                if (this.Model.ResponsibleCalculator != null && !this.Users.Contains(this.Model.ResponsibleCalculator))
                {
                    this.Users.Add(this.Model.ResponsibleCalculator);
                }
            }
            else
            {
                // else the users are retrieved from the database in order to populate the two combo boxes.
                this.Users = this.DataSourceManager.UserRepository.GetAll(false);

                if (this.EditMode == ViewModelEditMode.Edit)
                {
                    // if the model is in edit mode and the leader does not exist in the users' list (it's a disabled user), add it.
                    var projLeader = this.Model.ProjectLeader;
                    if (this.Model.ProjectLeader != null)
                    {
                        var leaderExists = this.Users.Contains(this.Model.ProjectLeader);
                        if (!leaderExists)
                        {
                            var leader = this.DataSourceManager.UserRepository.GetById(this.Model.ProjectLeader.Guid, true);
                            if (leader != null)
                            {
                                this.Users.Add(leader);
                            }
                        }
                    }

                    // if the model is in edit mode and the responsible calculator does not exist in the users' list (it's a disabled user), add it
                    if (this.Model.ResponsibleCalculator != null)
                    {
                        var responsibleExists = this.Users.Contains(this.Model.ResponsibleCalculator);
                        if (!responsibleExists)
                        {
                            var responsible = this.DataSourceManager.UserRepository.GetById(this.Model.ResponsibleCalculator.Guid, true);
                            if (responsible != null)
                            {
                                this.Users.Add(responsible);
                            }
                        }
                    }
                }
            }

            this.SupplierViewModel.DataSourceManager = this.DataSourceManager;
            this.SupplierViewModel.Model = this.Model.Customer;

            this.OverheadSettingsViewModel.DataSourceManager = this.DataSourceManager;
            this.OverheadSettingsViewModel.Model = this.Model.OverheadSettings;
            this.OverheadSettingsViewModel.CheckForUpdate = true;

            this.MediaViewModel.DataSourceManager = this.DataSourceManager;
            this.MediaViewModel.Model = this.Model;

            this.DocumentsViewModel.ModelDataSourceManager = this.DataSourceManager;
            this.DocumentsViewModel.Model = this.Model;

            this.UndoManager.Reset();
            this.UndoManager.Start();
            this.IsChanged = false;
        }

        /// <summary>
        /// Called when the DataSourceManager changed.
        /// </summary>
        protected override void OnDataSourceManagerChanged()
        {
            base.OnDataSourceManagerChanged();

            this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(this.DataSourceManager);
            this.unitsAdapterHandler = new UnitsAdapterUpdateHandler(this.MeasurementUnitsAdapter);
            this.unitsAdapterHandler.PauseUndoOnUnitsAdapterUpdate(this.UndoManager);
            this.cloneManager = new CloneManager(this.DataSourceManager);
        }

        /// <summary>
        /// Saves all changed back into the model and resets the changed status. This method is executed by the SaveToModelCommand command.
        /// The default implementation saves back into the model all view-model properties decorated with the ExposedModelProperty attribute.
        /// </summary>
        /// <exception cref="InvalidOperationException">The Model property name declared by a view-model property is invalid (a property with that name does not exist on the Model).</exception>
        protected override void SaveToModel()
        {
            base.SaveToModel();
            this.LastChangeTimestamp.Value = DateTime.Now;

            // Sync the model and view-model collections of currencies.
            // Remove from the model the currencies that no longer exist.
            var currenciesToRemove = this.Model.Currencies.Except(this.Model.Currencies.Join(this.Currencies, mc => mc.IsoCode, c => c.IsoCode, (mc, c) => mc)).ToList();
            foreach (var currency in currenciesToRemove)
            {
                this.Model.Currencies.Remove(currency);
                this.DataSourceManager.CurrencyRepository.RemoveAll(currency);
            }

            // Add the new currencies to the model.
            var currenciesToAdd = this.Currencies.Except(this.Currencies.Join(this.Model.Currencies, c => c.IsoCode, mc => mc.IsoCode, (c, mc) => c)).ToList();
            foreach (var currency in currenciesToAdd)
            {
                this.Model.Currencies.Add(currency);
            }

            // Copy the currencies values from the view-model to the model.
            foreach (var currency in this.Currencies)
            {
                var modelCurrency = this.Model.Currencies.FirstOrDefault(c => c.IsSameAs(currency));
                currency.CopyValuesTo(modelCurrency);
            }

            this.Model.BaseCurrency = this.Model.Currencies.FirstOrDefault(c => c.IsSameAs(this.BaseCurrency.Value));
        }

        /// <summary>
        /// Determines whether this instance can perform the save operation. Executed by the SaveCommand.
        /// The default implementation allows the save to be performed if the input is valid.
        /// </summary>
        /// <returns>
        /// true if the save operation can be performed, false otherwise.
        /// </returns>
        protected override bool CanSave()
        {
            // Overhead settings update must be available only when the model can be saved to database
            // Can not call this.SaveNestedViewModels.CanExecute(null) because its contains the Save Command for the Overhead Settings View Model
            // And the the Overhead Settings View Model even if it can not be saved, after update all its values will be replaced
            this.OverheadSettingsViewModel.IsUpdateEnabled = base.CanSave()
                                                            && this.DocumentsViewModel.SaveCommand.CanExecute(null)
                                                            && this.MediaViewModel.SaveCommand.CanExecute(null)
                                                            && this.SupplierViewModel.SaveCommand.CanExecute(null);

            return base.CanSave() && this.SaveNestedViewModels.CanExecute(null);
        }

        /// <summary>
        /// Performs the save operation. Executed by the SaveCommand.
        /// </summary>
        protected override void Save()
        {
            this.CheckModelAndDataSource();

            var change = this.EditMode == ViewModelEditMode.Create ? EntityChangeType.EntityCreated : EntityChangeType.EntityUpdated;
            var oldBaseCurrency = this.Model.BaseCurrency ?? CurrencyConversionManager.DefaultBaseCurrency;

            if (change == EntityChangeType.EntityUpdated
                && !this.BaseCurrency.Value.IsSameAs(oldBaseCurrency))
            {
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.General_BaseCurrencyChanged, LocalizedResources.General_CannotUndo, MessageDialogType.YesNo);
                if (result == MessageDialogResult.No)
                {
                    return;
                }
            }

            using (this.UndoManager.Pause())
            {
                // Save all changes back into the Model objects. The validity check of this operation is performed by the CanSave method.
                this.SaveToModel();
                this.SaveNestedViewModels.Execute(null);

                // Update the project.
                this.DataSourceManager.ProjectRepository.Save(this.Model);
                this.DataSourceManager.SaveChanges();

                // Save Media.
                this.MediaViewModel.SaveCommand.Execute(null);

                // Reset the form changed flag.
                this.IsChanged = false;

                // Notify the other UI components that the project was updated.
                var msg = new EntityChangedMessage(Notification.MyProjectsEntityChanged, this.Model, this.Model.ProjectFolder, change);
                this.messenger.Send(msg);
            }

            this.ConvertProjectHierarchy(oldBaseCurrency);
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Determines whether the Cancel operation can be performed. Executed by the CancelCommand.
        /// </summary>
        /// <returns>
        /// true if the changes can be canceled, false otherwise.
        /// </returns>
        protected override bool CanCancel()
        {
            return base.CanCancel() && this.CancelNestedViewModels.CanExecute(null);
        }

        /// <summary>
        /// Cancels all changes. Executed by the CancelCommand.
        /// </summary>
        protected override void Cancel()
        {
            if (!this.CanCancel())
            {
                return;
            }

            if (this.IsChanged)
            {
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_CancelChanges, MessageDialogType.YesNo);
                if (result != MessageDialogResult.Yes)
                {
                    // Don't cancel the changes and also don't close the view-model.
                    return;
                }
                else
                {
                    using (this.UndoManager.StartBatch())
                    {
                        // Cancel all changes
                        base.Cancel();
                        this.CancelNestedViewModels.Execute(null);

                        if (this.areCurrenciesUpdated)
                        {
                            // Reload the currencies only if they have been updated by the user.
                            this.LoadCurrenciesFromModel();
                            this.areCurrenciesUpdated = false;
                        }
                    }
                }
            }

            // Close the view-model when is displayed in a window.
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Method called just before the screen is unloaded
        /// </summary>
        /// <returns>true if the screen was successfully unloaded, false otherwise</returns>
        public override bool OnUnloading()
        {
            // Do nothing if the view-model is read-only, in viewer mode, it was not changed or the model was deleted.
            if (this.IsReadOnly || this.IsInViewerMode || !this.IsChanged || this.Model.IsDeleted)
            {
                return true;
            }

            if (this.EditMode == ViewModelEditMode.Create)
            {
                // Ask the user to confirm quitting
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_Quit, MessageDialogType.YesNo);
                if (result != MessageDialogResult.Yes)
                {
                    // The user chose to stay on the screen; return false to stop the screen unloading.
                    return false;
                }
                else
                {
                    this.IsChanged = false;
                }
            }
            else if (this.EditMode == ViewModelEditMode.Edit)
            {
                // Ask the user if he wants to save
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_UnsavedDataOnQuit, MessageDialogType.YesNoCancel);
                if (result == MessageDialogResult.Yes)
                {
                    // The user wishes to save but it is not possible because the input is not valid. Return false to stop the view-model from unloading.
                    if (!this.CanSave())
                    {
                        return false;
                    }

                    this.Save();
                }
                else if (result == MessageDialogResult.No)
                {
                    // The user does not want to save.                    
                    this.IsChanged = false;
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Loads the data from model.
        /// </summary>
        /// <param name="model">The model.</param>
        public override void LoadDataFromModel(Project model)
        {
            this.StartDate.ValueChanged -= this.OnStartOrEndDateChanged;
            this.EndDate.ValueChanged -= this.OnStartOrEndDateChanged;

            base.LoadDataFromModel(model);

            this.StartDate.ValueChanged += this.OnStartOrEndDateChanged;
            this.EndDate.ValueChanged += this.OnStartOrEndDateChanged;
        }

        /// <summary>
        /// Called when the Start or End Date have changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="oldValue">The old value.</param>
        private void OnStartOrEndDateChanged(object sender, object oldValue)
        {
            using (this.UndoManager.StartBatch(includePreviousItem: true, reverseUndoOrder: true, navigateToBatchControls: true))
            {
                // Adjust the end date so its not earlier then the start date
                if (!this.IsReadOnly && this.StartDate.Value > this.EndDate.Value)
                {
                    // This call must be async on the dispatcher so the DatePicker bound to EndDate is correctly updated.
                    this.EndDate.Value = this.StartDate.Value;
                }
            }
        }

        /// <summary>
        /// Calculates the lifetime production quantity of the product.
        /// </summary>
        private void CalculateLifetimeProductionQuantity()
        {
            this.LifetimeProductionQuantity.Value = (decimal?)this.YearlyProductionQuantity.Value * (decimal?)this.LifeTime.Value;
        }

        /// <summary>
        /// Updates the project currencies.
        /// </summary>
        private void UpdateCurrencies()
        {
            using (this.UndoManager.StartBatch(navigateToBatchControls: true))
            {
                // If the current base currency is updated, it will be reset to null, so we must restore it.
                Currency currentBaseCurrency = this.BaseCurrency.Value != null ? this.BaseCurrency.Value : null;

                foreach (var outdatedCurrency in this.OutdatedCurrencies.OrderBy(c => c.Name))
                {
                    if (outdatedCurrency.LocalCurrency == null)
                    {
                        var cloneCurrency = this.cloneManager.Clone(outdatedCurrency.MasterDataCurrency);
                        cloneCurrency.IsMasterData = false;
                        this.Currencies.Add(cloneCurrency);
                    }
                    else if (outdatedCurrency.MasterDataCurrency == null)
                    {
                        this.Currencies.Remove(outdatedCurrency.LocalCurrency);
                    }
                    else if (outdatedCurrency.LocalCurrency.ExchangeRate != outdatedCurrency.MasterDataCurrency.ExchangeRate)
                    {
                        // A copy of the outdated local currency is removed from currencies list and added after its exchange rate is updated, 
                        // as a workaround for the undo manager.
                        var currencyToUpdate = this.cloneManager.Clone(outdatedCurrency.LocalCurrency, true);

                        this.Currencies.Remove(outdatedCurrency.LocalCurrency);
                        currencyToUpdate.ExchangeRate = outdatedCurrency.MasterDataCurrency.ExchangeRate;
                        this.Currencies.Add(currencyToUpdate);

                        if (currentBaseCurrency != null && currentBaseCurrency.IsSameAs(currencyToUpdate))
                        {
                            // Because of the workaround for the undo manager, if the base currency was updated, its value is null, so it must be restored.
                            this.BaseCurrency.Value = currencyToUpdate;
                        }
                    }
                }

                if (this.BaseCurrency.Value == null)
                {
                    // If there is no base currency, the default one is set.
                    this.BaseCurrency.Value = this.Currencies.FirstOrDefault(c => c.IsoCode == Constants.DefaultCurrencyIsoCode);
                }

                this.areCurrenciesUpdated = true;
                this.CurrenciesUpdateExists.Value = false;
                this.IsChanged = true;

                while (this.OutdatedCurrencies.Count > 0)
                {
                    this.OutdatedCurrencies.RemoveAt(0);
                }
            }
        }

        /// <summary>
        /// Checks for currencies changes, master data vs. project currencies.
        /// </summary>
        private void CheckForCurrenciesChanges()
        {
            var dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var masterDataCurrencies = dataManager.CurrencyRepository.GetBaseCurrencies();

            var modifiedCurrencies = new Collection<CurrencyComparisonData>();
            foreach (var masterDataCurrency in masterDataCurrencies)
            {
                var currency = this.Currencies.FirstOrDefault(c => c.IsSameAs(masterDataCurrency));
                if (currency == null || currency.ExchangeRate != masterDataCurrency.ExchangeRate)
                {
                    modifiedCurrencies.Add(new CurrencyComparisonData() { MasterDataCurrency = masterDataCurrency, LocalCurrency = currency });
                }
            }

            foreach (var currency in this.Currencies)
            {
                var currencyFromMD = masterDataCurrencies.FirstOrDefault(c => c.IsSameAs(currency));
                if (currencyFromMD == null)
                {
                    modifiedCurrencies.Add(new CurrencyComparisonData() { MasterDataCurrency = currencyFromMD, LocalCurrency = currency });
                }
            }

            this.OutdatedCurrencies.Clear();
            this.OutdatedCurrencies.AddRange(modifiedCurrencies.OrderBy(c => c.Name));

            if (this.OutdatedCurrencies.Count > 0)
            {
                this.CurrenciesUpdateExists.Value = true;
            }
        }

        /// <summary>
        /// Converts the project hierarchy objects to the new base currency.
        /// </summary>
        /// <param name="oldBaseCurrency">The old base currency.</param>
        private void ConvertProjectHierarchy(Currency oldBaseCurrency)
        {
            var change = this.EditMode == ViewModelEditMode.Create ? EntityChangeType.EntityCreated : EntityChangeType.EntityUpdated;

            Action<PleaseWaitService.WorkParams> work = (workParams) =>
            {
                // Get the full project on another context.
                var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
                var fullProject = dsm.ProjectRepository.GetProjectFull(this.Model.Guid);
                if (fullProject != null)
                {
                    // Convert all the fields, of all the project entities hierarchy that represent money to the new base currency.
                    CurrencyConversionManager.ConvertObject(fullProject, this.BaseCurrency.Value, oldBaseCurrency);

                    // Save the converted project.
                    dsm.SaveChanges();

                    // Set the base currency and the currencies of the project to the adapter in case that the base currency has change and/or the list of currencies has changed.
                    this.MeasurementUnitsAdapter.BaseCurrency = this.BaseCurrency.Value;
                    this.MeasurementUnitsAdapter.Currencies = this.Currencies;
                }
            };

            Action<PleaseWaitService.WorkParams> workCompleted = (workParams) =>
            {
                if (workParams.Error != null)
                {
                    this.windowService.MessageDialogService.Show(workParams.Error);
                }
                else
                {
                    // Send a message to refresh the project's tree items children.
                    var entityChangedMessage = new EntityChangedMessage(Notification.MyProjectsEntityChanged)
                    {
                        ChangeType = EntityChangeType.RefreshData,
                        Entity = this.Model
                    };
                    this.messenger.Send<EntityChangedMessage>(entityChangedMessage);

                    // Send a message to reload the project.
                    var notificationMessage = new NotificationMessage(Notification.ReloadMainViewCurrentContent);
                    this.messenger.Send(notificationMessage);
                }
            };

            // If the project was created another time and the base currency has changed apply the base currency conversion.
            if (change == EntityChangeType.EntityUpdated
                && !this.BaseCurrency.Value.IsSameAs(oldBaseCurrency))
            {
                this.pleaseWaitService.Show(LocalizedResources.General_ConvertingProject, work, workCompleted);
            }
        }

        #region Inner classes

        /// <summary>
        /// Stores information for comparing different instances of the same currency.
        /// </summary>
        public class CurrencyComparisonData
        {
            /// <summary>
            /// Gets the name.
            /// </summary>
            public string Name
            {
                get
                {
                    return MasterDataCurrency != null ? MasterDataCurrency.Name : LocalCurrency.Name;
                }
            }

            /// <summary>
            /// Gets or sets the master data currency.
            /// </summary>
            public Currency MasterDataCurrency { get; set; }

            /// <summary>
            /// Gets or sets the local currency.
            /// </summary>
            public Currency LocalCurrency { get; set; }
        }

        #endregion Inner classes
    }
}