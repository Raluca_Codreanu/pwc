﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using ZPKTool.Common;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Represents the data source for a menu item in the application.
    /// </summary>
    public class SystemMenuItem : ObservableObject
    {
        #region Fields

        /// <summary>
        /// The text displayed for the item.
        /// </summary>
        private string headerText;

        /// <summary>
        /// The resource key for the image to be used as the menu item's icon.
        /// </summary>
        private object iconResourceKey;

        /// <summary>
        /// A value indicating whether this instance represents a top level menu item.
        /// </summary>
        private bool isTopLevel;

        /// <summary>
        /// A value indicating whether this item is a separator instead of a normal item.
        /// </summary>
        private bool isSeparator;

        /// <summary>
        /// The sub-items of this item.
        /// </summary>
        private ObservableCollection<SystemMenuItem> items = new ObservableCollection<SystemMenuItem>();

        /// <summary>
        /// A value indicating whether this menu item can be checked.
        /// </summary>
        private bool isCheckable;

        /// <summary>
        /// A value indicating whether this menu item is checked.
        /// </summary>
        private bool isChecked;

        /// <summary>
        /// The command associated with the menu item.
        /// </summary>
        private ICommand command;

        /// <summary>
        /// The parameter to pass to the <see cref="SystemMenuItem.Command"/> property.
        /// </summary>
        private object commandParameter;

        #endregion Fields

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemMenuItem" /> class.
        /// </summary>
        public SystemMenuItem()
        {
            this.AutomationId = string.Empty;
        }

        #region Properties

        /// <summary>
        /// Gets or sets the sub-items of this item.
        /// </summary>
        public ObservableCollection<SystemMenuItem> Items
        {
            get { return this.items; }
            set { this.SetProperty(ref this.items, value, () => this.Items); }
        }

        /// <summary>
        /// Gets or sets the text displayed for the item.
        /// </summary>        
        public string HeaderText
        {
            get { return this.headerText; }
            set { this.SetProperty(ref this.headerText, value, () => this.HeaderText); }
        }

        /// <summary>
        /// Gets or sets the resource key for the image to be used as the menu item's icon.
        /// This key should point to a resource containing a <see cref="System.Windows.Media.ImageSource"/> instance.
        /// </summary>
        public object IconResourceKey
        {
            get { return this.iconResourceKey; }
            set { this.SetProperty(ref this.iconResourceKey, value, () => this.IconResourceKey); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance represents a top level menu item.
        /// An item is top level if his direct parent is the menu, not another item.
        /// </summary>        
        public bool IsTopLevel
        {
            get { return this.isTopLevel; }
            set { this.SetProperty(ref this.isTopLevel, value, () => this.IsTopLevel); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this item is a separator instead of a normal item.
        /// </summary>
        public bool IsSeparator
        {
            get { return this.isSeparator; }
            set { this.SetProperty(ref this.isSeparator, value, () => this.IsSeparator); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this menu item can be checked.
        /// </summary>        
        public bool IsCheckable
        {
            get { return this.isCheckable; }
            set { this.SetProperty(ref this.isCheckable, value, () => this.IsCheckable); }
        }

        /// <summary>
        ///  Gets or sets a value indicating whether this menu item is checked.
        /// </summary>
        public bool IsChecked
        {
            get { return this.isChecked; }
            set { this.SetProperty(ref this.isChecked, value, () => this.IsChecked); }
        }

        /// <summary>
        ///  Gets or sets the command associated with the menu item.
        /// </summary>
        public ICommand Command
        {
            get { return this.command; }
            set { this.SetProperty(ref this.command, value, () => this.Command); }
        }

        /// <summary>
        /// Gets or sets the parameter to pass to the <see cref="SystemMenuItem.Command"/> property.
        /// </summary>        
        public object CommandParameter
        {
            get { return this.commandParameter; }
            set { this.SetProperty(ref this.commandParameter, value, () => this.CommandParameter); }
        }

        /// <summary>
        /// Gets or sets the position in which the item appears in its parent.        
        /// </summary>
        /// <remarks>
        /// For now this property is only used to order the top level items. The implementation for ordering sub-items will follow if it is necessary.
        /// </remarks>
        public int Index { get; set; }

        /// <summary>
        /// Gets or sets the automation id applied to the UI element generated for this item.
        /// </summary>
        public string AutomationId { get; set; }
        
        #endregion Properties
    }
}
