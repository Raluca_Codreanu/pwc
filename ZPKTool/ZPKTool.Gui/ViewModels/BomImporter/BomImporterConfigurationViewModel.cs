﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Xml.Serialization;
using ZPKTool.Business;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The configuration ViewModel class.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class BomImporterConfigurationViewModel : ViewModel
    {
        #region Fields

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The title of the bom import window.
        /// </summary>
        private string title;

        /// <summary>
        /// The backup configuration. Can be used to revert to original values.
        /// </summary>
        private BomConfiguration backupConfiguration;

        /// <summary>
        /// Indicates whether to revert the changes or not when closing.
        /// </summary>
        private bool revertChanges;

        /// <summary>
        /// The list of image extensions supported by the application.
        /// </summary>
        private StringCollection supportedImagesExtensions = new StringCollection { ".jpeg", ".jpg", ".gif", ".png", ".tiff", ".tif" };

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="BomImporterConfigurationViewModel"/> class.
        /// </summary>
        /// <param name="messenger">The messenger.</param>
        /// <param name="windowService">The window service.</param>
        [ImportingConstructor]
        public BomImporterConfigurationViewModel(IMessenger messenger, IWindowService windowService)
        {
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("windowService", windowService);

            this.messenger = messenger;
            this.windowService = windowService;

            this.Title = LocalizedResources.General_Configuration;

            this.SaveConfigurationCommand = new DelegateCommand<object>(this.Save, this.CanSave);
            this.CancelConfigurationCommand = new DelegateCommand<object>(this.Cancel, this.CanCancel);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the selected configuration.
        /// </summary>
        public VMProperty<BomConfiguration> Configuration { get; private set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        public string Title
        {
            get { return this.title; }
            set { this.SetProperty(ref this.title, value, () => this.Title); }
        }

        /// <summary>
        /// Gets or sets the table data imported from the import file.
        /// </summary>
        /// <value>
        /// The table data.
        /// </value>
        public List<List<object>> TableData { get; set; }

        /// <summary>
        /// Gets or sets the tokenized headers.
        /// </summary>
        public VMProperty<Dictionary<string, string>> TokenizedHeaders { get; set; }

        /// <summary>
        /// Gets or sets the parent representing the entity that is the destination of the imported data.
        /// </summary>
        /// <value>
        /// The destination of the imported data.
        /// </value>
        public object Parent { get; set; }

        /// <summary>
        /// Gets or sets the data context.
        /// </summary>
        /// <value>
        /// The data context.
        /// </value>
        public IDataSourceManager DataContext { get; set; }

        /// <summary>
        /// Gets the show cancel.
        /// </summary>
        public VMProperty<bool> ShowCancel { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is create.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is create; otherwise, <c>false</c>.
        /// </value>
        public bool IsCreate { get; set; }

        /// <summary>
        /// Gets or sets the level.
        /// </summary>
        /// <value>
        /// The level.
        /// </value>
        public string Level { get; set; }

        /// <summary>
        /// Gets or sets the type of the item.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the base currency in which the data from the imported file is represented.
        /// </summary>
        public Currency SourceBaseCurrency { get; set; }

        /// <summary>
        /// Gets or sets the base currency of the entity in which the data will be imported.
        /// </summary>
        public Currency DestinationBaseCurrency { get; set; }

        /// <summary>
        /// Gets or sets the weight unit.
        /// </summary>
        public MeasurementUnit WeightUnit { get; set; }

        /// <summary>
        /// Gets or sets the path to the folder that contains the pictures to import
        /// </summary>
        public string PicturesFolderPath { get; set; }

        /// <summary>
        /// Gets or sets the index of the column containing the pictures to import
        /// </summary>
        public int PicturesColumnIndex { get; set; }

        #endregion

        #region ICommand

        /// <summary>
        /// Gets the save configuration command.
        /// </summary>
        public ICommand SaveConfigurationCommand { get; private set; }

        /// <summary>
        /// Gets the cancel configuration command.
        /// </summary>
        public ICommand CancelConfigurationCommand { get; private set; }

        #endregion

        /// <summary>
        /// Called after the view has been loaded. Usually it performs some initialization that needs the view to be loaded, like
        /// starting to load data from a database.
        /// <para/>
        /// During unit tests this method must be manually called because there is no view to call it.
        /// </summary>
        public override void OnLoaded()
        {
            base.OnLoaded();

            backupConfiguration = BomConfiguration.Clone(Configuration.Value);
            revertChanges = true;
        }

        #region Save/Cancel Methods

        /// <summary>
        /// Saves the configuration.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void Save(object parameter)
        {
            try
            {
                if (this.IsCreate)
                {
                    this.messenger.Send(new NotificationMessage("NewConfigurationCreated"));
                }

                this.Configuration.Value.DateChanged = DateTime.Now;

                if (!Directory.Exists(BomConfiguration.ConfigurationFilesFolder))
                {
                    Directory.CreateDirectory(BomConfiguration.ConfigurationFilesFolder);
                }

                var fullNameSettingsFilePath = BomConfiguration.GetFullPathOfConfiguration(Configuration.Value);

                using (Stream stream = new FileStream(fullNameSettingsFilePath, FileMode.Create))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(BomConfiguration));
                    serializer.Serialize(stream, this.Configuration.Value);
                }

                revertChanges = false;
                this.windowService.CloseViewWindow(this);
            }
            catch (PathTooLongException ex)
            {
                log.ErrorException("An error occurred while trying to save the configuration.", ex);
                throw PathUtils.ParseFileRelatedException(ex);
            }
        }

        /// <summary>
        /// Determines whether this instance [can save configuration] the specified parameter.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        /// <c>true</c> if this instance [can save configuration] the specified parameter; otherwise, <c>false</c>.
        /// </returns>
        private bool CanSave(object parameter)
        {
            if (!string.IsNullOrWhiteSpace(this.Configuration.Value.Error))
            {
                return false;
            }

            if (this.Configuration.Value.ConfigurationItems != null)
            {
                foreach (var item in this.Configuration.Value.ConfigurationItems)
                {
                    if (item.IsMandatory && (string.IsNullOrWhiteSpace(item.ColumnName) && string.IsNullOrWhiteSpace(item.CustomValue)))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Cancels the configuration.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void Cancel(object parameter)
        {
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Called before unloading the view from its parent or closing it. Returning false will cancel the view's unloading or closing.
        /// </summary>
        /// <returns>
        /// True if the unloading process should continue and false if it should be canceled.
        /// </returns>
        public override bool OnUnloading()
        {
            var baseCanUnload = base.OnUnloading();
            var messageDialogResult = MessageDialogResult.Yes;
            var changesDetected = DetectChanges();

            if (baseCanUnload && revertChanges && changesDetected)
            {
                messageDialogResult = this.windowService.MessageDialogService.Show(LocalizedResources.Question_Quit, MessageDialogType.YesNo);
            }

            return baseCanUnload && messageDialogResult == MessageDialogResult.Yes;
        }

        /// <summary>
        /// Detects the changes.
        /// </summary>
        /// <returns><c>True</c>, if there were changes, otherwise <c>false</c></returns>
        private bool DetectChanges()
        {
            if (Configuration.Value.Comment != backupConfiguration.Comment ||
                Configuration.Value.DateChanged != backupConfiguration.DateChanged ||
                Configuration.Value.Name != backupConfiguration.Name ||
                Configuration.Value.PicturesColumnIndex != backupConfiguration.PicturesColumnIndex ||
                Configuration.Value.PicturesFolderCellAddress != backupConfiguration.PicturesFolderCellAddress)
            {
                return true;
            }

            for (var configurationIndex = 0; configurationIndex < backupConfiguration.ConfigurationItems.Count; configurationIndex++)
            {
                if (Configuration.Value.ConfigurationItems[configurationIndex].ColumnName != backupConfiguration.ConfigurationItems[configurationIndex].ColumnName ||
                    Configuration.Value.ConfigurationItems[configurationIndex].CustomValue != backupConfiguration.ConfigurationItems[configurationIndex].CustomValue)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Called after the view has been unloaded from the parent or closed.
        /// </summary>
        public override void OnUnloaded()
        {
            if (revertChanges)
            {
                Configuration.Value.Comment = backupConfiguration.Comment;
                Configuration.Value.DateChanged = backupConfiguration.DateChanged;
                Configuration.Value.Name = backupConfiguration.Name;
                Configuration.Value.PicturesColumnIndex = backupConfiguration.PicturesColumnIndex;
                Configuration.Value.PicturesFolderCellAddress = backupConfiguration.PicturesFolderCellAddress;

                for (var configurationIndex = 0; configurationIndex < backupConfiguration.ConfigurationItems.Count; configurationIndex++)
                {
                    Configuration.Value.ConfigurationItems[configurationIndex].ColumnName = backupConfiguration.ConfigurationItems[configurationIndex].ColumnName;
                    Configuration.Value.ConfigurationItems[configurationIndex].CustomValue = backupConfiguration.ConfigurationItems[configurationIndex].CustomValue;
                }
            }

            base.OnUnloaded();
        }

        /// <summary>
        /// Determines whether this instance [can cancel configuration] the specified parameter.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        /// <c>true</c> if this instance [can cancel configuration] the specified parameter; otherwise, <c>false</c>.
        /// </returns>
        private bool CanCancel(object parameter)
        {
            return true;
        }

        #endregion

        #region Import Data

        /// <summary>
        /// Imports the data specified in the table.
        /// </summary>
        public void ImportData()
        {
            // The level list.
            List<List<object>> levels = new List<List<object>>();

            // The table headers.
            List<object> headers = this.TableData.FirstOrDefault();

            // The total number of table rows.
            int rowCount = this.TableData.Count;

            // The total number of table columns.          
            int columnCount = headers.Count;

            // The type column index that represents the entity type.
            int typeColumn = headers.IndexOf(this.Type);

            // The level column index. 
            int levelColumn = headers.IndexOf(this.Level);

            if (typeColumn == -1)
            {
                this.windowService.MessageDialogService.Show(string.Format(LocalizedResources.BomImport_TypeNotFoundError, this.Type), MessageDialogType.Error);
            }
            else if (levelColumn == -1)
            {
                this.windowService.MessageDialogService.Show(string.Format(LocalizedResources.BomImport_LevelNotFoundError, this.Level), MessageDialogType.Error);
            }

            int rowIndex = 1;
            int indexColumn = 0;
            var importErrors = new BomImportErrors();

            // If the PicturesColumnIndex and PicturesFolderPath are not set means that no pictures can be imported 
            // because the bom import configuration does not contain information for picture import
            var canImportPictures = !string.IsNullOrWhiteSpace(this.Configuration.Value.PicturesColumnIndex) && !string.IsNullOrWhiteSpace(this.Configuration.Value.PicturesFolderCellAddress);

            Task.Factory.StartNew(() =>
            {
                this.messenger.Send<NotificationMessage>(new NotificationMessage("StartIsImporting"));

                // Reset the custom values for items which have columns set.
                foreach (var configurationItem in Configuration.Value.ConfigurationItems.Where(item => !string.IsNullOrWhiteSpace(item.ColumnName) && item.ColumnName != LocalizedResources.BomImport_None && !string.IsNullOrWhiteSpace(item.CustomValue)))
                {
                    configurationItem.CustomValue = string.Empty;
                }

                // Go through each table row and create the object graph hierarchy.
                for (rowIndex = 1; rowIndex < rowCount; rowIndex++)
                {
                    // The current traversed table row.
                    List<object> currentRow = this.TableData[rowIndex];
                    int level;

                    // The level of the current row.
                    if (int.TryParse(currentRow[levelColumn].ToString(), out level))
                    {
                        // The logical parent for the traversed row.
                        object parent = this.Parent;

                        // The master data flag.
                        bool isMasterData = false;

                        // The reference to the object to save to database.
                        object entityCreated = null;

                        if ((string)currentRow[typeColumn] == typeof(Assembly).Name.ToString())
                        {
                            Assembly assy = new Assembly();
                            this.AddParentAndDefaultValues(assy, levels, level, parent, isMasterData, this.DataContext);

                            this.DataContext.AssemblyRepository.Add(assy);
                            entityCreated = assy;
                        }
                        else if ((string)currentRow[typeColumn] == typeof(Part).Name.ToString())
                        {
                            Part part = new Part();
                            this.AddParentAndDefaultValues(part, levels, level, parent, isMasterData, this.DataContext);

                            this.DataContext.PartRepository.Add(part);
                            entityCreated = part;
                        }
                        else if ((string)currentRow[typeColumn] == typeof(RawMaterial).Name.ToString())
                        {
                            RawMaterial rawMaterial = new RawMaterial();
                            this.AddParentAndDefaultValues(rawMaterial, levels, level, parent, isMasterData, this.DataContext);

                            this.DataContext.RawMaterialRepository.Add(rawMaterial);
                            entityCreated = rawMaterial;
                        }
                        else if ((string)currentRow[typeColumn] == typeof(Commodity).Name.ToString())
                        {
                            Commodity commodity = new Commodity();
                            this.AddParentAndDefaultValues(commodity, levels, level, parent, isMasterData, this.DataContext);

                            this.DataContext.CommodityRepository.Add(commodity);
                            entityCreated = commodity;
                        }

                        if (entityCreated == null)
                        {
                            throw new InvalidOperationException("The created entity in the BomImporter is NULL.");
                        }

                        if (level > levels.Count)
                        {
                            List<object> newLevel = new List<object>();
                            levels.Add(newLevel);
                            newLevel.Add(entityCreated);
                        }
                        else
                        {
                            levels[level - 1].Add(entityCreated);
                        }

                        // Set properties for the created entity.
                        for (indexColumn = 0; indexColumn < headers.Count; indexColumn++)
                        {
                            string headerValue = headers[indexColumn].ToString();

                            // find the field name corresponding to the column name that is equal with the header name from the document.
                            BomConfigurationItem configurationItem = this.Configuration.Value.ConfigurationItems.FirstOrDefault(p => p.ColumnName != null && p.ColumnName.Equals(headerValue));

                            this.SetPropertyValue(entityCreated, configurationItem, TableData[rowIndex][indexColumn]);
                        }

                        this.AddCustomValues(entityCreated);
                        this.NormalizeWeightUnitValues(entityCreated);

                        // Convert the created entity to the parent project's base currency.
                        CurrencyConversionManager.ConvertObject(entityCreated, this.DestinationBaseCurrency, this.SourceBaseCurrency);

                        // Import the images and attach them to the imported object if possible
                        if (canImportPictures)
                        {
                            if (Directory.Exists(this.PicturesFolderPath))
                            {
                                // Get the pictures folder files path and files filename
                                var folderContent = Directory.GetFiles(this.PicturesFolderPath).ToList();
                                this.AddPicturesToEntity(entityCreated, rowIndex, folderContent, importErrors);
                            }
                            else
                            {
                                log.Error("The pictures folder does not exist.");
                            }
                        }
                    }
                }

                this.DataContext.SaveChanges();
            }).ContinueWith(
        (task) =>
        {
            this.messenger.Send<NotificationMessage>(new NotificationMessage("StopIsImporting"));

            if (task.Exception != null)
            {
                Exception error = task.Exception.InnerException;
                log.ErrorException("Bom import failed.", error);

                if (error is UnauthorizedAccessException)
                {
                    this.windowService.MessageDialogService.Show(LocalizedResources.General_FileAccessError, MessageDialogType.Error);
                }
                else if (error is IOException)
                {
                    this.windowService.MessageDialogService.Show(LocalizedResources.General_FileIOError, MessageDialogType.Error);
                }
                else if (error is Exception)
                {
                    string errorMessage = string.Format(LocalizedResources.Import_ErrorImport, headers[indexColumn], this.TableData[rowIndex][indexColumn]);
                    this.windowService.MessageDialogService.Show(errorMessage, MessageDialogType.Error);
                }
            }
            else
            {
                // Send the notification through the global messenger.
                EntityChangedMessage msg = new EntityChangedMessage(Notification.MyProjectsEntityChanged);
                msg.Entity = this.Parent;
                msg.Parent = null;
                msg.ChangeType = EntityChangeType.EntityUpdated;
                this.messenger.Send<EntityChangedMessage>(msg);

                if (importErrors.HasErrors)
                {
                    var errorMessage = this.ComputeErrorMessage(importErrors);
                    this.windowService.MessageDialogService.Show(errorMessage, MessageDialogType.Warning);
                }
                else
                {
                    this.windowService.MessageDialogService.Show(LocalizedResources.BomImport_SuccessImport, MessageDialogType.Info);
                }
            }
        },
        TaskScheduler.FromCurrentSynchronizationContext());
        }

        /// <summary>
        /// Sets the property value.
        /// </summary>
        /// <param name="createdEntity">The created entity.</param>
        /// <param name="configurationItem">The configuration item.</param>
        /// <param name="valueToSet">The value to set.</param>
        private void SetPropertyValue(object createdEntity, BomConfigurationItem configurationItem, object valueToSet)
        {
            if (configurationItem != null)
            {
                var category = configurationItem.Categories.FirstOrDefault(p => p.First == createdEntity.GetType().Name);

                if (category != null)
                {
                    if (category.Second == "Manufacturer.Name")
                    {
                        // Manufacturer Name.
                        Assembly assy = createdEntity as Assembly;
                        if (assy != null)
                        {
                            assy.Manufacturer.Name = valueToSet == null ? string.Empty : valueToSet.ToString();
                        }
                        else
                        {
                            Part part = createdEntity as Part;
                            if (part != null)
                            {
                                part.Manufacturer.Name = valueToSet == null ? string.Empty : valueToSet.ToString();
                            }
                            else
                            {
                                RawMaterial rawMat = createdEntity as RawMaterial;
                                if (rawMat != null)
                                {
                                    rawMat.Manufacturer.Name = valueToSet == null ? string.Empty : valueToSet.ToString();
                                }
                                else
                                {
                                    Commodity commodity = createdEntity as Commodity;
                                    if (commodity != null)
                                    {
                                        commodity.Manufacturer.Name = valueToSet == null ? string.Empty : valueToSet.ToString();
                                    }
                                }
                            }
                        }
                    }

                    var propertyInfo = createdEntity.GetType().GetProperty(category.Second);

                    if (propertyInfo != null && valueToSet != null)
                    {
                        var propertyType = propertyInfo.PropertyType;
                        var propName = propertyInfo.Name;
                        object result = null;

                        if (propName == "CalculationAccuracy")
                        {
                            var stringValueToSet = BomImporterConfigureViewModel.TokenizeString(valueToSet.ToString());
                            if (!string.IsNullOrWhiteSpace(stringValueToSet))
                            {
                                result = Enum.Parse(typeof(PartCalculationAccuracy), stringValueToSet, true);
                                propertyInfo.SetValue(createdEntity, result, null);
                            }
                        }
                        else if (propName == "DeliveryType" || propName == "DelivertType")
                        {
                            var stringValueToSet = BomImporterConfigureViewModel.TokenizeString(valueToSet.ToString());
                            if (createdEntity is Assembly || createdEntity is Part)
                            {
                                if (!string.IsNullOrWhiteSpace(stringValueToSet))
                                {
                                    result = Enum.Parse(typeof(PartDeliveryType), stringValueToSet, true);
                                    propertyInfo.SetValue(createdEntity, result, null);
                                }
                            }
                            else if (createdEntity is RawMaterial)
                            {
                                var deliveryTypes = this.DataContext.RawMaterialDeliveryTypeRepository.FindAll();
                                result = deliveryTypes.FirstOrDefault(p => string.Compare(p.Name, stringValueToSet, StringComparison.OrdinalIgnoreCase) == 0);
                                propertyInfo.SetValue(createdEntity, result, null);
                            }
                        }
                        else if (propName == "AssemblingCountry")
                        {
                            this.SetDefaultCountry(createdEntity as Assembly, valueToSet.ToString(), string.Empty, this.DataContext);
                        }
                        else if (propName == "ManufacturingCountry")
                        {
                            this.SetDefaultCountry(createdEntity as Part, valueToSet.ToString(), string.Empty, this.DataContext);
                        }
                        else if (propName == "CalculationStatus" && createdEntity is Part)
                        {
                            var stringValueToSet = BomImporterConfigureViewModel.TokenizeString(valueToSet.ToString());
                            if (!string.IsNullOrWhiteSpace(stringValueToSet))
                            {
                                result = Enum.Parse(typeof(PartCalculationStatus), stringValueToSet, true);
                                propertyInfo.SetValue(createdEntity, result, null);
                            }
                        }
                        else if (propName == "CalculationApproach")
                        {
                            var stringValueToSet = BomImporterConfigureViewModel.TokenizeString(valueToSet.ToString());
                            if (!string.IsNullOrWhiteSpace(stringValueToSet))
                            {
                                result = Enum.Parse(typeof(PartCalculationApproach), stringValueToSet, true);
                                propertyInfo.SetValue(createdEntity, result, null);
                            }
                        }
                        else
                        {
                            if (Converter.Cast(propertyType, valueToSet, ref result))
                            {
                                propertyInfo.SetValue(createdEntity, result, null);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Adds the custom values.
        /// </summary>
        /// <param name="createdEntity">The created entity.</param>
        private void AddCustomValues(object createdEntity)
        {
            if (createdEntity == null)
            {
                throw new ArgumentNullException("createdEntity");
            }

            foreach (var bomConfigurationItem in Configuration.Value.ConfigurationItems.Where(item => !string.IsNullOrWhiteSpace(item.CustomValue)))
            {
                SetPropertyValue(createdEntity, bomConfigurationItem, bomConfigurationItem.CustomValue);
            }
        }

        /// <summary>
        /// Normalizes the weight unit values.
        /// </summary>
        /// <param name="createdEntity">The created entity.</param>
        private void NormalizeWeightUnitValues(object createdEntity)
        {
            if (createdEntity == null)
            {
                throw new ArgumentNullException("createdEntity");
            }

            if (WeightUnit == null || WeightUnit.ScaleFactor == 0)
            {
                throw new InvalidOperationException("The WeightUnit is either NULL or the ScaleFactor is 0 when trying to normalize the weight-related values in the BOM Importer.");
            }

            Assembly assembly = createdEntity as Assembly;
            if (assembly != null)
            {
                assembly.Weight *= WeightUnit.ConversionRate;
                assembly.MeasurementUnit = WeightUnit;
                assembly.AssetRate /= 100;
            }
            else
            {
                Part part = createdEntity as Part;
                if (part != null)
                {
                    part.Weight *= WeightUnit.ConversionRate;
                    part.MeasurementUnit = WeightUnit;
                    part.AssetRate /= 100;
                }
                else
                {
                    RawMaterial rawMaterial = createdEntity as RawMaterial;
                    if (rawMaterial != null)
                    {
                        rawMaterial.ParentWeight *= WeightUnit.ConversionRate;
                        rawMaterial.ParentWeightUnitBase = WeightUnit;
                        rawMaterial.Quantity *= WeightUnit.ConversionRate;
                        rawMaterial.QuantityUnitBase = WeightUnit;
                        rawMaterial.Sprue /= 100;
                        rawMaterial.Loss /= 100;
                        rawMaterial.ScrapRefundRatio /= 100;
                        rawMaterial.RejectRatio /= 100;
                    }
                    else
                    {
                        Commodity commodity = createdEntity as Commodity;
                        if (commodity != null)
                        {
                            commodity.Weight *= WeightUnit.ConversionRate;
                            commodity.WeightUnitBase = WeightUnit;
                            commodity.RejectRatio /= 100;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Sets the column headers.
        /// </summary>
        /// <param name="tableHeaders">The table headers.</param>
        public void SetColumnHeaders(List<object> tableHeaders)
        {
            var columnsList = (from header in tableHeaders
                               select header.ToString()).ToList();

            columnsList.Remove(this.Type);
            columnsList.Remove(this.Level);
            columnsList.Insert(0, LocalizedResources.BomImport_None);

            foreach (var configurationItem in this.Configuration.Value.ConfigurationItems)
            {
                configurationItem.ColumnNames.Clear();
                configurationItem.ColumnNames.AddRange(columnsList);
            }
        }

        /// <summary>
        /// Imports the entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="levels">The levels.</param>
        /// <param name="level">The level.</param>
        /// <param name="parent">The parent.</param>
        /// <param name="isMasterData">if set to <c>true</c> [is master data].</param>
        /// <param name="dataManager">The data manager.</param>
        private void AddParentAndDefaultValues(object entity, List<List<object>> levels, int level, object parent, bool isMasterData, IDataSourceManager dataManager)
        {
            if (level == 1)
            {
                Project parentProject = this.Parent as Project;
                if (parentProject != null)
                {
                    parentProject = dataManager.ProjectRepository.GetProjectForEdit(parentProject.Guid);
                    if (parentProject != null)
                    {
                        Assembly assy = entity as Assembly;
                        if (assy != null)
                        {
                            this.SetDefaultValues(assy, parentProject, false, dataManager);
                        }
                        else
                        {
                            Part part = entity as Part;
                            if (part != null)
                            {
                                this.SetDefaultValues(part, parentProject, false, dataManager);
                            }
                        }
                    }
                }
                else
                {
                    Assembly parentAssembly = this.Parent as Assembly;
                    if (parentAssembly != null)
                    {
                        parentAssembly = dataManager.AssemblyRepository.GetAssemblyFull(parentAssembly.Guid);
                        if (parentAssembly != null)
                        {
                            Assembly assy = entity as Assembly;
                            if (assy != null)
                            {
                                this.SetDefaultValues(assy, parentAssembly, parentAssembly.IsMasterData, dataManager);
                            }
                            else
                            {
                                Part part = entity as Part;
                                if (part != null)
                                {
                                    this.SetDefaultValues(part, parentAssembly, parentAssembly.IsMasterData, dataManager);
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                parent = levels[level - 2].Last();

                Assembly parentAssy = parent as Assembly;
                if (parentAssy != null)
                {
                    Assembly assy = entity as Assembly;
                    if (assy != null)
                    {
                        this.SetDefaultValues(assy, parentAssy, parentAssy.IsMasterData, dataManager);
                    }
                    else
                    {
                        Part part = entity as Part;
                        if (part != null)
                        {
                            this.SetDefaultValues(part, parentAssy, parentAssy.IsMasterData, dataManager);
                        }
                    }
                }
                else
                {
                    Part parentPart = parent as Part;
                    if (parentPart != null)
                    {
                        RawMaterial material = entity as RawMaterial;
                        if (material != null)
                        {
                            this.SetDefaultValues(material, parentPart, parentPart.IsMasterData, dataManager);
                        }
                        else
                        {
                            Commodity commodity = entity as Commodity;
                            if (commodity != null)
                            {
                                this.SetDefaultValues(commodity, parentPart, parentPart.IsMasterData, dataManager);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Imports the pictures and attaches them to corresponding entity
        /// </summary>
        /// <param name="entity">The entity to attach pictures to</param>
        /// <param name="rowIndex">The entity row index corresponding </param>
        /// <param name="filesInPicturesFolder">A list containing all the files from the picture folder</param>
        /// <param name="importErrors">The <see cref="BomImportErrors"/> instance containing the import errors</param>
        private void AddPicturesToEntity(object entity, int rowIndex, List<string> filesInPicturesFolder, BomImportErrors importErrors)
        {
            if (rowIndex < 0 || rowIndex > this.TableData.Count || this.PicturesColumnIndex < 0 || this.PicturesColumnIndex > this.TableData.FirstOrDefault().Count)
            {
                log.Error("The row index or pictures column index are out of range");
                return;
            }

            // Sort the file names in filesInPicturesFolder according to the comparison rules used in the method (OrdinalIgnoreCase).
            var filesOrdered = filesInPicturesFolder.OrderBy(f => f, StringComparer.OrdinalIgnoreCase).ToList();

            var pictureCellValue = TableData[rowIndex][this.PicturesColumnIndex] as string;
            if (pictureCellValue != null)
            {
                var validPicturesToImport = new List<string>();
                var pictureNames = pictureCellValue.Split(',').Where(it => !string.IsNullOrWhiteSpace(it)).Select(item => item.Trim());
                foreach (var item in pictureNames)
                {
                    var extensions = Regex.Matches(item, @"\.+[A-Za-z]{3,}");

                    // The pictures item is a picture range (img001.jpg - img.007.jpg)
                    if (extensions.Count == 2)
                    {
                        var firstItemExtension = extensions[0];
                        var secondItemExtension = extensions[1];
                        if (!firstItemExtension.Value.Equals(secondItemExtension.Value, StringComparison.OrdinalIgnoreCase)
                            || string.IsNullOrWhiteSpace(firstItemExtension.Value)
                            || !this.supportedImagesExtensions.Contains(firstItemExtension.Value.ToLower()))
                        {
                            importErrors[entity].InvalidPictures.Add(item);
                            log.Warn("The range '{0}' of pictures have different file extensions or are not supported.", item);
                        }
                        else
                        {
                            // Get the filenames without extension
                            var fileNames = Regex.Split(item, @"\.+[A-Za-z]{3,}").Where(file => !string.IsNullOrWhiteSpace(file));

                            // Remove the range separator and any white spaces before and after the range separator 
                            var firstFileName = fileNames.ElementAt(0).Trim().Trim('-').Trim();
                            var secondFileName = fileNames.ElementAt(1).Trim().Trim('-').Trim();

                            // Check if the pictures defining the range exists on disk
                            var firstItemFilePath = filesOrdered.FirstOrDefault(pct => Path.GetFileName(pct).Equals(firstFileName + firstItemExtension, StringComparison.OrdinalIgnoreCase));
                            var secondItemFilePath = filesOrdered.FirstOrDefault(pct => Path.GetFileName(pct).Equals(secondFileName + secondItemExtension, StringComparison.OrdinalIgnoreCase));

                            if (!string.IsNullOrWhiteSpace(firstItemFilePath) && !string.IsNullOrWhiteSpace(secondItemFilePath))
                            {
                                var startRangeIndex = 0;
                                var endRangeIndex = 0;
                                if (string.Compare(firstItemFilePath, secondItemFilePath, StringComparison.OrdinalIgnoreCase) <= 0)
                                {
                                    startRangeIndex = filesOrdered.IndexOf(firstItemFilePath);
                                    endRangeIndex = filesOrdered.IndexOf(secondItemFilePath);

                                    // Get the valid pictures in the range
                                    for (int i = startRangeIndex; i <= endRangeIndex; i++)
                                    {
                                        var picturePath = filesOrdered.ElementAtOrDefault(i);
                                        if (Path.GetExtension(picturePath).Equals(firstItemExtension.Value, StringComparison.OrdinalIgnoreCase))
                                        {
                                            validPicturesToImport.Add(picturePath);
                                        }
                                    }
                                }
                                else
                                {
                                    importErrors[entity].InvalidPictures.Add(item);
                                    log.Warn("One of the range '{0}' is incorrectly specified.", item);
                                }
                            }
                            else
                            {
                                importErrors[entity].InvalidPictures.Add(item);
                                log.Warn("One of the range '{0}' start or end pictures couldn't be found.", item);
                            }
                        }
                    }
                    else if (extensions.Count == 1)
                    {
                        var pictureExtension = Path.GetExtension(item);
                        if (!string.IsNullOrWhiteSpace(pictureExtension) && this.supportedImagesExtensions.Contains(pictureExtension.ToLower()))
                        {
                            // The pictures item represents a single picture range (img001.jpg)
                            var picturePath = filesOrdered.FirstOrDefault(pct => Path.GetFileName(pct).Equals(item, StringComparison.OrdinalIgnoreCase));
                            if (!string.IsNullOrWhiteSpace(picturePath))
                            {
                                validPicturesToImport.Add(picturePath);
                            }
                            else
                            {
                                importErrors[entity].InvalidPictures.Add(item);
                                log.Warn("The picture '{0}' couldn't be found.", item);
                            }
                        }
                        else
                        {
                            importErrors[entity].InvalidPictures.Add(item);
                            log.Warn("The picture '{0}' type is not supported.", item);
                        }
                    }
                    else
                    {
                        importErrors[entity].InvalidPictures.Add(item);
                        log.Warn("The picture or range of pictures filenames '{0}' seems to be wrong.", item);
                    }
                }

                if (this.CanAddPictures(entity, validPicturesToImport.Count))
                {
                    foreach (var path in validPicturesToImport)
                    {
                        var added = this.AddPictureToEntity(entity, path);
                        if (!added)
                        {
                            importErrors[entity].InvalidPictures.Add(Path.GetFileName(path));
                        }
                    }
                }
                else
                {
                    importErrors[entity].InvalidPictures.Clear();
                    importErrors[entity].InvalidPictures.Add(pictureCellValue);
                    log.Warn("The pictures can not be added because it would exceed the maximum number of pictures allowed by the entity.");
                }
            }
        }

        /// <summary>
        /// Gets the picture media from a file path and adds it to the entity
        /// </summary>
        /// <param name="entity">The entity to add media to</param>
        /// <param name="pictureFilePath">The picture file path</param>
        /// <returns>
        /// True if the picture was successfully added to the entity, false otherwise.
        /// </returns>
        private bool AddPictureToEntity(object entity, string pictureFilePath)
        {
            byte[] content = null;
            if (!string.IsNullOrEmpty(pictureFilePath))
            {
                var imageData = MediaUtils.GetFileBytes(pictureFilePath);
                content = MediaUtils.GetMediaJPEGImageBytes(imageData);
            }

            if (content != null)
            {
                Media media = new Media();
                media.Type = (short)MediaType.Image;
                media.Content = content;
                media.Size = media.Content != null ? (int?)media.Content.Length : null;
                media.OriginalFileName = Path.GetFileName(pictureFilePath);

                IOwnedObject ownedObj = entity as IOwnedObject;
                media.Owner = ownedObj != null ? ownedObj.Owner : null;

                IMasterDataObject masterObj = entity as IMasterDataObject;
                media.IsMasterData = masterObj != null && masterObj.IsMasterData;

                var entityWithMediaCollection = entity as IEntityWithMediaCollection;
                if (entityWithMediaCollection != null)
                {
                    entityWithMediaCollection.Media.Add(media);
                }
                else
                {
                    var entityWithMedia = entity as IEntityWithMedia;
                    if (entityWithMedia != null)
                    {
                        entityWithMedia.Media = media;
                    }
                }
            }
            else
            {
                log.Warn("The file name '{0}' is not a valid image file.", Path.GetFileName(pictureFilePath));
                return false;
            }

            return true;
        }

        /// <summary>
        /// Checks if an entity supports adding an image
        /// </summary>
        /// <param name="entity">The entity to check</param>
        /// <param name="amountPicturesToAdd">The amount of pictures to add</param>
        /// <returns>True is the entity supports adding an image, False otherwise</returns>
        private bool CanAddPictures(object entity, int amountPicturesToAdd)
        {
            // Check if can add any pictures to the entity
            var entityWithMediaCollection = entity as IEntityWithMediaCollection;
            if (entityWithMediaCollection != null)
            {
                var picturesContained = entityWithMediaCollection.Media.Where(m => m.Type == (short)MediaType.Image).Count();
                if (picturesContained + amountPicturesToAdd <= Constants.NumberOfMaxAllowedPictures)
                {
                    return true;
                }
            }
            else
            {
                var entityWithMedia = entity as IEntityWithMedia;
                if (entityWithMedia != null && entityWithMedia.Media == null && amountPicturesToAdd <= 1)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Computes an message from a <see cref="BomImportErrors"/> instance. 
        /// It contains a list of errors for each entity imported.
        /// </summary>
        /// <param name="importErrors">The class congaing the bom import errors</param>
        /// <returns>The error message</returns>
        private string ComputeErrorMessage(BomImportErrors importErrors)
        {
            if (importErrors == null)
            {
                return string.Empty;
            }

            var errorMessage = string.Empty;
            var entityErrors = importErrors.BomImportEntityErrors;
            foreach (var item in entityErrors)
            {
                var entityName = EntityUtils.GetEntityName(item.Entity);

                if (!string.IsNullOrWhiteSpace(errorMessage))
                {
                    errorMessage += Environment.NewLine;
                }

                errorMessage += string.Format(LocalizedResources.BomImport_EntityImportErrors, entityName) + Environment.NewLine;
                errorMessage += string.Format("- {0} ", LocalizedResources.BomImport_InvalidPictures);
                errorMessage += string.Join(", ", item.InvalidPictures);
            }

            return errorMessage;
        }

        #endregion

        #region Set Default Values for New Assembly

        /// <summary>
        /// Sets the default values.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <param name="parent">The parent.</param>
        /// <param name="isMasterData">if set to <c>true</c> [is master data].</param>
        /// <param name="dataManager">The data manager.</param>
        private void SetDefaultValues(Assembly assembly, Assembly parent, bool isMasterData, IDataSourceManager dataManager)
        {
            if (assembly == null)
            {
                throw new ArgumentNullException("assembly", "The default values cannot be set on a null assembly.");
            }

            if (isMasterData == false && parent == null)
            {
                throw new ArgumentNullException("parent", "The default values cannot be set on an assembly with a null assembly parent.");
            }

            // Extract from the parent the data to be used for defaults when creating the assembly.
            string parentCountry = null;
            string parentState = null;

            if (!isMasterData)
            {
                if (parent != null)
                {
                    parentCountry = parent.AssemblingCountry;
                    parentState = parent.AssemblingSupplier;

                    assembly.YearlyProductionQuantity = parent.YearlyProductionQuantity;
                    assembly.LifeTime = parent.LifeTime;

                    assembly.ParentAssembly = parent;
                    if (parent.OverheadSettings != null)
                    {
                        assembly.OverheadSettings = parent.OverheadSettings.Copy();
                    }
                }
            }

            this.SetDefaultCountry(assembly, parentCountry, parentState, dataManager);
            this.SetDefaultBasicSetting(assembly, dataManager);
            this.SetPropertiesDefaults(assembly);

            // Set the master data flag and the owner at the end so they are applied to all sub-objects.
            assembly.SetIsMasterData(isMasterData);
            if (!assembly.IsMasterData)
            {
                User owner = dataManager.UserRepository.GetById(SecurityManager.Instance.CurrentUser.Guid, false);
                assembly.SetOwner(owner);
            }

            // Determine the position of the assembly to create in its parent Assembly.          
            if (parent.Subassemblies != null)
            {
                assembly.Index = parent.Subassemblies.Max(a => a.Index).GetValueOrDefault(-1) + 1;
            }
        }

        /// <summary>
        /// Sets the default values.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <param name="parentProj">The parent proj.</param>
        /// <param name="isMasterData">if set to <c>true</c> [is master data].</param>
        /// <param name="dataManager">The data manager.</param>
        private void SetDefaultValues(Assembly assembly, Project parentProj, bool isMasterData, IDataSourceManager dataManager)
        {
            // Extract from the parent the data to be used for defaults when creating the assembly.
            string parentCountry = null;
            string parentState = null;

            if (!isMasterData)
            {
                if (parentProj != null)
                {
                    if (parentProj.Customer != null)
                    {
                        parentCountry = parentProj.Customer.Country;
                        parentState = parentProj.Customer.State;
                    }

                    assembly.YearlyProductionQuantity = parentProj.YearlyProductionQuantity;
                    assembly.LifeTime = parentProj.LifeTime;

                    assembly.Project = parentProj;
                    if (parentProj.OverheadSettings != null)
                    {
                        assembly.OverheadSettings = parentProj.OverheadSettings.Copy();
                    }
                }
            }

            this.SetDefaultCountry(assembly, parentCountry, parentState, dataManager);
            this.SetDefaultBasicSetting(assembly, dataManager);
            this.SetPropertiesDefaults(assembly);

            // Set the master data flag and the owner at the end so they are applied to all sub-objects.
            assembly.SetIsMasterData(isMasterData);
            if (!assembly.IsMasterData)
            {
                User owner = dataManager.UserRepository.GetById(SecurityManager.Instance.CurrentUser.Guid, false);
                assembly.SetOwner(owner);
            }

            parentProj = this.DataContext.ProjectRepository.GetProjectIncludingTopLevelChildren(parentProj.Guid);
            if (parentProj != null)
            {
                int indexCountOldAssu = parentProj.Assemblies.Max(a => a.Index).GetValueOrDefault(-1) + 1;
                assembly.Index = indexCountOldAssu;
            }
        }

        /// <summary>
        /// Sets the default basic setting.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <param name="dataManager">The data manager.</param>
        private void SetDefaultBasicSetting(Assembly assembly, IDataSourceManager dataManager)
        {
            try
            {
                BasicSetting settings = dataManager.BasicSettingsRepository.GetBasicSettings();
                assembly.BatchSizePerYear = settings.PartBatch;
                assembly.AssetRate = settings.AssetRate;
            }
            catch (DataAccessException ex)
            {
                log.ErrorException("Failed to load the basic settings.", ex);
            }
        }

        /// <summary>
        /// Sets the default country.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <param name="parentCountry">The parent country.</param>
        /// <param name="parentState">State of the parent.</param>
        /// <param name="dataManager">The data manager.</param>
        private void SetDefaultCountry(Assembly assembly, string parentCountry, string parentState, IDataSourceManager dataManager)
        {
            try
            {
                if (parentCountry != null)
                {
                    parentCountry = parentCountry.Trim();
                }

                if (parentState != null)
                {
                    parentState = parentState.Trim();
                }

                // Use the country settings of the parent's country or state as default.
                Country country = dataManager.CountryRepository.GetByName(parentCountry, true);
                if (country != null)
                {
                    assembly.AssemblingCountry = country.Name;
                    if (string.IsNullOrWhiteSpace(parentState))
                    {
                        assembly.CountrySettings = country.CountrySetting.Copy();
                    }
                    else
                    {
                        CountryState state = country.States.FirstOrDefault(s => string.Compare(s.Name, parentState, StringComparison.OrdinalIgnoreCase) == 0);
                        if (state != null)
                        {
                            assembly.AssemblingSupplier = state.Name;
                            assembly.CountrySettings = state.CountrySettings.Copy();
                        }
                        else
                        {
                            assembly.AssemblingSupplier = null;
                            assembly.CountrySettings = country.CountrySetting.Copy();
                        }
                    }
                }
                else
                {
                    assembly.AssemblingCountry = null;
                    assembly.AssemblingSupplier = null;
                }
            }
            catch (DataAccessException ex)
            {
                log.ErrorException("A data access error occurred.", ex);
            }
        }

        /// <summary>
        /// Sets the defaults.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        private void SetPropertiesDefaults(Assembly assembly)
        {
            // Initialize the properties for which no condition is required.
            assembly.CalculationAccuracy = PartCalculationAccuracy.FineCalculation;
            assembly.CalculationApproach = null;
            assembly.CalculationVariant = CostCalculatorFactory.LatestVersion;
            assembly.DevelopmentCost = 0m;
            assembly.ProjectInvest = 0m;
            assembly.OtherCost = 0m;
            assembly.PackagingCost = 0m;
            assembly.LogisticCost = 0m;
            assembly.TransportCost = 0m;
            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = string.Empty;
            assembly.Manufacturer = manufacturer;

            assembly.Process = new Process();

            if (assembly.OverheadSettings == null)
            {
                assembly.OverheadSettings = new OverheadSetting();
            }

            if (assembly.CountrySettings == null)
            {
                assembly.CountrySettings = new CountrySetting();
            }
        }

        #endregion

        #region Set Default Values for New RawMaterial

        /// <summary>
        /// Sets the default values.
        /// </summary>
        /// <param name="rawMaterial">The raw material.</param>
        /// <param name="parent">The parent.</param>
        /// <param name="isMasterData">if set to <c>true</c> [is master data].</param>
        /// <param name="dataManager">The data manager.</param>
        private void SetDefaultValues(RawMaterial rawMaterial, Part parent, bool isMasterData, IDataSourceManager dataManager)
        {
            if (rawMaterial == null)
            {
                throw new ArgumentNullException("rawMaterial", "The default values cannot be set on a null rawMaterial.");
            }

            if (isMasterData == false && parent == null)
            {
                throw new ArgumentNullException("parentAssy", "The default values cannot be set on a part with a null assembly parent.");
            }

            rawMaterial.Part = parent;
            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = string.Empty;
            rawMaterial.Manufacturer = manufacturer;

            if (!rawMaterial.IsMasterData)
            {
                User owner = dataManager.UserRepository.GetById(SecurityManager.Instance.CurrentUser.Guid, false);
                rawMaterial.SetOwner(owner);
            }
        }

        #endregion

        #region Set Default Values for New Commodity

        /// <summary>
        /// Sets the default values.
        /// </summary>
        /// <param name="commodity">The raw material.</param>
        /// <param name="parent">The parent.</param>
        /// <param name="isMasterData">if set to <c>true</c> [is master data].</param>
        /// <param name="dataManager">The data manager.</param>
        private void SetDefaultValues(Commodity commodity, Part parent, bool isMasterData, IDataSourceManager dataManager)
        {
            if (commodity == null)
            {
                throw new ArgumentNullException("rawMaterial", "The default values cannot be set on a null rawMaterial.");
            }

            if (isMasterData == false && parent == null)
            {
                throw new ArgumentNullException("parentAssy", "The default values cannot be set on a part with a null assembly parent.");
            }

            commodity.Part = parent;
            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = string.Empty;
            commodity.Manufacturer = manufacturer;

            if (!commodity.IsMasterData)
            {
                User owner = dataManager.UserRepository.GetById(SecurityManager.Instance.CurrentUser.Guid, false);
                commodity.SetOwner(owner);
            }
        }

        #endregion

        #region Set Default Values for New Part

        /// <summary>
        /// Sets the default values.
        /// </summary>
        /// <param name="newPart">The new part.</param>
        /// <param name="parentAssy">The parent assy.</param>
        /// <param name="isMasterData">if set to <c>true</c> [is master data].</param>
        /// <param name="dataManager">The data manager.</param>
        private void SetDefaultValues(Part newPart, Assembly parentAssy, bool isMasterData, IDataSourceManager dataManager)
        {
            if (newPart == null)
            {
                throw new ArgumentNullException("newPart", "The default values cannot be set on a null part.");
            }

            if (isMasterData == false && parentAssy == null)
            {
                throw new ArgumentNullException("parentAssy", "The default values cannot be set on a part with a null assembly parent.");
            }

            // Extract from the parent the data to be used for defaults when creating the Part.
            string parentCountry = null;
            string parentState = null;

            if (!isMasterData)
            {
                parentCountry = parentAssy.AssemblingCountry;
                parentState = parentAssy.AssemblingSupplier;

                newPart.YearlyProductionQuantity = parentAssy.YearlyProductionQuantity;
                newPart.LifeTime = parentAssy.LifeTime;
                isMasterData = parentAssy.IsMasterData;

                newPart.Assembly = parentAssy;
                if (parentAssy.OverheadSettings != null)
                {
                    newPart.OverheadSettings = parentAssy.OverheadSettings.Copy();
                }
            }

            this.SetDefaultCountry(newPart, parentCountry, parentState, dataManager);
            this.SetDefaultBasicSetting(newPart, dataManager);
            this.SetPropertiesDefaults(newPart);

            // Set the master data flag and the owner at the end so they are applied to all sub-objects.
            newPart.SetIsMasterData(isMasterData);

            if (!newPart.IsMasterData)
            {
                User owner = dataManager.UserRepository.GetById(SecurityManager.Instance.CurrentUser.Guid, false);
                newPart.SetOwner(owner);
                newPart.CalculatorUser = owner;
            }

            // Determine the position of the assembly to create in its parent Assembly.          
            if (parentAssy.Subassemblies != null)
            {
                newPart.Index = parentAssy.Subassemblies.Max(a => a.Index).GetValueOrDefault(-1) + 1;
            }
        }

        /// <summary>
        /// Sets the default values.
        /// </summary>
        /// <param name="newPart">The new part.</param>
        /// <param name="parentProj">The parent proj.</param>
        /// <param name="isMasterData">if set to <c>true</c> [is master data].</param>
        /// <param name="dataManager">The data manager.</param>
        private void SetDefaultValues(Part newPart, Project parentProj, bool isMasterData, IDataSourceManager dataManager)
        {
            if (newPart == null)
            {
                throw new ArgumentNullException("newPart", "The default values cannot be set on a null part.");
            }

            if (isMasterData == false && parentProj == null)
            {
                throw new ArgumentNullException("parentAssy", "The default values cannot be set on a part with a null project parent.");
            }

            // Extract from the parent the data to be used for defaults when creating the Part.
            string parentCountry = null;
            string parentState = null;

            if (!isMasterData)
            {
                if (parentProj.Customer != null)
                {
                    parentCountry = parentProj.Customer.Country;
                    parentState = parentProj.Customer.State;
                }

                newPart.YearlyProductionQuantity = parentProj.YearlyProductionQuantity;
                newPart.LifeTime = parentProj.LifeTime;
                isMasterData = false;

                newPart.Project = parentProj;
                if (parentProj.OverheadSettings != null)
                {
                    newPart.OverheadSettings = parentProj.OverheadSettings.Copy();
                }
            }

            this.SetDefaultCountry(newPart, parentCountry, parentState, dataManager);
            this.SetDefaultBasicSetting(newPart, dataManager);
            this.SetPropertiesDefaults(newPart);

            // Set the master data flag and the owner at the end so they are applied to all sub-objects.
            newPart.SetIsMasterData(isMasterData);

            if (!newPart.IsMasterData)
            {
                User owner = dataManager.UserRepository.GetById(SecurityManager.Instance.CurrentUser.Guid, false);
                newPart.SetOwner(owner);
                newPart.CalculatorUser = owner;
            }

            parentProj = this.DataContext.ProjectRepository.GetProjectIncludingTopLevelChildren(parentProj.Guid);
            if (parentProj != null)
            {
                int indexCountOldAssu = parentProj.Assemblies.Max(a => a.Index).GetValueOrDefault(-1) + 1;
                newPart.Index = indexCountOldAssu;
            }
        }

        /// <summary>
        /// Sets the default country.
        /// </summary>
        /// <param name="newPart">The new part.</param>
        /// <param name="parentCountry">The parent country.</param>
        /// <param name="parentState">State of the parent.</param>
        /// <param name="dataManager">The data context.</param>
        private void SetDefaultCountry(Part newPart, string parentCountry, string parentState, IDataSourceManager dataManager)
        {
            try
            {
                if (parentCountry != null)
                {
                    parentCountry = parentCountry.Trim();
                }

                if (parentState != null)
                {
                    parentState = parentState.Trim();
                }

                // Use the country settings of the parent's country or state as default.
                Country country = dataManager.CountryRepository.GetByName(parentCountry, true);
                if (country != null)
                {
                    newPart.ManufacturingCountry = country.Name;
                    if (string.IsNullOrWhiteSpace(parentState))
                    {
                        newPart.CountrySettings = country.CountrySetting.Copy();
                    }
                    else
                    {
                        CountryState state = country.States.FirstOrDefault(s => string.Compare(s.Name, parentState, StringComparison.OrdinalIgnoreCase) == 0);
                        if (state != null)
                        {
                            newPart.ManufacturingSupplier = state.Name;
                            newPart.CountrySettings = state.CountrySettings.Copy();
                        }
                        else
                        {
                            newPart.ManufacturingSupplier = null;
                            newPart.CountrySettings = country.CountrySetting.Copy();
                        }
                    }
                }
                else
                {
                    newPart.ManufacturingCountry = null;
                    newPart.ManufacturingSupplier = null;
                }
            }
            catch (DataAccessException ex)
            {
                log.ErrorException("A data access error occurred.", ex);
            }
        }

        /// <summary>
        /// Sets the default basic setting.
        /// </summary>
        /// <param name="newPart">The new part.</param>
        /// <param name="dataManager">The data context.</param>
        private void SetDefaultBasicSetting(Part newPart, IDataSourceManager dataManager)
        {
            try
            {
                BasicSetting settings = dataManager.BasicSettingsRepository.GetBasicSettings();
                newPart.BatchSizePerYear = settings.PartBatch;
                newPart.AssetRate = settings.AssetRate;
            }
            catch (DataAccessException ex)
            {
                log.ErrorException("Failed to load the basic settings.", ex);
            }
        }

        /// <summary>
        /// Sets the defaults.
        /// </summary>
        /// <param name="newPart">The new part.</param>
        private void SetPropertiesDefaults(Part newPart)
        {
            // Initialize the properties for which no condition is required.
            newPart.CalculationAccuracy = PartCalculationAccuracy.FineCalculation;
            newPart.CalculationApproach = null;
            newPart.CalculationVariant = CostCalculatorFactory.LatestVersion;
            newPart.DevelopmentCost = 0m;
            newPart.ProjectInvest = 0m;
            newPart.OtherCost = 0m;
            newPart.PackagingCost = 0m;
            newPart.LogisticCost = 0m;
            newPart.TransportCost = 0m;
            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Name = string.Empty;
            newPart.Manufacturer = manufacturer;
            newPart.Process = new Process();

            if (newPart.OverheadSettings == null)
            {
                newPart.OverheadSettings = new OverheadSetting();
            }

            if (newPart.CountrySettings == null)
            {
                newPart.CountrySettings = new CountrySetting();
            }
        }

        #endregion
    }
}
