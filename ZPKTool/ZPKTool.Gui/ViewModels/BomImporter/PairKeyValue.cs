﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// A basic class that is used to store two related objects.
    /// </summary>
    /// <typeparam name="T">The type of the 1st object.</typeparam>
    /// <typeparam name="U">The type of the 2nd object.</typeparam>
    [Serializable]
    public class PairKeyValue<T, U>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PairKeyValue&lt;T, U&gt;"/> class.
        /// </summary>
        public PairKeyValue()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PairKeyValue&lt;T, U&gt;"/> class.
        /// </summary>
        /// <param name="first">The first.</param>
        /// <param name="second">The second.</param>
        public PairKeyValue(T first, U second)
        {
            this.First = first;
            this.Second = second;
        }

        /// <summary>
        /// Gets or sets the 1st object of the pair.
        /// </summary>
        [XmlAttribute("EntityType")]
        public T First { get; set; }

        /// <summary>
        /// Gets or sets the 2nd object of the pair.
        /// </summary>
        [XmlAttribute("PropertyName")]
        public U Second { get; set; }
    }
}
