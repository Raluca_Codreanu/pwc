﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The class containing the error during Bom import for a specify entity
    /// </summary>
    public class BomImportEntityError
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BomImportEntityError"/> class.
        /// </summary>
        /// <param name="entity">The parent entity for which the error belong</param>
        public BomImportEntityError(object entity)
        {
            this.Entity = entity;
            this.InvalidPictures = new List<string>();
        }

        /// <summary>
        /// Gets the entity for which the error occurred
        /// </summary>
        public object Entity { get; private set; }

        /// <summary>
        /// Gets a list containing the invalid pictures during bom import for a specific entity
        /// </summary>
        public ICollection<string> InvalidPictures { get; private set; }

        /// <summary>
        /// Gets a value indicating whether an error exists or not
        /// </summary>
        /// <returns>True if at least one error exist , or false otherwise</returns>
        public bool HasErrors
        {
            get { return this.InvalidPictures.Count > 0; }
        }
    }
}
