﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.Gui.Resources;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The configuration class that defines the mapping between the field name and the file table column name used by the BOM importing.
    /// </summary>
    [Serializable]
    public class BomConfiguration : ObservableObject, IDataErrorInfo, ISerializable
    {
        /// <summary>
        /// The folder of the configuration files.
        /// </summary>
        internal static readonly string ConfigurationFilesFolder = Path.Combine(Constants.ApplicationDataFolderPath, "BomImportConfigurations");

        /// <summary>
        /// The guid of the default configuration.
        /// </summary>
        internal static readonly Guid DefaultConfigurationGuid = new Guid("35bb9eca-3625-4e47-bfc4-8673a527672b");

        /// <summary>
        /// The full path of the default configuration file.
        /// </summary>
        internal static readonly string DefaultConfigurationFilePath = Path.Combine(ConfigurationFilesFolder, PathUtils.SanitizeFileName(DefaultConfigurationGuid + ".xml"));

        #region Fields

        /// <summary>
        /// The id of the configuration.
        /// </summary>
        private Guid id;

        /// <summary>
        /// The name of the configuration.
        /// </summary>
        private string name;

        /// <summary>
        /// The comment of the configuration.
        /// </summary>
        private string comment;

        /// <summary>
        /// The last changed date of the configuration.
        /// </summary>
        private DateTime dateChanged;

        /// <summary>
        /// The path to the pictures folder cell address
        /// </summary>
        private string picturesFolderCellAddress;

        /// <summary>
        /// The path to the pictures column index
        /// </summary>
        private string picturesColumnIndex;

        /// <summary>
        /// The configuration mapping.
        /// </summary>
        private ObservableCollection<BomConfigurationItem> configurationItems;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="BomConfiguration"/> class.
        /// Don't delete used for serialization default constructor.
        /// </summary>
        public BomConfiguration()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BomConfiguration"/> class.
        /// </summary>
        /// <param name="populate">if set to <c>true</c> [populate].</param>
        /// <param name="suggestColumnName">if set to <c>true</c> [suggest column name].</param>
        public BomConfiguration(bool populate, bool suggestColumnName)
        {
            this.Id = Guid.NewGuid();
            this.DateChanged = DateTime.Now;
            this.ConfigurationItems = new ObservableCollection<BomConfigurationItem>();

            if (populate)
            {
                this.PopulateConfiguration(suggestColumnName);
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BomConfiguration"/> class.   
        /// </summary>
        /// <param name="info">The Serialization Info.</param>
        /// <param name="ctxt">The Streaming Context.</param>
        public BomConfiguration(SerializationInfo info, StreamingContext ctxt)
        {
            // Get the values from info and assign them to the appropriate properties
            this.Name = (string)info.GetValue("Name", typeof(string));
            this.Comment = (string)info.GetValue("Comment", typeof(string));
            this.DateChanged = (DateTime)info.GetValue("DateChanged", typeof(DateTime));
            this.PicturesColumnIndex = (string)info.GetValue("PicturesColumnIndex", typeof(string));
            this.PicturesFolderCellAddress = (string)info.GetValue("PicturesFolderCellAddress", typeof(string));
            this.ConfigurationItems = (ObservableCollection<BomConfigurationItem>)info.GetValue("ConfigurationItems", typeof(ObservableCollection<BomConfigurationItem>));
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Guid Id
        {
            get { return this.id; }
            set { this.SetProperty(ref this.id, value, () => this.Id); }
        }

        /// <summary>
        /// Gets or sets the configuration's name.
        /// </summary>         
        [Required(ErrorMessageResourceName = "General_RequiredField", ErrorMessageResourceType = typeof(LocalizedResources))]
        [XmlElement("Name")]
        public string Name
        {
            get { return this.name; }
            set { this.SetProperty(ref this.name, value, () => this.Name); }
        }

        /// <summary>
        /// Gets or sets the configuration's comment.
        /// </summary>        
        [XmlElement("Comment")]
        public string Comment
        {
            get { return this.comment; }
            set { this.SetProperty(ref this.comment, value, () => this.Comment); }
        }

        /// <summary>
        /// Gets or sets the last changed date of the configuration.
        /// </summary>   
        [XmlElement("DateChanged")]
        public DateTime DateChanged
        {
            get { return this.dateChanged; }
            set { this.SetProperty(ref this.dateChanged, value, () => this.DateChanged); }
        }

        /// <summary>
        /// Gets or sets the path to the pictures folder cell address
        /// </summary>
        [RegularExpression(@"[a-zA-Z]+[0-9]+$", ErrorMessageResourceName = "BomImport_PicturesFolderError", ErrorMessageResourceType = typeof(LocalizedResources))]
        [XmlElement("PicturesFolderCellAddress")]
        public string PicturesFolderCellAddress
        {
            get { return this.picturesFolderCellAddress; }
            set { this.SetProperty(ref this.picturesFolderCellAddress, value, () => this.PicturesFolderCellAddress); }
        }

        /// <summary>
        /// Gets or sets the path to the pictures column index
        /// </summary>
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessageResourceName = "BomImport_PicturesColumnError", ErrorMessageResourceType = typeof(LocalizedResources))]
        [XmlElement("PicturesColumnIndex")]
        public string PicturesColumnIndex
        {
            get { return this.picturesColumnIndex; }
            set { this.SetProperty(ref this.picturesColumnIndex, value, () => this.PicturesColumnIndex); }
        }

        /// <summary>
        /// Gets or sets the configuration items.
        /// </summary>
        /// <value>
        /// The configuration items.
        /// </value>
        [XmlArray("ConfigurationItems"), XmlArrayItem("Configuration", typeof(BomConfigurationItem))]
        public ObservableCollection<BomConfigurationItem> ConfigurationItems
        {
            get { return this.configurationItems; }
            set { this.SetProperty(ref this.configurationItems, value, () => this.ConfigurationItems); }
        }

        #endregion

        #region IDataErrorInfo

        /// <summary>
        /// Gets an error message indicating what is wrong with this object.
        /// </summary>
        /// <returns>An error message indicating what is wrong with this object. The default is an empty string ("").</returns>
        [XmlIgnore]
        public string Error
        {
            get
            {
                List<ValidationResult> validationResults = new List<ValidationResult>();
                Validator.TryValidateObject(
                    this,
                    new ValidationContext(this, null, null),
                    validationResults,
                    true);

                return string.Join(Environment.NewLine, validationResults.Select(v => v.ErrorMessage));
            }
        }

        /// <summary>
        /// Gets the error message for the property with the given name.
        /// </summary>
        /// <param name="columnName">Name of the property.</param>
        /// <returns>
        /// The error message for the property. The default is an empty string ("").
        /// </returns>
        public string this[string columnName]
        {
            get
            {
                object propertyValue = null;
                System.Reflection.PropertyInfo propInfo = this.GetType().GetProperty(columnName);
                if (propInfo != null && propInfo.CanRead)
                {
                    propertyValue = propInfo.GetValue(this, null);
                }

                List<ValidationResult> validationResults = new List<ValidationResult>();
                Validator.TryValidateProperty(
                    propertyValue,
                    new ValidationContext(this, null, null) { MemberName = columnName },
                    validationResults);

                return string.Join(Environment.NewLine, validationResults.Select(v => v.ErrorMessage));
            }
        }

        #endregion

        #region Populate configuration

        /// <summary>
        /// Populates the configuration.
        /// </summary>
        /// <param name="suggestColumnName">if set to <c>true</c> [suggest column name].</param>
        private void PopulateConfiguration(bool suggestColumnName)
        {
            this.ConfigurationItems.AddRange(GetCurrentConfigurationItems(suggestColumnName));
        }

        /// <summary>
        /// Gets the current configuration items.
        /// </summary>
        /// <param name="suggestColumnName">if set to true, [suggest column name].</param>
        /// <returns>
        /// List with current system BOM configuration items.
        /// </returns>
        private List<BomConfigurationItem> GetCurrentConfigurationItems(bool suggestColumnName)
        {
            List<BomConfigurationItem> currentVersionConfigurationItems = new List<BomConfigurationItem>();

            Assembly assembly = new Assembly();
            Part part = new Part();
            Manufacturer manufacturer = new Manufacturer();

            assembly.Manufacturer = manufacturer;
            part.Manufacturer = manufacturer;

            RawMaterial rawMaterial = new RawMaterial();
            rawMaterial.Manufacturer = manufacturer;

            Commodity commodity = new Commodity();
            commodity.Manufacturer = manufacturer;

            // Name
            BomConfigurationItem nameItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => assembly.Name, () => part.Name, () => rawMaterial.Name, () => commodity.Name }, suggestColumnName);
            nameItem.IsMandatory = true;
            currentVersionConfigurationItems.Add(nameItem);

            // Number
            BomConfigurationItem numberItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => assembly.Number, () => part.Number }, suggestColumnName);
            currentVersionConfigurationItems.Add(numberItem);

            // UK Name
            BomConfigurationItem uknameItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => rawMaterial.NameUK }, suggestColumnName);
            currentVersionConfigurationItems.Add(uknameItem);

            // Manufacturer Name
            List<PairKeyValue<string, string>> manufacturerCategories = new List<PairKeyValue<string, string>>();
            var namePropertyManufacturer = ReflectionUtils.GetProperty(() => assembly.Manufacturer.Name);

            PairKeyValue<string, string> assemblyManufacturer = new PairKeyValue<string, string>(assembly.GetType().Name, assembly.Manufacturer.GetType().Name + "." + namePropertyManufacturer.Name);
            manufacturerCategories.Add(assemblyManufacturer);

            PairKeyValue<string, string> partManufacturer = new PairKeyValue<string, string>(part.GetType().Name, part.Manufacturer.GetType().Name + "." + namePropertyManufacturer.Name);
            manufacturerCategories.Add(partManufacturer);

            PairKeyValue<string, string> rawMaterialManufacturer = new PairKeyValue<string, string>(rawMaterial.GetType().Name, rawMaterial.Manufacturer.GetType().Name + "." + namePropertyManufacturer.Name);
            manufacturerCategories.Add(rawMaterialManufacturer);

            PairKeyValue<string, string> commodityManufacturer = new PairKeyValue<string, string>(commodity.GetType().Name, commodity.Manufacturer.GetType().Name + "." + namePropertyManufacturer.Name);
            manufacturerCategories.Add(commodityManufacturer);

            BomConfigurationItem manufacturerNameItem = new BomConfigurationItem(manufacturerCategories, LocalizedResources.BomImport_ManufacturerName, suggestColumnName);
            manufacturerNameItem.IsMandatory = true;
            currentVersionConfigurationItems.Add(manufacturerNameItem);

            // US Name
            BomConfigurationItem usmameItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => rawMaterial.NameUS }, suggestColumnName);
            currentVersionConfigurationItems.Add(usmameItem);

            // Norm Name
            BomConfigurationItem normNameItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => rawMaterial.NormName }, suggestColumnName);
            currentVersionConfigurationItems.Add(normNameItem);

            // Version
            BomConfigurationItem versionItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => assembly.Version, () => part.Version }, suggestColumnName);
            currentVersionConfigurationItems.Add(versionItem);

            // Description
            BomConfigurationItem descriptionItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => assembly.Description, () => part.Description }, suggestColumnName);
            currentVersionConfigurationItems.Add(descriptionItem);

            // Country
            BomConfigurationItem countryItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => assembly.AssemblingCountry, () => part.ManufacturingCountry }, suggestColumnName);
            countryItem.IsMandatory = true;
            currentVersionConfigurationItems.Add(countryItem);

            // Supplier
            BomConfigurationItem supplierItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => assembly.AssemblingSupplier, () => part.ManufacturingSupplier }, suggestColumnName);
            currentVersionConfigurationItems.Add(supplierItem);

            // Accuracy
            BomConfigurationItem accuracyItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => assembly.CalculationAccuracy, () => part.CalculationAccuracy }, suggestColumnName);
            currentVersionConfigurationItems.Add(accuracyItem);

            // Accuracy
            BomConfigurationItem approachItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => assembly.CalculationApproach, () => part.CalculationApproach }, suggestColumnName);
            currentVersionConfigurationItems.Add(approachItem);

            // Status
            BomConfigurationItem statusItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => part.CalculationStatus }, suggestColumnName);
            currentVersionConfigurationItems.Add(statusItem);

            // Part Number
            BomConfigurationItem partNumberItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => commodity.PartNumber }, suggestColumnName);
            currentVersionConfigurationItems.Add(partNumberItem);

            // Amount
            BomConfigurationItem amountItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => commodity.Amount }, suggestColumnName);
            currentVersionConfigurationItems.Add(amountItem);

            // Weight
            BomConfigurationItem weightItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => assembly.Weight, () => part.Weight, () => commodity.Weight }, suggestColumnName);
            currentVersionConfigurationItems.Add(weightItem);

            // Calculation Variant
            BomConfigurationItem calcVariantItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => assembly.CalculationVariant, () => part.CalculationVariant }, suggestColumnName);
            currentVersionConfigurationItems.Add(calcVariantItem);

            // Batch size
            BomConfigurationItem batchSizeItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => assembly.BatchSizePerYear, () => part.BatchSizePerYear }, suggestColumnName);
            currentVersionConfigurationItems.Add(batchSizeItem);

            // Yearly Production Quantity 
            BomConfigurationItem yearlyProductionQuantityItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => assembly.YearlyProductionQuantity, () => part.YearlyProductionQuantity }, suggestColumnName);
            currentVersionConfigurationItems.Add(yearlyProductionQuantityItem);

            // Life time
            BomConfigurationItem lifeTimeItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => assembly.LifeTime, () => part.LifeTime }, suggestColumnName);
            currentVersionConfigurationItems.Add(lifeTimeItem);

            // Purchase Price
            BomConfigurationItem purchasePriceItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => assembly.PurchasePrice, () => part.PurchasePrice }, suggestColumnName);
            currentVersionConfigurationItems.Add(purchasePriceItem);

            // Target Price
            BomConfigurationItem targetPriceItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => assembly.TargetPrice, () => part.TargetPrice }, suggestColumnName);
            currentVersionConfigurationItems.Add(targetPriceItem);

            // Delivery Type
            BomConfigurationItem deliveryTypeItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => assembly.DeliveryType, () => part.DelivertType, () => rawMaterial.DeliveryType }, suggestColumnName);
            currentVersionConfigurationItems.Add(deliveryTypeItem);

            // SBM Active
            BomConfigurationItem sbmActiveItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => assembly.SBMActive, () => part.SBMActive }, suggestColumnName);
            currentVersionConfigurationItems.Add(sbmActiveItem);

            // Asset Rate
            BomConfigurationItem assetRateItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => assembly.AssetRate, () => part.AssetRate }, suggestColumnName);
            currentVersionConfigurationItems.Add(assetRateItem);

            // External
            BomConfigurationItem externalItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => assembly.IsExternal, () => part.IsExternal }, suggestColumnName);
            currentVersionConfigurationItems.Add(externalItem);

            // Price
            BomConfigurationItem priceItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => rawMaterial.Price, () => commodity.Price }, suggestColumnName);
            currentVersionConfigurationItems.Add(priceItem);

            // Part Weight
            BomConfigurationItem partWeightItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => rawMaterial.ParentWeight }, suggestColumnName);
            currentVersionConfigurationItems.Add(partWeightItem);

            // Sprue
            BomConfigurationItem sprueItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => rawMaterial.Sprue }, suggestColumnName);
            currentVersionConfigurationItems.Add(sprueItem);

            // Material Weight
            BomConfigurationItem materialWeightItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => rawMaterial.Quantity }, suggestColumnName);
            currentVersionConfigurationItems.Add(materialWeightItem);

            // Material Loss
            BomConfigurationItem materialLossItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => rawMaterial.Loss }, suggestColumnName);
            currentVersionConfigurationItems.Add(materialLossItem);

            // Material Remarks
            BomConfigurationItem materialRemarksItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => rawMaterial.Remarks, () => commodity.Remarks }, suggestColumnName);
            currentVersionConfigurationItems.Add(materialRemarksItem);

            // Scrap calc
            BomConfigurationItem scrapCalcItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => rawMaterial.ScrapRefundRatio }, suggestColumnName);
            currentVersionConfigurationItems.Add(scrapCalcItem);

            // Reject ratio
            BomConfigurationItem rejectRatioItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => rawMaterial.RejectRatio, () => commodity.RejectRatio }, suggestColumnName);
            currentVersionConfigurationItems.Add(rejectRatioItem);

            // Yield strength
            BomConfigurationItem yieldStrenghtItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => rawMaterial.YieldStrength }, suggestColumnName);
            currentVersionConfigurationItems.Add(yieldStrenghtItem);

            // Rupture strength
            BomConfigurationItem ruptureStrenghtItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => rawMaterial.RuptureStrength }, suggestColumnName);
            currentVersionConfigurationItems.Add(ruptureStrenghtItem);

            // Density
            BomConfigurationItem densityItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => rawMaterial.Density }, suggestColumnName);
            currentVersionConfigurationItems.Add(densityItem);

            // Max Elongation
            BomConfigurationItem maxElongationItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => rawMaterial.MaxElongation }, suggestColumnName);
            currentVersionConfigurationItems.Add(maxElongationItem);

            // Glass Trans.
            BomConfigurationItem glassTranItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => rawMaterial.GlassTransitionTemperature }, suggestColumnName);
            currentVersionConfigurationItems.Add(glassTranItem);

            // Rx
            BomConfigurationItem rxpropItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => rawMaterial.Rx }, suggestColumnName);
            currentVersionConfigurationItems.Add(rxpropItem);

            // Rm
            BomConfigurationItem rmstrenghtItem = new BomConfigurationItem(new List<Expression<Func<object>>> { () => rawMaterial.Rm }, suggestColumnName);
            currentVersionConfigurationItems.Add(rmstrenghtItem);

            return currentVersionConfigurationItems;
        }

        #endregion

        #region Update configuration

        /// <summary>
        /// Updates the configuration with the current properties (mapping properties). 
        /// Removes the properties that are not found anymore and adds the new ones.
        /// </summary>
        public void UpdateConfiguration()
        {
            List<BomConfigurationItem> actualVersionItems = GetCurrentConfigurationItems(false);

            // Remove old properties.       
            foreach (var item in this.ConfigurationItems.ToList())
            {
                bool isFound = false;
                foreach (var actual in actualVersionItems)
                {
                    if (item.Equals(actual))
                    {
                        isFound = true;
                    }
                }

                if (!isFound)
                {
                    this.ConfigurationItems.Remove(item);
                }
            }

            // Add new properties
            foreach (var actual in actualVersionItems.ToList())
            {
                bool isFound = false;
                foreach (var item in this.ConfigurationItems)
                {
                    if (actual.Equals(item))
                    {
                        isFound = true;
                    }
                }

                if (!isFound)
                {
                    this.ConfigurationItems.Add(actual);
                }
            }
        }
        #endregion

        #region ISerializable

        /// <summary>
        /// Populates a <see cref="T:System.Runtime.Serialization.SerializationInfo"/> with the data needed to serialize the target object.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo"/> to populate with data.</param>
        /// <param name="context">The destination (see <see cref="T:System.Runtime.Serialization.StreamingContext"/>) for this serialization.</param>
        /// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Name", this.Name);
            info.AddValue("Comment", this.Comment);
            info.AddValue("DateChanged", this.DateChanged);
            info.AddValue("ConfigurationItems", this.ConfigurationItems);
            info.AddValue("PicturesFolderCellAddress", this.PicturesFolderCellAddress);
            info.AddValue("PicturesColumnIndex", this.PicturesColumnIndex);
        }

        #endregion

        /// <summary>
        /// Perform a deep Copy of the object.
        /// </summary>
        /// <typeparam name="T">The type of object being copied.</typeparam>
        /// <param name="source">The object instance to copy.</param>
        /// <returns>
        /// The copied object.
        /// </returns>
        internal static T Clone<T>(T source)
        {
            if (!typeof(T).IsSerializable)
            {
                throw new ArgumentException("The type must be serializable.", "source");
            }

            // Don't serialize a null object, simply return the default for that object
            if (ReferenceEquals(source, null))
            {
                return default(T);
            }

            IFormatter formatter = new BinaryFormatter();
            Stream stream = new MemoryStream();
            using (stream)
            {
                formatter.Serialize(stream, source);
                stream.Seek(0, SeekOrigin.Begin);
                return (T)formatter.Deserialize(stream);
            }
        }

        /// <summary>
        /// Gets the full path of configuration.
        /// </summary>
        /// <param name="theConfiguration">The configuration.</param>
        /// <returns>The full file path of the configuration.</returns>
        internal static string GetFullPathOfConfiguration(BomConfiguration theConfiguration)
        {
            if (theConfiguration == null)
            {
                throw new ArgumentNullException("theConfiguration");
            }

            var resultFilePath = Path.Combine(ConfigurationFilesFolder, PathUtils.SanitizeFileName(theConfiguration.Id + ".xml"));

            return resultFilePath;
        }
    }
}
