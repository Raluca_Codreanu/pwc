﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The class containing the error during Bom import
    /// </summary>
    public class BomImportErrors
    {
        /// <summary>
        /// The list containing the errors during import regarding entities
        /// </summary>
        private List<BomImportEntityError> bomImportEntityErrors = new List<BomImportEntityError>();

        /// <summary>
        /// Initializes a new instance of the <see cref="BomImportErrors"/> class.
        /// </summary>
        public BomImportErrors()
        {
        }

        /// <summary>
        /// Gets the list containing the errors during import regarding entities
        /// </summary>
        public IEnumerable<BomImportEntityError> BomImportEntityErrors
        {
            get
            {
                return this.bomImportEntityErrors;
            }
        }

        /// <summary>
        /// Gets a value indicating whether an error exists or not
        /// </summary>
        /// <returns>True if at least one error exist , or false otherwise</returns>
        public bool HasErrors
        {
            get { return this.bomImportEntityErrors.Count > 0; }
        }

        /// <summary>
        /// Gets the <see cref="BomImportEntityError"/> instance corresponding to the provided entity
        /// </summary>
        /// <param name="entity">The entity that the errors belong</param>
        /// <returns>The <see cref="BomImportEntityError"/> instance for entity provided</returns>
        public BomImportEntityError this[object entity]
        {
            get
            {
                var error = this.BomImportEntityErrors.FirstOrDefault(e => e.Entity == entity);
                if (error == null)
                {
                    error = new BomImportEntityError(entity);
                    this.bomImportEntityErrors.Add(error);
                }

                return error;
            }
        }
    }
}
