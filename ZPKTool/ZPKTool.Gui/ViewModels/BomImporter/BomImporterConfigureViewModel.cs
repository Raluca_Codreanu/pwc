﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using OfficeOpenXml;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The ViewModel class for the Select Adapter View.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class BomImporterConfigureViewModel : ViewModel
    {
        #region Fields

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The import file type.
        /// </summary>
        private BomImporterFileType fileType;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="BomImporterConfigureViewModel"/> class.
        /// </summary>
        /// <param name="messenger">The messenger.</param>
        /// <param name="windowService">The window service.</param>
        [ImportingConstructor]
        public BomImporterConfigureViewModel(IMessenger messenger, IWindowService windowService)
        {
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("windowService", windowService);

            this.messenger = messenger;
            this.windowService = windowService;

            TokenizedHeaders = new DataProperty<Dictionary<string, string>>(this, "TokenizedHeaders");
            TokenizedHeaders.Value = new Dictionary<string, string>();

            this.FirstCellTableAddress = new DataProperty<string>(this, "FirstCellTableAddress");
            this.Type = new DataProperty<string>(this, "Type");
            this.Level = new DataProperty<string>(this, "Level");

            this.TableHeadersType = new DataProperty<ObservableCollection<string>>(this, "TableHeadersType");
            this.TableHeadersType.Value = new ObservableCollection<string>();

            this.TableHeadersLevel = new DataProperty<ObservableCollection<string>>(this, "TableHeadersLevel");
            this.TableHeadersLevel.Value = new ObservableCollection<string>();

            this.SelectedSheet.ValueChanged += PopulateConfiguration;
            this.FirstCellTableAddress.ValueChanged += PopulateConfiguration;
            this.ValueSeparator.ValueChanged += PopulateConfiguration;

            this.ValueSeparators.Value = new ObservableCollection<Tuple<string, string>>();
            List<Tuple<string, string>> valueSeparators = new List<Tuple<string, string>>();
            Tuple<string, string> semicolon = new Tuple<string, string>(LocalizedResources.General_Semicolon + " (;)", ";");
            Tuple<string, string> comma = new Tuple<string, string>(LocalizedResources.General_Comma + " (,)", ",");
            valueSeparators.Add(semicolon);
            valueSeparators.Add(comma);
            this.ValueSeparators.Value.AddRange(valueSeparators);
            this.ValueSeparator.Value = this.ValueSeparators.Value.FirstOrDefault();

            this.TableData = new List<List<object>>();

            Currencies = new DataProperty<ObservableCollection<Currency>>(this, "Currencies");
            Currency = new DataProperty<Currency>(this, "Currency");
            WeightUnits = new DataProperty<ObservableCollection<MeasurementUnit>>(this, "WeightUnits");
            WeightUnit = new DataProperty<MeasurementUnit>(this, "WeightUnit");

            this.PicturesColumnIndex = -1;
            this.PicturesFolderPath = string.Empty;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the selected configuration.
        /// </summary>
        public VMProperty<BomConfiguration> Configuration { get; private set; }

        /// <summary>
        /// Gets the sheets.
        /// </summary>
        public VMProperty<ObservableCollection<object>> Sheets { get; private set; }

        /// <summary>
        /// Gets the selected sheet.
        /// </summary>
        public VMProperty<object> SelectedSheet { get; private set; }

        /// <summary>
        /// Gets the sheet display member path.
        /// </summary>
        public VMProperty<object> SheetDisplayMemberPath { get; private set; }

        /// <summary>
        /// Gets the first cell table address.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_XlsAddress", ErrorMessageResourceType = typeof(LocalizedResources))]
        [RegularExpression(@"[a-zA-Z]+[0-9]+$", ErrorMessageResourceName = "BomImport_RequiredValid_XlsAddress", ErrorMessageResourceType = typeof(LocalizedResources))]
        public DataProperty<string> FirstCellTableAddress { get; private set; }

        /// <summary>
        /// Gets the level.
        /// </summary>
        [Required(ErrorMessageResourceName = "General_RequiredField", ErrorMessageResourceType = typeof(LocalizedResources))]
        public DataProperty<string> Level { get; private set; }

        /// <summary>
        /// Gets the type.
        /// </summary>
        [Required(ErrorMessageResourceName = "General_RequiredField", ErrorMessageResourceType = typeof(LocalizedResources))]
        public DataProperty<string> Type { get; private set; }

        /// <summary>
        /// Gets the weight unit.
        /// </summary>
        [Required(ErrorMessageResourceName = "General_RequiredField", ErrorMessageResourceType = typeof(LocalizedResources))]
        public DataProperty<MeasurementUnit> WeightUnit { get; private set; }

        /// <summary>
        /// Gets the currency.
        /// </summary>
        [Required(ErrorMessageResourceName = "General_RequiredField", ErrorMessageResourceType = typeof(LocalizedResources))]
        public DataProperty<Currency> Currency { get; private set; }

        /// <summary>
        /// Gets or sets the table data.
        /// </summary>
        /// <value>
        /// The table data.
        /// </value>
        public List<List<object>> TableData { get; set; }

        /// <summary>
        /// Gets the table headers.
        /// </summary>
        public DataProperty<ObservableCollection<string>> TableHeadersType { get; private set; }

        /// <summary>
        /// Gets the table headers level.
        /// </summary>
        public DataProperty<ObservableCollection<string>> TableHeadersLevel { get; private set; }

        /// <summary>
        /// Gets the weight units.
        /// </summary>
        public DataProperty<ObservableCollection<MeasurementUnit>> WeightUnits { get; private set; }

        /// <summary>
        /// Gets the currencies.
        /// </summary>
        public DataProperty<ObservableCollection<Currency>> Currencies { get; private set; }

        /// <summary>
        /// Gets or sets the type of the file.
        /// </summary>
        /// <value>
        /// The type of the file.
        /// </value>
        public BomImporterFileType FileType
        {
            get
            {
                return this.fileType;
            }

            set
            {
                if (this.fileType != value)
                {
                    if (value == BomImporterFileType.Csv)
                    {
                        this.IsCsvFile.Value = true;
                        this.IsExcelFile.Value = false;
                    }
                    else if (value == BomImporterFileType.Xlsx || value == BomImporterFileType.Xls)
                    {
                        this.IsCsvFile.Value = false;
                        this.IsExcelFile.Value = true;
                    }

                    this.fileType = value;
                    OnPropertyChanged(() => this.FileType);
                }
            }
        }

        /// <summary>
        /// Gets the is XLSX file.
        /// </summary>
        public VMProperty<bool> IsExcelFile { get; private set; }

        /// <summary>
        /// Gets the is CSV file.
        /// </summary>
        public VMProperty<bool> IsCsvFile { get; private set; }

        /// <summary>
        /// Gets the value separators.
        /// </summary>
        public VMProperty<ObservableCollection<Tuple<string, string>>> ValueSeparators { get; private set; }

        /// <summary>
        /// Gets the value separator.
        /// </summary>
        public VMProperty<Tuple<string, string>> ValueSeparator { get; private set; }

        /// <summary>
        /// Gets the column names and their tokenized form.
        /// </summary>
        public VMProperty<Dictionary<string, string>> TokenizedHeaders { get; private set; }

        /// <summary>
        /// Gets or sets the file path.
        /// </summary>
        /// <value>
        /// The file path.
        /// </value>
        public string FilePath { get; set; }

        /// <summary>
        /// Gets the error message.
        /// </summary>
        public VMProperty<string> ErrorMessage { get; private set; }

        /// <summary>
        /// Gets or sets the data context.
        /// </summary>
        public IDataSourceManager DataContext { get; set; }

        /// <summary>
        /// Gets or sets the path to the folder containing the pictures to be imported
        /// </summary>
        public string PicturesFolderPath { get; set; }

        /// <summary>
        /// Gets or sets the column containing the filenames of pictures to be imported
        /// </summary>
        public int PicturesColumnIndex { get; set; }

        /// <summary>
        /// Gets or sets the parent project base currency of the entity were importing. Used for converting the imported file's data.
        /// </summary>
        public Currency ParentProjectBaseCurrency { get; set; }

        #endregion

        /// <summary>
        /// Tokenizes the string.
        /// </summary>
        /// <param name="theSourceString">The source string.</param>
        /// <returns>The tokenized form of the <see cref="theSourceString"/>.</returns>
        public static string TokenizeString(string theSourceString)
        {
            var result = string.Empty;

            if (!string.IsNullOrWhiteSpace(theSourceString))
            {
                result = string.Join(string.Empty, theSourceString.Where(char.IsLetterOrDigit)).ToLowerInvariant();
            }

            return result;
        }

        #region Populate table

        /// <summary>
        /// Selected value changed event.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="oldPropValue">The old prop value.</param>
        public void PopulateConfiguration(object source, object oldPropValue)
        {
            bool populate = false;

            if (File.Exists(FilePath))
            {
                if (this.FileType == BomImporterFileType.Xlsx || this.FileType == BomImporterFileType.Xls)
                {
                    var errorMessage = TryPopulateTableFromExcel();

                    if (!string.IsNullOrWhiteSpace(errorMessage))
                    {
                        this.ErrorMessage.Value = errorMessage;
                        this.TableHeadersType.Value.Clear();
                        this.TableHeadersLevel.Value.Clear();

                        return;
                    }
                    else
                    {
                        populate = true;
                    }
                }
                else if (this.FileType == BomImporterFileType.Csv)
                {
                    var errorMessage = TryPopulateTableFromCsv(this.FilePath);
                    if (!string.IsNullOrWhiteSpace(errorMessage))
                    {
                        this.ErrorMessage.Value = errorMessage;
                        this.TableHeadersType.Value.Clear();
                        this.TableHeadersLevel.Value.Clear();

                        return;
                    }
                    else
                    {
                        populate = true;
                    }
                }

                if (populate)
                {
                    this.ErrorMessage.Value = string.Empty;

                    // Populate Table headers.
                    this.TableHeadersType.Value.Clear();
                    this.TableHeadersLevel.Value.Clear();

                    if (this.TableData.Count > 0)
                    {
                        var columns = (from header in this.TableData.FirstOrDefault()
                                       select header.ToString()).ToList();

                        this.TableHeadersType.Value.AddRange(columns);
                        this.TableHeadersLevel.Value.AddRange(columns);
                    }

                    this.Type.Value = this.TableHeadersType.Value.FirstOrDefault(p => p != null && p.Contains(LocalizedResources.BomImport_GeneralType));
                    this.Level.Value = this.TableHeadersLevel.Value.FirstOrDefault(p => p != null && p.Contains(LocalizedResources.BomImport_GeneralLevel));
                }
            }
        }

        /// <summary>
        /// Gets the sheets from the xls file.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <returns>True if the sheets could be set, false otherwise.</returns>
        public bool TrySetSheets(string filePath)
        {
            try
            {
                if (this.FileType == BomImporterFileType.Xlsx)
                {
                    this.SheetDisplayMemberPath.Value = "Name";

                    using (Stream fileStream = new FileStream(filePath, FileMode.Open))
                    {
                        using (ExcelPackage package = new ExcelPackage(fileStream))
                        {
                            ExcelWorkbook workBook = package.Workbook;

                            this.Sheets.Value = new ObservableCollection<object>();
                            for (int index = 1; index <= workBook.Worksheets.Count; index++)
                            {
                                var sheet = workBook.Worksheets[index];
                                this.Sheets.Value.Add(sheet);
                            }
                        }
                    }
                }
                else if (this.FileType == BomImporterFileType.Xls)
                {
                    this.SheetDisplayMemberPath.Value = "SheetName";

                    using (Stream fileStream = new FileStream(filePath, FileMode.Open))
                    {
                        HSSFWorkbook workbook = new HSSFWorkbook(fileStream);

                        this.Sheets.Value = new ObservableCollection<object>();
                        for (int index = 0; index < workbook.NumberOfSheets; index++)
                        {
                            var sheet = workbook.GetSheetAt(index);
                            this.Sheets.Value.Add(sheet);
                        }
                    }
                }

                this.SelectedSheet.Value = this.Sheets.Value.FirstOrDefault();

                return true;
            }
            catch (IOException ex)
            {
                log.ErrorException("IO error occurred while saving the configuration.", ex);
                this.windowService.MessageDialogService.Show(LocalizedResources.General_FileIOError, MessageDialogType.Error);
                return false;
            }
            catch (UnauthorizedAccessException ex)
            {
                log.ErrorException("IO error occurred while saving the configuration.", ex);
                this.windowService.MessageDialogService.Show(LocalizedResources.General_FileAccessError, MessageDialogType.Error);
                return false;
            }
            catch (Exception ex)
            {
                log.ErrorException("Could not load data from selected file.", ex);
                this.windowService.MessageDialogService.Show(LocalizedResources.BomImport_CouldNotLoadDataFromFile, MessageDialogType.Error);
                return false;
            }
        }

        /// <summary>
        /// Imports the data from XLS.
        /// </summary>
        /// <returns>The proper error message or empty string if there were no issues.</returns>
        public string TryPopulateTableFromExcel()
        {
            var errorMessage = ExcelImporterHelper.TryPopulateTableFromExcel(this.SelectedSheet.Value, this.TableData, this.FirstCellTableAddress.Value);

            if (string.IsNullOrWhiteSpace(errorMessage))
            {
                var headersData = this.TableData.FirstOrDefault();
                errorMessage = TokenizeHeaders(headersData);
                if (!string.IsNullOrWhiteSpace(errorMessage))
                {
                    return errorMessage;
                }
            }

            return errorMessage;
        }

        /// <summary>
        /// Tries the populate table from CSV file.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <returns>The proper error message if there were problems; otherwise empty strin.</returns>
        public string TryPopulateTableFromCsv(string filePath)
        {
            var errorMessage = string.Empty;
            try
            {
                ExcelWorkbook workBook = new ExcelPackage().Workbook;
                workBook.Worksheets.Add("CSV");
                ExcelWorksheet csvSheet = workBook.Worksheets["CSV"];

                ExcelTextFormat textFormat = new ExcelTextFormat();
                textFormat.TextQualifier = '"';
                textFormat.Delimiter = this.ValueSeparator.Value.Item2[0];
                csvSheet.Cells["A1"].LoadFromText(new FileInfo(filePath), textFormat);

                errorMessage = ExcelImporterHelper.PopulateTableDataFromSheet(csvSheet, 1, 1, this.TableData);
            }
            catch (Exception ex)
            {
                log.Error("Could not populate table data from CSV file.", ex);
                errorMessage = LocalizedResources.Import_ErrorPopulateTable;
            }

            return errorMessage;
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Loads the pictures data configuration from the selected sheet file
        /// </summary>
        public void LoadPicturesConfiguration()
        {
            if (this.IsExcelFile.Value)
            {
                this.PicturesColumnIndex = !string.IsNullOrWhiteSpace(this.Configuration.Value.PicturesColumnIndex) ? ExcelImporterHelper.ConvertColumnAddressToColumnIndex(this.Configuration.Value.PicturesColumnIndex) - 1 : -1;
                this.PicturesFolderPath = string.Empty;

                ExcelWorksheet workSheet2007 = this.SelectedSheet.Value as ExcelWorksheet;
                if (workSheet2007 != null)
                {
                    this.PicturesFolderPath = workSheet2007.Cells[this.Configuration.Value.PicturesFolderCellAddress].Value as string ?? string.Empty;
                }
                else
                {
                    ISheet sheet = this.SelectedSheet.Value as ISheet;
                    if (sheet != null && !string.IsNullOrWhiteSpace(this.Configuration.Value.PicturesFolderCellAddress))
                    {
                        int rowIndex;
                        int columnIndex;
                        ExcelImporterHelper.GetRowAndColumnIndexNPOI(this.Configuration.Value.PicturesFolderCellAddress, out rowIndex, out columnIndex);

                        var path = string.Empty;
                        var pictureFolderRow = sheet.GetRow(rowIndex);
                        if (pictureFolderRow != null)
                        {
                            var pictureFolderCell = pictureFolderRow.GetCell(columnIndex);
                            if (pictureFolderCell != null)
                            {
                                path = pictureFolderCell.StringCellValue;
                            }
                        }

                        this.PicturesFolderPath = path;
                    }
                }
            }
        }

        /// <summary>
        /// Determines whether [contains duplicate columns] [the specified headers data].
        /// </summary>
        /// <param name="theHeaders">The headers data.</param>
        /// <returns>A proper error message if there were issues during the tokenizing, or empty string if there were no issues.</returns>
        private string TokenizeHeaders(IEnumerable<object> theHeaders)
        {
            var result = string.Empty;
            TokenizedHeaders.Value.Clear();

            foreach (var header in theHeaders)
            {
                if (header != null)
                {
                    var tokenizedValue = TokenizeString(header.ToString());

                    if (TokenizedHeaders.Value.ContainsKey(header.ToString()) || TokenizedHeaders.Value.ContainsValue(tokenizedValue))
                    {
                        result = string.Format(CultureInfo.InvariantCulture, LocalizedResources.BomImport_ErrorDuplicateColumnName, header);
                    }
                    else
                    {
                        TokenizedHeaders.Value.Add(header.ToString(), tokenizedValue);
                    }
                }
            }

            return result;
        }

        #endregion
    }
}
