﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using ZPKTool.Common;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Utils;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The configuration item used for defining a mapping: entity field - excel column name.
    /// </summary>
    [Serializable]
    public class BomConfigurationItem : ObservableObject, IDataErrorInfo, ISerializable
    {
        #region Fields

        /// <summary>
        /// The category type, representing the entity type.
        /// </summary>
        private List<PairKeyValue<string, string>> category;

        /// <summary>
        /// Indicates if this configuration item is mandatory.
        /// </summary>
        private bool isMandatory;

        /// <summary>
        /// The column name;
        /// </summary>
        private string columnName;

        /// <summary>
        /// The tokenized column name.
        /// </summary>
        private string tokenizedColumnName;

        /// <summary>
        /// The column names available for selection. 
        /// </summary>
        private List<string> columnNames;

        /// <summary>
        /// The custom value used instead the value read from the column with the given ColumnName.
        /// </summary>
        private string customValue;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="BomConfigurationItem"/> class.
        /// </summary>
        public BomConfigurationItem()
        {
            this.ColumnNames = new List<string>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BomConfigurationItem"/> class.
        /// </summary>
        /// <param name="categories">The category.</param>
        /// <param name="suggestColumnName">if set to <c>true</c> [suggest column name].</param>
        public BomConfigurationItem(IEnumerable<Expression<Func<object>>> categories, bool suggestColumnName)
        {
            this.Categories = new List<PairKeyValue<string, string>>();
            this.ColumnNames = new List<string>();

            foreach (var cat in categories)
            {
                var propertyInfo = ReflectionUtils.GetProperty(cat);
                this.Categories.Add(new PairKeyValue<string, string>(propertyInfo.DeclaringType.Name, propertyInfo.Name));

                if (string.IsNullOrWhiteSpace(this.ColumnName))
                {
                    if (suggestColumnName)
                    {
                        this.ColumnName = UIUtils.GetLocalizedNameOfProperty(propertyInfo);
                    }
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BomConfigurationItem"/> class.
        /// </summary>
        /// <param name="categories">The categories.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <param name="suggestColumnName">if set to <c>true</c> [suggest column name].</param>
        public BomConfigurationItem(IEnumerable<PairKeyValue<string, string>> categories, string columnName, bool suggestColumnName)
        {
            this.Categories = new List<PairKeyValue<string, string>>();
            this.Categories.AddRange(categories);
            this.ColumnNames = new List<string>();

            if (suggestColumnName)
            {
                this.ColumnName = columnName;
            }
        }

        /// Deserialization constructor.
        /// <summary>
        /// Initializes a new instance of the <see cref="BomConfigurationItem"/> class.
        /// </summary>
        /// <param name="info">The Serialization Info.</param>
        /// <param name="ctxt">The Streaming Context.</param>
        public BomConfigurationItem(SerializationInfo info, StreamingContext ctxt)
        {
            // Get the values from info and assign them to the appropriate properties
            this.Categories = (List<PairKeyValue<string, string>>)info.GetValue("Category", typeof(List<PairKeyValue<string, string>>));
            this.ColumnName = (string)info.GetValue("ColumnName", typeof(string));
            this.CustomValue = (string)info.GetValue("CustomValue", typeof(string));
            this.IsMandatory = (bool)info.GetValue("IsMandatory", typeof(bool));
            ColumnNames = new List<string>();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the categories.
        /// </summary>
        /// <value>
        /// The field.
        /// </value>
        [XmlArray("Categories"), XmlArrayItem("Category", typeof(PairKeyValue<string, string>))]
        public List<PairKeyValue<string, string>> Categories
        {
            get { return this.category; }
            set { this.SetProperty(ref this.category, value, () => this.Categories); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is mandatory.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is mandatory; otherwise, <c>false</c>.
        /// </value>
        [XmlAttribute("IsMandatory")]
        public bool IsMandatory
        {
            get { return this.isMandatory; }
            set { this.SetProperty(ref this.isMandatory, value, () => this.IsMandatory); }
        }

        /// <summary>
        /// Gets or sets the name of the column.
        /// </summary>
        /// <value>
        /// The name of the column.
        /// </value>
        [XmlElement("ColumnName")]
        public string ColumnName
        {
            get
            {
                return this.columnName;
            }

            set
            {
                this.SetProperty(ref this.columnName, value, () => this.ColumnName);
                TokenizedColumnName = BomImporterConfigureViewModel.TokenizeString(columnName);
                ResetCustomValue();
            }
        }

        /// <summary>
        /// Gets or sets the name of the tokenized column.
        /// </summary>
        /// <value>
        /// The name of the tokenized column.
        /// </value>
        public string TokenizedColumnName
        {
            get { return this.tokenizedColumnName; }
            set { this.SetProperty(ref this.tokenizedColumnName, value, () => this.TokenizedColumnName); }
        }
        
        /// <summary>
        /// Gets or sets the column names.
        /// </summary>
        /// <value>
        /// The column names.
        /// </value>
        [XmlIgnore]
        public List<string> ColumnNames
        {
            get { return this.columnNames; }
            set { this.SetProperty(ref this.columnNames, value, () => this.ColumnNames); }
        }

        /// <summary>
        /// Gets or sets the custom value used instead the value read from the column with the given ColumnName.
        /// </summary>
        [XmlElement("CustomValue")]
        public string CustomValue
        {
            get
            {
                return this.customValue;
            }

            set
            {
                this.SetProperty(ref this.customValue, value, () => this.CustomValue);
                ResetColumnName();
            }
        }

        #endregion

        #region IDateErrorInfo

        /// <summary>
        /// Gets or sets an error message indicating what is wrong with this object.
        /// </summary>
        /// <value>
        /// The validate error.
        /// </value>
        [XmlIgnore]
        private string ValidateError { get; set; }

        /// <summary>
        /// Gets an error message indicating what is wrong with this object.
        /// </summary>
        /// <returns>An error message indicating what is wrong with this object. The default is an empty string ("").</returns>
        [XmlIgnore]
        public string Error
        {
            get { return this.ValidateError; }
        }

        /// <summary>
        /// Gets the error message for the property with the given name.
        /// </summary>
        /// <param name="columnName">The name of the property.</param>
        /// <returns>The error message for the property. The default is an empty string ("").</returns>
        public string this[string columnName]
        {
            get
            {
                ValidateError = string.Empty;

                switch (columnName)
                {
                    case "ColumnName":
                        if (IsMandatory && string.IsNullOrWhiteSpace(CustomValue))
                        {
                            if (string.IsNullOrEmpty(this.ColumnName) || this.ColumnName.Equals(LocalizedResources.BomImport_None))
                            {
                                this.ValidateError = LocalizedResources.General_RequiredField;
                            }
                            else
                            {
                                if (this.ColumnNames != null && this.ColumnNames.Any() && !this.ColumnNames.Contains(this.ColumnName))
                                {
                                    this.ValidateError = LocalizedResources.General_RequiredField;
                                }
                            }
                        }

                        break;
                }

                return ValidateError;
            }
        }

        #endregion

        /// <summary>
        /// Resets the name of the column.
        /// </summary>
        private void ResetColumnName()
        {
            if (!string.IsNullOrWhiteSpace(CustomValue))
            {
                ColumnName = (ColumnNames == null || !ColumnNames.Any()) ? string.Empty : LocalizedResources.BomImport_None;
            }

            OnPropertyChanged("ColumnName");
        }

        /// <summary>
        /// Resets the custom value.
        /// </summary>
        private void ResetCustomValue()
        {
            if (((ColumnNames == null || !ColumnNames.Any()) && !string.IsNullOrWhiteSpace(ColumnName)) ||
                (ColumnNames != null && ColumnNames.Any() && ColumnName != LocalizedResources.BomImport_None))
            {
                CustomValue = string.Empty;
            }

            OnPropertyChanged("CustomValue");
        }

        #region ISerializable

        /// <summary>
        /// Populates a <see cref="T:System.Runtime.Serialization.SerializationInfo"/> with the data needed to serialize the target object.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo"/> to populate with data.</param>
        /// <param name="context">The destination (see <see cref="T:System.Runtime.Serialization.StreamingContext"/>) for this serialization.</param>
        /// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Category", this.Categories);
            info.AddValue("ColumnName", this.ColumnName);
            info.AddValue("CustomValue", this.CustomValue);
            info.AddValue("IsMandatory", this.IsMandatory);
        }

        #endregion

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
        /// The fields must be in the same position to be considered equal.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            // If parameter cannot be cast to BomConfigurationItem  
            BomConfigurationItem configuration = obj as BomConfigurationItem;
            if (configuration == null)
            {
                return false;
            }

            if ((this.Categories == null && configuration.Categories != null)
                || (this.Categories != null && configuration.Categories == null)
                || (this.Categories.Count != configuration.Categories.Count))
            {
                return false;
            }

            int count = this.Categories.Count;
            int index = 0;
            while (index < count)
            {
                if (!this.Categories[index].First.Equals(configuration.Categories[index].First)
                    || (!this.Categories[index].Second.Equals(configuration.Categories[index].Second)))
                {
                    return false;
                }

                index++;
            }

            return true;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
