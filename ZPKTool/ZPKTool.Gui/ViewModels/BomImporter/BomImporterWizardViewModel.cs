﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Linq;
using System.Windows.Input;
using ZPKTool.Data;
using ZPKTool.Gui.Resources;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The ViewModel for the import wizard view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class BomImporterWizardViewModel : ViewModel
    {
        #region Fields

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The service used to create open/save file dialogs.
        /// </summary>
        private IFileDialogService fileDialogService;

        /// <summary>
        /// The composition container.
        /// </summary>
        private CompositionContainer container;

        /// <summary>
        /// The current page showed by the wizard.
        /// </summary>
        private ViewModel currentPage;

        /// <summary>
        /// The pages included in the bom import wizard.
        /// </summary>
        private ReadOnlyCollection<ViewModel> pages;

        /// <summary>
        /// The title of the bom import window.
        /// </summary>
        private string title;

        /// <summary>
        /// Next button content message.
        /// </summary>
        private string nextButtonContent;

        /// <summary>
        /// The parent.
        /// </summary>
        private object parent;

        /// <summary>
        /// The parent project base currency of the entity were importing. Used for converting the imported file's data.
        /// </summary>
        private Currency parentProjectBaseCurrency;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="BomImporterWizardViewModel"/> class.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="messenger">The messenger.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="fileDialogService">The file dialog service.</param>
        /// <param name="selectConfigurationViewModel">The configure adapter view model.</param>
        /// <param name="configureViewModel">The select data adapter view model.</param>
        /// <param name="configurationViewModel">The configuration view model.</param>
        [ImportingConstructor]
        public BomImporterWizardViewModel(
            CompositionContainer container,
            IMessenger messenger,
            IWindowService windowService,
            IFileDialogService fileDialogService,
            BomImporterSelectConfigurationViewModel selectConfigurationViewModel,
            BomImporterConfigureViewModel configureViewModel,
            BomImporterConfigurationViewModel configurationViewModel)
        {
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("container", container);
            Argument.IsNotNull("selectConfigurationViewModel", container);
            Argument.IsNotNull("configureViewModel", container);
            Argument.IsNotNull("configurationViewModel", container);

            this.container = container;
            this.messenger = messenger;
            this.windowService = windowService;
            this.fileDialogService = fileDialogService;

            // Inner wizard page ViewModels.
            this.SelectConfigurationViewModel = selectConfigurationViewModel;
            this.ConfigureViewModel = configureViewModel;
            this.ConfigurationViewModel = configurationViewModel;

            // Wizard Navigation Commands.
            this.MovePreviousCommand = new DelegateCommand<object>(this.MovePrevious, this.CanMovePrevious);
            this.MoveNextCommand = new DelegateCommand<object>(this.MoveNext, this.CanMoveNext);
            this.CancelWizardCommand = new DelegateCommand<object>(this.Cancel, this.CanCancel);

            // Create wizard pages.
            this.CreatePages();

            this.Title = LocalizedResources.BomImporter_WizardTitle;
            this.NextButtonContent = LocalizedResources.General_Next;

            this.messenger.Register<NotificationMessage>(this.HandleNotificationMessage);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the current page displayed by the wizard.
        /// </summary>
        /// <value>
        /// The current page.
        /// </value>
        public ViewModel CurrentPage
        {
            get { return this.currentPage; }
            set { this.SetProperty(ref this.currentPage, value, () => this.CurrentPage); }
        }

        /// <summary>
        /// Gets the pages.
        /// </summary>
        public ReadOnlyCollection<ViewModel> Pages
        {
            get
            {
                return pages;
            }
        }

        /// <summary>
        /// Gets the index of the current page.
        /// </summary>
        /// <value>
        /// The index of the current page.
        /// </value>
        private int CurrentPageIndex
        {
            get
            {
                return this.Pages.IndexOf(this.CurrentPage);
            }
        }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        public string Title
        {
            get { return this.title; }
            set { this.SetProperty(ref this.title, value, () => this.Title); }
        }

        /// <summary>
        /// Gets or sets the content of the next button.
        /// </summary>
        /// <value>
        /// The content of the next button.
        /// </value>
        public string NextButtonContent
        {
            get { return this.nextButtonContent; }
            set { this.SetProperty(ref this.nextButtonContent, value, () => this.NextButtonContent); }
        }

        /// <summary>
        /// Gets or sets the parent.
        /// </summary>
        /// <value>
        /// The parent.
        /// </value>
        public object Parent
        {
            get
            {
                return this.parent;
            }

            set
            {
                if (this.parent != value)
                {
                    this.parent = value;
                    this.GetParentProjectBaseCurrency();
                }
            }
        }

        /// <summary>
        /// Gets or sets the data context.
        /// </summary>
        /// <value>
        /// The data context.
        /// </value>
        public ZPKTool.DataAccess.IDataSourceManager DataContext { get; set; }

        /// <summary>
        /// Gets the configure adapter view model.
        /// </summary>
        public BomImporterSelectConfigurationViewModel SelectConfigurationViewModel { get; private set; }

        /// <summary>
        /// Gets the select data adapter view model.
        /// </summary>
        public BomImporterConfigureViewModel ConfigureViewModel { get; private set; }

        /// <summary>
        /// Gets the configuration view model.
        /// </summary>
        public BomImporterConfigurationViewModel ConfigurationViewModel { get; private set; }

        /// <summary>
        /// Gets the property that indicates if the wizard is importing the bill of materials.
        /// </summary>
        public VMProperty<bool> IsImporting { get; private set; }

        #endregion

        #region ICommands

        /// <summary>
        /// Gets the move previous command.
        /// </summary>
        public ICommand MovePreviousCommand { get; private set; }

        /// <summary>
        /// Gets the move next command.
        /// </summary>
        public ICommand MoveNextCommand { get; private set; }

        /// <summary>
        /// Gets the cancel wizard command.
        /// </summary>
        public ICommand CancelWizardCommand { get; private set; }

        #endregion

        #region Wizard Pages

        /// <summary>
        /// Creates the wizard pages.
        /// </summary>
        private void CreatePages()
        {
            var pages = new List<ViewModel>();
            pages.Add(this.SelectConfigurationViewModel);
            pages.Add(this.ConfigureViewModel);
            pages.Add(this.ConfigurationViewModel);

            this.pages = new ReadOnlyCollection<ViewModel>(pages);
            this.CurrentPage = this.Pages[0];
        }

        #endregion

        #region Wizard Navigation

        /// <summary>
        /// Determines whether the wizard can move to previews page.
        /// </summary>
        /// <param name="param">The parameter.</param>
        /// <returns>
        ///   <c>true</c> if this instance [can move previews] the specified parameter; otherwise, <c>false</c>.
        /// </returns>
        private bool CanMovePrevious(object param)
        {
            return 0 < this.CurrentPageIndex;
        }

        /// <summary>
        /// Move to previews page.
        /// </summary>
        /// <param name="param">The parameter.</param>
        private void MovePrevious(object param)
        {
            this.NextButtonContent = LocalizedResources.General_Next;

            if (this.CurrentPage is BomImporterConfigureViewModel)
            {
                this.SelectConfigurationViewModel.LoadConfigurations();
            }

            this.CurrentPage = this.Pages[this.CurrentPageIndex - 1];
        }

        /// <summary>
        /// Determines whether this instance can move to the wizard's next page.
        /// </summary>
        /// <param name="param">The parameter.</param>
        /// <returns>
        ///   <c>true</c> if this instance [can move next] the specified parameter; otherwise, <c>false</c>.
        /// </returns>
        private bool CanMoveNext(object param)
        {
            if (this.CurrentPage is BomImporterSelectConfigurationViewModel)
            {
                if (string.IsNullOrWhiteSpace(this.SelectConfigurationViewModel.FilePath.Value))
                {
                    return false;
                }
            }

            if (this.CurrentPage is BomImporterConfigureViewModel)
            {
                if (string.IsNullOrWhiteSpace(this.ConfigureViewModel.FirstCellTableAddress.Value)
                    || !string.IsNullOrWhiteSpace(this.ConfigureViewModel.FirstCellTableAddress.Error)
                    || !string.IsNullOrWhiteSpace(this.ConfigureViewModel.Level.Error)
                    || !string.IsNullOrWhiteSpace(this.ConfigureViewModel.Type.Error)
                    || !string.IsNullOrWhiteSpace(this.ConfigureViewModel.ErrorMessage.Value))
                {
                    return false;
                }
            }

            if (this.CurrentPage is BomImporterConfigurationViewModel)
            {
                foreach (var item in this.ConfigurationViewModel.Configuration.Value.ConfigurationItems)
                {
                    if (!string.IsNullOrWhiteSpace(item.Error))
                    {
                        return false;
                    }
                }
            }

            return this.CurrentPage != null && (this.CurrentPageIndex < this.Pages.Count);
        }

        /// <summary>
        /// Move to next page.
        /// </summary>
        /// <param name="param">The parameter.</param>
        private void MoveNext(object param)
        {
            var isMoved = false;
            if (this.CurrentPage is BomImporterSelectConfigurationViewModel)
            {
                // Initialize the ConfigureViewModel to load the data from imported file Sheet
                isMoved = this.InitializeConfigureViewModel();
            }
            else if (this.CurrentPage is BomImporterConfigureViewModel)
            {
                // Initialize the ConfigurationViewModel and map the configurations on the data found in the imported file
                isMoved = this.InitializeConfigurationViewModel();
            }
            else if (this.CurrentPage is BomImporterConfigurationViewModel)
            {
                // Import the data
                this.Import();
            }

            // If the move to next page is not successfully then return
            if (isMoved)
            {
                // Set the current page the next page.
                this.CurrentPage = this.Pages[this.CurrentPageIndex + 1];
            }
        }

        /// <summary>
        /// Determines whether the wizard can cancel.
        /// </summary>
        /// <param name="param">The parameter.</param>
        /// <returns>
        /// <c>true</c> if this instance can cancel the specified parameter; otherwise, <c>false</c>.
        /// </returns>
        private bool CanCancel(object param)
        {
            return true;
        }

        /// <summary>
        /// Cancels the wizard.
        /// </summary>
        /// <param name="param">The parameter.</param>
        private void Cancel(object param)
        {
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Initialize the <see cref="ConfigurationViewModel"/> class.
        /// </summary>
        /// <returns>False if any exceptions was thrown, true otherwise</returns>
        private bool InitializeConfigurationViewModel()
        {
            this.NextButtonContent = LocalizedResources.General_Next;

            if (this.ConfigureViewModel.FileType == BomImporterFileType.Xlsx || this.ConfigureViewModel.FileType == BomImporterFileType.Xls)
            {
                // Next Page is Configuration ViewMode.
                var errorMessage = ConfigureViewModel.TryPopulateTableFromExcel();

                if (!string.IsNullOrWhiteSpace(errorMessage))
                {
                    this.windowService.MessageDialogService.Show(errorMessage, MessageDialogType.Error);
                    return false;
                }
            }
            else
            {
                var errorMessage = ConfigureViewModel.TryPopulateTableFromCsv(this.SelectConfigurationViewModel.FilePath.Value);

                if (!string.IsNullOrWhiteSpace(errorMessage))
                {
                    this.windowService.MessageDialogService.Show(errorMessage, MessageDialogType.Error);
                    return false;
                }
            }

            // The table headers.
            List<object> headers = this.ConfigureViewModel.TableData.FirstOrDefault();

            // The type column index that represents the entity type.
            int typeColumn = headers.IndexOf(this.ConfigureViewModel.Type.Value);

            // The level column index. 
            int levelColumn = headers.IndexOf(this.ConfigureViewModel.Level.Value);

            if (typeColumn == -1)
            {
                this.windowService.MessageDialogService.Show(string.Format(LocalizedResources.BomImport_TypeNotFoundError, this.ConfigureViewModel.Type.Value), MessageDialogType.Error);
                return false;
            }
            else if (levelColumn == -1)
            {
                this.windowService.MessageDialogService.Show(string.Format(LocalizedResources.BomImport_LevelNotFoundError, this.ConfigureViewModel.Level.Value), MessageDialogType.Error);
                return false;
            }

            if (this.ConfigureViewModel.Configuration.Value != null)
            {
                this.ConfigurationViewModel.Configuration.Value = this.ConfigureViewModel.Configuration.Value;
            }
            else
            {
                BomConfiguration emptyConfiguration = new BomConfiguration(true, false);
                this.ConfigurationViewModel.Configuration.Value = emptyConfiguration;
            }

            this.ConfigurationViewModel.DataContext = this.DataContext;
            this.ConfigurationViewModel.Type = this.ConfigureViewModel.Type.Value;
            this.ConfigurationViewModel.Level = this.ConfigureViewModel.Level.Value;
            this.ConfigurationViewModel.TableData = this.ConfigureViewModel.TableData;
            this.ConfigurationViewModel.TokenizedHeaders = this.ConfigureViewModel.TokenizedHeaders;
            this.ConfigurationViewModel.SourceBaseCurrency = this.ConfigureViewModel.Currency.Value;
            this.ConfigurationViewModel.DestinationBaseCurrency = this.ConfigureViewModel.Currencies.Value.FirstOrDefault(c => c.IsSameAs(this.parentProjectBaseCurrency));
            this.ConfigurationViewModel.WeightUnit = this.ConfigureViewModel.WeightUnit.Value;

            // Set Column Headers.
            var tableHeaders = this.ConfigureViewModel.TableData.FirstOrDefault();
            this.ConfigurationViewModel.SetColumnHeaders(tableHeaders);

            this.NextButtonContent = LocalizedResources.General_Import;

            return true;
        }

        /// <summary>
        /// Initialize the <see cref="ConfigureViewModel"/> class.
        /// </summary>
        /// <returns>False if any exceptions was thrown, true otherwise</returns>
        private bool InitializeConfigureViewModel()
        {
            this.NextButtonContent = LocalizedResources.General_Next;

            // Next Page is Configure ViewMode.
            this.ConfigureViewModel.DataContext = DataContext;
            this.ConfigureViewModel.Configuration.Value = this.SelectConfigurationViewModel.SelectedConfiguration.Value;
            this.ConfigureViewModel.FileType = this.SelectConfigurationViewModel.FileType;
            this.ConfigureViewModel.FirstCellTableAddress.Value = "A4";
            this.ConfigureViewModel.Type.Value = "Type";
            this.ConfigureViewModel.Level.Value = "Level";
            var allCurrencies = DataContext.CurrencyRepository.GetBaseCurrencies();
            this.ConfigureViewModel.Currencies.Value = new ObservableCollection<Currency>(allCurrencies.OrderBy(currency => currency.Symbol));
            var allWeightUnits = DataContext.MeasurementUnitRepository.GetAll(MeasurementUnitType.Weight);
            this.ConfigureViewModel.WeightUnits.Value = new ObservableCollection<MeasurementUnit>(allWeightUnits.OrderBy(unit => unit.Symbol));
            this.ConfigureViewModel.Currency.Value = this.ConfigureViewModel.Currencies.Value.FirstOrDefault(currency => currency.Name.Equals("Euro"));
            this.ConfigureViewModel.ParentProjectBaseCurrency = this.parentProjectBaseCurrency;
            this.ConfigureViewModel.WeightUnit.Value = this.ConfigureViewModel.WeightUnits.Value.FirstOrDefault(unit => unit.Symbol.Equals("Kg", StringComparison.InvariantCultureIgnoreCase));
            this.ConfigureViewModel.FilePath = this.SelectConfigurationViewModel.FilePath.Value;            

            if (this.ConfigureViewModel.FileType == BomImporterFileType.Xlsx || this.ConfigureViewModel.FileType == BomImporterFileType.Xls)
            {
                if (!this.ConfigureViewModel.TrySetSheets(this.SelectConfigurationViewModel.FilePath.Value))
                {
                    return false;
                }
            }

            this.ConfigureViewModel.PopulateConfiguration(null, null);
            return true;
        }

        /// <summary>
        /// Imports the data loaded from the imported file
        /// </summary>
        private void Import()
        {
            this.NextButtonContent = LocalizedResources.General_Import;

            // Check the pictures settings if the file is an xls or xlsx excel file
            if (this.SelectConfigurationViewModel.FileType == BomImporterFileType.Xls || this.SelectConfigurationViewModel.FileType == BomImporterFileType.Xlsx)
            {
                // Load the pictures folder path and the pictures column index
                this.ConfigureViewModel.LoadPicturesConfiguration();
                this.ConfigurationViewModel.PicturesFolderPath = this.ConfigureViewModel.PicturesFolderPath;
                this.ConfigurationViewModel.PicturesColumnIndex = this.ConfigureViewModel.PicturesColumnIndex;

                var pictureCellAddress = this.ConfigurationViewModel.Configuration.Value.PicturesFolderCellAddress;
                var pictureColumnIndex = this.ConfigurationViewModel.Configuration.Value.PicturesColumnIndex;
                var picturesFolderPath = this.ConfigureViewModel.PicturesFolderPath;
                var picturesindex = this.ConfigureViewModel.PicturesColumnIndex;

                // Check if are any errors on the pictures fields
                var msg = string.Empty;
                if (!string.IsNullOrWhiteSpace(pictureCellAddress) && !Directory.Exists(picturesFolderPath))
                {
                    // The folder doesn't exist.
                    msg = LocalizedResources.BomImport_PicturesFolderNotExist;
                }
                else if ((string.IsNullOrWhiteSpace(pictureCellAddress) && !string.IsNullOrWhiteSpace(pictureColumnIndex))
                          || (!string.IsNullOrWhiteSpace(picturesFolderPath) && string.IsNullOrWhiteSpace(pictureColumnIndex)))
                {
                    // Both picture folder and picture column must be set or unset
                    msg = LocalizedResources.BomImport_PicturesFieldsMustBeSet;
                }
                else
                {
                    var dataDetected = false;
                    if (this.ConfigurationViewModel.TableData[0].Count > picturesindex && picturesindex >= 0)
                    {
                        var rowCount = this.ConfigurationViewModel.TableData.Count;
                        for (int rowIndex = 1; rowIndex < rowCount; rowIndex++)
                        {
                            var cellData = this.ConfigurationViewModel.TableData[rowIndex][this.ConfigurationViewModel.PicturesColumnIndex] as string ?? string.Empty;
                            if (!string.IsNullOrWhiteSpace(cellData))
                            {
                                dataDetected = true;
                                break;
                            }
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(picturesFolderPath) && !dataDetected)
                    {
                        msg = LocalizedResources.BomImport_PicturesColumnEmpty;
                    }
                }

                if (!string.IsNullOrWhiteSpace(msg))
                {
                    this.windowService.MessageDialogService.Show(msg, MessageDialogType.Error);
                    return;
                }
            }

            // Import Data.
            this.ConfigurationViewModel.Parent = this.Parent;
            this.ConfigurationViewModel.ShowCancel.Value = false;

            this.ConfigurationViewModel.ImportData();
        }

        #endregion

        #region Handle notification messages

        /// <summary>
        /// Handles the notification message.
        /// </summary>
        /// <param name="message">The message.</param>
        private void HandleNotificationMessage(NotificationMessage message)
        {
            if (message.Notification == "StartIsImporting")
            {
                this.IsImporting.Value = true;
            }
            else if (message.Notification == "StopIsImporting")
            {
                this.IsImporting.Value = false;
                this.windowService.CloseViewWindow(this);
            }
        }

        #endregion

        /// <summary>
        /// Gets the parent project base currency.
        /// </summary>
        private void GetParentProjectBaseCurrency()
        {
            var project = this.Parent as Project;
            var projectId = Guid.Empty;
            if (project != null)
            {
                projectId = project.Guid;
            }
            else
            {
                var assy = this.Parent as Assembly;
                if (assy != null)
                {
                    projectId = this.DataContext.AssemblyRepository.GetParentProjectId(assy);
                }
            }

            if (projectId != Guid.Empty)
            {
                var baseCurrency = this.DataContext.CurrencyRepository.GetProjectBaseCurrency(projectId);
                this.parentProjectBaseCurrency = baseCurrency;
            }
        }
    }
}
