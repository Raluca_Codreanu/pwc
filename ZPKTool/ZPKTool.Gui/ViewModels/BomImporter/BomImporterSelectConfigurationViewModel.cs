﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Resources;
using System.Security;
using System.Windows.Input;
using System.Xml;
using System.Xml.Serialization;
using ZPKTool.Common;
using ZPKTool.Controls;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Resources;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The Configure Adapter ViewModel class.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class BomImporterSelectConfigurationViewModel : ViewModel
    {
        #region Fields

        /// <summary>
        /// The path to the bom example file.
        /// </summary>
        private const string BomExampleLoaction = "ZPKTool.Gui.Resources.ExcelTemplates.ExampleBOM.xls";

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The messenger service.
        /// </summary>
        private readonly IMessenger messenger;

        /// <summary>
        /// The file dialog service.
        /// </summary>
        private readonly IFileDialogService fileDialogService;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="BomImporterSelectConfigurationViewModel"/> class.
        /// </summary>
        /// <param name="messenger">The messenger.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="fileDialogService">The file dialog service.</param>
        /// <param name="configurationViewModel">The configuration view model.</param>
        [ImportingConstructor]
        public BomImporterSelectConfigurationViewModel(
            IMessenger messenger,
            IWindowService windowService,
            IFileDialogService fileDialogService,
            BomImporterConfigurationViewModel configurationViewModel)
        {
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("fileDialogService", windowService);
            Argument.IsNotNull("configurationViewModel", configurationViewModel);

            this.messenger = messenger;
            this.WindowService = windowService;
            this.fileDialogService = fileDialogService;
            this.ConfigurationViewModel = configurationViewModel;

            this.SelectPathCommand = new DelegateCommand(SelectPath);
            this.DisplayExampleCommand = new DelegateCommand(ExecuteDisplayExampleCommand);
            this.NewConfigurationCommand = new DelegateCommand(this.NewConfiguration, this.CanNewConfiguration);
            this.EditConfigurationCommand = new DelegateCommand(this.EditConfiguration, this.CanEditConfiguration);
            this.DuplicateConfigurationCommand = new DelegateCommand(this.DuplicateConfiguration, this.CanDuplicateConfiguration);
            this.DeleteConfigurationCommand = new DelegateCommand(this.DeleteConfiguration, this.CanDeleteConfiguration);
            this.ImportConfigurationCommand = new DelegateCommand(this.ImportConfiguration, this.CanImportConfiguration);
            this.ExportConfigurationCommand = new DelegateCommand(this.ExportConfiguration, this.CanExportConfiguration);
            this.FilePath = new DataProperty<string>(this, "FilePath");

            this.Configurations.Value = new ObservableCollection<BomConfiguration>();
            this.LoadConfigurations();
            this.messenger.Register<NotificationMessage>(this.HandleNotificationMessage);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the configuration view model.
        /// </summary>
        public BomImporterConfigurationViewModel ConfigurationViewModel { get; private set; }

        /// <summary>
        /// Gets the file path.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_FilePath", ErrorMessageResourceType = typeof(LocalizedResources))]
        public DataProperty<string> FilePath { get; private set; }

        /// <summary>
        /// Gets or sets the type of the file.
        /// </summary>
        /// <value>
        /// The type of the file.
        /// </value>
        public BomImporterFileType FileType { get; set; }

        /// <summary>
        /// Gets or sets the window service.
        /// </summary>
        public IWindowService WindowService
        {
            get
            {
                return this.windowService;
            }

            set
            {
                if (this.windowService != value)
                {
                    this.windowService = value;
                    OnPropertyChanged(() => this.WindowService);
                }
            }
        }

        /// <summary>
        /// Gets the configurations.
        /// </summary>
        public VMProperty<ObservableCollection<BomConfiguration>> Configurations { get; private set; }

        /// <summary>
        /// Gets the selected configuration.
        /// </summary>
        public VMProperty<BomConfiguration> SelectedConfiguration { get; private set; }

        #endregion

        #region ICommands

        /// <summary>
        /// Gets the select path command.
        /// </summary>
        public ICommand SelectPathCommand { get; private set; }

        /// <summary>
        /// Gets the display example command.
        /// </summary>
        public ICommand DisplayExampleCommand { get; private set; }

        /// <summary>
        /// Gets the new configuration command.
        /// </summary>
        public ICommand NewConfigurationCommand { get; private set; }

        /// <summary>
        /// Gets the edit configuration command.
        /// </summary>
        public ICommand EditConfigurationCommand { get; private set; }

        /// <summary>
        /// Gets the duplicate configuration command.
        /// </summary>
        public ICommand DuplicateConfigurationCommand { get; private set; }

        /// <summary>
        /// Gets the delete configuration command.
        /// </summary>
        public ICommand DeleteConfigurationCommand { get; private set; }

        /// <summary>
        /// Gets the import configuration command.
        /// </summary>
        public ICommand ImportConfigurationCommand { get; private set; }

        /// <summary>
        /// Gets the export configuration command.
        /// </summary>
        public ICommand ExportConfigurationCommand { get; private set; }

        #endregion

        #region Load

        /// <summary>
        /// Loads the configurations.
        /// </summary>
        public void LoadConfigurations()
        {
            var oldSelectedCfg = this.SelectedConfiguration.Value;

            this.Configurations.Value.Clear();
            this.SelectedConfiguration.Value = null;
            AddDefaultConfiguration();

            if (Directory.Exists(BomConfiguration.ConfigurationFilesFolder))
            {
                foreach (var fileName in Directory.EnumerateFiles(BomConfiguration.ConfigurationFilesFolder, "*.xml").Where(fileName => Path.GetFileNameWithoutExtension(fileName) != BomConfiguration.DefaultConfigurationGuid.ToString()))
                {
                    try
                    {
                        using (StreamReader stream = new StreamReader(fileName))
                        {
                            XmlTextReader reader = new XmlTextReader(stream);

                            XmlSerializer serializer = new XmlSerializer(typeof(BomConfiguration));
                            BomConfiguration configuration = serializer.Deserialize(reader) as BomConfiguration;

                            if (configuration != null)
                            {
                                configuration.UpdateConfiguration();
                                this.Configurations.Value.Add(configuration);
                            }
                            else
                            {
                                log.Error("The deserialized configuration is null. FileName: " + fileName);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        log.ErrorException("Exception occurred while trying to load the configuration file: " + fileName, ex);
                    }
                }
            }

            if (oldSelectedCfg != null && this.Configurations.Value.Any(cfg => cfg.Id == oldSelectedCfg.Id))
            {
                this.SelectedConfiguration.Value = Configurations.Value.FirstOrDefault(cfg => cfg.Id == oldSelectedCfg.Id);
            }
            else
            {
                SelectedConfiguration.Value = Configurations.Value.FirstOrDefault();
            }
        }

        /// <summary>
        /// Adds the default configuration.
        /// </summary>
        private void AddDefaultConfiguration()
        {
            try
            {
                var defaultConfigurationFilePath = BomConfiguration.DefaultConfigurationFilePath;
                BomConfiguration defaultConfiguration;

                if (!File.Exists(defaultConfigurationFilePath))
                {
                    if (!Directory.Exists(BomConfiguration.ConfigurationFilesFolder))
                    {
                        Directory.CreateDirectory(BomConfiguration.ConfigurationFilesFolder);
                    }

                    defaultConfiguration = new BomConfiguration(true, true);
                    defaultConfiguration.Name = LocalizedResources.General_DefaultConfigurationName;
                    defaultConfiguration.Id = BomConfiguration.DefaultConfigurationGuid;
                    defaultConfiguration.DateChanged = DateTime.Now;
                    defaultConfiguration.PicturesColumnIndex = string.Empty;
                    defaultConfiguration.PicturesFolderCellAddress = string.Empty;

                    using (Stream stream = new FileStream(defaultConfigurationFilePath, FileMode.Create))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(BomConfiguration));
                        serializer.Serialize(stream, defaultConfiguration);
                    }
                }
                else
                {
                    using (StreamReader stream = new StreamReader(defaultConfigurationFilePath))
                    {
                        XmlTextReader reader = new XmlTextReader(stream);

                        XmlSerializer serializer = new XmlSerializer(typeof(BomConfiguration));
                        defaultConfiguration = serializer.Deserialize(reader) as BomConfiguration;
                    }
                }

                if (defaultConfiguration != null)
                {
                    this.Configurations.Value.Add(defaultConfiguration);
                }
            }
            catch (Exception ex)
            {
                log.ErrorException("Exception occurred while trying to create/load the default configuration", ex);
                throw PathUtils.ParseFileRelatedException(ex);
            }
        }

        #endregion

        /// <summary>
        /// Handles the notification message.
        /// </summary>
        /// <param name="message">The message.</param>
        private void HandleNotificationMessage(NotificationMessage message)
        {
            if (message.Notification == "NewConfigurationCreated")
            {
                this.Configurations.Value.Add(this.ConfigurationViewModel.Configuration.Value);
                this.SelectedConfiguration.Value = this.ConfigurationViewModel.Configuration.Value;
            }
        }

        /// <summary>
        /// Executes the display example command.
        /// </summary>
        private void ExecuteDisplayExampleCommand()
        {
            var filePath = Path.Combine(Path.GetTempPath(), "ExampleBOM.xls");
            using (var templateStream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(BomExampleLoaction))
            {
                if (templateStream == null)
                {
                    log.Error("The template for the generate assembly report was null.");
                }
                else
                {
                    try
                    {
                        using (FileStream destFile = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None, 8192))
                        {
                            templateStream.CopyTo(destFile);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw PathUtils.ParseFileRelatedException(ex);
                    }
                }
            }

            FileOpenHandler fileOpenHandler = new FileOpenHandler(filePath, true);
        }

        /// <summary>
        /// Selects the file path.
        /// </summary>
        private void SelectPath()
        {
            fileDialogService.Reset();

            // Configure open file dialog box        
            fileDialogService.Filter = LocalizedResources.DialogFilter_OpenImportBom;
            fileDialogService.Multiselect = false;
            this.FileType = BomImporterFileType.None;
            this.FilePath.Value = null;

            // Show open file dialog box
            if (fileDialogService.ShowOpenFileDialog() != true)
            {
                return;
            }

            var fileExtension = Path.GetExtension(fileDialogService.FileName);

            if (fileExtension == ".xlsx")
            {
                this.FileType = BomImporterFileType.Xlsx;
            }
            else if (fileExtension == ".csv")
            {
                this.FileType = BomImporterFileType.Csv;
            }
            else if (fileExtension == ".xls")
            {
                this.FileType = BomImporterFileType.Xls;
            }

            if (this.FileType != BomImporterFileType.None)
            {
                this.FilePath.Value = fileDialogService.FileName;
            }
            else
            {
                windowService.MessageDialogService.Show(LocalizedResources.BomImport_ErrorNotSupportedFileType, MessageDialogType.Error);
            }
        }

        #region New

        /// <summary>
        /// Determines whether this instance [can new configuration].
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance [can new configuration]; otherwise, <c>false</c>.
        /// </returns>
        private bool CanNewConfiguration()
        {
            return true;
        }

        /// <summary>
        /// Create new configuration with empty columns.
        /// </summary>
        private void NewConfiguration()
        {
            BomConfiguration newConfiguration = new BomConfiguration(true, false);
            this.ConfigurationViewModel.Configuration.Value = newConfiguration;
            this.ConfigurationViewModel.ShowCancel.Value = true;
            this.ConfigurationViewModel.IsCreate = true;
            this.windowService.ShowViewInDialog(this.ConfigurationViewModel);
        }

        #endregion

        #region Edit

        /// <summary>
        /// Determines whether the selected configuration can be edited.
        /// </summary>
        /// <returns>
        ///  True if the selected configuration can be edited ; otherwise, false.
        /// </returns>
        private bool CanEditConfiguration()
        {
            return this.SelectedConfiguration.Value != null;
        }

        /// <summary>
        /// Edit the configuration.
        /// </summary>
        private void EditConfiguration()
        {
            this.ConfigurationViewModel.Configuration.Value = this.SelectedConfiguration.Value;
            this.ConfigurationViewModel.ShowCancel.Value = true;
            this.ConfigurationViewModel.IsCreate = false;
            this.windowService.ShowViewInDialog(this.ConfigurationViewModel);
        }

        #endregion

        #region Duplicate

        /// <summary>
        /// Determines whether the selected configuration can be duplicated.
        /// </summary>
        /// <returns>
        /// True if the selected configuration can be duplicated; otherwise, false.
        /// </returns>
        private bool CanDuplicateConfiguration()
        {
            return this.SelectedConfiguration.Value != null;
        }

        /// <summary>
        /// Duplicate the configuration.
        /// </summary>
        private void DuplicateConfiguration()
        {
            try
            {
                var clonedConfiguration = BomConfiguration.Clone(this.SelectedConfiguration.Value);
                clonedConfiguration.Id = Guid.NewGuid();
                clonedConfiguration.Name = LocalizedResources.General_CopyOf + " " + clonedConfiguration.Name;

                if (!Directory.Exists(BomConfiguration.ConfigurationFilesFolder))
                {
                    Directory.CreateDirectory(BomConfiguration.ConfigurationFilesFolder);
                }

                var fullNameSettingsFilePath = BomConfiguration.GetFullPathOfConfiguration(clonedConfiguration);

                using (Stream stream = new FileStream(fullNameSettingsFilePath, FileMode.Create))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(BomConfiguration));
                    serializer.Serialize(stream, clonedConfiguration);
                }

                this.Configurations.Value.Add(clonedConfiguration);
                this.SelectedConfiguration.Value = clonedConfiguration;
            }
            catch (Exception ex)
            {
                log.ErrorException("An error occurred while trying to duplicate a configuration.", ex);
                throw PathUtils.ParseFileRelatedException(ex);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Determines whether this instance [can delete configuration].
        /// </summary>
        /// <returns>
        ///  <c>true</c> if this instance [can delete configuration]; otherwise, <c>false</c>.
        /// </returns>
        private bool CanDeleteConfiguration()
        {
            return this.SelectedConfiguration.Value != null && this.SelectedConfiguration.Value.Id != BomConfiguration.DefaultConfigurationGuid;
        }

        /// <summary>
        /// Deletes the configuration.
        /// </summary>
        private void DeleteConfiguration()
        {
            try
            {
                var messageDialogResult = windowService.MessageDialogService.Show(LocalizedResources.BomImport_Question_DeleteConfiguration, MessageDialogType.YesNo);

                if (messageDialogResult == MessageDialogResult.Yes)
                {
                    var fullNameSettingsFilePath = BomConfiguration.GetFullPathOfConfiguration(SelectedConfiguration.Value);

                    File.Delete(fullNameSettingsFilePath);

                    this.Configurations.Value.Remove(this.SelectedConfiguration.Value);
                }
            }
            catch (Exception ex)
            {
                log.ErrorException("An error occurred while trying to delete a configuration.", ex);
                throw PathUtils.ParseFileRelatedException(ex);
            }
        }

        #endregion

        #region Import

        /// <summary>
        /// Determines whether can be imported configurations.
        /// </summary>
        /// <returns>
        /// <c>true</c> if this instance [can import configuration]; otherwise, <c>false</c>.
        /// </returns>
        private bool CanImportConfiguration()
        {
            return true;
        }

        /// <summary>
        /// Imports a configuration from a xml file.
        /// </summary>
        private void ImportConfiguration()
        {
            try
            {
                this.fileDialogService.Reset();
                this.fileDialogService.Filter = LocalizedResources.DialogFilter_XmlDocs;
                this.fileDialogService.Multiselect = false;

                if (fileDialogService.ShowOpenFileDialog() != true)
                {
                    return;
                }

                var filePath = fileDialogService.FileName;

                using (StreamReader stream = new StreamReader(filePath))
                {
                    BomConfiguration configuration = null;
                    XmlSerializer deserializer = null;

                    try
                    {
                        XmlTextReader reader = new XmlTextReader(stream);
                        deserializer = new XmlSerializer(typeof(BomConfiguration));
                        configuration = deserializer.Deserialize(reader) as BomConfiguration;
                    }
                    catch (Exception ex)
                    {
                        log.ErrorException("An error occurred while trying to import a configuration.", ex);
                        windowService.MessageDialogService.Show(LocalizedResources.General_ErrorImportConfiguration, MessageDialogType.Error);
                    }

                    if (configuration != null)
                    {
                        configuration.UpdateConfiguration();
                        configuration.Id = Guid.NewGuid();

                        this.Configurations.Value.Add(configuration);
                        this.SelectedConfiguration.Value = configuration;

                        if (!Directory.Exists(BomConfiguration.ConfigurationFilesFolder))
                        {
                            Directory.CreateDirectory(BomConfiguration.ConfigurationFilesFolder);
                        }

                        var fullNameSettingsFilePath = BomConfiguration.GetFullPathOfConfiguration(configuration);

                        try
                        {
                            using (Stream stream2 = new FileStream(fullNameSettingsFilePath, FileMode.Create))
                            {
                                deserializer.Serialize(stream2, configuration);
                            }
                        }
                        catch (Exception ex2)
                        {
                            log.ErrorException("An error occurred while trying to save the imported configuration.", ex2);
                            windowService.MessageDialogService.Show(LocalizedResources.BomImport_ErrorSavingConfiguration, MessageDialogType.Error);
                        }
                    }
                    else
                    {
                        log.Error("The deserialized configuration is null. FileName: " + filePath);
                    }
                }
            }
            catch (IOException ex)
            {
                log.ErrorException("IO error occurred while saving the configuration.", ex);
                this.windowService.MessageDialogService.Show(LocalizedResources.General_FileIOError, MessageDialogType.Error);
            }
        }

        #endregion

        #region Export

        /// <summary>
        /// Determines whether the selected configuration can be exported.
        /// </summary>
        /// <returns>
        /// True if the selected configuration can be exported; otherwise, false.
        /// </returns>
        private bool CanExportConfiguration()
        {
            return this.SelectedConfiguration.Value != null && SelectedConfiguration.Value.Id != BomConfiguration.DefaultConfigurationGuid;
        }

        /// <summary>
        /// Exports the selected configuration.
        /// </summary>
        private void ExportConfiguration()
        {
            try
            {
                this.fileDialogService.Reset();
                this.fileDialogService.Filter = LocalizedResources.DialogFilter_XmlDocs;
                var exportedFileName = PathUtils.SanitizeFileName(this.SelectedConfiguration.Value.Name);
                this.fileDialogService.FileName = exportedFileName;
                var result = this.fileDialogService.ShowSaveFileDialog();

                if (result == true)
                {
                    using (Stream stream = new FileStream(this.fileDialogService.FileName, FileMode.Create))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(BomConfiguration));
                        serializer.Serialize(stream, this.SelectedConfiguration.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                log.ErrorException("An error occurred while trying to export a configuration.", ex);
                throw PathUtils.ParseFileRelatedException(ex);
            }
        }

        #endregion
    }
}
