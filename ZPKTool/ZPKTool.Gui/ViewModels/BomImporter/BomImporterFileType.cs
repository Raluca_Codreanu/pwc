﻿namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Supported import file types.
    /// </summary>
    public enum BomImporterFileType
    {
        /// <summary>
        /// File Type Not Set.
        /// </summary>
        None,

        /// <summary>
        /// Word 2007 File Format.
        /// </summary>
        Xlsx,

        /// <summary>
        /// Word 2003 File Format.
        /// </summary>
        Xls,

        /// <summary>
        /// CSV File Format
        /// </summary>
        Csv
    }
}
