﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using ZPKTool.Common;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.MvvmCore;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for TransportCostCalculatorSummary view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class TransportCostCalculatorSummaryViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The calculation items (routes).
        /// </summary>
        private ObservableCollection<TransportCostCalculationViewModel> calculationItems;

        /// <summary>
        /// The selected calculation item (route).
        /// </summary>
        private TransportCostCalculationViewModel selectedCalculationItem;

        /// <summary>
        /// The total packaging cost [per piece] for all routes.
        /// </summary>
        private decimal totalPackagingCostPerPcs;

        /// <summary>
        /// The total transport cost [per piece] for all routes.
        /// </summary>
        private decimal totalTransportCostPerPcs;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="TransportCostCalculatorSummaryViewModel"/> class.
        /// </summary>
        /// <param name="calculationItems">The calculation items.</param>
        [ImportingConstructor]
        public TransportCostCalculatorSummaryViewModel(ObservableCollection<TransportCostCalculationViewModel> calculationItems)
        {
            Argument.IsNotNull("calculationItems", calculationItems);

            this.CalculationItems = calculationItems;

            this.TabHeader.Value = LocalizedResources.General_Summary;
            this.ShowDeleteButtonOnTab.Value = false;
        }

        #region Commands

        /// <summary>
        /// Gets or sets the command that adds a new route into transport calculator.
        /// </summary>
        public ICommand AddCalculationCommand { get; set; }

        /// <summary>
        /// Gets or sets the command that deletes the selected calculation route from transport calculator.
        /// </summary>
        public ICommand DeleteCalculationCommand { get; set; }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the calculation items (routes).
        /// </summary>
        public ObservableCollection<TransportCostCalculationViewModel> CalculationItems
        {
            get { return this.calculationItems; }
            set { this.SetProperty(ref this.calculationItems, value, () => this.CalculationItems); }
        }

        /// <summary>
        /// Gets or sets the selected calculation item (route).
        /// </summary>
        public TransportCostCalculationViewModel SelectedCalculationItem
        {
            get { return this.selectedCalculationItem; }
            set { this.SetProperty(ref this.selectedCalculationItem, value, () => this.SelectedCalculationItem); }
        }

        /// <summary>
        /// Gets or sets the total packaging cost [per piece] for all routes.
        /// </summary>
        [Range(0, (double)Constants.MaxNumberValueAllowedInDb, ErrorMessageResourceName = "TransportCostCalculator_PackagingCostValidation", ErrorMessageResourceType = typeof(LocalizedResources))]
        public decimal TotalPackagingCostPerPcs
        {
            get { return this.totalPackagingCostPerPcs; }
            set { this.SetProperty(ref this.totalPackagingCostPerPcs, value, () => this.TotalPackagingCostPerPcs); }
        }

        /// <summary>
        /// Gets or sets the total transport cost [per piece] for all routes.
        /// </summary>                
        [Range(0, (double)Constants.MaxNumberValueAllowedInDb, ErrorMessageResourceName = "TransportCostCalculator_TransportCostValidation", ErrorMessageResourceType = typeof(LocalizedResources))]
        public decimal TotalTransportCostPerPcs
        {
            get { return this.totalTransportCostPerPcs; }
            set { this.SetProperty(ref this.totalTransportCostPerPcs, value, () => this.TotalTransportCostPerPcs); }
        }

        /// <summary>
        /// Gets or sets the total annual packaging cost for all routes.
        /// </summary>
        public VMProperty<decimal> TotalPackagingCost { get; set; }

        /// <summary>
        /// Gets or sets the total annual transport cost for all routes.
        /// </summary>
        public VMProperty<decimal> TotalTransportCost { get; set; }

        /// <summary>
        /// Gets the tab's header.
        /// </summary>
        public VMProperty<string> TabHeader { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the delete button should be shown on the view model's corresponding tab.
        /// </summary>
        public VMProperty<bool> ShowDeleteButtonOnTab { get; private set; }

        /// <summary>
        /// Gets or sets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter { get; set; }

        #endregion
    }
}
