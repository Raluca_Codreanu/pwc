﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Security;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Ionic.Zip;
using ZPKTool.Business;
using ZPKTool.Business.Export;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the IMyView view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class EntityDocumentsViewModel : ViewModel, IUndoManager
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The window service.
        /// </summary>
        private readonly IWindowService windowService;

        /// <summary>
        /// The file dialog service.
        /// </summary>
        private readonly IFileDialogService fileDialogService;

        /// <summary>
        /// The model browser helper service.
        /// </summary>
        private readonly IModelBrowserHelperService modelBrowserHelperService;

        /// <summary>
        /// The undo list.
        /// </summary>
        private ObservableCollection<UndoableItem> undoStack;

        /// <summary>
        /// The model.
        /// </summary>
        private IEntityWithMediaCollection model;

        /// <summary>
        /// The data source manager associated with the model.
        /// </summary>
        private IDataSourceManager modelDataSourceManager;

        /// <summary>
        /// Utility used to get from the system the icon associated with a document's extension.
        /// </summary>
        private RegisteredFileTypeIconExtractor iconExtractor = new RegisteredFileTypeIconExtractor();

        /// <summary>
        /// The documents to be displayed.
        /// </summary>
        private DispatchedObservableCollection<DocumentItem> documents = new DispatchedObservableCollection<DocumentItem>();

        /// <summary>
        /// The documents that were removed form the view.
        /// </summary>
        private List<DocumentItem> deletedDocuments = new List<DocumentItem>();

        /// <summary>
        /// The index of the last filter selected in an OpenFileDialog
        /// </summary>
        private int lastOpenDialogFilterIndex = 0;

        #endregion Attributes

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityDocumentsViewModel"/> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        /// <param name="fileDialogService">The file dialog service.</param>
        /// <param name="modelBrowserHelperService">The model browser helper service.</param>
        [ImportingConstructor]
        public EntityDocumentsViewModel(
            IWindowService windowService,
            IFileDialogService fileDialogService,
            IModelBrowserHelperService modelBrowserHelperService)
        {
            this.windowService = windowService;
            this.fileDialogService = fileDialogService;
            this.modelBrowserHelperService = modelBrowserHelperService;

            this.undoStack = new ObservableCollection<UndoableItem>();
            this.AddDocumentCommand = new DelegateCommand(AddDocument, () => !this.IsReadOnly);
            this.OpenDocumentCommand = new DelegateCommand<DocumentItem>(OpenDocument);
            this.SaveToDiskCommand = new DelegateCommand<DocumentItem>(SaveDocumentToDisk, (s) => !this.IsReadOnly);
            this.DeleteDocumentCommand = new DelegateCommand<DocumentItem>(DeleteDocument, (s) => !this.IsReadOnly);

            this.Documents.CollectionChanged += DocumentsCollectionChanged;
        }

        #endregion Constructor

        #region Commands

        /// <summary>
        /// Gets a command that adds a new document.
        /// </summary>
        public ICommand AddDocumentCommand { get; private set; }

        /// <summary>
        /// Gets a command that opens and existing document.
        /// </summary>
        public ICommand OpenDocumentCommand { get; private set; }

        /// <summary>
        /// Gets a command that saves a document to disk.
        /// </summary>
        /// <value>The save to disk command.</value>
        public ICommand SaveToDiskCommand { get; private set; }

        /// <summary>
        /// Gets a command that deletes an existing document.
        /// </summary>
        public ICommand DeleteDocumentCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Sets the model.
        /// </summary>
        public IEntityWithMediaCollection Model
        {
            set
            {
                if (this.model != value)
                {
                    this.model = value;
                    OnModelChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the data source manager used to save the model changes.
        /// The model must have been obtained using this instance.
        /// </summary>
        public IDataSourceManager ModelDataSourceManager
        {
            get
            {
                return this.modelDataSourceManager;
            }

            set
            {
                if (!IsInViewerMode && value == null)
                {
                    throw new ArgumentNullException("value", "The data source manager was null.");
                }

                if (this.modelDataSourceManager != value)
                {
                    this.modelDataSourceManager = value;
                }
            }
        }

        /// <summary>
        /// Gets the documents to be displayed.
        /// </summary>
        public DispatchedObservableCollection<DocumentItem> Documents
        {
            get
            {
                return this.documents;
            }

            private set
            {
                if (this.documents != value)
                {
                    this.documents = value;
                    OnPropertyChanged(() => this.Documents);
                }
            }
        }

        #endregion Properties

        #region Public Methods

        /// <summary>
        /// Loads the media documents.
        /// </summary>
        /// <param name="documentsList">The documents media collection.</param>
        public void LoadDocuments(List<Media> documentsList)
        {
            if (this.Documents.Count > 0)
            {
                var removedDocuments = new List<object>();
                removedDocuments.AddRange(this.Documents);
                this.deletedDocuments.AddRange(this.Documents);
                this.Push(new UndoableItem(null, UndoActionType.Delete, removedDocuments));
            }

            this.Documents.Clear();

            foreach (Media media in documentsList.OrderBy(d => d.OriginalFileName))
            {
                DocumentItem item = new DocumentItem();
                item.Name = media.OriginalFileName;
                item.Icon = GetDocumentItemIcon(item.Name);
                item.Source = media;
                item.IsNew = true;
                this.Documents.Add(item);
            }

            this.IsChanged = false;

            if (this.Documents.Count > 0)
            {
                var addedDocuments = new List<object>();
                addedDocuments.AddRange(this.Documents);
                this.Push(new UndoableItem(null, UndoActionType.Insert, addedDocuments));
            }
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Called when the Model property has changed.
        /// </summary>
        private void OnModelChanged()
        {
            this.Documents.Clear();
            this.deletedDocuments.Clear();

            var mediaLoadTask = new Task<object>(AsynchronousDocumentsLoad);
            mediaLoadTask.ContinueWith(AsynchronousDocumentsLoadFinished);
            mediaLoadTask.Start();
        }

        /// <summary>
        /// Load current entity documents asynchronously.
        /// </summary>
        /// <returns>The loaded documents collection. It can be a collection of MediaMetaData, when documents are loaded from Database; 
        /// or a list of ImportedMedia, when the application runs in Viewer mode.</returns>
        private object AsynchronousDocumentsLoad()
        {
            if (IsInViewerMode)
            {
                return modelBrowserHelperService.GetDocumentsForEntity(model);
            }

            // Get the meta data of the entity's docs from the the db and display that info.
            var mediaManager = new MediaManager(DataAccessFactory.CreateDataSourceManager(this.ModelDataSourceManager.DatabaseId));
            return mediaManager.GetDocumentsMetaData(this.model);
        }

        /// <summary>
        /// The actions performed after the documents were loaded asynchronously.
        /// </summary>
        /// <param name="task">The documents loading asynchronous task.</param>
        private void AsynchronousDocumentsLoadFinished(Task<object> task)
        {
            if (task.Exception != null)
            {
                log.Error(task.Exception.InnerException);
                return;
            }

            this.Dispatcher.BeginInvoke(() => this.DocumentsProcessing(task));
        }

        /// <summary>
        /// Process the documents loaded in the task, and display them.
        /// </summary>
        /// <param name="task">The documents loading asynchronous task.</param>
        private void DocumentsProcessing(Task<object> task)
        {
            var cachedDocuments = task.Result as IList<ImportedMedia>;
            if (cachedDocuments != null)
            {
                foreach (var cachedDocument in cachedDocuments.OrderBy(d => d.OriginalFileName))
                {
                    var item = new DocumentItem { Name = cachedDocument.OriginalFileName };
                    item.Icon = GetDocumentItemIcon(item.Name);
                    item.Source = cachedDocument.CachedFilePath;
                    this.Documents.Add(item);
                }
            }

            var metaDataDocuments = task.Result as Collection<MediaMetaData>;
            if (metaDataDocuments != null)
            {
                foreach (var mediaInfo in metaDataDocuments.ToList().OrderBy(d => d.OriginalFileName))
                {
                    var item = new DocumentItem { Name = mediaInfo.OriginalFileName };
                    item.Icon = GetDocumentItemIcon(item.Name);
                    item.Source = mediaInfo;
                    this.Documents.Add(item);
                }
            }

            // Add any documents found in the media collection.
            // This is usually the case when documents are created by copying them and they are not yet in the db.
            foreach (var media in this.model.Media.Where(m => m.Type == MediaType.Document).OrderBy(d => d.OriginalFileName))
            {
                var item = new DocumentItem { Name = media.OriginalFileName };
                item.Icon = GetDocumentItemIcon(item.Name);
                item.Source = media;
                this.Documents.Add(item);
            }

            this.IsLoaded = true;
            this.IsChanged = false;
        }

        /// <summary>
        /// Handles the collection changed event on the documents collection
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Collections.Specialized.NotifyCollectionChangedEventArgs"/> instance containing the event data.</param>
        private void DocumentsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add || e.Action == NotifyCollectionChangedAction.Remove)
            {
                this.IsChanged = true;
            }
        }

        /// <summary>
        /// Gets the icon of a document item based on its file name.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns>An image containing the icon.</returns>
        private ImageSource GetDocumentItemIcon(string fileName)
        {
            if (string.IsNullOrWhiteSpace(fileName))
            {
                return null;
            }

            ImageSource icon = null;
            try
            {
                string ext = System.IO.Path.GetExtension(fileName);
                icon = this.iconExtractor.GetIconForFileType(ext);
            }
            catch
            {
            }

            if (icon == null && Application.Current != null)
            {
                icon = Application.Current.Resources["DocumentIcon"] as ImageSource;
            }

            return icon;
        }

        /// <summary>
        /// Adds a new user selected document to the view.
        /// </summary>
        private void AddDocument()
        {
            var addedItems = new List<object>();

            this.fileDialogService.Reset();
            this.fileDialogService.Filter = LocalizedResources.DialogFilter_OpenDocument;
            this.fileDialogService.Multiselect = true;
            this.fileDialogService.FilterIndex = this.lastOpenDialogFilterIndex;

            bool? result = this.fileDialogService.ShowOpenFileDialog();
            if (result == true)
            {
                if (this.Documents.Count + this.fileDialogService.FileNames.Count > 10)
                {
                    this.windowService.MessageDialogService.Show(LocalizedResources.DocumentsManagement_ToManyDocuments, MessageDialogType.Info);
                    return;
                }

                foreach (string filePath in this.fileDialogService.FileNames)
                {
                    try
                    {
                        FileInfo fi = new FileInfo(filePath);
                        if (!fi.Exists)
                        {
                            this.windowService.MessageDialogService.Show(LocalizedResources.Error_FileNotExists, MessageDialogType.Error);
                            continue;
                        }

                        if (fi.Length > ZPKTool.Common.Constants.MaxDocumentSize)
                        {
                            string msg = string.Format(
                                LocalizedResources.Error_FileTooBig,
                                (ZPKTool.Common.Constants.MaxDocumentSize / 1024 / 1024).ToString());
                            this.windowService.MessageDialogService.Show(msg, MessageDialogType.Error);
                            continue;
                        }
                    }
                    catch (SecurityException)
                    {
                        this.windowService.MessageDialogService.Show(LocalizedResources.Error_FilePermissionDenied, MessageDialogType.Error);
                        continue;
                    }
                    catch (UnauthorizedAccessException)
                    {
                        this.windowService.MessageDialogService.Show(LocalizedResources.Error_FilePermissionDenied, MessageDialogType.Error);
                        continue;
                    }
                    catch (ArgumentException)
                    {
                        this.windowService.MessageDialogService.Show(LocalizedResources.Error_InvalidFileName, MessageDialogType.Error);
                        continue;
                    }
                    catch (NotSupportedException)
                    {
                        this.windowService.MessageDialogService.Show(LocalizedResources.Error_InvalidFileName, MessageDialogType.Error);
                        continue;
                    }
                    catch (PathTooLongException)
                    {
                        this.windowService.MessageDialogService.Show(LocalizedResources.Error_FilePathTooLong, MessageDialogType.Error);
                        continue;
                    }
                    catch (Exception ex)
                    {
                        log.ErrorException("Unknown error.", ex);
                        this.windowService.MessageDialogService.Show(LocalizedResources.Error_Internal, MessageDialogType.Error);
                        continue;
                    }

                    // Create the document item corresponding to the selected file
                    DocumentItem item = new DocumentItem();
                    item.Name = filePath;
                    item.Icon = GetDocumentItemIcon(item.Name);
                    item.Source = filePath;
                    item.IsNew = true;
                    this.Documents.Add(item);
                    addedItems.Add(item);
                }

                this.Push(new UndoableItem(null, UndoActionType.Insert, addedItems));
            }

            lastOpenDialogFilterIndex = this.fileDialogService.FilterIndex;
        }

        /// <summary>
        /// Opens the document specified by a DocumentItem instance.
        /// </summary>
        /// <param name="documentItem">The document item.</param>
        private void OpenDocument(DocumentItem documentItem)
        {
            bool deleteAfterOpen = false;

            // The path in which the documents will be temporary saved and opened from.
            string path = null;

            // If the document item's source is a path open it from there.
            string docPath = documentItem.Source as string;
            if (docPath != null)
            {
                path = docPath;
            }
            else
            {
                // If the document item's source is a MediaMetaData instance, save it in the Temp folder using a random name.
                MediaMetaData docMetaData = documentItem.Source as MediaMetaData;
                if (docMetaData != null)
                {
                    try
                    {
                        path = Path.GetTempPath() + "\\" + Path.GetRandomFileName() + Path.GetExtension(docMetaData.OriginalFileName);
                        deleteAfterOpen = true;
                    }
                    catch (Exception ex)
                    {
                        log.ErrorException("Error while trying to obtain the extension of file to open " + path, ex);
                        this.windowService.MessageDialogService.Show(LocalizedResources.Error_FailedOpenFile, MessageDialogType.Error);
                    }
                }
                else
                {
                    // If the document item's source is a Media instance, save it in the Temp folder using a random name.
                    Media document = documentItem.Source as Media;
                    if (document != null)
                    {
                        try
                        {
                            path = Path.GetTempPath() + "\\" + Path.GetRandomFileName() + Path.GetExtension(document.OriginalFileName);
                            deleteAfterOpen = true;
                        }
                        catch (Exception ex)
                        {
                            log.ErrorException("Error while trying to obtain the extension of file to open " + path, ex);
                            this.windowService.MessageDialogService.Show(LocalizedResources.Error_FailedOpenFile, MessageDialogType.Error);
                        }
                    }
                }
            }

            SaveDocumentToFile(documentItem, path);

            if (File.Exists(path))
            {
                FileOpenHandler fileOpenHandler = new FileOpenHandler(path, deleteAfterOpen);
            }
            else
            {
                this.windowService.MessageDialogService.Show(LocalizedResources.Error_InvalidFileName, MessageDialogType.Error);
            }
        }

        /// <summary>
        /// Saves the document to disk.
        /// </summary>
        /// <param name="documentItem">The document item.</param>
        private void SaveDocumentToDisk(DocumentItem documentItem)
        {
            this.fileDialogService.Reset();
            this.fileDialogService.FileName = System.IO.Path.GetFileName(documentItem.Name);
            string fileExt = Path.GetExtension(this.fileDialogService.FileName);
            if (!string.IsNullOrEmpty(fileExt))
            {
                this.fileDialogService.Filter = string.Format("*{0}|*{0}|", fileExt) + LocalizedResources.DialogFilter_AllFiles;
            }
            else
            {
                this.fileDialogService.Filter = LocalizedResources.DialogFilter_AllFiles;
            }

            bool? result = this.fileDialogService.ShowSaveFileDialog();
            if (result == true)
            {
                SaveDocumentToFile(documentItem, this.fileDialogService.FileName);
            }
        }

        /// <summary>
        /// Deletes the document.
        /// </summary>
        /// <param name="documentItem">The document item.</param>
        private void DeleteDocument(DocumentItem documentItem)
        {
            if (documentItem != null)
            {
                var documentIndex = this.Documents.IndexOf(documentItem);
                this.Documents.Remove(documentItem);
                this.deletedDocuments.Add(documentItem);
                this.Push(new UndoableItem(null, UndoActionType.Delete, new List<object>() { documentItem }) { OldStartingIndex = documentIndex });
            }
        }

        /// <summary>
        /// Saves the document to a file.
        /// </summary>
        /// <param name="document">The document.</param>
        /// <param name="filePath">The file path.</param>
        private void SaveDocumentToFile(DocumentItem document, string filePath)
        {
            if (string.IsNullOrWhiteSpace(filePath))
            {
                return;
            }

            // If the document's source is a path, copy it from there to the destination.
            string docPath = document.Source as string;
            if (docPath != null)
            {
                try
                {
                    if (!filePath.Equals(docPath, StringComparison.InvariantCultureIgnoreCase))
                    {
                        File.Copy(docPath, filePath, true);
                    }
                }
                catch (Exception e)
                {
                    log.ErrorException("Failed to write file.", e);
                    this.windowService.MessageDialogService.Show(LocalizedResources.Error_FileWriteFailed, MessageDialogType.Error);
                }
            }
            else
            {
                // If the document's source is a MediaMetaData instance, get the corresponding media object from the database and save it to the destination.
                MediaMetaData docMetaData = document.Source as MediaMetaData;
                if (docMetaData != null)
                {
                    try
                    {
                        MediaManager mediaManager = new MediaManager(this.ModelDataSourceManager);
                        mediaManager.WriteMediaContentToFile(docMetaData, filePath);
                    }
                    catch (Exception ex)
                    {
                        log.ErrorException("Error while trying to save a document using meta data.", ex);
                        this.windowService.MessageDialogService.Show(LocalizedResources.Error_FileWriteFailed, MessageDialogType.Error);
                    }
                }
                else
                {
                    // If the document item's source is a Media instance, save it in the Temp folder using a random name.
                    Media docMedia = document.Source as Media;
                    if (docMedia != null)
                    {
                        try
                        {
                            using (FileStream destFile = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None, 8192))
                            {
                                destFile.Write(docMedia.Content, 0, docMedia.Content.Length);
                            }
                        }
                        catch (Exception ex)
                        {
                            log.ErrorException("Failed to write file.", ex);
                            this.windowService.MessageDialogService.Show(LocalizedResources.Error_FileWriteFailed, MessageDialogType.Error);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Performs the save operation. Executed by the SaveCommand.
        /// </summary>
        protected override void Save()
        {
            if (!this.IsLoaded)
            {
                return;
            }

            // Delete all removed documents
            var mediaManager = new MediaManager(this.ModelDataSourceManager);
            foreach (Media media in this.deletedDocuments.Where(doc => doc.Source is Media).Select(doc => doc.Source as Media))
            {
                mediaManager.DeleteMedia(media);
            }

            List<MediaMetaData> metadata = this.deletedDocuments.Where(doc => doc.Source is MediaMetaData).Select(doc => doc.Source as MediaMetaData).ToList();
            mediaManager.DeleteBatchMedia(metadata);
            this.RemoveDeletedMediaMetaDataFromUndoStack(metadata);

            this.deletedDocuments.Clear();

            var isMasterData = false;
            var masterObj = this.model as IMasterDataObject;
            if (masterObj != null)
            {
                isMasterData = masterObj.IsMasterData;
            }

            User owner = null;
            var ownedObj = this.model as IOwnedObject;
            if (ownedObj != null)
            {
                owner = ownedObj.Owner;
            }

            // Set the maximum documents batch size to save one time to database to half the max size of a document
            int maxDocsSize = ZPKTool.Common.Constants.MaxDocumentSize / 2;
            int currentDocsSize = 0;

            // Attach to the entity the newly added documents            
            foreach (DocumentItem docItem in this.Documents.Where(doc => doc.IsNew))
            {
                var src = docItem.Source as string;
                if (src != null)
                {
                    var newMedia = ReadDocumentFile(src);
                    newMedia.IsMasterData = isMasterData;
                    newMedia.Owner = owner;

                    this.ReplaceDocumentSourceWithMedia(docItem, newMedia);
                    this.model.Media.Add(newMedia);

                    currentDocsSize += newMedia.Size ?? 0;
                    if (currentDocsSize >= maxDocsSize)
                    {
                        this.ModelDataSourceManager.SaveChanges();
                        currentDocsSize = 0;
                    }
                }
                else
                {
                    var media = docItem.Source as Media;
                    if (media != null)
                    {
                        media.IsMasterData = isMasterData;
                        media.Owner = owner;
                        this.model.Media.Add(media);
                    }
                }
            }

            this.ModelDataSourceManager.SaveChanges();
            var currentDocuments = new List<DocumentItem>();
            currentDocuments.AddRange(this.Documents);
            this.Documents.Clear();

            // Get the meta data of the entity's docs from the the db and display that info.
            var documents = mediaManager.GetDocumentsMetaData(this.model);
            foreach (MediaMetaData mediaInfo in documents.ToList().OrderBy(d => d.OriginalFileName))
            {
                DocumentItem item = this.Documents.FirstOrDefault(d => d != null && (((d.Source is Media) && (d.Source as Media).Guid == mediaInfo.Guid)
                        || ((d.Source is MediaMetaData) && (d.Source as MediaMetaData).Guid == mediaInfo.Guid)));
                if (item == null)
                {
                    item = new DocumentItem();
                    this.Documents.Add(item);
                }

                item.Name = mediaInfo.OriginalFileName;
                item.Icon = GetDocumentItemIcon(item.Name);
                item.Source = mediaInfo;
                this.UpdateDocumentItemMedia(item);
            }

            this.deletedDocuments.Clear();
            this.IsChanged = false;
        }

        /// <summary>
        /// Cancels all changes. Executed by the CancelCommand.
        /// </summary>
        protected override void Cancel()
        {
            if (!this.IsLoaded)
            {
                return;
            }

            var currentDocuments = new List<DocumentItem>();
            var addedDocuments = new List<object>();

            currentDocuments.AddRange(this.Documents);
            this.Documents.Clear();

            // Get the meta data of the entity's docs from the the db and display that info.
            MediaManager mediaManager = new MediaManager(this.ModelDataSourceManager);
            var documents = mediaManager.GetDocumentsMetaData(this.model);

            foreach (MediaMetaData mediaInfo in documents.ToList().OrderBy(d => d.OriginalFileName))
            {
                DocumentItem item;
                var documentItem = currentDocuments.FirstOrDefault(d => d != null && (d.Source is MediaMetaData) && (d.Source as MediaMetaData).Guid == mediaInfo.Guid);
                var deletedItem = this.deletedDocuments.FirstOrDefault(d => d != null && (d.Source is MediaMetaData) && (d.Source as MediaMetaData).Guid == mediaInfo.Guid);

                if (documentItem != null)
                {
                    item = documentItem;
                    currentDocuments.Remove(documentItem);
                }
                else if (deletedItem != null)
                {
                    item = deletedItem;
                    addedDocuments.Add(deletedItem);
                }
                else
                {
                    item = new DocumentItem();
                }

                item.Name = mediaInfo.OriginalFileName;
                item.Icon = GetDocumentItemIcon(item.Name);
                item.Source = mediaInfo;
                this.Documents.Add(item);
            }

            if (currentDocuments.Count > 0)
            {
                var removedDocuments = new List<object>();
                removedDocuments.AddRange(currentDocuments);
                this.Push(new UndoableItem(null, UndoActionType.Delete, removedDocuments));
            }

            if (addedDocuments.Count > 0)
            {
                this.Push(new UndoableItem(null, UndoActionType.Insert, addedDocuments));
            }

            this.deletedDocuments.Clear();
            this.IsChanged = false;
        }

        /// <summary>
        /// Reads a document file from the disk.
        /// </summary>
        /// <param name="path">The path to the file.</param>
        /// <returns>A document (Media instance) or null if the file could not be opened 
        /// or exceeds the maximum allowed size.</returns>
        /// <exception cref="ZPKException">Failed to read the file, for any reason.</exception>
        private Media ReadDocumentFile(string path)
        {
            Media document = null;
            if (!string.IsNullOrEmpty(path))
            {
                try
                {
                    // Read the new document content from the disk and add it to the document object.
                    using (FileStream sourceFile = new FileStream(path, FileMode.Open, FileAccess.Read))
                    {
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            using (ZipFile zip = new ZipFile())
                            {
                                var fileName = System.IO.Path.GetFileName(path);
                                var newZipEntry = zip.AddEntry(fileName, sourceFile);
                                zip.Save(memoryStream);

                                document = new Media();
                                document.Type = MediaType.Document;
                                document.Content = memoryStream.ToArray();
                                document.Size = (int?)newZipEntry.CompressedSize;
                                document.OriginalFileName = System.IO.Path.GetFileName(path);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    document = null;
                    log.ErrorException("Failed to read file " + path, e);
                    throw new ZPKException(ErrorCodes.FileReadFail, LocalizedResources.Error_FileReadFailed);
                }
            }

            return document;
        }

        #endregion Private Methods

        #region Undo Manager

        /// <summary>
        /// Actions can only be undone if there are items in the <see cref="UndoStack"/>.
        /// </summary>
        /// <returns>True - if the action can be undone / False - otherwise.</returns>
        bool IUndoManager.CanUndo()
        {
            return this.undoStack.Count > 0;
        }

        /// <summary>
        /// Undo the last view-model change.
        /// </summary>
        void IUndoManager.Undo()
        {
            var undoableItem = this.undoStack.Last();
            this.undoStack.RemoveAt(this.undoStack.Count - 1);

            switch (undoableItem.ActionType)
            {
                case UndoActionType.Insert:
                    {
                        foreach (var value in undoableItem.Values)
                        {
                            var documentItem = value as DocumentItem;
                            if (documentItem != null)
                            {
                                this.Documents.Remove(documentItem);
                                this.deletedDocuments.Add(documentItem);
                            }
                        }

                        break;
                    }

                case UndoActionType.Delete:
                    {
                        foreach (var value in undoableItem.Values)
                        {
                            var documentItem = value as DocumentItem;
                            if (documentItem != null && undoableItem.OldStartingIndex <= this.Documents.Count)
                            {
                                if (undoableItem.Values.Count == 1)
                                {
                                    this.Documents.Insert(undoableItem.OldStartingIndex, documentItem);
                                }
                                else
                                {
                                    this.Documents.Add(documentItem);
                                }

                                if (this.deletedDocuments.Contains(documentItem))
                                {
                                    this.deletedDocuments.Remove(documentItem);
                                }
                            }
                        }

                        break;
                    }

                default:
                    break;
            }

            this.IsChanged = true;
        }

        /// <summary>
        /// Clear all items from the undo list.
        /// </summary>
        public void Reset()
        {
            this.undoStack.Clear();
        }

        /// <summary>
        /// Inserts an object at the top of the undoStack.
        /// </summary>
        /// <param name="item">The undoable item added.</param>
        private void Push(UndoableItem item)
        {
            this.undoStack.Add(item);
            this.UndoManager.Push(new UndoableItem(this, UndoActionType.UndoManager) { ViewModelInstance = this });
        }

        /// <summary>
        /// Replace in undo stack the source of a document item with the new media object passed as parameter.
        /// </summary>
        /// <param name="oldDocumentItem">The document item.</param>
        /// <param name="newMedia">The media source.</param>
        private void ReplaceDocumentSourceWithMedia(DocumentItem oldDocumentItem, Media newMedia)
        {
            foreach (var item in this.undoStack)
            {
                foreach (var itemValue in item.Values)
                {
                    var documentItem = itemValue as DocumentItem;
                    if (documentItem != null
                        && documentItem.Id == oldDocumentItem.Id
                        && documentItem.Source == oldDocumentItem.Source)
                    {
                        documentItem.Source = newMedia;
                    }
                }
            }
        }

        /// <summary>
        /// Update the undo stack documents source Media with MediaMetaData.
        /// </summary>
        /// <param name="currentDocument">The document item.</param>
        private void UpdateDocumentItemMedia(DocumentItem currentDocument)
        {
            foreach (var item in this.undoStack)
            {
                int index = 0;
                while (index < item.Values.Count)
                {
                    var documentItem = item.Values[index] as DocumentItem;
                    if (documentItem != null)
                    {
                        var documentMedia = documentItem.Source as Media;
                        var currentDocumentMetaData = currentDocument.Source as MediaMetaData;
                        if (documentMedia != null && currentDocumentMetaData != null
                            && documentMedia.Guid == currentDocumentMetaData.Guid)
                        {
                            item.Values[index] = currentDocument;
                        }
                    }

                    index++;
                }
            }
        }

        /// <summary>
        /// Remove a mediaMetaData object from the undo stack.
        /// </summary>
        /// <param name="mediaMetaData">The media meta data.</param>
        private void RemoveDeletedMediaMetaDataFromUndoStack(List<MediaMetaData> mediaMetaData)
        {
            foreach (var item in this.undoStack.ToList())
            {
                foreach (var itemValue in item.Values.ToList())
                {
                    var documentItem = itemValue as DocumentItem;
                    if (documentItem != null && mediaMetaData.Contains(documentItem.Source))
                    {
                        item.Values.Remove(itemValue);
                    }
                }

                if (item.Values.Count == 0)
                {
                    this.undoStack.Remove(item);
                }
            }
        }

        #endregion Undo Manager

        #region Inner classes

        /// <summary>
        /// This class represents an item in the documents list displayed on the UI.
        /// </summary>
        public class DocumentItem
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="DocumentItem"/> class.
            /// </summary>
            public DocumentItem()
            {
                this.IsNew = false;
                this.Id = Guid.NewGuid();
            }

            /// <summary>
            /// Gets or sets the document identifier.
            /// </summary>
            public Guid Id { get; set; }

            /// <summary>
            /// Gets or sets the name of the document.
            /// </summary>            
            public string Name { get; set; }

            /// <summary>
            /// Gets or sets the icon of the document.
            /// </summary>
            public ImageSource Icon { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether the is newly added and must be saved in the database.
            /// </summary>            
            public bool IsNew { get; set; }

            /// <summary>
            /// Gets or sets the source of the document.
            /// It can be a string (the path to the document), a MediaMetadata instance or a Media instance.
            /// </summary>
            public object Source { get; set; }
        }

        #endregion Inner classes
    }
}