﻿using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.DataAnnotations;
using System.Windows.Input;
using ZPKTool.Controls;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the IMyView view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class SupplierViewModel : ViewModel<Customer, IDataSourceManager>
    {
        #region Attributes

        /// <summary>
        /// The window service.
        /// </summary>
        private readonly IWindowService windowService;

        /// <summary>
        /// The MEF composition container.
        /// </summary>
        private readonly CompositionContainer compositionContainer;

        #endregion Attributes

        /// <summary>
        /// Initializes a new instance of the <see cref="SupplierViewModel"/> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        /// <param name="compositionContainer">The composition container.</param>
        [ImportingConstructor]
        public SupplierViewModel(IWindowService windowService, CompositionContainer compositionContainer)
        {
            this.windowService = windowService;
            this.compositionContainer = compositionContainer;

            this.IsChanged = false;
            this.ShowSaveControls = false;

            this.BrowseSupplierMasterDataCommand = new DelegateCommand(BrowseSupplierMasterData);
            this.BrowseCountryMasterDataCommand = new DelegateCommand(BrowseCountryMasterData);
            this.BrowseCountrySupplierMasterDataCommand = new DelegateCommand(BrowseCountrySupplierMasterData);
        }

        #region Commands

        /// <summary>
        /// Gets the browse supplier master data command.
        /// </summary>
        public ICommand BrowseSupplierMasterDataCommand { get; private set; }

        /// <summary>
        /// Gets the browse country master data command.
        /// </summary>
        public ICommand BrowseCountryMasterDataCommand { get; private set; }

        /// <summary>
        /// Gets the browse country supplier master data command.
        /// </summary>
        public ICommand BrowseCountrySupplierMasterDataCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets the name.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Name", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Name")]
        public DataProperty<string> Name { get; private set; }

        /// <summary>
        /// Gets the type.
        /// </summary>
        [UndoableProperty]
        [ExposesModelProperty("Type")]
        public DataProperty<SupplierType?> Type { get; private set; }

        /// <summary>
        /// Gets the number.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Number")]
        public DataProperty<string> Number { get; private set; }

        /// <summary>
        /// Gets the description.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Description")]
        public DataProperty<string> Description { get; private set; }

        /// <summary>
        /// Gets the street address.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("StreetAddress")]
        public DataProperty<string> StreetAddress { get; private set; }

        /// <summary>
        /// Gets the city.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("City")]
        public DataProperty<string> City { get; private set; }

        /// <summary>
        /// Gets the zip code.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("ZipCode")]
        public DataProperty<string> ZipCode { get; private set; }

        /// <summary>
        /// Gets the country.
        /// </summary>
        [UndoableProperty]
        [ExposesModelProperty("Country")]
        public DataProperty<string> Country { get; private set; }

        /// <summary>
        /// Gets the country supplier.
        /// </summary>
        [UndoableProperty]
        [ExposesModelProperty("State")]
        public DataProperty<string> CountrySupplier { get; private set; }

        /// <summary>
        /// Gets the first contact person name.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("FirstContactName")]
        public DataProperty<string> FirstContactName { get; private set; }

        /// <summary>
        /// Gets the first contact person phone no.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("FirstContactPhone")]
        public DataProperty<string> FirstContactPhoneNo { get; private set; }

        /// <summary>
        /// Gets the first contact person fax no.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("FirstContactFax")]
        public DataProperty<string> FirstContactFaxNo { get; private set; }

        /// <summary>
        /// Gets the first contact person mobile no.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("FirstContactMobile")]
        public DataProperty<string> FirstContactMobileNo { get; private set; }

        /// <summary>
        /// Gets the first contact person email.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("FirstContactEmail")]
        public DataProperty<string> FirstContactEmail { get; private set; }

        /// <summary>
        /// Gets the first contact person web page.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("FirstContactWebAddress")]
        public DataProperty<string> FirstContactWebPage { get; private set; }

        /// <summary>
        /// Gets the second contact person name.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("SecondContactName")]
        public DataProperty<string> SecondContactName { get; private set; }

        /// <summary>
        /// Gets the second contact person phone no.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("SecondContactPhone")]
        public DataProperty<string> SecondContactPhoneNo { get; private set; }

        /// <summary>
        /// Gets the second contact person fax no.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("SecondContactFax")]
        public DataProperty<string> SecondContactFaxNo { get; private set; }

        /// <summary>
        /// Gets the second contact person mobile no.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("SecondContactMobile")]
        public DataProperty<string> SecondContactMobileNo { get; private set; }

        /// <summary>
        /// Gets the second contact person email.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("SecondContactEmail")]
        public DataProperty<string> SecondContactEmail { get; private set; }

        /// <summary>
        /// Gets the second contact person web page.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("SecondContactWebAddress")]
        public DataProperty<string> SecondContactWebPage { get; private set; }

        #endregion Properties

        /// <summary>
        /// Called when the Model has changed.
        /// </summary>
        protected override void OnModelChanged()
        {
            this.LoadDataFromModel();
            this.UndoManager.Start();
        }

        /// <summary>
        /// Performs the save operation. Executed by the SaveCommand.
        /// </summary>
        protected override void Save()
        {
            this.CheckModelAndDataSource();

            // Save all changes back into the Model objects. The validity check of this operation is performed by the CanSave method.
            this.SaveToModel();

            // Update the supplier
            this.DataSourceManager.SupplierRepository.Save(this.Model);
            this.DataSourceManager.SaveChanges();

            // Close the view-model when is displayed in a window.
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Cancels all changes. Executed by the CancelCommand.
        /// </summary>
        protected override void Cancel()
        {
            if (this.IsChanged)
            {
                var result = MessageDialogResult.Yes;

                if (!this.IsChild)
                {
                    result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_CancelChanges, MessageDialogType.YesNo);
                }

                if (result != MessageDialogResult.Yes)
                {
                    // Don't cancel the changes and also don't close the view-model.
                    return;
                }

                // Cancel all changes
                base.Cancel();
            }

            // Close the view-model when is displayed in a window.
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Called before unloading the view from its parent or closing it. Returning false will cancel the view's unloading or closing.
        /// </summary>
        /// <returns>
        /// True if the unloading process should continue and false if it should be canceled.
        /// </returns>
        public override bool OnUnloading()
        {
            // Do nothing if is read-only or there was no changed made. If is nested, the root view-model will handle this.
            if (this.IsReadOnly || this.IsInViewerMode || !this.IsChanged || this.IsChild)
            {
                return true;
            }

            if (this.EditMode == ViewModelEditMode.Create)
            {
                // Ask the user to confirm quitting
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_Quit, MessageDialogType.YesNo);
                if (result != MessageDialogResult.Yes)
                {
                    // Don't quit.
                    return false;
                }
                else
                {
                    this.IsChanged = false;
                }
            }
            else if (this.EditMode == ViewModelEditMode.Edit)
            {
                // Ask the user if he wants to save
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_UnsavedDataOnQuit, MessageDialogType.YesNoCancel);
                if (result == MessageDialogResult.Yes)
                {
                    // The user whishes to save.
                    if (!this.CanSave())
                    {
                        // It is not possible to save because the input is not valid. Return false to stop the view-model from unloading.
                        return false;
                    }

                    this.Save();
                }
                else if (result == MessageDialogResult.Cancel)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Browses the supplier master data.
        /// </summary>
        private void BrowseSupplierMasterData()
        {
            var browserViewModel = this.compositionContainer.GetExportedValue<MasterDataBrowserViewModel>();
            browserViewModel.MasterDataSelected += (masterdataObject, context) =>
            {
                using (this.UndoManager.StartBatch(includePreviousItem: false, reverseUndoOrder: false, navigateToBatchControls: true, undoEachBatch: true))
                {
                    Customer supplier = masterdataObject as Customer;

                    if (supplier != null)
                    {
                        this.LoadDataFromModel(supplier);
                        this.IsChanged = true;
                    }
                }
            };

            browserViewModel.MasterDataType = typeof(Customer);
            this.windowService.ShowViewInDialog(browserViewModel);
        }

        /// <summary>
        /// Browses the country master data.
        /// </summary>
        private void BrowseCountryMasterData()
        {
            CountryAndSupplierBrowserViewModel viewmodel = this.compositionContainer.GetExportedValue<CountryAndSupplierBrowserViewModel>();
            viewmodel.LoadLocation = this.DataSourceManager.DatabaseId;
            viewmodel.CountryOrSupplierSelected += (masterDataObject, ctx) =>
            {
                using (this.UndoManager.StartBatch(includePreviousItem: false, reverseUndoOrder: false, navigateToBatchControls: true, undoEachBatch: true))
                {
                    Country country = masterDataObject as Country;
                    if (country != null)
                    {
                        this.Country.Value = country.Name;
                        this.CountrySupplier.Value = null;
                    }
                }
            };

            this.windowService.ShowViewInDialog(viewmodel);
        }

        /// <summary>
        /// Browses the country supplier master data.
        /// </summary>
        private void BrowseCountrySupplierMasterData()
        {
            if (!string.IsNullOrWhiteSpace(this.Country.Value))
            {
                CountryAndSupplierBrowserViewModel viewmodel = this.compositionContainer.GetExportedValue<CountryAndSupplierBrowserViewModel>();
                viewmodel.LoadLocation = this.DataSourceManager.DatabaseId;
                viewmodel.CountryName = this.Country.Value;
                viewmodel.CountryOrSupplierSelected += (masterDataObject, ctx) =>
                {
                    using (this.UndoManager.StartBatch(includePreviousItem: false, reverseUndoOrder: false, navigateToBatchControls: true, undoEachBatch: true))
                    {
                        CountryState supplier = masterDataObject as CountryState;
                        if (supplier != null)
                        {
                            this.CountrySupplier.Value = supplier.Name;
                        }
                    }
                };
                this.windowService.ShowViewInDialog(viewmodel);
            }
            else
            {
                this.windowService.MessageDialogService.Show(LocalizedResources.General_SelectCountryFirst, MessageDialogType.Error);
            }
        }
    }
}