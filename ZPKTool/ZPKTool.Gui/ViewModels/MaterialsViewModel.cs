﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using ZPKTool.Business;
using ZPKTool.Business.Export;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Managers;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for the materials list
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class MaterialsViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The delay to display the please wait service message for executing the paste command into material datagrid.
        /// </summary>
        private const int DisplayDelay = 1000;

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The composition container.
        /// </summary>
        private CompositionContainer container;

        /// <summary>
        /// The messenger reference
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The window service reference
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The service used to create open/save file dialogs.
        /// </summary>
        private IFileDialogService fileDialogService;

        /// <summary>
        /// The service used to brows for folders.
        /// </summary>
        private IFolderBrowserService folderBrowserService;

        /// <summary>
        /// The PleaseWaitService reference.
        /// </summary>
        private IPleaseWaitService pleaseWaitService;

        /// <summary>
        /// The collection of objects which are used in the binding on the data grid
        /// </summary>
        private ObservableCollection<MaterialsItem> materials = new ObservableCollection<MaterialsItem>();

        /// <summary>
        /// The currently selected item in the grid
        /// </summary>
        private ObservableCollection<object> selectedItems = new ObservableCollection<object>();

        /// <summary>
        /// The currently selected entities info data.
        /// </summary>
        private ObservableCollection<EntityInfo> selectedEntitiesInfo = new ObservableCollection<EntityInfo>();

        /// <summary>
        /// The reference to the part for which we display the materials
        /// </summary>
        private Part parentPart;

        /// <summary>
        /// The view/view-model displaying the details of the selected material.
        /// </summary>
        private object selectedMaterialDetails;

        /// <summary>
        /// Extra information for the report.
        /// </summary>
        private ExtendedDataGridAdditionalReportInformation additionalReportInformation;

        /// <summary>
        /// Indicates whether the parent is part or not (is raw part).
        /// </summary>
        private bool parentIsPart;

        /// <summary>
        /// The data context.
        /// </summary>
        private IDataSourceManager partDataContext;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the MaterialsViewModel class.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="messenger">The messenger service.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="fileDialogService">The file dialog service.</param>
        /// <param name="folderBrowserService">The folder browser service.</param>
        /// <param name="pleaseWaitService">The please wait service.</param>
        /// <param name="unitsService">The units service.</param>
        [ImportingConstructor]
        public MaterialsViewModel(
            CompositionContainer container,
            IMessenger messenger,
            IWindowService windowService,
            IFileDialogService fileDialogService,
            IFolderBrowserService folderBrowserService,
            IPleaseWaitService pleaseWaitService,
            IUnitsService unitsService)
        {
            Argument.IsNotNull("container", container);
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("fileDialogService", fileDialogService);
            Argument.IsNotNull("folderBrowserService", folderBrowserService);
            Argument.IsNotNull("pleaseWaitService", pleaseWaitService);
            Argument.IsNotNull("unitsService", unitsService);

            this.container = container;
            this.messenger = messenger;
            this.WindowService = windowService;
            this.fileDialogService = fileDialogService;
            this.folderBrowserService = folderBrowserService;
            this.pleaseWaitService = pleaseWaitService;
            this.unitsService = unitsService;

            this.InitializeCommands();
            this.SelectedItems.CollectionChanged += SelectedItemsOnCollectionChanged;
        }

        #endregion Constructor

        #region Commands

        /// <summary>
        /// Gets the command for the add raw material button
        /// </summary>
        public ICommand CreateRawMaterialCommand { get; private set; }

        /// <summary>
        /// Gets the command for the add commodity button
        /// </summary>
        public ICommand CreateCommodityCommand { get; private set; }

        /// <summary>
        /// Gets the command for the add raw part button
        /// </summary>
        public ICommand CreateRawPartCommand { get; private set; }

        /// <summary>
        /// Gets the command for the edit button
        /// </summary>
        public ICommand EditCommand { get; private set; }

        /// <summary>
        /// Gets the command for the delete button
        /// </summary>
        public ICommand DeleteCommand { get; private set; }

        /// <summary>
        /// Gets the command for the mouse double click event
        /// </summary>
        public ICommand MouseDoubleClickCommand { get; private set; }

        /// <summary>
        /// Gets the command for the drag over and drag enter actions
        /// </summary>
        public ICommand CheckDropTargetCommand { get; private set; }

        /// <summary>
        /// Gets the command for the drop action
        /// </summary>
        public ICommand DropCommand { get; private set; }

        /// <summary>
        /// Gets the command for the copy action
        /// </summary>
        public ICommand CopyCommand { get; private set; }

        /// <summary>
        /// Gets the command for the paste action
        /// </summary>
        public ICommand PasteCommand { get; private set; }

        /// <summary>
        /// Gets the command for the import action
        /// </summary>
        public ICommand ImportCommand { get; private set; }

        /// <summary>
        /// Gets the command for the export action
        /// </summary>
        public ICommand ExportCommand { get; private set; }

        /// <summary>
        /// Gets the command for the copy to master data action
        /// </summary>
        public ICommand CopyToMasterDataCommand { get; private set; }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the collection of objects displayed in the data grid
        /// </summary>
        public ObservableCollection<MaterialsItem> Materials
        {
            get { return this.materials; }
            set { this.SetProperty(ref this.materials, value, () => this.Materials); }
        }

        /// <summary>
        /// Gets or sets the selected item in the entity data grid
        /// </summary>
        public ObservableCollection<object> SelectedItems
        {
            get { return this.selectedItems; }
            set { this.SetProperty(ref this.selectedItems, value, () => this.SelectedItems); }
        }

        /// <summary>
        /// Gets or sets the currently selected entities info data.
        /// </summary>
        public ObservableCollection<EntityInfo> SelectedEntitiesInfo
        {
            get
            {
                return this.selectedEntitiesInfo;
            }

            set
            {
                if (!Equals(this.selectedEntitiesInfo, value))
                {
                    this.selectedEntitiesInfo = value;
                    OnPropertyChanged(() => this.SelectedEntitiesInfo);
                }
            }
        }

        /// <summary>
        /// Gets or sets the window service.
        /// </summary>
        public IWindowService WindowService
        {
            get { return this.windowService; }
            set { this.SetProperty(ref this.windowService, value, () => this.WindowService); }
        }

        /// <summary>
        /// Gets or sets the parent part for which we display the materials
        /// </summary>
        public Part ParentPart
        {
            get
            {
                return this.parentPart;
            }

            set
            {
                this.SetProperty(ref this.parentPart, value, () => this.ParentPart, OnParentPartChanged);
            }
        }

        /// <summary>
        /// Gets or sets the data source manager from which the materials are loaded
        /// </summary>
        public IDataSourceManager PartDataContext
        {
            get
            {
                return this.partDataContext;
            }

            set
            {
                if (this.partDataContext != value)
                {
                    this.partDataContext = value;
                    this.OnPropertyChanged(() => this.PartDataContext);
                }

                this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(value);
            }
        }

        /// <summary>
        /// Gets or sets the id of the item to select after the master data is loaded.
        /// </summary>        
        public Guid ItemToSelectId { get; set; }

        /// <summary>
        /// Gets or sets the view/view-model displaying the details of the selected material.
        /// </summary>
        public object SelectedMaterialDetails
        {
            get { return this.selectedMaterialDetails; }
            set { this.SetProperty(ref this.selectedMaterialDetails, value, () => this.SelectedMaterialDetails); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [parent is part].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [parent is part]; otherwise, <c>false</c>.
        /// </value>
        public bool ParentIsPart
        {
            get { return this.parentIsPart; }
            set { this.SetProperty(ref this.parentIsPart, value, () => this.ParentIsPart); }
        }

        /// <summary>
        /// Gets or sets the extra report information.
        /// </summary>
        public ExtendedDataGridAdditionalReportInformation AdditionalReportInformation
        {
            get
            {
                return this.additionalReportInformation;
            }

            set
            {
                if (this.additionalReportInformation != value)
                {
                    this.additionalReportInformation = value;
                    OnPropertyChanged(() => this.AdditionalReportInformation);
                }
            }
        }

        /// <summary>
        /// Gets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter { get; private set; }

        /// <summary>
        /// Gets the command parameter used for the import command.
        /// </summary>
        public IEnumerable<Type> ImportCommandParameter { get; private set; }

        #endregion

        #region Initialize

        /// <summary>
        /// Initializes the commands.
        /// </summary>
        private void InitializeCommands()
        {
            this.CreateRawMaterialCommand = new DelegateCommand(this.AddRawMaterial, () => !this.IsReadOnly);
            this.CreateCommodityCommand = new DelegateCommand(this.AddCommodity, () => !this.IsReadOnly);
            this.CreateRawPartCommand = new DelegateCommand(this.AddRawPart, () => !this.IsReadOnly && !(ParentPart is RawPart));
            this.DeleteCommand = new DelegateCommand(this.ExecuteDelete, this.CanExecuteDelete);
            this.EditCommand = new DelegateCommand(this.ExecuteEdit, this.CanExecuteEdit);
            this.CopyCommand = new DelegateCommand(this.ExecuteCopy, this.CanExecuteCopy);
            this.ImportCommand = new DelegateCommand<object>(this.ExecuteImport, o => !this.IsReadOnly);
            this.ExportCommand = new DelegateCommand(this.ExecuteExport, this.CanExecuteExport);

            this.MouseDoubleClickCommand = new DelegateCommand<MouseButtonEventArgs>(this.MouseDoubleClicked);
            this.CheckDropTargetCommand = new DelegateCommand<DragEventArgs>(this.CheckDropTarget);
            this.DropCommand = new DelegateCommand<DragEventArgs>(this.DropAction);
            this.PasteCommand = new DelegateCommand(this.ExecutePaste, this.CanPaste);
        }

        #endregion Initialize

        #region Commands handling

        /// <summary>
        /// Handles the double click event on the data grid
        /// </summary>
        /// <param name="e">The mouse button event args</param>
        private void MouseDoubleClicked(MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                this.ExecuteEdit();
            }
        }

        /// <summary>
        /// Performs the add raw material operation, creating a new entity and adding it in the data grid
        /// </summary>
        private void AddRawMaterial()
        {
            var rawMaterialVm = this.container.GetExportedValue<RawMaterialViewModel>();
            rawMaterialVm.DataSourceManager = this.PartDataContext;
            rawMaterialVm.InitializeForCreation(false, this.ParentPart, this.ParentPart.CalculationVariant);
            rawMaterialVm.Title = LocalizedResources.General_CreateRawMaterial;
            rawMaterialVm.IsInViewerMode = IsInViewerMode;

            this.WindowService.ShowViewInDialog(rawMaterialVm, "RawMaterialWindowViewTemplate");

            var newRawMaterial = rawMaterialVm.Model;
            if (newRawMaterial != null && this.ParentPart.RawMaterials.Contains(newRawMaterial))
            {
                var newItem = new MaterialsItem(newRawMaterial);
                this.Materials.Add(newItem);
                this.SelectedItems.Add(newItem);

                // the parent part will be refreshed in the tree
                var message = new EntityChangedMessage(Notification.MyProjectsEntityChanged);
                message.ChangeType = EntityChangeType.EntityCreated;
                message.Entity = newRawMaterial;
                message.Parent = this.ParentPart;
                this.messenger.Send(message);
            }
        }

        /// <summary>
        /// Performs the add commodity operation, creating a new entity and adding it in the data grid
        /// </summary>
        private void AddCommodity()
        {
            var commodityVm = this.container.GetExportedValue<CommodityViewModel>();
            commodityVm.DataSourceManager = this.PartDataContext;
            commodityVm.InitializeForCreation(false, this.ParentPart);
            commodityVm.Title = LocalizedResources.General_CreateCommodity;

            this.WindowService.ShowViewInDialog(commodityVm, "CommodityWindowViewTemplate");

            var newCommodity = commodityVm.Model;
            if (newCommodity != null && this.ParentPart.Commodities.Contains(newCommodity))
            {
                var newItem = new MaterialsItem(newCommodity);
                this.Materials.Add(newItem);
                this.SelectedItems.Add(newItem);
            }
        }

        /// <summary>
        /// Adds the raw part.
        /// </summary>
        private void AddRawPart()
        {
            if (ParentPart.RawPart != null)
            {
                this.windowService.MessageDialogService.Show(
                    this.ParentPart.RawPart.IsDeleted
                        ? LocalizedResources.Error_PartAlreadyContainsDeletedRawPart
                        : LocalizedResources.Error_PartAlreadyContainsRawPart,
                    MessageDialogType.Error);

                return;
            }

            var partVm = this.container.GetExportedValue<PartViewModel>();
            partVm.DataSourceManager = this.PartDataContext;
            partVm.InitializeForCreation(false, this.ParentPart, true);
            this.windowService.ShowViewInDialog(partVm, "RawPartWindowViewTemplate");

            var newRawPart = partVm.Model as RawPart;
            if (newRawPart == this.ParentPart.RawPart)
            {
                var newItem = new MaterialsItem(newRawPart);
                this.Materials.Add(newItem);
                this.SelectedItems.Add(newItem);

                // the parent part will be refreshed in the tree
                var message = new EntityChangedMessage(Notification.MyProjectsEntityChanged);
                message.ChangeType = EntityChangeType.EntityCreated;
                message.Entity = newRawPart;
                message.Parent = this.ParentPart;
                this.messenger.Send(message);
            }
        }

        /// <summary>
        /// Determines whether the ExecuteDelete command can be executed.
        /// </summary>
        /// <returns>
        /// true if the command can be executed with the specified parameter; otherwise, false.
        /// </returns>
        private bool CanExecuteDelete()
        {
            return !this.IsReadOnly && this.SelectedItems != null && this.SelectedItems.Count > 0;
        }

        /// <summary>
        /// Deletes the selected materials.
        /// </summary>
        private void ExecuteDelete()
        {
            var selectedMaterials = this.SelectedItems.Cast<MaterialsItem>().ToList();
            if (selectedMaterials.Count == 0)
            {
                this.windowService.MessageDialogService.Show(
                           LocalizedResources.Error_NoItemSelected,
                           MessageDialogType.Info);
                return;
            }

            string questionMessage;
            var permanentlyDelete = Keyboard.Modifiers == ModifierKeys.Shift;
            var isMasterData = selectedMaterials.Any(m => EntityUtils.IsMasterData(m.Entity));

            /* 
             * If the Shift modifier is not pressed or the selected materials 
             * are not Masterdata then the deletion should be made to the 
             * trash bin else should be deleted permanently.
             */
            if (permanentlyDelete || isMasterData)
            {
                questionMessage = LocalizedResources.Question_PermanentlyDeleteSelectedItems;
            }
            else
            {
                questionMessage = LocalizedResources.Question_DeleteSelectedObjects;
            }

            if (this.windowService.MessageDialogService.Show(questionMessage, MessageDialogType.YesNo) != MessageDialogResult.Yes)
            {
                return;
            }

            Action<PleaseWaitService.WorkParams> work = (workParams) =>
                {
                    var materialIndex = 1;
                    foreach (var selectedMaterial in selectedMaterials)
                    {
                        string waitMessage;
                        if (selectedItems.Count == 1)
                        {
                            waitMessage = LocalizedResources.General_WaitItemDelete;
                        }
                        else
                        {
                            waitMessage = string.Format(
                                LocalizedResources.General_WaitDeleteObjects,
                                materialIndex,
                                selectedItems.Count);
                        }

                        this.pleaseWaitService.Message = waitMessage;
                        materialIndex++;

                        /*
                         * Delete the item.
                         */
                        var manager = new TrashManager(this.PartDataContext);
                        if (isMasterData || permanentlyDelete)
                        {
                            manager.DeleteByStoreProcedure(selectedMaterial.Entity);
                        }
                        else
                        {
                            manager.DeleteToTrashBin(selectedMaterial.Entity);
                            this.PartDataContext.SaveChanges();
                        }
                    }
                };

            Action<PleaseWaitService.WorkParams> workCompleted = (workParams) =>
                {
                    if (workParams.Error != null)
                    {
                        this.windowService.MessageDialogService.Show(workParams.Error);
                    }
                    else
                    {
                        foreach (var deletedMaterial in selectedMaterials)
                        {
                            /*
                             * Refresh the projects tree.
                             */
                            var notification = this.ParentPart.IsMasterData
                                                   ? Notification.MasterDataEntityChanged
                                                   : Notification.MyProjectsEntityChanged;
                            var message = new EntityChangedMessage(notification);
                            message.Entity = deletedMaterial.Entity;
                            message.ChangeType = EntityChangeType.EntityDeleted;
                            message.Parent = this.ParentPart;
                            this.messenger.Send(message);

                            /*
                             * Remove the material from the list
                             */
                            this.Materials.Remove(deletedMaterial);
                        }
                    }
                };

            this.pleaseWaitService.Show(LocalizedResources.General_WaitItemDelete, work, workCompleted);
        }

        /// <summary>
        /// Handler for the check drop target command, checks if the dragged item can be dropped in the materials grid
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        private void CheckDropTarget(DragEventArgs e)
        {
            e.Handled = true;

            // obtaining the dropped item
            var sourceItem = (DragAndDropData)e.Data.GetData(typeof(DragAndDropData));

            if (sourceItem != null
                && (sourceItem.DataObject is RawMaterial || sourceItem.DataObject is Commodity || sourceItem.DataObject is RawPart))
            {
                e.Effects = DragDropEffects.Copy;
            }
            else
            {
                e.Effects = DragDropEffects.None;
            }
        }

        /// <summary>
        /// Handler for the drop action, performs the copy paste operation
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        private void DropAction(DragEventArgs e)
        {
            e.Handled = true;

            // obtaining the dropped item
            var sourceItem = (DragAndDropData)e.Data.GetData(typeof(DragAndDropData));

            // copy paste the material from the source to the parent part
            ClipboardManager.Instance.Copy(sourceItem.DataObject, sourceItem.DataObjectContext);
            var pastedObject = ClipboardManager.Instance.Paste(this.ParentPart, this.PartDataContext).First();
            if (pastedObject.Error != null)
            {
                this.windowService.MessageDialogService.Show(pastedObject.Error);
            }
            else
            {
                // add the material in the data grid
                var pastedMaterial = new MaterialsItem(pastedObject.Entity);
                this.Materials.Add(pastedMaterial);
                this.SelectedItems.Add(pastedMaterial);

                // Notify the projects tree that an object has been created.
                var msg = new EntityChangedMessage(Notification.MyProjectsEntityChanged, this.ParentPart, null, EntityChangeType.EntityUpdated);
                this.messenger.Send(msg);
            }
        }

        /// <summary>
        /// Determines whether the ExecuteEdit command can be executed.
        /// </summary>
        /// <returns>
        /// true if the command can be executed with the specified parameter; otherwise, false.
        /// </returns>
        private bool CanExecuteEdit()
        {
            return this.SelectedItems != null && this.SelectedItems.Count == 1 && !this.IsReadOnly
                   && SecurityManager.Instance.CurrentUserHasRight(Right.ProjectsAndCalculate);
        }

        /// <summary>
        /// Edits the specified material.
        /// </summary>
        private void ExecuteEdit()
        {
            if (this.SelectedItems == null
                || this.SelectedItems.Count != 1)
            {
                return;
            }

            var selectedItem = this.SelectedItems.FirstOrDefault() as MaterialsItem;
            if (selectedItem == null
                || selectedItem.Entity == null)
            {
                return;
            }

            var dbId = this.PartDataContext != null ? this.PartDataContext.DatabaseId : DbIdentifier.NotSet;
            var msgToken = this.IsInViewerMode
                               ? GlobalMessengerTokens.ModelBrowserTargetToken
                               : GlobalMessengerTokens.MainViewTargetToken;
            this.messenger.Send(new NavigateToEntityMessage(selectedItem.Entity, dbId), msgToken);
        }

        /// <summary>
        /// Determines whether the Copy command can be executed.
        /// </summary>
        /// <returns>
        /// true if the command can be executed with the specified parameter; otherwise, false.
        /// </returns>
        private bool CanExecuteCopy()
        {
            return this.SelectedItems != null
                   && this.SelectedItems.Count != 0
                   && !this.IsReadOnly
                   && SecurityManager.Instance.CurrentUserHasRight(Right.ProjectsAndCalculate);
        }

        /// <summary>
        /// Copies the specified material to clipboard.
        /// </summary>
        private void ExecuteCopy()
        {
            var selectedMaterials = this.SelectedItems.Cast<MaterialsItem>().ToList();
            if (selectedMaterials.Count == 0 || selectedMaterials.Any(m => m.Entity == null))
            {
                return;
            }

            var itemsToCopy = selectedMaterials
               .Select(item => new ClipboardObject
               {
                   Entity = item.Entity,
                   DbSource = this.PartDataContext.DatabaseId
               });

            ClipboardManager.Instance.Copy(itemsToCopy);
        }

        /// <summary>
        /// Contains the logic associated with the Import command.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void ExecuteImport(object parameter)
        {
            if (!this.ImportCommandParameter.Any())
            {
                throw new ArgumentException("The parameter for Import must be an instance of the Type class or a list of Type objects.", "parameter");
            }

            // Set the file dialog service.
            this.fileDialogService.Filter = UIUtils.GetDialogFilterForMultipleImportedTypes(this.ImportCommandParameter);
            this.fileDialogService.Multiselect = true;
            if (this.fileDialogService.ShowOpenFileDialog() != true)
            {
                return;
            }

            var filePaths = new List<string>(this.fileDialogService.FileNames);

            Action<PleaseWaitService.WorkParams, IEnumerable<ImportedData<object>>> importCompleted = (workParams, importedData) =>
            {
                // Perform action after the import is completed.
                foreach (var entityData in importedData.Where(imp => imp.Entity != null))
                {
                    bool importedObjectIsMasterData = EntityUtils.IsMasterData(entityData.Entity);
                    string notification = importedObjectIsMasterData ? Notification.MasterDataEntityChanged : Notification.MyProjectsEntityChanged;

                    // Only the last imported item will be selected.
                    bool selectItem = entityData == importedData.LastOrDefault(i => i.Entity != null);

                    var item = new MaterialsItem(entityData.Entity);
                    this.Materials.Add(item);
                    if (selectItem)
                    {
                        this.SelectedItems.Add(item);
                    }

                    var msg = new EntityChangedMessage(notification)
                    {
                        // Create a message to notify that a new item has been imported.
                        Entity = entityData.Entity,
                        Parent = this.ParentPart,
                        ChangeType = EntityChangeType.EntityImported,
                        SelectEntity = selectItem
                    };
                    this.messenger.Send(msg);
                }
            };

            var importService = this.container.GetExportedValue<IImportService>();
            importService.Import(filePaths, this.ParentPart, this.PartDataContext, true, importCompleted);
        }

        /// <summary>
        /// Determines whether the Export command can be executed.
        /// </summary>
        /// <returns>
        /// true if the command can be executed with the specified parameter; otherwise, false.
        /// </returns>
        private bool CanExecuteExport()
        {
            return this.SelectedItems != null
                   && this.SelectedItems.Count != 0
                   && !this.IsInViewerMode;
        }

        /// <summary>
        /// Exports the specified material.
        /// </summary>
        private void ExecuteExport()
        {
            var selectedMaterials = this.SelectedItems.Cast<MaterialsItem>().ToList();
            if (selectedMaterials.Count == 0 ||
                selectedMaterials.Any(item => item == null || item.Entity == null))
            {
                return;
            }

            string filePath = null;
            if (selectedMaterials.Count == 1)
            {
                var defaultFileName = EntityUtils.GetEntityName(selectedMaterials.First().Entity);
                if (!string.IsNullOrWhiteSpace(defaultFileName))
                {
                    defaultFileName = PathUtils.SanitizeFileName(defaultFileName);
                }

                this.fileDialogService.Reset();
                this.fileDialogService.FileName = defaultFileName;
                this.fileDialogService.Filter = UIUtils.GetDialogFilterForExportImport(selectedMaterials.First().Entity.GetType());

                var result = this.fileDialogService.ShowSaveFileDialog();
                if (result == true)
                {
                    filePath = this.fileDialogService.FileName;
                }
            }
            else
            {
                this.folderBrowserService.Description = LocalizedResources.General_ExportFolderBrowserDescription;
                var result = folderBrowserService.ShowFolderBrowserDialog();
                if (result)
                {
                    filePath = folderBrowserService.SelectedPath;
                }
            }

            if (string.IsNullOrEmpty(filePath))
            {
                return;
            }

            foreach (var item in selectedMaterials)
            {
                var objectToExport = item.Entity;
                var pathToExport = filePath;

                /*
                 * If there are more objects to be exported, we must get 
                 * for each object the path where it will be exported.
                 * Also we must check for file name collisions before export.
                 */
                if (selectedItems.Count > 1)
                {
                    pathToExport = Path.Combine(pathToExport, UIUtils.GetExportObjectFileName(objectToExport));
                    pathToExport = PathUtils.FixFilePathForCollisions(pathToExport);
                }

                ExportManager.Instance.Export(
                    item.Entity,
                    this.PartDataContext,
                    pathToExport,
                    this.unitsService.Currencies,
                    this.unitsService.BaseCurrency);
            }

            this.WindowService.MessageDialogService.Show(
                selectedMaterials.Count > 1
                    ? LocalizedResources.ExportImport_ExportItemsWithSuccess
                    : LocalizedResources.ExportImport_ExportSuccess,
                MessageDialogType.Info);
        }

        /// <summary>
        /// Determines whether this instance can paste the specified parameter.
        /// </summary>
        /// <returns>
        /// true if the command can be executed, false otherwise.
        /// </returns>
        private bool CanPaste()
        {
            var clipboardObjTypes = ClipboardManager.Instance.PeekTypes.ToList();
            if (this.IsReadOnly
                || clipboardObjTypes.Count == 0
                || !clipboardObjTypes.All(type => type == typeof(RawMaterial) || type == typeof(Commodity) || type == typeof(RawPart)))
            {
                return false;
            }

            if (ClipboardManager.Instance.Operation == ClipboardOperation.Cut)
            {
                // An item can not be moved into a master data.
                if (this.ParentPart.IsMasterData)
                {
                    return false;
                }

                var clipboardObjects = ClipboardManager.Instance.PeekClipboardObjects;
                foreach (var clpObject in clipboardObjects)
                {
                    // An item can not be moved if: object to move are released or are master data, already exists in the destination collection
                    var releaseableObj = clpObject as IReleasable;
                    var identifObject = clpObject as IIdentifiable;
                    if ((releaseableObj != null && releaseableObj.IsReleased)
                        || identifObject == null
                        || this.ParentPart.RawMaterials.Any(m => m.Guid == identifObject.Guid)
                        || this.ParentPart.Commodities.Any(c => c.Guid == identifObject.Guid)
                        || EntityUtils.IsMasterData(clpObject))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Performs the logic associated with the Paste command.
        /// </summary>
        private void ExecutePaste()
        {
            var operation = ClipboardManager.Instance.Operation;
            var clipboardObjTypes = ClipboardManager.Instance.PeekTypes.ToList();
            if (clipboardObjTypes.Count == 0 || !clipboardObjTypes.All(type => type == typeof(RawMaterial) || type == typeof(Commodity) || type == typeof(RawPart)))
            {
                return;
            }

            var pastedObjects = new List<PastedData>();
            Action<PleaseWaitService.WorkParams> work = (workParams) =>
            {
                pastedObjects = ClipboardManager.Instance.Paste(this.ParentPart, this.PartDataContext).ToList();
            };

            Action<PleaseWaitService.WorkParams> workCompleted = (workparams) =>
            {
                if (workparams.Error != null)
                {
                    Log.Error("An error occurred while executing the paste operation.", workparams.Error);
                    this.windowService.MessageDialogService.Show(workparams.Error);
                }
                else
                {
                    // Display errors appeared during the paste process.
                    var errorMessage = ClipboardManager.Instance.GetErrorMessageToDisplay(pastedObjects);
                    if (!string.IsNullOrWhiteSpace(errorMessage))
                    {
                        this.windowService.MessageDialogService.Show(errorMessage, MessageDialogType.Error);
                    }

                    this.SelectedItems.Clear();
                    var messages = new Collection<EntityChangedMessage>();
                    foreach (var obj in pastedObjects.Where(o => o.Error == null))
                    {
                        // Add pasted object into materials list.
                        var pastedEntity = obj.Entity;
                        var item = new MaterialsItem(pastedEntity);
                        this.Materials.Add(item);
                        this.SelectedItems.Add(item);
                        var notification = EntityUtils.IsMasterData(pastedEntity)
                            ? Notification.MasterDataEntityChanged
                            : Notification.MyProjectsEntityChanged;

                        var message = new EntityChangedMessage(notification)
                        {
                            Entity = pastedEntity,
                            Parent = this.ParentPart,
                            SelectEntity = false,
                            ChangeType = operation == ClipboardOperation.Cut ? EntityChangeType.EntityMoved : EntityChangeType.EntityCreated
                        };

                        messages.Add(message);
                    }

                    if (messages.Count == 1)
                    {
                        this.messenger.Send(messages.FirstOrDefault());
                    }
                    else if (messages.Any())
                    {
                        var message = new EntitiesChangedMessage(messages);
                        this.messenger.Send(message);
                    }
                }
            };

            this.pleaseWaitService.Show(LocalizedResources.General_CopyingObjects, DisplayDelay, work, workCompleted);
        }

        #endregion Commands handling

        #region Event handlers

        /// <summary>
        /// Called when the <see cref="ParentPart"/> property has changed.
        /// </summary>
        private void OnParentPartChanged()
        {
            // Load the materials from the part
            this.LoadMaterials();

            // Create some reports related info based on the part
            this.AdditionalReportInformation = new ExtendedDataGridAdditionalReportInformation();
            if (this.ParentPart != null)
            {
                this.ParentIsPart = this.ParentPart.GetType() == typeof(Part);
                var parentProject = !IsInViewerMode ? this.PartDataContext.ProjectRepository.GetParentProject(this.ParentPart) : ZPKTool.Data.EntityUtils.GetParentProject(this.ParentPart);

                this.AdditionalReportInformation.ProjectName += parentProject != null ? " " + parentProject.Name : string.Empty;
                this.AdditionalReportInformation.ParentName += " " + this.ParentPart.Name;
                this.AdditionalReportInformation.Name += " " + LocalizedResources.General_Materials + " (" + this.ParentPart.Name + ")";
                this.AdditionalReportInformation.Version += " " + this.ParentPart.Version;
            }

            // Determine what object types can be imported based on the part.
            var typesToImport = new List<Type>()
                {
                    typeof(RawMaterial),
                    typeof(Commodity)
                };

            if (this.ParentIsPart)
            {
                typesToImport.Add(typeof(RawPart));
            }

            this.ImportCommandParameter = typesToImport;
        }

        /// <summary>
        /// Handle the SelectedItems collection changed event.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="NotifyCollectionChangedEventArgs"/> instance containing the event data.</param>
        private void SelectedItemsOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            this.LoadSelectedMaterialDetails();

            if (this.IsInViewerMode)
            {
                return;
            }

            /*
             * Synchronizes the SelectedEntitiesInfo collection, 
             * every time an item from materials datagrid is selected/deselected.
             */
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        foreach (var newItem in e.NewItems)
                        {
                            var materialItem = newItem as MaterialsItem;
                            if (materialItem != null && materialItem.Entity != null)
                            {
                                this.SelectedEntitiesInfo.Add(
                                    new EntityInfo(materialItem.Entity, this.PartDataContext.DatabaseId));
                            }
                        }

                        break;
                    }

                case NotifyCollectionChangedAction.Remove:
                    {
                        foreach (var olditem in e.OldItems)
                        {
                            var materialItem = olditem as MaterialsItem;
                            if (materialItem != null && materialItem.Entity != null)
                            {
                                var index = 0;
                                var founded = false;
                                while (!founded && index < this.SelectedEntitiesInfo.Count)
                                {
                                    if (this.SelectedEntitiesInfo[index].Entity == materialItem.Entity)
                                    {
                                        founded = true;
                                        this.SelectedEntitiesInfo.RemoveAt(index);
                                    }

                                    index++;
                                }
                            }
                        }

                        break;
                    }

                case NotifyCollectionChangedAction.Reset:
                    {
                        this.SelectedEntitiesInfo.Clear();
                        break;
                    }
            }
        }

        #endregion Event handlers

        #region Private methods

        /// <summary>
        /// Method called after the view is loaded
        /// </summary>
        private void LoadMaterials()
        {
            foreach (var material in this.ParentPart.RawMaterials.Where(m => !m.IsDeleted))
            {
                this.Materials.Add(new MaterialsItem(material));
            }

            foreach (var commodity in this.ParentPart.Commodities.Where(m => !m.IsDeleted))
            {
                this.Materials.Add(new MaterialsItem(commodity));
            }

            if (this.ParentPart.RawPart != null && !this.ParentPart.RawPart.IsDeleted)
            {
                this.Materials.Add(new MaterialsItem(this.ParentPart.RawPart));
            }
        }

        /// <summary>
        /// Loads the details screen of the selected material.
        /// </summary>
        private void LoadSelectedMaterialDetails()
        {
            if (this.SelectedItems == null || this.SelectedItems.Count != 1)
            {
                this.SelectedMaterialDetails = null;
                return;
            }

            var selectedEntity = this.SelectedItems.FirstOrDefault() as MaterialsItem;
            if (selectedEntity == null)
            {
                return;
            }

            var material = selectedEntity.Entity as RawMaterial;
            if (material != null)
            {
                var rawMaterialVm = this.container.GetExportedValue<RawMaterialViewModel>();
                rawMaterialVm.IsReadOnly = true;
                rawMaterialVm.DataSourceManager = this.PartDataContext;
                rawMaterialVm.IsInViewerMode = this.IsInViewerMode;
                rawMaterialVm.CostCalculationVersion = material.Part != null ? material.Part.CalculationVariant : null;
                rawMaterialVm.Model = material;

                this.SelectedMaterialDetails = rawMaterialVm;
            }
            else
            {
                var commodity = selectedEntity.Entity as Commodity;
                if (commodity != null)
                {
                    var commodityVm = this.container.GetExportedValue<CommodityViewModel>();
                    commodityVm.IsReadOnly = true;
                    commodityVm.DataSourceManager = this.PartDataContext;
                    commodityVm.IsInViewerMode = this.IsInViewerMode;
                    commodityVm.Model = commodity;

                    this.SelectedMaterialDetails = commodityVm;
                }
                else
                {
                    var rawPart = selectedEntity.Entity as RawPart;
                    if (rawPart != null)
                    {
                        var partVm = this.container.GetExportedValue<PartViewModel>();
                        partVm.IsReadOnly = true;
                        partVm.DataSourceManager = this.PartDataContext;
                        partVm.IsInViewerMode = this.IsInViewerMode;
                        partVm.Model = rawPart;

                        this.SelectedMaterialDetails = partVm;
                    }
                }
            }
        }

        #endregion Private methods
    }
}