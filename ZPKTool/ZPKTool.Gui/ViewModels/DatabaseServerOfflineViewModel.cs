﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Windows.Input;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Views;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the IMyView view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class DatabaseServerOfflineViewModel : ViewModel
    {
        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messanger;

        /// <summary>
        /// The online check service.
        /// </summary>
        private IOnlineCheckService onlineChecker;

        /// <summary>
        /// Indicates whether this instance is retrying the connection status.
        /// </summary>
        private bool isRetrying;

        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseServerOfflineViewModel"/> class.
        /// </summary>
        /// <param name="messenger">The messenger service.</param>
        /// <param name="onlineChecker">The online check service.</param>
        [ImportingConstructor]
        public DatabaseServerOfflineViewModel(IMessenger messenger, IOnlineCheckService onlineChecker)
        {
            this.messanger = messenger;
            this.onlineChecker = onlineChecker;

            this.RetryConnectionCommand = new DelegateCommand(RetryConnection);
            this.IsRetrying = false;
        }

        #region Commands

        /// <summary>
        /// Gets the retry connection command.
        /// </summary>
        public ICommand RetryConnectionCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether this instance is retrying.
        /// </summary>        
        public bool IsRetrying
        {
            get
            {
                return this.isRetrying;
            }

            set
            {
                if (this.isRetrying != value)
                {
                    this.isRetrying = value;
                    OnPropertyChanged("IsRetrying");
                }
            }
        }

        #endregion Properties

        #region Command Actions

        /// <summary>
        /// Gets or sets the retry connection.
        /// </summary>
        /// <value>The retry connection.</value>
        private void RetryConnection()
        {
            if (this.IsRetrying)
            {
                return;
            }

            this.IsRetrying = true;
            bool isOnline = false;

            // TODO: if the screen is unloaded/closed while performing this background check, the background operation is not stopped
            // and at its end the ReloadMainViewCurrentContent message is sent, which can cause problems if the main view content was changed during the operation.
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += delegate
            {
                isOnline = this.onlineChecker.CheckNow();
            };

            worker.RunWorkerCompleted += delegate
            {
                if (isOnline)
                {
                    this.messanger.Send(new NotificationMessage(Notifications.Notification.ReloadMainViewCurrentContent));
                }

                this.IsRetrying = false;
            };
            worker.RunWorkerAsync();
        }

        #endregion
    }
}