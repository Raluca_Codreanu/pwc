﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for Preferences.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class PreferencesViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The file dialog service.
        /// </summary>
        private readonly IFileDialogService fileDialogService;

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The selected UI language.
        /// </summary>
        private object selectedUILanguage;

        /// <summary>
        /// The selected background id.
        /// </summary>
        private int selectedBackgroundID;

        /// <summary>
        /// The selected material localization.
        /// </summary>
        private RawMaterialLocalisedName selectedMaterialLocalization;

        /// <summary>
        /// The selected chart view.
        /// </summary>
        private ChartViewMode selectedChartViewMode;

        /// <summary>
        /// The selected database location.
        /// </summary>
        private DbLocation selectedMasterDataBrowserDataSource;

        /// <summary>
        /// A value indicating whether to display version and timestamp on some screens.
        /// </summary>
        private bool displayVersionAndTimestamp;

        /// <summary>
        /// A value indicating whether to display the Capacity Utilization column in Process Step -> Machines.
        /// </summary>
        private bool displayCapacityUtilization;

        /// <summary>
        /// A value indicating whether the weight information should be displayed in the app header.
        /// </summary>
        private bool headerDisplaysWeight;

        /// <summary>
        /// A value indicating whether the investment information should be displayed in the app header.
        /// </summary>
        private bool headerDisplaysInvestment;

        /// <summary>
        /// A value indicating whether the purchase price information should be displayed in the app header.
        /// </summary>
        private bool headerDisplaysPurchasePrice;

        /// <summary>
        /// A value indicating whether the target price information should be displayed in the app header.
        /// </summary>
        private bool headerDisplaysTargetPrice;

        /// <summary>
        /// A value indicating whether the empty subassemblies and parts should be displayed in the tree item.
        /// </summary>
        private bool hideEmptySubassembliesAndParts;

        /// <summary>
        /// A value indicating whether to check for any available application update.
        /// </summary>
        private bool checkForUpdate;

        /// <summary>
        /// The selected interval for checking for update.
        /// </summary>
        private int selectedCheckForUpdateInterval;

        /// <summary>
        /// The location where to check for an update.
        /// </summary>
        private string updateLocation;

        /// <summary>
        /// A value indicating whether the update location can be changed.
        /// </summary>
        private bool canChangeUpdateLocation;

        /// <summary>
        /// The address of the local database server.
        /// </summary>
        private string localDbServer;

        /// <summary>
        /// The user name for accessing the local database server.
        /// </summary>
        private string localDbUser;

        /// <summary>
        /// The password for accessing the local database server.
        /// </summary>
        private string localDbPassword;

        /// <summary>
        /// A value indicating whether to use windows authentication for connection to the local database server.
        /// </summary>
        private bool localDbUseWindowsAuth;

        /// <summary>
        /// A value indicating whether to use windows authentication for local database synchronization.
        /// </summary>
        private bool localDbUseWindowsAuthSync;

        /// <summary>
        /// The address of the central database server.
        /// </summary>
        private string centralDbServer;

        /// <summary>
        /// The user name for accessing the central database server.
        /// </summary>
        private string centralDbUser;

        /// <summary>
        /// The password for accessing the central database server.
        /// </summary>
        private string centralDbPassword;

        /// <summary>
        /// A value indicating whether to use windows authentication for connection to the central database server.
        /// </summary>
        private bool centralDbUseWindowsAuth;

        /// <summary>
        /// A value indicating whether to use windows authentication for central database synchronization.
        /// </summary>
        private bool centralDbUseWindowsAuthSync;

        /// <summary>
        /// A value indicating whether to use SSL support.
        /// </summary>
        private bool centralDbUseSSLSupport;

        /// <summary>
        /// The selected number of decimal places.
        /// </summary>
        private int selectedDecimalPlacesNumber;

        /// <summary>
        /// A value indicating whether to show the Restart button.
        /// </summary>
        private bool showRestart;

        /// <summary>
        /// A value indicating whether to show the Calculation result in header.
        /// </summary>
        private bool headerDisplaysCalculationResult;

        /// <summary>
        /// A value indicating whether to show the Start Screen.
        /// </summary>
        private bool headerShowStartScreen;

        /// <summary>
        /// A value indicating whether to show the Import Confirmation Screen.
        /// </summary>
        private bool headerDontShowImportConfirmation;

        /// <summary>
        /// A value indicating whether to show the percentages on the waterfall chart columns.
        /// </summary>
        private bool showPercentagesInWaterfallChart;

        /// <summary>
        /// A value indicating whether to show the warnings in result details or not.
        /// </summary>
        private bool showWarningsInResultDetails;

        /// <summary>
        /// The application logo image.
        /// </summary>
        private ImageSource logoImage;

        /// <summary>
        /// The full path to the file that contains the custom logo image.
        /// </summary>
        private string customLogoPath;

        /// <summary>
        /// The absolute path to the custom logo, that should be get by combining the application data folder and the custom logo path.
        /// </summary>
        private string customLogoFullPath;

        /// <summary>
        /// A value indicating whether the window with the route map will be displayed 
        /// after calculating the distance between source and destination in Transport Cost Calculator or not.
        /// </summary>
        private bool showMapWindow;

        /// <summary>
        /// The next check datetime for auto-update.
        /// </summary>
        private DateTime nextUpdateCheckDate;

        #endregion Attributes

        /// <summary>
        /// Initializes a new instance of the <see cref="PreferencesViewModel"/> class.
        /// </summary>
        /// <param name="messenger">The messenger.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="fileDialogService">The file dialog service.</param>
        [ImportingConstructor]
        public PreferencesViewModel(IMessenger messenger, IWindowService windowService, IFileDialogService fileDialogService)
        {
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("fileDialogService", fileDialogService);

            this.messenger = messenger;
            this.windowService = windowService;
            this.fileDialogService = fileDialogService;

            this.RestartApplicationCommand = new DelegateCommand(this.RestartApplication);
            this.ResetMainViewLayoutCommand = new DelegateCommand(this.ResetMainViewLayout);
            this.BrowseForLogoCommand = new DelegateCommand(this.BrowseForCustomLogo);
            this.ClearLogoCommand = new DelegateCommand(() => this.CustomLogoPath = string.Empty);
            this.PropertyChanged += OnPropertyChanged;

            this.InitializeProperties();
            this.EvaluateRestartVisibility();
            this.InitializeUndoManager();

            this.IsChanged = false;
        }

        #region Commands

        /// <summary>
        /// Gets the restart command.
        /// </summary>
        public ICommand RestartApplicationCommand { get; private set; }

        /// <summary>
        /// Gets the reset main view layout command.
        /// </summary>
        public ICommand ResetMainViewLayoutCommand { get; private set; }

        /// <summary>
        /// Gets the browse for logo command.
        /// </summary>
        public ICommand BrowseForLogoCommand { get; private set; }

        /// <summary>
        /// Gets the clear logo command.
        /// </summary>
        public ICommand ClearLogoCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets the available languages for the UI.
        /// </summary>
        public ObservableCollection<object> UILanguages { get; private set; }

        /// <summary>
        /// Gets the available background colors for the UI.
        /// </summary>
        public ObservableCollection<object> BackgroundColors { get; private set; }

        /// <summary>
        /// Gets or sets the selected UI language.
        /// </summary>
        [UndoableProperty]
        public object SelectedUILanguage
        {
            get
            {
                return this.selectedUILanguage;
            }

            set
            {
                if (this.selectedUILanguage != value)
                {
                    OnPropertyChanging(() => this.SelectedUILanguage);
                    this.selectedUILanguage = value;
                    this.IsChanged = true;
                    OnPropertyChanged(() => this.SelectedUILanguage);
                    EvaluateRestartVisibility();
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected background ID.
        /// </summary>
        [UndoableProperty]
        public int SelectedBackgroundID
        {
            get
            {
                return this.selectedBackgroundID;
            }

            set
            {
                if (this.selectedBackgroundID != value)
                {
                    OnPropertyChanging(() => this.SelectedBackgroundID);
                    this.selectedBackgroundID = value;
                    this.IsChanged = true;
                    OnPropertyChanged(() => this.SelectedBackgroundID);
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected material localization.
        /// </summary>
        [UndoableProperty]
        public RawMaterialLocalisedName SelectedMaterialLocalization
        {
            get
            {
                return this.selectedMaterialLocalization;
            }

            set
            {
                if (this.selectedMaterialLocalization != value)
                {
                    OnPropertyChanging(() => this.SelectedMaterialLocalization);
                    this.selectedMaterialLocalization = value;
                    this.IsChanged = true;
                    OnPropertyChanged(() => this.SelectedMaterialLocalization);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to display version and timestamp on some screens.
        /// </summary>
        [UndoableProperty]
        public bool DisplayVersionAndTimestamp
        {
            get
            {
                return this.displayVersionAndTimestamp;
            }

            set
            {
                if (this.displayVersionAndTimestamp != value)
                {
                    OnPropertyChanging(() => this.DisplayVersionAndTimestamp);
                    this.displayVersionAndTimestamp = value;
                    this.IsChanged = true;
                    OnPropertyChanged(() => this.DisplayVersionAndTimestamp);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to display the Capacity Utilization column in Process Step -> Machines.
        /// </summary>
        [UndoableProperty]
        public bool DisplayCapacityUtilization
        {
            get
            {
                return this.displayCapacityUtilization;
            }

            set
            {
                if (this.displayCapacityUtilization != value)
                {
                    this.OnPropertyChanging(() => this.DisplayCapacityUtilization);
                    this.displayCapacityUtilization = value;
                    this.IsChanged = true;
                    this.OnPropertyChanged(() => this.DisplayCapacityUtilization);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the weight information should be displayed in the app header.
        /// </summary>
        [UndoableProperty]
        public bool HeaderDisplaysWeight
        {
            get
            {
                return this.headerDisplaysWeight;
            }

            set
            {
                if (this.headerDisplaysWeight != value)
                {
                    OnPropertyChanging(() => this.HeaderDisplaysWeight);
                    this.headerDisplaysWeight = value;
                    this.IsChanged = true;
                    OnPropertyChanged(() => this.HeaderDisplaysWeight);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the investment information should be displayed in the app header.
        /// </summary>
        [UndoableProperty]
        public bool HeaderDisplaysInvestment
        {
            get
            {
                return this.headerDisplaysInvestment;
            }

            set
            {
                if (this.headerDisplaysInvestment != value)
                {
                    OnPropertyChanging(() => this.HeaderDisplaysInvestment);
                    this.headerDisplaysInvestment = value;
                    this.IsChanged = true;
                    OnPropertyChanged(() => this.HeaderDisplaysInvestment);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the purchase price information should be displayed in the app header.
        /// </summary>
        [UndoableProperty]
        public bool HeaderDisplaysPurchasePrice
        {
            get
            {
                return this.headerDisplaysPurchasePrice;
            }

            set
            {
                this.SetProperty(ref this.headerDisplaysPurchasePrice, value, () => this.HeaderDisplaysPurchasePrice);
                this.IsChanged = true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the target price information should be displayed in the app header.
        /// </summary>
        [UndoableProperty]
        public bool HeaderDisplaysTargetPrice
        {
            get
            {
                return this.headerDisplaysTargetPrice;
            }

            set
            {
                this.SetProperty(ref this.headerDisplaysTargetPrice, value, () => this.HeaderDisplaysTargetPrice);
                this.IsChanged = true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [header show start screen].
        /// </summary>
        [UndoableProperty]
        public bool HeaderShowStartScreen
        {
            get
            {
                return this.headerShowStartScreen;
            }

            set
            {
                this.SetProperty(ref this.headerShowStartScreen, value, () => this.HeaderShowStartScreen);
                this.IsChanged = true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [header show import confirmation screen].
        /// </summary>
        [UndoableProperty]
        public bool HeaderDontShowImportConfirmation
        {
            get
            {
                return this.headerDontShowImportConfirmation;
            }

            set
            {
                this.SetProperty(ref this.headerDontShowImportConfirmation, value, () => this.HeaderDontShowImportConfirmation);
                this.IsChanged = true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [header displays calculation result].
        /// </summary>
        [UndoableProperty]
        public bool HeaderDisplaysCalculationResult
        {
            get
            {
                return this.headerDisplaysCalculationResult;
            }

            set
            {
                this.SetProperty(ref this.headerDisplaysCalculationResult, value, () => this.HeaderDisplaysCalculationResult);
                this.IsChanged = true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to check for any available application update.
        /// </summary>
        [UndoableProperty]
        public bool CheckForUpdate
        {
            get
            {
                return this.checkForUpdate;
            }

            set
            {
                if (this.checkForUpdate != value)
                {
                    OnPropertyChanging(() => this.CheckForUpdate);
                    this.checkForUpdate = value;
                    this.IsChanged = true;
                    OnPropertyChanged(() => this.CheckForUpdate);
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected chart view.
        /// </summary>
        [UndoableProperty]
        public ChartViewMode SelectedChartViewMode
        {
            get
            {
                return this.selectedChartViewMode;
            }

            set
            {
                this.SetProperty(ref this.selectedChartViewMode, value, () => this.SelectedChartViewMode);
                this.IsChanged = true;
            }
        }

        /// <summary>
        /// Gets or sets the selected database location to load master data.
        /// </summary>
        [UndoableProperty]
        public DbLocation SelectedMasterDataBrowserDataSource
        {
            get
            {
                return this.selectedMasterDataBrowserDataSource;
            }

            set
            {
                this.SetProperty(ref this.selectedMasterDataBrowserDataSource, value, () => this.SelectedMasterDataBrowserDataSource);
                this.IsChanged = true;
            }
        }

        /// <summary>
        /// Gets the intervals at which to check for update, available for selection.
        /// </summary>
        public ObservableCollection<object> CheckForUpdateIntervals { get; private set; }

        /// <summary>
        /// Gets or sets the selected interval for checking for update.
        /// </summary>
        [UndoableProperty]
        public int SelectedCheckForUpdateInterval
        {
            get
            {
                return this.selectedCheckForUpdateInterval;
            }

            set
            {
                if (this.selectedCheckForUpdateInterval != value)
                {
                    OnPropertyChanging(() => this.SelectedCheckForUpdateInterval);
                    this.selectedCheckForUpdateInterval = value;
                    var nextCheckPreview = UserSettingsManager.Instance.AutomaticUpdateLastCheck.AddDays(this.selectedCheckForUpdateInterval);
                    this.NextUpdateCheckDate = nextCheckPreview > DateTime.Now ? nextCheckPreview : DateTime.Now;
                    this.IsChanged = true;
                    OnPropertyChanged(() => this.SelectedCheckForUpdateInterval);
                }
            }
        }

        /// <summary>
        /// Gets or sets the location where to check for an update.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public string UpdateLocation
        {
            get
            {
                return this.updateLocation;
            }

            set
            {
                if (this.updateLocation != value)
                {
                    OnPropertyChanging(() => this.UpdateLocation);
                    this.updateLocation = value;
                    this.IsChanged = true;
                    OnPropertyChanged(() => this.UpdateLocation);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the update location can be changed.
        /// </summary>
        public bool CanChangeUpdateLocation
        {
            get
            {
                return this.canChangeUpdateLocation;
            }

            set
            {
                if (this.canChangeUpdateLocation != value)
                {
                    this.canChangeUpdateLocation = value;
                    OnPropertyChanged(() => this.CanChangeUpdateLocation);
                }
            }
        }

        /// <summary>
        /// Gets or sets the address of the local database server.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public string LocalDbServer
        {
            get
            {
                return this.localDbServer;
            }

            set
            {
                if (this.localDbServer != value)
                {
                    OnPropertyChanging(() => this.LocalDbServer);
                    this.localDbServer = value;
                    this.IsChanged = true;
                    OnPropertyChanged(() => this.LocalDbServer);
                    EvaluateRestartVisibility();
                }
            }
        }

        /// <summary>
        /// Gets or sets the user name for accessing the local database server.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public string LocalDbUser
        {
            get
            {
                return this.localDbUser;
            }

            set
            {
                if (this.localDbUser != value)
                {
                    OnPropertyChanging(() => this.LocalDbUser);
                    this.localDbUser = value;
                    this.IsChanged = true;
                    OnPropertyChanged(() => this.LocalDbUser);
                    EvaluateRestartVisibility();
                }
            }
        }

        /// <summary>
        /// Gets or sets the password for accessing the local database server.
        /// </summary>
        [Required(ErrorMessageResourceName = "General_RequiredField", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public string LocalDbPassword
        {
            get
            {
                return this.localDbPassword;
            }

            set
            {
                if (this.localDbPassword != value)
                {
                    OnPropertyChanging(() => this.LocalDbPassword);
                    this.localDbPassword = value;
                    this.IsChanged = true;
                    OnPropertyChanged(() => this.LocalDbPassword);
                    EvaluateRestartVisibility();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to use windows authentication for connection to the local database server.
        /// </summary>
        [UndoableProperty]
        public bool LocalDbUseWindowsAuth
        {
            get
            {
                return this.localDbUseWindowsAuth;
            }

            set
            {
                if (this.localDbUseWindowsAuth != value)
                {
                    OnPropertyChanging(() => this.LocalDbUseWindowsAuth);
                    this.localDbUseWindowsAuth = value;
                    this.IsChanged = true;
                    OnPropertyChanged(() => this.LocalDbUseWindowsAuth);
                    EvaluateRestartVisibility();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to use windows authentification for local database synchronization.
        /// </summary>
        [UndoableProperty]
        public bool LocalDbUseWindowsAuthSync
        {
            get
            {
                return this.localDbUseWindowsAuthSync;
            }

            set
            {
                if (this.localDbUseWindowsAuthSync != value)
                {
                    OnPropertyChanging(() => this.LocalDbUseWindowsAuthSync);
                    this.localDbUseWindowsAuthSync = value;
                    this.IsChanged = true;
                    OnPropertyChanged(() => this.LocalDbUseWindowsAuthSync);
                    EvaluateRestartVisibility();
                }
            }
        }

        /// <summary>
        /// Gets or sets the address of the central database server.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public string CentralDbServer
        {
            get
            {
                return this.centralDbServer;
            }

            set
            {
                if (this.centralDbServer != value)
                {
                    OnPropertyChanging(() => this.CentralDbServer);
                    this.centralDbServer = value;
                    this.IsChanged = true;
                    OnPropertyChanged(() => this.CentralDbServer);
                    EvaluateRestartVisibility();
                }
            }
        }

        /// <summary>
        /// Gets or sets the user name for accessing the central database server.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public string CentralDbUser
        {
            get
            {
                return this.centralDbUser;
            }

            set
            {
                if (this.centralDbUser != value)
                {
                    OnPropertyChanging(() => this.CentralDbUser);
                    this.centralDbUser = value;
                    this.IsChanged = true;
                    OnPropertyChanged(() => this.CentralDbUser);
                    EvaluateRestartVisibility();
                }
            }
        }

        /// <summary>
        /// Gets or sets the password for accessing the central database server.
        /// </summary>
        [Required(ErrorMessageResourceName = "General_RequiredField", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public string CentralDbPassword
        {
            get
            {
                return this.centralDbPassword;
            }

            set
            {
                if (this.centralDbPassword != value)
                {
                    OnPropertyChanging(() => this.CentralDbPassword);
                    this.centralDbPassword = value;
                    this.IsChanged = true;
                    OnPropertyChanged(() => this.CentralDbPassword);
                    EvaluateRestartVisibility();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to use windows authentication for connection to the central database server.
        /// </summary>
        [UndoableProperty]
        public bool CentralDbUseWindowsAuth
        {
            get
            {
                return this.centralDbUseWindowsAuth;
            }

            set
            {
                if (this.centralDbUseWindowsAuth != value)
                {
                    OnPropertyChanging(() => this.CentralDbUseWindowsAuth);
                    this.centralDbUseWindowsAuth = value;
                    this.IsChanged = true;
                    OnPropertyChanged(() => this.CentralDbUseWindowsAuth);
                    EvaluateRestartVisibility();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to use windows authentification for central database synchronization.
        /// </summary>
        [UndoableProperty]
        public bool CentralDbUseWindowsAuthSync
        {
            get
            {
                return this.centralDbUseWindowsAuthSync;
            }

            set
            {
                if (this.centralDbUseWindowsAuthSync != value)
                {
                    OnPropertyChanging(() => this.CentralDbUseWindowsAuthSync);
                    this.centralDbUseWindowsAuthSync = value;
                    this.IsChanged = true;
                    OnPropertyChanged(() => this.CentralDbUseWindowsAuthSync);
                    EvaluateRestartVisibility();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to use SSL support for connection to the central database server.
        /// </summary>
        [UndoableProperty]
        public bool CentralDbUseSSLSupport
        {
            get
            {
                return this.centralDbUseSSLSupport;
            }

            set
            {
                if (this.centralDbUseSSLSupport != value)
                {
                    OnPropertyChanging(() => this.CentralDbUseSSLSupport);
                    this.centralDbUseSSLSupport = value;
                    this.IsChanged = true;
                    OnPropertyChanged(() => this.CentralDbUseSSLSupport);
                    EvaluateRestartVisibility();
                }
            }
        }

        /// <summary>
        /// Gets the list of options for the number decimal places.
        /// </summary>
        public ObservableCollection<object> DecimalPlacesOptions { get; private set; }

        /// <summary>
        /// Gets or sets the selected number of decimal places.
        /// </summary>
        [UndoableProperty]
        public int SelectedDecimalPlacesNumber
        {
            get
            {
                return this.selectedDecimalPlacesNumber;
            }

            set
            {
                if (this.selectedDecimalPlacesNumber != value)
                {
                    OnPropertyChanging(() => this.SelectedDecimalPlacesNumber);
                    this.selectedDecimalPlacesNumber = value;
                    this.IsChanged = true;
                    OnPropertyChanged(() => this.SelectedDecimalPlacesNumber);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to show the Restart button.
        /// </summary>
        public bool ShowRestart
        {
            get
            {
                return this.showRestart;
            }

            set
            {
                if (this.showRestart != value)
                {
                    this.showRestart = value;
                    OnPropertyChanged(() => this.ShowRestart);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the empty subassemblies and empty parts
        /// should be displayed in the projects tree.
        /// </summary>
        [UndoableProperty]
        public bool HideEmptySubassembliesAndParts
        {
            get
            {
                return this.hideEmptySubassembliesAndParts;
            }

            set
            {
                this.SetProperty(ref this.hideEmptySubassembliesAndParts, value, () => this.HideEmptySubassembliesAndParts);
                this.IsChanged = true;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the global settings can be edited.
        /// </summary>
        public VMProperty<bool> CanEditGlobalSettings { get; private set; }

        /// <summary>
        /// Gets or sets the selected units system.
        /// </summary>
        [UndoableProperty]
        public VMProperty<UnitsSystem> SelectedUnitsSystem { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to show the percentages on the waterfall chart columns.
        /// </summary>
        [UndoableProperty]
        public bool ShowPercentagesInWaterfallChart
        {
            get
            {
                return this.showPercentagesInWaterfallChart;
            }

            set
            {
                this.SetProperty(ref this.showPercentagesInWaterfallChart, value, () => this.ShowPercentagesInWaterfallChart);
                this.IsChanged = true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to show the warnings in result details or not.
        /// </summary>
        [UndoableProperty]
        public bool ShowWarningsInResultDetails
        {
            get
            {
                return this.showWarningsInResultDetails;
            }

            set
            {
                this.SetProperty(ref this.showWarningsInResultDetails, value, () => this.ShowWarningsInResultDetails);
                this.IsChanged = true;
            }
        }

        /// <summary>
        /// Gets or sets the application logo image.
        /// </summary>
        public ImageSource LogoImage
        {
            get { return this.logoImage; }
            set { this.SetProperty(ref this.logoImage, value, () => this.LogoImage); }
        }

        /// <summary>
        /// Gets or sets the full path to the file that contains the custom logo image.
        /// </summary>
        [UndoableProperty]
        public string CustomLogoPath
        {
            get
            {
                return this.customLogoPath;
            }

            set
            {
                if (this.customLogoPath != value)
                {
                    OnPropertyChanging(() => this.CustomLogoPath);
                    this.customLogoPath = value;
                    OnPropertyChanged(() => this.CustomLogoPath);
                    if (!string.IsNullOrWhiteSpace(this.customLogoPath))
                    {
                        try
                        {
                            // If there is a path to custom logo, create the logo image from that file.
                            var customLogo = new BitmapImage();
                            customLogo.BeginInit();
                            customLogo.CacheOption = BitmapCacheOption.OnLoad;
                            customLogo.CreateOptions = BitmapCreateOptions.IgnoreImageCache;
                            customLogo.UriSource = new Uri(this.customLogoPath);
                            customLogo.EndInit();

                            this.LogoImage = customLogo;
                        }
                        catch
                        {
                            this.LogoImage = Images.DefaultApplicationLogo;
                        }
                    }
                    else
                    {
                        // If no path to custom logo exists, the logo image is read from the default application logo resource.
                        this.LogoImage = Images.DefaultApplicationLogo;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the window with route map will be displayed 
        /// after calculating the distance between source and destination in Transport Cost Calculator or not.
        /// </summary>
        [UndoableProperty]
        public bool ShowMapWindow
        {
            get
            {
                return this.showMapWindow;
            }

            set
            {
                this.SetProperty(ref this.showMapWindow, value, () => this.ShowMapWindow);
                this.IsChanged = true;
            }
        }

        /// <summary>
        /// Gets or sets the next check datetime for auto-update.
        /// </summary>
        public DateTime NextUpdateCheckDate
        {
            get
            {
                return this.nextUpdateCheckDate;
            }

            set
            {
                this.SetProperty(ref this.nextUpdateCheckDate, value, () => this.NextUpdateCheckDate);
            }
        }

        #endregion Properties

        /// <summary>
        /// Initializes the properties of this instance.
        /// </summary>
        private void InitializeProperties()
        {
            // Initialize the General UI Settings
            this.UILanguages = new ObservableCollection<object>()
            {
                new { Name = LocalizedResources.UILanguage_English, Code = "en-US" },
                new { Name = LocalizedResources.UILanguage_German, Code = "de-DE" }
            };
            this.SelectedUILanguage = UserSettingsManager.Instance.UILanguage;
            this.SelectedUnitsSystem.Value = (UnitsSystem)UserSettingsManager.Instance.UnitsSystem;
            this.SelectedMaterialLocalization = (RawMaterialLocalisedName)UserSettingsManager.Instance.SearchRawMaterialLocalisedName;
            this.SelectedChartViewMode = (ChartViewMode)UserSettingsManager.Instance.DefaultChartViewMode;
            this.SelectedMasterDataBrowserDataSource = (DbLocation)UserSettingsManager.Instance.MasterDataBrowserDataSource;

            this.BackgroundColors = new ObservableCollection<object>();
            foreach (var background in Images.Backgrounds)
            {
                this.BackgroundColors.Add(new { ColorID = background.Item1, Name = background.Item2 });
            }

            if (Images.Backgrounds.Any(b => b.Item1 == UserSettingsManager.Instance.BackgroundColorID))
            {
                this.SelectedBackgroundID = UserSettingsManager.Instance.BackgroundColorID;
            }

            this.DisplayVersionAndTimestamp = UserSettingsManager.Instance.DisplayVersionAndTimestamp;
            this.DisplayCapacityUtilization = UserSettingsManager.Instance.DisplayCapacityUtilization;
            this.HideEmptySubassembliesAndParts = UserSettingsManager.Instance.HideEmptySubassembliesAndParts;

            this.DecimalPlacesOptions = new ObservableCollection<object>()
            {
                new { Name = "1", Value = 1 },
                new { Name = "2", Value = 2 },
                new { Name = "3", Value = 3 },
                new { Name = "4", Value = 4 },
                new { Name = "5", Value = 5 },
                new { Name = "6", Value = 6 },
                new { Name = "7", Value = 7 },
                new { Name = "8", Value = 8 }
            };
            this.SelectedDecimalPlacesNumber = UserSettingsManager.Instance.DecimalPlacesNumber;

            // Initialize the Header UI Settings
            this.HeaderDisplaysWeight = UserSettingsManager.Instance.HeaderDisplayWeight;
            this.HeaderDisplaysInvestment = UserSettingsManager.Instance.HeaderDisplayInvestment;
            this.HeaderDisplaysCalculationResult = UserSettingsManager.Instance.HeaderDisplayCalculationResult;
            this.HeaderDisplaysPurchasePrice = UserSettingsManager.Instance.HeaderDisplaysPurchasePrice;
            this.HeaderDisplaysTargetPrice = UserSettingsManager.Instance.HeaderDisplaysTargetPrice;
            this.HeaderShowStartScreen = UserSettingsManager.Instance.ShowStartScreen;
            this.HeaderDontShowImportConfirmation = UserSettingsManager.Instance.DontShowImportConfirmation;
            this.ShowPercentagesInWaterfallChart = UserSettingsManager.Instance.ShowPercentagesInWaterfallChart;
            this.ShowWarningsInResultDetails = UserSettingsManager.Instance.ShowWarningsInResultDetails;
            this.ShowMapWindow = UserSettingsManager.Instance.ShowMapWindow;

            // Initialize the Update UI Settings
            this.CheckForUpdateIntervals = new ObservableCollection<object>()
            {
                new { Value = 1, Display = LocalizedResources.General_PeriodDay },
                new { Value = 2, Display = LocalizedResources.General_Period2Days },
                new { Value = 4, Display = LocalizedResources.General_Period4Days },
                new { Value = 7, Display = LocalizedResources.General_PeriodWeek },
                new { Value = 14, Display = LocalizedResources.General_Period2Weeks },
                new { Value = 21, Display = LocalizedResources.General_Period3Weeks },
                new { Value = 28, Display = LocalizedResources.General_PeriodMonth }
            };

            this.CheckForUpdate = UserSettingsManager.Instance.AutomaticUpdatesOn;
            this.SelectedCheckForUpdateInterval = UserSettingsManager.Instance.AutomaticUpdateCheckPeriod;
            this.NextUpdateCheckDate = UserSettingsManager.Instance.AutomaticUpdateLastCheck.AddDays(UserSettingsManager.Instance.AutomaticUpdateCheckPeriod);

            // Initialize the Global Settings
            this.CanEditGlobalSettings.Value = GlobalSettingsManager.Instance.CanWriteSettingsFile();

            this.UpdateLocation = GlobalSettingsManager.Instance.AutomaticUpdatePath;
            if (SecurityManager.Instance.CurrentUser != null
                && SecurityManager.Instance.CurrentUserHasRole(Role.Admin)
                && this.CanEditGlobalSettings.Value)
            {
                this.CanChangeUpdateLocation = true;
            }

            this.LocalDbServer = GlobalSettingsManager.Instance.LocalConnectionDataSource;
            this.LocalDbUseWindowsAuth = GlobalSettingsManager.Instance.UseWindowsAuthForLocalDb;
            this.LocalDbUseWindowsAuthSync = GlobalSettingsManager.Instance.UseWindowsAuthForLocalDbSync;
            this.CentralDbUseWindowsAuthSync = GlobalSettingsManager.Instance.UseWindowsAuthForCentralDbSync;
            this.LocalDbUser = GlobalSettingsManager.Instance.LocalConnectionUser;
            this.LocalDbPassword =
                EncryptionManager.Instance.DecryptBlowfish(GlobalSettingsManager.Instance.LocalConnectionPasswordEncrypted, Constants.PasswordEncryptionKey);

            this.CentralDbServer = GlobalSettingsManager.Instance.CentralConnectionDataSource;
            this.CentralDbUseWindowsAuth = GlobalSettingsManager.Instance.UseWindowsAuthForCentralDb;
            this.CentralDbUseSSLSupport = GlobalSettingsManager.Instance.UseSSLSupportForCentralDb;
            this.CentralDbUser = GlobalSettingsManager.Instance.CentralConnectionUser;
            this.CentralDbPassword =
                EncryptionManager.Instance.DecryptBlowfish(GlobalSettingsManager.Instance.CentralConnectionPasswordEncrypted, Constants.PasswordEncryptionKey);

            if (!string.IsNullOrWhiteSpace(UserSettingsManager.Instance.CustomLogoPath))
            {
                this.customLogoFullPath = Path.Combine(Constants.ApplicationDataFolderPath, UserSettingsManager.Instance.CustomLogoPath);
                if (File.Exists(this.customLogoFullPath))
                {
                    // If a custom application logo exists, get the custom logo full path.
                    this.CustomLogoPath = this.customLogoFullPath;
                }
                else
                {
                    // If the file for the custom logo image does not exist, the custom logo path from user settings manager must be set to empty.
                    UserSettingsManager.Instance.CustomLogoPath = string.Empty;
                    this.customLogoFullPath = string.Empty;
                    this.CustomLogoPath = string.Empty;
                }
            }
            else
            {
                this.CustomLogoPath = string.Empty;
            }
        }

        /// <summary>
        /// Initialize the Undo manager.
        /// </summary>
        private void InitializeUndoManager()
        {
            this.UndoManager.Start();
        }

        /// <summary>
        /// Handles the PropertyChanged event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (this.IsLoaded && e.PropertyName == ReflectionUtils.GetPropertyName(() => this.SelectedBackgroundID))
            {
                Images.SetBackground(this.SelectedBackgroundID);
            }
        }

        /// <summary>
        /// Saves the preferences changes.
        /// </summary>
        protected override void Save()
        {
            string error = this.ValidateInput();
            if (error != null)
            {
                this.windowService.MessageDialogService.Show(error, MessageDialogType.Error);
                return;
            }

            if (!string.IsNullOrWhiteSpace(this.CustomLogoPath) && !string.Equals(this.CustomLogoPath, this.customLogoFullPath))
            {
                // If there is a custom logo selected, delete the previous logo file.
                if (!string.IsNullOrWhiteSpace(this.customLogoFullPath))
                {
                    File.Delete(this.customLogoFullPath);
                }

                // The new logo file is copied into the application data folder.
                File.Copy(this.CustomLogoPath, Path.Combine(Constants.ApplicationDataFolderPath, "CustomLogo") + Path.GetExtension(this.CustomLogoPath), true);
                UserSettingsManager.Instance.CustomLogoPath = "CustomLogo" + Path.GetExtension(this.CustomLogoPath);
            }
            else if (string.IsNullOrWhiteSpace(this.CustomLogoPath) && !string.IsNullOrWhiteSpace(this.customLogoFullPath))
            {
                // If the application logo is reset to the default one, the old custom logo is deleted.
                File.Delete(this.customLogoFullPath);
                UserSettingsManager.Instance.CustomLogoPath = string.Empty;
            }

            // Save the User Settings
            string newUILanguage = this.SelectedUILanguage as string;
            if (!string.IsNullOrWhiteSpace(newUILanguage))
            {
                UserSettingsManager.Instance.UILanguage = newUILanguage;
            }

            UserSettingsManager.Instance.SearchRawMaterialLocalisedName = (int)this.SelectedMaterialLocalization;
            UserSettingsManager.Instance.DisplayVersionAndTimestamp = this.DisplayVersionAndTimestamp;
            UserSettingsManager.Instance.DisplayCapacityUtilization = this.DisplayCapacityUtilization;
            UserSettingsManager.Instance.HeaderDisplayWeight = this.HeaderDisplaysWeight;
            UserSettingsManager.Instance.HeaderDisplayInvestment = this.HeaderDisplaysInvestment;
            UserSettingsManager.Instance.HeaderDisplaysPurchasePrice = this.HeaderDisplaysPurchasePrice;
            UserSettingsManager.Instance.HeaderDisplaysTargetPrice = this.HeaderDisplaysTargetPrice;
            UserSettingsManager.Instance.ShowStartScreen = this.HeaderShowStartScreen;
            UserSettingsManager.Instance.HeaderDisplayCalculationResult = this.HeaderDisplaysCalculationResult;
            UserSettingsManager.Instance.DecimalPlacesNumber = this.SelectedDecimalPlacesNumber;
            UserSettingsManager.Instance.AutomaticUpdatesOn = this.CheckForUpdate;
            UserSettingsManager.Instance.AutomaticUpdateCheckPeriod = this.SelectedCheckForUpdateInterval;
            UserSettingsManager.Instance.HideEmptySubassembliesAndParts = this.HideEmptySubassembliesAndParts;
            UserSettingsManager.Instance.DontShowImportConfirmation = this.HeaderDontShowImportConfirmation;
            UserSettingsManager.Instance.DefaultChartViewMode = (int)this.SelectedChartViewMode;
            UserSettingsManager.Instance.MasterDataBrowserDataSource = (int)this.SelectedMasterDataBrowserDataSource;
            UserSettingsManager.Instance.BackgroundColorID = this.SelectedBackgroundID;
            UserSettingsManager.Instance.UnitsSystem = this.SelectedUnitsSystem.Value;
            UserSettingsManager.Instance.ShowPercentagesInWaterfallChart = this.ShowPercentagesInWaterfallChart;
            UserSettingsManager.Instance.ShowWarningsInResultDetails = this.ShowWarningsInResultDetails;
            UserSettingsManager.Instance.ShowMapWindow = this.ShowMapWindow;

            UserSettingsManager.Instance.Save();

            // Save the Global Settings
            GlobalSettingsManager.Instance.LocalConnectionDataSource = this.LocalDbServer;
            GlobalSettingsManager.Instance.UseWindowsAuthForLocalDb = this.LocalDbUseWindowsAuth;
            GlobalSettingsManager.Instance.UseWindowsAuthForLocalDbSync = this.LocalDbUseWindowsAuthSync;
            GlobalSettingsManager.Instance.LocalConnectionUser = this.LocalDbUser;
            GlobalSettingsManager.Instance.LocalConnectionPasswordEncrypted =
                EncryptionManager.Instance.EncryptBlowfish(this.LocalDbPassword.Trim(), Constants.PasswordEncryptionKey);

            GlobalSettingsManager.Instance.CentralConnectionDataSource = this.CentralDbServer;
            GlobalSettingsManager.Instance.UseWindowsAuthForCentralDb = this.CentralDbUseWindowsAuth;
            GlobalSettingsManager.Instance.UseWindowsAuthForCentralDbSync = this.CentralDbUseWindowsAuthSync;
            GlobalSettingsManager.Instance.UseSSLSupportForCentralDb = this.CentralDbUseSSLSupport;
            GlobalSettingsManager.Instance.CentralConnectionUser = this.CentralDbUser;
            GlobalSettingsManager.Instance.CentralConnectionPasswordEncrypted =
                EncryptionManager.Instance.EncryptBlowfish(this.CentralDbPassword.Trim(), Constants.PasswordEncryptionKey);

            bool updatePathModified = false;
            if (this.CanChangeUpdateLocation)
            {
                GlobalSettingsManager.Instance.AutomaticUpdatePath = this.UpdateLocation;
                updatePathModified = true;
            }

            GlobalSettingsManager.Instance.Save();

            if (updatePathModified)
            {
                this.messenger.Send(new NotificationMessage(Notification.HideUpdateAvailable));
            }

            Images.LoadCustomApplicationLogo();

            this.IsChanged = false;
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Cancels any changes and closes the view.
        /// </summary>
        protected override void Cancel()
        {
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Called before unloading the view from its parent or closing it. Returning false will cancel the view's unloading or closing.
        /// </summary>
        /// <returns>
        /// True if the unloading process should continue and false if it should be canceled.
        /// </returns>
        public override bool OnUnloading()
        {
            if (this.IsChanged && !IsInViewerMode)
            {
                MessageDialogResult messageBoxResult = this.windowService.MessageDialogService.Show(LocalizedResources.Question_Quit, MessageDialogType.YesNo);
                if (MessageDialogResult.Yes == messageBoxResult)
                {
                    if (this.SelectedBackgroundID != UserSettingsManager.Instance.BackgroundColorID)
                    {
                        Images.SetBackground(UserSettingsManager.Instance.BackgroundColorID);
                    }

                    return true;
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Restarts the application.
        /// </summary>
        private void RestartApplication()
        {
            string error = this.ValidateInput();
            if (error != null)
            {
                this.windowService.MessageDialogService.Show(error, MessageDialogType.Error);
                return;
            }

            this.Save();

            messenger.Send(new NotificationMessage(Notification.RestartApplication));
        }

        /// <summary>
        /// Resets the main view layout.
        /// </summary>
        private void ResetMainViewLayout()
        {
            try
            {
                // Delete the file containing the main view layout
                string layoutFile = Constants.ApplicationDataFolderPath + "\\UILayout.xml";
                System.IO.File.Delete(layoutFile);

                // Notify the main view to reload its layout. With the layout file deleted, the main view will restore its default layout.
                this.messenger.Send(new NotificationMessage(Notification.ReloadMainViewLayout));
            }
            catch (Exception ex)
            {
                log.WarnException("Failed to delete the file containing the main view layout.", ex);
            }
        }

        /// <summary>
        /// Browses the image to set as custom application logo.
        /// </summary>
        private void BrowseForCustomLogo()
        {
            this.fileDialogService.Reset();
            this.fileDialogService.Filter = LocalizedResources.DialogFilter_Images;
            this.fileDialogService.Multiselect = false;

            if (fileDialogService.ShowOpenFileDialog() == false)
            {
                return;
            }

            var path = fileDialogService.FileName;
            var logoExtension = Path.GetExtension(path);
            if (string.IsNullOrWhiteSpace(path) ||
                (!logoExtension.Equals(".jpg", StringComparison.InvariantCultureIgnoreCase) &&
                !logoExtension.Equals(".jpeg", StringComparison.InvariantCultureIgnoreCase) &&
                !logoExtension.Equals(".png", StringComparison.InvariantCultureIgnoreCase)))
            {
                // If the image does not have a supported type, the user gets an error message.
                this.windowService.MessageDialogService.Show(LocalizedResources.General_ImageNotSupported, MessageDialogType.Error);
                return;
            }
            else if (!string.IsNullOrWhiteSpace(path))
            {
                try
                {
                    Image selectedLogoImage = Image.FromFile(path);
                    if (selectedLogoImage.Width > 800 || selectedLogoImage.Height > 600)
                    {
                        // If the image's resolution is not valid, the user gets an error message.
                        this.windowService.MessageDialogService.Show(LocalizedResources.General_ResolutionNotSupported, MessageDialogType.Error);
                        return;
                    }
                }
                catch
                {
                    // If the image file is corrupted and could not be opened, the user gets an error message.
                    this.windowService.MessageDialogService.Show(LocalizedResources.Error_CantCreateImageFromFile, MessageDialogType.Error);
                    return;
                }
            }

            this.CustomLogoPath = path;
        }

        /// <summary>
        /// Validates the input.
        /// </summary>
        /// <returns>
        /// The error message if the input is not valid; otherwise returns null.
        /// </returns>
        private string ValidateInput()
        {
            string errorMessage = null;

            if (string.IsNullOrWhiteSpace(this.LocalDbServer)
                || (string.IsNullOrWhiteSpace(this.LocalDbUser) && !this.LocalDbUseWindowsAuth)
                || (string.IsNullOrWhiteSpace(this.LocalDbPassword) && !this.LocalDbUseWindowsAuth)
                || string.IsNullOrWhiteSpace(this.CentralDbServer)
                || (string.IsNullOrWhiteSpace(this.CentralDbUser) && !this.CentralDbUseWindowsAuth)
                || (string.IsNullOrWhiteSpace(this.CentralDbPassword) && !this.CentralDbUseWindowsAuth))
            {
                errorMessage = LocalizedResources.Preferences_DbInfoMissing;
            }

            if (this.CanChangeUpdateLocation
                && !string.IsNullOrWhiteSpace(this.UpdateLocation))
            {
                if (!(this.UpdateLocation.StartsWith("\\\\")
                    || this.UpdateLocation.StartsWith("http://")
                    || this.UpdateLocation.StartsWith("https://")))
                {
                    errorMessage = LocalizedResources.Update_WrongFormatForUpdatePath;
                }
            }

            return errorMessage;
        }

        /// <summary>
        /// Determines if the Restart button should be visible or not.
        /// </summary>
        private void EvaluateRestartVisibility()
        {
            bool restartVisibility = false;

            string newUILanguage = this.SelectedUILanguage as string;
            if (newUILanguage != UserSettingsManager.Instance.UILanguage)
            {
                restartVisibility = true;
            }

            if (GlobalSettingsManager.Instance.LocalConnectionDataSource != this.LocalDbServer)
            {
                restartVisibility = true;
            }

            if (GlobalSettingsManager.Instance.LocalConnectionUser != this.LocalDbUser)
            {
                restartVisibility = true;
            }

            string initLocalPassword = EncryptionManager.Instance.DecryptBlowfish(
                GlobalSettingsManager.Instance.LocalConnectionPasswordEncrypted,
                Constants.PasswordEncryptionKey);
            if (initLocalPassword != this.LocalDbPassword)
            {
                restartVisibility = true;
            }

            if (GlobalSettingsManager.Instance.UseWindowsAuthForLocalDb != this.LocalDbUseWindowsAuth)
            {
                restartVisibility = true;
            }

            if (GlobalSettingsManager.Instance.UseWindowsAuthForCentralDbSync != this.CentralDbUseWindowsAuthSync)
            {
                restartVisibility = true;
            }

            if (GlobalSettingsManager.Instance.UseWindowsAuthForLocalDbSync != this.LocalDbUseWindowsAuthSync)
            {
                restartVisibility = true;
            }

            if (GlobalSettingsManager.Instance.CentralConnectionDataSource != this.CentralDbServer)
            {
                restartVisibility = true;
            }

            if (GlobalSettingsManager.Instance.CentralConnectionUser != this.CentralDbUser)
            {
                restartVisibility = true;
            }

            string initCentralPassword = EncryptionManager.Instance.DecryptBlowfish(
                GlobalSettingsManager.Instance.CentralConnectionPasswordEncrypted,
                Constants.PasswordEncryptionKey);
            if (initCentralPassword != this.CentralDbPassword)
            {
                restartVisibility = true;
            }

            if (GlobalSettingsManager.Instance.UseWindowsAuthForCentralDb != this.CentralDbUseWindowsAuth)
            {
                restartVisibility = true;
            }

            if (GlobalSettingsManager.Instance.UseSSLSupportForCentralDb != this.CentralDbUseSSLSupport)
            {
                restartVisibility = true;
            }

            this.ShowRestart = restartVisibility;
        }
    }
}