﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Resources;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for the ManageSettings view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ManageSettingsViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// A value indicating whether to prohibit the reuse of previously used passwords..
        /// </summary>
        private bool prohibitReuseOfPreviousPasswords;

        /// <summary>
        /// The number of previous passwords.
        /// </summary>
        private int? numberOfPreviousPasswords;

        /// <summary>
        /// The central database version.
        /// </summary>
        private string centralDbVersion;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ManageSettingsViewModel"/> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        [ImportingConstructor]
        public ManageSettingsViewModel(
            IWindowService windowService)
        {
            Argument.IsNotNull("windowService", windowService);

            this.windowService = windowService;

            this.InitializeProperties();
            this.InitializeUndoManager();
            this.IsChanged = false;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether to check the number of previous passwords.
        /// </summary>
        [UndoableProperty]
        public bool ProhibitReuseOfPreviousPasswords
        {
            get
            {
                return this.prohibitReuseOfPreviousPasswords;
            }

            set
            {
                this.SetProperty(ref this.prohibitReuseOfPreviousPasswords, value, () => this.ProhibitReuseOfPreviousPasswords);
                using (this.UndoManager.StartBatch(includePreviousItem: true, reverseUndoOrder: false, navigateToBatchControls: true))
                {
                    if (!value)
                    {
                        this.NumberOfPreviousPasswords = null;
                    }
                }

                this.IsChanged = true;
            }
        }

        /// <summary>
        /// Gets or sets the number of previous passwords.
        /// </summary>        
        [UndoableProperty]
        public int? NumberOfPreviousPasswords
        {
            get
            {
                return this.numberOfPreviousPasswords;
            }

            set
            {
                this.SetProperty(ref this.numberOfPreviousPasswords, value, () => this.NumberOfPreviousPasswords);
                this.IsChanged = true;
            }
        }

        /// <summary>
        /// The contact email
        /// </summary>
        private string contactEmail;

        /// <summary>
        /// Gets or sets the contact email.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public string ContactEmail
        {
            get
            {
                return this.contactEmail;
            }

            set
            {
                this.SetProperty(ref this.contactEmail, value, () => this.ContactEmail);
                this.IsChanged = true;
            }
        }

        /// <summary>
        /// Gets or sets the central database version.
        /// </summary>
        public string CentralDbVersion
        {
            get { return this.centralDbVersion; }
            set { this.SetProperty(ref this.centralDbVersion, value, () => this.CentralDbVersion); }
        }

        #endregion

        #region Implementation

        /// <summary>
        /// Initializes the properties of this instance.
        /// </summary>
        private void InitializeProperties()
        {
            var globalSettingsManager = new DbGlobalSettingsManager(DbIdentifier.CentralDatabase);
            var globalSettings = globalSettingsManager.Get();
            if (globalSettings.NumberOfPreviousPasswordsToCheck != 0)
            {
                this.ProhibitReuseOfPreviousPasswords = true;
                this.NumberOfPreviousPasswords = globalSettings.NumberOfPreviousPasswordsToCheck;
            }
            else
            {
                this.ProhibitReuseOfPreviousPasswords = false;
                this.NumberOfPreviousPasswords = null;
            }

            this.ContactEmail = globalSettings.ContactEmail;

            // Get a fresh (un-cached) database version value.
            var dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            var dbVersionSetting = dataManager.GlobalSettingsRepository.GetVersion();
            this.CentralDbVersion = string.Format(
                "{0}: {1}",
                LocalizedResources.General_CentralDbVersion,
                dbVersionSetting.Value);

            this.IsChanged = false;
        }

        /// <summary>
        /// Initialize the Undo Manager.
        /// </summary>
        private void InitializeUndoManager()
        {
            this.UndoManager.Start();
        }

        /// <summary>
        /// Determines whether this instance can perform the save operation. Executed by the SaveCommand.
        /// </summary>
        /// <returns>
        /// true if the save operation can be performed, false otherwise.
        /// </returns>
        protected override bool CanSave()
        {
            return base.CanSave()
                && (!this.ProhibitReuseOfPreviousPasswords || (this.ProhibitReuseOfPreviousPasswords && this.NumberOfPreviousPasswords.HasValue))
                && ValidationUtils.IsEmail(ContactEmail);
        }

        /// <summary>
        /// Performs the save operation. Executed by the SaveCommand.
        /// </summary>
        protected override void Save()
        {
            // Saftey vaidation.
            if (!CanSave())
            {
                return;
            }

            var globalSettingsManager = new DbGlobalSettingsManager(DbIdentifier.CentralDatabase);
            var globalSettings = globalSettingsManager.Get();

            if (this.ProhibitReuseOfPreviousPasswords && this.NumberOfPreviousPasswords.HasValue)
            {
                globalSettings.NumberOfPreviousPasswordsToCheck = this.NumberOfPreviousPasswords.Value;
            }
            else
            {
                globalSettings.NumberOfPreviousPasswordsToCheck = 0;
            }

            globalSettings.ContactEmail = ContactEmail;

            globalSettingsManager.Save(globalSettings);
            this.IsChanged = false;
        }

        /// <summary>
        /// Cancels all changes. Executed by the CancelCommand.
        /// </summary>
        protected override void Cancel()
        {
            if (this.IsChanged)
            {
                MessageDialogResult result = MessageDialogResult.Yes;
                result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_CancelChanges, MessageDialogType.YesNo);

                if (result != MessageDialogResult.Yes)
                {
                    // Don't cancel the changes and also don't close the view-model.
                    return;
                }
                else
                {
                    // Cancel all changes.
                    using (this.UndoManager.StartBatch(includePreviousItem: true, reverseUndoOrder: false, navigateToBatchControls: true))
                    {
                        this.InitializeProperties();
                    }
                }
            }

            // Close the view-model when is displayed in a window.
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Called before unloading the view from its parent. Returning false will cancel the view's unloading.
        /// </summary>
        /// <returns>
        /// True if the unloading process should continue and false if it should be canceled.
        /// </returns>
        public override bool OnUnloading()
        {
            // Do nothing if the view-model is read-only, in viewer mode or it was not changed.
            if (this.IsReadOnly || this.IsInViewerMode || !this.IsChanged)
            {
                return true;
            }

            // Ask the user if he wants to save
            var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_UnsavedDataOnQuit, MessageDialogType.YesNoCancel);
            if (result == MessageDialogResult.Yes)
            {
                // The user whishes to save but it is not possible because the input is not valid. Return false to stop the view-model from unloading.
                if (!this.CanSave())
                {
                    return false;
                }

                this.Save();
            }
            else if (result == MessageDialogResult.No)
            {
                // The user does not want to save.
                this.IsChanged = false;
            }
            else
            {
                return false;
            }

            return true;
        }

        #endregion
    }
}
