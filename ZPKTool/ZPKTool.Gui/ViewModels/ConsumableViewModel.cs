﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Input;
using ZPKTool.Business;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the IMyView view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ConsumableViewModel : ViewModel<Consumable, IDataSourceManager>
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The messenger.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The model browser helper service.
        /// </summary>
        private IModelBrowserHelperService modelBrowserHelperService;

        /// <summary>
        /// A list containing the price measure unit items.
        /// </summary>
        private Collection<MeasurementUnit> priceUnitBaseItems;

        /// <summary>
        /// The consumable cost
        /// </summary>
        private decimal consumableCost;

        /// <summary>
        /// The parent of the consumable model
        /// </summary>
        private object consumableParent;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        /// <summary>
        /// The UnitsAdapter handler, used to perform certain operations when some UnitsAdapter properties are updated.
        /// </summary>
        private UnitsAdapterUpdateHandler unitsAdapterHandler;

        #endregion Attributes

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ConsumableViewModel"/> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        /// <param name="messenger">The messenger.</param>
        /// <param name="modelBrowserHelperService">The model browser helper service.</param>
        /// <param name="costRecalculationCloneManager">The cost recalculation clone manager.</param>
        /// <param name="manufacturerViewModel">The manufacturer view model.</param>
        /// <param name="masterDataBrowser">The master data browser.</param>
        /// <param name="unitsService">The units service.</param>
        [ImportingConstructor]
        public ConsumableViewModel(
            IWindowService windowService,
            IMessenger messenger,
            IModelBrowserHelperService modelBrowserHelperService,
            ICostRecalculationCloneManager costRecalculationCloneManager,
            ManufacturerViewModel manufacturerViewModel,
            MasterDataBrowserViewModel masterDataBrowser,
            IUnitsService unitsService)
        {
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("manufacturerViewModel", manufacturerViewModel);
            Argument.IsNotNull("masterDataBrowser", masterDataBrowser);
            Argument.IsNotNull("unitsService", unitsService);
            Argument.IsNotNull("costRecalculationCloneManager", costRecalculationCloneManager);

            this.windowService = windowService;
            this.messenger = messenger;
            this.modelBrowserHelperService = modelBrowserHelperService;
            this.ManufacturerViewModel = manufacturerViewModel;
            this.MasterDataBrowser = masterDataBrowser;
            this.unitsService = unitsService;
            this.CloneManager = costRecalculationCloneManager;

            // Initialize the manufacturer view-model
            this.ManufacturerViewModel.ShowSaveControls = false;
            this.ManufacturerViewModel.IsChild = true;
                        
            // Initialize the commands
            this.SaveNestedViewModels = new CompositeCommand();
            this.SaveNestedViewModels.RegisterCommand(this.ManufacturerViewModel.SaveToModelCommand);
            this.CancelNestedViewModels = new CompositeCommand();
            this.CancelNestedViewModels.RegisterCommand(this.ManufacturerViewModel.CancelCommand);
            this.BrowseMasterDataCommand = new DelegateCommand(BrowseMasterData);

            this.InitializeProperties();

            this.ManufacturerViewModel.UndoManager = this.UndoManager;
        }

        #endregion Constructor

        #region Commands

        /// <summary>
        /// Gets the browse master data command.
        /// </summary>
        public ICommand BrowseMasterDataCommand { get; private set; }

        /// <summary>
        /// Gets or sets the command that save all changes in the nested view models back into their models.
        /// This command aggregates the SaveToModel commands of the nested view models.
        /// </summary>
        private CompositeCommand SaveNestedViewModels { get; set; }

        /// <summary>
        /// Gets or sets the command that cancels all changes of the nested view models.
        /// This command aggregates the Cancel commands of the nested view models.
        /// </summary>
        private CompositeCommand CancelNestedViewModels { get; set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets the name of the consumable.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Name", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Name")]
        public DataProperty<string> Name { get; private set; }

        /// <summary>
        /// Gets the consumable price.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Price", AffectsCost = true)]
        public DataProperty<decimal?> Price { get; private set; }

        /// <summary>
        /// Gets the price measurement unit.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_PriceUnitBase", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty]
        [ExposesModelProperty("PriceUnitBase", AffectsCost = true)]
        public DataProperty<MeasurementUnit> PriceUnit { get; private set; }

        /// <summary>
        /// Gets or sets a list of price of Units which populates the combo box.
        /// </summary>
        public Collection<MeasurementUnit> PriceUnitBaseItems
        {
            get { return this.priceUnitBaseItems; }
            set { this.SetProperty(ref this.priceUnitBaseItems, value, () => this.PriceUnitBaseItems); }
        }

        /// <summary>
        /// Gets the consumable amount.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Amount", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Amount", AffectsCost = true)]
        public DataProperty<decimal?> Amount { get; private set; }

        /// <summary>
        /// Gets the consumable amount measurement unit.
        /// </summary>
        [UndoableProperty]
        [ExposesModelProperty("AmountUnitBase")]
        public DataProperty<MeasurementUnit> AmountUnit { get; private set; }

        /// <summary>
        /// Gets the collection of Units which populates the textbox units.
        /// </summary>
        public VMProperty<ObservableCollection<MeasurementUnit>> AmountUnits { get; private set; }

        /// <summary>
        /// Gets the consumable description.
        /// </summary>
        /// <value>The remarks.</value>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Description")]
        public DataProperty<string> Description { get; private set; }

        /// <summary>
        /// Gets the manufacturer view model.
        /// </summary>
        public ManufacturerViewModel ManufacturerViewModel { get; private set; }

        /// <summary>
        /// Gets the master data browser.
        /// </summary>
        public MasterDataBrowserViewModel MasterDataBrowser { get; private set; }

        /// <summary>
        /// Gets or sets the consumable cost
        /// </summary>
        public decimal ConsumableCost
        {
            get { return this.consumableCost; }
            set { this.SetProperty(ref this.consumableCost, value, () => this.ConsumableCost); }
        }

        /// <summary>
        /// Gets or sets the consumable parent
        /// </summary>
        public object ConsumableParent
        {
            get { return this.consumableParent; }
            set { this.SetProperty(ref this.consumableParent, value, () => this.ConsumableParent); }
        }

        /// <summary>
        /// Gets or sets the project to which the Model belongs.
        /// This property is needed during cost calculations so it must be set when editing (it can be omitted when creating).
        /// </summary>
        public Project ParentProject { get; set; }

        /// <summary>
        /// Gets or sets the message token to be used when sending messages with token 
        /// </summary>
        public string MessageToken { get; set; }

        /// <summary>
        /// Gets or sets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter { get; set; }

        #endregion Properties

        #region Initialization

        /// <summary>
        /// Initializes the view-model for the creation of a new consumable.
        /// The ModelDataContext property must be set to a valid value before this call.
        /// </summary>
        /// <param name="isMasterData">If set to true the consumable will be created in master data; in this case the parent should be set to null.</param>
        /// <param name="parent">
        /// The parent object of the consumable that will be created. If this is set, the value of the <paramref name="isMasterData"/> argument is ignored and
        /// the parent's IsMasterData flag is used instead.
        /// </param>
        /// <remarks>
        /// This is a helper method for populating the model with default data for creation. It is not mandatory to use it; you can set the model to a new
        /// instance set up however you want.
        /// </remarks>
        /// <exception cref="ArgumentException">
        /// <paramref name="parent"/> is null and <paramref name="isMasterData"/> is false, or the type of<paramref name="parent"/> is not supported.
        /// </exception>
        /// <exception cref="InvalidOperationException">The provided <paramref name="parent"/> object type is not supported.</exception>
        public void InitializeForCreation(bool isMasterData, object parent)
        {
            this.CheckDataSource();
            this.EditMode = ViewModelEditMode.Create;
            Consumable newConsumable = new Consumable();

            if (!isMasterData)
            {
                if (parent == null)
                {
                    throw new ArgumentException("A non master data consumable cannot be created without a parent", "parent");
                }

                var processStep = parent as ProcessStep;
                if (processStep != null)
                {
                    newConsumable.ProcessStep = processStep;
                    isMasterData = processStep.IsMasterData;
                }
                else
                {
                    throw new InvalidOperationException(string.Format("An object of type '{0}' is not supported as consumable parent.", parent.GetType().Name));
                }
            }

            newConsumable.Manufacturer = new Manufacturer();

            // Set the master data flag and the owner at the end so they are applied to all sub-objects.
            newConsumable.SetIsMasterData(isMasterData);
            if (!newConsumable.IsMasterData)
            {
                User owner = this.DataSourceManager.UserRepository.GetById(SecurityManager.Instance.CurrentUser.Guid, false);
                newConsumable.SetOwner(owner);
            }

            this.Model = newConsumable;
        }

        /// <summary>
        /// Initializes the properties of this instance.
        /// </summary>
        private void InitializeProperties()
        {
            this.AmountUnits.Value = new ObservableCollection<MeasurementUnit>();
            this.PriceUnitBaseItems = new Collection<MeasurementUnit>();

            this.PriceUnit.ValueChanged += (s, e) => this.OnPriceUnitChanged();
            this.Amount.ValueChanged += (s, e) => this.OnAmountChanged();
            this.Price.ValueChanged += (s, e) => this.OnPriceChanged();
        }

        /// <summary>
        /// Initialize PriceBaseUnit Items
        /// </summary>
        private void InitializetPriceBaseMeasurementUnits()
        {
            if (this.IsInViewerMode)
            {
                return;
            }

            var units = this.DataSourceManager.MeasurementUnitRepository.GetBaseMeasurementUnits();
            var priceUnits = units.Where(unit =>
                unit.Type == MeasurementUnitType.Weight ||
                unit.Type == MeasurementUnitType.Volume ||
                unit.Type == MeasurementUnitType.Volume2 ||
                unit.Type == MeasurementUnitType.Pieces ||
                unit.Type == MeasurementUnitType.Length ||
                unit.Type == MeasurementUnitType.Area);

            foreach (MeasurementUnit unit in priceUnits)
            {
                this.PriceUnitBaseItems.Add(unit);
            }
        }

        /// <summary>
        /// Initializes the measurement units.
        /// </summary>
        /// <param name="measurementUnitScale">The measurement unit scale.</param>
        private void InitializeAmountUnits(MeasurementUnitScale measurementUnitScale)
        {
            if (this.IsInViewerMode)
            {
                return;
            }

            var measurementUnits = this.DataSourceManager.MeasurementUnitRepository.GetAll(measurementUnitScale);
            this.AmountUnits.Value = new ObservableCollection<MeasurementUnit>(measurementUnits);
        }

        #endregion Initialization

        #region Model handling

        /// <summary>
        /// Loads the data from the model.
        /// Each view-model property decorated with the ExposedModelProperty attribute is loaded from the associated Model property.
        /// </summary>
        /// <param name="model">The model instance.</param>
        public override void LoadDataFromModel(Consumable model)
        {
            base.LoadDataFromModel(model);

            if (this.IsInViewerMode
                && this.Model.PriceUnitBase != null)
            {
                this.PriceUnitBaseItems.Add(this.Model.PriceUnitBase);
            }

            if (this.PriceUnit.Value != null && !this.PriceUnitBaseItems.Contains(this.PriceUnit.Value))
            {
                // PriceUnit instance is not from the view model's DataSourceManager so PriceUnitBaseItems doesn't contain it but it may contain an equivalent instance.
                this.PriceUnit.Value = this.PriceUnitBaseItems.FirstOrDefault(u => u.Symbol == this.PriceUnit.Value.Symbol);
            }

            if (this.IsInViewerMode
                && this.Model.AmountUnitBase != null)
            {
                this.AmountUnits.Value.Add(this.Model.AmountUnitBase);
            }

            if (this.AmountUnit.Value != null && !this.AmountUnits.Value.Contains(this.AmountUnit.Value))
            {
                this.AmountUnit.Value = this.AmountUnits.Value.FirstOrDefault(u => u.Symbol == this.AmountUnit.Value.Symbol);
            }
        }

        /// <summary>
        /// Called when the Model has changed.
        /// </summary>
        protected override void OnModelChanged()
        {
            this.CheckDataSource();
            base.OnModelChanged();

            if (this.IsInViewerMode)
            {
                this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(null);
            }

            this.ManufacturerViewModel.DataSourceManager = this.DataSourceManager;
            this.ManufacturerViewModel.Model = this.Model.Manufacturer ?? new Manufacturer();

            this.CalculateConsumableCost();
            this.UndoManager.Start();

            if (!this.IsChild)
            {
                this.CloneManager.Clone(this);
            }
        }

        /// <summary>
        /// Called when DataSourceManager has changed.
        /// </summary>
        protected override void OnDataSourceManagerChanged()
        {
            base.OnDataSourceManagerChanged();

            this.InitializetPriceBaseMeasurementUnits();
            this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(this.DataSourceManager);
            this.unitsAdapterHandler = new UnitsAdapterUpdateHandler(this.MeasurementUnitsAdapter);
            this.unitsAdapterHandler.PauseUndoOnUnitsAdapterUpdate(this.UndoManager);
        }

        #endregion Model handling

        #region Property change handlers

        /// <summary>
        /// Called when the PriceUnitBase property has changed.
        /// </summary>
        private void OnPriceUnitChanged()
        {
            if (this.PriceUnit.Value != null)
            {
                this.UndoManager.Pause();
                bool isAmountValueInitializedBeforeUnit = false;

                // Get the amount displayed value when the unit type is changed.
                decimal? amountDisplayedValue = null;
                bool keepAmount = false;
                if (this.AmountUnit.Value != null && this.AmountUnit.Value.Type != this.PriceUnit.Value.Type
                    && this.AmountUnits.Value != null && this.AmountUnits.Value.Count > 0)
                {
                    amountDisplayedValue = this.Amount.Value / this.AmountUnit.Value.ConversionRate;
                    keepAmount = true;
                }

                if (this.AmountUnits.Value.Count == 0 && this.Amount.Value != null)
                {
                    isAmountValueInitializedBeforeUnit = true;
                }

                this.UndoManager.Resume();

                using (this.UndoManager.StartBatch(includePreviousItem: true, reverseUndoOrder: false, navigateToBatchControls: true))
                {
                    this.StopRecalculationNotifications();
                    this.InitializeAmountUnits((MeasurementUnitScale)this.PriceUnit.Value.ScaleID);
                    this.ResumeRecalculationNotifications(true);
                }

                using (this.UndoManager.Pause())
                {
                    if (keepAmount && this.AmountUnit.Value != null)
                    {
                        // Update the Amount value based on displayed value
                        this.Amount.Value = amountDisplayedValue * this.AmountUnit.Value.ConversionRate;
                    }

                    if (isAmountValueInitializedBeforeUnit)
                    {
                        this.Amount.Value = this.Amount.Value * this.PriceUnit.Value.ConversionRate;
                    }
                }
            }
        }

        /// <summary>
        /// Called when the Amount property has changed.
        /// </summary>
        private void OnAmountChanged()
        {
            this.CalculateConsumableCost();
        }

        /// <summary>
        /// Called when the Price property has changed.
        /// </summary>
        private void OnPriceChanged()
        {
            this.CalculateConsumableCost();
        }

        #endregion Property change handlers

        #region Master Data Browse handling

        /// <summary>
        /// Opens the master data browser to select a consumable.
        /// </summary>
        private void BrowseMasterData()
        {
            this.MasterDataBrowser.MasterDataType = typeof(Consumable);
            this.MasterDataBrowser.MasterDataSelected += this.OnMasterDataConsumableSelected;
            this.windowService.ShowViewInDialog(this.MasterDataBrowser);
            this.MasterDataBrowser.MasterDataSelected -= this.OnMasterDataConsumableSelected;
        }

        /// <summary>
        /// Called when a master data consumable is selected from the master data browser.
        /// </summary>
        /// <param name="masterDataEntity">The master data entity selected in the browser.</param>
        /// <param name="entitySourceDb">The source database of the selected master data.</param>
        private void OnMasterDataConsumableSelected(object masterDataEntity, DbIdentifier entitySourceDb)
        {
            Consumable masterConsumable = masterDataEntity as Consumable;
            if (masterConsumable == null)
            {
                return;
            }

            this.StopRecalculationNotifications();
            using (this.UndoManager.StartBatch(undoEachBatch: true))
            {
                // Convert the master data entity currency values.
                var dataManager = DataAccessFactory.CreateDataSourceManager(entitySourceDb);
                var currencies = dataManager.CurrencyRepository.GetBaseCurrencies();
                var baseCurrencyFromMD = currencies.FirstOrDefault(c => c.IsSameAs(this.MeasurementUnitsAdapter.BaseCurrency));
                CurrencyConversionManager.ConvertObject(masterConsumable, baseCurrencyFromMD, CurrencyConversionManager.DefaultBaseCurrency);

                // Load the data from the master consumable and sub-objects in the corresponding view-models. Use empty data for null sub-objects.
                this.LoadDataFromModel(masterConsumable);

                // Obtain the price unit from the data context of the model.
                if (this.PriceUnit.Value != null)
                {
                    this.PriceUnit.Value = this.DataSourceManager.MeasurementUnitRepository.GetByName(this.PriceUnit.Value.Name);
                }

                // If amount unit value is changing when price unit is changing 
                // so is needed to load it again from master data entity.
                if (this.AmountUnit.Value != masterConsumable.AmountUnitBase)
                {
                    this.AmountUnit.Value = masterConsumable.AmountUnitBase;
                }

                // Obtain the amount unit from the data context of the model.
                if (this.AmountUnit.Value != null)
                {
                    this.AmountUnit.Value = this.DataSourceManager.MeasurementUnitRepository.GetByName(this.AmountUnit.Value.Name);
                }

                this.ManufacturerViewModel.LoadDataFromModel(masterConsumable.Manufacturer ?? new Manufacturer());
            }

            this.ResumeRecalculationNotifications(true);
        }

        #endregion Master Data Browse handling

        #region Cost handling

        /// <summary>
        /// Calculate Consumable Cost as the product of Amount and Price.
        /// </summary>
        private void CalculateConsumableCost()
        {
            Consumable updatedConsumable = new Consumable();
            updatedConsumable.Price = this.Price.Value;
            updatedConsumable.Amount = this.Amount.Value;

            string costCalculationVersion = null;
            if (this.ConsumableParent is Part)
            {
                Part part = (Part)this.ConsumableParent;
                costCalculationVersion = part.CalculationVariant;
            }
            else if (this.ConsumableParent is Assembly)
            {
                Assembly assy = (Assembly)this.ConsumableParent;
                costCalculationVersion = assy.CalculationVariant;
            }
            else
            {
                costCalculationVersion = CostCalculatorFactory.LatestVersion;
            }

            ICostCalculator calculator = CostCalculatorFactory.GetCalculator(costCalculationVersion);
            ConsumableCost cost = calculator.CalculateConsumableCost(updatedConsumable, new ConsumableCostCalculationParameters());

            this.ConsumableCost = cost.Cost;
        }

        /// <summary>
        /// Refreshes the consumable parent's cost and notifies listeners.
        /// </summary>        
        private void RefreshParentCost()
        {
            this.RefreshCalculations(this.ConsumableParent);
        }

        #endregion Cost handling

        #region Save/Cancel

        /// <summary>
        /// Saves all changes back into the model.
        /// </summary>
        protected override void SaveToModel()
        {
            this.CheckModel();
            base.SaveToModel();

            var manufacturer = this.ManufacturerViewModel.Model;
            if (manufacturer != null)
            {
                this.Model.Manufacturer = manufacturer;
            }
        }

        /// <summary>
        /// Determines whether this instance can perform the save operation. Executed by the SaveCommand.
        /// The default implementation allows the save to be performed if the input is valid.
        /// </summary>
        /// <returns>
        /// true if the save operation can be performed, false otherwise.
        /// </returns>
        protected override bool CanSave()
        {
            return base.CanSave() && this.SaveNestedViewModels.CanExecute(null);
        }

        /// <summary>
        /// Performs the save operation. Executed by the SaveCommand.
        /// </summary>
        protected override void Save()
        {
            this.CheckModelAndDataSource();

            // Save all changes back into the Model objects. The validity check of this operation is performed by the CanSave method.
            this.SaveToModel();
            this.SaveNestedViewModels.Execute(null);

            if (this.SavesToDataSource)
            {
                // Update consumable
                this.DataSourceManager.ConsumableRepository.Save(this.Model);
                this.DataSourceManager.SaveChanges();

                // Notify the other components that the consumable was created/updated.
                EntityChangedMessage message = this.Model.IsMasterData ?
                    new EntityChangedMessage(Notification.MasterDataEntityChanged) :
                    new EntityChangedMessage(Notification.MyProjectsEntityChanged);

                message.ChangeType = this.EditMode == ViewModelEditMode.Create ? EntityChangeType.EntityCreated : EntityChangeType.EntityUpdated;
                message.Entity = this.Model;
                message.Parent = this.Model.ProcessStep;
                this.SendMessage(message, false);
            }

            this.RefreshParentCost();

            // Close the view-model when is displayed in a window.            
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Determines whether the Cancel operation can be performed. Executed by the CancelCommand.
        /// </summary>
        /// <returns>
        /// true if the changes can be canceled, false otherwise.
        /// </returns>
        protected override bool CanCancel()
        {
            return base.CanCancel() && this.CancelNestedViewModels.CanExecute(null);
        }

        /// <summary>
        /// Cancels all changes. Executed by the CancelCommand.
        /// </summary>
        protected override void Cancel()
        {
            if (!this.CanCancel())
            {
                return;
            }

            if (this.IsChanged)
            {
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_CancelChanges, MessageDialogType.YesNo);
                if (result != MessageDialogResult.Yes)
                {
                    // Don't cancel the changes and also don't close the view-model.
                    return;
                }
                else
                {
                    using (this.UndoManager.StartBatch())
                    {
                        // Cancel all changes
                        base.Cancel();
                        this.CancelNestedViewModels.Execute(null);
                    }
                }
            }

            // Close the view-model when is displayed in a window.            
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Called before unloading the view from its parent. Returning false will cancel the view's unloading.
        /// </summary>
        /// <returns>
        /// True if the unloading process should continue and false if it should be canceled.
        /// </returns>
        public override bool OnUnloading()
        {
            // Do nothing if the view-model is read-only, in viewer mode, it was not changed or the model was deleted.
            if (this.IsReadOnly || this.IsInViewerMode || !this.IsChanged || this.Model.IsDeleted)
            {
                return true;
            }

            if (this.EditMode == ViewModelEditMode.Create)
            {
                // Ask the user to confirm quitting
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_Quit, MessageDialogType.YesNo);
                if (result != MessageDialogResult.Yes)
                {
                    // The user chose to stay on the screen; return false to stop the screen unloading.
                    return false;
                }
                else
                {
                    this.IsChanged = false;
                }
            }
            else if (this.EditMode == ViewModelEditMode.Edit)
            {
                // Ask the user if he wants to save
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_UnsavedDataOnQuit, MessageDialogType.YesNoCancel);
                if (result == MessageDialogResult.Yes)
                {
                    // The user whishes to save but it is not possible because the input is not valid. Return false to stop the view-model from unloading.
                    if (!this.CanSave())
                    {
                        return false;
                    }

                    this.Save();
                }
                else if (result == MessageDialogResult.No)
                {
                    // The user does not want to save.                    
                    this.IsChanged = false;
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Called after one or more model properties value changed in order to refresh the calculations.
        /// </summary>
        protected override void RefreshCalculation()
        {
            if (this.EditMode == ViewModelEditMode.Create)
            {
                return;
            }

            var parenPart = this.ModelParentClone as Part;
            if (parenPart != null)
            {
                this.RefreshCalculations(parenPart);
            }
            else
            {
                var parentAssembly = this.ModelParentClone as Assembly;
                if (parentAssembly != null)
                {
                    this.RefreshCalculations(parentAssembly);
                }
            }
        }

        #endregion Save/Cancel

        /// <summary>
        /// Calculates the parent and sends a message with the result.
        /// </summary>
        /// <param name="entityParent">The entity parent.</param>
        private void RefreshCalculations(object entityParent)
        {
            if (entityParent == null)
            {
                return;
            }

            var parentPart = entityParent as Part;
            if (parentPart != null)
            {
                PartCostCalculationParameters calculationParams = null;
                if (this.ParentProject != null)
                {
                    calculationParams = CostCalculationHelper.CreatePartParamsFromProject(this.ParentProject);
                }
                else if (IsInViewerMode)
                {
                    // Note: theoretically this part is never reached because it's not possible to save in view mode.
                    calculationParams = this.modelBrowserHelperService.GetPartCostCalculationParameters();
                }

                if (calculationParams != null)
                {
                    var calculator = CostCalculatorFactory.GetCalculator(parentPart.CalculationVariant);
                    var result = calculator.CalculatePartCost(parentPart, calculationParams);
                    this.SendMessage(new CurrentComponentCostChangedMessage(result), true);
                }
            }
            else
            {
                var parentAssembly = entityParent as Assembly;
                if (parentAssembly != null)
                {
                    AssemblyCostCalculationParameters calculationParams = null;
                    if (this.ParentProject != null)
                    {
                        calculationParams = CostCalculationHelper.CreateAssemblyParamsFromProject(this.ParentProject);
                    }
                    else if (IsInViewerMode)
                    {
                        // Note: theoretically this part is never reached because it's not possible to save in view mode.
                        calculationParams = this.modelBrowserHelperService.GetAssemblyCostCalculationParameters();
                    }

                    if (calculationParams != null)
                    {
                        var calculator = CostCalculatorFactory.GetCalculator(parentAssembly.CalculationVariant);
                        var result = calculator.CalculateAssemblyCost(parentAssembly, calculationParams);
                        this.SendMessage(new CurrentComponentCostChangedMessage(result), true);
                    }
                }
            }
        }

        /// <summary>
        /// Sends a message with or without a token
        /// </summary>
        /// <typeparam name="TMessage">The message type</typeparam>
        /// <param name="message">The message to send</param>
        /// <param name="sendWithToken">A value indicating whether to send the message with a token or send it globally without one</param>
        private void SendMessage<TMessage>(TMessage message, bool sendWithToken)
        {
            if (sendWithToken)
            {
                if (this.MessageToken == null)
                {
                    this.messenger.Send(message, this.IsInViewerMode ? GlobalMessengerTokens.ModelBrowserTargetToken : GlobalMessengerTokens.MainViewTargetToken);
                }
                else
                {
                    this.messenger.Send(message, this.MessageToken);
                }
            }
            else
            {
                this.messenger.Send(message);
            }
        }
    }
}