﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Represents an item in the data source exposed by BatchReportingViewModel.
    /// </summary>
    public class BatchReportingDataSourceItem : INotifyPropertyChanged
    {
        /// <summary>
        /// A value indicating whether this item is checked.
        /// </summary>
        private bool? isChecked;

        /// <summary>
        /// The children of the item.
        /// </summary>
        private ObservableCollection<BatchReportingDataSourceItem> children;

        /// <summary>
        /// Initializes a new instance of the <see cref="BatchReportingDataSourceItem"/> class.
        /// </summary>
        public BatchReportingDataSourceItem()
        {
            this.IsChecked = false;
            this.Children = new ObservableCollection<BatchReportingDataSourceItem>();
        }

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets the entity represented by the object.
        /// </summary>
        public object Entity { get; set; }

        /// <summary>
        /// Gets or sets the display name of the item.
        /// </summary>
        public string DisplayName { get; set; }
        
        /// <summary>
        /// Gets or sets a value indicating whether this item is checked.
        /// </summary>
        public bool? IsChecked
        {
            get
            {
                return this.isChecked;
            }

            set
            {
                if (this.isChecked != value)
                {
                    this.isChecked = value;                    
                    RaisePropertyChanged("IsChecked");
                    OnIsCheckedPropertyChanged();
                }
            }
        }
                        
        /// <summary>
        /// Gets or sets the icon representing the type of the item.
        /// </summary>        
        public ImageSource TypeIcon { get; set; }
                        
        /// <summary>
        /// Gets or sets the children of the item. The default value is null.
        /// </summary>
        public ObservableCollection<BatchReportingDataSourceItem> Children
        {
            get
            {
                return this.children;
            }

            set
            {
                if (this.children != value)
                {
                    this.children = value;
                    RaisePropertyChanged("Children");
                }
            }
        }

        /// <summary>
        /// Called when the IsChecked property has changed.
        /// </summary>
        private void OnIsCheckedPropertyChanged()
        {
            // Propagate the value of the IsChanged property to all children, all children's children, and so on.
            bool? newValue = this.IsChecked;
            Queue<BatchReportingDataSourceItem> toggleQueue = new Queue<BatchReportingDataSourceItem>();
            toggleQueue.Enqueue(this.Children);
            while (toggleQueue.Count > 0)
            {
                BatchReportingDataSourceItem item = toggleQueue.Dequeue();
                item.IsChecked = newValue;
                toggleQueue.Enqueue(item.Children);
            }
        }

        /// <summary>
        /// This raises the INotifyPropertyChanged.PropertyChanged event to indicate a specific property has changed value.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        private void RaisePropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
