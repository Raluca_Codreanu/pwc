﻿using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.DataAnnotations;
using System.Windows.Input;
using ZPKTool.Controls;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Resources;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the IMyView view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ManufacturerViewModel : ViewModel<Manufacturer, IDataSourceManager>
    {
        #region Attributes

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The MEF composition container.
        /// </summary>
        private CompositionContainer compositionContainer;

        #endregion Attributes

        /// <summary>
        /// Initializes a new instance of the <see cref="ManufacturerViewModel"/> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        /// <param name="messenger">The messenger.</param>
        /// <param name="compositionContainer">The composition container.</param>
        [ImportingConstructor]
        public ManufacturerViewModel(IWindowService windowService, IMessenger messenger, CompositionContainer compositionContainer)
        {
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("compositionContainer", compositionContainer);

            this.windowService = windowService;
            this.messenger = messenger;
            this.compositionContainer = compositionContainer;

            this.BrowseMasterDataCommand = new DelegateCommand(BrowseMasterData);
        }

        #region Commands

        /// <summary>
        /// Gets the browse master data command.
        /// </summary>
        public ICommand BrowseMasterDataCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets the manufacturer name.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Name", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Name")]
        public DataProperty<string> Name { get; private set; }

        /// <summary>
        /// Gets the manufacturer description.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Description")]
        public DataProperty<string> Description { get; private set; }

        #endregion Properties

        /// <summary>
        /// Called when the Model has changed.
        /// </summary>
        protected override void OnModelChanged()
        {
            this.LoadDataFromModel();
            this.UndoManager.Start();
        }

        /// <summary>
        /// Saves all form data changes into the data source. Executed by the SaveCommand.
        /// </summary>
        protected override void Save()
        {
            this.CheckModelAndDataSource();

            // Copy the data from the view model into the model (manufacturer)
            this.SaveToModel();

            this.DataSourceManager.ManufacturerRepository.Save(this.Model);
            this.DataSourceManager.SaveChanges();

            // reset the form changed flag
            this.IsChanged = false;

            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Cancels all changes. Executed by the CancelCommand.
        /// </summary>
        protected override void Cancel()
        {
            if (!this.CanCancel())
            {
                return;
            }

            if (this.IsChanged)
            {
                MessageDialogResult result = MessageDialogResult.Yes;
                if (!this.IsChild)
                {
                    result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_CancelChanges, MessageDialogType.YesNo);
                }

                if (result != MessageDialogResult.Yes)
                {
                    // Don't cancel the changes and also don't close the view-model.
                    return;
                }
                else
                {
                    // Cancel all changes
                    base.Cancel();
                }
            }

            // Close the view-model when is displayed in a window.
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Called before unloading the view from its parent. Returning false will cancel the view's unloading.
        /// </summary>
        /// <returns>
        /// True if the unloading process should continue and false if it should be canceled.
        /// </returns>
        public override bool OnUnloading()
        {
            // Do nothing if the view-model is read-only, in viewer mode or it was not changed.
            if (this.IsReadOnly || this.IsInViewerMode || !this.IsChanged)
            {
                return true;
            }

            if (this.EditMode == ViewModelEditMode.Create)
            {
                // Ask the user to confirm quitting
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_Quit, MessageDialogType.YesNo);
                if (result != MessageDialogResult.Yes)
                {
                    // The user chose to stay on the screen; return false to stop the screen unloading.
                    return false;
                }
                else
                {
                    this.IsChanged = false;
                }
            }
            else if (this.EditMode == ViewModelEditMode.Edit)
            {
                // Ask the user if he wants to save
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_UnsavedDataOnQuit, MessageDialogType.YesNoCancel);
                if (result == MessageDialogResult.Yes)
                {
                    // The user whishes to save but it is not possible because the input is not valid. Return false to stop the view-model from unloading.
                    if (!this.CanSave())
                    {
                        return false;
                    }

                    this.Save();
                }
                else if (result == MessageDialogResult.No)
                {
                    // The user does not want to save.                    
                    this.IsChanged = false;
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Opens the master data browser to select a manufacturer.
        /// </summary>
        private void BrowseMasterData()
        {
            var browserViewModel = this.compositionContainer.GetExportedValue<MasterDataBrowserViewModel>();
            MasterDataEntitySelectionHandler selectionHandler = (selectedEntity, databaseId) =>
            {
                Manufacturer man = selectedEntity as Manufacturer;
                if (man != null)
                {
                    using (this.UndoManager.StartBatch(includePreviousItem: false, reverseUndoOrder: false, navigateToBatchControls: true, undoEachBatch: true))
                    {
                        this.LoadDataFromModel(man);
                    }
                }
            };
            browserViewModel.MasterDataSelected += selectionHandler;
            browserViewModel.MasterDataType = typeof(Manufacturer);
            this.windowService.ShowViewInDialog(browserViewModel);
            browserViewModel.MasterDataSelected -= selectionHandler;
        }
    }
}