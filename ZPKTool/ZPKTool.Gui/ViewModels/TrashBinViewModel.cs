﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the TrashBinView view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class TrashBinViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The composition container.
        /// </summary>
        private CompositionContainer container;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The messenger used to communicate with other view models.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The list of trash bin items selected in the items list.
        /// </summary>
        private List<TrashBinItem> checkedItems = new List<TrashBinItem>();

        /// <summary>
        /// The trash bin items to be displayed.
        /// </summary>
        private ObservableCollection<TrashBinItem> trashBinItems;

        /// <summary>
        /// The trash bin item currently selected in the items list.
        /// </summary>
        private TrashBinItem selectedItem;

        /// <summary>
        /// The item details control currently displayed in the details area.
        /// </summary>
        private object itemDetails;

        /// <summary>
        /// The PleaseWaitService reference.
        /// </summary>
        private IPleaseWaitService pleaseWaitService;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        /// <summary>
        /// A value indicating whether the delete is in progress or not.
        /// </summary>
        private bool isDeleteInProgress;

        #endregion Attributes

        /// <summary>
        /// Initializes a new instance of the <see cref="TrashBinViewModel"/> class.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="messenger">The messenger.</param>
        /// <param name="pleaseWaitService">The please wait service.</param>
        /// <param name="unitsService">The units service.</param>
        [ImportingConstructor]
        public TrashBinViewModel(
            CompositionContainer container,
            IWindowService windowService,
            IMessenger messenger,
            IPleaseWaitService pleaseWaitService,
            IUnitsService unitsService)
        {
            this.container = container;
            this.windowService = windowService;
            this.messenger = messenger;
            this.pleaseWaitService = pleaseWaitService;
            this.unitsService = unitsService;

            this.TrashBinItems = new ObservableCollection<TrashBinItem>();

            this.RecoverItemCommand = new DelegateCommand<TrashBinItem>(RecoverItem);
            this.RecoverCheckedItemsCommand = new DelegateCommand(RecoverCheckedItems, () => this.checkedItems.Count > 0);
            this.PermanentlyDeleteItemCommand = new DelegateCommand<TrashBinItem>(PermanentlyDeleteItem);
            this.PermanentlyDeleteCheckedItemsCommand = new DelegateCommand(PermanentlyDeleteCheckedItems, () => this.checkedItems.Count > 0);
            this.EmptyTrashBinCommand = new DelegateCommand(EmptyTrashBin, () => this.TrashBinItems.Count > 0);
            this.ViewItemDetailsCommand = new DelegateCommand<TrashBinItem>(DisplayItemDetails, CanViewItemDetails);
            this.CheckItemCommand = new DelegateCommand<CheckBox>(CheckItemHandler);

            LoadTrashBinItems();

            this.messenger.Register<NotificationMessage>(NotificationsHandler);
        }

        #region Properties

        /// <summary>
        /// Gets or sets the trash bin items to display.
        /// </summary>        
        public ObservableCollection<TrashBinItem> TrashBinItems
        {
            get
            {
                return this.trashBinItems;
            }

            set
            {
                if (this.trashBinItems != value)
                {
                    this.trashBinItems = value;
                    OnPropertyChanged("TrashBinItems");
                }
            }
        }

        /// <summary>
        /// Gets or sets the trash bin item currently selected in the items list.
        /// </summary>
        /// <value>The selected item.</value>
        public TrashBinItem SelectedItem
        {
            get
            {
                return this.selectedItem;
            }

            set
            {
                if (this.selectedItem != value)
                {
                    this.selectedItem = value;
                    OnPropertyChanged("SelectedItem");
                    this.ItemDetails = null;
                }
            }
        }

        /// <summary>
        /// Gets or sets the item details control currently displayed in the details area.
        /// </summary>
        /// <value>The item details.</value>
        public object ItemDetails
        {
            get
            {
                return this.itemDetails;
            }

            set
            {
                if (this.itemDetails != value)
                {
                    this.itemDetails = value;
                    OnPropertyChanged("ItemDetails");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the delete is in progress or not.
        /// </summary>
        public bool IsDeleteInProgress
        {
            get
            {
                return this.isDeleteInProgress;
            }

            set
            {
                if (this.isDeleteInProgress != value)
                {
                    this.isDeleteInProgress = value;
                    OnPropertyChanged(() => this.IsDeleteInProgress);
                }
            }
        }

        #endregion Properties

        #region Commands

        /// <summary>
        /// Gets the recover item command.
        /// </summary>
        /// <value>The recover item command.</value>
        public ICommand RecoverItemCommand { get; private set; }

        /// <summary>
        /// Gets the recover checked items command.
        /// </summary>
        public ICommand RecoverCheckedItemsCommand { get; private set; }

        /// <summary>
        /// Gets the permanently delete item command.
        /// </summary>
        public ICommand PermanentlyDeleteItemCommand { get; private set; }

        /// <summary>
        /// Gets the permanently delete checked items command.
        /// </summary>
        public ICommand PermanentlyDeleteCheckedItemsCommand { get; private set; }

        /// <summary>
        /// Gets the empty trash bin command.
        /// </summary>        
        public ICommand EmptyTrashBinCommand { get; private set; }

        /// <summary>
        /// Gets the view item details command.
        /// </summary>        
        public ICommand ViewItemDetailsCommand { get; private set; }

        /// <summary>
        /// Gets the command executed when an item is checked or un-checked in the trash bin items list.
        /// The command's parameter should the CheckBox instance that was checked or un-checked.
        /// </summary>        
        public ICommand CheckItemCommand { get; private set; }

        #endregion Commands

        /// <summary>
        /// Called after the view has been loaded. Usually it performs some initialization that needs the view to be loaded, like
        /// starting to load data from a database.
        /// <para />
        /// During unit tests this method must be manually called because there is no view to call it.
        /// </summary>
        public override void OnLoaded()
        {
            base.OnLoaded();
            this.LoadTrashBinItems();
        }

        /// <summary>
        /// Loads the trash bin items.
        /// </summary>
        private void LoadTrashBinItems()
        {
            try
            {
                TrashManager trashManager = new TrashManager(DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase));
                var items = trashManager.GetAllTrashBinItems();
                this.TrashBinItems = new ObservableCollection<TrashBinItem>(items);
                this.checkedItems.Clear();
            }
            catch (BusinessException ex)
            {
                log.ErrorException("Caught a business exception.", ex);
                this.windowService.MessageDialogService.Show(ex);
            }
        }

        /// <summary>
        /// Recovers the specified item.
        /// </summary>
        /// <param name="item">The item to recover.</param>
        private void RecoverItem(TrashBinItem item)
        {
            if (item != null)
            {
                Recover(new List<TrashBinItem>() { selectedItem });
            }
        }

        /// <summary>
        /// Recovers the items checked in the UI items list.
        /// </summary>
        private void RecoverCheckedItems()
        {
            Recover(this.checkedItems);
        }

        /// <summary>
        /// Permanently deletes the specified item.
        /// </summary>
        /// <param name="item">The item to delete.</param>
        private void PermanentlyDeleteItem(TrashBinItem item)
        {
            if (item != null)
            {
                var message = string.Format(LocalizedResources.Question_Delete_Item, item.EntityName);
                MessageDialogResult result = this.windowService.MessageDialogService.Show(message, MessageDialogType.YesNo);
                if (result != MessageDialogResult.Yes)
                {
                    return;
                }

                PermanentlyDelete(new List<TrashBinItem>() { selectedItem });
            }
        }

        /// <summary>
        /// Permanently deleted the items checked in the UI items list.
        /// </summary>
        private void PermanentlyDeleteCheckedItems()
        {
            MessageDialogResult result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_PermanentlyDeleteSelectedItems, MessageDialogType.YesNo);
            if (result != MessageDialogResult.Yes)
            {
                return;
            }

            PermanentlyDelete(this.checkedItems);
        }

        /// <summary>
        /// Permanently deletes all items in the trash bin.
        /// </summary>
        private void EmptyTrashBin()
        {
            if (this.TrashBinItems.Count == 0)
            {
                return;
            }

            MessageDialogResult result = this.windowService.MessageDialogService.Show(LocalizedResources.TrashBin_EmptyTrash, MessageDialogType.YesNo);
            if (result != MessageDialogResult.Yes)
            {
                return;
            }

            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += (s, e) =>
            {
                this.IsDeleteInProgress = true;

                IDataSourceManager changeSet = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
                TrashManager trashManager = new TrashManager(changeSet);
                trashManager.EmptyTrashBin();
                changeSet.SaveChanges();
            };

            worker.RunWorkerCompleted += (s, e) =>
            {
                if (e.Error != null)
                {
                    log.ErrorException("Caught a business exception.", e.Error);
                    this.windowService.MessageDialogService.Show(e.Error);
                }
                else
                {
                    NotifyItemsPermanentlyDeleted(new List<TrashBinItem>(this.trashBinItems));
                    this.checkedItems.Clear();
                    this.TrashBinItems.Clear();
                }

                this.IsDeleteInProgress = false;
            };

            worker.RunWorkerAsync();
        }

        /// <summary>
        /// Displays the details of a specified item.
        /// </summary>
        /// <param name="item">The item whose details to display.</param>
        private void DisplayItemDetails(TrashBinItem item)
        {
            //// TODO: cache the entities retrieved for display so they are not retrieved each time the user views them because they don't change.
            //// TODO: if the entity is not fount in db show a message

            if (item == null)
            {
                this.ItemDetails = null;
                return;
            }

            object viewControl = null;

            try
            {
                TrashBinItemType itemType = (TrashBinItemType)item.EntityType;
                IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

                switch (itemType)
                {
                    case TrashBinItemType.Project:
                        {
                            ProjectViewModel projectVM = null;
                            Project project = dataContext.ProjectRepository.GetProjectForView(item.EntityGuid);
                            if (project != null)
                            {
                                this.SetEntityCurrencies(project);

                                projectVM = this.container.GetExportedValue<ProjectViewModel>();
                                projectVM.IsReadOnly = true;
                                projectVM.DataSourceManager = dataContext;
                                projectVM.Model = project;
                            }

                            viewControl = projectVM;
                            break;
                        }

                    case TrashBinItemType.Assembly:
                        {
                            AssemblyViewModel assyVM = null;
                            Assembly assy = dataContext.AssemblyRepository.GetAssemblyForView(item.EntityGuid);
                            if (assy != null)
                            {
                                this.SetEntityCurrencies(assy);

                                assyVM = this.container.GetExportedValue<AssemblyViewModel>();
                                assyVM.IsReadOnly = true;
                                assyVM.DataSourceManager = dataContext;
                                assyVM.Model = assy;
                            }

                            viewControl = assyVM;
                            break;
                        }

                    case TrashBinItemType.RawPart:
                    case TrashBinItemType.Part:
                        {
                            PartViewModel partVM = null;
                            Part part = dataContext.PartRepository.GetPartForView(item.EntityGuid);
                            if (part != null)
                            {
                                this.SetEntityCurrencies(part);

                                partVM = this.container.GetExportedValue<PartViewModel>();
                                partVM.IsReadOnly = true;
                                partVM.DataSourceManager = dataContext;
                                partVM.Model = part;
                            }

                            viewControl = partVM;
                            break;
                        }

                    case TrashBinItemType.RawMaterial:
                        {
                            RawMaterialViewModel rawMaterialVM = null;
                            RawMaterial material = dataContext.RawMaterialRepository.GetById(item.EntityGuid);
                            if (material != null)
                            {
                                var parentPart = dataContext.PartRepository.GetParentPart(material);

                                this.SetEntityCurrencies(material);

                                rawMaterialVM = this.container.GetExportedValue<RawMaterialViewModel>();
                                rawMaterialVM.IsReadOnly = true;
                                rawMaterialVM.DataSourceManager = dataContext;
                                if (parentPart != null)
                                {
                                    rawMaterialVM.CostCalculationVersion = parentPart.CalculationVariant;
                                }

                                rawMaterialVM.Model = material;
                            }

                            viewControl = rawMaterialVM;
                            break;
                        }

                    case TrashBinItemType.Commodity:
                        {
                            CommodityViewModel commodityVM = null;
                            Commodity commodity = dataContext.CommodityRepository.GetById(item.EntityGuid);
                            if (commodity != null)
                            {
                                this.SetEntityCurrencies(commodity);

                                commodityVM = this.container.GetExportedValue<CommodityViewModel>();
                                commodityVM.IsReadOnly = true;
                                commodityVM.DataSourceManager = dataContext;
                                commodityVM.Model = commodity;
                            }

                            viewControl = commodityVM;
                            break;
                        }

                    case TrashBinItemType.Machine:
                        {
                            MachineViewModel machineVM = null;
                            Machine machine = dataContext.MachineRepository.GetById(item.EntityGuid);
                            if (machine != null)
                            {
                                this.SetEntityCurrencies(machine);

                                machineVM = this.container.GetExportedValue<MachineViewModel>();
                                machineVM.IsReadOnly = true;
                                machineVM.DataSourceManager = dataContext;
                                machineVM.Model = machine;
                            }

                            viewControl = machineVM;
                            break;
                        }

                    case TrashBinItemType.Consumable:
                        {
                            ConsumableViewModel consumableVM = null;
                            Consumable consumable = dataContext.ConsumableRepository.GetById(item.EntityGuid);
                            if (consumable != null)
                            {
                                this.SetEntityCurrencies(consumable);

                                consumableVM = this.container.GetExportedValue<ConsumableViewModel>();
                                consumableVM.IsReadOnly = true;
                                consumableVM.DataSourceManager = dataContext;
                                consumableVM.Model = consumable;
                            }

                            viewControl = consumableVM;
                            break;
                        }

                    case TrashBinItemType.Die:
                        {
                            DieViewModel dieVM = null;
                            Die die = dataContext.DieRepository.GetById(item.EntityGuid);
                            if (die != null)
                            {
                                this.SetEntityCurrencies(die);

                                dieVM = this.container.GetExportedValue<DieViewModel>();
                                dieVM.IsReadOnly = true;
                                dieVM.DataSourceManager = dataContext;
                                dieVM.Model = die;
                            }

                            viewControl = dieVM;
                            break;
                        }
                }
            }
            catch (BusinessException ex)
            {
                log.ErrorException("Caught a business exception.", ex);
                this.windowService.MessageDialogService.Show(ex);
            }

            this.ItemDetails = viewControl;
        }

        /// <summary>
        /// Determines whether the ViewItemDetails command can execute.
        /// </summary>
        /// <param name="item">The item to check if it can be viewed.</param>
        /// <returns>
        /// true if the command can execute, false if not.
        /// </returns>
        private bool CanViewItemDetails(TrashBinItem item)
        {
            if (item != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Sets into the units service the selected entity's parent project currencies and base currency.
        /// If the parent project has not currency set use the base currencies and the default currency.
        /// </summary>
        /// <param name="entity">The entity.</param>
        private void SetEntityCurrencies(object entity)
        {
            var isBaseCurrencySet = false;
            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            var parentProject = dsm.ProjectRepository.GetParentProject(entity);
            if (parentProject != null)
            {
                var projectCurrencies = dsm.CurrencyRepository.GetProjectCurrencies(parentProject.Guid);
                this.unitsService.Currencies = new Collection<Currency>(projectCurrencies);
                this.unitsService.BaseCurrency = parentProject.BaseCurrency;

                isBaseCurrencySet = true;
            }

            if (!isBaseCurrencySet
                || (isBaseCurrencySet
                    && this.unitsService.BaseCurrency == null))
            {
                var baseCurrencies = dsm.CurrencyRepository.GetBaseCurrencies();

                this.unitsService.Currencies = baseCurrencies;
                this.unitsService.BaseCurrency = baseCurrencies.FirstOrDefault(c => c.IsoCode == Constants.DefaultCurrencyIsoCode);
            }
        }

        /// <summary>
        /// Handles the checking/un-checking of trash bin items in the items list.
        /// </summary>
        /// <param name="source">The source checkbox.</param>
        private void CheckItemHandler(CheckBox source)
        {
            DataGridRow row = UIHelper.FindParent<DataGridRow>(source);
            if (row == null)
            {
                return;
            }

            TrashBinItem item = row.Item as TrashBinItem;
            if (item == null)
            {
                return;
            }

            if (source.IsChecked == true && !this.checkedItems.Contains(item))
            {
                this.checkedItems.Add(item);
            }
            else if (source.IsChecked != true && this.checkedItems.Contains(item))
            {
                this.checkedItems.Remove(item);
            }
        }

        /// <summary>
        /// Recovers the specified items.
        /// </summary>
        /// <param name="items">The items.</param>
        private void Recover(List<TrashBinItem> items)
        {
            MessageDialogResult result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_RecoverItem, MessageDialogType.YesNo);
            if (result != MessageDialogResult.Yes)
            {
                return;
            }

            List<TrashBinItem> recoveredItems = new List<TrashBinItem>();
            List<TrashBinItem> itemsNotFound = new List<TrashBinItem>();
            string waitMessage = items.Count > 1 ? LocalizedResources.General_RecoveryTrashBinItems : LocalizedResources.General_RecoveryTrashBinItem;
            IDataSourceManager changeSet = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            TrashManager trashManager = new TrashManager(changeSet);

            Action<PleaseWaitService.WorkParams> work = (workParams) =>
            {
                foreach (TrashBinItem item in items)
                {
                    try
                    {
                        TrashBinItem recoveredItem = trashManager.RestoreEntity(item);
                        if (recoveredItem == null)
                        {
                            var messageDialogResult = windowService.MessageDialogService.Show(LocalizedResources.TrashBin_RecoverParents, MessageDialogType.YesNo);
                            if (messageDialogResult == MessageDialogResult.Yes)
                            {
                                recoveredItem = trashManager.RestoreEntity(item, true);
                            }
                        }

                        if (recoveredItem != null)
                        {
                            changeSet.SaveChanges();
                            recoveredItems.Add(recoveredItem);
                        }
                    }
                    catch (BusinessException ex)
                    {
                        if (ex.ErrorCode == ErrorCodes.ItemNotFound)
                        {
                            itemsNotFound.Add(item);
                        }
                        else
                        {
                            throw ex;
                        }
                    }
                }
            };

            Action<PleaseWaitService.WorkParams> workCompleted = (workParams) =>
            {
                foreach (TrashBinItem item in itemsNotFound)
                {
                    MessageDialogResult questionResult = this.windowService.MessageDialogService.Show(
                          string.Format(LocalizedResources.Question_DeleteInvalidTrashBinItem, item.EntityName),
                          MessageDialogType.YesNo);
                    if (questionResult == MessageDialogResult.Yes)
                    {
                        TrashBinItem freshItem = changeSet.TrashBinRepository.GetByKey(item.Guid);
                        if (freshItem != null)
                        {
                            changeSet.TrashBinRepository.RemoveAll(freshItem);
                            changeSet.SaveChanges();
                        }
                    }
                }

                // If an error occurred, show the appropriate error message
                if (workParams.Error != null)
                {
                    log.ErrorException("Caught a business exception.", workParams.Error);
                    this.windowService.MessageDialogService.Show(workParams.Error);
                }

                // Send notifications that the items were recovered.
                NotifyItemsRecovery(recoveredItems);

                // Reload the trash bin content because the recover process might have recovered more than the selected items (in the case when the parent
                // of an item selected for recover is deleted, the parent is also recovered)
                LoadTrashBinItems();
            };

            this.pleaseWaitService.Show(waitMessage, work, workCompleted);
        }

        /// <summary>
        /// Sends notifications that the specified items were recovered from the trash bin.
        /// </summary>
        /// <param name="recoveredItems">The recovered items.</param>
        private void NotifyItemsRecovery(List<TrashBinItem> recoveredItems)
        {
            IDataSourceManager dc = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            foreach (TrashBinItem item in recoveredItems)
            {
                object recoveredEntity = null;
                object recoveredEntityParent = null;
                TrashBinItemType itemType = (TrashBinItemType)item.EntityType;
                switch (itemType)
                {
                    case TrashBinItemType.ProjectFolder:
                        var folder = dc.ProjectFolderRepository.GetFolderWithParent(item.EntityGuid);
                        recoveredEntity = folder;
                        recoveredEntityParent = folder != null ? folder.ParentProjectFolder : null;
                        break;

                    case TrashBinItemType.Project:
                        Project project = dc.ProjectRepository.GetProjectForProjectsTree(item.EntityGuid);
                        recoveredEntity = project;
                        recoveredEntityParent = project != null ? project.ProjectFolder : null;
                        break;

                    case TrashBinItemType.Assembly:
                        recoveredEntity = new Assembly() { Guid = item.EntityGuid };
                        recoveredEntityParent = dc.AssemblyRepository.GetParentAssembly(recoveredEntity);
                        if (recoveredEntityParent == null)
                        {
                            recoveredEntityParent = dc.ProjectRepository.GetParentProject(recoveredEntity, true);
                        }

                        break;

                    case TrashBinItemType.Part:
                        recoveredEntity = new Part() { Guid = item.EntityGuid };
                        recoveredEntityParent = dc.AssemblyRepository.GetParentAssembly(recoveredEntity);
                        if (recoveredEntityParent == null)
                        {
                            recoveredEntityParent = dc.ProjectRepository.GetParentProject(recoveredEntity, true);
                            if (recoveredEntityParent == null)
                            {
                                recoveredEntityParent = dc.PartRepository.GetParentPart(recoveredEntity);
                            }
                        }

                        break;

                    case TrashBinItemType.RawPart:
                        recoveredEntity = new RawPart() { Guid = item.EntityGuid };
                        recoveredEntityParent = dc.PartRepository.GetParentPart(recoveredEntity);

                        break;

                    case TrashBinItemType.RawMaterial:
                        recoveredEntity = new RawMaterial() { Guid = item.EntityGuid };
                        recoveredEntityParent = dc.PartRepository.GetParentPart(recoveredEntity);
                        break;

                    case TrashBinItemType.Commodity:
                        recoveredEntity = new Commodity() { Guid = item.EntityGuid };
                        recoveredEntityParent = dc.PartRepository.GetParentPart(recoveredEntity);
                        if (recoveredEntityParent == null)
                        {
                            recoveredEntityParent = dc.ProcessStepRepository.GetParentProcessStep(recoveredEntity);
                        }

                        break;

                    case TrashBinItemType.Consumable:
                        recoveredEntity = new Consumable() { Guid = item.EntityGuid };
                        recoveredEntityParent = dc.ProcessStepRepository.GetParentProcessStep(recoveredEntity);
                        break;

                    case TrashBinItemType.Die:
                        recoveredEntity = new Die() { Guid = item.EntityGuid };
                        recoveredEntityParent = dc.ProcessStepRepository.GetParentProcessStep(recoveredEntity);
                        break;

                    case TrashBinItemType.Machine:
                        recoveredEntity = new Machine() { Guid = item.EntityGuid };
                        recoveredEntityParent = dc.ProcessStepRepository.GetParentProcessStep(recoveredEntity);
                        break;
                }

                if (recoveredEntity != null)
                {
                    EntityChangedMessage message = new EntityChangedMessage(Notification.MyProjectsEntityChanged);
                    message.ChangeType = EntityChangeType.EntityRecovered;
                    message.Entity = recoveredEntity;
                    message.Parent = recoveredEntityParent;
                    this.messenger.Send<EntityChangedMessage>(message);
                }
            }
        }

        /// <summary>
        /// Permanently deletes the specified items.
        /// </summary>
        /// <param name="items">The items.</param>
        private void PermanentlyDelete(List<TrashBinItem> items)
        {
            List<TrashBinItem> deletedItems = new List<TrashBinItem>();

            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += (s, e) =>
            {
                this.IsDeleteInProgress = true;

                IDataSourceManager changeSet = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
                TrashManager trashManager = new TrashManager(changeSet);
                foreach (TrashBinItem item in items)
                {
                    trashManager.PermanentlyDeleteEntity(item);
                    changeSet.SaveChanges();
                    deletedItems.Add(item);
                }
            };

            worker.RunWorkerCompleted += (s, e) =>
           {
               try
               {
                   foreach (TrashBinItem item in deletedItems)
                   {
                       this.checkedItems.Remove(item);
                       this.TrashBinItems.Remove(item);
                   }

                   NotifyItemsPermanentlyDeleted(deletedItems);

                   // Reload the trash bin content because the delete process might have deleted more than the selected items (in the case when the parent
                   // of an item selected for delete is deleted, the parent is also deleted)
                   LoadTrashBinItems();
               }
               catch (Exception ex)
               {
                   log.ErrorException("Caught a business exception.", ex);
                   this.windowService.MessageDialogService.Show(ex);
               }

               this.IsDeleteInProgress = false;
           };

            worker.RunWorkerAsync();
        }

        /// <summary>
        /// Sends notifications for each item which is permanently deleted in order to ensure the change sets refresh in the main tree
        /// </summary>
        /// <param name="deletedItems">The list of deleted items</param>
        private void NotifyItemsPermanentlyDeleted(List<TrashBinItem> deletedItems)
        {
            foreach (TrashBinItem item in deletedItems)
            {
                object deletedEntity = null;
                TrashBinItemType itemType = (TrashBinItemType)item.EntityType;
                switch (itemType)
                {
                    case TrashBinItemType.ProjectFolder:
                        deletedEntity = new ProjectFolder() { Guid = item.EntityGuid };
                        break;
                    case TrashBinItemType.Project:
                        deletedEntity = new Project() { Guid = item.EntityGuid };
                        break;
                    case TrashBinItemType.Assembly:
                        deletedEntity = new Assembly() { Guid = item.EntityGuid };
                        break;
                    case TrashBinItemType.Part:
                        deletedEntity = new Part() { Guid = item.EntityGuid };
                        break;
                    case TrashBinItemType.RawPart:
                        deletedEntity = new RawPart() { Guid = item.EntityGuid };
                        break;
                    case TrashBinItemType.RawMaterial:
                        deletedEntity = new RawMaterial() { Guid = item.EntityGuid };
                        break;
                    case TrashBinItemType.Commodity:
                        deletedEntity = new Commodity() { Guid = item.EntityGuid };
                        break;
                    case TrashBinItemType.Consumable:
                        deletedEntity = new Consumable() { Guid = item.EntityGuid };
                        break;
                    case TrashBinItemType.Machine:
                        deletedEntity = new Machine() { Guid = item.EntityGuid };
                        break;
                    case TrashBinItemType.Die:
                        deletedEntity = new Die() { Guid = item.EntityGuid };
                        break;
                }

                if (deletedEntity != null)
                {
                    EntityChangedMessage message = new EntityChangedMessage(Notification.MyProjectsEntityChanged);
                    message.ChangeType = EntityChangeType.EntityPermanentlyDeleted;
                    message.Entity = deletedEntity;
                    this.messenger.Send<EntityChangedMessage>(message);
                }
            }
        }

        /// <summary>
        /// Handles all received messages of type NotificationMessage.
        /// </summary>
        /// <param name="message">The message.</param>
        private void NotificationsHandler(NotificationMessage message)
        {
            if (message.Notification == Notifications.Notification.TrashBinEmptied)
            {
                this.TrashBinItems.Clear();
            }
        }
    }
}