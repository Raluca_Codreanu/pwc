﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Windows.Input;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.Gui.Resources;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for the <see cref="ChangePasswordView"/>.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ChangePasswordViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The current password inputted by the user.
        /// </summary>
        private string oldPassword;

        /// <summary>
        /// The new password inputted by the user.
        /// </summary>
        private string newPassword;

        /// <summary>
        /// The new password inputted by the user for confirmation.
        /// </summary>
        private string confirmNewPassword;

        /// <summary>
        /// A value indicating whether to OnUnloading event should be handled or not.
        /// </summary>
        private bool handleOnUnloading;

        /// <summary>
        /// A value that indicates the password validation result.
        /// </summary>
        private bool? isPasswordValid;

        /// <summary>
        /// A value indicating wheter the user must chnage its password.
        /// </summary>
        private bool isPasswordChangeMandatory;

        /// <summary>
        /// A value that indicates that the passwords match.
        /// </summary>
        private bool? passwordsMatch;

        #endregion Attributes

        /// <summary>
        /// Initializes a new instance of the <see cref="ChangePasswordViewModel"/> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        [ImportingConstructor]
        public ChangePasswordViewModel(IWindowService windowService)
        {
            Argument.IsNotNull("windowService", windowService);

            this.windowService = windowService;

            this.ChangePasswordCommand = new DelegateCommand(ChangePassword, CanChangePassword);
            this.handleOnUnloading = true;
        }

        #region Properties

        /// <summary>
        /// Gets or sets the current password inputted by the user.
        /// </summary>
        [Required(ErrorMessageResourceName = "General_RequiredField", ErrorMessageResourceType = typeof(LocalizedResources))]
        public string OldPassword
        {
            get { return this.oldPassword; }
            set { this.SetProperty(ref this.oldPassword, value, () => this.OldPassword); }
        }

        /// <summary>
        /// Gets or sets the new password inputted by the user.
        /// </summary>
        [Required(ErrorMessageResourceName = "General_RequiredField", ErrorMessageResourceType = typeof(LocalizedResources))]
        public string NewPassword
        {
            get
            {
                return this.newPassword;
            }

            set
            {
                this.SetProperty(ref this.newPassword, value, () => this.NewPassword);
                SetPasswordsMatchValue();
            }
        }

        /// <summary>
        /// Gets or sets the new password inputted by the user for confirmation.
        /// </summary>
        [Required(ErrorMessageResourceName = "General_RequiredField", ErrorMessageResourceType = typeof(LocalizedResources))]
        public string ConfirmNewPassword
        {
            get
            {
                return this.confirmNewPassword;
            }

            set
            {
                this.SetProperty(ref this.confirmNewPassword, value, () => this.ConfirmNewPassword);
                SetPasswordsMatchValue();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the password validation result.
        /// </summary> 
        [Required(ErrorMessageResourceName = "General_RequiredField", ErrorMessageResourceType = typeof(LocalizedResources))]
        public bool? IsPasswordValid
        {
            get { return this.isPasswordValid; }
            set { this.SetProperty(ref this.isPasswordValid, value, () => this.IsPasswordValid); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this user must change its password.
        /// </summary>
        public bool IsPasswordChangeMandatory
        {
            get { return this.isPasswordChangeMandatory; }
            set { this.SetProperty(ref this.isPasswordChangeMandatory, value, () => this.IsPasswordChangeMandatory); }
        }

        /// <summary>
        /// Gets or sets a value indicanting whether the passwords match.
        /// </summary>
        public bool? PasswordsMatch
        {
            get { return this.passwordsMatch; }
            set { this.SetProperty(ref this.passwordsMatch, value, () => this.PasswordsMatch); }
        }

        #endregion Properties

        #region Commands

        /// <summary>
        /// Gets the change password command.
        /// </summary>        
        public ICommand ChangePasswordCommand { get; private set; }

        #endregion Commands

        /// <summary>
        /// Determines whether the ChangePassword command can be executed.
        /// </summary>
        /// <returns>true if the command can be executed; otherwise, false.</returns>
        private bool CanChangePassword()
        {
            if (string.IsNullOrWhiteSpace(this.OldPassword) || !PasswordsMatch.GetValueOrDefault() || !this.IsPasswordValid.Value)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Sets the passwords match value.
        /// </summary>
        private void SetPasswordsMatchValue()
        {
            if (string.IsNullOrWhiteSpace(this.ConfirmNewPassword))
            {
                this.PasswordsMatch = null;
            }
            else if (!this.ConfirmNewPassword.Equals(this.NewPassword, StringComparison.Ordinal))
            {
                this.PasswordsMatch = false;
            }
            else
            {
                this.PasswordsMatch = true;
            }
        }

        /// <summary>
        /// Starts the change password process.
        /// </summary>
        private void ChangePassword()
        {
            if (this.NewPassword == this.ConfirmNewPassword)
            {
                User currentUser = SecurityManager.Instance.CurrentUser;
                SecurityManager.Instance.ValidatePassword(this.ConfirmNewPassword);
                if (currentUser != null)
                {
                    SecurityManager.Instance.ChangePassword(currentUser.Guid, this.OldPassword, this.NewPassword);
                    this.windowService.MessageDialogService.Show(LocalizedResources.Info_PasswordChanged, MessageDialogType.Info);
                    SecurityManager.Instance.CurrentUser.IsPasswordSetByUser = true;

                    // Close the view-model when is displayed in a window.
                    handleOnUnloading = false;
                    this.windowService.CloseViewWindow(this);
                }
            }
            else
            {
                this.windowService.MessageDialogService.Show(LocalizedResources.ChangePassword_NewPasswordDoesntMatch, MessageDialogType.Error);
            }
        }

        /// <summary>
        /// Cancels all changes. Executed by the CancelCommand.
        /// </summary>
        protected override void Cancel()
        {
            if (!string.IsNullOrWhiteSpace(this.OldPassword) || !string.IsNullOrWhiteSpace(this.NewPassword) ||
                !string.IsNullOrWhiteSpace(this.ConfirmNewPassword))
            {
                MessageDialogResult result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_CancelChanges, MessageDialogType.YesNo);
                if (result == MessageDialogResult.No)
                {
                    // Don't cancel the changes and also don't close the view-model.
                    return;
                }
                else
                {
                    // Cancel all changes and set unload to false, so that the unloading process will be stopped. 
                    base.Cancel();
                    this.handleOnUnloading = false;
                }
            }

            // Close the view-model when is displayed in a window.
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Called before unloading the view from its parent. Returning false will cancel the view's unloading.
        /// </summary>
        /// <returns>
        /// True if the unloading process should continue and false if it should be canceled.
        /// </returns>
        public override bool OnUnloading()
        {
            if (handleOnUnloading == true)
            {
                if (!string.IsNullOrWhiteSpace(this.OldPassword) || !string.IsNullOrWhiteSpace(this.NewPassword) ||
                    !string.IsNullOrWhiteSpace(this.ConfirmNewPassword))
                {
                    MessageDialogResult result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_Quit, MessageDialogType.YesNo);
                    if (result == MessageDialogResult.No)
                    {
                        // Don't cancel the changes and also don't close the view-model.
                        return false;
                    }
                    else
                    {
                        // Cancel all changes
                        base.OnUnloading();
                    }
                }
            }

            return true;
        }
    }
}