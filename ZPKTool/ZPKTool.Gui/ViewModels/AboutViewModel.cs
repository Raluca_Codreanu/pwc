﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Input;
using ZPKTool.Gui.Resources;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view model for <see cref="AboutView"/>.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class AboutViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The application version.
        /// </summary>
        private string appVersion;

        /// <summary>
        /// The local database version.
        /// </summary>
        private string localDbVersion;

        /// <summary>
        /// The application copyright
        /// </summary>
        private string appCopyright;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AboutViewModel"/> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        [ImportingConstructor]
        public AboutViewModel(IWindowService windowService)
        {
            Argument.IsNotNull("windowService", windowService);
            this.windowService = windowService;

            this.NavigateToFullLicenseCommand = new DelegateCommand(this.NavigateToFullLicense);

            this.InitializeDetails();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the application version.
        /// </summary>
        public string AppVersion
        {
            get { return this.appVersion; }
            set { this.SetProperty(ref this.appVersion, value, () => this.AppVersion); }
        }

        /// <summary>
        /// Gets or sets the local database version.
        /// </summary>
        public string LocalDbVersion
        {
            get { return this.localDbVersion; }
            set { this.SetProperty(ref this.localDbVersion, value, () => this.LocalDbVersion); }
        }

        /// <summary>
        /// Gets or sets the application copyright.
        /// </summary>
        public string AppCopyright
        {
            get { return this.appCopyright; }
            set { this.SetProperty(ref this.appCopyright, value, () => this.AppCopyright); }
        }

        #endregion

        #region Commands

        /// <summary>
        /// Gets the command for navigating to full license folder.
        /// </summary>        
        public ICommand NavigateToFullLicenseCommand { get; private set; }

        #endregion Commands

        #region Implementation

        /// <summary>
        /// Initializes the details related to application for displaying in the window.
        /// </summary>
        private void InitializeDetails()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            object[] attrs = assembly.GetCustomAttributes(false);

            this.AppVersion = LocalizedResources.General_Version + " " + assembly.GetName().Version.ToString();

            this.LocalDbVersion = string.Format(
                "{0} {1}",
                LocalizedResources.General_LocalDbVersion,
                Common.Constants.DataBaseVersion.ToString(System.Globalization.CultureInfo.InvariantCulture));

            string copyright = string.Empty;
            AssemblyCopyrightAttribute copyrightAttr = attrs.OfType<AssemblyCopyrightAttribute>().FirstOrDefault();
            if (copyrightAttr != null)
            {
                copyright = copyrightAttr.Copyright;
            }

            this.AppCopyright = string.Format(LocalizedResources.AboutWindow_Copyright, copyright);
        }

        /// <summary>
        /// Navigates to full license and opens the containing folder.
        /// </summary>
        private void NavigateToFullLicense()
        {
            try
            {
                Assembly assembly = Assembly.GetExecutingAssembly();
                string appFolderPath = System.IO.Path.GetDirectoryName(assembly.Location);
                string licensesFolderPath = System.IO.Path.Combine(appFolderPath, "Licenses");
                System.Diagnostics.Process.Start(licensesFolderPath);
            }
            catch (Exception ex)
            {
                log.Error("Could not open the folder with application licenses.", ex);
            }
        }

        /// <summary>
        /// Closes this instance.
        /// </summary>
        protected override void Close()
        {
            this.windowService.CloseViewWindow(this);
        }

        #endregion
    }
}
