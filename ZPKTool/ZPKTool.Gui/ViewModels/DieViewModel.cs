﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Input;
using ZPKTool.Business;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view model for dies
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class DieViewModel : ViewModel<Die, IDataSourceManager>
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The composition container.
        /// </summary>
        private CompositionContainer container;

        /// <summary>
        /// The model browser helper service.
        /// </summary>
        private IModelBrowserHelperService modelBrowserHelperService;

        /// <summary>
        /// The messenger.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The die wear value
        /// </summary>
        private decimal? dieWearCost;

        /// <summary>
        /// the die wear in percentage
        /// </summary>
        private decimal? dieWearPercentage;

        /// <summary>
        /// The die maintenance value
        /// </summary>
        private decimal? dieMaintenanceCost;

        /// <summary>
        /// The die maintenance value in percent
        /// </summary>
        private decimal? dieMaintenancePercentage;

        /// <summary>
        /// The die total cost
        /// </summary>
        private decimal dieTotalCost;

        /// <summary>
        /// The die media view model
        /// </summary>
        private MediaViewModel mediaViewModel;

        /// <summary>
        /// The die manufacturer view model
        /// </summary>
        private ManufacturerViewModel manufacturerViewModel;

        /// <summary>
        /// The parent of the die model
        /// </summary>
        private object dieParent;

        /// <summary>
        /// The calculated necessary dies per life time
        /// </summary>
        private int calcDiesPerTime;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        /// <summary>
        /// The UnitsAdapter handler, used to perform certain operations when some UnitsAdapter properties are updated.
        /// </summary>
        private UnitsAdapterUpdateHandler unitsAdapterHandler;

        /// <summary>
        /// A bool value indicating whether the model is loading or not.
        /// </summary>
        private bool isModelLoading = false;

        #endregion Attributes

        /// <summary>
        /// Initializes a new instance of the <see cref="DieViewModel" /> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        /// <param name="manufacturerViewModel">The manufacturer view model</param>
        /// <param name="modelBrowserHelperService">The model browser helper service.</param>
        /// <param name="costRecalculationCloneManager">The cost recalculation clone manager.</param>
        /// <param name="mediaViewModel">The media view model</param>
        /// <param name="container">The composite container</param>
        /// <param name="unitsService">The units service.</param>
        /// <param name="messenger">The messenger.</param>
        [ImportingConstructor]
        public DieViewModel(
            IWindowService windowService,
            ManufacturerViewModel manufacturerViewModel,
            IModelBrowserHelperService modelBrowserHelperService,
            ICostRecalculationCloneManager costRecalculationCloneManager,
            MediaViewModel mediaViewModel,
            CompositionContainer container,
            IUnitsService unitsService,
            IMessenger messenger)
        {
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("manufacturerViewModel", manufacturerViewModel);
            Argument.IsNotNull("mediaViewModel", mediaViewModel);
            Argument.IsNotNull("unitsService", unitsService);
            Argument.IsNotNull("costRecalculationCloneManager", costRecalculationCloneManager);

            this.container = container;
            this.messenger = messenger;
            this.modelBrowserHelperService = modelBrowserHelperService;
            this.windowService = windowService;
            this.MediaViewModel = mediaViewModel;
            this.ManufacturerViewModel = manufacturerViewModel;
            this.MediaViewModel.Mode = MediaControlMode.SingleImage;
            this.MediaViewModel.IsChild = true;
            this.unitsService = unitsService;
            this.CloneManager = costRecalculationCloneManager;

            // Initialize the manufacturer view-model
            this.ManufacturerViewModel.ShowSaveControls = false;
            this.ManufacturerViewModel.IsChild = true;

            this.InitializeCommands();
            this.InitializePropertyValueChangedHandlers();
            this.InitializeUndoManager();
        }

        #region Commands

        /// <summary>
        /// Gets the Save all command
        /// </summary>
        public CompositeCommand SaveAllCommand { get; private set; }

        /// <summary>
        /// Gets the Cancel all command
        /// </summary>
        public CompositeCommand CancelAllCommand { get; private set; }

        /// <summary>
        /// Gets the command for the browse master data
        /// </summary>
        public ICommand BrowseCommand { get; private set; }

        #endregion Commands

        #region Model related properties

        /// <summary>
        /// Gets  the die description
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Name", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Name")]
        public DataProperty<string> Name { get; private set; }

        /// <summary>
        /// Gets the die type
        /// </summary>
        [UndoableProperty]
        [ExposesModelProperty("Type")]
        public DataProperty<DieType?> DieType { get; private set; }

        /// <summary>
        /// Gets the die investment value
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Investment", AffectsCost = true)]
        public DataProperty<decimal?> DieInvestment { get; private set; }

        /// <summary>
        /// Gets the reusable investment value
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("ReusableInvest", AffectsCost = true)]
        public DataProperty<decimal?> DieReusableInvest { get; private set; }

        /// <summary>
        /// Gets the die wear flag value
        /// </summary>
        [UndoableProperty]
        [ExposesModelProperty("IsWearInPercentage", AffectsCost = true)]
        public DataProperty<bool> DieIsWearInPercentage { get; private set; }

        /// <summary>
        /// Gets the die maintenance flag value
        /// </summary>
        [UndoableProperty]
        [ExposesModelProperty("IsMaintenanceInPercentage", AffectsCost = true)]
        public DataProperty<bool> DieIsMaintenanceInPercentage { get; private set; }

        /// <summary>
        /// Gets the all dies paid by customer flag value.
        /// </summary>
        [UndoableProperty]
        [ExposesModelProperty("AreAllPaidByCustomer", AffectsCost = true)]
        public DataProperty<bool?> DiesAreAllPaidByCustomer { get; private set; }

        /// <summary>
        /// Gets the die life time
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("LifeTime", AffectsCost = true)]
        public DataProperty<decimal?> DieLifeTime { get; private set; }

        /// <summary>
        /// Gets the die ratio of allocation
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("AllocationRatio", AffectsCost = true)]
        public DataProperty<decimal?> DieRatio { get; private set; }

        /// <summary>
        /// Gets the number on dies paid by customer
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("DiesetsNumberPaidByCustomer", AffectsCost = true)]
        public DataProperty<int?> DiesPaidByCustomer { get; private set; }

        /// <summary>
        /// Gets the die cost allocation based on number of parts per life time
        /// </summary>
        [UndoableProperty]
        [ExposesModelProperty("CostAllocationBasedOnPartsPerLifeTime", AffectsCost = true)]
        public DataProperty<bool> DieCostAllocationPartsPerLifeTime { get; private set; }

        /// <summary>
        /// Gets the die cost allocation based on number of parts
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("CostAllocationNumberOfParts", AffectsCost = true)]
        public DataProperty<int?> DieCostAllocationNumberOfParts { get; private set; }

        /// <summary>
        /// Gets the die remark
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Remark")]
        public DataProperty<string> DieRemark { get; private set; }

        #endregion

        #region Other Properties

        /// <summary>
        /// Gets or sets the calculated necessary dies per life time
        /// </summary>
        public int CalcDiesPerTime
        {
            get { return this.calcDiesPerTime; }
            set { this.SetProperty(ref this.calcDiesPerTime, value, () => this.CalcDiesPerTime, () => UpdatePaidByCustomer()); }
        }

        /// <summary>
        /// Gets or sets the die wear value
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? DieWearCost
        {
            get { return this.dieWearCost; }
            set { this.SetProperty(ref this.dieWearCost, value, () => this.DieWearCost, () => this.OnDieWearValueChanged(value)); }
        }

        /// <summary>
        /// Gets or sets the die wear value in percent
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? DieWearPercentage
        {
            get { return this.dieWearPercentage; }
            set { this.SetProperty(ref this.dieWearPercentage, value, () => this.DieWearPercentage, () => this.OnDieWearValueChanged(value)); }
        }

        /// <summary>
        /// Gets or sets the die maintenance value
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? DieMaintenanceCost
        {
            get { return this.dieMaintenanceCost; }
            set { this.SetProperty(ref this.dieMaintenanceCost, value, () => this.DieMaintenanceCost, () => this.OnDieMaintenanceValueChanged(value)); }
        }

        /// <summary>
        /// Gets or sets the die maintenance value in percent
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? DieMaintenancePercentage
        {
            get { return this.dieMaintenancePercentage; }
            set { this.SetProperty(ref this.dieMaintenancePercentage, value, () => this.DieMaintenancePercentage, () => this.OnDieMaintenanceValueChanged(value)); }
        }

        /// <summary>
        /// Gets or sets the die parent
        /// </summary>
        public object DieParent
        {
            get { return this.dieParent; }
            set { this.SetProperty(ref this.dieParent, value, () => this.DieParent); }
        }

        /// <summary>
        /// Gets or sets the die total cost
        /// </summary>
        public decimal DieTotalCost
        {
            get { return this.dieTotalCost; }
            set { this.SetProperty(ref this.dieTotalCost, value, () => this.DieTotalCost); }
        }

        /// <summary>
        /// Gets or sets the die media view model
        /// </summary>
        public MediaViewModel MediaViewModel
        {
            get { return this.mediaViewModel; }
            set { this.SetProperty(ref this.mediaViewModel, value, () => this.MediaViewModel); }
        }

        /// <summary>
        /// Gets or sets the manufacturer view model
        /// </summary>
        public ManufacturerViewModel ManufacturerViewModel
        {
            get { return this.manufacturerViewModel; }
            set { this.SetProperty(ref this.manufacturerViewModel, value, () => this.ManufacturerViewModel); }
        }

        /// <summary>
        /// Gets or sets the project to which the Model belongs.
        /// This property is needed during cost calculations so it must be set when editing (it can be omitted when creating).
        /// </summary>
        public Project ParentProject { get; set; }

        /// <summary>
        /// Gets or sets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter { get; set; }

        /// <summary>
        /// Gets or sets the message token to be used when sending messages with token 
        /// </summary>
        public string MessageToken { get; set; }

        #endregion

        /// <summary>
        /// Initializes the commands of this instance.
        /// </summary>
        private void InitializeCommands()
        {
            this.BrowseCommand = new DelegateCommand(this.BrowseDieMasterData);

            this.SaveAllCommand = new CompositeCommand();
            this.SaveAllCommand.RegisterCommand(this.MediaViewModel.SaveToModelCommand);
            this.SaveAllCommand.RegisterCommand(this.ManufacturerViewModel.SaveToModelCommand);

            this.CancelAllCommand = new CompositeCommand();
            this.CancelAllCommand.RegisterCommand(this.MediaViewModel.CancelCommand);
            this.CancelAllCommand.RegisterCommand(this.ManufacturerViewModel.CancelCommand);
        }

        /// <summary>
        /// Initializes the view-model for the creation of a new die
        /// The ModelDataContext property must be set to a valid value before this call.
        /// </summary>
        /// <param name="isMasterData">If set to true the die will be created in master data; in this case the parent should be set to null.</param>
        /// <param name="parent">
        /// The parent object of the die that will be created. If this is set, the value of the <paramref name="isMasterData"/> argument is ignored and
        /// the parent's IsMasterData flag is used instead.
        /// </param>
        /// <remarks>
        /// This is a helper method for populating the model with default data for creation. It is not mandatory to use it; you can set the model to a new
        /// instance set up however you want.
        /// </remarks>
        /// <exception cref="ArgumentException">
        /// <paramref name="parent"/> is null and <paramref name="isMasterData"/> is false, or the type of<paramref name="parent"/> is not supported.
        /// </exception>
        /// <exception cref="InvalidOperationException">The provided <paramref name="parent"/> object type is not supported.</exception>
        public void InitializeForCreation(bool isMasterData, object parent)
        {
            this.CheckDataSource();
            this.EditMode = ViewModelEditMode.Create;

            Die newDie = new Die();

            if (!isMasterData)
            {
                if (parent == null)
                {
                    throw new ArgumentException("A non master data die cannot be created without a parent", "parent");
                }

                var stepParent = parent as ProcessStep;
                if (stepParent != null)
                {
                    isMasterData = stepParent.IsMasterData;
                    newDie.ProcessStep = stepParent;
                }
            }

            newDie.Manufacturer = new Manufacturer();

            newDie.Investment = 0m;
            newDie.ReusableInvest = 0m;
            newDie.IsWearInPercentage = false;
            newDie.Wear = 0m;
            newDie.IsMaintenanceInPercentage = false;
            newDie.Maintenance = 0m;
            newDie.LifeTime = 0m;
            newDie.AllocationRatio = 1m;
            newDie.DiesetsNumberPaidByCustomer = 0;
            newDie.CostAllocationBasedOnPartsPerLifeTime = true;
            newDie.CostAllocationNumberOfParts = 0;
            newDie.Remark = string.Empty;
            newDie.AreAllPaidByCustomer = false;

            // Set the master data flag and the owner at the end so they are applied to all sub-objects.
            newDie.SetIsMasterData(isMasterData);
            if (!newDie.IsMasterData)
            {
                User owner = this.DataSourceManager.UserRepository.GetById(SecurityManager.Instance.CurrentUser.Guid, false);
                newDie.SetOwner(owner);
            }

            this.Model = newDie;
        }

        /// <summary>
        /// Initialize the undo manager.
        /// </summary>
        private void InitializeUndoManager()
        {
            this.ManufacturerViewModel.UndoManager = this.UndoManager;
            this.MediaViewModel.UndoManager = this.UndoManager;
        }

        #region Property change handlers

        /// <summary>
        /// Called when the Model has changed.
        /// </summary>
        protected override void OnModelChanged()
        {
            this.CheckDataSource();
            base.OnModelChanged();

            if (this.IsInViewerMode)
            {
                this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(null);
            }

            this.ManufacturerViewModel.DataSourceManager = this.DataSourceManager;
            if (this.Model.Manufacturer != null)
            {
                this.ManufacturerViewModel.Model = this.Model.Manufacturer;
            }
            else
            {
                this.ManufacturerViewModel.Model = new Manufacturer();
            }

            this.MediaViewModel.DataSourceManager = this.DataSourceManager;
            this.MediaViewModel.Model = this.Model;

            this.UpdateDieCost();
            this.UndoManager.Start();

            if (!this.IsChild)
            {
                this.CloneManager.Clone(this);
            }
        }

        /// <summary>
        /// Called when the DataSourceManager changed.
        /// </summary>
        protected override void OnDataSourceManagerChanged()
        {
            base.OnDataSourceManagerChanged();

            this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(this.DataSourceManager);
            this.unitsAdapterHandler = new UnitsAdapterUpdateHandler(this.MeasurementUnitsAdapter);
            this.unitsAdapterHandler.PauseUndoOnUnitsAdapterUpdate(this.UndoManager);
        }

        /// <summary>
        /// Add Events to Fields for calculating the costs
        /// </summary>
        private void InitializePropertyValueChangedHandlers()
        {
            this.DieInvestment.ValueChanged += (s, e) => this.UpdateDieCost();
            this.DieReusableInvest.ValueChanged += (s, e) => this.UpdateDieCost();
            this.DieLifeTime.ValueChanged += (s, e) => this.UpdateDieCost();
            this.DieRatio.ValueChanged += (s, e) => this.UpdateDieCost();
            this.DieCostAllocationNumberOfParts.ValueChanged += (s, e) => this.UpdateDieCost();
            this.DieCostAllocationPartsPerLifeTime.ValueChanged += (s, e) => this.UpdateDieCost();
            this.DiesPaidByCustomer.ValueChanged += (s, e) => UpdateDieCost();
            this.DieIsMaintenanceInPercentage.ValueChanged += (s, e) => this.UpdateDieCost();
            this.DieIsWearInPercentage.ValueChanged += (s, e) => this.UpdateDieCost();
        }

        /// <summary>
        /// Loads the data from model
        /// </summary>
        /// <param name="model">The die model</param>
        public override void LoadDataFromModel(Die model)
        {
            try
            {
                this.isModelLoading = true;
                base.LoadDataFromModel(model);

                // load the rest of the fields
                if (model.Wear != null)
                {
                    if (model.IsWearInPercentage.Value)
                    {
                        this.DieWearPercentage = model.Wear;
                        this.DieWearCost = 0;
                    }
                    else
                    {
                        this.DieWearCost = model.Wear;
                        this.DieWearPercentage = 0;
                    }
                }
                else
                {
                    this.DieWearPercentage = 0;
                    this.DieWearCost = 0;
                }

                if (model.Maintenance != null)
                {
                    if (model.IsMaintenanceInPercentage.Value)
                    {
                        this.DieMaintenancePercentage = model.Maintenance;
                        this.DieMaintenanceCost = 0;
                    }
                    else
                    {
                        this.DieMaintenanceCost = model.Maintenance;
                        this.DieMaintenancePercentage = 0;
                    }
                }
                else
                {
                    this.DieMaintenancePercentage = 0;
                    this.DieMaintenanceCost = 0;
                }

                // The property was not previously saved in the database so if it does not have a value we set it. 
                if (!this.DiesAreAllPaidByCustomer.Value.HasValue)
                {
                    if (this.CalcDiesPerTime != 0 && this.CalcDiesPerTime == this.DiesPaidByCustomer.Value)
                    {
                        this.DiesAreAllPaidByCustomer.Value = true;
                    }
                    else
                    {
                        this.DiesAreAllPaidByCustomer.Value = false;
                    }
                }
            }
            finally
            {
                this.isModelLoading = false;
            }
        }

        /// <summary>
        /// Opens the master data browser to select a Die.
        /// </summary>
        private void BrowseDieMasterData()
        {
            var browserViewModel = this.container.GetExportedValue<MasterDataBrowserViewModel>();
            MasterDataEntitySelectionHandler selectionHandler = (selectedEntity, databaseId) =>
            {
                Die dieFromMasterData = selectedEntity as Die;
                if (dieFromMasterData == null)
                {
                    return;
                }

                this.StopRecalculationNotifications();
                using (this.UndoManager.StartBatch(undoEachBatch: true))
                {
                    // Retrieve the media of the die master data.
                    var dataManager = DataAccessFactory.CreateDataSourceManager(databaseId);
                    MediaManager mediaManager = new MediaManager(dataManager);
                    var mediaList = new List<Media>();
                    var media = mediaManager.GetPictureOrVideo(dieFromMasterData);
                    if (media != null && (MediaType)media.Type != MediaType.Document)
                    {
                        mediaList.Add(media.Copy());
                    }

                    // Load the die master data media in Media view-model.
                    this.MediaViewModel.LoadMedia(mediaList);

                    // Convert the master data entity currency values.
                    var currencies = dataManager.CurrencyRepository.GetBaseCurrencies();
                    var baseCurrencyFromMD = currencies.FirstOrDefault(c => c.IsSameAs(this.MeasurementUnitsAdapter.BaseCurrency));
                    CurrencyConversionManager.ConvertObject(dieFromMasterData, baseCurrencyFromMD, CurrencyConversionManager.DefaultBaseCurrency);

                    this.LoadDataFromModel(dieFromMasterData);

                    if (dieFromMasterData.Manufacturer != null)
                    {
                        this.ManufacturerViewModel.LoadDataFromModel(dieFromMasterData.Manufacturer);
                        this.Model.Manufacturer = this.ManufacturerViewModel.Model;
                    }
                    else
                    {
                        this.ManufacturerViewModel.LoadDataFromModel(new Manufacturer());
                    }
                }

                this.ResumeRecalculationNotifications(true);
            };

            browserViewModel.MasterDataSelected += selectionHandler;
            browserViewModel.MasterDataType = typeof(Die);
            this.windowService.ShowViewInDialog(browserViewModel);
            browserViewModel.MasterDataSelected -= selectionHandler;
        }

        /// <summary>
        /// Called when the DieWearCost property or the DieWearPercentage property has changed.
        /// </summary>
        /// <param name="wearValue">The new Die Wear value.</param>
        private void OnDieWearValueChanged(decimal? wearValue)
        {
            if (!this.isModelLoading && this.ModelClone != null)
            {
                this.ModelClone.Wear = wearValue;
                this.RefreshCalculation();
            }

            this.UpdatePaidByCustomer();
        }

        /// <summary>
        /// Called when the DieMaintenanceCost property or the DieMaintenancePercentage property has changed.
        /// </summary>
        /// <param name="maintenanceValue">The new Die Maintenance value.</param>
        private void OnDieMaintenanceValueChanged(decimal? maintenanceValue)
        {
            if (!this.isModelLoading && this.ModelClone != null)
            {
                this.ModelClone.Maintenance = maintenanceValue;
                this.RefreshCalculation();
            }

            this.UpdatePaidByCustomer();
        }

        /// <summary>
        /// Updates The Dies Paid By Customer
        /// And calculates the cost
        /// </summary>
        private void UpdatePaidByCustomer()
        {
            using (this.UndoManager.StartBatch(includePreviousItem: true, navigateToBatchControls: true))
            {
                if (DiesAreAllPaidByCustomer.Value.Value)
                {
                    this.DiesPaidByCustomer.Value = this.CalcDiesPerTime;
                }

                // After view is loaded any changes on the properties that are not directly mapped to the model
                // will mark the view model as changed
                if (this.IsLoaded)
                {
                    this.IsChanged = true;
                }
            }

            // update the die cost after change
            this.UpdateDieCost();
        }

        #endregion Property change handlers

        #region Cost handling

        /// <summary>
        /// Updates the die cost.
        /// </summary>
        private void UpdateDieCost()
        {
            if (this.DieParent == null || this.isModelLoading)
            {
                return;
            }

            using (this.UndoManager.Pause())
            {
                Die updatedDie = new Die();
                updatedDie.Investment = this.DieInvestment.Value;
                updatedDie.ReusableInvest = this.DieReusableInvest.Value;

                updatedDie.IsWearInPercentage = this.DieIsWearInPercentage.Value;
                updatedDie.Wear = this.DieIsWearInPercentage.Value ? this.DieWearPercentage : this.DieWearCost;

                updatedDie.IsMaintenanceInPercentage = this.DieIsMaintenanceInPercentage.Value;
                updatedDie.Maintenance = this.DieIsMaintenanceInPercentage.Value ? this.DieMaintenancePercentage : this.DieMaintenanceCost;

                updatedDie.LifeTime = this.DieLifeTime.Value;
                updatedDie.AllocationRatio = this.DieRatio.Value;
                updatedDie.DiesetsNumberPaidByCustomer = this.DiesPaidByCustomer.Value;
                updatedDie.AreAllPaidByCustomer = this.DiesAreAllPaidByCustomer.Value;
                updatedDie.CostAllocationBasedOnPartsPerLifeTime = this.DieCostAllocationPartsPerLifeTime.Value;
                updatedDie.CostAllocationNumberOfParts = this.DieCostAllocationNumberOfParts.Value;

                decimal parentLifeCycleProductionQuantity = 0m;
                ICostCalculator costCalculator = null;
                if (this.DieParent is Part)
                {
                    Part part = (Part)this.DieParent;
                    costCalculator = CostCalculatorFactory.GetCalculator(part.CalculationVariant);
                    parentLifeCycleProductionQuantity = costCalculator.CalculateNetLifetimeProductionQuantity(part);
                }
                else if (this.DieParent is Assembly)
                {
                    Assembly assy = (Assembly)this.DieParent;
                    costCalculator = CostCalculatorFactory.GetCalculator(assy.CalculationVariant);
                    parentLifeCycleProductionQuantity = costCalculator.CalculateNetLifetimeProductionQuantity(assy);
                }
                else
                {
                    throw new InvalidOperationException(string.Format("A die parent of type '{0}' is not handled.", DieParent.GetType().Name));
                }

                var calculationParams = new DieCostCalculationParameters() { ParentLifecycleProductionQuantity = parentLifeCycleProductionQuantity };
                DieCost cost = costCalculator.CalculateDieCost(updatedDie, calculationParams);

                this.DieTotalCost = cost.Cost;
                this.CalcDiesPerTime = (int)cost.DiesPerLifeTime;
            }
        }

        /// <summary>
        /// Refreshes the die parent's cost and notifies listeners.
        /// </summary>        
        private void RefreshParentCost()
        {
            this.RefreshCalculations(this.DieParent);
        }

        #endregion Cost handling

        #region Save/Cancel

        /// <summary>
        /// Determines whether this instance can perform the save operation. Executed by the SaveCommand.
        /// The default implementation allows the save to be performed if the input is valid.
        /// </summary>
        /// <returns>
        /// true if the save operation can be performed, false otherwise.
        /// </returns>
        protected override bool CanSave()
        {
            return base.CanSave() && this.SaveAllCommand.CanExecute(null);
        }

        /// <summary>
        /// Determines whether the Cancel operation can be performed. Executed by the CancelCommand.
        /// </summary>
        /// <returns>
        /// true if the changes can be canceled, false otherwise.
        /// </returns>
        protected override bool CanCancel()
        {
            return base.CanCancel() && this.CancelAllCommand.CanExecute(null);
        }

        /// <summary>
        /// Saves all changes back into the model.
        /// </summary>
        protected override void SaveToModel()
        {
            this.CheckModel();

            // set the remaining properties from model
            if (this.DieIsWearInPercentage.Value)
            {
                this.Model.Wear = this.DieWearPercentage;
            }
            else
            {
                this.Model.Wear = this.DieWearCost;
            }

            if (this.DieIsMaintenanceInPercentage.Value)
            {
                this.Model.Maintenance = this.DieMaintenancePercentage;
            }
            else
            {
                this.Model.Maintenance = this.DieMaintenanceCost;
            }

            if (this.DiesAreAllPaidByCustomer.Value.Value)
            {
                this.Model.DiesetsNumberPaidByCustomer = this.CalcDiesPerTime;
            }

            var manufacturer = this.ManufacturerViewModel.Model;
            if (manufacturer != null)
            {
                this.Model.Manufacturer = manufacturer;
            }

            // save to model
            base.SaveToModel();
        }

        /// <summary>
        /// Performs the save operation. Executed by the SaveCommand.
        /// </summary>
        protected override void Save()
        {
            this.CheckModelAndDataSource();

            this.SaveAllCommand.Execute(null);

            // Save all changes back into the Model objects. The validity check of this operation is performed by the CanSave method.
            this.SaveToModel();

            if (this.SavesToDataSource)
            {
                // Update die
                this.DataSourceManager.DieRepository.Save(this.Model);
                this.DataSourceManager.SaveChanges();

                // Notify the other components that the machine was created/updated.
                EntityChangedMessage message = this.Model.IsMasterData ?
                    new EntityChangedMessage(Notification.MasterDataEntityChanged) :
                    new EntityChangedMessage(Notification.MyProjectsEntityChanged);

                message.ChangeType = this.EditMode == ViewModelEditMode.Create ? EntityChangeType.EntityCreated : EntityChangeType.EntityUpdated;
                message.Entity = this.Model;

                this.SendMessage(message, false);
            }

            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Cancels all changes. Executed by the CancelCommand.
        /// </summary>
        protected override void Cancel()
        {
            if (!this.CanCancel())
            {
                return;
            }

            if (this.IsChanged)
            {
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_CancelChanges, MessageDialogType.YesNo);
                if (result != MessageDialogResult.Yes)
                {
                    // Don't cancel the changes and also don't close the view-model.
                    return;
                }

                using (this.UndoManager.StartBatch())
                {
                    // Cancel all changes
                    base.Cancel();
                    this.CancelAllCommand.Execute(null);
                }
            }

            // Close the view-model when is displayed in a window.            
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Called before unloading the view from its parent. Returning false will cancel the view's unloading.
        /// </summary>
        /// <returns>
        /// True if the unloading process should continue and false if it should be canceled.
        /// </returns>
        public override bool OnUnloading()
        {
            // Do nothing if the view-model is read-only, in viewer mode, it was not changed or the model was deleted.
            if (this.IsReadOnly || this.IsInViewerMode || !this.IsChanged || this.Model.IsDeleted)
            {
                return true;
            }

            if (this.EditMode == ViewModelEditMode.Create)
            {
                // Ask the user to confirm quitting
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_Quit, MessageDialogType.YesNo);
                if (result != MessageDialogResult.Yes)
                {
                    // The user chose to stay on the screen; return false to stop the screen unloading.
                    return false;
                }
                else
                {
                    this.IsChanged = false;
                }
            }
            else if (this.EditMode == ViewModelEditMode.Edit)
            {
                // Ask the user if he wants to save
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_UnsavedDataOnQuit, MessageDialogType.YesNoCancel);
                if (result == MessageDialogResult.Yes)
                {
                    // The user whishes to save but it is not possible because the input is not valid. Return false to stop the view-model from unloading.
                    if (!this.CanSave())
                    {
                        return false;
                    }

                    this.Save();
                }
                else if (result == MessageDialogResult.No)
                {
                    // The user does not want to save.                    
                    this.IsChanged = false;
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Called after one or more model properties value changed in order to refresh the calculations.
        /// </summary>
        protected override void RefreshCalculation()
        {
            if (this.EditMode == ViewModelEditMode.Create)
            {
                return;
            }

            var parenPart = this.ModelParentClone as Part;
            if (parenPart != null)
            {
                this.RefreshCalculations(parenPart);
            }
            else
            {
                var parentAssembly = this.ModelParentClone as Assembly;
                if (parentAssembly != null)
                {
                    this.RefreshCalculations(parentAssembly);
                }
            }
        }

        #endregion Save/Cancel

        /// <summary>
        /// Calculates the parent and sends a message with the result.
        /// </summary>
        /// <param name="entityParent">The entity parent.</param>
        private void RefreshCalculations(object entityParent)
        {
            if (entityParent == null)
            {
                return;
            }

            var parentPart = entityParent as Part;
            if (parentPart != null)
            {
                PartCostCalculationParameters calculationParams = null;
                if (this.ParentProject != null)
                {
                    calculationParams = CostCalculationHelper.CreatePartParamsFromProject(this.ParentProject);
                }
                else if (IsInViewerMode)
                {
                    // Note: theoretically this part is never reached because it's not possible to save in view mode.
                    calculationParams = this.modelBrowserHelperService.GetPartCostCalculationParameters();
                }

                if (calculationParams != null)
                {
                    var calculator = CostCalculatorFactory.GetCalculator(parentPart.CalculationVariant);
                    var result = calculator.CalculatePartCost(parentPart, calculationParams);
                    this.SendMessage(new CurrentComponentCostChangedMessage(result), true);
                }
            }
            else
            {
                var parentAssembly = entityParent as Assembly;
                if (parentAssembly != null)
                {
                    AssemblyCostCalculationParameters calculationParams = null;
                    if (this.ParentProject != null)
                    {
                        calculationParams = CostCalculationHelper.CreateAssemblyParamsFromProject(this.ParentProject);
                    }
                    else if (IsInViewerMode)
                    {
                        // Note: theoretically this part is never reached because it's not possible to save in view mode.                        
                        calculationParams = this.modelBrowserHelperService.GetAssemblyCostCalculationParameters();
                    }

                    if (calculationParams != null)
                    {
                        var calculator = CostCalculatorFactory.GetCalculator(parentAssembly.CalculationVariant);
                        var result = calculator.CalculateAssemblyCost(parentAssembly, calculationParams);
                        this.SendMessage(new CurrentComponentCostChangedMessage(result), true);
                    }
                }
            }
        }

        /// <summary>
        /// Sends a message with or without a token
        /// </summary>
        /// <typeparam name="TMessage">The message type</typeparam>
        /// <param name="message">The message to send</param>
        /// <param name="sendWithToken">A value indicating whether to send the message with a token or send it globally without one</param>
        private void SendMessage<TMessage>(TMessage message, bool sendWithToken)
        {
            if (sendWithToken)
            {
                if (this.MessageToken == null)
                {
                    this.messenger.Send(message, this.IsInViewerMode ? GlobalMessengerTokens.ModelBrowserTargetToken : GlobalMessengerTokens.MainViewTargetToken);
                }
                else
                {
                    this.messenger.Send(message, this.MessageToken);
                }
            }
            else
            {
                this.messenger.Send(message);
            }
        }
    }
}