﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Input;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using OfficeOpenXml;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Reporting;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for Master Data Importer\Exporter.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class MasterDataImporterExporterViewModel : ViewModel
    {
        #region Fields

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The title.
        /// </summary>
        private string title;

        /// <summary>
        /// The entity type.
        /// </summary>
        private ImportedEntityType entityType;

        /// <summary>
        /// The type of the operation Import\Export.
        /// </summary>
        private MasterDataOperation operation = MasterDataOperation.Import;

        /// <summary>
        /// The data source manager to work with.
        /// </summary>
        private IDataSourceManager dataManager;

        /// <summary>
        /// The file dialog service.
        /// </summary>
        private IFileDialogService fileDialogService;

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The headers.
        /// </summary>
        private List<string> headers = new List<string>();

        /// <summary>
        /// The table data imported from the import file.
        /// </summary>
        private List<List<object>> importedFileData = new List<List<object>>();

        /// <summary>
        /// The import\export button content.
        /// </summary>
        private string importExportButtonContent;

        /// <summary>
        /// The busy message durring import\export.
        /// </summary>
        private string importExportBusyMessage;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="MasterDataImporterExporterViewModel"/> class.
        /// </summary>
        /// <param name="fileDialogService">The file dialog service.</param>
        /// <param name="messenger">The messenger.</param>
        /// <param name="windowService">The window service.</param>
        [ImportingConstructor]
        public MasterDataImporterExporterViewModel(
            IFileDialogService fileDialogService,
            IMessenger messenger,
            IWindowService windowService)
        {
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("fileDialogService", windowService);

            this.fileDialogService = fileDialogService;
            this.messenger = messenger;
            this.WindowService = windowService;

            this.SelectPathCommand = new DelegateCommand(SelectPath);
            this.CloseCommand = new DelegateCommand<object>(this.Close, this.CanClose);
            this.ImportExportCommand = new DelegateCommand<object>(this.ExecuteImportExport, this.CanImportExport);

            this.FilePath = new DataProperty<string>(this, "FilePath");

            this.WindowClosing = new DelegateCommand<CancelEventArgs>(this.OnViewClosing);
        }

        #region Properties

        /// <summary>
        /// Gets or sets the content of the title.
        /// </summary>
        public string Title
        {
            get
            {
                return this.title;
            }

            set
            {
                if (this.title != value)
                {
                    this.title = value;
                    OnPropertyChanged(() => this.Title);
                }
            }
        }

        /// <summary>
        /// Gets or sets the data source manager.
        /// </summary>
        public IDataSourceManager DataManager
        {
            get
            {
                return this.dataManager;
            }

            set
            {
                if (this.dataManager != value)
                {
                    this.dataManager = value;
                    OnPropertyChanged(() => this.DataManager);
                }
            }
        }

        /// <summary>
        /// Gets or sets the type of the imported entity.
        /// </summary>
        public ImportedEntityType EntityType
        {
            get
            {
                return this.entityType;
            }

            set
            {
                this.SetProperty(ref this.entityType, value, () => this.EntityType);
                this.InitializeHeaders();
            }
        }

        /// <summary>
        /// Gets or sets the window service.
        /// </summary>
        public IWindowService WindowService
        {
            get
            {
                return this.windowService;
            }

            set
            {
                if (this.windowService != value)
                {
                    this.windowService = value;
                    OnPropertyChanged(() => this.WindowService);
                }
            }
        }

        /// <summary>
        /// Gets or sets the content of the import\export button.
        /// </summary>
        public string ImportExportButtonContent
        {
            get
            {
                return this.importExportButtonContent;
            }

            set
            {
                if (this.importExportButtonContent != value)
                {
                    this.importExportButtonContent = value;
                    OnPropertyChanged(() => this.ImportExportButtonContent);
                }
            }
        }

        /// <summary>
        /// Gets or sets the busy message during import\export.
        /// </summary>
        public string ImportExportBusyMessage
        {
            get
            {
                return this.importExportBusyMessage;
            }

            set
            {
                if (this.importExportBusyMessage != value)
                {
                    this.importExportBusyMessage = value;
                    OnPropertyChanged(() => this.ImportExportBusyMessage);
                }
            }
        }

        /// <summary>
        /// Gets the file path.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_FilePath", ErrorMessageResourceType = typeof(LocalizedResources))]
        public DataProperty<string> FilePath { get; private set; }

        /// <summary>
        /// Gets the property that indicates if the import\export operation is in progress.
        /// </summary>
        public VMProperty<bool> IsImportOrExportInProgress { get; private set; }

        /// <summary>
        /// Gets or sets the operation type.
        /// </summary>
        public MasterDataOperation Operation
        {
            get
            {
                return this.operation;
            }

            set
            {
                this.operation = value;
                if (this.operation == MasterDataOperation.Import)
                {
                    this.Title = LocalizedResources.MasterDataImporter_Title;
                    this.ImportExportButtonContent = LocalizedResources.General_Import;
                    this.ImportExportBusyMessage = LocalizedResources.MasterData_ImportingBusyMessage;
                }
                else
                {
                    this.ImportExportButtonContent = LocalizedResources.General_Export;
                    this.Title = LocalizedResources.MasterDataExporter_Title;
                    this.ImportExportBusyMessage = LocalizedResources.MasterData_ExportingBusyMessage;
                }
            }
        }

        /// <summary>
        /// Gets the selected sheet.
        /// </summary>
        public VMProperty<object> SelectedSheet { get; private set; }

        /// <summary>
        /// Gets the sheets.
        /// </summary>
        public VMProperty<ObservableCollection<object>> Sheets { get; private set; }

        /// <summary>
        /// Gets or sets the type of the file.
        /// </summary>
        public MasterDataImportExportFileType FileType { get; set; }

        #endregion

        #region ICommands

        /// <summary>
        /// Gets the import\export command.
        /// </summary>>
        public ICommand ImportExportCommand { get; private set; }

        /// <summary>
        /// Gets the select path command.
        /// </summary>
        public ICommand SelectPathCommand { get; private set; }

        /// <summary>
        /// Gets the command for the closing event on the view
        /// </summary>
        public ICommand WindowClosing { get; private set; }

        #endregion

        /// <summary>
        /// Closes the view. 
        /// </summary>
        /// <param name="param">The parameter.</param>
        private void Close(object param)
        {
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Determines whether the view can close.
        /// </summary>
        /// <param name="param">The parameter.</param>
        /// <returns>
        /// <c>true</c> if this instance can close the specified parameter; otherwise, <c>false</c>.
        /// </returns>
        private bool CanClose(object param)
        {
            return true;
        }

        /// <summary>
        /// Method is called when an attempt is made to close the window
        /// </summary>
        /// <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        private void OnViewClosing(CancelEventArgs e)
        {
            if (this.IsImportOrExportInProgress.Value)
            {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// Determines whether this instance [can export-import].
        /// </summary>
        /// <param name="param">The parameter.</param>
        /// <returns>
        /// <c>true</c> if this instance can export/import the specified parameter; otherwise, <c>false</c>.
        /// </returns>
        private bool CanImportExport(object param)
        {
            if (string.IsNullOrWhiteSpace(this.FilePath.Value))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Starts the import\export operation.
        /// </summary>
        /// <param name="param">The parameter.</param>
        public void ExecuteImportExport(object param)
        {
            if (this.Operation == MasterDataOperation.Import)
            {
                this.ImportFile();
            }
            else
            {
                this.ExportData();
            }
        }

        /// <summary>
        /// Selects the file path.
        /// </summary>
        private void SelectPath()
        {
            this.fileDialogService.Reset();

            if (this.Operation == MasterDataOperation.Import)
            {
                this.fileDialogService.Filter = LocalizedResources.DialogFilter_ExcelDocs;
                this.fileDialogService.Multiselect = false;
                this.FileType = MasterDataImportExportFileType.None;
                this.FilePath.Value = null;

                if (this.fileDialogService.ShowOpenFileDialog() != true)
                {
                    return;
                }

                var fileExtension = Path.GetExtension(this.fileDialogService.FileName);

                if (fileExtension == ".xlsx")
                {
                    this.FileType = MasterDataImportExportFileType.Xlsx;
                }
                else if (fileExtension == ".xls")
                {
                    this.FileType = MasterDataImportExportFileType.Xls;
                }

                if (this.FileType != MasterDataImportExportFileType.None)
                {
                    this.FilePath.Value = this.fileDialogService.FileName;
                }
                else
                {
                    windowService.MessageDialogService.Show(LocalizedResources.ExportImport_ImportInvalidFileContent, MessageDialogType.Error);
                }
            }
            else
            {
                var exportFileName = PathUtils.FixFilePathForCollisions(this.EntityType.ToString() + ".xls");
                this.fileDialogService.FileName = exportFileName;
                this.fileDialogService.Filter = LocalizedResources.DialogFilter_ExcelDocs;

                var result = this.fileDialogService.ShowSaveFileDialog();
                if (result == true)
                {
                    this.FilePath.Value = this.fileDialogService.FileName;
                }

                if (string.IsNullOrWhiteSpace(this.FilePath.Value))
                {
                    return;
                }
            }
        }

        #region Import

        /// <summary>
        /// Tries to populate table from excel.
        /// </summary>
        /// <returns>True if the table could be populate, false otherwise.</returns>
        public bool TryPopulateTableFromExcel()
        {
            bool isOK = false;
            if (File.Exists(this.FilePath.Value))
            {
                if (this.FileType == MasterDataImportExportFileType.Xlsx || this.FileType == MasterDataImportExportFileType.Xls)
                {
                    var errorMessage = ExcelImporterHelper.TryPopulateTableFromExcel(this.SelectedSheet.Value, this.importedFileData, "A1");
                    if (string.IsNullOrWhiteSpace(errorMessage))
                    {
                        isOK = true;
                    }
                    else
                    {
                        this.windowService.MessageDialogService.Show(LocalizedResources.MassDataImport_CouldNotLoadDataFromFile, MessageDialogType.Error);
                    }
                }
            }

            return isOK;
        }

        /// <summary>
        /// Gets the sheets from the xls file.
        /// </summary>
        /// <returns>True if the sheets could be set, false otherwise.</returns>
        private bool TrySetSheets()
        {
            try
            {
                if (this.FileType == MasterDataImportExportFileType.Xlsx)
                {
                    using (Stream fileStream = new FileStream(this.FilePath.Value, FileMode.Open))
                    {
                        using (ExcelPackage package = new ExcelPackage(fileStream))
                        {
                            ExcelWorkbook workBook = package.Workbook;

                            this.Sheets.Value = new ObservableCollection<object>();
                            for (int index = 1; index <= workBook.Worksheets.Count; index++)
                            {
                                var sheet = workBook.Worksheets[index];
                                this.Sheets.Value.Add(sheet);
                            }
                        }
                    }
                }
                else if (this.FileType == MasterDataImportExportFileType.Xls)
                {
                    using (Stream fileStream = new FileStream(this.FilePath.Value, FileMode.Open))
                    {
                        HSSFWorkbook workbook = new HSSFWorkbook(fileStream);

                        this.Sheets.Value = new ObservableCollection<object>();
                        for (int index = 0; index < workbook.NumberOfSheets; index++)
                        {
                            var sheet = workbook.GetSheetAt(index);
                            this.Sheets.Value.Add(sheet);
                        }
                    }
                }

                this.SelectedSheet.Value = this.Sheets.Value.FirstOrDefault();

                return true;
            }
            catch (IOException ex)
            {
                log.ErrorException("IO error occurred while opening the selected file.", ex);
                this.windowService.MessageDialogService.Show(LocalizedResources.General_FileIOError, MessageDialogType.Error);
                return false;
            }
            catch (UnauthorizedAccessException ex)
            {
                log.ErrorException("IO error occurred while accessing the selected file.", ex);
                this.windowService.MessageDialogService.Show(LocalizedResources.General_FileAccessError, MessageDialogType.Error);
                return false;
            }
            catch (Exception ex)
            {
                log.ErrorException("Could not load data from selected file.", ex);
                this.windowService.MessageDialogService.Show(LocalizedResources.MassDataImport_CouldNotLoadDataFromFile, MessageDialogType.Error);
                return false;
            }
        }

        /// <summary>
        /// Imports this instance.
        /// </summary>
        private void ImportFile()
        {
            if (!this.TrySetSheets())
            {
                return;
            }

            if (!this.TryPopulateTableFromExcel())
            {
                return;
            }

            // The list of entities.
            List<object> entities = new List<object>();

            // The table headers.
            List<object> headers = this.importedFileData.FirstOrDefault();

            var isHeaderInFile = this.headers.All(h => headers.Contains(h));
            if (!isHeaderInFile)
            {
                this.windowService.MessageDialogService.Show(LocalizedResources.Error_FileContentNotSupported, MessageDialogType.Error);
                return;
            }

            // The total number of table rows.
            int rowCount = this.importedFileData.Count;

            int rowIndex = 1;
            int indexColumn = 0;
            bool isEntityCreated = false;

            this.IsImportOrExportInProgress.Value = true;

            Task.Factory.StartNew(() =>
            {
                // Go through each table row and create the object graph hierarchy.
                for (rowIndex = 1; rowIndex < rowCount; rowIndex++)
                {
                    // The reference to the object to save to database.
                    object entityCreated = null;

                    if (this.EntityType == ImportedEntityType.Currency)
                    {
                        Currency currency = new Currency();
                        entityCreated = currency;
                        entities.Add(entityCreated);
                    }
                    else if (this.EntityType == ImportedEntityType.CountrySetting)
                    {
                        CountrySetting countrySetting = new CountrySetting();
                        entityCreated = countrySetting;
                        entities.Add(entityCreated);
                    }
                    else if (this.EntityType == ImportedEntityType.RawMaterial)
                    {
                        RawMaterial rawMaterial = new RawMaterial();
                        entityCreated = rawMaterial;
                        entities.Add(entityCreated);
                    }
                    else if (this.EntityType == ImportedEntityType.Machine)
                    {
                        Machine machine = new Machine();
                        entityCreated = machine;
                        entities.Add(entityCreated);
                    }

                    // Set properties for the created entity.                    
                    for (indexColumn = 0; indexColumn < headers.Count; indexColumn++)
                    {
                        string headerValue = headers[indexColumn].ToString();

                        object valueToSet = this.importedFileData[rowIndex][indexColumn];
                        var propertyInfo = entityCreated.GetType().GetProperty(headerValue);
                        if (propertyInfo != null && valueToSet != null)
                        {
                            var propertyType = propertyInfo.PropertyType;
                            var propName = propertyInfo.Name;
                            object result = null;

                            if (Converter.Cast(propertyType, valueToSet, ref result))
                            {
                                propertyInfo.SetValue(entityCreated, result, null);
                            }
                        }
                    }
                }

                isEntityCreated = true;
                this.ImportData(entities);
            }).ContinueWith(
                (task) =>
                {
                    this.IsImportOrExportInProgress.Value = false;
                    if (task.Exception == null)
                    {
                        // Send the notification through the global messenger.
                        EntityImportedMessage msg = new EntityImportedMessage(this.EntityType);
                        this.messenger.Send(msg);

                        this.windowService.MessageDialogService.Show(LocalizedResources.MasterData_ImportSuccessful, MessageDialogType.Info);
                    }
                    else
                    {
                        Exception error = task.Exception.InnerException;
                        log.ErrorException("Master Data import failed.", error);

                        if (error is UnauthorizedAccessException)
                        {
                            this.windowService.MessageDialogService.Show(LocalizedResources.General_FileAccessError, MessageDialogType.Error);
                        }
                        else if (error is IOException)
                        {
                            this.windowService.MessageDialogService.Show(LocalizedResources.General_FileIOError, MessageDialogType.Error);
                        }
                        else if (error is Exception)
                        {
                            if (!isEntityCreated)
                            {
                                string errorMessage = string.Format(LocalizedResources.Import_ErrorImport, headers[indexColumn], this.importedFileData[rowIndex][indexColumn]);
                                this.windowService.MessageDialogService.Show(errorMessage, MessageDialogType.Error);
                            }
                            else
                            {
                                this.windowService.MessageDialogService.Show(LocalizedResources.Error_FileContentNotSupported, MessageDialogType.Error);
                            }
                        }
                    }

                    this.windowService.CloseViewWindow(this);
                },
            TaskScheduler.FromCurrentSynchronizationContext());
        }

        /// <summary>
        /// Imports the data entity based on unique identifier.
        /// </summary>
        /// <param name="entities">The list of entities.</param>
        private void ImportData(List<object> entities)
        {
            var isStockMasterData = false;
            if (this.EntityType == ImportedEntityType.Currency)
            {
                var allCurrencies = this.DataManager.CurrencyRepository.GetBaseCurrencies();
                foreach (var entity in entities)
                {
                    Currency currency = entity as Currency;
                    var dbCurrency = allCurrencies.FirstOrDefault(c => c.Guid == currency.Guid);
                    if (dbCurrency == null)
                    {
                        dbCurrency = new Currency();
                        this.DataManager.CurrencyRepository.Add(dbCurrency);
                    }

                    isStockMasterData = dbCurrency.IsStockMasterData;

                    currency.CopyValuesTo(dbCurrency);
                    dbCurrency.IsMasterData = true;
                    dbCurrency.IsStockMasterData = isStockMasterData;
                }
            }
            else if (this.EntityType == ImportedEntityType.CountrySetting)
            {
                var countries = this.DataManager.CountryRepository.GetAll();
                foreach (var entity in entities)
                {
                    CountrySetting countrySetting = entity as CountrySetting;
                    var dbCountrySetting = countries.FirstOrDefault(c => c.CountrySetting.Guid == countrySetting.Guid).CountrySetting;
                    if (dbCountrySetting != null)
                    {
                        var isScrambled = dbCountrySetting.IsScrambled;
                        countrySetting.CopyValuesTo(dbCountrySetting);
                        dbCountrySetting.IsScrambled = isScrambled;
                    }
                }
            }
            else if (this.EntityType == ImportedEntityType.RawMaterial)
            {
                var rawMaterials = this.DataManager.RawMaterialRepository.GetAllMasterData();
                foreach (var entity in entities)
                {
                    RawMaterial rawMaterial = entity as RawMaterial;
                    var dbRawMaterial = rawMaterials.FirstOrDefault(r => r.Guid == rawMaterial.Guid);
                    if (dbRawMaterial == null)
                    {
                        dbRawMaterial = new RawMaterial();
                        this.DataManager.RawMaterialRepository.Add(dbRawMaterial);
                    }

                    isStockMasterData = dbRawMaterial.IsStockMasterData;

                    rawMaterial.CopyValuesTo(dbRawMaterial);
                    dbRawMaterial.IsMasterData = true;
                    dbRawMaterial.IsStockMasterData = isStockMasterData;
                }
            }
            else if (this.EntityType == ImportedEntityType.Machine)
            {
                var machines = this.DataManager.MachineRepository.GetAllMasterData();
                foreach (var entity in entities)
                {
                    Machine machine = entity as Machine;
                    var dbMachine = machines.FirstOrDefault(m => m.Guid == machine.Guid);
                    if (dbMachine == null)
                    {
                        dbMachine = new Machine();
                        this.DataManager.MachineRepository.Add(dbMachine);
                    }

                    isStockMasterData = dbMachine.IsStockMasterData;

                    machine.CopyValuesTo(dbMachine);
                    dbMachine.IsMasterData = true;
                    dbMachine.IsStockMasterData = isStockMasterData;
                }
            }

            this.DataManager.SaveChanges();
        }

        #endregion Import

        #region Export

        /// <summary>
        /// Exports the master data of the specified type.
        /// </summary>
        private void ExportData()
        {
            var dbData = new List<object>();

            Task.Factory.StartNew(() =>
            {
                this.IsImportOrExportInProgress.Value = true;
                if (this.EntityType == ImportedEntityType.Currency)
                {
                    var allCurrencies = this.DataManager.CurrencyRepository.GetBaseCurrencies().OrderBy(c => c.Name);
                    dbData.AddRange(allCurrencies);
                    dbData.Remove(allCurrencies.FirstOrDefault(c => c.IsoCode == Constants.DefaultCurrencyIsoCode));
                }
                else if (this.EntityType == ImportedEntityType.CountrySetting)
                {
                    var allCountries = this.DataManager.CountryRepository.GetAll().OrderBy(c => c.Name);
                    dbData.AddRange(allCountries);
                }
                else if (this.EntityType == ImportedEntityType.RawMaterial)
                {
                    var rawMaterials = this.DataManager.RawMaterialRepository.GetAllMasterData().OrderBy(r => r.Name);
                    dbData.AddRange(rawMaterials);
                }
                else if (this.EntityType == ImportedEntityType.Machine)
                {
                    var machines = this.DataManager.MachineRepository.GetAllMasterData().OrderBy(m => m.Name);
                    dbData.AddRange(machines);
                }

                this.GenerateExcelFile(this.FilePath.Value, dbData);
            }).ContinueWith(
                (task) =>
                {
                    this.IsImportOrExportInProgress.Value = false;
                    if (task.Exception == null)
                    {
                        this.windowService.MessageDialogService.Show(LocalizedResources.MasterData_ExportSuccessful, MessageDialogType.Info);
                    }
                    else
                    {
                        log.ErrorException("Failed to export master data to excel.", task.Exception);
                        this.windowService.MessageDialogService.Show(LocalizedResources.Error_FileExportFailed, MessageDialogType.Error);
                    }

                    this.windowService.CloseViewWindow(this);
                },
            TaskScheduler.FromCurrentSynchronizationContext());
        }

        /// <summary>
        /// Generates the excel file for this instance.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="entities">The entities.</param>
        private void GenerateExcelFile(string filePath, List<object> entities)
        {
            var workbook = new HSSFWorkbook();
            var sheet = workbook.CreateSheet();

            // Set the sheet name.
            workbook.SetSheetName(0, ReportsHelper.ValidateWorksheetName(this.EntityType.ToString()));

            object[] headerToAdd = headers.ToArray();
            object[,] data = new object[entities.Count, headerToAdd.Length];

            // Write data in the excel file.
            for (int i = 0; i < entities.Count; i++)
            {
                var itemEntity = entities[i];
                for (int j = 0; j < headerToAdd.Length; j++)
                {
                    object memberData = null;
                    if (headerToAdd[j].ToString().Contains("."))
                    {
                        var propObject = itemEntity.GetType().InvokeMember(headerToAdd[j].ToString().Substring(0, headerToAdd[j].ToString().IndexOf(".")), BindingFlags.GetProperty, null, itemEntity, null);
                        memberData = propObject.GetType().InvokeMember(headerToAdd[j].ToString().Substring(headerToAdd[j].ToString().IndexOf(".") + 1), BindingFlags.GetProperty, null, propObject, null);
                    }
                    else
                    {
                        memberData = itemEntity.GetType().InvokeMember(headerToAdd[j].ToString(), BindingFlags.GetProperty, null, itemEntity, null);
                    }

                    data[i, j] = memberData;
                }
            }

            // Add rows in the excel file.
            this.AddRowsInExcel(workbook, sheet, headerToAdd, data, entities);

            // Save the exported file.
            using (FileStream file = new FileStream(filePath, FileMode.Create))
            {
                workbook.Write(file);
            }
        }

        /// <summary>
        /// Adds the rows in excel.
        /// </summary>
        /// <param name="workbook">The workbook.</param>
        /// <param name="sheet">The sheet.</param>
        /// <param name="headerToAdd">The header to add.</param>
        /// <param name="data">The data member.</param>
        /// <param name="entities">The entities.</param>
        private void AddRowsInExcel(HSSFWorkbook workbook, ISheet sheet, object[] headerToAdd, object[,] data, List<object> entities)
        {
            CellRangeAddress dataCellRangeAddress = new CellRangeAddress(0, entities.Count, 0, headerToAdd.Length);
            var headerRow = sheet.CreateRow(0);
            for (var j = 0; j <= dataCellRangeAddress.LastColumn - 1; j++)
            {
                var headerCell = headerRow.CreateCell(j);
                if (headerToAdd[j].ToString().Contains("."))
                {
                    headerCell.SetCellValue(headerToAdd[j].ToString().Substring(headerToAdd[j].ToString().IndexOf(".") + 1));
                }
                else
                {
                    headerCell.SetCellValue(headerToAdd[j]);
                }

                this.SetExcelHeaderStyle(workbook, headerCell);
            }

            for (var i = dataCellRangeAddress.FirstRow + 1; i <= dataCellRangeAddress.LastRow; i++)
            {
                var row = sheet.CreateRow(i);
                for (var j = dataCellRangeAddress.FirstColumn; j <= dataCellRangeAddress.LastColumn - 1; j++)
                {
                    var cell = row.CreateCell(j);
                    cell.SetCellValue(data[i - 1, j]);
                }
            }

            for (var j = dataCellRangeAddress.FirstColumn; j <= dataCellRangeAddress.LastColumn - 1; j++)
            {
                sheet.AutoSizeColumn(j);
            }

            // Hide the first column from the excel file which contains the Guid informations.
            sheet.SetColumnWidth(0, 0);
        }

        /// <summary>
        /// Sets the excel headers style.
        /// </summary>
        /// <param name="workbook">The workbook.</param>
        /// <param name="cellHeader">The cell header.</param>
        private void SetExcelHeaderStyle(HSSFWorkbook workbook, ICell cellHeader)
        {
            var font = workbook.CreateFont();
            var style = workbook.CreateCellStyle();

            font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;
            style.SetFont(font);

            cellHeader.CellStyle = style;
        }

        /// <summary>
        /// Initializes the headers for the excel file.
        /// </summary>
        private void InitializeHeaders()
        {
            this.headers.Clear();
            if (this.EntityType == ImportedEntityType.Currency)
            {
                var currency = new Currency();
                this.headers.Add(ReflectionUtils.GetProperty(() => currency.Guid).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => currency.Name).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => currency.Symbol).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => currency.ExchangeRate).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => currency.IsoCode).Name);
            }
            else if (this.EntityType == ImportedEntityType.CountrySetting)
            {
                var country = new Country();
                var countrySettingString = ReflectionUtils.GetProperty(() => country.CountrySetting).Name;

                this.headers.Add(string.Format("{0}.{1}", countrySettingString, ReflectionUtils.GetProperty(() => country.CountrySetting.Guid).Name));
                this.headers.Add(ReflectionUtils.GetProperty(() => country.Name).Name);
                this.headers.Add(string.Format("{0}.{1}", countrySettingString, ReflectionUtils.GetProperty(() => country.CountrySetting.UnskilledLaborCost).Name));
                this.headers.Add(string.Format("{0}.{1}", countrySettingString, ReflectionUtils.GetProperty(() => country.CountrySetting.SkilledLaborCost).Name));
                this.headers.Add(string.Format("{0}.{1}", countrySettingString, ReflectionUtils.GetProperty(() => country.CountrySetting.ForemanCost).Name));
                this.headers.Add(string.Format("{0}.{1}", countrySettingString, ReflectionUtils.GetProperty(() => country.CountrySetting.TechnicianCost).Name));
                this.headers.Add(string.Format("{0}.{1}", countrySettingString, ReflectionUtils.GetProperty(() => country.CountrySetting.EngineerCost).Name));
                this.headers.Add(string.Format("{0}.{1}", countrySettingString, ReflectionUtils.GetProperty(() => country.CountrySetting.ShiftCharge1ShiftModel).Name));
                this.headers.Add(string.Format("{0}.{1}", countrySettingString, ReflectionUtils.GetProperty(() => country.CountrySetting.ShiftCharge2ShiftModel).Name));
                this.headers.Add(string.Format("{0}.{1}", countrySettingString, ReflectionUtils.GetProperty(() => country.CountrySetting.ShiftCharge3ShiftModel).Name));
                this.headers.Add(string.Format("{0}.{1}", countrySettingString, ReflectionUtils.GetProperty(() => country.CountrySetting.LaborAvailability).Name));
                this.headers.Add(string.Format("{0}.{1}", countrySettingString, ReflectionUtils.GetProperty(() => country.CountrySetting.EnergyCost).Name));
                this.headers.Add(string.Format("{0}.{1}", countrySettingString, ReflectionUtils.GetProperty(() => country.CountrySetting.WaterCost).Name));
                this.headers.Add(string.Format("{0}.{1}", countrySettingString, ReflectionUtils.GetProperty(() => country.CountrySetting.AirCost).Name));
                this.headers.Add(string.Format("{0}.{1}", countrySettingString, ReflectionUtils.GetProperty(() => country.CountrySetting.ProductionAreaRentalCost).Name));
                this.headers.Add(string.Format("{0}.{1}", countrySettingString, ReflectionUtils.GetProperty(() => country.CountrySetting.OfficeAreaRentalCost).Name));
                this.headers.Add(string.Format("{0}.{1}", countrySettingString, ReflectionUtils.GetProperty(() => country.CountrySetting.InterestRate).Name));
            }
            else if (this.EntityType == ImportedEntityType.RawMaterial)
            {
                var rawMaterial = new RawMaterial();
                this.headers.Add(ReflectionUtils.GetProperty(() => rawMaterial.Guid).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => rawMaterial.Name).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => rawMaterial.NameUK).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => rawMaterial.NameUS).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => rawMaterial.NormName).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => rawMaterial.DeliveryType).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => rawMaterial.Price).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => rawMaterial.ParentWeight).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => rawMaterial.Sprue).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => rawMaterial.Quantity).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => rawMaterial.Loss).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => rawMaterial.RecyclingRatio).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => rawMaterial.ScrapRefundRatio).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => rawMaterial.RejectRatio).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => rawMaterial.YieldStrength).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => rawMaterial.RuptureStrength).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => rawMaterial.Density).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => rawMaterial.MaxElongation).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => rawMaterial.GlassTransitionTemperature).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => rawMaterial.Rx).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => rawMaterial.Rm).Name);
            }
            else if (this.EntityType == ImportedEntityType.Machine)
            {
                var machine = new Machine();
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.Guid).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.Name).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.ManufacturingYear).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.DepreciationPeriod).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.DepreciationRate).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.RegistrationNumber).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.DescriptionOfFunction).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.FloorSize).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.WorkspaceArea).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.PowerConsumption).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.AirConsumption).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.WaterConsumption).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.FullLoadRate).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.MaxCapacity).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.OEE).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.Availability).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.MachineInvestment).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.MachineLeaseCosts).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.SetupInvestment).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.AdditionalEquipmentInvestment).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.FundamentalSetupInvestment).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.InvestmentRemarks).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.ExternalWorkCost).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.MaterialCost).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.ConsumablesCost).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.ManualConsumableCostPercentage).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.RepairsCost).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.KValue).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.CalculateWithKValue).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.MountingCubeLength).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.MountingCubeWidth).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.MountingCubeHeight).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.MaxDiameterRodPerChuckMaxThickness).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.MaxPartsWeightPerCapacity).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.LockingForce).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.PressCapacity).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.MaximumSpeed).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.RapidFeedPerCuttingSpeed).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.StrokeRate).Name);
                this.headers.Add(ReflectionUtils.GetProperty(() => machine.Other).Name);
            }
        }

        #endregion Export
    }
}