﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Group of collected data representing rows from the same group.
    /// </summary>
    public class CompareDataGroup
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the group is expanded.
        /// </summary>        
        public bool IsExpanded { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is highlighted.
        /// </summary>
        /// <value>
        /// true if this instance is highlighted; otherwise, false.
        /// </value>
        public bool IsHighlighted { get; set; }

        /// <summary>
        /// Gets or sets the rows located in this group.
        /// </summary>
        /// <value>
        /// The rows.
        /// </value>
        public List<CompareDataRow> Rows { get; set; }
    }
}
