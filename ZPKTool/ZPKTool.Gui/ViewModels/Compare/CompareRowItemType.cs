﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The type of the items that are compared
    /// </summary>
    public enum CompareRowItemType
    {
        /// <summary>
        /// The item doesn't have any type assigned
        /// </summary>
        None,

        /// <summary>
        /// The item represents the cost of the entities
        /// </summary>
        Cost = 1,

        /// <summary>
        /// The item represents the property of the entities
        /// </summary>
        Property = 2,

        /// <summary>
        /// The item is a sub entity (child) of the entities
        /// </summary>
        SubEntity = 3
    }
}
