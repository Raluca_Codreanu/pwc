﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Represents the header of a column in the Compare view.
    /// </summary>
    public class CompareHeaderItem : INotifyPropertyChanged
    {
        #region Fields

        /// <summary>
        /// The item header.
        /// </summary>
        private string header;

        /// <summary>
        /// The reference to its parent entity that represents
        /// </summary>
        private object parentEntity;

        #endregion

        #region Event

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the header.
        /// </summary>
        /// <value>
        /// The header.
        /// </value>
        public string Header
        {
            get
            {
                return this.header;
            }

            set
            {
                this.header = value;
                RaisePropertyChangeEvent("Header");
            }
        }

        /// <summary>
        /// Gets or sets the reference to its parent entity that represents
        /// </summary>
        public object ParentEntity
        {
            get
            {
                return this.parentEntity;
            }

            set
            {
                this.parentEntity = value;
                RaisePropertyChangeEvent("ParentEntity");
            }
        }

        #endregion

        #region Raise Property Change

        /// <summary>
        /// When a property has been changed, this method notifies the gui to update the contents
        /// </summary>
        /// <param name="propertyName">The name of the property which has been changed</param>
        private void RaisePropertyChangeEvent(string propertyName)
        {
            if (null != PropertyChanged)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}
