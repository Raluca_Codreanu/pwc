﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows.Media;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.Gui.Managers;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The comparison item.
    /// </summary>
    public class CompareRowPropertyItem : INotifyPropertyChanged
    {
        #region Fields

        /// <summary>
        /// The item header.
        /// </summary>
        private string header;

        /// <summary>
        /// The group of the item.
        /// </summary>
        private GroupHeader group;

        /// <summary>
        /// The row foreground.
        /// </summary>
        private bool isHighlighted;

        /// <summary>
        /// The display template type.
        /// </summary>
        private DisplayTemplateType displayTemplatetype;

        /// <summary>
        /// Column headers.
        /// </summary>
        private ObservableCollection<CompareHeaderItem> colomnHeaders;

        /// <summary>
        /// The measurement unit adapters corresponding to the compared items
        /// </summary>
        private ObservableCollection<UnitsAdapter> measurementUnitsAdapters;

        /// <summary>
        /// A weak event listener for changes in the "DecimalPlacesNumber" application setting.
        /// </summary>
        private WeakEventListener<PropertyChangedEventArgs> numberOfDigitsToDisplayChangedListener;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="CompareRowPropertyItem"/> class.
        /// </summary>
        /// <param name="columnHeaders">The column headers.</param>
        /// <param name="header">The header.</param>
        /// <param name="symbolFormat">The symbol format.</param>
        /// <param name="group">The group.</param>
        /// <param name="displayTemplateType">Display type of the template.</param>
        /// <param name="comparedRowItems">The compared row items.</param>
        /// <param name="type">The type of the compared items.</param>
        public CompareRowPropertyItem(ObservableCollection<CompareHeaderItem> columnHeaders, string header, string symbolFormat, GroupHeader group, DisplayTemplateType displayTemplateType, IEnumerable comparedRowItems, Type type)
        {
            this.ColumnHeaders = columnHeaders;
            this.IsHighlighted = false;
            this.Header = header;
            this.Group = group;
            this.DisplayTemplateType = displayTemplateType;
            this.SymbolFormat = symbolFormat;

            this.ComparedRowItems = new ObservableCollection<object>();
            foreach (object obj in comparedRowItems)
            {
                this.ComparedRowItems.Add(obj);
            }

            this.Type = type;

            this.Group.IsHighlighted = false;
            this.ItemType = CompareRowItemType.None;
            this.ItemCostType = null;

            this.numberOfDigitsToDisplayChangedListener = new WeakEventListener<PropertyChangedEventArgs>(HandleNumberOfDecimalPlacesChanged);
            PropertyChangedEventManager.AddListener(
                UserSettingsManager.Instance,
                numberOfDigitsToDisplayChangedListener,
                ReflectionUtils.GetPropertyName(() => UserSettingsManager.Instance.DecimalPlacesNumber));
        }

        #endregion

        #region Event

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Commands

        /// <summary>
        /// Gets or sets the go to entity command
        /// Is used to pass the command to the context menu
        /// </summary>
        public ICommand GoToCommand { get; set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets or sets the display type of the template.
        /// </summary>
        /// <value>
        /// The display type of the template.
        /// </value>
        public DisplayTemplateType DisplayTemplateType
        {
            get
            {
                return this.displayTemplatetype;
            }

            set
            {
                this.displayTemplatetype = value;
                RaisePropertyChangeEvent("DisplayTemplateType");
            }
        }

        /// <summary>
        /// Gets or sets the header.
        /// </summary>
        /// <value>
        /// The header.
        /// </value>
        public string Header
        {
            get
            {
                return this.header;
            }

            set
            {
                this.header = value;
                RaisePropertyChangeEvent("Header");
            }
        }

        /// <summary>
        /// Gets or sets the sorting option.
        /// True = ascending sort,
        /// False = descending sort,
        /// Null = no sorting applied yet
        /// </summary>       
        public bool? IsSortedAscending { get; set; }

        /// <summary>
        /// Gets or sets the group.
        /// </summary>
        /// <value>
        /// The group.
        /// </value>
        public GroupHeader Group
        {
            get
            {
                return this.group;
            }

            set
            {
                this.group = value;
                RaisePropertyChangeEvent("Group");
            }
        }

        /// <summary>
        /// Gets or sets my property.
        /// </summary>
        /// <value>
        /// My property.
        /// </value>
        public ObservableCollection<object> ComparedRowItems { get; set; }

        /// <summary>
        /// Gets or sets the symbol format.
        /// </summary>
        /// <value>
        /// The symbol format.
        /// </value>
        public string SymbolFormat { get; set; }

        /// <summary>
        /// Gets or sets the type of the unit.
        /// </summary>
        /// <value>
        /// The type of the unit.
        /// </value>
        public UnitType UnitType { get; set; }

        /// <summary>
        /// Gets or sets the max digits to display.
        /// </summary>
        /// <value>
        /// The max digits to display.
        /// </value>
        public int? MaxDigitsToDisplay { get; set; }

        /// <summary>
        /// Gets or sets the symbol.
        /// </summary>
        /// <value>
        /// The symbol.
        /// </value>
        public string Symbol { get; set; }

        /// <summary>
        /// Gets or sets the type of the collection of compared items.
        /// </summary>
        /// <value>
        /// The type of items.
        /// </value>
        public Type Type { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is highlighted.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is highlighted; otherwise, <c>false</c>.
        /// </value>
        public bool IsHighlighted
        {
            get
            {
                return this.isHighlighted;
            }

            set
            {
                this.isHighlighted = value;
                RaisePropertyChangeEvent("IsHighlighted");
            }
        }

        /// <summary>
        /// Gets or sets the column headers.
        /// </summary>
        /// <value>
        /// The column headers.
        /// </value>
        public ObservableCollection<CompareHeaderItem> ColumnHeaders
        {
            get
            {
                return this.colomnHeaders;
            }

            set
            {
                this.colomnHeaders = value;
                RaisePropertyChangeEvent("ColumnHeaders");
            }
        }

        /// <summary>
        /// Gets or sets the item to compare type for parent entity (ex:cost,property...)
        /// </summary>
        public CompareRowItemType ItemType { get; set; }

        /// <summary>
        /// Gets or sets the cost type for the compared items
        /// </summary>
        public Type ItemCostType { get; set; }

        /// <summary>
        /// Gets or sets the measurement unit adapters corresponding to the compared items
        /// </summary>
        public ObservableCollection<UnitsAdapter> MeasurementUnitsAdapters
        {
            get
            {
                return this.measurementUnitsAdapters;
            }

            set
            {
                this.measurementUnitsAdapters = value;
                this.SetDataGridRowHighlighted(true);
                RaisePropertyChangeEvent("MeasurementUnitsAdapters");
            }
        }

        #endregion

        #region Raise Property Change

        /// <summary>
        /// When a property has been changed, this method notifies the gui to update the contents
        /// </summary>
        /// <param name="propertyName">The name of the property which has been changed</param>
        private void RaisePropertyChangeEvent(string propertyName)
        {
            if (null != PropertyChanged)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// Handles the number of decimal places changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void HandleNumberOfDecimalPlacesChanged(object sender, PropertyChangedEventArgs e)
        {
            SetDataGridRowHighlighted(IsHighlighted);
        }

        #endregion

        /// <summary>
        /// Sets the data grid row items foreground.
        /// </summary>
        /// <param name="isHighlightShown">if set to <c>true</c> [is highlight shown].</param>
        public void SetDataGridRowHighlighted(bool isHighlightShown)
        {
            if (isHighlightShown)
            {
                var first = ComparedRowItems.FirstOrDefault();
                if (first == null)
                {
                    bool areAllNull = ComparedRowItems.All(x => x == null);
                    if (areAllNull)
                    {
                        this.IsHighlighted = false;
                    }
                    else
                    {
                        this.IsHighlighted = true;
                    }
                }
                else
                {
                    // Convert the first value to the value that will be seen on the UI if the item type represents a currency
                    var firstUnitsAdapter = this.MeasurementUnitsAdapters.FirstOrDefault();
                    if (this.UnitType == UnitType.Currency)
                    {
                        first = CurrencyConversionManager.ConvertBaseValueToDisplayValue((decimal)first, firstUnitsAdapter.BaseCurrency.ExchangeRate, firstUnitsAdapter.UICurrency.ConversionFactor);
                    }

                    bool areTheSame = true;
                    for (int i = 0; i < this.ComparedRowItems.Count; i++)
                    {
                        var comparedItem = this.ComparedRowItems[i];
                        if (comparedItem == null)
                        {
                            areTheSame = false;
                        }
                        else
                        {
                            if (this.DisplayTemplateType != ViewModels.DisplayTemplateType.List)
                            {
                                if (this.DisplayTemplateType == ViewModels.DisplayTemplateType.ExtendedLabelControl)
                                {
                                    // Convert the value to compare to the value that will be seen on the UI if the item type represents a currency
                                    var unitsAdapter = this.MeasurementUnitsAdapters[i];
                                    if (this.UnitType == UnitType.Currency)
                                    {
                                        comparedItem = CurrencyConversionManager.ConvertBaseValueToDisplayValue((decimal)comparedItem, unitsAdapter.BaseCurrency.ExchangeRate, unitsAdapter.UICurrency.ConversionFactor);
                                    }

                                    var firstStr = Formatter.FormatNumber(first, UserSettingsManager.Instance.DecimalPlacesNumber);
                                    var comparedItemStr = Formatter.FormatNumber(comparedItem, UserSettingsManager.Instance.DecimalPlacesNumber);

                                    if (!comparedItemStr.Equals(firstStr))
                                    {
                                        areTheSame = false;
                                    }
                                }
                                else
                                {
                                    if (!comparedItem.Equals(first))
                                    {
                                        areTheSame = false;
                                    }
                                }
                            }
                        }
                    }

                    this.IsHighlighted = !areTheSame;
                }
            }
            else
            {
                this.IsHighlighted = false;
            }
        }

        /// <summary>
        /// The Group header class.
        /// </summary>
        public class GroupHeader : INotifyPropertyChanged
        {
            /// <summary>
            /// The row foreground.
            /// </summary>
            private bool isHighlighted;

            /// <summary>
            /// Gets or sets a value indicating whether this instance is expanded.
            /// </summary>
            /// <value>
            /// <c>true</c> if this instance is expanded; otherwise, <c>false</c>.
            /// </value>
            private bool isExpanded;

            /// <summary>
            /// Occurs when a property value changes.
            /// </summary>
            public event PropertyChangedEventHandler PropertyChanged;

            /// <summary>
            /// Gets or sets the name of the group.
            /// </summary>
            /// <value>
            /// The name of the group.
            /// </value>
            public string GroupName { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether this instance is expanded.
            /// </summary>
            /// <value>
            /// <c>true</c> if this instance is expanded; otherwise, <c>false</c>.
            /// </value>
            public bool IsExpanded
            {
                get
                {
                    return this.isExpanded;
                }

                set
                {
                    this.isExpanded = value;
                    RaisePropertyChangeEvent("IsExpanded");
                }
            }

            /// <summary>
            /// Gets or sets a value indicating whether this instance is highlighted.
            /// </summary>
            /// <value>
            /// <c>true</c> if this instance is highlighted; otherwise, <c>false</c>.
            /// </value>
            public bool IsHighlighted
            {
                get
                {
                    return this.isHighlighted;
                }

                set
                {
                    this.isHighlighted = value;
                    RaisePropertyChangeEvent("IsHighlighted");
                }
            }

            /// <summary>
            /// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
            /// </summary>
            /// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
            /// <returns>
            ///   <c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
            /// </returns>
            public override bool Equals(object obj)
            {
                // If parameter is null return false.
                if (obj == null)
                {
                    return false;
                }

                // If parameter cannot be cast to Point return false.
                GroupHeader group = obj as GroupHeader;
                if (group == null)
                {
                    return false;
                }

                return this.GroupName.Equals(group.GroupName);
            }

            /// <summary>
            /// Returns a hash code for this instance.
            /// </summary>
            /// <returns>
            /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
            /// </returns>
            public override int GetHashCode()
            {
                return base.GetHashCode();
            }

            #region Raise Property Change
            /// <summary>
            /// When a property has been changed, this method notifies the gui to update the contents
            /// </summary>
            /// <param name="propertyName">The name of the property which has been changed</param>
            private void RaisePropertyChangeEvent(string propertyName)
            {
                if (null != PropertyChanged)
                {
                    this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
            }
            #endregion
        }
    }
}
