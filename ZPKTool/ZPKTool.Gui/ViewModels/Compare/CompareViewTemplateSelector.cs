﻿using System.Windows;
using System.Windows.Controls;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The compare view template selector.
    /// </summary>
    public class CompareViewTemplateSelector : DataTemplateSelector
    {
        /// <summary>
        /// HACK: in order to prevent the ContentControl to reuse the data template
        /// </summary>
        /// <param name="item">The data object for which to select the template.</param>
        /// <param name="container">The data-bound object.</param>
        /// <returns>
        /// Returns a <see cref="T:System.Windows.DataTemplate"/> or null. The default value is null.
        /// </returns>
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item == null || container == null)
            {
                return null;
            }

            // get the current template
            DataTemplateKey dataTemplateKey = new DataTemplateKey(item.GetType());
            var dataTemplate = ((FrameworkElement)container).TryFindResource(dataTemplateKey);

            if (dataTemplate == null)
            {
                return null;
            }
            
            // wrap the template into another one which is instantiated each time the template is selected
            FrameworkElementFactory frameworkElementFactory = new FrameworkElementFactory(typeof(ContentPresenter));
            frameworkElementFactory.SetValue(ContentPresenter.ContentTemplateProperty, dataTemplate);
            DataTemplate wrappedDataTemplate = new DataTemplate();
            wrappedDataTemplate.VisualTree = frameworkElementFactory;

            return wrappedDataTemplate;
        }
    }
}
