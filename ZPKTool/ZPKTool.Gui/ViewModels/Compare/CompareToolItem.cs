﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using ZPKTool.Data;
using ZPKTool.Gui.Resources;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The compare tool item class.
    /// </summary>
    public class CompareToolItem
    {
        /// <summary>
        /// The entity type.
        /// </summary>
        private Type type;

        /// <summary>
        /// Initializes a new instance of the <see cref="CompareToolItem"/> class.
        /// </summary>
        /// <param name="name">The item name.</param>
        /// <param name="guid">The item GUID.</param>
        /// <param name="type">The item type.</param>
        /// <param name="databaseId">A value indicating the source database of the entity represented by the item.</param>
        public CompareToolItem(string name, Guid guid, Type type, ZPKTool.DataAccess.DbIdentifier databaseId)
        {
            this.Type = type;
            this.Guid = guid;
            this.Name = name;
            this.SourceDb = databaseId;
        }

        /// <summary>
        /// Gets the type of the item.
        /// </summary>        
        public Type Type
        {
            get
            {
                return this.type;
            }

            private set
            {
                if (value == typeof(Project))
                {
                    this.Icon = Images.ProjectIcon;
                }
                else if (value == typeof(Assembly))
                {
                    this.Icon = Images.AssemblyIcon;
                }
                else if (value == typeof(Part))
                {
                    this.Icon = Images.PartIcon;
                }

                this.type = value;
            }
        }

        /// <summary>
        /// Gets or sets the item's GUID.
        /// </summary>
        /// <value>
        /// The GUID of the entity.
        /// </value>
        public Guid Guid { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The entity name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the icon.
        /// </summary>
        /// <value>
        /// The entity icon.
        /// </value>
        public ImageSource Icon { get; set; }

        /// <summary>
        /// Gets or sets the data context of the entity represented by this item.
        /// </summary>
        /// <value>
        /// The entity context.
        /// </value>
        public ZPKTool.DataAccess.DbIdentifier SourceDb { get; set; }
    }
}
