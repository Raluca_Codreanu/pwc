﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The template type selected by the DataGrid template selector.
    /// </summary>
    public enum DisplayTemplateType
    {
        /// <summary>
        /// Text Block display template type.
        /// </summary>
        TextBlock = 1,

        /// <summary>
        /// List display template type.
        /// </summary>
        List = 2,

        /// <summary>
        /// Extended label control display template type.
        /// </summary>
        ExtendedLabelControl = 3,

        /// <summary>
        /// Check Box display template type.
        /// </summary>
        CheckBox = 4,

        /// <summary>
        /// Date display template type.
        /// </summary>
        Date = 5
    }
}
