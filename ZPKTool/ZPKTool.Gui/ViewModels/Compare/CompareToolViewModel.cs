﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using ZPKTool.Data;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view model of the CompareTool that keeps a list of CompareToolItems, which contains information about
    /// the items that can be compared.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class CompareToolViewModel : ViewModel
    {
        #region Fields

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The compared items collection.
        /// </summary>
        private ObservableCollection<CompareToolItem> comparedCollection;

        /// <summary>
        /// The message used to inform the status of the added item.
        /// </summary>
        private string message;

        /// <summary>
        /// Indicates if the popup is opened.
        /// </summary>
        private bool isOpenedPopup;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="CompareToolViewModel"/> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        /// <param name="messenger">The messenger.</param>
        [ImportingConstructor]
        public CompareToolViewModel(IWindowService windowService, IMessenger messenger)
        {
            this.windowService = windowService;
            this.messenger = messenger;

            this.CompareCollection = new ObservableCollection<CompareToolItem>();
            this.CompareCommand = new DelegateCommand(this.Compare, () => this.CompareCollection.Count > 0);
            this.RemoveItemCommand = new DelegateCommand<CompareToolItem>(this.RemoveItem);
            this.ClosePopupCommand = new DelegateCommand(this.ClosePopup);
            this.DropCommand = new DelegateCommand<DragEventArgs>(this.Drop);
            this.DragLeaveCommand = new DelegateCommand<DragEventArgs>(this.DragLeave);
            this.DragOverCommand = new DelegateCommand<DragEventArgs>(this.DragOver);
            this.DragEnterCommand = new DelegateCommand<DragEventArgs>(this.DragEnter);
            this.QueryContinueDragCommand = new DelegateCommand<QueryContinueDragEventArgs>(this.QueryContinueDrag);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the compared items.
        /// </summary>        
        public ObservableCollection<CompareToolItem> CompareCollection
        {
            get
            {
                return this.comparedCollection;
            }

            private set
            {
                if (this.comparedCollection != value)
                {
                    this.comparedCollection = value;
                    OnPropertyChanged(() => this.CompareCollection);
                }
            }
        }

        /// <summary>
        /// Gets or sets the error message displayed beneath the selected items for comparison.
        /// </summary>
        /// <value>
        /// The message.
        /// </value>
        public string Message
        {
            get
            {
                return this.message;
            }

            set
            {
                this.message = value;
                OnPropertyChanged(() => this.Message);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is opened popup.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is opened popup; otherwise, <c>false</c>.
        /// </value>
        public bool IsShowedMessage
        {
            get
            {
                return this.isOpenedPopup;
            }

            set
            {
                if (this.isOpenedPopup != value)
                {
                    this.isOpenedPopup = value;
                    OnPropertyChanged(() => this.IsShowedMessage);
                }
            }
        }

        #endregion

        #region Commands

        /// <summary>
        /// Gets the compare command.
        /// </summary>
        public ICommand CompareCommand { get; private set; }

        /// <summary>
        /// Gets the restart command.
        /// </summary>
        public ICommand RemoveItemCommand { get; private set; }

        /// <summary>
        /// Gets the close popup command.
        /// </summary>
        public ICommand ClosePopupCommand { get; private set; }

        /// <summary>
        /// Gets the drop command.
        /// </summary>
        public ICommand DropCommand { get; private set; }

        /// <summary>
        /// Gets the drag leave command.
        /// </summary>
        public ICommand DragLeaveCommand { get; private set; }

        /// <summary>
        /// Gets the drag over command.
        /// </summary>
        public ICommand DragOverCommand { get; private set; }

        /// <summary>
        /// Gets the drag enter command.
        /// </summary>
        public ICommand DragEnterCommand { get; private set; }

        /// <summary>
        /// Gets the query continue drag command.
        /// </summary>
        public ICommand QueryContinueDragCommand { get; private set; }

        #endregion

        #region Compare

        /// <summary>
        /// Compares the entities included in the collection that contains the information about the items selected for comparison.
        /// </summary>
        private void Compare()
        {
            // Notify the main view to show the compare view.
            this.messenger.Send(new NotificationMessage(Notification.DisplayCompareView));
            this.windowService.CloseViewWindow(this);
            this.CompareCollection.Clear();
            this.IsShowedMessage = false;
        }

        #endregion

        #region Add/ Remove item from the list of compared items

        /// <summary>
        /// Removes the item from the compare collection.
        /// </summary>
        /// <param name="compareItem">The compare item.</param>
        private void RemoveItem(CompareToolItem compareItem)
        {
            this.CompareCollection.Remove(compareItem);
            this.IsShowedMessage = false;
        }

        /// <summary>
        /// Adds the item to the compared entities list.
        /// </summary>
        /// <param name="entity">The object must be Project,Assembly or Part.</param>
        /// <param name="entitySourceDb">A value indicating the entity's source database.</param>
        public void AddItemToCompare(object entity, ZPKTool.DataAccess.DbIdentifier entitySourceDb)
        {
            if (!(entity is Project
                || entity is Assembly
                || entity is Part))
            {
                throw new ArgumentException("The entity parameter must be of type Project, Assembly or Part.");
            }

            this.IsShowedMessage = false;
            var firstItem = this.CompareCollection.FirstOrDefault();

            if (firstItem != null && firstItem.Type != entity.GetType())
            {
                this.Message = LocalizedResources.CompareTool_DifferentTypeCompare;
                this.IsShowedMessage = true;
            }

            if (IsShowedMessage == false)
            {
                INameable nameableItem = entity as INameable;
                IIdentifiable identifiableItem = entity as IIdentifiable;

                if (nameableItem != null && identifiableItem != null)
                {
                    CompareToolItem item = new CompareToolItem(nameableItem.Name, identifiableItem.Guid, entity.GetType(), entitySourceDb);

                    if (this.CompareCollection.FirstOrDefault(i => i.Guid == item.Guid) == null)
                    {
                        this.CompareCollection.Add(item);
                    }
                }
            }
        }

        #endregion  Add/ Remove item from the list of compared items

        #region Message Show

        /// <summary>
        /// Closes the popup.
        /// </summary>
        private void ClosePopup()
        {
            this.IsShowedMessage = false;
        }
        #endregion

        #region Drag and Drop

        /// <summary>
        /// Drop action.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        private void Drop(DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(DragAndDropData)))
            {
                DragAndDropData draggedItem = e.Data.GetData(typeof(DragAndDropData)) as DragAndDropData;

                object entity = draggedItem.DataObject;
                var databaseId = draggedItem.DataObjectContext;
                this.AddDropItemToCompare(e, entity, databaseId);
            }
        }

        /// <summary>
        /// Adds the drop item to compare.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        /// <param name="entity">The entity.</param>
        /// <param name="entitySourceDb">A value indicating the entity's source database.</param>
        private void AddDropItemToCompare(DragEventArgs e, object entity, ZPKTool.DataAccess.DbIdentifier entitySourceDb)
        {
            if (entity != null && entitySourceDb != ZPKTool.DataAccess.DbIdentifier.NotSet)
            {
                INameable name = entity as INameable;
                IIdentifiable identifiable = entity as IIdentifiable;

                if (name != null && identifiable != null)
                {
                    this.AddItemToCompare(identifiable, entitySourceDb);
                    e.Effects = DragDropEffects.Copy;
                }
                else
                {
                    e.Effects = DragDropEffects.None;
                }
            }
            else
            {
                e.Effects = DragDropEffects.None;
            }

            e.Handled = true;
        }

        /// <summary>
        /// Drags the leave.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        private void DragLeave(DragEventArgs e)
        {
            e.Effects = DragDropEffects.None;
            e.Handled = true;
        }

        /// <summary>
        /// Drags the over.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        private void DragOver(DragEventArgs e)
        {
            CheckDropTarget(e);
        }

        /// <summary>
        /// Drags the enter.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        private void DragEnter(DragEventArgs e)
        {
            CheckDropTarget(e);
        }

        /// <summary>
        /// Queries the continue drag.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.QueryContinueDragEventArgs"/> instance containing the event data.</param>
        private void QueryContinueDrag(QueryContinueDragEventArgs e)
        {
            if (e.EscapePressed)
            {
                e.Action = DragAction.Cancel;
                e.Handled = true;
            }
        }

        /// <summary>
        /// Checks the drop target.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs" /> instance containing the event data.</param>
        private void CheckDropTarget(DragEventArgs e)
        {
            e.Effects = DragDropEffects.None;

            if (e.Data.GetDataPresent(typeof(DragAndDropData)))
            {
                DragAndDropData draggedItem = e.Data.GetData(typeof(DragAndDropData)) as DragAndDropData;
                if (draggedItem != null && draggedItem.DataObject != null)
                {
                    var dataObject = draggedItem.DataObject;
                    if (dataObject is Assembly
                        || dataObject is Project
                        || dataObject is Part)
                    {
                        e.Effects = DragDropEffects.Copy;
                    }
                }
            }

            e.Handled = true;
        }

        #endregion
    }
}
