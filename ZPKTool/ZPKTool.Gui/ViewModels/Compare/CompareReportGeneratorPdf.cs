﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using ZPKTool.Common;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Creates report in pdf for compared items.
    /// </summary>
    public class CompareReportGeneratorPdf : CompareReportGenerator
    {
        #region Fields

        /// <summary>
        /// Small font.
        /// </summary>
        private readonly iTextSharp.text.Font pdfReportFontSmall = FontFactory.GetFont(Constants.PdfReportsFont, 8, iTextSharp.text.Font.NORMAL);

        /// <summary>
        /// Small red font.
        /// </summary>
        private readonly iTextSharp.text.Font pdfReportFontSmallRed = FontFactory.GetFont(Constants.PdfReportsFont, 8, iTextSharp.text.Font.NORMAL, Color.RED);

        /// <summary>
        /// Small bold font.
        /// </summary>
        private readonly iTextSharp.text.Font pdfReportFontSmallBold = FontFactory.GetFont(Constants.PdfReportsFont, 8, iTextSharp.text.Font.BOLD);

        /// <summary>
        /// Small bold red font.
        /// </summary>
        private readonly iTextSharp.text.Font pdfReportFontSmallBoldRed = FontFactory.GetFont(Constants.PdfReportsFont, 8, iTextSharp.text.Font.BOLD, Color.RED);

        /// <summary>
        /// Normal small.
        /// </summary>
        private readonly iTextSharp.text.Font pdfReportFont = FontFactory.GetFont(Constants.PdfReportsFont, 10, iTextSharp.text.Font.NORMAL);

        /// <summary>
        /// Bold big font.
        /// </summary>
        private readonly iTextSharp.text.Font pdfReportFontBold = FontFactory.GetFont(Constants.PdfReportsFont, 10, iTextSharp.text.Font.BOLD);

        /// <summary>
        /// Red big font.
        /// </summary>
        private readonly iTextSharp.text.Font pdfReportFontBoldRed = FontFactory.GetFont(Constants.PdfReportsFont, 10, iTextSharp.text.Font.BOLD, Color.RED); 

        #endregion

        #region Constructor
        
        /// <summary>
        /// Initializes a new instance of the <see cref="CompareReportGeneratorPdf"/> class.
        /// </summary>
        /// <param name="data">The data collected.</param>
        /// <param name="filePath">The file path.</param>
        /// <param name="author">The author.</param>
        public CompareReportGeneratorPdf(CompareDataSheet data, string filePath, string author)
            : base(data, filePath, author)
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the document.
        /// </summary>
        /// <value>
        /// The document.
        /// </value>
        private Document Document { get; set; }

        /// <summary>
        /// Gets or sets the PDF writer.
        /// </summary>
        /// <value>
        /// The PDF writer.
        /// </value>
        private PdfWriter PdfWriter { get; set; }

        /// <summary>
        /// Gets or sets the PDF table.
        /// </summary>
        /// <value>
        /// The PDF table.
        /// </value>
        private PdfPTable PdfTable { get; set; } 

        #endregion

        #region Create Document

        /// <summary>
        /// Creates the document.
        /// </summary>
        protected override void CreateDocument()
        {
            this.Document = new Document();

            // Create a writer that listens to the document and directs a PDF-stream to a file.
            this.PdfWriter = PdfWriter.GetInstance(this.Document, new FileStream(this.FilePath, FileMode.Create));
            this.Document.Open();
        } 

        #endregion

        #region Add Summary, Headers, Group Header, Row Header, CellValue with Symbol

        /// <summary>
        /// Adds the summary.
        /// </summary>
        protected override void AddSummary()
        {
            if (this.Document == null)
            {
                throw new InvalidOperationException("Create first the document.");
            }

            this.Document.AddAuthor(this.Author);
        }

        /// <summary>
        /// Adds the headers.
        /// </summary>
        protected override void AddHeaders()
        {
            this.PdfTable = new PdfPTable(this.CompareData.Headers.Count + 1);
            this.PdfTable.WidthPercentage = 100;
            this.PdfTable.KeepTogether = true;

            PdfPCell emptycell = new PdfPCell(new Phrase(string.Empty));
            this.PdfTable.AddCell(emptycell);

            // Add Header of the pdf table  
            foreach (string header in this.CompareData.Headers)
            {
                PdfPCell pdfPCell = new PdfPCell(new Phrase(header, pdfReportFontSmallBold));
                this.PdfTable.AddCell(pdfPCell);
            }
        }

        /// <summary>
        /// Adds the group header.
        /// </summary>
        /// <param name="group">The group.</param>
        protected override void AddGroupHeader(CompareDataGroup group)
        {
            if (this.PdfTable == null)
            {
                throw new InvalidOperationException("Add first the headers.");
            }

            this.PdfTable.DefaultCell.DisableBorderSide(Table.RIGHT_BORDER);

            Font font = group.IsHighlighted ? pdfReportFontBoldRed : pdfReportFontBold;
            this.PdfTable.DefaultCell.PaddingTop = 5;
            this.PdfTable.AddCell(new Phrase(group.Name, font));
            this.PdfTable.DefaultCell.PaddingTop = this.PdfTable.DefaultCell.PaddingBottom;

            this.PdfTable.DefaultCell.DisableBorderSide(Table.LEFT_BORDER);
            for (int index = 1; index < this.CompareData.Headers.Count; index++)
            {
                this.PdfTable.AddCell(string.Empty);
            }

            this.PdfTable.DefaultCell.Border = Table.BOTTOM_BORDER | Table.RIGHT_BORDER | Table.TOP_BORDER;
            this.PdfTable.AddCell(string.Empty);
            this.PdfTable.DefaultCell.Border = Table.BOTTOM_BORDER | Table.LEFT_BORDER | Table.RIGHT_BORDER | Table.TOP_BORDER;
        }

        /// <summary>
        /// Adds the row header.
        /// </summary>
        /// <param name="header">The header.</param>
        /// <param name="isLastInGroup">if set to <c>true</c> [is last in group].</param>
        protected override void AddRowHeader(string header, bool isLastInGroup)
        {
            if (this.PdfTable == null)
            {
                throw new InvalidOperationException("Add first the headers.");
            }

            PdfPCell pdfPCell = new PdfPCell(new Phrase(header, pdfReportFontSmallBold));
            this.PdfTable.AddCell(pdfPCell);
        }

        /// <summary>
        /// Adds the cell value.
        /// </summary>
        /// <param name="cellContent">Content of the cell.</param>
        /// <param name="symbol">The symbol.</param>
        /// <param name="isHighlighted">if set to <c>true</c> [is highlighted].</param>
        /// <param name="isLastGroup">if set to <c>true</c> [is last in group].</param>
        /// <param name="isLastInCompareRow">if set to <c>true</c> [is last in compare row].</param>
        protected override void AddCellValueAndSymbol(object cellContent, string symbol, bool isHighlighted, bool isLastGroup, bool isLastInCompareRow)
        {
            if (this.PdfTable == null)
            {
                throw new InvalidOperationException("Add first the headers.");
            }

            string cellCont = cellContent == null ? string.Empty : cellContent.ToString() + " " + symbol;

            Font font = isHighlighted ? pdfReportFontSmallRed : pdfReportFontSmall;
            PdfPCell pdfPCell = new PdfPCell(new Phrase(cellCont, font));
            this.PdfTable.AddCell(pdfPCell);
        }

        #endregion

        #region Group Rows

        /// <summary>
        /// Groups the rows.
        /// </summary>
        /// <param name="isExpanded">if set to <c>true</c> [is expanded].</param>
        protected override void GroupRows(bool isExpanded)
        {
        }

        /// <summary>
        /// Autoes the size columns.
        /// </summary>
        protected override void AutoSizeColumns()
        {
        }

        #endregion  

        #region Write to File

        /// <summary>
        /// Writes to file.
        /// </summary>
        protected override void WriteToFile()
        {
            if (this.Document == null)
            {
                throw new InvalidOperationException("Create first the document.");
            }

            // Add pdf table to the document  
            this.Document.Add(this.PdfTable);
            this.Document.Close();
        } 

        #endregion
    }
}
