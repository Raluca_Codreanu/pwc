﻿using System;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Abstract class Compare Report.
    /// </summary>
    public abstract class CompareReportGenerator
    {
        #region Fields

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="CompareReportGenerator"/> class.
        /// </summary>
        /// <param name="data">The data collected.</param>
        /// <param name="filePath">The file path.</param>
        /// <param name="author">The author.</param>
        public CompareReportGenerator(CompareDataSheet data, string filePath, string author)
        {
            this.CompareData = data;
            this.Author = author;
            this.FilePath = filePath;
        }

        #endregion Constructor

        #region Properties

        /// <summary>
        /// Gets or sets the compare data.
        /// </summary>
        /// <value>
        /// The compare data.
        /// </value>
        protected CompareDataSheet CompareData { get; set; }

        /// <summary>
        /// Gets or sets the author.
        /// </summary>
        /// <value>
        /// The author.
        /// </value>
        protected string Author { get; set; }

        /// <summary>
        /// Gets or sets the file path.
        /// </summary>
        /// <value>
        /// The file path.
        /// </value>
        protected string FilePath { get; set; } 

        #endregion Properties

        #region Create Report

        /// <summary>
        /// Creates the report.
        /// </summary>
        /// <returns>True if the report creation succeded, false otherwise.</returns>
        public bool CreateReport()
        {
            try
            {
                this.CreateDocument();
                this.AddSummary();
                this.AddHeaders();

                foreach (CompareDataGroup group in this.CompareData.GroupedRows)
                {
                    this.AddGroupHeader(group);

                    foreach (CompareDataRow compareRow in group.Rows)
                    {
                        var isLastInGroup = group.Rows.IndexOf(compareRow) == group.Rows.Count - 1;
                        this.AddRowHeader(compareRow.Header, isLastInGroup);

                        for (var comparedDataIndex = 0; comparedDataIndex < compareRow.Items.Count; comparedDataIndex++)
                        {
                            this.AddCellValueAndSymbol(
                                compareRow.Items[comparedDataIndex], 
                                compareRow.Symbol, 
                                compareRow.IsHighlighted, 
                                isLastInGroup,
                                comparedDataIndex == compareRow.Items.Count - 1);
                        }
                    }

                    this.GroupRows(group.IsExpanded);
                }

                this.AutoSizeColumns();
                this.WriteToFile();
            }
            catch (InvalidOperationException)
            {
                throw;
            }
            catch (Exception ex)
            {
                log.ErrorException("Error while exporting compared items.", ex);
                return false;
            }

            return true;
        }

        #endregion Create Report

        #region Abstract methods

        /// <summary>
        /// Creates the document.
        /// </summary>
        protected abstract void CreateDocument();

        /// <summary>
        /// Adds the summary.
        /// </summary>
        protected abstract void AddSummary();

        /// <summary>
        /// Adds the headers.
        /// </summary>
        protected abstract void AddHeaders();

        /// <summary>
        /// Adds the group header.
        /// </summary>
        /// <param name="group">The group.</param>        
        protected abstract void AddGroupHeader(CompareDataGroup group);

        /// <summary>
        /// Adds the row header.
        /// </summary>
        /// <param name="header">The header.</param>
        /// <param name="isLastInGroup">if set to <c>true</c> [is last in group].</param>
        protected abstract void AddRowHeader(string header, bool isLastInGroup);

        /// <summary>
        /// Adds the cell value.
        /// </summary>
        /// <param name="cellContent">Content of the cell.</param>
        /// <param name="symbol">The symbol.</param>
        /// <param name="isHighlighted">if set to <c>true</c> [is highlighted].</param>
        /// <param name="isLastGroup">if set to <c>true</c> [is last in group].</param>
        /// <param name="isLastInCompareRow">if set to <c>true</c> [is last in compare row].</param>
        protected abstract void AddCellValueAndSymbol(object cellContent, string symbol, bool isHighlighted, bool isLastGroup, bool isLastInCompareRow);

        /// <summary>
        /// Groups the rows.
        /// </summary>
        /// <param name="isExpanded">if set to <c>true</c> [is expanded].</param>
        protected abstract void GroupRows(bool isExpanded);

        /// <summary>
        /// Autoes the size columns.
        /// </summary>
        protected abstract void AutoSizeColumns();

        /// <summary>
        /// Writes to file.
        /// </summary>
        protected abstract void WriteToFile();

        #endregion
    }
}
