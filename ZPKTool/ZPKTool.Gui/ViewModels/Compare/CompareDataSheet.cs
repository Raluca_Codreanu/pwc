﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Compare Data used for export to pdf/xls.
    /// </summary>
    public class CompareDataSheet
    {
        /// <summary>
        /// Gets or sets the headers collected.
        /// </summary>
        /// <value>
        /// The headers collected.
        /// </value>
        public StringCollection Headers { get; set; }

        /// <summary>
        /// Gets or sets the grouped rows collected.
        /// </summary>
        /// <value>
        /// The grouped rows collected.
        /// </value>
        public List<CompareDataGroup> GroupedRows { get; set; }

        /// <summary>
        /// Gets or sets the not grouped rows collected.
        /// </summary>
        /// <value>
        /// The not grouped rows collected.
        /// </value>
        public List<CompareDataRow> NotGroupedRows { get; set; }   
    } 
}
