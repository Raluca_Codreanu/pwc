﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel.Composition;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using ZPKTool.Business;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Common;
using ZPKTool.Controls;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Reporting;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the Comparison view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class CompareViewModel : ViewModel
    {
        #region Fields

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The service used to create open/save file dialogs.
        /// </summary>
        private IFileDialogService fileDialogService;

        /// <summary>
        /// The open file overlay window witch allows to open the exported PDF or xml file.
        /// </summary>
        private OpenFileOverlayWindow openFileOverlayWindow;

        /// <summary>
        /// The compared row items.
        /// </summary>
        private ObservableCollection<CompareRowPropertyItem> compareCollection;

        /// <summary>
        /// The column headers.
        /// </summary>
        private ObservableCollection<CompareHeaderItem> colomnHeaders;

        /// <summary>
        /// The state of the group headers before the refresh.
        /// </summary>
        private List<CompareRowPropertyItem.GroupHeader> groupHeadersBeforeRefresh = new List<CompareRowPropertyItem.GroupHeader>();

        /// <summary>
        /// True if entity is loading, false otherwise.
        /// </summary>
        private bool isLoadingEntity;

        /// <summary>
        /// The data is loaded.
        /// </summary>
        private bool isLoaded;

        /// <summary>
        /// The model browser helper service.
        /// </summary>
        private IModelBrowserHelperService modelBrowserHelperService;

        /// <summary>
        /// Gets or sets a value indicating whether [are differences highlighted].
        /// </summary>
        /// <value>
        /// <c>true</c> if [are differences highlighted]; otherwise, <c>false</c>.
        /// </value>
        private bool areDifferencesHighlighted;

        /// <summary>
        /// A value indicating whether the enhanced breakdown option is active
        /// When true means that the displayed result is the enhanced breakdown result 
        /// </summary>
        private bool isEnhanceBreakdownActive;

        /// <summary>
        /// A value indicating whether the enhanced breakdown option is available
        /// The enhanced breakdown option is available only for assembly entities
        /// </summary>
        private bool isEnhancedBreakdownEnabled;

        /// <summary>
        /// The calculation results for the entities that are being compared as they were originally calculated
        /// This is the calculation result that is used by default for comparing
        /// </summary>
        private List<CalculationResult> normalCalculationResults;

        /// <summary>
        /// The enhanced calculation result for the entities that are being compared
        /// This calculation result is available only for <see cref="Assembly"/> entities
        /// </summary>
        private List<CalculationResult> enhancedCalculationResults;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        /// <summary>
        /// The measurements units adapters for the entities that are being compared
        /// </summary>
        private ObservableCollection<UnitsAdapter> measurementsUnitsAdapters;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="CompareViewModel"/> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        /// <param name="messenger">The messenger.</param>
        /// <param name="fileDialogService">The file dialog service.</param>
        /// <param name="modelBrowserHelperService">The model browser helper service.</param>
        /// <param name="unitsService">The units service.</param>
        [ImportingConstructor]
        public CompareViewModel(
            IWindowService windowService,
            IMessenger messenger,
            IFileDialogService fileDialogService,
            IModelBrowserHelperService modelBrowserHelperService,
            IUnitsService unitsService)
        {
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("fileDialogService", fileDialogService);
            Argument.IsNotNull("modelBrowserHelperService", modelBrowserHelperService);
            Argument.IsNotNull("unitsService", unitsService);

            this.windowService = windowService;
            this.messenger = messenger;
            this.fileDialogService = fileDialogService;
            this.isLoaded = false;
            this.modelBrowserHelperService = modelBrowserHelperService;
            this.unitsService = unitsService;

            this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(null);

            this.ViewLoadedCommand = new DelegateCommand(this.ViewLoaded);
            this.RefreshCommand = new DelegateCommand(this.RefreshCompare);

            this.CompareCollection = new ObservableCollection<CompareRowPropertyItem>();
            this.CompareToolItemCollection = new ObservableCollection<CompareToolItem>();
            this.CompareEntityAndParentCollection = new ObservableCollection<Tuple<IIdentifiable, IIdentifiable, DbIdentifier>>();
            this.ColumnHeaders = new ObservableCollection<CompareHeaderItem>();

            this.ExportToXlsCommand = new DelegateCommand(this.ExportToXls);
            this.ExportToPdfCommand = new DelegateCommand(this.ExportToPdf);
            this.GoToCommand = new DelegateCommand<object>(this.GoToEntity);
            this.NavigateToCommand = new DelegateCommand<Tuple<object, int>>(this.NavigateToEntity);

            this.AreDifferencesHighlighted = true;
            this.IsEnhanceBreakdownActive = false;
            this.IsEnhancedBreakdownEnabled = false;
            this.normalCalculationResults = null;
            this.enhancedCalculationResults = null;
        }

        #endregion Constructor

        #region Properties

        /// <summary>
        /// Gets or sets the compare tool item collection.
        /// </summary>
        /// <value>
        /// The compare tool item collection.
        /// </value>
        public ObservableCollection<CompareToolItem> CompareToolItemCollection { get; set; }

        /// <summary>
        /// Gets or sets the information necessary for each entity to be compared.        
        /// </summary>
        /// <remarks>
        /// Each item in the collection consists of a triplet:
        ///   - the 1st item of the triplet is the entity to be compared
        ///   - the 2nd item is the project to which the entity belongs
        ///   - the 3rd item represents the data source of the entity
        /// </remarks>
        private ObservableCollection<Tuple<IIdentifiable, IIdentifiable, DbIdentifier>> CompareEntityAndParentCollection { get; set; }

        /// <summary>
        /// Gets or sets the comparison items.
        /// </summary>
        /// <value>
        /// The comparison items.
        /// </value>
        public ObservableCollection<CompareRowPropertyItem> CompareCollection
        {
            get
            {
                return this.compareCollection;
            }

            set
            {
                if (this.compareCollection != value)
                {
                    this.compareCollection = value;
                    OnPropertyChanged(() => this.CompareCollection);
                }
            }
        }

        /// <summary>
        /// Gets or sets the column headers.
        /// </summary>
        /// <value>
        /// The column headers.
        /// </value>
        public ObservableCollection<CompareHeaderItem> ColumnHeaders
        {
            get
            {
                return this.colomnHeaders;
            }

            set
            {
                if (this.colomnHeaders != value)
                {
                    this.colomnHeaders = value;
                    OnPropertyChanged(() => this.ColumnHeaders);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is loading entity.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is loading entity; otherwise, <c>false</c>.
        /// </value>
        public bool IsLoadingEntity
        {
            get
            {
                return this.isLoadingEntity;
            }

            set
            {
                if (this.isLoadingEntity != value)
                {
                    this.isLoadingEntity = value;
                    OnPropertyChanged(() => this.IsLoadingEntity);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the different items should be highlighted.
        /// </summary>
        /// <value>
        /// <c>true</c> if are differences highlighted; otherwise, <c>false</c>.
        /// </value>
        public bool AreDifferencesHighlighted
        {
            get { return this.areDifferencesHighlighted; }
            set { this.SetProperty(ref this.areDifferencesHighlighted, value, () => this.AreDifferencesHighlighted, this.HighlightDifferences); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the enhanced breakdown option is active
        /// When true means that the displayed result is the enhanced breakdown result 
        /// </summary>
        public bool IsEnhanceBreakdownActive
        {
            get { return this.isEnhanceBreakdownActive; }
            set { this.SetProperty(ref this.isEnhanceBreakdownActive, value, () => this.IsEnhanceBreakdownActive, this.ComputeEnhanceBreakdownResults); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the enhanced breakdown option is available or not
        /// The enhanced breakdown option is available only for assembly entities
        /// </summary>
        public bool IsEnhancedBreakdownEnabled
        {
            get { return this.isEnhancedBreakdownEnabled; }
            set { this.SetProperty(ref this.isEnhancedBreakdownEnabled, value, () => this.IsEnhancedBreakdownEnabled); }
        }

        /// <summary>
        /// Gets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter { get; private set; }

        #endregion Properties

        #region Initialization

        /// <summary>
        /// Initializes the column headers.
        /// </summary>
        private void InitializeProjectColumnHeaders()
        {
            List<CompareHeaderItem> columnheaders = new List<CompareHeaderItem>();

            foreach (Tuple<IIdentifiable, IIdentifiable, DbIdentifier> tuple in CompareEntityAndParentCollection)
            {
                Project proj = tuple.Item1 as Project;
                if (proj != null)
                {
                    CompareHeaderItem header = new CompareHeaderItem();
                    header.Header = proj.Name;
                    header.ParentEntity = proj;
                    columnheaders.Add(header);
                }
            }

            this.ColumnHeaders = new ObservableCollection<CompareHeaderItem>(columnheaders);
        }

        /// <summary>
        /// Initializes the part column headers.
        /// </summary>
        private void InitializePartColumnHeaders()
        {
            List<CompareHeaderItem> columnheaders = new List<CompareHeaderItem>();

            foreach (Tuple<IIdentifiable, IIdentifiable, DbIdentifier> tuple in CompareEntityAndParentCollection)
            {
                Part part = tuple.Item1 as Part;

                if (part != null)
                {
                    CompareHeaderItem header = new CompareHeaderItem();
                    header.Header = part.Name;
                    header.ParentEntity = part;
                    columnheaders.Add(header);
                }
            }

            this.ColumnHeaders = new ObservableCollection<CompareHeaderItem>(columnheaders);
        }

        /// <summary>
        /// Initializes the assembly column headers.
        /// </summary>
        private void InitializeAssemblyColumnHeaders()
        {
            List<CompareHeaderItem> columnheaders = new List<CompareHeaderItem>();

            foreach (Tuple<IIdentifiable, IIdentifiable, DbIdentifier> tuple in CompareEntityAndParentCollection)
            {
                Assembly assy = tuple.Item1 as Assembly;
                if (assy != null)
                {
                    CompareHeaderItem header = new CompareHeaderItem();
                    header.Header = assy.Name;
                    header.ParentEntity = assy;
                    columnheaders.Add(header);
                }
            }

            this.ColumnHeaders = new ObservableCollection<CompareHeaderItem>(columnheaders);
        }

        /// <summary>
        /// Handles the CollectionChanged event of the comparedObjects control.
        /// </summary>
        private void InitializeComparedObjects()
        {
            if (this.CompareEntityAndParentCollection != null && this.CompareEntityAndParentCollection.Count > 0)
            {
                Tuple<IIdentifiable, IIdentifiable, DbIdentifier> firstItem = this.CompareEntityAndParentCollection.FirstOrDefault(p => p.Item1 != null);

                if (firstItem != null)
                {
                    if (firstItem.Item1 != null)
                    {
                        var firstCmparedObj = firstItem.Item1;

                        if (firstCmparedObj is Project)
                        {
                            this.IsEnhancedBreakdownEnabled = false;
                            PopulateProjectsComparison();
                        }
                        else if (firstCmparedObj is Assembly)
                        {
                            this.IsEnhancedBreakdownEnabled = true;
                            PopulateAssembliesComparison();
                        }
                        else if (firstCmparedObj is Part)
                        {
                            this.IsEnhancedBreakdownEnabled = false;
                            PopulatePartsComparison();
                        }
                    }
                }
            }
        }

        #endregion Initialization

        #region Command

        /// <summary>
        /// Gets the view loaded command.
        /// </summary>
        public ICommand ViewLoadedCommand { get; private set; }

        /// <summary>
        /// Gets the refresh command.
        /// </summary>
        public ICommand RefreshCommand { get; private set; }

        /// <summary>
        /// Gets the highlight differences command.
        /// </summary>
        public ICommand HighlightDifferencesCommand { get; private set; }

        /// <summary>
        /// Gets the export to XLS.
        /// </summary>
        public ICommand ExportToXlsCommand { get; private set; }

        /// <summary>
        /// Gets the export to PDF.
        /// </summary>
        public ICommand ExportToPdfCommand { get; private set; }

        /// <summary>
        /// Gets the go to entity command
        /// </summary>
        public ICommand GoToCommand { get; private set; }

        /// <summary>
        /// Gets the navigate to entity command
        /// </summary>
        public ICommand NavigateToCommand { get; private set; }

        #endregion Command

        #region Get Entities

        /// <summary>
        /// Gets the entities.
        /// </summary>
        private void GetEntities()
        {
            this.IsLoadingEntity = true;

            List<Tuple<IIdentifiable, IIdentifiable, DbIdentifier>> entities = new List<Tuple<IIdentifiable, IIdentifiable, DbIdentifier>>();
            List<CompareToolItem> notFoundItems = new List<CompareToolItem>();

            Task.Factory.StartNew(() =>
            {
                IDataSourceManager localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
                IDataSourceManager centralDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

                foreach (CompareToolItem item in this.CompareToolItemCollection)
                {
                    IIdentifiable entity = null;
                    IIdentifiable parentEntity = null;

                    IDataSourceManager dataContext = null;
                    if (item.SourceDb == DbIdentifier.LocalDatabase)
                    {
                        dataContext = localDataManager;
                    }
                    else if (item.SourceDb == DbIdentifier.CentralDatabase)
                    {
                        dataContext = centralDataManager;
                    }

                    if (item.Type == typeof(Project))
                    {
                        entity = dataContext.ProjectRepository.GetProjectIncludingTopLevelChildren(item.Guid);
                    }
                    else if (item.Type == typeof(Assembly))
                    {
                        entity = dataContext.AssemblyRepository.GetAssemblyFull(item.Guid);
                        parentEntity = dataContext.ProjectRepository.GetParentProject(entity);
                    }
                    else if ((item.Type == typeof(Part)) || (item.Type == typeof(RawPart)))
                    {
                        entity = dataContext.PartRepository.GetPartFull(item.Guid);
                        parentEntity = dataContext.ProjectRepository.GetParentProject(entity);
                    }

                    // Check if the entity is deleted permanently or is marked as deleted
                    var trashableEntity = entity as ITrashable;
                    if (entity == null || (trashableEntity != null && trashableEntity.IsDeleted))
                    {
                        notFoundItems.Add(item);
                    }
                    else
                    {
                        Tuple<IIdentifiable, IIdentifiable, DbIdentifier> tuple = new Tuple<IIdentifiable, IIdentifiable, DbIdentifier>(entity, parentEntity, item.SourceDb);
                        entities.Add(tuple);
                    }
                }
            }).ContinueWith(
            (task) =>
            {
                this.IsLoadingEntity = false;

                if (task.Exception != null)
                {
                    Exception error = task.Exception.InnerException;
                    log.ErrorException("Loading entity failed.", error);
                    this.windowService.MessageDialogService.Show(error);
                }

                if (notFoundItems.Count > 0)
                {
                    string notFoundEntityNames = " ";
                    foreach (CompareToolItem item in notFoundItems)
                    {
                        notFoundEntityNames += item.Name.Trim() + ", ";
                        this.CompareToolItemCollection.Remove(item);
                    }

                    if (notFoundEntityNames.Length > 2)
                    {
                        notFoundEntityNames = notFoundEntityNames.Substring(0, notFoundEntityNames.Length - 2);
                        notFoundEntityNames += ".";
                    }

                    this.windowService.MessageDialogService.Show(LocalizedResources.CompareTool_CouldNotFindObjects + notFoundEntityNames, MessageDialogType.Warning);
                }

                this.CompareEntityAndParentCollection.AddRange(entities);

                // Calculate the entities cost
                var comparedObjectsType = entities.FirstOrDefault().Item1.GetType();
                if (comparedObjectsType == typeof(Part) || comparedObjectsType == typeof(RawPart))
                {
                    this.CalculatePartsCalculationResults();
                }
                else if (comparedObjectsType == typeof(Assembly))
                {
                    this.CalculateAssembliesCalculationResults();
                }

                this.InitializeComparedObjects();

                if (groupHeadersBeforeRefresh.Count > 0)
                {
                    // Restore the grouping.
                    foreach (var comparedItem in this.CompareCollection)
                    {
                        var foundHeader = groupHeadersBeforeRefresh.FirstOrDefault(p => p.Equals(comparedItem.Group));
                        if (foundHeader != null)
                        {
                            comparedItem.Group.IsExpanded = foundHeader.IsExpanded;
                        }
                    }
                }
            },
            System.Threading.Tasks.TaskScheduler.FromCurrentSynchronizationContext());
        }

        #endregion Get Entities

        #region Populate Common Part/Assembly Comparison

        /// <summary>
        /// Populates the assembly investment costs group.
        /// </summary>
        /// <param name="calcInvestmentsCosts">The calc investments costs.</param>
        /// <param name="compareItems">The compare items.</param>
        private void PopulateInverstmentCostsGroup(List<InvestmentCost> calcInvestmentsCosts, List<CompareRowPropertyItem> compareItems)
        {
            var invCostsGroup = new CompareRowPropertyItem.GroupHeader();
            invCostsGroup.GroupName = LocalizedResources.General_InvestmentCost;
            invCostsGroup.IsExpanded = false;

            IList machinesInvs = new List<decimal>();
            IList diesInvs = new List<decimal>();

            foreach (InvestmentCost invCost in calcInvestmentsCosts)
            {
                if (invCost != null)
                {
                    machinesInvs.Add(invCost.TotalMachinesInvestment);
                    diesInvs.Add(invCost.TotalDiesInvestment);
                }
                else
                {
                    machinesInvs.Add(0m);
                    diesInvs.Add(0m);
                }
            }

            CompareRowPropertyItem costsSumsRowItem = new CompareRowPropertyItem(
                                                                this.ColumnHeaders,
                                                                LocalizedResources.General_MachineInvestment,
                                                                null,
                                                                invCostsGroup,
                                                                DisplayTemplateType.ExtendedLabelControl,
                                                                machinesInvs,
                                                                typeof(decimal));
            costsSumsRowItem.UnitType = UnitType.Currency;
            costsSumsRowItem.ItemType = CompareRowItemType.Cost;
            costsSumsRowItem.ItemCostType = typeof(InvestmentCost);
            costsSumsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(costsSumsRowItem);

            CompareRowPropertyItem overheadSumsRowItem = new CompareRowPropertyItem(
                                                                this.ColumnHeaders,
                                                               LocalizedResources.General_DieToolingInvestment,
                                                               null,
                                                               invCostsGroup,
                                                               DisplayTemplateType.ExtendedLabelControl,
                                                               diesInvs,
                                                               typeof(decimal));
            overheadSumsRowItem.UnitType = UnitType.Currency;
            overheadSumsRowItem.ItemType = CompareRowItemType.Cost;
            overheadSumsRowItem.ItemCostType = typeof(InvestmentCost);
            overheadSumsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(overheadSumsRowItem);
        }

        /// <summary>
        /// Populates the material costs group.
        /// </summary>
        /// <param name="calcMaterialCosts">The calc material costs.</param>
        /// <param name="compareItems">The compare items.</param>
        private void PopulateMaterialCostsGroup(List<RawMaterialsCost> calcMaterialCosts, List<CompareRowPropertyItem> compareItems)
        {
            var materialCostsGroup = new CompareRowPropertyItem.GroupHeader();
            materialCostsGroup.GroupName = LocalizedResources.General_MaterialCost;
            materialCostsGroup.IsExpanded = false;

            IList rawMatNeCosts = new List<decimal>();
            IList overheadRawMatCosts = new List<decimal>();
            IList wipTotalCost = new List<decimal>();

            foreach (RawMaterialsCost materialCost in calcMaterialCosts)
            {
                if (materialCost != null)
                {
                    rawMatNeCosts.Add(materialCost.NetCostSum);
                    overheadRawMatCosts.Add(materialCost.OverheadSum);
                    wipTotalCost.Add(materialCost.WIPCostSum);
                }
                else
                {
                    rawMatNeCosts.Add(0m);
                    overheadRawMatCosts.Add(0m);
                    wipTotalCost.Add(0m);
                }
            }

            CompareRowPropertyItem costsSumsRowItem = new CompareRowPropertyItem(
                                                            this.ColumnHeaders,
                                                            LocalizedResources.General_SumRawMaterialNetCost,
                                                            null,
                                                            materialCostsGroup,
                                                            DisplayTemplateType.ExtendedLabelControl,
                                                            rawMatNeCosts,
                                                            typeof(decimal));
            costsSumsRowItem.UnitType = UnitType.Currency;
            costsSumsRowItem.ItemType = CompareRowItemType.Cost;
            costsSumsRowItem.ItemCostType = typeof(RawMaterialsCost);
            costsSumsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(costsSumsRowItem);

            CompareRowPropertyItem overheadSumsRowItem = new CompareRowPropertyItem(
                                                                this.ColumnHeaders,
                                                            LocalizedResources.General_SumOverheadOnRawMaterial,
                                                            null,
                                                            materialCostsGroup,
                                                            DisplayTemplateType.ExtendedLabelControl,
                                                            overheadRawMatCosts,
                                                            typeof(decimal));
            overheadSumsRowItem.UnitType = UnitType.Currency;
            overheadSumsRowItem.ItemType = CompareRowItemType.Cost;
            overheadSumsRowItem.ItemCostType = typeof(RawMaterialsCost);
            overheadSumsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(overheadSumsRowItem);

            CompareRowPropertyItem wipSumsRowItem = new CompareRowPropertyItem(
                                                                this.ColumnHeaders,
                                                            LocalizedResources.General_WIPCostTotal,
                                                            null,
                                                            materialCostsGroup,
                                                            DisplayTemplateType.ExtendedLabelControl,
                                                            wipTotalCost,
                                                            typeof(decimal));
            wipSumsRowItem.UnitType = UnitType.Currency;
            wipSumsRowItem.ItemType = CompareRowItemType.Cost;
            wipSumsRowItem.ItemCostType = typeof(RawMaterialsCost);
            wipSumsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(wipSumsRowItem);
        }

        /// <summary>
        /// Populates the assembly consumable costs group.
        /// </summary>
        /// <param name="calcConsumableCosts">The calc consumable costs.</param>
        /// <param name="compareItems">The compare items.</param>
        private void PopulateConsumableCostsGroup(List<ConsumablesCost> calcConsumableCosts, List<CompareRowPropertyItem> compareItems)
        {
            var consumablesCostsGroup = new CompareRowPropertyItem.GroupHeader();
            consumablesCostsGroup.GroupName = LocalizedResources.General_ConsumablesCost;
            consumablesCostsGroup.IsExpanded = false;

            IList costsSums = new List<decimal>();
            IList overheadSums = new List<decimal>();

            foreach (ConsumablesCost consCost in calcConsumableCosts)
            {
                if (consCost != null)
                {
                    costsSums.Add(consCost.CostsSum);
                    overheadSums.Add(consCost.OverheadSum);
                }
                else
                {
                    costsSums.Add(0m);
                    overheadSums.Add(0m);
                }
            }

            CompareRowPropertyItem costsSumsRowItem = new CompareRowPropertyItem(
                                                                this.ColumnHeaders,
                                                                LocalizedResources.General_SumConsumableCosts,
                                                                null,
                                                                consumablesCostsGroup,
                                                                DisplayTemplateType.ExtendedLabelControl,
                                                                costsSums,
                                                                typeof(decimal));
            costsSumsRowItem.UnitType = UnitType.Currency;
            costsSumsRowItem.ItemType = CompareRowItemType.Cost;
            costsSumsRowItem.ItemCostType = typeof(ConsumablesCost);
            costsSumsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(costsSumsRowItem);

            CompareRowPropertyItem overheadSumsRowItem = new CompareRowPropertyItem(
                                                                    this.ColumnHeaders,
                                                                    LocalizedResources.General_SumOverheadOnConsumables,
                                                                    null,
                                                                    consumablesCostsGroup,
                                                                    DisplayTemplateType.ExtendedLabelControl,
                                                                    overheadSums,
                                                                    typeof(decimal));
            overheadSumsRowItem.UnitType = UnitType.Currency;
            overheadSumsRowItem.ItemType = CompareRowItemType.Cost;
            overheadSumsRowItem.ItemCostType = typeof(ConsumablesCost);
            overheadSumsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(overheadSumsRowItem);
        }

        /// <summary>
        /// Populates the assembly commodities costs group.
        /// </summary>
        /// <param name="calcCommodityCosts">The calc commodity costs.</param>
        /// <param name="compareItems">The compare items.</param>
        private void PopulateCommoditiesCostsGroup(List<CommoditiesCost> calcCommodityCosts, List<CompareRowPropertyItem> compareItems)
        {
            var commoditiesCostsGroup = new CompareRowPropertyItem.GroupHeader();
            commoditiesCostsGroup.GroupName = LocalizedResources.General_CommoditiesCost;
            commoditiesCostsGroup.IsExpanded = false;

            var rejectCostSums = new List<decimal>();
            var costsSums = new List<decimal>();
            var overheadSums = new List<decimal>();

            foreach (CommoditiesCost cost in calcCommodityCosts)
            {
                if (cost != null)
                {
                    rejectCostSums.Add(cost.RejectCostSum);
                    costsSums.Add(cost.CostsSum);
                    overheadSums.Add(cost.OverheadSum);
                }
                else
                {
                    rejectCostSums.Add(0m);
                    costsSums.Add(0m);
                    overheadSums.Add(0m);
                }
            }

            CompareRowPropertyItem rejectCostSumsRowItem = new CompareRowPropertyItem(
                this.ColumnHeaders,
                LocalizedResources.General_SumRejectCost,
                null,
                commoditiesCostsGroup,
                DisplayTemplateType.ExtendedLabelControl,
                rejectCostSums,
                typeof(decimal));
            rejectCostSumsRowItem.UnitType = UnitType.Currency;
            rejectCostSumsRowItem.ItemType = CompareRowItemType.Cost;
            rejectCostSumsRowItem.ItemCostType = typeof(CommoditiesCost);
            rejectCostSumsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(rejectCostSumsRowItem);

            CompareRowPropertyItem costsSumsRowItem = new CompareRowPropertyItem(
                this.ColumnHeaders,
                LocalizedResources.General_SumCost,
                null,
                commoditiesCostsGroup,
                DisplayTemplateType.ExtendedLabelControl,
                costsSums,
                typeof(decimal));
            costsSumsRowItem.UnitType = UnitType.Currency;
            costsSumsRowItem.ItemType = CompareRowItemType.Cost;
            costsSumsRowItem.ItemCostType = typeof(CommoditiesCost);
            costsSumsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(costsSumsRowItem);

            CompareRowPropertyItem overheadSumsRowItem = new CompareRowPropertyItem(
                this.ColumnHeaders,
                LocalizedResources.General_SumOverheadCost,
                null,
                commoditiesCostsGroup,
                DisplayTemplateType.ExtendedLabelControl,
                overheadSums,
                typeof(decimal));
            overheadSumsRowItem.UnitType = UnitType.Currency;
            overheadSumsRowItem.ItemType = CompareRowItemType.Cost;
            overheadSumsRowItem.ItemCostType = typeof(CommoditiesCost);
            overheadSumsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(overheadSumsRowItem);
        }

        /// <summary>
        /// Populates the assembly assembling costs.
        /// </summary>
        /// <param name="sourceCalcType">Type of the source calc.</param>
        /// <param name="calcAssemblingCosts">The calc assembling costs.</param>
        /// <param name="compareItems">The compare items.</param>
        private void PopulateAssemblingManufacturingCostsGroup(Type sourceCalcType, List<ProcessCost> calcAssemblingCosts, List<CompareRowPropertyItem> compareItems)
        {
            var costsGroup = new CompareRowPropertyItem.GroupHeader();
            if (sourceCalcType == typeof(Assembly))
            {
                costsGroup.GroupName = LocalizedResources.General_AssemblingCost;
            }
            else if (sourceCalcType == typeof(Part) || sourceCalcType == typeof(RawPart))
            {
                costsGroup.GroupName = LocalizedResources.General_ManufacturingCost;
            }

            costsGroup.IsExpanded = false;

            IList sumMachineEquipmentCosts = new List<decimal>();
            IList sumSetupCosts = new List<decimal>();
            IList sumDirectLabourCosts = new List<decimal>();
            IList sumToolCosts = new List<decimal>();
            IList sumMaintanences = new List<decimal>();
            IList sumAssemblingCosts = new List<decimal>();
            IList sumEstimationAssyCost = new List<decimal>();
            IList sumOhOnAssemblings = new List<decimal>();
            IList sumRejectCosts = new List<decimal>();
            IList sumNTierTransportCosts = new List<decimal>();
            IList totalSumAssemblingCosts = new List<decimal>();

            foreach (ProcessCost procCost in calcAssemblingCosts)
            {
                if (procCost != null)
                {
                    sumMachineEquipmentCosts.Add(procCost.MachineCostSum);
                    sumSetupCosts.Add(procCost.SetupCostSum);
                    sumDirectLabourCosts.Add(procCost.DirectLabourCostSum);
                    sumToolCosts.Add(procCost.ToolAndDieCostSum);
                    sumMaintanences.Add(procCost.DiesMaintenanceCostSum);
                    sumAssemblingCosts.Add(procCost.ManufacturingCostSum);
                    sumOhOnAssemblings.Add(procCost.ManufacturingOverheadSum);
                    sumRejectCosts.Add(procCost.RejectCostSum);
                    sumNTierTransportCosts.Add(procCost.ManufacturingTransportCost);
                    totalSumAssemblingCosts.Add(procCost.TotalManufacturingCostSum);
                    sumEstimationAssyCost.Add(procCost.EstimatedManufacturingCostSum);
                }
                else
                {
                    sumMachineEquipmentCosts.Add(0m);
                    sumSetupCosts.Add(0m);
                    sumDirectLabourCosts.Add(0m);
                    sumToolCosts.Add(0m);
                    sumMaintanences.Add(0m);
                    sumAssemblingCosts.Add(0m);
                    sumOhOnAssemblings.Add(0m);
                    sumRejectCosts.Add(0m);
                    sumNTierTransportCosts.Add(0m);
                    totalSumAssemblingCosts.Add(0m);
                    sumEstimationAssyCost.Add(0m);
                }
            }

            CompareRowPropertyItem sumMachineEquipmentCostsRowItem = new CompareRowPropertyItem(this.ColumnHeaders, LocalizedResources.General_SumMachineEquipmentCost, null, costsGroup, DisplayTemplateType.ExtendedLabelControl, sumMachineEquipmentCosts, typeof(decimal));
            sumMachineEquipmentCostsRowItem.UnitType = UnitType.Currency;
            sumMachineEquipmentCostsRowItem.ItemType = CompareRowItemType.Cost;
            sumMachineEquipmentCostsRowItem.ItemCostType = typeof(ProcessCost);
            sumMachineEquipmentCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(sumMachineEquipmentCostsRowItem);

            CompareRowPropertyItem sumSetupCostsRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                            LocalizedResources.General_SumSetUpCost,
                                                                            null,
                                                                            costsGroup,
                                                                            DisplayTemplateType.ExtendedLabelControl,
                                                                            sumSetupCosts,
                                                                            typeof(decimal));
            sumSetupCostsRowItem.UnitType = UnitType.Currency;
            sumSetupCostsRowItem.ItemType = CompareRowItemType.Cost;
            sumSetupCostsRowItem.ItemCostType = typeof(ProcessCost);
            sumSetupCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(sumSetupCostsRowItem);

            CompareRowPropertyItem sumDirectLabourCostsRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                            LocalizedResources.General_SumDirectLabourCost,
                                                                            null,
                                                                            costsGroup,
                                                                            DisplayTemplateType.ExtendedLabelControl,
                                                                            sumDirectLabourCosts,
                                                                            typeof(decimal));
            sumDirectLabourCostsRowItem.UnitType = UnitType.Currency;
            sumDirectLabourCostsRowItem.ItemType = CompareRowItemType.Cost;
            sumDirectLabourCostsRowItem.ItemCostType = typeof(ProcessCost);
            sumDirectLabourCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(sumDirectLabourCostsRowItem);

            CompareRowPropertyItem sumToolCostsRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                            LocalizedResources.General_SumDieCost,
                                                                            null,
                                                                            costsGroup,
                                                                            DisplayTemplateType.ExtendedLabelControl,
                                                                            sumToolCosts,
                                                                            typeof(decimal));
            sumToolCostsRowItem.UnitType = UnitType.Currency;
            sumToolCostsRowItem.ItemType = CompareRowItemType.Cost;
            sumToolCostsRowItem.ItemCostType = typeof(ProcessCost);
            sumToolCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(sumToolCostsRowItem);

            CompareRowPropertyItem sumMaintanencesRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                            LocalizedResources.General_SumMaintenanceDiesTools,
                                                                            null,
                                                                            costsGroup,
                                                                            DisplayTemplateType.ExtendedLabelControl,
                                                                            sumMaintanences,
                                                                            typeof(decimal));
            sumMaintanencesRowItem.UnitType = UnitType.Currency;
            sumMaintanencesRowItem.ItemType = CompareRowItemType.Cost;
            sumMaintanencesRowItem.ItemCostType = typeof(ProcessCost);
            sumMaintanencesRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(sumMaintanencesRowItem);

            if (sourceCalcType == typeof(Assembly))
            {
                CompareRowPropertyItem sumEstimatedAssyCostsRowItem = new CompareRowPropertyItem(
                                                                           this.ColumnHeaders,
                                                                           LocalizedResources.General_SumEstimatedAssemblingCost,
                                                                           null,
                                                                           costsGroup,
                                                                           DisplayTemplateType.ExtendedLabelControl,
                                                                           sumEstimationAssyCost,
                                                                           typeof(decimal));
                sumEstimatedAssyCostsRowItem.UnitType = UnitType.Currency;
                sumEstimatedAssyCostsRowItem.ItemType = CompareRowItemType.Cost;
                sumEstimatedAssyCostsRowItem.ItemCostType = typeof(ProcessCost);
                sumEstimatedAssyCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
                compareItems.Add(sumEstimatedAssyCostsRowItem);

                CompareRowPropertyItem sumAssemblingCostsRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                            LocalizedResources.General_SumAssemblingCost,
                                                                            null,
                                                                            costsGroup,
                                                                            DisplayTemplateType.ExtendedLabelControl,
                                                                            sumAssemblingCosts,
                                                                            typeof(decimal));
                sumAssemblingCostsRowItem.UnitType = UnitType.Currency;
                sumAssemblingCostsRowItem.ItemType = CompareRowItemType.Cost;
                sumAssemblingCostsRowItem.ItemCostType = typeof(ProcessCost);
                sumAssemblingCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
                compareItems.Add(sumAssemblingCostsRowItem);
            }
            else if (sourceCalcType == typeof(Part) || sourceCalcType == typeof(RawPart))
            {
                CompareRowPropertyItem sumAssemblingCostsRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                            LocalizedResources.General_SumManufacturingCost,
                                                                            null,
                                                                            costsGroup,
                                                                            DisplayTemplateType.ExtendedLabelControl,
                                                                            sumAssemblingCosts,
                                                                            typeof(decimal));
                sumAssemblingCostsRowItem.UnitType = UnitType.Currency;
                sumAssemblingCostsRowItem.ItemType = CompareRowItemType.Cost;
                sumAssemblingCostsRowItem.ItemCostType = typeof(ProcessCost);
                sumAssemblingCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
                compareItems.Add(sumAssemblingCostsRowItem);
            }

            if (sourceCalcType == typeof(Assembly))
            {
                CompareRowPropertyItem sumOhOnAssemblingsRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                            LocalizedResources.General_SumOHOnAssembling,
                                                                            null,
                                                                            costsGroup,
                                                                            DisplayTemplateType.ExtendedLabelControl,
                                                                            sumOhOnAssemblings,
                                                                            typeof(decimal));
                sumOhOnAssemblingsRowItem.UnitType = UnitType.Currency;
                sumOhOnAssemblingsRowItem.ItemType = CompareRowItemType.Cost;
                sumOhOnAssemblingsRowItem.ItemCostType = typeof(ProcessCost);
                sumOhOnAssemblingsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
                compareItems.Add(sumOhOnAssemblingsRowItem);
            }
            else if (sourceCalcType == typeof(Part) || sourceCalcType == typeof(RawPart))
            {
                CompareRowPropertyItem sumOhOnAssemblingsRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                           LocalizedResources.General_SumOHOnManufacturing,
                                                                           null,
                                                                           costsGroup,
                                                                           DisplayTemplateType.ExtendedLabelControl,
                                                                           sumOhOnAssemblings,
                                                                           typeof(decimal));
                sumOhOnAssemblingsRowItem.UnitType = UnitType.Currency;
                sumOhOnAssemblingsRowItem.ItemType = CompareRowItemType.Cost;
                sumOhOnAssemblingsRowItem.ItemCostType = typeof(ProcessCost);
                sumOhOnAssemblingsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
                compareItems.Add(sumOhOnAssemblingsRowItem);
            }

            CompareRowPropertyItem sumRejectCostsRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.General_SumRejectCost,
                                                                        null,
                                                                        costsGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        sumRejectCosts,
                                                                        typeof(decimal));
            sumRejectCostsRowItem.UnitType = UnitType.Currency;
            sumRejectCostsRowItem.ItemType = CompareRowItemType.Cost;
            sumRejectCostsRowItem.ItemCostType = typeof(ProcessCost);
            sumRejectCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(sumRejectCostsRowItem);

            CompareRowPropertyItem sumTransportCostsRowItem = new CompareRowPropertyItem(
                this.ColumnHeaders,
                LocalizedResources.General_NTierTransportCostSum,
                null,
                costsGroup,
                DisplayTemplateType.ExtendedLabelControl,
                sumNTierTransportCosts,
                typeof(decimal));
            sumTransportCostsRowItem.UnitType = UnitType.Currency;
            sumTransportCostsRowItem.ItemType = CompareRowItemType.Cost;
            sumTransportCostsRowItem.ItemCostType = typeof(ProcessCost);
            sumTransportCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(sumTransportCostsRowItem);

            if (sourceCalcType == typeof(Assembly))
            {
                CompareRowPropertyItem totalSumAssemblingCostsRowItem = new CompareRowPropertyItem(this.ColumnHeaders, LocalizedResources.General_TotalSumAssemblingCost, null, costsGroup, DisplayTemplateType.ExtendedLabelControl, totalSumAssemblingCosts, typeof(decimal));
                totalSumAssemblingCostsRowItem.UnitType = UnitType.Currency;
                totalSumAssemblingCostsRowItem.ItemType = CompareRowItemType.Cost;
                totalSumAssemblingCostsRowItem.ItemCostType = typeof(ProcessCost);
                totalSumAssemblingCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
                compareItems.Add(totalSumAssemblingCostsRowItem);
            }
            else if (sourceCalcType == typeof(Part) || sourceCalcType == typeof(RawPart))
            {
                CompareRowPropertyItem totalSumAssemblingCostsRowItem = new CompareRowPropertyItem(this.ColumnHeaders, LocalizedResources.General_TotalSumManufacturingCost, null, costsGroup, DisplayTemplateType.ExtendedLabelControl, totalSumAssemblingCosts, typeof(decimal));
                totalSumAssemblingCostsRowItem.UnitType = UnitType.Currency;
                totalSumAssemblingCostsRowItem.ItemType = CompareRowItemType.Cost;
                totalSumAssemblingCostsRowItem.ItemCostType = typeof(ProcessCost);
                totalSumAssemblingCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
                compareItems.Add(totalSumAssemblingCostsRowItem);
            }
        }

        /// <summary>
        /// Populates the assembly calculation result overheads costs.
        /// </summary>
        /// <param name="sourceCalculatorType">Type of the source calculator.</param>
        /// <param name="calculationResultOverheadCosts">The calculation result overhead costs.</param>
        /// <param name="compareItems">The compare items.</param>
        private void PopulateCalculationResultOverheadsCostsGroup(Type sourceCalculatorType, List<OverheadCost> calculationResultOverheadCosts, List<CompareRowPropertyItem> compareItems)
        {
            var overheadAndMarginCostgroup = new CompareRowPropertyItem.GroupHeader();
            overheadAndMarginCostgroup.GroupName = LocalizedResources.General_OverheadAndMarginCost;
            overheadAndMarginCostgroup.IsExpanded = false;

            IList materialsOverheadCosts = new List<decimal>();
            IList consumablesOverheadCosts = new List<decimal>();
            IList commodityOverheadCosts = new List<decimal>();
            IList assemblingOverheadCosts = new List<decimal>();
            IList manufacturingOverheadCots = new List<decimal>();
            IList otherCostOverheadCosts = new List<decimal>();
            IList packagingOverheadCosts = new List<decimal>();
            IList transportAndLogisticOverheadCosts = new List<decimal>();
            IList salesAndAdminOverheadCosts = new List<decimal>();
            IList companySurchargeOverheadCosts = new List<decimal>();
            IList externalWorkOverheadCosts = new List<decimal>();
            IList sumOverheadCosts = new List<decimal>();
            IList materialsMarginCosts = new List<decimal>();
            IList commMarginCosts = new List<decimal>();
            IList consMarginCosts = new List<decimal>();
            IList asseMarginCosts = new List<decimal>();
            IList manufacMarginCosts = new List<decimal>();
            IList externalWorkMarginCosts = new List<decimal>();
            IList sumMarginCosts = new List<decimal>();
            IList totalSumMarginAndOverhead = new List<decimal>();

            foreach (OverheadCost overheadCost in calculationResultOverheadCosts)
            {
                if (overheadCost != null)
                {
                    if (sourceCalculatorType == typeof(Part) || sourceCalculatorType == typeof(RawPart) || this.IsEnhanceBreakdownActive)
                    {
                        materialsOverheadCosts.Add(overheadCost.RawMaterialOverhead);
                        materialsMarginCosts.Add(overheadCost.RawMaterialMargin);
                    }

                    if (sourceCalculatorType == typeof(Assembly))
                    {
                        assemblingOverheadCosts.Add(overheadCost.ManufacturingOverhead);
                        asseMarginCosts.Add(overheadCost.ManufacturingMargin);
                    }
                    else if (sourceCalculatorType == typeof(Part) || sourceCalculatorType == typeof(RawPart))
                    {
                        manufacturingOverheadCots.Add(overheadCost.ManufacturingOverhead);
                        manufacMarginCosts.Add(overheadCost.ManufacturingOverhead);
                    }

                    consumablesOverheadCosts.Add(overheadCost.ConsumableOverhead);
                    commodityOverheadCosts.Add(overheadCost.CommodityOverhead);
                    otherCostOverheadCosts.Add(overheadCost.OtherCostOverhead);
                    packagingOverheadCosts.Add(overheadCost.PackagingOverhead);
                    transportAndLogisticOverheadCosts.Add(overheadCost.LogisticOverhead);
                    salesAndAdminOverheadCosts.Add(overheadCost.SalesAndAdministrationOverhead);
                    companySurchargeOverheadCosts.Add(overheadCost.CompanySurchargeOverhead);
                    externalWorkOverheadCosts.Add(overheadCost.ExternalWorkOverhead);
                    sumOverheadCosts.Add(overheadCost.TotalSumOverhead);
                    commMarginCosts.Add(overheadCost.CommodityMargin);
                    consMarginCosts.Add(overheadCost.ConsumableMargin);
                    externalWorkMarginCosts.Add(overheadCost.ExternalWorkMargin);
                    sumMarginCosts.Add(overheadCost.TotalSumMargin);
                    totalSumMarginAndOverhead.Add(overheadCost.TotalSumOverheadAndMargin);
                }
                else
                {
                    if (sourceCalculatorType == typeof(Part) || sourceCalculatorType == typeof(RawPart) || this.IsEnhanceBreakdownActive)
                    {
                        materialsOverheadCosts.Add(0m);
                        materialsMarginCosts.Add(0m);
                    }

                    if (sourceCalculatorType == typeof(Assembly))
                    {
                        assemblingOverheadCosts.Add(0m);
                        asseMarginCosts.Add(0m);
                    }
                    else if (sourceCalculatorType == typeof(Part) || sourceCalculatorType == typeof(RawPart))
                    {
                        manufacturingOverheadCots.Add(0m);
                        manufacMarginCosts.Add(0m);
                    }

                    consumablesOverheadCosts.Add(0m);
                    commodityOverheadCosts.Add(0m);
                    otherCostOverheadCosts.Add(0m);
                    packagingOverheadCosts.Add(0m);
                    transportAndLogisticOverheadCosts.Add(0m);
                    salesAndAdminOverheadCosts.Add(0m);
                    companySurchargeOverheadCosts.Add(0m);
                    externalWorkOverheadCosts.Add(0m);
                    sumOverheadCosts.Add(0m);
                    commMarginCosts.Add(0m);
                    consMarginCosts.Add(0m);
                    externalWorkMarginCosts.Add(0m);
                    sumMarginCosts.Add(0m);
                    totalSumMarginAndOverhead.Add(0m);
                }
            }

            if (sourceCalculatorType == typeof(Part) || sourceCalculatorType == typeof(RawPart) || this.IsEnhanceBreakdownActive)
            {
                CompareRowPropertyItem materialOverheadCostsRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                            LocalizedResources.OverheadSetting_MaterialOverhead,
                                                                            null,
                                                                            overheadAndMarginCostgroup,
                                                                            DisplayTemplateType.ExtendedLabelControl,
                                                                            materialsOverheadCosts,
                                                                            typeof(decimal));
                materialOverheadCostsRowItem.UnitType = UnitType.Currency;
                materialOverheadCostsRowItem.ItemType = CompareRowItemType.Cost;
                materialOverheadCostsRowItem.ItemCostType = typeof(OverheadCost);
                materialOverheadCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
                compareItems.Add(materialOverheadCostsRowItem);
            }

            CompareRowPropertyItem consumablesOverheadCostsRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.General_OverheadConsumable,
                                                                        null,
                                                                        overheadAndMarginCostgroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        consumablesOverheadCosts,
                                                                        typeof(decimal));
            consumablesOverheadCostsRowItem.UnitType = UnitType.Currency;
            consumablesOverheadCostsRowItem.ItemType = CompareRowItemType.Cost;
            consumablesOverheadCostsRowItem.ItemCostType = typeof(OverheadCost);
            consumablesOverheadCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(consumablesOverheadCostsRowItem);

            CompareRowPropertyItem commodityOverheadCostsRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.General_OverheadCommodity,
                                                                        null,
                                                                        overheadAndMarginCostgroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        commodityOverheadCosts,
                                                                        typeof(decimal));
            commodityOverheadCostsRowItem.UnitType = UnitType.Currency;
            commodityOverheadCostsRowItem.ItemType = CompareRowItemType.Cost;
            commodityOverheadCostsRowItem.ItemCostType = typeof(OverheadCost);
            commodityOverheadCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(commodityOverheadCostsRowItem);

            if (sourceCalculatorType == typeof(Assembly))
            {
                CompareRowPropertyItem assemblingOverheadCostsRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                            LocalizedResources.General_OverheadAssembling,
                                                                            null,
                                                                            overheadAndMarginCostgroup,
                                                                            DisplayTemplateType.ExtendedLabelControl,
                                                                            assemblingOverheadCosts,
                                                                            typeof(decimal));
                assemblingOverheadCostsRowItem.UnitType = UnitType.Currency;
                assemblingOverheadCostsRowItem.ItemType = CompareRowItemType.Cost;
                assemblingOverheadCostsRowItem.ItemCostType = typeof(OverheadCost);
                assemblingOverheadCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
                compareItems.Add(assemblingOverheadCostsRowItem);
            }
            else if (sourceCalculatorType == typeof(Part) || sourceCalculatorType == typeof(RawPart))
            {
                CompareRowPropertyItem manOverheadCostsRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                            LocalizedResources.General_OverheadManufacturingCost,
                                                                            null,
                                                                            overheadAndMarginCostgroup,
                                                                            DisplayTemplateType.ExtendedLabelControl,
                                                                            manufacturingOverheadCots,
                                                                            typeof(decimal));
                manOverheadCostsRowItem.UnitType = UnitType.Currency;
                manOverheadCostsRowItem.ItemType = CompareRowItemType.Cost;
                manOverheadCostsRowItem.ItemCostType = typeof(OverheadCost);
                manOverheadCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
                compareItems.Add(manOverheadCostsRowItem);
            }

            CompareRowPropertyItem otherCostOverheadCostsRowItem = new CompareRowPropertyItem(
                                                                                this.ColumnHeaders,
                                                                              LocalizedResources.General_OverheadOtherCost,
                                                                              null,
                                                                              overheadAndMarginCostgroup,
                                                                              DisplayTemplateType.ExtendedLabelControl,
                                                                              otherCostOverheadCosts,
                                                                              typeof(decimal));
            otherCostOverheadCostsRowItem.UnitType = UnitType.Currency;
            otherCostOverheadCostsRowItem.ItemType = CompareRowItemType.Cost;
            otherCostOverheadCostsRowItem.ItemCostType = typeof(OverheadCost);
            otherCostOverheadCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(otherCostOverheadCostsRowItem);

            CompareRowPropertyItem packagingOverheadCostsRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                            LocalizedResources.General_OverheadPackaging,
                                                                            null,
                                                                            overheadAndMarginCostgroup,
                                                                            DisplayTemplateType.ExtendedLabelControl,
                                                                            packagingOverheadCosts,
                                                                            typeof(decimal));
            packagingOverheadCostsRowItem.UnitType = UnitType.Currency;
            packagingOverheadCostsRowItem.ItemType = CompareRowItemType.Cost;
            packagingOverheadCostsRowItem.ItemCostType = typeof(OverheadCost);
            packagingOverheadCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(packagingOverheadCostsRowItem);

            CompareRowPropertyItem logisticsOverheadCostsRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                             LocalizedResources.General_OverheadLogistics,
                                                                             null,
                                                                             overheadAndMarginCostgroup,
                                                                             DisplayTemplateType.ExtendedLabelControl,
                                                                             transportAndLogisticOverheadCosts,
                                                                             typeof(decimal));
            logisticsOverheadCostsRowItem.UnitType = UnitType.Currency;
            logisticsOverheadCostsRowItem.ItemType = CompareRowItemType.Cost;
            logisticsOverheadCostsRowItem.ItemCostType = typeof(OverheadCost);
            logisticsOverheadCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(logisticsOverheadCostsRowItem);

            CompareRowPropertyItem salesAndAdminOverheadCostsRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                            LocalizedResources.Genaral_OverheadSalesAndAdmin,
                                                                            null,
                                                                            overheadAndMarginCostgroup,
                                                                            DisplayTemplateType.ExtendedLabelControl,
                                                                            salesAndAdminOverheadCosts,
                                                                            typeof(decimal));
            salesAndAdminOverheadCostsRowItem.UnitType = UnitType.Currency;
            salesAndAdminOverheadCostsRowItem.ItemType = CompareRowItemType.Cost;
            salesAndAdminOverheadCostsRowItem.ItemCostType = typeof(OverheadCost);
            salesAndAdminOverheadCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(salesAndAdminOverheadCostsRowItem);

            CompareRowPropertyItem companySurchargeOverheadCostsRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                            LocalizedResources.General_OverheadCompanySurcharge,
                                                                            null,
                                                                            overheadAndMarginCostgroup,
                                                                            DisplayTemplateType.ExtendedLabelControl,
                                                                            companySurchargeOverheadCosts,
                                                                            typeof(decimal));
            companySurchargeOverheadCostsRowItem.UnitType = UnitType.Currency;
            companySurchargeOverheadCostsRowItem.ItemType = CompareRowItemType.Cost;
            companySurchargeOverheadCostsRowItem.ItemCostType = typeof(OverheadCost);
            companySurchargeOverheadCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(companySurchargeOverheadCostsRowItem);

            CompareRowPropertyItem externalWorkOHCostsRowItem = new CompareRowPropertyItem(
                this.ColumnHeaders,
                LocalizedResources.OverheadSetting_ExternalWorkOverhead,
                null,
                overheadAndMarginCostgroup,
                DisplayTemplateType.ExtendedLabelControl,
                externalWorkOverheadCosts,
                typeof(decimal));
            externalWorkOHCostsRowItem.UnitType = UnitType.Currency;
            externalWorkOHCostsRowItem.ItemType = CompareRowItemType.Cost;
            externalWorkOHCostsRowItem.ItemCostType = typeof(OverheadCost);
            externalWorkOHCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(externalWorkOHCostsRowItem);

            CompareRowPropertyItem sumOverheadCostsRowItem = new CompareRowPropertyItem(this.ColumnHeaders, LocalizedResources.General_SumOverheads, null, overheadAndMarginCostgroup, DisplayTemplateType.ExtendedLabelControl, sumOverheadCosts, typeof(decimal));
            sumOverheadCostsRowItem.UnitType = UnitType.Currency;
            sumOverheadCostsRowItem.ItemType = CompareRowItemType.Cost;
            sumOverheadCostsRowItem.ItemCostType = typeof(OverheadCost);
            sumOverheadCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(sumOverheadCostsRowItem);

            if (sourceCalculatorType == typeof(Part) || sourceCalculatorType == typeof(RawPart) || this.IsEnhanceBreakdownActive)
            {
                CompareRowPropertyItem materialMarginCostsRowItem = new CompareRowPropertyItem(this.ColumnHeaders, LocalizedResources.OverheadSetting_MaterialMargin, null, overheadAndMarginCostgroup, DisplayTemplateType.ExtendedLabelControl, materialsMarginCosts, typeof(decimal));
                materialMarginCostsRowItem.UnitType = UnitType.Currency;
                materialMarginCostsRowItem.ItemType = CompareRowItemType.Cost;
                materialMarginCostsRowItem.ItemCostType = typeof(OverheadCost);
                materialMarginCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
                compareItems.Add(materialMarginCostsRowItem);
            }

            CompareRowPropertyItem commMarginCostsRowItem = new CompareRowPropertyItem(this.ColumnHeaders, LocalizedResources.General_MarginCommodity, null, overheadAndMarginCostgroup, DisplayTemplateType.ExtendedLabelControl, commMarginCosts, typeof(decimal));
            commMarginCostsRowItem.UnitType = UnitType.Currency;
            commMarginCostsRowItem.ItemType = CompareRowItemType.Cost;
            commMarginCostsRowItem.ItemCostType = typeof(OverheadCost);
            commMarginCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(commMarginCostsRowItem);

            CompareRowPropertyItem consMarginCostsRowItem = new CompareRowPropertyItem(this.ColumnHeaders, LocalizedResources.General_MarginConsumable, null, overheadAndMarginCostgroup, DisplayTemplateType.ExtendedLabelControl, consMarginCosts, typeof(decimal));
            consMarginCostsRowItem.UnitType = UnitType.Currency;
            consMarginCostsRowItem.ItemType = CompareRowItemType.Cost;
            consMarginCostsRowItem.ItemCostType = typeof(OverheadCost);
            consMarginCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(consMarginCostsRowItem);

            if (sourceCalculatorType == typeof(Assembly))
            {
                CompareRowPropertyItem asseMarginCostsRowItem = new CompareRowPropertyItem(this.ColumnHeaders, LocalizedResources.General_MarginAssembling, null, overheadAndMarginCostgroup, DisplayTemplateType.ExtendedLabelControl, asseMarginCosts, typeof(decimal));
                asseMarginCostsRowItem.UnitType = UnitType.Currency;
                asseMarginCostsRowItem.ItemType = CompareRowItemType.Cost;
                asseMarginCostsRowItem.ItemCostType = typeof(OverheadCost);
                asseMarginCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
                compareItems.Add(asseMarginCostsRowItem);
            }
            else if (sourceCalculatorType == typeof(Part) || sourceCalculatorType == typeof(RawPart))
            {
                CompareRowPropertyItem manMarginCostsRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                            LocalizedResources.General_MarginManufacturingCost,
                                                                            null,
                                                                            overheadAndMarginCostgroup,
                                                                            DisplayTemplateType.ExtendedLabelControl,
                                                                            manufacMarginCosts,
                                                                            typeof(decimal));
                manMarginCostsRowItem.UnitType = UnitType.Currency;
                manMarginCostsRowItem.ItemType = CompareRowItemType.Cost;
                manMarginCostsRowItem.ItemCostType = typeof(OverheadCost);
                manMarginCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
                compareItems.Add(manMarginCostsRowItem);
            }

            CompareRowPropertyItem externalWorkMarginCostsRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                            LocalizedResources.General_MarginExternalWork,
                                                                            null,
                                                                            overheadAndMarginCostgroup,
                                                                            DisplayTemplateType.ExtendedLabelControl,
                                                                            externalWorkMarginCosts,
                                                                            typeof(decimal));
            externalWorkMarginCostsRowItem.UnitType = UnitType.Currency;
            externalWorkMarginCostsRowItem.ItemType = CompareRowItemType.Cost;
            externalWorkMarginCostsRowItem.ItemCostType = typeof(OverheadCost);
            externalWorkMarginCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(externalWorkMarginCostsRowItem);

            CompareRowPropertyItem sumMarginCostsRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                            LocalizedResources.General_SumMargin,
                                                                            null,
                                                                            overheadAndMarginCostgroup,
                                                                            DisplayTemplateType.ExtendedLabelControl,
                                                                            sumMarginCosts,
                                                                            typeof(decimal));
            sumMarginCostsRowItem.UnitType = UnitType.Currency;
            sumMarginCostsRowItem.ItemType = CompareRowItemType.Cost;
            sumMarginCostsRowItem.ItemCostType = typeof(OverheadCost);
            sumMarginCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(sumMarginCostsRowItem);

            CompareRowPropertyItem totalSumMarginAndOverheadRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                           LocalizedResources.General_TotalOverheadAndMargin,
                                                                           null,
                                                                           overheadAndMarginCostgroup,
                                                                           DisplayTemplateType.ExtendedLabelControl,
                                                                           totalSumMarginAndOverhead,
                                                                           typeof(decimal));
            totalSumMarginAndOverheadRowItem.UnitType = UnitType.Currency;
            totalSumMarginAndOverheadRowItem.ItemType = CompareRowItemType.Cost;
            totalSumMarginAndOverheadRowItem.ItemCostType = typeof(OverheadCost);
            totalSumMarginAndOverheadRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(totalSumMarginAndOverheadRowItem);
        }

        /// <summary>
        /// Populates the common calculation result summary group.
        /// Common for assembly and part.
        /// </summary>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="calculationResultSummaries">The calculation result summaries.</param>
        /// <param name="compareItems">The compare items.</param>
        private void PopulateCalculationResultSummaryGroup(Type targetType, List<CalculationResultSummary> calculationResultSummaries, List<CompareRowPropertyItem> compareItems)
        {
            var summaryGroup = new CompareRowPropertyItem.GroupHeader();
            summaryGroup.GroupName = LocalizedResources.General_Cost + " " + LocalizedResources.General_Summary;
            summaryGroup.IsExpanded = true;

            // part
            IList materialsCosts = new List<decimal>();
            IList wipCost = new List<decimal>();
            IList totalManufacturingCost = new List<decimal>();

            // assembly
            IList assemblingCosts = new List<decimal>();
            IList subPartsAssembliesCosts = new List<decimal>();

            // common
            IList rejectCosts = new List<decimal>();
            IList productionCosts = new List<decimal>();
            IList manufacturingTransportCosts = new List<decimal>();
            IList commoditiesCosts = new List<decimal>();
            IList consumablesCosts = new List<decimal>();
            IList developmentCosts = new List<decimal>();
            IList projectRelatedInvests = new List<decimal>();
            IList otherCosts = new List<decimal>();
            IList packagingCosts = new List<decimal>();
            IList logisticCosts = new List<decimal>();
            IList transportCosts = new List<decimal>();
            IList overheadAndMarginCosts = new List<decimal>();
            IList paymentCosts = new List<decimal>();
            IList targetSalesPrice = new List<decimal>();
            IList costsNotes = new List<string>();

            foreach (CalculationResultSummary summary in calculationResultSummaries)
            {
                if (summary != null)
                {
                    if (targetType == typeof(Part) || targetType == typeof(RawPart) || this.IsEnhanceBreakdownActive)
                    {
                        materialsCosts.Add(summary.RawMaterialCost);
                        wipCost.Add(summary.WIPCost);
                    }

                    if (targetType == typeof(Part) || targetType == typeof(RawPart))
                    {
                        totalManufacturingCost.Add(summary.TotalManufacturingCost);
                    }
                    else if (targetType == typeof(Assembly))
                    {
                        assemblingCosts.Add(summary.TotalManufacturingCost);
                        subPartsAssembliesCosts.Add(summary.SubpartsAndSubassembliesCost);
                    }

                    commoditiesCosts.Add(summary.CommoditiesCost);
                    consumablesCosts.Add(summary.ConsumableCost);
                    rejectCosts.Add(summary.ManufacturingRejectCost);
                    manufacturingTransportCosts.Add(summary.ManufacturingTransportCost);
                    productionCosts.Add(summary.ProductionCost);
                    developmentCosts.Add(summary.DevelopmentCost);
                    projectRelatedInvests.Add(summary.ProjectInvest);
                    otherCosts.Add(summary.OtherCost);
                    packagingCosts.Add(summary.PackagingCost);
                    logisticCosts.Add(summary.LogisticCost);
                    transportCosts.Add(summary.TransportCost);
                    overheadAndMarginCosts.Add(summary.OverheadAndMarginCost);
                    paymentCosts.Add(summary.PaymentCost);
                    targetSalesPrice.Add(summary.TargetCost);
                }
                else
                {
                    if (targetType == typeof(Part) || targetType == typeof(RawPart) || this.IsEnhanceBreakdownActive)
                    {
                        materialsCosts.Add(0m);
                        wipCost.Add(0m);
                    }

                    if (targetType == typeof(Part) || targetType == typeof(RawPart))
                    {
                        materialsCosts.Add(0m);
                        totalManufacturingCost.Add(0m);
                        wipCost.Add(0m);
                    }
                    else if (targetType == typeof(Assembly))
                    {
                        assemblingCosts.Add(0m);
                        subPartsAssembliesCosts.Add(0m);
                    }

                    commoditiesCosts.Add(0m);
                    consumablesCosts.Add(0m);
                    rejectCosts.Add(0m);
                    manufacturingTransportCosts.Add(0m);
                    productionCosts.Add(0m);
                    developmentCosts.Add(0m);
                    projectRelatedInvests.Add(0m);
                    otherCosts.Add(0m);
                    packagingCosts.Add(0m);
                    logisticCosts.Add(0m);
                    transportCosts.Add(0m);
                    overheadAndMarginCosts.Add(0m);
                    paymentCosts.Add(0m);
                    targetSalesPrice.Add(0m);
                }
            }

            foreach (Tuple<IIdentifiable, IIdentifiable, DbIdentifier> tuple in CompareEntityAndParentCollection)
            {
                var assy = tuple.Item1 as Assembly;
                if (assy != null)
                {
                    costsNotes.Add(assy.AdditionalCostsNotes);
                }

                var part = tuple.Item1 as Part;
                if (part != null)
                {
                    costsNotes.Add(part.AdditionalCostsNotes);
                }
            }

            if (targetType == typeof(Part) || targetType == typeof(RawPart) || this.IsEnhanceBreakdownActive)
            {
                CompareRowPropertyItem materialsCostsRowItem = new CompareRowPropertyItem(
                                                                    this.ColumnHeaders,
                                                                    LocalizedResources.General_MaterialCost,
                                                                    null,
                                                                    summaryGroup,
                                                                    DisplayTemplateType.ExtendedLabelControl,
                                                                    materialsCosts,
                                                                    typeof(decimal));
                materialsCostsRowItem.UnitType = UnitType.Currency;
                materialsCostsRowItem.ItemType = CompareRowItemType.Cost;
                materialsCostsRowItem.ItemCostType = typeof(CalculationResultSummary);
                materialsCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
                compareItems.Add(materialsCostsRowItem);

                CompareRowPropertyItem wipCostsRowItem = new CompareRowPropertyItem(
                                                                   this.ColumnHeaders,
                                                                   LocalizedResources.General_WIPCost,
                                                                   null,
                                                                   summaryGroup,
                                                                   DisplayTemplateType.ExtendedLabelControl,
                                                                   wipCost,
                                                                   typeof(decimal));
                wipCostsRowItem.UnitType = UnitType.Currency;
                wipCostsRowItem.ItemType = CompareRowItemType.Cost;
                wipCostsRowItem.ItemCostType = typeof(CalculationResultSummary);
                wipCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
                compareItems.Add(wipCostsRowItem);
            }

            CompareRowPropertyItem commoditiesCostsRowItem = new CompareRowPropertyItem(
                                                                    this.ColumnHeaders,
                                                                    LocalizedResources.General_CommoditiesCost,
                                                                    null,
                                                                    summaryGroup,
                                                                    DisplayTemplateType.ExtendedLabelControl,
                                                                    commoditiesCosts,
                                                                    typeof(decimal));
            commoditiesCostsRowItem.UnitType = UnitType.Currency;
            commoditiesCostsRowItem.ItemType = CompareRowItemType.Cost;
            commoditiesCostsRowItem.ItemCostType = typeof(CalculationResultSummary);
            commoditiesCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(commoditiesCostsRowItem);

            CompareRowPropertyItem consumablesCostsRowItem = new CompareRowPropertyItem(
                                                                    this.ColumnHeaders,
                                                                    LocalizedResources.General_ConsumablesCost,
                                                                    null,
                                                                    summaryGroup,
                                                                    DisplayTemplateType.ExtendedLabelControl,
                                                                    consumablesCosts,
                                                                    typeof(decimal));
            consumablesCostsRowItem.UnitType = UnitType.Currency;
            consumablesCostsRowItem.ItemType = CompareRowItemType.Cost;
            consumablesCostsRowItem.ItemCostType = typeof(CalculationResultSummary);
            consumablesCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(consumablesCostsRowItem);

            if (targetType == typeof(Assembly))
            {
                CompareRowPropertyItem assemblingCostsRowItem = new CompareRowPropertyItem(
                                                                    this.ColumnHeaders,
                                                                    LocalizedResources.General_AssemblingCost,
                                                                    null,
                                                                    summaryGroup,
                                                                    DisplayTemplateType.ExtendedLabelControl,
                                                                    assemblingCosts,
                                                                    typeof(decimal));
                assemblingCostsRowItem.UnitType = UnitType.Currency;
                assemblingCostsRowItem.ItemType = CompareRowItemType.Cost;
                assemblingCostsRowItem.ItemCostType = typeof(CalculationResultSummary);
                assemblingCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
                compareItems.Add(assemblingCostsRowItem);
            }
            else if (targetType == typeof(Part) || targetType == typeof(RawPart))
            {
                CompareRowPropertyItem totalManCostsRowItem = new CompareRowPropertyItem(
                                                                    this.ColumnHeaders,
                                                                    LocalizedResources.General_TotalManufacturingCost,
                                                                    null,
                                                                    summaryGroup,
                                                                    DisplayTemplateType.ExtendedLabelControl,
                                                                    totalManufacturingCost,
                                                                    typeof(decimal));
                totalManCostsRowItem.UnitType = UnitType.Currency;
                totalManCostsRowItem.ItemType = CompareRowItemType.Cost;
                totalManCostsRowItem.ItemCostType = typeof(CalculationResultSummary);
                totalManCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
                compareItems.Add(totalManCostsRowItem);
            }

            CompareRowPropertyItem rejectCostsRowItem = new CompareRowPropertyItem(
                                                                this.ColumnHeaders,
                                                                LocalizedResources.General_RejectCost,
                                                                null,
                                                                summaryGroup,
                                                                DisplayTemplateType.ExtendedLabelControl,
                                                                rejectCosts,
                                                                typeof(decimal));
            rejectCostsRowItem.UnitType = UnitType.Currency;
            rejectCostsRowItem.ItemType = CompareRowItemType.Cost;
            rejectCostsRowItem.ItemCostType = typeof(CalculationResultSummary);
            rejectCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(rejectCostsRowItem);

            CompareRowPropertyItem manufTransportCostsRowItem = new CompareRowPropertyItem(
                this.ColumnHeaders,
                LocalizedResources.General_TransportCostManufacturing,
                null,
                summaryGroup,
                DisplayTemplateType.ExtendedLabelControl,
                manufacturingTransportCosts,
                typeof(decimal));
            manufTransportCostsRowItem.UnitType = UnitType.Currency;
            manufTransportCostsRowItem.ItemType = CompareRowItemType.Cost;
            manufTransportCostsRowItem.ItemCostType = typeof(CalculationResultSummary);
            manufTransportCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(manufTransportCostsRowItem);

            CompareRowPropertyItem productionCostsRowItem = new CompareRowPropertyItem(
                                                                    this.ColumnHeaders,
                                                                LocalizedResources.General_ProductionCost,
                                                                null,
                                                                summaryGroup,
                                                                DisplayTemplateType.ExtendedLabelControl,
                                                                productionCosts,
                                                                typeof(decimal));
            productionCostsRowItem.UnitType = UnitType.Currency;
            productionCostsRowItem.ItemType = CompareRowItemType.Cost;
            productionCostsRowItem.ItemCostType = typeof(CalculationResultSummary);
            productionCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(productionCostsRowItem);

            if (targetType == typeof(Assembly))
            {
                CompareRowPropertyItem subPartsAssembliesCostsRowItem = new CompareRowPropertyItem(
                                                                this.ColumnHeaders,
                                                                LocalizedResources.General_SinglePartCost,
                                                                null,
                                                                summaryGroup,
                                                                DisplayTemplateType.ExtendedLabelControl,
                                                                subPartsAssembliesCosts,
                                                                typeof(decimal));
                subPartsAssembliesCostsRowItem.UnitType = UnitType.Currency;
                subPartsAssembliesCostsRowItem.ItemType = CompareRowItemType.Cost;
                subPartsAssembliesCostsRowItem.ItemCostType = typeof(CalculationResultSummary);
                subPartsAssembliesCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
                compareItems.Add(subPartsAssembliesCostsRowItem);
            }

            CompareRowPropertyItem developmentCostsRowItem = new CompareRowPropertyItem(
                                                                this.ColumnHeaders,
                                                                LocalizedResources.General_DevelopmentCost,
                                                                null,
                                                                summaryGroup,
                                                                DisplayTemplateType.ExtendedLabelControl,
                                                                developmentCosts,
                                                                typeof(decimal));
            developmentCostsRowItem.UnitType = UnitType.Currency;
            developmentCostsRowItem.ItemType = CompareRowItemType.Cost;
            developmentCostsRowItem.ItemCostType = typeof(CalculationResultSummary);
            developmentCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(developmentCostsRowItem);

            CompareRowPropertyItem projectRelatedInvestsRowItem = new CompareRowPropertyItem(
                                                                this.ColumnHeaders,
                                                                LocalizedResources.General_ProjectRelatedInvest,
                                                                null,
                                                                summaryGroup,
                                                                DisplayTemplateType.ExtendedLabelControl,
                                                                projectRelatedInvests,
                                                                typeof(decimal));
            projectRelatedInvestsRowItem.UnitType = UnitType.Currency;
            projectRelatedInvestsRowItem.ItemType = CompareRowItemType.Cost;
            projectRelatedInvestsRowItem.ItemCostType = typeof(CalculationResultSummary);
            projectRelatedInvestsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(projectRelatedInvestsRowItem);

            CompareRowPropertyItem otherCostsRowItem = new CompareRowPropertyItem(
                                                                this.ColumnHeaders,
                                                                LocalizedResources.General_OtherCost,
                                                                null,
                                                                summaryGroup,
                                                                DisplayTemplateType.ExtendedLabelControl,
                                                                otherCosts,
                                                                typeof(decimal));
            otherCostsRowItem.UnitType = UnitType.Currency;
            otherCostsRowItem.ItemType = CompareRowItemType.Cost;
            otherCostsRowItem.ItemCostType = typeof(CalculationResultSummary);
            otherCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(otherCostsRowItem);

            CompareRowPropertyItem packagingCostsRowItem = new CompareRowPropertyItem(
                                                                this.ColumnHeaders,
                                                                LocalizedResources.General_CostOfPackaging,
                                                                null,
                                                                summaryGroup,
                                                                DisplayTemplateType.ExtendedLabelControl,
                                                                packagingCosts,
                                                                typeof(decimal));
            packagingCostsRowItem.UnitType = UnitType.Currency;
            packagingCostsRowItem.ItemType = CompareRowItemType.Cost;
            packagingCostsRowItem.ItemCostType = typeof(CalculationResultSummary);
            packagingCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(packagingCostsRowItem);

            CompareRowPropertyItem logisticCostsRowItem = new CompareRowPropertyItem(
                                                          this.ColumnHeaders,
                                                          LocalizedResources.General_LogisticCost,
                                                          null,
                                                          summaryGroup,
                                                          DisplayTemplateType.ExtendedLabelControl,
                                                          logisticCosts,
                                                          typeof(decimal));
            logisticCostsRowItem.UnitType = UnitType.Currency;
            logisticCostsRowItem.ItemType = CompareRowItemType.Cost;
            logisticCostsRowItem.ItemCostType = typeof(CalculationResultSummary);
            logisticCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(logisticCostsRowItem);

            CompareRowPropertyItem transportCostsRowItem = new CompareRowPropertyItem(
                                                           this.ColumnHeaders,
                                                           LocalizedResources.General_TransportCost,
                                                           null,
                                                           summaryGroup,
                                                           DisplayTemplateType.ExtendedLabelControl,
                                                           transportCosts,
                                                           typeof(decimal));
            transportCostsRowItem.UnitType = UnitType.Currency;
            transportCostsRowItem.ItemType = CompareRowItemType.Cost;
            transportCostsRowItem.ItemCostType = typeof(CalculationResultSummary);
            transportCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(transportCostsRowItem);

            CompareRowPropertyItem overheadAndMarginCostsRowItem = new CompareRowPropertyItem(
                                                                    this.ColumnHeaders,
                                                                    LocalizedResources.General_OverheadAndMargin,
                                                                    null,
                                                                    summaryGroup,
                                                                    DisplayTemplateType.ExtendedLabelControl,
                                                                    overheadAndMarginCosts,
                                                                    typeof(decimal));
            overheadAndMarginCostsRowItem.UnitType = UnitType.Currency;
            overheadAndMarginCostsRowItem.ItemType = CompareRowItemType.Cost;
            overheadAndMarginCostsRowItem.ItemCostType = typeof(CalculationResultSummary);
            overheadAndMarginCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(overheadAndMarginCostsRowItem);

            CompareRowPropertyItem paymentCostsRowItem = new CompareRowPropertyItem(
                                                                this.ColumnHeaders,
                                                                LocalizedResources.General_PaymentCost,
                                                                null,
                                                                summaryGroup,
                                                                DisplayTemplateType.ExtendedLabelControl,
                                                                paymentCosts,
                                                                typeof(decimal));
            paymentCostsRowItem.UnitType = UnitType.Currency;
            paymentCostsRowItem.ItemType = CompareRowItemType.Cost;
            paymentCostsRowItem.ItemCostType = typeof(CalculationResultSummary);
            paymentCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(paymentCostsRowItem);

            CompareRowPropertyItem targetSalesPriceRowItem = new CompareRowPropertyItem(
                                                                    this.ColumnHeaders,
                                                                    LocalizedResources.General_TargetSalesPrice,
                                                                    null,
                                                                    summaryGroup,
                                                                    DisplayTemplateType.ExtendedLabelControl,
                                                                    targetSalesPrice,
                                                                    typeof(decimal));
            targetSalesPriceRowItem.UnitType = UnitType.Currency;
            targetSalesPriceRowItem.ItemType = CompareRowItemType.Cost;
            targetSalesPriceRowItem.ItemCostType = typeof(CalculationResultSummary);
            targetSalesPriceRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(targetSalesPriceRowItem);

            CompareRowPropertyItem costsNotesRowItem = new CompareRowPropertyItem(
                                                                             this.ColumnHeaders,
                                                                             LocalizedResources.General_Notes,
                                                                             null,
                                                                             summaryGroup,
                                                                             DisplayTemplateType.TextBlock,
                                                                             costsNotes,
                                                                             typeof(string));
            costsNotesRowItem.ItemType = CompareRowItemType.Property;
            costsNotesRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(costsNotesRowItem);
        }

        #endregion Populate Common Part/Assembly Comparison

        #region Populate Projects Comparison

        /// <summary>
        /// Initializes the projects.
        /// </summary>
        private void PopulateProjectsComparison()
        {
            this.measurementsUnitsAdapters = new ObservableCollection<UnitsAdapter>();

            foreach (var tuple in CompareEntityAndParentCollection)
            {
                Project proj = tuple.Item1 as Project;
                if (proj != null)
                {
                    var dataManager = DataAccessFactory.CreateDataSourceManager(tuple.Item3);
                    ICollection<Currency> currencies = null;
                    Currency baseCurrency = null;

                    CurrencyConversionManager.GetEntityCurrenciesAndBaseCurrency(proj, dataManager, out currencies, out baseCurrency);
                    var unitsAdapter = new UnitsAdapter(dataManager);
                    unitsAdapter.Currencies = currencies;
                    unitsAdapter.BaseCurrency = baseCurrency;
                    this.measurementsUnitsAdapters.Add(unitsAdapter);
                }
            }

            List<CompareRowPropertyItem> compareItems = new List<CompareRowPropertyItem>();

            this.InitializeProjectColumnHeaders();

            this.PopulateProjectGeneralGroup(compareItems);
            this.PopulateProjectSupplierGroup(compareItems);
            this.PopulateProjectSettingsGroup(compareItems);
            this.PopulateProjectOHOverheadAndMarginGroup(compareItems);
            this.PopulateProjectAssembliesGroup(compareItems);
            this.PopulateProjectPartsGroup(compareItems);

            this.CompareCollection = new ObservableCollection<CompareRowPropertyItem>(compareItems);
            this.HighlightDifferences();
        }

        /// <summary>
        /// Populates the project OH group.
        /// </summary>
        /// <param name="compareItems">The compare items.</param>
        private void PopulateProjectOHOverheadAndMarginGroup(List<CompareRowPropertyItem> compareItems)
        {
            var overheadMarginGroup = new CompareRowPropertyItem.GroupHeader();
            overheadMarginGroup.GroupName = LocalizedResources.General_OverheadAndMargin;
            overheadMarginGroup.IsExpanded = false;

            IList materialOverheads = new List<decimal>();
            IList cunsumablesOverheads = new List<decimal>();
            IList commodityOverheads = new List<decimal>();
            IList manufacturingOverheads = new List<decimal>();
            IList packagingOverheads = new List<decimal>();
            IList salesAdministrationOverheads = new List<decimal>();
            IList externalWorkOverheads = new List<decimal>();
            IList otherCostsOverhead = new List<decimal>();
            IList logisticOverheads = new List<decimal>();
            IList companySurchargeOverheads = new List<decimal>();

            IList materialMarginMargins = new List<decimal>();
            IList consumableMargins = new List<decimal>();
            IList commodityMargins = new List<decimal>();
            IList externalWorkMargins = new List<decimal>();
            IList manufacturingMargins = new List<decimal>();

            foreach (Tuple<IIdentifiable, IIdentifiable, DbIdentifier> tuple in CompareEntityAndParentCollection)
            {
                Project proj = tuple.Item1 as Project;

                if (proj != null)
                {
                    if (proj.OverheadSettings != null)
                    {
                        materialOverheads.Add(proj.OverheadSettings.MaterialOverhead);
                        cunsumablesOverheads.Add(proj.OverheadSettings.ConsumableOverhead);
                        commodityOverheads.Add(proj.OverheadSettings.CommodityOverhead);
                        manufacturingOverheads.Add(proj.OverheadSettings.ManufacturingOverhead);
                        packagingOverheads.Add(proj.OverheadSettings.PackagingOHValue);
                        salesAdministrationOverheads.Add(proj.OverheadSettings.SalesAndAdministrationOHValue);
                        externalWorkOverheads.Add(proj.OverheadSettings.ExternalWorkOverhead);
                        otherCostsOverhead.Add(proj.OverheadSettings.OtherCostOHValue);
                        logisticOverheads.Add(proj.OverheadSettings.LogisticOHValue);
                        companySurchargeOverheads.Add(proj.OverheadSettings.CompanySurchargeOverhead);

                        materialMarginMargins.Add(proj.OverheadSettings.MaterialMargin);
                        consumableMargins.Add(proj.OverheadSettings.ConsumableMargin);
                        commodityMargins.Add(proj.OverheadSettings.CommodityMargin);
                        externalWorkMargins.Add(proj.OverheadSettings.ExternalWorkMargin);
                        manufacturingMargins.Add(proj.OverheadSettings.ManufacturingMargin);
                    }
                    else
                    {
                        materialOverheads.Add(0m);
                        cunsumablesOverheads.Add(0m);
                        commodityOverheads.Add(0m);
                        manufacturingOverheads.Add(0m);
                        packagingOverheads.Add(0m);
                        salesAdministrationOverheads.Add(0m);
                        externalWorkOverheads.Add(0m);
                        otherCostsOverhead.Add(0m);
                        logisticOverheads.Add(0m);
                        companySurchargeOverheads.Add(0m);

                        materialMarginMargins.Add(0m);
                        consumableMargins.Add(0m);
                        commodityMargins.Add(0m);
                        externalWorkMargins.Add(0m);
                        manufacturingMargins.Add(0m);
                    }
                }
            }

            CompareRowPropertyItem materialOverheadsRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.OverheadSetting_MaterialOverhead,
                                                                        null,
                                                                        overheadMarginGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        materialOverheads,
                                                                        typeof(decimal));
            materialOverheadsRowItem.UnitType = UnitType.Percentage;
            materialOverheadsRowItem.ItemType = CompareRowItemType.Property;
            materialOverheadsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(materialOverheadsRowItem);

            CompareRowPropertyItem consumablesOverheadsRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.OverheadSetting_ConsumableOverhead,
                                                                        null,
                                                                        overheadMarginGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        cunsumablesOverheads,
                                                                        typeof(decimal));
            consumablesOverheadsRowItem.UnitType = UnitType.Percentage;
            consumablesOverheadsRowItem.ItemType = CompareRowItemType.Property;
            consumablesOverheadsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(consumablesOverheadsRowItem);

            CompareRowPropertyItem commodityOverheadsRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                            LocalizedResources.OverheadSetting_CommodityOverhead,
                                                                            null,
                                                                            overheadMarginGroup,
                                                                            DisplayTemplateType.ExtendedLabelControl,
                                                                            commodityOverheads,
                                                                            typeof(decimal));
            commodityOverheadsRowItem.UnitType = UnitType.Percentage;
            commodityOverheadsRowItem.ItemType = CompareRowItemType.Property;
            commodityOverheadsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(commodityOverheadsRowItem);

            CompareRowPropertyItem manufacturingOverheadsRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                           LocalizedResources.OverheadSetting_ManufacturingOverhead,
                                                                           null,
                                                                           overheadMarginGroup,
                                                                           DisplayTemplateType.ExtendedLabelControl,
                                                                           manufacturingOverheads,
                                                                           typeof(decimal));
            manufacturingOverheadsRowItem.UnitType = UnitType.Percentage;
            manufacturingOverheadsRowItem.ItemType = CompareRowItemType.Property;
            manufacturingOverheadsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(manufacturingOverheadsRowItem);

            CompareRowPropertyItem packagingOverheadsRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                            LocalizedResources.OverheadSetting_PackagingOHValue,
                                                                            null,
                                                                            overheadMarginGroup,
                                                                            DisplayTemplateType.ExtendedLabelControl,
                                                                            packagingOverheads,
                                                                            typeof(decimal));
            packagingOverheadsRowItem.UnitType = UnitType.Percentage;
            packagingOverheadsRowItem.ItemType = CompareRowItemType.Property;
            packagingOverheadsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(packagingOverheadsRowItem);

            CompareRowPropertyItem salesAdministrationOverheadsRowItem = new CompareRowPropertyItem(this.ColumnHeaders, LocalizedResources.OverheadSetting_SalesAndAdministrationOHValue, null, overheadMarginGroup, DisplayTemplateType.ExtendedLabelControl, salesAdministrationOverheads, typeof(decimal));
            salesAdministrationOverheadsRowItem.UnitType = UnitType.Percentage;
            salesAdministrationOverheadsRowItem.ItemType = CompareRowItemType.Property;
            salesAdministrationOverheadsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(salesAdministrationOverheadsRowItem);

            CompareRowPropertyItem externalWorkOverheadsRowItem = new CompareRowPropertyItem(this.ColumnHeaders, LocalizedResources.OverheadSetting_ExternalWorkOverhead, null, overheadMarginGroup, DisplayTemplateType.ExtendedLabelControl, externalWorkOverheads, typeof(decimal));
            externalWorkOverheadsRowItem.UnitType = UnitType.Percentage;
            externalWorkOverheadsRowItem.ItemType = CompareRowItemType.Property;
            externalWorkOverheadsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(externalWorkOverheadsRowItem);

            CompareRowPropertyItem otherCostsOverheadRowItem = new CompareRowPropertyItem(this.ColumnHeaders, LocalizedResources.OverheadSetting_OtherCostOHValue, null, overheadMarginGroup, DisplayTemplateType.ExtendedLabelControl, otherCostsOverhead, typeof(decimal));
            otherCostsOverheadRowItem.UnitType = UnitType.Percentage;
            otherCostsOverheadRowItem.ItemType = CompareRowItemType.Property;
            otherCostsOverheadRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(otherCostsOverheadRowItem);

            CompareRowPropertyItem logisticOverheadsRowItem = new CompareRowPropertyItem(this.ColumnHeaders, LocalizedResources.OverheadSetting_LogisticOHValue, null, overheadMarginGroup, DisplayTemplateType.ExtendedLabelControl, logisticOverheads, typeof(decimal));
            logisticOverheadsRowItem.UnitType = UnitType.Percentage;
            logisticOverheadsRowItem.ItemType = CompareRowItemType.Property;
            logisticOverheadsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(logisticOverheadsRowItem);

            CompareRowPropertyItem companySurchargeOverheadsRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.OverheadSettings_CompanySurchargeOH,
                                                                        null,
                                                                        overheadMarginGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        companySurchargeOverheads,
                                                                        typeof(decimal));
            companySurchargeOverheadsRowItem.UnitType = UnitType.Percentage;
            companySurchargeOverheadsRowItem.ItemType = CompareRowItemType.Property;
            companySurchargeOverheadsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(companySurchargeOverheadsRowItem);

            CompareRowPropertyItem materialMarginMarginsRowItem = new CompareRowPropertyItem(this.ColumnHeaders, LocalizedResources.OverheadSetting_MaterialMargin, null, overheadMarginGroup, DisplayTemplateType.ExtendedLabelControl, materialMarginMargins, typeof(decimal));
            materialMarginMarginsRowItem.UnitType = UnitType.Percentage;
            materialMarginMarginsRowItem.ItemType = CompareRowItemType.Property;
            materialMarginMarginsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(materialMarginMarginsRowItem);

            CompareRowPropertyItem consumableMarginsRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.OverheadSetting_ConsumableMargin,
                                                                        null,
                                                                        overheadMarginGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        consumableMargins,
                                                                        typeof(decimal));
            consumableMarginsRowItem.UnitType = UnitType.Percentage;
            consumableMarginsRowItem.ItemType = CompareRowItemType.Property;
            consumableMarginsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(consumableMarginsRowItem);

            CompareRowPropertyItem commodityMarginsRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                      LocalizedResources.OverheadSetting_CommodityMargin,
                                                                      null,
                                                                      overheadMarginGroup,
                                                                      DisplayTemplateType.ExtendedLabelControl,
                                                                      commodityMargins,
                                                                      typeof(decimal));
            commodityMarginsRowItem.UnitType = UnitType.Percentage;
            commodityMarginsRowItem.ItemType = CompareRowItemType.Property;
            commodityMarginsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(commodityMarginsRowItem);

            CompareRowPropertyItem externalWorkMarginsRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.OverheadSetting_ExternalWorkMargin,
                                                                        null,
                                                                        overheadMarginGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        externalWorkMargins,
                                                                        typeof(decimal));
            externalWorkMarginsRowItem.UnitType = UnitType.Percentage;
            externalWorkMarginsRowItem.ItemType = CompareRowItemType.Property;
            externalWorkMarginsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(externalWorkMarginsRowItem);

            CompareRowPropertyItem manufacturingMarginsRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.OverheadSetting_ManufacturingMargin,
                                                                        null,
                                                                        overheadMarginGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        manufacturingMargins,
                                                                        typeof(decimal));
            manufacturingMarginsRowItem.UnitType = UnitType.Percentage;
            manufacturingMarginsRowItem.ItemType = CompareRowItemType.Property;
            manufacturingMarginsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(manufacturingMarginsRowItem);
        }

        /// <summary>
        /// Populates the project settings group.
        /// </summary>
        /// <param name="compareItems">The compare items.</param>
        private void PopulateProjectSettingsGroup(List<CompareRowPropertyItem> compareItems)
        {
            var settingsGroup = new CompareRowPropertyItem.GroupHeader();
            settingsGroup.GroupName = LocalizedResources.General_Settings;
            settingsGroup.IsExpanded = false;

            IList depreciationPeriods = new List<decimal?>();
            IList depreciationRates = new List<decimal?>();
            IList logisticCostRatios = new List<decimal?>();
            IList hoursPerShifts = new List<decimal?>();
            IList shiftsPerWeeks = new List<decimal?>();
            IList productionDaysPerWeeks = new List<decimal?>();
            IList productionWeeksPerYears = new List<decimal?>();

            foreach (Tuple<IIdentifiable, IIdentifiable, DbIdentifier> tuple in CompareEntityAndParentCollection)
            {
                Project proj = tuple.Item1 as Project;

                if (proj != null)
                {
                    depreciationPeriods.Add((decimal?)proj.DepreciationPeriod);
                    depreciationRates.Add(proj.DepreciationRate);
                    logisticCostRatios.Add(proj.LogisticCostRatio);
                    hoursPerShifts.Add(proj.HoursPerShift);
                    shiftsPerWeeks.Add(proj.ShiftsPerWeek);
                    productionDaysPerWeeks.Add(proj.ProductionDaysPerWeek);
                    productionWeeksPerYears.Add(proj.ProductionWeeksPerYear);
                }
            }

            CompareRowPropertyItem depreciationPeriodsRowItem = new CompareRowPropertyItem(
                                                                                this.ColumnHeaders,
                                                                        LocalizedResources.General_DeprPeriod,
                                                                        null,
                                                                        settingsGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        depreciationPeriods,
                                                                        typeof(decimal?));
            depreciationPeriodsRowItem.UnitType = UnitType.None;
            depreciationPeriodsRowItem.MaxDigitsToDisplay = 0;
            depreciationPeriodsRowItem.Symbol = LocalizedResources.General_Years;
            depreciationPeriodsRowItem.ItemType = CompareRowItemType.Property;
            depreciationPeriodsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(depreciationPeriodsRowItem);

            CompareRowPropertyItem depreciationRatesRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.General_DeprRate,
                                                                        null,
                                                                        settingsGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        depreciationRates,
                                                                        typeof(decimal?));
            depreciationRatesRowItem.UnitType = UnitType.Percentage;
            depreciationRatesRowItem.ItemType = CompareRowItemType.Property;
            depreciationRatesRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(depreciationRatesRowItem);

            CompareRowPropertyItem logisticCostRatiosRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.General_LogisticCostRatio,
                                                                        null,
                                                                        settingsGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        logisticCostRatios,
                                                                        typeof(decimal?));
            logisticCostRatiosRowItem.UnitType = UnitType.Percentage;
            logisticCostRatiosRowItem.ItemType = CompareRowItemType.Property;
            logisticCostRatiosRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(logisticCostRatiosRowItem);

            CompareRowPropertyItem hoursPerShiftsRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.General_HoursPerShift,
                                                                        null,
                                                                        settingsGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        hoursPerShifts,
                                                                        typeof(decimal?));
            hoursPerShiftsRowItem.UnitType = UnitType.None;
            hoursPerShiftsRowItem.MaxDigitsToDisplay = 2;
            hoursPerShiftsRowItem.ItemType = CompareRowItemType.Property;
            hoursPerShiftsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(hoursPerShiftsRowItem);

            CompareRowPropertyItem shiftsPerWeeksRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.General_ShiftsPerWeek,
                                                                        null,
                                                                        settingsGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        shiftsPerWeeks,
                                                                        typeof(decimal?));
            shiftsPerWeeksRowItem.UnitType = UnitType.None;
            shiftsPerWeeksRowItem.MaxDigitsToDisplay = 2;
            shiftsPerWeeksRowItem.ItemType = CompareRowItemType.Property;
            shiftsPerWeeksRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(shiftsPerWeeksRowItem);

            CompareRowPropertyItem productionDaysPerWeeksRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.General_ProductionDaysPerWeek,
                                                                        null,
                                                                        settingsGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        productionDaysPerWeeks,
                                                                        typeof(decimal?));
            productionDaysPerWeeksRowItem.UnitType = UnitType.None;
            productionDaysPerWeeksRowItem.MaxDigitsToDisplay = 2;
            productionDaysPerWeeksRowItem.ItemType = CompareRowItemType.Property;
            productionDaysPerWeeksRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(productionDaysPerWeeksRowItem);

            CompareRowPropertyItem productionWeeksPerYearsRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.General_ProductionWeeksPerYear,
                                                                        null,
                                                                        settingsGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        productionWeeksPerYears,
                                                                        typeof(decimal?));
            productionWeeksPerYearsRowItem.UnitType = UnitType.None;
            productionWeeksPerYearsRowItem.MaxDigitsToDisplay = 2;
            productionWeeksPerYearsRowItem.ItemType = CompareRowItemType.Property;
            productionWeeksPerYearsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(productionWeeksPerYearsRowItem);
        }

        /// <summary>
        /// Populates the project supplier group.
        /// </summary>
        /// <param name="compareItems">The compare items.</param>
        private void PopulateProjectSupplierGroup(List<CompareRowPropertyItem> compareItems)
        {
            var supplierGroup = new CompareRowPropertyItem.GroupHeader();
            supplierGroup.GroupName = LocalizedResources.General_Supplier;
            supplierGroup.IsExpanded = false;

            IList names = new List<string>();
            IList types = new List<string>();
            IList numbers = new List<string>();
            IList descriptions = new List<string>();
            IList addresses = new List<string>();
            IList countries = new List<string>();
            IList states = new List<string>();

            foreach (Tuple<IIdentifiable, IIdentifiable, DbIdentifier> tuple in CompareEntityAndParentCollection)
            {
                Project proj = tuple.Item1 as Project;

                if (proj != null)
                {
                    if (proj.Customer != null)
                    {
                        names.Add(proj.Customer.Name);
                        numbers.Add(proj.Customer.Number);

                        if (proj.Customer.Type.HasValue)
                        {
                            types.Add(UIUtils.GetEnumValueLabel((SupplierType)proj.Customer.Type.Value));
                        }
                        else
                        {
                            types.Add(string.Empty);
                        }

                        descriptions.Add(proj.Customer.Description);
                        addresses.Add(proj.Customer.StreetAddress);
                        countries.Add(proj.Customer.Country);
                        states.Add(proj.Customer.State);
                    }
                    else
                    {
                        names.Add(string.Empty);
                        types.Add(string.Empty);
                        numbers.Add(string.Empty);
                        descriptions.Add(string.Empty);
                        addresses.Add(string.Empty);
                        countries.Add(string.Empty);
                        states.Add(string.Empty);
                    }
                }
            }

            CompareRowPropertyItem nameRowItem = new CompareRowPropertyItem(
                                                                    this.ColumnHeaders,
                                                                    LocalizedResources.General_Name,
                                                                    null,
                                                                    supplierGroup,
                                                                    DisplayTemplateType.TextBlock,
                                                                    names,
                                                                    typeof(string));
            nameRowItem.ItemType = CompareRowItemType.Property;
            nameRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(nameRowItem);

            CompareRowPropertyItem numberRowItem = new CompareRowPropertyItem(
                                                                    this.ColumnHeaders,
                                                                    LocalizedResources.General_Number,
                                                                    null,
                                                                    supplierGroup,
                                                                    DisplayTemplateType.TextBlock,
                                                                    numbers,
                                                                    typeof(string));
            numberRowItem.ItemType = CompareRowItemType.Property;
            numberRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(numberRowItem);

            CompareRowPropertyItem typesRowItem = new CompareRowPropertyItem(
                                                                    this.ColumnHeaders,
                                                                    LocalizedResources.General_Type,
                                                                    null,
                                                                    supplierGroup,
                                                                    DisplayTemplateType.TextBlock,
                                                                    types,
                                                                    typeof(string));
            typesRowItem.ItemType = CompareRowItemType.Property;
            typesRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(typesRowItem);

            CompareRowPropertyItem descriptionsRowItem = new CompareRowPropertyItem(
                                                                    this.ColumnHeaders,
                                                                  LocalizedResources.General_Description,
                                                                  null,
                                                                  supplierGroup,
                                                                  DisplayTemplateType.TextBlock,
                                                                  descriptions,
                                                                  typeof(string));
            descriptionsRowItem.ItemType = CompareRowItemType.Property;
            descriptionsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(descriptionsRowItem);

            CompareRowPropertyItem addressesRowItem = new CompareRowPropertyItem(
                                                                    this.ColumnHeaders,
                                                                    LocalizedResources.General_StreetAddress,
                                                                    null,
                                                                    supplierGroup,
                                                                    DisplayTemplateType.TextBlock,
                                                                    addresses,
                                                                    typeof(string));
            addressesRowItem.ItemType = CompareRowItemType.Property;
            addressesRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(addressesRowItem);

            CompareRowPropertyItem countriesRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                   LocalizedResources.General_Country,
                                                                   null,
                                                                   supplierGroup,
                                                                   DisplayTemplateType.TextBlock,
                                                                   countries,
                                                                   typeof(string));
            countriesRowItem.ItemType = CompareRowItemType.Property;
            countriesRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(countriesRowItem);

            CompareRowPropertyItem stateRowItem = new CompareRowPropertyItem(
                                                                    this.ColumnHeaders,
                                                                    LocalizedResources.General_Supplier,
                                                                    null,
                                                                    supplierGroup,
                                                                    DisplayTemplateType.TextBlock,
                                                                    states,
                                                                    typeof(string));
            stateRowItem.ItemType = CompareRowItemType.Property;
            stateRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(stateRowItem);
        }

        /// <summary>
        /// Initializes the project general group.
        /// </summary>
        /// <param name="compareItems">The compare items.</param>
        private void PopulateProjectGeneralGroup(List<CompareRowPropertyItem> compareItems)
        {
            // General Group
            var generalGroup = new CompareRowPropertyItem.GroupHeader();
            generalGroup.GroupName = LocalizedResources.General_General;
            generalGroup.IsExpanded = true;

            IList numbers = new List<string>();
            IList startDates = new List<DateTime?>();
            IList endDates = new List<DateTime?>();
            IList statuses = new List<string>();
            IList partners = new List<string>();
            IList leaders = new List<string>();
            IList respCalcs = new List<string>();
            IList versions = new List<decimal?>();
            IList versionDates = new List<DateTime?>();
            IList yearlyProdQty = new List<decimal?>();
            IList lifeTime = new List<decimal?>();
            IList lifeTimeProdQty = new List<decimal?>();

            foreach (Tuple<IIdentifiable, IIdentifiable, DbIdentifier> tuple in CompareEntityAndParentCollection)
            {
                Project proj = tuple.Item1 as Project;

                if (proj != null)
                {
                    numbers.Add(proj.Number);
                    startDates.Add(proj.StartDate);
                    endDates.Add(proj.EndDate);

                    if (proj.Status.HasValue)
                    {
                        statuses.Add(UIUtils.GetEnumValueLabel((ProjectStatus)proj.Status.Value));
                    }
                    else
                    {
                        statuses.Add(null);
                    }

                    partners.Add(proj.Partner);

                    if (proj.ProjectLeader != null)
                    {
                        leaders.Add(proj.ProjectLeader.Name);
                    }
                    else
                    {
                        leaders.Add(null);
                    }

                    if (proj.ResponsibleCalculator != null)
                    {
                        respCalcs.Add(proj.ResponsibleCalculator.Name);
                    }
                    else
                    {
                        respCalcs.Add(null);
                    }

                    versions.Add(proj.Version);
                    versionDates.Add(proj.VersionDate);
                    yearlyProdQty.Add((decimal?)proj.YearlyProductionQuantity);
                    lifeTime.Add((decimal?)proj.LifeTime);

                    if (proj.YearlyProductionQuantity.HasValue && proj.LifeTime.HasValue)
                    {
                        lifeTimeProdQty.Add((decimal?)(proj.YearlyProductionQuantity.Value * proj.LifeTime.Value));
                    }
                    else
                    {
                        lifeTimeProdQty.Add(null);
                    }
                }
            }

            CompareRowPropertyItem numberRowItem = new CompareRowPropertyItem(
                                                            this.ColumnHeaders,
                                                            LocalizedResources.General_Number,
                                                            null,
                                                            generalGroup,
                                                            DisplayTemplateType.TextBlock,
                                                            numbers,
                                                            typeof(string));
            numberRowItem.ItemType = CompareRowItemType.Property;
            numberRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(numberRowItem);

            CompareRowPropertyItem startDateRowItem = new CompareRowPropertyItem(
                                                            this.ColumnHeaders,
                                                            LocalizedResources.General_StartDate,
                                                            null,
                                                            generalGroup,
                                                            DisplayTemplateType.Date,
                                                            startDates,
                                                            typeof(DateTime));
            startDateRowItem.ItemType = CompareRowItemType.Property;
            startDateRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(startDateRowItem);

            CompareRowPropertyItem endDateRowItem = new CompareRowPropertyItem(
                                                                this.ColumnHeaders,
                                                                LocalizedResources.General_EndDate,
                                                                null,
                                                                generalGroup,
                                                                DisplayTemplateType.Date,
                                                                endDates,
                                                                typeof(DateTime));
            endDateRowItem.ItemType = CompareRowItemType.Property;
            endDateRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(endDateRowItem);

            CompareRowPropertyItem statusesRowItem = new CompareRowPropertyItem(
                                                                this.ColumnHeaders,
                                                               LocalizedResources.General_Status,
                                                               null,
                                                               generalGroup,
                                                               DisplayTemplateType.TextBlock,
                                                               statuses,
                                                               typeof(string));
            statusesRowItem.ItemType = CompareRowItemType.Property;
            statusesRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(statusesRowItem);

            CompareRowPropertyItem partnersRowItem = new CompareRowPropertyItem(
                                                                this.ColumnHeaders,
                                                                LocalizedResources.General_Partner,
                                                                null,
                                                                generalGroup,
                                                                DisplayTemplateType.TextBlock,
                                                                partners,
                                                                typeof(string));
            partnersRowItem.ItemType = CompareRowItemType.Property;
            partnersRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(partnersRowItem);

            CompareRowPropertyItem leadersRowItem = new CompareRowPropertyItem(
                                                                this.ColumnHeaders,
                                                                LocalizedResources.General_Leader,
                                                                null,
                                                                generalGroup,
                                                                DisplayTemplateType.TextBlock,
                                                                leaders,
                                                                typeof(string));
            leadersRowItem.ItemType = CompareRowItemType.Property;
            leadersRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(leadersRowItem);

            CompareRowPropertyItem respCalcRowItem = new CompareRowPropertyItem(
                                                                this.ColumnHeaders,
                                                                LocalizedResources.General_ResCalc,
                                                                null,
                                                                generalGroup,
                                                                DisplayTemplateType.TextBlock,
                                                                respCalcs,
                                                                typeof(string));
            respCalcRowItem.ItemType = CompareRowItemType.Property;
            respCalcRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(respCalcRowItem);

            CompareRowPropertyItem versionsRowItem = new CompareRowPropertyItem(
                                                                    this.ColumnHeaders,
                                                                    LocalizedResources.General_Version,
                                                                    null,
                                                                    generalGroup,
                                                                    DisplayTemplateType.TextBlock,
                                                                    versions,
                                                                    typeof(decimal?));
            versionsRowItem.ItemType = CompareRowItemType.Property;
            versionsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(versionsRowItem);

            CompareRowPropertyItem versionDatesRowItem = new CompareRowPropertyItem(
                                                                    this.ColumnHeaders,
                                                                    LocalizedResources.General_VersionDate,
                                                                    null,
                                                                    generalGroup,
                                                                    DisplayTemplateType.Date,
                                                                    versionDates,
                                                                    typeof(DateTime));
            versionDatesRowItem.ItemType = CompareRowItemType.Property;
            versionDatesRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(versionDatesRowItem);

            CompareRowPropertyItem yearlyProdQtyRowItem = new CompareRowPropertyItem(
                                                                    this.ColumnHeaders,
                                                                    LocalizedResources.General_YearlyProductionQuantity,
                                                                    null,
                                                                    generalGroup,
                                                                    DisplayTemplateType.ExtendedLabelControl,
                                                                    yearlyProdQty,
                                                                    typeof(decimal?));
            yearlyProdQtyRowItem.Symbol = LocalizedResources.General_Pieces;
            yearlyProdQtyRowItem.MaxDigitsToDisplay = 0;
            yearlyProdQtyRowItem.UnitType = UnitType.None;
            yearlyProdQtyRowItem.ItemType = CompareRowItemType.Property;
            yearlyProdQtyRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(yearlyProdQtyRowItem);

            CompareRowPropertyItem lifeTimeRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.General_LifeTime,
                                                                        null,
                                                                        generalGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        lifeTime,
                                                                        typeof(decimal?));
            lifeTimeRowItem.Symbol = LocalizedResources.General_Years;
            lifeTimeRowItem.MaxDigitsToDisplay = 0;
            lifeTimeRowItem.UnitType = UnitType.None;
            lifeTimeRowItem.ItemType = CompareRowItemType.Property;
            lifeTimeRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(lifeTimeRowItem);

            CompareRowPropertyItem lifeTimeProdQtyRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.Production_LifeTimeProdQuantity,
                                                                        null,
                                                                        generalGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        lifeTimeProdQty,
                                                                        typeof(decimal?));
            lifeTimeProdQtyRowItem.Symbol = LocalizedResources.General_Pieces;
            lifeTimeProdQtyRowItem.MaxDigitsToDisplay = 0;
            lifeTimeProdQtyRowItem.UnitType = UnitType.None;
            lifeTimeProdQtyRowItem.ItemType = CompareRowItemType.Property;
            lifeTimeProdQtyRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(lifeTimeProdQtyRowItem);
        }

        /// <summary>
        /// Populates the project assemblies group.
        /// </summary>
        /// <param name="compareItems">The compare items.</param>
        private void PopulateProjectAssembliesGroup(List<CompareRowPropertyItem> compareItems)
        {
            CompareRowPropertyItem.GroupHeader assembliesGroup = new CompareRowPropertyItem.GroupHeader();
            assembliesGroup.GroupName = LocalizedResources.General_Assemblies;
            assembliesGroup.IsExpanded = true;

            IList assemblies = new List<KeyValuePair<object, DbIdentifier>>();

            foreach (Tuple<IIdentifiable, IIdentifiable, DbIdentifier> tuple in CompareEntityAndParentCollection)
            {
                Project proj = tuple.Item1 as Project;
                if (proj != null)
                {
                    assemblies.Add(new KeyValuePair<object, DbIdentifier>(
                        proj.Assemblies.Where(p => !p.IsDeleted).OrderBy(p => p.Index),
                        tuple.Item3));
                }
            }

            CompareRowPropertyItem assembliesRowItem = new CompareRowPropertyItem(
                this.ColumnHeaders,
                string.Empty,
                null,
                assembliesGroup,
                DisplayTemplateType.List,
                assemblies,
                typeof(object));
            assembliesRowItem.GoToCommand = this.GoToCommand;
            assembliesRowItem.ItemType = CompareRowItemType.SubEntity;
            assembliesRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(assembliesRowItem);
        }

        /// <summary>
        /// Populates the project parts group.
        /// </summary>
        /// <param name="compareItems">The compare items.</param>
        private void PopulateProjectPartsGroup(List<CompareRowPropertyItem> compareItems)
        {
            CompareRowPropertyItem.GroupHeader partsGroup = new CompareRowPropertyItem.GroupHeader();
            partsGroup.GroupName = LocalizedResources.General_Parts;
            partsGroup.IsExpanded = true;

            IList parts = new List<KeyValuePair<object, DbIdentifier>>();

            foreach (Tuple<IIdentifiable, IIdentifiable, DbIdentifier> tuple in CompareEntityAndParentCollection)
            {
                Project proj = tuple.Item1 as Project;
                if (proj != null)
                {
                    parts.Add(new KeyValuePair<object, DbIdentifier>(
                        proj.Parts.Where(p => !p.IsDeleted).OrderBy(p => p.Index),
                        tuple.Item3));
                }
            }

            CompareRowPropertyItem partsRowItem = new CompareRowPropertyItem(
                this.ColumnHeaders,
                string.Empty,
                null,
                partsGroup,
                DisplayTemplateType.List,
                parts,
                typeof(object));
            partsRowItem.GoToCommand = this.GoToCommand;
            partsRowItem.ItemType = CompareRowItemType.SubEntity;
            partsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(partsRowItem);
        }

        #endregion Populate Projects Comparison

        #region Populate Assemblies Comparison

        /// <summary>
        /// Initializes the assemblies.
        /// </summary>
        private void PopulateAssembliesComparison()
        {
            List<CompareRowPropertyItem> compareItems = new List<CompareRowPropertyItem>();

            this.InitializeAssemblyColumnHeaders();

            this.PopulateAssemblyGeneralGroup(compareItems);
            this.PopulateAssemblyManufacturerGroup(compareItems);
            this.PopulateAssemblySettingsGroup(compareItems);
            this.PopulateAssemblyOHOverheadAndMarginGroup(compareItems);
            this.PopulateAssemblyCountrySettingsGroup(compareItems);

            List<CalculationResult> calculationResults = null;
            if (this.IsEnhanceBreakdownActive)
            {
                calculationResults = this.enhancedCalculationResults;
            }
            else
            {
                calculationResults = this.normalCalculationResults;
            }

            List<CalculationResultSummary> calcResultSummaries = GetCalculationResultsSummaries(calculationResults);
            this.PopulateCalculationResultSummaryGroup(typeof(Assembly), calcResultSummaries, compareItems);

            List<OverheadCost> calcResultOverheads = GetCalculationResultOverheadCosts(calculationResults);
            this.PopulateCalculationResultOverheadsCostsGroup(typeof(Assembly), calcResultOverheads, compareItems);

            List<ProcessCost> calcAssemblingCosts = GetCalculationResultAssemblingCosts(calculationResults);
            this.PopulateAssemblingManufacturingCostsGroup(typeof(Assembly), calcAssemblingCosts, compareItems);

            List<CommoditiesCost> calcCommodityCosts = GetCalculationResultCommodityCosts(calculationResults);
            this.PopulateCommoditiesCostsGroup(calcCommodityCosts, compareItems);

            List<ConsumablesCost> calcConsumableCosts = GetCalculationResultConsumableCosts(calculationResults);
            this.PopulateConsumableCostsGroup(calcConsumableCosts, compareItems);

            if (this.IsEnhanceBreakdownActive)
            {
                List<RawMaterialsCost> calcMaterialCosts = GetCalculationResultMaterialCosts(calculationResults);
                this.PopulateMaterialCostsGroup(calcMaterialCosts, compareItems);
            }

            List<InvestmentCost> calcInvestmentsCosts = GetCalculationResultInvestmentCosts(calculationResults);
            this.PopulateInverstmentCostsGroup(calcInvestmentsCosts, compareItems);

            this.PopulateSubassembliesGroup(compareItems);
            this.PopulateAssemblyPartsGroup(compareItems);

            this.CompareCollection = new ObservableCollection<CompareRowPropertyItem>(compareItems);
            this.HighlightDifferences();
        }

        /// <summary>
        /// Populates the assembly OH overhead group.
        /// </summary>
        /// <param name="compareItems">The compare items.</param>
        private void PopulateAssemblyOHOverheadAndMarginGroup(List<CompareRowPropertyItem> compareItems)
        {
            var overheadAndMarginGroup = new CompareRowPropertyItem.GroupHeader();
            overheadAndMarginGroup.GroupName = LocalizedResources.General_OverheadAndMargin;
            overheadAndMarginGroup.IsExpanded = false;

            IList materialOverheads = new List<decimal>();
            IList cunsumablesOverheads = new List<decimal>();
            IList commodityOverheads = new List<decimal>();
            IList manufacturingOverheads = new List<decimal>();
            IList packagingOverheads = new List<decimal>();
            IList salesAdministrationOverheads = new List<decimal>();
            IList externalWorkOverheads = new List<decimal>();
            IList otherCostsOverhead = new List<decimal>();
            IList logisticOverheads = new List<decimal>();
            IList companySurchargeOverheads = new List<decimal>();

            IList materialMarginMargins = new List<decimal>();
            IList consumableMargins = new List<decimal>();
            IList commodityMargins = new List<decimal>();
            IList externalWorkMargins = new List<decimal>();
            IList manufacturingMargins = new List<decimal>();

            foreach (Tuple<IIdentifiable, IIdentifiable, DbIdentifier> tuple in CompareEntityAndParentCollection)
            {
                Assembly assy = tuple.Item1 as Assembly;

                if (assy != null)
                {
                    if (assy.OverheadSettings != null)
                    {
                        materialOverheads.Add(assy.OverheadSettings.MaterialOverhead);
                        cunsumablesOverheads.Add(assy.OverheadSettings.ConsumableOverhead);
                        commodityOverheads.Add(assy.OverheadSettings.CommodityOverhead);
                        manufacturingOverheads.Add(assy.OverheadSettings.ManufacturingOverhead);
                        packagingOverheads.Add(assy.OverheadSettings.PackagingOHValue);
                        salesAdministrationOverheads.Add(assy.OverheadSettings.SalesAndAdministrationOHValue);
                        externalWorkOverheads.Add(assy.OverheadSettings.ExternalWorkOverhead);
                        otherCostsOverhead.Add(assy.OverheadSettings.OtherCostOHValue);
                        logisticOverheads.Add(assy.OverheadSettings.LogisticOHValue);
                        companySurchargeOverheads.Add(assy.OverheadSettings.CompanySurchargeOverhead);

                        materialMarginMargins.Add(assy.OverheadSettings.MaterialMargin);
                        consumableMargins.Add(assy.OverheadSettings.ConsumableMargin);
                        commodityMargins.Add(assy.OverheadSettings.CommodityMargin);
                        externalWorkMargins.Add(assy.OverheadSettings.ExternalWorkMargin);
                        manufacturingMargins.Add(assy.OverheadSettings.ManufacturingMargin);
                    }
                    else
                    {
                        materialOverheads.Add(0m);
                        cunsumablesOverheads.Add(0m);
                        commodityOverheads.Add(0m);
                        manufacturingOverheads.Add(0m);
                        packagingOverheads.Add(0m);
                        salesAdministrationOverheads.Add(0m);
                        externalWorkOverheads.Add(0m);
                        otherCostsOverhead.Add(0m);
                        logisticOverheads.Add(0m);
                        companySurchargeOverheads.Add(0m);

                        materialMarginMargins.Add(0m);
                        consumableMargins.Add(0m);
                        commodityMargins.Add(0m);
                        externalWorkMargins.Add(0m);
                        manufacturingMargins.Add(0m);
                    }
                }
            }

            CompareRowPropertyItem materialOverheadsRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.OverheadSetting_MaterialOverhead,
                                                                        null,
                                                                        overheadAndMarginGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        materialOverheads,
                                                                        typeof(decimal));
            materialOverheadsRowItem.UnitType = UnitType.Percentage;
            materialOverheadsRowItem.ItemType = CompareRowItemType.Property;
            materialOverheadsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(materialOverheadsRowItem);

            CompareRowPropertyItem consumablesOverheadsRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.OverheadSetting_ConsumableOverhead,
                                                                        null,
                                                                        overheadAndMarginGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        cunsumablesOverheads,
                                                                        typeof(decimal));
            consumablesOverheadsRowItem.UnitType = UnitType.Percentage;
            consumablesOverheadsRowItem.ItemType = CompareRowItemType.Property;
            consumablesOverheadsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(consumablesOverheadsRowItem);

            CompareRowPropertyItem commodityOverheadsRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.OverheadSetting_CommodityOverhead,
                                                                        null,
                                                                        overheadAndMarginGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        commodityOverheads,
                                                                        typeof(decimal));
            commodityOverheadsRowItem.UnitType = UnitType.Percentage;
            commodityOverheadsRowItem.ItemType = CompareRowItemType.Property;
            commodityOverheadsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(commodityOverheadsRowItem);

            CompareRowPropertyItem manufacturingOverheadsRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                            LocalizedResources.OverheadSetting_ManufacturingOverhead,
                                                                            null,
                                                                            overheadAndMarginGroup,
                                                                            DisplayTemplateType.ExtendedLabelControl,
                                                                            manufacturingOverheads,
                                                                            typeof(decimal));
            manufacturingOverheadsRowItem.UnitType = UnitType.Percentage;
            manufacturingOverheadsRowItem.ItemType = CompareRowItemType.Property;
            manufacturingOverheadsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(manufacturingOverheadsRowItem);

            CompareRowPropertyItem packagingOverheadsRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                         LocalizedResources.OverheadSetting_PackagingOHValue,
                                                                         null,
                                                                         overheadAndMarginGroup,
                                                                         DisplayTemplateType.ExtendedLabelControl,
                                                                         packagingOverheads,
                                                                         typeof(decimal));
            packagingOverheadsRowItem.UnitType = UnitType.Percentage;
            packagingOverheadsRowItem.ItemType = CompareRowItemType.Property;
            packagingOverheadsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(packagingOverheadsRowItem);

            CompareRowPropertyItem salesAdministrationOverheadsRowItem = new CompareRowPropertyItem(this.ColumnHeaders, LocalizedResources.OverheadSetting_SalesAndAdministrationOHValue, null, overheadAndMarginGroup, DisplayTemplateType.ExtendedLabelControl, salesAdministrationOverheads, typeof(decimal));
            salesAdministrationOverheadsRowItem.UnitType = UnitType.Percentage;
            salesAdministrationOverheadsRowItem.ItemType = CompareRowItemType.Property;
            salesAdministrationOverheadsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(salesAdministrationOverheadsRowItem);

            CompareRowPropertyItem externalWorkOverheadsRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                            LocalizedResources.OverheadSetting_ExternalWorkOverhead,
                                                                            null,
                                                                            overheadAndMarginGroup,
                                                                            DisplayTemplateType.ExtendedLabelControl,
                                                                            externalWorkOverheads,
                                                                            typeof(decimal));
            externalWorkOverheadsRowItem.UnitType = UnitType.Percentage;
            externalWorkOverheadsRowItem.ItemType = CompareRowItemType.Property;
            externalWorkOverheadsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(externalWorkOverheadsRowItem);

            CompareRowPropertyItem otherCostsOverheadRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.OverheadSetting_OtherCostOHValue,
                                                                        null,
                                                                        overheadAndMarginGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        otherCostsOverhead,
                                                                        typeof(decimal));
            otherCostsOverheadRowItem.UnitType = UnitType.Percentage;
            otherCostsOverheadRowItem.ItemType = CompareRowItemType.Property;
            otherCostsOverheadRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(otherCostsOverheadRowItem);

            CompareRowPropertyItem logisticOverheadsRowItem = new CompareRowPropertyItem(
                                                                     this.ColumnHeaders,
                                                                     LocalizedResources.OverheadSetting_LogisticOHValue,
                                                                     null,
                                                                     overheadAndMarginGroup,
                                                                     DisplayTemplateType.ExtendedLabelControl,
                                                                     logisticOverheads,
                                                                     typeof(decimal));
            logisticOverheadsRowItem.UnitType = UnitType.Percentage;
            logisticOverheadsRowItem.ItemType = CompareRowItemType.Property;
            logisticOverheadsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(logisticOverheadsRowItem);

            CompareRowPropertyItem companySurchargeOverheadsRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.OverheadSettings_CompanySurchargeOH,
                                                                        null,
                                                                        overheadAndMarginGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        companySurchargeOverheads,
                                                                        typeof(decimal));
            companySurchargeOverheadsRowItem.UnitType = UnitType.Percentage;
            companySurchargeOverheadsRowItem.ItemType = CompareRowItemType.Property;
            companySurchargeOverheadsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(companySurchargeOverheadsRowItem);

            CompareRowPropertyItem materialMarginMarginsRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.OverheadSetting_MaterialMargin,
                                                                        null,
                                                                        overheadAndMarginGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        materialMarginMargins,
                                                                        typeof(decimal));
            materialMarginMarginsRowItem.UnitType = UnitType.Percentage;
            materialMarginMarginsRowItem.ItemType = CompareRowItemType.Property;
            materialMarginMarginsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(materialMarginMarginsRowItem);

            CompareRowPropertyItem consumableMarginsRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                        LocalizedResources.OverheadSetting_ConsumableMargin,
                                                                        null,
                                                                        overheadAndMarginGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        consumableMargins,
                                                                        typeof(decimal));
            consumableMarginsRowItem.UnitType = UnitType.Percentage;
            consumableMarginsRowItem.ItemType = CompareRowItemType.Property;
            consumableMarginsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(consumableMarginsRowItem);

            CompareRowPropertyItem commodityMarginsRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.OverheadSetting_CommodityMargin,
                                                                        null,
                                                                        overheadAndMarginGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        commodityMargins,
                                                                        typeof(decimal));
            commodityMarginsRowItem.UnitType = UnitType.Percentage;
            commodityMarginsRowItem.ItemType = CompareRowItemType.Property;
            commodityMarginsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(commodityMarginsRowItem);

            CompareRowPropertyItem externalWorkMarginsRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.OverheadSetting_ExternalWorkMargin,
                                                                        null,
                                                                        overheadAndMarginGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        externalWorkMargins,
                                                                        typeof(decimal));
            externalWorkMarginsRowItem.UnitType = UnitType.Percentage;
            externalWorkMarginsRowItem.ItemType = CompareRowItemType.Property;
            externalWorkMarginsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(externalWorkMarginsRowItem);

            CompareRowPropertyItem manufacturingMarginsRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.OverheadSetting_ManufacturingMargin,
                                                                        null,
                                                                        overheadAndMarginGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        manufacturingMargins,
                                                                        typeof(decimal));
            manufacturingMarginsRowItem.UnitType = UnitType.Percentage;
            manufacturingMarginsRowItem.ItemType = CompareRowItemType.Property;
            manufacturingMarginsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(manufacturingMarginsRowItem);
        }

        /// <summary>
        /// Populates the assembly country settings group.
        /// </summary>
        /// <param name="compareItems">The compare items.</param>
        private void PopulateAssemblyCountrySettingsGroup(List<CompareRowPropertyItem> compareItems)
        {
            var countrySettingsGroup = new CompareRowPropertyItem.GroupHeader();
            countrySettingsGroup.GroupName = LocalizedResources.General_CountrySettings;
            countrySettingsGroup.IsExpanded = false;

            IList unskilledLabourCosts = new List<decimal>();
            IList skilledLabourCosts = new List<decimal>();
            IList foremanCosts = new List<decimal>();
            IList technicianCosts = new List<decimal>();
            IList engineerCosts = new List<decimal>();
            IList labourAvailability = new List<decimal?>();
            IList energyCosts = new List<decimal>();
            IList airCosts = new List<decimal>();
            IList waterCosts = new List<decimal>();
            IList productionAreaRentalCosts = new List<decimal>();
            IList officeAreaRentalCosts = new List<decimal>();
            IList interestRates = new List<decimal>();

            foreach (Tuple<IIdentifiable, IIdentifiable, DbIdentifier> tuple in CompareEntityAndParentCollection)
            {
                Assembly assy = tuple.Item1 as Assembly;

                if (assy != null)
                {
                    if (assy.CountrySettings != null)
                    {
                        unskilledLabourCosts.Add(assy.CountrySettings.UnskilledLaborCost);
                        skilledLabourCosts.Add(assy.CountrySettings.SkilledLaborCost);
                        foremanCosts.Add(assy.CountrySettings.ForemanCost);
                        technicianCosts.Add(assy.CountrySettings.TechnicianCost);
                        engineerCosts.Add(assy.CountrySettings.EngineerCost);
                        labourAvailability.Add(assy.CountrySettings.LaborAvailability);
                        energyCosts.Add(assy.CountrySettings.EnergyCost);
                        airCosts.Add(assy.CountrySettings.AirCost);
                        waterCosts.Add(assy.CountrySettings.WaterCost);
                        productionAreaRentalCosts.Add(assy.CountrySettings.ProductionAreaRentalCost);
                        officeAreaRentalCosts.Add(assy.CountrySettings.OfficeAreaRentalCost);
                        interestRates.Add(assy.CountrySettings.InterestRate);
                    }
                }
            }

            CompareRowPropertyItem unskilledLabourCostsRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.General_UnskilledLabourCost,
                                                                        "{0} {1}/h",
                                                                        countrySettingsGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        unskilledLabourCosts,
                                                                        typeof(decimal));
            unskilledLabourCostsRowItem.UnitType = UnitType.Currency;
            unskilledLabourCostsRowItem.ItemType = CompareRowItemType.Property;
            unskilledLabourCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(unskilledLabourCostsRowItem);

            CompareRowPropertyItem skilledLabourCostsRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.General_SkilledLabourCost,
                                                                        "{0} {1}/h",
                                                                        countrySettingsGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        skilledLabourCosts,
                                                                        typeof(decimal));
            skilledLabourCostsRowItem.UnitType = UnitType.Currency;
            skilledLabourCostsRowItem.ItemType = CompareRowItemType.Property;
            skilledLabourCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(skilledLabourCostsRowItem);

            CompareRowPropertyItem foremanCostsRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.General_ForemanCost,
                                                                        "{0} {1}/h",
                                                                        countrySettingsGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        foremanCosts,
                                                                        typeof(decimal));
            foremanCostsRowItem.UnitType = UnitType.Currency;
            foremanCostsRowItem.ItemType = CompareRowItemType.Property;
            foremanCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(foremanCostsRowItem);

            CompareRowPropertyItem technicianCostsRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.General_TechnicianCost,
                                                                        "{0} {1}/h",
                                                                        countrySettingsGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        technicianCosts,
                                                                        typeof(decimal));
            technicianCostsRowItem.UnitType = UnitType.Currency;
            technicianCostsRowItem.ItemType = CompareRowItemType.Property;
            technicianCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(technicianCostsRowItem);

            CompareRowPropertyItem engineerCostsRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.General_EngineerCost,
                                                                        "{0} {1}/h",
                                                                        countrySettingsGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        engineerCosts,
                                                                        typeof(decimal));
            engineerCostsRowItem.UnitType = UnitType.Currency;
            engineerCostsRowItem.ItemType = CompareRowItemType.Property;
            engineerCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(engineerCostsRowItem);

            CompareRowPropertyItem labourAvailabilityRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                            LocalizedResources.CountrySetting_LabourAvailability,
                                                                            null,
                                                                            countrySettingsGroup,
                                                                            DisplayTemplateType.ExtendedLabelControl,
                                                                            labourAvailability,
                                                                            typeof(decimal?));
            labourAvailabilityRowItem.UnitType = UnitType.Percentage;
            labourAvailabilityRowItem.ItemType = CompareRowItemType.Property;
            labourAvailabilityRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(labourAvailabilityRowItem);

            CompareRowPropertyItem energyCostsRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                            LocalizedResources.General_EnergyCost,
                                                                            "{0} {1}/kWh",
                                                                            countrySettingsGroup,
                                                                            DisplayTemplateType.ExtendedLabelControl,
                                                                            energyCosts,
                                                                            typeof(decimal));
            energyCostsRowItem.UnitType = UnitType.Currency;
            energyCostsRowItem.ItemType = CompareRowItemType.Property;
            energyCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(energyCostsRowItem);

            CompareRowPropertyItem airCostsRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.General_AirCost,
                                                                        "{0} {1}/m³",
                                                                        countrySettingsGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        airCosts,
                                                                        typeof(decimal));
            airCostsRowItem.UnitType = UnitType.Currency;
            airCostsRowItem.ItemType = CompareRowItemType.Property;
            airCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(airCostsRowItem);

            CompareRowPropertyItem waterCostsRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.General_WaterCost,
                                                                        "{0} {1}/l",
                                                                        countrySettingsGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        waterCosts,
                                                                        typeof(decimal));
            waterCostsRowItem.UnitType = UnitType.Currency;
            waterCostsRowItem.ItemType = CompareRowItemType.Property;
            waterCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(waterCostsRowItem);

            CompareRowPropertyItem productionAreaRentalCostsRowItem = new CompareRowPropertyItem(
                                                                          this.ColumnHeaders,
                                                                          LocalizedResources.General_ProductionAreaRentalCost,
                                                                          "{0} {1}/m² " + LocalizedResources.General_PerMonth,
                                                                          countrySettingsGroup,
                                                                          DisplayTemplateType.ExtendedLabelControl,
                                                                          productionAreaRentalCosts,
                                                                          typeof(decimal));
            productionAreaRentalCostsRowItem.UnitType = UnitType.Currency;
            productionAreaRentalCostsRowItem.ItemType = CompareRowItemType.Property;
            productionAreaRentalCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(productionAreaRentalCostsRowItem);

            CompareRowPropertyItem officeAreaRentalCostsRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.General_OfficeAreaRentalCost,
                                                                        "{0} {1}/m² " + LocalizedResources.General_PerMonth,
                                                                        countrySettingsGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        officeAreaRentalCosts,
                                                                        typeof(decimal));
            officeAreaRentalCostsRowItem.UnitType = UnitType.Currency;
            officeAreaRentalCostsRowItem.ItemType = CompareRowItemType.Property;
            officeAreaRentalCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(officeAreaRentalCostsRowItem);

            CompareRowPropertyItem interestRatesRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                            LocalizedResources.General_InterestRate,
                                                                            null,
                                                                            countrySettingsGroup,
                                                                            DisplayTemplateType.ExtendedLabelControl,
                                                                            interestRates,
                                                                            typeof(decimal));
            interestRatesRowItem.UnitType = UnitType.Percentage;
            interestRatesRowItem.ItemType = CompareRowItemType.Property;
            interestRatesRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(interestRatesRowItem);
        }

        /// <summary>
        /// Populates the assembly general group.
        /// </summary>
        /// <param name="compareItems">The compare items.</param>
        private void PopulateAssemblyGeneralGroup(List<CompareRowPropertyItem> compareItems)
        {
            CompareRowPropertyItem.GroupHeader generalGroup = new CompareRowPropertyItem.GroupHeader();
            generalGroup.GroupName = LocalizedResources.General_General;
            generalGroup.IsExpanded = true;

            IList numbers = new List<string>();
            IList assemblingCountries = new List<string>();
            IList assemblingSupplier = new List<string>();
            IList versions = new List<decimal?>();
            IList versionDates = new List<DateTime?>();
            IList accuracies = new List<string>();
            IList aproaches = new List<string>();
            IList calculations = new List<string>();
            IList batchSizes = new List<decimal?>();
            IList yearlyProdQtys = new List<decimal?>();
            IList lifeTimes = new List<decimal?>();
            IList partsTotals = new List<decimal>();
            IList neededPartsTotals = new List<decimal>();

            foreach (Tuple<IIdentifiable, IIdentifiable, DbIdentifier> tuple in CompareEntityAndParentCollection)
            {
                Assembly assy = tuple.Item1 as Assembly;
                Project parentProject = tuple.Item2 as Project;

                if (assy != null)
                {
                    numbers.Add(assy.Number);
                    assemblingCountries.Add(assy.AssemblingCountry);
                    assemblingSupplier.Add(assy.AssemblingSupplier);
                    versions.Add(assy.Version);
                    versionDates.Add(assy.VersionDate);

                    if (assy.CalculationAccuracy.HasValue)
                    {
                        accuracies.Add(UIUtils.GetEnumValueLabel((PartCalculationAccuracy)assy.CalculationAccuracy.Value));
                    }
                    else
                    {
                        accuracies.Add(null);
                    }

                    if (assy.CalculationApproach.HasValue)
                    {
                        aproaches.Add(UIUtils.GetEnumValueLabel((PartCalculationApproach)assy.CalculationApproach.Value));
                    }
                    else
                    {
                        aproaches.Add(null);
                    }

                    calculations.Add(assy.CalculationVariant);

                    batchSizes.Add((decimal?)assy.BatchSizePerYear);
                    yearlyProdQtys.Add((decimal?)assy.YearlyProductionQuantity);
                    lifeTimes.Add((decimal?)assy.LifeTime);

                    ICostCalculator calculator = CostCalculatorFactory.GetCalculator(assy.CalculationVariant);

                    var partsTotal = calculator.CalculateNetLifetimeProductionQuantity(assy);
                    partsTotals.Add(partsTotal);

                    if (!assy.IsMasterData
                        && assy.Process != null
                        && assy.Process.Steps.Count > 0
                        && parentProject != null)
                    {
                        var calculationParams = CostCalculationHelper.CreateAssemblyParamsFromProject(parentProject);
                        neededPartsTotals.Add(calculator.CalculateGrossLifetimeProductionQuantity(assy, calculationParams));
                    }
                    else
                    {
                        neededPartsTotals.Add(partsTotal);
                    }
                }
            }

            CompareRowPropertyItem numberRowItem = new CompareRowPropertyItem(this.ColumnHeaders, LocalizedResources.General_Number, null, generalGroup, DisplayTemplateType.TextBlock, numbers, typeof(string));
            numberRowItem.ItemType = CompareRowItemType.Property;
            numberRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(numberRowItem);
            CompareRowPropertyItem versionsRowItem = new CompareRowPropertyItem(this.ColumnHeaders, LocalizedResources.General_Version, null, generalGroup, DisplayTemplateType.TextBlock, versions, typeof(decimal?));
            versionsRowItem.ItemType = CompareRowItemType.Property;
            versionsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(versionsRowItem);
            CompareRowPropertyItem versionDatesRowItem = new CompareRowPropertyItem(this.ColumnHeaders, LocalizedResources.General_VersionDate, null, generalGroup, DisplayTemplateType.Date, versionDates, typeof(DateTime));
            versionDatesRowItem.ItemType = CompareRowItemType.Property;
            versionDatesRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(versionDatesRowItem);
            CompareRowPropertyItem assemblingCountriesRowItem = new CompareRowPropertyItem(this.ColumnHeaders, LocalizedResources.General_AssemblingCountry, null, generalGroup, DisplayTemplateType.TextBlock, assemblingCountries, typeof(string));
            assemblingCountriesRowItem.ItemType = CompareRowItemType.Property;
            assemblingCountriesRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(assemblingCountriesRowItem);
            CompareRowPropertyItem assemblingSupplierRowItem = new CompareRowPropertyItem(this.ColumnHeaders, LocalizedResources.General_AssemblingState, null, generalGroup, DisplayTemplateType.TextBlock, assemblingSupplier, typeof(string));
            assemblingSupplierRowItem.ItemType = CompareRowItemType.Property;
            assemblingSupplierRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(assemblingSupplierRowItem);
            CompareRowPropertyItem accuraciesRowItem = new CompareRowPropertyItem(this.ColumnHeaders, LocalizedResources.General_CalculationAccuracy, null, generalGroup, DisplayTemplateType.TextBlock, accuracies, typeof(string));
            accuraciesRowItem.ItemType = CompareRowItemType.Property;
            accuraciesRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(accuraciesRowItem);
            CompareRowPropertyItem approachesRowItem = new CompareRowPropertyItem(this.ColumnHeaders, LocalizedResources.General_CalculationApproach, null, generalGroup, DisplayTemplateType.TextBlock, aproaches, typeof(string));
            approachesRowItem.ItemType = CompareRowItemType.Property;
            approachesRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(approachesRowItem);
            CompareRowPropertyItem calculationsRowItem = new CompareRowPropertyItem(this.ColumnHeaders, LocalizedResources.General_CalculationVariant, null, generalGroup, DisplayTemplateType.TextBlock, calculations, typeof(string));
            calculationsRowItem.ItemType = CompareRowItemType.Property;
            calculationsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(calculationsRowItem);

            CompareRowPropertyItem batchSizesRowItem = new CompareRowPropertyItem(this.ColumnHeaders, LocalizedResources.General_PartBatch, null, generalGroup, DisplayTemplateType.ExtendedLabelControl, batchSizes, typeof(decimal?));
            batchSizesRowItem.UnitType = UnitType.None;
            batchSizesRowItem.Symbol = LocalizedResources.General_PerYear;
            batchSizesRowItem.MaxDigitsToDisplay = 0;
            batchSizesRowItem.ItemType = CompareRowItemType.Property;
            batchSizesRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(batchSizesRowItem);

            CompareRowPropertyItem yearlyProdQtyRowItem = new CompareRowPropertyItem(this.ColumnHeaders, LocalizedResources.General_YearlyProductionQuantity, null, generalGroup, DisplayTemplateType.ExtendedLabelControl, yearlyProdQtys, typeof(decimal?));
            yearlyProdQtyRowItem.UnitType = UnitType.None;
            yearlyProdQtyRowItem.Symbol = LocalizedResources.General_Pieces;
            yearlyProdQtyRowItem.MaxDigitsToDisplay = 0;
            yearlyProdQtyRowItem.ItemType = CompareRowItemType.Property;
            yearlyProdQtyRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(yearlyProdQtyRowItem);

            CompareRowPropertyItem lifeTimesRowItem = new CompareRowPropertyItem(this.ColumnHeaders, LocalizedResources.General_LifeTime, null, generalGroup, DisplayTemplateType.ExtendedLabelControl, lifeTimes, typeof(decimal?));
            lifeTimesRowItem.UnitType = UnitType.None;
            lifeTimesRowItem.Symbol = LocalizedResources.General_Years;
            lifeTimesRowItem.MaxDigitsToDisplay = 0;
            lifeTimesRowItem.ItemType = CompareRowItemType.Property;
            lifeTimesRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(lifeTimesRowItem);

            CompareRowPropertyItem partsTotalsRowItem = new CompareRowPropertyItem(this.ColumnHeaders, LocalizedResources.Part_PartsTotal, null, generalGroup, DisplayTemplateType.ExtendedLabelControl, partsTotals, typeof(decimal?));
            partsTotalsRowItem.UnitType = UnitType.None;
            partsTotalsRowItem.Symbol = LocalizedResources.General_Pieces;
            partsTotalsRowItem.MaxDigitsToDisplay = 0;
            partsTotalsRowItem.ItemType = CompareRowItemType.Property;
            partsTotalsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(partsTotalsRowItem);

            CompareRowPropertyItem neededPartsTotalsRowItem = new CompareRowPropertyItem(this.ColumnHeaders, LocalizedResources.Part_NeededPartsTotal, null, generalGroup, DisplayTemplateType.ExtendedLabelControl, neededPartsTotals, typeof(decimal?));
            neededPartsTotalsRowItem.UnitType = UnitType.None;
            neededPartsTotalsRowItem.Symbol = LocalizedResources.General_Pieces;
            neededPartsTotalsRowItem.MaxDigitsToDisplay = 0;
            neededPartsTotalsRowItem.ItemType = CompareRowItemType.Property;
            neededPartsTotalsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(neededPartsTotalsRowItem);
        }

        /// <summary>
        /// Populates the part manufacturing group.
        /// </summary>
        /// <param name="compareItems">The compare items.</param>
        private void PopulateAssemblyManufacturerGroup(List<CompareRowPropertyItem> compareItems)
        {
            var manufacturingGroup = new CompareRowPropertyItem.GroupHeader();
            manufacturingGroup.GroupName = LocalizedResources.General_Manufacturer;
            manufacturingGroup.IsExpanded = false;

            List<string> names = new List<string>();
            List<string> descriptions = new List<string>();

            foreach (Tuple<IIdentifiable, IIdentifiable, DbIdentifier> tuple in CompareEntityAndParentCollection)
            {
                Assembly assy = tuple.Item1 as Assembly;

                if (assy != null)
                {
                    if (assy.Manufacturer != null)
                    {
                        names.Add(assy.Manufacturer.Name);
                        descriptions.Add(assy.Manufacturer.Description);
                    }
                    else
                    {
                        names.Add(null);
                        descriptions.Add(null);
                    }
                }
            }

            CompareRowPropertyItem namesRowItem = new CompareRowPropertyItem(
                                                            this.ColumnHeaders,
                                                            LocalizedResources.General_Name,
                                                            null,
                                                            manufacturingGroup,
                                                            DisplayTemplateType.TextBlock,
                                                            names,
                                                            typeof(string));
            namesRowItem.ItemType = CompareRowItemType.Property;
            namesRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(namesRowItem);

            CompareRowPropertyItem descriptionsRowItem = new CompareRowPropertyItem(
                                                                    this.ColumnHeaders,
                                                                    LocalizedResources.General_Description,
                                                                    null,
                                                                    manufacturingGroup,
                                                                    DisplayTemplateType.TextBlock,
                                                                    descriptions,
                                                                    typeof(string));
            descriptionsRowItem.ItemType = CompareRowItemType.Property;
            descriptionsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(descriptionsRowItem);
        }

        /// <summary>
        /// Populates the assembly settings group.
        /// </summary>
        /// <param name="compareItems">The compare items.</param>
        private void PopulateAssemblySettingsGroup(List<CompareRowPropertyItem> compareItems)
        {
            var settingsGroup = new CompareRowPropertyItem.GroupHeader();
            settingsGroup.GroupName = LocalizedResources.General_Settings;
            settingsGroup.IsExpanded = false;

            IList purchasePrices = new List<decimal?>();
            IList deliveryTypes = new List<string>();
            IList assetRates = new List<decimal?>();
            IList targetPrices = new List<decimal?>();
            IList toolingActives = new List<bool?>();
            IList external = new List<bool?>();

            foreach (Tuple<IIdentifiable, IIdentifiable, DbIdentifier> tuple in CompareEntityAndParentCollection)
            {
                Assembly assy = tuple.Item1 as Assembly;
                if (assy != null)
                {
                    purchasePrices.Add(assy.PurchasePrice);

                    if (assy.DeliveryType.HasValue)
                    {
                        deliveryTypes.Add(UIUtils.GetEnumValueLabel((PartDeliveryType)assy.DeliveryType.Value));
                    }
                    else
                    {
                        deliveryTypes.Add(null);
                    }

                    assetRates.Add(assy.AssetRate);
                    targetPrices.Add(assy.TargetPrice);
                    toolingActives.Add(assy.SBMActive);
                    external.Add(assy.IsExternal);
                }
            }

            CompareRowPropertyItem purchasePricesRowItem = new CompareRowPropertyItem(this.ColumnHeaders, LocalizedResources.General_PurchasePrice, null, settingsGroup, DisplayTemplateType.ExtendedLabelControl, purchasePrices, typeof(decimal?));
            purchasePricesRowItem.UnitType = UnitType.Currency;
            purchasePricesRowItem.ItemType = CompareRowItemType.Property;
            purchasePricesRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(purchasePricesRowItem);
            CompareRowPropertyItem deliveryTypesRowItem = new CompareRowPropertyItem(this.ColumnHeaders, LocalizedResources.General_DeliveryType, null, settingsGroup, DisplayTemplateType.TextBlock, deliveryTypes, typeof(string));
            deliveryTypesRowItem.ItemType = CompareRowItemType.Property;
            deliveryTypesRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(deliveryTypesRowItem);
            CompareRowPropertyItem assetRatesRowItem = new CompareRowPropertyItem(this.ColumnHeaders, LocalizedResources.General_AssetRate, null, settingsGroup, DisplayTemplateType.ExtendedLabelControl, assetRates, typeof(decimal?));
            assetRatesRowItem.UnitType = UnitType.Percentage;
            assetRatesRowItem.ItemType = CompareRowItemType.Property;
            assetRatesRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(assetRatesRowItem);
            CompareRowPropertyItem targetPricesRowItem = new CompareRowPropertyItem(this.ColumnHeaders, LocalizedResources.General_TargetPrice, null, settingsGroup, DisplayTemplateType.ExtendedLabelControl, targetPrices, typeof(decimal?));
            targetPricesRowItem.UnitType = UnitType.Currency;
            targetPricesRowItem.ItemType = CompareRowItemType.Property;
            targetPricesRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(targetPricesRowItem);
            CompareRowPropertyItem toolingActivesRowItem = new CompareRowPropertyItem(this.ColumnHeaders, LocalizedResources.General_SBMActive, null, settingsGroup, DisplayTemplateType.CheckBox, toolingActives, typeof(bool?));
            toolingActivesRowItem.ItemType = CompareRowItemType.Property;
            toolingActivesRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(toolingActivesRowItem);
            CompareRowPropertyItem externalRowItem = new CompareRowPropertyItem(this.ColumnHeaders, LocalizedResources.General_IsExternal, null, settingsGroup, DisplayTemplateType.CheckBox, external, typeof(bool?));
            externalRowItem.ItemType = CompareRowItemType.Property;
            externalRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(externalRowItem);
        }

        /// <summary>
        /// Populates the assembly subassemblies group.
        /// </summary>
        /// <param name="compareItems">The compare items.</param>
        private void PopulateSubassembliesGroup(List<CompareRowPropertyItem> compareItems)
        {
            CompareRowPropertyItem.GroupHeader subassembliesGroup = new CompareRowPropertyItem.GroupHeader();
            subassembliesGroup.GroupName = LocalizedResources.General_Subassemblies;
            subassembliesGroup.IsExpanded = true;

            IList subassemblies = new List<KeyValuePair<object, DbIdentifier>>();
            foreach (Tuple<IIdentifiable, IIdentifiable, DbIdentifier> tuple in CompareEntityAndParentCollection)
            {
                Assembly assy = tuple.Item1 as Assembly;
                if (assy != null)
                {
                    subassemblies.Add(new KeyValuePair<object, DbIdentifier>(
                        assy.Subassemblies.Where(a => !a.IsDeleted).OrderBy(a => a.Index),
                        tuple.Item3));
                }
            }

            CompareRowPropertyItem subassembliesRowItem = new CompareRowPropertyItem(
                this.ColumnHeaders,
                string.Empty,
                null,
                subassembliesGroup,
                DisplayTemplateType.List,
                subassemblies,
                typeof(object));
            subassembliesRowItem.GoToCommand = this.GoToCommand;
            subassembliesRowItem.ItemType = CompareRowItemType.SubEntity;
            subassembliesRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(subassembliesRowItem);
        }

        /// <summary>
        /// Populates the assembly parts group.
        /// </summary>
        /// <param name="compareItems">The compare items.</param>
        private void PopulateAssemblyPartsGroup(List<CompareRowPropertyItem> compareItems)
        {
            CompareRowPropertyItem.GroupHeader partsGroup = new CompareRowPropertyItem.GroupHeader();
            partsGroup.GroupName = LocalizedResources.General_Parts;
            partsGroup.IsExpanded = true;

            IList parts = new List<KeyValuePair<object, DbIdentifier>>();
            foreach (Tuple<IIdentifiable, IIdentifiable, DbIdentifier> tuple in CompareEntityAndParentCollection)
            {
                Assembly assy = tuple.Item1 as Assembly;
                if (assy != null)
                {
                    parts.Add(new KeyValuePair<object, DbIdentifier>(
                        assy.Parts.Where(p => !p.IsDeleted).OrderBy(p => p.Index),
                        tuple.Item3));
                }
            }

            CompareRowPropertyItem partsRowItem = new CompareRowPropertyItem(
                this.ColumnHeaders,
                string.Empty,
                null,
                partsGroup,
                DisplayTemplateType.List,
                parts,
                typeof(object));
            partsRowItem.GoToCommand = this.GoToCommand;
            partsRowItem.ItemType = CompareRowItemType.SubEntity;
            partsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(partsRowItem);
        }

        #endregion Populate Assemblies Comparison

        #region Populate Parts Comparison

        /// <summary>
        /// Populates the parts comparison.
        /// </summary>
        private void PopulatePartsComparison()
        {
            List<CompareRowPropertyItem> compareItems = new List<CompareRowPropertyItem>();

            this.InitializePartColumnHeaders();

            this.PopulatePartGeneralGroup(compareItems);
            this.PopulatePartManufacturingGroup(compareItems);
            this.PopulatePartSettingsGroup(compareItems);
            this.PopulatePartOHOverheadAndMarginGroup(compareItems);
            this.PopulatePartCountrySettingsGroup(compareItems);

            List<CalculationResultSummary> calcResultSummaries = GetCalculationResultsSummaries(this.normalCalculationResults);
            this.PopulateCalculationResultSummaryGroup(typeof(Part), calcResultSummaries, compareItems);
            List<OverheadCost> calcResultOverheads = GetCalculationResultOverheadCosts(this.normalCalculationResults);
            this.PopulateCalculationResultOverheadsCostsGroup(typeof(Part), calcResultOverheads, compareItems);
            List<ProcessCost> calcAssemblingCosts = GetCalculationResultAssemblingCosts(this.normalCalculationResults);
            this.PopulateAssemblingManufacturingCostsGroup(typeof(Part), calcAssemblingCosts, compareItems);
            List<CommoditiesCost> calcCommodityCosts = GetCalculationResultCommodityCosts(this.normalCalculationResults);
            this.PopulateCommoditiesCostsGroup(calcCommodityCosts, compareItems);
            List<ConsumablesCost> calcConsumableCosts = GetCalculationResultConsumableCosts(this.normalCalculationResults);
            this.PopulateConsumableCostsGroup(calcConsumableCosts, compareItems);
            List<RawMaterialsCost> calcMaterialCosts = GetCalculationResultMaterialCosts(this.normalCalculationResults);
            this.PopulateMaterialCostsGroup(calcMaterialCosts, compareItems);
            List<InvestmentCost> calcInvestmentsCosts = GetCalculationResultInvestmentCosts(this.normalCalculationResults);
            this.PopulateInverstmentCostsGroup(calcInvestmentsCosts, compareItems);

            this.CompareCollection = new ObservableCollection<CompareRowPropertyItem>(compareItems);
            this.HighlightDifferences();
        }

        /// <summary>
        /// Populates the part country settings group.
        /// </summary>
        /// <param name="compareItems">The compare items.</param>
        private void PopulatePartCountrySettingsGroup(List<CompareRowPropertyItem> compareItems)
        {
            var countrySettingsGroup = new CompareRowPropertyItem.GroupHeader();
            countrySettingsGroup.GroupName = LocalizedResources.General_CountrySettings;
            countrySettingsGroup.IsExpanded = false;

            IList unskilledLabourCosts = new List<decimal>();
            IList skilledLabourCosts = new List<decimal>();
            IList foremanCosts = new List<decimal>();
            IList technicianCosts = new List<decimal>();
            IList engineerCosts = new List<decimal>();
            IList labourAvailability = new List<decimal?>();
            IList energyCosts = new List<decimal>();
            IList airCosts = new List<decimal>();
            IList waterCosts = new List<decimal>();
            IList productionAreaRentalCosts = new List<decimal>();
            IList officeAreaRentalCosts = new List<decimal>();
            IList interestRates = new List<decimal>();

            foreach (Tuple<IIdentifiable, IIdentifiable, DbIdentifier> tuple in CompareEntityAndParentCollection)
            {
                Part part = tuple.Item1 as Part;
                if (part != null)
                {
                    if (part.CountrySettings != null)
                    {
                        unskilledLabourCosts.Add(part.CountrySettings.UnskilledLaborCost);
                        skilledLabourCosts.Add(part.CountrySettings.SkilledLaborCost);
                        foremanCosts.Add(part.CountrySettings.ForemanCost);
                        technicianCosts.Add(part.CountrySettings.TechnicianCost);
                        engineerCosts.Add(part.CountrySettings.EngineerCost);
                        labourAvailability.Add(part.CountrySettings.LaborAvailability);
                        energyCosts.Add(part.CountrySettings.EnergyCost);
                        airCosts.Add(part.CountrySettings.AirCost);
                        waterCosts.Add(part.CountrySettings.WaterCost);
                        productionAreaRentalCosts.Add(part.CountrySettings.ProductionAreaRentalCost);
                        officeAreaRentalCosts.Add(part.CountrySettings.OfficeAreaRentalCost);
                        interestRates.Add(part.CountrySettings.InterestRate);
                    }
                    else
                    {
                        unskilledLabourCosts.Add(0m);
                        skilledLabourCosts.Add(0m);
                        foremanCosts.Add(0m);
                        technicianCosts.Add(0m);
                        engineerCosts.Add(0m);
                        labourAvailability.Add(0m);
                        energyCosts.Add(0m);
                        airCosts.Add(0m);
                        waterCosts.Add(0m);
                        productionAreaRentalCosts.Add(0m);
                        officeAreaRentalCosts.Add(0m);
                        interestRates.Add(0m);
                    }
                }
            }

            CompareRowPropertyItem unskilledLabourCostsRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.General_UnskilledLabourCost,
                                                                        "{0} {1}/h",
                                                                        countrySettingsGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        unskilledLabourCosts,
                                                                        typeof(decimal));
            unskilledLabourCostsRowItem.UnitType = UnitType.Currency;
            unskilledLabourCostsRowItem.ItemType = CompareRowItemType.Property;
            unskilledLabourCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(unskilledLabourCostsRowItem);

            CompareRowPropertyItem skilledLabourCostsRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.General_SkilledLabourCost,
                                                                        "{0} {1}/h",
                                                                        countrySettingsGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        skilledLabourCosts,
                                                                        typeof(decimal));
            skilledLabourCostsRowItem.UnitType = UnitType.Currency;
            skilledLabourCostsRowItem.ItemType = CompareRowItemType.Property;
            skilledLabourCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(skilledLabourCostsRowItem);

            CompareRowPropertyItem foremanCostsRowItem = new CompareRowPropertyItem(
                                                                    this.ColumnHeaders,
                                                                    LocalizedResources.General_ForemanCost,
                                                                    "{0} {1}/h",
                                                                    countrySettingsGroup,
                                                                    DisplayTemplateType.ExtendedLabelControl,
                                                                    foremanCosts,
                                                                    typeof(decimal));
            foremanCostsRowItem.UnitType = UnitType.Currency;
            foremanCostsRowItem.ItemType = CompareRowItemType.Property;
            foremanCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(foremanCostsRowItem);

            CompareRowPropertyItem technicianCostsRowItem = new CompareRowPropertyItem(
                                                                    this.ColumnHeaders,
                                                                    LocalizedResources.General_TechnicianCost,
                                                                    "{0} {1}/h",
                                                                    countrySettingsGroup,
                                                                    DisplayTemplateType.ExtendedLabelControl,
                                                                    technicianCosts,
                                                                    typeof(decimal));
            technicianCostsRowItem.UnitType = UnitType.Currency;
            technicianCostsRowItem.ItemType = CompareRowItemType.Property;
            technicianCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(technicianCostsRowItem);

            CompareRowPropertyItem engineerCostsRowItem = new CompareRowPropertyItem(
                                                                this.ColumnHeaders,
                                                                LocalizedResources.General_EngineerCost,
                                                                "{0} {1}/h",
                                                                countrySettingsGroup,
                                                                DisplayTemplateType.ExtendedLabelControl,
                                                                engineerCosts,
                                                                typeof(decimal));
            engineerCostsRowItem.UnitType = UnitType.Currency;
            engineerCostsRowItem.ItemType = CompareRowItemType.Property;
            engineerCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(engineerCostsRowItem);

            CompareRowPropertyItem labourAvailabilityRowItem = new CompareRowPropertyItem(
                                                                    this.ColumnHeaders,
                                                                    LocalizedResources.CountrySetting_LabourAvailability,
                                                                    null,
                                                                    countrySettingsGroup,
                                                                    DisplayTemplateType.ExtendedLabelControl,
                                                                    labourAvailability,
                                                                    typeof(decimal));
            labourAvailabilityRowItem.UnitType = UnitType.Percentage;
            labourAvailabilityRowItem.ItemType = CompareRowItemType.Property;
            labourAvailabilityRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(labourAvailabilityRowItem);

            CompareRowPropertyItem energyCostsRowItem = new CompareRowPropertyItem(
                                                               this.ColumnHeaders,
                                                                LocalizedResources.General_EnergyCost,
                                                                "{0} {1}/kWh",
                                                                countrySettingsGroup,
                                                                DisplayTemplateType.ExtendedLabelControl,
                                                                energyCosts,
                                                                typeof(decimal));
            energyCostsRowItem.UnitType = UnitType.Currency;
            energyCostsRowItem.ItemType = CompareRowItemType.Property;
            energyCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(energyCostsRowItem);

            CompareRowPropertyItem airCostsRowItem = new CompareRowPropertyItem(
                                                            this.ColumnHeaders,
                                                            LocalizedResources.General_AirCost,
                                                            "{0} {1}/m³",
                                                            countrySettingsGroup,
                                                            DisplayTemplateType.ExtendedLabelControl,
                                                            airCosts,
                                                            typeof(decimal));
            airCostsRowItem.UnitType = UnitType.Currency;
            airCostsRowItem.ItemType = CompareRowItemType.Property;
            airCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(airCostsRowItem);

            CompareRowPropertyItem waterCostsRowItem = new CompareRowPropertyItem(
                                                                this.ColumnHeaders,
                                                                LocalizedResources.General_WaterCost,
                                                                "{0} {1}/l",
                                                                countrySettingsGroup,
                                                                DisplayTemplateType.ExtendedLabelControl,
                                                                waterCosts,
                                                                typeof(decimal));
            waterCostsRowItem.UnitType = UnitType.Currency;
            waterCostsRowItem.ItemType = CompareRowItemType.Property;
            waterCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(waterCostsRowItem);

            CompareRowPropertyItem productionAreaRentalCostsRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.General_ProductionAreaRentalCost,
                                                                        "{0} {1}/m² " + LocalizedResources.General_PerMonth,
                                                                        countrySettingsGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        productionAreaRentalCosts,
                                                                        typeof(decimal));
            productionAreaRentalCostsRowItem.UnitType = UnitType.Currency;
            productionAreaRentalCostsRowItem.ItemType = CompareRowItemType.Property;
            productionAreaRentalCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(productionAreaRentalCostsRowItem);

            CompareRowPropertyItem officeAreaRentalCostsRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                            LocalizedResources.General_OfficeAreaRentalCost,
                                                                            "{0} {1}/m² " + LocalizedResources.General_PerMonth,
                                                                            countrySettingsGroup,
                                                                            DisplayTemplateType.ExtendedLabelControl,
                                                                            officeAreaRentalCosts,
                                                                            typeof(decimal));
            officeAreaRentalCostsRowItem.UnitType = UnitType.Currency;
            officeAreaRentalCostsRowItem.ItemType = CompareRowItemType.Property;
            officeAreaRentalCostsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(officeAreaRentalCostsRowItem);

            CompareRowPropertyItem interestRatesRowItem = new CompareRowPropertyItem(
                                                                    this.ColumnHeaders,
                                                                    LocalizedResources.General_InterestRate,
                                                                    null,
                                                                    countrySettingsGroup,
                                                                    DisplayTemplateType.ExtendedLabelControl,
                                                                    interestRates,
                                                                    typeof(decimal));
            interestRatesRowItem.UnitType = UnitType.Percentage;
            interestRatesRowItem.ItemType = CompareRowItemType.Property;
            interestRatesRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(interestRatesRowItem);
        }

        /// <summary>
        /// Populates the part OH overhead group.
        /// </summary>
        /// <param name="compareItems">The compare items.</param>
        private void PopulatePartOHOverheadAndMarginGroup(List<CompareRowPropertyItem> compareItems)
        {
            var overheadAndMarginGroup = new CompareRowPropertyItem.GroupHeader();
            overheadAndMarginGroup.GroupName = LocalizedResources.General_OverheadAndMargin;
            overheadAndMarginGroup.IsExpanded = false;

            IList materialOverheads = new List<decimal>();
            IList cunsumablesOverheads = new List<decimal>();
            IList commodityOverheads = new List<decimal>();
            IList manufacturingOverheads = new List<decimal>();
            IList packagingOverheads = new List<decimal>();
            IList salesAdministrationOverheads = new List<decimal>();
            IList externalWorkOverheads = new List<decimal>();
            IList otherCostsOverhead = new List<decimal>();
            IList logisticOverheads = new List<decimal>();
            IList companySurchargeOverheads = new List<decimal>();

            IList materialMarginMargins = new List<decimal>();
            IList consumableMargins = new List<decimal>();
            IList commodityMargins = new List<decimal>();
            IList externalWorkMargins = new List<decimal>();
            IList manufacturingMargins = new List<decimal>();

            foreach (Tuple<IIdentifiable, IIdentifiable, DbIdentifier> tuple in CompareEntityAndParentCollection)
            {
                Part part = tuple.Item1 as Part;
                if (part != null)
                {
                    if (part.OverheadSettings != null)
                    {
                        materialOverheads.Add(part.OverheadSettings.MaterialOverhead);
                        cunsumablesOverheads.Add(part.OverheadSettings.ConsumableOverhead);
                        commodityOverheads.Add(part.OverheadSettings.CommodityOverhead);
                        manufacturingOverheads.Add(part.OverheadSettings.ManufacturingOverhead);
                        packagingOverheads.Add(part.OverheadSettings.PackagingOHValue);
                        salesAdministrationOverheads.Add(part.OverheadSettings.SalesAndAdministrationOHValue);
                        externalWorkOverheads.Add(part.OverheadSettings.ExternalWorkOverhead);
                        otherCostsOverhead.Add(part.OverheadSettings.OtherCostOHValue);
                        logisticOverheads.Add(part.OverheadSettings.LogisticOHValue);
                        companySurchargeOverheads.Add(part.OverheadSettings.CompanySurchargeOverhead);

                        materialMarginMargins.Add(part.OverheadSettings.MaterialMargin);
                        consumableMargins.Add(part.OverheadSettings.ConsumableMargin);
                        commodityMargins.Add(part.OverheadSettings.CommodityMargin);
                        externalWorkMargins.Add(part.OverheadSettings.ExternalWorkMargin);
                        manufacturingMargins.Add(part.OverheadSettings.ManufacturingMargin);
                    }
                    else
                    {
                        materialOverheads.Add(0m);
                        cunsumablesOverheads.Add(0m);
                        commodityOverheads.Add(0m);
                        manufacturingOverheads.Add(0m);
                        packagingOverheads.Add(0m);
                        salesAdministrationOverheads.Add(0m);
                        externalWorkOverheads.Add(0m);
                        otherCostsOverhead.Add(0m);
                        logisticOverheads.Add(0m);
                        companySurchargeOverheads.Add(0m);

                        materialMarginMargins.Add(0m);
                        consumableMargins.Add(0m);
                        commodityMargins.Add(0m);
                        externalWorkMargins.Add(0m);
                        manufacturingMargins.Add(0m);
                    }
                }
            }

            CompareRowPropertyItem materialOverheadsRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.OverheadSetting_MaterialOverhead,
                                                                        null,
                                                                        overheadAndMarginGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        materialOverheads,
                                                                        typeof(decimal));
            materialOverheadsRowItem.UnitType = UnitType.Percentage;
            materialOverheadsRowItem.ItemType = CompareRowItemType.Property;
            materialOverheadsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(materialOverheadsRowItem);

            CompareRowPropertyItem consumablesOverheadsRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                            LocalizedResources.OverheadSetting_ConsumableOverhead,
                                                                            null,
                                                                            overheadAndMarginGroup,
                                                                            DisplayTemplateType.ExtendedLabelControl,
                                                                            cunsumablesOverheads,
                                                                            typeof(decimal));
            consumablesOverheadsRowItem.UnitType = UnitType.Percentage;
            consumablesOverheadsRowItem.ItemType = CompareRowItemType.Property;
            consumablesOverheadsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(consumablesOverheadsRowItem);

            CompareRowPropertyItem commodityOverheadsRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                            LocalizedResources.OverheadSetting_CommodityOverhead,
                                                                            null,
                                                                            overheadAndMarginGroup,
                                                                            DisplayTemplateType.ExtendedLabelControl,
                                                                            commodityOverheads,
                                                                            typeof(decimal));
            commodityOverheadsRowItem.UnitType = UnitType.Percentage;
            commodityOverheadsRowItem.ItemType = CompareRowItemType.Property;
            commodityOverheadsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(commodityOverheadsRowItem);

            CompareRowPropertyItem manufacturingOverheadsRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                         LocalizedResources.OverheadSetting_ManufacturingOverhead,
                                                                         null,
                                                                         overheadAndMarginGroup,
                                                                         DisplayTemplateType.ExtendedLabelControl,
                                                                         manufacturingOverheads,
                                                                         typeof(decimal));
            manufacturingOverheadsRowItem.UnitType = UnitType.Percentage;
            manufacturingOverheadsRowItem.ItemType = CompareRowItemType.Property;
            manufacturingOverheadsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(manufacturingOverheadsRowItem);

            CompareRowPropertyItem packagingOverheadsRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                            LocalizedResources.OverheadSetting_PackagingOHValue,
                                                                            null,
                                                                            overheadAndMarginGroup,
                                                                            DisplayTemplateType.ExtendedLabelControl,
                                                                            packagingOverheads,
                                                                            typeof(decimal));
            packagingOverheadsRowItem.UnitType = UnitType.Percentage;
            packagingOverheadsRowItem.ItemType = CompareRowItemType.Property;
            packagingOverheadsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(packagingOverheadsRowItem);

            CompareRowPropertyItem salesAdministrationOverheadsRowItem = new CompareRowPropertyItem(
                                                                              this.ColumnHeaders,
                                                                              LocalizedResources.OverheadSetting_SalesAndAdministrationOHValue,
                                                                              null,
                                                                              overheadAndMarginGroup,
                                                                              DisplayTemplateType.ExtendedLabelControl,
                                                                              salesAdministrationOverheads,
                                                                              typeof(decimal));
            salesAdministrationOverheadsRowItem.UnitType = UnitType.Percentage;
            salesAdministrationOverheadsRowItem.ItemType = CompareRowItemType.Property;
            salesAdministrationOverheadsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(salesAdministrationOverheadsRowItem);

            CompareRowPropertyItem externalWorkOverheadsRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                            LocalizedResources.OverheadSetting_ExternalWorkOverhead,
                                                                            null,
                                                                            overheadAndMarginGroup,
                                                                            DisplayTemplateType.ExtendedLabelControl,
                                                                            externalWorkOverheads,
                                                                            typeof(decimal));
            externalWorkOverheadsRowItem.UnitType = UnitType.Percentage;
            externalWorkOverheadsRowItem.ItemType = CompareRowItemType.Property;
            externalWorkOverheadsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(externalWorkOverheadsRowItem);

            CompareRowPropertyItem otherCostsOverheadRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                            LocalizedResources.OverheadSetting_OtherCostOHValue,
                                                                            null,
                                                                            overheadAndMarginGroup,
                                                                            DisplayTemplateType.ExtendedLabelControl,
                                                                            otherCostsOverhead,
                                                                            typeof(decimal));
            otherCostsOverheadRowItem.UnitType = UnitType.Percentage;
            otherCostsOverheadRowItem.ItemType = CompareRowItemType.Property;
            otherCostsOverheadRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(otherCostsOverheadRowItem);

            CompareRowPropertyItem logisticOverheadsRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                            LocalizedResources.OverheadSetting_LogisticOHValue,
                                                                            null,
                                                                            overheadAndMarginGroup,
                                                                            DisplayTemplateType.ExtendedLabelControl,
                                                                            logisticOverheads,
                                                                            typeof(decimal));
            logisticOverheadsRowItem.UnitType = UnitType.Percentage;
            logisticOverheadsRowItem.ItemType = CompareRowItemType.Property;
            logisticOverheadsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(logisticOverheadsRowItem);

            CompareRowPropertyItem companySurchargeOverheadsRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.OverheadSettings_CompanySurchargeOH,
                                                                        null,
                                                                        overheadAndMarginGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        companySurchargeOverheads,
                                                                        typeof(decimal));
            companySurchargeOverheadsRowItem.UnitType = UnitType.Percentage;
            companySurchargeOverheadsRowItem.ItemType = CompareRowItemType.Property;
            companySurchargeOverheadsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(companySurchargeOverheadsRowItem);

            CompareRowPropertyItem materialMarginMarginsRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                            LocalizedResources.OverheadSetting_MaterialMargin,
                                                                            null,
                                                                            overheadAndMarginGroup,
                                                                            DisplayTemplateType.ExtendedLabelControl,
                                                                            materialMarginMargins,
                                                                            typeof(decimal));
            materialMarginMarginsRowItem.UnitType = UnitType.Percentage;
            materialMarginMarginsRowItem.ItemType = CompareRowItemType.Property;
            materialMarginMarginsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(materialMarginMarginsRowItem);

            CompareRowPropertyItem consumableMarginsRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.OverheadSetting_ConsumableMargin,
                                                                        null,
                                                                        overheadAndMarginGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        consumableMargins,
                                                                        typeof(decimal));
            consumableMarginsRowItem.UnitType = UnitType.Percentage;
            consumableMarginsRowItem.ItemType = CompareRowItemType.Property;
            consumableMarginsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(consumableMarginsRowItem);

            CompareRowPropertyItem commodityMarginsRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                            LocalizedResources.OverheadSetting_CommodityMargin,
                                                                            null,
                                                                            overheadAndMarginGroup,
                                                                            DisplayTemplateType.ExtendedLabelControl,
                                                                            commodityMargins,
                                                                            typeof(decimal));
            commodityMarginsRowItem.UnitType = UnitType.Percentage;
            commodityMarginsRowItem.ItemType = CompareRowItemType.Property;
            commodityMarginsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(commodityMarginsRowItem);

            CompareRowPropertyItem externalWorkMarginsRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                         LocalizedResources.OverheadSetting_ExternalWorkMargin,
                                                                         null,
                                                                         overheadAndMarginGroup,
                                                                         DisplayTemplateType.ExtendedLabelControl,
                                                                         externalWorkMargins,
                                                                         typeof(decimal));
            externalWorkMarginsRowItem.UnitType = UnitType.Percentage;
            externalWorkMarginsRowItem.ItemType = CompareRowItemType.Property;
            externalWorkMarginsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(externalWorkMarginsRowItem);

            CompareRowPropertyItem manufacturingMarginsRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.OverheadSetting_ManufacturingMargin,
                                                                        null,
                                                                        overheadAndMarginGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        manufacturingMargins,
                                                                        typeof(decimal));
            manufacturingMarginsRowItem.UnitType = UnitType.Percentage;
            manufacturingMarginsRowItem.ItemType = CompareRowItemType.Property;
            manufacturingMarginsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(manufacturingMarginsRowItem);
        }

        /// <summary>
        /// Populates the part settings group.
        /// </summary>
        /// <param name="compareItems">The compare items.</param>
        private void PopulatePartSettingsGroup(List<CompareRowPropertyItem> compareItems)
        {
            var settingsGroup = new CompareRowPropertyItem.GroupHeader();
            settingsGroup.GroupName = LocalizedResources.General_Settings;
            settingsGroup.IsExpanded = false;

            IList purchasePrices = new List<decimal?>();
            IList deliveryTypes = new List<string>();
            IList assetRates = new List<decimal?>();
            IList targetPrices = new List<decimal?>();
            IList toolingActives = new List<bool?>();
            IList external = new List<bool?>();

            foreach (Tuple<IIdentifiable, IIdentifiable, DbIdentifier> tuple in CompareEntityAndParentCollection)
            {
                Part part = tuple.Item1 as Part;
                if (part != null)
                {
                    purchasePrices.Add(part.PurchasePrice);

                    if (part.DelivertType.HasValue)
                    {
                        deliveryTypes.Add(UIUtils.GetEnumValueLabel((PartDeliveryType)part.DelivertType.Value));
                    }
                    else
                    {
                        deliveryTypes.Add(null);
                    }

                    assetRates.Add(part.AssetRate);
                    targetPrices.Add(part.TargetPrice);
                    toolingActives.Add(part.SBMActive);
                    external.Add(part.IsExternal);
                }
            }

            CompareRowPropertyItem purchasePricesRowItem = new CompareRowPropertyItem(
                                                                    this.ColumnHeaders,
                                                                    LocalizedResources.General_PurchasePrice,
                                                                    null,
                                                                    settingsGroup,
                                                                    DisplayTemplateType.ExtendedLabelControl,
                                                                    purchasePrices,
                                                                    typeof(decimal?));
            purchasePricesRowItem.UnitType = UnitType.Currency;
            purchasePricesRowItem.ItemType = CompareRowItemType.Property;
            purchasePricesRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(purchasePricesRowItem);

            CompareRowPropertyItem deliveryTypesRowItem = new CompareRowPropertyItem(
                                                                       this.ColumnHeaders,
                                                                        LocalizedResources.General_DeliveryType,
                                                                        null,
                                                                        settingsGroup,
                                                                        DisplayTemplateType.TextBlock,
                                                                        deliveryTypes,
                                                                        typeof(string));
            deliveryTypesRowItem.ItemType = CompareRowItemType.Property;
            deliveryTypesRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(deliveryTypesRowItem);

            CompareRowPropertyItem assetRatesRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.General_AssetRate,
                                                                        null,
                                                                        settingsGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        assetRates,
                                                                        typeof(decimal?));
            assetRatesRowItem.UnitType = UnitType.Percentage;
            assetRatesRowItem.ItemType = CompareRowItemType.Property;
            assetRatesRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(assetRatesRowItem);

            CompareRowPropertyItem targetPricesRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.General_TargetPrice,
                                                                        null,
                                                                        settingsGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        targetPrices,
                                                                        typeof(decimal?));
            targetPricesRowItem.UnitType = UnitType.Currency;
            targetPricesRowItem.ItemType = CompareRowItemType.Property;
            targetPricesRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(targetPricesRowItem);

            CompareRowPropertyItem toolingActivesRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.General_SBMActive,
                                                                        null,
                                                                        settingsGroup,
                                                                        DisplayTemplateType.CheckBox,
                                                                        toolingActives,
                                                                        typeof(bool?));
            toolingActivesRowItem.ItemType = CompareRowItemType.Property;
            toolingActivesRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(toolingActivesRowItem);

            CompareRowPropertyItem externalRowItem = new CompareRowPropertyItem(
                                                                    this.ColumnHeaders,
                                                                    LocalizedResources.General_IsExternal,
                                                                    null,
                                                                    settingsGroup,
                                                                    DisplayTemplateType.CheckBox,
                                                                    external,
                                                                    typeof(bool?));
            externalRowItem.ItemType = CompareRowItemType.Property;
            externalRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(externalRowItem);
        }

        /// <summary>
        /// Populates the part manufacturing group.
        /// </summary>
        /// <param name="compareItems">The compare items.</param>
        private void PopulatePartManufacturingGroup(List<CompareRowPropertyItem> compareItems)
        {
            var manufacturingGroup = new CompareRowPropertyItem.GroupHeader();
            manufacturingGroup.GroupName = LocalizedResources.Comparison_Manufacturing;
            manufacturingGroup.IsExpanded = false;

            List<string> names = new List<string>();
            List<string> descriptions = new List<string>();
            List<string> countries = new List<string>();
            List<string> suppliers = new List<string>();

            foreach (Tuple<IIdentifiable, IIdentifiable, DbIdentifier> tuple in CompareEntityAndParentCollection)
            {
                Part part = tuple.Item1 as Part;
                if (part != null)
                {
                    if (part.Manufacturer != null)
                    {
                        names.Add(part.Manufacturer.Name);
                        descriptions.Add(part.Manufacturer.Description);
                        countries.Add(part.ManufacturingCountry);
                        suppliers.Add(part.ManufacturingSupplier);
                    }
                    else
                    {
                        names.Add(string.Empty);
                        descriptions.Add(string.Empty);
                        countries.Add(string.Empty);
                        suppliers.Add(string.Empty);
                    }
                }
            }

            CompareRowPropertyItem namesRowItem = new CompareRowPropertyItem(
                                                          this.ColumnHeaders,
                                                          LocalizedResources.General_Name,
                                                          null,
                                                          manufacturingGroup,
                                                          DisplayTemplateType.TextBlock,
                                                          names,
                                                          typeof(string));
            namesRowItem.ItemType = CompareRowItemType.Property;
            namesRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(namesRowItem);

            CompareRowPropertyItem descriptionsRowItem = new CompareRowPropertyItem(
                                                                    this.ColumnHeaders,
                                                                    LocalizedResources.General_Description,
                                                                    null,
                                                                    manufacturingGroup,
                                                                    DisplayTemplateType.TextBlock,
                                                                    descriptions,
                                                                    typeof(string));
            descriptionsRowItem.ItemType = CompareRowItemType.Property;
            descriptionsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(descriptionsRowItem);

            CompareRowPropertyItem countriesRowItem = new CompareRowPropertyItem(
                                                                this.ColumnHeaders,
                                                                LocalizedResources.General_Country,
                                                                null,
                                                                manufacturingGroup,
                                                                DisplayTemplateType.TextBlock,
                                                                countries,
                                                                typeof(string));
            countriesRowItem.ItemType = CompareRowItemType.Property;
            countriesRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(countriesRowItem);

            CompareRowPropertyItem suppliersRowItem = new CompareRowPropertyItem(
                                                                    this.ColumnHeaders,
                                                                    LocalizedResources.General_Supplier,
                                                                    null,
                                                                    manufacturingGroup,
                                                                    DisplayTemplateType.TextBlock,
                                                                    suppliers,
                                                                    typeof(string));
            suppliersRowItem.ItemType = CompareRowItemType.Property;
            suppliersRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(suppliersRowItem);
        }

        /// <summary>
        /// Populates the part general group.
        /// </summary>
        /// <param name="compareItems">The compare items.</param>
        private void PopulatePartGeneralGroup(List<CompareRowPropertyItem> compareItems)
        {
            var generalGroup = new CompareRowPropertyItem.GroupHeader();
            generalGroup.GroupName = LocalizedResources.General_General;
            generalGroup.IsExpanded = true;

            IList numbers = new List<string>();
            IList versions = new List<decimal?>();
            IList versionDates = new List<DateTime?>();
            IList descriptions = new List<string>();
            IList statuses = new List<string>();
            IList accuracies = new List<string>();
            IList aproaches = new List<string>();
            IList calculations = new List<string>();
            IList calculators = new List<string>();
            IList batchSizes = new List<decimal?>();
            IList yearlyProdQtys = new List<decimal?>();
            IList lifeTimes = new List<decimal?>();
            IList partsTotals = new List<decimal>();
            IList neededPartsTotals = new List<decimal>();

            foreach (Tuple<IIdentifiable, IIdentifiable, DbIdentifier> tuple in CompareEntityAndParentCollection)
            {
                Part part = tuple.Item1 as Part;
                Project parentProject = tuple.Item2 as Project;

                if (part != null)
                {
                    if (!string.IsNullOrWhiteSpace(part.Number))
                    {
                        numbers.Add(part.Number);
                    }
                    else
                    {
                        numbers.Add(string.Empty);
                    }

                    versions.Add(part.Version);
                    versionDates.Add(part.VersionDate);
                    descriptions.Add(part.Description);

                    if (part.CalculationStatus.HasValue)
                    {
                        statuses.Add(UIUtils.GetEnumValueLabel((PartCalculationStatus)part.CalculationStatus.Value));
                    }
                    else
                    {
                        statuses.Add(null);
                    }

                    if (part.CalculationAccuracy.HasValue)
                    {
                        accuracies.Add(UIUtils.GetEnumValueLabel((PartCalculationAccuracy)part.CalculationAccuracy.Value));
                    }
                    else
                    {
                        accuracies.Add(null);
                    }

                    if (part.CalculationApproach.HasValue)
                    {
                        aproaches.Add(UIUtils.GetEnumValueLabel((PartCalculationApproach)part.CalculationApproach.Value));
                    }
                    else
                    {
                        aproaches.Add(null);
                    }

                    calculations.Add(part.CalculationVariant);
                    if (part.CalculatorUser != null)
                    {
                        calculators.Add(part.CalculatorUser.Name);
                    }
                    else
                    {
                        calculators.Add(null);
                    }

                    batchSizes.Add((decimal?)part.BatchSizePerYear);
                    yearlyProdQtys.Add((decimal?)part.YearlyProductionQuantity);
                    lifeTimes.Add((decimal?)part.LifeTime);

                    ICostCalculator calculator = CostCalculatorFactory.GetCalculator(part.CalculationVariant);

                    var partsTotal = calculator.CalculateNetLifetimeProductionQuantity(part);
                    partsTotals.Add(partsTotal);

                    if (!part.IsMasterData
                        && part.Process != null
                        && part.Process.Steps.Count > 0
                        && parentProject != null)
                    {
                        var calculationParams = CostCalculationHelper.CreatePartParamsFromProject(parentProject);
                        neededPartsTotals.Add(calculator.CalculateGrossLifetimeProductionQuantity(part, calculationParams));
                    }
                    else
                    {
                        neededPartsTotals.Add(partsTotal);
                    }
                }
            }

            CompareRowPropertyItem numberRowItem = new CompareRowPropertyItem(
                                                                this.ColumnHeaders,
                                                                LocalizedResources.General_Number,
                                                                null,
                                                                generalGroup,
                                                                DisplayTemplateType.TextBlock,
                                                                numbers,
                                                                typeof(string));
            numberRowItem.ItemType = CompareRowItemType.Property;
            numberRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(numberRowItem);

            CompareRowPropertyItem versionsRowItem = new CompareRowPropertyItem(
                                                                this.ColumnHeaders,
                                                                LocalizedResources.General_Version,
                                                                null,
                                                                generalGroup,
                                                                DisplayTemplateType.TextBlock,
                                                                versions,
                                                                typeof(decimal?));
            versionsRowItem.ItemType = CompareRowItemType.Property;
            versionsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(versionsRowItem);

            CompareRowPropertyItem versionDatesRowItem = new CompareRowPropertyItem(
                                                                    this.ColumnHeaders,
                                                                    LocalizedResources.General_VersionDate,
                                                                    null,
                                                                    generalGroup,
                                                                    DisplayTemplateType.Date,
                                                                    versionDates,
                                                                    typeof(DateTime));
            versionDatesRowItem.ItemType = CompareRowItemType.Property;
            versionDatesRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(versionDatesRowItem);

            CompareRowPropertyItem descriptionsRowItem = new CompareRowPropertyItem(
                                                                    this.ColumnHeaders,
                                                                    LocalizedResources.General_Description,
                                                                    null,
                                                                    generalGroup,
                                                                    DisplayTemplateType.TextBlock,
                                                                    descriptions,
                                                                    typeof(string));
            descriptionsRowItem.ItemType = CompareRowItemType.Property;
            descriptionsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(descriptionsRowItem);

            CompareRowPropertyItem statusesRowItem = new CompareRowPropertyItem(
                                                                this.ColumnHeaders,
                                                                LocalizedResources.General_Status,
                                                                null,
                                                                generalGroup,
                                                                DisplayTemplateType.TextBlock,
                                                                statuses,
                                                                typeof(string));
            statusesRowItem.ItemType = CompareRowItemType.Property;
            statusesRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(statusesRowItem);

            CompareRowPropertyItem accuraciesRowItem = new CompareRowPropertyItem(
                                                                this.ColumnHeaders,
                                                                LocalizedResources.General_CalculationAccuracy,
                                                                null,
                                                                generalGroup,
                                                                DisplayTemplateType.TextBlock,
                                                                accuracies,
                                                                typeof(string));
            accuraciesRowItem.ItemType = CompareRowItemType.Property;
            accuraciesRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(accuraciesRowItem);

            CompareRowPropertyItem approachesRowItem = new CompareRowPropertyItem(
                                                                this.ColumnHeaders,
                                                                LocalizedResources.General_CalculationApproach,
                                                                null,
                                                                generalGroup,
                                                                DisplayTemplateType.TextBlock,
                                                                aproaches,
                                                                typeof(string));
            approachesRowItem.ItemType = CompareRowItemType.Property;
            approachesRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(approachesRowItem);

            CompareRowPropertyItem calculationsRowItem = new CompareRowPropertyItem(
                                                                    this.ColumnHeaders,
                                                                    LocalizedResources.General_CalculationVariant,
                                                                    null,
                                                                    generalGroup,
                                                                    DisplayTemplateType.TextBlock,
                                                                    calculations,
                                                                    typeof(string));
            calculationsRowItem.ItemType = CompareRowItemType.Property;
            calculationsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(calculationsRowItem);

            CompareRowPropertyItem calculatorsRowItem = new CompareRowPropertyItem(
                                                                    this.ColumnHeaders,
                                                                    LocalizedResources.General_Calculator,
                                                                    null,
                                                                    generalGroup,
                                                                    DisplayTemplateType.TextBlock,
                                                                     calculators,
                                                                    typeof(string));
            calculatorsRowItem.ItemType = CompareRowItemType.Property;
            calculatorsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(calculatorsRowItem);

            CompareRowPropertyItem batchSizesRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.General_PartBatch,
                                                                        null,
                                                                        generalGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        batchSizes,
                                                                        typeof(decimal?));
            batchSizesRowItem.UnitType = UnitType.None;
            batchSizesRowItem.Symbol = LocalizedResources.General_PerYear;
            batchSizesRowItem.MaxDigitsToDisplay = 0;
            batchSizesRowItem.ItemType = CompareRowItemType.Property;
            batchSizesRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(batchSizesRowItem);

            CompareRowPropertyItem yearlyProdQtyRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.General_YearlyProductionQuantity,
                                                                        null,
                                                                        generalGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        yearlyProdQtys,
                                                                        typeof(decimal?));
            yearlyProdQtyRowItem.UnitType = UnitType.None;
            yearlyProdQtyRowItem.Symbol = LocalizedResources.General_Pieces;
            yearlyProdQtyRowItem.MaxDigitsToDisplay = 0;
            yearlyProdQtyRowItem.ItemType = CompareRowItemType.Property;
            yearlyProdQtyRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(yearlyProdQtyRowItem);

            CompareRowPropertyItem lifeTimesRowItem = new CompareRowPropertyItem(
                                                               this.ColumnHeaders,
                                                               LocalizedResources.General_LifeTime,
                                                               null,
                                                               generalGroup,
                                                               DisplayTemplateType.ExtendedLabelControl,
                                                               lifeTimes,
                                                               typeof(decimal?));
            lifeTimesRowItem.UnitType = UnitType.None;
            lifeTimesRowItem.Symbol = LocalizedResources.General_Years;
            lifeTimesRowItem.MaxDigitsToDisplay = 0;
            lifeTimesRowItem.ItemType = CompareRowItemType.Property;
            lifeTimesRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(lifeTimesRowItem);

            CompareRowPropertyItem partsTotalsRowItem = new CompareRowPropertyItem(
                                                                        this.ColumnHeaders,
                                                                        LocalizedResources.Part_PartsTotal,
                                                                        null,
                                                                        generalGroup,
                                                                        DisplayTemplateType.ExtendedLabelControl,
                                                                        partsTotals,
                                                                        typeof(decimal?));
            partsTotalsRowItem.UnitType = UnitType.None;
            partsTotalsRowItem.Symbol = LocalizedResources.General_Pieces;
            partsTotalsRowItem.MaxDigitsToDisplay = 0;
            partsTotalsRowItem.ItemType = CompareRowItemType.Property;
            partsTotalsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(partsTotalsRowItem);

            CompareRowPropertyItem neededpartsTotalsRowItem = new CompareRowPropertyItem(
                                                                            this.ColumnHeaders,
                                                                            LocalizedResources.Part_NeededPartsTotal,
                                                                            null,
                                                                            generalGroup,
                                                                            DisplayTemplateType.ExtendedLabelControl,
                                                                            neededPartsTotals,
                                                                            typeof(decimal?));
            neededpartsTotalsRowItem.UnitType = UnitType.None;
            neededpartsTotalsRowItem.Symbol = LocalizedResources.General_Pieces;
            neededpartsTotalsRowItem.MaxDigitsToDisplay = 0;
            neededpartsTotalsRowItem.ItemType = CompareRowItemType.Property;
            neededpartsTotalsRowItem.MeasurementUnitsAdapters = this.measurementsUnitsAdapters;
            compareItems.Add(neededpartsTotalsRowItem);
        }

        #endregion Populate Parts Comparison

        #region Get Calculation Results Lists

        /// <summary>
        /// Gets the calculation result material costs.
        /// </summary>
        /// <param name="calculationResults">The calculation results.</param>
        /// <returns>List of raw material costs</returns>
        private List<RawMaterialsCost> GetCalculationResultMaterialCosts(List<CalculationResult> calculationResults)
        {
            List<RawMaterialsCost> rawMat = new List<RawMaterialsCost>();

            foreach (CalculationResult result in calculationResults)
            {
                if (result != null)
                {
                    rawMat.Add(result.RawMaterialsCost);
                }
                else
                {
                    rawMat.Add(null);
                }
            }

            return rawMat;
        }

        /// <summary>
        /// Calculates the calculation results.
        /// </summary>
        private void CalculateAssembliesCalculationResults()
        {
            this.normalCalculationResults = new List<CalculationResult>();
            this.enhancedCalculationResults = new List<CalculationResult>();
            this.measurementsUnitsAdapters = new ObservableCollection<UnitsAdapter>();

            foreach (var tuple in this.CompareEntityAndParentCollection)
            {
                Assembly assy = tuple.Item1 as Assembly;
                Project parentProject = tuple.Item2 as Project;
                AssemblyCostCalculationParameters calculationParams = null;

                if (assy != null)
                {
                    if (parentProject != null)
                    {
                        calculationParams = CostCalculationHelper.CreateAssemblyParamsFromProject(parentProject);
                    }
                    else if (IsInViewerMode)
                    {
                        calculationParams = this.modelBrowserHelperService.GetAssemblyCostCalculationParameters();
                    }
                }

                if (assy != null && calculationParams != null)
                {
                    var calculator = CostCalculatorFactory.GetCalculator(assy.CalculationVariant);
                    var calculatorResult = calculator.CalculateAssemblyCost(assy, calculationParams);

                    this.normalCalculationResults.Add(calculatorResult);
                    var enhancedCalculationResult = ReportsHelper.CreateEnhancedCostBreakdown(calculatorResult);
                    this.enhancedCalculationResults.Add(enhancedCalculationResult);
                }
                else
                {
                    this.normalCalculationResults.Add(null);
                    this.enhancedCalculationResults.Add(null);
                }

                var dataManager = DataAccessFactory.CreateDataSourceManager(tuple.Item3);
                ICollection<Currency> currencies = null;
                Currency baseCurrency = null;

                CurrencyConversionManager.GetEntityCurrenciesAndBaseCurrency(parentProject, dataManager, out currencies, out baseCurrency);
                var unitsAdapter = new UnitsAdapter(dataManager);
                unitsAdapter.Currencies = currencies;
                unitsAdapter.BaseCurrency = baseCurrency;
                this.measurementsUnitsAdapters.Add(unitsAdapter);
            }
        }

        /// <summary>
        /// Calculates the parts calculation results.
        /// </summary>
        private void CalculatePartsCalculationResults()
        {
            this.normalCalculationResults = new List<CalculationResult>();
            this.enhancedCalculationResults = null;
            this.measurementsUnitsAdapters = new ObservableCollection<UnitsAdapter>();

            foreach (var tuple in this.CompareEntityAndParentCollection)
            {
                Part part = tuple.Item1 as Part;
                Project parentProject = tuple.Item2 as Project;
                PartCostCalculationParameters calculationParams = null;

                if (part != null)
                {
                    if (parentProject != null)
                    {
                        calculationParams = CostCalculationHelper.CreatePartParamsFromProject(parentProject);
                    }
                    else if (IsInViewerMode)
                    {
                        calculationParams = this.modelBrowserHelperService.GetPartCostCalculationParameters();
                    }
                }

                if (part != null && calculationParams != null)
                {
                    var calculator = CostCalculatorFactory.GetCalculator(part.CalculationVariant);
                    var calculatorResult = calculator.CalculatePartCost(part, calculationParams);

                    this.normalCalculationResults.Add(calculatorResult);
                }
                else
                {
                    this.normalCalculationResults.Add(null);
                }

                var dataManager = DataAccessFactory.CreateDataSourceManager(tuple.Item3);
                ICollection<Currency> currencies = null;
                Currency baseCurrency = null;

                CurrencyConversionManager.GetEntityCurrenciesAndBaseCurrency(parentProject, dataManager, out currencies, out baseCurrency);
                var unitsAdapter = new UnitsAdapter(dataManager);
                unitsAdapter.Currencies = currencies;
                unitsAdapter.BaseCurrency = baseCurrency;
                this.measurementsUnitsAdapters.Add(unitsAdapter);
            }
        }

        /// <summary>
        /// Gets the calculation results summaries.
        /// </summary>
        /// <param name="calculationsResults">The calculations results.</param>
        /// <returns>List of Calculation Result Summary costs</returns>
        private List<CalculationResultSummary> GetCalculationResultsSummaries(List<CalculationResult> calculationsResults)
        {
            List<CalculationResultSummary> summaries = new List<CalculationResultSummary>();

            foreach (CalculationResult result in calculationsResults)
            {
                if (result != null)
                {
                    summaries.Add(result.Summary);
                }
                else
                {
                    summaries.Add(null);
                }
            }

            return summaries;
        }

        /// <summary>
        /// Gets the calculation result overheads.
        /// </summary>
        /// <param name="calculationsResults">The calculations results.</param>
        /// <returns>List of Overhead Costs</returns>
        private List<OverheadCost> GetCalculationResultOverheadCosts(List<CalculationResult> calculationsResults)
        {
            List<OverheadCost> summaries = new List<OverheadCost>();

            foreach (CalculationResult result in calculationsResults)
            {
                if (result != null)
                {
                    summaries.Add(result.OverheadCost);
                }
                else
                {
                    summaries.Add(null);
                }
            }

            return summaries;
        }

        /// <summary>
        /// Gets the calculation result assembling costs.
        /// </summary>
        /// <param name="calculationResults">The calculation results.</param>
        /// <returns>List of Process Costs</returns>
        private List<ProcessCost> GetCalculationResultAssemblingCosts(List<CalculationResult> calculationResults)
        {
            List<ProcessCost> assembliesCost = new List<ProcessCost>();

            foreach (CalculationResult result in calculationResults)
            {
                if (result != null)
                {
                    assembliesCost.Add(result.ProcessCost);
                }
                else
                {
                    assembliesCost.Add(null);
                }
            }

            return assembliesCost;
        }

        /// <summary>
        /// Gets the calculation result commodity costs.
        /// </summary>
        /// <param name="calculationResults">The calculation results.</param>
        /// <returns>List of Commodities costs</returns>
        private List<CommoditiesCost> GetCalculationResultCommodityCosts(List<CalculationResult> calculationResults)
        {
            List<CommoditiesCost> commoditiesCosts = new List<CommoditiesCost>();

            foreach (CalculationResult result in calculationResults)
            {
                if (result != null)
                {
                    commoditiesCosts.Add(result.CommoditiesCost);
                }
                else
                {
                    commoditiesCosts.Add(null);
                }
            }

            return commoditiesCosts;
        }

        /// <summary>
        /// Gets the calculation result consumable costs.
        /// </summary>
        /// <param name="calculationResults">The calculation results.</param>
        /// <returns>List of Consumables Costs</returns>
        private List<ConsumablesCost> GetCalculationResultConsumableCosts(List<CalculationResult> calculationResults)
        {
            List<ConsumablesCost> consumableCosts = new List<ConsumablesCost>();

            foreach (CalculationResult result in calculationResults)
            {
                if (result != null)
                {
                    consumableCosts.Add(result.ConsumablesCost);
                }
                else
                {
                    consumableCosts.Add(null);
                }
            }

            return consumableCosts;
        }

        /// <summary>
        /// Gets the calculation result investment costs.
        /// </summary>
        /// <param name="calculationResults">The calculation results.</param>
        /// <returns>List of Investment Costs</returns>
        private List<InvestmentCost> GetCalculationResultInvestmentCosts(List<CalculationResult> calculationResults)
        {
            List<InvestmentCost> invCosts = new List<InvestmentCost>();

            foreach (CalculationResult result in calculationResults)
            {
                if (result != null)
                {
                    invCosts.Add(result.InvestmentCost);
                }
                else
                {
                    invCosts.Add(null);
                }
            }

            return invCosts;
        }

        #endregion Get Calculation Results Lists

        #region View Loaded

        /// <summary>
        /// Views the loaded.
        /// </summary>
        private void ViewLoaded()
        {
            if (this.isLoaded == false)
            {
                this.GetEntities();
                this.isLoaded = true;
            }
        }

        #endregion View Loaded

        #region Refresh

        /// <summary>
        /// Refreshes the compare.
        /// </summary>
        private void RefreshCompare()
        {
            // Save the grouping.
            this.groupHeadersBeforeRefresh.Clear();
            foreach (var comparedItem in this.CompareCollection)
            {
                if (!groupHeadersBeforeRefresh.Contains(comparedItem.Group))
                {
                    groupHeadersBeforeRefresh.Add(comparedItem.Group);
                }
            }

            // Clear the collections.
            this.CompareEntityAndParentCollection.Clear();

            this.GetEntities();
        }

        #endregion Refresh

        #region Enhanced Breakdown

        /// <summary>
        /// Computes the enhanced breakdown result of the compared objects
        /// </summary>
        private void ComputeEnhanceBreakdownResults()
        {
            // Save the grouping.
            this.groupHeadersBeforeRefresh.Clear();
            foreach (var comparedItem in this.CompareCollection)
            {
                if (!groupHeadersBeforeRefresh.Contains(comparedItem.Group))
                {
                    groupHeadersBeforeRefresh.Add(comparedItem.Group);
                }
            }

            this.InitializeComparedObjects();

            if (groupHeadersBeforeRefresh.Count > 0)
            {
                // Restore the grouping.
                foreach (var comparedItem in this.CompareCollection)
                {
                    var foundHeader = groupHeadersBeforeRefresh.FirstOrDefault(p => p.Equals(comparedItem.Group));
                    if (foundHeader != null)
                    {
                        comparedItem.Group.IsExpanded = foundHeader.IsExpanded;
                    }
                }
            }
        }

        #endregion Enhanced Breakdown

        #region Highlight

        /// <summary>
        /// Highlights the row differences in the data grid.
        /// </summary>
        private void HighlightDifferences()
        {
            foreach (var item in this.CompareCollection)
            {
                item.SetDataGridRowHighlighted(this.AreDifferencesHighlighted);
            }

            var keys = this.CompareCollection.GroupBy(p => p.Group).Select(p => p.Key).ToList();

            foreach (var key in keys)
            {
                HighlightComparedGroupHeaders(key);
            }
        }

        /// <summary>
        /// Highlights the compared group headers.
        /// </summary>
        /// <param name="groupHeader">The group header.</param>
        private void HighlightComparedGroupHeaders(CompareRowPropertyItem.GroupHeader groupHeader)
        {
            bool shouldHeiglightGroupHeader = false;

            if (this.AreDifferencesHighlighted)
            {
                foreach (var item in CompareCollection.Where(p => p.Group == groupHeader).ToList())
                {
                    if (item.IsHighlighted)
                    {
                        shouldHeiglightGroupHeader = true;
                        break;
                    }
                }
            }

            foreach (var item in CompareCollection.Where(p => p.Group == groupHeader).ToList())
            {
                if (this.AreDifferencesHighlighted == false)
                {
                    item.IsHighlighted = false;
                }

                if (item.DisplayTemplateType != DisplayTemplateType.List)
                {
                    item.Group.IsHighlighted = shouldHeiglightGroupHeader;
                }
            }
        }

        #endregion Highlight

        #region Export To PDF\XLS

        /// <summary>
        /// Exports to PDF.
        /// </summary>
        private void ExportToPdf()
        {
            string fileName = CreateDocumentFileName();

            this.fileDialogService.Reset();
            this.fileDialogService.FileName = fileName;
            this.fileDialogService.Filter = LocalizedResources.DialogFilter_PdfDocs;

            string filePath = null;
            bool? result = this.fileDialogService.ShowSaveFileDialog();
            if (result == true)
            {
                filePath = this.fileDialogService.FileName;
            }

            if (string.IsNullOrEmpty(filePath))
            {
                return;
            }

            CompareDataSheet dataCollected = CollectDataForExport();
            CompareReportGeneratorPdf reportPdf = new CompareReportGeneratorPdf(dataCollected, filePath, string.Empty);
            bool succesfull = reportPdf.CreateReport();

            this.openFileOverlayWindow = new ZPKTool.Controls.OpenFileOverlayWindow();
            this.openFileOverlayWindow.LastCreatedFilePath = filePath;

            if (succesfull)
            {
                this.openFileOverlayWindow.IsOpenFileAvailable = true;
                this.openFileOverlayWindow.CreatedFileStatus = LocalizedResources.General_ExportSuccesfully;
            }
            else
            {
                this.openFileOverlayWindow.IsOpenFileAvailable = false;
                this.openFileOverlayWindow.CreatedFileStatus = LocalizedResources.FileExport_Error;
            }

            this.windowService.ShowDialog(this.openFileOverlayWindow);
        }

        /// <summary>
        /// Creates the name of the export to document file.
        /// </summary>
        /// <returns>The file name for the export document.</returns>
        private string CreateDocumentFileName()
        {
            string fileName = "Comp_";

            foreach (var columnHeader in this.ColumnHeaders)
            {
                fileName += columnHeader.Header + "_";
            }

            fileName += DateTime.Now.ToString("yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
            return fileName;
        }

        /// <summary>
        /// Exports to XLS.
        /// </summary>
        private void ExportToXls()
        {
            string fileName = CreateDocumentFileName();

            this.fileDialogService.Reset();
            this.fileDialogService.FileName = fileName;
            this.fileDialogService.Filter = LocalizedResources.DialogFilter_ExcelDocs;

            string filePath = null;
            bool? result = this.fileDialogService.ShowSaveFileDialog();
            if (result == true)
            {
                // If the file path exceeds the maximum length for Excel files (218 characters)
                // then notify the user to short the file name or the path and try again
                if (this.fileDialogService.FileName.Length < 218)
                {
                    filePath = this.fileDialogService.FileName;
                }
                else
                {
                    this.windowService.MessageDialogService.Show(LocalizedResources.Error_MaxExcelFilePathExceeded, MessageDialogType.Error);
                }
            }

            if (string.IsNullOrEmpty(filePath))
            {
                return;
            }

            CompareDataSheet dataCollected = CollectDataForExport();
            CompareReportGeneratorXls xlsReport = new CompareReportGeneratorXls(dataCollected, filePath, string.Empty);
            bool succesfull = xlsReport.CreateReport();

            this.openFileOverlayWindow = new ZPKTool.Controls.OpenFileOverlayWindow();
            this.openFileOverlayWindow.LastCreatedFilePath = filePath;

            if (succesfull)
            {
                this.openFileOverlayWindow.IsOpenFileAvailable = true;
                this.openFileOverlayWindow.CreatedFileStatus = LocalizedResources.General_ExportSuccesfully;
            }
            else
            {
                this.openFileOverlayWindow.IsOpenFileAvailable = false;
                this.openFileOverlayWindow.CreatedFileStatus = LocalizedResources.FileExport_Error;
            }

            this.windowService.ShowDialog(this.openFileOverlayWindow);
        }

        /// <summary>
        /// Collects the data for export.
        /// </summary>
        /// <returns>CompareDataSheet with collected data for export.</returns>
        private CompareDataSheet CollectDataForExport()
        {
            CompareDataSheet collectedData = new CompareDataSheet();

            string currencyUsed = string.Empty;

            // Collect the column headers
            StringCollection columnHeaders = new StringCollection();
            foreach (CompareHeaderItem header in this.ColumnHeaders)
            {
                columnHeaders.Add(header.Header);
            }

            collectedData.Headers = columnHeaders;

            var keys = this.CompareCollection.GroupBy(p => p.Group).Select(p => p.Key).ToList();

            List<CompareDataGroup> groupedRows = new List<CompareDataGroup>();
            foreach (var key in keys)
            {
                groupedRows.Add(CollectDataForExportFromGroup(key));
            }

            collectedData.GroupedRows = groupedRows;

            return collectedData;
        }

        /// <summary>
        /// Collects the data for export from group.
        /// </summary>
        /// <param name="groupHeader">The group header.</param>
        /// <returns>The group with collected data.</returns>
        private CompareDataGroup CollectDataForExportFromGroup(CompareRowPropertyItem.GroupHeader groupHeader)
        {
            var group = new CompareDataGroup();

            group.Name = groupHeader.GroupName;
            group.IsExpanded = groupHeader.IsExpanded;
            group.IsHighlighted = groupHeader.IsHighlighted;

            group.Rows = new List<CompareDataRow>();

            foreach (var compareItem in CompareCollection.Where(p => Equals(p.Group, groupHeader)).ToList())
            {
                var collectedRow = new CompareDataRow { Items = new List<object>() };
                for (var i = 0; i < compareItem.ComparedRowItems.Count; i++)
                {
                    var rowItem = compareItem.ComparedRowItems[i];
                    var measurementUnitsAdapter = compareItem.MeasurementUnitsAdapters[i];

                    collectedRow.Header = compareItem.Header;
                    if (!string.IsNullOrWhiteSpace(compareItem.Symbol))
                    {
                        collectedRow.Symbol = compareItem.Symbol;
                    }
                    else
                    {
                        var unit = measurementUnitsAdapter.GetCurrentUnit(compareItem.UnitType);
                        var symbol = unit != null ? unit.Symbol : string.Empty;
                        if (!string.IsNullOrWhiteSpace(compareItem.SymbolFormat))
                        {
                            symbol = string.Format(compareItem.SymbolFormat, string.Empty, symbol);
                        }

                        collectedRow.Symbol = symbol;
                    }

                    var assyCollection = rowItem as IEnumerable<Assembly>;
                    if (assyCollection != null)
                    {
                        var assyNames = assyCollection.Select(assy => assy.Name).ToList();
                        collectedRow.Items.Add(ConvertToDisplayValue(measurementUnitsAdapter, compareItem.UnitType, compareItem.Symbol, assyNames));
                    }

                    var partCollection = rowItem as IEnumerable<Part>;
                    if (partCollection != null)
                    {
                        var partNames = partCollection.Select(part => part.Name).ToList();
                        collectedRow.Items.Add(ConvertToDisplayValue(measurementUnitsAdapter, compareItem.UnitType, compareItem.Symbol, partNames));
                    }

                    if (rowItem is KeyValuePair<object, DbIdentifier>)
                    {
                        var entityNames = new List<string>();
                        var entityList = ((KeyValuePair<object, DbIdentifier>)rowItem).Key;

                        if (entityList != null)
                        {
                            var nameableList = entityList as IEnumerable<INameable>;

                            if (nameableList != null)
                            {
                                entityNames.AddRange(nameableList.Select(nameable => nameable.Name));
                            }
                        }

                        collectedRow.Items.Add(ConvertToDisplayValue(measurementUnitsAdapter, compareItem.UnitType, compareItem.Symbol, entityNames));
                    }
                    else
                    {
                        collectedRow.Items.Add(ConvertToDisplayValue(measurementUnitsAdapter, compareItem.UnitType, compareItem.Symbol, rowItem));
                    }
                }

                collectedRow.IsHighlighted = compareItem.IsHighlighted;
                group.Rows.Add(collectedRow);
            }

            return group;
        }

        /// <summary>
        /// Gets the compared item display value.
        /// </summary>
        /// <param name="measurementUnitsAdapter">The measurement units adapter of the compared item</param>
        /// <param name="uiUnitType">Type of the unit.</param>
        /// <param name="uiunitSymbol">The UI unit symbol.</param>
        /// <param name="compareItem">The compare item.</param>
        /// <returns>The value to display.</returns>
        private object ConvertToDisplayValue(UnitsAdapter measurementUnitsAdapter, UnitType uiUnitType, string uiunitSymbol, object compareItem)
        {
            object displayValue = null;
            if (uiUnitType != UnitType.None)
            {
                decimal? baseValue = (decimal?)compareItem;
                if (baseValue.HasValue)
                {
                    if (uiUnitType == UnitType.Currency)
                    {
                        var convertedValue = CurrencyConversionManager.ConvertBaseValueToDisplayValue(baseValue.Value, measurementUnitsAdapter.BaseCurrency.ExchangeRate, measurementUnitsAdapter.UICurrency.ConversionFactor);
                        displayValue = Formatter.FormatMoney(convertedValue);
                    }
                    else if (uiUnitType == UnitType.Percentage)
                    {
                        var convertedValue = baseValue.Value * measurementUnitsAdapter.UIPercentage.ConversionFactor;
                        displayValue = Formatter.FormatNumber(convertedValue, 4);
                    }
                    else
                    {
                        displayValue = Formatter.FormatNumber(baseValue.Value, 4);
                    }
                }
                else
                {
                    displayValue = string.Empty;
                }
            }
            else
            {
                displayValue = FormatValue(compareItem);
            }

            return displayValue;
        }

        /// <summary>
        /// Converts the value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>The formatted value.</returns>
        private object FormatValue(object value)
        {
            if (value == null)
            {
                return null;
            }

            // First it check if the value is string; if is not a string try to format it as a number and if that fails too, use its ToString() method.
            object result = null;
            if (value is string)
            {
                result = (string)value;
            }
            else if (value is bool)
            {
                bool boolVal = (bool)value;
                result = boolVal ? LocalizedResources.General_Yes : LocalizedResources.General_No;
            }
            else if (value is DateTime)
            {
                var date = (DateTime)value;
                result = date.ToShortDateString();
            }
            else if (value is List<string>)
            {
                string cellContent = string.Empty;
                List<string> contentLst = value as List<string>;
                foreach (string str in contentLst)
                {
                    cellContent += str + Environment.NewLine;
                }

                result = cellContent;
            }
            else
            {
                // Format the value as number and if it fails format it as string.
                string formattedValue = Formatter.FormatNumber(value, 4);
                if (!string.IsNullOrEmpty(formattedValue))
                {
                    result = formattedValue;
                }
                else
                {
                    result = value.ToString();
                }
            }

            return result;
        }

        #endregion Export To PDF\XLS

        /// <summary>
        /// Navigates to the specify entity
        /// </summary>
        /// <param name="obj">The entity</param>
        private void GoToEntity(object obj)
        {
            var identifiableObj = obj as IIdentifiable;

            if (obj != null
                && identifiableObj != null)
            {
                var masterDataObject = obj as IMasterDataObject;
                var ownedDataObject = obj as IOwnedObject;
                DbIdentifier databaseId = DbIdentifier.LocalDatabase;
                if ((masterDataObject != null && masterDataObject.IsMasterData)
                    || (ownedDataObject.Owner != null && ownedDataObject.Owner.Guid != ZPKTool.Business.SecurityManager.Instance.CurrentUser.Guid))
                {
                    databaseId = DbIdentifier.CentralDatabase;
                }

                NavigateToEntityMessage msg = new NavigateToEntityMessage(obj, databaseId);
                this.messenger.Send(msg, IsInViewerMode ? GlobalMessengerTokens.ModelBrowserTargetToken : GlobalMessengerTokens.MainViewTargetToken);
            }
        }

        /// <summary>
        /// Navigates to a specify entity based on a type
        /// </summary>
        /// <param name="obj">The object</param>
        private void NavigateToEntity(Tuple<object, int> obj)
        {
            if (obj == null)
            {
                return;
            }

            var item = obj.Item1 as CompareRowPropertyItem;
            int index = obj.Item2;
            if (item != null && index >= 0)
            {
                switch (item.ItemType)
                {
                    case CompareRowItemType.Property:
                        // Navigate to the entity represented by data
                        var columnHeader = item.ColumnHeaders.ElementAtOrDefault(index);
                        if (columnHeader != null && columnHeader.ParentEntity != null)
                        {
                            this.GoToEntity(columnHeader.ParentEntity);
                        }

                        break;

                    case CompareRowItemType.Cost:
                        // Navigate to the tab in result details for the corresponding entity
                        var header = item.ColumnHeaders.ElementAtOrDefault(index);
                        if (header != null && header.ParentEntity != null)
                        {
                            var entity = header.ParentEntity;

                            // If the entity is from master data it doesn't have costs so don't navigate anywhere
                            var masterDataObject = entity as IMasterDataObject;
                            if (masterDataObject != null && masterDataObject.IsMasterData)
                            {
                                break;
                            }

                            DbIdentifier databaseId = DbIdentifier.LocalDatabase;
                            var ownedDataObject = entity as IOwnedObject;
                            if (ownedDataObject != null && ownedDataObject.Owner != null && ownedDataObject.Owner.Guid != ZPKTool.Business.SecurityManager.Instance.CurrentUser.Guid)
                            {
                                databaseId = DbIdentifier.CentralDatabase;
                            }

                            var msg = new NavigateToEntityMessage(entity, databaseId);
                            msg.AdditionalData = item.ItemCostType;
                            this.messenger.Send(msg, IsInViewerMode ? GlobalMessengerTokens.ModelBrowserTargetToken : GlobalMessengerTokens.MainViewTargetToken);
                        }

                        break;

                    case CompareRowItemType.None:
                    case CompareRowItemType.SubEntity:
                        // Don't do anything if the type is unknown or is a sub-entity
                        // The case of sub-entity is processed in other place
                        break;
                }
            }
        }
    }
}