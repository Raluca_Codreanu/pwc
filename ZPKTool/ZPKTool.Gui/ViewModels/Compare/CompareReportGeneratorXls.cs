﻿using System;
using System.IO;
using NPOI.HPSF;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using ZPKTool.Business;
using ZPKTool.Gui.Reporting;
using ZPKTool.Gui.Resources;
using HorizontalAlignment = NPOI.SS.UserModel.HorizontalAlignment;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Creates report in xls for compared items.
    /// </summary>
    public class CompareReportGeneratorXls : CompareReportGenerator
    {
        #region Const and static readonly fields

        /// <summary>
        /// The name of the font to be used.
        /// </summary>
        private const string FontNameToUse = "Calibri";

        /// <summary>
        /// The max number of columns.
        /// </summary>
        private const int MaxColumnNumber = 256;

        /// <summary>
        /// The integer number format string.
        /// </summary>
        private const string IntegerFormatString = "#,##0";

        /// <summary>
        /// The decimal number format string.
        /// </summary>
        private const string DecimalFormatString = "#,##0.0###";

        #endregion

        #region Private members

        /// <summary>
        /// The empty cell style.
        /// </summary>
        private readonly ICellStyle emptyCellStyle;

        /// <summary>
        /// Style of a group header for middle cell.
        /// </summary>
        private readonly ICellStyle groupMiddleStyle;

        /// <summary>
        /// Style of  a group header for last cell.
        /// </summary>
        private readonly ICellStyle groupLastStyle;

        /// <summary>
        /// Style of the row header cell.
        /// </summary>
        private readonly ICellStyle rowHeaderStyle;

        /// <summary>
        /// Style of the last row header cell.
        /// </summary>
        private readonly ICellStyle lastRowHeaderStyle;

        /// <summary>
        /// ICell style with red font and thin border on bottom.
        /// </summary>
        private readonly ICellStyle redCellBottomBorderStyle;

        /// <summary>
        /// ICell style with red font and thin border on bottom and right.
        /// </summary>
        private readonly ICellStyle redCellBottomRightBorderStyle;

        /// <summary>
        /// ICell style with read font and thin border on the right.
        /// </summary>
        private readonly ICellStyle redCellRightBorderStyle;

        /// <summary>
        /// ICell style with wrapped read font and thin bottom border.
        /// </summary>
        private readonly ICellStyle redWrapBottomBorderStyle;

        /// <summary>
        /// ICell style with black font and thin border on bottom.
        /// </summary>
        private readonly ICellStyle blackCellBottomBorderStyle;

        /// <summary>
        /// ICell style with black font and thin border on bottom and right.
        /// </summary>
        private readonly ICellStyle blackCellBottomRightBorderStyle;

        /// <summary>
        /// ICell style with read font and thin border on the right.
        /// </summary>
        private readonly ICellStyle blackCellRightBorderStyle;

        /// <summary>
        /// ICell style with wrapped read font and thin bottom border.
        /// </summary>
        private readonly ICellStyle blackWrapBottomBorderStyle;

        /// <summary>
        /// Clone of the <see cref="RedCellStyle"/> having integer formatting.
        /// </summary>
        private readonly ICellStyle redIntegerCellStyle;

        /// <summary>
        /// Clone of the <see cref="RedCellStyle"/> having decimal formatting.
        /// </summary>
        private readonly ICellStyle redDecimalCellStyle;

        /// <summary>
        /// Clone of the <see cref="BlackCellStyle"/> having integer formatting.
        /// </summary>
        private readonly ICellStyle blackIntegerCellStyle;

        /// <summary>
        /// Clone of the <see cref="BlackCellStyle"/> having decimal formatting.
        /// </summary>
        private readonly ICellStyle blackDecimalCellStyle;

        /// <summary>
        /// Clone of the <see cref="redCellBottomBorderStyle"/> having integer formatting.
        /// </summary>
        private readonly ICellStyle redIntegerCellBottomBorderStyle;

        /// <summary>
        /// Clone of the <see cref="redCellBottomBorderStyle"/> having decimal formatting.
        /// </summary>
        private readonly ICellStyle redDecimalCellBottomBorderStyle;

        /// <summary>
        /// /// Clone of the <see cref="blackCellBottomBorderStyle"/> having integer formatting.
        /// </summary>
        private readonly ICellStyle blackIntegerCellBottomBorderStyle;

        /// <summary>
        /// /// Clone of the <see cref="blackCellBottomBorderStyle"/> having decimal formatting.
        /// </summary>
        private readonly ICellStyle blackDecimalCellBottomBorderStyle;

        /// <summary>
        /// Represents the total number of used columns.
        /// </summary>
        private int totalNumberOfColumns;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="CompareReportGeneratorXls"/> class.
        /// </summary>
        /// <param name="data">The data collected.</param>
        /// <param name="filePath">The file path.</param>
        /// <param name="author">The author.</param>
        public CompareReportGeneratorXls(CompareDataSheet data, string filePath, string author)
            : base(data, filePath, author)
        {
            this.Workbook = new HSSFWorkbook();
            var integerFormat = Workbook.CreateDataFormat();
            var decimalFormat = Workbook.CreateDataFormat();
            var palette = Workbook.GetCustomPalette();
            palette.SetColorAtIndex(IndexedColors.LIGHT_BLUE.Index, 219, 229, 241);

            // Black cell Style.
            IFont blackCellFont = this.Workbook.CreateFont();
            blackCellFont.Color = (short)FontColor.NORMAL;
            blackCellFont.FontName = FontNameToUse;
            
            this.BlackCellStyle = this.Workbook.CreateCellStyle();
            this.BlackCellStyle.SetFont(blackCellFont);
            this.BlackCellStyle.BorderBottom = BorderStyle.DOTTED;
            this.BlackCellStyle.BorderTop = BorderStyle.DOTTED;
            this.BlackCellStyle.BorderLeft = BorderStyle.DOTTED;
            this.BlackCellStyle.BorderRight = BorderStyle.DOTTED;

            blackCellBottomBorderStyle = Workbook.CreateCellStyle();
            blackCellBottomBorderStyle.CloneStyleFrom(BlackCellStyle);
            blackCellBottomBorderStyle.BorderBottom = BorderStyle.THIN;

            blackCellRightBorderStyle = Workbook.CreateCellStyle();
            blackCellRightBorderStyle.CloneStyleFrom(BlackCellStyle);
            blackCellRightBorderStyle.BorderRight = BorderStyle.THIN;

            blackCellBottomRightBorderStyle = Workbook.CreateCellStyle();
            blackCellBottomRightBorderStyle.CloneStyleFrom(blackCellBottomBorderStyle);
            blackCellBottomRightBorderStyle.BorderRight = BorderStyle.THIN;

            blackIntegerCellStyle = Workbook.CreateCellStyle();
            blackIntegerCellStyle.CloneStyleFrom(BlackCellStyle);
            blackIntegerCellStyle.DataFormat = integerFormat.GetFormat(IntegerFormatString);

            blackDecimalCellStyle = Workbook.CreateCellStyle();
            blackDecimalCellStyle.CloneStyleFrom(BlackCellStyle);
            blackDecimalCellStyle.DataFormat = decimalFormat.GetFormat(DecimalFormatString);

            blackIntegerCellBottomBorderStyle = Workbook.CreateCellStyle();
            blackIntegerCellBottomBorderStyle.CloneStyleFrom(blackCellBottomBorderStyle);
            blackIntegerCellBottomBorderStyle.DataFormat = integerFormat.GetFormat(IntegerFormatString);

            blackDecimalCellBottomBorderStyle = Workbook.CreateCellStyle();
            blackDecimalCellBottomBorderStyle.CloneStyleFrom(blackCellBottomBorderStyle);
            blackDecimalCellBottomBorderStyle.DataFormat = decimalFormat.GetFormat(DecimalFormatString);

            // Red color cell Style.
            IFont redCellFont = this.Workbook.CreateFont();
            redCellFont.Color = (short)FontColor.RED;
            redCellFont.FontName = FontNameToUse;

            this.RedCellStyle = this.Workbook.CreateCellStyle();
            this.RedCellStyle.SetFont(redCellFont);
            this.RedCellStyle.BorderBottom = BorderStyle.DOTTED;
            this.RedCellStyle.BorderTop = BorderStyle.DOTTED;
            this.RedCellStyle.BorderLeft = BorderStyle.DOTTED;
            this.RedCellStyle.BorderRight = BorderStyle.DOTTED;

            redCellBottomBorderStyle = Workbook.CreateCellStyle();
            redCellBottomBorderStyle.CloneStyleFrom(RedCellStyle);
            redCellBottomBorderStyle.BorderBottom = BorderStyle.THIN;

            redCellRightBorderStyle = Workbook.CreateCellStyle();
            redCellRightBorderStyle.CloneStyleFrom(RedCellStyle);
            redCellRightBorderStyle.BorderRight = BorderStyle.THIN;

            redCellBottomRightBorderStyle = Workbook.CreateCellStyle();
            redCellBottomRightBorderStyle.CloneStyleFrom(redCellBottomBorderStyle);
            redCellBottomRightBorderStyle.BorderRight = BorderStyle.THIN;

            redIntegerCellStyle = Workbook.CreateCellStyle();
            redIntegerCellStyle.CloneStyleFrom(RedCellStyle);
            redIntegerCellStyle.DataFormat = integerFormat.GetFormat(IntegerFormatString);

            redDecimalCellStyle = Workbook.CreateCellStyle();
            redDecimalCellStyle.CloneStyleFrom(RedCellStyle);
            redDecimalCellStyle.DataFormat = decimalFormat.GetFormat(DecimalFormatString);

            redIntegerCellBottomBorderStyle = Workbook.CreateCellStyle();
            redIntegerCellBottomBorderStyle.CloneStyleFrom(redCellBottomBorderStyle);
            redIntegerCellBottomBorderStyle.DataFormat = integerFormat.GetFormat(IntegerFormatString);

            redDecimalCellBottomBorderStyle = Workbook.CreateCellStyle();
            redDecimalCellBottomBorderStyle.CloneStyleFrom(redCellBottomBorderStyle);
            redDecimalCellBottomBorderStyle.DataFormat = decimalFormat.GetFormat(DecimalFormatString);

            // Black, Wrap ICell style.
            this.BlackWrapCellStyle = this.Workbook.CreateCellStyle();
            this.BlackWrapCellStyle.WrapText = true;
            this.BlackWrapCellStyle.SetFont(blackCellFont);
            this.BlackWrapCellStyle.BorderBottom = BorderStyle.DOTTED;
            this.BlackWrapCellStyle.BorderTop = BorderStyle.DOTTED;
            this.BlackWrapCellStyle.BorderLeft = BorderStyle.DOTTED;
            this.BlackWrapCellStyle.BorderRight = BorderStyle.DOTTED;

            blackWrapBottomBorderStyle = Workbook.CreateCellStyle();
            blackWrapBottomBorderStyle.CloneStyleFrom(BlackWrapCellStyle);
            blackWrapBottomBorderStyle.BorderBottom = BorderStyle.THIN;

            // Red, Wrap ICell style.
            this.RedWrapCellStyle = this.Workbook.CreateCellStyle();
            this.RedWrapCellStyle.WrapText = true;
            this.RedWrapCellStyle.SetFont(redCellFont);
            this.RedWrapCellStyle.BorderBottom = BorderStyle.DOTTED;
            this.RedWrapCellStyle.BorderTop = BorderStyle.DOTTED;
            this.RedWrapCellStyle.BorderLeft = BorderStyle.DOTTED;
            this.RedWrapCellStyle.BorderRight = BorderStyle.DOTTED;

            redWrapBottomBorderStyle = Workbook.CreateCellStyle();
            redWrapBottomBorderStyle.CloneStyleFrom(RedWrapCellStyle);
            redWrapBottomBorderStyle.BorderBottom = BorderStyle.THIN;

            // Simple Bold cell style.
            IFont boldcellFont = this.Workbook.CreateFont();
            boldcellFont.Boldweight = (short)FontBoldWeight.BOLD;
            boldcellFont.FontName = FontNameToUse;
            this.HeaderCellStyle = this.Workbook.CreateCellStyle();
            this.HeaderCellStyle.SetFont(boldcellFont);
            this.HeaderCellStyle.BorderBottom = BorderStyle.DOTTED;
            this.HeaderCellStyle.BorderTop = BorderStyle.DOTTED;
            this.HeaderCellStyle.BorderLeft = BorderStyle.DOTTED;
            this.HeaderCellStyle.BorderRight = BorderStyle.DOTTED;

            // The Style of the Row header (Not to be used for the last row).
            rowHeaderStyle = Workbook.CreateCellStyle();
            rowHeaderStyle.CloneStyleFrom(HeaderCellStyle);
            rowHeaderStyle.BorderLeft = BorderStyle.THIN;

            // The Style of the last row header.
            lastRowHeaderStyle = Workbook.CreateCellStyle();
            lastRowHeaderStyle.CloneStyleFrom(rowHeaderStyle);
            lastRowHeaderStyle.BorderBottom = BorderStyle.THIN;

            // Group with black color font cell style. This is the first cell of a group.
            IFont groupCellFont = this.Workbook.CreateFont();
            groupCellFont.Boldweight = (short)FontBoldWeight.BOLD;
            groupCellFont.FontHeightInPoints = 12;
            groupCellFont.IsItalic = true;
            groupCellFont.FontName = FontNameToUse;
            groupCellFont.Color = (short)FontColor.NORMAL;
            this.BlackGroupFirstCellStyle = this.Workbook.CreateCellStyle();
            this.BlackGroupFirstCellStyle.SetFont(groupCellFont);
            this.BlackGroupFirstCellStyle.FillPattern = FillPatternType.SOLID_FOREGROUND;
            this.BlackGroupFirstCellStyle.FillForegroundColor = IndexedColors.LIGHT_BLUE.Index;
            this.BlackGroupFirstCellStyle.BorderLeft = BorderStyle.THIN;
            this.BlackGroupFirstCellStyle.BorderTop = BorderStyle.THIN;
            this.BlackGroupFirstCellStyle.BorderBottom = BorderStyle.THIN;

            // Group with red color font cell style. This is the first cell of a group.
            IFont redgroupCellFont = this.Workbook.CreateFont();
            redgroupCellFont.Boldweight = (short)FontBoldWeight.BOLD;
            redgroupCellFont.FontHeightInPoints = 12;
            redgroupCellFont.IsItalic = true;
            redgroupCellFont.FontName = FontNameToUse;
            redgroupCellFont.Color = (short)FontColor.RED;
            this.RedGroupFirstCellStyle = this.Workbook.CreateCellStyle();
            this.RedGroupFirstCellStyle.SetFont(redgroupCellFont);
            this.RedGroupFirstCellStyle.FillPattern = FillPatternType.SOLID_FOREGROUND;
            this.RedGroupFirstCellStyle.FillForegroundColor = IndexedColors.LIGHT_BLUE.Index;
            this.RedGroupFirstCellStyle.BorderLeft = BorderStyle.THIN;
            this.RedGroupFirstCellStyle.BorderTop = BorderStyle.THIN;
            this.RedGroupFirstCellStyle.BorderBottom = BorderStyle.THIN;

            // Group cell style for cells in the middle of the line.
            groupMiddleStyle = this.Workbook.CreateCellStyle();
            this.groupMiddleStyle.FillPattern = FillPatternType.SOLID_FOREGROUND;
            this.groupMiddleStyle.FillForegroundColor = IndexedColors.LIGHT_BLUE.Index;
            this.groupMiddleStyle.BorderTop = BorderStyle.THIN;
            this.groupMiddleStyle.BorderBottom = BorderStyle.THIN;

            // Group cell style for cells in the end of the line.
            groupLastStyle = this.Workbook.CreateCellStyle();
            groupLastStyle.CloneStyleFrom(groupMiddleStyle);
            this.groupLastStyle.BorderRight = BorderStyle.THIN;

            // Apply the empty style on an empty cell. This will automatically apply it on all empty cells.
            emptyCellStyle = Workbook.CreateCellStyle();
            emptyCellStyle.FillPattern = FillPatternType.SOLID_FOREGROUND;
            emptyCellStyle.FillForegroundColor = IndexedColors.WHITE.Index;
            emptyCellStyle.FillBackgroundColor = IndexedColors.WHITE.Index;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the workbook.
        /// </summary>
        /// <value>
        /// The workbook.
        /// </value>
        private HSSFWorkbook Workbook { get; set; }

        /// <summary>
        /// Gets or sets the sheet.
        /// </summary>
        /// <value>
        /// The sheet.
        /// </value>
        private ISheet Sheet { get; set; }

        /// <summary>
        /// Gets or sets the current row.
        /// </summary>
        /// <value>
        /// The current row.
        /// </value>
        private IRow CurrentRow { get; set; }

        /// <summary>
        /// Gets or sets the black cell style.
        /// </summary>
        /// <value>
        /// The black cell style.
        /// </value>
        private ICellStyle BlackCellStyle { get; set; }

        /// <summary>
        /// Gets or sets the red cell style.
        /// </summary>
        /// <value>
        /// The red cell style.
        /// </value>
        private ICellStyle RedCellStyle { get; set; }

        /// <summary>
        /// Gets or sets the black cell style.
        /// </summary>
        /// <value>
        /// The black cell style.
        /// </value>
        private ICellStyle BlackWrapCellStyle { get; set; }

        /// <summary>
        /// Gets or sets the red cell style.
        /// </summary>
        /// <value>
        /// The red cell style.
        /// </value>
        private ICellStyle RedWrapCellStyle { get; set; }

        /// <summary>
        /// Gets or sets the header cell style.
        /// </summary>
        private ICellStyle HeaderCellStyle { get; set; }

        /// <summary>
        /// Gets or sets the group cell style.
        /// </summary>
        /// <value>
        /// The group cell style.
        /// </value>
        private ICellStyle BlackGroupFirstCellStyle { get; set; }

        /// <summary>
        /// Gets or sets the redgroup cell style.
        /// </summary>
        /// <value>
        /// The redgroup cell style.
        /// </value>
        private ICellStyle RedGroupFirstCellStyle { get; set; }

        /// <summary>
        /// Gets or sets the column number.
        /// </summary>
        /// <value>
        /// The column no.
        /// </value>
        protected int ColumnIndex { get; set; }

        /// <summary>
        /// Gets or sets the index of the row.
        /// </summary>
        /// <value>
        /// The index of the row.
        /// </value>
        protected int RowIndex { get; set; }

        /// <summary>
        /// Gets or sets the start index of the group.
        /// </summary>
        /// <value>
        /// The start index of the group.
        /// </value>
        protected int StartGroupIndex { get; set; }

        #endregion

        #region Create Document

        /// <summary>
        /// Creates the document.
        /// </summary>
        protected override void CreateDocument()
        {
            this.Sheet = this.Workbook.CreateSheet();
        }

        #endregion

        #region Add Summary, Headers, Group Header, Row Header, CellValue with Symbol

        /// <summary>
        /// Add the summary to the document.
        /// </summary>
        protected override void AddSummary()
        {
            if (!string.IsNullOrWhiteSpace(this.Author))
            {
                // Create a entry of SummaryInformation
                SummaryInformation summaryInfo = PropertySetFactory.CreateSummaryInformation();
                summaryInfo.Author = this.Author;
                this.Workbook.SummaryInformation = summaryInfo;
            }
        }

        /// <summary>
        /// Add the headers to the document.
        /// </summary>
        protected override void AddHeaders()
        {
            if (this.Sheet == null)
            {
                throw new InvalidOperationException("Create first the ISheet before adding the headers.");
            }

            // Set default cell style.
            for (var columnIndex = 0; columnIndex < MaxColumnNumber; columnIndex++)
            {
                Sheet.SetDefaultColumnStyle(columnIndex, emptyCellStyle);
            }

            // Header Row.
            var headerRow = this.Sheet.CreateRow(6);

            // Add the headers in the sheet.                
            this.ColumnIndex = 1;
            foreach (var header in this.CompareData.Headers)
            {
                var cell = headerRow.CreateCell(this.ColumnIndex);
                cell.CellStyle = this.HeaderCellStyle;
                cell.CellStyle.WrapText = true;
                cell.SetCellValue(header);
                this.ColumnIndex += 2;
            }

            totalNumberOfColumns = ColumnIndex;
            this.RowIndex = 7;
        }

        /// <summary>
        /// Adds the group header.
        /// </summary>
        /// <param name="group">The group.</param>
        protected override void AddGroupHeader(CompareDataGroup group)
        {
            if (this.Sheet == null)
            {
                throw new InvalidOperationException("Create first a ISheet before adding the group headers.");
            }

            // Set the content and style of the first cell.
            var groupRow = this.Sheet.CreateRow(this.RowIndex);
            var groupCell = groupRow.CreateCell(0);
            var cellStyle = group.IsHighlighted ? this.RedGroupFirstCellStyle : this.BlackGroupFirstCellStyle;
            groupCell.CellStyle = cellStyle;
            groupCell.SetCellValue(group.Name);

            // Set the style of middle cells in this row.
            for (var columnIndex = 1; columnIndex < totalNumberOfColumns - 1; columnIndex++)
            {
                var middleCell = groupRow.CreateCell(columnIndex);
                middleCell.CellStyle = groupMiddleStyle;
            }

            // The the style of the last cell in this row.
            if (totalNumberOfColumns > 0)
            {
                var lastCell = groupRow.CreateCell(totalNumberOfColumns - 1);
                lastCell.CellStyle = groupLastStyle;
            }

            this.StartGroupIndex = this.RowIndex + 1;
        }

        /// <summary>
        /// Adds the row header.
        /// </summary>
        /// <param name="header">The header.</param>
        /// <param name="isLastInGroup">if set to <c>true</c> [is last in group].</param>
        protected override void AddRowHeader(string header, bool isLastInGroup)
        {
            if (this.Sheet == null)
            {
                throw new InvalidOperationException("Create a document with a sheet before trying to add headers.");
            }

            this.RowIndex++;

            this.CurrentRow = this.Sheet.CreateRow(this.RowIndex);
            var headerCell = this.CurrentRow.CreateCell(0);
            headerCell.CellStyle = isLastInGroup ? lastRowHeaderStyle : rowHeaderStyle;
            headerCell.SetCellValue(header);

            this.ColumnIndex = 1;
        }

        /// <summary>
        /// Adds the cell value.
        /// </summary>
        /// <param name="content">The content.</param>
        /// <param name="symbol">The symbol.</param>
        /// <param name="isHighlighted">if set to <c>true</c> [is highlighted].</param>
        /// <param name="isLastGroup">if set to <c>true</c> [is last in group].</param>
        /// <param name="isLastInCompareRow">if set to <c>true</c> [is last in compare row].</param>
        protected override void AddCellValueAndSymbol(object content, string symbol, bool isHighlighted, bool isLastGroup, bool isLastInCompareRow)
        {
            if (this.CurrentRow == null)
            {
                throw new InvalidOperationException("Create a row first and than add cell values.");
            }

            // Add the value to the cell.
            var valueCell = this.CurrentRow.CreateCell(this.ColumnIndex);

            ICellStyle valueCellStyleToApply;
            ICellStyle symbolCellStyleToApply;
            ICellStyle wrapCellStyleToApply;

            this.GetCellStyleToApply(isHighlighted, isLastGroup, isLastInCompareRow, out valueCellStyleToApply, out symbolCellStyleToApply, out wrapCellStyleToApply);

            if (content == null)
            {
                valueCell.CellStyle = valueCellStyleToApply;
            }
            else
            {
                if (content is bool)
                {
                    valueCell.CellStyle = valueCellStyleToApply;
                    valueCell.SetCellValue((bool)content);
                }
                else if (content is string)
                {
                    decimal decimalNumber;
                    if (decimal.TryParse((string)content, out decimalNumber))
                    {
                        valueCell.CellStyle = GetCorrespondingNumberStyle(valueCellStyleToApply, decimalNumber == decimal.Round(decimalNumber));
                        valueCell.SetCellValue(decimalNumber);
                    }
                    else
                    {
                        valueCell.CellStyle = wrapCellStyleToApply;
                        valueCell.SetCellValue((string)content);
                    }
                }
            }

            this.ColumnIndex++;

            // Add the symbol to the cell.
            var unitCell = this.CurrentRow.CreateCell(this.ColumnIndex);
            unitCell.CellStyle = symbolCellStyleToApply;
            unitCell.SetCellValue(symbol);

            this.ColumnIndex++;
        }

        #endregion

        #region Group rows

        /// <summary>
        /// Groups the rows.
        /// </summary>
        /// <param name="isExpanded">if set to <c>true</c> [is expanded].</param>
        protected override void GroupRows(bool isExpanded)
        {
            if (this.Sheet == null)
            {
                throw new InvalidOperationException("Create first the ISheet before grouping rows.");
            }

            // Group rows.
            this.Sheet.GroupRow(this.StartGroupIndex, this.RowIndex);
            this.Sheet.SetRowGroupCollapsed(this.StartGroupIndex, !isExpanded);

            this.RowIndex += 2;
        }

        /// <summary>
        /// Autoes the size columns.
        /// </summary>
        protected override void AutoSizeColumns()
        {
            SetPrintArea();

            if (this.Sheet == null)
            {
                throw new InvalidOperationException("Create first a document with a ISheet before trying to autosize to columns.");
            }

            // Set the width of the sheet's columns to auto.
            for (int i = 0; i < this.ColumnIndex; i++)
            {
                this.Sheet.AutoSizeColumn(i);
            }

            int numberOfHeaders = this.CompareData.Headers.Count;

            for (int index = 1; index <= numberOfHeaders * 2; index++)
            {
                if (index % 2 == 0)
                {
                    this.Sheet.SetColumnWidth(index, 2000);
                }
                else
                {
                    this.Sheet.SetColumnWidth(index, 9000);
                }
            }

            AddTitleAndRelatedInfo();
        }

        #endregion

        #region Write to file

        /// <summary>
        /// Writes to file.
        /// </summary>
        protected override void WriteToFile()
        {
            using (FileStream file = new FileStream(this.FilePath, FileMode.Create))
            {
                this.Workbook.Write(file);
            }
        }

        #endregion

        /// <summary>
        /// Adds the title and related info.
        /// </summary>
        private void AddTitleAndRelatedInfo()
        {
            // Define the background color of the title cell.
            var palette = Workbook.GetCustomPalette();
            palette.SetColorAtIndex(IndexedColors.BLUE.Index, 31, 73, 125);

            // Create the title font.
            var titleFont = this.Workbook.CreateFont();
            titleFont.Color = IndexedColors.WHITE.Index;
            titleFont.FontHeightInPoints = 18;
            titleFont.Boldweight = (short)FontBoldWeight.BOLD;
            titleFont.FontName = FontNameToUse;

            // Create the title style.
            var titleCellStyle = Workbook.CreateCellStyle();
            titleCellStyle.SetFont(titleFont);
            titleCellStyle.FillPattern = FillPatternType.SOLID_FOREGROUND;
            titleCellStyle.FillForegroundColor = IndexedColors.BLUE.Index;
            titleCellStyle.Alignment = HorizontalAlignment.CENTER;

            // Create the title row.
            var titleRow = Sheet.CreateRow(0);
            titleRow.HeightInPoints = 33;

            // Create the merged cells for the title.
            var titleCellRange = new CellRangeAddress(0, 0, 0, totalNumberOfColumns - 1);
            Sheet.AddMergedRegion(titleCellRange);

            // Set cell content and style.
            var titleCell = titleRow.CreateCell(0);
            titleCell.SetCellValue(LocalizedResources.Comparison_ReportTitle);
            titleCell.CellStyle = titleCellStyle;

            // Create author and date header cell style
            var boldGreyCellFont = this.Workbook.CreateFont();
            boldGreyCellFont.Boldweight = (short)FontBoldWeight.BOLD;
            boldGreyCellFont.FontName = FontNameToUse;
            boldGreyCellFont.Color = IndexedColors.GREY_50_PERCENT.Index;
            var authorHeaderStyle = Workbook.CreateCellStyle();
            authorHeaderStyle.CloneStyleFrom(HeaderCellStyle);
            authorHeaderStyle.SetFont(boldGreyCellFont);

            // Create author and date value cell style
            var authorValueStyle = Workbook.CreateCellStyle();
            authorValueStyle.CloneStyleFrom(BlackCellStyle);
            authorValueStyle.Alignment = HorizontalAlignment.CENTER;

            // Set the Author
            var authorRow = Sheet.CreateRow(2);
            var authorLabelCell = authorRow.CreateCell(0);
            authorLabelCell.SetCellValue(LocalizedResources.Report_Author);
            authorLabelCell.CellStyle = authorHeaderStyle;

            var authorValueCell = authorRow.CreateCell(1);
            authorValueCell.SetCellValue(SecurityManager.Instance.CurrentUser.Username);
            authorValueCell.CellStyle = authorValueStyle;

            // Separator Line
            var separatorRow = Sheet.CreateRow(3);
            separatorRow.HeightInPoints = 4.5f;

            // Set the Date
            var dateRow = Sheet.CreateRow(4);
            var dateLabelCell = dateRow.CreateCell(0);
            dateLabelCell.SetCellValue(LocalizedResources.Report_Date);
            dateLabelCell.CellStyle = authorHeaderStyle;

            var dateValueCell = dateRow.CreateCell(1);
            dateValueCell.SetCellValue(DateTime.Now.ToShortDateString());
            dateValueCell.CellStyle = authorValueStyle;
        }

        /// <summary>
        /// Sets the print area.
        /// </summary>
        private void SetPrintArea()
        {
            Sheet.PrintSetup.FitWidth = 1;
            Sheet.PrintSetup.FitHeight = 0;
            Sheet.FitToPage = true;
            Workbook.SetPrintArea(0, 0, ColumnIndex > 0 ? ColumnIndex - 1 : ColumnIndex, 0, RowIndex);
        }

        /// <summary>
        /// Gets the cell style to apply.
        /// </summary>
        /// <param name="isHighlighted">if set to <c>true</c> [is highlighted].</param>
        /// <param name="isLastGroup">if set to <c>true</c> [is last group].</param>
        /// <param name="isLastInCompareRow">if set to <c>true</c> [is last in compare row].</param>
        /// <param name="valueCellStyleToApply">The value cell style to apply.</param>
        /// <param name="symbolCellStyleToApply">The symbol cell style to apply.</param>
        /// <param name="wrapCellStyleToApply">The wrap cell style to apply.</param>
        private void GetCellStyleToApply(
            bool isHighlighted,
            bool isLastGroup,
            bool isLastInCompareRow,
            out ICellStyle valueCellStyleToApply,
            out ICellStyle symbolCellStyleToApply,
            out ICellStyle wrapCellStyleToApply)
        {
            valueCellStyleToApply = isHighlighted ? this.RedCellStyle : BlackCellStyle;
            symbolCellStyleToApply = isHighlighted ? this.RedCellStyle : BlackCellStyle;
            wrapCellStyleToApply = isHighlighted ? this.RedWrapCellStyle : BlackWrapCellStyle;

            if (isLastGroup && isLastInCompareRow)
            {
                valueCellStyleToApply = isHighlighted ? redCellBottomBorderStyle : blackCellBottomBorderStyle;
                symbolCellStyleToApply = isHighlighted ? redCellBottomRightBorderStyle : blackCellBottomRightBorderStyle;
                wrapCellStyleToApply = isHighlighted ? redWrapBottomBorderStyle : blackWrapBottomBorderStyle;
            }
            else
            {
                if (isLastGroup)
                {
                    valueCellStyleToApply = isHighlighted ? redCellBottomBorderStyle : blackCellBottomBorderStyle;
                    symbolCellStyleToApply = isHighlighted ? redCellBottomBorderStyle : blackCellBottomBorderStyle;
                    wrapCellStyleToApply = isHighlighted ? redWrapBottomBorderStyle : blackWrapBottomBorderStyle;
                }
                else
                {
                    if (isLastInCompareRow)
                    {
                        symbolCellStyleToApply = isHighlighted ? redCellRightBorderStyle : blackCellRightBorderStyle;
                    }
                }
            }
        }

        /// <summary>
        /// Gets the corresponding number style.
        /// </summary>
        /// <param name="theSourceCellStyle">The value cell style to apply.</param>
        /// <param name="isInteger">if set to <c>true</c> [is integer].</param>
        /// <returns>
        /// The corresponding style containing decimal formatting.
        /// </returns>
        private ICellStyle GetCorrespondingNumberStyle(ICellStyle theSourceCellStyle, bool isInteger)
        {
            var result = theSourceCellStyle;

            if (Equals(result, this.RedCellStyle))
            {
                result = isInteger ? redIntegerCellStyle : redDecimalCellStyle;
            }
            else
            {
                if (Equals(result, this.BlackCellStyle))
                {
                    result = isInteger ? blackIntegerCellStyle : blackDecimalCellStyle;
                }
                else
                {
                    if (Equals(result, this.redCellBottomBorderStyle))
                    {
                        result = isInteger ? redIntegerCellBottomBorderStyle : redDecimalCellBottomBorderStyle;
                    }
                    else
                    {
                        if (Equals(result, this.blackCellBottomBorderStyle))
                        {
                            result = isInteger ? blackIntegerCellBottomBorderStyle : blackDecimalCellBottomBorderStyle;
                        }
                    }
                }
            }

            return result;
        }
    }
}
