﻿using System;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Globalization;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml;
using ZPKTool.Common;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the TransportCostCalculationDrivingRoute view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class TransportCostCalculationDrivingRouteViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The app key received from Map Quest API.
        /// </summary>
        private const string MapQuestAppKey = "Fmjtd%7Cluubnuut2h%2C8a%3Do5-9u1n5w";

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The PleaseWaitService reference.
        /// </summary>
        private IPleaseWaitService pleaseWaitService;

        /// <summary>
        /// The information for the current route.
        /// </summary>        
        private RouteInformation routeInfo;

        /// <summary>
        /// The image containing the map with the route.
        /// </summary>
        private ImageSource mapImage;

        /// <summary>
        /// A value indicating whether the map with the route is displayed or not.
        /// </summary>
        private bool showMap;

        /// <summary>
        /// A value indicating whether this instance is loading the route map or not.
        /// </summary>
        private bool isLoading;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="TransportCostCalculationDrivingRouteViewModel"/> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        /// <param name="messenger">The messenger.</param>
        /// <param name="pleaseWaitService">The please wait service.</param>
        [ImportingConstructor]
        public TransportCostCalculationDrivingRouteViewModel(IWindowService windowService, IMessenger messenger, IPleaseWaitService pleaseWaitService)
        {
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("pleaseWaitService", pleaseWaitService);

            this.windowService = windowService;
            this.messenger = messenger;
            this.pleaseWaitService = pleaseWaitService;

            this.ShowMap = UserSettingsManager.Instance.ShowMapWindow;
            this.RouteInfo = new RouteInformation();
        }

        #region Properties

        /// <summary>
        /// Gets or sets the latitude coordinate of the route's source.
        /// </summary>
        public decimal SourceLatitude { get; set; }

        /// <summary>
        /// Gets or sets the longitude coordinate of the route's source.
        /// </summary>
        public decimal SourceLongitude { get; set; }

        /// <summary>
        /// Gets or sets the latitude coordinate of the route's destination.
        /// </summary>
        public decimal DestinationLatitude { get; set; }

        /// <summary>
        /// Gets or sets the longitude coordinate of the route's destination.
        /// </summary>
        public decimal DestinationLongitude { get; set; }

        /// <summary>
        /// Gets or sets the information for the current route.
        /// </summary>
        public RouteInformation RouteInfo
        {
            get { return this.routeInfo; }
            set { this.SetProperty(ref this.routeInfo, value, () => this.RouteInfo); }
        }

        /// <summary>
        /// Gets or sets the image containing the map with the route.
        /// </summary>
        public ImageSource MapImage
        {
            get { return this.mapImage; }
            set { this.SetProperty(ref this.mapImage, value, () => this.MapImage); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the map with the route is displayed or not.
        /// </summary>
        public bool ShowMap
        {
            get { return this.showMap; }
            set { this.SetProperty(ref this.showMap, value, () => this.ShowMap); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is loading the route map or not.
        /// </summary>
        public bool IsLoading
        {
            get { return this.isLoading; }
            set { this.SetProperty(ref this.isLoading, value, () => this.IsLoading); }
        }

        #endregion

        /// <summary>
        /// Called after the view has been loaded.
        /// </summary>
        public override void OnLoaded()
        {
            base.OnLoaded();
            this.CalculateDrivingDistanceAndRoute();
        }

        /// <summary>
        /// Calculates the driving distance and route displayed on the map from the source to destination.
        /// </summary>
        private void CalculateDrivingDistanceAndRoute()
        {
            this.IsLoading = true;
            Task.Factory.StartNew(() =>
            {
                this.RouteInfo = this.GetRouteDirections();
                if (!this.RouteInfo.HasError && !string.IsNullOrWhiteSpace(this.RouteInfo.SessionId))
                {
                    // If the map directions return a session id, the map with the route can be requested.
                    this.DisplayRouteMap(this.RouteInfo.SessionId);
                    this.IsLoading = false;
                }
                else
                {
                    this.IsLoading = false;
                }
            });
        }

        /// <summary>
        /// Gets the route directions from the Mapquest API and the calculated distance between the source and destination coordinates.
        /// </summary>
        /// <returns>Information about the route.</returns>
        public RouteInformation GetRouteDirections()
        {
            // Create the url for the request sent to Mapquest API.
            string urlForDirections = string.Format(
                CultureInfo.InvariantCulture,
                "http://open.mapquestapi.com/directions/v1/route?key={0}&outFormat=xml&unit=k&from={1},{2}&to={3},{4}&doReverseGeocode=false&narrativeType=none&stateBoundaryDisplay=false&countryBoundaryDisplay=false&sideOfStreetDisplay=false&destinationManeuverDisplay=false",
                MapQuestAppKey,
                this.SourceLatitude,
                this.SourceLongitude,
                this.DestinationLatitude,
                this.DestinationLongitude);

            var routeInfo = new RouteInformation();
            string responseReader = string.Empty;

            HttpWebRequest routeRequest = (HttpWebRequest)WebRequest.Create(urlForDirections);
            try
            {
                using (WebResponse routeResponse = routeRequest.GetResponse())
                {
                    // Read the response from the Mapquest API.
                    Stream responseToStream = routeResponse.GetResponseStream();
                    StreamReader responseToStreamReader = new StreamReader(responseToStream);
                    responseReader = responseToStreamReader.ReadToEnd();

                    // Get the route information from the response.
                    routeInfo = this.GetRouteInformation(responseReader, routeInfo);
                }
            }
            catch (WebException ex)
            {
                log.ErrorException("Could not connect to route directions from Mapquest API through internet.", ex);
                routeInfo.ErrorMessage = LocalizedResources.DrivingRoute_ServiceConnectionError;
            }

            return routeInfo;
        }

        /// <summary>
        /// Gets the route information object containing the distance and session id for the current route, or an error message if any errors appear.
        /// </summary>
        /// <param name="responseReader">The string with the response got from the web request.</param>
        /// <param name="routeInfo">The route information object.</param>
        /// <returns>The route information object containing the distance and session id or the error message.</returns>
        private RouteInformation GetRouteInformation(string responseReader, RouteInformation routeInfo)
        {
            try
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(responseReader);

                // Get the info node to check if the status is ok.
                var infoNode = xmldoc.SelectSingleNode("/response/info");
                if (infoNode != null)
                {
                    var statusNode = infoNode.SelectSingleNode("statusCode");
                    if (statusNode != null)
                    {
                        if (statusNode.InnerText == "0")
                        {
                            // If the status code is OK, the distance and session id can be read from the response.
                            var distanceNode = xmldoc.SelectSingleNode("/response/route/distance");
                            if (distanceNode != null && !string.IsNullOrWhiteSpace(distanceNode.InnerText))
                            {
                                // Get the distance value.
                                routeInfo.Distance = Convert.ToDecimal(distanceNode.InnerText, CultureInfo.InvariantCulture);
                            }

                            var sessionIdNode = xmldoc.SelectSingleNode("/response/route/sessionId");
                            if (sessionIdNode != null)
                            {
                                // Get the session id.
                                routeInfo.SessionId = sessionIdNode.InnerText;
                            }
                        }
                        else if (Convert.ToInt32(statusNode.InnerText) >= 600 && Convert.ToInt32(statusNode.InnerText) < 700)
                        {
                            // If the status code has a value between 600 and 699, the route request was valid,
                            // but there are no driving directions for the given coordinates.
                            log.Error("The request to Mapquest API had invalid coordinates for calculating the directions; the following error code was returned: {0}.", statusNode.InnerText);
                            routeInfo.ErrorMessage = LocalizedResources.DrivingRoute_InvalidLatLongError;
                        }
                        else
                        {
                            // Handle the case of wrong status code or missing info node.
                            var messageNode = infoNode.SelectSingleNode("messages");
                            if (messageNode != null)
                            {
                                log.Error("The following error message was returned from Mapquest API: {0}.", messageNode.InnerText);
                                routeInfo.ErrorMessage = messageNode.InnerText;
                            }
                            else
                            {
                                log.Error("The response returned from Mapquest API is not well formed.");
                                routeInfo.ErrorMessage = LocalizedResources.DrivingRoute_ServiceConnectionError;
                            }
                        }
                    }
                    else
                    {
                        // Handle the case of missing status code node.
                        log.Error("The response returned from Mapquest API is not well formed.");
                        routeInfo.ErrorMessage = LocalizedResources.DrivingRoute_ServiceConnectionError;
                    }
                }
            }
            catch (XmlException ex)
            {
                log.ErrorException("An error occurred during parsing the response from Mapquest API for route directions into a XML document.", ex);
                routeInfo.ErrorMessage = LocalizedResources.DrivingRoute_DataError;
            }
            catch (ArgumentNullException ex)
            {
                log.ErrorException("An error occurred during parsing the response from Mapquest API for route directions into a XML document.", ex);
                routeInfo.ErrorMessage = LocalizedResources.DrivingRoute_DataError;
            }

            return routeInfo;
        }

        /// <summary>
        /// Gets the map containing the route for the directions calculated in the previous request to Mapquest API, using the session id.
        /// </summary>
        /// <param name="sessionId">The session id.</param>
        private void DisplayRouteMap(string sessionId)
        {
            // Create the url for the request sent to Mapquest API.
            string urlForMap = string.Format(
                CultureInfo.InvariantCulture,
                "http://open.mapquestapi.com/staticmap/v4/getmap?key={0}&size=640,480&bestfit={1},{2},{3},{4}&session={5}",
                MapQuestAppKey,
                this.SourceLatitude,
                this.SourceLongitude,
                this.DestinationLatitude,
                this.DestinationLongitude,
                sessionId);

            HttpWebRequest staticMapRequest = (HttpWebRequest)WebRequest.Create(urlForMap);
            try
            {
                using (var webResponse = (HttpWebResponse)staticMapRequest.GetResponse())
                {
                    using (BinaryReader reader = new BinaryReader(webResponse.GetResponseStream()))
                    {
                        // Read the bytes from the Mapquest API response into a byte stream.
                        int bufferSize = 2048;
                        int bytesRead;
                        byte[] buffer = new byte[bufferSize];
                        MemoryStream byteStream = new MemoryStream();

                        while ((bytesRead = reader.Read(buffer, 0, bufferSize)) > 0)
                        {
                            byteStream.Write(buffer, 0, bytesRead);
                        }

                        try
                        {
                            // Create a bitmap image from the byte stream.
                            BitmapImage bitmapImage = new BitmapImage();
                            bitmapImage.BeginInit();
                            bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                            bitmapImage.StreamSource = byteStream;
                            bitmapImage.StreamSource.Position = 0L;
                            bitmapImage.EndInit();
                            bitmapImage.Freeze();

                            // Set the map image from the bitmap image.
                            this.MapImage = bitmapImage;
                        }
                        catch (InvalidOperationException ex)
                        {
                            log.ErrorException("The static map image obtained from Mapquest API is not a valid image file.", ex);
                            this.RouteInfo.ErrorMessage = LocalizedResources.DrivingRoute_DisplayMapError;
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                log.ErrorException("Could not connect to static map from Mapquest API through internet.", ex);
                this.RouteInfo.ErrorMessage = LocalizedResources.DrivingRoute_DisplayMapError;
            }
        }

        /// <summary>
        /// Closes the view associated with this instance.
        /// </summary>
        protected override void Close()
        {
            if (UserSettingsManager.Instance.ShowMapWindow != this.ShowMap)
            {
                // If the user modified the value of ShowMapWindow setting, the new value is saved.
                UserSettingsManager.Instance.ShowMapWindow = this.ShowMap;
                UserSettingsManager.Instance.Save();
            }

            this.windowService.CloseViewWindow(this);
        }

        #region Inner Class

        /// <summary>
        /// Stores information about the route.
        /// </summary>
        public class RouteInformation : ObservableObject
        {
            #region Attributes

            /// <summary>
            /// The distance between the source and destination coordinates.
            /// </summary>
            private decimal distance;

            /// <summary>
            /// The error message that describes the problems occurred while sending or getting the results from Mapquest API.
            /// </summary>
            private string errorMessage;

            #endregion

            #region Properties

            /// <summary>
            /// Gets or sets the distance between the source and destination coordinates.
            /// </summary>
            public decimal Distance
            {
                get { return this.distance; }
                set { this.SetProperty(ref this.distance, value, () => this.Distance); }
            }

            /// <summary>
            /// Gets or sets the error message that describes the problems occurred while sending or getting the results from Mapquest API.
            /// </summary>
            public string ErrorMessage
            {
                get
                {
                    return this.errorMessage;
                }

                set
                {
                    this.SetProperty(ref this.errorMessage, value, () => this.ErrorMessage);
                    if (string.IsNullOrWhiteSpace(value))
                    {
                        this.HasError = false;
                    }
                    else
                    {
                        this.HasError = true;
                    }
                }
            }

            /// <summary>
            /// Gets or sets the session id of the request sent to Mapquest API to get the directions between coordinates.
            /// </summary>
            public string SessionId { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether the route info has an error or not.
            /// </summary>
            public bool HasError { get; set; }

            #endregion
        }

        #endregion
    }
}
