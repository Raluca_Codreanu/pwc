﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Contains information that configures the start up of the Model Viewer.
    /// </summary>
    public class ModelViewerStartupInfo
    {
        /// <summary>
        /// Gets or sets a value indicating whether the app should display the Model Viewer window at startup.
        /// </summary>        
        public bool StartInViewerMode { get; set; }

        /// <summary>
        /// Gets or sets the model file path to load when the Model Viewer window is displayed.
        /// </summary>        
        public string ModelFilePath { get; set; }
    }
}
