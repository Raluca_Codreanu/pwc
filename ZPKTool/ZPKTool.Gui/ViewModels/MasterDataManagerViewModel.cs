﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ZPKTool.Business;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for the master data manager screen
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class MasterDataManagerViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The messenger reference
        /// </summary>
        private readonly IMessenger messenger;

        /// <summary>
        /// The composition container reference which is used to get other view-models
        /// </summary>
        private readonly CompositionContainer compositionContainer;

        /// <summary>
        /// The data manager on which the master data entities are attached
        /// </summary>
        private readonly IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

        /// <summary>
        /// The window service reference
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The PleaseWaitService reference.
        /// </summary>
        private IPleaseWaitService pleaseWaitService;

        /// <summary>
        /// The collection of objects which are used in the binding on the data grid
        /// </summary>
        private ObservableCollection<object> masterDataItems = new ObservableCollection<object>();

        /// <summary>
        /// The currently selected items in the grid.
        /// </summary>
        private ObservableCollection<object> selectedItems = new ObservableCollection<object>();

        /// <summary>
        /// Reference to the user control loaded in the details section of the screen
        /// </summary>
        private object detailsContent;

        /// <summary>
        /// The type of the master data to display in the browser.
        /// </summary>
        private Type masterDataType;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        /// <summary>
        /// The import entity.
        /// </summary>
        private bool importTheEntity;

        #endregion

        /// <summary>
        /// Initializes a new instance of the MasterDataManagerViewModel class.
        /// </summary>
        /// <param name="container">The composition container.</param>
        /// <param name="messenger">The messenger service.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="pleaseWaitService">The please wait service.</param>
        /// <param name="unitsService">The units service.</param>
        [ImportingConstructor]
        public MasterDataManagerViewModel(
            CompositionContainer container,
            IMessenger messenger,
            IWindowService windowService,
            IPleaseWaitService pleaseWaitService,
            IUnitsService unitsService)
        {
            Argument.IsNotNull("container", container);
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("pleaseWaitService", pleaseWaitService);
            Argument.IsNotNull("unitsService", unitsService);

            this.messenger = messenger;
            this.WindowService = windowService;
            this.compositionContainer = container;
            this.pleaseWaitService = pleaseWaitService;
            this.unitsService = unitsService;

            this.AddCommand = new DelegateCommand(AddEntity);
            this.DeleteCommand = new DelegateCommand(this.DeleteSelectedEntities, this.CanDeleteSelectedEntities);
            this.EditCommand = new DelegateCommand(
                () => EditEntity(this.SelectedItems.FirstOrDefault()),
                () => this.SelectedItems != null && this.SelectedItems.Count == 1);
            this.ImportCommand = new DelegateCommand(this.Import);
            this.ExportCommand = new DelegateCommand(this.Export);
            this.MouseDoubleClickCommand = new DelegateCommand<MouseButtonEventArgs>(MouseDoubleClicked);

            this.IsLoaded = false;

            this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(this.dataContext);
            this.SelectedItems.CollectionChanged += SelectedItemsOnCollectionChanged;
        }

        #region Commands

        /// <summary>
        /// Gets the command for the add button
        /// </summary>
        public ICommand AddCommand { get; private set; }

        /// <summary>
        /// Gets the command for the edit button
        /// </summary>
        public ICommand EditCommand { get; private set; }

        /// <summary>
        /// Gets the command for the delete button
        /// </summary>
        public ICommand DeleteCommand { get; private set; }

        /// <summary>
        /// Gets the command for the import button.
        /// </summary>
        public ICommand ImportCommand { get; private set; }

        /// <summary>
        /// Gets the command for the export button.
        /// </summary>
        public ICommand ExportCommand { get; private set; }

        /// <summary>
        /// Gets the command for the mouse double click event
        /// </summary>
        public ICommand MouseDoubleClickCommand { get; private set; }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the collection of objects displayed in the data grid
        /// </summary>
        public ObservableCollection<object> MasterDataItems
        {
            get
            {
                return this.masterDataItems;
            }

            set
            {
                if (this.masterDataItems != value)
                {
                    this.masterDataItems = value;
                    OnPropertyChanged(() => this.MasterDataItems);
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected items in the entity data grid
        /// </summary>
        public ObservableCollection<object> SelectedItems
        {
            get
            {
                return this.selectedItems;
            }

            set
            {
                if (this.selectedItems != value)
                {
                    this.selectedItems = value;
                    OnPropertyChanged(() => this.SelectedItems);
                }
            }
        }

        /// <summary>
        /// Gets or sets the user control loaded in the details content area of the screen
        /// </summary>
        public object DetailsContent
        {
            get
            {
                return this.detailsContent;
            }

            set
            {
                if (this.detailsContent != value)
                {
                    this.detailsContent = value;
                    OnPropertyChanged(() => this.DetailsContent);
                }
            }
        }

        /// <summary>
        /// Gets or sets the window service.
        /// </summary>
        public IWindowService WindowService
        {
            get
            {
                return this.windowService;
            }

            set
            {
                if (this.windowService != value)
                {
                    this.windowService = value;
                    OnPropertyChanged(() => this.WindowService);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [import the entity].
        /// </summary>
        public bool ImportTheEntity
        {
            get
            {
                return this.importTheEntity;
            }

            set
            {
                if (this.importTheEntity != value)
                {
                    this.importTheEntity = value;
                    OnPropertyChanged(() => this.ImportTheEntity);
                }
            }
        }

        /// <summary>
        /// Gets or sets the type of the master data entities which are managed in this instance.
        /// </summary>
        public Type MasterDataType
        {
            get { return this.masterDataType; }
            set { this.SetProperty(ref this.masterDataType, value, () => this.MasterDataType, this.OnMasterDataTypeChanged); }
        }

        /// <summary>
        /// Gets or sets the id of the item to select after the master data is loaded.
        /// </summary>        
        public Guid ItemToSelectId { get; set; }

        /// <summary>
        /// Gets or sets the DataGrid's persist ID, depending on the loaded items' type.
        /// </summary>
        public string DataGridPersistId { get; set; }

        /// <summary>
        /// Gets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter { get; private set; }

        #endregion

        /// <summary>
        /// Called after the view has been loaded. Usually it performs some initialization that needs the view to be loaded, like
        /// starting to load data from a database.
        /// <para/>
        /// During unit tests this method must be manually called because there is no view to call it.
        /// </summary>
        public override void OnLoaded()
        {
            base.OnLoaded();
            this.LoadMasterDataItems();
        }

        /// <summary>
        /// Registers the necessary message handlers with the Messenger service.
        /// </summary>
        private void RegisterMessageHandlers()
        {
            this.messenger.Register<EntityImportedMessage>(this.HandleNotificationMessage);
        }

        /// <summary>
        /// Handles some generic messages, of type EntityImportedMessage.
        /// </summary>
        /// <param name="message">The message.</param>
        protected virtual void HandleNotificationMessage(EntityImportedMessage message)
        {
            if (message.EntityType == ImportedEntityType.RawMaterial
                || message.EntityType == ImportedEntityType.Machine)
            {
                this.LoadMasterDataItems();
            }
        }

        /// <summary>
        /// Sets the DataGrid's persist ID according to the master data type.
        /// </summary>
        private void OnMasterDataTypeChanged()
        {
            if (MasterDataType == typeof(Machine))
            {
                this.DataGridPersistId = "1D706C5A-C6CB-4AD0-9EBC-A9B76A4BDCF3";
            }
            else if (MasterDataType == typeof(RawPart))
            {
                this.DataGridPersistId = "AF0FA69C-1625-48BC-A28A-DC64F3F64BF3";
            }
            else if (MasterDataType == typeof(Part))
            {
                this.DataGridPersistId = "C4D0B5AB-EFC2-448A-AE1E-9DCB6779DA7C";
            }
            else if (MasterDataType == typeof(Assembly))
            {
                this.DataGridPersistId = "A3F175F1-783C-4F00-A6C6-60E32FB76B02";
            }
            else if (MasterDataType == typeof(Die))
            {
                this.DataGridPersistId = "4E56D174-E024-4C89-A7D2-CA53E7F21F1A";
            }
            else if (MasterDataType == typeof(Commodity))
            {
                this.DataGridPersistId = "41A1E9E9-3682-40D0-B6FD-1B5E0481E563";
            }
            else if (MasterDataType == typeof(Consumable))
            {
                this.DataGridPersistId = "6B385DF2-7D8E-4D41-A9C8-95532BA31BEA";
            }
            else if (MasterDataType == typeof(RawMaterial))
            {
                this.DataGridPersistId = "3EE75F41-D97B-4C3B-9B8A-0199A777C925";
            }
            else if (MasterDataType == typeof(Customer))
            {
                this.DataGridPersistId = "E8788E51-EF1E-441D-B39E-50A840990B76";
            }
            else if (MasterDataType == typeof(Manufacturer))
            {
                this.DataGridPersistId = "FD71D8DB-59E1-4E10-829A-A151F76AA07C";
            }
        }

        /// <summary>
        /// Loads the master data items.
        /// </summary>
        private void LoadMasterDataItems()
        {
            this.MasterDataItems.Clear();
            IEnumerable<object> data = null;

            Action<PleaseWaitService.WorkParams> work = (workParams) =>
            {
                data = LoadDataFromDB();
            };

            Action<PleaseWaitService.WorkParams> workCompleted = (workParams) =>
            {
                if (workParams.Error != null)
                {
                    this.windowService.MessageDialogService.Show(workParams.Error);
                }
                else
                {
                    // Add the loaded data to the data source collection for the view.
                    this.MasterDataItems.AddRange(data);

                    // Select the item specified by the ItemToSelectId property.
                    var itemToSelect =
                        this.MasterDataItems.OfType<IIdentifiable>()
                            .FirstOrDefault(it => it.Guid == this.ItemToSelectId);
                    if (itemToSelect != null)
                    {
                        this.SelectedItems.Add(itemToSelect);
                    }

                    if (MasterDataType == typeof(Machine)
                        || MasterDataType == typeof(RawMaterial))
                    {
                        this.ImportTheEntity = true;
                    }

                    // Set to true the value that indicates is the view model is loaded.
                    this.IsLoaded = true;
                }
            };

            this.pleaseWaitService.Show(LocalizedResources.General_LoadingMasterData, work, workCompleted);
        }

        /// <summary>
        /// Loads the data from DB based on the selected entity type
        /// </summary>
        /// <returns>The loaded data.</returns>
        private IEnumerable<object> LoadDataFromDB()
        {
            if (this.MasterDataType == typeof(Assembly))
            {
                return dataContext.AssemblyRepository.GetTopLevelMasterData().OrderBy(assy => assy.Name);
            }

            if (this.MasterDataType == typeof(Part))
            {
                return dataContext.PartRepository.GetAllMasterDataPart().OrderBy(part => part.Name);
            }

            if (this.MasterDataType == typeof(RawPart))
            {
                return dataContext.PartRepository.GetAllMasterDataRawPart().OrderBy(part => part.Name);
            }

            if (this.MasterDataType == typeof(Machine))
            {
                return dataContext.MachineRepository.GetAllMasterData().OrderBy(m => m.Name);
            }

            if (this.MasterDataType == typeof(RawMaterial))
            {
                return dataContext.RawMaterialRepository.GetAllMasterData().OrderBy(m => m.Name);
            }

            if (this.MasterDataType == typeof(Consumable))
            {
                return dataContext.ConsumableRepository.GetAllMasterData().OrderBy(c => c.Name);
            }

            if (this.MasterDataType == typeof(Commodity))
            {
                return dataContext.CommodityRepository.GetAllMasterData().OrderBy(c => c.Name);
            }

            if (this.MasterDataType == typeof(Die))
            {
                return dataContext.DieRepository.GetAllMasterData().OrderBy(d => d.Name);
            }

            if (this.MasterDataType == typeof(Manufacturer))
            {
                return dataContext.ManufacturerRepository.GetAllMasterData().OrderBy(m => m.Name);
            }

            if (this.MasterDataType == typeof(Customer))
            {
                return dataContext.SupplierRepository.FindAllMasterData().OrderBy(s => s.Name);
            }

            return new List<object>();
        }

        /// <summary>
        /// Performs the add operation, creating a new entity and adding it in the data grid based on the type of master data displayed
        /// </summary>
        private void AddEntity()
        {
            if (this.MasterDataType == typeof(Assembly))
            {
                var assyVm = this.compositionContainer.GetExportedValue<AssemblyViewModel>();
                assyVm.DataSourceManager = this.dataContext;
                assyVm.InitializeForAssemblyCreation(true, null);

                this.windowService.ShowViewInDialog(assyVm, "CreateAssemblyViewTemplate");
                var newAssembly = assyVm.Model;
                if (assyVm.Saved && newAssembly != null)
                {
                    this.SelectedItems.Clear();
                    this.MasterDataItems.Add(newAssembly);
                    this.SelectedItems.Add(newAssembly);
                }
            }
            else if (this.MasterDataType == typeof(Part) || this.MasterDataType == typeof(RawPart))
            {
                var partVm = this.compositionContainer.GetExportedValue<PartViewModel>();
                partVm.DataSourceManager = this.dataContext;
                partVm.InitializeForCreation(true, null, this.MasterDataType == typeof(RawPart));

                this.windowService.ShowViewInDialog(partVm, this.MasterDataType == typeof(Part) ? "PartWindowViewTemplate" : "RawPartWindowViewTemplate");

                var newPart = partVm.Model;
                if (partVm.Saved && newPart != null)
                {
                    this.SelectedItems.Clear();
                    this.MasterDataItems.Add(newPart);
                    this.SelectedItems.Add(newPart);
                }
            }
            else if (this.MasterDataType == typeof(Machine))
            {
                var machineVm = this.compositionContainer.GetExportedValue<MachineViewModel>();
                machineVm.Title = LocalizedResources.General_CreateMachine;
                machineVm.DataSourceManager = this.dataContext;
                machineVm.InitializeForCreation(true, null);
                this.windowService.ShowViewInDialog(machineVm, "MachineWindowViewTemplate");

                var machine = machineVm.Model;
                if (machineVm.Saved && machine != null)
                {
                    this.SelectedItems.Clear();
                    this.MasterDataItems.Add(machine);
                    this.SelectedItems.Add(machine);
                }
            }
            else if (this.MasterDataType == typeof(RawMaterial))
            {
                var rawMaterialVm = this.compositionContainer.GetExportedValue<RawMaterialViewModel>();
                rawMaterialVm.DataSourceManager = this.dataContext;
                rawMaterialVm.InitializeForCreation(true, null, null);
                rawMaterialVm.Title = LocalizedResources.General_CreateRawMaterial;

                this.windowService.ShowViewInDialog(rawMaterialVm, "RawMaterialWindowViewTemplate");

                var newRawMaterial = rawMaterialVm.Model;
                if (rawMaterialVm.Saved && newRawMaterial != null)
                {
                    this.SelectedItems.Clear();
                    this.MasterDataItems.Add(newRawMaterial);
                    this.SelectedItems.Add(newRawMaterial);
                }
            }
            else if (this.MasterDataType == typeof(Consumable))
            {
                var consumableVm = this.compositionContainer.GetExportedValue<ConsumableViewModel>();
                consumableVm.DataSourceManager = this.dataContext;
                consumableVm.InitializeForCreation(true, null);
                consumableVm.Title = LocalizedResources.General_CreateConsumable;

                this.windowService.ShowViewInDialog(consumableVm, "ConsumableWindowViewTemplate");

                var newConsumable = consumableVm.Model;
                if (consumableVm.Saved && newConsumable != null)
                {
                    this.SelectedItems.Clear();
                    this.MasterDataItems.Add(newConsumable);
                    this.SelectedItems.Add(newConsumable);
                }
            }
            else if (this.MasterDataType == typeof(Commodity))
            {
                var commodityVm = this.compositionContainer.GetExportedValue<CommodityViewModel>();
                commodityVm.DataSourceManager = this.dataContext;
                commodityVm.InitializeForCreation(true, null);
                commodityVm.Title = LocalizedResources.General_CreateCommodity;

                this.windowService.ShowViewInDialog(commodityVm, "CommodityWindowViewTemplate");

                var newCommodity = commodityVm.Model;
                if (commodityVm.Saved && newCommodity != null)
                {
                    this.SelectedItems.Clear();
                    this.MasterDataItems.Add(newCommodity);
                    this.SelectedItems.Add(newCommodity);
                }
            }
            else if (this.MasterDataType == typeof(Die))
            {
                var dieVm = this.compositionContainer.GetExportedValue<DieViewModel>();
                dieVm.DataSourceManager = this.dataContext;
                dieVm.IsReadOnly = this.IsReadOnly;
                dieVm.IsInViewerMode = false;
                dieVm.InitializeForCreation(true, null);
                dieVm.Title = LocalizedResources.General_CreateDie;
                this.windowService.ShowViewInDialog(dieVm, "DieViewTemplate");

                var newDie = dieVm.Model;

                if (dieVm.Saved && newDie != null)
                {
                    this.SelectedItems.Clear();
                    this.MasterDataItems.Add(newDie);
                    this.SelectedItems.Add(newDie);
                }
            }
            else if (this.MasterDataType == typeof(Manufacturer))
            {
                var manufacturerVm = this.compositionContainer.GetExportedValue<ManufacturerViewModel>();
                manufacturerVm.EditMode = ViewModelEditMode.Create;
                manufacturerVm.ShowSaveControls = true;
                manufacturerVm.Title = LocalizedResources.General_CreateManufacturer;
                manufacturerVm.DataSourceManager = this.dataContext;

                var model = new Manufacturer();
                model.IsMasterData = true;
                manufacturerVm.Model = model;

                this.windowService.ShowViewInDialog(manufacturerVm, "ManufacturerWindowViewTemplate");
                var createdManufacturer = manufacturerVm.Model;
                if (manufacturerVm.Saved && createdManufacturer != null)
                {
                    this.SelectedItems.Clear();
                    this.MasterDataItems.Add(createdManufacturer);
                    this.SelectedItems.Add(createdManufacturer);
                }
            }
            else if (this.MasterDataType == typeof(Customer))
            {
                var supplierVm = this.compositionContainer.GetExportedValue<SupplierViewModel>();
                supplierVm.EditMode = ViewModelEditMode.Create;
                supplierVm.ShowSaveControls = true;
                supplierVm.DataSourceManager = this.dataContext;
                supplierVm.Title = LocalizedResources.General_CreateSupplier;

                var newSupplier = new Customer();
                newSupplier.IsMasterData = true;
                supplierVm.Model = newSupplier;

                this.windowService.ShowViewInDialog(supplierVm, "SupplierWindowViewTemplate");
                var model = supplierVm.Model;
                if (supplierVm.Saved && model != null)
                {
                    this.SelectedItems.Clear();
                    this.MasterDataItems.Add(model);
                    this.SelectedItems.Add(newSupplier);
                }
            }
        }

        /// <summary>
        /// Imports master data.
        /// </summary>
        private void Import()
        {
            var masterDataImporterExporter = this.compositionContainer.GetExportedValue<MasterDataImporterExporterViewModel>();
            masterDataImporterExporter.Operation = MasterDataOperation.Import;
            masterDataImporterExporter.DataManager = this.dataContext;

            if (this.MasterDataType == typeof(RawMaterial))
            {
                masterDataImporterExporter.EntityType = ImportedEntityType.RawMaterial;
                this.windowService.ShowViewInDialog(masterDataImporterExporter);
            }
            else if (this.MasterDataType == typeof(Machine))
            {
                masterDataImporterExporter.EntityType = ImportedEntityType.Machine;
                this.windowService.ShowViewInDialog(masterDataImporterExporter);
            }
        }

        /// <summary>
        /// Exports the master data.
        /// </summary>
        private void Export()
        {
            var masterDataImporterExporter = this.compositionContainer.GetExportedValue<MasterDataImporterExporterViewModel>();
            masterDataImporterExporter.Operation = MasterDataOperation.Export;
            masterDataImporterExporter.DataManager = this.dataContext;

            if (this.MasterDataType == typeof(RawMaterial))
            {
                masterDataImporterExporter.EntityType = ImportedEntityType.RawMaterial;
                this.windowService.ShowViewInDialog(masterDataImporterExporter);
            }
            else if (this.MasterDataType == typeof(Machine))
            {
                masterDataImporterExporter.EntityType = ImportedEntityType.Machine;
                this.windowService.ShowViewInDialog(masterDataImporterExporter);
            }
        }

        /// <summary>
        /// Handles the double click event on the data grid
        /// </summary>
        /// <param name="e">The mouse button event args</param>
        private void MouseDoubleClicked(MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                // try to find the clicked one
                var source = (DependencyObject)e.OriginalSource;
                var row = UIHelper.FindParent<DataGridRow>(source);
                if (row != null && row.Item is IIdentifiable)
                {
                    EditEntity(row.Item);
                }
            }
        }

        /// <summary>
        /// Shows the edit screen popup for the selected item in the data grid
        /// </summary>
        /// <param name="editedEntity">The entity which will populate the edit screen</param>
        private void EditEntity(object editedEntity)
        {
            if (editedEntity == null)
            {
                return;
            }

            // check if the entity has a parent and warn the user.
            if (this.CheckIfEntityHasParent(editedEntity))
            {
                var result =
                    this.windowService.MessageDialogService.Show(LocalizedResources.Question_HasParentEdit, MessageDialogType.YesNo);
                if (result == MessageDialogResult.No)
                {
                    return;
                }
            }

            if (this.MasterDataType == typeof(Assembly))
            {
                this.EditAssembly(editedEntity as Assembly);
            }
            else if (this.MasterDataType == typeof(Part) || this.MasterDataType == typeof(RawPart))
            {
                this.EditPart(editedEntity as Part);
            }
            else if (this.MasterDataType == typeof(Machine))
            {
                this.EditMachine(editedEntity as Machine);
            }
            else if (this.MasterDataType == typeof(RawMaterial))
            {
                this.EditRawMaterial(editedEntity as RawMaterial);
            }
            else if (this.MasterDataType == typeof(Consumable))
            {
                this.EditConsumable(editedEntity as Consumable);
            }
            else if (this.MasterDataType == typeof(Commodity))
            {
                this.EditCommodity(editedEntity as Commodity);
            }
            else if (this.MasterDataType == typeof(Die))
            {
                this.EditDie(editedEntity as Die);
            }
            else if (this.MasterDataType == typeof(Manufacturer))
            {
                this.EditManufacturer(editedEntity as Manufacturer);
            }
            else if (this.MasterDataType == typeof(Customer))
            {
                this.EditCustomer(editedEntity as Customer);
            }

            this.LoadSelectedEntityDetails();
        }

        /// <summary>
        /// Edits the assembly.
        /// </summary>
        /// <param name="assembly">The assembly to edit.</param>
        private void EditAssembly(Assembly assembly)
        {
            if (assembly == null)
            {
                throw new ArgumentNullException("assembly", "The assembly was null.");
            }

            Assembly assyClone = null;
            if (assembly.IsStockMasterData)
            {
                var dataSourceManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
                var cloneManager = new CloneManager(dataSourceManager, dataSourceManager);

                var fullAssembly = dataSourceManager.AssemblyRepository.GetAssemblyFull(assembly.Guid);
                assyClone = cloneManager.Clone(fullAssembly);

                dataSourceManager.AssemblyRepository.Add(assyClone);
                dataSourceManager.SaveChanges();

                this.SelectedItems.Add(assyClone);
            }

            // if the entity assembly, just signal that the entity must be added in the tree to be edited
            var message = new EditMasterDataEntityMessage(assyClone ?? assembly);
            this.messenger.Send(message);
        }

        /// <summary>
        /// Edits the part.
        /// </summary>
        /// <param name="part">The part to edit.</param>
        private void EditPart(Part part)
        {
            if (part == null)
            {
                throw new ArgumentNullException("part", "The part was null.");
            }

            Part partClone = null;
            if (part.IsStockMasterData)
            {
                var dataSourceManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
                var cloneManager = new CloneManager(dataSourceManager, dataSourceManager);

                var fullPart = dataSourceManager.PartRepository.GetPartFull(part.Guid);
                partClone = cloneManager.Clone(fullPart);

                dataSourceManager.PartRepository.Add(partClone);
                dataSourceManager.SaveChanges();

                this.SelectedItems.Add(partClone);
            }

            // if the entity assembly, just signal that the entity must be added in the tree to be edited
            var message = new EditMasterDataEntityMessage(partClone ?? part);
            this.messenger.Send(message);
        }

        /// <summary>
        /// Edits the machine.
        /// </summary>
        /// <param name="machine">The machine to edit.</param>
        private void EditMachine(Machine machine)
        {
            if (machine == null)
            {
                throw new ArgumentNullException("machine", "The machine was null.");
            }

            Machine machineClone = null;
            if (machine.IsStockMasterData)
            {
                var cloneManager = new CloneManager(this.dataContext, this.dataContext);
                machineClone = cloneManager.Clone(machine);
            }

            var machineVm = this.compositionContainer.GetExportedValue<MachineViewModel>();
            machineVm.EditMode = ViewModelEditMode.Edit;
            machineVm.DataSourceManager = this.dataContext;
            machineVm.Model = machineClone ?? machine;
            machineVm.Title = LocalizedResources.General_EditMachine;
            this.windowService.ShowViewInDialog(machineVm, "MachineWindowViewTemplate");

            if (machineVm.Saved && machineClone != null)
            {
                this.SelectedItems.Clear();
                machineClone.IsStockMasterData = false;
                var index = this.MasterDataItems.IndexOf(machine);
                if (index != -1)
                {
                    this.MasterDataItems.Insert(index + 1, machineClone);
                    this.SelectedItems.Add(machineClone);
                }
            }
        }

        /// <summary>
        /// Edits the raw material.
        /// </summary>
        /// <param name="material">The raw material to edit.</param>
        private void EditRawMaterial(RawMaterial material)
        {
            if (material == null)
            {
                throw new ArgumentNullException("material", "The raw material was null.");
            }

            RawMaterial materialClone = null;
            if (material.IsStockMasterData)
            {
                var cloneManager = new CloneManager(this.dataContext, this.dataContext);
                materialClone = cloneManager.Clone(material);
            }

            var rawMaterialVm = this.compositionContainer.GetExportedValue<RawMaterialViewModel>();
            rawMaterialVm.EditMode = ViewModelEditMode.Edit;
            rawMaterialVm.DataSourceManager = this.dataContext;
            rawMaterialVm.Model = materialClone ?? material;
            rawMaterialVm.Title = LocalizedResources.General_EditRawMaterial;
            this.windowService.ShowViewInDialog(rawMaterialVm, "RawMaterialWindowViewTemplate");

            if (rawMaterialVm.Saved && materialClone != null)
            {
                this.SelectedItems.Clear();
                materialClone.IsStockMasterData = false;
                var index = this.MasterDataItems.IndexOf(material);
                if (index != -1)
                {
                    this.MasterDataItems.Insert(index + 1, materialClone);
                    this.SelectedItems.Add(materialClone);
                }
            }
        }

        /// <summary>
        /// Edits the consumable.
        /// </summary>
        /// <param name="consumable">The consumable to edit.</param>
        private void EditConsumable(Consumable consumable)
        {
            if (consumable == null)
            {
                throw new ArgumentNullException("consumable", "The consumable was null.");
            }

            Consumable consumableClone = null;
            if (consumable.IsStockMasterData)
            {
                var cloneManager = new CloneManager(this.dataContext, this.dataContext);
                consumableClone = cloneManager.Clone(consumable);
            }

            var consumableVm = this.compositionContainer.GetExportedValue<ConsumableViewModel>();
            consumableVm.EditMode = ViewModelEditMode.Edit;
            consumableVm.DataSourceManager = this.dataContext;
            consumableVm.Model = consumableClone ?? consumable;
            consumableVm.Title = LocalizedResources.General_EditConsumable;
            this.windowService.ShowViewInDialog(consumableVm, "ConsumableWindowViewTemplate");

            if (consumableVm.Saved && consumableClone != null)
            {
                this.SelectedItems.Clear();
                consumableClone.IsStockMasterData = false;
                var index = this.MasterDataItems.IndexOf(consumable);
                if (index != -1)
                {
                    this.MasterDataItems.Insert(index + 1, consumableClone);
                    this.SelectedItems.Add(consumableClone);
                }
            }
        }

        /// <summary>
        /// Edits the commodity.
        /// </summary>
        /// <param name="commodity">The commodity to edit.</param>
        private void EditCommodity(Commodity commodity)
        {
            if (commodity == null)
            {
                throw new ArgumentNullException("commodity", "The commodity was null.");
            }

            Commodity commodityClone = null;
            if (commodity.IsStockMasterData)
            {
                var cloneManager = new CloneManager(this.dataContext, this.dataContext);
                commodityClone = cloneManager.Clone(commodity);
            }

            var commodityVm = this.compositionContainer.GetExportedValue<CommodityViewModel>();
            commodityVm.EditMode = ViewModelEditMode.Edit;
            commodityVm.DataSourceManager = this.dataContext;
            commodityVm.Model = commodityClone ?? commodity;
            commodityVm.Title = LocalizedResources.General_EditCommodity;
            this.windowService.ShowViewInDialog(commodityVm, "CommodityWindowViewTemplate");

            if (commodityVm.Saved && commodityClone != null)
            {
                this.SelectedItems.Clear();
                commodityClone.IsStockMasterData = false;
                var index = this.MasterDataItems.IndexOf(commodity);
                if (index != -1)
                {
                    this.MasterDataItems.Insert(index + 1, commodityClone);
                    this.SelectedItems.Add(commodityClone);
                }
            }
        }

        /// <summary>
        /// Edits the die.
        /// </summary>
        /// <param name="die">The die to edit.</param>
        private void EditDie(Die die)
        {
            if (die == null)
            {
                throw new ArgumentNullException("die", "The die was null.");
            }

            Die dieClone = null;
            if (die.IsStockMasterData)
            {
                var cloneManager = new CloneManager(this.dataContext, this.dataContext);
                dieClone = cloneManager.Clone(die);
            }

            var dieVm = this.compositionContainer.GetExportedValue<DieViewModel>();
            dieVm.EditMode = ViewModelEditMode.Edit;
            dieVm.DataSourceManager = this.dataContext;
            dieVm.IsReadOnly = this.IsReadOnly;
            dieVm.IsInViewerMode = false;
            dieVm.Model = dieClone ?? die;
            dieVm.Title = LocalizedResources.General_EditDie;
            this.windowService.ShowViewInDialog(dieVm, "DieViewTemplate");

            if (dieVm.Saved && dieClone != null)
            {
                this.SelectedItems.Clear();
                dieClone.IsStockMasterData = false;
                var index = this.MasterDataItems.IndexOf(die);
                if (index != -1)
                {
                    this.MasterDataItems.Insert(index + 1, dieClone);
                    this.SelectedItems.Add(dieClone);
                }
            }
        }

        /// <summary>
        /// Edits the manufacturer.
        /// </summary>
        /// <param name="manufacturer">The manufacturer to edit.</param>
        private void EditManufacturer(Manufacturer manufacturer)
        {
            if (manufacturer == null)
            {
                throw new ArgumentNullException("manufacturer", "The manufacturer was null.");
            }

            Manufacturer manufacturerClone = null;
            if (manufacturer.IsStockMasterData)
            {
                var cloneManager = new CloneManager(this.dataContext, this.dataContext);
                manufacturerClone = cloneManager.Clone(manufacturer);
            }

            var viewModel = this.compositionContainer.GetExportedValue<ManufacturerViewModel>();
            viewModel.EditMode = ViewModelEditMode.Edit;
            viewModel.DataSourceManager = this.dataContext;
            viewModel.Model = manufacturerClone ?? manufacturer;
            viewModel.ShowSaveControls = true;
            viewModel.Title = LocalizedResources.General_EditManufacturer;
            this.windowService.ShowViewInDialog(viewModel, "ManufacturerWindowViewTemplate");

            if (viewModel.Saved && manufacturerClone != null)
            {
                this.SelectedItems.Clear();
                manufacturerClone.IsStockMasterData = false;
                var index = this.MasterDataItems.IndexOf(manufacturer);
                if (index != -1)
                {
                    this.MasterDataItems.Insert(index + 1, manufacturerClone);
                    this.SelectedItems.Add(manufacturerClone);
                }
            }
        }

        /// <summary>
        /// Edits the customer.
        /// </summary>
        /// <param name="customer">The customer to edit.</param>
        private void EditCustomer(Customer customer)
        {
            if (customer == null)
            {
                throw new ArgumentNullException("customer", "The customer was null.");
            }

            Customer customerClone = null;
            if (customer.IsStockMasterData)
            {
                var cloneManager = new CloneManager(this.dataContext, this.dataContext);
                customerClone = cloneManager.Clone(customer);
            }

            var viewModel = this.compositionContainer.GetExportedValue<SupplierViewModel>();
            viewModel.EditMode = ViewModelEditMode.Edit;
            viewModel.ShowSaveControls = true;
            viewModel.Title = LocalizedResources.General_EditSupplier;
            viewModel.DataSourceManager = this.dataContext;
            viewModel.Model = customerClone ?? customer;

            this.windowService.ShowViewInDialog(viewModel, "SupplierWindowViewTemplate");

            if (viewModel.Saved && customerClone != null)
            {
                this.SelectedItems.Clear();
                customerClone.IsStockMasterData = false;
                var index = this.MasterDataItems.IndexOf(customer);
                if (index != -1)
                {
                    this.MasterDataItems.Insert(index + 1, customerClone);
                    this.SelectedItems.Add(customerClone);
                }
            }
        }

        /// <summary>
        /// Determines whether the DeleteSelectedEntities command can be executed.
        /// </summary>
        /// <returns>
        /// true if the command can be executed with the specified parameter; otherwise, false.
        /// </returns>
        public bool CanDeleteSelectedEntities()
        {
            return this.SelectedItems != null && this.SelectedItems.Count > 0;
        }

        /// <summary>
        /// Deletes a master data entity based on the type of the input parameter
        /// </summary>
        private void DeleteSelectedEntities()
        {
            if (this.SelectedItems == null || this.SelectedItems.Count == 0)
            {
                this.windowService.MessageDialogService.Show(LocalizedResources.Error_NoItemSelected, MessageDialogType.Info);
                return;
            }

            if (this.SelectedItems.Any(this.CheckIfEntityHasParent))
            {
                this.windowService.MessageDialogService.Show(LocalizedResources.Error_CantDeleteHasParents, MessageDialogType.Info);
                return;
            }

            var itemsToDelete = this.SelectedItems.ToList();
            var questionMessage = LocalizedResources.Question_DeleteSelectedObjects;
            if (this.windowService.MessageDialogService.Show(questionMessage, MessageDialogType.YesNo) != MessageDialogResult.Yes)
            {
                return;
            }

            Action<PleaseWaitService.WorkParams> work = (workParams) =>
            {
                var itemIndex = 1;
                foreach (var itemToDelete in itemsToDelete)
                {
                    string waitMessage;
                    if (selectedItems.Count == 1)
                    {
                        waitMessage = LocalizedResources.General_WaitItemDelete;
                    }
                    else
                    {
                        waitMessage = string.Format(
                            LocalizedResources.General_WaitDeleteObjects,
                            itemIndex,
                            selectedItems.Count);
                    }

                    this.pleaseWaitService.Message = waitMessage;
                    itemIndex++;

                    /*
                     * Permanently delete the selected item.
                     */
                    var manager = new TrashManager(this.dataContext);
                    manager.DeleteByStoreProcedure(itemToDelete);
                }
            };

            Action<PleaseWaitService.WorkParams> workCompleted = (workParams) =>
            {
                if (workParams.Error != null)
                {
                    this.windowService.MessageDialogService.Show(workParams.Error);
                }
                else
                {
                    var reloadMasterDataItems = false;
                    foreach (var deletedEntity in itemsToDelete)
                    {
                        /*
                         * If the deleted entity is an assembly reload the master data items to refresh the grid.
                         * This is necessary because the subassemblies of the deleted assembly 
                         * also need to be removed from the grid.
                         */
                        if (deletedEntity is Assembly)
                        {
                            reloadMasterDataItems = true;
                        }
                        else
                        {
                            this.MasterDataItems.Remove(deletedEntity);
                        }

                        /*
                         * For parts and assemblies send a message to delete the potential nodes in the tree
                         */
                        if (deletedEntity is Assembly || deletedEntity is Part)
                        {
                            var message = new EntityChangedMessage(Notification.MasterDataEntityChanged);
                            message.Entity = deletedEntity;
                            message.ChangeType = EntityChangeType.EntityDeleted;
                            this.messenger.Send(message);
                        }
                    }

                    if (reloadMasterDataItems)
                    {
                        /*
                         * Reload the master data items to refresh the grid.
                         */
                        this.LoadMasterDataItems();
                    }
                }
            };

            this.pleaseWaitService.Show(LocalizedResources.General_WaitItemDelete, work, workCompleted);
        }

        /// <summary>
        /// Loads the details screen of the selected entity based on the entity type
        /// </summary>
        private void LoadSelectedEntityDetails()
        {
            if (this.SelectedItems == null || this.SelectedItems.Count != 1)
            {
                this.DetailsContent = null;
                return;
            }

            var selectedItem = this.SelectedItems.FirstOrDefault();
            if (this.MasterDataType == typeof(Assembly))
            {
                AssemblyViewModel assyVm = null;
                var assembly = selectedItem as Assembly;
                if (assembly != null)
                {
                    assyVm = this.compositionContainer.GetExportedValue<AssemblyViewModel>();
                    assyVm.IsReadOnly = true;
                    assyVm.DataSourceManager = this.dataContext;
                    assyVm.Model = assembly;
                }

                this.DetailsContent = assyVm;
            }
            else if (this.MasterDataType == typeof(Part) || this.MasterDataType == typeof(RawPart))
            {
                PartViewModel partVm = null;
                var part = selectedItem as Part;
                if (part != null)
                {
                    partVm = this.compositionContainer.GetExportedValue<PartViewModel>();
                    partVm.IsReadOnly = true;
                    partVm.DataSourceManager = this.dataContext;
                    partVm.Model = part;
                }

                this.DetailsContent = partVm;
            }
            else if (this.MasterDataType == typeof(Machine))
            {
                var machineVm = this.compositionContainer.GetExportedValue<MachineViewModel>();
                machineVm.IsReadOnly = true;
                machineVm.DataSourceManager = this.dataContext;
                machineVm.Model = selectedItem as Machine;
                this.DetailsContent = machineVm;
            }
            else if (this.MasterDataType == typeof(RawMaterial))
            {
                var rawMaterialVm = this.compositionContainer.GetExportedValue<RawMaterialViewModel>();
                rawMaterialVm.IsReadOnly = true;
                rawMaterialVm.DataSourceManager = this.dataContext;
                rawMaterialVm.Model = selectedItem as RawMaterial;
                this.DetailsContent = rawMaterialVm;
            }
            else if (this.MasterDataType == typeof(Consumable))
            {
                var consumableVm = this.compositionContainer.GetExportedValue<ConsumableViewModel>();
                consumableVm.IsReadOnly = true;
                consumableVm.DataSourceManager = this.dataContext;
                consumableVm.Model = selectedItem as Consumable;
                this.DetailsContent = consumableVm;
            }
            else if (this.MasterDataType == typeof(Commodity))
            {
                var commodityVm = this.compositionContainer.GetExportedValue<CommodityViewModel>();
                commodityVm.IsReadOnly = true;
                commodityVm.DataSourceManager = this.dataContext;
                commodityVm.Model = selectedItem as Commodity;
                this.DetailsContent = commodityVm;
            }
            else if (this.MasterDataType == typeof(Die))
            {
                var dieVm = this.compositionContainer.GetExportedValue<DieViewModel>();
                dieVm.IsReadOnly = true;
                dieVm.DataSourceManager = this.dataContext;
                dieVm.Model = selectedItem as Die;
                this.DetailsContent = dieVm;
            }
            else if (this.MasterDataType == typeof(Manufacturer))
            {
                ManufacturerViewModel manufacturerVm = null;
                var manufacturer = selectedItem as Manufacturer;
                if (manufacturer != null)
                {
                    manufacturerVm = this.compositionContainer.GetExportedValue<ManufacturerViewModel>();
                    manufacturerVm.IsReadOnly = true;
                    manufacturerVm.DataSourceManager = this.dataContext;
                    manufacturerVm.Model = manufacturer;
                }

                this.DetailsContent = manufacturerVm;
            }
            else if (this.MasterDataType == typeof(Customer))
            {
                SupplierViewModel supplierVm = null;
                var supplier = selectedItem as Customer;
                if (supplier != null)
                {
                    supplierVm = this.compositionContainer.GetExportedValue<SupplierViewModel>();
                    supplierVm.IsReadOnly = true;
                    supplierVm.DataSourceManager = this.dataContext;
                    supplierVm.Model = supplier;
                }

                this.DetailsContent = supplierVm;
            }
        }

        /// <summary>
        /// Handle the SelectedItems collection changed event.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="NotifyCollectionChangedEventArgs"/> instance containing the event data.</param>
        private void SelectedItemsOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            this.LoadSelectedEntityDetails();
        }

        #region Helpers

        /// <summary>
        /// Determines whether the specified entity has a parent.
        /// </summary>
        /// <param name="entity">The entity to verify if has parent or not.</param>
        /// <returns><c>true</c> if the specified entity has parent; otherwise, <c>false</c>.</returns>
        private bool CheckIfEntityHasParent(object entity)
        {
            var dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            if (entity is Assembly || entity.GetType() == typeof(Part))
            {
                if (dataManager.AssemblyRepository.GetParentAssembly(entity) != null)
                {
                    return true;
                }
            }

            if (entity is RawMaterial || entity is Commodity || entity is RawPart)
            {
                if (dataManager.PartRepository.GetParentPart(entity) != null)
                {
                    return true;
                }
            }

            if (entity is Consumable || entity is Machine || entity is Die || entity is Commodity)
            {
                if (dataManager.ProcessStepRepository.GetParentProcessStep(entity) != null)
                {
                    return true;
                }
            }

            var manufacturer = entity as Manufacturer;
            if (manufacturer != null)
            {
                if (dataManager.ManufacturerRepository.GetParent(manufacturer.Guid) != null)
                {
                    return true;
                }
            }

            return false;
        }

        #endregion
    }
}