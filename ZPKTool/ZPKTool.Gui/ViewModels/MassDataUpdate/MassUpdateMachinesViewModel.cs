﻿using System;
using System.ComponentModel.Composition;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for Mass Update Machines View
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class MassUpdateMachinesViewModel : ViewModel<object, IDataSourceManager>
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The messenger reference
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The window service reference
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// Specifies if the updates are applied on the sub-objects of the current object
        /// </summary>
        private bool applyUpdatesToSubObjects;

        /// <summary>
        /// Counts the number of updated objects
        /// </summary>
        private int numberOfUpdatedObjects = 0;

        /// <summary>
        /// The manufacturing year current value
        /// </summary>
        private string manufacturingYearCurrentValue;

        /// <summary>
        /// The manufacturing year new value
        /// </summary>
        private string manufacturingYearNewValue;

        /// <summary>
        /// The depreciation period current value
        /// </summary>
        private string depreciationPeriodCurrentValue;

        /// <summary>
        /// The depreciation period new value
        /// </summary>
        private string depreciationPeriodNewValue;

        /// <summary>
        /// The depreciation rate current value
        /// </summary>
        private decimal? depreciationRateCurrentValue;

        /// <summary>
        /// The depreciation rate increase percentage
        /// </summary>
        private decimal? depreciationRateIncreasePercentage;

        /// <summary>
        /// The depreciation rate new value
        /// </summary>
        private decimal? depreciationRateNewValue;

        /// <summary>
        /// The k value current value
        /// </summary>
        private decimal? kValueCurrentValue;

        /// <summary>
        /// The k value increase percentage
        /// </summary>
        private decimal? kValueIncreasePercentage;

        /// <summary>
        /// The k value new value
        /// </summary>
        private decimal? kValueNewValue;

        /// <summary>
        /// The consumables cost current value
        /// </summary>
        private decimal? consumablesCostCurrentValue;

        /// <summary>
        /// The consumables cost increase percentage
        /// </summary>
        private decimal? consumablesCostIncreasePercentage;

        /// <summary>
        /// The consumables cost new value
        /// </summary>
        private decimal? consumablesCostNewValue;

        /// <summary>
        /// The project specific new value
        /// </summary>
        private bool? projectSpecificNewValue;

        /// <summary>
        /// The floor size new value.
        /// </summary>
        private decimal? floorSizeNewValue;

        /// <summary>
        /// The workspace are new value.
        /// </summary>
        private decimal? workspaceAreaNewValue;

        /// <summary>
        /// The power consumption new value.
        /// </summary>
        private decimal? powerConsumptionNewValue;

        /// <summary>
        /// The air consumption new value.
        /// </summary>
        private decimal? airConsumptionNewValue;

        /// <summary>
        /// The water consumption new value.
        /// </summary>
        private decimal? waterConsumptionNewValue;

        /// <summary>
        /// The max capacity new value.
        /// </summary>
        private int? maxCapacityNewValue;

        /// <summary>
        /// The availability new value.
        /// </summary>
        private decimal? availabilityNewValue;

        /// <summary>
        /// The floor size increase percentage.
        /// </summary>
        private decimal? floorSizeIncreasePercentage;

        /// <summary>
        /// The full load rate new value.
        /// </summary>
        private decimal? fullLoadRateNewValue;

        /// <summary>
        /// The oee new value.
        /// </summary>
        private decimal? oeeNewValue;

        /// <summary>
        /// The floor size current value.
        /// </summary>
        private decimal? floorSizeCurrentValue;

        /// <summary>
        /// THe workspace are increase percentage.
        /// </summary>
        private decimal? workspaceAreaIncreasePercentage;

        /// <summary>
        /// The workspace are current value.
        /// </summary>
        private decimal? workspaceAreaCurrentValue;

        /// <summary>
        /// The power consumption current value.
        /// </summary>
        private decimal? powerConsumptionCurrentValue;

        /// <summary>
        /// The power consumption increase percentage.
        /// </summary>
        private decimal? powerConsumptionIncreasePercentage;

        /// <summary>
        /// The air consumption current value.
        /// </summary>
        private decimal? airConsumptionCurrentValue;

        /// <summary>
        /// The air consumption increase percentage.
        /// </summary>
        private decimal? airConsumptionIncreasePercentage;

        /// <summary>
        /// The water consumption current value.
        /// </summary>
        private decimal? waterConsumptionCurrentValue;

        /// <summary>
        /// The water consumption increase percentage.
        /// </summary>
        private decimal? waterConsumptionIncreasePercentage;

        /// <summary>
        /// The full load rate current value.
        /// </summary>
        private decimal? fullLoadRateCurrentValue;

        /// <summary>
        /// The full load rate increase percentage.
        /// </summary>
        private decimal? fullLoadRateIncreasePercentage;

        /// <summary>
        /// The max capacity current value.
        /// </summary>
        private int? maxCapacityCurrentValue;

        /// <summary>
        /// The OEE current value.
        /// </summary>
        private decimal? oeeCurrentValue;

        /// <summary>
        /// The OEE increase percentage.
        /// </summary>
        private decimal? oeeIncreasePercentage;

        /// <summary>
        /// The availability current value.
        /// </summary>
        private decimal? availabilityCurrentValue;

        /// <summary>
        /// The availability increase percentage.
        /// </summary>
        private decimal? availabilityIncreasePercentage;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="MassUpdateMachinesViewModel"/> class.
        /// </summary>
        /// <param name="messenger">The messenger service.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="unitsService">The units service.</param>
        [ImportingConstructor]
        public MassUpdateMachinesViewModel(
            IMessenger messenger,
            IWindowService windowService,
            IUnitsService unitsService)
        {
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("unitsService", unitsService);

            this.messenger = messenger;
            this.windowService = windowService;
            this.unitsService = unitsService;

            this.UpdateCommand = new DelegateCommand(UpdateAction);
            this.SelectedDateChangedCommand = new DelegateCommand<SelectionChangedEventArgs>(OnDatePickerSelectionChanged);
        }

        #region Commands

        /// <summary>
        /// Gets the command for the Update button
        /// </summary>
        public ICommand UpdateCommand { get; private set; }

        /// <summary>
        /// Gets the command for the selection date changed in the date picker
        /// </summary>
        public ICommand SelectedDateChangedCommand { get; private set; }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether to apply updates to sub objects.
        /// </summary>
        public bool ApplyUpdatesToSubObjects
        {
            get
            {
                return this.applyUpdatesToSubObjects;
            }

            set
            {
                if (this.applyUpdatesToSubObjects != value)
                {
                    this.applyUpdatesToSubObjects = value;
                    OnPropertyChanged(() => this.ApplyUpdatesToSubObjects);
                }
            }
        }

        /// <summary>
        /// Gets or sets the manufacturing year current value.
        /// </summary>
        public string ManufacturingYearCurrentValue
        {
            get
            {
                return this.manufacturingYearCurrentValue;
            }

            set
            {
                if (this.manufacturingYearCurrentValue != value)
                {
                    this.manufacturingYearCurrentValue = value;
                    OnPropertyChanged(() => this.ManufacturingYearCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the manufacturing year new value.
        /// </summary>
        public string ManufacturingYearNewValue
        {
            get
            {
                return this.manufacturingYearNewValue;
            }

            set
            {
                if (this.manufacturingYearNewValue != value)
                {
                    this.manufacturingYearNewValue = value;
                    OnPropertyChanged(() => this.ManufacturingYearNewValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the depreciation period current value.
        /// </summary>
        public string DepreciationPeriodCurrentValue
        {
            get
            {
                return this.depreciationPeriodCurrentValue;
            }

            set
            {
                if (this.depreciationPeriodCurrentValue != value)
                {
                    this.depreciationPeriodCurrentValue = value;
                    OnPropertyChanged(() => this.DepreciationPeriodCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the depreciation period new value.
        /// </summary>
        public string DepreciationPeriodNewValue
        {
            get
            {
                return this.depreciationPeriodNewValue;
            }

            set
            {
                if (this.depreciationPeriodNewValue != value)
                {
                    this.depreciationPeriodNewValue = value;
                    OnPropertyChanged(() => this.DepreciationPeriodNewValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the depreciation rate current value.
        /// </summary>
        public decimal? DepreciationRateCurrentValue
        {
            get
            {
                return this.depreciationRateCurrentValue;
            }

            set
            {
                if (this.depreciationRateCurrentValue != value)
                {
                    this.depreciationRateCurrentValue = value;
                    OnPropertyChanged(() => this.DepreciationRateCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the depreciation rate increase percentage.
        /// </summary>
        public decimal? DepreciationRateIncreasePercentage
        {
            get
            {
                return this.depreciationRateIncreasePercentage;
            }

            set
            {
                if (this.depreciationRateIncreasePercentage != value)
                {
                    this.depreciationRateIncreasePercentage = value;
                    OnPropertyChanged(() => this.DepreciationRateIncreasePercentage);

                    if (value != null)
                    {
                        DepreciationRateNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the depreciation rate new value.
        /// </summary>
        public decimal? DepreciationRateNewValue
        {
            get
            {
                return this.depreciationRateNewValue;
            }

            set
            {
                if (this.depreciationRateNewValue != value)
                {
                    this.depreciationRateNewValue = value;
                    OnPropertyChanged(() => this.DepreciationRateNewValue);

                    if (value != null)
                    {
                        DepreciationRateIncreasePercentage = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the K value current value.
        /// </summary>
        public decimal? KValueCurrentValue
        {
            get
            {
                return this.kValueCurrentValue;
            }

            set
            {
                if (this.kValueCurrentValue != value)
                {
                    this.kValueCurrentValue = value;
                    OnPropertyChanged(() => this.KValueCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the K value increase percentage.
        /// </summary>
        public decimal? KValueIncreasePercentage
        {
            get
            {
                return this.kValueIncreasePercentage;
            }

            set
            {
                if (this.kValueIncreasePercentage != value)
                {
                    this.kValueIncreasePercentage = value;
                    OnPropertyChanged(() => this.KValueIncreasePercentage);

                    if (value != null)
                    {
                        KValueNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the K value new value.
        /// </summary>
        public decimal? KValueNewValue
        {
            get
            {
                return this.kValueNewValue;
            }

            set
            {
                if (this.kValueNewValue != value)
                {
                    this.kValueNewValue = value;
                    OnPropertyChanged(() => this.KValueNewValue);

                    if (value != null)
                    {
                        KValueIncreasePercentage = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the consumables cost current value.
        /// </summary>
        public decimal? ConsumablesCostCurrentValue
        {
            get
            {
                return this.consumablesCostCurrentValue;
            }

            set
            {
                if (this.consumablesCostCurrentValue != value)
                {
                    this.consumablesCostCurrentValue = value;
                    OnPropertyChanged(() => this.ConsumablesCostCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the consumables cost increase percentage.
        /// </summary>
        public decimal? ConsumablesCostIncreasePercentage
        {
            get
            {
                return this.consumablesCostIncreasePercentage;
            }

            set
            {
                if (this.consumablesCostIncreasePercentage != value)
                {
                    this.consumablesCostIncreasePercentage = value;
                    OnPropertyChanged(() => this.ConsumablesCostIncreasePercentage);

                    if (value != null)
                    {
                        ConsumablesCostNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the consumables cost new value.
        /// </summary>
        public decimal? ConsumablesCostNewValue
        {
            get
            {
                return this.consumablesCostNewValue;
            }

            set
            {
                if (this.consumablesCostNewValue != value)
                {
                    this.consumablesCostNewValue = value;
                    OnPropertyChanged(() => this.ConsumablesCostNewValue);

                    if (value != null)
                    {
                        ConsumablesCostIncreasePercentage = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the project specific new value.
        /// </summary>
        public bool? ProjectSpecificNewValue
        {
            get
            {
                return this.projectSpecificNewValue;
            }

            set
            {
                if (this.projectSpecificNewValue != value)
                {
                    this.projectSpecificNewValue = value;
                    OnPropertyChanged(() => this.ProjectSpecificNewValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the floor size current value.
        /// </summary>
        public decimal? FloorSizeCurrentValue
        {
            get
            {
                return this.floorSizeCurrentValue;
            }

            set
            {
                if (this.floorSizeCurrentValue != value)
                {
                    this.floorSizeCurrentValue = value;
                    OnPropertyChanged(() => this.FloorSizeCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the floor size increase percentage.
        /// </summary>
        public decimal? FloorSizeIncreasePercentage
        {
            get
            {
                return this.floorSizeIncreasePercentage;
            }

            set
            {
                if (this.floorSizeIncreasePercentage != value)
                {
                    this.floorSizeIncreasePercentage = value;
                    OnPropertyChanged(() => this.FloorSizeIncreasePercentage);

                    if (value != null)
                    {
                        this.FloorSizeNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the floor size new value.
        /// </summary>
        public decimal? FloorSizeNewValue
        {
            get
            {
                return this.floorSizeNewValue;
            }

            set
            {
                if (this.floorSizeNewValue != value)
                {
                    this.floorSizeNewValue = value;
                    OnPropertyChanged(() => this.FloorSizeNewValue);

                    if (value != null)
                    {
                        this.FloorSizeIncreasePercentage = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the workspace area current value.
        /// </summary>
        public decimal? WorkspaceAreaCurrentValue
        {
            get
            {
                return this.workspaceAreaCurrentValue;
            }

            set
            {
                if (this.workspaceAreaCurrentValue != value)
                {
                    this.workspaceAreaCurrentValue = value;
                    OnPropertyChanged(() => this.WorkspaceAreaCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the workspace area increase percentage.
        /// </summary>
        public decimal? WorkspaceAreaIncreasePercentage
        {
            get
            {
                return this.workspaceAreaIncreasePercentage;
            }

            set
            {
                if (this.workspaceAreaIncreasePercentage != value)
                {
                    this.workspaceAreaIncreasePercentage = value;
                    OnPropertyChanged(() => this.WorkspaceAreaIncreasePercentage);

                    if (value != null)
                    {
                        this.WorkspaceAreaNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the workspace area new value.
        /// </summary>
        public decimal? WorkspaceAreaNewValue
        {
            get
            {
                return this.workspaceAreaNewValue;
            }

            set
            {
                if (this.workspaceAreaNewValue != value)
                {
                    this.workspaceAreaNewValue = value;
                    OnPropertyChanged(() => this.WorkspaceAreaNewValue);

                    if (value != null)
                    {
                        this.WorkspaceAreaIncreasePercentage = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the power consumption current value.
        /// </summary>
        public decimal? PowerConsumptionCurrentValue
        {
            get
            {
                return this.powerConsumptionCurrentValue;
            }

            set
            {
                if (this.powerConsumptionCurrentValue != value)
                {
                    this.powerConsumptionCurrentValue = value;
                    OnPropertyChanged(() => this.PowerConsumptionCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the power consumption increase percentage.
        /// </summary>
        public decimal? PowerConsumptionIncreasePercentage
        {
            get
            {
                return this.powerConsumptionIncreasePercentage;
            }

            set
            {
                if (this.powerConsumptionIncreasePercentage != value)
                {
                    this.powerConsumptionIncreasePercentage = value;
                    OnPropertyChanged(() => this.PowerConsumptionIncreasePercentage);

                    if (value != null)
                    {
                        this.PowerConsumptionNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the power consumption new value.
        /// </summary>
        public decimal? PowerConsumptionNewValue
        {
            get
            {
                return this.powerConsumptionNewValue;
            }

            set
            {
                if (this.powerConsumptionNewValue != value)
                {
                    this.powerConsumptionNewValue = value;
                    OnPropertyChanged(() => this.PowerConsumptionNewValue);

                    if (value != null)
                    {
                        this.PowerConsumptionIncreasePercentage = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the air consumption current value.
        /// </summary>
        public decimal? AirConsumptionCurrentValue
        {
            get
            {
                return this.airConsumptionCurrentValue;
            }

            set
            {
                if (this.airConsumptionCurrentValue != value)
                {
                    this.airConsumptionCurrentValue = value;
                    OnPropertyChanged(() => this.AirConsumptionCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the air consumption increase percentage.
        /// </summary>
        public decimal? AirConsumptionIncreasePercentage
        {
            get
            {
                return this.airConsumptionIncreasePercentage;
            }

            set
            {
                if (this.airConsumptionIncreasePercentage != value)
                {
                    this.airConsumptionIncreasePercentage = value;
                    OnPropertyChanged(() => this.AirConsumptionIncreasePercentage);

                    if (value != null)
                    {
                        this.AirConsumptionNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the air consumption new value.
        /// </summary>
        public decimal? AirConsumptionNewValue
        {
            get
            {
                return this.airConsumptionNewValue;
            }

            set
            {
                if (this.airConsumptionNewValue != value)
                {
                    this.airConsumptionNewValue = value;
                    OnPropertyChanged(() => this.AirConsumptionNewValue);

                    if (value != null)
                    {
                        this.AirConsumptionIncreasePercentage = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the water consumption current value.
        /// </summary>
        public decimal? WaterConsumptionCurrentValue
        {
            get
            {
                return this.waterConsumptionCurrentValue;
            }

            set
            {
                if (this.waterConsumptionCurrentValue != value)
                {
                    this.waterConsumptionCurrentValue = value;
                    OnPropertyChanged(() => this.WaterConsumptionCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the water consumption increase percentage.
        /// </summary>
        public decimal? WaterConsumptionIncreasePercentage
        {
            get
            {
                return this.waterConsumptionIncreasePercentage;
            }

            set
            {
                if (this.waterConsumptionIncreasePercentage != value)
                {
                    this.waterConsumptionIncreasePercentage = value;
                    OnPropertyChanged(() => this.WaterConsumptionIncreasePercentage);

                    if (value != null)
                    {
                        this.WaterConsumptionNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the water consumption new value.
        /// </summary>
        public decimal? WaterConsumptionNewValue
        {
            get
            {
                return this.waterConsumptionNewValue;
            }

            set
            {
                if (this.waterConsumptionNewValue != value)
                {
                    this.waterConsumptionNewValue = value;
                    OnPropertyChanged(() => this.WaterConsumptionNewValue);

                    if (value != null)
                    {
                        this.WaterConsumptionIncreasePercentage = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the full load rate current value.
        /// </summary>
        public decimal? FullLoadRateCurrentValue
        {
            get
            {
                return this.fullLoadRateCurrentValue;
            }

            set
            {
                if (this.fullLoadRateCurrentValue != value)
                {
                    this.fullLoadRateCurrentValue = value;
                    OnPropertyChanged(() => this.FullLoadRateCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the full load rate increase percentage.
        /// </summary>
        public decimal? FullLoadRateIncreasePercentage
        {
            get
            {
                return this.fullLoadRateIncreasePercentage;
            }

            set
            {
                if (this.fullLoadRateIncreasePercentage != value)
                {
                    this.fullLoadRateIncreasePercentage = value;
                    OnPropertyChanged(() => this.FullLoadRateIncreasePercentage);

                    if (value != null)
                    {
                        this.FullLoadRateNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the full load rate new value.
        /// </summary>
        public decimal? FullLoadRateNewValue
        {
            get
            {
                return this.fullLoadRateNewValue;
            }

            set
            {
                if (this.fullLoadRateNewValue != value)
                {
                    this.fullLoadRateNewValue = value;
                    OnPropertyChanged(() => this.FullLoadRateNewValue);

                    if (value != null)
                    {
                        this.FullLoadRateIncreasePercentage = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the max capacity current value.
        /// </summary>
        public int? MaxCapacityCurrentValue
        {
            get
            {
                return this.maxCapacityCurrentValue;
            }

            set
            {
                if (this.maxCapacityCurrentValue != value)
                {
                    this.maxCapacityCurrentValue = value;
                    OnPropertyChanged(() => this.MaxCapacityCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the max capacity new value.
        /// </summary>
        public int? MaxCapacityNewValue
        {
            get
            {
                return this.maxCapacityNewValue;
            }

            set
            {
                if (this.maxCapacityNewValue != value)
                {
                    this.maxCapacityNewValue = value;
                    OnPropertyChanged(() => this.MaxCapacityNewValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the OEE current value.
        /// </summary>
        public decimal? OEECurrentValue
        {
            get
            {
                return this.oeeCurrentValue;
            }

            set
            {
                if (this.oeeCurrentValue != value)
                {
                    this.oeeCurrentValue = value;
                    OnPropertyChanged(() => this.OEECurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the OEE increase percentage.
        /// </summary>
        public decimal? OEEIncreasePercentage
        {
            get
            {
                return this.oeeIncreasePercentage;
            }

            set
            {
                if (this.oeeIncreasePercentage != value)
                {
                    this.oeeIncreasePercentage = value;
                    OnPropertyChanged(() => this.OEEIncreasePercentage);

                    if (value != null)
                    {
                        this.OEENewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the OEE new value.
        /// </summary>
        public decimal? OEENewValue
        {
            get
            {
                return this.oeeNewValue;
            }

            set
            {
                if (this.oeeNewValue != value)
                {
                    this.oeeNewValue = value;
                    OnPropertyChanged(() => this.OEENewValue);

                    if (value != null)
                    {
                        this.OEEIncreasePercentage = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the availability current value.
        /// </summary>
        public decimal? AvailabilityCurrentValue
        {
            get
            {
                return this.availabilityCurrentValue;
            }

            set
            {
                if (this.availabilityCurrentValue != value)
                {
                    this.availabilityCurrentValue = value;
                    OnPropertyChanged(() => this.AvailabilityCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the availability increase percentage.
        /// </summary>
        public decimal? AvailabilityIncreasePercentage
        {
            get
            {
                return this.availabilityIncreasePercentage;
            }

            set
            {
                if (this.availabilityIncreasePercentage != value)
                {
                    this.availabilityIncreasePercentage = value;
                    OnPropertyChanged(() => this.AvailabilityIncreasePercentage);

                    if (value != null)
                    {
                        this.AvailabilityNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the availability new value.
        /// </summary>
        public decimal? AvailabilityNewValue
        {
            get
            {
                return this.availabilityNewValue;
            }

            set
            {
                if (this.availabilityNewValue != value)
                {
                    this.availabilityNewValue = value;
                    OnPropertyChanged(() => this.AvailabilityNewValue);

                    if (value != null)
                    {
                        this.AvailabilityIncreasePercentage = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter { get; private set; }

        #endregion

        /// <summary>
        /// Handler for the date picker date changed event
        /// </summary>
        /// <param name="e">The selection changed event args</param>
        private void OnDatePickerSelectionChanged(SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count <= 0)
            {
                return;
            }

            DateTime selectedDateTime = Convert.ToDateTime(e.AddedItems[0]);
            this.ManufacturingYearNewValue = selectedDateTime.Year.ToString();
        }

        /// <summary>
        /// Set up the gui based on the input object
        /// </summary>
        protected override void OnModelChanged()
        {
            var machine = this.Model as Machine;
            if (machine != null)
            {
                if (machine.ManufacturingYear != null)
                {
                    ManufacturingYearCurrentValue = Convert.ToString(machine.ManufacturingYear);
                }

                if (machine.DepreciationPeriod != null)
                {
                    DepreciationPeriodCurrentValue = Convert.ToString(machine.DepreciationPeriod);
                }

                if (machine.DepreciationRate != null)
                {
                    DepreciationRateCurrentValue = machine.DepreciationRate;
                }

                if (machine.KValue != null)
                {
                    KValueCurrentValue = machine.KValue;
                }

                if (machine.ConsumablesCost != null)
                {
                    ConsumablesCostCurrentValue = machine.ManualConsumableCostPercentage;
                }

                if (machine.FloorSize != null)
                {
                    this.FloorSizeCurrentValue = machine.FloorSize;
                }

                if (machine.WorkspaceArea != null)
                {
                    this.WorkspaceAreaCurrentValue = machine.WorkspaceArea;
                }

                if (machine.PowerConsumption != null)
                {
                    this.PowerConsumptionCurrentValue = machine.PowerConsumption;
                }

                if (machine.AirConsumption != null)
                {
                    this.AirConsumptionCurrentValue = machine.AirConsumption;
                }

                if (machine.WaterConsumption != null)
                {
                    this.WaterConsumptionCurrentValue = machine.WaterConsumption;
                }

                if (machine.FullLoadRate != null)
                {
                    this.FullLoadRateCurrentValue = machine.FullLoadRate;
                }

                if (machine.MaxCapacity != null)
                {
                    this.MaxCapacityCurrentValue = machine.MaxCapacity;
                }

                if (machine.OEE != null)
                {
                    this.OEECurrentValue = machine.OEE;
                }

                if (machine.Availability != null)
                {
                    this.AvailabilityCurrentValue = machine.Availability;
                }
            }
        }

        /// <summary>
        /// Called when DataSourceManager has changed.
        /// </summary>
        protected override void OnDataSourceManagerChanged()
        {
            base.OnDataSourceManagerChanged();

            this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(this.DataSourceManager);
        }

        /// <summary>
        /// Performs the update operation
        /// </summary>
        private void UpdateAction()
        {
            // set the appropriate message
            string message = this.ApplyUpdatesToSubObjects ? LocalizedResources.MassDataUpdate_ConfirmationWithSubobjects : LocalizedResources.MassDataUpdate_ConfirmationNoSubobjects;
            MessageDialogResult result = this.windowService.MessageDialogService.Show(message, MessageDialogType.YesNo);
            if (result == MessageDialogResult.No)
            {
                return;
            }

            object updatedEntity = null;
            numberOfUpdatedObjects = 0;

            // apply for all sub-objects starting from the current object
            // search for all the machines in the project
            Project project = this.Model as Project;
            if (project != null && this.ApplyUpdatesToSubObjects)
            {
                // special case for project, must be retrieved on a new context
                Task.Factory.StartNew(() =>
                {
                    this.messenger.Send<NotificationMessage>(new NotificationMessage("UpdateMassData"));
                    Project fullProject = this.DataSourceManager.ProjectRepository.GetProjectFull(project.Guid);

                    if (fullProject != null)
                    {
                        this.UpdateProject(fullProject);
                    }

                    this.DataSourceManager.SaveChanges();
                })
                .ContinueWith(
                    (task) =>
                    {
                        this.messenger.Send<NotificationMessage>(new NotificationMessage("UpdateMassDataFinished"));
                        if (task.Exception != null)
                        {
                            Exception error = task.Exception.InnerException;
                            log.ErrorException("Project mass update failed.", error);
                            this.windowService.MessageDialogService.Show(error);
                        }
                        else
                        {
                            // refresh the context of the main view
                            EntityChangedMessage notificationMessage = new EntityChangedMessage(Notification.MyProjectsEntityChanged);
                            notificationMessage.ChangeType = EntityChangeType.RefreshData;
                            notificationMessage.Entity = project;
                            this.messenger.Send<EntityChangedMessage>(notificationMessage);

                            this.windowService.MessageDialogService.Show(LocalizedResources.MassDataUpdate_NumberOfUpdatedObjects + numberOfUpdatedObjects, MessageDialogType.Info);
                            this.messenger.Send<NotificationMessage>(new NotificationMessage("CloseMassDataUpdateWindow"));
                        }
                    },
                    System.Threading.Tasks.TaskScheduler.FromCurrentSynchronizationContext());

                return;
            }

            this.DataSourceManager.RevertChanges();

            // search for all the machines in the assembly
            Assembly assembly = this.Model as Assembly;
            if (assembly != null)
            {
                this.UpdateAssembly(assembly);
                updatedEntity = assembly;
            }
            else
            {
                // search for all the machines in the part
                Part part = this.Model as Part;
                if (part != null)
                {
                    this.UpdatePart(part);
                    updatedEntity = part;
                }
                else
                {
                    // search for all the machines in the process
                    Process process = this.Model as Process;
                    if (process != null)
                    {
                        // undo changes in the process                        
                        foreach (ProcessStep ps in process.Steps)
                        {
                            foreach (Machine mc in ps.Machines.Where(p => !p.IsDeleted))
                            {
                                this.UpdateMachine(mc);
                            }
                        }

                        updatedEntity = process;
                    }
                    else
                    {
                        // search for all the machines in the process step
                        ProcessStep step = this.Model as ProcessStep;
                        if (step != null)
                        {
                            // undo changes in the step                            
                            foreach (Machine mc in step.Machines.Where(p => !p.IsDeleted))
                            {
                                this.UpdateMachine(mc);
                            }

                            updatedEntity = step;
                        }
                        else
                        {
                            // apply only for the current object if it is a machine
                            Machine machine = this.Model as Machine;
                            if (machine != null)
                            {
                                this.UpdateMachine(machine);
                                updatedEntity = machine;
                            }
                        }
                    }
                }
            }

            if (this.DataSourceManager != null)
            {
                this.DataSourceManager.SaveChanges();
            }

            if (updatedEntity != null)
            {
                EntityChangedMessage msg = new EntityChangedMessage(Notification.MyProjectsEntityChanged);
                msg.Entity = updatedEntity;
                msg.Parent = null;
                msg.ChangeType = EntityChangeType.RefreshData;
                this.messenger.Send(msg);
            }

            this.windowService.MessageDialogService.Show(LocalizedResources.MassDataUpdate_NumberOfUpdatedObjects + numberOfUpdatedObjects, MessageDialogType.Info);
            this.messenger.Send<NotificationMessage>(new NotificationMessage("CloseMassDataUpdateWindow"));
        }

        /// <summary>
        /// Update the machines in a project
        /// </summary>
        /// <param name="project">The project.</param>
        private void UpdateProject(Project project)
        {
            foreach (Assembly assembly in project.Assemblies.Where(p => !p.IsDeleted))
            {
                UpdateAssembly(assembly);
            }

            foreach (Part part in project.Parts.Where(p => !p.IsDeleted))
            {
                UpdatePart(part);
            }
        }

        /// <summary>
        /// Update the machines in an assembly
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        private void UpdateAssembly(Assembly assembly)
        {
            // update machines in process steps
            foreach (ProcessStep step in assembly.Process.Steps)
            {
                foreach (Machine machine in step.Machines.Where(p => !p.IsDeleted))
                {
                    UpdateMachine(machine);
                }
            }

            // apply for sub-objects if selected
            if (this.ApplyUpdatesToSubObjects)
            {
                foreach (Assembly subAssembly in assembly.Subassemblies.Where(p => !p.IsDeleted))
                {
                    UpdateAssembly(subAssembly);
                }

                foreach (Part part in assembly.Parts.Where(p => !p.IsDeleted))
                {
                    UpdatePart(part);
                }
            }
        }

        /// <summary>
        /// Update the machines in a part
        /// </summary>
        /// <param name="part">The part.</param>
        private void UpdatePart(Part part)
        {
            // update machines in process steps
            foreach (ProcessStep step in part.Process.Steps)
            {
                foreach (Machine machine in step.Machines.Where(p => !p.IsDeleted))
                {
                    UpdateMachine(machine);
                }
            }

            // update sub-objects if option is selected
            if (ApplyUpdatesToSubObjects && part.RawPart != null && !part.RawPart.IsDeleted)
            {
                this.UpdatePart(part.RawPart);
            }
        }

        /// <summary>
        /// Updates the fields in a machine
        /// </summary>
        /// <param name="machine">The machine.</param>
        private void UpdateMachine(Machine machine)
        {
            bool objectUpdated = false;

            if (!string.IsNullOrEmpty(this.ManufacturingYearNewValue))
            {
                machine.ManufacturingYear = (short?)Converter.StringToNullableInt(this.ManufacturingYearNewValue);
                objectUpdated = true;
            }

            if (!string.IsNullOrEmpty(this.DepreciationPeriodNewValue))
            {
                machine.DepreciationPeriod = Converter.StringToNullableInt(this.DepreciationPeriodNewValue);
                objectUpdated = true;
            }

            if (this.DepreciationRateNewValue != null)
            {
                machine.DepreciationRate = this.DepreciationRateNewValue;
                objectUpdated = true;
            }

            if (this.DepreciationRateIncreasePercentage != null)
            {
                machine.DepreciationRate = machine.DepreciationRate * this.DepreciationRateIncreasePercentage;
                objectUpdated = true;
            }

            if (machine.CalculateWithKValue == true)
            {
                if (this.kValueNewValue != null)
                {
                    machine.KValue = this.kValueNewValue;
                    objectUpdated = true;
                }

                if (this.kValueIncreasePercentage != null)
                {
                    machine.KValue = machine.KValue * this.kValueIncreasePercentage;
                    objectUpdated = true;
                }
            }

            if (machine.ManualConsumableCostCalc == true)
            {
                if (this.ConsumablesCostNewValue != null)
                {
                    machine.ManualConsumableCostPercentage = this.ConsumablesCostNewValue;
                    objectUpdated = true;
                }

                if (this.ConsumablesCostIncreasePercentage != null)
                {
                    machine.ManualConsumableCostPercentage = machine.ManualConsumableCostPercentage * this.ConsumablesCostIncreasePercentage;
                    objectUpdated = true;
                }
            }

            if (this.ProjectSpecificNewValue != null)
            {
                if (machine.IsProjectSpecific != this.ProjectSpecificNewValue.Value)
                {
                    machine.IsProjectSpecific = this.ProjectSpecificNewValue.Value;
                    objectUpdated = true;
                }
            }

            if (this.FloorSizeNewValue != null && machine.FloorSize != this.FloorSizeNewValue)
            {
                machine.FloorSize = this.FloorSizeNewValue;
                objectUpdated = true;
            }

            if (this.FloorSizeIncreasePercentage != null)
            {
                machine.FloorSize = machine.FloorSize * this.FloorSizeIncreasePercentage;
                objectUpdated = true;
            }

            if (this.WorkspaceAreaNewValue != null && machine.WorkspaceArea != this.WorkspaceAreaNewValue)
            {
                machine.WorkspaceArea = this.WorkspaceAreaNewValue;
                objectUpdated = true;
            }

            if (this.WorkspaceAreaIncreasePercentage != null)
            {
                machine.WorkspaceArea = machine.WorkspaceArea * this.WorkspaceAreaIncreasePercentage;
                objectUpdated = true;
            }

            if (this.PowerConsumptionNewValue != null && machine.PowerConsumption != this.PowerConsumptionNewValue)
            {
                machine.PowerConsumption = this.PowerConsumptionNewValue;
                objectUpdated = true;
            }

            if (this.PowerConsumptionIncreasePercentage != null)
            {
                machine.PowerConsumption = machine.PowerConsumption * this.PowerConsumptionIncreasePercentage;
                objectUpdated = true;
            }

            if (this.AirConsumptionNewValue != null && machine.AirConsumption != this.AirConsumptionNewValue)
            {
                machine.AirConsumption = this.AirConsumptionNewValue;
                objectUpdated = true;
            }

            if (this.AirConsumptionIncreasePercentage != null)
            {
                machine.AirConsumption = machine.AirConsumption * this.AirConsumptionIncreasePercentage;
                objectUpdated = true;
            }

            if (this.WaterConsumptionNewValue != null && machine.WaterConsumption != this.WaterConsumptionNewValue)
            {
                machine.WaterConsumption = this.WaterConsumptionNewValue;
                objectUpdated = true;
            }

            if (this.WaterConsumptionIncreasePercentage != null)
            {
                machine.WaterConsumption = machine.WaterConsumption * this.WaterConsumptionIncreasePercentage;
                objectUpdated = true;
            }

            if (this.FullLoadRateNewValue != null && machine.FullLoadRate != this.FullLoadRateNewValue)
            {
                machine.FullLoadRate = this.FullLoadRateNewValue;
                objectUpdated = true;
            }

            if (this.FullLoadRateIncreasePercentage != null)
            {
                machine.FullLoadRate = machine.FullLoadRate * this.FullLoadRateIncreasePercentage;
                objectUpdated = true;
            }

            if (this.MaxCapacityNewValue != null && machine.MaxCapacity != this.MaxCapacityNewValue)
            {
                machine.MaxCapacity = this.MaxCapacityNewValue;
                objectUpdated = true;
            }

            if (this.OEENewValue != null && machine.OEE != this.OEENewValue)
            {
                machine.OEE = this.OEENewValue;
                objectUpdated = true;
            }

            if (this.OEEIncreasePercentage != null)
            {
                machine.OEE = machine.OEE * this.OEEIncreasePercentage;
                objectUpdated = true;
            }

            if (this.AvailabilityNewValue != null && machine.Availability != this.AvailabilityNewValue)
            {
                machine.Availability = this.AvailabilityNewValue;
                objectUpdated = true;
            }

            if (this.AvailabilityIncreasePercentage != null)
            {
                machine.Availability = machine.Availability * this.AvailabilityIncreasePercentage;
                objectUpdated = true;
            }

            if (objectUpdated)
            {
                numberOfUpdatedObjects++;
            }
        }
    }
}
