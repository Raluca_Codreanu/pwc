﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Media;
using ZPKTool.Common;
using ZPKTool.Data;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Represents a raw material in the materials list on the Mass Update - Raw Materials view.
    /// </summary>
    public class RawMaterialDataItem : ObservableObject
    {
        /// <summary>
        /// Backing field for the count property
        /// </summary>
        private int count;

        /// <summary>
        /// Initializes a new instance of the <see cref="RawMaterialDataItem"/> class.
        /// </summary>
        public RawMaterialDataItem()
        {
            this.RawMaterials = new Collection<RawMaterial>();
        }

        /// <summary>
        /// Gets or sets the common name of the raw materials
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets the raw material entities which have the same name.
        /// </summary>
        public Collection<RawMaterial> RawMaterials { get; private set; }

        /// <summary>
        /// Gets or sets the icon of the item
        /// </summary>
        public ImageSource Icon { get; set; }

        /// <summary>
        /// Gets or sets the number of times the material appears
        /// </summary>
        public int Count
        {
            get
            {
                return this.count;
            }

            set
            {
                this.count = value;
                OnPropertyChanged(() => Count);
            }
        }
    }
}
