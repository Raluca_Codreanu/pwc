﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for Mass Update Quantity
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class MassUpdateQuantityViewModel : ViewModel<object, IDataSourceManager>
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The messenger reference
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The window service reference
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// Counts the number of updated entities
        /// </summary>
        private int numberOfUpdatedObjects = 0;

        /// <summary>
        /// Specifies if the quantity fields are enabled
        /// </summary>
        private bool quantityFieldsEnabled;

        /// <summary>
        /// Specifies if the updates are applied on the sub-objects of the current object
        /// </summary>
        private bool applyUpdatesToSubObjects;

        /// <summary>
        /// The batch size current value
        /// </summary>
        private string batchSizeCurrentValue;

        /// <summary>
        /// The batch size new value
        /// </summary>
        private string batchSizeNewValue;

        /// <summary>
        /// The yearly prod QTY current value
        /// </summary>
        private string yearlyProdQTYCurrentValue;

        /// <summary>
        /// The yearly prod QTY increase percentage
        /// </summary>
        private double? yearlyProdQTYIncreasePercentage;

        /// <summary>
        /// The yearly prod QTY new value
        /// </summary>
        private string yearlyProdQTYNewValue;

        /// <summary>
        /// The life time current value
        /// </summary>
        private string lifeTimeCurrentValue;

        /// <summary>
        /// The life time new value
        /// </summary>
        private string lifeTimeNewValue;

        /// <summary>
        /// The calculated batch size current value
        /// </summary>
        private string calculatedBatchSizeCurrentValue;

        /// <summary>
        /// The calculated batch size increase percentage
        /// </summary>
        private double? calculatedBatchSizeIncreasePercentage;

        /// <summary>
        /// The calculated batch size new value
        /// </summary>
        private string calculatedBatchSizeNewValue;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="MassUpdateQuantityViewModel"/> class.
        /// </summary>
        /// <param name="messenger">The messenger service.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="unitsService">The units service.</param>
        [ImportingConstructor]
        public MassUpdateQuantityViewModel(
            IMessenger messenger,
            IWindowService windowService,
            IUnitsService unitsService)
        {
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("unitsService", unitsService);

            this.messenger = messenger;
            this.windowService = windowService;
            this.unitsService = unitsService;

            this.UpdateCommand = new DelegateCommand(UpdateQuantity);
        }

        #region Commands

        /// <summary>
        /// Gets the command for the Update button
        /// </summary>
        public ICommand UpdateCommand { get; private set; }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether to apply updates to sub objects.
        /// </summary>
        public bool ApplyUpdatesToSubObjects
        {
            get
            {
                return this.applyUpdatesToSubObjects;
            }

            set
            {
                if (this.applyUpdatesToSubObjects != value)
                {
                    this.applyUpdatesToSubObjects = value;
                    OnPropertyChanged(() => this.ApplyUpdatesToSubObjects);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the quantity fields are enabled.
        /// </summary>
        public bool QuantityFieldsEnabled
        {
            get
            {
                return this.quantityFieldsEnabled;
            }

            set
            {
                if (this.quantityFieldsEnabled != value)
                {
                    this.quantityFieldsEnabled = value;
                    OnPropertyChanged(() => this.QuantityFieldsEnabled);
                }
            }
        }

        /// <summary>
        /// Gets or sets the batch size current value.
        /// </summary>
        public string BatchSizeCurrentValue
        {
            get
            {
                return this.batchSizeCurrentValue;
            }

            set
            {
                if (this.batchSizeCurrentValue != value)
                {
                    this.batchSizeCurrentValue = value;
                    OnPropertyChanged(() => this.BatchSizeCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the batch size new value.
        /// </summary>
        public string BatchSizeNewValue
        {
            get
            {
                return this.batchSizeNewValue;
            }

            set
            {
                if (this.batchSizeNewValue != value)
                {
                    this.batchSizeNewValue = value;
                    OnPropertyChanged(() => this.BatchSizeNewValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the yearly prod QTY current value.
        /// </summary>
        public string YearlyProdQTYCurrentValue
        {
            get
            {
                return this.yearlyProdQTYCurrentValue;
            }

            set
            {
                if (this.yearlyProdQTYCurrentValue != value)
                {
                    this.yearlyProdQTYCurrentValue = value;
                    OnPropertyChanged(() => this.YearlyProdQTYCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the yearly prod QTY increase percentage.
        /// </summary>
        public double? YearlyProdQTYIncreasePercentage
        {
            get
            {
                return this.yearlyProdQTYIncreasePercentage;
            }

            set
            {
                if (this.yearlyProdQTYIncreasePercentage != value)
                {
                    this.yearlyProdQTYIncreasePercentage = value;
                    OnPropertyChanged(() => this.YearlyProdQTYIncreasePercentage);

                    if (value != null)
                    {
                        YearlyProdQTYNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the yearly prod QTY new value.
        /// </summary>
        public string YearlyProdQTYNewValue
        {
            get
            {
                return this.yearlyProdQTYNewValue;
            }

            set
            {
                if (this.yearlyProdQTYNewValue != value)
                {
                    this.yearlyProdQTYNewValue = value;
                    OnPropertyChanged(() => this.YearlyProdQTYNewValue);

                    if (value != null)
                    {
                        YearlyProdQTYIncreasePercentage = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the life time current value.
        /// </summary>
        public string LifeTimeCurrentValue
        {
            get
            {
                return this.lifeTimeCurrentValue;
            }

            set
            {
                if (this.lifeTimeCurrentValue != value)
                {
                    this.lifeTimeCurrentValue = value;
                    OnPropertyChanged(() => this.LifeTimeCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the life time new value.
        /// </summary>
        public string LifeTimeNewValue
        {
            get
            {
                return this.lifeTimeNewValue;
            }

            set
            {
                if (this.lifeTimeNewValue != value)
                {
                    this.lifeTimeNewValue = value;
                    OnPropertyChanged(() => this.LifeTimeNewValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the calculated batch size current value.
        /// </summary>
        public string CalculatedBatchSizeCurrentValue
        {
            get
            {
                return this.calculatedBatchSizeCurrentValue;
            }

            set
            {
                if (this.calculatedBatchSizeCurrentValue != value)
                {
                    this.calculatedBatchSizeCurrentValue = value;
                    OnPropertyChanged(() => this.CalculatedBatchSizeCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the calculated batch size increase percentage.
        /// </summary>
        public double? CalculatedBatchSizeIncreasePercentage
        {
            get
            {
                return this.calculatedBatchSizeIncreasePercentage;
            }

            set
            {
                if (this.calculatedBatchSizeIncreasePercentage != value)
                {
                    this.calculatedBatchSizeIncreasePercentage = value;
                    OnPropertyChanged(() => this.CalculatedBatchSizeIncreasePercentage);

                    if (value != null)
                    {
                        CalculatedBatchSizeNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the calculated batch size new value.
        /// </summary>
        public string CalculatedBatchSizeNewValue
        {
            get
            {
                return this.calculatedBatchSizeNewValue;
            }

            set
            {
                if (this.calculatedBatchSizeNewValue != value)
                {
                    this.calculatedBatchSizeNewValue = value;
                    OnPropertyChanged(() => this.CalculatedBatchSizeNewValue);

                    if (value != null)
                    {
                        CalculatedBatchSizeIncreasePercentage = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter { get; private set; }

        #endregion

        /// <summary>
        /// Set up the gui based on the input object
        /// </summary>
        protected override void OnModelChanged()
        {
            if (this.Model is Process || this.Model is ProcessStep)
            {
                QuantityFieldsEnabled = false;
                ProcessStep step = this.Model as ProcessStep;
                if (step != null)
                {
                    this.CalculatedBatchSizeCurrentValue = Convert.ToString(step.BatchSize);
                }
            }
            else
            {
                QuantityFieldsEnabled = true;
                Project proj = this.Model as Project;
                if (proj != null)
                {
                    if (proj.YearlyProductionQuantity.HasValue)
                    {
                        this.YearlyProdQTYCurrentValue = Convert.ToString(proj.YearlyProductionQuantity);
                    }

                    if (proj.LifeTime.HasValue)
                    {
                        this.LifeTimeCurrentValue = Convert.ToString(proj.LifeTime);
                    }
                }

                Assembly assy = this.Model as Assembly;
                if (assy != null)
                {
                    if (assy.BatchSizePerYear.HasValue)
                    {
                        this.BatchSizeCurrentValue = Convert.ToString(assy.BatchSizePerYear);
                    }

                    if (assy.YearlyProductionQuantity.HasValue)
                    {
                        this.YearlyProdQTYCurrentValue = Convert.ToString(assy.YearlyProductionQuantity);
                    }

                    if (assy.LifeTime.HasValue)
                    {
                        this.LifeTimeCurrentValue = Convert.ToString(assy.LifeTime);
                    }
                }

                Part part = this.Model as Part;
                if (part != null)
                {
                    if (part.BatchSizePerYear.HasValue)
                    {
                        this.BatchSizeCurrentValue = Convert.ToString(part.BatchSizePerYear);
                    }

                    if (part.YearlyProductionQuantity.HasValue)
                    {
                        this.YearlyProdQTYCurrentValue = Convert.ToString(part.YearlyProductionQuantity);
                    }

                    if (part.LifeTime.HasValue)
                    {
                        this.LifeTimeCurrentValue = Convert.ToString(part.LifeTime);
                    }
                }
            }
        }

        /// <summary>
        /// Called when DataSourceManager has changed.
        /// </summary>
        protected override void OnDataSourceManagerChanged()
        {
            base.OnDataSourceManagerChanged();

            this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(this.DataSourceManager);
        }

        /// <summary>
        /// Performs the update operation
        /// </summary>
        private void UpdateQuantity()
        {
            // set the appropriate message
            string message = this.ApplyUpdatesToSubObjects ? LocalizedResources.MassDataUpdate_ConfirmationWithSubobjects : LocalizedResources.MassDataUpdate_ConfirmationNoSubobjects;
            MessageDialogResult result = this.windowService.MessageDialogService.Show(message, MessageDialogType.YesNo);
            if (result == MessageDialogResult.No)
            {
                return;
            }

            object updatedEntity = null;
            numberOfUpdatedObjects = 0;

            Project project = this.Model as Project;
            if (project != null && this.ApplyUpdatesToSubObjects)
            {
                // special case for project, must be retrieved on a new context
                Task.Factory.StartNew(() =>
                {
                    this.messenger.Send<NotificationMessage>(new NotificationMessage("UpdateMassData"));
                    Project fullProject = this.DataSourceManager.ProjectRepository.GetProjectFull(project.Guid);

                    if (fullProject != null)
                    {
                        this.UpdateProject(fullProject);
                    }

                    this.DataSourceManager.SaveChanges();
                })
                .ContinueWith(
                    (task) =>
                    {
                        this.messenger.Send<NotificationMessage>(new NotificationMessage("UpdateMassDataFinished"));
                        if (task.Exception != null)
                        {
                            Exception error = task.Exception.InnerException;
                            log.ErrorException("Project loading failed.", error);
                            this.windowService.MessageDialogService.Show(error);
                        }
                        else
                        {
                            // refresh the context of the main view
                            EntityChangedMessage notificationMessage = new EntityChangedMessage(Notification.MyProjectsEntityChanged);
                            notificationMessage.ChangeType = EntityChangeType.RefreshData;
                            notificationMessage.Entity = project;
                            this.messenger.Send<EntityChangedMessage>(notificationMessage);

                            this.windowService.MessageDialogService.Show(LocalizedResources.MassDataUpdate_NumberOfUpdatedObjects + numberOfUpdatedObjects, MessageDialogType.Info);
                            this.messenger.Send<NotificationMessage>(new NotificationMessage("CloseMassDataUpdateWindow"));
                        }
                    },
                    System.Threading.Tasks.TaskScheduler.FromCurrentSynchronizationContext());

                return;
            }

            this.DataSourceManager.RevertChanges();

            if (project != null && !this.ApplyUpdatesToSubObjects)
            {
                Project proj = this.Model as Project;
                if (proj != null)
                {
                    this.UpdateProject(proj);
                    updatedEntity = proj;
                }
            }
            else
            {
                Assembly assembly = this.Model as Assembly;
                if (assembly != null)
                {
                    this.UpdateAssembly(assembly);
                    updatedEntity = assembly;
                }
                else
                {
                    Part part = this.Model as Part;
                    if (part != null)
                    {
                        this.UpdatePart(part);
                        updatedEntity = part;
                    }
                    else
                    {
                        Process process = this.Model as Process;
                        if (process != null)
                        {
                            foreach (ProcessStep ps in process.Steps)
                            {
                                this.UpdateProcessStep(ps);
                            }

                            updatedEntity = process;
                        }
                        else
                        {
                            ProcessStep step = this.Model as ProcessStep;
                            if (step != null)
                            {
                                this.UpdateProcessStep(step);
                                updatedEntity = step;
                            }
                        }
                    }
                }
            }

            if (this.DataSourceManager != null)
            {
                this.DataSourceManager.SaveChanges();
            }

            if (updatedEntity != null)
            {
                EntityChangedMessage msg = new EntityChangedMessage(Notification.MyProjectsEntityChanged);
                msg.Entity = updatedEntity;
                msg.Parent = null;
                msg.ChangeType = EntityChangeType.RefreshData;
                this.messenger.Send(msg);
            }

            this.windowService.MessageDialogService.Show(LocalizedResources.MassDataUpdate_NumberOfUpdatedObjects + numberOfUpdatedObjects, MessageDialogType.Info);
            this.messenger.Send<NotificationMessage>(new NotificationMessage("CloseMassDataUpdateWindow"));
        }

        /// <summary>
        /// Update the fields in a project
        /// </summary>
        /// <param name="project">The project.</param>
        private void UpdateProject(Project project)
        {
            bool objectUpdated = false;
            if (!string.IsNullOrEmpty(this.yearlyProdQTYNewValue))
            {
                project.YearlyProductionQuantity = Converter.StringToNullableInt(this.yearlyProdQTYNewValue);
                objectUpdated = true;
            }

            if (this.yearlyProdQTYIncreasePercentage != null)
            {
                project.YearlyProductionQuantity = Convert.ToInt32(project.YearlyProductionQuantity * this.yearlyProdQTYIncreasePercentage);
                objectUpdated = true;
            }

            if (!string.IsNullOrEmpty(this.lifeTimeNewValue))
            {
                project.LifeTime = Converter.StringToNullableInt(this.lifeTimeNewValue);
                objectUpdated = true;
            }

            if (objectUpdated)
            {
                numberOfUpdatedObjects++;
            }

            if (this.ApplyUpdatesToSubObjects)
            {
                foreach (Assembly assembly in project.Assemblies.Where(p => !p.IsDeleted))
                {
                    UpdateAssembly(assembly);
                }

                foreach (Part part in project.Parts.Where(p => !p.IsDeleted))
                {
                    UpdatePart(part);
                }
            }
        }

        /// <summary>
        /// Update the fields in an assembly
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        private void UpdateAssembly(Assembly assembly)
        {
            bool objectUpdated = false;
            if (!string.IsNullOrEmpty(this.batchSizeNewValue))
            {
                assembly.BatchSizePerYear = Converter.StringToNullableInt(this.batchSizeNewValue);
                objectUpdated = true;
            }

            if (!string.IsNullOrEmpty(this.yearlyProdQTYNewValue))
            {
                assembly.YearlyProductionQuantity = Converter.StringToNullableInt(this.yearlyProdQTYNewValue);
                objectUpdated = true;
            }

            if (this.yearlyProdQTYIncreasePercentage != null)
            {
                assembly.YearlyProductionQuantity = Convert.ToInt32(assembly.YearlyProductionQuantity * this.yearlyProdQTYIncreasePercentage);
                objectUpdated = true;
            }

            if (!string.IsNullOrEmpty(this.lifeTimeNewValue))
            {
                assembly.LifeTime = Converter.StringToNullableInt(this.lifeTimeNewValue);
                objectUpdated = true;
            }

            if (objectUpdated)
            {
                numberOfUpdatedObjects++;
            }

            // update process steps
            foreach (ProcessStep step in assembly.Process.Steps)
            {
                UpdateProcessStep(step);
            }

            // update sub-objects if option is selected
            if (this.ApplyUpdatesToSubObjects)
            {
                foreach (Assembly subAssembly in assembly.Subassemblies.Where(p => !p.IsDeleted))
                {
                    UpdateAssembly(subAssembly);
                }

                foreach (Part part in assembly.Parts.Where(p => !p.IsDeleted))
                {
                    UpdatePart(part);
                }
            }
        }

        /// <summary>
        /// Update the fields in a part
        /// </summary>
        /// <param name="part">The part.</param>
        private void UpdatePart(Part part)
        {
            bool objectUpdated = false;
            if (!string.IsNullOrEmpty(this.batchSizeNewValue))
            {
                part.BatchSizePerYear = Converter.StringToNullableInt(this.batchSizeNewValue);
                objectUpdated = true;
            }

            if (!string.IsNullOrEmpty(this.yearlyProdQTYNewValue))
            {
                part.YearlyProductionQuantity = Converter.StringToNullableInt(this.yearlyProdQTYNewValue);
                objectUpdated = true;
            }

            if (this.yearlyProdQTYIncreasePercentage != null)
            {
                part.YearlyProductionQuantity = Convert.ToInt32(part.YearlyProductionQuantity * this.yearlyProdQTYIncreasePercentage);
                objectUpdated = true;
            }

            if (!string.IsNullOrEmpty(this.lifeTimeNewValue))
            {
                part.LifeTime = Converter.StringToNullableInt(this.lifeTimeNewValue);
                objectUpdated = true;
            }

            if (objectUpdated)
            {
                numberOfUpdatedObjects++;
            }

            // update process steps
            foreach (ProcessStep step in part.Process.Steps)
            {
                UpdateProcessStep(step);
            }

            if (ApplyUpdatesToSubObjects && part.RawPart != null && !part.RawPart.IsDeleted)
            {
                this.UpdatePart(part.RawPart);
            }
        }

        /// <summary>
        /// Update the fields in a Process Step
        /// </summary>
        /// <param name="step">The step.</param>
        private void UpdateProcessStep(ProcessStep step)
        {
            bool objectUpdated = false;
            if (!string.IsNullOrEmpty(this.calculatedBatchSizeNewValue))
            {
                step.BatchSize = Converter.StringToNullableInt(this.calculatedBatchSizeNewValue);
                objectUpdated = true;
            }

            if (this.calculatedBatchSizeIncreasePercentage != null)
            {
                step.BatchSize = Convert.ToInt32(step.BatchSize * this.calculatedBatchSizeIncreasePercentage);
                objectUpdated = true;
            }

            if (objectUpdated)
            {
                numberOfUpdatedObjects++;
            }
        }
    }
}
