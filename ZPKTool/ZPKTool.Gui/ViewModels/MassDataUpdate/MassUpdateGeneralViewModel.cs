﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using ZPKTool.Business;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// View model for the mass data update general tab
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class MassUpdateGeneralViewModel : ViewModel<object, IDataSourceManager>
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The messenger reference
        /// </summary>
        private readonly IMessenger messenger;

        /// <summary>
        /// The window service reference
        /// </summary>
        private readonly IWindowService windowService;

        /// <summary>
        /// Counts the number of updated objects
        /// </summary>
        private int numberOfUpdatedObjects;

        /// <summary>
        /// The current value of the version number
        /// </summary>
        private string versionNumberCurrentValue;

        /// <summary>
        /// The new value of the version number
        /// </summary>
        private decimal? versionNumberNewValue;

        /// <summary>
        /// Value indicating whether this instance is tooling active current value.
        /// </summary>
        private bool isToolingActiveCurrentValue;

        /// <summary>
        /// Value indicating whether this instance is tooling active new value.
        /// </summary>
        private bool isToolingActiveNewValue;

        /// <summary>
        /// The project currencies.
        /// </summary>
        private ObservableCollection<Currency> projectCurrencies;

        /// <summary>
        /// Gets the set of available calculation variants.
        /// </summary>
        private Collection<string> calculationVariants;

        /// <summary>
        /// The current value of the base currency.
        /// </summary>
        private string baseCurrencyCurrentValue;

        /// <summary>
        /// The current version of the calculation variant.
        /// </summary>
        private string calculationVariantCurrentValue;

        /// <summary>
        /// A value indicating whether this instance is loading the project currencies.
        /// </summary>
        private bool isLoadingCurrencies;

        /// <summary>
        /// A value indicating whether this instance is a project.
        /// </summary>
        private bool isProject;

        /// <summary>
        /// The data manager.
        /// </summary>
        private IDataSourceManager dataManager;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="MassUpdateGeneralViewModel"/> class.
        /// </summary>
        /// <param name="messenger">The messenger service.</param>
        /// <param name="windowService">The window service.</param>
        [ImportingConstructor]
        public MassUpdateGeneralViewModel(IMessenger messenger, IWindowService windowService)
        {
            this.messenger = messenger;
            this.windowService = windowService;
            this.UpdateCommand = new DelegateCommand(UpdateAction);
            this.ProjectCurrencies = new ObservableCollection<Currency>();
            this.CalculationVariants = new Collection<string>(CostCalculatorFactory.CalculationVersions);
            this.BaseCurrencyNewValue.ValueChanged += BaseCurrencyNewValue_ValueChanged;
            this.ApplyUpdatesToSubObjects.PropertyChanged += ApplyUpdatesToSubObjects_PropertyChanged;
        }

        #region Commands

        /// <summary>
        /// Gets the command for the Update button
        /// </summary>
        public ICommand UpdateCommand { get; private set; }

        #endregion

        #region Properties

        /// <summary>
        /// Gets indicating whether the update is applied to the sub-objects or not.
        /// </summary>
        public VMProperty<bool> ApplyUpdatesToSubObjects { get; private set; }

        /// <summary>
        /// Gets or sets the current value of the version.
        /// </summary>
        public string VersionNumberCurrentValue
        {
            get
            {
                return this.versionNumberCurrentValue;
            }

            set
            {
                if (this.versionNumberCurrentValue != value)
                {
                    this.versionNumberCurrentValue = value;
                    this.OnPropertyChanged(() => this.VersionNumberCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the current value of the calculation variant version..
        /// </summary>
        public string CalculationVariantCurrentValue
        {
            get
            {
                return this.calculationVariantCurrentValue;
            }

            set
            {
                if (this.calculationVariantCurrentValue != value)
                {
                    this.calculationVariantCurrentValue = value;
                    this.OnPropertyChanged(() => this.CalculationVariantCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets current value of the base currency.
        /// </summary>
        public string BaseCurrencyCurrentValue
        {
            get
            {
                return this.baseCurrencyCurrentValue;
            }

            set
            {
                if (this.baseCurrencyCurrentValue != value)
                {
                    this.baseCurrencyCurrentValue = value;
                    this.OnPropertyChanged(() => this.BaseCurrencyCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the values of the calculation variants.
        /// </summary>
        public Collection<string> CalculationVariants
        {
            get
            {
                return this.calculationVariants;
            }

            set
            {
                if (this.calculationVariants != value)
                {
                    this.calculationVariants = value;
                    this.OnPropertyChanged(() => this.CalculationVariants);
                }
            }
        }

        /// <summary>
        /// Gets or sets the new value of the version
        /// </summary>
        public decimal? VersionNumberNewValue
        {
            get
            {
                return this.versionNumberNewValue;
            }

            set
            {
                if (this.versionNumberNewValue != value)
                {
                    this.versionNumberNewValue = value;
                    this.OnPropertyChanged(() => this.VersionNumberNewValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is project.
        /// </summary>
        public bool IsProject
        {
            get
            {
                return this.isProject;
            }

            set
            {
                if (this.isProject != value)
                {
                    this.isProject = value;
                    this.OnPropertyChanged(() => this.IsProject);
                }
            }
        }

        /// <summary>
        /// Gets or sets the currencies.
        /// </summary>
        [UndoableProperty]
        public ObservableCollection<Currency> ProjectCurrencies
        {
            get
            {
                return this.projectCurrencies;
            }

            set
            {
                if (this.projectCurrencies != value)
                {
                    this.projectCurrencies = value;
                    OnPropertyChanged(() => this.ProjectCurrencies);
                }
            }
        }

        /// <summary>
        /// Gets the project's Base Currency.
        /// </summary>
        [UndoableProperty]
        [ExposesModelProperty("BaseCurrencyNewValue")]
        public DataProperty<Currency> BaseCurrencyNewValue { get; private set; }

        /// <summary>
        /// Gets the calculation variant of the mass data.
        /// </summary>
        [UndoableProperty]
        [ExposesModelProperty("CalculationVariant")]
        public DataProperty<string> CalculationVariantNewValue { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is tooling active current value.
        /// </summary>
        /// <value><c>true</c> if this instance is tooling active current value; otherwise, <c>false</c>.</value>
        public bool IsToolingActiveCurrentValue
        {
            get { return this.isToolingActiveCurrentValue; }
            set { this.SetProperty(ref this.isToolingActiveCurrentValue, value, () => this.IsToolingActiveCurrentValue); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is tooling active new value.
        /// </summary>
        /// <value><c>true</c> if this instance is tooling active new value; otherwise, <c>false</c>.</value>
        public bool IsToolingActiveNewValue
        {
            get { return this.isToolingActiveNewValue; }
            set { this.SetProperty(ref this.isToolingActiveNewValue, value, () => this.IsToolingActiveNewValue); }
        }

        #endregion

        /// <summary>
        /// Called when the Model value has changed.
        /// Set up the view-model based on the input model object.
        /// </summary>
        protected override void OnModelChanged()
        {
            IsToolingActiveCurrentValue = false;

            Project project = this.Model as Project;
            if (project != null)
            {
                this.IsProject = true;
                this.LoadCurrencies(project);
                if (project.Version.HasValue)
                {
                    this.VersionNumberCurrentValue = Convert.ToString(Formatter.FormatNumber(project.Version, 2));
                }
            }
            else
            {
                this.IsProject = false;
                Assembly assy = this.Model as Assembly;
                if (assy != null)
                {
                    if (assy.Version.HasValue)
                    {
                        this.VersionNumberCurrentValue = Convert.ToString(Formatter.FormatNumber(assy.Version, 2));
                    }

                    if (assy.SBMActive.HasValue)
                    {
                        this.IsToolingActiveCurrentValue = (bool)assy.SBMActive;
                        this.IsToolingActiveNewValue = (bool)assy.SBMActive;
                    }

                    this.CalculationVariantNewValue.Value = assy.CalculationVariant;
                    this.CalculationVariantCurrentValue = assy.CalculationVariant;
                }
                else
                {
                    Part part = this.Model as Part;
                    if (part != null)
                    {
                        if (part.Version.HasValue)
                        {
                            this.VersionNumberCurrentValue = Convert.ToString(Formatter.FormatNumber(part.Version, 2));
                        }

                        if (part.SBMActive.HasValue)
                        {
                            this.IsToolingActiveCurrentValue = (bool)part.SBMActive;
                            this.IsToolingActiveNewValue = (bool)part.SBMActive;
                        }

                        this.CalculationVariantNewValue.Value = part.CalculationVariant;
                        this.CalculationVariantCurrentValue = part.CalculationVariant;
                    }
                }
            }
        }

        /// <summary>
        /// Handles the PropertyChanged event of the ApplyUpdatesToSubObjects control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void ApplyUpdatesToSubObjects_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            Project project = this.Model as Project;
            if (project != null)
            {
                if (!this.ApplyUpdatesToSubObjects.Value
                    && !string.Equals(this.BaseCurrencyCurrentValue, this.BaseCurrencyNewValue.Value.Name))
                {
                    var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_ApplyUpdatesToSubObjects, MessageDialogType.YesNo);
                    if (result != MessageDialogResult.Yes)
                    {
                        this.messenger.Send<NotificationMessage>(new NotificationMessage(MassDataUpdateViewModel.SetApplyUpdatesToSubObjectsTrue));
                    }
                }
            }
        }

        /// <summary>
        /// Handles the ValueChanged event of the BaseCurrencyNewValue control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ValueChangedEventArgs{Currency}"/> instance containing the event data.</param>
        private void BaseCurrencyNewValue_ValueChanged(object sender, ValueChangedEventArgs<Currency> e)
        {
            if (!this.ApplyUpdatesToSubObjects.Value
                 && !this.isLoadingCurrencies)
            {
                this.windowService.MessageDialogService.Show(LocalizedResources.MassDataUpdate_BaseCurrency, MessageDialogType.Info);
                this.messenger.Send<NotificationMessage>(new NotificationMessage(MassDataUpdateViewModel.SetApplyUpdatesToSubObjectsTrue));
            }
        }

        /// <summary>
        /// Loads the currencies from the project model.
        /// </summary>
        /// <param name="project">The project</param>
        private void LoadCurrencies(Project project)
        {
            this.isLoadingCurrencies = true;
            this.ProjectCurrencies.Clear();
            this.ProjectCurrencies.AddRange<Currency>(project.Currencies.OrderBy(c => c.Name));
            if (project.BaseCurrency != null)
            {
                this.BaseCurrencyNewValue.Value = this.ProjectCurrencies.FirstOrDefault(c => c.IsoCode == project.BaseCurrency.IsoCode);
                this.BaseCurrencyCurrentValue = this.BaseCurrencyNewValue.Value.Name;
            }
            else
            {
                dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
                CloneManager cloneManager = new CloneManager(dataManager);
                var baseCurrencies = dataManager.CurrencyRepository.GetBaseCurrencies();
                foreach (var currency in baseCurrencies)
                {
                    var currencyClone = cloneManager.Clone(currency);
                    if (currencyClone != null)
                    {
                        currencyClone.IsMasterData = false;
                        this.ProjectCurrencies.Add(currencyClone);
                    }
                }

                this.BaseCurrencyNewValue.Value = this.ProjectCurrencies.FirstOrDefault(c => c.IsoCode == Constants.DefaultCurrencyIsoCode);
                this.BaseCurrencyCurrentValue = this.BaseCurrencyNewValue.Value.Name;
            }

            this.isLoadingCurrencies = false;
        }

        /// <summary>
        /// Performs the update operation
        /// </summary>
        private void UpdateAction()
        {
            this.CheckModelAndDataSource();

            // set the appropriate message
            string message = this.ApplyUpdatesToSubObjects.Value ? LocalizedResources.MassDataUpdate_ConfirmationWithSubobjects : LocalizedResources.MassDataUpdate_ConfirmationNoSubobjects;
            MessageDialogResult result = this.windowService.MessageDialogService.Show(message, MessageDialogType.YesNo);
            if (result == MessageDialogResult.No)
            {
                return;
            }

            object updatedEntity = null;
            numberOfUpdatedObjects = 0;

            this.DataSourceManager.RevertChanges();

            // apply for all sub-objects starting from the current object
            // search for all the machines in the project
            Project project = this.Model as Project;
            if (project != null)
            {
                if (this.ApplyUpdatesToSubObjects.Value)
                {
                    // special case for project, must be retrieved on a new context
                    Task.Factory.StartNew(() =>
                    {
                        this.messenger.Send<NotificationMessage>(new NotificationMessage("UpdateMassData"));
                        Project fullProject = this.DataSourceManager.ProjectRepository.GetProjectFull(project.Guid);

                        if (fullProject != null)
                        {
                            this.UpdateProject(fullProject);
                        }

                        this.DataSourceManager.SaveChanges();
                    })
                    .ContinueWith(
                        (task) =>
                        {
                            this.messenger.Send<NotificationMessage>(new NotificationMessage("UpdateMassDataFinished"));
                            if (task.Exception != null)
                            {
                                Exception error = task.Exception.InnerException;
                                log.ErrorException("Project loading failed.", error);
                                this.windowService.MessageDialogService.Show(error);
                            }
                            else
                            {
                                // refresh the context of the main view
                                EntityChangedMessage notificationMessage = new EntityChangedMessage(Notification.MyProjectsEntityChanged);
                                notificationMessage.ChangeType = EntityChangeType.RefreshData;
                                notificationMessage.Entity = project;
                                this.messenger.Send<EntityChangedMessage>(notificationMessage);

                                this.windowService.MessageDialogService.Show(LocalizedResources.MassDataUpdate_NumberOfUpdatedObjects + numberOfUpdatedObjects, MessageDialogType.Info);
                                this.messenger.Send<NotificationMessage>(new NotificationMessage("CloseMassDataUpdateWindow"));
                            }
                        },
                    System.Threading.Tasks.TaskScheduler.FromCurrentSynchronizationContext());

                    return;
                }
                else
                {
                    // else just update the current project
                    this.UpdateProject(project);
                    updatedEntity = project;
                }
            }
            else
            {
                // search for all the machines in the assembly
                Assembly assembly = this.Model as Assembly;
                if (assembly != null)
                {
                    this.UpdateAssembly(assembly);
                    updatedEntity = assembly;
                }
                else
                {
                    // search for all the machines in the part
                    Part part = this.Model as Part;
                    if (part != null)
                    {
                        this.UpdatePart(part);
                        updatedEntity = part;
                    }
                }
            }

            if (this.DataSourceManager != null)
            {
                this.DataSourceManager.SaveChanges();
            }

            if (updatedEntity != null)
            {
                EntityChangedMessage msg = new EntityChangedMessage(Notification.MyProjectsEntityChanged);
                msg.Entity = updatedEntity;
                msg.Parent = null;
                msg.ChangeType = EntityChangeType.RefreshData;
                this.messenger.Send(msg);
            }

            this.windowService.MessageDialogService.Show(LocalizedResources.MassDataUpdate_NumberOfUpdatedObjects + numberOfUpdatedObjects, MessageDialogType.Info);
            this.messenger.Send(new NotificationMessage("CloseMassDataUpdateWindow"));
        }

        /// <summary>
        /// Update the machines in a project
        /// </summary>
        /// <param name="project">The project</param>
        private void UpdateProject(Project project)
        {
            var wasObjectUpdated = false;
            if (this.VersionNumberNewValue != null)
            {
                project.Version = this.VersionNumberNewValue;
                wasObjectUpdated = true;
            }

            if (!string.Equals(this.BaseCurrencyCurrentValue, this.BaseCurrencyNewValue.Value.Name))
            {
                if (project.BaseCurrency == null)
                {
                    foreach (var currency in this.ProjectCurrencies)
                    {
                        project.Currencies.Add(currency);
                    }

                    project.BaseCurrency = project.Currencies.First(c => c.IsoCode == Constants.DefaultCurrencyIsoCode);
                }

                CurrencyConversionManager.ConvertObject(project, this.BaseCurrencyNewValue.Value, project.BaseCurrency);

                project.BaseCurrency = this.BaseCurrencyNewValue.Value;
                wasObjectUpdated = true;
            }

            if (wasObjectUpdated)
            {
                this.numberOfUpdatedObjects++;
            }

            // apply for sub-objects if selected
            if (this.ApplyUpdatesToSubObjects.Value)
            {
                foreach (Assembly assembly in project.Assemblies.Where(p => !p.IsDeleted))
                {
                    UpdateAssembly(assembly);
                }

                foreach (Part part in project.Parts.Where(p => !p.IsDeleted))
                {
                    UpdatePart(part);
                }
            }
        }

        /// <summary>
        /// Updates machines in an assembly
        /// </summary>
        /// <param name="assembly">The assembly</param>
        private void UpdateAssembly(Assembly assembly)
        {
            var wasObjectUpdated = false;
            if (this.VersionNumberNewValue != null)
            {
                assembly.Version = this.VersionNumberNewValue;
                wasObjectUpdated = true;
            }

            if (this.CalculationVariantNewValue.Value != assembly.CalculationVariant
                && !string.IsNullOrEmpty(this.CalculationVariantNewValue.Value))
            {
                assembly.CalculationVariant = this.CalculationVariantNewValue.Value;
                wasObjectUpdated = true;
            }

            if (assembly.SBMActive != IsToolingActiveNewValue)
            {
                assembly.SBMActive = IsToolingActiveNewValue;
                wasObjectUpdated = true;
            }

            if (wasObjectUpdated)
            {
                this.numberOfUpdatedObjects++;
            }

            // apply for sub-objects if selected
            if (this.ApplyUpdatesToSubObjects.Value)
            {
                foreach (Assembly subAssembly in assembly.Subassemblies.Where(p => !p.IsDeleted))
                {
                    UpdateAssembly(subAssembly);
                }

                foreach (Part part in assembly.Parts.Where(p => !p.IsDeleted))
                {
                    UpdatePart(part);
                }
            }
        }

        /// <summary>
        /// Update the machines in a part
        /// </summary>
        /// <param name="part">The part</param>
        private void UpdatePart(Part part)
        {
            var wasObjectUpdated = false;
            if (this.VersionNumberNewValue != null)
            {
                part.Version = this.VersionNumberNewValue;
                wasObjectUpdated = true;
            }

            if (this.CalculationVariantNewValue.Value != part.CalculationVariant
                && !string.IsNullOrEmpty(this.CalculationVariantNewValue.Value))
            {
                part.CalculationVariant = this.CalculationVariantNewValue.Value;
                wasObjectUpdated = true;
            }

            if (part.SBMActive != IsToolingActiveNewValue)
            {
                part.SBMActive = IsToolingActiveNewValue;
                wasObjectUpdated = true;
            }

            if (wasObjectUpdated)
            {
                this.numberOfUpdatedObjects++;
            }

            if (ApplyUpdatesToSubObjects.Value && part.RawPart != null && !part.RawPart.IsDeleted)
            {
                this.UpdatePart(part.RawPart);
            }
        }
    }
}
