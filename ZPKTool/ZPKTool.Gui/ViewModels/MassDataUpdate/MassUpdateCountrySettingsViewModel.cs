﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ZPKTool.Business;
using ZPKTool.Controls;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for Mass Update Country Settings View
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class MassUpdateCountrySettingsViewModel : ViewModel<object, IDataSourceManager>
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The messenger reference
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The window service reference
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// Specifies if the updates are applied on the sub-objects of the current object
        /// </summary>
        private bool applyUpdatesToSubObjects;

        /// <summary>
        /// Counts the number of updated objects
        /// </summary>
        private int numberOfUpdatedObjects = 0;

        /// <summary>
        /// Current value of the country
        /// </summary>
        private string countryCurrentValue;

        /// <summary>
        /// New value of the country
        /// </summary>
        private string countryNewValue;

        /// <summary>
        /// current value of the supplier
        /// </summary>
        private string supplierCurrentValue;

        /// <summary>
        /// new value of the supplier
        /// </summary>
        private string supplierNewValue;

        /// <summary>
        /// Reference to the country and supplier browser view model
        /// </summary>
        private CountryAndSupplierBrowserViewModel countryAndSupplierBrowser;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="MassUpdateCountrySettingsViewModel"/> class.
        /// </summary>
        /// <param name="messenger">The messenger service.</param>
        /// <param name="windowService">The window service.</param>
        [ImportingConstructor]
        public MassUpdateCountrySettingsViewModel(IMessenger messenger, IWindowService windowService)
        {
            this.messenger = messenger;
            this.windowService = windowService;
            this.UpdateCommand = new DelegateCommand(UpdateCountrySettings);
            this.BrowseCountryCommand = new DelegateCommand(BrowseCountry);
            this.BrowseSupplierCommand = new DelegateCommand(BrowseSupplier);
        }

        #region Commands

        /// <summary>
        /// Gets the command for the Update button
        /// </summary>
        public ICommand UpdateCommand { get; private set; }

        /// <summary>
        /// Gets the command for the browse master data button
        /// </summary>
        public ICommand BrowseCountryCommand { get; private set; }

        /// <summary>
        /// Gets the command for the browse master data button
        /// </summary>
        public ICommand BrowseSupplierCommand { get; private set; }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the new country settings object which will replace the current settings.
        /// </summary>
        public CountrySetting NewCountrySettings { get; set; }

        /// <summary>
        /// Gets or sets the country and supplier browser view model.
        /// It is a public property in order to be accessible from the unit tests
        /// </summary>
        [Import]
        public CountryAndSupplierBrowserViewModel CountryAndSupplierBrowser
        {
            get
            {
                return this.countryAndSupplierBrowser;
            }

            set
            {
                if (value != null &&
                    this.countryAndSupplierBrowser != value)
                {
                    this.countryAndSupplierBrowser = value;
                    this.countryAndSupplierBrowser.CountryOrSupplierSelected += new MasterDataEntitySelectionHandler(CountryOrSupplierSelected);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to apply updates to sub objects.
        /// </summary>
        public bool ApplyUpdatesToSubObjects
        {
            get
            {
                return this.applyUpdatesToSubObjects;
            }

            set
            {
                if (this.applyUpdatesToSubObjects != value)
                {
                    this.applyUpdatesToSubObjects = value;
                    OnPropertyChanged(() => this.ApplyUpdatesToSubObjects);
                }
            }
        }

        /// <summary>
        /// Gets or sets the country current value.
        /// </summary>
        public string CountryCurrentValue
        {
            get
            {
                return this.countryCurrentValue;
            }

            set
            {
                if (this.countryCurrentValue != value)
                {
                    this.countryCurrentValue = value;
                    OnPropertyChanged(() => this.CountryCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the country new value.
        /// </summary>
        public string CountryNewValue
        {
            get
            {
                return this.countryNewValue;
            }

            set
            {
                if (this.countryNewValue != value)
                {
                    this.countryNewValue = value;
                    OnPropertyChanged(() => this.CountryNewValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the supplier current value.
        /// </summary>
        public string SupplierCurrentValue
        {
            get
            {
                return this.supplierCurrentValue;
            }

            set
            {
                if (this.supplierCurrentValue != value)
                {
                    this.supplierCurrentValue = value;
                    OnPropertyChanged(() => this.SupplierCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the supplier new value.
        /// </summary>
        public string SupplierNewValue
        {
            get
            {
                return this.supplierNewValue;
            }

            set
            {
                if (this.supplierNewValue != value)
                {
                    this.supplierNewValue = value;
                    OnPropertyChanged(() => this.SupplierNewValue);
                }
            }
        }

        #endregion

        /// <summary>
        /// Set up the gui based on the input object
        /// </summary>
        protected override void OnModelChanged()
        {
            Assembly assy = this.Model as Assembly;
            if (assy != null)
            {
                if (assy.AssemblingCountry != null)
                {
                    CountryCurrentValue = assy.AssemblingCountry;
                }

                if (assy.AssemblingSupplier != null)
                {
                    SupplierCurrentValue = assy.AssemblingSupplier;
                }
            }

            Part part = this.Model as Part;
            if (part != null)
            {
                if (part.ManufacturingCountry != null)
                {
                    CountryCurrentValue = part.ManufacturingCountry;
                }

                if (part.ManufacturingSupplier != null)
                {
                    SupplierCurrentValue = part.ManufacturingSupplier;
                }
            }
        }

        /// <summary>
        /// Performs the update operation
        /// </summary>
        private void UpdateCountrySettings()
        {
            // set the appropriate message
            string message = this.ApplyUpdatesToSubObjects ? LocalizedResources.MassDataUpdate_ConfirmationWithSubobjects : LocalizedResources.MassDataUpdate_ConfirmationNoSubobjects;
            MessageDialogResult result = this.windowService.MessageDialogService.Show(message, MessageDialogType.YesNo);
            if (result == MessageDialogResult.No)
            {
                return;
            }

            // Get the selected entity parent project, to get it's base currency, and convert the selected country setting.
            // If the project has not base currency set the default currency will be used.
            var dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var parentProject = dataManager.ProjectRepository.GetParentProject(this.Model);
            var parentProjectBaseCurrency = dataManager.CurrencyRepository.GetProjectBaseCurrency(parentProject.Guid);
            var currencies = dataManager.CurrencyRepository.GetBaseCurrencies();
            var projectBaseCurrencyFromMD = currencies.FirstOrDefault(c => c.IsSameAs(parentProjectBaseCurrency));

            CurrencyConversionManager.ConvertObject(this.NewCountrySettings, projectBaseCurrencyFromMD, CurrencyConversionManager.DefaultBaseCurrency);

            object updatedEntity = null;
            numberOfUpdatedObjects = 0;

            Project project = this.Model as Project;
            if (project != null && this.ApplyUpdatesToSubObjects)
            {
                // special case for project, must be retrieved on a new context
                Task.Factory.StartNew(() =>
                {
                    this.messenger.Send<NotificationMessage>(new NotificationMessage("UpdateMassData"));
                    Project fullProject = this.DataSourceManager.ProjectRepository.GetProjectFull(project.Guid);

                    if (fullProject != null)
                    {
                        this.UpdateProject(fullProject);
                    }

                    this.DataSourceManager.SaveChanges();
                })
                .ContinueWith(
                    (task) =>
                    {
                        this.messenger.Send<NotificationMessage>(new NotificationMessage("UpdateMassDataFinished"));
                        if (task.Exception != null)
                        {
                            Exception error = task.Exception.InnerException;
                            log.ErrorException("Project loading failed.", error);
                            this.windowService.MessageDialogService.Show(error);
                        }
                        else
                        {
                            // refresh the context of the main view
                            EntityChangedMessage notificationMessage = new EntityChangedMessage(Notification.MyProjectsEntityChanged);
                            notificationMessage.ChangeType = EntityChangeType.RefreshData;
                            notificationMessage.Entity = project;
                            this.messenger.Send<EntityChangedMessage>(notificationMessage);

                            this.windowService.MessageDialogService.Show(LocalizedResources.MassDataUpdate_NumberOfUpdatedObjects + numberOfUpdatedObjects, MessageDialogType.Info);
                            this.messenger.Send<NotificationMessage>(new NotificationMessage("CloseMassDataUpdateWindow"));
                        }
                    },
                    System.Threading.Tasks.TaskScheduler.FromCurrentSynchronizationContext());

                return;
            }

            this.DataSourceManager.RevertChanges();

            Assembly assembly = this.Model as Assembly;
            if (assembly != null)
            {
                this.UpdateAssembly(assembly);
                updatedEntity = assembly;
            }
            else
            {
                Part part = this.Model as Part;
                if (part != null)
                {
                    this.UpdatePart(part);
                    updatedEntity = part;
                }
            }

            if (this.DataSourceManager != null)
            {
                this.DataSourceManager.SaveChanges();
            }

            if (updatedEntity != null)
            {
                EntityChangedMessage msg = new EntityChangedMessage(Notification.MyProjectsEntityChanged);
                msg.Entity = updatedEntity;
                msg.Parent = null;
                msg.ChangeType = EntityChangeType.RefreshData;
                this.messenger.Send(msg);
            }

            this.windowService.MessageDialogService.Show(LocalizedResources.MassDataUpdate_NumberOfUpdatedObjects + numberOfUpdatedObjects, MessageDialogType.Info);
            this.messenger.Send<NotificationMessage>(new NotificationMessage("CloseMassDataUpdateWindow"));
        }

        /// <summary>
        /// Update the fields in a project
        /// </summary>
        /// <param name="project">The project.</param>
        private void UpdateProject(Project project)
        {
            foreach (Assembly assembly in project.Assemblies.Where(p => !p.IsDeleted))
            {
                this.UpdateAssembly(assembly);
            }

            foreach (Part part in project.Parts.Where(p => !p.IsDeleted))
            {
                this.UpdatePart(part);
            }
        }

        /// <summary>
        /// Update the fields in an assembly
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        private void UpdateAssembly(Assembly assembly)
        {
            bool objectUpdated = false;

            if (this.supplierNewValue != assembly.AssemblingSupplier && this.countryNewValue != null)
            {
                assembly.AssemblingSupplier = this.supplierNewValue;
                objectUpdated = true;
            }

            if (this.countryNewValue != null)
            {
                assembly.AssemblingCountry = this.countryNewValue;
                objectUpdated = true;
            }

            if (this.NewCountrySettings != null)
            {
                this.NewCountrySettings.CopyValuesTo(assembly.CountrySettings);
            }

            if (objectUpdated)
            {
                numberOfUpdatedObjects++;
            }

            // update sub-objects if option is selected
            if (this.ApplyUpdatesToSubObjects)
            {
                foreach (Assembly subAssembly in assembly.Subassemblies.Where(p => !p.IsDeleted))
                {
                    this.UpdateAssembly(subAssembly);
                }

                foreach (Part part in assembly.Parts.Where(p => !p.IsDeleted))
                {
                    this.UpdatePart(part);
                }
            }
        }

        /// <summary>
        /// Update the fields in a part
        /// </summary>
        /// <param name="part">The part.</param>
        private void UpdatePart(Part part)
        {
            bool objectUpdated = false;

            if (this.supplierNewValue != part.ManufacturingSupplier && this.countryNewValue != null)
            {
                part.ManufacturingSupplier = this.supplierNewValue;
                objectUpdated = true;
            }

            if (this.countryNewValue != null)
            {
                part.ManufacturingCountry = this.countryNewValue;
                objectUpdated = true;
            }

            if (this.NewCountrySettings != null)
            {
                this.NewCountrySettings.CopyValuesTo(part.CountrySettings);
            }

            if (objectUpdated)
            {
                numberOfUpdatedObjects++;
            }

            // update sub-objects if option is selected
            if (ApplyUpdatesToSubObjects && part.RawPart != null && !part.RawPart.IsDeleted)
            {
                this.UpdatePart(part.RawPart);
            }
        }

        /// <summary>
        /// Browse master data countries
        /// </summary>
        private void BrowseCountry()
        {
            CountryAndSupplierBrowser.LoadLocation = DbIdentifier.LocalDatabase;
            CountryAndSupplierBrowser.CountryName = null;
            this.windowService.ShowViewInDialog(CountryAndSupplierBrowser);
        }

        /// <summary>
        /// Browse master data suppliers
        /// </summary>
        private void BrowseSupplier()
        {
            string countryName = null;
            if (CountryNewValue != null)
            {
                countryName = CountryNewValue;
            }
            else if (!string.IsNullOrWhiteSpace(CountryCurrentValue))
            {
                countryName = CountryCurrentValue;
            }

            if (countryName != null)
            {
                CountryAndSupplierBrowser.LoadLocation = DbIdentifier.LocalDatabase;
                CountryAndSupplierBrowser.CountryName = countryName;
                var result = this.windowService.ShowViewInDialog(CountryAndSupplierBrowser);
            }
            else
            {
                this.windowService.MessageDialogService.Show(LocalizedResources.General_SelectCountryFirst, MessageDialogType.Info);
            }
        }

        /// <summary>
        /// A country or a supplier is selected
        /// </summary>
        /// <param name="masterDataEntity">Master data entity selected</param>
        /// <param name="databaseId">A value indicating the source database of the entity.</param>
        private void CountryOrSupplierSelected(object masterDataEntity, DbIdentifier databaseId)
        {
            Country country = masterDataEntity as Country;
            if (country != null)
            {
                CountryNewValue = country.Name;
                SupplierNewValue = null;
                this.NewCountrySettings = country.CountrySetting;
            }

            CountryState supplier = masterDataEntity as CountryState;
            if (supplier != null)
            {
                SupplierNewValue = supplier.Name;
                CountryNewValue = supplier.Country.Name;
                this.NewCountrySettings = supplier.CountrySettings;
            }
        }
    }
}