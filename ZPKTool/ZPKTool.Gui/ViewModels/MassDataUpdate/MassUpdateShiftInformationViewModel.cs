﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for Mass Update Shift Information
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class MassUpdateShiftInformationViewModel : ViewModel<object, IDataSourceManager>
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The messenger reference
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The window service reference
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// Specifies if the updates are applied on the sub-objects of the current object
        /// </summary>
        private bool applyUpdatesToSubObjects;

        /// <summary>
        /// Counts the number of updated entities
        /// </summary>
        private int numberOfUpdatedObjects = 0;

        /// <summary>
        /// Current value for shifts per week
        /// </summary>
        private string shiftsPerWeekCurrentValue;

        /// <summary>
        /// Current value for hours per shift
        /// </summary>
        private string hoursPerShiftCurrentValue;

        /// <summary>
        /// Current value for production days per week
        /// </summary>
        private string productionDaysPerWeekCurrentValue;

        /// <summary>
        /// Current value for production weeks per year
        /// </summary>
        private string productionWeeksPerYearCurrentValue;

        /// <summary>
        /// Increase Percentage value for shifts per week
        /// </summary>
        private decimal? shiftsPerWeekIncreasePercentage;

        /// <summary>
        /// Increase Percentage value for hours per shift
        /// </summary>
        private decimal? hoursPerShiftIncreasePercentage;

        /// <summary>
        /// Increase Percentage for production days per week
        /// </summary>
        private decimal? productionDaysPerWeekIncreasePercentage;

        /// <summary>
        /// Increase percentage for production weeks per year
        /// </summary>
        private decimal? productionWeeksPerYearIncreasePercentage;

        /// <summary>
        /// New value for shifts per week
        /// </summary>
        private string shiftsPerWeekNewValue;

        /// <summary>
        /// New value for hours per shift
        /// </summary>
        private string hoursPerShiftNewValue;

        /// <summary>
        /// New value for production days per week
        /// </summary>
        private string productionDaysPerWeekNewValue;

        /// <summary>
        /// Current value for production weeks per year
        /// </summary>
        private string productionWeeksPerYearNewValue;

        /// <summary>
        /// Shifts per week MaxContentLimit value
        /// </summary>
        private decimal? shiftsPerWeekMaxContentLimit;

        /// <summary>
        /// Hours per shift MaxContentLimit value
        /// </summary>
        private decimal? hoursPerShiftMaxContentLimit;

        /// <summary>
        /// Increase Percentage max limit value for shifts per week.
        /// </summary>
        private decimal? shiftsPerWeekIncreasePercentageMaxLimit;

        /// <summary>
        /// Increase Percentage max limit value for hours per shift.
        /// </summary>
        private decimal? hoursPerShiftIncreasePercentageMaxLimit;

        /// <summary>
        /// Increase Percentage maximum limit for production days per week.
        /// </summary>
        private decimal? productionDaysPerWeekIncreasePercentageMaxLimit;

        /// <summary>
        /// Increase percentage maximum limit for production weeks per year.
        /// </summary>
        private decimal? productionWeeksPerYearIncreasePercentageMaxLimit;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        #endregion Attributes

        /// <summary>
        /// Initializes a new instance of the <see cref="MassUpdateShiftInformationViewModel"/> class.
        /// </summary>
        /// <param name="messenger">The messenger service.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="unitsService">The units service.</param>
        [ImportingConstructor]
        public MassUpdateShiftInformationViewModel(
            IMessenger messenger,
            IWindowService windowService,
            IUnitsService unitsService)
        {
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("unitsService", unitsService);

            this.messenger = messenger;
            this.windowService = windowService;
            this.unitsService = unitsService;

            this.UpdateCommand = new DelegateCommand(UpdateShiftInformation);
            this.ShiftsPerWeekGotFocusCommand = new DelegateCommand(CalculateShiftMaxContentLimits);
            this.HoursPerShiftGotFocusCommand = new DelegateCommand(CalculateShiftMaxContentLimits);
        }

        #region Commands

        /// <summary>
        /// Gets the command for the Update button
        /// </summary>
        public ICommand UpdateCommand { get; private set; }

        /// <summary>
        /// Gets the command executed for the GotFocus event of the ShiftsPerWeekTextBox.
        /// </summary>
        public ICommand ShiftsPerWeekGotFocusCommand { get; private set; }

        /// <summary>
        /// Gets the command executed for the GotFocus event of the HoursPerShiftGotTextBox.
        /// </summary>
        public ICommand HoursPerShiftGotFocusCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether to apply updates to sub objects.
        /// </summary>
        public bool ApplyUpdatesToSubObjects
        {
            get
            {
                return this.applyUpdatesToSubObjects;
            }

            set
            {
                if (this.applyUpdatesToSubObjects != value)
                {
                    this.applyUpdatesToSubObjects = value;
                    OnPropertyChanged(() => this.ApplyUpdatesToSubObjects);
                }
            }
        }

        /// <summary>
        /// Gets or sets the shifts per week current value.
        /// </summary>
        public string ShiftsPerWeekCurrentValue
        {
            get
            {
                return this.shiftsPerWeekCurrentValue;
            }

            set
            {
                if (this.shiftsPerWeekCurrentValue != value)
                {
                    this.shiftsPerWeekCurrentValue = value;
                    OnPropertyChanged(() => this.ShiftsPerWeekCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the hours per shift current value.
        /// </summary>
        public string HoursPerShiftCurrentValue
        {
            get
            {
                return this.hoursPerShiftCurrentValue;
            }

            set
            {
                if (this.hoursPerShiftCurrentValue != value)
                {
                    this.hoursPerShiftCurrentValue = value;
                    OnPropertyChanged(() => this.HoursPerShiftCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the production days per week current value.
        /// </summary>
        public string ProductionDaysPerWeekCurrentValue
        {
            get
            {
                return this.productionDaysPerWeekCurrentValue;
            }

            set
            {
                if (this.productionDaysPerWeekCurrentValue != value)
                {
                    this.productionDaysPerWeekCurrentValue = value;
                    OnPropertyChanged(() => this.ProductionDaysPerWeekCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the production weeks per year current value.
        /// </summary>
        public string ProductionWeeksPerYearCurrentValue
        {
            get
            {
                return this.productionWeeksPerYearCurrentValue;
            }

            set
            {
                if (this.productionWeeksPerYearCurrentValue != value)
                {
                    this.productionWeeksPerYearCurrentValue = value;
                    OnPropertyChanged(() => this.ProductionWeeksPerYearCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the shifts per week increase percentage.
        /// </summary>
        public decimal? ShiftsPerWeekIncreasePercentage
        {
            get
            {
                return this.shiftsPerWeekIncreasePercentage;
            }

            set
            {
                if (this.shiftsPerWeekIncreasePercentage != value)
                {
                    this.shiftsPerWeekIncreasePercentage = value;
                    OnPropertyChanged(() => this.ShiftsPerWeekIncreasePercentage);

                    this.CalculateShiftMaxContentLimits();
                    if (value != null)
                    {
                        ShiftsPerWeekNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the hours per shift increase percentage.
        /// </summary>
        public decimal? HoursPerShiftIncreasePercentage
        {
            get
            {
                return this.hoursPerShiftIncreasePercentage;
            }

            set
            {
                if (this.hoursPerShiftIncreasePercentage != value)
                {
                    this.hoursPerShiftIncreasePercentage = value;
                    OnPropertyChanged(() => this.HoursPerShiftIncreasePercentage);

                    this.CalculateShiftMaxContentLimits();
                    if (value != null)
                    {
                        HoursPerShiftNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the production days per week increase percentage.
        /// </summary>
        public decimal? ProductionDaysPerWeekIncreasePercentage
        {
            get
            {
                return this.productionDaysPerWeekIncreasePercentage;
            }

            set
            {
                if (this.productionDaysPerWeekIncreasePercentage != value)
                {
                    this.productionDaysPerWeekIncreasePercentage = value;
                    OnPropertyChanged(() => this.ProductionDaysPerWeekIncreasePercentage);

                    if (value != null)
                    {
                        ProductionDaysPerWeekNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the production weeks per year increase percentage.
        /// </summary>
        public decimal? ProductionWeeksPerYearIncreasePercentage
        {
            get
            {
                return this.productionWeeksPerYearIncreasePercentage;
            }

            set
            {
                if (this.productionWeeksPerYearIncreasePercentage != value)
                {
                    this.productionWeeksPerYearIncreasePercentage = value;
                    OnPropertyChanged(() => this.ProductionWeeksPerYearIncreasePercentage);

                    if (value != null)
                    {
                        ProductionWeeksPerYearNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the shifts per week increase Percentage max limit.
        /// </summary>
        public decimal? ShiftsPerWeekIncreasePercentageMaxLimit
        {
            get
            {
                return this.shiftsPerWeekIncreasePercentageMaxLimit;
            }

            set
            {
                if (this.shiftsPerWeekIncreasePercentageMaxLimit != value)
                {
                    this.shiftsPerWeekIncreasePercentageMaxLimit = value;
                    OnPropertyChanged(() => this.ShiftsPerWeekIncreasePercentageMaxLimit);
                }
            }
        }

        /// <summary>
        /// Gets or sets the hours per shift increase percentage max limit.
        /// </summary>
        public decimal? HoursPerShiftIncreasePercentageMaxLimit
        {
            get
            {
                return this.hoursPerShiftIncreasePercentageMaxLimit;
            }

            set
            {
                if (this.hoursPerShiftIncreasePercentageMaxLimit != value)
                {
                    this.hoursPerShiftIncreasePercentageMaxLimit = value;
                    OnPropertyChanged(() => this.HoursPerShiftIncreasePercentageMaxLimit);
                }
            }
        }

        /// <summary>
        /// Gets or sets the production days per week increase percentage maximum limit.
        /// </summary>
        public decimal? ProductionDaysPerWeekIncreasePercentageMaxLimit
        {
            get
            {
                return this.productionDaysPerWeekIncreasePercentageMaxLimit;
            }

            set
            {
                if (this.productionDaysPerWeekIncreasePercentageMaxLimit != value)
                {
                    this.productionDaysPerWeekIncreasePercentageMaxLimit = value;
                    OnPropertyChanged(() => this.ProductionDaysPerWeekIncreasePercentageMaxLimit);
                }
            }
        }

        /// <summary>
        /// Gets or sets the production weeks per year increase percentage maximum limit.
        /// </summary>
        public decimal? ProductionWeeksPerYearIncreasePercentageMaxLimit
        {
            get
            {
                return this.productionWeeksPerYearIncreasePercentageMaxLimit;
            }

            set
            {
                if (this.productionWeeksPerYearIncreasePercentageMaxLimit != value)
                {
                    this.productionWeeksPerYearIncreasePercentageMaxLimit = value;
                    OnPropertyChanged(() => this.ProductionWeeksPerYearIncreasePercentageMaxLimit);
                }
            }
        }

        /// <summary>
        /// Gets or sets the shifts per week new value.
        /// </summary>
        public string ShiftsPerWeekNewValue
        {
            get
            {
                return this.shiftsPerWeekNewValue;
            }

            set
            {
                if (this.shiftsPerWeekNewValue != value)
                {
                    this.shiftsPerWeekNewValue = value;
                    OnPropertyChanged(() => this.ShiftsPerWeekNewValue);
                    this.CalculateShiftMaxContentLimits();

                    if (value != null)
                    {
                        ShiftsPerWeekIncreasePercentage = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the hours per shift new value.
        /// </summary>
        public string HoursPerShiftNewValue
        {
            get
            {
                return this.hoursPerShiftNewValue;
            }

            set
            {
                if (this.hoursPerShiftNewValue != value)
                {
                    this.hoursPerShiftNewValue = value;
                    OnPropertyChanged(() => this.HoursPerShiftNewValue);
                    this.CalculateShiftMaxContentLimits();

                    if (value != null)
                    {
                        HoursPerShiftIncreasePercentage = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the production days per week new value.
        /// </summary>
        public string ProductionDaysPerWeekNewValue
        {
            get
            {
                return this.productionDaysPerWeekNewValue;
            }

            set
            {
                if (this.productionDaysPerWeekNewValue != value)
                {
                    this.productionDaysPerWeekNewValue = value;
                    OnPropertyChanged(() => this.ProductionDaysPerWeekNewValue);

                    if (value != null)
                    {
                        ProductionDaysPerWeekIncreasePercentage = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the production weeks per year new value.
        /// </summary>
        public string ProductionWeeksPerYearNewValue
        {
            get
            {
                return this.productionWeeksPerYearNewValue;
            }

            set
            {
                if (this.productionWeeksPerYearNewValue != value)
                {
                    this.productionWeeksPerYearNewValue = value;
                    OnPropertyChanged(() => this.ProductionWeeksPerYearNewValue);

                    if (value != null)
                    {
                        ProductionWeeksPerYearIncreasePercentage = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the shifts per week max content limit.
        /// </summary>
        public decimal? ShiftsPerWeekMaxContentLimit
        {
            get
            {
                return this.shiftsPerWeekMaxContentLimit;
            }

            set
            {
                if (this.shiftsPerWeekMaxContentLimit != value)
                {
                    this.shiftsPerWeekMaxContentLimit = value;
                    OnPropertyChanged(() => this.ShiftsPerWeekMaxContentLimit);
                }
            }
        }

        /// <summary>
        /// Gets or sets the hours per shift max content limit.
        /// </summary>
        public decimal? HoursPerShiftMaxContentLimit
        {
            get
            {
                return this.hoursPerShiftMaxContentLimit;
            }

            set
            {
                if (this.hoursPerShiftMaxContentLimit != value)
                {
                    this.hoursPerShiftMaxContentLimit = value;
                    OnPropertyChanged(() => this.HoursPerShiftMaxContentLimit);
                }
            }
        }

        /// <summary>
        /// Gets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter { get; private set; }

        #endregion Properties

        /// <summary>
        /// Set up the gui based on the input object
        /// </summary>
        protected override void OnModelChanged()
        {
            // Load Shift Information current values from selected ProcessStep
            ProcessStep step = this.Model as ProcessStep;
            if (step != null)
            {
                if (step.HoursPerShift != null)
                {
                    HoursPerShiftCurrentValue = Formatter.FormatNumber(step.HoursPerShift.Value, 2);
                }

                if (step.ShiftsPerWeek != null)
                {
                    ShiftsPerWeekCurrentValue = Formatter.FormatNumber(step.ShiftsPerWeek.Value, 2);
                }

                if (step.ProductionDaysPerWeek != null)
                {
                    ProductionDaysPerWeekCurrentValue = Formatter.FormatNumber(step.ProductionDaysPerWeek.Value, 2);
                }

                if (step.ProductionWeeksPerYear != null)
                {
                    ProductionWeeksPerYearCurrentValue = Formatter.FormatNumber(step.ProductionWeeksPerYear.Value, 2);
                }
            }

            // Load Shift Information current values from selected Project
            Project project = this.Model as Project;
            if (project != null)
            {
                if (project.HoursPerShift != null)
                {
                    HoursPerShiftCurrentValue = Formatter.FormatNumber(project.HoursPerShift.Value, 2);
                }

                if (project.ShiftsPerWeek != null)
                {
                    ShiftsPerWeekCurrentValue = Formatter.FormatNumber(project.ShiftsPerWeek.Value, 2);
                }

                if (project.ProductionDaysPerWeek != null)
                {
                    ProductionDaysPerWeekCurrentValue = Formatter.FormatNumber(project.ProductionDaysPerWeek.Value, 2);
                }

                if (project.ProductionWeeksPerYear != null)
                {
                    ProductionWeeksPerYearCurrentValue = Formatter.FormatNumber(project.ProductionWeeksPerYear.Value, 2);
                }
            }

            this.CalculateShiftMaxContentLimits();
            this.SetProductionPercentagesMaxLimits();
        }

        /// <summary>
        /// Called when DataSourceManager has changed.
        /// </summary>
        protected override void OnDataSourceManagerChanged()
        {
            base.OnDataSourceManagerChanged();

            this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(this.DataSourceManager);
        }

        /// <summary>
        /// Perform the update operations
        /// </summary>
        private void UpdateShiftInformation()
        {
            // set the appropriate message
            string message = this.ApplyUpdatesToSubObjects ? LocalizedResources.MassDataUpdate_ConfirmationWithSubobjects : LocalizedResources.MassDataUpdate_ConfirmationNoSubobjects;
            MessageDialogResult result = this.windowService.MessageDialogService.Show(message, MessageDialogType.YesNo);
            if (result == MessageDialogResult.No)
            {
                return;
            }

            object updatedEntity = null;
            numberOfUpdatedObjects = 0;

            Project project = this.Model as Project;
            if (project != null && this.ApplyUpdatesToSubObjects)
            {
                // special case for project, must be retrieved on a new context
                Task.Factory.StartNew(() =>
                {
                    this.messenger.Send<NotificationMessage>(new NotificationMessage("UpdateMassData"));
                    var newDataManager = DataAccessFactory.CreateDataSourceManager(this.DataSourceManager.DatabaseId);
                    Project fullProject = newDataManager.ProjectRepository.GetProjectFull(project.Guid);

                    if (fullProject != null)
                    {
                        this.UpdateProject(fullProject);
                    }

                    newDataManager.SaveChanges();
                })
                .ContinueWith(
                    (task) =>
                    {
                        this.messenger.Send<NotificationMessage>(new NotificationMessage("UpdateMassDataFinished"));
                        if (task.Exception != null)
                        {
                            Exception error = task.Exception.InnerException;
                            log.ErrorException("Project loading failed.", error);
                            this.windowService.MessageDialogService.Show(error);
                        }

                        // refresh the context of the main view
                        EntityChangedMessage notificationMessage = new EntityChangedMessage(Notification.MyProjectsEntityChanged);
                        notificationMessage.ChangeType = EntityChangeType.RefreshData;
                        notificationMessage.Entity = project;
                        this.messenger.Send<EntityChangedMessage>(notificationMessage);

                        this.windowService.MessageDialogService.Show(LocalizedResources.MassDataUpdate_NumberOfUpdatedObjects + numberOfUpdatedObjects, MessageDialogType.Info);
                        this.messenger.Send<NotificationMessage>(new NotificationMessage("CloseMassDataUpdateWindow"));
                    },
                    System.Threading.Tasks.TaskScheduler.FromCurrentSynchronizationContext());

                return;
            }

            this.DataSourceManager.RevertChanges();

            if (project != null)
            {
                this.UpdateProject(project);
                updatedEntity = project;
            }
            else
            {
                Assembly assembly = this.Model as Assembly;
                if (assembly != null)
                {
                    this.UpdateAssembly(assembly);
                    updatedEntity = assembly;
                }
                else
                {
                    Part part = this.Model as Part;
                    if (part != null)
                    {
                        this.UpdatePart(part);
                        updatedEntity = part;
                    }
                    else
                    {
                        Process process = this.Model as Process;
                        if (process != null)
                        {
                            foreach (ProcessStep ps in process.Steps)
                            {
                                this.UpdateProcessStep(ps);
                            }

                            updatedEntity = process;
                        }
                        else
                        {
                            ProcessStep step = this.Model as ProcessStep;
                            if (step != null)
                            {
                                this.UpdateProcessStep(step);
                                updatedEntity = step;
                            }
                        }
                    }
                }
            }

            if (this.DataSourceManager != null)
            {
                this.DataSourceManager.SaveChanges();
            }

            if (updatedEntity != null)
            {
                EntityChangedMessage msg = new EntityChangedMessage(Notification.MyProjectsEntityChanged);
                msg.Entity = updatedEntity;
                msg.Parent = null;
                msg.ChangeType = EntityChangeType.RefreshData;
                this.messenger.Send(msg);
            }

            this.windowService.MessageDialogService.Show(LocalizedResources.MassDataUpdate_NumberOfUpdatedObjects + numberOfUpdatedObjects, MessageDialogType.Info);
            this.messenger.Send<NotificationMessage>(new NotificationMessage("CloseMassDataUpdateWindow"));
        }

        /// <summary>
        /// Update the fields in a project
        /// </summary>
        /// <param name="project">The project.</param>
        private void UpdateProject(Project project)
        {
            bool objectUpdated = false;

            // Update Project Shift information values
            if (!string.IsNullOrEmpty(this.shiftsPerWeekNewValue))
            {
                project.ShiftsPerWeek = Converter.StringToNullableDecimal(this.shiftsPerWeekNewValue);
                objectUpdated = true;
            }

            if (this.shiftsPerWeekIncreasePercentage != null)
            {
                project.ShiftsPerWeek = project.ShiftsPerWeek * this.shiftsPerWeekIncreasePercentage;
                objectUpdated = true;
            }

            if (!string.IsNullOrEmpty(this.hoursPerShiftNewValue))
            {
                project.HoursPerShift = Converter.StringToNullableDecimal(this.hoursPerShiftNewValue);
                objectUpdated = true;
            }

            if (this.hoursPerShiftIncreasePercentage != null)
            {
                project.HoursPerShift = project.HoursPerShift * this.hoursPerShiftIncreasePercentage;
                objectUpdated = true;
            }

            if (!string.IsNullOrEmpty(this.productionWeeksPerYearNewValue))
            {
                project.ProductionWeeksPerYear = Converter.StringToNullableDecimal(this.productionWeeksPerYearNewValue);
                objectUpdated = true;
            }

            if (this.productionWeeksPerYearIncreasePercentage != null)
            {
                project.ProductionWeeksPerYear = project.ProductionWeeksPerYear * this.productionWeeksPerYearIncreasePercentage;
                objectUpdated = true;
            }

            if (!string.IsNullOrEmpty(this.productionDaysPerWeekNewValue))
            {
                project.ProductionDaysPerWeek = Converter.StringToNullableDecimal(this.productionDaysPerWeekNewValue);
                objectUpdated = true;
            }

            if (this.productionDaysPerWeekIncreasePercentage != null)
            {
                project.ProductionDaysPerWeek = project.ProductionDaysPerWeek * this.productionDaysPerWeekIncreasePercentage;
                objectUpdated = true;
            }

            if (objectUpdated)
            {
                numberOfUpdatedObjects++;
            }

            // apply for sub-objects if selected
            if (this.ApplyUpdatesToSubObjects)
            {
                foreach (Assembly assembly in project.Assemblies.Where(p => !p.IsDeleted))
                {
                    UpdateAssembly(assembly);
                }

                foreach (Part part in project.Parts.Where(p => !p.IsDeleted))
                {
                    UpdatePart(part);
                }
            }
        }

        /// <summary>
        /// Update the fields in an assembly
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        private void UpdateAssembly(Assembly assembly)
        {
            // update values in process steps
            foreach (ProcessStep step in assembly.Process.Steps)
            {
                UpdateProcessStep(step);
            }

            if (this.ApplyUpdatesToSubObjects)
            {
                foreach (Assembly subAssembly in assembly.Subassemblies.Where(p => !p.IsDeleted))
                {
                    UpdateAssembly(subAssembly);
                }

                foreach (Part part in assembly.Parts.Where(p => !p.IsDeleted))
                {
                    UpdatePart(part);
                }
            }
        }

        /// <summary>
        /// Update the fields in a part
        /// </summary>
        /// <param name="part">The part.</param>
        private void UpdatePart(Part part)
        {
            foreach (ProcessStep step in part.Process.Steps)
            {
                UpdateProcessStep(step);
            }

            if (ApplyUpdatesToSubObjects && part.RawPart != null && !part.RawPart.IsDeleted)
            {
                this.UpdatePart(part.RawPart);
            }
        }

        /// <summary>
        /// Update the fields in a Process Step
        /// </summary>
        /// <param name="step">The step.</param>
        private void UpdateProcessStep(ProcessStep step)
        {
            bool objectUpdated = false;

            if (!string.IsNullOrEmpty(this.shiftsPerWeekNewValue))
            {
                step.ShiftsPerWeek = Converter.StringToNullableDecimal(this.shiftsPerWeekNewValue);
                objectUpdated = true;
            }

            if (this.shiftsPerWeekIncreasePercentage != null)
            {
                step.ShiftsPerWeek = step.ShiftsPerWeek * this.shiftsPerWeekIncreasePercentage;
                objectUpdated = true;
            }

            if (!string.IsNullOrEmpty(this.hoursPerShiftNewValue))
            {
                step.HoursPerShift = Converter.StringToNullableDecimal(this.hoursPerShiftNewValue);
                objectUpdated = true;
            }

            if (this.hoursPerShiftIncreasePercentage != null)
            {
                step.HoursPerShift = step.HoursPerShift * this.hoursPerShiftIncreasePercentage;
                objectUpdated = true;
            }

            if (!string.IsNullOrEmpty(this.productionWeeksPerYearNewValue))
            {
                step.ProductionWeeksPerYear = Converter.StringToNullableDecimal(this.productionWeeksPerYearNewValue);
                objectUpdated = true;
            }

            if (this.productionWeeksPerYearIncreasePercentage != null)
            {
                step.ProductionWeeksPerYear = step.ProductionWeeksPerYear * this.productionWeeksPerYearIncreasePercentage;
                objectUpdated = true;
            }

            if (!string.IsNullOrEmpty(this.productionDaysPerWeekNewValue))
            {
                step.ProductionDaysPerWeek = Converter.StringToNullableDecimal(this.productionDaysPerWeekNewValue);
                objectUpdated = true;
            }

            if (this.productionDaysPerWeekIncreasePercentage != null)
            {
                step.ProductionDaysPerWeek = step.ProductionDaysPerWeek * this.productionDaysPerWeekIncreasePercentage;
                objectUpdated = true;
            }

            if (objectUpdated)
            {
                numberOfUpdatedObjects++;
            }
        }

        /// <summary>
        /// Calculate MaxContentLimit for ShiftsPerWeekNewValue &amp; HoursPerShiftNewValue values
        /// </summary>
        private void CalculateShiftMaxContentLimits()
        {
            decimal val;
            decimal? newShiftsPerWeek = null;
            if (decimal.TryParse(this.ShiftsPerWeekNewValue, out val))
            {
                newShiftsPerWeek = val;
            }

            decimal? currentShiftsPerWeek = null;
            if (decimal.TryParse(this.ShiftsPerWeekCurrentValue, out val))
            {
                currentShiftsPerWeek = val;
            }

            decimal? newHoursPerShift = null;
            if (decimal.TryParse(this.HoursPerShiftNewValue, out val))
            {
                newHoursPerShift = val;
            }

            decimal? currentHoursPerShift = null;
            if (decimal.TryParse(this.HoursPerShiftCurrentValue, out val))
            {
                currentHoursPerShift = val;
            }

            // Set HoursPerShift maximum content limit
            var shiftsPerWeek = newShiftsPerWeek.HasValue
                                    ? newShiftsPerWeek
                                    : currentShiftsPerWeek.HasValue ? currentShiftsPerWeek : null;
            if (shiftsPerWeek.HasValue && shiftsPerWeek > Constants.MinShiftInfoValue
                && shiftsPerWeek <= Constants.MaxTotalHoursAndShifts)
            {
                this.HoursPerShiftMaxContentLimit = decimal.Round(Constants.MaxTotalHoursAndShifts / shiftsPerWeek.Value, 2);
            }
            else
            {
                this.HoursPerShiftMaxContentLimit = Constants.MaxTotalHoursAndShifts;
            }

            // Set ShiftsPerWeek maximum content limit
            var hoursPerShift = newHoursPerShift.HasValue
                                    ? newHoursPerShift
                                    : currentHoursPerShift.HasValue ? currentHoursPerShift : null;
            if (hoursPerShift.HasValue && hoursPerShift > Constants.MinShiftInfoValue
                && hoursPerShift <= Constants.MaxTotalHoursAndShifts)
            {
                this.ShiftsPerWeekMaxContentLimit = decimal.Round(Constants.MaxTotalHoursAndShifts / hoursPerShift.Value, 2);
            }
            else
            {
                this.ShiftsPerWeekMaxContentLimit = Constants.MaxTotalHoursAndShifts;
            }

            // Set ShiftsPerWeekIncreasePercentage and HoursPerShiftIncreasePercentage maximum limits
            if (currentShiftsPerWeek.HasValue && currentHoursPerShift.HasValue
                && currentShiftsPerWeek > Constants.MinShiftInfoValue
                && currentShiftsPerWeek <= Constants.MaxTotalHoursAndShifts
                && currentHoursPerShift > Constants.MinShiftInfoValue
                && currentHoursPerShift <= Constants.MaxTotalHoursAndShifts)
            {
                var maxPercentage = Constants.MaxTotalHoursAndShifts * 100
                                    / (currentShiftsPerWeek.Value * currentHoursPerShift.Value);
                if (this.ShiftsPerWeekIncreasePercentage.HasValue)
                {
                    var hoursLimitPercentage = decimal.Round(maxPercentage / this.ShiftsPerWeekIncreasePercentage.Value, 2);
                    var hoursPerShiftValuePreview = hoursLimitPercentage * hoursPerShift.Value / 100;
                    if (hoursPerShiftValuePreview > Constants.MaxTotalHoursAndShifts)
                    {
                        hoursLimitPercentage = maxPercentage;
                    }

                    if (hoursPerShiftValuePreview < Constants.MinShiftInfoValue)
                    {
                        hoursLimitPercentage = Constants.MinShiftInfoValue;
                    }

                    this.HoursPerShiftIncreasePercentageMaxLimit = hoursLimitPercentage;
                }
                else
                {
                    this.HoursPerShiftIncreasePercentageMaxLimit = decimal.Round(maxPercentage, 2);
                }

                if (this.HoursPerShiftIncreasePercentage.HasValue)
                {
                    var shiftsPerHourLimitPercentage = decimal.Round(maxPercentage / this.HoursPerShiftIncreasePercentage.Value, 2);
                    var shiftsPerHourValuePreview = shiftsPerHourLimitPercentage * shiftsPerWeek.Value / 100;
                    if (shiftsPerHourValuePreview > Constants.MaxTotalHoursAndShifts)
                    {
                        shiftsPerHourLimitPercentage = maxPercentage;
                    }

                    if (shiftsPerHourValuePreview < Constants.MinShiftInfoValue)
                    {
                        shiftsPerHourLimitPercentage = Constants.MinShiftInfoValue;
                    }

                    this.ShiftsPerWeekIncreasePercentageMaxLimit = shiftsPerHourLimitPercentage;
                }
                else
                {
                    this.ShiftsPerWeekIncreasePercentageMaxLimit = decimal.Round(maxPercentage, 2);
                }
            }
            else
            {
                this.ShiftsPerWeekIncreasePercentageMaxLimit = Constants.MaxTotalHoursAndShifts;
                this.HoursPerShiftIncreasePercentageMaxLimit = Constants.MaxTotalHoursAndShifts;
            }
        }

        /// <summary>
        /// Sets the ProductionDaysPerWeekIncreasePercentage and ProductionWeeksPerYearIncreasePercentage maximum limits.
        /// </summary>
        private void SetProductionPercentagesMaxLimits()
        {
            decimal val;
            if (decimal.TryParse(this.ProductionDaysPerWeekCurrentValue, out val) && val != 0)
            {
                this.ProductionDaysPerWeekIncreasePercentageMaxLimit = decimal.Round(Constants.MaxProductionDaysPerWeek * 100 / val, 2);
            }

            if (decimal.TryParse(this.ProductionWeeksPerYearCurrentValue, out val) && val != 0)
            {
                this.ProductionWeeksPerYearIncreasePercentageMaxLimit = decimal.Round(Constants.MaxProductionWeeksPerYear * 100 / val, 2);
            }
        }
    }
}