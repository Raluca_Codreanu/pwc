﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for Mass Update Shift Information
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class MassUpdateOverheadAndMarginViewModel : ViewModel<object, IDataSourceManager>
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The messenger reference
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The window service reference
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// Specifies if the updates are applied on the sub-objects of the current object
        /// </summary>
        private bool applyUpdatesToSubObjects;

        /// <summary>
        /// Counts the number of updated objects
        /// </summary>
        private int numberOfUpdatedObjects = 0;

        /// <summary>
        /// The material OH percent
        /// </summary>
        private decimal? materialOHPercent;

        /// <summary>
        /// The material OH new value
        /// </summary>
        private decimal? materialOHNewValue;

        /// <summary>
        /// The consumable OH percent
        /// </summary>
        private decimal? consumableOHPercent;

        /// <summary>
        /// The consumable OH new value
        /// </summary>
        private decimal? consumableOHNewValue;

        /// <summary>
        /// The commodity OH percent
        /// </summary>
        private decimal? commodityOHPercent;

        /// <summary>
        /// The commodity OH new value
        /// </summary>
        private decimal? commodityOHNewValue;

        /// <summary>
        /// The external work OH percent
        /// </summary>
        private decimal? externalWorkOHPercent;

        /// <summary>
        /// The external work OH new value
        /// </summary>
        private decimal? externalWorkOHNewValue;

        /// <summary>
        /// The manufacturing OH percent
        /// </summary>
        private decimal? manufacturingOHPercent;

        /// <summary>
        /// The manufacturing OH new value
        /// </summary>
        private decimal? manufacturingOHNewValue;

        /// <summary>
        /// The other cost OH percent
        /// </summary>
        private decimal? otherCostOHPercent;

        /// <summary>
        /// The other cost OH new value
        /// </summary>
        private decimal? otherCostOHNewValue;

        /// <summary>
        /// The packaging OH percent
        /// </summary>
        private decimal? packagingOHPercent;

        /// <summary>
        /// The packaging OH new value
        /// </summary>
        private decimal? packagingOHNewValue;

        /// <summary>
        /// The logistic OH percent
        /// </summary>
        private decimal? logisticOHPercent;

        /// <summary>
        /// The logistic OH new value
        /// </summary>
        private decimal? logisticOHNewValue;

        /// <summary>
        /// The sales and admin OH percent
        /// </summary>
        private decimal? salesAndAdminOHPercent;

        /// <summary>
        /// The sales and admin OH new value
        /// </summary>
        private decimal? salesAndAdminOHNewValue;

        /// <summary>
        /// The company surcharge OH percent
        /// </summary>
        private decimal? companySurchargeOHPercent;

        /// <summary>
        /// The company surcharge OH new value
        /// </summary>
        private decimal? companySurchargeOHNewValue;

        /// <summary>
        /// The material margin percent
        /// </summary>
        private decimal? materialMarginPercent;

        /// <summary>
        /// The material margin new value
        /// </summary>
        private decimal? materialMarginNewValue;

        /// <summary>
        /// The consumable margin percent
        /// </summary>
        private decimal? consumableMarginPercent;

        /// <summary>
        /// The consumable margin new value
        /// </summary>
        private decimal? consumableMarginNewValue;

        /// <summary>
        /// The commodity margin percent
        /// </summary>
        private decimal? commodityMarginPercent;

        /// <summary>
        /// The commodity margin new value
        /// </summary>
        private decimal? commodityMarginNewValue;

        /// <summary>
        /// The external work margin percent
        /// </summary>
        private decimal? externalWorkMarginPercent;

        /// <summary>
        /// The external work margin new value
        /// </summary>
        private decimal? externalWorkMarginNewValue;

        /// <summary>
        /// The manufacturing margin percent
        /// </summary>
        private decimal? manufacturingMarginPercent;

        /// <summary>
        /// The manufacturing margin new value
        /// </summary>
        private decimal? manufacturingMarginNewValue;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        #endregion Attributes

        /// <summary>
        /// Initializes a new instance of the <see cref="MassUpdateOverheadAndMarginViewModel"/> class.
        /// </summary>
        /// <param name="messenger">The messenger service.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="unitsService">The units service.</param>
        [ImportingConstructor]
        public MassUpdateOverheadAndMarginViewModel(
            IMessenger messenger,
            IWindowService windowService,
            IUnitsService unitsService)
        {
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("unitsService", unitsService);

            this.messenger = messenger;
            this.windowService = windowService;
            this.unitsService = unitsService;

            this.UpdateCommand = new DelegateCommand(UpdateOH);
        }

        #region Commands

        /// <summary>
        /// Gets the command for the Update button
        /// </summary>
        public ICommand UpdateCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether to apply updates to sub objects.
        /// </summary>
        public bool ApplyUpdatesToSubObjects
        {
            get
            {
                return this.applyUpdatesToSubObjects;
            }

            set
            {
                if (this.applyUpdatesToSubObjects != value)
                {
                    this.applyUpdatesToSubObjects = value;
                    OnPropertyChanged(() => this.ApplyUpdatesToSubObjects);
                }
            }
        }

        /// <summary>
        /// Gets or sets the current overhead settings.
        /// </summary>
        public OverheadSetting CurrentOH { get; set; }

        /// <summary>
        /// Gets or sets the material OH percent.
        /// </summary>
        public decimal? MaterialOHPercent
        {
            get
            {
                return this.materialOHPercent;
            }

            set
            {
                if (this.materialOHPercent != value)
                {
                    this.materialOHPercent = value;
                    OnPropertyChanged(() => this.MaterialOHPercent);

                    if (value != null)
                    {
                        MaterialOHNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the material OH new value.
        /// </summary>
        public decimal? MaterialOHNewValue
        {
            get
            {
                return this.materialOHNewValue;
            }

            set
            {
                if (this.materialOHNewValue != value)
                {
                    this.materialOHNewValue = value;
                    OnPropertyChanged(() => this.MaterialOHNewValue);

                    if (value != null)
                    {
                        MaterialOHPercent = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the consumable OH percent.
        /// </summary>
        public decimal? ConsumableOHPercent
        {
            get
            {
                return this.consumableOHPercent;
            }

            set
            {
                if (this.consumableOHPercent != value)
                {
                    this.consumableOHPercent = value;
                    OnPropertyChanged(() => this.ConsumableOHPercent);

                    if (value != null)
                    {
                        ConsumableOHNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the consumable OH new value.
        /// </summary>
        public decimal? ConsumableOHNewValue
        {
            get
            {
                return this.consumableOHNewValue;
            }

            set
            {
                if (this.consumableOHNewValue != value)
                {
                    this.consumableOHNewValue = value;
                    OnPropertyChanged(() => this.ConsumableOHNewValue);

                    if (value != null)
                    {
                        ConsumableOHPercent = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the commodity OH percent.
        /// </summary>
        public decimal? CommodityOHPercent
        {
            get
            {
                return this.commodityOHPercent;
            }

            set
            {
                if (this.commodityOHPercent != value)
                {
                    this.commodityOHPercent = value;
                    OnPropertyChanged(() => this.CommodityOHPercent);

                    if (value != null)
                    {
                        CommodityOHNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the commodity OH new value.
        /// </summary>
        public decimal? CommodityOHNewValue
        {
            get
            {
                return this.commodityOHNewValue;
            }

            set
            {
                if (this.commodityOHNewValue != value)
                {
                    this.commodityOHNewValue = value;
                    OnPropertyChanged(() => this.CommodityOHNewValue);

                    if (value != null)
                    {
                        CommodityOHPercent = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the external work OH percent.
        /// </summary>
        public decimal? ExternalWorkOHPercent
        {
            get
            {
                return this.externalWorkOHPercent;
            }

            set
            {
                if (this.externalWorkOHPercent != value)
                {
                    this.externalWorkOHPercent = value;
                    OnPropertyChanged(() => this.ExternalWorkOHPercent);

                    if (value != null)
                    {
                        ExternalWorkOHNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the external work OH new value.
        /// </summary>
        public decimal? ExternalWorkOHNewValue
        {
            get
            {
                return this.externalWorkOHNewValue;
            }

            set
            {
                if (this.externalWorkOHNewValue != value)
                {
                    this.externalWorkOHNewValue = value;
                    OnPropertyChanged(() => this.ExternalWorkOHNewValue);

                    if (value != null)
                    {
                        ExternalWorkOHPercent = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the manufacturing OH percent.
        /// </summary>
        public decimal? ManufacturingOHPercent
        {
            get
            {
                return this.manufacturingOHPercent;
            }

            set
            {
                if (this.manufacturingOHPercent != value)
                {
                    this.manufacturingOHPercent = value;
                    OnPropertyChanged(() => this.ManufacturingOHPercent);

                    if (value != null)
                    {
                        ManufacturingOHNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the manufacturing OH new value.
        /// </summary>
        public decimal? ManufacturingOHNewValue
        {
            get
            {
                return this.manufacturingOHNewValue;
            }

            set
            {
                if (this.manufacturingOHNewValue != value)
                {
                    this.manufacturingOHNewValue = value;
                    OnPropertyChanged(() => this.ManufacturingOHNewValue);

                    if (value != null)
                    {
                        ManufacturingOHPercent = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the other cost OH percent.
        /// </summary>
        public decimal? OtherCostOHPercent
        {
            get
            {
                return this.otherCostOHPercent;
            }

            set
            {
                if (this.otherCostOHPercent != value)
                {
                    this.otherCostOHPercent = value;
                    OnPropertyChanged(() => this.OtherCostOHPercent);

                    if (value != null)
                    {
                        OtherCostOHNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the other cost OH new value.
        /// </summary>
        public decimal? OtherCostOHNewValue
        {
            get
            {
                return this.otherCostOHNewValue;
            }

            set
            {
                if (this.otherCostOHNewValue != value)
                {
                    this.otherCostOHNewValue = value;
                    OnPropertyChanged(() => this.OtherCostOHNewValue);

                    if (value != null)
                    {
                        OtherCostOHPercent = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the packaging OH percent.
        /// </summary>
        public decimal? PackagingOHPercent
        {
            get
            {
                return this.packagingOHPercent;
            }

            set
            {
                if (this.packagingOHPercent != value)
                {
                    this.packagingOHPercent = value;
                    OnPropertyChanged(() => this.PackagingOHPercent);

                    if (value != null)
                    {
                        PackagingOHNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the packaging OH new value.
        /// </summary>
        public decimal? PackagingOHNewValue
        {
            get
            {
                return this.packagingOHNewValue;
            }

            set
            {
                if (this.packagingOHNewValue != value)
                {
                    this.packagingOHNewValue = value;
                    OnPropertyChanged(() => this.PackagingOHNewValue);

                    if (value != null)
                    {
                        PackagingOHPercent = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the logistic OH percent.
        /// </summary>
        public decimal? LogisticOHPercent
        {
            get
            {
                return this.logisticOHPercent;
            }

            set
            {
                if (this.logisticOHPercent != value)
                {
                    this.logisticOHPercent = value;
                    OnPropertyChanged(() => this.LogisticOHPercent);

                    if (value != null)
                    {
                        LogisticOHNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the logistic OH new value.
        /// </summary>
        public decimal? LogisticOHNewValue
        {
            get
            {
                return this.logisticOHNewValue;
            }

            set
            {
                if (this.logisticOHNewValue != value)
                {
                    this.logisticOHNewValue = value;
                    OnPropertyChanged(() => this.LogisticOHNewValue);

                    if (value != null)
                    {
                        LogisticOHPercent = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the sales and admin OH percent.
        /// </summary>
        public decimal? SalesAndAdminOHPercent
        {
            get
            {
                return this.salesAndAdminOHPercent;
            }

            set
            {
                if (this.salesAndAdminOHPercent != value)
                {
                    this.salesAndAdminOHPercent = value;
                    OnPropertyChanged(() => this.SalesAndAdminOHPercent);

                    if (value != null)
                    {
                        SalesAndAdminOHNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the sales and admin OH new value.
        /// </summary>
        public decimal? SalesAndAdminOHNewValue
        {
            get
            {
                return this.salesAndAdminOHNewValue;
            }

            set
            {
                if (this.salesAndAdminOHNewValue != value)
                {
                    this.salesAndAdminOHNewValue = value;
                    OnPropertyChanged(() => this.SalesAndAdminOHNewValue);

                    if (value != null)
                    {
                        SalesAndAdminOHPercent = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the company surcharge OH percent.
        /// </summary>
        public decimal? CompanySurchargeOHPercent
        {
            get
            {
                return this.companySurchargeOHPercent;
            }

            set
            {
                if (this.companySurchargeOHPercent != value)
                {
                    this.companySurchargeOHPercent = value;
                    OnPropertyChanged(() => this.CompanySurchargeOHPercent);

                    if (value != null)
                    {
                        CompanySurchargeOHNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the company surcharge OH new value.
        /// </summary>
        public decimal? CompanySurchargeOHNewValue
        {
            get
            {
                return this.companySurchargeOHNewValue;
            }

            set
            {
                if (this.companySurchargeOHNewValue != value)
                {
                    this.companySurchargeOHNewValue = value;
                    OnPropertyChanged(() => this.CompanySurchargeOHNewValue);

                    if (value != null)
                    {
                        CompanySurchargeOHPercent = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the material margin percent.
        /// </summary>
        public decimal? MaterialMarginPercent
        {
            get
            {
                return this.materialMarginPercent;
            }

            set
            {
                if (this.materialMarginPercent != value)
                {
                    this.materialMarginPercent = value;
                    OnPropertyChanged(() => this.MaterialMarginPercent);

                    if (value != null)
                    {
                        MaterialMarginNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the material margin new value.
        /// </summary>
        public decimal? MaterialMarginNewValue
        {
            get
            {
                return this.materialMarginNewValue;
            }

            set
            {
                if (this.materialMarginNewValue != value)
                {
                    this.materialMarginNewValue = value;
                    OnPropertyChanged(() => this.MaterialMarginNewValue);

                    if (value != null)
                    {
                        MaterialMarginPercent = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the consumable margin percent.
        /// </summary>
        public decimal? ConsumableMarginPercent
        {
            get
            {
                return this.consumableMarginPercent;
            }

            set
            {
                if (this.consumableMarginPercent != value)
                {
                    this.consumableMarginPercent = value;
                    OnPropertyChanged(() => this.ConsumableMarginPercent);

                    if (value != null)
                    {
                        ConsumableMarginNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the consumable margin new value.
        /// </summary>
        public decimal? ConsumableMarginNewValue
        {
            get
            {
                return this.consumableMarginNewValue;
            }

            set
            {
                if (this.consumableMarginNewValue != value)
                {
                    this.consumableMarginNewValue = value;
                    OnPropertyChanged(() => this.ConsumableMarginNewValue);

                    if (value != null)
                    {
                        ConsumableMarginPercent = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the commodity margin percent.
        /// </summary>
        public decimal? CommodityMarginPercent
        {
            get
            {
                return this.commodityMarginPercent;
            }

            set
            {
                if (this.commodityMarginPercent != value)
                {
                    this.commodityMarginPercent = value;
                    OnPropertyChanged(() => this.CommodityMarginPercent);

                    if (value != null)
                    {
                        CommodityMarginNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the commodity margin new value.
        /// </summary>
        public decimal? CommodityMarginNewValue
        {
            get
            {
                return this.commodityMarginNewValue;
            }

            set
            {
                if (this.commodityMarginNewValue != value)
                {
                    this.commodityMarginNewValue = value;
                    OnPropertyChanged(() => this.CommodityMarginNewValue);

                    if (value != null)
                    {
                        CommodityMarginPercent = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the external work margin percent.
        /// </summary>
        public decimal? ExternalWorkMarginPercent
        {
            get
            {
                return this.externalWorkMarginPercent;
            }

            set
            {
                if (this.externalWorkMarginPercent != value)
                {
                    this.externalWorkMarginPercent = value;
                    OnPropertyChanged(() => this.ExternalWorkMarginPercent);

                    if (value != null)
                    {
                        ExternalWorkMarginNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the external work margin new value.
        /// </summary>
        public decimal? ExternalWorkMarginNewValue
        {
            get
            {
                return this.externalWorkMarginNewValue;
            }

            set
            {
                if (this.externalWorkMarginNewValue != value)
                {
                    this.externalWorkMarginNewValue = value;
                    OnPropertyChanged(() => this.ExternalWorkMarginNewValue);

                    if (value != null)
                    {
                        ExternalWorkMarginPercent = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the manufacturing margin percent.
        /// </summary>
        public decimal? ManufacturingMarginPercent
        {
            get
            {
                return this.manufacturingMarginPercent;
            }

            set
            {
                if (this.manufacturingMarginPercent != value)
                {
                    this.manufacturingMarginPercent = value;
                    OnPropertyChanged(() => this.ManufacturingMarginPercent);

                    if (value != null)
                    {
                        ManufacturingMarginNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the manufacturing margin new value.
        /// </summary>
        public decimal? ManufacturingMarginNewValue
        {
            get
            {
                return this.manufacturingMarginNewValue;
            }

            set
            {
                if (this.manufacturingMarginNewValue != value)
                {
                    this.manufacturingMarginNewValue = value;
                    OnPropertyChanged(() => this.ManufacturingMarginNewValue);

                    if (value != null)
                    {
                        ManufacturingMarginPercent = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter { get; private set; }

        #endregion Properties

        /// <summary>
        /// Set up the gui based on the input object
        /// </summary>
        protected override void OnModelChanged()
        {
            // initialize the current values
            Project prj = this.Model as Project;
            if (prj != null)
            {
                this.CurrentOH = prj.OverheadSettings;
            }

            Assembly assy = this.Model as Assembly;
            if (assy != null)
            {
                this.CurrentOH = assy.OverheadSettings;
            }

            Part part = this.Model as Part;
            if (part != null)
            {
                this.CurrentOH = part.OverheadSettings;
            }
        }

        /// <summary>
        /// Called when DataSourceManager has changed.
        /// </summary>
        protected override void OnDataSourceManagerChanged()
        {
            base.OnDataSourceManagerChanged();

            this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(this.DataSourceManager);
        }

        /// <summary>
        /// Performs the update operation
        /// </summary>
        private void UpdateOH()
        {
            // set the appropriate message
            string message = this.ApplyUpdatesToSubObjects ? LocalizedResources.MassDataUpdate_ConfirmationWithSubobjects : LocalizedResources.MassDataUpdate_ConfirmationNoSubobjects;
            MessageDialogResult result = this.windowService.MessageDialogService.Show(message, MessageDialogType.YesNo);
            if (result == MessageDialogResult.No)
            {
                return;
            }

            object updatedEntity = null;
            numberOfUpdatedObjects = 0;

            Project project = this.Model as Project;
            if (project != null)
            {
                // reset the counter
                numberOfUpdatedObjects = 0;

                if (this.ApplyUpdatesToSubObjects)
                {
                    // special case for project, must be retrieved
                    // get the project only of the sub-objects must be updated
                    Task.Factory.StartNew(() =>
                    {
                        this.messenger.Send<NotificationMessage>(new NotificationMessage("UpdateMassData"));
                        Project fullProject = this.DataSourceManager.ProjectRepository.GetProjectFull(project.Guid);

                        if (fullProject != null)
                        {
                            this.UpdateProject(fullProject);
                        }

                        this.DataSourceManager.SaveChanges();
                    })
                    .ContinueWith(
                        (task) =>
                        {
                            this.messenger.Send<NotificationMessage>(new NotificationMessage("UpdateMassDataFinished"));
                            if (task.Exception != null)
                            {
                                Exception error = task.Exception.InnerException;
                                log.ErrorException("Project loading failed.", error);
                                this.windowService.MessageDialogService.Show(error);
                            }
                            else
                            {
                                // refresh the context of the main view
                                EntityChangedMessage msg = new EntityChangedMessage(Notification.MyProjectsEntityChanged);
                                msg.Entity = project;
                                msg.Parent = null;
                                msg.ChangeType = EntityChangeType.RefreshData;
                                this.messenger.Send(msg);

                                this.windowService.MessageDialogService.Show(LocalizedResources.MassDataUpdate_NumberOfUpdatedObjects + numberOfUpdatedObjects, MessageDialogType.Info);
                                this.messenger.Send<NotificationMessage>(new NotificationMessage("CloseMassDataUpdateWindow"));
                            }
                        },
                        System.Threading.Tasks.TaskScheduler.FromCurrentSynchronizationContext());

                    return;
                }
                else
                {
                    // if only the project must be updated, do not load it fully.
                    // perform the update and save the changes in the db.
                    this.UpdateOverheadSetting(project.OverheadSettings);
                    updatedEntity = project;
                }
            }
            else
            {
                Assembly assembly = this.Model as Assembly;
                if (assembly != null)
                {
                    this.UpdateAssembly(assembly);
                    updatedEntity = assembly;
                }
                else
                {
                    Part part = this.Model as Part;
                    if (part != null)
                    {
                        this.UpdatePart(part);
                        updatedEntity = part;
                    }
                }
            }

            if (this.DataSourceManager != null)
            {
                this.DataSourceManager.SaveChanges();
            }

            if (updatedEntity != null)
            {
                EntityChangedMessage msg = new EntityChangedMessage(Notification.MyProjectsEntityChanged);
                msg.Entity = updatedEntity;
                msg.Parent = null;
                msg.ChangeType = EntityChangeType.RefreshData;
                this.messenger.Send(msg);
            }

            this.windowService.MessageDialogService.Show(LocalizedResources.MassDataUpdate_NumberOfUpdatedObjects + numberOfUpdatedObjects, MessageDialogType.Info);
            this.messenger.Send<NotificationMessage>(new NotificationMessage("CloseMassDataUpdateWindow"));
        }

        /// <summary>
        /// Update the fields in a project
        /// </summary>
        /// <param name="project">The project.</param>
        private void UpdateProject(Project project)
        {
            UpdateOverheadSetting(project.OverheadSettings);

            // update sub-objects if option is selected
            if (this.ApplyUpdatesToSubObjects)
            {
                foreach (Assembly assembly in project.Assemblies.Where(p => !p.IsDeleted))
                {
                    UpdateAssembly(assembly);
                }

                foreach (Part part in project.Parts.Where(p => !p.IsDeleted))
                {
                    UpdatePart(part);
                }
            }
        }

        /// <summary>
        /// Update the fields in an assembly
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        private void UpdateAssembly(Assembly assembly)
        {
            UpdateOverheadSetting(assembly.OverheadSettings);

            // update sub-objects if option is selected
            if (this.ApplyUpdatesToSubObjects)
            {
                foreach (Assembly subAssembly in assembly.Subassemblies.Where(p => !p.IsDeleted))
                {
                    UpdateAssembly(subAssembly);
                }

                foreach (Part part in assembly.Parts.Where(p => !p.IsDeleted))
                {
                    UpdatePart(part);
                }
            }
        }

        /// <summary>
        /// Updates the part.
        /// </summary>
        /// <param name="part">The part.</param>
        private void UpdatePart(Part part)
        {
            if (part != null)
            {
                this.UpdateOverheadSetting(part.OverheadSettings);

                if (ApplyUpdatesToSubObjects && part.RawPart != null && !part.RawPart.IsDeleted)
                {
                    this.UpdatePart(part.RawPart);
                }
            }
        }

        /// <summary>
        /// Updates one Overhead setting item
        /// </summary>
        /// <param name="overheadSettings">The overhead settings.</param>
        private void UpdateOverheadSetting(OverheadSetting overheadSettings)
        {
            if (overheadSettings == null)
            {
                return;
            }

            bool objectUpdated = false;

            if (this.materialOHNewValue != null)
            {
                overheadSettings.MaterialOverhead = this.materialOHNewValue.Value;
                objectUpdated = true;
            }

            if (this.materialOHPercent != null)
            {
                overheadSettings.MaterialOverhead = overheadSettings.MaterialOverhead * this.materialOHPercent.Value;
                objectUpdated = true;
            }

            if (this.consumableOHNewValue != null)
            {
                overheadSettings.ConsumableOverhead = this.consumableOHNewValue.Value;
                objectUpdated = true;
            }

            if (this.consumableOHPercent != null)
            {
                overheadSettings.ConsumableOverhead = overheadSettings.ConsumableOverhead * this.consumableOHPercent.Value;
                objectUpdated = true;
            }

            if (this.commodityOHNewValue != null)
            {
                overheadSettings.CommodityOverhead = this.commodityOHNewValue.Value;
                objectUpdated = true;
            }

            if (this.commodityOHPercent != null)
            {
                overheadSettings.CommodityOverhead = overheadSettings.CommodityOverhead * this.commodityOHPercent.Value;
                objectUpdated = true;
            }

            if (this.externalWorkOHNewValue != null)
            {
                overheadSettings.ExternalWorkOverhead = this.externalWorkOHNewValue.Value;
                objectUpdated = true;
            }

            if (this.externalWorkOHPercent != null)
            {
                overheadSettings.ExternalWorkOverhead = overheadSettings.ExternalWorkOverhead * this.externalWorkOHPercent.Value;
                objectUpdated = true;
            }

            if (this.manufacturingOHNewValue != null)
            {
                overheadSettings.ManufacturingOverhead = this.manufacturingOHNewValue.Value;
                objectUpdated = true;
            }

            if (this.manufacturingOHPercent != null)
            {
                overheadSettings.ManufacturingOverhead = overheadSettings.ManufacturingOverhead * this.manufacturingOHPercent.Value;
                objectUpdated = true;
            }

            if (this.otherCostOHNewValue != null)
            {
                overheadSettings.OtherCostOHValue = this.otherCostOHNewValue.Value;
                objectUpdated = true;
            }

            if (this.otherCostOHPercent != null)
            {
                overheadSettings.OtherCostOHValue = overheadSettings.OtherCostOHValue * this.otherCostOHPercent.Value;
                objectUpdated = true;
            }

            if (this.packagingOHNewValue != null)
            {
                overheadSettings.PackagingOHValue = this.packagingOHNewValue.Value;
                objectUpdated = true;
            }

            if (this.packagingOHPercent != null)
            {
                overheadSettings.PackagingOHValue = overheadSettings.PackagingOHValue * this.packagingOHPercent.Value;
                objectUpdated = true;
            }

            if (this.logisticOHNewValue != null)
            {
                overheadSettings.LogisticOHValue = this.logisticOHNewValue.Value;
                objectUpdated = true;
            }

            if (this.logisticOHPercent != null)
            {
                overheadSettings.LogisticOHValue = overheadSettings.LogisticOHValue * this.logisticOHPercent.Value;
                objectUpdated = true;
            }

            if (this.salesAndAdminOHNewValue != null)
            {
                overheadSettings.SalesAndAdministrationOHValue = this.salesAndAdminOHNewValue.Value;
                objectUpdated = true;
            }

            if (this.salesAndAdminOHPercent != null)
            {
                overheadSettings.SalesAndAdministrationOHValue = overheadSettings.SalesAndAdministrationOHValue * this.salesAndAdminOHPercent.Value;
                objectUpdated = true;
            }

            if (this.companySurchargeOHNewValue != null)
            {
                overheadSettings.CompanySurchargeOverhead = this.companySurchargeOHNewValue.Value;
                objectUpdated = true;
            }

            if (this.companySurchargeOHPercent != null)
            {
                overheadSettings.CompanySurchargeOverhead = overheadSettings.CompanySurchargeOverhead * this.companySurchargeOHPercent.Value;
                objectUpdated = true;
            }

            if (this.materialMarginNewValue != null)
            {
                overheadSettings.MaterialMargin = this.materialMarginNewValue.Value;
                objectUpdated = true;
            }

            if (this.materialMarginPercent != null)
            {
                overheadSettings.MaterialMargin = overheadSettings.MaterialMargin * this.materialMarginPercent.Value;
                objectUpdated = true;
            }

            if (this.consumableMarginNewValue != null)
            {
                overheadSettings.ConsumableMargin = this.consumableMarginNewValue.Value;
                objectUpdated = true;
            }

            if (this.consumableMarginPercent != null)
            {
                overheadSettings.ConsumableMargin = overheadSettings.ConsumableMargin * this.consumableMarginPercent.Value;
                objectUpdated = true;
            }

            if (this.commodityMarginNewValue != null)
            {
                overheadSettings.CommodityMargin = this.commodityMarginNewValue.Value;
                objectUpdated = true;
            }

            if (this.commodityMarginPercent != null)
            {
                overheadSettings.CommodityMargin = overheadSettings.CommodityMargin * this.commodityMarginPercent.Value;
                objectUpdated = true;
            }

            if (this.externalWorkMarginNewValue != null)
            {
                overheadSettings.ExternalWorkMargin = this.externalWorkMarginNewValue.Value;
                objectUpdated = true;
            }

            if (this.externalWorkMarginPercent != null)
            {
                overheadSettings.ExternalWorkMargin = overheadSettings.ExternalWorkMargin * this.externalWorkMarginPercent.Value;
                objectUpdated = true;
            }

            if (this.manufacturingMarginNewValue != null)
            {
                overheadSettings.ManufacturingMargin = this.manufacturingMarginNewValue.Value;
                objectUpdated = true;
            }

            if (this.manufacturingMarginPercent != null)
            {
                overheadSettings.ManufacturingMargin = overheadSettings.ManufacturingMargin * this.manufacturingMarginPercent.Value;
                objectUpdated = true;
            }

            if (objectUpdated)
            {
                numberOfUpdatedObjects++;
            }
        }
    }
}