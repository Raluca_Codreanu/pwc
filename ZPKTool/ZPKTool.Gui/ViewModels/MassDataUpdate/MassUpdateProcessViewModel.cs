﻿using System.ComponentModel.Composition;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for Mass Update Process
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class MassUpdateProcessViewModel : ViewModel<object, IDataSourceManager>
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The messenger reference
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The window service reference
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        /// <summary>
        /// Specifies if the updates are applied on the sub-objects of the current object
        /// </summary>
        private bool applyUpdatesToSubObjects;

        /// <summary>
        /// Counts the number of updated entities
        /// </summary>
        private int numberOfUpdatedObjects = 0;

        /// <summary>
        /// Current value for process time.
        /// </summary>
        private decimal? processTimeCurrentValue;

        /// <summary>
        /// The process time unit current value.
        /// </summary>
        private MeasurementUnit processTimeUnitCurrentValue;

        /// <summary>
        /// Current value for cycle time.
        /// </summary>
        private decimal? cycleTimeCurrentValue;

        /// <summary>
        /// The cycle time unit current value.
        /// </summary>
        private MeasurementUnit cycleTimeUnitCurrentValue;

        /// <summary>
        /// Current value for parts per cycle.
        /// </summary>
        private int? partsPerCycleCurrentValue;

        /// <summary>
        /// Current value for scrap amount.
        /// </summary>
        private decimal? scrapAmountCurrentValue;

        /// <summary>
        /// Current value for setups per batch,
        /// </summary>
        private int? setupsPerBatchCurrentValue;

        /// <summary>
        /// Current value for max downtime.
        /// </summary>
        private decimal? maxDowntimeCurrentValue;

        /// <summary>
        /// The max downtime unit current value.
        /// </summary>
        private MeasurementUnit maxDowntimeUnitCurrentValue;

        /// <summary>
        /// Current value for shifts per week
        /// </summary>
        private decimal? setupTimeCurrentValue;

        /// <summary>
        /// The setup time unit current value.
        /// </summary>
        private MeasurementUnit setupTimeUnitCurrentValue;

        /// <summary>
        /// Current value for batch size.
        /// </summary>
        private int? batchSizeCurrentValue;

        /// <summary>
        /// Current value for manufacturing overhead.
        /// </summary>
        private decimal? manufacturingOverheadCurrentValue;

        /// <summary>
        /// Increase Percentage value for process time.
        /// </summary>
        private decimal? processTimeIncreasePercentage;

        /// <summary>
        /// Increase Percentage value for cycle time.
        /// </summary>
        private decimal? cycleTimeIncreasePercentage;

        /// <summary>
        /// Increase Percentage value for scrap amount.
        /// </summary>
        private decimal? scrapAmountIncreasePercentage;

        /// <summary>
        /// Increase Percentage value for max downtime.
        /// </summary>
        private decimal? maxDowntimeIncreasePercentage;

        /// <summary>
        /// Increase Percentage value for setup time.
        /// </summary>
        private decimal? setupTimeIncreasePercentage;

        /// <summary>
        /// Increase Percentage value for manufacturing overhead.
        /// </summary>
        private decimal? manufacturingOverheadIncreasePercentage;

        /// <summary>
        /// The new value for process time.
        /// </summary>
        private decimal? processTimeNewValue;

        /// <summary>
        /// The process time unit new value.
        /// </summary>
        private MeasurementUnit processTimeUnitNewValue;

        /// <summary>
        /// New value for cycle time.
        /// </summary>
        private decimal? cycleTimeNewValue;

        /// <summary>
        /// The cycle time unit new value.
        /// </summary>
        private MeasurementUnit cycleTimeUnitNewValue;

        /// <summary>
        /// The parts per cycle new value.
        /// </summary>
        private int? partsPerCycleNewValue;

        /// <summary>
        /// The scrap amount new value.
        /// </summary>
        private decimal? scrapAmountNewValue;

        /// <summary>
        /// The setups per batch new value.
        /// </summary>
        private int? setupsPerBatchNewValue;

        /// <summary>
        /// The max downtime new value.
        /// </summary>
        private decimal? maxDowntimeNewValue;

        /// <summary>
        /// The max downtime unit new value.
        /// </summary>
        private MeasurementUnit maxDowntimeUnitNewValue;

        /// <summary>
        /// The setup time new value.
        /// </summary>
        private decimal? setupTimeNewValue;

        /// <summary>
        /// The setup time unit new value.
        /// </summary>
        private MeasurementUnit setupTimeUnitNewValue;

        /// <summary>
        /// The batch size new value.
        /// </summary>
        private int? batchSizeNewValue;

        /// <summary>
        /// The manufacturing overhead new value.
        /// </summary>
        private decimal? manufacturingOverheadNewValue;

        /// <summary>
        /// A value indicating whether the current model is process step or not.
        /// </summary>
        private bool isProcessStep;

        #endregion Attributes

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MassUpdateProcessViewModel"/> class.
        /// </summary>
        /// <param name="messenger">The messenger service.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="unitsService">The units service.</param>
        [ImportingConstructor]
        public MassUpdateProcessViewModel(
            IMessenger messenger,
            IWindowService windowService,
            IUnitsService unitsService)
        {
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("unitsService", unitsService);

            this.messenger = messenger;
            this.windowService = windowService;
            this.unitsService = unitsService;

            this.UpdateCommand = new DelegateCommand(this.UpdateProcessInformation);
        }

        #endregion Constructor

        #region Commands

        /// <summary>
        /// Gets the command for the Update button
        /// </summary>
        public ICommand UpdateCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets or sets the process time current value.
        /// </summary>
        public decimal? ProcessTimeCurrentValue
        {
            get
            {
                return this.processTimeCurrentValue;
            }

            set
            {
                if (this.processTimeCurrentValue != value)
                {
                    this.processTimeCurrentValue = value;
                    OnPropertyChanged(() => this.ProcessTimeCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the process time unit current value.
        /// </summary>
        public MeasurementUnit ProcessTimeUnitCurrentValue
        {
            get
            {
                return this.processTimeUnitCurrentValue;
            }

            set
            {
                if (this.processTimeUnitCurrentValue != value)
                {
                    this.processTimeUnitCurrentValue = value;
                    OnPropertyChanged(() => this.ProcessTimeUnitCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the cycle time current value.
        /// </summary>
        public decimal? CycleTimeCurrentValue
        {
            get
            {
                return this.cycleTimeCurrentValue;
            }

            set
            {
                if (this.cycleTimeCurrentValue != value)
                {
                    this.cycleTimeCurrentValue = value;
                    OnPropertyChanged(() => this.CycleTimeCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the cycle time unit current value.
        /// </summary>
        public MeasurementUnit CycleTimeUnitCurrentValue
        {
            get
            {
                return this.cycleTimeUnitCurrentValue;
            }

            set
            {
                if (this.cycleTimeUnitCurrentValue != value)
                {
                    this.cycleTimeUnitCurrentValue = value;
                    OnPropertyChanged(() => this.CycleTimeUnitCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the parts per cycle current value.
        /// </summary>
        public int? PartsPerCycleCurrentValue
        {
            get
            {
                return this.partsPerCycleCurrentValue;
            }

            set
            {
                if (this.partsPerCycleCurrentValue != value)
                {
                    this.partsPerCycleCurrentValue = value;
                    OnPropertyChanged(() => this.PartsPerCycleCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the setups per batch current value.
        /// </summary>
        public int? SetupsPerBatchCurrentValue
        {
            get
            {
                return this.setupsPerBatchCurrentValue;
            }

            set
            {
                if (this.setupsPerBatchCurrentValue != value)
                {
                    this.setupsPerBatchCurrentValue = value;
                    OnPropertyChanged(() => this.SetupsPerBatchCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the max downtime current value.
        /// </summary>
        public decimal? MaxDowntimeCurrentValue
        {
            get
            {
                return this.maxDowntimeCurrentValue;
            }

            set
            {
                if (this.maxDowntimeCurrentValue != value)
                {
                    this.maxDowntimeCurrentValue = value;
                    OnPropertyChanged(() => this.MaxDowntimeCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the max downtime unit current value.
        /// </summary>
        public MeasurementUnit MaxDowntimeUnitCurrentValue
        {
            get
            {
                return this.maxDowntimeUnitCurrentValue;
            }

            set
            {
                if (this.maxDowntimeUnitCurrentValue != value)
                {
                    this.maxDowntimeUnitCurrentValue = value;
                    OnPropertyChanged(() => this.MaxDowntimeUnitCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the setup time current value.
        /// </summary>
        public decimal? SetupTimeCurrentValue
        {
            get
            {
                return this.setupTimeCurrentValue;
            }

            set
            {
                if (this.setupTimeCurrentValue != value)
                {
                    this.setupTimeCurrentValue = value;
                    OnPropertyChanged(() => this.SetupTimeCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the setup time unit current value.
        /// </summary>
        public MeasurementUnit SetupTimeUnitCurrentValue
        {
            get
            {
                return this.setupTimeUnitCurrentValue;
            }

            set
            {
                if (this.setupTimeUnitCurrentValue != value)
                {
                    this.setupTimeUnitCurrentValue = value;
                    OnPropertyChanged(() => this.SetupTimeUnitCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the batch size current value.
        /// </summary>
        public int? BatchSizeCurrentValue
        {
            get
            {
                return this.batchSizeCurrentValue;
            }

            set
            {
                if (this.batchSizeCurrentValue != value)
                {
                    this.batchSizeCurrentValue = value;
                    OnPropertyChanged(() => this.BatchSizeCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the manufacturing overhead current value.
        /// </summary>
        public decimal? ManufacturingOverheadCurrentValue
        {
            get
            {
                return this.manufacturingOverheadCurrentValue;
            }

            set
            {
                if (this.manufacturingOverheadCurrentValue != value)
                {
                    this.manufacturingOverheadCurrentValue = value;
                    OnPropertyChanged(() => this.ManufacturingOverheadCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the scrap amount current value.
        /// </summary>
        public decimal? ScrapAmountCurrentValue
        {
            get
            {
                return this.scrapAmountCurrentValue;
            }

            set
            {
                if (this.scrapAmountCurrentValue != value)
                {
                    this.scrapAmountCurrentValue = value;
                    OnPropertyChanged(() => this.ScrapAmountCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the process time increase percentage.
        /// </summary>
        public decimal? ProcessTimeIncreasePercentage
        {
            get
            {
                return this.processTimeIncreasePercentage;
            }

            set
            {
                if (this.processTimeIncreasePercentage != value)
                {
                    this.processTimeIncreasePercentage = value;
                    OnPropertyChanged(() => this.ProcessTimeIncreasePercentage);

                    if (value != null)
                    {
                        this.ProcessTimeNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the cycle time increase percentage.
        /// </summary>
        public decimal? CycleTimeIncreasePercentage
        {
            get
            {
                return this.cycleTimeIncreasePercentage;
            }

            set
            {
                if (this.cycleTimeIncreasePercentage != value)
                {
                    this.cycleTimeIncreasePercentage = value;
                    OnPropertyChanged(() => this.CycleTimeIncreasePercentage);

                    if (value != null)
                    {
                        this.CycleTimeNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the max downtime increase percentage.
        /// </summary>
        public decimal? MaxDowntimeIncreasePercentage
        {
            get
            {
                return this.maxDowntimeIncreasePercentage;
            }

            set
            {
                if (this.maxDowntimeIncreasePercentage != value)
                {
                    this.maxDowntimeIncreasePercentage = value;
                    OnPropertyChanged(() => this.MaxDowntimeIncreasePercentage);

                    if (value != null)
                    {
                        this.MaxDowntimeNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the setup time increase percentage.
        /// </summary>
        public decimal? SetupTimeIncreasePercentage
        {
            get
            {
                return this.setupTimeIncreasePercentage;
            }

            set
            {
                if (this.setupTimeIncreasePercentage != value)
                {
                    this.setupTimeIncreasePercentage = value;
                    OnPropertyChanged(() => this.SetupTimeIncreasePercentage);

                    if (value != null)
                    {
                        this.SetupTimeNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the manufacturing overhead increase percentage.
        /// </summary>
        public decimal? ManufacturingOverheadIncreasePercentage
        {
            get
            {
                return this.manufacturingOverheadIncreasePercentage;
            }

            set
            {
                if (this.manufacturingOverheadIncreasePercentage != value)
                {
                    this.manufacturingOverheadIncreasePercentage = value;
                    OnPropertyChanged(() => this.ManufacturingOverheadIncreasePercentage);

                    if (value != null)
                    {
                        this.ManufacturingOverheadNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the scrap amount increase percentage.
        /// </summary>
        public decimal? ScrapAmountIncreasePercentage
        {
            get
            {
                return this.scrapAmountIncreasePercentage;
            }

            set
            {
                if (this.scrapAmountIncreasePercentage != value)
                {
                    this.scrapAmountIncreasePercentage = value;
                    OnPropertyChanged(() => this.ScrapAmountIncreasePercentage);

                    if (value != null)
                    {
                        this.ScrapAmountNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the process time new value.
        /// </summary>
        public decimal? ProcessTimeNewValue
        {
            get
            {
                return this.processTimeNewValue;
            }

            set
            {
                if (this.processTimeNewValue != value)
                {
                    this.processTimeNewValue = value;
                    OnPropertyChanged(() => this.ProcessTimeNewValue);

                    if (value != null)
                    {
                        this.ProcessTimeIncreasePercentage = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the process time unit new value.
        /// </summary>
        public MeasurementUnit ProcessTimeUnitNewValue
        {
            get
            {
                return this.processTimeUnitNewValue;
            }

            set
            {
                if (this.processTimeUnitNewValue != value)
                {
                    this.processTimeUnitNewValue = value;
                    OnPropertyChanged(() => this.ProcessTimeUnitNewValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the cycle time new value.
        /// </summary>
        public decimal? CycleTimeNewValue
        {
            get
            {
                return this.cycleTimeNewValue;
            }

            set
            {
                if (this.cycleTimeNewValue != value)
                {
                    this.cycleTimeNewValue = value;
                    OnPropertyChanged(() => this.CycleTimeNewValue);

                    if (value != null)
                    {
                        this.CycleTimeIncreasePercentage = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the cycle time unit new value.
        /// </summary>
        public MeasurementUnit CycleTimeUnitNewValue
        {
            get
            {
                return this.cycleTimeUnitNewValue;
            }

            set
            {
                if (this.cycleTimeUnitNewValue != value)
                {
                    this.cycleTimeUnitNewValue = value;
                    OnPropertyChanged(() => this.CycleTimeUnitNewValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the parts per cycle new value.
        /// </summary>
        public int? PartsPerCycleNewValue
        {
            get
            {
                return this.partsPerCycleNewValue;
            }

            set
            {
                if (this.partsPerCycleNewValue != value)
                {
                    this.partsPerCycleNewValue = value;
                    OnPropertyChanged(() => this.PartsPerCycleNewValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the setups per batch new value.
        /// </summary>
        public int? SetupsPerBatchNewValue
        {
            get
            {
                return this.setupsPerBatchNewValue;
            }

            set
            {
                if (this.setupsPerBatchNewValue != value)
                {
                    this.setupsPerBatchNewValue = value;
                    OnPropertyChanged(() => this.SetupsPerBatchNewValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the max downtime new value.
        /// </summary>
        public decimal? MaxDowntimeNewValue
        {
            get
            {
                return this.maxDowntimeNewValue;
            }

            set
            {
                if (this.maxDowntimeNewValue != value)
                {
                    this.maxDowntimeNewValue = value;
                    OnPropertyChanged(() => this.MaxDowntimeNewValue);

                    if (value != null)
                    {
                        this.MaxDowntimeIncreasePercentage = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the max downtime unit new value.
        /// </summary>
        public MeasurementUnit MaxDowntimeUnitNewValue
        {
            get
            {
                return this.maxDowntimeUnitNewValue;
            }

            set
            {
                if (this.maxDowntimeUnitNewValue != value)
                {
                    this.maxDowntimeUnitNewValue = value;
                    OnPropertyChanged(() => this.MaxDowntimeUnitNewValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the setup time new value.
        /// </summary>
        public decimal? SetupTimeNewValue
        {
            get
            {
                return this.setupTimeNewValue;
            }

            set
            {
                if (this.setupTimeNewValue != value)
                {
                    this.setupTimeNewValue = value;
                    OnPropertyChanged(() => this.SetupTimeNewValue);

                    if (value != null)
                    {
                        this.SetupTimeIncreasePercentage = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the setup time unit new value.
        /// </summary>
        public MeasurementUnit SetupTimeUnitNewValue
        {
            get
            {
                return this.setupTimeUnitNewValue;
            }

            set
            {
                if (this.setupTimeUnitNewValue != value)
                {
                    this.setupTimeUnitNewValue = value;
                    OnPropertyChanged(() => this.SetupTimeUnitNewValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the batch size new value.
        /// </summary>
        public int? BatchSizeNewValue
        {
            get
            {
                return this.batchSizeNewValue;
            }

            set
            {
                if (this.batchSizeNewValue != value)
                {
                    this.batchSizeNewValue = value;
                    OnPropertyChanged(() => this.BatchSizeNewValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the manufacturing overhead new value.
        /// </summary>
        public decimal? ManufacturingOverheadNewValue
        {
            get
            {
                return this.manufacturingOverheadNewValue;
            }

            set
            {
                if (this.manufacturingOverheadNewValue != value)
                {
                    this.manufacturingOverheadNewValue = value;
                    OnPropertyChanged(() => this.ManufacturingOverheadNewValue);

                    if (value != null)
                    {
                        this.ManufacturingOverheadIncreasePercentage = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the scrap amount new value.
        /// </summary>
        public decimal? ScrapAmountNewValue
        {
            get
            {
                return this.scrapAmountNewValue;
            }

            set
            {
                if (this.scrapAmountNewValue != value)
                {
                    this.scrapAmountNewValue = value;
                    OnPropertyChanged(() => this.ScrapAmountNewValue);

                    if (value != null)
                    {
                        this.ScrapAmountIncreasePercentage = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the update is applied to the sub-objects or not
        /// </summary>
        public bool ApplyUpdatesToSubObjects
        {
            get
            {
                return this.applyUpdatesToSubObjects;
            }

            set
            {
                if (this.applyUpdatesToSubObjects != value)
                {
                    this.applyUpdatesToSubObjects = value;
                    OnPropertyChanged(() => this.ApplyUpdatesToSubObjects);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the current model is process step or not.
        /// </summary>
        public bool IsProcessStep
        {
            get
            {
                return this.isProcessStep;
            }

            set
            {
                if (this.isProcessStep != value)
                {
                    this.isProcessStep = value;
                    OnPropertyChanged(() => this.IsProcessStep);
                }
            }
        }

        /// <summary>
        /// Gets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether the project model is updating or not.
        /// </summary>
        public bool IsProjectUpdating { get; set; }

        #endregion Properties

        #region Model handling

        /// <summary>
        /// Set up the gui based on the input object.
        /// </summary>
        protected override void OnModelChanged()
        {
            // Load process Information current values from selected ProcessStep.
            var step = this.Model as ProcessStep;
            if (step != null)
            {
                this.ProcessTimeCurrentValue = step.ProcessTime;
                this.CycleTimeCurrentValue = step.CycleTime;
                this.PartsPerCycleCurrentValue = step.PartsPerCycle;
                this.ScrapAmountCurrentValue = step.ScrapAmount;
                this.SetupsPerBatchCurrentValue = step.SetupsPerBatch;
                this.MaxDowntimeCurrentValue = step.MaxDownTime;
                this.SetupTimeCurrentValue = step.SetupTime;
                this.BatchSizeCurrentValue = step.BatchSize;
                this.ManufacturingOverheadCurrentValue = step.ManufacturingOverhead;

                this.ProcessTimeUnitCurrentValue = step.ProcessTimeUnit;
                this.CycleTimeUnitCurrentValue = step.CycleTimeUnit;
                this.MaxDowntimeUnitCurrentValue = step.MaxDownTimeUnit;
                this.SetupTimeUnitCurrentValue = step.SetupTimeUnit;
            }

            this.IsProcessStep = step != null;
        }

        /// <summary>
        /// Called when DataSourceManager has changed.
        /// </summary>
        protected override void OnDataSourceManagerChanged()
        {
            base.OnDataSourceManagerChanged();
            this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(this.DataSourceManager);

            // Initialize the default measurement units.
            var measurementUnit = this.MeasurementUnitsAdapter.GetBaseMeasurementUnit(MeasurementUnitType.Time);
            this.ProcessTimeUnitNewValue = measurementUnit;
            this.CycleTimeUnitNewValue = measurementUnit;

            measurementUnit = this.MeasurementUnitsAdapter.GetMeasurementUnits(MeasurementUnitType.Time).FirstOrDefault(u => u.ScaleFactor == 60m);
            this.MaxDowntimeUnitNewValue = measurementUnit;
            this.SetupTimeUnitNewValue = measurementUnit;
        }

        #endregion Model handling

        #region Methods

        /// <summary>
        /// Perform the update operations.
        /// </summary>
        private void UpdateProcessInformation()
        {
            // Set the appropriate message.
            var message = this.ApplyUpdatesToSubObjects ? LocalizedResources.MassDataUpdate_ConfirmationWithSubobjects : LocalizedResources.MassDataUpdate_ConfirmationNoSubobjects;
            var result = this.windowService.MessageDialogService.Show(message, MessageDialogType.YesNo);
            if (result == MessageDialogResult.No)
            {
                return;
            }

            object updatedEntity = null;
            numberOfUpdatedObjects = 0;

            var project = this.Model as Project;
            if (project != null && this.ApplyUpdatesToSubObjects)
            {
                this.IsProjectUpdating = true;

                // Special case for project, must be retrieved on a new context.
                Task.Factory.StartNew(() =>
                {
                    this.messenger.Send(new NotificationMessage("UpdateMassData"));
                    var fullProject = this.DataSourceManager.ProjectRepository.GetProjectFull(project.Guid);

                    if (fullProject != null)
                    {
                        this.UpdateProject(fullProject);
                    }

                    this.DataSourceManager.SaveChanges();
                })
                .ContinueWith(
                    (task) =>
                    {
                        this.IsProjectUpdating = false;
                        this.messenger.Send(new NotificationMessage("UpdateMassDataFinished"));
                        if (task.Exception != null)
                        {
                            var error = task.Exception.InnerException;
                            log.ErrorException("Project loading failed.", error);
                            this.windowService.MessageDialogService.Show(error);
                        }

                        // Refresh the context of the main view.
                        var notificationMessage = new EntityChangedMessage(Notification.MyProjectsEntityChanged)
                        {
                            ChangeType = EntityChangeType.RefreshData,
                            Entity = project
                        };
                        this.messenger.Send(notificationMessage);

                        this.windowService.MessageDialogService.Show(LocalizedResources.MassDataUpdate_NumberOfUpdatedObjects + numberOfUpdatedObjects, MessageDialogType.Info);
                        this.messenger.Send(new NotificationMessage("CloseMassDataUpdateWindow"));
                    },
                    TaskScheduler.FromCurrentSynchronizationContext());

                return;
            }

            this.DataSourceManager.RevertChanges();

            if (project != null)
            {
                this.UpdateProject(project);
                updatedEntity = project;
            }
            else
            {
                var assembly = this.Model as Assembly;
                if (assembly != null)
                {
                    this.UpdateAssembly(assembly);
                    updatedEntity = assembly;
                }
                else
                {
                    var part = this.Model as Part;
                    if (part != null)
                    {
                        this.UpdatePart(part);
                        updatedEntity = part;
                    }
                    else
                    {
                        var process = this.Model as Process;
                        if (process != null)
                        {
                            foreach (var ps in process.Steps)
                            {
                                this.UpdateProcessStep(ps);
                            }

                            updatedEntity = process;
                        }
                        else
                        {
                            var step = this.Model as ProcessStep;
                            if (step != null)
                            {
                                this.UpdateProcessStep(step);
                                updatedEntity = step;
                            }
                        }
                    }
                }
            }

            if (this.DataSourceManager != null)
            {
                this.DataSourceManager.SaveChanges();
            }

            if (updatedEntity != null)
            {
                var msg = new EntityChangedMessage(Notification.MyProjectsEntityChanged)
                {
                    Entity = updatedEntity,
                    Parent = null,
                    ChangeType = EntityChangeType.RefreshData
                };
                this.messenger.Send(msg);
            }

            this.windowService.MessageDialogService.Show(LocalizedResources.MassDataUpdate_NumberOfUpdatedObjects + numberOfUpdatedObjects, MessageDialogType.Info);
            this.messenger.Send(new NotificationMessage("CloseMassDataUpdateWindow"));
        }

        /// <summary>
        /// Update the fields in a project
        /// </summary>
        /// <param name="project">The project.</param>
        private void UpdateProject(Project project)
        {
            if (!this.ApplyUpdatesToSubObjects)
            {
                return;
            }

            // Apply for sub-objects if selected.
            foreach (var assembly in project.Assemblies.Where(p => !p.IsDeleted))
            {
                this.UpdateAssembly(assembly);
            }

            foreach (var part in project.Parts.Where(p => !p.IsDeleted))
            {
                this.UpdatePart(part);
            }
        }

        /// <summary>
        /// Update the fields in an assembly
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        private void UpdateAssembly(Assembly assembly)
        {
            // update values in process steps
            foreach (var step in assembly.Process.Steps)
            {
                this.UpdateProcessStep(step);
            }

            if (this.ApplyUpdatesToSubObjects)
            {
                foreach (var subAssembly in assembly.Subassemblies.Where(p => !p.IsDeleted))
                {
                    this.UpdateAssembly(subAssembly);
                }

                foreach (var part in assembly.Parts.Where(p => !p.IsDeleted))
                {
                    this.UpdatePart(part);
                }
            }
        }

        /// <summary>
        /// Update the fields in a part.
        /// </summary>
        /// <param name="part">The part.</param>
        private void UpdatePart(Part part)
        {
            foreach (var step in part.Process.Steps)
            {
                this.UpdateProcessStep(step);
            }

            if (ApplyUpdatesToSubObjects && part.RawPart != null && !part.RawPart.IsDeleted)
            {
                this.UpdatePart(part.RawPart);
            }
        }

        /// <summary>
        /// Update the fields in a Process Step
        /// </summary>
        /// <param name="step">The step.</param>
        private void UpdateProcessStep(ProcessStep step)
        {
            var objectUpdated = false;

            if (this.ProcessTimeNewValue != null && this.ProcessTimeNewValue != step.ProcessTime)
            {
                step.ProcessTime = this.ProcessTimeNewValue;
                step.ProcessTimeUnit = this.ProcessTimeUnitNewValue;
                objectUpdated = true;
            }

            if (this.ProcessTimeIncreasePercentage != null)
            {
                step.ProcessTime = step.ProcessTime * this.ProcessTimeIncreasePercentage;
                objectUpdated = true;
            }

            if (this.CycleTimeNewValue != null && this.CycleTimeNewValue != step.CycleTime)
            {
                step.CycleTime = this.CycleTimeNewValue;
                step.CycleTimeUnit = this.CycleTimeUnitNewValue;
                objectUpdated = true;
            }

            if (this.CycleTimeIncreasePercentage != null)
            {
                step.CycleTime = step.CycleTime * this.CycleTimeIncreasePercentage;
                objectUpdated = true;
            }

            if (this.PartsPerCycleNewValue != null && this.PartsPerCycleNewValue != step.PartsPerCycle)
            {
                step.PartsPerCycle = this.PartsPerCycleNewValue;
                objectUpdated = true;
            }

            if (this.ScrapAmountNewValue != null && this.ScrapAmountNewValue != step.ScrapAmount)
            {
                step.ScrapAmount = this.ScrapAmountNewValue;
                objectUpdated = true;
            }

            if (this.ScrapAmountIncreasePercentage != null)
            {
                step.ScrapAmount = step.ScrapAmount * this.ScrapAmountIncreasePercentage;
                objectUpdated = true;
            }

            if (this.SetupsPerBatchNewValue != null && this.SetupsPerBatchNewValue != step.SetupsPerBatch)
            {
                step.SetupsPerBatch = this.SetupsPerBatchNewValue;
                objectUpdated = true;
            }

            if (this.MaxDowntimeNewValue != null && this.MaxDowntimeNewValue != step.MaxDownTime)
            {
                step.MaxDownTime = this.MaxDowntimeNewValue;
                step.MaxDownTimeUnit = this.MaxDowntimeUnitNewValue;
                objectUpdated = true;
            }

            if (this.MaxDowntimeIncreasePercentage != null)
            {
                step.MaxDownTime = step.MaxDownTime * this.MaxDowntimeIncreasePercentage;
                objectUpdated = true;
            }

            if (this.SetupTimeNewValue != null && this.SetupTimeNewValue != step.SetupTime)
            {
                step.SetupTime = this.SetupTimeNewValue;
                step.SetupTimeUnit = this.SetupTimeUnitNewValue;
                objectUpdated = true;
            }

            if (this.SetupTimeIncreasePercentage != null)
            {
                step.SetupTime = step.SetupTime * this.SetupTimeIncreasePercentage;
                objectUpdated = true;
            }

            if (this.BatchSizeNewValue != null && this.BatchSizeNewValue != step.BatchSize)
            {
                step.BatchSize = this.BatchSizeNewValue;
                objectUpdated = true;
            }

            if (this.ManufacturingOverheadNewValue != null && this.ManufacturingOverheadNewValue != step.ManufacturingOverhead)
            {
                step.ManufacturingOverhead = this.ManufacturingOverheadNewValue;
                objectUpdated = true;
            }

            if (this.ManufacturingOverheadIncreasePercentage != null)
            {
                step.ManufacturingOverhead = step.ManufacturingOverhead * this.ManufacturingOverheadIncreasePercentage;
                objectUpdated = true;
            }

            if (objectUpdated)
            {
                this.numberOfUpdatedObjects++;
            }
        }

        #endregion Methods
    }
}
