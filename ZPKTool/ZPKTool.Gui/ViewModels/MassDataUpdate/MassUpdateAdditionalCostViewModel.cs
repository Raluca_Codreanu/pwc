﻿using System;
using System.ComponentModel.Composition;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for Mass Update Additional Cost View
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class MassUpdateAdditionalCostViewModel : ViewModel<object, IDataSourceManager>
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The messenger reference
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The window service reference
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// Specifies if the updates are applied on the sub-objects of the current object
        /// </summary>
        private bool applyUpdatesToSubObjects;

        /// <summary>
        /// Counts the number of updated objects
        /// </summary>
        private int numberOfUpdatedObjects = 0;

        /// <summary>
        /// Specifies the state of the checkbox
        /// </summary>
        private bool autoCalculateLogisticCost;

        /// <summary>
        /// The development cost current value
        /// </summary>
        private decimal? developmentCostCurrentValue;

        /// <summary>
        /// The development cost increase percentage
        /// </summary>
        private decimal? developmentCostIncreasePercentage;

        /// <summary>
        /// The development cost new value
        /// </summary>
        private decimal? developmentCostNewValue;

        /// <summary>
        /// The packaging cost current value
        /// </summary>
        private decimal? packagingCostCurrentValue;

        /// <summary>
        /// The packaging cost increase percentage
        /// </summary>
        private decimal? packagingCostIncreasePercentage;

        /// <summary>
        /// The packaging cost new value
        /// </summary>
        private decimal? packagingCostNewValue;

        /// <summary>
        /// The project invest current value
        /// </summary>
        private decimal? projectInvestCurrentValue;

        /// <summary>
        /// The project invest increase percentage
        /// </summary>
        private decimal? projectInvestIncreasePercentage;

        /// <summary>
        /// The project invest new value
        /// </summary>
        private decimal? projectInvestNewValue;

        /// <summary>
        /// The logistic cost current value
        /// </summary>
        private decimal? logisticCostCurrentValue;

        /// <summary>
        /// The logistic cost increase percentage
        /// </summary>
        private decimal? logisticCostIncreasePercentage;

        /// <summary>
        /// The logistic cost new value
        /// </summary>
        private decimal? logisticCostNewValue;

        /// <summary>
        /// The transport cost current value
        /// </summary>
        private decimal? transportCostCurrentValue;

        /// <summary>
        /// The transport cost increase percentage
        /// </summary>
        private decimal? transportCostIncreasePercentage;

        /// <summary>
        /// The transport cost new value
        /// </summary>
        private decimal? transportCostNewValue;

        /// <summary>
        /// The other cost current value
        /// </summary>
        private decimal? otherCostCurrentValue;

        /// <summary>
        /// The other cost increase percentage
        /// </summary>
        private decimal? otherCostIncreasePercentage;

        /// <summary>
        /// The other cost new value
        /// </summary>
        private decimal? otherCostNewValue;

        /// <summary>
        /// The payment terms current value
        /// </summary>
        private int? paymentTermsCurrentValue;

        /// <summary>
        /// The payment terms new value
        /// </summary>
        private int? paymentTermsNewValue;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="MassUpdateAdditionalCostViewModel"/> class.
        /// </summary>
        /// <param name="messenger">The messenger service.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="unitsService">The units service.</param>
        [ImportingConstructor]
        public MassUpdateAdditionalCostViewModel(
            IMessenger messenger,
            IWindowService windowService,
            IUnitsService unitsService)
        {
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("unitsService", unitsService);

            this.messenger = messenger;
            this.windowService = windowService;
            this.unitsService = unitsService;

            this.UpdateCommand = new DelegateCommand(UpdateAdditionalCost);
        }

        #region Commands

        /// <summary>
        /// Gets the command for the Update button
        /// </summary>
        public ICommand UpdateCommand { get; private set; }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether to apply updates to sub objects.
        /// </summary>
        public bool ApplyUpdatesToSubObjects
        {
            get
            {
                return this.applyUpdatesToSubObjects;
            }

            set
            {
                if (this.applyUpdatesToSubObjects != value)
                {
                    this.applyUpdatesToSubObjects = value;
                    OnPropertyChanged(() => this.ApplyUpdatesToSubObjects);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to auto calculate the logistic cost.
        /// </summary>
        public bool AutoCalculateLogisticCost
        {
            get
            {
                return this.autoCalculateLogisticCost;
            }

            set
            {
                if (this.autoCalculateLogisticCost != value)
                {
                    this.autoCalculateLogisticCost = value;
                    OnPropertyChanged(() => this.AutoCalculateLogisticCost);
                }
            }
        }

        /// <summary>
        /// Gets or sets the development cost current value.
        /// </summary>
        public decimal? DevelopmentCostCurrentValue
        {
            get
            {
                return this.developmentCostCurrentValue;
            }

            set
            {
                if (this.developmentCostCurrentValue != value)
                {
                    this.developmentCostCurrentValue = value;
                    OnPropertyChanged(() => this.DevelopmentCostCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the development cost increase percentage.
        /// </summary>
        public decimal? DevelopmentCostIncreasePercentage
        {
            get
            {
                return this.developmentCostIncreasePercentage;
            }

            set
            {
                if (this.developmentCostIncreasePercentage != value)
                {
                    this.developmentCostIncreasePercentage = value;
                    OnPropertyChanged(() => this.DevelopmentCostIncreasePercentage);

                    if (value != null)
                    {
                        DevelopmentCostNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the development cost new value.
        /// </summary>
        public decimal? DevelopmentCostNewValue
        {
            get
            {
                return this.developmentCostNewValue;
            }

            set
            {
                if (this.developmentCostNewValue != value)
                {
                    this.developmentCostNewValue = value;
                    OnPropertyChanged(() => this.DevelopmentCostNewValue);

                    if (value != null)
                    {
                        DevelopmentCostIncreasePercentage = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the packaging cost current value.
        /// </summary>
        public decimal? PackagingCostCurrentValue
        {
            get
            {
                return this.packagingCostCurrentValue;
            }

            set
            {
                if (this.packagingCostCurrentValue != value)
                {
                    this.packagingCostCurrentValue = value;
                    OnPropertyChanged(() => this.PackagingCostCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the packaging cost increase percentage.
        /// </summary>
        public decimal? PackagingCostIncreasePercentage
        {
            get
            {
                return this.packagingCostIncreasePercentage;
            }

            set
            {
                if (this.packagingCostIncreasePercentage != value)
                {
                    this.packagingCostIncreasePercentage = value;
                    OnPropertyChanged(() => this.PackagingCostIncreasePercentage);

                    if (value != null)
                    {
                        PackagingCostNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the packaging cost new value.
        /// </summary>
        public decimal? PackagingCostNewValue
        {
            get
            {
                return this.packagingCostNewValue;
            }

            set
            {
                if (this.packagingCostNewValue != value)
                {
                    this.packagingCostNewValue = value;
                    OnPropertyChanged(() => this.PackagingCostNewValue);

                    if (value != null)
                    {
                        PackagingCostIncreasePercentage = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the project invest current value.
        /// </summary>
        public decimal? ProjectInvestCurrentValue
        {
            get
            {
                return this.projectInvestCurrentValue;
            }

            set
            {
                if (this.projectInvestCurrentValue != value)
                {
                    this.projectInvestCurrentValue = value;
                    OnPropertyChanged(() => this.ProjectInvestCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the project invest increase percentage.
        /// </summary>
        public decimal? ProjectInvestIncreasePercentage
        {
            get
            {
                return this.projectInvestIncreasePercentage;
            }

            set
            {
                if (this.projectInvestIncreasePercentage != value)
                {
                    this.projectInvestIncreasePercentage = value;
                    OnPropertyChanged(() => this.ProjectInvestIncreasePercentage);

                    if (value != null)
                    {
                        ProjectInvestNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the project invest new value.
        /// </summary>
        public decimal? ProjectInvestNewValue
        {
            get
            {
                return this.projectInvestNewValue;
            }

            set
            {
                if (this.projectInvestNewValue != value)
                {
                    this.projectInvestNewValue = value;
                    OnPropertyChanged(() => this.ProjectInvestNewValue);

                    if (value != null)
                    {
                        ProjectInvestIncreasePercentage = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the logistic cost current value.
        /// </summary>
        public decimal? LogisticCostCurrentValue
        {
            get
            {
                return this.logisticCostCurrentValue;
            }

            set
            {
                if (this.logisticCostCurrentValue != value)
                {
                    this.logisticCostCurrentValue = value;
                    OnPropertyChanged(() => this.LogisticCostCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the logistic cost increase percentage.
        /// </summary>
        public decimal? LogisticCostIncreasePercentage
        {
            get
            {
                return this.logisticCostIncreasePercentage;
            }

            set
            {
                if (this.logisticCostIncreasePercentage != value)
                {
                    this.logisticCostIncreasePercentage = value;
                    OnPropertyChanged(() => this.LogisticCostIncreasePercentage);

                    if (value != null)
                    {
                        LogisticCostNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the logistic cost new value.
        /// </summary>
        public decimal? LogisticCostNewValue
        {
            get
            {
                return this.logisticCostNewValue;
            }

            set
            {
                if (this.logisticCostNewValue != value)
                {
                    this.logisticCostNewValue = value;
                    OnPropertyChanged(() => this.LogisticCostNewValue);

                    if (value != null)
                    {
                        LogisticCostIncreasePercentage = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the transport cost current value.
        /// </summary>
        public decimal? TransportCostCurrentValue
        {
            get
            {
                return this.transportCostCurrentValue;
            }

            set
            {
                if (this.transportCostCurrentValue != value)
                {
                    this.transportCostCurrentValue = value;
                    OnPropertyChanged(() => this.TransportCostCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the transport cost increase percentage.
        /// </summary>
        public decimal? TransportCostIncreasePercentage
        {
            get
            {
                return this.transportCostIncreasePercentage;
            }

            set
            {
                if (this.transportCostIncreasePercentage != value)
                {
                    this.transportCostIncreasePercentage = value;
                    OnPropertyChanged(() => this.TransportCostIncreasePercentage);

                    if (value != null)
                    {
                        TransportCostNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the transport cost new value.
        /// </summary>
        public decimal? TransportCostNewValue
        {
            get
            {
                return this.transportCostNewValue;
            }

            set
            {
                if (this.transportCostNewValue != value)
                {
                    this.transportCostNewValue = value;
                    OnPropertyChanged(() => this.TransportCostNewValue);

                    if (value != null)
                    {
                        TransportCostIncreasePercentage = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the other cost current value.
        /// </summary>
        public decimal? OtherCostCurrentValue
        {
            get
            {
                return this.otherCostCurrentValue;
            }

            set
            {
                if (this.otherCostCurrentValue != value)
                {
                    this.otherCostCurrentValue = value;
                    OnPropertyChanged(() => this.OtherCostCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the other cost increase percentage.
        /// </summary>
        public decimal? OtherCostIncreasePercentage
        {
            get
            {
                return this.otherCostIncreasePercentage;
            }

            set
            {
                if (this.otherCostIncreasePercentage != value)
                {
                    this.otherCostIncreasePercentage = value;
                    OnPropertyChanged(() => this.OtherCostIncreasePercentage);

                    if (value != null)
                    {
                        OtherCostNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the other cost new value.
        /// </summary>
        public decimal? OtherCostNewValue
        {
            get
            {
                return this.otherCostNewValue;
            }

            set
            {
                if (this.otherCostNewValue != value)
                {
                    this.otherCostNewValue = value;
                    OnPropertyChanged(() => this.OtherCostNewValue);

                    if (value != null)
                    {
                        OtherCostIncreasePercentage = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the payment terms current value.
        /// </summary>
        public int? PaymentTermsCurrentValue
        {
            get
            {
                return this.paymentTermsCurrentValue;
            }

            set
            {
                if (this.paymentTermsCurrentValue != value)
                {
                    this.paymentTermsCurrentValue = value;
                    OnPropertyChanged(() => this.PaymentTermsCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the payment terms new value.
        /// </summary>
        public int? PaymentTermsNewValue
        {
            get
            {
                return this.paymentTermsNewValue;
            }

            set
            {
                if (this.paymentTermsNewValue != value)
                {
                    this.paymentTermsNewValue = value;
                    OnPropertyChanged(() => this.PaymentTermsNewValue);
                }
            }
        }

        /// <summary>
        /// Gets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter { get; private set; }

        #endregion

        /// <summary>
        /// Set up the gui based on the input object
        /// </summary>
        protected override void OnModelChanged()
        {
            Assembly assy = this.Model as Assembly;
            if (assy != null)
            {
                DevelopmentCostCurrentValue = assy.DevelopmentCost;
                PackagingCostCurrentValue = assy.PackagingCost;
                ProjectInvestCurrentValue = assy.ProjectInvest;
                LogisticCostCurrentValue = assy.LogisticCost;
                TransportCostCurrentValue = assy.TransportCost;
                AutoCalculateLogisticCost = assy.CalculateLogisticCost != null ? assy.CalculateLogisticCost.Value : false;
                OtherCostCurrentValue = assy.OtherCost;
                PaymentTermsCurrentValue = (int?)assy.PaymentTerms;
            }

            Part part = this.Model as Part;
            if (part != null)
            {
                DevelopmentCostCurrentValue = part.DevelopmentCost;
                PackagingCostCurrentValue = part.PackagingCost;
                ProjectInvestCurrentValue = part.ProjectInvest;
                LogisticCostCurrentValue = part.LogisticCost;
                TransportCostCurrentValue = part.TransportCost;
                AutoCalculateLogisticCost = part.CalculateLogisticCost != null ? part.CalculateLogisticCost.Value : false;
                OtherCostCurrentValue = part.OtherCost;
                PaymentTermsCurrentValue = (int?)part.PaymentTerms;
            }
        }

        /// <summary>
        /// Called when DataSourceManager has changed.
        /// </summary>
        protected override void OnDataSourceManagerChanged()
        {
            base.OnDataSourceManagerChanged();

            this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(this.DataSourceManager);
        }

        /// <summary>
        /// Performs the update operation
        /// </summary>
        private void UpdateAdditionalCost()
        {
            // set the appropriate message
            string message = this.ApplyUpdatesToSubObjects ? LocalizedResources.MassDataUpdate_ConfirmationWithSubobjects : LocalizedResources.MassDataUpdate_ConfirmationNoSubobjects;
            MessageDialogResult result = this.windowService.MessageDialogService.Show(message, MessageDialogType.YesNo);
            if (result == MessageDialogResult.No)
            {
                return;
            }

            object updatedEntity = null;
            numberOfUpdatedObjects = 0;

            Project project = this.Model as Project;
            if (project != null && this.ApplyUpdatesToSubObjects)
            {
                // special case for project, must be retrieved on a new context
                Task.Factory.StartNew(() =>
                {
                    this.messenger.Send<NotificationMessage>(new NotificationMessage("UpdateMassData"));
                    Project fullProject = this.DataSourceManager.ProjectRepository.GetProjectFull(project.Guid);

                    if (fullProject != null)
                    {
                        this.UpdateProject(fullProject);
                    }

                    this.DataSourceManager.SaveChanges();
                })
                .ContinueWith(
                    (task) =>
                    {
                        this.messenger.Send<NotificationMessage>(new NotificationMessage("UpdateMassDataFinished"));
                        if (task.Exception != null)
                        {
                            Exception error = task.Exception.InnerException;
                            log.ErrorException("Project loading failed.", error);
                            this.windowService.MessageDialogService.Show(error);
                        }
                        else
                        {
                            // refresh the context of the main view
                            EntityChangedMessage notificationMessage = new EntityChangedMessage(Notification.MyProjectsEntityChanged);
                            notificationMessage.ChangeType = EntityChangeType.RefreshData;
                            notificationMessage.Entity = project;
                            this.messenger.Send<EntityChangedMessage>(notificationMessage);

                            this.windowService.MessageDialogService.Show(LocalizedResources.MassDataUpdate_NumberOfUpdatedObjects + numberOfUpdatedObjects, MessageDialogType.Info);
                            this.messenger.Send<NotificationMessage>(new NotificationMessage("CloseMassDataUpdateWindow"));
                        }
                    },
                    System.Threading.Tasks.TaskScheduler.FromCurrentSynchronizationContext());

                return;
            }

            this.DataSourceManager.RevertChanges();

            Assembly assembly = this.Model as Assembly;
            if (assembly != null)
            {
                this.UpdateAssembly(assembly);
                updatedEntity = assembly;
            }
            else
            {
                Part part = this.Model as Part;
                if (part != null)
                {
                    this.UpdatePart(part);
                    updatedEntity = part;
                }
            }

            if (this.DataSourceManager != null)
            {
                this.DataSourceManager.SaveChanges();
            }

            if (updatedEntity != null)
            {
                EntityChangedMessage msg = new EntityChangedMessage(Notification.MyProjectsEntityChanged);
                msg.Entity = updatedEntity;
                msg.Parent = null;
                msg.ChangeType = EntityChangeType.RefreshData;
                this.messenger.Send(msg);
            }

            this.windowService.MessageDialogService.Show(LocalizedResources.MassDataUpdate_NumberOfUpdatedObjects + numberOfUpdatedObjects, MessageDialogType.Info);
            this.messenger.Send<NotificationMessage>(new NotificationMessage("CloseMassDataUpdateWindow"));
        }

        /// <summary>
        /// Update the fields in a project
        /// </summary>
        /// <param name="project">The project.</param>
        private void UpdateProject(Project project)
        {
            foreach (Assembly assembly in project.Assemblies.Where(p => !p.IsDeleted))
            {
                UpdateAssembly(assembly);
            }

            foreach (Part part in project.Parts.Where(p => !p.IsDeleted))
            {
                UpdatePart(part);
            }
        }

        /// <summary>
        /// Update the fields in an assembly
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        private void UpdateAssembly(Assembly assembly)
        {
            bool objectUpdated = false;

            if (this.developmentCostNewValue != null)
            {
                assembly.DevelopmentCost = this.developmentCostNewValue;
                objectUpdated = true;
            }

            if (this.developmentCostIncreasePercentage != null)
            {
                assembly.DevelopmentCost = assembly.DevelopmentCost * this.developmentCostIncreasePercentage;
                objectUpdated = true;
            }

            if (this.projectInvestNewValue != null)
            {
                assembly.ProjectInvest = this.projectInvestNewValue;
                objectUpdated = true;
            }

            if (this.projectInvestIncreasePercentage != null)
            {
                assembly.ProjectInvest = assembly.ProjectInvest * this.projectInvestIncreasePercentage;
                objectUpdated = true;
            }

            if (this.autoCalculateLogisticCost)
            {
                if (assembly.CalculateLogisticCost == false)
                {
                    assembly.CalculateLogisticCost = true;
                    assembly.LogisticCost = null;
                    objectUpdated = true;
                }
            }
            else
            {
                if (this.logisticCostNewValue != null)
                {
                    assembly.LogisticCost = this.logisticCostNewValue;
                    assembly.CalculateLogisticCost = false;
                    objectUpdated = true;
                }

                if (this.logisticCostIncreasePercentage != null)
                {
                    assembly.LogisticCost = assembly.LogisticCost * this.logisticCostIncreasePercentage;
                    assembly.CalculateLogisticCost = false;
                    objectUpdated = true;
                }
            }

            if (assembly.TransportCostCalculations.Count == 0)
            {
                // Apply updates only for assemblies that do not have transport cost calculations.
                if (this.packagingCostNewValue != null)
                {
                    assembly.PackagingCost = this.packagingCostNewValue;
                    objectUpdated = true;
                }

                if (this.packagingCostIncreasePercentage != null)
                {
                    assembly.PackagingCost = assembly.PackagingCost * this.packagingCostIncreasePercentage;
                    objectUpdated = true;
                }

                if (this.transportCostNewValue != null)
                {
                    assembly.TransportCost = this.transportCostNewValue.Value;
                    objectUpdated = true;
                }

                if (this.transportCostIncreasePercentage != null)
                {
                    assembly.TransportCost = assembly.TransportCost * this.transportCostIncreasePercentage.Value;
                    objectUpdated = true;
                }
            }

            if (this.otherCostNewValue != null)
            {
                assembly.OtherCost = this.otherCostNewValue;
                objectUpdated = true;
            }

            if (this.otherCostIncreasePercentage != null)
            {
                assembly.OtherCost = assembly.OtherCost * this.otherCostIncreasePercentage;
                objectUpdated = true;
            }

            if (this.paymentTermsNewValue != null)
            {
                assembly.PaymentTerms = this.paymentTermsNewValue.Value;
                objectUpdated = true;
            }

            if (objectUpdated)
            {
                numberOfUpdatedObjects++;
            }

            // update sub-objects if option is selected
            if (this.ApplyUpdatesToSubObjects)
            {
                foreach (Assembly subAssembly in assembly.Subassemblies.Where(p => !p.IsDeleted))
                {
                    UpdateAssembly(subAssembly);
                }

                foreach (Part part in assembly.Parts.Where(p => !p.IsDeleted))
                {
                    UpdatePart(part);
                }
            }
        }

        /// <summary>
        /// Update the fields in a part
        /// </summary>
        /// <param name="part">The part.</param>
        private void UpdatePart(Part part)
        {
            bool objectUpdated = false;

            if (this.developmentCostNewValue != null)
            {
                part.DevelopmentCost = this.developmentCostNewValue;
                objectUpdated = true;
            }

            if (this.developmentCostIncreasePercentage != null)
            {
                part.DevelopmentCost = part.DevelopmentCost * this.developmentCostIncreasePercentage;
                objectUpdated = true;
            }

            if (this.projectInvestNewValue != null)
            {
                part.ProjectInvest = this.projectInvestNewValue;
                objectUpdated = true;
            }

            if (this.projectInvestIncreasePercentage != null)
            {
                part.ProjectInvest = part.ProjectInvest * this.projectInvestIncreasePercentage;
                objectUpdated = true;
            }

            if (this.autoCalculateLogisticCost)
            {
                if (part.CalculateLogisticCost == false)
                {
                    part.CalculateLogisticCost = true;
                    part.LogisticCost = null;
                    objectUpdated = true;
                }
            }
            else
            {
                if (this.logisticCostNewValue != null)
                {
                    part.LogisticCost = this.logisticCostNewValue;
                    part.CalculateLogisticCost = false;
                    objectUpdated = true;
                }

                if (this.logisticCostIncreasePercentage != null)
                {
                    part.LogisticCost = part.LogisticCost * this.logisticCostIncreasePercentage;
                    part.CalculateLogisticCost = false;
                    objectUpdated = true;
                }
            }

            if (part.TransportCostCalculations.Count == 0)
            {
                // Apply updates only for parts that do not have transport cost calculations.
                if (this.packagingCostNewValue != null)
                {
                    part.PackagingCost = this.packagingCostNewValue;
                    objectUpdated = true;
                }

                if (this.packagingCostIncreasePercentage != null)
                {
                    part.PackagingCost = part.PackagingCost * this.packagingCostIncreasePercentage;
                    objectUpdated = true;
                }

                if (this.transportCostNewValue != null)
                {
                    part.TransportCost = this.transportCostNewValue.Value;
                    objectUpdated = true;
                }

                if (this.transportCostIncreasePercentage != null)
                {
                    part.TransportCost = part.TransportCost * this.transportCostIncreasePercentage.Value;
                    objectUpdated = true;
                }
            }

            if (this.otherCostNewValue != null)
            {
                part.OtherCost = this.otherCostNewValue;
                objectUpdated = true;
            }

            if (this.otherCostIncreasePercentage != null)
            {
                part.OtherCost = part.OtherCost * this.otherCostIncreasePercentage;
                objectUpdated = true;
            }

            if (this.paymentTermsNewValue != null)
            {
                part.PaymentTerms = this.paymentTermsNewValue.Value;
                objectUpdated = true;
            }

            if (objectUpdated)
            {
                numberOfUpdatedObjects++;
            }

            // update sub-objects if option is selected
            if (ApplyUpdatesToSubObjects && part.RawPart != null && !part.RawPart.IsDeleted)
            {
                this.UpdatePart(part.RawPart);
            }
        }
    }
}
