﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for Mass Update Raw Materials View
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class MassUpdateRawMaterialsViewModel : ViewModel<object, IDataSourceManager>
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The messenger reference
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The window service reference
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// Specifies if the updates are applied on the sub-objects of the current object
        /// </summary>
        private bool applyUpdatesToSubObjects;

        /// <summary>
        /// Counts the number of updated objects
        /// </summary>
        private int numberOfUpdatedObjects = 0;

        /// <summary>
        /// Specifies if the data must be loaded (ex: raw materials from project loaded)
        /// </summary>
        private bool isDataLoaded = false;

        /// <summary>
        /// Collection of raw material data items loaded in the list box
        /// </summary>
        private ObservableCollection<RawMaterialDataItem> rawMaterialsCollection;

        /// <summary>
        /// Specifies the visibility of the warning message placed on the bottom of the control
        /// </summary>
        private System.Windows.Visibility warningMessageVisiblity = System.Windows.Visibility.Collapsed;

        /// <summary>
        /// The current value of the price.
        /// </summary>
        private decimal? priceCurrentValue;

        /// <summary>
        /// The percentage to be applied to the price.
        /// </summary>
        private decimal? pricePercent;

        /// <summary>
        /// The new value of the price.
        /// </summary>
        private decimal? priceNewValue;

        /// <summary>
        /// The current value of the yield strength.
        /// </summary>
        private string yieldStrengthCurrentValue;

        /// <summary>
        /// The new value of the yield strength.
        /// </summary>
        private string yieldStrengthNewValue;

        /// <summary>
        /// The current value of the rupture strength.
        /// </summary>
        private string ruptureStrengthCurrentValue;

        /// <summary>
        /// The new value of the rupture strength.
        /// </summary>
        private string ruptureStrengthNewValue;

        /// <summary>
        /// The current value for the density.
        /// </summary>
        private string densityCurrentValue;

        /// <summary>
        /// The new value of the density.
        /// </summary>
        private string densityNewValue;

        /// <summary>
        /// The current value for the max elongation.
        /// </summary>
        private string maxElongationCurrentValue;

        /// <summary>
        /// The new value of the max elongation.
        /// </summary>
        private string maxElongationNewValue;

        /// <summary>
        /// The current value for the glass TT.
        /// </summary>
        private string glassTTCurrentValue;

        /// <summary>
        /// The new value of the glass TT.
        /// </summary>
        private string glassTTNewValue;

        /// <summary>
        /// The current value for the RX property.
        /// </summary>
        [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "The naming is intentional; this is the real world name of a property.")]
        private string rxCurrentValue;

        /// <summary>
        /// The new value of the RX property.
        /// </summary>
        [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "The naming is intentional; this is the real world name of a property.")]
        private string rxNewValue;

        /// <summary>
        /// The current value for the RM property.
        /// </summary>
        [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "The naming is intentional; this is the real world name of a property.")]
        private string rmCurrentValue;

        /// <summary>
        /// The new value of the RM property.
        /// </summary>
        [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "The naming is intentional; this is the real world name of a property.")]
        private string rmNewValue;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="MassUpdateRawMaterialsViewModel"/> class.
        /// </summary>
        /// <param name="messenger">The messenger service.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="unitsService">The units service.</param>
        [ImportingConstructor]
        public MassUpdateRawMaterialsViewModel(
            IMessenger messenger,
            IWindowService windowService,
            IUnitsService unitsService)
        {
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("unitsService", unitsService);

            this.messenger = messenger;
            this.windowService = windowService;
            this.unitsService = unitsService;

            this.UpdateCommand = new DelegateCommand(UpdateAction);
            this.RawMaterialsCollection = new ObservableCollection<RawMaterialDataItem>();
            this.SelectedRawMaterials = new List<RawMaterialDataItem>();
            this.SelectionChangedCommand = new DelegateCommand<SelectionChangedEventArgs>(ListBoxSelectionChanged);
        }

        #region Commands

        /// <summary>
        /// Gets the command for the Update button
        /// </summary>
        public ICommand UpdateCommand { get; private set; }

        /// <summary>
        /// Gets the command for the Selection Changed event
        /// </summary>
        public ICommand SelectionChangedCommand { get; private set; }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the new country settings object which will replace the current settings.
        /// </summary>
        public CountrySetting NewCountrySettings { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the raw material mass update tab is selected
        /// </summary>
        public bool IsTabSelected { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to apply any updates to sub objects.
        /// </summary>
        public bool ApplyUpdatesToSubObjects
        {
            get
            {
                return this.applyUpdatesToSubObjects;
            }

            set
            {
                if (this.applyUpdatesToSubObjects != value)
                {
                    this.applyUpdatesToSubObjects = value;
                    OnPropertyChanged(() => this.ApplyUpdatesToSubObjects);

                    if (this.IsTabSelected)
                    {
                        if (!value)
                        {
                            this.isDataLoaded = false;
                        }

                        ChangeMaterialsCollection();
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the collection of raw material items which populates the list box
        /// </summary>
        public ObservableCollection<RawMaterialDataItem> RawMaterialsCollection
        {
            get
            {
                return this.rawMaterialsCollection;
            }

            set
            {
                if (this.rawMaterialsCollection != value)
                {
                    this.rawMaterialsCollection = value;
                    OnPropertyChanged(() => this.RawMaterialsCollection);
                }
            }
        }

        /// <summary>
        /// Gets or sets the collection of selected raw materials, the collection is always synchronized with the selected items from the list box.
        /// </summary>
        public List<RawMaterialDataItem> SelectedRawMaterials { get; set; }

        /// <summary>
        /// Gets or sets the current value of the price.
        /// </summary>
        public decimal? PriceCurrentValue
        {
            get
            {
                return this.priceCurrentValue;
            }

            set
            {
                if (this.priceCurrentValue != value)
                {
                    this.priceCurrentValue = value;
                    OnPropertyChanged(() => this.PriceCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the percentage to be applied to the price.
        /// </summary>
        public decimal? PricePercent
        {
            get
            {
                return this.pricePercent;
            }

            set
            {
                if (this.pricePercent != value)
                {
                    this.pricePercent = value;
                    OnPropertyChanged(() => this.PricePercent);
                    if (value != null)
                    {
                        PriceNewValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the new value of the price.
        /// </summary>
        public decimal? PriceNewValue
        {
            get
            {
                return this.priceNewValue;
            }

            set
            {
                if (this.priceNewValue != value)
                {
                    this.priceNewValue = value;
                    OnPropertyChanged(() => this.PriceNewValue);
                    if (value != null)
                    {
                        PricePercent = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the current value of the yield strength.
        /// </summary>
        public string YieldStrengthCurrentValue
        {
            get
            {
                return this.yieldStrengthCurrentValue;
            }

            set
            {
                if (this.yieldStrengthCurrentValue != value)
                {
                    this.yieldStrengthCurrentValue = value;
                    OnPropertyChanged(() => this.YieldStrengthCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the new value of the yield strength.
        /// </summary>
        public string YieldStrengthNewValue
        {
            get
            {
                return this.yieldStrengthNewValue;
            }

            set
            {
                if (this.yieldStrengthNewValue != value)
                {
                    this.yieldStrengthNewValue = value;
                    OnPropertyChanged(() => this.YieldStrengthNewValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the current value of the rupture strength.
        /// </summary>
        public string RuptureStrengthCurrentValue
        {
            get
            {
                return this.ruptureStrengthCurrentValue;
            }

            set
            {
                if (this.ruptureStrengthCurrentValue != value)
                {
                    this.ruptureStrengthCurrentValue = value;
                    OnPropertyChanged(() => this.RuptureStrengthCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the new value of rupture strength.
        /// </summary>
        public string RuptureStrengthNewValue
        {
            get
            {
                return this.ruptureStrengthNewValue;
            }

            set
            {
                if (this.ruptureStrengthNewValue != value)
                {
                    this.ruptureStrengthNewValue = value;
                    OnPropertyChanged(() => this.RuptureStrengthNewValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the current value of the density.
        /// </summary>
        public string DensityCurrentValue
        {
            get
            {
                return this.densityCurrentValue;
            }

            set
            {
                if (this.densityCurrentValue != value)
                {
                    this.densityCurrentValue = value;
                    OnPropertyChanged(() => this.DensityCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the new value of the density.
        /// </summary>
        public string DensityNewValue
        {
            get
            {
                return this.densityNewValue;
            }

            set
            {
                if (this.densityNewValue != value)
                {
                    this.densityNewValue = value;
                    OnPropertyChanged(() => this.DensityNewValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the current value of the max elongation.
        /// </summary>
        public string MaxElongationCurrentValue
        {
            get
            {
                return this.maxElongationCurrentValue;
            }

            set
            {
                if (this.maxElongationCurrentValue != value)
                {
                    this.maxElongationCurrentValue = value;
                    OnPropertyChanged(() => this.MaxElongationCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the new value of the max elongation.
        /// </summary>
        public string MaxElongationNewValue
        {
            get
            {
                return this.maxElongationNewValue;
            }

            set
            {
                if (this.maxElongationNewValue != value)
                {
                    this.maxElongationNewValue = value;
                    OnPropertyChanged(() => this.MaxElongationNewValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the current value of the  glass TT.
        /// </summary>
        public string GlassTTCurrentValue
        {
            get
            {
                return this.glassTTCurrentValue;
            }

            set
            {
                if (this.glassTTCurrentValue != value)
                {
                    this.glassTTCurrentValue = value;
                    OnPropertyChanged(() => this.GlassTTCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the new value of the glass TT property.
        /// </summary>
        public string GlassTTNewValue
        {
            get
            {
                return this.glassTTNewValue;
            }

            set
            {
                if (this.glassTTNewValue != value)
                {
                    this.glassTTNewValue = value;
                    OnPropertyChanged(() => this.GlassTTNewValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the current value of the RX property.
        /// </summary>
        public string RxCurrentValue
        {
            get
            {
                return this.rxCurrentValue;
            }

            set
            {
                if (this.rxCurrentValue != value)
                {
                    this.rxCurrentValue = value;
                    OnPropertyChanged(() => this.RxCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the new value of the RX property.
        /// </summary>
        public string RxNewValue
        {
            get
            {
                return this.rxNewValue;
            }

            set
            {
                if (this.rxNewValue != value)
                {
                    this.rxNewValue = value;
                    OnPropertyChanged(() => this.RxNewValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the current value of the RM property.
        /// </summary>
        public string RmCurrentValue
        {
            get
            {
                return this.rmCurrentValue;
            }

            set
            {
                if (this.rmCurrentValue != value)
                {
                    this.rmCurrentValue = value;
                    OnPropertyChanged(() => this.RmCurrentValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the new value of the RM property.
        /// </summary>
        public string RmNewValue
        {
            get
            {
                return this.rmNewValue;
            }

            set
            {
                if (this.rmNewValue != value)
                {
                    this.rmNewValue = value;
                    OnPropertyChanged(() => this.RmNewValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the visibility of the warning message.
        /// </summary>
        public Visibility WarningMessageVisibility
        {
            get
            {
                return this.warningMessageVisiblity;
            }

            set
            {
                if (this.warningMessageVisiblity != value)
                {
                    this.warningMessageVisiblity = value;
                    OnPropertyChanged(() => this.WarningMessageVisibility);
                }
            }
        }

        /// <summary>
        /// Gets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter { get; private set; }

        #endregion

        /// <summary>
        /// Handler for selection changed event
        /// </summary>
        /// <param name="e">The <see cref="SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void ListBoxSelectionChanged(SelectionChangedEventArgs e)
        {
            var addedItems = e.AddedItems.OfType<RawMaterialDataItem>();
            SelectedRawMaterials.AddRange(addedItems);

            var removedItems = e.RemovedItems.OfType<RawMaterialDataItem>();
            foreach (RawMaterialDataItem item in removedItems)
            {
                SelectedRawMaterials.Remove(item);
            }

            SetCurrentValues();
        }

        /// <summary>
        /// Gets the raw materials in an assembly searching in its sub-objects
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <param name="materials">The materials.</param>
        private void GetAssemblyMaterials(Assembly assembly, List<RawMaterial> materials)
        {
            foreach (Assembly assy in assembly.Subassemblies.Where(p => !p.IsDeleted))
            {
                GetAssemblyMaterials(assy, materials);
            }

            foreach (Part part in assembly.Parts.Where(p => !p.IsDeleted))
            {
                materials.AddRange(part.RawMaterials.Where(p => !p.IsDeleted));

                if (ApplyUpdatesToSubObjects && part.RawPart != null && !part.RawPart.IsDeleted)
                {
                    materials.AddRange(part.RawPart.RawMaterials.Where(rawMaterial => !rawMaterial.IsDeleted));
                }
            }
        }

        /// <summary>
        /// Method called for changing the collection of materials available
        /// </summary>
        public void ChangeMaterialsCollection()
        {
            if (this.isDataLoaded)
            {
                return;
            }

            if (this.Model is Project || this.Model is Assembly || this.Model.GetType() == typeof(Part))
            {
                // Note: the RawParts are excluded since they don't have child objects to get the raw materials from.
                this.RawMaterialsCollection.Clear();
                this.SelectedRawMaterials.Clear();
            }

            Project project = this.Model as Project;
            if (project != null && this.applyUpdatesToSubObjects)
            {
                this.isDataLoaded = true;
                this.DataSourceManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
                List<RawMaterial> materials = new List<RawMaterial>();

                Task.Factory.StartNew(() =>
                {
                    this.messenger.Send<NotificationMessage>(new NotificationMessage("LoadingEntity"));

                    Collection<Assembly> assemblies = this.DataSourceManager.AssemblyRepository.GetProjectAssemblies(project.Guid);
                    foreach (Assembly assy in assemblies.Where(p => !p.IsDeleted))
                    {
                        Assembly assyWithRawMaterials = this.DataSourceManager.AssemblyRepository.GetAssemblyFull(assy.Guid);
                        GetAssemblyMaterials(assyWithRawMaterials, materials);
                    }

                    Collection<Part> parts = this.DataSourceManager.PartRepository.GetProjectParts(project.Guid);
                    foreach (Part part in parts.Where(p => !p.IsDeleted))
                    {
                        Part partWithRawMaterials = this.DataSourceManager.PartRepository.GetPartFull(part.Guid);
                        materials.AddRange(partWithRawMaterials.RawMaterials.Where(rawMaterial => !rawMaterial.IsDeleted));

                        if (ApplyUpdatesToSubObjects && part.RawPart != null && !part.RawPart.IsDeleted)
                        {
                            materials.AddRange(part.RawPart.RawMaterials.Where(rawMaterial => !rawMaterial.IsDeleted));
                        }
                    }
                })
                .ContinueWith(
                    task =>
                    {
                        this.messenger.Send(new NotificationMessage("LoadingEntityFinished"));
                        if (task.Exception != null)
                        {
                            Exception error = task.Exception.InnerException;
                            log.ErrorException("Project loading failed.", error);
                            this.windowService.MessageDialogService.Show(error);
                        }

                        foreach (RawMaterial material in materials)
                        {
                            AddMaterialToCollection(material);
                        }
                    },
                    TaskScheduler.FromCurrentSynchronizationContext());
            }

            Assembly assembly = this.Model as Assembly;
            if (assembly != null && this.applyUpdatesToSubObjects)
            {
                this.isDataLoaded = true;
                List<RawMaterial> materials = new List<RawMaterial>();
                GetAssemblyMaterials(assembly, materials);

                foreach (RawMaterial material in materials)
                {
                    AddMaterialToCollection(material);
                }
            }

            Part partToUpdate = Model as Part;
            if (partToUpdate != null && partToUpdate.GetType() == typeof(Part))
            {
                List<RawMaterial> materials = new List<RawMaterial>();
                materials.AddRange(partToUpdate.RawMaterials.Where(rawMaterial => !rawMaterial.IsDeleted));

                if (ApplyUpdatesToSubObjects && partToUpdate.RawPart != null && !partToUpdate.RawPart.IsDeleted)
                {
                    materials.AddRange(partToUpdate.RawPart.RawMaterials.Where(rawMaterial => !rawMaterial.IsDeleted));
                    this.isDataLoaded = true;
                }

                foreach (var material in materials)
                {
                    AddMaterialToCollection(material);
                }
            }
        }

        /// <summary>
        /// Adds a material to the materials list
        /// </summary>
        /// <param name="material">input parameter</param>
        private void AddMaterialToCollection(RawMaterial material)
        {
            RawMaterialDataItem item = this.RawMaterialsCollection.FirstOrDefault(r => r.Name == material.Name);
            if (item == null)
            {
                // if the material was not added before, create a new item
                item = new RawMaterialDataItem();
                item.Name = material.Name;
                item.RawMaterials.Add(material);
                item.Icon = Images.RawMaterialIcon;
                item.Count = 1;
                RawMaterialsCollection.Add(item);
            }
            else
            {
                // else add the material in the item containing the materials with the same name
                item.RawMaterials.Add(material);
                item.Count++;
            }
        }

        /// <summary>
        /// Sets the initial values and populates the raw materials list
        /// </summary>
        protected override void OnModelChanged()
        {
            Part part = this.Model as Part;
            if (part != null)
            {
                foreach (RawMaterial material in part.RawMaterials.Where(p => !p.IsDeleted))
                {
                    AddMaterialToCollection(material);
                }

                if (ApplyUpdatesToSubObjects && part.RawPart != null && !part.RawPart.IsDeleted)
                {
                    foreach (var rawPartMaterial in part.RawPart.RawMaterials.Where(p => !p.IsDeleted))
                    {
                        AddMaterialToCollection(rawPartMaterial);
                    }
                }
            }

            RawMaterial mat = this.Model as RawMaterial;
            if (mat != null)
            {
                AddMaterialToCollection(mat);
            }

            SetCurrentValues();
        }

        /// <summary>
        /// Called when DataSourceManager has changed.
        /// </summary>
        protected override void OnDataSourceManagerChanged()
        {
            base.OnDataSourceManagerChanged();

            this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(this.DataSourceManager);
        }

        /// <summary>
        /// Sets the current values on the UI based on the raw materials selected.
        /// </summary>
        private void SetCurrentValues()
        {
            this.PriceCurrentValue = null;
            this.WarningMessageVisibility = System.Windows.Visibility.Collapsed;

            // no items selected
            if (this.SelectedRawMaterials.Count <= 0)
            {
                return;
            }

            RawMaterial firstMat = null;
            RawMaterialDataItem firstItem = SelectedRawMaterials.FirstOrDefault();
            if (firstItem != null)
            {
                firstMat = firstItem.RawMaterials.FirstOrDefault();
            }

            if (firstMat == null)
            {
                return;
            }

            // get the values of the first material to compare with the others
            decimal? price = firstMat.Price;
            string yieldStr = firstMat.YieldStrength;
            string ruptureStr = firstMat.RuptureStrength;
            string density = firstMat.Density;
            string elongation = firstMat.MaxElongation;
            string glassTT = firstMat.GlassTransitionTemperature;
            string rx = firstMat.Rx;
            string rm = firstMat.Rm;
            bool differentValues = false;

            foreach (RawMaterialDataItem item in SelectedRawMaterials)
            {
                foreach (RawMaterial mat in item.RawMaterials)
                {
                    // if a value is different than the first one, set it to null, meaning that the warning message will be visible
                    if (mat.Price != price)
                    {
                        price = null;
                        differentValues = true;
                    }

                    if (mat.YieldStrength != yieldStr)
                    {
                        yieldStr = null;
                        differentValues = true;
                    }

                    if (mat.RuptureStrength != ruptureStr)
                    {
                        ruptureStr = null;
                        differentValues = true;
                    }

                    if (mat.Density != density)
                    {
                        density = null;
                        differentValues = true;
                    }

                    if (mat.MaxElongation != elongation)
                    {
                        elongation = null;
                        differentValues = true;
                    }

                    if (mat.GlassTransitionTemperature != glassTT)
                    {
                        glassTT = null;
                        differentValues = true;
                    }

                    if (mat.Rx != rx)
                    {
                        rx = null;
                        differentValues = true;
                    }

                    if (mat.Rm != rm)
                    {
                        rm = null;
                        differentValues = true;
                    }
                }
            }

            if (price != null)
            {
                this.PriceCurrentValue = price;
            }

            if (yieldStr != null)
            {
                this.YieldStrengthCurrentValue = yieldStr;
            }

            if (ruptureStr != null)
            {
                this.RuptureStrengthCurrentValue = ruptureStr;
            }

            if (density != null)
            {
                this.DensityCurrentValue = density;
            }

            if (elongation != null)
            {
                this.MaxElongationCurrentValue = elongation;
            }

            if (glassTT != null)
            {
                this.GlassTTCurrentValue = glassTT;
            }

            if (rx != null)
            {
                this.RxCurrentValue = rx;
            }

            if (rm != null)
            {
                this.RmCurrentValue = rm;
            }

            if (differentValues)
            {
                this.WarningMessageVisibility = System.Windows.Visibility.Visible;
            }
        }

        /// <summary>
        /// Updates the raw materials selected
        /// </summary>
        private void UpdateAction()
        {
            // set the appropriate message
            string message = this.ApplyUpdatesToSubObjects ? LocalizedResources.MassDataUpdate_ConfirmationWithSubobjects : LocalizedResources.MassDataUpdate_ConfirmationNoSubobjects;
            MessageDialogResult result = this.windowService.MessageDialogService.Show(message, MessageDialogType.YesNo);
            if (result == MessageDialogResult.No)
            {
                return;
            }

            object updatedEntity = null;
            numberOfUpdatedObjects = 0;

            foreach (RawMaterialDataItem item in SelectedRawMaterials)
            {
                foreach (RawMaterial material in item.RawMaterials)
                {
                    UpdateMaterial(material);
                }
            }

            if (this.DataSourceManager != null)
            {
                this.DataSourceManager.SaveChanges();
            }

            Project project = this.Model as Project;
            if (project != null)
            {
                updatedEntity = project;
            }
            else
            {
                Assembly assembly = this.Model as Assembly;
                if (assembly != null)
                {
                    updatedEntity = assembly;
                }
                else
                {
                    Part part = this.Model as Part;
                    if (part != null)
                    {
                        updatedEntity = part;
                    }
                    else
                    {
                        RawMaterial rawMaterial = this.Model as RawMaterial;
                        if (rawMaterial != null)
                        {
                            updatedEntity = rawMaterial;
                        }
                    }
                }
            }

            if (updatedEntity != null)
            {
                // refresh the context of the main view
                EntityChangedMessage msg = new EntityChangedMessage(Notification.MyProjectsEntityChanged);
                msg.Entity = updatedEntity;
                msg.Parent = null;
                msg.ChangeType = EntityChangeType.RefreshData;
                this.messenger.Send(msg);
            }

            this.windowService.MessageDialogService.Show(LocalizedResources.MassDataUpdate_NumberOfUpdatedObjects + numberOfUpdatedObjects, MessageDialogType.Info);
            this.messenger.Send<NotificationMessage>(new NotificationMessage("CloseMassDataUpdateWindow"));
        }

        /// <summary>
        /// Update the fields in a material
        /// </summary>
        /// <param name="material">The material.</param>
        private void UpdateMaterial(RawMaterial material)
        {
            bool objectUpdated = false;

            if (this.priceNewValue != null)
            {
                material.Price = PriceNewValue;
                objectUpdated = true;
            }

            if (this.pricePercent != null)
            {
                material.Price = material.Price * this.pricePercent;
                objectUpdated = true;
            }

            if (this.yieldStrengthNewValue != null)
            {
                material.YieldStrength = this.yieldStrengthNewValue;
                objectUpdated = true;
            }

            if (this.ruptureStrengthNewValue != null)
            {
                material.RuptureStrength = this.ruptureStrengthNewValue;
                objectUpdated = true;
            }

            if (this.densityNewValue != null)
            {
                material.Density = this.densityNewValue;
                objectUpdated = true;
            }

            if (this.maxElongationNewValue != null)
            {
                material.MaxElongation = this.maxElongationNewValue;
                objectUpdated = true;
            }

            if (this.glassTTNewValue != null)
            {
                material.GlassTransitionTemperature = this.glassTTNewValue;
                objectUpdated = true;
            }

            if (this.rxNewValue != null)
            {
                material.Rx = this.rxNewValue;
                objectUpdated = true;
            }

            if (this.rmNewValue != null)
            {
                material.Rm = this.rmNewValue;
                objectUpdated = true;
            }

            if (objectUpdated)
            {
                numberOfUpdatedObjects++;
            }
        }
    }
}
