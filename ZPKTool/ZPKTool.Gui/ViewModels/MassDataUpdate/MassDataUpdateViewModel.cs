﻿using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Windows;
using System.Windows.Input;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for Mass Data Update
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class MassDataUpdateViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// A value indicating that the apply updates to sub-objects is set to true.
        /// </summary>
        public const string SetApplyUpdatesToSubObjectsTrue = "SetApplyUpdatesToSubObjectsTrue";

        /// <summary>
        /// The messenger reference
        /// </summary>
        private readonly IMessenger messenger;

        /// <summary>
        /// The window service reference
        /// </summary>
        private readonly IWindowService windowService;

        /// <summary>
        /// Reference to the current object on which we apply the updates
        /// </summary>
        private object currentObject;

        /// <summary>
        /// The title of the window
        /// </summary>
        private string title;

        /// <summary>
        /// Specifies if the update is applied on all the sub-objects
        /// </summary>
        private bool applyUpdatesToSubObjects;

        /// <summary>
        /// Specifies that an entity is being loaded and the screen is blocked
        /// </summary>
        private bool isLoadingEntity;

        /// <summary>
        /// The message written while data is being loaded/processed
        /// </summary>
        private string waitAnimationMessage;

        /// <summary>
        /// Visibility state for the shift info tab
        /// </summary>
        private Visibility shiftInformationTabVisibility;

        /// <summary>
        /// Visibility state for the quantity tab
        /// </summary>
        private Visibility quantityTabVisibility;

        /// <summary>
        /// Visibility state for the OH and margin tab
        /// </summary>
        private Visibility overheadAndMarginTabVisibility;

        /// <summary>
        /// Visibility state for the country settings tab
        /// </summary>
        private Visibility countrySettingsTabVisibility;

        /// <summary>
        /// Visibility state for the machines tab
        /// </summary>
        private Visibility machinesTabVisibility;

        /// <summary>
        /// Visibility state for the additional settings tab
        /// </summary>
        private Visibility additionalCostTabVisibility;

        /// <summary>
        /// Visibility state for the raw materials tab
        /// </summary>
        private Visibility rawMaterialsTabVisibility;

        /// <summary>
        /// Visibility state for the general tab
        /// </summary>
        private Visibility generalTabVisibility;

        /// <summary>
        /// Visibility state for the process tab
        /// </summary>
        private Visibility processTabVisibility;

        /// <summary>
        /// Specifies if the raw materials tab should be selected by default
        /// </summary>
        private bool selectRawMaterialTab;

        /// <summary>
        /// A value indicating whether the shift information tab is selected.
        /// </summary>
        private bool selectShiftInformationTab;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="MassDataUpdateViewModel"/> class.
        /// </summary>
        /// <param name="messenger">The messenger service.</param>
        /// <param name="windowService">The window service.</param>
        [ImportingConstructor]
        public MassDataUpdateViewModel(IMessenger messenger, IWindowService windowService)
        {
            this.messenger = messenger;
            this.windowService = windowService;

            this.WindowClosing = new DelegateCommand<CancelEventArgs>(OnViewClosing);
            this.messenger.Register<NotificationMessage>(msg =>
            {
                if (msg.Notification == "LoadingEntity")
                {
                    this.WaitAnimationMessage = LocalizedResources.General_LoadingData;
                    this.IsLoadingEntity = true;
                }

                if (msg.Notification == "LoadingEntityFinished")
                {
                    this.IsLoadingEntity = false;
                }

                if (msg.Notification == "UpdateMassData")
                {
                    this.WaitAnimationMessage = LocalizedResources.MassDataUpdate_UpdatingMassData;
                    this.IsLoadingEntity = true;
                }

                if (msg.Notification == "UpdateMassDataFinished")
                {
                    this.IsLoadingEntity = false;
                }

                if (msg.Notification == "CloseMassDataUpdateWindow")
                {
                    // refresh the content of main view
                    this.windowService.CloseViewWindow(this);
                    this.messenger.Send(new NotificationMessage(Notification.ReloadMainViewCurrentContent));
                }

                if (msg.Notification == SetApplyUpdatesToSubObjectsTrue)
                {
                    this.ApplyUpdatesToSubObjects = true;
                }
            });
        }

        #region Commands

        /// <summary>
        /// Gets the command for the closing event on the view
        /// </summary>
        public ICommand WindowClosing { get; private set; }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the shift information view model.
        /// </summary>
        [Import]
        public MassUpdateShiftInformationViewModel ShiftInformationViewModel { get; set; }

        /// <summary>
        /// Gets or sets the quantity view model.
        /// </summary>
        [Import]
        public MassUpdateQuantityViewModel QuantityViewModel { get; set; }

        /// <summary>
        /// Gets or sets the quantity view model.
        /// </summary>
        [Import]
        public MassUpdateOverheadAndMarginViewModel OverheadAndMarginViewModel { get; set; }

        /// <summary>
        /// Gets or sets the quantity view model.
        /// </summary>
        [Import]
        public MassUpdateCountrySettingsViewModel CountrySettingsViewModel { get; set; }

        /// <summary>
        /// Gets or sets the quantity view model.
        /// </summary>
        [Import]
        public MassUpdateMachinesViewModel MachinesViewModel { get; set; }

        /// <summary>
        /// Gets or sets the quantity view model.
        /// </summary>
        [Import]
        public MassUpdateAdditionalCostViewModel AdditionalCostViewModel { get; set; }

        /// <summary>
        /// Gets or sets the raw materials view model.
        /// </summary>
        [Import]
        public MassUpdateRawMaterialsViewModel RawMaterialsViewModel { get; set; }

        /// <summary>
        /// Gets or sets the general view model.
        /// </summary>
        [Import]
        public MassUpdateGeneralViewModel GeneralViewModel { get; set; }

        /// <summary>
        /// Gets or sets the mass update process view model.
        /// </summary>
        [Import]
        public MassUpdateProcessViewModel ProcessViewModel { get; set; }

        /// <summary>
        /// Gets or sets the current Object reference
        /// </summary>
        public object CurrentObject
        {
            get
            {
                return this.currentObject;
            }

            set
            {
                if (this.currentObject != value)
                {
                    this.currentObject = value;
                    OnPropertyChanged(() => this.CurrentObject);
                    OnCurrentObjectChanged();

                    this.ShiftInformationViewModel.Model = value;
                    this.QuantityViewModel.Model = value;
                    this.OverheadAndMarginViewModel.Model = value;
                    this.CountrySettingsViewModel.Model = value;
                    this.MachinesViewModel.Model = value;
                    this.AdditionalCostViewModel.Model = value;
                    this.RawMaterialsViewModel.Model = value;
                    this.GeneralViewModel.Model = value;
                    this.ProcessViewModel.Model = value;
                }
            }
        }

        /// <summary>
        /// Sets the data context on which we commit the changes
        /// </summary>
        public IDataSourceManager DataContext
        {
            set
            {
                this.ShiftInformationViewModel.DataSourceManager = value;
                this.QuantityViewModel.DataSourceManager = value;
                this.OverheadAndMarginViewModel.DataSourceManager = value;
                this.CountrySettingsViewModel.DataSourceManager = value;
                this.MachinesViewModel.DataSourceManager = value;
                this.AdditionalCostViewModel.DataSourceManager = value;
                this.RawMaterialsViewModel.DataSourceManager = value;
                this.GeneralViewModel.DataSourceManager = value;
                this.ProcessViewModel.DataSourceManager = value;
            }
        }

        /// <summary>
        /// Gets or sets the title string
        /// </summary>
        public string Title
        {
            get
            {
                return this.title;
            }

            set
            {
                if (this.title != value)
                {
                    this.title = value;
                    OnPropertyChanged(() => this.Title);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to apply the updates to sub-objects.
        /// </summary>
        public bool ApplyUpdatesToSubObjects
        {
            get
            {
                return this.applyUpdatesToSubObjects;
            }

            set
            {
                if (this.applyUpdatesToSubObjects != value)
                {
                    this.applyUpdatesToSubObjects = value;
                    OnPropertyChanged(() => this.ApplyUpdatesToSubObjects);

                    ShiftInformationViewModel.ApplyUpdatesToSubObjects = value;
                    QuantityViewModel.ApplyUpdatesToSubObjects = value;
                    OverheadAndMarginViewModel.ApplyUpdatesToSubObjects = value;
                    CountrySettingsViewModel.ApplyUpdatesToSubObjects = value;
                    MachinesViewModel.ApplyUpdatesToSubObjects = value;
                    AdditionalCostViewModel.ApplyUpdatesToSubObjects = value;
                    RawMaterialsViewModel.ApplyUpdatesToSubObjects = value;
                    GeneralViewModel.ApplyUpdatesToSubObjects.Value = value;
                    ProcessViewModel.ApplyUpdatesToSubObjects = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the source entity is being loaded.
        /// </summary>        
        public bool IsLoadingEntity
        {
            get
            {
                return this.isLoadingEntity;
            }

            set
            {
                if (this.isLoadingEntity != value)
                {
                    this.isLoadingEntity = value;
                    OnPropertyChanged(() => this.IsLoadingEntity);
                }
            }
        }

        /// <summary>
        /// Gets or sets the wait animation message
        /// </summary>
        public string WaitAnimationMessage
        {
            get
            {
                return this.waitAnimationMessage;
            }

            set
            {
                if (this.waitAnimationMessage != value)
                {
                    this.waitAnimationMessage = value;
                    OnPropertyChanged(() => this.WaitAnimationMessage);
                }
            }
        }

        /// <summary>
        /// Gets or sets the visibility of the shift info tab
        /// </summary>
        public Visibility ShiftInformationTabVisibility
        {
            get
            {
                return this.shiftInformationTabVisibility;
            }

            set
            {
                if (this.shiftInformationTabVisibility != value)
                {
                    this.shiftInformationTabVisibility = value;
                    OnPropertyChanged(() => this.ShiftInformationTabVisibility);
                }
            }
        }

        /// <summary>
        /// Gets or sets the visibility of the quantity tab
        /// </summary>
        public Visibility QuantityTabVisibility
        {
            get
            {
                return this.quantityTabVisibility;
            }

            set
            {
                if (this.quantityTabVisibility != value)
                {
                    this.quantityTabVisibility = value;
                    OnPropertyChanged(() => this.QuantityTabVisibility);
                }
            }
        }

        /// <summary>
        /// Gets or sets the visibility of the oh and margin tab
        /// </summary>
        public Visibility OverheadAndMarginTabVisibility
        {
            get
            {
                return this.overheadAndMarginTabVisibility;
            }

            set
            {
                if (this.overheadAndMarginTabVisibility != value)
                {
                    this.overheadAndMarginTabVisibility = value;
                    OnPropertyChanged(() => this.OverheadAndMarginTabVisibility);
                }
            }
        }

        /// <summary>
        /// Gets or sets the visibility of the country settings tab
        /// </summary>
        public Visibility CountrySettingsTabVisibility
        {
            get
            {
                return this.countrySettingsTabVisibility;
            }

            set
            {
                if (this.countrySettingsTabVisibility != value)
                {
                    this.countrySettingsTabVisibility = value;
                    OnPropertyChanged(() => this.CountrySettingsTabVisibility);
                }
            }
        }

        /// <summary>
        /// Gets or sets the visibility of the machines tab
        /// </summary>
        public Visibility MachinesTabVisibility
        {
            get
            {
                return this.machinesTabVisibility;
            }

            set
            {
                if (this.machinesTabVisibility != value)
                {
                    this.machinesTabVisibility = value;
                    OnPropertyChanged(() => this.MachinesTabVisibility);
                }
            }
        }

        /// <summary>
        /// Gets or sets the visibility of the additional settings tab
        /// </summary>
        public Visibility AdditionalCostTabVisibility
        {
            get
            {
                return this.additionalCostTabVisibility;
            }

            set
            {
                if (this.additionalCostTabVisibility != value)
                {
                    this.additionalCostTabVisibility = value;
                    OnPropertyChanged(() => this.AdditionalCostTabVisibility);
                }
            }
        }

        /// <summary>
        /// Gets or sets the visibility of the raw materials tab
        /// </summary>
        public Visibility RawMaterialsTabVisibility
        {
            get
            {
                return this.rawMaterialsTabVisibility;
            }

            set
            {
                if (this.rawMaterialsTabVisibility != value)
                {
                    this.rawMaterialsTabVisibility = value;
                    OnPropertyChanged(() => this.RawMaterialsTabVisibility);
                }
            }
        }

        /// <summary>
        /// Gets or sets the visibility of the general tab
        /// </summary>
        public Visibility GeneralTabVisibility
        {
            get
            {
                return this.generalTabVisibility;
            }

            set
            {
                if (this.generalTabVisibility != value)
                {
                    this.generalTabVisibility = value;
                    OnPropertyChanged(() => this.GeneralTabVisibility);
                }
            }
        }

        /// <summary>
        /// Gets or sets the visibility of the general tab
        /// </summary>
        public Visibility ProcessTabVisibility
        {
            get
            {
                return this.processTabVisibility;
            }

            set
            {
                if (this.processTabVisibility != value)
                {
                    this.processTabVisibility = value;
                    OnPropertyChanged(() => this.ProcessTabVisibility);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the raw material tab is selected.
        /// </summary>
        public bool SelectRawMaterialTab
        {
            get
            {
                return this.selectRawMaterialTab;
            }

            set
            {
                if (this.selectRawMaterialTab != value)
                {
                    this.selectRawMaterialTab = value;
                    OnPropertyChanged(() => this.SelectRawMaterialTab);
                    this.RawMaterialsViewModel.IsTabSelected = value;
                    if (this.applyUpdatesToSubObjects)
                    {
                        this.RawMaterialsViewModel.ChangeMaterialsCollection();
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the shift information tab is selected.
        /// </summary>        
        public bool SelectShiftInformationTab
        {
            get { return this.selectShiftInformationTab; }
            set { this.SetProperty(ref this.selectShiftInformationTab, value, () => this.SelectShiftInformationTab); }
        }

        #endregion

        /// <summary>
        /// Method is called when an attempt is made to close the window
        /// </summary>
        /// <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        private void OnViewClosing(CancelEventArgs e)
        {
            if (this.isLoadingEntity)
            {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// Hides the unnecessary tabs
        /// </summary>
        private void OnCurrentObjectChanged()
        {
            ShiftInformationTabVisibility = Visibility.Visible;
            QuantityTabVisibility = Visibility.Visible;
            OverheadAndMarginTabVisibility = Visibility.Visible;
            CountrySettingsTabVisibility = Visibility.Visible;
            MachinesTabVisibility = Visibility.Visible;
            AdditionalCostTabVisibility = Visibility.Visible;
            RawMaterialsTabVisibility = Visibility.Visible;
            GeneralTabVisibility = Visibility.Visible;
            ProcessTabVisibility = Visibility.Visible;
            SelectRawMaterialTab = false;
            SelectShiftInformationTab = false;

            if (this.currentObject is Machine)
            {
                ShiftInformationTabVisibility = Visibility.Collapsed;
                QuantityTabVisibility = Visibility.Collapsed;
                OverheadAndMarginTabVisibility = Visibility.Collapsed;
                CountrySettingsTabVisibility = Visibility.Collapsed;
                AdditionalCostTabVisibility = Visibility.Collapsed;
                RawMaterialsTabVisibility = Visibility.Collapsed;
                GeneralTabVisibility = Visibility.Collapsed;
                ProcessTabVisibility = Visibility.Collapsed;
                SelectShiftInformationTab = true;
            }

            if (this.currentObject is Process || this.currentObject is ProcessStep)
            {
                OverheadAndMarginTabVisibility = Visibility.Collapsed;
                CountrySettingsTabVisibility = Visibility.Collapsed;
                AdditionalCostTabVisibility = Visibility.Collapsed;
                RawMaterialsTabVisibility = Visibility.Collapsed;
                GeneralTabVisibility = Visibility.Collapsed;
                SelectShiftInformationTab = true;
            }

            if (this.currentObject is RawMaterial)
            {
                ShiftInformationTabVisibility = Visibility.Collapsed;
                QuantityTabVisibility = Visibility.Collapsed;
                OverheadAndMarginTabVisibility = Visibility.Collapsed;
                CountrySettingsTabVisibility = Visibility.Collapsed;
                AdditionalCostTabVisibility = Visibility.Collapsed;
                MachinesTabVisibility = Visibility.Collapsed;
                GeneralTabVisibility = Visibility.Collapsed;
                ProcessTabVisibility = Visibility.Collapsed;
                SelectRawMaterialTab = true;
            }

            var objName = EntityUtils.GetEntityName(this.currentObject);
            if (!string.IsNullOrWhiteSpace(objName))
            {
                this.Title = LocalizedResources.General_MassDataUpdate + " - " + objName;
            }
            else
            {
                this.Title = LocalizedResources.General_MassDataUpdate;
            }
        }
    }
}
