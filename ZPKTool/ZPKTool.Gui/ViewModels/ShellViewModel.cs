﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;
using ZPKTool.Synchronization;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the shell view (thee root view of the application).
    /// </summary>
    [Export]
    public class ShellViewModel : ViewModel
    {
        #region Fields

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The wait handle for the abort execution.
        /// </summary>
        private static ManualResetEvent abortEvent = new ManualResetEvent(false);

        /// <summary>
        /// The composition container.
        /// </summary>
        private readonly CompositionContainer container;

        /// <summary>
        /// The window service.
        /// </summary>
        private readonly IWindowService windowService;

        /// <summary>
        /// The synchronization service.
        /// </summary>
        private readonly ISyncService syncService;

        /// <summary>
        /// The please wait service.
        /// </summary>
        private readonly IPleaseWaitService pleaseWaitService;

        /// <summary>
        /// The synchronization manager.
        /// </summary>
        private readonly ISynchronizationManager syncManager;

        /// <summary>
        /// The shell header view model.
        /// </summary>
        private readonly ShellHeaderViewModel headerViewModel;

        /// <summary>
        /// The content displayed in the shell.
        /// </summary>
        private object content;

        /// <summary>
        /// The visibility of the EnterRegistrationCode menu item
        /// </summary>
        private Visibility registrationMenuItemVisibility;

        /// <summary>
        /// The current zoom level.
        /// </summary>
        private double selectedZoomLevel;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ShellViewModel"/> class.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="messenger">The messenger service.</param>
        /// <param name="syncService">The synchronization service.</param>
        /// <param name="pleaseWaitService">The please wait service.</param>
        [ImportingConstructor]
        public ShellViewModel(
            CompositionContainer container,
            IWindowService windowService,
            IMessenger messenger,
            ISyncService syncService,
            IPleaseWaitService pleaseWaitService)
        {
            this.container = container;
            this.windowService = windowService;
            this.syncService = syncService;
            this.pleaseWaitService = pleaseWaitService;

            this.InitializeCommands();
            this.InitializeMainMenuContent();
            this.InitializeZoomLevel();

            this.syncManager = SynchronizationFactory.GetSyncManager();

            // Initialize the header
            this.headerViewModel = this.container.GetExportedValue<ShellHeaderViewModel>();

            // GoHome can be executed after log in and if not on the welcome screen
            this.headerViewModel.GoHomeCommand = new DelegateCommand(
                () => this.Content = this.container.GetExportedValue<WelcomeViewModel>(),
                () => SecurityManager.Instance.CurrentUser != null
                    && SecurityManager.Instance.CurrentUserRole != Role.None
                    && !(this.Content is WelcomeViewModel));

            messenger.Register<NotificationMessage>(HandleSystemMessages);
            messenger.Register<SynchronizationFinishedMessage>(HandleSynchronizationFinishedMessage);

            DisplayInitialView();
        }

        #endregion Constructor

        #region Commands

        /// <summary>
        /// Gets the display model viewer command.
        /// </summary>
        public ICommand DisplayModelViewerCommand { get; private set; }

        /// <summary>
        /// Gets the logout command.
        /// </summary>
        public ICommand LogoutCommand { get; private set; }

        /// <summary>
        /// Gets the exit command.
        /// </summary>
        public ICommand ExitCommand { get; private set; }

        /// <summary>
        /// Gets the change password command.
        /// </summary>
        public ICommand ChangePasswordCommand { get; private set; }

        /// <summary>
        /// Gets the register command.
        /// </summary>
        public ICommand LicenseManagementCommand { get; private set; }

        /// <summary>
        /// Gets the preferences command.
        /// </summary>
        public ICommand PreferencesCommand { get; private set; }

        /// <summary>
        /// Gets  the open help command.
        /// </summary>
        public ICommand OpenHelpCommand { get; private set; }

        /// <summary>
        /// Gets the open app data folder command.
        /// </summary>
        public ICommand OpenAppDataFolderCommand { get; private set; }

        /// <summary>
        /// Gets the open update command.
        /// </summary>
        public ICommand OpenUpdateCommand { get; private set; }

        /// <summary>
        /// Gets the send error report command.
        /// </summary>
        public ICommand SendErrorReportCommand { get; private set; }

        /// <summary>
        /// Gets the about command.
        /// </summary>
        public ICommand AboutCommand { get; private set; }

        /// <summary>
        /// Gets the PreviewKeyDown command.
        /// </summary>
        public ICommand PreviewKeyDownCommand { get; private set; }

        /// <summary>
        /// Gets the command executed before the shell view is closed.
        /// </summary>
        public ICommand ClosingCommand { get; private set; }

        /// <summary>
        /// Gets the Synchronize command.
        /// </summary>
        public ICommand SynchronizeCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets or sets the content of the shell.
        /// </summary>
        public object Content
        {
            get
            {
                return this.content;
            }

            set
            {
                OnContentChanged(value);
            }
        }

        /// <summary>
        /// Gets the shell header view-model.
        /// </summary>
        public ShellHeaderViewModel HeaderViewModel
        {
            get
            {
                return this.headerViewModel;
            }
        }

        /// <summary>
        /// Gets or sets the visibility of the EnterRegistrationCode menu item
        /// </summary>
        public Visibility RegistrationMenuItemVisibility
        {
            get
            {
                return this.registrationMenuItemVisibility;
            }

            set
            {
                if (this.registrationMenuItemVisibility != value)
                {
                    this.registrationMenuItemVisibility = value;
                    OnPropertyChanged("RegistrationMenuItemVisibility");
                }
            }
        }

        /// <summary>
        /// Gets the items to be displayed in the main menu.
        /// </summary>
        public ObservableCollection<SystemMenuItem> MainMenuItems { get; private set; }

        /// <summary>
        /// Gets the items to be listed in the zooming menu.
        /// </summary>
        public ObservableCollection<Tuple<string, double>> ZoomLevels { get; private set; }

        /// <summary>
        /// Gets or sets the current zoom level.
        /// </summary>
        public double SelectedZoomLevel
        {
            get
            {
                return this.selectedZoomLevel;
            }

            set
            {
                if (value != this.selectedZoomLevel)
                {
                    this.selectedZoomLevel = value;
                    this.OnPropertyChanged(() => this.SelectedZoomLevel);
                    this.OnZoomLevelChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the current zoom level text.
        /// </summary>
        public string SelectedZoomLevelText
        {
            get
            {
                return this.SelectedZoomLevel.ToString("### %", System.Globalization.CultureInfo.InvariantCulture);
            }

            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    double zoomLevel;
                    if (double.TryParse(value.Replace("%", string.Empty).Trim(), out zoomLevel))
                    {
                        zoomLevel = zoomLevel / 100d;
                        double minZoom = this.ZoomLevels.Min(z => z.Item2);
                        double maxZoom = this.ZoomLevels.Max(z => z.Item2);

                        if (zoomLevel < minZoom)
                        {
                            zoomLevel = minZoom;
                        }

                        if (zoomLevel > maxZoom)
                        {
                            zoomLevel = maxZoom;
                        }

                        this.SelectedZoomLevel = zoomLevel;
                    }
                }
            }
        }

        #endregion Properties

        /// <summary>
        /// Initializes the commands.
        /// </summary>
        private void InitializeCommands()
        {
            this.DisplayModelViewerCommand = new DelegateCommand(DisplayModelViewerAction);
            this.LogoutCommand = new DelegateCommand(LogoutAction, () => !(this.Content is LoginViewModel));
            this.ExitCommand = new DelegateCommand(ExitAction);
            this.ChangePasswordCommand = new DelegateCommand(ChangePasswordAction, CanChangePassword);
            this.LicenseManagementCommand = new DelegateCommand(LicenseManagementAction);
            this.PreferencesCommand = new DelegateCommand(PreferencesAction);
            this.OpenHelpCommand = new DelegateCommand(OpenHelpAction);
            this.OpenAppDataFolderCommand = new DelegateCommand(OpenAppDataFolderAction);
            this.OpenUpdateCommand = new DelegateCommand(OpenUpdateAction);
            this.SendErrorReportCommand = new DelegateCommand(SendErrorReportAction);
            this.AboutCommand = new DelegateCommand(AboutAction);
            this.PreviewKeyDownCommand = new DelegateCommand<KeyEventArgs>(PreviewKeyDownAction);
            this.ClosingCommand = new DelegateCommand<CancelEventArgs>(ClosingAction);
            this.SynchronizeCommand = new DelegateCommand<object>(this.Synchronize, this.CanSynchronize);
        }

        /// <summary>
        /// Decide which is the initial screen and display it.
        /// </summary>
        private void DisplayInitialView()
        {
            this.Content = this.container.GetExportedValue<LoginViewModel>();
        }

        /// <summary>
        /// Called when Content property has changed.
        /// </summary>
        /// <param name="newValue">The new value of the property.</param>
        private void OnContentChanged(object newValue)
        {
            if (this.content == newValue)
            {
                return;
            }

            // Unload the current content.
            if (!this.UnloadCurrentContent(false))
            {
                return;
            }

            // Load the new content
            this.content = newValue;
            OnPropertyChanged(() => this.Content);

            // Load the new content's main menu items as defined by its IMainMenuContentProvider implementation.
            var menuProvider = newValue as IMainMenuContentProvider;
            if (menuProvider != null)
            {
                foreach (var item in menuProvider.GetMainMenuItems())
                {
                    this.AddItemToMainMenu(item);
                }
            }

            // Execute the ILifeCycleManager.OnLoaded callback of the new content, if it is supported.
            var lifeCycleMngr = newValue as ILifeCycleManager;
            if (lifeCycleMngr != null)
            {
                lifeCycleMngr.OnLoaded();
            }
        }

        /// <summary>
        /// Unloads the current shell content.
        /// </summary>
        /// <param name="isAppClosing">A value that indicates whether the content unload is done before closing the application or not. [by default is set to true]</param>
        /// <returns>true if the content was unloaded; otherwise returns false.</returns>
        private bool UnloadCurrentContent(bool isAppClosing = true)
        {
            /*
             * DO NOT allow closing the application without the user confirmation
             * if a long process is running.
             */
            Window activeWindow = WindowHelper.GetApplicationWindows().FirstOrDefault(wnd => wnd.IsActive);
            if (isAppClosing && activeWindow != null && !activeWindow.IsEnabled)
            {
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_Exit, MessageDialogType.YesNo);
                if (result != MessageDialogResult.Yes)
                {
                    return false;
                }

                return true;
            }

            /*
             * DO NOT allow closing the application while
             * the synchronization abort is still running.
             */
            if (isAppClosing &&
                this.syncManager.CurrentStatus.State == SyncState.Aborting)
            {
                return false;
            }

            /*
             * Abort the synchronization before closing 
             * the application, if it still runs.
             */
            if (isAppClosing &&
                this.syncManager.CurrentStatus.State == SyncState.Running)
            {
                var result = this.windowService.MessageDialogService.Show(
                    LocalizedResources.Synchronization_AbortSyncOnAppClosingMessage,
                    MessageDialogType.YesNo);
                if (result == MessageDialogResult.Yes
                    && this.syncManager.CurrentStatus.State == SyncState.Running)
                {
                    Action<PleaseWaitService.WorkParams> work = (workparams) =>
                    {
                        abortEvent.Reset();
                        this.syncManager.AbortSynchronization();

                        /*
                         * Blocks the current thread until the synchronization abort 
                         * is finished. WaitHandle state will be set to signaled
                         * when the Synchronization abort is complete.
                         */
                        abortEvent.WaitOne();
                    };
                    Action<PleaseWaitService.WorkParams> workCompleted = (workparams) =>
                    {
                        if (workparams.Error != null)
                        {
                            Log.ErrorException(
                                "The update of the Sync Scopes could not be completed.",
                                workparams.Error);
                        }

                        /*
                         * Reset the WaitHandle, so that all future operations 
                         * waiting after this event to be locked.
                         */
                        abortEvent.Reset();
                        this.windowService.CloseViewWindow(this);
                    };

                    this.pleaseWaitService.Show(LocalizedResources.General_Aborting, work, workCompleted);
                }

                return false;
            }

            var currentContent = this.Content;
            if (currentContent == null)
            {
                return true;
            }

            var unloaded = true;

            // Execute the ILifeCycleManager.OnUnloading callback of the current content, if it is available.
            var lifeCycleMngrContent = currentContent as ILifeCycleManager;
            if (lifeCycleMngrContent != null)
            {
                unloaded = lifeCycleMngrContent.OnUnloading();
            }

            if (SecurityManager.Instance.CurrentUser != null)
            {
                // DO NOT allow closing the application while the trash bin's delete operation is still running.
                TrashBinViewModel trashBin = this.container.GetExportedValue<TrashBinViewModel>();
                if (isAppClosing &&
                    trashBin.IsDeleteInProgress == true)
                {
                    var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_TrashBinQuit, MessageDialogType.YesNo);
                    if (result != MessageDialogResult.Yes)
                    {
                        return false;
                    }

                    return true;
                }
            }

            if (unloaded)
            {
                // If the current content was successfully unloaded, remove its menu items from the main menu.
                var menuProvider = currentContent as IMainMenuContentProvider;
                if (menuProvider != null)
                {
                    foreach (var item in menuProvider.GetMainMenuItems())
                    {
                        this.MainMenuItems.Remove(item);
                    }
                }

                // Execute the ILifeCycleManager.OnUnloaded callback of the current content, if it is available.
                if (lifeCycleMngrContent != null)
                {
                    lifeCycleMngrContent.OnUnloaded();
                }
            }

            return unloaded;
        }

        /// <summary>
        /// Handles the system messages.
        /// </summary>
        /// <param name="message">The message.</param>
        private void HandleSystemMessages(NotificationMessage message)
        {
            if (message.Notification == Notification.DisableShellView)
            {
                if (InputManager.Current != null)
                {
                    InputManager.Current.PreProcessInput += new PreProcessInputEventHandler(PreProcessInput);
                }
            }
            else if (message.Notification == Notification.EnableShellView)
            {
                if (InputManager.Current != null)
                {
                    InputManager.Current.PreProcessInput -= new PreProcessInputEventHandler(PreProcessInput);
                }
            }
            else if (message.Notification == Notification.ShutdownApplication)
            {
                if (Application.Current != null)
                {
                    Application.Current.Shutdown();
                }
            }
            else if (message.Notification == Notification.RestartApplication)
            {
                // Unload the current document and restart application
                var canRestart = this.UnloadCurrentContent();
                if (canRestart && Application.Current != null)
                {
                    Application.Current.Exit += (s1, e1) =>
                    {
                        System.Diagnostics.Process thisProc = System.Diagnostics.Process.GetCurrentProcess();
                        thisProc.StartInfo.FileName = Application.ResourceAssembly.Location;
                        var filePathCurrentProcess = thisProc.StartInfo.FileName;

                        System.Diagnostics.Process appProcess = new System.Diagnostics.Process();
                        appProcess.StartInfo.FileName = typeof(Updater.Updater).Module.Name;
                        appProcess.StartInfo.Arguments = string.Format("/start \"{0}\"", filePathCurrentProcess);
                        appProcess.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                        appProcess.Start();
                    };

                    Application.Current.Shutdown();
                }
            }
        }

        /// <summary>
        /// Handles the SynchronizationFinished message.
        /// </summary>
        /// <param name="message">The message.</param>
        private void HandleSynchronizationFinishedMessage(SynchronizationFinishedMessage message)
        {
            /*
             * Sets the state of the WaitHandle to signaled, in order that the instructions that 
             * are waiting for the completion of the synchronization abort, to be executed.
             */
            abortEvent.Set();
        }

        /// <summary>
        /// Cancels the processing of the input events (mouse, keyboard). The user cannot interact with the application.
        /// Occurs when the InputManager starts to process the input item.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.PreProcessInputEventArgs"/> instance containing the event data.</param>
        private void PreProcessInput(object sender, PreProcessInputEventArgs e)
        {
            // Cancels the processing of the input event if the device that initiated
            // the event is not null (if the input was raised by the user).
            if (e.StagingItem.Input.Device != null)
            {
                e.StagingItem.Input.Handled = true;
            }
        }

        /// <summary>
        /// Initializes the application zoom.
        /// </summary>
        private void InitializeZoomLevel()
        {
            this.ZoomLevels = new ObservableCollection<Tuple<string, double>>();
            this.ZoomLevels.Add(new Tuple<string, double>(" 50 %", 0.50d));
            this.ZoomLevels.Add(new Tuple<string, double>(" 75 %", 0.75d));
            this.ZoomLevels.Add(new Tuple<string, double>(" 90 %", 0.90d));
            this.ZoomLevels.Add(new Tuple<string, double>(" 95 %", 0.95d));
            this.ZoomLevels.Add(new Tuple<string, double>("100 %", 1.00d));
            this.ZoomLevels.Add(new Tuple<string, double>("105 %", 1.05d));
            this.ZoomLevels.Add(new Tuple<string, double>("110 %", 1.10d));
            this.ZoomLevels.Add(new Tuple<string, double>("125 %", 1.25d));
            this.ZoomLevels.Add(new Tuple<string, double>("150 %", 1.50d));
            this.ZoomLevels.Add(new Tuple<string, double>("200 %", 2.00d));

            double minZoom = this.ZoomLevels.Min(z => z.Item2);
            double maxZoom = this.ZoomLevels.Max(z => z.Item2);

            if (UserSettingsManager.Instance.ZoomLevel < minZoom)
            {
                UserSettingsManager.Instance.ZoomLevel = minZoom;
            }
            else if (UserSettingsManager.Instance.ZoomLevel > maxZoom)
            {
                UserSettingsManager.Instance.ZoomLevel = maxZoom;
            }

            this.SelectedZoomLevel = UserSettingsManager.Instance.ZoomLevel != 0d ? UserSettingsManager.Instance.ZoomLevel : 1.0d;
        }

        /// <summary>
        /// Called when the SelectedZoomLevel property has changed.
        /// </summary>
        private void OnZoomLevelChanged()
        {
            // save selected zoom level in settings
            UserSettingsManager.Instance.ZoomLevel = this.SelectedZoomLevel;

            UserSettingsManager.Instance.Save();
        }

        #region Main menu

        /// <summary>
        /// Initializes the content of the main menu.
        /// </summary>
        private void InitializeMainMenuContent()
        {
            this.MainMenuItems = null;
            var menuItems = new ObservableCollection<SystemMenuItem>();

            var systemItem = new SystemMenuItem();
            systemItem.HeaderText = LocalizedResources.General_System;
            systemItem.AutomationId = "SystemMenuItem";
            systemItem.IsTopLevel = true;
            menuItems.Add(systemItem);

            var optionsItem = new SystemMenuItem();
            optionsItem.HeaderText = LocalizedResources.General_Options;
            optionsItem.AutomationId = "OptionsMenuItem";
            optionsItem.IsTopLevel = true;
            menuItems.Add(optionsItem);

            var helpItem = new SystemMenuItem();
            helpItem.HeaderText = LocalizedResources.General_Help;
            helpItem.IsTopLevel = true;
            helpItem.AutomationId = "HelpMenuItem";
            menuItems.Add(helpItem);

            // Model viewer content
            var modelViewerItem = new SystemMenuItem();
            modelViewerItem.HeaderText = LocalizedResources.General_ModelViewer;
            modelViewerItem.Command = this.DisplayModelViewerCommand;
            modelViewerItem.AutomationId = "ModelViewerMenuItem";
            systemItem.Items.Add(modelViewerItem);

            // System Menu content
            var logoutItem = new SystemMenuItem();
            logoutItem.HeaderText = LocalizedResources.General_LogOut;
            logoutItem.Command = this.LogoutCommand;
            logoutItem.AutomationId = "LogOutMenuItem";
            systemItem.Items.Add(logoutItem);

            var exitItem = new SystemMenuItem();
            exitItem.HeaderText = LocalizedResources.General_Exit;
            exitItem.Command = this.ExitCommand;
            exitItem.AutomationId = "ExitMenuItem";
            systemItem.Items.Add(exitItem);

            // Options Menu content
            var changePwdItem = new SystemMenuItem();
            changePwdItem.HeaderText = LocalizedResources.General_ChangePassword;
            changePwdItem.Command = this.ChangePasswordCommand;
            changePwdItem.AutomationId = "ChangePwdMenuItem";
            optionsItem.Items.Add(changePwdItem);

            var licenseManagementItem = new SystemMenuItem();
            licenseManagementItem.HeaderText = LocalizedResources.General_LicenseManagement;
            licenseManagementItem.Command = this.LicenseManagementCommand;
            licenseManagementItem.AutomationId = "LicenseManagementMenuItem";
            optionsItem.Items.Add(licenseManagementItem);

            var prefsItem = new SystemMenuItem();
            prefsItem.HeaderText = LocalizedResources.General_Preferences;
            prefsItem.Command = this.PreferencesCommand;
            prefsItem.AutomationId = "PreferencesMenuItem";
            optionsItem.Items.Add(prefsItem);

            // Help menu content
            var openHelpItem = new SystemMenuItem();
            openHelpItem.HeaderText = LocalizedResources.General_OpenHelpFile;
            openHelpItem.Command = this.OpenHelpCommand;
            openHelpItem.AutomationId = "OpenHelpMenuItem";
            helpItem.Items.Add(openHelpItem);

            var openAppDataFolderItem = new SystemMenuItem();
            openAppDataFolderItem.HeaderText = LocalizedResources.General_OpenAppDataFolder;
            openAppDataFolderItem.Command = this.OpenAppDataFolderCommand;
            openAppDataFolderItem.AutomationId = "OpenAppDataFolderMenuItem";
            helpItem.Items.Add(openAppDataFolderItem);

            var openUpdateItem = new SystemMenuItem();
            openUpdateItem.HeaderText = LocalizedResources.Update_CheckForUpdates;
            openUpdateItem.Command = this.OpenUpdateCommand;
            openUpdateItem.AutomationId = "CheckForUpdatesMenuItem";
            helpItem.Items.Add(openUpdateItem);

            var sendErrorReportItem = new SystemMenuItem();
            sendErrorReportItem.HeaderText = LocalizedResources.ErrorReport_SendErrorReport;
            sendErrorReportItem.Command = this.SendErrorReportCommand;
            sendErrorReportItem.AutomationId = "SendErrorReportMenuItem";
            helpItem.Items.Add(sendErrorReportItem);

            var separatorItem = new SystemMenuItem();
            separatorItem.IsSeparator = true;
            helpItem.Items.Add(separatorItem);

            var releaseNotesItem = new SystemMenuItem();
            releaseNotesItem.HeaderText = LocalizedResources.Update_ReleaseNotes;
            releaseNotesItem.Command = new DelegateCommand(this.ShowReleaseNotes);
            releaseNotesItem.AutomationId = "ReleaseNotesMenuItem";
            helpItem.Items.Add(releaseNotesItem);

            var aboutItem = new SystemMenuItem();
            aboutItem.HeaderText = LocalizedResources.General_About;
            aboutItem.Command = this.AboutCommand;
            aboutItem.AutomationId = "AboutAppMenuItem";
            helpItem.Items.Add(aboutItem);

#if DEBUG
            // Menu items for some development tasks, available only in debug mode.
            var developer = new SystemMenuItem();
            developer.HeaderText = "Developer";
            menuItems.Add(developer);

            var garbageCollect = new SystemMenuItem();
            garbageCollect.HeaderText = "GC Collect";
            garbageCollect.Command = new DelegateCommand(() =>
                {
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                });
            developer.Items.Add(garbageCollect);
#endif

            this.MainMenuItems = menuItems;
        }

        /// <summary>
        /// Adds a menu item into menu on the position specified by the <see cref="SystemMenuItem.Index" /> property.
        /// If the item specifies an invalid index, the item is added on the last position.
        /// If the item specify the same Index value as other items it is added after the last item with the specified index.
        /// </summary>
        /// <param name="item">The item to add.</param>
        private void AddItemToMainMenu(SystemMenuItem item)
        {
            if (item == null)
            {
                return;
            }

            int insertIndex = item.Index;
            if (insertIndex < 0 || insertIndex > this.MainMenuItems.Count)
            {
                insertIndex = this.MainMenuItems.Count;
            }
            else
            {
                var lastItem = this.MainMenuItems.LastOrDefault(it => it.Index == insertIndex);
                if (lastItem != null)
                {
                    insertIndex = this.MainMenuItems.IndexOf(lastItem);
                }
            }

            this.MainMenuItems.Insert(insertIndex, item);
        }

        #endregion Main menu

        #region Command actions

        /// <summary>
        /// Displays the model viewer action.
        /// </summary>
        private void DisplayModelViewerAction()
        {
            var modelBrowserViewModel = this.container.GetExportedValue<ModelBrowserViewModel>();
            modelBrowserViewModel.IsStartedDirectly = false;
            windowService.ShowViewInDialog(modelBrowserViewModel, handleEscKey: false, singleInstance: true);

            // Update the zoom level in the full app after closing the model browser view.
            this.SelectedZoomLevel = modelBrowserViewModel.SelectedZoomLevel;
        }

        /// <summary>
        /// The action performed by the logout command.
        /// </summary>
        private void LogoutAction()
        {
            if (!(this.Content is LoginViewModel))
            {
                if (this.syncManager.CurrentStatus.State == SyncState.Running)
                {
                    var result = this.windowService.MessageDialogService.Show(
                    LocalizedResources.Synchronization_AbortSyncOnLogoutMessage,
                    MessageDialogType.YesNo);
                    if (result == MessageDialogResult.Yes
                        && this.syncManager.CurrentStatus.State == SyncState.Running)
                    {
                        Action<PleaseWaitService.WorkParams> work = (workparams) =>
                        {
                            abortEvent.Reset();
                            this.syncManager.AbortSynchronization();

                            /*
                             * Blocks the current thread until the synchronization abort 
                             * is finished. WaitHandle state will be set to signaled
                             * when the Synchronization abort is complete.
                             */
                            abortEvent.WaitOne();
                        };
                        Action<PleaseWaitService.WorkParams> workCompleted = (workparams) =>
                        {
                            if (workparams.Error != null)
                            {
                                Log.ErrorException(
                                    "The update of the Sync Scopes could not be completed.",
                                    workparams.Error);
                            }

                            /*
                             * Reset the WaitHandle, so that all future operations 
                             * waiting after this event to be locked.
                             */
                            abortEvent.Reset();
                            this.DoLogout();
                        };

                        this.pleaseWaitService.Show(LocalizedResources.General_Aborting, work, workCompleted);
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    this.DoLogout();
                }
            }
        }

        /// <summary>
        /// Performed the logout operations.
        /// </summary>
        private void DoLogout()
        {
            SynchronizationFactory.ResetSyncManager();
            SecurityManager.Instance.Logout();
            this.Content = this.container.GetExportedValue<LoginViewModel>();

            IMessenger messenger = this.container.GetExportedValue<IMessenger>();
            messenger.Send<NotificationMessage>(new NotificationMessage(Notification.LoggedOut));
        }

        /// <summary>
        /// The action performed by the exit command.
        /// </summary>
        private void ExitAction()
        {
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// The action performed by the change password action.
        /// </summary>
        private void ChangePasswordAction()
        {
            var viewModel = this.container.GetExportedValue<ChangePasswordViewModel>();
            this.windowService.ShowViewInDialog(viewModel, "ChangePasswordViewTemplate");
        }

        /// <summary>
        /// Determines whether the ChangePassword action can execute.
        /// </summary>
        /// <returns>
        /// True if the action can be executed, false otherwise.
        /// </returns>
        private bool CanChangePassword()
        {
            return SecurityManager.Instance.CurrentUser != null;
        }

        /// <summary>
        /// The action invoked by the <see cref="LicenseManagementCommand"/>.
        /// </summary>
        private void LicenseManagementAction()
        {
            var viewModel = this.container.GetExportedValue<RegisterViewModel>();
            this.windowService.ShowViewInDialog(viewModel);
        }

        /// <summary>
        /// The action performed by the preferences command.
        /// </summary>
        private void PreferencesAction()
        {
            PreferencesViewModel prefs = this.container.GetExportedValue<PreferencesViewModel>();
            var windowService = this.container.GetExportedValue<IWindowService>();
            windowService.ShowViewInDialog(prefs);
        }

        /// <summary>
        /// The action performed by the open help action.
        /// </summary>
        private void OpenHelpAction()
        {
            string helpFilePath = AppDomain.CurrentDomain.BaseDirectory + @"\Resources\SystemDocumentation.chm";
            if (System.IO.File.Exists(helpFilePath))
            {
                System.Diagnostics.Process.Start(helpFilePath);
            }
            else
            {
                Log.Error("Help file not found: {0}", helpFilePath);
                this.windowService.MessageDialogService.Show(LocalizedResources.Error_HelpFileNotFound, MessageDialogType.Error);
            }
        }

        /// <summary>
        /// The action performed by the OpenAppDataFolder command.
        /// </summary>
        private void OpenAppDataFolderAction()
        {
            string args = "/n,/e," + Constants.ApplicationDataFolderPath;
            System.Diagnostics.Process.Start("explorer.exe", args);
        }

        /// <summary>
        /// The action performed by the OpenUpdate command.
        /// </summary>
        private void OpenUpdateAction()
        {
            if (this.syncManager.CurrentStatus.State == SyncState.Running)
            {
                var result = this.windowService.MessageDialogService.Show(
                LocalizedResources.Synchronization_AbortSyncOnUpdateAppMessage,
                MessageDialogType.YesNo);
                if (result == MessageDialogResult.Yes
                    && this.syncManager.CurrentStatus.State == SyncState.Running)
                {
                    Action<PleaseWaitService.WorkParams> work = (workparams) =>
                    {
                        abortEvent.Reset();
                        this.syncManager.AbortSynchronization();

                        /*
                         * Blocks the current thread until the synchronization abort 
                         * is finished. WaitHandle state will be set to signaled
                         * when the Synchronization abort is complete.
                         */
                        abortEvent.WaitOne();
                    };
                    Action<PleaseWaitService.WorkParams> workCompleted = (workparams) =>
                    {
                        if (workparams.Error != null)
                        {
                            Log.ErrorException(
                                "The synchronization abort could not be completed.",
                                workparams.Error);
                        }

                        /*
                         * Reset the WaitHandle, so that all future operations 
                         * waiting after this event to be locked.
                         */
                        abortEvent.Reset();
                        var viewModel = this.container.GetExportedValue<UpdateApplicationViewModel>();
                        this.windowService.ShowViewInDialog(viewModel);
                    };

                    this.pleaseWaitService.Show(LocalizedResources.General_Aborting, work, workCompleted);
                }
                else
                {
                    return;
                }
            }
            else
            {
                var viewModel = this.container.GetExportedValue<UpdateApplicationViewModel>();
                this.windowService.ShowViewInDialog(viewModel);
            }
        }

        /// <summary>
        /// The action performed by the SendErrorReport command.
        /// </summary>
        private void SendErrorReportAction()
        {
            var viewModel = new SendErrorReportViewModel(windowService, null);
            this.windowService.ShowViewInDialog(viewModel);
        }

        /// <summary>
        /// The action performed by the About command.
        /// </summary>
        private void AboutAction()
        {
            var viewModel = new AboutViewModel(this.windowService);
            this.windowService.ShowViewInDialog(viewModel);
        }

        /// <summary>
        /// The action performed by the PreviewKeyDown command.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.Input.KeyEventArgs"/> instance containing the event data.</param>
        private void PreviewKeyDownAction(KeyEventArgs e)
        {
            if (e.Key == Key.F1)
            {
                this.OpenHelpAction();
            }
        }

        /// <summary>
        /// The action performed by the Closing command.
        /// </summary>
        /// <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        private void ClosingAction(CancelEventArgs e)
        {
            // Unload the current content; if its unloading was canceled, cancel the shell closing.
            bool canClose = UnloadCurrentContent();
            if (!canClose)
            {
                e.Cancel = true;
            }
            else if (Application.Current != null)
            {
                Application.Current.Shutdown();
            }
        }

        /// <summary>
        /// Determines whether the Synchronize command can be executed.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        /// true if the command can be executed; otherwise, false.
        /// </returns>
        private bool CanSynchronize(object parameter)
        {
            return true;
        }

        /// <summary>
        /// The logic associated with the Synchronize command.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void Synchronize(object parameter)
        {
            var synchVm = this.container.GetExportedValue<SynchronizationViewModel>();
            this.windowService.ShowViewInDialog(synchVm);
        }

        /// <summary>
        /// Shows the release notes to the user.
        /// </summary>
        private void ShowReleaseNotes()
        {
            string releaseNotesPdfPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ReleaseNotes.pdf");
            if (System.IO.File.Exists(releaseNotesPdfPath))
            {
                try
                {
                    System.Diagnostics.Process.Start(releaseNotesPdfPath);
                }
                catch (Win32Exception ex)
                {
                    if (ex.NativeErrorCode == 1155)
                    {
                        // No pdf reader available. Display the .rtf version.
                        string releaseNotesRtfPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ReleaseNotes.rtf");
                        if (System.IO.File.Exists(releaseNotesRtfPath))
                        {
                            // There are very little chances that .rtf file is not associated with a program.
                            System.Diagnostics.Process.Start(releaseNotesRtfPath);
                        }
                        else
                        {
                            Log.Error("Release notes file not found: '{0}'.", releaseNotesRtfPath);
                            this.windowService.MessageDialogService.Show(LocalizedResources.Error_FileNotFound, MessageDialogType.Error);
                        }
                    }
                }
            }
            else
            {
                Log.Error("Release notes file not found: '{0}'.", releaseNotesPdfPath);
                this.windowService.MessageDialogService.Show(LocalizedResources.Error_FileNotFound, MessageDialogType.Error);
            }
        }

        #endregion Command actions
    }
}