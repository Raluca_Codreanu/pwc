﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.MvvmCore;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the LogEvents view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class LogEventsViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The data manager to work with.
        /// </summary>
        private IDataSourceManager dataSourceManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

        /// <summary>
        /// A list of the logs.
        /// </summary>
        private ObservableCollection<Log> logs;

        /// <summary>
        /// The start date.
        /// </summary>
        private DateTime? startDate = DateTime.Now.Date.Subtract(TimeSpan.FromDays(14));

        /// <summary>
        /// The end date.
        /// </summary>
        private DateTime? endDate = DateTime.Now.Date;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="LogEventsViewModel"/> class.
        /// </summary>
        [ImportingConstructor]
        public LogEventsViewModel()
        {
            this.logs = new ObservableCollection<Log>();
            this.DisplayLogEvents();
        }

        #region Properties

        /// <summary>
        /// Gets the data source that contains the whole logs.
        /// </summary>
        public ObservableCollection<Log> Logs
        {
            get { return this.logs; }
            private set { this.SetProperty(ref this.logs, value, () => this.Logs); }
        }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        public DateTime? StartDate
        {
            get
            {
                return this.startDate;
            }

            set
            {
                if (this.startDate != value)
                {
                    this.startDate = value;
                    OnPropertyChanging("StartDate");
                    DisplayLogEvents();
                }
            }
        }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        public DateTime? EndDate
        {
            get
            {
                return this.endDate;
            }

            set
            {
                if (this.endDate != value)
                {
                    this.endDate = value;
                    OnPropertyChanging("EndDate");
                    DisplayLogEvents();
                }
            }
        }

        #endregion

        /// <summary>
        /// Displays the log events.
        /// </summary>
        private void DisplayLogEvents()
        {
            IEnumerable<Log> logList = null;
            if (this.StartDate.HasValue && this.EndDate.HasValue)
            {
                logList = this.dataSourceManager.LogRepository.GetLogs(this.StartDate.Value, this.EndDate.Value).OrderByDescending(l => l.Date);
            }
            else
            {
                logList = this.dataSourceManager.LogRepository.GetAllLogs().OrderByDescending(l => l.Date);
            }

            this.Logs.Clear();
            this.Logs.AddRange(logList);
        }
    }
}
