﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Caching;
using System.Windows;
using System.Windows.Controls;
using ZPKTool.Business;
using ZPKTool.Business.Search;
using ZPKTool.Controls;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Resources;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// This is the tooltip that appears when hovering an item from the Search Tool search results.
    /// </summary>
    public class SearchTreeItemToolTip : ToolTip
    {
        /// <summary>
        /// Represents the  cost of entity.
        /// </summary>
        private object cost;

        /// <summary>
        /// Background worker used to calculate the cost of search entities.
        /// </summary>
        private BackgroundWorker calculateCostWorker;

        /// <summary>
        /// Reference to the entity behind the item
        /// </summary>
        private object entity;

        /// <summary>
        /// Flag specifying if the item is loaded from the central or local database
        /// </summary>
        private bool fromCentral;

        /// <summary>
        /// The measurement units adapter.
        /// </summary>
        private UnitsAdapter measurementUnitsAdapter;

        /// <summary>
        /// Initializes a new instance of the <see cref="SearchTreeItemToolTip"/> class.
        /// </summary>
        public SearchTreeItemToolTip()
        {
            this.Opened += new RoutedEventHandler(SearchItemToolTip_Opened);
            this.Closed += new RoutedEventHandler(SearchItemToolTip_Closed);

            this.calculateCostWorker = new BackgroundWorker();
        }

        /// <summary>
        /// The entity property is registered as a DP to support binding
        /// </summary>
        public static readonly DependencyProperty EntityProperty = DependencyProperty.Register("Entity", typeof(object), typeof(SearchTreeItemToolTip));

        /// <summary>
        /// Gets or sets the Entity property
        /// </summary>
        public object Entity
        {
            get { return (object)GetValue(EntityProperty); }
            set { SetValue(EntityProperty, value); }
        }

        /// <summary>
        /// The isFromCentral property is registered as a DP to support binding
        /// </summary>
        public static readonly DependencyProperty IsFromCentralProperty = DependencyProperty.Register("IsFromCentral", typeof(bool), typeof(SearchTreeItemToolTip));

        /// <summary>
        /// Gets or sets a value indicating whether the item represents an object from the central database.
        /// </summary>
        public bool IsFromCentral
        {
            get { return (bool)GetValue(IsFromCentralProperty); }
            set { SetValue(IsFromCentralProperty, value); }
        }

        /// <summary>
        /// Event handler for the Closed event of the tooltip
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void SearchItemToolTip_Closed(object sender, RoutedEventArgs e)
        {
            if (this.calculateCostWorker.IsBusy)
            {
                this.calculateCostWorker.Dispose();
            }

            this.Content = null;
        }

        /// <summary>
        /// Event handler for the Opened event of the tooltip
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void SearchItemToolTip_Opened(object sender, RoutedEventArgs e)
        {
            if (this.Entity == null)
            {
                return;
            }

            LoadTooltip();
        }
             
        /// <summary>
        /// Perform the calculation of cost asynchronously. 
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.ComponentModel.DoWorkEventArgs"/> instance containing the event data.</param>
        private void CalculateCostWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            var context = fromCentral ? ZPKTool.DataAccess.DbIdentifier.CentralDatabase : ZPKTool.DataAccess.DbIdentifier.LocalDatabase;

            // Cache the cost so you do not have to load the entity from database if the tooltip is diplayed again.
            ObjectCache cache = MemoryCache.Default;
            CacheItemPolicy policy = new CacheItemPolicy();
            policy.AbsoluteExpiration = DateTimeOffset.Now.AddMinutes(10);
            IIdentifiable guidIdentifable = this.entity as IIdentifiable;

            if (guidIdentifable.Guid != Guid.Empty)
            {
                this.cost = cache.Get(guidIdentifable.Guid.ToString());
            }

            if (cost == null)
            {
                if (this.entity is Assembly)
                {
                    this.cost = SearchResultsCostCalculator.CalculateCostOfSearchResultEntity(this.entity as Assembly, context);
                }
                else if (this.entity is Part)
                {
                    this.cost = SearchResultsCostCalculator.CalculateCostOfSearchResultEntity(this.entity as Part, context);
                }
                else if (this.entity is RawMaterial)
                {
                    this.cost = SearchResultsCostCalculator.CalculateCostOfSearchResultEntity(this.entity as RawMaterial, context);
                }
                else if (this.entity is Commodity)
                {
                    this.cost = SearchResultsCostCalculator.CalculateCostOfSearchResultEntity(this.entity as Commodity, context);
                }
                else if (this.entity is Consumable)
                {
                    this.cost = SearchResultsCostCalculator.CalculateCostOfSearchResultEntity(this.entity as Consumable, context);
                }
                else if (this.entity is Die)
                {
                    this.cost = SearchResultsCostCalculator.CalculateCostOfSearchResultEntity(this.entity as Die, context);
                }
                else
                {
                    Machine machine = this.entity as Machine;
                    if (machine != null)
                    {
                        this.cost = SearchResultsCostCalculator.CalculateCostOfSearchResultEntity(machine, context);
                    }
                }

                cache.Add(guidIdentifable.Guid.ToString(), cost, policy);
            }

            // Initialize the measurement units adapter and set the currencies and base currency of the entity's project.
            var dataManager = DataAccessFactory.CreateDataSourceManager(context);
            ICollection<Currency> currencies = null;
            Currency baseCurrency = null;

            CurrencyConversionManager.GetEntityCurrenciesAndBaseCurrency(this.entity, dataManager, out currencies, out baseCurrency);
            this.measurementUnitsAdapter = new UnitsAdapter(dataManager);
            this.measurementUnitsAdapter.Currencies = currencies;
            this.measurementUnitsAdapter.BaseCurrency = baseCurrency;
        }        
        
        /// <summary>
        /// Handles the completion of calculate cost process.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.RunWorkerCompletedEventArgs"/> instance containing the event data.</param>
        private void CalculateCostWorker_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                this.cost = null;
            }

            this.Content = SearchTreeItemToolTipGenerator.GenerateTooltipContent(this.Entity, this.cost, this.measurementUnitsAdapter);
        }

        /// <summary>
        /// Loads the tooltip
        /// </summary>
        private void LoadTooltip()
        {
            if (this.Content == null)
            {
                Grid contentOfToolTip = new Grid();
                contentOfToolTip.Height = 45;
                contentOfToolTip.Width = 50;
                contentOfToolTip.VerticalAlignment = VerticalAlignment.Stretch;
                CircularProgressControl waitControl = new CircularProgressControl();
                LabelControl waitMessage = new LabelControl(LocalizedResources.General_Loading);
                waitControl.Margin = new Thickness(1, 1, 1, 1);
                waitMessage.TextAlignment = TextAlignment.Center;
                waitMessage.Margin = new Thickness(1, 0, 1, 1);

                RowDefinition waitControlRow = new RowDefinition();
                GridLength heightOfFirstRow = new GridLength(30, GridUnitType.Pixel);
                waitControlRow.Height = heightOfFirstRow;
                contentOfToolTip.RowDefinitions.Add(waitControlRow);

                RowDefinition waitMessageRow = new RowDefinition();
                GridLength heightOfSecondRow = new GridLength(30, GridUnitType.Star);
                waitMessageRow.Height = heightOfFirstRow;
                contentOfToolTip.RowDefinitions.Add(waitMessageRow);

                Grid.SetRow(waitControl, 0);
                Grid.SetRow(waitMessage, 1);
                contentOfToolTip.Children.Add(waitControl);
                contentOfToolTip.Children.Add(waitMessage);

                this.Content = contentOfToolTip;

                this.entity = this.Entity;
                this.fromCentral = this.IsFromCentral;
                this.calculateCostWorker = new BackgroundWorker();
                calculateCostWorker.DoWork += new DoWorkEventHandler(CalculateCostWorker_DoWork);
                calculateCostWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(CalculateCostWorker_Completed);

                calculateCostWorker.RunWorkerAsync();
            }
        }
    }
}
