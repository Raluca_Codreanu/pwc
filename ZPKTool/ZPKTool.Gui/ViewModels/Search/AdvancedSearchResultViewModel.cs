﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using ZPKTool.Business.Search;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Managers;
using ZPKTool.Gui.Notifications;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The advanced search result ViewModel class.
    /// </summary>
    public class AdvancedSearchResultViewModel : ViewModel
    {
        #region Fields

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The type of the data.
        /// </summary>
        private Type searchedEntityType;

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AdvancedSearchResultViewModel" /> class.
        /// </summary>
        /// <param name="messenger">The messenger.</param>
        /// <param name="results">The results.</param>
        /// <param name="title">The title.</param>
        public AdvancedSearchResultViewModel(
            IMessenger messenger,
            IEnumerable<AdvancedSearchResult> results,
            string title)
        {
            Argument.IsNotNull("messenger", messenger);

            this.messenger = messenger;

            this.Title = title;
            this.SearchResults = new Collection<AdvancedSearchResult>(results.ToList());

            if (this.SearchResults.Count > 0)
            {
                this.SearchedEntityType = this.SearchResults.FirstOrDefault().Entity.GetType();
            }

            this.CopyCommand = new DelegateCommand<object>(this.CopyCommandAction);
            this.NavigateToObjectCommand = new DelegateCommand<object>(this.NavigateToObject);
        }

        #endregion Constructors

        #region Commands

        /// <summary>
        /// Gets the command that copies an item to the clipboard
        /// </summary>
        public ICommand CopyCommand { get; private set; }

        /// <summary>
        /// Gets the command that navigates in the main view to a specified object.
        /// </summary>        
        public ICommand NavigateToObjectCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets or sets the type of the data.
        /// </summary>
        public Type SearchedEntityType
        {
            get { return this.searchedEntityType; }
            set { this.SetProperty(ref this.searchedEntityType, value, () => this.SearchedEntityType, this.OnSearchedEntityTypeChanged); }
        }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the results.
        /// </summary>
        public Collection<AdvancedSearchResult> SearchResults { get; set; }

        /// <summary>
        /// Gets or sets the DataGrid's persist ID, depending on the loaded items' type.
        /// </summary>
        public string DataGridPersistId { get; set; }

        #endregion Properties

        #region Actions

        /// <summary>
        /// Copies an item to the clipboard.
        /// </summary>
        /// <param name="obj">the SearchTreeItem object is passed as a command parameter</param>
        private void CopyCommandAction(object obj)
        {
            if (obj != null)
            {
                var item = this.SearchResults.FirstOrDefault(o => o == obj);
                if (item != null)
                {
                    ClipboardManager.Instance.Copy(item.Entity, item.DatabaseId == DbIdentifier.CentralDatabase ? DbIdentifier.CentralDatabase : DbIdentifier.LocalDatabase);
                }
            }
        }

        /// <summary>
        /// Navigates to a specified object in the main view.
        /// </summary>
        /// <param name="obj">The object to navigate to.</param>
        private void NavigateToObject(object obj)
        {
            if (obj != null)
            {
                var item = this.SearchResults.FirstOrDefault(o => o == obj);
                if (item != null)
                {
                    var msg = new NavigateToEntityMessage(item.Entity, item.DatabaseId);
                    this.messenger.Send(msg, IsInViewerMode ? GlobalMessengerTokens.ModelBrowserTargetToken : GlobalMessengerTokens.MainViewTargetToken);
                }
            }
        }

        #endregion Actions

        /// <summary>
        /// Sets the DataGrid's persist ID according to the data type.
        /// </summary>
        private void OnSearchedEntityTypeChanged()
        {
            if (SearchedEntityType == typeof(Machine))
            {
                this.DataGridPersistId = "0120A5DC-612B-45E8-94EB-21071A18D30D";
            }
            else if (SearchedEntityType == typeof(RawPart))
            {
                this.DataGridPersistId = "08675C4E-26B9-4B79-BDEA-D234D6EB4B59";
            }
            else if (SearchedEntityType == typeof(Part))
            {
                this.DataGridPersistId = "0AA5244C-48BD-434D-9914-F79C499215FA";
            }
            else if (SearchedEntityType == typeof(Assembly))
            {
                this.DataGridPersistId = "BADDFE26-6239-4ADE-9A68-B34D87AA891D";
            }
            else if (SearchedEntityType == typeof(Die))
            {
                this.DataGridPersistId = "E1D0BEAA-8809-46F9-8134-728F24855EA1";
            }
            else if (SearchedEntityType == typeof(Commodity))
            {
                this.DataGridPersistId = "38F1FF3D-CEC4-4B98-B84A-518EFBA296AA";
            }
            else if (SearchedEntityType == typeof(Consumable))
            {
                this.DataGridPersistId = "C0096FBA-C6B4-44B1-8AD0-509E5EC2BF63";
            }
            else if (SearchedEntityType == typeof(RawMaterial))
            {
                this.DataGridPersistId = "1826D637-7E98-49E6-8E1A-DC433E39443B";
            }
            else if (SearchedEntityType == typeof(Customer))
            {
                this.DataGridPersistId = "131A72E3-D6F2-4094-AF0A-F461EA1988E5";
            }
            else if (SearchedEntityType == typeof(Manufacturer))
            {
                this.DataGridPersistId = "114DD1CB-17AF-4032-84CD-1B8727B55B55";
            }
            else if (SearchedEntityType == typeof(Project))
            {
                this.DataGridPersistId = "B7597604-2902-43EB-A9DF-14F98BC9581E";
            }
            else if (SearchedEntityType.IsSubclassOf(typeof(ProcessStep)))
            {
                this.DataGridPersistId = "363953E8-DB59-4813-B9F1-03AC15C9CDD0";
            }
        }
    }
}