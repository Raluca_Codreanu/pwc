﻿using System;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using ZPKTool.Common;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The configuration that defines a search.
    /// </summary>
    [Serializable]
    public class AdvancedSearchConfiguration : ObservableObject, ISerializable
    {
        #region Fields

        /// <summary>
        /// The title.
        /// </summary>
        private string title;

        /// <summary>
        /// The searched entity family.
        /// </summary>
        private string searchedEntityFamily;

        /// <summary>
        /// The search filters location.
        /// </summary>
        private string searchFiltersLocation;

        /// <summary>
        /// The search configuration items.
        /// </summary>
        private ObservableCollection<AdvancedSearchConfigurationItem> searchConfigurationItems;

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AdvancedSearchConfiguration"/> class.
        /// Don't delete, used for serialization default constructor.
        /// </summary>
        public AdvancedSearchConfiguration()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AdvancedSearchConfiguration" /> class.
        /// </summary>
        /// <param name="title">The title.</param>
        public AdvancedSearchConfiguration(string title)
        {
            this.Title = title;
            this.SearchConfigurationItems = new ObservableCollection<AdvancedSearchConfigurationItem>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AdvancedSearchConfiguration"/> class.
        /// </summary>
        /// <param name="info">The Serialization Info.</param>
        /// <param name="context">The Streaming Context.</param>
        public AdvancedSearchConfiguration(SerializationInfo info, StreamingContext context)
        {
            // Get the values from info and assign them to the appropriate properties.
            this.Title = (string)info.GetValue("Title", typeof(string));
            this.SearchedEntityFamily = (string)info.GetValue("searchedEntityFamily", typeof(string));
            this.SearchFiltersLocation = (string)info.GetValue("SearchFiltersLocation", typeof(string));
            this.SearchConfigurationItems = (ObservableCollection<AdvancedSearchConfigurationItem>)info.GetValue("SearchConfigurationItems", typeof(ObservableCollection<AdvancedSearchConfigurationItem>));
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        [XmlElement("Title")]
        public string Title
        {
            get { return this.title; }
            set { this.SetProperty(ref this.title, value, () => this.Title); }
        }

        /// <summary>
        /// Gets or sets the family of the searched entity.
        /// </summary>
        [XmlElement("searchedEntityFamily")]
        public string SearchedEntityFamily
        {
            get { return this.searchedEntityFamily; }
            set { this.SetProperty(ref this.searchedEntityFamily, value, () => this.SearchedEntityFamily); }
        }

        /// <summary>
        /// Gets or sets the search filters location.
        /// </summary>
        [XmlElement("SearchFiltersLocation")]
        public string SearchFiltersLocation
        {
            get { return this.searchFiltersLocation; }
            set { this.SetProperty(ref this.searchFiltersLocation, value, () => this.SearchFiltersLocation); }
        }

        /// <summary>
        /// Gets or sets the configuration properties.
        /// </summary>
        [XmlArray("SearchConfigurationItems"), XmlArrayItem("SearchConfigurationItem", typeof(AdvancedSearchConfigurationItem))]
        public ObservableCollection<AdvancedSearchConfigurationItem> SearchConfigurationItems
        {
            get { return this.searchConfigurationItems; }
            set { this.SetProperty(ref this.searchConfigurationItems, value, () => this.SearchConfigurationItems); }
        }

        /// <summary>
        /// Gets or sets the search result count.
        /// </summary>
        [XmlIgnore]
        public int SearchResultCount { get; set; }

        #endregion Properties

        #region ISerializable

        /// <summary>
        /// Populates a <see cref="T:System.Runtime.Serialization.SerializationInfo"/> with the data needed to serialize the target object.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo"/> to populate with data.</param>
        /// <param name="context">The destination (see <see cref="T:System.Runtime.Serialization.StreamingContext"/>) for this serialization.</param>
        /// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Title", this.Title);
            info.AddValue("SearchedEntityFamily", this.SearchedEntityFamily);
            info.AddValue("SearchFiltersLocation", this.SearchFiltersLocation);
            info.AddValue("SearchConfigurationItems", this.SearchConfigurationItems);
        }

        #endregion
    }
}