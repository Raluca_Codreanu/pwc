﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Windows.Input;
using System.Xml;
using System.Xml.Serialization;
using ZPKTool.Business.Search;
using ZPKTool.Common;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The advanced search ViewModel class.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class AdvancedSearchViewModel : ViewModel
    {
        #region Fields

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// Indicates whether a search is in progress or not.
        /// </summary>
        private volatile bool isSearchInProgress;

        /// <summary>
        /// The selected item.
        /// </summary>
        private object selectedItem;

        /// <summary>
        /// The PleaseWaitService reference.
        /// </summary>
        private IPleaseWaitService pleaseWaitService;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        /// <summary>
        /// The data source manager.
        /// </summary>
        private IDataSourceManager dataSourceManager;

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AdvancedSearchViewModel" /> class.
        /// </summary>
        /// <param name="messenger">The messenger.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="unitsService">The units service.</param>
        /// <param name="pleaseWaitService">The please wait service.</param>
        [ImportingConstructor]
        public AdvancedSearchViewModel(
            IMessenger messenger,
            IWindowService windowService,
            IUnitsService unitsService,
            IPleaseWaitService pleaseWaitService)
        {
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("unitsService", unitsService);
            Argument.IsNotNull("pleaseWaitService", pleaseWaitService);

            this.messenger = messenger;
            this.windowService = windowService;
            this.unitsService = unitsService;
            this.pleaseWaitService = pleaseWaitService;

            this.AdvancedSearchVMs = new DispatchedObservableCollection<ViewModel>();

            this.dataSourceManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(this.dataSourceManager);

            AdvancedSearchConfigurationViewModel.MeasurementUnitsAdapter = this.MeasurementUnitsAdapter;

            this.InitializeCommands();
        }

        #endregion Constructors

        #region Commands

        /// <summary>
        /// Gets the add search tab command.
        /// </summary>
        public ICommand AddSearchTabCommand { get; private set; }

        /// <summary>
        /// Gets the close search tab command.
        /// </summary>
        public ICommand CloseSearchTabCommand { get; private set; }

        /// <summary>
        /// Gets the search command.
        /// </summary>
        public ICommand SearchCommand { get; private set; }

        /// <summary>
        /// Gets the text box key down command.
        /// </summary>
        public ICommand TextBoxKeyDownCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets or sets the advanced search view models.
        /// </summary>
        public DispatchedObservableCollection<ViewModel> AdvancedSearchVMs { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the search is in progress.
        /// </summary>
        public bool IsSearchInProgress
        {
            get
            {
                return this.isSearchInProgress;
            }

            set
            {
                if (this.isSearchInProgress != value)
                {
                    this.isSearchInProgress = value;
                    this.OnPropertyChanged(() => this.IsSearchInProgress);
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected item.
        /// </summary>
        public object SelectedItem
        {
            get
            {
                return this.selectedItem;
            }

            set
            {
                if (this.selectedItem != value)
                {
                    this.selectedItem = value;
                    this.OnPropertyChanged(() => this.SelectedItem);
                }
            }
        }

        /// <summary>
        /// Gets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter { get; private set; }

        #endregion Properties

        #region Initializations

        /// <summary>
        /// Initializes the commands.
        /// </summary>
        private void InitializeCommands()
        {
            this.AddSearchTabCommand = new DelegateCommand(this.AddSearchTab);
            this.CloseSearchTabCommand = new DelegateCommand<ViewModel>(this.CloseSearchTab);

            this.SearchCommand = new DelegateCommand<object>(this.SearchAction, this.CanSearch);

            this.TextBoxKeyDownCommand = new DelegateCommand<KeyEventArgs>(this.KeyDownAction);
        }

        #endregion Initializations

        #region Commands handling

        /// <summary>
        /// Adds a search tab.
        /// </summary>
        private void AddSearchTab()
        {
            var searchNumber = this.AdvancedSearchVMs.Count + 1;
            var searchConfig = new AdvancedSearchConfiguration(LocalizedResources.General_Search + " " + searchNumber.ToString());
            this.AdvancedSearchVMs.Add(new AdvancedSearchConfigurationViewModel(searchConfig, this.windowService));
        }

        /// <summary>
        /// Closes a search tab.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        private void CloseSearchTab(ViewModel viewModel)
        {
            if (viewModel != null)
            {
                this.AdvancedSearchVMs.Remove(viewModel);
            }
        }

        #endregion Commands handling

        #region Command actions

        /// <summary>
        /// Action performed when the user presses a key in configuration items textbox.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.Input.KeyEventArgs"/> instance containing the event data.</param>
        private void KeyDownAction(KeyEventArgs e)
        {
            if (e.Key == Key.Return
                && !this.IsSearchInProgress
                && this.SelectedItem is AdvancedSearchConfigurationViewModel)
            {
                this.SearchAction(this.SelectedItem);
            }
        }

        #endregion Command actions

        #region Actions

        /// <summary>
        /// Determines whether this instance can search the specified parameter.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        /// true if this instance can search the specified parameter, false otherwise.
        /// </returns>
        private bool CanSearch(object parameter)
        {
            if (parameter == null
                || !(parameter is AdvancedSearchConfigurationViewModel)
                || this.IsSearchInProgress)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Action that starts the search. It is performed when the search command is triggered.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void SearchAction(object parameter)
        {
            this.IsSearchInProgress = true;

            var configurationVM = parameter as AdvancedSearchConfigurationViewModel;
            var configuration = configurationVM.SearchConfiguration;

            // If no search configuration has been made the search does not continue.
            if (configuration.SearchConfigurationItems.FirstOrDefault(i => i.FirstValue != null && (i.ValueInputOption == (int)ValueInputOptions.SingleValue || (i.ValueInputOption == (int)ValueInputOptions.Range && i.SecondValue != null))) == null)
            {
                this.IsSearchInProgress = false;
                this.windowService.MessageDialogService.Show(LocalizedResources.General_SearchNothingSpecified, MessageDialogType.Info);
                return;
            }

            var results = new Collection<AdvancedSearchResult>();

            Action<PleaseWaitService.WorkParams> work = (workParams) =>
            {
                var searchManager = new AdvancedSearchManager();
                var searchConfig = new Collection<AdvancedSearchBasicConfiguration>();

                foreach (var item in configuration.SearchConfigurationItems)
                {
                    searchConfig.Add(new AdvancedSearchBasicConfiguration(item.PropertyName, item.FirstValue, item.SecondValue, item.SearchedProperties, item.ConsumableAmountUnit));
                }

                searchManager.Configurations = searchConfig;
                searchManager.SelectedEntityType = configurationVM.SelectedEntityType;
                searchManager.ScopeSelection = configurationVM.ScopeSelection;
                results = searchManager.Search();
                
                if (results.Count > 0)
                {
                    configuration.SearchResultCount++;
                    this.AdvancedSearchVMs.Add(new AdvancedSearchResultViewModel(this.messenger, results, configuration.Title + " - Results " + configuration.SearchResultCount.ToString()));
                }
            };

            Action<PleaseWaitService.WorkParams> workCompleted = (workParams) =>
            {
                this.IsSearchInProgress = false;
                if (workParams.Error != null)
                {
                    windowService.MessageDialogService.Show(workParams.Error);
                }
                else if (results != null
                        && results.Count == 0)
                {
                    this.windowService.MessageDialogService.Show(LocalizedResources.General_SearchWithoutResults, MessageDialogType.Info);
                }
            };

            this.pleaseWaitService.Show(LocalizedResources.General_Searching, work, workCompleted);
        }

        #endregion Actions

        /// <summary>
        /// Called after the view has been loaded. Usually it performs some initialization that needs the view to be loaded, like
        /// starting to load data from a database.
        /// <para />
        /// During unit tests this method must be manually called because there is no view to call it.
        /// </summary>
        public override void OnLoaded()
        {
            base.OnLoaded();

            this.DeserializeConfigurations();
        }

        /// <summary>
        /// Called after the view has been unloaded from the parent or closed.
        /// </summary>
        public override void OnUnloaded()
        {
            base.OnUnloaded();

            this.SerializeConfigurations();
        }

        #region Serialization / Deserialization

        /// <summary>
        /// Serializes the configurations.
        /// </summary>
        private void SerializeConfigurations()
        {
            try
            {
                var search = new AdvancedSearch();
                foreach (var searchVM in this.AdvancedSearchVMs)
                {
                    var configurationVM = searchVM as AdvancedSearchConfigurationViewModel;
                    if (configurationVM != null)
                    {
                        search.SearchConfigurations.Add(configurationVM.SearchConfiguration);
                    }
                }

                // Remove all the configuration items without property.
                foreach (var configuration in search.SearchConfigurations)
                {
                    var itemsToRemove = new List<AdvancedSearchConfigurationItem>();
                    foreach (var item in configuration.SearchConfigurationItems)
                    {
                        if (string.IsNullOrEmpty(item.PropertyName))
                        {
                            itemsToRemove.Add(item);
                        }
                    }

                    foreach (var item in itemsToRemove)
                    {
                        configuration.SearchConfigurationItems.Remove(item);
                    }
                }

                // Remove all the search configurations without an entity type or without configuration items.
                var configToRemove = new List<AdvancedSearchConfiguration>();
                foreach (var config in search.SearchConfigurations)
                {
                    if (string.IsNullOrEmpty(config.SearchedEntityFamily) || config.SearchConfigurationItems.Count == 0)
                    {
                        configToRemove.Add(config);
                    }
                }

                foreach (var config in configToRemove)
                {
                    search.SearchConfigurations.Remove(config);
                }

                // Serialize the search configurations.
                using (var stream = new FileStream(AdvancedSearch.ConfigurationsFilePath, FileMode.Create))
                {
                    var serializer = new XmlSerializer(typeof(AdvancedSearch));
                    serializer.Serialize(stream, search);
                }
            }
            catch (Exception ex)
            {
                log.ErrorException("Exception occurred while trying to create the advanced search configurations.", ex);
            }
        }

        /// <summary>
        /// Deserializes the configurations.
        /// </summary>
        private void DeserializeConfigurations()
        {
            try
            {
                var configurationsFilePath = AdvancedSearch.ConfigurationsFilePath;
                if (File.Exists(configurationsFilePath))
                {
                    using (var stream = new FileStream(configurationsFilePath, FileMode.Open))
                    {
                        var reader = new XmlTextReader(stream);
                        var serializer = new XmlSerializer(typeof(AdvancedSearch));
                        var search = serializer.Deserialize(reader) as AdvancedSearch;

                        foreach (var configuration in search.SearchConfigurations)
                        {
                            this.AdvancedSearchVMs.Add(new AdvancedSearchConfigurationViewModel(configuration, this.windowService));
                        }

                        // Remove the configurations with invalid entity type.
                        var vmsToRemove = new List<ViewModel>();
                        foreach (var vm in this.AdvancedSearchVMs)
                        {
                            var configurationVM = vm as AdvancedSearchConfigurationViewModel;
                            if (configurationVM != null)
                            {
                                if (configurationVM.SelectedEntityFamily == SearchFilter.None)
                                {
                                    vmsToRemove.Add(vm);
                                }
                            }
                        }

                        foreach (var vm in vmsToRemove)
                        {
                            this.AdvancedSearchVMs.Remove(vm);
                        }
                    }

                    this.SelectedItem = this.AdvancedSearchVMs.LastOrDefault();
                }

                this.SelectedItem = this.AdvancedSearchVMs.LastOrDefault();
            }
            catch (Exception ex)
            {
                log.ErrorException("Exception occurred while trying to load the advanced search configurations.", ex);
            }
        }

        #endregion Serialization / Deserialization
    }
}