﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using ZPKTool.Common;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The advanced search class that contains the search configurations.
    /// </summary>
    [Serializable]
    public class AdvancedSearch : ObservableObject, ISerializable
    {
        /// <summary>
        /// The name of the configurations file.
        /// </summary>
        internal static readonly string ConfigurationFileName = "AdvancedSearchConfigurations.xml";

        /// <summary>
        /// The full path of the configurations file.
        /// </summary>
        internal static readonly string ConfigurationsFilePath = Path.Combine(Constants.ApplicationDataFolderPath, ConfigurationFileName);

        #region Fields

        /// <summary>
        /// The search configurations.
        /// </summary>
        private Collection<AdvancedSearchConfiguration> searchConfigurations;

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AdvancedSearch" /> class.
        /// </summary>
        public AdvancedSearch()
        {
            this.SearchConfigurations = new Collection<AdvancedSearchConfiguration>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AdvancedSearch" /> class.
        /// </summary>
        /// <param name="info">The info.</param>
        /// <param name="context">The context.</param>
        public AdvancedSearch(SerializationInfo info, StreamingContext context)
        {
            this.SearchConfigurations = (Collection<AdvancedSearchConfiguration>)info.GetValue("SearchConfigurations", typeof(Collection<AdvancedSearchConfiguration>));
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Gets or sets the search configurations.
        /// </summary>
        [XmlArray("SearchConfigurations"), XmlArrayItem("SearchConfiguration", typeof(AdvancedSearchConfiguration))]
        public Collection<AdvancedSearchConfiguration> SearchConfigurations
        {
            get { return this.searchConfigurations; }
            set { this.SetProperty(ref this.searchConfigurations, value, () => this.SearchConfigurations); }
        }

        #endregion Propeties

        #region ISerializable

        /// <summary>
        /// Populates a <see cref="T:System.Runtime.Serialization.SerializationInfo" /> with the data needed to serialize the target object.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> to populate with data.</param>
        /// <param name="context">The destination (see <see cref="T:System.Runtime.Serialization.StreamingContext" />) for this serialization.</param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("SearchConfigurations", this.SearchConfigurations);
        }

        #endregion
    }
}