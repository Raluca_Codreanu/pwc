﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;
using ZPKTool.Business.Search;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Common;
using ZPKTool.Controls;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Resources;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The advanced search configuration ViewModel class.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class AdvancedSearchConfigurationViewModel : ViewModel
    {
        #region Fields

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The selected entity family.
        /// </summary>
        private SearchFilter selectedEntityFamily;

        /// <summary>
        /// The selected entity properties.
        /// </summary>
        private Collection<EntityProperty> selectedEntityProperties = new Collection<EntityProperty>();

        /// <summary>
        /// Specifies if the scope popup is open.
        /// </summary>
        private bool scopePopupIsOpen;

        /// <summary>
        /// The scope selection.
        /// </summary>
        private SearchScopeFilter scopeSelection;

        /// <summary>
        /// The text in the scope label.
        /// </summary>
        private string scopeText;

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AdvancedSearchConfigurationViewModel"/> class.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <param name="windowsService">The windows service.</param>
        public AdvancedSearchConfigurationViewModel(AdvancedSearchConfiguration configuration, IWindowService windowsService)
        {
            this.SearchConfiguration = configuration;
            this.windowService = windowsService;

            this.InitializeConfiguration();
            this.CheckConfiguration();

            this.ScopeText = LocalizedResources.General_None;
            this.ScopePopupIsOpen = false;

            this.LoadSearchLocations();

            this.InitializeCommands();
            this.InitializeProperties();
        }

        #endregion Constructors

        /// <summary>
        /// Gets or sets the measurement units adapter.
        /// </summary>
        public static UnitsAdapter MeasurementUnitsAdapter { get; set; }

        #region Commands

        /// <summary>
        /// Gets the add property command.
        /// </summary>
        public ICommand AddPropertyCommand { get; private set; }

        /// <summary>
        /// Gets the remove property command.
        /// </summary>
        public ICommand RemovePropertyCommand { get; private set; }

        /// <summary>
        /// Gets the selection changed command.
        /// </summary>
        public ICommand SelectionChangedCommand { get; private set; }

        /// <summary>
        /// Gets the command for opening the scope popup
        /// </summary>
        public ICommand ShowScopePopupCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets or sets the selected entity family.
        /// </summary>
        public SearchFilter SelectedEntityFamily
        {
            get
            {
                return this.selectedEntityFamily;
            }

            set
            {
                if (this.selectedEntityFamily != value)
                {
                    if (this.SelectedEntityFamily != SearchFilter.None)
                    {
                        this.SearchConfiguration.SearchConfigurationItems.Clear();
                    }

                    this.selectedEntityFamily = value;
                    this.SelectedEntityProperties = this.GetEntityProperties(value);
                    this.SearchConfiguration.SearchedEntityFamily = value.ToString();

                    // Set the selected entity type to the configuration items, needed after deserialization.
                    foreach (var item in this.SearchConfiguration.SearchConfigurationItems)
                    {
                        item.EntityType = this.SelectedEntityType;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the type of the selected entity.
        /// </summary>
        public Type SelectedEntityType { get; set; }

        /// <summary>
        /// Gets or sets the properties of the selected entity.
        /// </summary>
        public Collection<EntityProperty> SelectedEntityProperties
        {
            get { return this.selectedEntityProperties; }
            set { this.selectedEntityProperties = value; }
        }

        /// <summary>
        /// Gets the entity types.
        /// </summary>
        public Collection<object> EntityTypes
        {
            get
            {
                var entityTypes = new Collection<object>();
                entityTypes.Add(new Tuple<string, SearchFilter>(LocalizedResources.General_Projects, SearchFilter.Projects));
                entityTypes.Add(new Tuple<string, SearchFilter>(LocalizedResources.General_Assemblies, SearchFilter.Assemblies));
                entityTypes.Add(new Tuple<string, SearchFilter>(LocalizedResources.General_Parts, SearchFilter.Parts));
                entityTypes.Add(new Tuple<string, SearchFilter>(LocalizedResources.General_ProcessSteps, SearchFilter.ProcessSteps));
                entityTypes.Add(new Tuple<string, SearchFilter>(LocalizedResources.General_Machines, SearchFilter.Machines));
                entityTypes.Add(new Tuple<string, SearchFilter>(LocalizedResources.General_RawMaterials, SearchFilter.RawMaterials));
                entityTypes.Add(new Tuple<string, SearchFilter>(LocalizedResources.General_Commodities, SearchFilter.Commodities));
                entityTypes.Add(new Tuple<string, SearchFilter>(LocalizedResources.General_Consumables, SearchFilter.Consumables));
                entityTypes.Add(new Tuple<string, SearchFilter>(LocalizedResources.General_Dies, SearchFilter.Dies));
                entityTypes.Add(new Tuple<string, SearchFilter>(LocalizedResources.General_RawParts, SearchFilter.RawParts));

                return entityTypes;
            }
        }

        /// <summary>
        /// Gets the search configuration.
        /// </summary>
        public AdvancedSearchConfiguration SearchConfiguration { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether the scope popup is open or not.
        /// </summary>
        public bool ScopePopupIsOpen
        {
            get
            {
                return this.scopePopupIsOpen;
            }

            set
            {
                if (this.scopePopupIsOpen != value)
                {
                    this.scopePopupIsOpen = value;
                    OnPropertyChanged("ScopePopupIsOpen");

                    if (value == false)
                    {
                        this.SaveSearchLocations();
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the scope selection property.
        /// </summary>
        public SearchScopeFilter ScopeSelection
        {
            get
            {
                return this.scopeSelection;
            }

            set
            {
                if (this.scopeSelection != value)
                {
                    this.scopeSelection = value;
                    OnPropertyChanged("ScopeSelection");
                    this.RefreshSearchLocationsText();
                }
            }
        }

        /// <summary>
        /// Gets or sets the scope text property.
        /// </summary>
        public string ScopeText
        {
            get
            {
                return this.scopeText;
            }

            set
            {
                if (this.scopeText != value)
                {
                    this.scopeText = value;
                    OnPropertyChanged("ScopeText");
                }
            }
        }

        /// <summary>
        /// Gets the users.
        /// </summary>
        public Collection<User> Users { get; private set; }

        /// <summary>
        /// Gets the set of available calculation variants.
        /// </summary>
        public Collection<string> CalculationVariants { get; private set; }

        /// <summary>
        /// Gets a list of price of Units which populates the combobox.
        /// </summary>
        public Collection<MeasurementUnit> PriceUnitBaseItems { get; private set; }

        /// <summary>
        /// Gets the collection of RawMaterialDeliveryTypes available.
        /// </summary>
        public Collection<RawMaterialDeliveryType> RawMaterialDeliveryTypes { get; private set; }

        #endregion Properties

        #region Initialization

        /// <summary>
        /// Initializes the commands.
        /// </summary>
        private void InitializeCommands()
        {
            this.AddPropertyCommand = new DelegateCommand(this.AddProperty);
            this.RemovePropertyCommand = new DelegateCommand<AdvancedSearchConfigurationItem>(this.RemoveProperty);
            this.SelectionChangedCommand = new DelegateCommand<SelectionChangedEventArgs>(this.SelectionChanged);
            this.ShowScopePopupCommand = new DelegateCommand(() => this.ScopePopupIsOpen = true);
        }

        /// <summary>
        /// Initializes the properties.
        /// </summary>
        private void InitializeProperties()
        {
            this.CalculationVariants = new Collection<string>(CostCalculatorFactory.CalculationVersions);

            var dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

            this.Users = new Collection<User>(dataManager.UserRepository.GetAll(false));

            this.RawMaterialDeliveryTypes = dataManager.RawMaterialDeliveryTypeRepository.FindAll();

            this.PriceUnitBaseItems = new Collection<MeasurementUnit>();
            var units = dataManager.MeasurementUnitRepository.GetBaseMeasurementUnits();
            var priceUnits = units.Where(unit =>
                unit.Type == MeasurementUnitType.Weight ||
                unit.Type == MeasurementUnitType.Volume ||
                unit.Type == MeasurementUnitType.Volume2 ||
                unit.Type == MeasurementUnitType.Pieces ||
                unit.Type == MeasurementUnitType.Length ||
                unit.Type == MeasurementUnitType.Area);

            foreach (MeasurementUnit unit in priceUnits)
            {
                this.PriceUnitBaseItems.Add(unit);
            }
        }

        /// <summary>
        /// Initializes the configuration.
        /// </summary>
        private void InitializeConfiguration()
        {
            if (this.SearchConfiguration != null
                && this.SearchConfiguration.SearchedEntityFamily != null
                && Enum.IsDefined(typeof(SearchFilter), this.SearchConfiguration.SearchedEntityFamily))
            {
                this.SelectedEntityFamily = (SearchFilter)Enum.Parse(typeof(SearchFilter), this.SearchConfiguration.SearchedEntityFamily, true);

                foreach (var item in this.SearchConfiguration.SearchConfigurationItems)
                {
                    var entityProperty = this.SelectedEntityProperties.FirstOrDefault(p => p.PropertyName == item.PropertyName);
                    if (entityProperty != null
                        && entityProperty.SearchedProperties != null)
                    {
                        item.SearchedProperties.AddRange(entityProperty.SearchedProperties);
                    }
                }
            }
            else
            {
                this.SelectedEntityFamily = SearchFilter.None;
            }
        }

        /// <summary>
        /// Checks the search configuration and removes the invalid data.
        /// </summary>
        private void CheckConfiguration()
        {
            // Remove all the items with invalid entity property name.
            var itemsToRemove = new List<AdvancedSearchConfigurationItem>();
            foreach (var item in this.SearchConfiguration.SearchConfigurationItems)
            {
                var property = this.SelectedEntityProperties.FirstOrDefault(p => p.PropertyName == item.PropertyName);
                if (property == null)
                {
                    itemsToRemove.Add(item);
                }
            }

            foreach (var item in itemsToRemove)
            {
                this.SearchConfiguration.SearchConfigurationItems.Remove(item);
            }

            if (this.SearchConfiguration.SearchConfigurationItems.Count == 0)
            {
                this.SelectedEntityFamily = SearchFilter.None;
            }
        }

        #endregion Initialization

        #region Commands handling

        /// <summary>
        /// Adds a property.
        /// </summary>
        private void AddProperty()
        {
            if (this.SelectedEntityFamily == SearchFilter.None)
            {
                return;
            }

            var item = new AdvancedSearchConfigurationItem(this.windowService);

            // Add the default property values of the selected entity type to the new configuration item.
            var defaultProperty = this.SelectedEntityProperties.FirstOrDefault(p => p.IsDefault);
            if (defaultProperty != null)
            {
                item.PropertyName = defaultProperty.PropertyName;
                item.ValueControlType = (short)defaultProperty.ControlType;
                item.EntityType = this.SelectedEntityType;

                if (defaultProperty.SearchedProperties != null)
                {
                    item.SearchedProperties.AddRange(defaultProperty.SearchedProperties);
                }
            }

            this.SearchConfiguration.SearchConfigurationItems.Add(item);
        }

        /// <summary>
        /// Removes a property.
        /// </summary>
        /// <param name="item">The item.</param>
        private void RemoveProperty(AdvancedSearchConfigurationItem item)
        {
            if (item != null)
            {
                this.SearchConfiguration.SearchConfigurationItems.Remove(item);
            }
        }

        /// <summary>
        /// Handles the SelectionChanged event of the properties ComboBox control.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.Controls.SelectionChangedEventArgs" /> instance containing the event data.</param>
        private void SelectionChanged(SelectionChangedEventArgs e)
        {
            var addedProperty = e.AddedItems.Count != 0 ? e.AddedItems[0] as EntityProperty : null;
            if (addedProperty != null)
            {
                // The values are set to all the items with the same property name because we only know the property name and not exact item.
                var items = this.SearchConfiguration.SearchConfigurationItems.Where(i => i.PropertyName == addedProperty.PropertyName);
                if (items != null)
                {
                    foreach (var item in items.Where(it => it != null))
                    {
                        item.ValueControlType = (short)addedProperty.ControlType;
                        item.EntityType = this.SelectedEntityType;

                        if (addedProperty.SearchedProperties != null)
                        {
                            item.SearchedProperties.AddRange(addedProperty.SearchedProperties);
                        }
                    }
                }
            }
        }

        #endregion Commands handling

        #region Search Locations

        /// <summary>
        /// Loads the search locations.
        /// </summary>
        private void LoadSearchLocations()
        {
            string filtersLocations = this.SearchConfiguration.SearchFiltersLocation;
            SearchScopeFilter searchScope = SearchScopeFilter.None;

            // If the filterLocation is empty set the as default all filters and scopes.
            if (string.IsNullOrWhiteSpace(filtersLocations))
            {
                // Default - all search scopes.
                searchScope |= SearchScopeFilter.MasterData;
                searchScope |= SearchScopeFilter.MyProjects;
                searchScope |= SearchScopeFilter.OtherUsersProjects;
                searchScope |= SearchScopeFilter.ReleasedProjects;
            }
            else
            {
                string[] searchSettings = !string.IsNullOrWhiteSpace(filtersLocations) ? filtersLocations.Split('/') : null;
                string[] searchLocation = searchSettings != null && searchSettings[1] != null ? searchSettings[1].Split(',') : null;
                int i = 0;

                if (searchLocation != null)
                {
                    i = 0;
                    foreach (string location in searchLocation)
                    {
                        if (location == "True")
                        {
                            switch (i)
                            {
                                case 0:
                                    searchScope |= SearchScopeFilter.MasterData;
                                    break;
                                case 1:
                                    searchScope |= SearchScopeFilter.MyProjects;
                                    break;
                                case 2:
                                    searchScope |= SearchScopeFilter.OtherUsersProjects;
                                    break;
                                case 3:
                                    searchScope |= SearchScopeFilter.ReleasedProjects;
                                    break;
                                default:
                                    break;
                            }
                        }

                        i++;
                    }
                }
            }

            this.ScopeSelection = searchScope;
        }

        /// <summary>
        /// Saves the search locations.
        /// </summary>
        private void SaveSearchLocations()
        {
            string scope = string.Empty;

            if ((this.ScopeSelection & SearchScopeFilter.MasterData) != 0)
            {
                scope += "/True";
            }
            else
            {
                scope += "/False";
            }

            if ((this.ScopeSelection & SearchScopeFilter.MyProjects) != 0)
            {
                scope += ",True";
            }
            else
            {
                scope += ",False";
            }

            if ((this.ScopeSelection & SearchScopeFilter.OtherUsersProjects) != 0)
            {
                scope += ",True";
            }
            else
            {
                scope += ",False";
            }

            if ((this.ScopeSelection & SearchScopeFilter.ReleasedProjects) != 0)
            {
                scope += ",True";
            }
            else
            {
                scope += ",False";
            }

            this.SearchConfiguration.SearchFiltersLocation = scope;
        }

        /// <summary>
        /// Refreshes the search locations text.
        /// </summary>
        private void RefreshSearchLocationsText()
        {
            this.ScopeText = string.Empty;
            if (this.ScopeSelection == SearchScopeFilter.None)
            {
                this.ScopeText = LocalizedResources.General_None;
                return;
            }

            if ((this.ScopeSelection & SearchScopeFilter.MasterData) != 0)
            {
                this.ScopeText += LocalizedResources.General_IsMasterData + ", ";
            }

            if ((this.ScopeSelection & SearchScopeFilter.MyProjects) != 0)
            {
                this.ScopeText += LocalizedResources.General_MyProjects + ", ";
            }

            if ((this.ScopeSelection & SearchScopeFilter.OtherUsersProjects) != 0)
            {
                this.ScopeText += LocalizedResources.General_OtherUsersProjects + ", ";
            }

            if ((this.ScopeSelection & SearchScopeFilter.ReleasedProjects) != 0)
            {
                this.ScopeText += LocalizedResources.General_ReleasedProjects + ", ";
            }

            if (!string.IsNullOrEmpty(this.ScopeText))
            {
                this.ScopeText = this.ScopeText.Remove(this.scopeText.Length - 2);
            }
        }

        #endregion Search Locations

        /// <summary>
        /// Gets the entity properties.
        /// </summary>
        /// <param name="entityFamily">Entity family.</param>
        /// <returns>A collection containing the entity properties.</returns>
        private Collection<EntityProperty> GetEntityProperties(SearchFilter entityFamily)
        {
            var entityProperties = new Collection<EntityProperty>();

            var manufacturer = new Manufacturer();
            var manufacturerType = manufacturer.GetType();
            var manufacturerName = typeof(Manufacturer).Name;

            switch (entityFamily)
            {
                case SearchFilter.Projects:
                    var project = new Project();
                    var projectName = typeof(Project).Name;

                    var customer = new Customer();
                    var customerType = customer.GetType();
                    var customerName = typeof(Customer).Name;

                    this.SelectedEntityType = typeof(Project);

                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_ProjectDefinition, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Name, PropertyName = projectName + "." + ReflectionUtils.GetPropertyName(() => project.Name), ControlType = ValueControlTypes.TextExtendedTextBox, IsDefault = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Number, PropertyName = projectName + "." + ReflectionUtils.GetPropertyName(() => project.Number), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_StartDate, PropertyName = projectName + "." + ReflectionUtils.GetPropertyName(() => project.StartDate), ControlType = ValueControlTypes.DatePicker });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_EndDate, PropertyName = projectName + "." + ReflectionUtils.GetPropertyName(() => project.EndDate), ControlType = ValueControlTypes.DatePicker });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Status, PropertyName = projectName + "." + ReflectionUtils.GetPropertyName(() => project.Status), ControlType = ValueControlTypes.ProjectStatusComboBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Partner, PropertyName = projectName + "." + ReflectionUtils.GetPropertyName(() => project.Partner), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Leader, PropertyName = projectName + "." + "ProjectLeaderGuid", ControlType = ValueControlTypes.UsersComboBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_ResCalc, PropertyName = projectName + "." + "ResponsableCalculatorGuid", ControlType = ValueControlTypes.UsersComboBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Version, PropertyName = projectName + "." + ReflectionUtils.GetPropertyName(() => project.Version), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Decimal });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_VersionDate, PropertyName = projectName + "." + ReflectionUtils.GetPropertyName(() => project.VersionDate), ControlType = ValueControlTypes.DatePicker });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Product, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_ProductName, PropertyName = projectName + "." + ReflectionUtils.GetPropertyName(() => project.ProductName), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_ProductNumber, PropertyName = projectName + "." + ReflectionUtils.GetPropertyName(() => project.ProductNumber), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_YearlyProductionQuantity, PropertyName = projectName + "." + ReflectionUtils.GetPropertyName(() => project.YearlyProductionQuantity), ControlType = ValueControlTypes.RangeExtendedTextBox, Symbol = LocalizedResources.General_Pieces, ControlInputType = TextBoxControlInputType.Integer });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_LifeTime, PropertyName = projectName + "." + ReflectionUtils.GetPropertyName(() => project.LifeTime), ControlType = ValueControlTypes.RangeExtendedTextBox, Symbol = LocalizedResources.General_Years, ControlInputType = TextBoxControlInputType.Integer });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_ProjectDescription, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_ProductDescription, PropertyName = projectName + "." + ReflectionUtils.GetPropertyName(() => project.ProductDescription), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_CustomerDescription, PropertyName = projectName + "." + ReflectionUtils.GetPropertyName(() => project.CustomerDescription), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_AdditionalRemarks, PropertyName = projectName + "." + ReflectionUtils.GetPropertyName(() => project.Remarks), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Supplier, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_SupplierName, PropertyName = customerName + "." + ReflectionUtils.GetPropertyName(() => customer.Name), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Type, PropertyName = customerName + "." + ReflectionUtils.GetPropertyName(() => customer.Type), ControlType = ValueControlTypes.SupplierTypeComboBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_SupplierNumber, PropertyName = customerName + "." + ReflectionUtils.GetPropertyName(() => customer.Number), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Description, PropertyName = customerName + "." + ReflectionUtils.GetPropertyName(() => customer.Description), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_StreetAddress, PropertyName = customerName + "." + ReflectionUtils.GetPropertyName(() => customer.StreetAddress), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_City, PropertyName = customerName + "." + ReflectionUtils.GetPropertyName(() => customer.City), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_ZipCode, PropertyName = customerName + "." + ReflectionUtils.GetPropertyName(() => customer.ZipCode), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Address_Country, PropertyName = customerName + "." + ReflectionUtils.GetPropertyName(() => customer.Country), ControlType = ValueControlTypes.Country });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Address_State, PropertyName = customerName + "." + ReflectionUtils.GetPropertyName(() => customer.State), ControlType = ValueControlTypes.Supplier });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_ContactName, PropertyName = customerName + "." + ReflectionUtils.GetPropertyName(() => customer.FirstContactName), SearchedProperties = new Collection<string>() { customerName + "." + ReflectionUtils.GetPropertyName(() => customer.SecondContactName) }, ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Phone, PropertyName = customerName + "." + ReflectionUtils.GetPropertyName(() => customer.FirstContactPhone), SearchedProperties = new Collection<string>() { customerName + "." + ReflectionUtils.GetPropertyName(() => customer.SecondContactPhone) }, ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Contact_FaxNumber, PropertyName = customerName + "." + ReflectionUtils.GetPropertyName(() => customer.FirstContactFax), SearchedProperties = new Collection<string>() { customerName + "." + ReflectionUtils.GetPropertyName(() => customer.SecondContactFax) }, ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Contact_MobileNumber, PropertyName = customerName + "." + ReflectionUtils.GetPropertyName(() => customer.FirstContactMobile), SearchedProperties = new Collection<string>() { customerName + "." + ReflectionUtils.GetPropertyName(() => customer.SecondContactMobile) }, ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Contact_EmailAddress, PropertyName = customerName + "." + ReflectionUtils.GetPropertyName(() => customer.FirstContactEmail), SearchedProperties = new Collection<string>() { customerName + "." + ReflectionUtils.GetPropertyName(() => customer.SecondContactEmail) }, ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_WebAddress, PropertyName = customerName + "." + ReflectionUtils.GetPropertyName(() => customer.FirstContactWebAddress), SearchedProperties = new Collection<string>() { customerName + "." + ReflectionUtils.GetPropertyName(() => customer.SecondContactWebAddress) }, ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_AdditionalInformation, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_DeprPeriod, PropertyName = projectName + "." + ReflectionUtils.GetPropertyName(() => project.DepreciationPeriod), ControlType = ValueControlTypes.RangeExtendedTextBox, Symbol = LocalizedResources.General_Years, ControlInputType = TextBoxControlInputType.Integer });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_DeprRate, PropertyName = projectName + "." + ReflectionUtils.GetPropertyName(() => project.DepreciationRate), ControlType = ValueControlTypes.RangeExtendedTextBox, UIUnit = MeasurementUnitsAdapter.UIPercentage, ControlInputType = TextBoxControlInputType.Percentage });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_LogisticCostRatio, PropertyName = projectName + "." + ReflectionUtils.GetPropertyName(() => project.LogisticCostRatio), ControlType = ValueControlTypes.RangeExtendedTextBox, UIUnit = MeasurementUnitsAdapter.UIPercentage, ControlInputType = TextBoxControlInputType.Percentage });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.MassDataUpdate_ShiftInformation, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_ShiftsPerWeek, PropertyName = projectName + "." + ReflectionUtils.GetPropertyName(() => project.ShiftsPerWeek), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Decimal });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_HoursPerShift, PropertyName = projectName + "." + ReflectionUtils.GetPropertyName(() => project.HoursPerShift), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Decimal });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_ProductionDaysPerWeek, PropertyName = projectName + "." + ReflectionUtils.GetPropertyName(() => project.ProductionDaysPerWeek), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Decimal });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_ProductionWeeksPerYear, PropertyName = projectName + "." + ReflectionUtils.GetPropertyName(() => project.ProductionWeeksPerYear), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Decimal });

                    for (int i = 0; i < entityProperties.Count; i++)
                    {
                        entityProperties[i].Position = i;
                    }

                    break;
                case SearchFilter.Assemblies:
                    var assembly = new Assembly();
                    var assemblyName = typeof(Assembly).Name;

                    this.SelectedEntityType = typeof(Assembly);

                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_BasicInformation, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Assembly_Name, PropertyName = assemblyName + "." + ReflectionUtils.GetPropertyName(() => assembly.Name), ControlType = ValueControlTypes.TextExtendedTextBox, IsDefault = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Assembly_Number, PropertyName = assemblyName + "." + ReflectionUtils.GetPropertyName(() => assembly.Number), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Assembly_Version, PropertyName = assemblyName + "." + ReflectionUtils.GetPropertyName(() => assembly.Version), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Decimal });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_VersionDate, PropertyName = assemblyName + "." + ReflectionUtils.GetPropertyName(() => assembly.VersionDate), ControlType = ValueControlTypes.DatePicker });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Assembly_Description, PropertyName = assemblyName + "." + ReflectionUtils.GetPropertyName(() => assembly.Description), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Manufacturer_Location, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Assembly_AssemblingCountryName, PropertyName = assemblyName + "." + ReflectionUtils.GetPropertyName(() => assembly.AssemblingCountry), ControlType = ValueControlTypes.Country });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Assembly_AssemblingSupplierName, PropertyName = assemblyName + "." + ReflectionUtils.GetPropertyName(() => assembly.AssemblingSupplier), ControlType = ValueControlTypes.Supplier });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Calculation, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Assembly_CalculationAccuracy, PropertyName = assemblyName + "." + ReflectionUtils.GetPropertyName(() => assembly.CalculationAccuracy), ControlType = ValueControlTypes.CalculationAccuracyComboBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Assembly_EstimatedCost, PropertyName = assemblyName + "." + ReflectionUtils.GetPropertyName(() => assembly.EstimatedCost), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Money });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Weight, PropertyName = assemblyName + "." + ReflectionUtils.GetPropertyName(() => assembly.Weight), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Decimal });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Assembly_CalculationApproach, PropertyName = assemblyName + "." + ReflectionUtils.GetPropertyName(() => assembly.CalculationApproach), ControlType = ValueControlTypes.CalculationApproachComboBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Assembly_CalculationVariant, PropertyName = assemblyName + "." + ReflectionUtils.GetPropertyName(() => assembly.CalculationVariant), ControlType = ValueControlTypes.CalculationVariantComboBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Assembly_BatchSizePerYear, PropertyName = assemblyName + "." + ReflectionUtils.GetPropertyName(() => assembly.BatchSizePerYear), ControlType = ValueControlTypes.RangeExtendedTextBox, Symbol = LocalizedResources.General_PerYear, ControlInputType = TextBoxControlInputType.Integer });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_YearlyProductionQuantity, PropertyName = assemblyName + "." + ReflectionUtils.GetPropertyName(() => assembly.YearlyProductionQuantity), ControlType = ValueControlTypes.RangeExtendedTextBox, Symbol = LocalizedResources.General_Pieces, ControlInputType = TextBoxControlInputType.Integer });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Assembly_LifeTime, PropertyName = assemblyName + "." + ReflectionUtils.GetPropertyName(() => assembly.LifeTime), ControlType = ValueControlTypes.RangeExtendedTextBox, Symbol = LocalizedResources.General_Years, ControlInputType = TextBoxControlInputType.Integer });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Manufacturer, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Manufacturer_Name, PropertyName = manufacturerName + "." + ReflectionUtils.GetPropertyName(() => manufacturer.Name), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Manufacturer_Description, PropertyName = manufacturerName + "." + ReflectionUtils.GetPropertyName(() => manufacturer.Description), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Settings, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Assembly_PurchasePrice, PropertyName = assemblyName + "." + ReflectionUtils.GetPropertyName(() => assembly.PurchasePrice), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Money });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Assembly_TargetPrice, PropertyName = assemblyName + "." + ReflectionUtils.GetPropertyName(() => assembly.TargetPrice), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Money });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Assembly_DeliveryType, PropertyName = assemblyName + "." + ReflectionUtils.GetPropertyName(() => assembly.DeliveryType), ControlType = ValueControlTypes.AssemblyPartDeliveryTypeComboBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Assembly_SBMActive, PropertyName = assemblyName + "." + ReflectionUtils.GetPropertyName(() => assembly.SBMActive), ControlType = ValueControlTypes.CheckBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Assembly_AssetRate, PropertyName = assemblyName + "." + ReflectionUtils.GetPropertyName(() => assembly.AssetRate), ControlType = ValueControlTypes.RangeExtendedTextBox, UIUnit = MeasurementUnitsAdapter.UIPercentage, ControlInputType = TextBoxControlInputType.Percentage });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Assembly_IsExternal, PropertyName = assemblyName + "." + ReflectionUtils.GetPropertyName(() => assembly.IsExternal), ControlType = ValueControlTypes.CheckBox });

                    for (int i = 0; i < entityProperties.Count; i++)
                    {
                        entityProperties[i].Position = i;
                    }

                    break;
                case SearchFilter.Parts:
                    var part = new Part();
                    var partName = typeof(Part).Name;

                    this.SelectedEntityType = typeof(Part);

                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_BasicInformation, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_Name, PropertyName = partName + "." + ReflectionUtils.GetPropertyName(() => part.Name), ControlType = ValueControlTypes.TextExtendedTextBox, IsDefault = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_Number, PropertyName = partName + "." + ReflectionUtils.GetPropertyName(() => part.Number), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_Version, PropertyName = partName + "." + ReflectionUtils.GetPropertyName(() => part.Version), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Decimal });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_VersionDate, PropertyName = partName + "." + ReflectionUtils.GetPropertyName(() => part.VersionDate), ControlType = ValueControlTypes.DatePicker });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_Description, PropertyName = partName + "." + ReflectionUtils.GetPropertyName(() => part.Description), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Manufacturer_Location, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_ManufacturingCountryName, PropertyName = partName + "." + ReflectionUtils.GetPropertyName(() => part.ManufacturingCountry), ControlType = ValueControlTypes.Country });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_ManufacturingSupplierName, PropertyName = partName + "." + ReflectionUtils.GetPropertyName(() => part.ManufacturingSupplier), ControlType = ValueControlTypes.Supplier });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Calculation, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_CalculationStatus, PropertyName = partName + "." + ReflectionUtils.GetPropertyName(() => part.CalculationStatus), ControlType = ValueControlTypes.CalculationStatusComboBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_CalculationAccuracy, PropertyName = partName + "." + ReflectionUtils.GetPropertyName(() => part.CalculationAccuracy), ControlType = ValueControlTypes.CalculationAccuracyComboBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_EstimatedCost, PropertyName = partName + "." + ReflectionUtils.GetPropertyName(() => part.EstimatedCost), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Money });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_ManufacturingRatio, PropertyName = partName + "." + ReflectionUtils.GetPropertyName(() => part.ManufacturingRatio), ControlType = ValueControlTypes.RangeExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Weight, PropertyName = partName + "." + ReflectionUtils.GetPropertyName(() => part.Weight), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Decimal });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_CalculationApproach, PropertyName = partName + "." + ReflectionUtils.GetPropertyName(() => part.CalculationApproach), ControlType = ValueControlTypes.CalculationApproachComboBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_CalculationVariant, PropertyName = partName + "." + ReflectionUtils.GetPropertyName(() => part.CalculationVariant), ControlType = ValueControlTypes.CalculationVariantComboBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_CalculatorUser, PropertyName = partName + "." + "CalculatorGuid", ControlType = ValueControlTypes.UsersComboBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_BatchSizePerYear, PropertyName = partName + "." + ReflectionUtils.GetPropertyName(() => part.BatchSizePerYear), ControlType = ValueControlTypes.RangeExtendedTextBox, Symbol = LocalizedResources.General_PerYear, ControlInputType = TextBoxControlInputType.Integer });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_YearlyProductionQuantity, PropertyName = partName + "." + ReflectionUtils.GetPropertyName(() => part.YearlyProductionQuantity), ControlType = ValueControlTypes.RangeExtendedTextBox, Symbol = LocalizedResources.General_Pieces, ControlInputType = TextBoxControlInputType.Integer });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_LifeTime, PropertyName = partName + "." + ReflectionUtils.GetPropertyName(() => part.LifeTime), ControlType = ValueControlTypes.RangeExtendedTextBox, Symbol = LocalizedResources.General_Years, ControlInputType = TextBoxControlInputType.Integer });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Manufacturer, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Manufacturer_Name, PropertyName = manufacturerName + "." + ReflectionUtils.GetPropertyName(() => manufacturer.Name), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Manufacturer_Description, PropertyName = manufacturerName + "." + ReflectionUtils.GetPropertyName(() => manufacturer.Description), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Settings, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_PurchasePrice, PropertyName = partName + "." + ReflectionUtils.GetPropertyName(() => part.PurchasePrice), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Money });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_TargetPrice, PropertyName = partName + "." + ReflectionUtils.GetPropertyName(() => part.TargetPrice), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Money });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_DelivertType, PropertyName = partName + "." + ReflectionUtils.GetPropertyName(() => part.DelivertType), ControlType = ValueControlTypes.AssemblyPartDeliveryTypeComboBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_SBMActive, PropertyName = partName + "." + ReflectionUtils.GetPropertyName(() => part.SBMActive), ControlType = ValueControlTypes.CheckBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_AssetRate, PropertyName = partName + "." + ReflectionUtils.GetPropertyName(() => part.AssetRate), ControlType = ValueControlTypes.RangeExtendedTextBox, UIUnit = MeasurementUnitsAdapter.UIPercentage, ControlInputType = TextBoxControlInputType.Percentage });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_IsExternal, PropertyName = partName + "." + ReflectionUtils.GetPropertyName(() => part.IsExternal), ControlType = ValueControlTypes.CheckBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Description, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_AdditionalRemarks, PropertyName = partName + "." + ReflectionUtils.GetPropertyName(() => part.AdditionalRemarks), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_AdditionalDescription, PropertyName = partName + "." + ReflectionUtils.GetPropertyName(() => part.AdditionalDescription), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_ManufacturerDescription, PropertyName = partName + "." + ReflectionUtils.GetPropertyName(() => part.ManufacturerDescription), ControlType = ValueControlTypes.TextExtendedTextBox });

                    for (int i = 0; i < entityProperties.Count; i++)
                    {
                        entityProperties[i].Position = i;
                    }

                    break;
                case SearchFilter.RawParts:
                    var rawPart = new RawPart();
                    var rawPartName = typeof(RawPart).Name;

                    this.SelectedEntityType = typeof(RawPart);

                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_BasicInformation, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_Name, PropertyName = rawPartName + "." + ReflectionUtils.GetPropertyName(() => rawPart.Name), ControlType = ValueControlTypes.TextExtendedTextBox, IsDefault = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_Number, PropertyName = rawPartName + "." + ReflectionUtils.GetPropertyName(() => rawPart.Number), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_Version, PropertyName = rawPartName + "." + ReflectionUtils.GetPropertyName(() => rawPart.Version), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Decimal });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_VersionDate, PropertyName = rawPartName + "." + ReflectionUtils.GetPropertyName(() => rawPart.VersionDate), ControlType = ValueControlTypes.DatePicker });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_Description, PropertyName = rawPartName + "." + ReflectionUtils.GetPropertyName(() => rawPart.Description), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Manufacturer_Location, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_ManufacturingCountryName, PropertyName = rawPartName + "." + ReflectionUtils.GetPropertyName(() => rawPart.ManufacturingCountry), ControlType = ValueControlTypes.Country });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_ManufacturingSupplierName, PropertyName = rawPartName + "." + ReflectionUtils.GetPropertyName(() => rawPart.ManufacturingSupplier), ControlType = ValueControlTypes.Supplier });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Calculation, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_CalculationStatus, PropertyName = rawPartName + "." + ReflectionUtils.GetPropertyName(() => rawPart.CalculationStatus), ControlType = ValueControlTypes.CalculationStatusComboBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_CalculationAccuracy, PropertyName = rawPartName + "." + ReflectionUtils.GetPropertyName(() => rawPart.CalculationAccuracy), ControlType = ValueControlTypes.CalculationAccuracyComboBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_EstimatedCost, PropertyName = rawPartName + "." + ReflectionUtils.GetPropertyName(() => rawPart.EstimatedCost), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Money });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_ManufacturingRatio, PropertyName = rawPartName + "." + ReflectionUtils.GetPropertyName(() => rawPart.ManufacturingRatio), ControlType = ValueControlTypes.RangeExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Weight, PropertyName = rawPartName + "." + ReflectionUtils.GetPropertyName(() => rawPart.Weight), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Decimal });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_CalculationApproach, PropertyName = rawPartName + "." + ReflectionUtils.GetPropertyName(() => rawPart.CalculationApproach), ControlType = ValueControlTypes.CalculationApproachComboBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_CalculationVariant, PropertyName = rawPartName + "." + ReflectionUtils.GetPropertyName(() => rawPart.CalculationVariant), ControlType = ValueControlTypes.CalculationVariantComboBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_CalculatorUser, PropertyName = rawPartName + "." + "CalculatorGuid", ControlType = ValueControlTypes.UsersComboBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_BatchSizePerYear, PropertyName = rawPartName + "." + ReflectionUtils.GetPropertyName(() => rawPart.BatchSizePerYear), ControlType = ValueControlTypes.RangeExtendedTextBox, Symbol = LocalizedResources.General_PerYear, ControlInputType = TextBoxControlInputType.Integer });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_YearlyProductionQuantity, PropertyName = rawPartName + "." + ReflectionUtils.GetPropertyName(() => rawPart.YearlyProductionQuantity), ControlType = ValueControlTypes.RangeExtendedTextBox, Symbol = LocalizedResources.General_Pieces, ControlInputType = TextBoxControlInputType.Integer });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_LifeTime, PropertyName = rawPartName + "." + ReflectionUtils.GetPropertyName(() => rawPart.LifeTime), ControlType = ValueControlTypes.RangeExtendedTextBox, Symbol = LocalizedResources.General_Years, ControlInputType = TextBoxControlInputType.Integer });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Manufacturer, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Manufacturer_Name, PropertyName = manufacturerName + "." + ReflectionUtils.GetPropertyName(() => manufacturer.Name), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Manufacturer_Description, PropertyName = manufacturerName + "." + ReflectionUtils.GetPropertyName(() => manufacturer.Description), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Settings, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_PurchasePrice, PropertyName = rawPartName + "." + ReflectionUtils.GetPropertyName(() => rawPart.PurchasePrice), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Money });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_TargetPrice, PropertyName = rawPartName + "." + ReflectionUtils.GetPropertyName(() => rawPart.TargetPrice), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Money });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_DelivertType, PropertyName = rawPartName + "." + ReflectionUtils.GetPropertyName(() => rawPart.DelivertType), ControlType = ValueControlTypes.AssemblyPartDeliveryTypeComboBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_SBMActive, PropertyName = rawPartName + "." + ReflectionUtils.GetPropertyName(() => rawPart.SBMActive), ControlType = ValueControlTypes.CheckBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_AssetRate, PropertyName = rawPartName + "." + ReflectionUtils.GetPropertyName(() => rawPart.AssetRate), ControlType = ValueControlTypes.RangeExtendedTextBox, UIUnit = MeasurementUnitsAdapter.UIPercentage, ControlInputType = TextBoxControlInputType.Percentage });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_IsExternal, PropertyName = rawPartName + "." + ReflectionUtils.GetPropertyName(() => rawPart.IsExternal), ControlType = ValueControlTypes.CheckBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Description, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_AdditionalRemarks, PropertyName = rawPartName + "." + ReflectionUtils.GetPropertyName(() => rawPart.AdditionalRemarks), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_AdditionalDescription, PropertyName = rawPartName + "." + ReflectionUtils.GetPropertyName(() => rawPart.AdditionalDescription), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Part_ManufacturerDescription, PropertyName = rawPartName + "." + ReflectionUtils.GetPropertyName(() => rawPart.ManufacturerDescription), ControlType = ValueControlTypes.TextExtendedTextBox });

                    for (int i = 0; i < entityProperties.Count; i++)
                    {
                        entityProperties[i].Position = i;
                    }

                    break;
                case SearchFilter.ProcessSteps:
                    var processStep = new AssemblyProcessStep();
                    var processStepName = typeof(ProcessStep).Name;

                    this.SelectedEntityType = typeof(ProcessStep);

                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Information, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Name, PropertyName = processStepName + "." + ReflectionUtils.GetPropertyName(() => processStep.Name), ControlType = ValueControlTypes.TextExtendedTextBox, IsDefault = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_CalculationAccuracy, PropertyName = processStepName + "." + ReflectionUtils.GetPropertyName(() => processStep.Accuracy), ControlType = ValueControlTypes.ProcessAccuracyComboBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_EstimatedCost, PropertyName = processStepName + "." + ReflectionUtils.GetPropertyName(() => processStep.Price), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Money });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_ProcessTime, PropertyName = processStepName + "." + ReflectionUtils.GetPropertyName(() => processStep.ProcessTime), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Decimal });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_SetupsPerBatch, PropertyName = processStepName + "." + ReflectionUtils.GetPropertyName(() => processStep.SetupsPerBatch), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Integer });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_CycleTime, PropertyName = processStepName + "." + ReflectionUtils.GetPropertyName(() => processStep.CycleTime), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Decimal });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_MaxDowntime, PropertyName = processStepName + "." + ReflectionUtils.GetPropertyName(() => processStep.MaxDownTime), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Decimal });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_PartsPerCycle, PropertyName = processStepName + "." + ReflectionUtils.GetPropertyName(() => processStep.PartsPerCycle), ControlType = ValueControlTypes.RangeExtendedTextBox, Symbol = LocalizedResources.General_Pieces, ControlInputType = TextBoxControlInputType.Integer });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_SetupTime, PropertyName = processStepName + "." + ReflectionUtils.GetPropertyName(() => processStep.SetupTime), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Decimal });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Reject, PropertyName = processStepName + "." + ReflectionUtils.GetPropertyName(() => processStep.ScrapAmount), ControlType = ValueControlTypes.RangeExtendedTextBox, UIUnit = MeasurementUnitsAdapter.UIPercentage, ControlInputType = TextBoxControlInputType.Percentage });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_PartBatch, PropertyName = processStepName + "." + ReflectionUtils.GetPropertyName(() => processStep.BatchSize), ControlType = ValueControlTypes.RangeExtendedTextBox, Symbol = LocalizedResources.General_Pieces, ControlInputType = TextBoxControlInputType.Integer });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.OverheadSetting_ManufacturingOverhead, PropertyName = processStepName + "." + ReflectionUtils.GetPropertyName(() => processStep.ManufacturingOverhead), ControlType = ValueControlTypes.RangeExtendedTextBox, UIUnit = MeasurementUnitsAdapter.UIPercentage, ControlInputType = TextBoxControlInputType.Percentage });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.MassDataUpdate_ShiftInformation, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_ShiftsPerWeek, PropertyName = processStepName + "." + ReflectionUtils.GetPropertyName(() => processStep.ShiftsPerWeek), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Decimal });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_HoursPerShift, PropertyName = processStepName + "." + ReflectionUtils.GetPropertyName(() => processStep.HoursPerShift), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Decimal });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_ProductionDaysPerWeek, PropertyName = processStepName + "." + ReflectionUtils.GetPropertyName(() => processStep.ProductionDaysPerWeek), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Decimal });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_ProductionWeeksPerYear, PropertyName = processStepName + "." + ReflectionUtils.GetPropertyName(() => processStep.ProductionWeeksPerYear), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Decimal });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.ProcessStep_ExceedShiftCost, PropertyName = processStepName + "." + ReflectionUtils.GetPropertyName(() => processStep.ExceedShiftCost), ControlType = ValueControlTypes.CheckBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.ProcessStep_ShiftNoExceedingCost, PropertyName = processStepName + "." + ReflectionUtils.GetPropertyName(() => processStep.ExtraShiftsNumber), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Decimal });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.ProcessStep_ShiftCostExceedRatio, PropertyName = processStepName + "." + ReflectionUtils.GetPropertyName(() => processStep.ShiftCostExceedRatio), ControlType = ValueControlTypes.RangeExtendedTextBox, UIUnit = MeasurementUnitsAdapter.UIPercentage, ControlInputType = TextBoxControlInputType.Percentage });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Description, PropertyName = processStepName + "." + ReflectionUtils.GetPropertyName(() => processStep.Description), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Labour_DirectLabourSettingsForProduction, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Labour_UnskilledLabourProduction, PropertyName = processStepName + "." + ReflectionUtils.GetPropertyName(() => processStep.ProductionUnskilledLabour), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Decimal });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Labour_SkilledLabourProduction, PropertyName = processStepName + "." + ReflectionUtils.GetPropertyName(() => processStep.ProductionSkilledLabour), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Decimal });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Labour_ForemanProduction, PropertyName = processStepName + "." + ReflectionUtils.GetPropertyName(() => processStep.ProductionForeman), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Decimal });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Labour_TechniciansProduction, PropertyName = processStepName + "." + ReflectionUtils.GetPropertyName(() => processStep.ProductionTechnicians), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Decimal });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Labour_EngineersProduction, PropertyName = processStepName + "." + ReflectionUtils.GetPropertyName(() => processStep.ProductionEngineers), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Decimal });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Labour_DirectLabourSettingsForSetUp, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Labour_UnskilledLabourSetup, PropertyName = processStepName + "." + ReflectionUtils.GetPropertyName(() => processStep.SetupUnskilledLabour), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Decimal });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Labour_SkilledLabourSetup, PropertyName = processStepName + "." + ReflectionUtils.GetPropertyName(() => processStep.SetupSkilledLabour), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Decimal });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Labour_ForemanSetup, PropertyName = processStepName + "." + ReflectionUtils.GetPropertyName(() => processStep.SetupForeman), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Decimal });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Labour_TechniciansSetup, PropertyName = processStepName + "." + ReflectionUtils.GetPropertyName(() => processStep.SetupTechnicians), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Decimal });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Labour_EngineersSetup, PropertyName = processStepName + "." + ReflectionUtils.GetPropertyName(() => processStep.SetupEngineers), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Decimal });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_AdditionalSettings, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_IsExternal, PropertyName = processStepName + "." + ReflectionUtils.GetPropertyName(() => processStep.IsExternal), ControlType = ValueControlTypes.CheckBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_TransportCostManufacturing, PropertyName = processStepName + "." + ReflectionUtils.GetPropertyName(() => processStep.TransportCost), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Money });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_TransportCostQuantity, PropertyName = processStepName + "." + ReflectionUtils.GetPropertyName(() => processStep.TransportCostQty), ControlType = ValueControlTypes.RangeExtendedTextBox, Symbol = LocalizedResources.General_Pieces, ControlInputType = TextBoxControlInputType.Integer });

                    for (int i = 0; i < entityProperties.Count; i++)
                    {
                        entityProperties[i].Position = i;
                    }

                    break;
                case SearchFilter.Machines:
                    var machine = new Machine();
                    var machineName = typeof(Machine).Name;

                    this.SelectedEntityType = typeof(Machine);

                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Description, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_Name, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.Name), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_ManufacturingYear, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.ManufacturingYear), ControlType = ValueControlTypes.YearPicker });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_DepreciationPeriod, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.DepreciationPeriod), ControlType = ValueControlTypes.RangeExtendedTextBox, Symbol = LocalizedResources.General_Years, ControlInputType = TextBoxControlInputType.Integer });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_DepreciationRate, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.DepreciationRate), ControlType = ValueControlTypes.RangeExtendedTextBox, UIUnit = MeasurementUnitsAdapter.UIPercentage, ControlInputType = TextBoxControlInputType.Percentage });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_RegistrationNumber, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.RegistrationNumber), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_DescriptionOfFunction, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.DescriptionOfFunction), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Manufacturer, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Manufacturer_Name, PropertyName = manufacturerName + "." + ReflectionUtils.GetPropertyName(() => manufacturer.Name), ControlType = ValueControlTypes.TextExtendedTextBox, IsDefault = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Manufacturer_Description, PropertyName = manufacturerName + "." + ReflectionUtils.GetPropertyName(() => manufacturer.Description), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Classification, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Classification, PropertyName = machineName + "." + "MainClassificationGuid", SearchedProperties = new Collection<string>() { machineName + "." + "TypeClassificationGuid", machineName + "." + "SubClassificationGuid", machineName + "." + "ClassificationLevel4Guid" }, ControlType = ValueControlTypes.ClassificationSelector });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_FloorEnergy, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_FloorSize, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.ScrambledFloorSize), ControlType = ValueControlTypes.NumberExtendedTextBox, UIUnit = MeasurementUnitsAdapter.UIArea, ControlInputType = TextBoxControlInputType.Decimal });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_WorkspaceArea, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.ScrambledWorkspaceArea), ControlType = ValueControlTypes.NumberExtendedTextBox, UIUnit = MeasurementUnitsAdapter.UIArea, ControlInputType = TextBoxControlInputType.Decimal });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_PowerConsumption, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.ScrambledPowerConsumption), ControlType = ValueControlTypes.NumberExtendedTextBox, Symbol = LocalizedResources.General_Kwh, ControlInputType = TextBoxControlInputType.Decimal });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_AirConsumption, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.AirConsumption), ControlType = ValueControlTypes.RangeExtendedTextBox, UIUnit = MeasurementUnitsAdapter.UIVolume, ControlInputType = TextBoxControlInputType.Decimal });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_WaterConsumption, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.WaterConsumption), ControlType = ValueControlTypes.RangeExtendedTextBox, UIUnit = MeasurementUnitsAdapter.UIVolume, ControlInputType = TextBoxControlInputType.Decimal });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_FullLoadRate, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.FullLoadRate), ControlType = ValueControlTypes.RangeExtendedTextBox, UIUnit = MeasurementUnitsAdapter.UIPercentage, ControlInputType = TextBoxControlInputType.Percentage });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_Fossil, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.FossilEnergyConsumptionRatio), ControlType = ValueControlTypes.RangeExtendedTextBox, UIUnit = MeasurementUnitsAdapter.UIPercentage, ControlInputType = TextBoxControlInputType.Percentage });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_Renewable, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.RenewableEnergyConsumptionRatio), ControlType = ValueControlTypes.RangeExtendedTextBox, UIUnit = MeasurementUnitsAdapter.UIPercentage, ControlInputType = TextBoxControlInputType.Percentage });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_TechnicalInformation, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_MaxCapacity, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.MaxCapacity), ControlType = ValueControlTypes.RangeExtendedTextBox, Symbol = LocalizedResources.General_PiecesPerHour, ControlInputType = TextBoxControlInputType.Integer });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_OEE, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.OEE), ControlType = ValueControlTypes.RangeExtendedTextBox, UIUnit = MeasurementUnitsAdapter.UIPercentage, ControlInputType = TextBoxControlInputType.Percentage });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_Availability, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.Availability), ControlType = ValueControlTypes.RangeExtendedTextBox, UIUnit = MeasurementUnitsAdapter.UIPercentage, ControlInputType = TextBoxControlInputType.Percentage });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Investment, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_MachineInvestment, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.ScrambledMachineInvestment), ControlType = ValueControlTypes.NumberExtendedTextBox, ControlInputType = TextBoxControlInputType.Money });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_LeaseCosts, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.MachineLeaseCosts), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Money });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_SetupInvestment, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.ScrambledSetupInvestment), ControlType = ValueControlTypes.NumberExtendedTextBox, ControlInputType = TextBoxControlInputType.Money });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_AdditionalEquipmentInvestment, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.ScrambledAdditionalEquipmentInvestment), ControlType = ValueControlTypes.NumberExtendedTextBox, ControlInputType = TextBoxControlInputType.Money });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_FundamentalSetupInvestment, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.ScrambledFundamentalSetupInvestment), ControlType = ValueControlTypes.NumberExtendedTextBox, ControlInputType = TextBoxControlInputType.Money });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_InvestmentRemarks, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.InvestmentRemarks), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Costs, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_ExternalWorkCost, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.ExternalWorkCost), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Money });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_MaterialCost, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.MaterialCost), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Money });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_ConsumablesCost, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.ConsumablesCost), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Money });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_ManualConsumableCostPercentage, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.ManualConsumableCostPercentage), ControlType = ValueControlTypes.RangeExtendedTextBox, UIUnit = MeasurementUnitsAdapter.UIPercentage, ControlInputType = TextBoxControlInputType.Percentage });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_RepairsCost, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.RepairsCost), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Money });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_KValue, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.KValue), ControlType = ValueControlTypes.RangeExtendedTextBox, UIUnit = MeasurementUnitsAdapter.UIPercentage, ControlInputType = TextBoxControlInputType.Percentage });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_CalculateWithKValue, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.CalculateWithKValue), ControlType = ValueControlTypes.CheckBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Properties, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_MountingCubeLength, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.MountingCubeLength), ControlType = ValueControlTypes.TextExtendedTextBox, Symbol = LocalizedResources.MachiningCalculator_LengthUnit });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_MountingCubeWidth, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.MountingCubeWidth), ControlType = ValueControlTypes.TextExtendedTextBox, Symbol = LocalizedResources.MachiningCalculator_LengthUnit });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_MountingCubeHeight, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.MountingCubeHeight), ControlType = ValueControlTypes.TextExtendedTextBox, Symbol = LocalizedResources.MachiningCalculator_LengthUnit });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_MaxDiameterRodPerChuckMaxThickness, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.MaxDiameterRodPerChuckMaxThickness), ControlType = ValueControlTypes.TextExtendedTextBox, Symbol = LocalizedResources.MachiningCalculator_LengthUnit });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_MaxPartsWeightPerCapacity, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.MaxPartsWeightPerCapacity), ControlType = ValueControlTypes.TextExtendedTextBox, Symbol = LocalizedResources.MachiningCalculator_Density });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_LockingForce, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.LockingForce), ControlType = ValueControlTypes.TextExtendedTextBox, Symbol = LocalizedResources.MachiningCalculator_KiloNewton });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_PressCapacity, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.PressCapacity), ControlType = ValueControlTypes.TextExtendedTextBox, Symbol = LocalizedResources.MachiningCalculator_KiloNewton });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_MaximumSpeed, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.MaximumSpeed), ControlType = ValueControlTypes.TextExtendedTextBox, Symbol = LocalizedResources.MachiningCalculator_TurningSpeedUnit });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_RapidFeedPerCuttingSpeed, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.RapidFeedPerCuttingSpeed), ControlType = ValueControlTypes.TextExtendedTextBox, Symbol = LocalizedResources.MachiningCalculator_SpeedUnit });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_StrokeRate, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.StrokeRate), ControlType = ValueControlTypes.TextExtendedTextBox, Symbol = LocalizedResources.MachiningCalculator_StrokeRateUnit });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Machine_Other, PropertyName = machineName + "." + ReflectionUtils.GetPropertyName(() => machine.Other), ControlType = ValueControlTypes.TextExtendedTextBox });

                    for (int i = 0; i < entityProperties.Count; i++)
                    {
                        entityProperties[i].Position = i;
                    }

                    break;
                case SearchFilter.RawMaterials:
                    var rawMaterial = new RawMaterial();
                    var rawMaterialName = typeof(RawMaterial).Name;

                    this.SelectedEntityType = typeof(RawMaterial);

                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_BasicInformation, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.RawMaterial_Name, PropertyName = rawMaterialName + "." + ReflectionUtils.GetPropertyName(() => rawMaterial.Name), ControlType = ValueControlTypes.TextExtendedTextBox, IsDefault = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.RawMaterial_NameUK, PropertyName = rawMaterialName + "." + ReflectionUtils.GetPropertyName(() => rawMaterial.NameUK), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.RawMaterial_NameUS, PropertyName = rawMaterialName + "." + ReflectionUtils.GetPropertyName(() => rawMaterial.NameUS), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.RawMaterial_NormName, PropertyName = rawMaterialName + "." + ReflectionUtils.GetPropertyName(() => rawMaterial.NormName), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.RawMaterial_DeliveryType, PropertyName = rawMaterialName + "." + "DeliveryTypeGuid", ControlType = ValueControlTypes.RawMaterialDeliveryTypeComboBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.RawMaterial_Price, PropertyName = rawMaterialName + "." + ReflectionUtils.GetPropertyName(() => rawMaterial.ScrambledPrice), ControlType = ValueControlTypes.TextExtendedTextBox, ControlInputType = TextBoxControlInputType.Money });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.RawMaterial_ParentWeight, PropertyName = rawMaterialName + "." + ReflectionUtils.GetPropertyName(() => rawMaterial.ParentWeight), ControlType = ValueControlTypes.RangeExtendedTextBox, UIUnit = MeasurementUnitsAdapter.UIWeight, ControlInputType = TextBoxControlInputType.Decimal });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.RawMaterial_Sprue, PropertyName = rawMaterialName + "." + ReflectionUtils.GetPropertyName(() => rawMaterial.Sprue), ControlType = ValueControlTypes.RangeExtendedTextBox, UIUnit = MeasurementUnitsAdapter.UIPercentage, ControlInputType = TextBoxControlInputType.Percentage });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.RawMaterial_Quantity, PropertyName = rawMaterialName + "." + ReflectionUtils.GetPropertyName(() => rawMaterial.Quantity), ControlType = ValueControlTypes.RangeExtendedTextBox, UIUnit = MeasurementUnitsAdapter.UIWeight, ControlInputType = TextBoxControlInputType.Decimal });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.RawMaterial_Loss, PropertyName = rawMaterialName + "." + ReflectionUtils.GetPropertyName(() => rawMaterial.Loss), ControlType = ValueControlTypes.RangeExtendedTextBox, UIUnit = MeasurementUnitsAdapter.UIPercentage, ControlInputType = TextBoxControlInputType.Percentage });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.RawMaterial_ScrapRefundRatio, PropertyName = rawMaterialName + "." + ReflectionUtils.GetPropertyName(() => rawMaterial.ScrapRefundRatio), ControlType = ValueControlTypes.RangeExtendedTextBox, UIUnit = MeasurementUnitsAdapter.UIPercentage, ControlInputType = TextBoxControlInputType.Percentage });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.ScrapCalculationType, PropertyName = rawMaterialName + "." + ReflectionUtils.GetPropertyName(() => rawMaterial.ScrapCalculationType), ControlType = ValueControlTypes.ScrapCalculationType });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.RawMaterial_RejectRatio, PropertyName = rawMaterialName + "." + ReflectionUtils.GetPropertyName(() => rawMaterial.RejectRatio), ControlType = ValueControlTypes.RangeExtendedTextBox, UIUnit = MeasurementUnitsAdapter.UIPercentage, ControlInputType = TextBoxControlInputType.Percentage });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Recycling, PropertyName = rawMaterialName + "." + ReflectionUtils.GetPropertyName(() => rawMaterial.RecyclingRatio), ControlType = ValueControlTypes.RangeExtendedTextBox, UIUnit = MeasurementUnitsAdapter.UIPercentage, ControlInputType = TextBoxControlInputType.Percentage });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_StockKeeping, PropertyName = rawMaterialName + "." + ReflectionUtils.GetPropertyName(() => rawMaterial.StockKeeping), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Integer });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Remarks, PropertyName = rawMaterialName + "." + ReflectionUtils.GetPropertyName(() => rawMaterial.Remarks), ControlType = ValueControlTypes.TextExtendedTextBox, });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Classification, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Classification, PropertyName = rawMaterialName + "." + "ClassificationLevel1Guid", SearchedProperties = new Collection<string>() { rawMaterialName + "." + "ClassificationLevel2Guid", rawMaterialName + "." + "ClassificationLevel3Guid", rawMaterialName + "." + "ClassificationLevel4Guid" }, ControlType = ValueControlTypes.ClassificationSelector });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Manufacturer, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Manufacturer_Name, PropertyName = manufacturerName + "." + ReflectionUtils.GetPropertyName(() => manufacturer.Name), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Manufacturer_Description, PropertyName = manufacturerName + "." + ReflectionUtils.GetPropertyName(() => manufacturer.Description), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Properties, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.RawMaterial_YieldStrength, PropertyName = rawMaterialName + "." + ReflectionUtils.GetPropertyName(() => rawMaterial.ScrambledYieldStrength), ControlType = ValueControlTypes.TextExtendedTextBox, Symbol = LocalizedResources.Materials_StrengthUnit });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.RawMaterial_RuptureStrength, PropertyName = rawMaterialName + "." + ReflectionUtils.GetPropertyName(() => rawMaterial.ScrambledRuptureStrength), ControlType = ValueControlTypes.TextExtendedTextBox, Symbol = LocalizedResources.Materials_StrengthUnit });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.RawMaterial_Density, PropertyName = rawMaterialName + "." + ReflectionUtils.GetPropertyName(() => rawMaterial.ScrambledDensity), ControlType = ValueControlTypes.TextExtendedTextBox, Symbol = LocalizedResources.Materials_DensityUnit });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.RawMaterial_MaxElongation, PropertyName = rawMaterialName + "." + ReflectionUtils.GetPropertyName(() => rawMaterial.ScrambledMaxElongation), ControlType = ValueControlTypes.TextExtendedTextBox, UIUnit = MeasurementUnitsAdapter.UIPercentage });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.RawMaterial_GlassTransitionTemperature, PropertyName = rawMaterialName + "." + ReflectionUtils.GetPropertyName(() => rawMaterial.ScrambledGlassTransitionTemperature), ControlType = ValueControlTypes.TextExtendedTextBox, Symbol = LocalizedResources.General_TemperatureUnit });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.RawMaterial_Rx, PropertyName = rawMaterialName + "." + ReflectionUtils.GetPropertyName(() => rawMaterial.ScrambledRx), ControlType = ValueControlTypes.TextExtendedTextBox, UIUnit = MeasurementUnitsAdapter.UIPercentage });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.RawMaterial_Rm, PropertyName = rawMaterialName + "." + ReflectionUtils.GetPropertyName(() => rawMaterial.ScrambledRm), ControlType = ValueControlTypes.TextExtendedTextBox, Symbol = LocalizedResources.Materials_StrengthUnit });

                    for (int i = 0; i < entityProperties.Count; i++)
                    {
                        entityProperties[i].Position = i;
                    }

                    break;
                case SearchFilter.Commodities:
                    var commodity = new Commodity();
                    var commodityName = typeof(Commodity).Name;

                    this.SelectedEntityType = typeof(Commodity);

                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_BasicInformation, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Commodity_Name, PropertyName = commodityName + "." + ReflectionUtils.GetPropertyName(() => commodity.Name), ControlType = ValueControlTypes.TextExtendedTextBox, IsDefault = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Commodity_PartNumber, PropertyName = commodityName + "." + ReflectionUtils.GetPropertyName(() => commodity.PartNumber), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Commodity_Price, PropertyName = commodityName + "." + ReflectionUtils.GetPropertyName(() => commodity.Price), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Money });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Commodity_Amount, PropertyName = commodityName + "." + ReflectionUtils.GetPropertyName(() => commodity.Amount), ControlType = ValueControlTypes.RangeExtendedTextBox, UIUnit = MeasurementUnitsAdapter.UIPieces, ControlInputType = TextBoxControlInputType.Integer });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Commodity_RejectRatio, PropertyName = commodityName + "." + ReflectionUtils.GetPropertyName(() => commodity.RejectRatio), ControlType = ValueControlTypes.RangeExtendedTextBox, UIUnit = MeasurementUnitsAdapter.UIPercentage, ControlInputType = TextBoxControlInputType.Percentage });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Commodity_Weight, PropertyName = commodityName + "." + ReflectionUtils.GetPropertyName(() => commodity.Weight), ControlType = ValueControlTypes.RangeExtendedTextBox, UIUnit = MeasurementUnitsAdapter.UIWeight, ControlInputType = TextBoxControlInputType.Decimal });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Commodity_Remarks, PropertyName = commodityName + "." + ReflectionUtils.GetPropertyName(() => commodity.Remarks), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Manufacturer, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Manufacturer_Name, PropertyName = manufacturerName + "." + ReflectionUtils.GetPropertyName(() => manufacturer.Name), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Manufacturer_Description, PropertyName = manufacturerName + "." + ReflectionUtils.GetPropertyName(() => manufacturer.Description), ControlType = ValueControlTypes.TextExtendedTextBox });

                    for (int i = 0; i < entityProperties.Count; i++)
                    {
                        entityProperties[i].Position = i;
                    }

                    break;
                case SearchFilter.Consumables:
                    var consumable = new Consumable();
                    var consumableName = typeof(Consumable).Name;

                    this.SelectedEntityType = typeof(Consumable);

                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_BasicInformation, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Consumable_Name, PropertyName = consumableName + "." + ReflectionUtils.GetPropertyName(() => consumable.Name), ControlType = ValueControlTypes.TextExtendedTextBox, IsDefault = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Consumable_Amount, PropertyName = consumableName + "." + ReflectionUtils.GetPropertyName(() => consumable.Amount), ControlType = ValueControlTypes.ConsumableAmountComboBox, ControlInputType = TextBoxControlInputType.Decimal });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Consumable_Price, PropertyName = consumableName + "." + ReflectionUtils.GetPropertyName(() => consumable.Price), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Money });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Description, PropertyName = consumableName + "." + ReflectionUtils.GetPropertyName(() => consumable.Description), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Manufacturer, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Manufacturer_Name, PropertyName = manufacturerName + "." + ReflectionUtils.GetPropertyName(() => manufacturer.Name), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Manufacturer_Description, PropertyName = manufacturerName + "." + ReflectionUtils.GetPropertyName(() => manufacturer.Description), ControlType = ValueControlTypes.TextExtendedTextBox });

                    for (int i = 0; i < entityProperties.Count; i++)
                    {
                        entityProperties[i].Position = i;
                    }

                    break;
                case SearchFilter.Dies:
                    var die = new Die();
                    var dieName = typeof(Die).Name;

                    this.SelectedEntityType = typeof(Die);

                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_BasicInformation, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Die_Name, PropertyName = dieName + "." + ReflectionUtils.GetPropertyName(() => die.Name), ControlType = ValueControlTypes.TextExtendedTextBox, IsDefault = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Die_Type, PropertyName = dieName + "." + ReflectionUtils.GetPropertyName(() => die.Type), ControlType = ValueControlTypes.DieType });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Die_Investment, PropertyName = dieName + "." + ReflectionUtils.GetPropertyName(() => die.Investment), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Money });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Die_ReusableInvest, PropertyName = dieName + "." + ReflectionUtils.GetPropertyName(() => die.ReusableInvest), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Money });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Die_Wear, PropertyName = dieName + "." + ReflectionUtils.GetPropertyName(() => die.Wear), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Money });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Die_WearInPercentage, PropertyName = dieName + "." + ReflectionUtils.GetPropertyName(() => die.WearPercentage), ControlType = ValueControlTypes.RangeExtendedTextBox, UIUnit = MeasurementUnitsAdapter.UIPercentage, ControlInputType = TextBoxControlInputType.Percentage });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Die_Maintenance, PropertyName = dieName + "." + ReflectionUtils.GetPropertyName(() => die.Maintenance), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Money });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_KValue, PropertyName = dieName + "." + ReflectionUtils.GetPropertyName(() => die.KValue), ControlType = ValueControlTypes.RangeExtendedTextBox, UIUnit = MeasurementUnitsAdapter.UIPercentage, ControlInputType = TextBoxControlInputType.Percentage });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Die_LifeTime, PropertyName = dieName + "." + ReflectionUtils.GetPropertyName(() => die.LifeTime), ControlType = ValueControlTypes.RangeExtendedTextBox, UIUnit = MeasurementUnitsAdapter.UIPieces, ControlInputType = TextBoxControlInputType.Integer });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Die_AllocationOfCost, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Die_AllocationRatio, PropertyName = dieName + "." + ReflectionUtils.GetPropertyName(() => die.AllocationRatio), ControlType = ValueControlTypes.RangeExtendedTextBox, UIUnit = MeasurementUnitsAdapter.UIPercentage, ControlInputType = TextBoxControlInputType.Percentage });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Die_DiesetsNumberPaidByCustomer, PropertyName = dieName + "." + ReflectionUtils.GetPropertyName(() => die.DiesetsNumberPaidByCustomer), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Integer });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Die_CostAllocationBasedOnPartsPerLifeTime, PropertyName = dieName + "." + ReflectionUtils.GetPropertyName(() => die.CostAllocationBasedOnPartsPerLifeTime), ControlType = ValueControlTypes.CheckBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Die_CostAllocationNumberOfParts, PropertyName = dieName + "." + ReflectionUtils.GetPropertyName(() => die.CostAllocationNumberOfParts), ControlType = ValueControlTypes.RangeExtendedTextBox, ControlInputType = TextBoxControlInputType.Integer });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Die_Remark, PropertyName = dieName + "." + ReflectionUtils.GetPropertyName(() => die.Remark), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.General_Manufacturer, IsCategory = true });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Manufacturer_Name, PropertyName = manufacturerName + "." + ReflectionUtils.GetPropertyName(() => manufacturer.Name), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Manufacturer_Description, PropertyName = manufacturerName + "." + ReflectionUtils.GetPropertyName(() => manufacturer.Description), ControlType = ValueControlTypes.TextExtendedTextBox });
                    entityProperties.Add(new EntityProperty() { Text = LocalizedResources.Die_AllDiesPaidByCustomer, PropertyName = dieName + "." + ReflectionUtils.GetPropertyName(() => die.AreAllPaidByCustomer), ControlType = ValueControlTypes.CheckBox });

                    for (int i = 0; i < entityProperties.Count; i++)
                    {
                        entityProperties[i].Position = i;
                    }

                    break;
            }

            return entityProperties;
        }

        #region Inner Classes

        /// <summary>
        /// Contains the entity properties.
        /// </summary>
        public class EntityProperty : ObservableObject
        {
            /// <summary>
            /// The units system changed listener.
            /// </summary>
            private readonly WeakEventListener<PropertyChangedEventArgs> unitsSystemChangedListener;

            /// <summary>
            /// Initializes a new instance of the <see cref="EntityProperty"/> class.
            /// </summary>
            public EntityProperty()
            {
                this.unitsSystemChangedListener = new WeakEventListener<PropertyChangedEventArgs>(this.HandleUnitsSystemChanged);
                PropertyChangedEventManager.AddListener(
                    UserSettingsManager.Instance,
                    unitsSystemChangedListener,
                    ReflectionUtils.GetPropertyName(() => UserSettingsManager.Instance.UnitsSystem));
            }

            /// <summary>
            /// Gets or sets the text.
            /// </summary>
            public string Text { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether this instance is category.
            /// </summary>
            public bool IsCategory { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether this instance is default.
            /// </summary>
            public bool IsDefault { get; set; }

            /// <summary>
            /// Gets or sets the name of the property.
            /// </summary>
            public string PropertyName { get; set; }

            /// <summary>
            /// Gets or sets the searched properties.
            /// </summary>
            public Collection<string> SearchedProperties { get; set; }

            /// <summary>
            /// Gets or sets the type of the control.
            /// </summary>
            public ValueControlTypes ControlType { get; set; }

            /// <summary>
            /// Gets or sets the symbol.
            /// </summary>
            public string Symbol { get; set; }

            /// <summary>
            /// Gets or sets the type of the input.
            /// </summary>
            public TextBoxControlInputType ControlInputType { get; set; }

            /// <summary>
            /// Gets or sets the UI unit.
            /// </summary>
            public Unit UIUnit { get; set; }

            /// <summary>
            /// Gets or sets the position.
            /// </summary>
            public int Position { get; set; }

            /// <summary>
            /// Handles the system units changed.
            /// </summary>
            /// <param name="sender">The sender.</param>
            /// <param name="e">The <see cref="PropertyChangedEventArgs"/> instance containing the event data.</param>
            private void HandleUnitsSystemChanged(object sender, PropertyChangedEventArgs e)
            {
                if (this.UIUnit != null)
                {
                    this.UIUnit = AdvancedSearchConfigurationViewModel.MeasurementUnitsAdapter.GetCurrentUnit(this.UIUnit.Type);
                    this.OnPropertyChanged(() => this.UIUnit);
                }
            }
        }

        #endregion Inner Classes
    }
}