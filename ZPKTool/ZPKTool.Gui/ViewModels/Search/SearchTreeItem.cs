﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using ZPKTool.Business;
using ZPKTool.Data;
using ZPKTool.DataAccess;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Represents the data source for a result tree item in the Simple Search.
    /// </summary>
    public class SearchTreeItem
    {
        /// <summary>
        /// Specifies if the node is expanded
        /// </summary>
        private bool isExpanded;

        /// <summary>
        /// Specifies if the entity behind the node is loaded
        /// </summary>
        private bool isLoaded;

        /// <summary>
        /// Specifies if the node was expanded at some point in the past, meaning that the sub-entities are loaded already.
        /// </summary>
        private bool wasExpanded;

        /// <summary>
        /// Initializes a new instance of the <see cref="SearchTreeItem"/> class.
        /// </summary>
        /// <param name="entity">The entity backing the item.</param>
        /// <param name="fromCentral">if set to true, the entity comes from the central database; otherwise, it comes from the local database.</param>
        /// <param name="name">The display name for the item.</param>
        /// <param name="icon">The icon to use for the item.</param>
        /// <param name="topIcon">The icon to be displayed in the top right corner of the item.</param>
        /// <param name="bottomIcon">The icon to be displayed in the bottom right corner of the item.</param>
        public SearchTreeItem(object entity, bool fromCentral, string name, ImageSource icon, ImageSource topIcon, ImageSource bottomIcon)
        {
            if (entity == null)
            {
                this.IsRootNode = true;
            }
            else
            {
                this.IsRootNode = false;
            }

            this.Entity = entity;
            this.IsFromCentral = fromCentral;
            this.Label = name;
            this.Icon = icon;
            this.BottomRightCornerIcon = bottomIcon;
            this.TopRightCornerIcon = topIcon;
            this.Children = new ObservableCollection<SearchTreeItem>();
            this.IsExpanded = false;
            this.CircularProgressVisibility = Visibility.Collapsed;
            this.CopyCommandEnabled = true;

            // Go to disabled for master data objects when the user does not have the right to edit
            if (this.Entity != null && this.Entity is IMasterDataObject &&
                (this.Entity as IMasterDataObject).IsMasterData &&
                !SecurityManager.Instance.CurrentUserHasRight(Right.EditMasterData))
            {
                GoToCommandEnabled = false;
            }
            else
            {
                GoToCommandEnabled = true;
            }

            // If the node can be expanded, then add a dummy item to show the expander.
            if (this.Entity != null && (this.Entity is Project || this.Entity is Assembly || this.Entity is Part || this.Entity is ProcessStep))
            {
                SearchTreeItem item = null;
                this.Children.Add(item);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance represents a root node (item).
        /// </summary>
        public bool IsRootNode { get; set; }

        /// <summary>
        /// Gets or sets the entity backing this instance.
        /// </summary>
        public object Entity { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the backing entity is from the central or local database.
        /// </summary>
        public bool IsFromCentral { get; set; }

        /// <summary>
        /// Gets or sets the icon of the item
        /// </summary>
        public ImageSource Icon { get; set; }

        /// <summary>
        /// Gets or sets the status icon (the icon on the bottom right corner).
        /// </summary>
        public ImageSource BottomRightCornerIcon { get; set; }

        /// <summary>
        /// Gets or sets  the icon in the item's top right corner.
        /// </summary>
        public ImageSource TopRightCornerIcon { get; set; }

        /// <summary>
        /// Gets or sets the text of the item
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// Gets the collection of children.
        /// </summary>
        public ObservableCollection<SearchTreeItem> Children { get; private set; }

        /// <summary>
        /// Gets or sets the visibility of the progress animation displayed while this instance loads its children.
        /// </summary>
        public Visibility CircularProgressVisibility { get; set; }
                
        /// <summary>
        /// Gets or sets a value indicating whether if the item is expanded.
        /// </summary>
        public bool IsExpanded
        {
            get
            {
                return isExpanded;
            }

            set
            {
                isExpanded = value;
                if (value && !IsRootNode && !wasExpanded)
                {
                    LoadSubEntities();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the copy command is enabled in the context menu.
        /// </summary>
        //// TODO: this should be done from the command's CanExecute handler.
        public bool CopyCommandEnabled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the go to command is enabled in the context menu.
        /// </summary>
        //// TODO: this should be done from the command's CanExecute handler.
        public bool GoToCommandEnabled { get; set; }

        /// <summary>
        /// Starts the asynchronous loading of the sub-entities
        /// </summary>
        private void LoadSubEntities()
        {
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += new DoWorkEventHandler(AsnychronousLoad);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(AsynchronousLoadFinished);
            this.Children.Clear();
            this.Children.Add(SearchTreeItemFactory.GetLoadingNode());
            worker.RunWorkerAsync();
        }

        /// <summary>
        /// Adds the loaded sub-entities in the tree
        /// </summary>
        private void AddSubEntities()
        {
            // remove the dummy element
            this.Children.Clear();

            var project = this.Entity as Project;
            if (project != null)
            {
                foreach (Assembly subAssembly in project.Assemblies.Where(a => !a.IsDeleted))
                {
                    SearchTreeItem item = SearchTreeItemFactory.GetTreeNode(subAssembly, IsFromCentral);
                    
                    // the sub-entity is also fully loaded
                    item.isLoaded = true;
                    this.Children.Add(item);
                }

                foreach (Part subPart in project.Parts.Where(p => !p.IsDeleted))
                {
                    SearchTreeItem item = SearchTreeItemFactory.GetTreeNode(subPart, IsFromCentral);
                    
                    // the sub-entity is also fully loaded
                    item.isLoaded = true;
                    this.Children.Add(item);
                }
            }

            var assembly = this.Entity as Assembly;
            if (assembly != null)
            {
                if (assembly.Subassemblies.Count > 0)
                {
                    SearchTreeItem group = SearchTreeItemFactory.GetRootNode(SearchTreeItemType.Subassemblies);
                    this.Children.Add(group);
                    foreach (Assembly subAssembly in assembly.Subassemblies.Where(a => !a.IsDeleted))
                    {
                        SearchTreeItem item = SearchTreeItemFactory.GetTreeNode(subAssembly, IsFromCentral);
                        item.isLoaded = true;
                        group.Children.Add(item);
                    }
                }

                if (assembly.Parts.Count > 0)
                {
                    SearchTreeItem group = SearchTreeItemFactory.GetRootNode(SearchTreeItemType.Parts);
                    this.Children.Add(group);
                    foreach (Part subPart in assembly.Parts.Where(p => !p.IsDeleted))
                    {
                        SearchTreeItem item = SearchTreeItemFactory.GetTreeNode(subPart, IsFromCentral);
                        item.isLoaded = true;
                        group.Children.Add(item);
                    }
                }

                if (assembly.Process != null && assembly.Process.Steps.Count > 0)
                {
                    SearchTreeItem group = SearchTreeItemFactory.GetRootNode(SearchTreeItemType.Process);
                    this.Children.Add(group);
                    foreach (ProcessStep step in assembly.Process.Steps)
                    {
                        SearchTreeItem item = SearchTreeItemFactory.GetTreeNode(step, IsFromCentral);
                        item.isLoaded = true;
                        group.Children.Add(item);
                    }
                }
            }

            Part part = this.Entity as Part;
            if (part != null)
            {
                if (part.RawMaterials.Count > 0)
                {
                    SearchTreeItem group = SearchTreeItemFactory.GetRootNode(SearchTreeItemType.RawMaterials);
                    this.Children.Add(group);
                    foreach (RawMaterial material in part.RawMaterials.Where(m => !m.IsDeleted))
                    {
                        SearchTreeItem item = SearchTreeItemFactory.GetTreeNode(material, IsFromCentral);
                        item.isLoaded = true;
                        group.Children.Add(item);
                    }
                }

                if (part.Commodities.Count > 0)
                {
                    SearchTreeItem group = SearchTreeItemFactory.GetRootNode(SearchTreeItemType.Commodities);
                    this.Children.Add(group);
                    foreach (Commodity commodity in part.Commodities.Where(c => !c.IsDeleted))
                    {
                        SearchTreeItem item = SearchTreeItemFactory.GetTreeNode(commodity, IsFromCentral);
                        item.isLoaded = true;
                        group.Children.Add(item);
                    }
                }

                RawPart rawPart = part.RawPart as RawPart;
                if (rawPart != null && !rawPart.IsDeleted)
                {
                    SearchTreeItem rawPartItem = SearchTreeItemFactory.GetTreeNode(rawPart, IsFromCentral);
                    rawPartItem.isLoaded = true;
                    this.Children.Add(rawPartItem);
                }

                if (part.Process != null && part.Process.Steps.Count > 0)
                {
                    SearchTreeItem group = SearchTreeItemFactory.GetRootNode(SearchTreeItemType.Process);
                    this.Children.Add(group);
                    foreach (ProcessStep step in part.Process.Steps)
                    {
                        SearchTreeItem item = SearchTreeItemFactory.GetTreeNode(step, IsFromCentral);
                        item.isLoaded = true;
                        group.Children.Add(item);
                    }
                }
            }

            var processStep = this.Entity as ProcessStep;
            if (processStep != null)
            {
                if (processStep.Machines.Count > 0)
                {
                    SearchTreeItem group = SearchTreeItemFactory.GetRootNode(SearchTreeItemType.Machines);
                    this.Children.Add(group);
                    foreach (Machine machine in processStep.Machines.Where(m => !m.IsDeleted))
                    {
                        SearchTreeItem item = SearchTreeItemFactory.GetTreeNode(machine, IsFromCentral);
                        item.isLoaded = true;
                        group.Children.Add(item);
                    }
                }

                if (processStep.Dies.Count > 0)
                {
                    SearchTreeItem group = SearchTreeItemFactory.GetRootNode(SearchTreeItemType.Die);
                    this.Children.Add(group);
                    foreach (Die die in processStep.Dies.Where(d => !d.IsDeleted))
                    {
                        SearchTreeItem item = SearchTreeItemFactory.GetTreeNode(die, IsFromCentral);
                        item.isLoaded = true;
                        group.Children.Add(item);
                    }
                }

                if (processStep.Commodities.Count > 0)
                {
                    SearchTreeItem group = SearchTreeItemFactory.GetRootNode(SearchTreeItemType.Commodities);
                    this.Children.Add(group);
                    foreach (Commodity commodity in processStep.Commodities.Where(c => !c.IsDeleted))
                    {
                        SearchTreeItem item = SearchTreeItemFactory.GetTreeNode(commodity, IsFromCentral);
                        item.isLoaded = true;
                        group.Children.Add(item);
                    }
                }

                if (processStep.Consumables.Count > 0)
                {
                    SearchTreeItem group = SearchTreeItemFactory.GetRootNode(SearchTreeItemType.Consumables);
                    this.Children.Add(group);
                    foreach (Consumable consumable in processStep.Consumables.Where(c => !c.IsDeleted))
                    {
                        SearchTreeItem item = SearchTreeItemFactory.GetTreeNode(consumable, IsFromCentral);
                        item.isLoaded = true;
                        group.Children.Add(item);
                    }
                }
            }

            // the entity becomes loaded and was expanded
            this.isLoaded = true;
            this.wasExpanded = true;
        }

        /// <summary>
        /// Handler for the worker completed event
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RunWorkerCompletedEventArgs" /> instance containing the event data.</param>
        private void AsynchronousLoadFinished(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                throw e.Error;
            }
            else
            {
                AddSubEntities();
            }
        }

        /// <summary>
        /// Handler for the asynchronous loading of the entity
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.ComponentModel.DoWorkEventArgs"/> instance containing the event data.</param>
        private void AsnychronousLoad(object sender, DoWorkEventArgs e)
        {
            // if the current entity is not loaded, then go to the database and get it full.
            if (this.isLoaded)
            {
                return;
            }

            DbIdentifier context = this.IsFromCentral ? DbIdentifier.CentralDatabase : DbIdentifier.LocalDatabase;
            IDataSourceManager dc = DataAccessFactory.CreateDataSourceManager(context);

            if (this.Entity is Project)
            {
                Project fullProj = dc.ProjectRepository.GetProjectFull(((Project)Entity).Guid);
                this.Entity = fullProj;
            }

            if (this.Entity is Assembly)
            {
                Assembly fullAssembly = dc.AssemblyRepository.GetAssemblyFull(((Assembly)Entity).Guid);
                this.Entity = fullAssembly;
            }

            if (this.Entity is Part)
            {
                Part fullPart = dc.PartRepository.GetPartFull(((Part)Entity).Guid);
                this.Entity = fullPart;
            }

            if (this.Entity is ProcessStep)
            {
                ProcessStep fullProcessStep = dc.ProcessStepRepository.GetProcessStepFull(((ProcessStep)Entity).Guid);
                this.Entity = fullProcessStep;
            }
        }        
    }
}
