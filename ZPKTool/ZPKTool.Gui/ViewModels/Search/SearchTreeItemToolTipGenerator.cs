﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Controls;
using ZPKTool.Data;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Resources;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Generates the tooltip content for a search result item.
    /// </summary>
    public static class SearchTreeItemToolTipGenerator
    {
        /// <summary>
        /// A dictionary which contains the type of an entity and a collection of Pair &lt;string,string&gt; which contains 
        /// properties names and UI properties names of entity, which has to be displayed in the toolTip.
        /// </summary>
        private static Dictionary<Type, Collection<Tuple<string, string>>> typeProps = new Dictionary<Type, Collection<Tuple<string, string>>>();

        /// <summary>
        /// Initializes static members of the <see cref="SearchTreeItemToolTipGenerator"/> class.
        /// </summary>
        static SearchTreeItemToolTipGenerator()
        {
            Intitialize();
        }

        /// <summary>
        /// Generates the content of the tooltip.
        /// </summary>
        /// <param name="entity">The entity which represents the parent of the ToolTip.</param>
        /// <param name="cost">An object containing the cost of the entity.</param>
        /// <param name="measurementUnitsAdapter">The measurement units adapter.</param>
        /// <returns>
        /// UIElement - which represents the content of the tooltip.
        /// </returns>
        public static UIElement GenerateTooltipContent(object entity, object cost, UnitsAdapter measurementUnitsAdapter)
        {
            Collection<Tuple<string, string>> props;

            if (!typeProps.TryGetValue(entity.GetType(), out props))
            {
                return null;
            }

            Grid content = new Grid();
            ColumnDefinition nameColumn = new ColumnDefinition();
            ColumnDefinition valueColumn = new ColumnDefinition();
            content.HorizontalAlignment = HorizontalAlignment.Center;
            content.VerticalAlignment = VerticalAlignment.Center;
            content.ColumnDefinitions.Add(nameColumn);
            content.ColumnDefinitions.Add(valueColumn);

            foreach (Tuple<string, string> property in props)
            {
                LabelControl propertyNameLabel = new LabelControl();
                LabelControl propertyValueLabel = new LabelControl();
                RowDefinition row = new RowDefinition();
                ExtendedLabelControl extendedLabel = new ExtendedLabelControl();

                propertyNameLabel.Text = property.Item2 + ": ";
                content.RowDefinitions.Add(row);
                Grid.SetRow(propertyNameLabel, content.RowDefinitions.Count - 1);
                content.Children.Add(propertyNameLabel);

                switch (property.Item1)
                {
                    case "Cost":
                        extendedLabel.BaseCurrency = measurementUnitsAdapter.BaseCurrency;
                        extendedLabel.UIUnit = measurementUnitsAdapter.UICurrency;
                        extendedLabel.BaseValue = cost as decimal?;
                        Grid.SetRow(extendedLabel, content.RowDefinitions.Count - 1);
                        Grid.SetColumn(extendedLabel, 1);
                        content.Children.Add(extendedLabel);
                        break;
                    case "MaintenancePerHour":
                    case "DepreciationPerHour":
                    case "MachineInvestment":
                        extendedLabel.BaseCurrency = measurementUnitsAdapter.BaseCurrency;
                        extendedLabel.UIUnit = measurementUnitsAdapter.UICurrency;
                        extendedLabel.BaseValue = (decimal?)GetPropertyValue(entity, property.Item1);
                        Grid.SetRow(extendedLabel, content.RowDefinitions.Count - 1);
                        Grid.SetColumn(extendedLabel, 1);
                        content.Children.Add(extendedLabel);
                        break;
                    case "FloorSize":
                        extendedLabel.UIUnit = measurementUnitsAdapter.UIArea;
                        extendedLabel.BaseValue = (decimal?)GetPropertyValue(entity, property.Item1);
                        Grid.SetRow(extendedLabel, content.RowDefinitions.Count - 1);
                        Grid.SetColumn(extendedLabel, 1);
                        content.Children.Add(extendedLabel);
                        break;
                    case "EnergyCostPerHour":
                        StackPanel stackPanel = new StackPanel();
                        LabelControl symbolLabel = new LabelControl();
                        stackPanel.Orientation = Orientation.Horizontal;
                        extendedLabel.BaseCurrency = measurementUnitsAdapter.BaseCurrency;
                        extendedLabel.UIUnit = measurementUnitsAdapter.UICurrency;
                        extendedLabel.BaseValue = (decimal?)GetPropertyValue(entity, property.Item1);
                        symbolLabel.Text = "/h";
                        stackPanel.Children.Add(extendedLabel);
                        stackPanel.Children.Add(symbolLabel);
                        Grid.SetRow(stackPanel, content.RowDefinitions.Count - 1);
                        Grid.SetColumn(stackPanel, 1);
                        content.Children.Add(stackPanel);
                        break;
                    case "CalculationVariant":
                        string calculationVariant = (string)GetPropertyValue(entity, property.Item1);
                        if (!string.IsNullOrEmpty(calculationVariant))
                        {
                            propertyValueLabel.Text = calculationVariant;
                        }
                        else
                        {
                            propertyValueLabel.Text = CostCalculatorFactory.OldestVersion;
                        }

                        Grid.SetRow(propertyValueLabel, content.RowDefinitions.Count - 1);
                        Grid.SetColumn(propertyValueLabel, 1);
                        content.Children.Add(propertyValueLabel);
                        break;
                    default:
                        string value = (string)GetPropertyValue(entity, property.Item1);
                        if (!string.IsNullOrEmpty(value))
                        {
                            propertyValueLabel.Text = value;
                        }
                        else
                        {
                            propertyValueLabel.Text = LocalizedResources.General_NotAvailable;
                        }

                        Grid.SetRow(propertyValueLabel, content.RowDefinitions.Count - 1);
                        Grid.SetColumn(propertyValueLabel, 1);
                        content.Children.Add(propertyValueLabel);
                        break;
                }
            }

            // Display the Machine Cost in the tooltip
            var machineCost = cost as ZPKTool.Calculations.CostCalculation.MachineCost;
            if (machineCost != null)
            {
                // Display the DepreciationPerHour cost
                content.RowDefinitions.Add(new RowDefinition());
                LabelControl deprLabel = new LabelControl() { Text = LocalizedResources.General_DeprecationPerHour + ": " };
                Grid.SetRow(deprLabel, content.RowDefinitions.Count - 1);
                content.Children.Add(deprLabel);

                ExtendedLabelControl deprValue = new ExtendedLabelControl();
                deprValue.BaseCurrency = measurementUnitsAdapter.BaseCurrency;
                deprValue.UIUnit = measurementUnitsAdapter.UICurrency;
                deprValue.BaseValue = machineCost.DepreciationPerHour;
                Grid.SetRow(deprValue, content.RowDefinitions.Count - 1);
                Grid.SetColumn(deprValue, 1);
                content.Children.Add(deprValue);

                // Display the EnergyCostPerHour
                content.RowDefinitions.Add(new RowDefinition());
                LabelControl energyLabel = new LabelControl() { Text = LocalizedResources.General_EnergyCostPerHour + ": " };
                Grid.SetRow(energyLabel, content.RowDefinitions.Count - 1);
                content.Children.Add(energyLabel);

                ExtendedLabelControl energyValue = new ExtendedLabelControl();
                energyValue.BaseCurrency = measurementUnitsAdapter.BaseCurrency;
                energyValue.UIUnit = measurementUnitsAdapter.UICurrency;
                energyValue.BaseValue = machineCost.EnergyCostPerHour;
                energyValue.StringFormat = "{0} {1}/" + "h";
                Grid.SetRow(energyValue, content.RowDefinitions.Count - 1);
                Grid.SetColumn(energyValue, 1);
                content.Children.Add(energyValue);

                // Display the MaintenancePerHour
                content.RowDefinitions.Add(new RowDefinition());
                LabelControl maintenanceLabel = new LabelControl() { Text = LocalizedResources.General_MaintenancePerHour + ": " };
                Grid.SetRow(maintenanceLabel, content.RowDefinitions.Count - 1);
                content.Children.Add(maintenanceLabel);

                ExtendedLabelControl maintenanceValue = new ExtendedLabelControl();
                maintenanceValue.BaseCurrency = measurementUnitsAdapter.BaseCurrency;
                maintenanceValue.UIUnit = measurementUnitsAdapter.UICurrency;
                maintenanceValue.BaseValue = machineCost.MaintenancePerHour;
                Grid.SetRow(maintenanceValue, content.RowDefinitions.Count - 1);
                Grid.SetColumn(maintenanceValue, 1);
                content.Children.Add(maintenanceValue);

                // Display the FullCostPerHour
                content.RowDefinitions.Add(new RowDefinition());
                LabelControl fullCostLabel = new LabelControl() { Text = LocalizedResources.General_FullMachineCostPerHour + ": " };
                Grid.SetRow(fullCostLabel, content.RowDefinitions.Count - 1);
                content.Children.Add(fullCostLabel);

                ExtendedLabelControl fullCostValue = new ExtendedLabelControl();
                fullCostValue.BaseCurrency = measurementUnitsAdapter.BaseCurrency;
                fullCostValue.UIUnit = measurementUnitsAdapter.UICurrency;
                fullCostValue.BaseValue = machineCost.FullCostPerHour;
                Grid.SetRow(fullCostValue, content.RowDefinitions.Count - 1);
                Grid.SetColumn(fullCostValue, 1);
                content.Children.Add(fullCostValue);
            }

            return content;
        }

        /// <summary>
        /// Returns the value of the property from the given object.
        /// Uses reflection to get the property.
        /// Can handle one sub object e.g. Manufacturer.Name on the Die object.
        /// </summary>
        /// <param name="obj">The object where to search for the property.</param>
        /// <param name="propertyName">The property needed.</param>
        /// <returns>The value of the property.</returns>
        private static object GetPropertyValue(object obj, string propertyName)
        {
            // define the resulting property value
            object result = null;

            // use reflection to get the type of the object
            Type type = obj.GetType();

            // check if we have a complex property
            if (propertyName.Contains("."))
            {
                // separate the properties - navigator and property
                string initialProperty = propertyName.Substring(0, propertyName.IndexOf('.'));
                string secondProperty = propertyName.Substring(propertyName.IndexOf('.') + 1);

                // get the initial property information
                System.Reflection.PropertyInfo pi = type.GetProperty(initialProperty);
                if (pi != null && pi.CanRead)
                {
                    // get the actual object - the navigator
                    object intermObject = pi.GetValue(obj, null);
                    if (intermObject != null)
                    {
                        // perform the reflection on the navigator
                        Type intermType = intermObject.GetType();

                        // get the secondary property
                        System.Reflection.PropertyInfo intermPi = intermType.GetProperty(secondProperty);
                        if (intermPi != null && intermPi.CanRead)
                        {
                            var value = intermPi.GetValue(intermObject, null);
                            if (value != null)
                            {
                                result = value;
                            }
                        }
                    }
                }
            }
            else
            {
                // simple property - use reflection on the object
                System.Reflection.PropertyInfo pi = type.GetProperty(propertyName);
                if (pi != null && pi.CanRead)
                {
                    var value = pi.GetValue(obj, null);
                    if (value != null)
                    {
                        result = value;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Initializes the typeProps dictionary.
        /// </summary>
        private static void Intitialize()
        {
            Collection<Tuple<string, string>> projectProps = new Collection<Tuple<string, string>>()
            {
                new Tuple<string, string>("Name", LocalizedResources.General_Name),
                new Tuple<string, string>("Number", LocalizedResources.General_Number),
                new Tuple<string, string>("Customer.Name", LocalizedResources.General_Supplier)
            };
            typeProps.Add(typeof(Project), projectProps);

            Collection<Tuple<string, string>> assemblyProps = new Collection<Tuple<string, string>>()
            {
                new Tuple<string, string>("Name", LocalizedResources.General_Name),
                new Tuple<string, string>("Manufacturer.Name", LocalizedResources.General_Manufacturer),
                new Tuple<string, string>("CalculationVariant", LocalizedResources.General_CalculationVariant),
                new Tuple<string, string>("Cost", "Cost")
            };
            typeProps.Add(typeof(Assembly), assemblyProps);

            Collection<Tuple<string, string>> partProps = new Collection<Tuple<string, string>>()
            {
                new Tuple<string, string>("Name", LocalizedResources.General_Name),
                new Tuple<string, string>("Manufacturer.Name", LocalizedResources.General_Manufacturer),
                new Tuple<string, string>("CalculationVariant", LocalizedResources.General_CalculationVariant),
                new Tuple<string, string>("Cost", "Cost")
            };
            typeProps.Add(typeof(Part), partProps);
            typeProps.Add(typeof(RawPart), partProps);

            Collection<Tuple<string, string>> partProcessStepProps = new Collection<Tuple<string, string>>()
            {
                new Tuple<string, string>("Name", LocalizedResources.ProcessStep_Name),
                new Tuple<string, string>("Type.Name", LocalizedResources.General_Type),
                new Tuple<string, string>("SubType.Name", "Subtype")
            };
            typeProps.Add(typeof(PartProcessStep), partProcessStepProps);

            Collection<Tuple<string, string>> assemblyProcessStepProps = new Collection<Tuple<string, string>>()
            {
                new Tuple<string, string>("Name", LocalizedResources.ProcessStep_Name),
                new Tuple<string, string>("Type.Name", LocalizedResources.General_Type),
                new Tuple<string, string>("SubType.Name", "Subtype")
            };
            typeProps.Add(typeof(AssemblyProcessStep), assemblyProcessStepProps);

            Collection<Tuple<string, string>> rawMaterialProps = new Collection<Tuple<string, string>>()
            {
                new Tuple<string, string>("Name", LocalizedResources.RawMaterial_Name),
                new Tuple<string, string>("NameUK", LocalizedResources.RawMaterial_NameUK),
                new Tuple<string, string>("NameUS", LocalizedResources.RawMaterial_NameUS),
                new Tuple<string, string>("Manufacturer.Name", LocalizedResources.General_Manufacturer),
                new Tuple<string, string>("Cost", LocalizedResources.General_Cost)
            };
            typeProps.Add(typeof(RawMaterial), rawMaterialProps);

            Collection<Tuple<string, string>> commodityProps = new Collection<Tuple<string, string>>()
            {
                new Tuple<string, string>("Name", LocalizedResources.Commodity_Name),
                new Tuple<string, string>("Manufacturer.Name", LocalizedResources.General_Manufacturer),
                new Tuple<string, string>("Cost", LocalizedResources.General_Cost)
            };
            typeProps.Add(typeof(Commodity), commodityProps);

            Collection<Tuple<string, string>> consumableProps = new Collection<Tuple<string, string>>()
            {
                new Tuple<string, string>("Name", LocalizedResources.Consumable_Name),
                new Tuple<string, string>("Manufacturer.Name", LocalizedResources.General_Manufacturer),
                new Tuple<string, string>("Cost", LocalizedResources.General_Cost)
            };
            typeProps.Add(typeof(Consumable), consumableProps);

            Collection<Tuple<string, string>> dieProps = new Collection<Tuple<string, string>>()
            {
                new Tuple<string, string>("Name", LocalizedResources.Die_Name),
                new Tuple<string, string>("Manufacturer.Name", LocalizedResources.General_Manufacturer),
                new Tuple<string, string>("Cost", LocalizedResources.General_Cost)
            };
            typeProps.Add(typeof(Die), dieProps);

            Collection<Tuple<string, string>> machineProps = new Collection<Tuple<string, string>>()
            {
                new Tuple<string, string>("Name", LocalizedResources.Machine_Name),
                new Tuple<string, string>("Manufacturer.Name", LocalizedResources.General_Manufacturer),
                new Tuple<string, string>("MachineInvestment", LocalizedResources.General_Investment),
                new Tuple<string, string>("FloorSize", LocalizedResources.Machine_FloorSize)
            };
            typeProps.Add(typeof(Machine), machineProps);
        }
    }
}
