﻿namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Represents the value input options.
    /// </summary>
    public enum ValueInputOptions
    {
        /// <summary>
        /// This item represents a single value.
        /// </summary>
        SingleValue = 0,

        /// <summary>
        /// This item represents a range.
        /// </summary>
        Range = 1
    }
}
