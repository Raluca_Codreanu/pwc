﻿using System.Windows.Media;
using ZPKTool.Data;
using ZPKTool.Gui.Resources;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Factory class for creating <see cref="SearchTreeItem"/> instances.
    /// </summary>
    public class SearchTreeItemFactory
    {
        /// <summary>
        /// Returns a new instance of SearchTreeItem in the form of a project node
        /// </summary>
        /// <param name="project">reference to the project entity</param>
        /// <param name="isFromCentral">specifies if the project is taken from the central or local database</param>
        /// <returns>The created <see cref="SearchTreeItem"/> instance.</returns>
        private static SearchTreeItem GetProjectNode(Project project, bool isFromCentral)
        {
            string name = project.Name;
            ImageSource icon = Images.ProjectIcon;
            ImageSource bottomIcon = null;

            if (project.Number != null)
            {
                name += " (" + project.Number + ")";
            }

            if (isFromCentral)
            {
                bottomIcon = Images.OnlineIcon;
            }

            SearchTreeItem treeItem = new SearchTreeItem(project, isFromCentral, name, icon, null, bottomIcon);
            return treeItem;
        }

        /// <summary>
        /// Returns a new instance of SearchTreeItem in the form of an assembly node
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <param name="isFromCentral">specifies if the assembly is taken from the central or local database</param>
        /// <returns>The created <see cref="SearchTreeItem"/> instance.</returns>
        private static SearchTreeItem GetAssemblyNode(Assembly assembly, bool isFromCentral)
        {
            string name = assembly.Name;
            ImageSource icon = Images.AssemblyIcon;
            ImageSource bottomIcon = null;
            ImageSource topIcon = null;

            if (assembly.Number != null)
            {
                name += " (" + assembly.Number + ")";
            }

            if (assembly.IsExternal == true)
            {
                topIcon = Images.IsExternalIcon;
            }

            if (assembly.IsMasterData)
            {
                bottomIcon = assembly.IsStockMasterData ? Images.MasterDataIcon : Images.CustomMasterDataIcon;
            }
            else if (isFromCentral)
            {
                bottomIcon = Images.OnlineIcon;
            }

            SearchTreeItem treeItem = new SearchTreeItem(assembly, isFromCentral, name, icon, topIcon, bottomIcon);
            return treeItem;
        }

        /// <summary>
        /// Returns a new instance of SearchTreeItem in the form of a part node
        /// </summary>
        /// <param name="part">The part.</param>
        /// <param name="isFromCentral">specifies if the part is taken from the central or local database</param>
        /// <returns>The created <see cref="SearchTreeItem"/> instance.</returns>
        private static SearchTreeItem GetPartNode(Part part, bool isFromCentral)
        {
            ImageSource icon = part is RawPart ? Images.RawPartIcon : Images.PartIcon;
            ImageSource topIcon = null;
            ImageSource bottomIcon = null;
            string name = part.Name;

            if (part.Number != null)
            {
                name += " (" + part.Number + ")";
            }

            if (part.IsExternal == true)
            {
                topIcon = Images.IsExternalIcon;
            }

            if (part.IsMasterData)
            {
                bottomIcon = part.IsStockMasterData ? Images.MasterDataIcon : Images.CustomMasterDataIcon;
            }
            else if (isFromCentral)
            {
                bottomIcon = Images.OnlineIcon;
            }

            SearchTreeItem treeItem = new SearchTreeItem(part, isFromCentral, name, icon, topIcon, bottomIcon);
            return treeItem;
        }

        /// <summary>
        /// Returns a new instance of SearchTreeItem in the form of a raw material node
        /// </summary>
        /// <param name="material">The material.</param>
        /// <param name="isFromCentral">specifies if the raw material is taken from the central or local database</param>
        /// <returns>The created <see cref="SearchTreeItem"/> instance.</returns>
        private static SearchTreeItem GetRawMaterialNode(RawMaterial material, bool isFromCentral)
        {
            string name = null;
            ImageSource icon = Images.RawMaterialIcon;
            ImageSource bottomIcon = null;

            // 0 - german, 1 - uk, 2 - us
            int nameTypeValue = ZPKTool.Common.UserSettingsManager.Instance.SearchRawMaterialLocalisedName;
            if (nameTypeValue == 1)
            {
                name = material.NameUK;
            }
            else if (nameTypeValue == 2)
            {
                name = material.NameUS;
            }

            // german name default
            if (string.IsNullOrEmpty(name))
            {
                name = material.Name;
            }

            if (material.IsMasterData)
            {
                bottomIcon = material.IsStockMasterData ? Images.MasterDataIcon : Images.CustomMasterDataIcon;
            }
            else if (isFromCentral)
            {
                bottomIcon = Images.OnlineIcon;
            }

            SearchTreeItem treeItem = new SearchTreeItem(material, isFromCentral, name, icon, null, bottomIcon);
            return treeItem;
        }

        /// <summary>
        /// Returns a new instance of SearchTreeItem in the form of a machine node
        /// </summary>
        /// <param name="machine">The machine.</param>
        /// <param name="isFromCentral">specifies if the machine is taken from the central or local database</param>
        /// <returns>The created <see cref="SearchTreeItem"/> instance.</returns>
        private static SearchTreeItem GetMachineNode(Machine machine, bool isFromCentral)
        {
            ImageSource icon = Images.MachineIcon;
            ImageSource bottomIcon = null;
            string name = machine.Name;

            if (machine.IsMasterData)
            {
                bottomIcon = machine.IsStockMasterData ? Images.MasterDataIcon : Images.CustomMasterDataIcon;
            }
            else if (isFromCentral)
            {
                bottomIcon = Images.OnlineIcon;
            }

            SearchTreeItem treeItem = new SearchTreeItem(machine, isFromCentral, name, icon, null, bottomIcon);
            return treeItem;
        }

        /// <summary>
        /// Returns a new instance of SearchTreeItem in the form of a commodity node
        /// </summary>
        /// <param name="commodity">The commodity.</param>
        /// <param name="isFromCentral">specifies if the commodity is taken from the central or local database</param>
        /// <returns>The created <see cref="SearchTreeItem"/> instance.</returns>
        private static SearchTreeItem GetCommodityNode(Commodity commodity, bool isFromCentral)
        {
            ImageSource icon = Images.CommodityIcon;
            ImageSource topIcon = Images.IsExternalIcon;
            ImageSource bottomIcon = null;
            string name = commodity.Name;

            if (commodity.IsMasterData)
            {
                bottomIcon = commodity.IsStockMasterData ? Images.MasterDataIcon : Images.CustomMasterDataIcon;
            }
            else if (isFromCentral)
            {
                bottomIcon = Images.OnlineIcon;
            }

            SearchTreeItem treeItem = new SearchTreeItem(commodity, isFromCentral, name, icon, topIcon, bottomIcon);
            return treeItem;
        }

        /// <summary>
        /// Returns a new instance of SearchTreeItem in the form of a consumable node
        /// </summary>
        /// <param name="consumable">The consumable.</param>
        /// <param name="isFromCentral">specifies if the consumable is taken from the central or local database</param>
        /// <returns>The created <see cref="SearchTreeItem"/> instance.</returns>
        private static SearchTreeItem GetConsumableNode(Consumable consumable, bool isFromCentral)
        {
            ImageSource icon = Images.ConsumableIcon;
            ImageSource bottomIcon = null;
            string name = consumable.Name;

            if (consumable.IsMasterData)
            {
                bottomIcon = consumable.IsStockMasterData ? Images.MasterDataIcon : Images.CustomMasterDataIcon;
            }
            else if (isFromCentral)
            {
                bottomIcon = Images.OnlineIcon;
            }

            SearchTreeItem treeItem = new SearchTreeItem(consumable, isFromCentral, name, icon, null, bottomIcon);
            return treeItem;
        }

        /// <summary>
        /// Returns a new instance of SearchTreeItem in the form of a die node
        /// </summary>
        /// <param name="die">The die.</param>
        /// <param name="isFromCentral">specifies if the die is taken from the central or local database</param>
        /// <returns>The created <see cref="SearchTreeItem"/> instance.</returns>
        private static SearchTreeItem GetDieNode(Die die, bool isFromCentral)
        {
            ImageSource icon = Images.DieIcon;
            ImageSource bottomIcon = null;
            string name = die.Name;

            if (die.IsMasterData)
            {
                bottomIcon = die.IsStockMasterData ? Images.MasterDataIcon : Images.CustomMasterDataIcon;
            }
            else if (isFromCentral)
            {
                bottomIcon = Images.OnlineIcon;
            }

            SearchTreeItem treeItem = new SearchTreeItem(die, isFromCentral, name, icon, null, bottomIcon);
            return treeItem;
        }

        /// <summary>
        /// Returns a new instance of SearchTreeItem in the form of a process step node
        /// </summary>
        /// <param name="step">The step.</param>
        /// <param name="isFromCentral">specifies if the process step is taken from the central or local database</param>
        /// <returns>The created <see cref="SearchTreeItem"/> instance.</returns>
        private static SearchTreeItem GetProcessStepNode(ProcessStep step, bool isFromCentral)
        {
            ImageSource icon = Images.ProcessStepIcon;
            ImageSource bottomIcon = null;
            string name = step.Name;

            if (step.IsMasterData)
            {
                bottomIcon = step.IsStockMasterData ? Images.MasterDataIcon : Images.CustomMasterDataIcon;
            }
            else if (isFromCentral)
            {
                bottomIcon = Images.OnlineIcon;
            }

            SearchTreeItem treeItem = new SearchTreeItem(step, isFromCentral, name, icon, null, bottomIcon);
            return treeItem;
        }

        /// <summary>
        /// Returns a new instance of SearchTreeItem in the form of a loading node
        /// </summary>
        /// <returns>The created <see cref="SearchTreeItem"/> instance.</returns>
        public static SearchTreeItem GetLoadingNode()
        {
            SearchTreeItem loadingNode = new SearchTreeItem(null, false, LocalizedResources.General_Loading, null, null, null);
            loadingNode.CircularProgressVisibility = System.Windows.Visibility.Visible;
            return loadingNode;
        }

        /// <summary>
        /// Returns a new instance of SearchTreeItem depending on the input parameter
        /// </summary>
        /// <param name="item">the item which is behind the tree node</param>
        /// <param name="fromCentral">specifies if the item is from the central or local database</param>
        /// <returns>The created <see cref="SearchTreeItem"/> instance.</returns>
        public static SearchTreeItem GetTreeNode(object item, bool fromCentral)
        {
            if (item is Project)
            {
                return GetProjectNode(item as Project, fromCentral);
            }
            else if (item is Part)
            {
                return GetPartNode(item as Part, fromCentral);
            }
            else if (item is RawMaterial)
            {
                return GetRawMaterialNode(item as RawMaterial, fromCentral);
            }
            else if (item is Commodity)
            {
                return GetCommodityNode(item as Commodity, fromCentral);
            }
            else if (item is Consumable)
            {
                return GetConsumableNode(item as Consumable, fromCentral);
            }
            else if (item is Assembly)
            {
                return GetAssemblyNode(item as Assembly, fromCentral);
            }
            else if (item is Machine)
            {
                return GetMachineNode(item as Machine, fromCentral);
            }
            else if (item is Die)
            {
                return GetDieNode(item as Die, fromCentral);
            }
            else if (item is PartProcessStep || item is AssemblyProcessStep)
            {
                return GetProcessStepNode(item as ProcessStep, fromCentral);
            }

            return null;
        }

        /// <summary>
        /// Returns a new instance of SearchTreeItem with in the form of a root node depending on the input parameter
        /// </summary>
        /// <param name="nodeType">type of the root node which should be created</param>
        /// <returns>The created <see cref="SearchTreeItem"/> instance.</returns>
        public static SearchTreeItem GetRootNode(SearchTreeItemType nodeType)
        {
            ImageSource icon = null;
            string label = string.Empty;

            if (nodeType == SearchTreeItemType.Projects)
            {
                icon = Images.ProjectIcon;
                label = LocalizedResources.General_Projects;
            }
            else if (nodeType == SearchTreeItemType.Parts)
            {
                icon = Images.PartsIcon;
                label = LocalizedResources.General_Parts;
            }
            else if (nodeType == SearchTreeItemType.RawMaterials)
            {
                icon = Images.RawMaterialIcon;
                label = LocalizedResources.General_RawMaterials;
            }
            else if (nodeType == SearchTreeItemType.Commodities)
            {
                icon = Images.CommodityIcon;
                label = LocalizedResources.General_Commodities;
            }
            else if (nodeType == SearchTreeItemType.Consumables)
            {
                icon = Images.ConsumableIcon;
                label = LocalizedResources.General_Consumables;
            }
            else if (nodeType == SearchTreeItemType.Machines)
            {
                icon = Images.MachineIcon;
                label = LocalizedResources.General_Machines;
            }
            else if (nodeType == SearchTreeItemType.Assemblies)
            {
                icon = Images.AssembliesIcon;
                label = LocalizedResources.General_Assemblies;
            }
            else if (nodeType == SearchTreeItemType.Die)
            {
                icon = Images.DieIcon;
                label = LocalizedResources.General_Dies;
            }
            else if (nodeType == SearchTreeItemType.ProcessStep)
            {
                icon = Images.ProcessStepIcon;
                label = LocalizedResources.General_ProcessSteps;
            }
            else if (nodeType == SearchTreeItemType.Process)
            {
                icon = Images.ProcessIcon;
                label = LocalizedResources.General_Process;
            }
            else if (nodeType == SearchTreeItemType.Subassemblies)
            {
                icon = Images.AssembliesIcon;
                label = LocalizedResources.General_Subassemblies;
            }
            else if (nodeType == SearchTreeItemType.RawParts)
            {
                icon = Images.RawPartIcon;
                label = LocalizedResources.General_RawParts;
            }

            return new SearchTreeItem(null, false, label, icon, null, null);
        }
    }
}
