﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The types of items in the search results tree.
    /// </summary>
    public enum SearchTreeItemType
    {
        /// <summary>
        /// The item represents the Projects results group.
        /// </summary>
        Projects,

        /// <summary>
        /// The item represents the Assemblies results group.
        /// </summary>
        Assemblies,

        /// <summary>
        /// The item represents the Parts results group.
        /// </summary>
        Parts,

        /// <summary>
        /// The item represents the Process results group.
        /// </summary>
        Process,

        /// <summary>
        /// The item represents the Process Steps results group.
        /// </summary>
        ProcessStep,

        /// <summary>
        /// The item represents the Machines results group.
        /// </summary>
        Machines,

        /// <summary>
        /// The item represents the Commodities results group.
        /// </summary>
        Commodities,

        /// <summary>
        /// The item represents the Dies results group.
        /// </summary>
        Die,

        /// <summary>
        /// The item represents the Consumables results group.
        /// </summary>
        Consumables,

        /// <summary>
        /// The item represents the Subassemblies results group.
        /// </summary>
        Subassemblies,

        /// <summary>
        /// The item represents the Raw Materials results group.
        /// </summary>
        RawMaterials,

        /// <summary>
        /// The item represents the Raw Parts results group.
        /// </summary>
        RawParts
    }
}
