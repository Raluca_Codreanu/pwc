﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ZPKTool.Business.Search;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Managers;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the Search Tool.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class SearchToolViewModel : ViewModel
    {
        #region Members

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The mediator service.
        /// </summary>
        private IMessenger mediator;

        /// <summary>
        /// The tree items collection
        /// </summary>
        private ObservableCollection<SearchTreeItem> treeItems;

        /// <summary>
        /// Indicates whether a search is in progress or not.
        /// </summary>
        private volatile bool isSearchInProgress;

        /// <summary>
        /// Flag indicating whether a stop search request was made.
        /// </summary>
        private bool stopSearch = false;

        /// <summary>
        /// The search text
        /// </summary>
        private string searchText;

        /// <summary>
        /// Sets the visibility of the "no results" label
        /// </summary>
        private Visibility noResultsLabelVisibility;

        /// <summary>
        /// Specifies if the results expander is expanded
        /// </summary>
        private bool resultsExpanded;

        /// <summary>
        /// Specifies if the scope popup is open
        /// </summary>
        private bool scopePopupIsOpen;

        /// <summary>
        /// Specifies if the filter popup is open
        /// </summary>
        private bool filterPopupIsOpen;

        /// <summary>
        /// The scope selection
        /// </summary>
        private SearchScopeFilter scopeSelection;

        /// <summary>
        /// The filter selection
        /// </summary>
        private SearchFilter filterSelection;

        /// <summary>
        /// The text in the scope label
        /// </summary>
        private string scopeText;

        /// <summary>
        /// The text in the filter label
        /// </summary>
        private string filterText;

        /// <summary>
        /// The reference to the selected item in the tree
        /// </summary>
        private SearchTreeItem selectedItem;

        /// <summary>
        /// A value indicating whether the search results should contain distinct results
        /// </summary>
        private bool areSearchResultsDistinct;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SearchToolViewModel"/> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        /// <param name="mediator">The mediator.</param>
        [ImportingConstructor]
        public SearchToolViewModel(IWindowService windowService, IMessenger mediator)
        {
            this.windowService = windowService;
            this.mediator = mediator;
            this.TreeItems = new ObservableCollection<SearchTreeItem>();
            this.TreeItems.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(TreeItems_CollectionChanged);

            this.SearchCommand = new DelegateCommand(this.SearchAction);
            this.StopCommand = new DelegateCommand(() => this.stopSearch = true);
            this.ClearCommand = new DelegateCommand(this.ClearResultsAction, () => !this.IsSearchInProgress);
            this.TextBoxKeyDownCommand = new DelegateCommand<KeyEventArgs>(KeyDownAction);
            this.ShowScopePopupCommand = new DelegateCommand(() => this.ScopePopupIsOpen = true);
            this.ShowFilterPopupCommand = new DelegateCommand(() => this.FilterPopupIsOpen = true);
            this.CopyCommand = new DelegateCommand<object>(CopyCommandAction);
            this.NavigateToObjectCommand = new DelegateCommand<object>(NavigateToObject);
            this.SelectedTreeItemChangedCommand = new DelegateCommand<RoutedPropertyChangedEventArgs<object>>((e) => this.selectedItem = e.NewValue as SearchTreeItem);
            this.MouseMoveCommand = new DelegateCommand<MouseEventArgs>((e) => StartDragAction(e));
            this.PasteAndSearchCommand = new DelegateCommand(() => { ApplicationCommands.Paste.Execute(null, null); SearchAction(); }, () => Clipboard.ContainsText());

            this.ResultsExpanded = false;
            this.ScopePopupIsOpen = false;
            this.ScopeText = LocalizedResources.General_None;
            this.FilterPopupIsOpen = false;
            this.FilterText = LocalizedResources.General_None;
            this.NoResultsLabelVisibility = Visibility.Visible;

            LoadScopeAndFilter();
        }

        #endregion

        #region Commands

        /// <summary>
        /// Gets the command for the search action
        /// </summary>
        public ICommand SearchCommand { get; private set; }

        /// <summary>
        /// Gets the command for the key down event
        /// </summary>
        public ICommand TextBoxKeyDownCommand { get; private set; }

        /// <summary>
        /// Gets the command for the stop action
        /// </summary>
        public ICommand StopCommand { get; private set; }

        /// <summary>
        /// Gets the command for the clear results action
        /// </summary>
        public ICommand ClearCommand { get; private set; }

        /// <summary>
        /// Gets the command for opening the scope popup
        /// </summary>
        public ICommand ShowScopePopupCommand { get; private set; }

        /// <summary>
        /// Gets the command for opening the filter popup
        /// </summary>
        public ICommand ShowFilterPopupCommand { get; private set; }

        /// <summary>
        /// Gets the command that copies an item to the clipboard
        /// </summary>
        public ICommand CopyCommand { get; private set; }

        /// <summary>
        /// Gets the command that navigates in the main view to a specified object.
        /// </summary>        
        public ICommand NavigateToObjectCommand { get; private set; }

        /// <summary>
        /// Gets the command that is triggered when the selected is changed in the tree
        /// </summary>
        public ICommand SelectedTreeItemChangedCommand { get; private set; }

        /// <summary>
        /// Gets the command that is triggered when the mouse is moved 
        /// </summary>
        public ICommand MouseMoveCommand { get; private set; }

        /// <summary>
        /// Gets the command for the paste and search operation
        /// </summary>
        public ICommand PasteAndSearchCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets or sets tree items collection
        /// </summary>
        public ObservableCollection<SearchTreeItem> TreeItems
        {
            get
            {
                return this.treeItems;
            }

            set
            {
                if (this.treeItems != value)
                {
                    this.treeItems = value;
                    OnPropertyChanged("TreeItems");
                }
            }
        }

        /// <summary>
        /// Gets or sets the search string
        /// </summary>
        public string SearchText
        {
            get
            {
                return this.searchText;
            }

            set
            {
                if (this.searchText != value)
                {
                    this.searchText = value;
                    OnPropertyChanged("SearchText");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the search is in progress.
        /// </summary>
        public bool IsSearchInProgress
        {
            get
            {
                return this.isSearchInProgress;
            }

            set
            {
                if (this.isSearchInProgress != value)
                {
                    this.isSearchInProgress = value;
                    OnPropertyChanged(() => this.IsSearchInProgress);
                }
            }
        }

        /// <summary>
        /// Gets or sets the visiblity property for the no results label
        /// </summary>
        public Visibility NoResultsLabelVisibility
        {
            get
            {
                return this.noResultsLabelVisibility;
            }

            set
            {
                if (this.noResultsLabelVisibility != value)
                {
                    this.noResultsLabelVisibility = value;
                    OnPropertyChanged("NoResultsLabelVisibility");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the results sections is expanded.
        /// </summary>
        public bool ResultsExpanded
        {
            get
            {
                return this.resultsExpanded;
            }

            set
            {
                if (this.resultsExpanded != value)
                {
                    this.resultsExpanded = value;
                    OnPropertyChanged("ResultsExpanded");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the scope popup is open.
        /// </summary>
        public bool ScopePopupIsOpen
        {
            get
            {
                return this.scopePopupIsOpen;
            }

            set
            {
                if (this.scopePopupIsOpen != value)
                {
                    this.scopePopupIsOpen = value;
                    OnPropertyChanged("ScopePopupIsOpen");

                    // when the popup is closed, save the settings
                    if (value == false)
                    {
                        SaveSearchSettings();
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the filter popup is open.
        /// </summary>
        public bool FilterPopupIsOpen
        {
            get
            {
                return this.filterPopupIsOpen;
            }

            set
            {
                if (this.filterPopupIsOpen != value)
                {
                    this.filterPopupIsOpen = value;
                    OnPropertyChanged("FilterPopupIsOpen");

                    // when the popup is closed, save the settings
                    if (value == false)
                    {
                        SaveSearchSettings();
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the scope selection property
        /// </summary>
        public SearchScopeFilter ScopeSelection
        {
            get
            {
                return this.scopeSelection;
            }

            set
            {
                if (this.scopeSelection != value)
                {
                    this.scopeSelection = value;
                    OnPropertyChanged("ScopeSelection");
                    RefreshScopeText();
                }
            }
        }

        /// <summary>
        /// Gets or sets the filter selection property
        /// </summary>
        public SearchFilter FilterSelection
        {
            get
            {
                return this.filterSelection;
            }

            set
            {
                if (this.filterSelection != value)
                {
                    this.filterSelection = value;
                    OnPropertyChanged("FilterSelection");
                    RefreshFilterText();
                }
            }
        }

        /// <summary>
        /// Gets or sets the scope text property
        /// </summary>
        public string ScopeText
        {
            get
            {
                return this.scopeText;
            }

            set
            {
                if (this.scopeText != value)
                {
                    this.scopeText = value;
                    OnPropertyChanged("ScopeText");
                }
            }
        }

        /// <summary>
        /// Gets or sets the filter text property
        /// </summary>
        public string FilterText
        {
            get
            {
                return this.filterText;
            }

            set
            {
                if (this.filterText != value)
                {
                    this.filterText = value;
                    OnPropertyChanged("FilterText");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the search results should contain distinct results
        /// The result entities are distinct if they have distinct names
        /// </summary>
        public bool AreSearchResultsDistinct
        {
            get { return this.areSearchResultsDistinct; }
            set { this.SetProperty(ref this.areSearchResultsDistinct, value, () => this.AreSearchResultsDistinct); }
        }

        #endregion Properties

        #region Events

        /// <summary>
        /// Handles the CollectionChanged event of the TreeItems control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Collections.Specialized.NotifyCollectionChangedEventArgs"/> instance containing the event data.</param>
        private void TreeItems_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (this.TreeItems != null)
            {
                this.NoResultsLabelVisibility = this.TreeItems.Count > 0 ? Visibility.Collapsed : Visibility.Visible;
            }
        }

        #endregion

        #region Actions

        /// <summary>
        /// Action that starts the search . It is performed when the search command is triggered.
        /// </summary>
        private void SearchAction()
        {
            if (this.IsSearchInProgress)
            {
                return;
            }

            string searchText = this.SearchText;
            if (string.IsNullOrWhiteSpace(searchText))
            {
                this.windowService.MessageDialogService.Show(LocalizedResources.Info_EnterTextToSearch, MessageDialogType.Info);
                return;
            }

            this.IsSearchInProgress = true;
            this.stopSearch = false;

            this.TreeItems.Clear();
            this.NoResultsLabelVisibility = Visibility.Collapsed;
            this.ResultsExpanded = true;

            System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                SearchFilter searchFilter = this.FilterSelection;
                var searchParameter = new SimpleSearchParameters() { SearchText = searchText, ScopeFilters = this.ScopeSelection, GetDistinctResults = AreSearchResultsDistinct };

                // Perform the search for whatever entities were selected on the UI
                if ((searchFilter & SearchFilter.Projects) == SearchFilter.Projects && !this.stopSearch)
                {
                    SearchForProjects(searchParameter);
                }

                if ((searchFilter & SearchFilter.Assemblies) == SearchFilter.Assemblies && !this.stopSearch)
                {
                    SearchForAssemblies(searchParameter);
                }

                if ((searchFilter & SearchFilter.Parts) == SearchFilter.Parts && !this.stopSearch)
                {
                    SearchForParts(searchParameter);
                }

                if ((searchFilter & SearchFilter.ProcessSteps) == SearchFilter.ProcessSteps && !this.stopSearch)
                {
                    SearchForProcessSteps(searchParameter);
                }

                if ((searchFilter & SearchFilter.Machines) == SearchFilter.Machines && !this.stopSearch)
                {
                    SearchForMachines(searchParameter);
                }

                if ((searchFilter & SearchFilter.RawMaterials) == SearchFilter.RawMaterials && !this.stopSearch)
                {
                    SearchForRawMaterials(searchParameter);
                }

                if ((searchFilter & SearchFilter.Commodities) == SearchFilter.Commodities && !this.stopSearch)
                {
                    SearchForCommodities(searchParameter);
                }

                if ((searchFilter & SearchFilter.Consumables) == SearchFilter.Consumables && !this.stopSearch)
                {
                    SearchForConsumables(searchParameter);
                }

                if ((searchFilter & SearchFilter.Dies) == SearchFilter.Dies && !this.stopSearch)
                {
                    SearchForDies(searchParameter);
                }

                if ((searchFilter & SearchFilter.RawParts) == SearchFilter.RawParts && !this.stopSearch)
                {
                    SearchForRawParts(searchParameter);
                }
            })
            .ContinueWith(task =>
            {
                this.IsSearchInProgress = false;
                this.stopSearch = false;
                this.NoResultsLabelVisibility = this.TreeItems.Count > 0 ? Visibility.Collapsed : Visibility.Visible;

                if (task.Exception != null)
                {
                    log.ErrorException("Caught an exception during search.", task.Exception.InnerException);
                    this.windowService.MessageDialogService.Show(task.Exception.InnerException);
                }
            });
        }

        /// <summary>
        /// Action performed when the user presses a key in the textbox
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.Input.KeyEventArgs"/> instance containing the event data.</param>
        private void KeyDownAction(System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Return && !this.IsSearchInProgress)
            {
                SearchAction();
            }
        }

        /// <summary>
        /// Action performed when clear results command is triggered
        /// </summary>
        private void ClearResultsAction()
        {
            var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_EmptySearchResults, MessageDialogType.YesNo);
            if (result == MessageDialogResult.Yes)
            {
                // Clear the search results
                this.NoResultsLabelVisibility = Visibility.Visible;
                this.selectedItem = null;
                this.TreeItems.Clear();
            }
        }

        /// <summary>
        /// Checks if the mouse is moved over a tree view item. If not, the drag operation should not start (ex: when moving the scroll bar)
        /// </summary>
        /// <param name="child">the input parameter of the recursive function (original source of the event)</param>
        /// <returns>
        /// <c>true</c> if if the mouse is moved over a tree view item; otherwise, <c>false</c>.
        /// </returns>
        private bool IsMouseInTreeViewItem(DependencyObject child)
        {
            DependencyObject parent = ZPKTool.Gui.Utils.UIHelper.GetParentObject(child);

            if (parent == null)
            {
                return false;
            }

            if (parent is TreeViewItem)
            {
                return true;
            }

            if (parent is TreeViewEx)
            {
                return false;
            }

            return IsMouseInTreeViewItem(parent);
        }

        /// <summary>
        /// Starts the drag operation of the drag and drop feature
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.Input.MouseEventArgs"/> instance containing the event data.</param>
        private void StartDragAction(MouseEventArgs e)
        {
            if (!IsMouseInTreeViewItem(e.OriginalSource as DependencyObject))
            {
                return;
            }

            // Check for Drag action
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                if (this.selectedItem != null && !this.selectedItem.IsRootNode)
                {
                    // try to initialize a drag&drop action                    
                    DragAndDropData sourceItem = new DragAndDropData(
                        selectedItem.Entity,
                        selectedItem.IsFromCentral ? DbIdentifier.CentralDatabase : DbIdentifier.LocalDatabase);

                    // new UIElement() used because we are not interested in the dependency obj source
                    DragDrop.DoDragDrop(new UIElement(), sourceItem, DragDropEffects.All);
                }
            }
        }

        /// <summary>
        /// Copies an item to the clipboard
        /// </summary>
        /// <param name="obj">the SearchTreeItem object is passed as a command parameter</param>
        private void CopyCommandAction(object obj)
        {
            if (obj is SearchTreeItem)
            {
                SearchTreeItem selectedItem = obj as SearchTreeItem;
                if (selectedItem.Entity != null)
                {
                    ClipboardManager.Instance.Copy(selectedItem.Entity, selectedItem.IsFromCentral ? DbIdentifier.CentralDatabase : DbIdentifier.LocalDatabase);
                }
            }
        }

        /// <summary>
        /// Navigates to a specified object in the main view.
        /// </summary>
        /// <param name="obj">The object to navigate to.</param>
        private void NavigateToObject(object obj)
        {
            if (obj is SearchTreeItem)
            {
                SearchTreeItem selectedItem = obj as SearchTreeItem;
                if (selectedItem.Entity != null)
                {
                    DbIdentifier databaseId = selectedItem.IsFromCentral ? DbIdentifier.CentralDatabase : DbIdentifier.LocalDatabase;
                    NavigateToEntityMessage msg = new NavigateToEntityMessage(selectedItem.Entity, databaseId);
                    this.mediator.Send(msg, IsInViewerMode ? GlobalMessengerTokens.ModelBrowserTargetToken : GlobalMessengerTokens.MainViewTargetToken);
                }
            }
        }

        #endregion

        #region Search Related

        /// <summary>
        /// Searches for projects.
        /// </summary>
        /// <param name="searchParameters">The parameters used in search algorithm</param>
        private void SearchForProjects(SimpleSearchParameters searchParameters)
        {
            // searching for Projects
            var result = SearchManager.Instance.SearchEntities<Project>(searchParameters, ref this.stopSearch);

            if (result.LocalData.Count > 0 || result.CentralData.Count > 0)
            {
                List<INameable> convertedList1 = result.LocalData.OrderBy(obj => obj.Name).Cast<INameable>().ToList();
                List<INameable> convertedList2 = result.CentralData.OrderBy(obj => obj.Name).Cast<INameable>().ToList();

                if (searchParameters.GetDistinctResults)
                {
                    convertedList2 = this.EliminateDuplicatesFromCentralResults(convertedList1, convertedList2);
                }

                DisplaySearchResults(SearchTreeItemType.Projects, convertedList1, convertedList2);
            }
        }

        /// <summary>
        /// Searches for consumables.
        /// </summary>
        /// <param name="searchParameters">The parameters used in search algorithm</param>
        private void SearchForConsumables(SimpleSearchParameters searchParameters)
        {
            var result = SearchManager.Instance.SearchEntities<Consumable>(searchParameters, ref this.stopSearch);

            if (result.LocalData.Count > 0 || result.CentralData.Count > 0)
            {
                List<INameable> convertedList1 = result.LocalData.OrderBy(obj => obj.Name).Cast<INameable>().ToList();
                List<INameable> convertedList2 = result.CentralData.OrderBy(obj => obj.Name).Cast<INameable>().ToList();

                if (searchParameters.GetDistinctResults)
                {
                    convertedList2 = this.EliminateDuplicatesFromCentralResults(convertedList1, convertedList2);
                }

                DisplaySearchResults(SearchTreeItemType.Consumables, convertedList1, convertedList2);
            }
        }

        /// <summary>
        /// Searches for dies.
        /// </summary>
        /// <param name="searchParameters">The parameters used in search algorithm</param>
        private void SearchForDies(SimpleSearchParameters searchParameters)
        {
            var result = SearchManager.Instance.SearchEntities<Die>(searchParameters, ref this.stopSearch);

            if (result.LocalData.Count > 0 || result.CentralData.Count > 0)
            {
                List<INameable> convertedList1 = result.LocalData.OrderBy(obj => obj.Name).Cast<INameable>().ToList();
                List<INameable> convertedList2 = result.CentralData.OrderBy(obj => obj.Name).Cast<INameable>().ToList();

                if (searchParameters.GetDistinctResults)
                {
                    convertedList2 = this.EliminateDuplicatesFromCentralResults(convertedList1, convertedList2);
                }

                DisplaySearchResults(SearchTreeItemType.Die, convertedList1, convertedList2);
            }
        }

        /// <summary>
        /// Searches for commodities.
        /// </summary>
        /// <param name="searchParameters">The parameters used in search algorithm</param>
        private void SearchForCommodities(SimpleSearchParameters searchParameters)
        {
            var result = SearchManager.Instance.SearchEntities<Commodity>(searchParameters, ref this.stopSearch);

            if (result.LocalData.Count > 0 || result.CentralData.Count > 0)
            {
                var convertedList1 = result.LocalData.OrderBy(obj => obj.Name).Cast<INameable>().ToList();
                var convertedList2 = result.CentralData.OrderBy(obj => obj.Name).Cast<INameable>().ToList();

                if (searchParameters.GetDistinctResults)
                {
                    convertedList2 = this.EliminateDuplicatesFromCentralResults(convertedList1, convertedList2);
                }

                DisplaySearchResults(SearchTreeItemType.Commodities, convertedList1, convertedList2);
            }
        }

        /// <summary>
        /// Searches for raw materials.
        /// </summary>
        /// <param name="searchParameters">The parameters used in search algorithm</param>
        private void SearchForRawMaterials(SimpleSearchParameters searchParameters)
        {
            var result = SearchManager.Instance.SearchEntities<RawMaterial>(searchParameters, ref this.stopSearch);

            if (result.LocalData.Count > 0 || result.CentralData.Count > 0)
            {
                List<INameable> convertedList1 = new List<INameable>();
                List<INameable> convertedList2 = new List<INameable>();

                var nameTypeValue = (RawMaterialLocalisedName)UserSettingsManager.Instance.SearchRawMaterialLocalisedName;
                switch (nameTypeValue)
                {
                    case RawMaterialLocalisedName.GermanName:
                        {
                            convertedList1 = result.LocalData.OrderBy(obj => obj.Name).Cast<INameable>().ToList();
                            if (searchParameters.GetDistinctResults)
                            {
                                var centralDistinctResult = result.CentralData.Where(p => result.LocalData.FirstOrDefault(r => r.Name == p.Name) == null);
                                convertedList2 = centralDistinctResult.OrderBy(obj => obj.Name).Cast<INameable>().ToList();
                            }
                            else
                            {
                                convertedList2 = result.CentralData.OrderBy(obj => obj.Name).Cast<INameable>().ToList();
                            }

                            break;
                        }

                    case RawMaterialLocalisedName.UKName:
                        {
                            convertedList1 = result.LocalData.OrderBy(obj => obj.NameUK).Cast<INameable>().ToList();
                            if (searchParameters.GetDistinctResults)
                            {
                                var centralDistinctResult = result.CentralData.Where(p => result.LocalData.FirstOrDefault(r => r.NameUK == p.NameUK) == null);
                                convertedList2 = centralDistinctResult.OrderBy(obj => obj.NameUK).Cast<INameable>().ToList();
                            }
                            else
                            {
                                convertedList2 = result.CentralData.OrderBy(obj => obj.NameUK).Cast<INameable>().ToList();
                            }

                            break;
                        }

                    case RawMaterialLocalisedName.USName:
                        {
                            convertedList1 = result.LocalData.OrderBy(obj => obj.NameUS).Cast<INameable>().ToList();
                            if (searchParameters.GetDistinctResults)
                            {
                                var centralDistinctResult = result.CentralData.Where(p => result.LocalData.FirstOrDefault(r => r.NameUS == p.NameUS) == null);
                                convertedList2 = centralDistinctResult.OrderBy(obj => obj.NameUS).Cast<INameable>().ToList();
                            }
                            else
                            {
                                convertedList2 = result.CentralData.OrderBy(obj => obj.NameUS).Cast<INameable>().ToList();
                            }

                            break;
                        }

                    default:
                        break;
                }

                DisplaySearchResults(SearchTreeItemType.RawMaterials, convertedList1, convertedList2);
            }
        }

        /// <summary>
        /// Searches for raw parts.
        /// </summary>
        /// <param name="searchParameters">The parameters used in search algorithm</param>
        private void SearchForRawParts(SimpleSearchParameters searchParameters)
        {
            var result = SearchManager.Instance.SearchEntities<RawPart>(searchParameters, ref this.stopSearch);

            if (result.LocalData.Count > 0 || result.CentralData.Count > 0)
            {
                var convertedList1 = result.LocalData.OrderBy(obj => obj.Name).Cast<INameable>().ToList();
                var convertedList2 = result.CentralData.OrderBy(obj => obj.Name).Cast<INameable>().ToList();

                if (searchParameters.GetDistinctResults)
                {
                    convertedList2 = this.EliminateDuplicatesFromCentralResults(convertedList1, convertedList2);
                }

                DisplaySearchResults(SearchTreeItemType.RawParts, convertedList1, convertedList2);
            }
        }

        /// <summary>
        /// Searches for machines.
        /// </summary>
        /// <param name="searchParameters">The parameters used in search algorithm</param>
        private void SearchForMachines(SimpleSearchParameters searchParameters)
        {
            var result = SearchManager.Instance.SearchEntities<Machine>(searchParameters, ref this.stopSearch);

            if (result.LocalData.Count > 0 || result.CentralData.Count > 0)
            {
                List<INameable> convertedList1 = result.LocalData.OrderBy(obj => obj.Name).Cast<INameable>().ToList();
                List<INameable> convertedList2 = result.CentralData.OrderBy(obj => obj.Name).Cast<INameable>().ToList();

                if (searchParameters.GetDistinctResults)
                {
                    convertedList2 = this.EliminateDuplicatesFromCentralResults(convertedList1, convertedList2);
                }

                DisplaySearchResults(SearchTreeItemType.Machines, convertedList1, convertedList2);
            }
        }

        /// <summary>
        /// Searches for assemblies.
        /// </summary>
        /// <param name="searchParameters">The parameters used in search algorithm</param>
        private void SearchForAssemblies(SimpleSearchParameters searchParameters)
        {
            var result = SearchManager.Instance.SearchEntities<Assembly>(searchParameters, ref this.stopSearch);

            if (result.LocalData.Count > 0 || result.CentralData.Count > 0)
            {
                List<INameable> convertedList1 = result.LocalData.OrderBy(obj => obj.Name).Cast<INameable>().ToList();
                List<INameable> convertedList2 = result.CentralData.OrderBy(obj => obj.Name).Cast<INameable>().ToList();

                if (searchParameters.GetDistinctResults)
                {
                    convertedList2 = this.EliminateDuplicatesFromCentralResults(convertedList1, convertedList2);
                }

                DisplaySearchResults(SearchTreeItemType.Assemblies, convertedList1, convertedList2);
            }
        }

        /// <summary>
        /// Searches for parts.
        /// </summary>
        /// <param name="searchParameters">The parameters used in search algorithm</param>
        private void SearchForParts(SimpleSearchParameters searchParameters)
        {
            var result = SearchManager.Instance.SearchEntities<Part>(searchParameters, ref this.stopSearch);

            if (result.LocalData.Count > 0 || result.CentralData.Count > 0)
            {
                List<INameable> convertedList1 = result.LocalData.OrderBy(obj => obj.Name).Cast<INameable>().ToList();
                List<INameable> convertedList2 = result.CentralData.OrderBy(obj => obj.Name).Cast<INameable>().ToList();

                if (searchParameters.GetDistinctResults)
                {
                    convertedList2 = this.EliminateDuplicatesFromCentralResults(convertedList1, convertedList2);
                }

                DisplaySearchResults(SearchTreeItemType.Parts, convertedList1, convertedList2);
            }
        }

        /// <summary>
        /// Searches for process steps.
        /// </summary>
        /// <param name="searchParameters">The parameters used in search algorithm</param>
        private void SearchForProcessSteps(SimpleSearchParameters searchParameters)
        {
            var result = SearchManager.Instance.SearchEntities<ProcessStep>(searchParameters, ref this.stopSearch);

            if (result.LocalData.Count > 0 || result.CentralData.Count > 0)
            {
                List<INameable> convertedList1 = result.LocalData.OrderBy(obj => obj.Name).Cast<INameable>().ToList();
                List<INameable> convertedList2 = result.CentralData.OrderBy(obj => obj.Name).Cast<INameable>().ToList();

                if (searchParameters.GetDistinctResults)
                {
                    convertedList2 = this.EliminateDuplicatesFromCentralResults(convertedList1, convertedList2);
                }

                DisplaySearchResults(SearchTreeItemType.ProcessStep, convertedList1, convertedList2);
            }
        }

        /// <summary>
        /// Displays the results of the search for a particular class of objects
        /// </summary>
        /// <param name="nodeType">The parent node type which should be added in the root of the tree</param>
        /// <param name="localResults">The list of items retrieved from the local database</param>
        /// <param name="centralResults">The list of items retrieved from the central database</param>
        private void DisplaySearchResults(SearchTreeItemType nodeType, List<INameable> localResults, List<INameable> centralResults)
        {
            if ((localResults == null || localResults.Count <= 0) && (centralResults == null || centralResults.Count <= 0))
            {
                return;
            }

            SearchTreeItem groupNode = SearchTreeItemFactory.GetRootNode(nodeType);
            groupNode.Children.Clear();
            DisplaySubItems(groupNode, localResults, false);
            DisplaySubItems(groupNode, centralResults, true);
            int itemsfound = localResults.Count + centralResults.Count;
            groupNode.Label += " (" + itemsfound + ")";
            this.Dispatcher.BeginInvoke(() => this.TreeItems.Add(groupNode));
        }

        /// <summary>
        /// Displays the one of the two lists retrieved from the databases
        /// </summary>
        /// <param name="groupNode">The parent node</param>
        /// <param name="results">The list of items which should be added</param>
        /// <param name="fromCentral">A flag specifying if the results are from central or local db</param>
        private void DisplaySubItems(SearchTreeItem groupNode, List<INameable> results, bool fromCentral)
        {
            if (results == null || results.Count <= 0)
            {
                return;
            }

            if (this.stopSearch)
            {
                return;
            }

            if (groupNode == null)
            {
                return;
            }

            // For each entity in the search results add a corresponding node in the grouping node.
            foreach (var item in results)
            {
                if (this.stopSearch)
                {
                    return;
                }

                SearchTreeItem treeItem = SearchTreeItemFactory.GetTreeNode(item, fromCentral);
                groupNode.Children.Add(treeItem);
            }
        }

        /// <summary>
        /// Eliminates the search results from central database that have the same name than the search results from the local database
        /// </summary>
        /// <typeparam name="T">The type of the entities from the search result</typeparam>
        /// <param name="local">The results from the local database</param>
        /// <param name="central">The results from the central database</param>
        /// <returns>The search results from central database after filtering</returns>
        private List<T> EliminateDuplicatesFromCentralResults<T>(IEnumerable<T> local, IEnumerable<T> central) where T : INameable
        {
            return central.Where(c => local.FirstOrDefault(l => l.Name == c.Name) == null).ToList();
        }

        #endregion

        #region Other Methods

        /// <summary>
        /// Applies the search settings from .config file.
        /// </summary>
        private void LoadScopeAndFilter()
        {
            string filtersLocationSetting = UserSettingsManager.Instance.SearchFiltersLocation;
            SearchScopeFilter searchScope = SearchScopeFilter.None;
            SearchFilter searchFilter = SearchFilter.None;

            // if the filterLocationSetting is empty set the as default all filters and scopes.
            if (string.IsNullOrWhiteSpace(filtersLocationSetting))
            {
                // Default - all search filters.
                searchFilter |= SearchFilter.Projects;
                searchFilter |= SearchFilter.Assemblies;
                searchFilter |= SearchFilter.Parts;
                searchFilter |= SearchFilter.ProcessSteps;
                searchFilter |= SearchFilter.Machines;
                searchFilter |= SearchFilter.RawMaterials;
                searchFilter |= SearchFilter.Commodities;
                searchFilter |= SearchFilter.Consumables;
                searchFilter |= SearchFilter.Dies;
                searchFilter |= SearchFilter.RawParts;

                // Default - all search scopes.
                searchScope |= SearchScopeFilter.MasterData;
                searchScope |= SearchScopeFilter.MyProjects;
                searchScope |= SearchScopeFilter.OtherUsersProjects;
                searchScope |= SearchScopeFilter.ReleasedProjects;
            }
            else
            {
                string[] searchSettings = !string.IsNullOrWhiteSpace(filtersLocationSetting) ? filtersLocationSetting.Split('/') : null;
                string[] searchFilters = searchSettings != null ? searchSettings[0].Split(',') : null;
                string[] searchLocation = searchSettings != null && searchSettings[1] != null ? searchSettings[1].Split(',') : null;
                int i = 0;

                if (searchFilters != null)
                {
                    foreach (string filter in searchFilters)
                    {
                        if (filter == "True")
                        {
                            switch (i)
                            {
                                case 0:
                                    searchFilter |= SearchFilter.Projects;
                                    break;
                                case 1:
                                    searchFilter |= SearchFilter.Assemblies;
                                    break;
                                case 2:
                                    searchFilter |= SearchFilter.Parts;
                                    break;
                                case 3:
                                    searchFilter |= SearchFilter.ProcessSteps;
                                    break;
                                case 4:
                                    searchFilter |= SearchFilter.Machines;
                                    break;
                                case 5:
                                    searchFilter |= SearchFilter.RawMaterials;
                                    break;
                                case 6:
                                    searchFilter |= SearchFilter.Commodities;
                                    break;
                                case 7:
                                    searchFilter |= SearchFilter.Consumables;
                                    break;
                                case 8:
                                    searchFilter |= SearchFilter.Dies;
                                    break;
                                case 9:
                                    searchFilter |= SearchFilter.RawParts;
                                    break;
                                default:
                                    break;
                            }
                        }

                        i++;
                    }
                }

                if (searchLocation != null)
                {
                    i = 0;
                    foreach (string location in searchLocation)
                    {
                        if (location == "True")
                        {
                            switch (i)
                            {
                                case 0:
                                    searchScope |= SearchScopeFilter.MasterData;
                                    break;
                                case 1:
                                    searchScope |= SearchScopeFilter.MyProjects;
                                    break;
                                case 2:
                                    searchScope |= SearchScopeFilter.OtherUsersProjects;
                                    break;
                                case 3:
                                    searchScope |= SearchScopeFilter.ReleasedProjects;
                                    break;
                                default:
                                    break;
                            }
                        }

                        i++;
                    }
                }
            }

            this.FilterSelection = searchFilter;
            this.ScopeSelection = searchScope;
        }

        /// <summary>
        /// Saves the search settings(filters and location) in .config file.
        /// </summary>
        private void SaveSearchSettings()
        {
            string filtersAndScope = string.Empty;

            if ((this.FilterSelection & SearchFilter.Projects) != 0)
            {
                filtersAndScope += "True";
            }
            else
            {
                filtersAndScope += "False";
            }

            if ((this.FilterSelection & SearchFilter.Assemblies) != 0)
            {
                filtersAndScope += ",True";
            }
            else
            {
                filtersAndScope += ",False";
            }

            if ((this.FilterSelection & SearchFilter.Parts) != 0)
            {
                filtersAndScope += ",True";
            }
            else
            {
                filtersAndScope += ",False";
            }

            if ((this.FilterSelection & SearchFilter.ProcessSteps) != 0)
            {
                filtersAndScope += ",True";
            }
            else
            {
                filtersAndScope += ",False";
            }

            if ((this.FilterSelection & SearchFilter.Machines) != 0)
            {
                filtersAndScope += ",True";
            }
            else
            {
                filtersAndScope += ",False";
            }

            if ((this.FilterSelection & SearchFilter.RawMaterials) != 0)
            {
                filtersAndScope += ",True";
            }
            else
            {
                filtersAndScope += ",False";
            }

            if ((this.FilterSelection & SearchFilter.Commodities) != 0)
            {
                filtersAndScope += ",True";
            }
            else
            {
                filtersAndScope += ",False";
            }

            if ((this.FilterSelection & SearchFilter.Consumables) != 0)
            {
                filtersAndScope += ",True";
            }
            else
            {
                filtersAndScope += ",False";
            }

            if ((this.FilterSelection & SearchFilter.Dies) != 0)
            {
                filtersAndScope += ",True";
            }
            else
            {
                filtersAndScope += ",False";
            }

            if ((this.FilterSelection & SearchFilter.RawParts) != 0)
            {
                filtersAndScope += ",True";
            }
            else
            {
                filtersAndScope += ",False";
            }

            if ((this.ScopeSelection & SearchScopeFilter.MasterData) != 0)
            {
                filtersAndScope += "/True";
            }
            else
            {
                filtersAndScope += "/False";
            }

            if ((this.ScopeSelection & SearchScopeFilter.MyProjects) != 0)
            {
                filtersAndScope += ",True";
            }
            else
            {
                filtersAndScope += ",False";
            }

            if ((this.ScopeSelection & SearchScopeFilter.OtherUsersProjects) != 0)
            {
                filtersAndScope += ",True";
            }
            else
            {
                filtersAndScope += ",False";
            }

            if ((this.ScopeSelection & SearchScopeFilter.ReleasedProjects) != 0)
            {
                filtersAndScope += ",True";
            }
            else
            {
                filtersAndScope += ",False";
            }

            UserSettingsManager.Instance.SearchFiltersLocation = filtersAndScope;
            UserSettingsManager.Instance.Save();
        }

        /// <summary>
        /// Changes the text of the scope label
        /// </summary>
        private void RefreshScopeText()
        {
            this.ScopeText = string.Empty;
            if (this.ScopeSelection == SearchScopeFilter.None)
            {
                this.ScopeText = LocalizedResources.General_None;
                return;
            }

            if ((this.ScopeSelection & SearchScopeFilter.MasterData) != 0)
            {
                this.ScopeText += LocalizedResources.General_IsMasterData + ", ";
            }

            if ((this.ScopeSelection & SearchScopeFilter.MyProjects) != 0)
            {
                this.ScopeText += LocalizedResources.General_MyProjects + ", ";
            }

            if ((this.ScopeSelection & SearchScopeFilter.OtherUsersProjects) != 0)
            {
                this.ScopeText += LocalizedResources.General_OtherUsersProjects + ", ";
            }

            if ((this.ScopeSelection & SearchScopeFilter.ReleasedProjects) != 0)
            {
                this.ScopeText += LocalizedResources.General_ReleasedProjects + ", ";
            }

            if (!string.IsNullOrEmpty(this.ScopeText))
            {
                this.ScopeText = this.ScopeText.Remove(this.scopeText.Length - 2);
            }
        }

        /// <summary>
        /// Changes the text of the filter label
        /// </summary>
        private void RefreshFilterText()
        {
            this.FilterText = string.Empty;
            if (this.FilterSelection == SearchFilter.None)
            {
                this.FilterText = LocalizedResources.General_None;
                return;
            }

            if ((this.FilterSelection & SearchFilter.Projects) != 0)
            {
                this.FilterText += LocalizedResources.General_Projects + ", ";
            }

            if ((this.FilterSelection & SearchFilter.Assemblies) != 0)
            {
                this.FilterText += LocalizedResources.General_Assemblies + ", ";
            }

            if ((this.FilterSelection & SearchFilter.Parts) != 0)
            {
                this.FilterText += LocalizedResources.General_Parts + ", ";
            }

            if ((this.FilterSelection & SearchFilter.ProcessSteps) != 0)
            {
                this.FilterText += LocalizedResources.General_ProcessSteps + ", ";
            }

            if ((this.FilterSelection & SearchFilter.Machines) != 0)
            {
                this.FilterText += LocalizedResources.General_Machines + ", ";
            }

            if ((this.FilterSelection & SearchFilter.RawMaterials) != 0)
            {
                this.FilterText += LocalizedResources.General_RawMaterials + ", ";
            }

            if ((this.FilterSelection & SearchFilter.Commodities) != 0)
            {
                this.FilterText += LocalizedResources.General_Commodities + ", ";
            }

            if ((this.FilterSelection & SearchFilter.Consumables) != 0)
            {
                this.FilterText += LocalizedResources.General_Consumables + ", ";
            }

            if ((this.FilterSelection & SearchFilter.Dies) != 0)
            {
                this.FilterText += LocalizedResources.General_Dies + ", ";
            }

            if ((this.FilterSelection & SearchFilter.RawParts) != 0)
            {
                this.FilterText += LocalizedResources.General_RawParts + ", ";
            }

            if (!string.IsNullOrEmpty(this.FilterText))
            {
                this.FilterText = this.FilterText.Remove(this.FilterText.Length - 2);
            }
        }

        #endregion
    }
}