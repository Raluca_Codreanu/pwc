﻿namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Represents the value control types.
    /// </summary>
    public enum ValueControlTypes
    {
        /// <summary>
        /// This item represents no control.
        /// </summary>
        None = 0,

        /// <summary>
        /// This item represents a text extended text box.
        /// </summary>
        TextExtendedTextBox = 1,

        /// <summary>
        /// This item represents a number extended text box.
        /// </summary>
        NumberExtendedTextBox = 2,

        /// <summary>
        /// This item represents a range of extended text boxes.
        /// </summary>
        RangeExtendedTextBox = 3,

        /// <summary>
        /// This item represents a checkbox
        /// </summary>
        CheckBox = 4,

        /// <summary>
        /// This item represents an year picker.
        /// </summary>
        YearPicker = 5,

        /// <summary>
        /// This item represents a date picker.
        /// </summary>
        DatePicker = 6,

        /// <summary>
        /// This item represents a users combo box.
        /// </summary>
        UsersComboBox = 7,

        /// <summary>
        /// This item represesnts a project status combo box.
        /// </summary>
        ProjectStatusComboBox = 8,

        /// <summary>
        /// This item represents a supplier type combo box.
        /// </summary>
        SupplierTypeComboBox = 9,

        /// <summary>
        /// This item represents a calculation accuracy combo box.
        /// </summary>
        CalculationAccuracyComboBox = 10,

        /// <summary>
        /// This item represents a calculation approach combo box.
        /// </summary>
        CalculationApproachComboBox = 11,

        /// <summary>
        /// This item represents a calculation variant combo box.
        /// </summary>
        CalculationVariantComboBox = 12,

        /// <summary>
        /// This item represents a calculation status combo box.
        /// </summary>
        CalculationStatusComboBox = 13,

        /// <summary>
        /// This item represents an assembly/part delivery type combo box.
        /// </summary>
        AssemblyPartDeliveryTypeComboBox = 14,

        /// <summary>
        /// This item represents a raw material delivery type combo box.
        /// </summary>
        RawMaterialDeliveryTypeComboBox = 15,

        /// <summary>
        /// This item represents a process accuracy combo box.
        /// </summary>
        ProcessAccuracyComboBox = 16,

        /// <summary>
        /// This item represents a consumable amount combo box.
        /// </summary>
        ConsumableAmountComboBox = 17,

        /// <summary>
        /// This item represents a die type.
        /// </summary>
        DieType = 18,

        /// <summary>
        /// This item represents a country.
        /// </summary>
        Country = 19,

        /// <summary>
        /// This item represents a supplier.
        /// </summary>
        Supplier = 20,

        /// <summary>
        /// This item represents a scrap calculation type.
        /// </summary>
        ScrapCalculationType = 21,

        /// <summary>
        /// This item represents the classification selector.
        /// </summary>
        ClassificationSelector = 22
    }
}
