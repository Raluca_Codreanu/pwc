﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Runtime.Serialization;
using System.Windows.Input;
using System.Xml.Serialization;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Controls;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The configuration item that defines a search parameter.
    /// </summary>
    [Serializable]
    public class AdvancedSearchConfigurationItem : ObservableObject, ISerializable
    {
        #region Fields

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The property name.
        /// </summary>
        private string propertyName;

        /// <summary>
        /// The value control type.
        /// </summary>
        private int valueControlType;

        /// <summary>
        /// The value input option.
        /// </summary>
        private int valueInputOption;

        /// <summary>
        /// The unit type.
        /// </summary>
        private UnitType unitType;

        /// <summary>
        /// The first value.
        /// </summary>
        private object firstValue;

        /// <summary>
        /// The second value.
        /// </summary>
        private object secondValue;

        /// <summary>
        /// The country browser view model.
        /// </summary>
        private CountryAndSupplierBrowserViewModel countryBrowser;

        /// <summary>
        /// The consumable price unit.
        /// </summary>
        private MeasurementUnit consumablePriceUnit;

        /// <summary>
        /// The consumable amount measurement unit.
        /// </summary>
        private MeasurementUnit consumableAmountUnit;

        /// <summary>
        /// The consumable amount units.
        /// </summary>
        private ObservableCollection<MeasurementUnit> consumableAmountUnits;

        /// <summary>
        /// The classification selector view model.
        /// </summary>
        private ClassificationSelectorViewModel classificationSelectorVM;

        /// <summary>
        /// The entity type.
        /// </summary>
        private Type entityType;

        /// <summary>
        /// The data source manager.
        /// </summary>
        private IDataSourceManager dataSourceManager;

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AdvancedSearchConfigurationItem" /> class.
        /// Used for serialization default constructor.
        /// </summary>
        /// <param name="windowService">The window service.</param>             
        public AdvancedSearchConfigurationItem(IWindowService windowService)
        {
            this.SearchedProperties = new Collection<string>();
            this.windowService = windowService;
            this.BrowseMasterDataCommand = new DelegateCommand<Type>(this.BrowseMasterData);

            this.dataSourceManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
        }

        /// Deserialization constructor.
        /// <summary>
        /// Initializes a new instance of the <see cref="AdvancedSearchConfigurationItem"/> class.
        /// </summary>
        /// <param name="info">The Serialization Info.</param>
        /// <param name="context">The Streaming Context.</param>
        public AdvancedSearchConfigurationItem(SerializationInfo info, StreamingContext context)
        {
            // Get the values from info and assign them to the appropriate properties.
            this.PropertyName = (string)info.GetValue("PropertyName", typeof(string));
            this.ValueControlType = (int)info.GetValue("ValueControlType", typeof(int));
            this.ValueInputOption = (int)info.GetValue("ValueInputOption", typeof(int));
        }

        #endregion Constructors

        #region Commands

        /// <summary>
        /// Gets the command that opens the Master Data Browser. The expected parameter is the Type of master data to browse.
        /// </summary>
        [XmlIgnore]
        public ICommand BrowseMasterDataCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets or sets the name of the property.
        /// </summary>
        [XmlElement("PropertyName")]
        public string PropertyName
        {
            get
            {
                return this.propertyName;
            }

            set
            {
                if (this.propertyName != value)
                {
                    this.propertyName = value;
                    this.OnPropertyChanged(() => this.PropertyName);

                    this.FirstValue = null;
                    this.SecondValue = null;
                    this.ValueInputOption = (int)ValueInputOptions.SingleValue;

                    this.countryBrowser = null;

                    this.consumablePriceUnit = null;
                    this.consumableAmountUnits = null;
                    this.consumableAmountUnit = null;

                    this.SearchedProperties.Clear();
                    this.SearchedProperties.Add(value);
                }
            }
        }

        /// <summary>
        /// Gets or sets the type of the value control.
        /// </summary>
        [XmlElement("ValueControlType")]
        public int ValueControlType
        {
            get
            {
                return this.valueControlType;
            }

            set
            {
                if (this.valueControlType != value)
                {
                    this.valueControlType = value;
                    this.OnPropertyChanged(() => this.ValueControlType);

                    this.InitializeClassification();
                }
            }
        }

        /// <summary>
        /// Gets or sets the value input option.
        /// </summary>
        [XmlElement("ValueInputOption")]
        public int ValueInputOption
        {
            get
            {
                return this.valueInputOption;
            }

            set
            {
                if (this.valueInputOption != value)
                {
                    this.valueInputOption = value;
                    this.OnPropertyChanged(() => this.ValueInputOption);
                    this.SecondValue = null;
                }
            }
        }

        /// <summary>
        /// Gets or sets the searched properties.
        /// Contains the property name and additional properties that need to be searched.
        /// </summary>
        [XmlIgnore]
        public Collection<string> SearchedProperties { get; set; }

        /// <summary>
        /// Gets or sets the type of the unit.
        /// </summary>
        [XmlIgnore]
        public UnitType UnitType
        {
            get { return this.unitType; }
            set { this.SetProperty(ref this.unitType, value, () => this.UnitType); }
        }

        /// <summary>
        /// Gets or sets the first value.
        /// </summary>
        [XmlIgnore]
        public object FirstValue
        {
            get
            {
                return this.firstValue;
            }

            set
            {
                if (this.firstValue != value)
                {
                    if (value != null
                        && value.GetType() == typeof(string)
                        && string.IsNullOrWhiteSpace(value.ToString()))
                    {
                        value = null;
                    }

                    this.firstValue = value;
                    this.OnPropertyChanged(() => this.FirstValue);
                }
            }
        }

        /// <summary>
        /// Gets or sets the second value.
        /// </summary>
        [XmlIgnore]
        public object SecondValue
        {
            get { return this.secondValue; }
            set { this.SetProperty(ref this.secondValue, value, () => this.SecondValue); }
        }

        /// <summary>
        /// Gets or sets the consumable price measurement unit.
        /// </summary>
        [XmlIgnore]
        public MeasurementUnit ConsumablePriceUnit
        {
            get
            {
                return this.consumablePriceUnit;
            }

            set
            {
                if (this.consumablePriceUnit != value)
                {
                    this.consumablePriceUnit = value;
                    this.OnPropertyChanged(() => this.PropertyName);

                    if (this.ConsumablePriceUnit != null)
                    {
                        this.InitializeAmountUnits((MeasurementUnitScale)this.ConsumablePriceUnit.ScaleID);
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the collection of units which populates the textbox units.
        /// </summary>
        [XmlIgnore]
        public ObservableCollection<MeasurementUnit> ConsumableAmountUnits
        {
            get { return this.consumableAmountUnits; }
            set { this.SetProperty(ref this.consumableAmountUnits, value, () => this.ConsumableAmountUnits); }
        }

        /// <summary>
        /// Gets or sets the consumable amount measurement unit.
        /// </summary>
        [XmlIgnore]
        public MeasurementUnit ConsumableAmountUnit
        {
            get { return this.consumableAmountUnit; }
            set { this.SetProperty(ref this.consumableAmountUnit, value, () => this.ConsumableAmountUnit); }
        }

        /// <summary>
        /// Gets or sets the classification selector view model.
        /// </summary>
        [XmlIgnore]
        public ClassificationSelectorViewModel ClassificationSelectorVM
        {
            get { return this.classificationSelectorVM; }
            set { this.SetProperty(ref this.classificationSelectorVM, value, () => this.ClassificationSelectorVM); }
        }

        /// <summary>
        /// Gets or sets the property entity type.
        /// </summary>
        [XmlIgnore]
        public Type EntityType
        {
            get
            {
                return this.entityType;
            }

            set
            {
                if (this.entityType != value)
                {
                    this.entityType = value;

                    this.InitializeClassification();
                }
            }
        }

        #endregion Properties

        #region Initialization

        /// <summary>
        /// Initializes the measurement units.
        /// </summary>
        /// <param name="measurementUnitScale">The measurement unit scale.</param>
        private void InitializeAmountUnits(MeasurementUnitScale measurementUnitScale)
        {
            try
            {
                var measurementUnits = this.dataSourceManager.MeasurementUnitRepository.GetAll(measurementUnitScale);
                this.ConsumableAmountUnits = new ObservableCollection<MeasurementUnit>(measurementUnits);
            }
            catch (BusinessException ex)
            {
                log.ErrorException("Caught a business exception.", ex);
                this.windowService.MessageDialogService.Show(ex);
            }
        }

        /// <summary>
        /// Initializes the classifications. Used to initialize the view model with machines or materials classification.
        /// </summary>
        private void InitializeClassification()
        {
            if (this.EntityType != typeof(Machine)
                && this.EntityType != typeof(RawMaterial))
            {
                return;
            }

            if (this.ValueControlType == (short)ValueControlTypes.ClassificationSelector)
            {
                if (this.ClassificationSelectorVM == null)
                {
                    this.ClassificationSelectorVM = new ClassificationSelectorViewModel();

                    if (this.EntityType == typeof(Machine))
                    {
                        this.ClassificationSelectorVM.ClassificationType = typeof(MachinesClassification);
                    }
                    else if (this.EntityType == typeof(RawMaterial))
                    {
                        this.ClassificationSelectorVM.ClassificationType = typeof(MaterialsClassification);
                    }

                    this.ClassificationSelectorVM.DataAccessContext = this.dataSourceManager;

                    var classification = new ObservableCollection<object>();
                    this.ClassificationSelectorVM.ClassificationLevels = classification;
                    classification.CollectionChanged += this.ClassificationSourceContentUpdated;
                }
                else
                {
                    this.FirstValue = null;
                }
            }
            else
            {
                this.ClassificationSelectorVM = null;
            }
        }

        #endregion Initialization

        #region Command handling

        /// <summary>
        /// Browses the specified type of master data using the master data browser.
        /// </summary>
        /// <param name="masterDataType">Type of the master data.</param>
        public void BrowseMasterData(Type masterDataType)
        {
            if (this.countryBrowser == null)
            {
                this.countryBrowser = new CountryAndSupplierBrowserViewModel(windowService);
            }

            if (masterDataType == typeof(Country))
            {
                this.countryBrowser.CountryName = null;
                this.countryBrowser.LoadLocation = DbIdentifier.LocalDatabase;

                this.countryBrowser.CountryOrSupplierSelected += this.OnCountryOrSupplierSelected;
                this.windowService.ShowViewInDialog(this.countryBrowser);
                this.countryBrowser.CountryOrSupplierSelected -= this.OnCountryOrSupplierSelected;
            }
            else if (masterDataType == typeof(CountryState))
            {
                this.countryBrowser.LoadSuppliers = true;
                this.countryBrowser.LoadLocation = DbIdentifier.LocalDatabase;
                this.countryBrowser.CountryOrSupplierSelected += this.OnCountryOrSupplierSelected;
                this.windowService.ShowViewInDialog(this.countryBrowser);
                this.countryBrowser.CountryOrSupplierSelected -= this.OnCountryOrSupplierSelected;
            }
        }

        /// <summary>
        /// Called when a country or supplier is selected from the country browser.
        /// </summary>
        /// <param name="masterDataEntity">The master data entity selected in the browser.</param>
        /// <param name="databaseId">The source database of the selected master data.</param>
        private void OnCountryOrSupplierSelected(object masterDataEntity, DbIdentifier databaseId)
        {
            var country = masterDataEntity as Country;
            if (country != null)
            {
                this.FirstValue = country.Name;
            }
            else
            {
                var supplier = masterDataEntity as CountryState;
                if (supplier != null)
                {
                    this.FirstValue = supplier.Name;
                }
            }
        }

        #endregion Command handling

        #region Event handlers

        /// <summary>
        /// Handles the CollectionChanged event of the ClassificationSelector.
        /// This event is trigger whenever something is added to or removed from the source collection.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.ComponentModel.NotifyCollectionChangedEventArgs"/> instance containing the event data.</param>
        private void ClassificationSourceContentUpdated(object sender, NotifyCollectionChangedEventArgs e)
        {
            // If the property that holds the collection values of the classification selector is null it will be initialized.
            // If the source collection is cleared the collection that holds the values must be cleared.
            if (this.FirstValue == null)
            {
                this.FirstValue = new Collection<Guid>();
            }
            else if (sender is ICollection<object>
                && (sender as ICollection<object>).Count == 0)
            {
                this.FirstValue = null;
            }

            // The last value of the update is added to the collection.
            if (e.NewItems != null
                && e.NewItems.Count > 0)
            {
                var identity = e.NewItems[0] as IIdentifiable;
                if (identity != null)
                {
                    ((Collection<Guid>)this.FirstValue).Insert(0, identity.Guid);
                }
            }
        }

        #endregion Event handlers

        #region ISerializable

        /// <summary>
        /// Populates a <see cref="T:System.Runtime.Serialization.SerializationInfo"/> with the data needed to serialize the target object.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo"/> to populate with data.</param>
        /// <param name="context">The destination (see <see cref="T:System.Runtime.Serialization.StreamingContext"/>) for this serialization.</param>
        /// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("PropertyName", this.PropertyName);
            info.AddValue("ValueControlType", this.ValueControlType);
            info.AddValue("ValueInputOption", this.ValueInputOption);
        }

        #endregion ISerializable
    }
}