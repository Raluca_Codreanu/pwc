﻿namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Type of the imported entity.
    /// </summary>
    public enum ImportedEntityType
    {
        /// <summary>
        /// The currency.
        /// </summary>
        Currency,

        /// <summary>
        /// The country.
        /// </summary>
        CountrySetting,

        /// <summary>
        /// The raw material.
        /// </summary>
        RawMaterial,

        /// <summary>
        /// The machine.
        /// </summary>
        Machine
    }
}