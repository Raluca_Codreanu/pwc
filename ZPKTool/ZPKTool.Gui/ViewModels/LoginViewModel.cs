﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Controls;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;
using ZPKTool.Synchronization;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the Login view. 
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class LoginViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The composition container.
        /// </summary>
        private readonly CompositionContainer container;

        /// <summary>
        /// The message dialog service.
        /// </summary>
        private readonly IMessageDialogService messageDialogService;

        /// <summary>
        /// The window service.
        /// </summary>
        private readonly IWindowService windowService;

        /// <summary>
        /// The dispatcher service.
        /// </summary>
        private readonly IDispatcherService dispatcher;

        /// <summary>
        /// The PleaseWaitService reference.
        /// </summary>
        private readonly IPleaseWaitService pleaseWaitService;

        /// <summary>
        /// The messenger service.
        /// </summary>
        private readonly IMessenger messenger;

        /// <summary>
        /// The synchronization manager.
        /// </summary>
        private readonly ISynchronizationManager syncManager;

        /// <summary>
        /// The visibility of the login progress UI.
        /// </summary>
        private Visibility loginProgressVisibility = Visibility.Collapsed;

        /// <summary>
        /// The username inputted by the user.
        /// </summary>
        private string username;

        /// <summary>
        /// Background worker used to asynchronously execute the log in operation.
        /// </summary>
        private BackgroundWorker loginWorker;

        /// <summary>
        /// The status icon for the "Connecting to the local database" operation.
        /// </summary>
        private UIElement connectToLocalDbStatusIcon;

        /// <summary>
        /// The status icon for the "Connecting to central server" operation
        /// </summary>
        private UIElement connectToCentralServerStatusIcon;

        /// <summary>
        /// The status icon for the "Synchronizing data" operation
        /// </summary>
        private UIElement synchStatusIcon;

        /// <summary>
        /// The status icon for the "Logging in" operation
        /// </summary>
        private UIElement loginStatusIcon;

        #endregion Attributes

        /// <summary>
        /// Initializes a new instance of the <see cref="LoginViewModel"/> class.
        /// </summary>
        /// <param name="container">The composition container.</param>
        /// <param name="messageDialogService">The message dialog service.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="messenger">The messenger service.</param>
        /// <param name="dispatcher">The dispacher service.</param>
        /// <param name="pleaseWaitService">The please wait service.</param>
        [ImportingConstructor]
        public LoginViewModel(
            CompositionContainer container,
            IMessageDialogService messageDialogService,
            IWindowService windowService,
            IMessenger messenger,
            IDispatcherService dispatcher,
            IPleaseWaitService pleaseWaitService)
        {
            this.container = container;
            this.windowService = windowService;
            this.dispatcher = dispatcher;
            this.messageDialogService = messageDialogService;
            this.messenger = messenger;
            this.pleaseWaitService = pleaseWaitService;

            this.syncManager = SynchronizationFactory.GetSyncManager();

            this.LoginCommand = new DelegateCommand(Login, CanLogin);
            this.LoginProgressVisibility = Visibility.Collapsed;
            this.Username = UserSettingsManager.Instance.LastLoginUserName;

            this.IsSyncScopesUpdateNecessary = false;
        }

        #region Enums

        /// <summary>
        /// The steps performed during the login process.
        /// </summary>
        private enum LoginSteps
        {
            /// <summary>
            /// Represents the action of connecting to the local database.
            /// </summary>
            ConnectToLocalDb,

            /// <summary>
            /// Represents the action of connecting to the central server.
            /// </summary>
            ConnectToCentralServer,

            /// <summary>
            /// Represents the action of synchronizing data.
            /// </summary>
            SynchronizeData,

            /// <summary>
            /// Represents the action of authorizing a user.
            /// </summary>
            Login
        }

        /// <summary>
        /// The statuses a login step can have.
        /// </summary>
        private enum LoginStepStatus
        {
            /// <summary>
            /// The login step is being performed.
            /// </summary>
            InProgress,

            /// <summary>
            /// The login step has finished and the result was success.
            /// </summary>
            Success,

            /// <summary>
            /// The login step has finished and the result was failure.
            /// </summary>
            Fail
        }

        #endregion Enums

        #region Commands

        /// <summary>
        /// Gets the login command.
        /// </summary>        
        public DelegateCommand LoginCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets the visibility of the login progress UI.
        /// </summary>
        public Visibility LoginProgressVisibility
        {
            get
            {
                return this.loginProgressVisibility;
            }

            private set
            {
                if (this.loginProgressVisibility != value)
                {
                    this.loginProgressVisibility = value;
                    OnPropertyChanged("LoginProgressVisibility");
                }
            }
        }

        /// <summary>
        /// Gets or sets the username inputted by the user.
        /// </summary>
        public string Username
        {
            get
            {
                return this.username;
            }

            set
            {
                if (this.username != value)
                {
                    this.username = value;
                    OnPropertyChanged("Username");
                }
            }
        }

        /// <summary>
        /// Gets or sets the password inputted by the user.
        /// </summary>
        /// <value>The password.</value>
        public string Password { private get; set; }

        /// <summary>
        /// Gets or sets the status icon for the "Connecting to central server" operation
        /// </summary>
        public UIElement ConnectToCentralServerStatusIcon
        {
            get
            {
                return this.connectToCentralServerStatusIcon;
            }

            set
            {
                if (this.connectToCentralServerStatusIcon != value)
                {
                    this.connectToCentralServerStatusIcon = value;
                    OnPropertyChanged("ConnectToCentralServerStatusIcon");
                }
            }
        }

        /// <summary>
        /// Gets or sets the status icon for the "Connecting to the local database" operation.
        /// </summary>
        public UIElement ConnectToLocalDbStatusIcon
        {
            get { return this.connectToLocalDbStatusIcon; }
            set { this.SetProperty(ref this.connectToLocalDbStatusIcon, value, () => this.ConnectToLocalDbStatusIcon); }
        }

        /// <summary>
        /// Gets or sets the status icon for the "Synchronizing user data" operation
        /// </summary>        
        public UIElement SynchStatusIcon
        {
            get
            {
                return this.synchStatusIcon;
            }

            set
            {
                if (this.synchStatusIcon != value)
                {
                    this.synchStatusIcon = value;
                    OnPropertyChanged("SynchStatusIcon");
                }
            }
        }

        /// <summary>
        /// Gets or sets the status icon for the "Logging in" operation
        /// </summary>        
        public UIElement LoginStatusIcon
        {
            get
            {
                return this.loginStatusIcon;
            }

            set
            {
                if (this.loginStatusIcon != value)
                {
                    this.loginStatusIcon = value;
                    OnPropertyChanged("LoginStatusIcon");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is sync maintenance necessary.
        /// </summary>        
        public bool IsSyncScopesUpdateNecessary { get; set; }

        #endregion Properties

        /// <summary>
        /// Determines whether the Login command can be executed.
        /// </summary>
        /// <returns>true if the command can be executed; otherwise, false.</returns>
        private bool CanLogin()
        {
            if (!string.IsNullOrWhiteSpace(this.Username) && !string.IsNullOrWhiteSpace(this.Password))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Starts the login process.
        /// </summary>
        private void Login()
        {
            if (this.loginWorker != null && this.loginWorker.IsBusy)
            {
                return;
            }

            if (string.IsNullOrWhiteSpace(this.Username))
            {
                this.messageDialogService.Show(LocalizedResources.UserAuthentication_NoUsername, MessageDialogType.Error);
                return;
            }

            if (string.IsNullOrEmpty(this.Password))
            {
                this.messageDialogService.Show(LocalizedResources.UserAuthentication_NoPassword, MessageDialogType.Error);
                return;
            }

            this.loginWorker = new BackgroundWorker();
            this.loginWorker.WorkerReportsProgress = true;
            this.loginWorker.DoWork += new DoWorkEventHandler(DoLoginAsync);
            this.loginWorker.ProgressChanged += new ProgressChangedEventHandler(LogingProgressChanged);
            this.loginWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(LoginCompleted);

            ResetLoginStatusIcons();
            this.LoginProgressVisibility = Visibility.Visible;
            string[] args = new string[2] { this.Username, this.Password };
            this.loginWorker.RunWorkerAsync(args);
        }

        /// <summary>
        /// Performs the login asynchronously.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.ComponentModel.DoWorkEventArgs"/> instance containing the event data.</param>
        private void DoLoginAsync(object sender, DoWorkEventArgs e)
        {
            var args = e.Argument as string[];
            var userName = args[0];
            var password = args[1];
            var worker = (BackgroundWorker)sender;

            ConnectToLocalDatabase(worker);

            // Initialize the local data cache.
            LocalDataCache.Initialize();

            // Check central server state
            ConnectToCentralDatabase(userName, worker);

            // Authenticate user.
            var authResult = AuthenticateUser(userName, password, worker);
            e.Result = authResult;

            // Add a bit of delay so the user can see the "Logging in" status before the window disappears.
            System.Threading.Thread.Sleep(500);
        }

        /// <summary>
        /// Connects to the local database during the login process.
        /// </summary>
        /// <param name="worker">The worker calling the method.</param>
        private void ConnectToLocalDatabase(BackgroundWorker worker)
        {
            worker.ReportProgress((int)LoginSteps.ConnectToLocalDb, LoginStepStatus.InProgress);
            try
            {
                DatabaseHelper.ValidateDatabaseVersion(DbIdentifier.LocalDatabase);
                worker.ReportProgress((int)LoginSteps.ConnectToLocalDb, LoginStepStatus.Success);
            }
            catch
            {
                worker.ReportProgress((int)LoginSteps.ConnectToLocalDb, LoginStepStatus.Fail);
                throw;
            }
        }

        /// <summary>
        /// Connects to the central database during the login process. 
        /// </summary>
        /// <param name="userName">The ser name used for logging in.</param>
        /// <param name="worker">The worker calling the method.</param>
        private void ConnectToCentralDatabase(string userName, BackgroundWorker worker)
        {
            worker.ReportProgress((int)LoginSteps.ConnectToCentralServer, LoginStepStatus.InProgress);
            var centralServerIsOnline = DatabaseHelper.IsCentralDatabaseOnline();

            var status = centralServerIsOnline ? LoginStepStatus.Success : LoginStepStatus.Fail;
            worker.ReportProgress((int)LoginSteps.ConnectToCentralServer, status);

            // Synchronize the user's local data with the data from the central server.
            if (centralServerIsOnline)
            {
                worker.ReportProgress((int)LoginSteps.SynchronizeData, LoginStepStatus.InProgress);
                try
                {
                    var success = SyncDataWithCentral(userName, worker);
                    if (success)
                    {
                        worker.ReportProgress((int)LoginSteps.SynchronizeData, LoginStepStatus.Success);
                    }
                    else
                    {
                        worker.ReportProgress((int)LoginSteps.SynchronizeData, LoginStepStatus.Fail);
                    }
                }
                catch (DataAccessException)
                {
                    worker.ReportProgress((int)LoginSteps.SynchronizeData, LoginStepStatus.Fail);
                }
                catch (SynchronizationException)
                {
                    worker.ReportProgress((int)LoginSteps.SynchronizeData, LoginStepStatus.Fail);
                }
                catch (UIException)
                {
                    worker.ReportProgress((int)LoginSteps.SynchronizeData, LoginStepStatus.Fail);
                }
            }
            else
            {
                worker.ReportProgress((int)LoginSteps.SynchronizeData, LoginStepStatus.Fail);
            }
        }

        /// <summary>
        /// Synchronizes the necessary data during the connect to central step.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="worker">The worker.</param>
        /// <returns>True if it has succeeded, false otherwise</returns>
        private bool SyncDataWithCentral(string userName, BackgroundWorker worker)
        {
            // Sync the global settings
            this.syncManager.Synchronize(SyncType.GlobalSettings, Guid.Empty, SyncDirection.Download, false);

            // Sync the user logging in
            var dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            var user = dataManager.UserRepository.GetByUsername(userName, true, true);
            if (user != null)
            {
                this.syncManager.Synchronize(SyncType.User, user.Guid, SyncDirection.Download, false);
                return true;
            }
            else
            {
                SecurityManager.Instance.DeleteUser(userName);
                Log.Info("Could not get the user with the user name {0}.", userName);
                return false;
            }
        }

        /// <summary>
        /// The user authentication part of the login process.
        /// </summary>
        /// <param name="userName">The user name to be used for authentication.</param>
        /// <param name="password">The password to be used for authentication.</param>
        /// <param name="worker">The worker calling the method.</param>
        /// <returns>True if the authentication was successful; otherwise, false.</returns>
        private bool AuthenticateUser(string userName, string password, BackgroundWorker worker)
        {
            worker.ReportProgress((int)LoginSteps.Login, LoginStepStatus.InProgress);
            try
            {
                var loggedInUser = SecurityManager.Instance.Login(userName, password);
                var loginSuccess = loggedInUser != null;
                worker.ReportProgress((int)LoginSteps.Login, loginSuccess ? LoginStepStatus.Success : LoginStepStatus.Fail);

                if (loginSuccess)
                {
                    // If the password was not set by the user he needs to change its password before continuing.
                    var centralServerIsOnline = DatabaseHelper.IsCentralDatabaseOnline();
                    if (!SecurityManager.Instance.CurrentUser.IsPasswordSetByUser
                        && centralServerIsOnline)
                    {
                        var viewModel = this.container.GetExportedValue<ChangePasswordViewModel>();
                        viewModel.IsPasswordChangeMandatory = true;
                        this.dispatcher.Invoke(() =>
                        {
                            this.windowService.ShowViewInDialog(viewModel, "ChangePasswordViewTemplate");
                        });

                        if (!viewModel.IsPasswordValid.GetValueOrDefault() || !SecurityManager.Instance.CurrentUser.IsPasswordSetByUser)
                        {
                            throw new BusinessException(ErrorCodes.PasswordChangeMandatory);
                        }
                        else
                        {
                            // If the password was successfully changed update the local users data because the user data synchronization already occurred.
                            using (var centralDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase))
                            {
                                var userFromCentral = centralDataManager.UserRepository.GetByUsername(userName, false, true);
                                if (userFromCentral != null)
                                {
                                    using (var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase))
                                    {
                                        var userFromLocal = localDataManager.UserRepository.GetByUsername(userName, false);
                                        if (userFromLocal != null)
                                        {
                                            userFromCentral.CopyValuesTo(userFromLocal);
                                            localDataManager.SaveChanges();
                                        }
                                    }
                                }
                            }
                        }
                    }

                    this.IsSyncScopesUpdateNecessary = this.syncManager.IsSyncScopeUpdateNecessary(SecurityManager.Instance.CurrentUser.Guid);
                }

                return loginSuccess;
            }
            catch
            {
                worker.ReportProgress((int)LoginSteps.Login, LoginStepStatus.Fail);
                throw;
            }
        }

        /// <summary>
        /// Handles the progress change for the login process.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.ComponentModel.ProgressChangedEventArgs"/> instance containing the event data.</param>
        private void LogingProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            var step = (LoginSteps)e.ProgressPercentage;
            var stepStatus = (LoginStepStatus)e.UserState;
            SetLoginStepStatus(step, stepStatus);
        }

        /// <summary>
        /// Handles the completion of the login process
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.ComponentModel.RunWorkerCompletedEventArgs"/> instance containing the event data.</param>
        private void LoginCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.LoginProgressVisibility = Visibility.Collapsed;
            if (e.Error != null)
            {
                this.messageDialogService.Show(e.Error);
            }
            else
            {
                var loginSuccess = (bool)e.Result;
                if (!loginSuccess)
                {
                    // if the user doesn't exist in the database, display error message
                    this.messageDialogService.Show(LocalizedResources.UserAuthentication_InvalidUserPass, MessageDialogType.Error);
                }
                else
                {
                    // Save last login user name in the app configuration file. 
                    UserSettingsManager.Instance.LastLoginUserName = this.Username;
                    UserSettingsManager.Instance.Save();

                    if (this.IsSyncScopesUpdateNecessary)
                    {
                        this.PerformDatabaseMaintenance();
                    }
                    else
                    {
                        NavigateToNextScreenAfterLogin();
                    }
                }
            }
        }

        /// <summary>
        /// Navigate to next screen after the login
        /// </summary>
        private void NavigateToNextScreenAfterLogin()
        {
            // if the user has more than one role, give the user the possibility to select one
            var shell = this.container.GetExportedValue<ShellViewModel>();
            if (RoleRights.HasMultipleRoles(SecurityManager.Instance.CurrentUser.Roles))
            {
                shell.Content = this.container.GetExportedValue<RoleSelectionViewModel>();
            }
            else if (UserSettingsManager.Instance.ShowStartScreen)
            {
                shell.Content = this.container.GetExportedValue<WelcomeViewModel>();
            }
            else
            {
                shell.Content = this.container.GetExportedValue<MainViewModel>();
            }

            this.messenger.Send(new NotificationMessage(Notification.LoggedIn));
        }

        /// <summary>
        /// Resets the login status icons.
        /// </summary>
        private void ResetLoginStatusIcons()
        {
            this.ConnectToLocalDbStatusIcon = null;
            this.ConnectToCentralServerStatusIcon = null;
            this.SynchStatusIcon = null;
            this.LoginStatusIcon = null;
        }

        /// <summary>
        /// Sets the status of a login step.
        /// </summary>
        /// <param name="step">The step for which to set the status.</param>
        /// <param name="status">The status to set.</param>
        private void SetLoginStepStatus(LoginSteps step, LoginStepStatus status)
        {
            // TODO: the status icons should be changed from xaml/styles not from VM
            UIElement statusIcon = null;
            switch (status)
            {
                case LoginStepStatus.Success:
                    Image image = new Image();
                    image.SetResourceReference(Image.SourceProperty, Images.CheckmarkIconKey);
                    statusIcon = image;
                    break;

                case LoginStepStatus.Fail:
                    Image image2 = new Image();
                    image2.SetResourceReference(Image.SourceProperty, Images.ErrorIconKey);
                    statusIcon = image2;
                    break;

                case LoginStepStatus.InProgress:
                    CircularProgressControl animation = new CircularProgressControl();
                    statusIcon = animation;
                    break;
            }

            switch (step)
            {
                case LoginSteps.ConnectToLocalDb:
                    this.ConnectToLocalDbStatusIcon = statusIcon;
                    break;

                case LoginSteps.ConnectToCentralServer:
                    this.ConnectToCentralServerStatusIcon = statusIcon;
                    break;

                case LoginSteps.Login:
                    this.LoginStatusIcon = statusIcon;
                    break;

                case LoginSteps.SynchronizeData:
                    this.SynchStatusIcon = statusIcon;
                    break;
            }
        }

        #region Database maintenance

        /// <summary>
        /// Performs the database maintenance and country settings timestamp fix.
        /// </summary>
        private void PerformDatabaseMaintenance()
        {
            var onlineChecker = this.container.GetExportedValue<IOnlineCheckService>();
            Action<PleaseWaitService.WorkParams> work = (workparams) =>
            {
                var localDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
                this.FixCountrySettingsWithDefaultTimestamp(localDataManager);

                if (onlineChecker.IsOnline)
                {
                    var centralDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
                    this.FixCountrySettingsWithDefaultTimestamp(centralDataManager);

                    this.syncManager.InitializeScopesAndConvert(SecurityManager.Instance.CurrentUser.Guid, true);
                }
            };

            Action<PleaseWaitService.WorkParams> workCompleted = (workparams) =>
            {
                if (workparams.Error != null)
                {
                    Log.ErrorException("The update of the Sync Scopes could not be completed.", workparams.Error);
                };

                NavigateToNextScreenAfterLogin();
            };

            this.pleaseWaitService.Show(LocalizedResources.Synchronization_DataBaseMaintenanceWaitMessage, work, workCompleted);
        }

        /// <summary>
        /// Fixes the country settings with default timestamp by setting it to the right value. This is done by comparing them with the master data country settings.
        /// </summary>
        /// <param name="dataManager">The data manager.</param>
        /// <exception cref="System.ArgumentNullException">dataManager;The data manager was null.</exception>
        private void FixCountrySettingsWithDefaultTimestamp(IDataSourceManager dataManager)
        {
            var countrySettingsToFix = dataManager.CountrySettingRepository.GetCountrySettingsWithDefaultTimestamp();
            if (countrySettingsToFix.Count == 0)
            {
                return;
            }

            var countries = dataManager.CountryRepository.GetAll();
            foreach (var settingToFix in countrySettingsToFix)
            {
                var parentAssembly = settingToFix.Assemblies.FirstOrDefault();
                if (parentAssembly != null)
                {
                    this.SetCountrySettingTimestamp(settingToFix, parentAssembly.AssemblingCountryId, parentAssembly.AssemblingCountry, parentAssembly.AssemblingSupplier, countries);
                }

                var parentPart = settingToFix.Parts.FirstOrDefault();
                if (parentPart != null)
                {
                    var parentCountryId = parentPart.ManufacturingCountryId ?? Guid.Empty;

                    Country country = null;
                    if (parentPart.ManufacturingCountry != null)
                    {
                        foreach (var c in countries)
                        {
                            if (c.Guid != parentCountryId &&
                                (c.Name == null || !c.Name.Trim().Equals(parentPart.ManufacturingCountry.Trim(), StringComparison.OrdinalIgnoreCase)) &&
                                (c.Name == null || !c.Name.Trim().Equals(parentPart.ManufacturingCountry.Trim() + " country", StringComparison.OrdinalIgnoreCase)))
                            {
                                continue;
                            }

                            country = c;
                            break;
                        }
                    }

                    if (country != null)
                    {
                        this.SetCountrySettingTimestamp(settingToFix, parentPart.ManufacturingCountryId, parentPart.ManufacturingCountry, parentPart.ManufacturingSupplier, countries);
                    }
                }
            }

            dataManager.SaveChanges();
        }

        /// <summary>
        /// Sets the country setting timestamp.
        /// </summary>
        /// <param name="setting">The country setting.</param>
        /// <param name="countryId">The country setting parent country Id.</param>
        /// <param name="countryName">The country setting parent country name.</param>
        /// <param name="supplierName">The country setting parent supplier name.</param>
        /// <param name="countries">The countries.</param>
        /// <exception cref="System.ArgumentNullException">dataManager;The data manager was null.</exception>
        /// <remarks>
        /// The last change timestamp is determined by comparing the values of the current and master data country settings.
        /// If they are equal the timestamp of the master data country is used, else and older timestamp is set.
        /// If the country setting has no master data country setting associated the current timestamp is used.
        /// </remarks>
        private void SetCountrySettingTimestamp(CountrySetting setting, Guid? countryId, string countryName, string supplierName, Collection<Country> countries)
        {
            if (setting == null
                || (!countryId.HasValue && string.IsNullOrWhiteSpace(countryName)))
            {
                return;
            }

            // Try to get the country by Id; if this is null
            // try to get it by name (case-insensitive); if this is null
            // try to get it by name combined with "Country" (case-insensitive); the search string will be "<country-name> Country" (ex: "China Country").
            CountrySetting masterDataCountrySetting = null;
            var parentCountryId = countryId ?? Guid.Empty;
            var country = countries.FirstOrDefault(c => c.Guid == parentCountryId || c.Name.Trim() == countryName.Trim() || c.Name.Trim() == countryName.Trim() + " country");
            if (country != null)
            {
                masterDataCountrySetting = country.CountrySetting;
                if (!string.IsNullOrWhiteSpace(supplierName))
                {
                    var supplier = country.States.FirstOrDefault(s => s.Name.Trim() == supplierName.Trim());
                    if (supplier != null
                        && supplier.CountrySettings != null)
                    {
                        masterDataCountrySetting = supplier.CountrySettings;
                    }
                }
            }

            if (masterDataCountrySetting != null)
            {
                // If the compared settings have the same values set the timestamp from the master data country setting.
                // Else set an older timestamp.
                if (setting.EqualsValues(masterDataCountrySetting))
                {
                    setting.LastChangeTimestamp = masterDataCountrySetting.LastChangeTimestamp;
                }
                else
                {
                    setting.LastChangeTimestamp = masterDataCountrySetting.LastChangeTimestamp.AddMonths(-1);
                }
            }
        }

        #endregion Database maintenance
    }
}