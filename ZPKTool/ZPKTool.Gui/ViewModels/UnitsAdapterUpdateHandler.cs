﻿namespace ZPKTool.Gui.ViewModels
{
    using System.ComponentModel;
    using ZPKTool.Common;
    using ZPKTool.MvvmCore.Services;

    /// <summary>
    /// This class has the role of handling different changes performed in UnitsAdapter.
    /// </summary>
    public class UnitsAdapterUpdateHandler
    {
        /// <summary>
        /// The units adapter.
        /// </summary>
        private UnitsAdapter unitsAdapter;

        /// <summary>
        /// Weak event listener for the PropertyChanging notification.
        /// </summary>
        private WeakEventListener<PropertyChangingEventArgs> propertyChangingListener;

        /// <summary>
        /// Weak event listener for the PropertyChanged notification.
        /// </summary>
        private WeakEventListener<PropertyChangedEventArgs> propertyChangedListener;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitsAdapterUpdateHandler"/> class.
        /// </summary>
        /// <param name="adapter">The units adapter.</param>
        public UnitsAdapterUpdateHandler(UnitsAdapter adapter)
        {
            this.unitsAdapter = adapter;
        }

        /// <summary>
        /// Pause the undo manager when some units from UnitsAdapter are changing.
        /// </summary>
        /// <param name="undoManager">The undo manager.</param>
        public void PauseUndoOnUnitsAdapterUpdate(UndoManager undoManager)
        {
            if (this.propertyChangingListener != null)
            {
                PropertyChangingEventManager.RemoveListener(this.unitsAdapter, propertyChangingListener);
            }

            this.propertyChangingListener = new WeakEventListener<PropertyChangingEventArgs>((s, e) => { undoManager.Pause(); });
            PropertyChangingEventManager.AddListener(this.unitsAdapter, propertyChangingListener);

            if (this.propertyChangedListener != null)
            {
                PropertyChangedEventManager.RemoveListener(this.unitsAdapter, propertyChangedListener, string.Empty);
            }

            this.propertyChangedListener = new WeakEventListener<PropertyChangedEventArgs>((s, e) => { undoManager.Resume(); });
            PropertyChangedEventManager.AddListener(this.unitsAdapter, propertyChangedListener, string.Empty);
        }
    }
}
