﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Input;
using ZPKTool.Business.TransportCostCalculator;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the TransportCostCalculation view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class TransportCostCalculationViewModel : ViewModel<TransportCostCalculation, IDataSourceManager>
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        /// <summary>
        /// The composition container.
        /// </summary>
        private CompositionContainer compositionContainer;

        /// <summary>
        /// The PleaseWaitService reference.
        /// </summary>
        private IPleaseWaitService pleaseWaitService;

        /// <summary>
        /// The supplier browser view model.
        /// </summary>
        private CountryAndSupplierBrowserViewModel supplierBrowser;

        /// <summary>
        /// A value indicating whether the popup for settings is opened or not.
        /// </summary>
        private bool isSettingsPopupOpen;

        /// <summary>
        /// A value indicating whether the popup that displays the error message for calculating distance is opened or not.
        /// </summary>
        private bool isRouteErrorPopupOpen;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="TransportCostCalculationViewModel"/> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        /// <param name="messenger">The messenger.</param>
        /// <param name="unitsService">The units service.</param>
        /// <param name="compositionContainer">The composition container.</param>
        /// <param name="pleaseWaitService">The please wait service.</param>
        [ImportingConstructor]
        public TransportCostCalculationViewModel(
            IWindowService windowService,
            IMessenger messenger,
            IUnitsService unitsService,
            CompositionContainer compositionContainer,
            IPleaseWaitService pleaseWaitService)
        {
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("unitsService", unitsService);
            Argument.IsNotNull("compositionContainer", compositionContainer);
            Argument.IsNotNull("pleaseWaitService", pleaseWaitService);

            this.windowService = windowService;
            this.messenger = messenger;
            this.unitsService = unitsService;
            this.compositionContainer = compositionContainer;
            this.pleaseWaitService = pleaseWaitService;

            this.BrowseMasterDataCommand = new DelegateCommand(this.BrowseMasterData, () => !this.IsReadOnly);
            this.OpenSettingsPopupCommand = new DelegateCommand(() => this.IsSettingsPopupOpen = true);
            this.CalculateDistanceCommand = new DelegateCommand(this.CalculateDrivingDistance, this.CanCalculateDrivingDistance);
            this.CloseErrorPopupCommand = new DelegateCommand(() => this.IsRouteErrorPopupOpen = false);

            // Initialize the lists.
            this.Carriers = TransportCostCalculator.GetCarriers();
            this.Transporters = TransportCostCalculator.GetTransporters();

            this.ShowDeleteButtonOnTab.Value = true;
        }

        #region Commands

        /// <summary>
        /// Gets the command that opens the Master Data Browser.
        /// </summary>
        public ICommand BrowseMasterDataCommand { get; private set; }

        /// <summary>
        /// Gets the command that opens the popup with the additional settings.
        /// </summary>
        public ICommand OpenSettingsPopupCommand { get; private set; }

        /// <summary>
        /// Gets the command that calculated the driving distance and route between source and destination.
        /// </summary>
        public ICommand CalculateDistanceCommand { get; private set; }

        /// <summary>
        /// Gets the command that closes the popup displayed for errors occurred after calculating the driving distance.
        /// </summary>
        public ICommand CloseErrorPopupCommand { get; private set; }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the index of the calculation.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Index")]
        public DataProperty<int?> Index { get; private set; }

        /// <summary>
        /// Gets the name of the part for which the route is calculated.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("PartName")]
        public DataProperty<string> PartName { get; private set; }

        /// <summary>
        /// Gets the number of the part for which the route is calculated.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("PartNumber")]
        public DataProperty<string> PartNumber { get; private set; }

        /// <summary>
        /// Gets the weight of the part for which the route is calculated.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("PartWeight")]
        public DataProperty<decimal?> PartWeight { get; private set; }

        /// <summary>
        /// Gets the supplier of the part for which the route is calculated.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Supplier")]
        public DataProperty<string> Supplier { get; private set; }

        /// <summary>
        /// Gets the location of the route's source.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Location", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("SourceLocation")]
        public DataProperty<string> SourceLocation { get; private set; }

        /// <summary>
        /// Gets the country of the route's source.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_CountryName", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("SourceCountry")]
        public DataProperty<string> SourceCountry { get; private set; }

        /// <summary>
        /// Gets the zip code of the route's source.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_ZipCode", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("SourceZipCode")]
        public DataProperty<string> SourceZipCode { get; private set; }

        /// <summary>
        /// Gets the latitude coordinate of the route's source.
        /// </summary>
        [Range(-90d, 90d, ErrorMessageResourceName = "TransportCostCalculator_LatitudeValidation", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("SourceLat")]
        public DataProperty<decimal?> SourceLatitude { get; private set; }

        /// <summary>
        /// Gets the longitude coordinate of the route's source.
        /// </summary>
        [Range(-180d, 180d, ErrorMessageResourceName = "TransportCostCalculator_LongitudeValidation", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("SourceLong")]
        public DataProperty<decimal?> SourceLongitude { get; private set; }

        /// <summary>
        /// Gets the location of the route's destination.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Location", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("DestinationLocation")]
        public DataProperty<string> DestinationLocation { get; private set; }

        /// <summary>
        /// Gets the country of the route's destination.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_CountryName", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("DestinationCountry")]
        public DataProperty<string> DestinationCountry { get; private set; }

        /// <summary>
        /// Gets the zip code of the route's destination.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_ZipCode", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("DestinationZipCode")]
        public DataProperty<string> DestinationZipCode { get; private set; }

        /// <summary>
        /// Gets the latitude coordinate of the route's source.
        /// </summary>
        [Range(-90d, 90d, ErrorMessageResourceName = "TransportCostCalculator_LatitudeValidation", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("DestinationLat")]
        public DataProperty<decimal?> DestinationLatitude { get; private set; }

        /// <summary>
        /// Gets the longitude coordinate of the route's source.
        /// </summary>
        [Range(-180d, 180d, ErrorMessageResourceName = "TransportCostCalculator_LongitudeValidation", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("DestinationLong")]
        public DataProperty<decimal?> DestinationLongitude { get; private set; }

        /// <summary>
        /// Gets the distance from source to destination.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Distance", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Distance")]
        public DataProperty<decimal?> Distance { get; private set; }

        /// <summary>
        /// Gets the daily needed quantity.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_DailyNeededQuantity", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("DailyNeededQuantity")]
        public DataProperty<int?> DailyNeededQuantity { get; private set; }

        /// <summary>
        /// Gets the selected part type.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_PartType", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("PartType")]
        public DataProperty<PartType> SelectedPartType { get; private set; }

        /// <summary>
        /// Gets the quantity per pack.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("QuantityPerPack")]
        public DataProperty<int?> QuantityPerPack { get; private set; }

        /// <summary>
        /// Gets the selected type of packaging.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_PackagingType", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty]
        [ExposesModelProperty("PackagingType")]
        public DataProperty<PackagingType> SelectedPackagingType { get; private set; }

        /// <summary>
        /// Gets the route's description.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Description")]
        public DataProperty<string> Description { get; private set; }

        /// <summary>
        /// Gets the cost of a package.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_CostByPackage", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("CostByPackage")]
        public DataProperty<decimal?> CostByPackage { get; private set; }

        /// <summary>
        /// Gets the weight of a package.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Weight", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("PackageWeight")]
        public DataProperty<decimal?> PackageWeight { get; private set; }

        /// <summary>
        /// Gets the length of a bouncing box. 
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Length", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("BouncingBoxLength")]
        public DataProperty<decimal?> BouncingBoxLength { get; private set; }

        /// <summary>
        /// Gets the interference percentage if the part is a single component.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Interference", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Interference")]
        public DataProperty<decimal?> Interference { get; private set; }

        /// <summary>
        /// Gets the width of the bouncing box.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Width", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("BouncingBoxWidth")]
        public DataProperty<decimal?> BouncingBoxWidth { get; private set; }

        /// <summary>
        /// Gets the height of the bouncing box.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Height", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("BouncingBoxHeight")]
        public DataProperty<decimal?> BouncingBoxHeight { get; private set; }

        /// <summary>
        /// Gets the density percentage of packing if there are bulk goods.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_PackingDensity", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("PackingDensity")]
        public DataProperty<decimal?> PackingDensity { get; private set; }

        /// <summary>
        /// Gets the type of the carrier.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Type", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty]
        [ExposesModelProperty("CarrierType")]
        public DataProperty<short?> CarrierId { get; private set; }

        /// <summary>
        /// Gets the length of the carrier.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Length", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("CarrierLength")]
        public DataProperty<decimal?> CarrierLength { get; private set; }

        /// <summary>
        /// Gets the height of the carrier.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Height", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("CarrierHeight")]
        public DataProperty<decimal?> CarrierHeight { get; private set; }

        /// <summary>
        /// Gets the width of the carrier.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Width", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("CarrierWidth")]
        public DataProperty<decimal?> CarrierWidth { get; private set; }

        /// <summary>
        /// Gets the net weight of the carrier.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_NetWeight", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("NetWeight")]
        public DataProperty<decimal?> NetWeight { get; private set; }

        /// <summary>
        /// Gets the maximum package carrier per packaging.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_MaxPackageCarrier", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("MaxPackageCarrier")]
        public DataProperty<int?> MaxPackageCarrier { get; private set; }

        /// <summary>
        /// Gets the maximum load capacity.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("MaxLoadCapacity")]
        [Required(ErrorMessageResourceName = "RequiredField_MaxLoadCapacity", ErrorMessageResourceType = typeof(LocalizedResources))]
        [WeightValidator]
        public DataProperty<decimal?> MaxLoadCapacity { get; private set; }

        /// <summary>
        /// Gets the type of the transporter.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_TransporterType", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("TransporterType")]
        public DataProperty<short?> TransporterId { get; private set; }

        /// <summary>
        /// Gets the length of the transporter.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Length", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("TransporterLength")]
        public DataProperty<decimal?> TransporterLength { get; private set; }

        /// <summary>
        /// Gets the width of the transporter.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Width", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("TransporterWidth")]
        public DataProperty<decimal?> TransporterWidth { get; private set; }

        /// <summary>
        /// Gets the height of the transporter.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Height", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("TransporterHeight")]
        public DataProperty<decimal?> TransporterHeight { get; private set; }

        /// <summary>
        /// Gets the cost per km of the transporter.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_TransporterCostPerKm", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("TransporterCostPerKm")]
        public DataProperty<decimal?> TransporterCostPerKm { get; private set; }

        /// <summary>
        /// Gets the country of the part for which the route is calculated.
        /// </summary>
        public VMProperty<string> PartCountry { get; private set; }

        /// <summary>
        /// Gets the country's guid of the part for which the route is calculated.
        /// </summary>
        public VMProperty<Guid?> PartCountryId { get; private set; }

        /// <summary>
        /// Gets the route's annual packaging cost.
        /// </summary>
        public VMProperty<decimal> PackagingCost { get; private set; }

        /// <summary>
        /// Gets the route's annual transport cost.
        /// </summary>
        public VMProperty<decimal> TransportCost { get; private set; }

        /// <summary>
        /// Gets the route's packaging cost per piece.
        /// </summary>
        public VMProperty<decimal> PackagingCostPerPcs { get; private set; }

        /// <summary>
        /// Gets the route's transport cost per piece.
        /// </summary>
        public VMProperty<decimal> TransportCostPerPcs { get; private set; }

        /// <summary>
        /// Gets the annual transported quantity.
        /// </summary>
        public VMProperty<decimal> AnnualQty { get; private set; }

        /// <summary>
        /// Gets the volume of the bouncing box.
        /// </summary>
        public VMProperty<decimal> BouncingBoxVolume { get; private set; }

        /// <summary>
        /// Gets the volume of the carrier.
        /// </summary>
        public VMProperty<decimal> CarrierVolume { get; private set; }

        /// <summary>
        /// Gets the parts per carrier per package carrier.
        /// </summary>
        public VMProperty<decimal> PartsPerCarrier { get; private set; }

        /// <summary>
        /// Gets the package carrier per packaging for release.
        /// </summary>
        public VMProperty<decimal> PackageCarrierPerPackaging { get; private set; }

        /// <summary>
        /// Gets the annual number of package carrier.
        /// </summary>
        public VMProperty<decimal> AnnualNumberOfPackageCarrier { get; private set; }

        /// <summary>
        /// Gets the maximum theoretical value for maximum package carrier per packaging field.
        /// </summary>
        public VMProperty<decimal> Maxtheo { get; private set; }

        /// <summary>
        /// Gets the needed transports for release.
        /// </summary>
        public VMProperty<decimal> NeededTransportsForRelease { get; private set; }

        /// <summary>
        /// Gets the annual number of transports.
        /// </summary>
        public VMProperty<decimal> AnnualNumberOfTransports { get; private set; }

        /// <summary>
        /// Gets the percentage of filling degree transporter per transport.
        /// </summary>
        public VMProperty<decimal> FillingDegreeTransporter { get; private set; }

        /// <summary>
        /// Gets the weight of the carrier.
        /// </summary>
        public VMProperty<decimal> CarrierWeight { get; private set; }

        /// <summary>
        /// Gets the weight of parts, including packaging.
        /// </summary>
        public VMProperty<decimal> PartsWeight { get; private set; }

        /// <summary>
        /// Gets the total weight transported in a route.
        /// </summary>
        public DataProperty<decimal> TotalWeight { get; private set; }

        /// <summary>
        /// Gets the cargo volume of the transporter.
        /// </summary>
        public VMProperty<decimal> TransporterVolume { get; private set; }

        /// <summary>
        /// Gets the tab header.
        /// </summary>
        public VMProperty<string> TabHeader { get; private set; }

        /// <summary>
        /// Gets the list of carriers.
        /// </summary>
        public ICollection<Carrier> Carriers { get; private set; }

        /// <summary>
        /// Gets the list of transporters.
        /// </summary>
        public ICollection<Transporter> Transporters { get; private set; }

        /// <summary>
        /// Gets the TransportCostCalculationSettingViewModel.
        /// </summary>
        public TransportCostCalculationSettingViewModel SettingViewModel { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the delete button should be shown on the view model's corresponding tab.
        /// </summary>
        public VMProperty<bool> ShowDeleteButtonOnTab { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the text boxes from view that depend on the carrier type should be enabled or not.
        /// </summary>
        public VMProperty<bool> IsEnabledForManualCarrierType { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the text boxes from view that depend on the transporter type should be enabled or not.
        /// </summary>
        public VMProperty<bool> IsEnabledForManualTransporterType { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the text boxes from view that depend on the part type should be enabled or not.
        /// </summary>
        public VMProperty<bool> IsEnabledForPartType { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether the popup for settings is opened or not.
        /// </summary>
        public bool IsSettingsPopupOpen
        {
            get
            {
                return this.isSettingsPopupOpen;
            }

            set
            {
                this.SetProperty(ref this.isSettingsPopupOpen, value, () => this.IsSettingsPopupOpen);
                if (!value)
                {
                    if (this.SettingViewModel.IsChanged)
                    {
                        // If the setting view model has no error and the values are changed, save them into model and get the transporter cost per km.
                        this.SettingViewModel.SaveCommand.Execute(null);
                        this.SetTransporterCostPerKmValue();
                    }

                    this.HandleSettingsViewModelsError();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the popup that displays the error message for calculating distance is opened or not.
        /// </summary>
        public bool IsRouteErrorPopupOpen
        {
            get { return this.isRouteErrorPopupOpen; }
            set { this.SetProperty(ref this.isRouteErrorPopupOpen, value, () => this.IsRouteErrorPopupOpen); }
        }

        /// <summary>
        /// Gets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the distance is being calculated or not.
        /// </summary>
        public VMProperty<bool> IsCalculatingDistance { get; private set; }

        /// <summary>
        /// Gets the error message returned after calculating the driving route.
        /// </summary>
        public VMProperty<string> RouteErrorMessage { get; private set; }

        /// <summary>
        /// Gets or sets the view model's error.
        /// </summary>
        public override string Error
        {
            get
            {
                return base.Error;
            }

            set
            {
                base.Error = value;
                if (this.SettingViewModel != null)
                {
                    // Each time the view model's error is reset, it must reevaluate the error from setting view model.
                    this.HandleSettingsViewModelsError();
                }
            }
        }

        #endregion

        /// <summary>
        /// Called when the model has changed.
        /// </summary>
        protected override void OnModelChanged()
        {
            base.OnModelChanged();

            this.InitializeSettingsViewModel();
            this.RegisterValueChangedEvent();

            this.IsEnabledForManualCarrierType.Value = !this.IsReadOnly && (!this.CarrierId.Value.HasValue || this.CarrierId.Value == 0);
            this.IsEnabledForManualTransporterType.Value = !this.IsReadOnly && (!this.TransporterId.Value.HasValue || this.TransporterId.Value == 0);
            this.IsEnabledForPartType.Value = !this.IsReadOnly && (this.SelectedPartType == null || this.SelectedPartType.Value == PartType.SingleComponent);

            this.TabHeader.Value = string.Format(LocalizedResources.TransportCostCalculator_RouteTab, this.Index.Value);
            this.IsSettingsPopupOpen = false;
            this.UpdateCosts();
        }

        /// <summary>
        /// Initializes the settings view model for the current calculation and registers the undo actions for its properties.
        /// </summary>
        private void InitializeSettingsViewModel()
        {
            // Create the setting view model.
            this.SettingViewModel = new TransportCostCalculationSettingViewModel(unitsService);
            this.SettingViewModel.IsReadOnly = this.IsReadOnly;
            this.SettingViewModel.IsInViewerMode = this.IsInViewerMode;
            this.SettingViewModel.DataSourceManager = this.DataSourceManager;
            this.SettingViewModel.UndoManager = this.UndoManager;
            this.SettingViewModel.Model = this.Model.TransportCostCalculationSetting;
            this.SettingViewModel.CloseSettingsPopupCommand = new DelegateCommand(() => this.IsSettingsPopupOpen = false);

            // Register property action in undo manager for the settings properties.
            this.UndoManager.RegisterPropertyAction(() => this.SettingViewModel.DaysPerYear, this.HandleUndo, this.SettingViewModel, true);
            this.UndoManager.RegisterPropertyAction(() => this.SettingViewModel.AverageLoadTarget, this.HandleUndo, this.SettingViewModel, true);
            this.UndoManager.RegisterPropertyAction(() => this.SettingViewModel.TruckTrailerCost, this.HandleUndo, this.SettingViewModel, true);
            this.UndoManager.RegisterPropertyAction(() => this.SettingViewModel.MegaTrailerCost, this.HandleUndo, this.SettingViewModel, true);
            this.UndoManager.RegisterPropertyAction(() => this.SettingViewModel.Truck7tCost, this.HandleUndo, this.SettingViewModel, true);
            this.UndoManager.RegisterPropertyAction(() => this.SettingViewModel.Truck3_5tCost, this.HandleUndo, this.SettingViewModel, true);
            this.UndoManager.RegisterPropertyAction(() => this.SettingViewModel.VanTypeCost, this.HandleUndo, this.SettingViewModel, true);
        }

        /// <summary>
        /// Called when the data source manager has changed.
        /// </summary>
        protected override void OnDataSourceManagerChanged()
        {
            base.OnDataSourceManagerChanged();

            this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(this.DataSourceManager);
        }

        /// <summary>
        /// Registers the value changed event for some properties.
        /// </summary>
        private void RegisterValueChangedEvent()
        {
            this.PartWeight.ValueChanged += (s, e) => this.UpdateCosts();
            this.Distance.ValueChanged += (s, e) => this.UpdateCosts();
            this.DailyNeededQuantity.ValueChanged += (s, e) => this.UpdateCosts();
            this.CostByPackage.ValueChanged += (s, e) => this.UpdateCosts();
            this.PackageWeight.ValueChanged += (s, e) => this.UpdateCosts();
            this.BouncingBoxLength.ValueChanged += (s, e) => this.UpdateCosts();
            this.Interference.ValueChanged += (s, e) => this.UpdateCosts();
            this.BouncingBoxWidth.ValueChanged += (s, e) => this.UpdateCosts();
            this.BouncingBoxHeight.ValueChanged += (s, e) => this.UpdateCosts();
            this.PackingDensity.ValueChanged += (s, e) => this.UpdateCosts();
            this.CarrierLength.ValueChanged += (s, e) => this.UpdateCosts();
            this.CarrierHeight.ValueChanged += (s, e) => this.UpdateCosts();
            this.CarrierWidth.ValueChanged += (s, e) => this.UpdateCosts();
            this.NetWeight.ValueChanged += (s, e) => this.UpdateCosts();
            this.MaxPackageCarrier.ValueChanged += (s, e) => this.UpdateCosts();
            this.TransporterLength.ValueChanged += (s, e) => this.UpdateCosts();
            this.TransporterWidth.ValueChanged += (s, e) => this.UpdateCosts();
            this.TransporterHeight.ValueChanged += (s, e) => this.UpdateCosts();
            this.TransporterCostPerKm.ValueChanged += (s, e) => this.UpdateCosts();
            this.SelectedPackagingType.ValueChanged += (s, e) => this.UpdateCosts();
            this.SettingViewModel.DaysPerYear.ValueChanged += (s, e) => this.UpdateCosts();
            this.SelectedPartType.ValueChanged += (s, e) => this.OnSelectedPartTypeChanged();
            this.CarrierId.ValueChanged += (s, e) => this.OnSelectedCarrierChanged();
            this.TransporterId.ValueChanged += (s, e) => this.OnSelectedTransporterChanged();
            this.Index.ValueChanged += (s, e) => this.OnIndexChanged();

            // This is a workaround to validate the field containing the MaxLoadCapacity in GUI.
            this.TotalWeight.ValueChanged += (s, e) => this.OnPropertyChanged(() => this.MaxLoadCapacity);
        }

        /// <summary>
        /// Handles the ValueChanged event of the route's index.
        /// </summary>
        private void OnIndexChanged()
        {
            this.TabHeader.Value = string.Format(LocalizedResources.TransportCostCalculator_RouteTab, this.Index.Value);
        }

        /// <summary>
        /// Handles the ValueChanged event of the selected carrier.
        /// </summary>
        private void OnSelectedCarrierChanged()
        {
            using (this.UndoManager.StartBatch(true, true, navigateToBatchControls: true, undoEachBatch: true))
            {
                if (this.CarrierId.Value.HasValue && this.CarrierId.Value != 0)
                {
                    Carrier selectedCarrier = this.Carriers.FirstOrDefault(it => it.Id == this.CarrierId.Value);
                    if (selectedCarrier != null)
                    {
                        // If the selected carrier type is not Manual, set the default values.
                        this.CarrierLength.Value = selectedCarrier.Length;
                        this.CarrierHeight.Value = selectedCarrier.Height;
                        this.CarrierWidth.Value = selectedCarrier.Width;
                        this.NetWeight.Value = selectedCarrier.NetWeight;

                        this.IsEnabledForManualCarrierType.Value = false;
                    }
                }
                else
                {
                    // If the selected carrier type is Manual, set the default values to 0.
                    this.CarrierLength.Value = 0m;
                    this.CarrierHeight.Value = 0m;
                    this.CarrierWidth.Value = 0m;
                    this.NetWeight.Value = 0m;

                    this.IsEnabledForManualCarrierType.Value = true;
                }
            }
        }

        /// <summary>
        /// Handles the ValueChanged event of the selected transporter.
        /// </summary>
        private void OnSelectedTransporterChanged()
        {
            using (this.UndoManager.StartBatch(true, true, navigateToBatchControls: true, undoEachBatch: true))
            {
                if (this.TransporterId.Value.HasValue && this.TransporterId.Value != 0)
                {
                    Transporter selectedTransporter = this.Transporters.FirstOrDefault(it => it.Id == this.TransporterId.Value);
                    if (selectedTransporter != null)
                    {
                        // If the selected transporter type is not Manual, set the default values.
                        this.TransporterHeight.Value = selectedTransporter.Height;
                        this.TransporterLength.Value = selectedTransporter.Length;
                        this.TransporterWidth.Value = selectedTransporter.Width;
                        this.MaxLoadCapacity.Value = selectedTransporter.MaxLoadCapacity;

                        this.IsEnabledForManualTransporterType.Value = false;
                    }

                    this.SetTransporterCostPerKmValue();
                }
                else
                {
                    // If the selected carrier type is Manual, set the default values to 0.
                    this.TransporterHeight.Value = 0m;
                    this.TransporterLength.Value = 0m;
                    this.TransporterWidth.Value = 0m;
                    this.TransporterCostPerKm.Value = 0m;
                    this.MaxLoadCapacity.Value = 0m;

                    this.IsEnabledForManualTransporterType.Value = true;
                }
            }
        }

        /// <summary>
        /// Sets the value of transporter cost per km, depending on the selected transported type.
        /// </summary>
        private void SetTransporterCostPerKmValue()
        {
            switch (this.TransporterId.Value)
            {
                case 1:
                    this.TransporterCostPerKm.Value = this.SettingViewModel.TruckTrailerCost.Value.HasValue ? this.SettingViewModel.TruckTrailerCost.Value.Value : 0;
                    break;

                case 2:
                    this.TransporterCostPerKm.Value = this.SettingViewModel.MegaTrailerCost.Value.HasValue ? this.SettingViewModel.MegaTrailerCost.Value.Value : 0;
                    break;

                case 3:
                    this.TransporterCostPerKm.Value = this.SettingViewModel.Truck7tCost.Value.HasValue ? this.SettingViewModel.Truck7tCost.Value.Value : 0;
                    break;

                case 4:
                    this.TransporterCostPerKm.Value = this.SettingViewModel.Truck3_5tCost.Value.HasValue ? this.SettingViewModel.Truck3_5tCost.Value.Value : 0;
                    break;

                case 5:
                    this.TransporterCostPerKm.Value = this.SettingViewModel.VanTypeCost.Value.HasValue ? this.SettingViewModel.VanTypeCost.Value.Value : 0;
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// Called when the SelectedPartType property has changed.
        /// </summary>
        private void OnSelectedPartTypeChanged()
        {
            using (this.UndoManager.StartBatch(true, true, navigateToBatchControls: true, undoEachBatch: true))
            {
                if (!this.IsReadOnly && this.SelectedPartType != null)
                {
                    if (this.SelectedPartType.Value == PartType.SingleComponent)
                    {
                        // If the SelectedPartType is SingleComponent, the interference value must be set by the user.
                        // The packing density is set to a default value.
                        this.Interference.Value = null;
                        this.PackingDensity.Value = 0m;

                        this.IsEnabledForPartType.Value = true;
                    }
                    else if (this.SelectedPartType.Value == PartType.BulkGoods)
                    {
                        // If the SelectedPartType is BulkGoods, the packing density value must be set by the user.
                        // The interference is set to a default value.
                        this.Interference.Value = 0m;
                        this.PackingDensity.Value = null;

                        this.IsEnabledForPartType.Value = false;
                    }
                }
            }
        }

        /// <summary>
        /// Opens the master data browser to select a Supplier.
        /// </summary>
        private void BrowseMasterData()
        {
            if (!string.IsNullOrEmpty(this.PartCountry.Value))
            {
                if (this.supplierBrowser == null)
                {
                    this.supplierBrowser = new CountryAndSupplierBrowserViewModel(windowService);
                }

                // Check if the country name is not changed.
                string countryName = this.PartCountry.Value;
                Guid? countryId = this.PartCountryId.Value;
                var country = this.DataSourceManager.CountryRepository.ResolveCountry(countryId ?? Guid.Empty, countryName);
                if (country != null && !countryName.Equals(country.Name))
                {
                    this.supplierBrowser.CountryName = country.Name;
                }
                else
                {
                    this.supplierBrowser.CountryName = this.PartCountry.Value;
                }

                this.supplierBrowser.LoadLocation = this.DataSourceManager.DatabaseId;
                this.supplierBrowser.CountryOrSupplierSelected += this.OnCountryOrSupplierSelected;
                this.windowService.ShowViewInDialog(this.supplierBrowser);
                this.supplierBrowser.CountryOrSupplierSelected -= this.OnCountryOrSupplierSelected;
            }
            else
            {
                this.windowService.MessageDialogService.Show(LocalizedResources.TransportCostCalculator_ParentWithNoCountry, MessageDialogType.Error);
            }
        }

        /// <summary>
        /// Called when a supplier is selected from the country browser.
        /// </summary>
        /// <param name="masterDataEntity">The master data entity selected in the browser.</param>
        /// <param name="databaseId">The source database of the selected master data.</param>
        private void OnCountryOrSupplierSelected(object masterDataEntity, DbIdentifier databaseId)
        {
            var supplier = masterDataEntity as CountryState;
            if (supplier != null)
            {
                this.Supplier.Value = supplier.Name;
            }
        }

        /// <summary>
        /// Determines whether this instance can calculate the driving distance.
        /// </summary>
        /// <returns> True if this instance can calculate the driving distance; otherwise, false. </returns>
        private bool CanCalculateDrivingDistance()
        {
            if (this.SourceLatitude.Value == null || !string.IsNullOrWhiteSpace(this.SourceLatitude.Error)
                || this.SourceLongitude.Value == null || !string.IsNullOrWhiteSpace(this.SourceLongitude.Error)
                || this.DestinationLatitude.Value == null || !string.IsNullOrWhiteSpace(this.DestinationLatitude.Error)
                || this.DestinationLongitude.Value == null || !string.IsNullOrWhiteSpace(this.DestinationLongitude.Error))
            {
                // If the coordinates for source and destination are incorrect, the user can not calculate the driving distance.
                return false;
            }

            return true;
        }

        /// <summary>
        /// Calculates the driving distance between the coordinates of source and destination.
        /// If the user wants to view the map window, a new window will be opened to display the route.
        /// </summary>
        private void CalculateDrivingDistance()
        {
            if (UserSettingsManager.Instance.ShowMapWindow)
            {
                // Open a new window to display the route.
                var drivingRouteVM = this.compositionContainer.GetExportedValue<TransportCostCalculationDrivingRouteViewModel>();

                drivingRouteVM.SourceLatitude = this.SourceLatitude.Value.GetValueOrDefault();
                drivingRouteVM.SourceLongitude = this.SourceLongitude.Value.GetValueOrDefault();
                drivingRouteVM.DestinationLatitude = this.DestinationLatitude.Value.GetValueOrDefault();
                drivingRouteVM.DestinationLongitude = this.DestinationLongitude.Value.GetValueOrDefault();

                this.windowService.ShowViewInDialog(drivingRouteVM, "TransportCostCalculationDrivingRouteTemplate");
                if (drivingRouteVM.RouteInfo != null)
                {
                    // Get the distance calculated in driving route view model.
                    this.Distance.Value = drivingRouteVM.RouteInfo.Distance;
                }
            }
            else
            {
                System.Threading.Tasks.Task.Factory.StartNew(() =>
                {
                    // Calculate only the distance, without showing the map.
                    this.IsCalculatingDistance.Value = true;
                    var drivingRouteVM = new TransportCostCalculationDrivingRouteViewModel(windowService, messenger, pleaseWaitService);

                    drivingRouteVM.SourceLatitude = this.SourceLatitude.Value.GetValueOrDefault();
                    drivingRouteVM.SourceLongitude = this.SourceLongitude.Value.GetValueOrDefault();
                    drivingRouteVM.DestinationLatitude = this.DestinationLatitude.Value.GetValueOrDefault();
                    drivingRouteVM.DestinationLongitude = this.DestinationLongitude.Value.GetValueOrDefault();

                    var routeInfo = drivingRouteVM.GetRouteDirections();
                    this.IsCalculatingDistance.Value = false;

                    if (!routeInfo.HasError)
                    {
                        // If there is no error occurred during distance calculation, the value is set.
                        this.Distance.Value = routeInfo.Distance;
                    }
                    else
                    {
                        // Else, the error message is displayed.
                        this.IsRouteErrorPopupOpen = true;
                        this.RouteErrorMessage.Value = routeInfo.ErrorMessage;
                    }
                });
            }
        }

        /// <summary>
        /// Updates all the specific costs for a route.
        /// </summary>
        private void UpdateCosts()
        {
            using (this.UndoManager.Pause())
            {
                TransportCostCalculationResult result = this.CalculateCosts();

                this.AnnualQty.Value = result.AnnualQty;
                this.CarrierVolume.Value = result.CarrierVolume;
                this.BouncingBoxVolume.Value = result.BouncingBoxVolume;
                this.PartsPerCarrier.Value = result.PartsPerCarrier;
                this.PackageCarrierPerPackaging.Value = result.PackageCarrierPerPackaging;
                this.AnnualNumberOfPackageCarrier.Value = result.AnnualNumberOfPackageCarrier;
                this.Maxtheo.Value = result.Maxtheo;
                this.NeededTransportsForRelease.Value = result.NeededTransportsForRelease;
                this.AnnualNumberOfTransports.Value = result.AnnualNumberOfTransports;
                this.FillingDegreeTransporter.Value = result.FillingDegreeTransporter;
                this.CarrierWeight.Value = result.CarrierWeight;
                this.PartsWeight.Value = result.PartsWeight;
                this.TotalWeight.Value = result.TotalWeight;
                this.PackagingCost.Value = result.PackagingCost;
                this.PackagingCostPerPcs.Value = result.PackagingCostPerPcs;
                this.TransportCost.Value = result.TransportCost;
                this.TransportCostPerPcs.Value = result.TransportCostPerPcs;
                this.TransporterVolume.Value = result.TransporterVolume;
            }

            // Send a notification to the calculator, to update the total costs.
            this.messenger.Send<NotificationMessage>(new NotificationMessage("UpdateTotalCosts"));
        }

        /// <summary>
        /// Calculates the route's costs.
        /// </summary>
        /// <returns>The route's costs.</returns>
        private TransportCostCalculationResult CalculateCosts()
        {
            TransportCostCalculationInputData inputData = new TransportCostCalculationInputData();

            inputData.PartWeight = this.PartWeight.Value.GetValueOrDefault();
            inputData.Distance = this.Distance.Value.GetValueOrDefault();
            inputData.DaysPerYear = this.SettingViewModel.DaysPerYear.Value.GetValueOrDefault();
            inputData.DailyNeededQty = this.DailyNeededQuantity.Value.GetValueOrDefault();
            inputData.PartType = this.SelectedPartType.Value;
            inputData.PackagingType = this.SelectedPackagingType.Value;
            inputData.CostByPackage = this.CostByPackage.Value.GetValueOrDefault();
            inputData.PackageWeight = this.PackageWeight.Value.GetValueOrDefault();
            inputData.BouncingBoxLength = this.BouncingBoxLength.Value.GetValueOrDefault();
            inputData.Interference = this.Interference.Value.GetValueOrDefault();
            inputData.BouncingBoxWidth = this.BouncingBoxWidth.Value.GetValueOrDefault();
            inputData.BouncingBoxHeight = this.BouncingBoxHeight.Value.GetValueOrDefault();
            inputData.PackingDensity = this.PackingDensity.Value.GetValueOrDefault();
            inputData.CarrierLength = this.CarrierLength.Value.GetValueOrDefault();
            inputData.CarrierHeight = this.CarrierHeight.Value.GetValueOrDefault();
            inputData.CarrierWidth = this.CarrierWidth.Value.GetValueOrDefault();
            inputData.NetWeight = this.NetWeight.Value.GetValueOrDefault();
            inputData.MaxPackageCarrier = this.MaxPackageCarrier.Value.GetValueOrDefault();
            inputData.TransporterLength = this.TransporterLength.Value.GetValueOrDefault();
            inputData.TransporterWidth = this.TransporterWidth.Value.GetValueOrDefault();
            inputData.TransporterHeight = this.TransporterHeight.Value.GetValueOrDefault();
            inputData.CostPerKm = this.TransporterCostPerKm.Value.GetValueOrDefault();

            TransportCostCalculator calculator = new TransportCostCalculator();
            return calculator.CalculateTransportCost(inputData);
        }

        /// <summary>
        /// Handles the error from setting view model, because the parent view model does not automatically handle them.
        /// The error messages from setting view model are appended to the error messages from the parent calculation view model.
        /// </summary>
        private void HandleSettingsViewModelsError()
        {
            if (!string.IsNullOrWhiteSpace(this.SettingViewModel.Error) && !this.Error.Contains(this.SettingViewModel.Error))
            {
                // If the error messages from setting view model do not exist in the parent view model, they are added.
                if (string.IsNullOrWhiteSpace(this.Error))
                {
                    this.Error = this.SettingViewModel.Error;
                }
                else if (this.Error.Contains("- "))
                {
                    if (this.SettingViewModel.Error.Contains("- "))
                    {
                        this.Error = string.Join(Environment.NewLine, this.Error, this.SettingViewModel.Error);
                    }
                    else
                    {
                        this.Error = string.Join(Environment.NewLine + "- ", this.Error, this.SettingViewModel.Error);
                    }
                }
                else
                {
                    this.Error = this.Error.Insert(0, "- ");
                }
            }
        }

        /// <summary>
        /// Determines whether this instance can perform the save operation. Executed by the SaveCommand.
        /// </summary>
        /// <returns>
        /// True if the save operation can be performed, false otherwise.
        /// </returns>
        protected override bool CanSave()
        {
            return base.CanSave() && string.IsNullOrWhiteSpace(this.SettingViewModel.Error);
        }

        /// <summary>
        /// Performs the save operation. Executed by the SaveCommand.
        /// </summary>
        protected override void Save()
        {
            this.CheckModelAndDataSource();

            // Save all changes back into the Model objects. The validity check of this operation is performed by the CanSave method.
            this.SaveToModel();

            this.DataSourceManager.TransportCostCalculationRepository.Save(this.Model);
        }

        /// <summary>
        /// Handle the Undo on properties from Settings ViewModel.
        /// </summary>
        /// <param name="undoItem">The undoable item.</param>
        private void HandleUndo(UndoableItem undoItem)
        {
            if (undoItem == null)
            {
                return;
            }

            // Open the settings popup when the undo action is performed on settings.
            this.IsSettingsPopupOpen = true;
        }

        #region Inner Classes

        /// <summary>
        /// Maximum Load Weight Validator inner class.
        /// </summary>
        private class WeightValidator : ValidationAttribute
        {
            /// <summary>
            /// Validates the specified value with respect to the current validation attribute.
            /// It this particular case, the maximum load weight is compared with the total load weight.
            /// If the maximum weight is smaller than the total weight, the field is not valid.
            /// </summary>
            /// <param name="value">The value to validate.</param>
            /// <param name="validationContext">The context information about the validation operation.</param>
            /// <returns>
            /// An instance of the <see cref="T:System.ComponentModel.DataAnnotations.ValidationResult"/> class.
            /// </returns>
            protected override ValidationResult IsValid(object value, ValidationContext validationContext)
            {
                object viewModel;
                validationContext.Items.TryGetValue("ViewModel", out viewModel);

                bool isValid = true;
                var maxWeightValue = value as decimal?;
                var calculationVM = viewModel as TransportCostCalculationViewModel;

                if (maxWeightValue != null && calculationVM != null)
                {
                    var totalWeight = calculationVM.TotalWeight;
                    if (maxWeightValue < totalWeight.Value)
                    {
                        isValid = false;
                    }
                }

                return isValid ? ValidationResult.Success : new ValidationResult(LocalizedResources.TransportCostCalculator_InvalidTotalWeight);
            }
        }

        #endregion Inner Classes
    }
}