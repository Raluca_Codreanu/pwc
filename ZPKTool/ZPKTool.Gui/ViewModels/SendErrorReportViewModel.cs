﻿using System;
using System.ComponentModel.Composition;
using System.IO;
using System.Windows.Input;
using Ionic.Zip;
using ZPKTool.Business;
using ZPKTool.Common.Mapi;
using ZPKTool.Gui.Resources;
using ZPKTool.LicenseValidator;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the SendErrorReportView view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class SendErrorReportViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The text written in the technical details section.
        /// </summary>
        private string technicalDetailsText;

        /// <summary>
        /// The text written by the user.
        /// </summary>
        private string userFeedbackText;

        /// <summary>
        /// A value indicating whether an internal error exists or not.
        /// </summary>
        private bool internalErrorExists;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="SendErrorReportViewModel"/> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        /// <param name="ex">The exception for which to send the error report.</param>
        [ImportingConstructor]
        public SendErrorReportViewModel(IWindowService windowService, Exception ex)
        {
            this.windowService = windowService;

            this.CloseScreen = new DelegateCommand(CloseScreenAction);
            this.SubmitReport = new DelegateCommand(SubmitReportAction, CanSubmitReport);

            if (ex != null)
            {
                this.TechnicalDetailsText = ex.ToString();
                this.InternalErrorExists = true;
            }
            else
            {
                this.InternalErrorExists = false;
            }
        }

        #region Commands

        /// <summary>
        /// Gets the close screen command
        /// </summary>
        public ICommand CloseScreen { get; private set; }

        /// <summary>
        /// Gets the submit report command
        /// </summary>
        public ICommand SubmitReport { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets or sets the text written in the technical details section.
        /// </summary>
        public string TechnicalDetailsText
        {
            get { return this.technicalDetailsText; }
            set { this.SetProperty(ref this.technicalDetailsText, value, () => this.TechnicalDetailsText); }
        }

        /// <summary>
        /// Gets or sets the text written by the user.
        /// </summary>
        public string UserFeedbackText
        {
            get { return this.userFeedbackText; }
            set { this.SetProperty(ref this.userFeedbackText, value, () => this.UserFeedbackText); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether an internal error exists or not.
        /// </summary>
        public bool InternalErrorExists
        {
            get { return this.internalErrorExists; }
            set { this.SetProperty(ref this.internalErrorExists, value, () => this.InternalErrorExists); }
        }

        #endregion Properties

        #region Actions

        /// <summary>
        /// Closes the screen
        /// </summary>
        private void CloseScreenAction()
        {
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Builds the first part of the mail
        /// </summary>
        /// <returns>A string containing information about the license.</returns>
        private string GetUserInfo()
        {
            string result = string.Empty;

            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            object[] attributes = assembly.GetCustomAttributes(false);
            Version version = assembly.GetName().Version;

            try
            {
                LicenseValidation validator = new LicenseValidation(SecurityManager.LicensePublicKey, SecurityManager.LicenseFilePath);
                LicenseInfo licenseInfo = validator.ValidateLicense(version);

                result += LocalizedResources.License_User + ": " + licenseInfo.UserName + Environment.NewLine;
                result += LocalizedResources.General_Email + ": " + licenseInfo.UserEmail + Environment.NewLine;
                result += LocalizedResources.License_Company + ": " + licenseInfo.CompanyName + Environment.NewLine;
                result += LocalizedResources.License_SerialNumber + ": " + licenseInfo.SerialNumber + Environment.NewLine;
                result += LocalizedResources.License_ApplicationVersion + ": " + version;
            }
            catch (LicenseException ex)
            {
                log.ErrorException("License validation failed at error report sending", ex);
            }

            return result;
        }

        /// <summary>
        /// Determines if the error report can be sent or not.
        /// </summary>
        /// <returns>True if the user can send the error report; false, otherwise.</returns>
        private bool CanSubmitReport()
        {
            if (!this.InternalErrorExists && string.IsNullOrWhiteSpace(this.UserFeedbackText))
            {
                // The error report can not be sent if there is no user feedback and no internal error occurred.
                return false;
            }

            return true;
        }

        /// <summary>
        /// Submits the report via email or trac
        /// </summary>
        private void SubmitReportAction()
        {
            string zipPath = Path.GetTempPath() + "PcmLog";
            if (File.Exists(zipPath + ".zip"))
            {
                int index = 1;
                while (File.Exists(zipPath + "-" + index + ".zip"))
                {
                    index++;
                }

                zipPath += "-" + index + ".zip";
            }
            else
            {
                zipPath += ".zip";
            }

            try
            {
                using (ZipFile zip = new ZipFile())
                {
                    string filePath = Utils.UIUtils.GetLogFilePath();
                    if (!string.IsNullOrEmpty(filePath))
                    {
                        zip.CompressionLevel = Ionic.Zlib.CompressionLevel.Level9;
                        zip.AddFile(filePath, ".");
                        zip.Save(zipPath);
                    }
                }

                // Send the error report with the log file attached.
                SendReportByEmail(zipPath);
            }
            finally
            {
                try
                {
                    File.Delete(zipPath);
                }
                catch
                {
                }

                this.CloseScreenAction();
            }
        }

        /// <summary>
        /// Sends the error report via email
        /// </summary>
        /// <param name="attachmentPath">The file to attach to the email.</param>
        private void SendReportByEmail(string attachmentPath)
        {
            try
            {
                var dbGlobalSettingsMgr = new DbGlobalSettingsManager(DataAccess.DbIdentifier.LocalDatabase);
                var dbGlobalSettings = dbGlobalSettingsMgr.Get();
                string destinationAddress = dbGlobalSettings.ContactEmail;

                if (destinationAddress == null && string.IsNullOrEmpty(destinationAddress))
                {
                    this.windowService.MessageDialogService.Show(LocalizedResources.ErrorReport_ReportSendingFailed, MessageDialogType.Error);
                    return;
                }

                string subject = LocalizedResources.ErrorReport_ErrorReport;
                string body = LocalizedResources.General_LicenseInfo + Environment.NewLine + GetUserInfo() + Environment.NewLine + Environment.NewLine +
                    LocalizedResources.ErrorReport_UserInput + Environment.NewLine + this.UserFeedbackText + Environment.NewLine + Environment.NewLine +
                    LocalizedResources.ErrorReport_TechnicalDetails + Environment.NewLine + this.TechnicalDetailsText;

                MapiMail mail = new MapiMail();
                mail.Subject = subject;
                mail.Body = body;
                mail.Recipients.Add(new MapiRecipient(destinationAddress, MapiRecipientType.To));

                if (!string.IsNullOrEmpty(attachmentPath))
                {
                    mail.Attachments.Add(new MapiAttachment(attachmentPath));
                }

                MapiMailer mailer = new MapiMailer();
                mailer.SendMail(mail);
            }
            catch (MapiException ex)
            {
                log.ErrorException("Sending the error report email failed.", ex);
                this.windowService.MessageDialogService.Show(LocalizedResources.ErrorReport_ReportSendingFailed, MessageDialogType.Error);
            }
            catch (Exception ex)
            {
                log.ErrorException("Error occurred while creating or sending report", ex);
                this.windowService.MessageDialogService.Show(LocalizedResources.ErrorReport_ErrorSending, MessageDialogType.Error);
            }
        }

        #endregion
    }
}