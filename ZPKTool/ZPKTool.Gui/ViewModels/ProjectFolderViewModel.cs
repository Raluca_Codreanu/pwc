﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Input;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for ...
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ProjectFolderViewModel : ViewModel<ProjectFolder, IDataSourceManager>
    {
        #region Attributes

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        #endregion Attributes

        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectFolderViewModel"/> class.
        /// </summary>
        /// <param name="messenger">The messenger service.</param>
        /// <param name="windowService">The window service.</param>
        [ImportingConstructor]
        public ProjectFolderViewModel(IMessenger messenger, IWindowService windowService)
        {
            this.messenger = messenger;
            this.windowService = windowService;
        }

        #region Properties

        /// <summary>
        /// Gets the name of the folder.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Name", ErrorMessageResourceType = typeof(LocalizedResources))]
        [ExposesModelProperty("Name")]
        public DataProperty<string> Name { get; private set; }

        #endregion Properties

        /// <summary>
        /// Initializes the view-model for the creation of a new folder.
        /// The ModelDataContext property must be set to a valid value before this call.
        /// </summary>
        /// <param name="parent">The parent of the folder that will be created. If is set to null the project will be created in My Projects.</param>
        /// <param name="isReleased">if set to true the new folder will be initialized as released; otherwise it is initialized as not released.</param>
        /// <remarks>
        /// This is a helper method for populating the model with default data for creation. It is not mandatory to use it; you can set the model to a new
        /// instance set up however you want.
        /// </remarks>
        public void InitializeForCreation(ProjectFolder parent, bool isReleased = false)
        {
            this.CheckDataSource();
            this.EditMode = ViewModelEditMode.Create;

            ProjectFolder folder = new ProjectFolder();
            folder.ParentProjectFolder = parent;
            folder.IsReleased = isReleased;
            if (parent != null)
            {
                folder.IsOffline = parent.IsOffline;
                folder.SetOwner(parent.Owner);
            }

            this.Model = folder;
        }

        /// <summary>
        /// Called when the Model property has changed.
        /// </summary>
        protected override void OnModelChanged()
        {
            this.CheckDataSource();
            base.OnModelChanged();
        }

        /// <summary>
        /// Submit all form changes.
        /// </summary>
        protected override void Save()
        {
            this.CheckModelAndDataSource();

            this.SaveToModel();

            if (!this.Model.IsReleased && this.Model.Owner == null)
            {
                User loggedInUser = this.DataSourceManager.UserRepository.GetByKey(SecurityManager.Instance.CurrentUser.Guid);
                this.Model.SetOwner(loggedInUser);
            }

            this.DataSourceManager.ProjectFolderRepository.Save(this.Model);
            this.DataSourceManager.SaveChanges();

            EntityChangeType notificationType = this.EditMode == ViewModelEditMode.Create ? EntityChangeType.EntityCreated : EntityChangeType.EntityUpdated;
            EntityChangedMessage msg =
                new EntityChangedMessage(Notification.MyProjectsEntityChanged, this.Model, this.Model.ParentProjectFolder, notificationType);
            this.messenger.Send(msg);
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Cancel all form changes.
        /// </summary>
        protected override void Cancel()
        {
            base.Cancel();
            this.windowService.CloseViewWindow(this);
        }
    }
}