﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using ZPKTool.Business;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Common;
using ZPKTool.Controls;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;
using ZPKTool.Synchronization;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the ShellHeader view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ShellHeaderViewModel : ViewModel
    {
        #region Constants

        /// <summary>
        /// The reminder max limit before notify.
        /// </summary>
        public const int ReminderMaxLimit = 10000;

        /// <summary>
        /// The initial time required until the start of the automatic synchronization [specified in minutes].
        /// </summary>
        public const int AutoSyncDelayTime = 5;

        /// <summary>
        /// The color used to indicate that there is a large number of unsynchronized Project items, and the synchronization must be performed as soon as posible.
        /// </summary>
        public readonly Color SyncRequired = Colors.Red;

        /// <summary>
        /// The color used to indicate that synchronization has been completed successfully.
        /// </summary>
        public readonly Color SyncSuccess = Colors.Green;

        /// <summary>
        /// The color used to indicate that some conflicts occurred during data synchronization.
        /// </summary>
        public readonly Color SyncConflicts = (Color)ColorConverter.ConvertFromString("#ffffcc00");

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        #endregion Constants

        #region Attributes

        /// <summary>
        /// The composition container.
        /// </summary>
        private readonly CompositionContainer container;

        /// <summary>
        /// The window service.
        /// </summary>
        private readonly IWindowService windowService;

        /// <summary>
        /// The synchronization service.
        /// </summary>
        private readonly ISyncService syncService;

        /// <summary>
        /// The units service.
        /// </summary>
        private readonly IUnitsService unitsService;

        /// <summary>
        /// The icon representing the online status.
        /// </summary>
        private ImageSource onlineStatusIcon;

        /// <summary>
        /// The text explaining the online status.
        /// </summary>
        private string onlineStatusText;

        /// <summary>
        /// The tooltip giving more details about the online status.
        /// </summary>
        private string onlineStatusTooltip;

        /// <summary>
        /// The selected UI currency.
        /// </summary>
        private Currency selectedUICurrency;

        /// <summary>
        /// The UI currencies.
        /// </summary>
        private DispatchedObservableCollection<Currency> uiCurrencies;

        /// <summary>
        /// Indicates whether the UICurrency selection should be enabled.
        /// </summary>
        private bool canSelectUICurrency;

        /// <summary>
        /// The entity name for which the calculation result was made.
        /// </summary>
        private string entityName;

        /// <summary>
        /// A string describing the logged in user.
        /// </summary>
        private string loggedInAs;

        /// <summary>
        /// The weight.
        /// </summary>
        private decimal? weight;

        /// <summary>
        /// The investment.
        /// </summary>
        private decimal? investment;

        /// <summary>
        /// The cost calculation result.
        /// </summary>
        private decimal? cost;

        /// <summary>
        /// The purchase price
        /// </summary>
        private decimal? purchasePrice;

        /// <summary>
        /// The target price
        /// </summary>
        private decimal? targetPrice;

        /// <summary>
        /// A value indicating whether the weight is displayed.
        /// </summary>
        private Visibility weightVisibility;

        /// <summary>
        /// A value indicating whether to display the investment.
        /// </summary>
        private Visibility investmentVisibility;

        /// <summary>
        /// A value indicating whether to display the cost.
        /// </summary>
        private Visibility costVisibility;

        /// <summary>
        /// A value indicating whether to display the purchase price.
        /// </summary>
        private Visibility purchasePriceVisibility;

        /// <summary>
        /// A value indicating whether to display the target price.
        /// </summary>
        private Visibility targetPriceVisibility;

        /// <summary>
        /// A value indicating whether to display the update available popup in the screen header
        /// </summary>
        private bool showUpdateAvailable = false;

        /// <summary>
        /// Weak event listener for the PropertyChanged notification
        /// </summary>
        private WeakEventListener<PropertyChangedEventArgs> propertyChangedListener;

        /// <summary>
        /// The GoHome command.
        /// </summary>
        private ICommand goHomeCommand;

        /// <summary>
        /// The current status of the UI currency.
        /// </summary>
        private UICurrencyStatus crtUICurrencyStatus;

        /// <summary>
        /// This flag is used to avoid saving the UI Currency when it was not changed by the user's 
        /// interaction with the UI Currency selector (when setting it from code).
        /// </summary>
        private volatile bool suspendUICurrencySave = false;

        /// <summary>
        /// Indicates whether the Sync reminder is visible or not.
        /// </summary>
        private bool isSyncReminderVisible;

        /// <summary>
        /// The measurement units adapter.
        /// </summary>
        private UnitsAdapter measurementUnitsAdapter;

        /// <summary>
        /// The timer used to handle synchronization postpone.
        /// </summary>
        private DispatcherTimer syncDelayTimer;

        /// <summary>
        /// A value indicating whether to show the auto sync popup or not.
        /// </summary>
        private bool showAutoSyncPopup;

        /// <summary>
        /// The time left until the start of synchronization.
        /// </summary>
        private string syncStartTimeRemaining;

        /// <summary>
        /// The time when the sync popup has opened.
        /// </summary>
        private DateTime syncTimerStartTime;

        /// <summary>
        /// The color of the synchronization image button.
        /// </summary>
        private Color syncColor;

        /// <summary>
        /// The synchronization state message displayed as tooltip for the sync button.
        /// </summary>
        private string syncStateMessage;

        /// <summary>
        /// Indicates whether the synchtonization animation is enabled or not.
        /// </summary>
        private bool syncAnimationIsEnabled;

        /// <summary>
        /// The selected synchronization postpone time. [in minutes]
        /// </summary>
        private int selectedPostponeTime;

        /// <summary>
        /// A value indicating whether the synchronization has failed or not.
        /// </summary>
        private bool syncHasFailed;

        /// <summary>
        /// The synchronization auto sync popup synchronization button name.
        /// </summary>
        private string syncActionName;

        #endregion Attributes

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ShellHeaderViewModel"/> class.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="messenger">The messenger service.</param>
        /// <param name="unitsService">The units service.</param>
        /// <param name="syncService">The synchronization service.</param>
        [ImportingConstructor]
        public ShellHeaderViewModel(
            CompositionContainer container,
            IWindowService windowService,
            IMessenger messenger,
            IUnitsService unitsService,
            ISyncService syncService)
        {
            Argument.IsNotNull("container", container);
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("unitsService", unitsService);
            Argument.IsNotNull("syncService", syncService);

            this.container = container;
            this.windowService = windowService;
            this.unitsService = unitsService;
            this.syncService = syncService;

            var onlineChecker = container.GetExportedValue<IOnlineCheckService>();

            // Initialize commands.
            this.CheckOnlineStatusCommand = new DelegateCommand(() => onlineChecker.CheckNow());
            this.HidePopup = new DelegateCommand(() => this.ShowUpdateAvailable = false);
            this.ShowUpdateWindow = new DelegateCommand(ShowUpdateWindowAction);
            this.ShowSynchronizationPopupCommand = new DelegateCommand(
                this.ShowSynchronizationPopup,
                () => this.syncService.SyncManager != null &&
                    this.syncService.SyncManager.CurrentStatus.State != SyncState.Running &&
                    this.syncService.SyncManager.CurrentStatus.State != SyncState.Aborting);
            this.SynchronizationCommand = new DelegateCommand(this.StartSynchronization);
            this.PostponeSynchronizationCommand = new DelegateCommand(this.PostponeSynchronization);
            this.ShowErrorReportCommand = new DelegateCommand(
                () => this.SendErrorReportAction(this.syncService.SyncManager.CurrentStatus.Error));

            // Initialize central server state check.
            this.SetOnlineStatus(new OnlineStateChangedMessage(onlineChecker.LastOnlineCheck));
            messenger.Register<OnlineStateChangedMessage>(msg => SetOnlineStatus(msg));

            this.UICurrencies = new DispatchedObservableCollection<Currency>();

            messenger.Register<NotificationMessage>(ReceiveMessage);
            messenger.Register<CurrentComponentCostChangedMessage>((msg) => RefreshCost(msg.Content), GlobalMessengerTokens.MainViewTargetToken);
            messenger.Register<SynchronizationStartedMessage>(this.HandleSynchronizationStartedMessage);
            messenger.Register<SynchronizationFinishedMessage>(this.HandleSynchronizationFinishedMessage);

            messenger.Register<RefreshUnitsAdapterMessage>((msg) =>
            {
                if (msg != null)
                {
                    this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(msg.DataSourceManager);
                }
            });

            // Initialize weight and investment display.
            this.ComputeHeaderInformationVisibility();
            this.propertyChangedListener = new WeakEventListener<PropertyChangedEventArgs>(Settings_PropertyChanged);
            PropertyChangedEventManager.AddListener(UserSettingsManager.Instance, propertyChangedListener, ReflectionUtils.GetPropertyName(() => UserSettingsManager.Instance.HeaderDisplayWeight));
            PropertyChangedEventManager.AddListener(UserSettingsManager.Instance, propertyChangedListener, ReflectionUtils.GetPropertyName(() => UserSettingsManager.Instance.HeaderDisplayInvestment));
            PropertyChangedEventManager.AddListener(UserSettingsManager.Instance, propertyChangedListener, ReflectionUtils.GetPropertyName(() => UserSettingsManager.Instance.HeaderDisplayCalculationResult));
            PropertyChangedEventManager.AddListener(UserSettingsManager.Instance, propertyChangedListener, ReflectionUtils.GetPropertyName(() => UserSettingsManager.Instance.HeaderDisplaysPurchasePrice));
            PropertyChangedEventManager.AddListener(UserSettingsManager.Instance, propertyChangedListener, ReflectionUtils.GetPropertyName(() => UserSettingsManager.Instance.HeaderDisplaysTargetPrice));
        }

        #endregion Constructor

        #region Enums

        /// <summary>
        /// The status of the UI Currency.
        /// </summary>
        public enum UICurrencyStatus
        {
            /// <summary>
            /// This status indicates that the UI currency is in a neutral state. 
            /// </summary>
            Normal = 0,

            /// <summary>
            /// The UI currency is being saved.
            /// </summary>
            Saving = 1,

            /// <summary>
            /// The UI currency is not set.
            /// </summary>
            NotSet = 2
        }

        #endregion Enums

        #region Commands

        /// <summary>
        /// Gets a command that checks the online status of the central server.
        /// </summary>
        public ICommand CheckOnlineStatusCommand { get; private set; }

        /// <summary>
        /// Gets or sets the GoHome command.
        /// </summary>
        public ICommand GoHomeCommand
        {
            get
            {
                return this.goHomeCommand;
            }

            set
            {
                if (this.goHomeCommand != value)
                {
                    this.goHomeCommand = value;
                    OnPropertyChanged("GoHomeCommand");
                }
            }
        }

        /// <summary>
        /// Gets the show update window command
        /// </summary>        
        public ICommand ShowUpdateWindow { get; private set; }

        /// <summary>
        /// Gets the hide popup command
        /// </summary>        
        public ICommand HidePopup { get; private set; }

        /// <summary>
        /// Gets the show synchronization popup command.
        /// </summary>
        public ICommand ShowSynchronizationPopupCommand { get; private set; }

        /// <summary>
        /// Gets the synchronization command.
        /// </summary>
        public ICommand SynchronizationCommand { get; private set; }

        /// <summary>
        /// Gets the Show error report command.
        /// </summary>
        public ICommand ShowErrorReportCommand { get; private set; }

        /// <summary>
        /// Gets the postpone synchronization command.
        /// </summary>
        public ICommand PostponeSynchronizationCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets or sets the selected synchronization postpone time. [in minutes]
        /// </summary>
        public int SelectedPostponeTime
        {
            get
            {
                return this.selectedPostponeTime;
            }

            set
            {
                if (this.selectedPostponeTime != value)
                {
                    this.selectedPostponeTime = value;
                    OnPropertyChanged("SelectedPostponeTime");
                }
            }
        }

        /// <summary>
        /// Gets or sets the postpone times to delay syncing.
        /// </summary>
        public Dictionary<int, string> PostponeTimes { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to show the auto sync popup or not.
        /// </summary>
        public bool ShowAutoSyncPopup
        {
            get
            {
                return this.showAutoSyncPopup;
            }

            set
            {
                if (this.showAutoSyncPopup != value)
                {
                    this.showAutoSyncPopup = value;
                    this.OnPropertyChanged("ShowAutoSyncPopup");
                }
            }
        }

        /// <summary>
        /// Gets or sets the entity name for which the calculation result was made.
        /// </summary>
        public string EntityName
        {
            get
            {
                return this.entityName;
            }

            set
            {
                if (this.entityName != value)
                {
                    this.entityName = value;
                    OnPropertyChanged("EntityName");
                }
            }
        }

        /// <summary>
        /// Gets or sets the icon representing the online status.
        /// </summary>
        public ImageSource OnlineStatusIcon
        {
            get
            {
                return this.onlineStatusIcon;
            }

            set
            {
                if (!Equals(this.onlineStatusIcon, value))
                {
                    this.onlineStatusIcon = value;
                    OnPropertyChanged("OnlineStatusIcon");
                }
            }
        }

        /// <summary>
        /// Gets or sets the text explaining the online status.
        /// </summary>
        public string OnlineStatusText
        {
            get
            {
                return this.onlineStatusText;
            }

            set
            {
                if (this.onlineStatusText != value)
                {
                    this.onlineStatusText = value;
                    OnPropertyChanged("OnlineStatusText");
                }
            }
        }

        /// <summary>
        /// Gets or sets the tooltip text giving more details about the online status.
        /// </summary>
        public string OnlineStatusTooltip
        {
            get { return onlineStatusTooltip; }
            set { SetProperty(ref onlineStatusTooltip, value, () => OnlineStatusTooltip); }
        }

        /// <summary>
        /// Gets or sets the currently selected UI currency.
        /// Do not set this property directly, use the SetUICurrency method.
        /// </summary>
        public Currency SelectedUICurrency
        {
            get
            {
                return this.selectedUICurrency;
            }

            set
            {
                if (this.CanSelectUICurrency && value == null)
                {
                    this.CurrenctUICurrencyStatus = UICurrencyStatus.NotSet;
                }
                else
                {
                    this.CurrenctUICurrencyStatus = UICurrencyStatus.Normal;
                }

                if (this.selectedUICurrency != value)
                {
                    this.selectedUICurrency = value;
                    this.OnPropertyChanged("SelectedUICurrency");
                    this.SaveUICurrency(value);
                }
            }
        }

        /// <summary>
        /// Gets or sets the current status of the UI currency.
        /// </summary>
        public UICurrencyStatus CurrenctUICurrencyStatus
        {
            get
            {
                return this.crtUICurrencyStatus;
            }

            set
            {
                if (this.crtUICurrencyStatus != value)
                {
                    this.crtUICurrencyStatus = value;
                    OnPropertyChanged(() => this.CurrenctUICurrencyStatus);
                }
            }
        }

        /// <summary>
        /// Gets the UI currencies.
        /// </summary>
        public DispatchedObservableCollection<Currency> UICurrencies
        {
            get
            {
                return this.uiCurrencies;
            }

            private set
            {
                if (this.uiCurrencies != value)
                {
                    this.uiCurrencies = value;
                    OnPropertyChanged("UICurrencies");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the UICurrency selection should be enabled.
        /// </summary>        
        public bool CanSelectUICurrency
        {
            get
            {
                return this.canSelectUICurrency;
            }

            set
            {
                if (this.canSelectUICurrency != value)
                {
                    this.canSelectUICurrency = value;
                    OnPropertyChanged("CanSelectUICurrency");
                }
            }
        }

        /// <summary>
        /// Gets or sets a string describing the logged in user.
        /// </summary>
        public string LoggedInAs
        {
            get
            {
                return this.loggedInAs;
            }

            set
            {
                if (this.loggedInAs != value)
                {
                    this.loggedInAs = value;
                    OnPropertyChanged("LoggedInAs");
                }
            }
        }

        /// <summary>
        /// Gets or sets the cost calculation result.
        /// </summary>        
        public decimal? Cost
        {
            get
            {
                return this.cost;
            }

            set
            {
                if (this.cost != value)
                {
                    this.cost = value;
                    OnPropertyChanged("Cost");
                }
            }
        }

        /// <summary>
        /// Gets or sets the cost visibility.
        /// </summary>
        public Visibility CostVisibility
        {
            get
            {
                return this.costVisibility;
            }

            set
            {
                if (this.costVisibility != value)
                {
                    this.costVisibility = value;
                    OnPropertyChanged("CostVisibility");
                }
            }
        }

        /// <summary>
        /// Gets or sets the weight.
        /// </summary>        
        public decimal? Weight
        {
            get
            {
                return this.weight;
            }

            set
            {
                if (this.weight != value)
                {
                    this.weight = value;
                    OnPropertyChanged("Weight");
                    ComputeHeaderInformationVisibility();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the weight is displayed.
        /// </summary>
        public Visibility WeightVisibility
        {
            get
            {
                return this.weightVisibility;
            }

            set
            {
                if (this.weightVisibility != value)
                {
                    this.weightVisibility = value;
                    OnPropertyChanged("WeightVisibility");
                }
            }
        }

        /// <summary>
        /// Gets or sets the investment.
        /// </summary>
        public decimal? Investment
        {
            get
            {
                return this.investment;
            }

            set
            {
                if (this.investment != value)
                {
                    this.investment = value;
                    OnPropertyChanged("Investment");
                    ComputeHeaderInformationVisibility();
                }
            }
        }

        /// <summary>
        /// Gets or sets the purchase price
        /// </summary>
        public decimal? PurchasePrice
        {
            get { return this.purchasePrice; }
            set { this.SetProperty(ref this.purchasePrice, value, () => this.PurchasePrice, this.ComputeHeaderInformationVisibility); }
        }

        /// <summary>
        /// Gets or sets the target price
        /// </summary>
        public decimal? TargetPrice
        {
            get { return this.targetPrice; }
            set { this.SetProperty(ref this.targetPrice, value, () => this.TargetPrice, this.ComputeHeaderInformationVisibility); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to display the purchase price.
        /// </summary>
        public Visibility PurchasePriceVisibility
        {
            get { return this.purchasePriceVisibility; }
            set { this.SetProperty(ref this.purchasePriceVisibility, value, () => this.PurchasePriceVisibility); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to display the target price.
        /// </summary>
        public Visibility TargetPriceVisibility
        {
            get { return this.targetPriceVisibility; }
            set { this.SetProperty(ref this.targetPriceVisibility, value, () => this.TargetPriceVisibility); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to display the investment.
        /// </summary>
        public Visibility InvestmentVisibility
        {
            get
            {
                return this.investmentVisibility;
            }

            set
            {
                if (this.investmentVisibility != value)
                {
                    this.investmentVisibility = value;
                    OnPropertyChanged("InvestmentVisibility");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to display the update available popup
        /// </summary>
        public bool ShowUpdateAvailable
        {
            get
            {
                return this.showUpdateAvailable;
            }

            set
            {
                if (this.showUpdateAvailable != value)
                {
                    this.showUpdateAvailable = value;
                    OnPropertyChanged("ShowUpdateAvailable");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is sync reminder visible.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is sync reminder visible; otherwise, <c>false</c>.
        /// </value>
        public bool IsSyncReminderVisible
        {
            get
            {
                return this.isSyncReminderVisible;
            }

            set
            {
                if (this.isSyncReminderVisible != value)
                {
                    this.isSyncReminderVisible = value;
                    OnPropertyChanged(() => this.IsSyncReminderVisible);
                }
            }
        }

        /// <summary>
        /// Gets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter
        {
            get { return this.measurementUnitsAdapter; }
            private set { this.SetProperty(ref this.measurementUnitsAdapter, value, () => this.MeasurementUnitsAdapter); }
        }

        /// <summary>
        /// Gets or sets the time left until the start of synchronization.
        /// </summary>
        public string SyncStartTimeRemaining
        {
            get
            {
                return this.syncStartTimeRemaining;
            }

            set
            {
                if (this.syncStartTimeRemaining != value)
                {
                    this.syncStartTimeRemaining = value;
                    OnPropertyChanged(() => this.SyncStartTimeRemaining);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the synchronization animation is enabled or not.
        /// </summary>
        public bool SyncAnimationIsEnabled
        {
            get
            {
                return this.syncAnimationIsEnabled;
            }

            set
            {
                if (this.syncAnimationIsEnabled != value)
                {
                    this.syncAnimationIsEnabled = value;
                    OnPropertyChanged(() => this.SyncAnimationIsEnabled);
                }
            }
        }

        /// <summary>
        /// Gets or sets the color of the synchronization image button.
        /// </summary>
        public Color SyncColor
        {
            get
            {
                return this.syncColor;
            }

            set
            {
                if (this.syncColor != value)
                {
                    this.syncColor = value;
                    OnPropertyChanged(() => this.SyncColor);
                }
            }
        }

        /// <summary>
        /// Gets or sets the synchronization state message displayed as tooltip for the sync button.
        /// </summary>
        public string SyncStateMessage
        {
            get
            {
                return this.syncStateMessage;
            }

            set
            {
                if (this.syncStateMessage != value)
                {
                    this.syncStateMessage = value;
                    OnPropertyChanged(() => this.SyncStateMessage);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the synchronization has failed or not.
        /// </summary>
        public bool SyncHasFailed
        {
            get
            {
                return this.syncHasFailed;
            }

            set
            {
                if (this.syncHasFailed != value)
                {
                    this.syncHasFailed = value;
                    OnPropertyChanged(() => this.SyncHasFailed);
                }
            }
        }

        /// <summary>
        /// Gets or sets the synchronization auto sync popup synchronization button name.
        /// </summary>
        public string SyncActionName
        {
            get
            {
                return this.syncActionName;
            }

            set
            {
                if (this.syncActionName != value)
                {
                    this.syncActionName = value;
                    OnPropertyChanged(() => this.SyncActionName);
                }
            }
        }

        #endregion Properties

        #region ILifeCycleManager

        /// <summary>
        /// Called before unloading the view from its parent or closing it. Returning false will cancel the view's unloading or closing.
        /// </summary>
        /// <returns>
        /// True if the unloading process should continue and false if it should be canceled.
        /// </returns>
        public override bool OnUnloading()
        {
            base.OnUnloading();

            return false;
        }

        #endregion ILifeCycleManager

        #region Commands handling

        /// <summary>
        /// Shows the update window.
        /// </summary>
        private void ShowUpdateWindowAction()
        {
            var viewModel = this.container.GetExportedValue<UpdateApplicationViewModel>();
            this.windowService.ShowViewInDialog(viewModel);
        }

        /// <summary>
        /// Shows the synchronization popup.
        /// </summary>
        private void ShowSynchronizationPopup()
        {
            if (this.SyncColor == SyncRequired)
            {
                this.ShowAutoSyncPopup = !this.ShowAutoSyncPopup;
            }
            else if (this.SyncColor == SyncSuccess)
            {
                this.IsSyncReminderVisible = false;
                this.windowService.MessageDialogService.Show(this.SyncStateMessage, MessageDialogType.Info);
            }
            else if (this.SyncColor == SyncConflicts)
            {
                var syncConflictsVM = this.container.GetExportedValue<SynchronizationConflictsViewModel>();
                this.windowService.ShowViewInDialog(syncConflictsVM);
            }
        }

        /// <summary>
        /// Postpones the synchronization start.
        /// </summary>
        private void PostponeSynchronization()
        {
            this.ShowAutoSyncPopup = false;
            this.syncTimerStartTime = DateTime.Now.AddMinutes(this.SelectedPostponeTime);
            this.syncService.PostponeSync(SecurityManager.Instance.CurrentUser.Guid, this.SelectedPostponeTime);
        }

        /// <summary>
        /// Starts the synchronization.
        /// </summary>
        private void StartSynchronization()
        {
            this.ShowAutoSyncPopup = false;
            this.syncService.StartSync(SecurityManager.Instance.CurrentUser.Guid);
        }

        /// <summary>
        /// The action performed by the SendErrorReport command.
        /// </summary>
        /// <param name="ex">The exception for which to send the error report.</param>
        private void SendErrorReportAction(Exception ex = null)
        {
            var viewModel = new SendErrorReportViewModel(windowService, ex);
            this.windowService.ShowViewInDialog(viewModel);
        }

        #endregion Commands handling

        #region Currency handling

        /// <summary>
        /// Initializes the UI currencies.
        /// </summary>
        private void IntializeUICurrencies()
        {
            this.CanSelectUICurrency = SecurityManager.Instance.CurrentUser != null;

            try
            {
                // Get the currencies and sort them alphabetically.
                var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
                var currencies = dsm.CurrencyRepository.GetBaseCurrencies().OrderBy(c => c.Name);

                try
                {
                    // Clear the UICurrencies collection (which is bound to the UI Currency combo box) to add the new ones.
                    // Clearing it causes the combo box to set the SelectedItem to null so we have to suspend the saving of the UI Currency for this operation so null is not saved in the database.
                    this.suspendUICurrencySave = true;
                    this.UICurrencies.Clear();
                }
                finally
                {
                    this.suspendUICurrencySave = false;
                }

                // Add the countries to the collection and select the UI Currency of the current user.
                this.UICurrencies.AddRange(currencies);
                this.SelectCurrentUICurrency();
            }
            catch (DataAccessException e)
            {
                Log.DebugException("Failed to initialize the UI Currency selector.", e);
            }
        }

        /// <summary>
        /// Selects the current user's UI currency without triggering its save.
        /// </summary>
        private void SelectCurrentUICurrency()
        {
            this.SelectUICurrency(SecurityManager.Instance.UICurrency);
        }

        /// <summary>
        /// Selects the specified UI currency in the UICurrencies selector without saving it as the current user's UI Currency.        
        /// </summary>
        /// <param name="currency">The currency to select.</param>
        private void SelectUICurrency(Currency currency)
        {
            if (!this.CanSelectUICurrency)
            {
                return;
            }

            try
            {
                this.suspendUICurrencySave = true;
                if (currency != null)
                {
                    this.SelectedUICurrency = this.UICurrencies.FirstOrDefault(c => c.Guid == currency.Guid);
                }
                else
                {
                    this.SelectedUICurrency = null;
                }
            }
            finally
            {
                this.suspendUICurrencySave = false;
            }
        }

        /// <summary>
        /// Sets the specified currency as the UI currency.
        /// </summary>
        /// <param name="value">The value.</param>
        private void SaveUICurrency(Currency value)
        {
            if (this.suspendUICurrencySave)
            {
                return;
            }

            // Save the UI currency in the background and do not report any errors.
            this.Dispatcher.BeginInvoke(
                () =>
                {
                    try
                    {
                        this.CanSelectUICurrency = false;
                        this.CurrenctUICurrencyStatus = UICurrencyStatus.Saving;

                        // Change the UI currency to the specified value.                    
                        bool currencyChanged = false;
                        try
                        {
                            currencyChanged = SecurityManager.Instance.ChangeCurrentUserUICurrency(value);
                        }
                        catch
                        {
                            // Restore the current UI currency in the UI Currency selector if changing it has failed (necessary because the UI Currency selection change has triggered the save).
                            this.CanSelectUICurrency = true;
                            try
                            {
                                this.SelectCurrentUICurrency();
                            }
                            finally
                            {
                                this.CanSelectUICurrency = false;
                            }

                            throw;
                        }

                        if (currencyChanged)
                        {
                            // If the UI currency was changed, sync the current user so the new UI currency is uploaded to the central db.
                            this.syncService.SyncManager.Synchronize(SyncType.User, SecurityManager.Instance.CurrentUser.Guid, SyncDirection.Upload, false);
                        }
                    }
                    catch (BusinessException ex)
                    {
                        Log.ErrorException("Failed to save the new UI Currency in the local database.", ex);
                    }
                    catch (DataAccessException ex)
                    {
                        Log.ErrorException("Failed to save the new UI Currency in the local database.", ex);
                    }
                    catch (SynchronizationException ex)
                    {
                        Log.ErrorException("Failed to synchronize the current user in order to push the new UI currency to the central database.", ex);
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorException("Unknown error occurred while saving the UI currency.", ex);
                    }
                    finally
                    {
                        this.CurrenctUICurrencyStatus = UICurrencyStatus.Normal;
                        this.CanSelectUICurrency = true;
                    }
                },
                System.Windows.Threading.DispatcherPriority.Loaded);
        }

        #endregion Currency handling

        #region Sync handling

        /// <summary>
        /// Sync Reminder Check.
        /// It's used to check if are many unsynchronized items and sets the IsSyncReminderVisible flag
        /// that shows or hide the sync reminder icon
        /// </summary>
        private void SyncReminderCheck()
        {
            if (SecurityManager.Instance.CurrentUserHasRight(Right.ProjectsAndCalculate))
            {
                Task.Factory.StartNew(() =>
                {
                    SyncUtils.ValidateDatabaseVersion();

                    var dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
                    var notSynchronizedItemsCount = dataContext.CountUnSynchedItems(SecurityManager.Instance.CurrentUser.Guid);

                    if (notSynchronizedItemsCount > ReminderMaxLimit)
                    {
                        this.Dispatcher.BeginInvoke(() => this.InitializeAutoSyncReminder());
                    }
                    else
                    {
                        this.IsSyncReminderVisible = false;
                    }
                }).ContinueWith((task) =>
                {
                    if (task.Exception != null)
                    {
                        var error = task.Exception.InnerException;
                        Log.ErrorException("Sync Reminder exception.", error);
                    }
                });
            }
        }

        /// <summary>
        /// Make visible the auto sync image button, initialize the auto sync popup fields 
        /// and start the synchronization delay timer.
        /// </summary>
        private void InitializeAutoSyncReminder()
        {
            this.PostponeTimes = new Dictionary<int, string>
            {
                { 30, "30 min" },
                { 60, "1h" },
                { 120, "2h" },
                { 240, "4h" }
            };
            this.SelectedPostponeTime = 30;

            this.IsSyncReminderVisible = true;
            this.SyncHasFailed = false;
            this.SyncColor = SyncRequired;
            this.SyncActionName = LocalizedResources.Synchronization_SynchronizeNow;
            this.SyncStateMessage = LocalizedResources.Synchronization_SyncReminderToolTip;
            this.InitializeSyncDelayTimer();
        }

        /// <summary>
        /// Initializes the synchronization delay timer.
        /// </summary>
        private void InitializeSyncDelayTimer()
        {
            this.syncTimerStartTime = DateTime.Now.AddMinutes(AutoSyncDelayTime).AddSeconds(1);
            this.SyncStartTimeRemaining = string.Format("{0}:00", AutoSyncDelayTime);

            // Start the synchronization timer 
            this.syncDelayTimer = new DispatcherTimer();
            this.syncDelayTimer.Tick += this.SyncDelayTimerTick;
            this.syncDelayTimer.Interval = new TimeSpan(0, 0, 1);
            this.syncDelayTimer.Start();
        }

        /// <summary>
        /// Handles the Tick event of the SyncDelayTimer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void SyncDelayTimerTick(object sender, EventArgs e)
        {
            if (this.syncTimerStartTime > DateTime.Now)
            {
                var currentDelayDuration = this.syncTimerStartTime - DateTime.Now;
                var currentDelayDurationDateTime = DateTime.MinValue.Add(currentDelayDuration);
                this.SyncStartTimeRemaining = currentDelayDurationDateTime > new DateTime(1, 1, 1, 0, 59, 59)
                    ? currentDelayDurationDateTime.ToString("h:m:ss")
                    : currentDelayDurationDateTime.ToString("m:ss");
            }
            else
            {
                this.syncDelayTimer.Stop();
                this.StartSynchronization();
            }
        }

        #endregion Sync handling

        #region Message handling

        /// <summary>
        /// Handles messages from the messenger service.
        /// </summary>
        /// <param name="message">The message.</param>
        private void ReceiveMessage(NotificationMessage message)
        {
            if (message.Notification == Notification.LoggedIn)
            {
                this.UpdateLoggedInUserInfo();
                this.IntializeUICurrencies();
                this.CanSelectUICurrency = true;
                this.SelectCurrentUICurrency();
                this.RefreshCost(null); // reset the cost information
                this.ComputeHeaderInformationVisibility();

                var onlineChecker = this.container.GetExportedValue<IOnlineCheckService>();
                if (onlineChecker.IsOnline)
                {
                    SyncReminderCheck();
                }
            }
            else if (message.Notification == Notification.LoggedOut)
            {
                if (this.syncDelayTimer != null && this.syncDelayTimer.IsEnabled)
                {
                    this.syncDelayTimer.Stop();
                }

                this.syncService.CancelSyncPostponement();
                this.UpdateLoggedInUserInfo();
                this.SelectUICurrency(null);
                this.CanSelectUICurrency = false;
                this.CurrenctUICurrencyStatus = UICurrencyStatus.Normal;
                this.RefreshCost(null); // reset the cost information
                this.ComputeHeaderInformationVisibility();

                this.IsSyncReminderVisible = false;
            }
            else if (message.Notification == Notification.ShowUpdateAvailable)
            {
                this.ShowUpdateAvailable = true;
            }
            else if (message.Notification == Notification.HideUpdateAvailable)
            {
                this.ShowUpdateAvailable = false;
            }
        }

        /// <summary>
        /// Handles the synchronization started message.
        /// </summary>
        /// <param name="message">The message.</param>
        private void HandleSynchronizationStartedMessage(SynchronizationStartedMessage message)
        {
            this.IsSyncReminderVisible = true;
            this.SyncColor = Colors.Transparent;
            this.SyncStateMessage = LocalizedResources.Synchronization_SyncInProgress;
            this.SyncAnimationIsEnabled = true;
            if (this.syncDelayTimer != null && this.syncDelayTimer.IsEnabled)
            {
                this.syncDelayTimer.Stop();
            }

            this.syncService.CancelSyncPostponement();
        }

        /// <summary>
        /// Handles the SynchronizationFinished message.
        /// </summary>
        /// <param name="message">The message.</param>
        private void HandleSynchronizationFinishedMessage(SynchronizationFinishedMessage message)
        {
            this.SyncAnimationIsEnabled = false;
            if (this.syncService.SyncManager.CurrentStatus.Error != null)
            {
                this.SyncHasFailed = true;
                this.SyncColor = SyncRequired;
                this.SyncActionName = LocalizedResources.Synchronization_TryAgain;
                this.SyncStateMessage = LocalizedResources.Synchronization_SyncFailed;
            }
            else if (this.syncService.SyncManager.Conflicts.Count > 0)
            {
                this.SyncColor = SyncConflicts;
                this.SyncStateMessage =
                    LocalizedResources.Synchronization_SyncCompletedWithConflicts;
            }
            else
            {
                this.SyncColor = SyncSuccess;
                this.SyncStateMessage =
                    this.syncService.SyncManager.CurrentStatus.State ==
                    SyncState.Aborted ?
                        LocalizedResources.Synchronization_SyncAborted :
                        LocalizedResources.Synchronization_SyncCompletedSuccesfully;
            }

            // Reload the UI currencies if they were synchronized.
            if (message.CompletedTasks.Contains(SynchronizationTask.StaticDataAndSettings))
            {
                this.IntializeUICurrencies();
            }
        }

        #endregion Message handling

        #region Pivate methods

        /// <summary>
        /// Changes UI to reflect an online state.
        /// </summary>
        /// <param name="onlineStateChangeMessage">The message containing info about the online state change.</param>
        private void SetOnlineStatus(OnlineStateChangedMessage onlineStateChangeMessage)
        {
            if (onlineStateChangeMessage.IsOnline)
            {
                this.OnlineStatusIcon = Images.OnlineIcon;
                this.OnlineStatusText = LocalizedResources.General_Online;
                OnlineStatusTooltip = null;
            }
            else
            {
                this.OnlineStatusIcon = Images.OfflineIcon;
                this.OnlineStatusText = LocalizedResources.General_Offline;
                switch (onlineStateChangeMessage.OfflineReason)
                {
                    case DbOfflineReason.CanNotConnect:
                        OnlineStatusTooltip = LocalizedResources.OnlineTooltip_CanNotConnect;
                        break;
                    case DbOfflineReason.IncompatibleVersion:
                        OnlineStatusTooltip = LocalizedResources.OnlineTooltip_IncompatibleDbVersion;
                        break;
                    default:
                        OnlineStatusTooltip = null;
                        break;
                }
            }
        }

        /// <summary>
        /// Updates the info display from the logged in user.
        /// </summary>
        private void UpdateLoggedInUserInfo()
        {
            string loggedInMsg = LocalizedResources.General_LoggedInAs + " ";
            User currentUser = SecurityManager.Instance.CurrentUser;
            if (currentUser != null)
            {
                loggedInMsg += currentUser.Name;
                Role currentRole = SecurityManager.Instance.CurrentUserRole;
                loggedInMsg += " (" + UIUtils.GetEnumValueLabel(currentRole) + ")";
            }
            else
            {
                loggedInMsg += LocalizedResources.General_NotAvailable;
            }

            this.LoggedInAs = loggedInMsg;
        }

        /// <summary>
        /// Refreshes the cost, weight, investment, purchase price and target price
        /// </summary>
        /// <param name="result">The result.</param>
        private void RefreshCost(CalculationResult result)
        {
            if (result == null)
            {
                this.Cost = null;
                this.Weight = null;
                this.Investment = null;
                this.TargetPrice = null;
                this.PurchasePrice = null;
                this.EntityName = string.Empty;
            }
            else
            {
                this.Cost = result.TotalCost;
                this.Weight = result.Weight;
                this.Investment = result.InvestmentCostCumulated;
                this.TargetPrice = result.TargetPrice;
                this.PurchasePrice = result.PurchasePrice;
                this.EntityName = result.Name;
            }
        }

        /// <summary>
        /// Handles the PropertyChanged event of the Settings
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void Settings_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "HeaderDisplayWeight" || e.PropertyName == "HeaderDisplayInvestment" || e.PropertyName == "HeaderDisplayCalculationResult"
                || e.PropertyName == "HeaderDisplaysPurchasePrice" || e.PropertyName == "HeaderDisplaysTargetPrice")
            {
                ComputeHeaderInformationVisibility();
            }
        }

        /// <summary>
        /// Computes whether to display the weight and investment.
        /// </summary>
        private void ComputeHeaderInformationVisibility()
        {
            if (UserSettingsManager.Instance.HeaderDisplayWeight && this.Weight.HasValue && SecurityManager.Instance.CurrentUser != null)
            {
                this.WeightVisibility = Visibility.Visible;
            }
            else
            {
                this.WeightVisibility = Visibility.Collapsed;
            }

            if (UserSettingsManager.Instance.HeaderDisplayInvestment && this.Investment.HasValue && SecurityManager.Instance.CurrentUser != null)
            {
                this.InvestmentVisibility = Visibility.Visible;
            }
            else
            {
                this.InvestmentVisibility = Visibility.Collapsed;
            }

            if (UserSettingsManager.Instance.HeaderDisplayCalculationResult && this.Cost.HasValue && SecurityManager.Instance.CurrentUser != null)
            {
                this.CostVisibility = Visibility.Visible;
            }
            else
            {
                this.CostVisibility = Visibility.Collapsed;
            }

            if (UserSettingsManager.Instance.HeaderDisplaysPurchasePrice && this.PurchasePrice.HasValue && SecurityManager.Instance.CurrentUser != null)
            {
                this.PurchasePriceVisibility = Visibility.Visible;
            }
            else
            {
                this.PurchasePriceVisibility = Visibility.Collapsed;
            }

            if (UserSettingsManager.Instance.HeaderDisplaysTargetPrice && this.TargetPrice.HasValue && SecurityManager.Instance.CurrentUser != null)
            {
                this.TargetPriceVisibility = Visibility.Visible;
            }
            else
            {
                this.TargetPriceVisibility = Visibility.Collapsed;
            }
        }

        #endregion Pivate methods
    }
}