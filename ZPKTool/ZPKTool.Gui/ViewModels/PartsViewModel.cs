﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using ZPKTool.Business;
using ZPKTool.Business.Export;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Managers;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the Parts view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class PartsViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The service used to create open/save file dialogs.
        /// </summary>
        private IFileDialogService fileDialogService;

        /// <summary>
        /// The service used to brows for folders.
        /// </summary>
        private IFolderBrowserService folderBrowserService;

        /// <summary>
        /// The PleaseWaitService reference.
        /// </summary>
        private IPleaseWaitService pleaseWaitService;

        /// <summary>
        /// The composition container.
        /// </summary>
        private CompositionContainer container;

        /// <summary>
        /// The collection of objects which are used in the binding on the data grid
        /// </summary>
        private DispatchedObservableCollection<PartItem> parts = new DispatchedObservableCollection<PartItem>();

        /// <summary>
        /// The currently selected items in the grid
        /// </summary>
        private ObservableCollection<object> selectedItems = new ObservableCollection<object>();

        /// <summary>
        /// The currently selected entities info data.
        /// </summary>
        private ObservableCollection<EntityInfo> selectedEntitiesInfo = new ObservableCollection<EntityInfo>();

        /// <summary>
        /// Reference to the user control loaded in the details section of the screen
        /// </summary>
        private object partDetails;

        /// <summary>
        /// The parent Assembly.
        /// </summary>
        private Assembly parentAssembly;

        /// <summary>
        /// The data context.
        /// </summary>
        private IDataSourceManager partDataContext;

        /// <summary>
        /// Extra information for the report.
        /// </summary>
        private ExtendedDataGridAdditionalReportInformation additionalReportInformation;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        /// <summary>
        /// The project that contains the parent assembly.
        /// </summary>
        private Project parentProject;

        #endregion Attributes

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PartsViewModel"/> class.
        /// </summary>
        /// <param name="container">The composition container.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="messenger">The messenger.</param>
        /// <param name="fileDialogService">The file dialog service.</param>
        /// <param name="folderBrowserService">The folder browser service.</param>
        /// <param name="pleaseWaitService">The please wait service.</param>
        /// <param name="unitsService">The units service.</param>
        [ImportingConstructor]
        public PartsViewModel(
            CompositionContainer container,
            IWindowService windowService,
            IMessenger messenger,
            IFileDialogService fileDialogService,
            IFolderBrowserService folderBrowserService,
            IPleaseWaitService pleaseWaitService,
            IUnitsService unitsService)
        {
            Argument.IsNotNull("container", container);
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("fileDialogService", fileDialogService);
            Argument.IsNotNull("folderBrowserService", folderBrowserService);
            Argument.IsNotNull("pleaseWaitService", pleaseWaitService);
            Argument.IsNotNull("unitsService", unitsService);

            this.container = container;
            this.WindowService = windowService;
            this.messenger = messenger;
            this.fileDialogService = fileDialogService;
            this.folderBrowserService = folderBrowserService;
            this.pleaseWaitService = pleaseWaitService;
            this.unitsService = unitsService;

            this.AddCommand = new DelegateCommand(this.Add, () => !this.IsReadOnly);
            this.DeleteCommand = new DelegateCommand(
                this.Delete,
                () => !this.IsReadOnly && this.SelectedItems != null && this.SelectedItems.Count > 0);
            this.EditCommand = new DelegateCommand(
                this.Edit,
                () => !this.IsReadOnly && this.SelectedItems != null && this.SelectedItems.Count == 1);
            this.MouseDoubleClickCommand = new DelegateCommand<MouseButtonEventArgs>(this.MouseDoubleClicked);
            this.DragOverCommand = new DelegateCommand<DragEventArgs>(this.CheckDropTarget, e => !this.IsReadOnly);
            this.DragEnterCommand = new DelegateCommand<DragEventArgs>(this.CheckDropTarget, e => !this.IsReadOnly);
            this.DropCommand = new DelegateCommand<DragEventArgs>(this.Drop, e => !this.IsReadOnly);
            this.CopyCommand = new DelegateCommand<object>(this.ExecuteCopy, this.CanCopy);
            this.PasteCommand = new DelegateCommand<object>(this.ExecutePaste, this.CanPaste);
            this.ImportCommand = new DelegateCommand<object>(this.ExecuteImport, o => !this.IsReadOnly);
            this.ExportCommand = new DelegateCommand<object>(this.ExecuteExport, this.CanExport);
            this.SelectedItems.CollectionChanged += SelectedItemsOnCollectionChanged;
        }

        #endregion

        #region Commands

        /// <summary>
        /// Gets the command for the add button
        /// </summary>
        public ICommand AddCommand { get; private set; }

        /// <summary>
        /// Gets the command for the edit button
        /// </summary>
        public ICommand EditCommand { get; private set; }

        /// <summary>
        /// Gets the command for the delete button
        /// </summary>
        public ICommand DeleteCommand { get; private set; }

        /// <summary>
        /// Gets the command for the mouse DragEnter event
        /// </summary>
        public ICommand DragEnterCommand { get; private set; }

        /// <summary>
        /// Gets the DragOver command.
        /// </summary>
        public ICommand DragOverCommand { get; private set; }

        /// <summary>
        /// Gets the drop command.
        /// </summary>
        public ICommand DropCommand { get; private set; }

        /// <summary>
        /// Gets the mouse double click command.
        /// </summary>
        public ICommand MouseDoubleClickCommand { get; private set; }

        /// <summary>
        /// Gets the copy command.
        /// </summary>
        public ICommand CopyCommand { get; private set; }

        /// <summary>
        /// Gets the paste command.
        /// </summary>
        public ICommand PasteCommand { get; private set; }

        /// <summary>
        /// Gets the import command.
        /// </summary>
        public ICommand ImportCommand { get; private set; }

        /// <summary>
        /// Gets the export command.
        /// </summary>
        public ICommand ExportCommand { get; private set; }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the window service.
        /// </summary>
        public IWindowService WindowService
        {
            get
            {
                return this.windowService;
            }

            set
            {
                if (this.windowService != value)
                {
                    this.windowService = value;
                    OnPropertyChanged(() => this.WindowService);
                }
            }
        }

        /// <summary>
        /// Gets or sets the collection of parts displayed in the data grid
        /// </summary>
        public DispatchedObservableCollection<PartItem> Parts
        {
            get
            {
                return this.parts;
            }

            set
            {
                if (this.parts != value)
                {
                    this.parts = value;
                    OnPropertyChanged(() => this.Parts);
                }
            }
        }

        /// <summary>
        /// Gets or sets the currently selected items in the grid.
        /// </summary>
        public ObservableCollection<object> SelectedItems
        {
            get
            {
                return this.selectedItems;
            }

            set
            {
                if (!Equals(this.selectedItems, value))
                {
                    this.selectedItems = value;
                    OnPropertyChanged(() => this.SelectedItems);
                }
            }
        }

        /// <summary>
        /// Gets or sets the currently selected entities info data.
        /// </summary>
        public ObservableCollection<EntityInfo> SelectedEntitiesInfo
        {
            get
            {
                return this.selectedEntitiesInfo;
            }

            set
            {
                if (!Equals(this.selectedEntitiesInfo, value))
                {
                    this.selectedEntitiesInfo = value;
                    OnPropertyChanged(() => this.SelectedEntitiesInfo);
                }
            }
        }

        /// <summary>
        /// Gets or sets the user control loaded in the details content area of the screen
        /// </summary>
        public object PartDetails
        {
            get
            {
                return this.partDetails;
            }

            set
            {
                if (this.partDetails != value)
                {
                    this.partDetails = value;
                    OnPropertyChanged(() => this.PartDetails);
                }
            }
        }

        /// <summary>
        /// Gets or sets the data context.
        /// </summary>
        /// <value>
        /// The data context.
        /// </value>
        public IDataSourceManager PartDataContext
        {
            get
            {
                return this.partDataContext;
            }

            set
            {
                if (this.partDataContext != value)
                {
                    this.partDataContext = value;
                    this.OnPropertyChanged(() => this.PartDataContext);
                }

                this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(value);
            }
        }

        /// <summary>
        /// Gets or sets the parent assembly.
        /// </summary>
        /// <value>
        /// The parent assembly.
        /// </value>
        public Assembly ParentAssembly
        {
            get
            {
                return this.parentAssembly;
            }

            set
            {
                if (this.parentAssembly != value)
                {
                    this.parentAssembly = value;
                    OnPropertyChanged(() => this.IsReadOnly);

                    this.Parts = new DispatchedObservableCollection<PartItem>();
                    this.AdditionalReportInformation = new ExtendedDataGridAdditionalReportInformation();

                    if (this.ParentAssembly != null)
                    {
                        parentProject = !IsInViewerMode
                            ? this.PartDataContext.ProjectRepository.GetParentProject(this.ParentAssembly)
                            : EntityUtils.GetParentProject(this.ParentAssembly);

                        foreach (var part in this.parentAssembly.Parts.Where(p => !p.IsDeleted).OrderBy(p => p.Index))
                        {
                            var item = new PartItem();
                            item.Part = part;
                            item.CalculateCost(parentProject, IsInViewerMode);
                            Parts.Add(item);
                        }

                        this.AdditionalReportInformation.ProjectName += parentProject != null ? " " + parentProject.Name : string.Empty;
                        this.AdditionalReportInformation.ParentName += " " + this.ParentAssembly.Name;
                        this.AdditionalReportInformation.Name += " " + LocalizedResources.General_Parts + " (" + this.ParentAssembly.Name + ")";
                        this.AdditionalReportInformation.Version += " " + this.ParentAssembly.Version;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the extra report information.
        /// </summary>
        public ExtendedDataGridAdditionalReportInformation AdditionalReportInformation
        {
            get
            {
                return this.additionalReportInformation;
            }

            set
            {
                if (this.additionalReportInformation != value)
                {
                    this.additionalReportInformation = value;
                    OnPropertyChanged(() => this.AdditionalReportInformation);
                }
            }
        }

        /// <summary>
        /// Gets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter { get; private set; }

        #endregion

        /// <summary>
        /// Adds this instance.
        /// </summary>
        private void Add()
        {
            var partVm = this.container.GetExportedValue<PartViewModel>();
            partVm.DataSourceManager = this.PartDataContext;
            partVm.InitializeForCreation(false, this.ParentAssembly);
            this.windowService.ShowViewInDialog(partVm, "PartWindowViewTemplate");

            var newPart = partVm.Model;
            if (newPart != null && partVm.Saved)
            {
                var partItem = new PartItem();
                partItem.Part = newPart;
                partItem.CalculateCost(parentProject, IsInViewerMode);
                this.Parts.Add(partItem);
            }
        }

        /// <summary>
        /// Deletes the selected parts.
        /// </summary>
        private void Delete()
        {
            var selectedParts = this.SelectedItems.Cast<PartItem>().ToList();
            if (selectedParts.Count == 0)
            {
                this.windowService.MessageDialogService.Show(
                            LocalizedResources.Error_NoItemSelected,
                            MessageDialogType.Info);
                return;
            }

            string questionMessage;
            var permanentlyDelete = Keyboard.Modifiers == ModifierKeys.Shift;
            var isMasterData = selectedParts.Any(p => EntityUtils.IsMasterData(p.Part));

            /* 
             * If the Shift modifier is not pressed or the selected parts 
             * are not Masterdata then the deletion should be made to the 
             * trash bin else should be deleted permanently.
             */
            if (permanentlyDelete || isMasterData)
            {
                questionMessage = LocalizedResources.Question_PermanentlyDeleteSelectedItems;
            }
            else
            {
                questionMessage = LocalizedResources.Question_DeleteSelectedObjects;
            }

            var result = this.windowService.MessageDialogService.Show(
                questionMessage,
                MessageDialogType.YesNo);

            /*
             * If the answer is Yes, delete the selected items.
             */
            if (result == MessageDialogResult.Yes)
            {
                Action<PleaseWaitService.WorkParams> work = (workParams) =>
                    {
                        var partIndex = 1;
                        foreach (var selectedPartItem in selectedParts)
                        {
                            string waitMessage;
                            if (selectedItems.Count == 1)
                            {
                                waitMessage = LocalizedResources.General_WaitItemDelete;
                            }
                            else
                            {
                                waitMessage = string.Format(
                                    LocalizedResources.General_WaitDeleteObjects,
                                    partIndex,
                                    selectedItems.Count);
                            }

                            this.pleaseWaitService.Message = waitMessage;
                            partIndex++;

                            var trashManager = new TrashManager(this.PartDataContext);
                            if (isMasterData || permanentlyDelete)
                            {
                                trashManager.DeleteByStoreProcedure(selectedPartItem.Part);
                            }
                            else
                            {
                                trashManager.DeleteToTrashBin(selectedPartItem.Part);
                                this.PartDataContext.SaveChanges();
                            }
                        }
                    };

                Action<PleaseWaitService.WorkParams> workCompleted = (workParams) =>
                    {
                        if (workParams.Error != null)
                        {
                            this.windowService.MessageDialogService.Show(workParams.Error);
                        }
                        else
                        {
                            foreach (var partItem in selectedParts)
                            {
                                var notification = this.ParentAssembly.IsMasterData
                                                       ? Notification.MasterDataEntityChanged
                                                       : Notification.MyProjectsEntityChanged;
                                var message = new EntityChangedMessage(notification);
                                message.Entity = partItem.Part;
                                message.Parent = this.ParentAssembly;
                                message.ChangeType = EntityChangeType.EntityDeleted;
                                this.messenger.Send(message);
                                this.Parts.Remove(partItem);
                            }
                        }
                    };

                this.pleaseWaitService.Show(LocalizedResources.General_WaitItemDelete, work, workCompleted);
            }
        }

        /// <summary>
        /// Edits this instance.
        /// </summary>
        private void Edit()
        {
            var selectedParts = this.SelectedItems.Cast<PartItem>().ToList();
            if (selectedParts.Count == 1)
            {
                var part = selectedParts.First().Part;
                SelectPart(part);
            }
        }

        /// <summary>
        /// Loads the selected part details.
        /// </summary>
        private void LoadSelectedPartDetails()
        {
            var selectedParts = this.SelectedItems.Cast<PartItem>().ToList();
            if (selectedParts.Count != 1)
            {
                this.PartDetails = null;
                return;
            }

            var partVm = this.container.GetExportedValue<PartViewModel>();
            partVm.IsReadOnly = true;
            partVm.DataSourceManager = this.PartDataContext;
            partVm.IsInViewerMode = IsInViewerMode;
            partVm.Model = selectedParts.First().Part;

            this.PartDetails = partVm;
        }

        /// <summary>
        /// Selects a specified part in the projects tree.
        /// </summary>
        /// <param name="part">The part.</param>
        private void SelectPart(Part part)
        {
            if (part == null)
            {
                return;
            }

            this.messenger.Send(
                new NavigateToEntityMessage(
                    part,
                    this.PartDataContext != null ? this.partDataContext.DatabaseId : DbIdentifier.NotSet),
                IsInViewerMode ? GlobalMessengerTokens.ModelBrowserTargetToken : GlobalMessengerTokens.MainViewTargetToken);
        }

        #region Mouse events

        /// <summary>
        /// DataGrid mouse double click event.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void MouseDoubleClicked(MouseButtonEventArgs e)
        {
            this.Edit();
        }

        /// <summary>
        /// Handles the CheckDropTarget event of the SubassembliesDataGrid control.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        private void CheckDropTarget(DragEventArgs e)
        {
            e.Handled = true;

            var sourceItem = (DragAndDropData)e.Data.GetData(typeof(DragAndDropData));
            if (sourceItem != null && sourceItem.DataObject is Part)
            {
                e.Effects = DragDropEffects.Copy;
            }
            else
            {
                e.Effects = DragDropEffects.None;
            }
        }

        /// <summary>
        /// Handles the Drop event of the SubassembliesDataGrid control.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        private void Drop(DragEventArgs e)
        {
            e.Handled = true;

            var sourceItem = (DragAndDropData)e.Data.GetData(typeof(DragAndDropData));
            if (sourceItem.DataObject is Part)
            {
                // Add the pasted item into its parent
                ClipboardManager.Instance.Copy(sourceItem.DataObject, sourceItem.DataObjectContext);

                var pastedObject = ClipboardManager.Instance.Paste(this.ParentAssembly, this.PartDataContext).First();
                if (pastedObject.Error != null)
                {
                    this.windowService.MessageDialogService.Show(pastedObject.Error);
                }
                else
                {
                    var pastedPart = pastedObject.Entity as Part;
                    if (pastedPart != null)
                    {
                        var pastedItem = new PartItem();
                        pastedItem.Part = pastedPart;
                        pastedItem.CalculateCost(parentProject, IsInViewerMode);
                        this.Parts.Add(pastedItem);

                        var message = new EntityChangedMessage(Notification.MyProjectsEntityChanged);
                        message.Entity = this.ParentAssembly;
                        message.Parent = null;
                        message.ChangeType = EntityChangeType.EntityUpdated;
                        this.messenger.Send(message);

                        this.SelectedItems.Add(pastedItem);
                    }
                }
            }
        }

        #endregion

        /// <summary>
        /// Determines whether this instance can paste the specified parameter.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        /// true if this instance can paste the specified parameter; otherwise, false.
        /// </returns>
        private bool CanPaste(object parameter)
        {
            var clipboardObjTypes = ClipboardManager.Instance.PeekTypes.ToList();
            if (this.IsReadOnly
                || clipboardObjTypes.Count == 0
                || clipboardObjTypes.Any(type => type != typeof(Part)))
            {
                return false;
            }

            if (ClipboardManager.Instance.Operation == ClipboardOperation.Cut)
            {
                // An item can not be moved into a master data.
                if (this.ParentAssembly.IsMasterData)
                {
                    return false;
                }

                var clipboardObjects = ClipboardManager.Instance.PeekClipboardObjects;
                foreach (var clpObject in clipboardObjects)
                {
                    // An assembly can not be moved if: object to move are released or are master data,
                    // already exists in the destination collection
                    var releaseableObj = clpObject as IReleasable;
                    var identifObject = clpObject as IIdentifiable;
                    if ((releaseableObj != null && releaseableObj.IsReleased)
                        || identifObject == null
                        || this.ParentAssembly.Parts.Any(part => part.Guid == identifObject.Guid)
                        || EntityUtils.IsMasterData(clpObject))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Performs the logic associated with the Paste command.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void ExecutePaste(object parameter)
        {
            var clipboardObjTypes = ClipboardManager.Instance.PeekTypes.ToList();
            if (this.IsReadOnly
                || clipboardObjTypes.Count == 0
                || clipboardObjTypes.Any(type => type != typeof(Part)))
            {
                return;
            }

            var operation = ClipboardManager.Instance.Operation;
            var clipboardObjects = ClipboardManager.Instance.PeekClipboardObjects.ToList();
            string displayedMessage;

            // If the operation is Cut, the message displayed is "Moving object(s)..."; else, the operation is Copy, and the message displayed is "Copying object(s)..."
            if (clipboardObjects.Count == 1)
            {
                var objName = EntityUtils.GetEntityName(clipboardObjects.First());
                if (!string.IsNullOrWhiteSpace(objName))
                {
                    objName = "\"" + objName + "\"";
                }

                displayedMessage = string.Format(operation == ClipboardOperation.Cut ? LocalizedResources.General_MovingItem : LocalizedResources.General_CopyingItem, objName);
            }
            else
            {
                displayedMessage = operation == ClipboardOperation.Cut ? LocalizedResources.General_MovingObjects : LocalizedResources.General_CopyingObjects;
            }

            var pastedObjects = new List<PastedData>();
            Action<PleaseWaitService.WorkParams> work = (workParams) =>
            {
                pastedObjects = ClipboardManager.Instance.Paste(this.ParentAssembly, this.PartDataContext).ToList();
            };

            Action<PleaseWaitService.WorkParams> workCompleted = (workparams) =>
            {
                if (workparams.Error != null)
                {
                    Log.Error("An error occurred while executing the paste operation.", workparams.Error);
                    this.windowService.MessageDialogService.Show(workparams.Error);
                }
                else
                {
                    // Display errors appeared during the paste process.
                    var errorMessage = ClipboardManager.Instance.GetErrorMessageToDisplay(pastedObjects);
                    if (!string.IsNullOrWhiteSpace(errorMessage))
                    {
                        this.windowService.MessageDialogService.Show(errorMessage, MessageDialogType.Error);
                    }

                    var lastPastedObject = pastedObjects.LastOrDefault(o => o.Error == null);
                    var messages = new Collection<EntityChangedMessage>();
                    this.SelectedItems.Clear();
                    foreach (var obj in pastedObjects.Where(o => o.Error == null))
                    {
                        var pastedPart = obj.Entity as Part;
                        if (pastedPart != null)
                        {
                            var partItem = new PartItem { Part = pastedPart };
                            partItem.CalculateCost(parentProject, IsInViewerMode);
                            this.Parts.Add(partItem);
                            this.SelectedItems.Add(partItem);

                            if (obj == lastPastedObject)
                            {
                                var notification = pastedPart.IsMasterData
                                    ? Notification.MasterDataEntityChanged
                                    : Notification.MyProjectsEntityChanged;

                                var message = new EntityChangedMessage(notification);
                                message.Entity = pastedPart;
                                message.Parent = this.ParentAssembly;
                                message.SelectEntity = false;
                                message.ChangeType = operation == ClipboardOperation.Cut
                                    ? EntityChangeType.EntityMoved
                                    : EntityChangeType.EntityCreated;

                                messages.Add(message);
                            }
                        }
                    }

                    if (messages.Count == 1)
                    {
                        this.messenger.Send(messages.FirstOrDefault());
                    }
                    else if (messages.Any())
                    {
                        var message = new EntitiesChangedMessage(messages);
                        this.messenger.Send(message);
                    }
                }
            };

            this.pleaseWaitService.Show(displayedMessage, work, workCompleted);
        }

        /// <summary>
        /// Determines whether this instance can copy the specified parameter.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        ///   <c>true</c> if this instance can copy the specified parameter; otherwise, <c>false</c>.
        /// </returns>
        private bool CanCopy(object parameter)
        {
            if (this.SelectedItems == null
                || this.SelectedItems.Count == 0
                || !SecurityManager.Instance.CurrentUserHasRight(Right.ProjectsAndCalculate)
                || this.IsReadOnly)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Executes the copy.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void ExecuteCopy(object parameter)
        {
            var selectedParts = this.SelectedItems.Cast<PartItem>().ToList();
            if (selectedParts.Count == 0)
            {
                return;
            }

            var itemsToCopy = selectedParts
                .Select(item => new ClipboardObject()
                {
                    Entity = item.Part,
                    DbSource = this.PartDataContext.DatabaseId
                });

            ClipboardManager.Instance.Copy(itemsToCopy);
        }

        /// <summary>
        /// Contains the logic associated with the Import command.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void ExecuteImport(object parameter)
        {
            /*
             * Set the file dialog service.
             */
            this.fileDialogService.Filter = UIUtils.GetDialogFilterForExportImport(typeof(Part));
            this.fileDialogService.Multiselect = true;
            if (this.fileDialogService.ShowOpenFileDialog() != true)
            {
                return;
            }

            var filePaths = new List<string>(this.fileDialogService.FileNames);

            Action<PleaseWaitService.WorkParams, IEnumerable<ImportedData<object>>> importCompleted = (workParams, importedData) =>
            {
                // Perform action after the import is completed.
                foreach (var entityData in importedData.Where(imp => imp.Entity != null))
                {
                    var importedObjectIsMasterData = EntityUtils.IsMasterData(entityData.Entity);
                    var notification = importedObjectIsMasterData ? Notification.MasterDataEntityChanged : Notification.MyProjectsEntityChanged;

                    // Only the last imported item will be selected.
                    var selectItem = entityData == importedData.LastOrDefault(i => i.Entity != null);

                    // Create the corresponding part item and add it into the parts list.
                    var item = new PartItem();
                    item.Part = entityData.Entity as Part;
                    item.CalculateCost(parentProject, IsInViewerMode);
                    this.Parts.Add(item);

                    if (selectItem)
                    {
                        this.SelectedItems.Add(item);
                    }

                    var msg = new EntityChangedMessage(notification)
                    {
                        // Create a message to notify that a new item has been imported.
                        Entity = entityData.Entity,
                        Parent = this.ParentAssembly,
                        ChangeType = EntityChangeType.EntityImported,
                        SelectEntity = selectItem
                    };
                    this.messenger.Send(msg);
                }
            };

            var importService = this.container.GetExportedValue<IImportService>();
            importService.Import(filePaths, this.ParentAssembly, this.PartDataContext, true, importCompleted);
        }

        /// <summary>
        /// Determines whether this instance can export the specified parameter.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        ///   <c>true</c> if this instance can export the specified parameter; otherwise, <c>false</c>.
        /// </returns>
        private bool CanExport(object parameter)
        {
            if (this.SelectedItems == null
                || this.SelectedItems.Count == 0
                || this.IsInViewerMode)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Executes the export.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void ExecuteExport(object parameter)
        {
            var selectedParts = this.SelectedItems.Cast<PartItem>().ToList();
            if (selectedParts.Count == 0 ||
                selectedParts.Any(item => item == null || item.Part == null))
            {
                return;
            }

            string filePath = null;
            if (selectedParts.Count == 1)
            {
                var exportPart = selectedParts.First().Part;
                var fileName = EntityUtils.GetEntityName(exportPart);
                if (!string.IsNullOrWhiteSpace(fileName))
                {
                    fileName = PathUtils.SanitizeFileName(fileName);
                }

                this.fileDialogService.Reset();
                this.fileDialogService.FileName = fileName;
                this.fileDialogService.Filter = UIUtils.GetDialogFilterForExportImport(exportPart.GetType());

                var result = this.fileDialogService.ShowSaveFileDialog();
                if (result == true)
                {
                    filePath = this.fileDialogService.FileName;
                }
            }
            else
            {
                this.folderBrowserService.Description = LocalizedResources.General_ExportFolderBrowserDescription;
                var result = folderBrowserService.ShowFolderBrowserDialog();
                if (result)
                {
                    filePath = folderBrowserService.SelectedPath;
                }
            }

            if (string.IsNullOrEmpty(filePath))
            {
                return;
            }

            foreach (var item in selectedParts)
            {
                object objectToExport = item.Part;
                var pathToExport = filePath;

                /*
                 * If there are more objects to be exported, we must get 
                 * for each object the path where it will be exported.
                 * Also we must check for file name collisions before export.
                 */
                if (selectedItems.Count > 1)
                {
                    pathToExport = Path.Combine(pathToExport, UIUtils.GetExportObjectFileName(objectToExport));
                    pathToExport = PathUtils.FixFilePathForCollisions(pathToExport);
                }

                ExportManager.Instance.Export(
                    objectToExport,
                    this.PartDataContext,
                    pathToExport,
                    this.unitsService.Currencies,
                    this.unitsService.BaseCurrency);
            }

            this.WindowService.MessageDialogService.Show(
                selectedParts.Count > 1
                    ? LocalizedResources.ExportImport_ExportItemsWithSuccess
                    : LocalizedResources.ExportImport_ExportSuccess,
                MessageDialogType.Info);
        }

        /// <summary>
        /// Handle the SelectedItems collection changed event.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="NotifyCollectionChangedEventArgs"/> instance containing the event data.</param>
        private void SelectedItemsOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            this.LoadSelectedPartDetails();

            if (this.IsInViewerMode)
            {
                return;
            }        

            /*
             * Synchronizes the SelectedEntitiesInfo collection, 
             * every time an item from parts datagrid is selected/deselected.
             */
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        foreach (var newItem in e.NewItems)
                        {
                            var partItem = newItem as PartItem;
                            if (partItem != null && partItem.Part != null)
                            {
                                this.SelectedEntitiesInfo.Add(
                                    new EntityInfo(partItem.Part, this.PartDataContext.DatabaseId));
                            }
                        }

                        break;
                    }

                case NotifyCollectionChangedAction.Remove:
                    {
                        foreach (var oldItem in e.OldItems)
                        {
                            var partItem = oldItem as PartItem;
                            if (partItem != null && partItem.Part != null)
                            {
                                var index = 0;
                                var founded = false;
                                while (!founded && index < this.SelectedEntitiesInfo.Count)
                                {
                                    if (this.SelectedEntitiesInfo[index].Entity == partItem.Part)
                                    {
                                        founded = true;
                                        this.SelectedEntitiesInfo.RemoveAt(index);
                                    }

                                    index++;
                                }
                            }
                        }

                        break;
                    }

                case NotifyCollectionChangedAction.Reset:
                    {
                        this.SelectedEntitiesInfo.Clear();
                        break;
                    }
            }
        }
    }
}