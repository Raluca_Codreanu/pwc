﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Calculations.CostCalculation;

namespace ZPKTool.Gui.ViewModels.ResultDetails
{
    /// <summary>
    /// This class is used for data binding in the Raw Materials data grid in the ResultDetails view.
    /// </summary>
    public class RawMaterialBreakdownGridItem
    {
        /// <summary>
        /// Gets or sets the cost of the raw material.
        /// </summary>
        public RawMaterialCost Cost { get; set; }

        /// <summary>
        /// Gets or sets the name of the raw material's parent entity.
        /// </summary>
        public string ParentName { get; set; }
    }
}
