﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Calculations.CostCalculation;

namespace ZPKTool.Gui.ViewModels.ResultDetails
{
    /// <summary>
    /// This class is used for data binding in the Consumables data grid in the ResultDetails view.
    /// </summary>
    public class ConsumableBreakdownGridItem
    {
        /// <summary>
        /// Gets or sets the cost of the consumable.
        /// </summary>
        public ConsumableCost Cost { get; set; }

        /// <summary>
        /// Gets or sets the name of the consumable's parent entity.
        /// </summary>
        public string ParentName { get; set; }
    }
}
