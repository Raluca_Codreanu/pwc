﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using ZPKTool.Business;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Controls;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.ViewModels.ResultDetails;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for part list breakdown tab from result details view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class PartListBreakdownViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The model browser helper service.
        /// </summary>
        private IModelBrowserHelperService modelBrowserHelperService;

        /// <summary>
        /// The entity for which to compute and display the costs.
        /// </summary>
        private object entity;

        /// <summary>
        /// The measurement units adapter.
        /// </summary>
        private UnitsAdapter measurementUnitsAdapter;

        /// <summary>
        /// The chart builder, used for building chart items from cost calculation results.
        /// </summary>
        private CostChartBuilder chartBuilder;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PartListBreakdownViewModel"/> class.
        /// </summary>
        /// <param name="messenger">The messenger.</param>
        /// <param name="modelBrowserHelperService">The model browser helper service.</param>
        [ImportingConstructor]
        public PartListBreakdownViewModel(IMessenger messenger, IModelBrowserHelperService modelBrowserHelperService)
        {
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("modelBrowserHelperService", modelBrowserHelperService);

            this.messenger = messenger;
            this.modelBrowserHelperService = modelBrowserHelperService;

            this.ChartItems.Value = new Collection<LabeledPieChartItem>();
            this.DataGridItems.Value = new Collection<PartListBreakdownGridItem>();

            this.CurrentUserName = SecurityManager.Instance.CurrentUser != null ? SecurityManager.Instance.CurrentUser.Name : string.Empty;

            this.GridMouseDoubleClickCommand = new DelegateCommand<PartListBreakdownGridItem>(this.HandleMouseDoubleClickEvent);
            this.RowDetailsMouseDoubleClickCommand = new DelegateCommand<PartListBreakdownRowDetailsItem>(this.HandleMouseDoubleClickEvent);
            this.ShowEntityDetailsCommand = new DelegateCommand<PartListBreakdownGridItem>(this.GetEntityDetails);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the entity for which to compute and display the costs.
        /// </summary>
        public object Entity
        {
            get
            {
                return this.entity;
            }

            set
            {
                Argument.IsNotNull("Entity", value);
                if (this.entity != value)
                {
                    var entityType = value.GetType();
                    if (entityType != typeof(Assembly) && entityType != typeof(Part) && entityType != typeof(RawPart))
                    {
                        throw new InvalidOperationException("The entity type: " + entityType.FullName + " is not supported. Entity must be Assembly, Part or RawPart.");
                    }

                    this.entity = value;
                    OnPropertyChanged(() => this.Entity);
                }
            }
        }

        /// <summary>
        /// Gets or sets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter
        {
            get
            {
                return this.measurementUnitsAdapter;
            }

            set
            {
                if (this.measurementUnitsAdapter != value)
                {
                    this.measurementUnitsAdapter = value;
                    OnPropertyChanged(() => this.MeasurementUnitsAdapter);
                }
            }
        }

        /// <summary>
        /// Gets or sets the chart builder, used for building chart items from cost calculation results.
        /// </summary>
        public CostChartBuilder ChartBuilder
        {
            get
            {
                return this.chartBuilder;
            }

            set
            {
                Argument.IsNotNull("ChartBuilder", value);
                if (this.chartBuilder != value)
                {
                    this.chartBuilder = value;
                }
            }
        }
        
        /// <summary>
        /// Gets or sets the project to which the entity belongs.
        /// </summary>
        public Project ParentProject { get; set; }

        /// <summary>
        /// Gets or sets the identifier of database manager.
        /// </summary>
        public DbIdentifier DatabaseManagerId { get; set; }

        /// <summary>
        /// Gets or sets the normal cost calculation result.
        /// </summary>
        public CalculationResult NormalCalculationResult { get; set; }

        /// <summary>
        /// Gets or sets the token for sending message notifications for parent view model.
        /// </summary>
        public Guid MessengerToken { get; set; }

        /// <summary>
        /// Gets the name of the current logged-in user.
        /// </summary>
        public string CurrentUserName { get; private set; }

        /// <summary>
        /// Gets or sets the extra information for data grid reports.
        /// </summary>
        public ExtendedDataGridAdditionalReportInformation AdditionalReportInformation { get; set; }

        /// <summary>
        /// Gets or sets the visibility of tab containing the breakdown.
        /// </summary>
        public VMProperty<Visibility> TabVisibility { get; set; }

        /// <summary>
        /// Gets or sets the current chart mode.
        /// </summary>
        public VMProperty<ChartModes> ChartMode { get; set; }

        /// <summary>
        /// Gets the items to display in chart.
        /// </summary>
        public VMProperty<ICollection<LabeledPieChartItem>> ChartItems { get; private set; }

        /// <summary>
        /// Gets the total cost of entity.
        /// </summary>
        public VMProperty<decimal?> TotalCost { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the entity has costs (total value > 0) or not.
        /// </summary>
        public VMProperty<bool> EntityHasCosts { get; private set; }

        /// <summary>
        /// Gets the cost items displayed in the data grid.
        /// </summary>
        public VMProperty<ICollection<PartListBreakdownGridItem>> DataGridItems { get; private set; }

        /// <summary>
        /// Gets the selected cost item from the data grid.
        /// </summary>
        public VMProperty<PartListBreakdownGridItem> SelectedDataGridItem { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the cost calculation result is enhanced or not.
        /// </summary>
        public VMProperty<bool> IsEnhancedCostCalculationResult { get; private set; }

        #endregion

        #region Commands

        /// <summary>
        /// Gets the command for the mouse double click event on grid item from data grid.
        /// </summary>
        public ICommand GridMouseDoubleClickCommand { get; private set; }

        /// <summary>
        /// Gets the command for the mouse double click event on tree view item from data grid's row details.
        /// </summary>
        public ICommand RowDetailsMouseDoubleClickCommand { get; private set; }

        /// <summary>
        /// Gets the command for showing the details of an entity.
        /// </summary>
        public ICommand ShowEntityDetailsCommand { get; private set; }

        #endregion

        #region Implementation

        /// <summary>
        /// Initializes costs from the breakdown.
        /// </summary>
        public void InitializeBreakdown()
        {
            // Attach event handler for total cost changed before setting the cost, to raise event on initialization too
            this.TotalCost.ValueChanged += (s, e) => this.OnTotalCostChanged();

            this.SetupChart();
            this.SetupPartListDataGrid();

            this.ChartMode.ValueChanged += (s, e) => this.OnChartModeChanged();
        }

        /// <summary>
        /// Refreshes costs from the breakdown (called after the costs have changed).
        /// </summary>
        public void RefreshBreakdown()
        {
            this.SetupChart();
            this.SetupPartListDataGrid();
        }

        /// <summary>
        /// Gets the costs information displayed in the chart.
        /// </summary>
        private void SetupChart()
        {
            decimal totalCost;

            this.ChartItems.Value = this.ChartBuilder.GetPartListChart(out totalCost);
            this.TotalCost.Value = totalCost;

            this.IsEnhancedCostCalculationResult.Value = this.ChartBuilder.IsEnhancedCostCalculationResult;
        }

        /// <summary>
        /// Gets the items to display in the data grid.
        /// </summary>
        private void SetupPartListDataGrid()
        {
            if (this.ChartBuilder.CostCalculationResult != null)
            {
                int? amountValue = 0;
                amountValue += this.ChartBuilder.CostCalculationResult.PartsCost.PartAmountsSum;
                amountValue += this.ChartBuilder.CostCalculationResult.AssembliesCost.AssemblyAmountsSum;

                var assembly = this.Entity as Assembly;
                if (assembly != null)
                {
                    this.DataGridItems.Value = BuildPartListDataGridSource(assembly);
                }
            }
        }

        /// <summary>
        /// Gets the details of a data grid item.
        /// </summary>
        /// <param name="item">The item.</param>
        private void GetEntityDetails(PartListBreakdownGridItem item)
        {
            Assembly assy = item.Entity as Assembly;
            if (assy != null)
            {
                var assySubItems = CalculateAssemblyCost(assy);
                var partSubItems = CalculatePartsCost(assy);

                item.ItemDetails.AddRange(assySubItems);
                item.ItemDetails.AddRange(partSubItems);
            }
        }

        /// <summary>
        /// Called when the current chart mode changed, notifies the parent view model that the chart mode changed, to refresh it in all breakdowns.
        /// </summary>
        private void OnChartModeChanged()
        {
            var message = new NotificationMessage<ChartModes>(Notification.ResultDetailsChartModeChanged, this.ChartMode.Value);
            this.messenger.Send<NotificationMessage<ChartModes>>(message, this.MessengerToken);
        }

        /// <summary>
        /// Called when the total costs of the entity changed, checks if the entity has costs or not.
        /// </summary>
        private void OnTotalCostChanged()
        {
            this.EntityHasCosts.Value = this.TotalCost.Value.HasValue && this.TotalCost.Value != 0m;
        }

        #endregion

        #region MouseDoubleClick Event

        /// <summary>
        /// Handles the mouse double click event on a data grid item.
        /// </summary>
        /// <param name="item">The item.</param>
        private void HandleMouseDoubleClickEvent(PartListBreakdownGridItem item)
        {
            if (item != null)
            {
                this.NavigateToItem(item.Entity);
            }
        }

        /// <summary>
        /// Handles the mouse double click event on a row details tree item.
        /// </summary>
        /// <param name="item">The item.</param>
        private void HandleMouseDoubleClickEvent(PartListBreakdownRowDetailsItem item)
        {
            if (item != null)
            {
                this.NavigateToItem(item.Entity);
            }
        }

        /// <summary>
        /// Navigates in the project's tree, to the specified navigation target (an entity from data grid or row details).
        /// </summary>
        /// <param name="target">The target.</param>
        private void NavigateToItem(object target)
        {
            if (target != null)
            {
                NavigateToEntityMessage navMessage = new NavigateToEntityMessage(target, this.DatabaseManagerId);
                this.messenger.Send(navMessage, this.IsInViewerMode ? GlobalMessengerTokens.ModelBrowserTargetToken : GlobalMessengerTokens.MainViewTargetToken);
            }
        }

        #endregion

        #region DataGrid Source

        /// <summary>
        /// Builds the part-list data grid items source.
        /// </summary>
        /// <param name="assembly">The assembly from which to build it.</param>
        /// <returns>A list of objects for data binding.</returns>
        private List<PartListBreakdownGridItem> BuildPartListDataGridSource(Assembly assembly)
        {
            if (assembly == null)
            {
                return new List<PartListBreakdownGridItem>();
            }

            var gridItems = new List<PartListBreakdownGridItem>();
            foreach (AssemblyCost cost in this.ChartBuilder.CostCalculationResult.AssembliesCost.AssemblyCosts.OrderBy(c => c.AssemblyIndex).ThenBy(c => c.Name))
            {
                var item = new PartListBreakdownGridItem();
                item.Guid = cost.AssemblyId;
                item.Name = cost.Name;
                item.Number = cost.Number;
                item.Description = cost.Description;
                item.TargetPrice = cost.TargetPrice;
                item.AmountPerAssembly = cost.AmountPerAssembly;
                item.TargetCost = cost.TargetCost;
                item.TotalCost = item.TargetCost * item.AmountPerAssembly;
                item.EntityType = typeof(Assembly);
                item.TypeIcon = Images.AssemblyIcon;
                item.TypeName = LocalizedResources.General_Assembly;

                Assembly entityAssembly = this.GetSubassemblyOfAssembly(assembly, item.Guid);
                item.Entity = entityAssembly;
                gridItems.Add(item);
            }

            foreach (PartCost cost in this.ChartBuilder.CostCalculationResult.PartsCost.PartCosts.OrderBy(c => c.PartIndex).ThenBy(c => c.Name))
            {
                var item = new PartListBreakdownGridItem();
                item.Guid = cost.PartId;
                item.Name = cost.Name;
                item.Number = cost.Number;
                item.Description = cost.Description;
                item.TargetPrice = cost.TargetPrice;
                item.AmountPerAssembly = cost.AmountPerAssembly;
                item.TargetCost = cost.TargetCost;
                item.TotalCost = item.TargetCost * item.AmountPerAssembly;
                item.EntityType = typeof(Part);
                item.TypeIcon = Images.PartIcon;
                item.TypeName = LocalizedResources.General_Part;

                Part entityPart = this.GetPartOfAssembly(assembly, item.Guid);
                item.Entity = entityPart;
                gridItems.Add(item);
            }

            if (this.ChartBuilder.IsEnhancedCostCalculationResult)
            {
                int lastSortNumber = 1;
                OrderPartListDataGridSourceForEnhancedBreakdown(gridItems, this.NormalCalculationResult, string.Empty, ref lastSortNumber);
                gridItems = gridItems.OrderBy(item => item.OrderNumberSortValue).ToList();
            }

            return gridItems;
        }

        /// <summary>
        /// Gets the sub-assembly of a fully loaded assembly.
        /// </summary>
        /// <param name="parent">The parent assembly.</param>
        /// <param name="subAssemblyId">The id of the assembly we are looking for.</param>
        /// <returns>The sub-assembly, with the id specified by <paramref name="subAssemblyId"/>, or null if it is not found.</returns>
        private Assembly GetSubassemblyOfAssembly(Assembly parent, Guid subAssemblyId)
        {
            if (parent == null)
            {
                return null;
            }

            var child = parent.Subassemblies.FirstOrDefault(s => s.Guid == subAssemblyId);
            if (child != null)
            {
                return child;
            }
            else
            {
                foreach (var subAssembly in parent.Subassemblies)
                {
                    return this.GetSubassemblyOfAssembly(subAssembly, subAssemblyId);
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the part from an fully loaded assembly.
        /// </summary>
        /// <param name="parent">The parent assembly.</param>
        /// <param name="partId">The ID of the part we are looking for.</param>
        /// <returns>The part with the id specified by <paramref name="partId"/> or null if it is not found.</returns>
        private Part GetPartOfAssembly(Assembly parent, Guid partId)
        {
            if (parent == null)
            {
                return null;
            }

            var child = parent.Parts.FirstOrDefault(s => s.Guid == partId);
            if (child != null)
            {
                return child;
            }
            else
            {
                foreach (var subAssembly in parent.Subassemblies)
                {
                    return this.GetPartOfAssembly(subAssembly, partId);
                }
            }

            return null;
        }

        /// <summary>
        /// Assigns appropriate order number and sort number to all process steps items in a process data grid source list, for the enhanced breakdown.
        /// </summary>
        /// <param name="datagridSource">The process data grid source items.</param>
        /// <param name="originalResult">The original result from which the enhanced breakdown result was built.</param>
        /// <param name="orderPrefix">The prefix to apply to the order of items.</param>
        /// <param name="lastSortNumber">The last sort number assigned by this call. Used in recursive calls so set it to 1 in non-recursive calls.</param>
        private void OrderPartListDataGridSourceForEnhancedBreakdown(
            List<PartListBreakdownGridItem> datagridSource,
            CalculationResult originalResult,
            string orderPrefix,
            ref int lastSortNumber)
        {
            // The current order number
            int orderNumber = 1;

            foreach (AssemblyCost assyCost in originalResult.AssembliesCost.AssemblyCosts.OrderBy(c => c.AssemblyIndex).ThenBy(c => c.Name))
            {
                // Create the order string for each sub-assembly                
                string crtOrderNo = !string.IsNullOrEmpty(orderPrefix) ? orderPrefix + "." + orderNumber : orderNumber.ToString(CultureInfo.InvariantCulture);
                var partListItem = datagridSource.FirstOrDefault(item => item.EntityType == typeof(Assembly) && item.Guid == assyCost.AssemblyId);
                if (partListItem != null)
                {
                    partListItem.OrderNumberDisplayValue = crtOrderNo;
                    partListItem.OrderNumberSortValue = lastSortNumber;

                    lastSortNumber++;
                    orderNumber++;
                }

                string prefix = "  " + crtOrderNo;
                OrderPartListDataGridSourceForEnhancedBreakdown(datagridSource, assyCost.FullCalculationResult, prefix, ref lastSortNumber);
            }

            foreach (PartCost partCost in originalResult.PartsCost.PartCosts.OrderBy(c => c.PartIndex).ThenBy(c => c.Name))
            {
                // Create the order string for each sub-part                
                var partListItem = datagridSource.FirstOrDefault(item => item.EntityType == typeof(Part) && item.Guid == partCost.PartId);
                if (partListItem != null)
                {
                    partListItem.OrderNumberDisplayValue =
                        !string.IsNullOrEmpty(orderPrefix) ? orderPrefix + "." + orderNumber : orderNumber.ToString(CultureInfo.InvariantCulture);
                    partListItem.OrderNumberSortValue = lastSortNumber;

                    lastSortNumber++;
                    orderNumber++;
                }
            }
        }

        #endregion

        #region Calculate Costs

        /// <summary>
        /// Calculates the assembly cost.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <returns>List PartListObject entities which represents the subassemblies of assembly with their costs.</returns>
        private List<PartListBreakdownRowDetailsItem> CalculateAssemblyCost(Assembly assembly)
        {
            var assemblyCostList = new List<PartListBreakdownRowDetailsItem>();

            AssemblyCostCalculationParameters costCalculationParameters = null;
            if (this.ParentProject != null)
            {
                costCalculationParameters = CostCalculationHelper.CreateAssemblyParamsFromProject(this.ParentProject);
            }
            else if (this.IsInViewerMode)
            {
                costCalculationParameters = this.modelBrowserHelperService.GetAssemblyCostCalculationParameters();
            }

            foreach (Assembly subassembly in assembly.Subassemblies.Where(a => !a.IsDeleted).OrderBy(a => a.Index).ThenBy(a => a.Name))
            {
                var assemblyCostObject = new PartListBreakdownRowDetailsItem()
                {
                    Name = subassembly.Name,
                    Icon = Images.AssemblyIcon,
                    AmountPerAssembly = GetAssemblyAmountFromProcess(subassembly, assembly.Process),
                    Entity = subassembly
                };

                if (costCalculationParameters != null)
                {
                    ICostCalculator calculator = CostCalculatorFactory.GetCalculator(subassembly.CalculationVariant);
                    var calculationResult = calculator.CalculateAssemblyCost(assembly, costCalculationParameters);
                    assemblyCostObject.TargetCost = calculationResult.Summary.TargetCost;
                    assemblyCostObject.TotalCost = assemblyCostObject.TargetCost * assemblyCostObject.AmountPerAssembly;
                }

                var subassy = CalculateAssemblyCost(subassembly);
                var subpart = CalculatePartsCost(subassembly);

                assemblyCostObject.Children.AddRange(subassy);
                assemblyCostObject.Children.AddRange(subpart);

                assemblyCostList.Add(assemblyCostObject);
            }

            return assemblyCostList;
        }

        /// <summary>
        /// Calculates the cost of the parts in the specified assembly.
        /// </summary>
        /// <param name="assembly">The assembly for which to calculate the cost.</param>
        /// <returns> List PartListObject entities which represents the parts of assembly with their costs. </returns>
        private List<PartListBreakdownRowDetailsItem> CalculatePartsCost(Assembly assembly)
        {
            var partCostList = new List<PartListBreakdownRowDetailsItem>();

            PartCostCalculationParameters costCalculationParameters = null;
            if (this.ParentProject != null)
            {
                costCalculationParameters = CostCalculationHelper.CreatePartParamsFromProject(this.ParentProject);
            }
            else if (this.IsInViewerMode)
            {
                costCalculationParameters = this.modelBrowserHelperService.GetPartCostCalculationParameters();
            }

            foreach (Part part in assembly.Parts.Where(p => !p.IsDeleted).OrderBy(p => p.Index).ThenBy(p => p.Name))
            {
                var partCostObject = new PartListBreakdownRowDetailsItem()
                {
                    Name = part.Name,
                    Icon = Images.PartIcon,
                    AmountPerAssembly = GetPartAmountFromProcess(part, assembly.Process),
                    Entity = part
                };

                if (costCalculationParameters != null)
                {
                    ICostCalculator calculator = CostCalculatorFactory.GetCalculator(part.CalculationVariant);
                    var result = calculator.CalculatePartCost(part, costCalculationParameters);
                    partCostObject.TargetCost = result.Summary.TargetCost;
                    partCostObject.TotalCost = partCostObject.TargetCost * partCostObject.AmountPerAssembly;
                }

                partCostList.Add(partCostObject);
            }

            return partCostList;
        }

        /// <summary>
        /// Gets the part amount from process.
        /// </summary>
        /// <param name="part">The part for which it's calculated the amount.</param>
        /// <param name="process">The process in which the assembly is.</param>
        /// <returns>The amount of part</returns>
        private int GetPartAmountFromProcess(Part part, Process process)
        {
            var assemblingSteps = process.Steps.OfType<AssemblyProcessStep>();
            int amount = 0;

            foreach (AssemblyProcessStep step in assemblingSteps)
            {
                var partAmount = step.PartAmounts.FirstOrDefault(pa => pa.FindPartId() == part.Guid);
                if (partAmount != null && partAmount.Amount > 0)
                {
                    amount += partAmount.Amount;
                }
            }

            return amount;
        }

        /// <summary>
        /// Gets the assembly amount from process.
        /// </summary>
        /// <param name="assembly">The assembly for which it's calculated the amount.</param>
        /// <param name="process">The process in which the assembly is.</param>
        /// <returns> The amount of assembly.</returns>
        private int GetAssemblyAmountFromProcess(Assembly assembly, Process process)
        {
            var assemblingSteps = process.Steps.OfType<AssemblyProcessStep>();
            int amount = 0;

            foreach (AssemblyProcessStep step in assemblingSteps)
            {
                var assemblyAmount = step.AssemblyAmounts.FirstOrDefault(a => a.FindAssemblyId() == assembly.Guid);
                if (assemblyAmount != null && assemblyAmount.Amount > 0)
                {
                    amount += assemblyAmount.Amount;
                }
            }

            return amount;
        }

        #endregion
    }
}
