﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using ZPKTool.Business;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Controls;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.ViewModels.ResultDetails;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for investment breakdown tab from result details view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class InvestmentBreakdownViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The entity for which to compute and display the costs.
        /// </summary>
        private object entity;

        /// <summary>
        /// The measurement units adapter.
        /// </summary>
        private UnitsAdapter measurementUnitsAdapter;

        /// <summary>
        /// The chart builder, used for building chart items from cost calculation results.
        /// </summary>
        private CostChartBuilder chartBuilder;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="InvestmentBreakdownViewModel"/> class.
        /// </summary>
        /// <param name="messenger">The messenger.</param>
        [ImportingConstructor]
        public InvestmentBreakdownViewModel(IMessenger messenger)
        {
            Argument.IsNotNull("messenger", messenger);
            this.messenger = messenger;

            this.ChartItems.Value = new Collection<LabeledPieChartItem>();
            this.DataGridItems.Value = new Collection<InvestmentBreakdownGridItem>();

            this.CurrentUserName = SecurityManager.Instance.CurrentUser != null ? SecurityManager.Instance.CurrentUser.Name : string.Empty;

            this.MouseDoubleClickCommand = new DelegateCommand(this.NavigateToItem);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the entity for which to compute and display the costs.
        /// </summary>
        public object Entity
        {
            get
            {
                return this.entity;
            }

            set
            {
                Argument.IsNotNull("Entity", value);
                if (this.entity != value)
                {
                    var entityType = value.GetType();
                    if (entityType != typeof(Assembly) && entityType != typeof(Part) && entityType != typeof(RawPart))
                    {
                        throw new InvalidOperationException("The entity type: " + entityType.FullName + " is not supported. Entity must be Assembly, Part or RawPart.");
                    }

                    this.entity = value;
                    OnPropertyChanged(() => this.Entity);
                }
            }
        }

        /// <summary>
        /// Gets or sets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter
        {
            get
            {
                return this.measurementUnitsAdapter;
            }

            set
            {
                if (this.measurementUnitsAdapter != value)
                {
                    this.measurementUnitsAdapter = value;
                    OnPropertyChanged(() => this.MeasurementUnitsAdapter);
                }
            }
        }

        /// <summary>
        /// Gets or sets the chart builder, used for building chart items from cost calculation results.
        /// </summary>
        public CostChartBuilder ChartBuilder
        {
            get
            {
                return this.chartBuilder;
            }

            set
            {
                Argument.IsNotNull("ChartBuilder", value);
                if (this.chartBuilder != value)
                {
                    this.chartBuilder = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the identifier of database manager.
        /// </summary>
        public DbIdentifier DatabaseManagerId { get; set; }

        /// <summary>
        /// Gets or sets the normal cost calculation result.
        /// </summary>
        public CalculationResult NormalCalculationResult { get; set; }

        /// <summary>
        /// Gets or sets the token for sending message notifications for parent view model.
        /// </summary>
        public Guid MessengerToken { get; set; }

        /// <summary>
        /// Gets the name of the current logged-in user.
        /// </summary>
        public string CurrentUserName { get; private set; }

        /// <summary>
        /// Gets or sets the extra information for data grid reports.
        /// </summary>
        public ExtendedDataGridAdditionalReportInformation AdditionalReportInformation { get; set; }

        /// <summary>
        /// Gets or sets the visibility of tab containing the breakdown.
        /// </summary>
        public VMProperty<Visibility> TabVisibility { get; set; }

        /// <summary>
        /// Gets or sets the current chart mode.
        /// </summary>
        public VMProperty<ChartModes> ChartMode { get; set; }

        /// <summary>
        /// Gets the items to display in chart.
        /// </summary>
        public VMProperty<ICollection<LabeledPieChartItem>> ChartItems { get; private set; }

        /// <summary>
        /// Gets the total cost of entity.
        /// </summary>
        public VMProperty<decimal?> TotalCost { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the entity has costs (total value > 0) or not.
        /// </summary>
        public VMProperty<bool> EntityHasCosts { get; private set; }

        /// <summary>
        /// Gets the cost items displayed in the data grid.
        /// </summary>
        public VMProperty<ICollection<InvestmentBreakdownGridItem>> DataGridItems { get; private set; }

        /// <summary>
        /// Gets the selected cost item from the data grid.
        /// </summary>
        public VMProperty<InvestmentBreakdownGridItem> SelectedDataGridItem { get; private set; }

        #endregion

        #region Commands

        /// <summary>
        /// Gets the command for the mouse double click event in investment data grid.
        /// </summary>
        public ICommand MouseDoubleClickCommand { get; private set; }

        #endregion

        #region Implementation

        /// <summary>
        /// Initializes costs from the breakdown.
        /// </summary>
        public void InitializeBreakdown()
        {
            // Attach event handler for total cost changed before setting the cost, to raise event on initialization too
            this.TotalCost.ValueChanged += (s, e) => this.OnTotalCostChanged();

            this.SetupChart();
            this.SetupInvestmentDataGrid();

            this.ChartMode.ValueChanged += (s, e) => this.OnChartModeChanged();
        }

        /// <summary>
        /// Refreshes costs from the breakdown (called after the costs have changed).
        /// </summary>
        public void RefreshBreakdown()
        {
            this.SetupChart();
            this.SetupInvestmentDataGrid();
        }

        /// <summary>
        /// Gets the costs information displayed in the chart.
        /// </summary>
        private void SetupChart()
        {
            decimal totalCost;

            this.ChartItems.Value = this.ChartBuilder.GetInvestmentChart(out totalCost);
            this.TotalCost.Value = totalCost;
        }

        /// <summary>
        /// Gets the items to display in the data grid.
        /// </summary>
        private void SetupInvestmentDataGrid()
        {
            if (this.NormalCalculationResult != null)
            {
                var items = this.BuildInvestmentDataGridItemsSource(this.NormalCalculationResult);
                this.DataGridItems.Value = items.OrderBy(i => i.Name).ToList();
            }
        }

        /// <summary>
        /// Builds the investment data grid items source.
        /// </summary>
        /// <param name="result">The cost calculation result from which to extract investment data.</param>
        /// <returns>A list of objects for data binding.</returns>
        private List<InvestmentBreakdownGridItem> BuildInvestmentDataGridItemsSource(CalculationResult result)
        {
            var gridItems = new List<InvestmentBreakdownGridItem>();
            foreach (var singleCost in result.InvestmentCost.MachineInvestments.OrderBy(c => c.Name))
            {
                var item = new InvestmentBreakdownGridItem()
                {
                    TypeIcon = Images.MachineIcon,
                    TypeName = LocalizedResources.General_Machine,
                    ParentName = result.Name,
                    Name = singleCost.Name,
                    Investment = singleCost.Investment,
                    NeededAmount = singleCost.Amount,
                    TotalInvestment = singleCost.TotalInvestment,
                    CapacityUtilization =
                        (from step in result.ProcessCost.StepCosts
                         from machineCost in step.MachineCosts
                         where machineCost.MachineId == singleCost.ParentId
                         select machineCost).Select(
                             machineCost =>
                             machineCost.CapacityUtilization)
                         .FirstOrDefault(),
                    Id = singleCost.ParentId,
                    EntityType = typeof(Machine)
                };

                gridItems.Add(item);
            }

            foreach (var singleCost in result.InvestmentCost.DieInvestments.OrderBy(c => c.Name))
            {
                var item = new InvestmentBreakdownGridItem()
                {
                    TypeIcon = Images.DieIcon,
                    TypeName = LocalizedResources.General_DieToolingFixture,
                    ParentName = result.Name,
                    Name = singleCost.Name,
                    Investment = singleCost.Investment,
                    NeededAmount = singleCost.Amount,
                    TotalInvestment = singleCost.TotalInvestment,
                    Id = singleCost.ParentId,
                    EntityType = typeof(Die)
                };

                gridItems.Add(item);
            }

            if (this.ChartBuilder.IsEnhancedCostCalculationResult)
            {
                foreach (var partCost in result.PartsCost.PartCosts.OrderBy(c => c.PartIndex))
                {
                    var items = this.BuildInvestmentDataGridItemsSource(partCost.FullCalculationResult);
                    gridItems.AddRange(items);
                }

                foreach (var assyCost in result.AssembliesCost.AssemblyCosts.OrderBy(c => c.AssemblyIndex))
                {
                    var items = this.BuildInvestmentDataGridItemsSource(assyCost.FullCalculationResult);
                    gridItems.AddRange(items);
                }
            }

            return gridItems;
        }

        /// <summary>
        /// Navigates in the project's tree, to the selected item from the data grid.
        /// </summary>
        private void NavigateToItem()
        {
            if (this.SelectedDataGridItem.Value != null && this.SelectedDataGridItem.Value.Id != Guid.Empty)
            {
                object navigationTarget = null;

                // Create an entity with the id of whose investment and navigate to it
                if (this.SelectedDataGridItem.Value.EntityType == typeof(Machine))
                {
                    navigationTarget = new Machine() { Guid = this.SelectedDataGridItem.Value.Id };
                }
                else if (this.SelectedDataGridItem.Value.EntityType == typeof(Die))
                {
                    navigationTarget = new Die() { Guid = this.SelectedDataGridItem.Value.Id };
                }

                NavigateToEntityMessage navMessage = new NavigateToEntityMessage(navigationTarget, this.DatabaseManagerId);
                this.messenger.Send(navMessage, this.IsInViewerMode ? GlobalMessengerTokens.ModelBrowserTargetToken : GlobalMessengerTokens.MainViewTargetToken);
            }
        }

        /// <summary>
        /// Called when the current chart mode changed, notifies the parent view model that the chart mode changed, to refresh it in all breakdowns.
        /// </summary>
        private void OnChartModeChanged()
        {
            var message = new NotificationMessage<ChartModes>(Notification.ResultDetailsChartModeChanged, this.ChartMode.Value);
            this.messenger.Send<NotificationMessage<ChartModes>>(message, this.MessengerToken);
        }

        /// <summary>
        /// Called when the total costs of the entity changed, checks if the entity has costs or not.
        /// </summary>
        private void OnTotalCostChanged()
        {
            this.EntityHasCosts.Value = this.TotalCost.Value.HasValue && this.TotalCost.Value != 0m;
        }

        #endregion
    }
}
