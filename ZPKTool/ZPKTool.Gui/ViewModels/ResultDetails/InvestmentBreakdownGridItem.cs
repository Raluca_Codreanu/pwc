﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace ZPKTool.Gui.ViewModels.ResultDetails
{
    /// <summary>
    /// Item for data binding in the Investment breakdown data grid from the ResultDetails view.
    /// </summary>
    public class InvestmentBreakdownGridItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InvestmentBreakdownGridItem"/> class.
        /// </summary>
        public InvestmentBreakdownGridItem()
        {
            this.Id = Guid.Empty;
        }

        /// <summary>
        /// Gets or sets the icon corresponding to the type of entity the investment belongs to.
        /// </summary>
        public ImageSource TypeIcon { get; set; }

        /// <summary>
        /// Gets or sets the type name that replaces the type icon when exporting this item to xls/pdf file.
        /// </summary>
        /// <value>The name of the type.</value>
        public string TypeName { get; set; }

        /// <summary>
        /// Gets or sets the name of the parent assembly/part to which this investment's object belongs.
        /// </summary>
        public string ParentName { get; set; }

        /// <summary>
        /// Gets or sets the name of the entity whose this investment is.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the investment for one amount.
        /// </summary>
        public decimal Investment { get; set; }

        /// <summary>
        /// Gets or sets the needed amount.
        /// </summary>        
        public decimal NeededAmount { get; set; }

        /// <summary>
        /// Gets or sets the total investment.
        /// </summary>        
        public decimal TotalInvestment { get; set; }

        /// <summary>
        /// Gets or sets the capacity utilization.
        /// </summary>        
        public decimal? CapacityUtilization { get; set; }

        /// <summary>
        /// Gets or sets the id of the entity whose investment is.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the type of the entity whose investment is.
        /// </summary>
        public Type EntityType { get; set; }
    }
}
