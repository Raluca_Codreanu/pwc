﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace ZPKTool.Gui.ViewModels.ResultDetails
{
    /// <summary>
    /// A class used as an item for the tree view control in the part list breakdown, for row details.
    /// </summary>
    public class PartListBreakdownRowDetailsItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PartListBreakdownRowDetailsItem"/> class.
        /// </summary>
        public PartListBreakdownRowDetailsItem()
        {
            this.Children = new Collection<PartListBreakdownRowDetailsItem>();
        }

        /// <summary>
        /// Gets or sets the name of the assembly/part
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the icon of the item
        /// </summary>
        public ImageSource Icon { get; set; }

        /// <summary>
        /// Gets or sets the target cost of the assembly/part
        /// </summary>
        public decimal TargetCost { get; set; }

        /// <summary>
        /// Gets or sets the total cost of the assembly/part
        /// </summary>
        public decimal TotalCost { get; set; }

        /// <summary>
        /// Gets or sets the amount of assembly/part used
        /// </summary>
        public int AmountPerAssembly { get; set; }

        /// <summary>
        /// Gets or sets the assembly/part represented in the tree
        /// </summary>
        public object Entity { get; set; }
        
        /// <summary>
        /// Gets the children of this item.
        /// </summary>
        public Collection<PartListBreakdownRowDetailsItem> Children { get; private set; }
    }
}
