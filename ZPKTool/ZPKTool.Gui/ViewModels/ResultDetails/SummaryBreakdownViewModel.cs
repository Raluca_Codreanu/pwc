﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Globalization;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Common;
using ZPKTool.Controls;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.ViewModels.ResultDetails;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for summary breakdown tab from result details view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class SummaryBreakdownViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The composition container.
        /// </summary>
        private CompositionContainer compositionContainer;

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The entity for which to compute and display the costs.
        /// </summary>
        private object entity;

        /// <summary>
        /// The measurement units adapter.
        /// </summary>
        private UnitsAdapter measurementUnitsAdapter;

        /// <summary>
        /// The chart builder, used for building chart items from cost calculation results.
        /// </summary>
        private CostChartBuilder chartBuilder;

        /// <summary>
        /// Timer used to periodically check if the current part/assembly needs to be saved in the db.
        /// </summary>
        private DispatcherTimer saveChangesTimer;

        /// <summary>
        /// Indicates whether the current part/assembly needs to be saved in the db or not.
        /// </summary>
        private bool needsToBeSaved;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SummaryBreakdownViewModel"/> class.
        /// </summary>
        /// <param name="compositionContainer">The composition container.</param>
        /// <param name="messenger">The messenger.</param>
        /// <param name="windowService">The window service.</param>
        [ImportingConstructor]
        public SummaryBreakdownViewModel(
            CompositionContainer compositionContainer,
            IMessenger messenger,
            IWindowService windowService)
        {
            Argument.IsNotNull("compositionContainer", compositionContainer);
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("windowService", windowService);

            this.compositionContainer = compositionContainer;
            this.messenger = messenger;
            this.windowService = windowService;

            this.ChartItems.Value = new Collection<LabeledPieChartItem>();

            this.OpenTransportCostCalculatorCommand = new DelegateCommand(OpenTransportCostCalculator);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the entity for which to compute and display the costs.
        /// </summary>
        public object Entity
        {
            get
            {
                return this.entity;
            }

            set
            {
                Argument.IsNotNull("Entity", value);
                if (this.entity != value)
                {
                    var entityType = value.GetType();
                    if (entityType != typeof(Assembly) && entityType != typeof(Part) && entityType != typeof(RawPart))
                    {
                        throw new InvalidOperationException("The entity type: " + entityType.FullName + " is not supported. Entity must be Assembly, Part or RawPart.");
                    }

                    this.entity = value;
                    OnPropertyChanged(() => this.Entity);
                }
            }
        }

        /// <summary>
        /// Gets or sets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter
        {
            get
            {
                return this.measurementUnitsAdapter;
            }

            set
            {
                if (this.measurementUnitsAdapter != value)
                {
                    this.measurementUnitsAdapter = value;
                    OnPropertyChanged(() => this.MeasurementUnitsAdapter);
                }
            }
        }

        /// <summary>
        /// Gets or sets the chart builder, used for building chart items from cost calculation results.
        /// </summary>
        public CostChartBuilder ChartBuilder
        {
            get
            {
                return this.chartBuilder;
            }

            set
            {
                Argument.IsNotNull("ChartBuilder", value);
                if (this.chartBuilder != value)
                {
                    this.chartBuilder = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the data context associated with the entity.
        /// </summary>
        public IDataSourceManager DataSourceManager { get; set; }

        /// <summary>
        /// Gets or sets the token for sending message notifications for parent view model.
        /// </summary>
        public Guid MessengerToken { get; set; }

        /// <summary>
        /// Gets or sets the current chart mode.
        /// </summary>
        public VMProperty<ChartModes> ChartMode { get; set; }

        /// <summary>
        /// Gets the items to display in chart.
        /// </summary>
        public VMProperty<ICollection<LabeledPieChartItem>> ChartItems { get; private set; }

        /// <summary>
        /// Gets the total cost of entity.
        /// </summary>
        public VMProperty<decimal?> TotalCost { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the entity has costs (total value > 0) or not.
        /// </summary>
        public VMProperty<bool> EntityHasCosts { get; private set; }

        /// <summary>
        /// Gets the visibility of the additional costs group.
        /// </summary>
        public VMProperty<Visibility> AdditionalCostsVisibility { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the costs displayed are per total parts of entity or not (if not, costs are per part).
        /// </summary>
        public VMProperty<bool> AreCostsPerPartsTotal { get; private set; }

        /// <summary>
        /// Gets the total number of parts of entity for which the costs are displayed.
        /// </summary>
        public VMProperty<decimal> PartsTotalNo { get; private set; }

        /// <summary>
        /// Gets the entity's cost for development.
        /// </summary>
        public VMProperty<decimal?> DevelopmentCost { get; private set; }

        /// <summary>
        /// Gets the entity's cost for project invest.
        /// </summary>
        public VMProperty<decimal?> ProjectInvest { get; private set; }

        /// <summary>
        /// Gets the entity's cost for transport.
        /// </summary>
        public VMProperty<decimal?> TransportCost { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the entity has calculations for transport cost (in which case, the cost is calculated) or not.
        /// </summary>
        public VMProperty<bool> TransportCostCalculationsExist { get; private set; }

        /// <summary>
        /// Gets the entity's cost for packing.
        /// </summary>
        public VMProperty<decimal?> PackagingCost { get; private set; }

        /// <summary>
        /// Gets the entity's cost for logistic.
        /// </summary>
        public VMProperty<decimal?> LogisticCost { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the logistic cost for entity is calculated based on some formulas or not (in which case, are inputted by user).
        /// </summary>
        public VMProperty<bool> CalculateLogisticCost { get; private set; }

        /// <summary>
        /// Gets the entity's other costs.
        /// </summary>
        public VMProperty<decimal?> OtherCost { get; private set; }

        /// <summary>
        /// Gets the entity's payment terms (in days).
        /// </summary>
        public VMProperty<int?> PaymentTerms { get; private set; }

        /// <summary>
        /// Gets the entity's additional costs notes.
        /// </summary>
        public VMProperty<string> AdditionalCostsNotes { get; private set; }

        #endregion

        #region Commands

        /// <summary>
        /// Gets the command for opening the transport cost calculator view.
        /// </summary>
        public ICommand OpenTransportCostCalculatorCommand { get; private set; }

        #endregion

        #region Implementation

        /// <summary>
        /// Initializes costs from the breakdown.
        /// </summary>
        public void InitializeBreakdown()
        {
            // Check data source manager
            if (!this.IsInViewerMode && this.DataSourceManager == null)
            {
                throw new ArgumentNullException("DataSourceManager", "The data source manager cannot be null when view model is not in viewer mode.");
            }

            // Attach event handler for total cost changed before setting the cost, to raise event on initialization too
            this.TotalCost.ValueChanged += (s, e) => this.OnTotalCostChanged();

            this.SetupChart();

            // If the entity's cost is estimated or externally calculated, do not show the additional costs in summary
            PartCalculationAccuracy accuracy = PartCalculationAccuracy.FineCalculation;
            Assembly assy = this.Entity as Assembly;
            if (assy != null)
            {
                accuracy = EntityUtils.ConvertToEnum(assy.CalculationAccuracy, PartCalculationAccuracy.FineCalculation);
            }
            else
            {
                Part part = this.Entity as Part;
                if (part != null)
                {
                    accuracy = EntityUtils.ConvertToEnum(part.CalculationAccuracy, PartCalculationAccuracy.FineCalculation);
                }
            }

            if (accuracy == PartCalculationAccuracy.Estimation || accuracy == PartCalculationAccuracy.OfferOrExternalCalculation)
            {
                // Hide additional costs area
                this.AdditionalCostsVisibility.Value = Visibility.Collapsed;
            }
            else
            {
                // Additional costs are shown
                this.InitializeAdditionalCosts();
                this.InitializePropertyValueChangedHandlers();
                this.SetUpSaveTimer();
            }
        }

        /// <summary>
        /// Refreshes costs from the breakdown (called after the costs have changed).
        /// </summary>
        public void RefreshBreakdown()
        {
            this.CalculateLogisticCostValue();
            this.SetupChart();
        }

        /// <summary>
        /// Initializes the entity`s additional cost.
        /// </summary>
        private void InitializeAdditionalCosts()
        {
            Assembly assy = this.Entity as Assembly;
            if (assy != null)
            {
                this.DevelopmentCost.Value = assy.DevelopmentCost;
                this.ProjectInvest.Value = assy.ProjectInvest;
                this.OtherCost.Value = assy.OtherCost;
                this.TransportCost.Value = assy.TransportCost;
                this.PackagingCost.Value = assy.PackagingCost;
                this.CalculateLogisticCost.Value = assy.CalculateLogisticCost.GetValueOrDefault();
                this.PaymentTerms.Value = (int)assy.PaymentTerms;
                this.AdditionalCostsNotes.Value = assy.AdditionalCostsNotes;
                this.AreCostsPerPartsTotal.Value = assy.AreAdditionalCostsPerPartsTotal;
                this.TransportCostCalculationsExist.Value = assy.TransportCostCalculations.Count > 0;

                ICostCalculator costCalculator = CostCalculatorFactory.GetCalculator(assy.CalculationVariant);
                this.PartsTotalNo.Value = costCalculator.CalculateNetLifetimeProductionQuantity(assy);

                if (this.CalculateLogisticCost.Value)
                {
                    this.CalculateLogisticCostValue();
                }
                else
                {
                    this.LogisticCost.Value = assy.LogisticCost;
                }
            }
            else
            {
                Part part = this.Entity as Part;
                if (part != null)
                {
                    this.DevelopmentCost.Value = part.DevelopmentCost;
                    this.ProjectInvest.Value = part.ProjectInvest;
                    this.OtherCost.Value = part.OtherCost;
                    this.TransportCost.Value = part.TransportCost;
                    this.PackagingCost.Value = part.PackagingCost;
                    this.CalculateLogisticCost.Value = part.CalculateLogisticCost.GetValueOrDefault();
                    this.PaymentTerms.Value = (int)part.PaymentTerms;
                    this.AdditionalCostsNotes.Value = part.AdditionalCostsNotes;
                    this.AreCostsPerPartsTotal.Value = part.AreAdditionalCostsPerPartsTotal;
                    this.TransportCostCalculationsExist.Value = part.TransportCostCalculations.Count > 0;

                    ICostCalculator costCalculator = CostCalculatorFactory.GetCalculator(part.CalculationVariant);
                    this.PartsTotalNo.Value = costCalculator.CalculateNetLifetimeProductionQuantity(part);

                    if (this.CalculateLogisticCost.Value)
                    {
                        this.CalculateLogisticCostValue();
                    }
                    else
                    {
                        this.LogisticCost.Value = part.LogisticCost;
                    }
                }
            }
        }

        /// <summary>
        /// Calculates the value of logistic cost, if the value must be internally calculated and not read from user input.
        /// </summary>
        private void CalculateLogisticCostValue()
        {
            if (!this.CalculateLogisticCost.Value)
            {
                return;
            }

            decimal? logisticCost = this.ChartBuilder.CostCalculationResult != null ? this.ChartBuilder.CostCalculationResult.LogisticCost : 0;
            if (this.AreCostsPerPartsTotal.Value)
            {
                // If the logistic cost is auto-calculated and the additional costs are per parts total, we have to multiply it by the total number of parts per lifetime because it is calculated per part
                logisticCost *= this.PartsTotalNo.Value;
            }

            this.LogisticCost.Value = logisticCost;
        }

        /// <summary>
        /// Registers the value changed event for some properties.
        /// </summary>
        private void InitializePropertyValueChangedHandlers()
        {
            this.DevelopmentCost.ValueChanged += (s, e) => this.OnDevelopmentCostChanged();
            this.ProjectInvest.ValueChanged += (s, e) => this.OnProjectInvestChanged();
            this.OtherCost.ValueChanged += (s, e) => this.OnOtherCostChanged();
            this.TransportCost.ValueChanged += (s, e) => this.OnTransportCostChanged();
            this.PackagingCost.ValueChanged += (s, e) => this.OnPackagingCostChanged();
            this.LogisticCost.ValueChanged += (s, e) => this.OnLogisticCostChanged();
            this.CalculateLogisticCost.ValueChanged += (s, e) => this.OnCalculateLogisticCostChanged();
            this.PaymentTerms.ValueChanged += (s, e) => this.OnPaymentTermsChanged();
            this.AdditionalCostsNotes.ValueChanged += (s, e) => this.OnAdditionalCostsNotesChanged();
            this.AreCostsPerPartsTotal.ValueChanged += (s, e) => this.OnCostsPerPartsTotalChanged();
            this.ChartMode.ValueChanged += (s, e) => this.OnChartModeChanged();
        }

        /// <summary>
        /// Sets up timer that automatically saves the costs changes from GUI, without pressing a save button, with a delay of 5 seconds.
        /// </summary>
        private void SetUpSaveTimer()
        {
            saveChangesTimer = new DispatcherTimer();
            saveChangesTimer.Interval = new TimeSpan(0, 0, 0, 0, 500);
            saveChangesTimer.Tick += (s, e) =>
            {
                if (this.needsToBeSaved)
                {
                    this.SaveEntityChanges();
                    this.needsToBeSaved = false;
                }

                saveChangesTimer.Stop();
            };
        }

        /// <summary>
        /// Saves the changes of entity costs and sends a message to notify the parent view model that the costs have been updated.
        /// </summary>
        private void SaveEntityChanges()
        {
            try
            {
                Assembly assy = this.Entity as Assembly;
                if (assy != null)
                {
                    this.DataSourceManager.AssemblyRepository.Save(assy);
                    this.DataSourceManager.SaveChanges();
                }
                else
                {
                    Part part = this.Entity as Part;
                    if (part != null)
                    {
                        this.DataSourceManager.PartRepository.Save(part);
                        this.DataSourceManager.SaveChanges();
                    }
                }

                // Send a notification to result details, to notify that costs have been updated
                this.messenger.Send<NotificationMessage>(new NotificationMessage(Notification.ResultDetailsAdditionalCostsUpdated), this.MessengerToken);
            }
            catch (DataAccessException ex)
            {
                log.ErrorException("Caught a data exception.", ex);
                this.windowService.MessageDialogService.Show(ex);
            }
        }

        /// <summary>
        /// Opens the window for transport cost calculator and assigns the costs for transport and packing to the current entity.
        /// </summary>
        private void OpenTransportCostCalculator()
        {
            var transportCostCalculatorVM = this.compositionContainer.GetExportedValue<TransportCostCalculatorViewModel>();
            transportCostCalculatorVM.IsReadOnly = this.IsReadOnly;
            transportCostCalculatorVM.IsInViewerMode = this.IsInViewerMode;
            transportCostCalculatorVM.DataSourceManager = this.DataSourceManager;
            transportCostCalculatorVM.ParentEntity = this.Entity;

            this.windowService.ShowViewInDialog(transportCostCalculatorVM, "TransportCostCalculatorViewTemplate");

            if (!this.IsInViewerMode && transportCostCalculatorVM.Saved)
            {
                // If the changes made in transport cost calculator are saved, the new values for transport and packing cost are saved into result details view
                this.TransportCost.Value = transportCostCalculatorVM.SummaryViewModel.TotalTransportCostPerPcs;
                this.PackagingCost.Value = transportCostCalculatorVM.SummaryViewModel.TotalPackagingCostPerPcs;
                this.TransportCostCalculationsExist.Value = transportCostCalculatorVM.CalculationItems.Count > 0;
            }
        }

        /// <summary>
        /// Gets the costs information displayed in the chart.
        /// </summary>
        private void SetupChart()
        {
            decimal totalCost;

            this.ChartItems.Value = this.ChartBuilder.GetCostSummaryChart(out totalCost);
            this.TotalCost.Value = totalCost;
        }

        #endregion

        #region Value changed handlers

        /// <summary>
        /// Called when the value of development cost changed, automatically saves the value in the database.
        /// </summary>
        private void OnDevelopmentCostChanged()
        {
            Assembly assy = this.Entity as Assembly;
            if (assy != null)
            {
                assy.DevelopmentCost = this.DevelopmentCost.Value;
            }
            else
            {
                Part part = this.Entity as Part;
                if (part != null)
                {
                    part.DevelopmentCost = this.DevelopmentCost.Value;
                }
            }

            this.needsToBeSaved = true;
            saveChangesTimer.Stop();
            saveChangesTimer.Start();
        }

        /// <summary>
        /// Called when the value of project invest changed, automatically saves the value in the database.
        /// </summary>
        private void OnProjectInvestChanged()
        {
            Assembly assy = this.Entity as Assembly;
            if (assy != null)
            {
                assy.ProjectInvest = this.ProjectInvest.Value;
            }
            else
            {
                Part part = this.Entity as Part;
                if (part != null)
                {
                    part.ProjectInvest = this.ProjectInvest.Value;
                }
            }

            this.needsToBeSaved = true;
            saveChangesTimer.Stop();
            saveChangesTimer.Start();
        }

        /// <summary>
        /// Called when the value of other cost changed, automatically saves the value in the database.
        /// </summary>
        private void OnOtherCostChanged()
        {
            Assembly assy = this.Entity as Assembly;
            if (assy != null)
            {
                assy.OtherCost = this.OtherCost.Value;
            }
            else
            {
                Part part = this.Entity as Part;
                if (part != null)
                {
                    part.OtherCost = this.OtherCost.Value;
                }
            }

            this.needsToBeSaved = true;
            saveChangesTimer.Stop();
            saveChangesTimer.Start();
        }

        /// <summary>
        /// Called when the value of transport cost changed, automatically saves the value in the database.
        /// </summary>
        private void OnTransportCostChanged()
        {
            if (this.TransportCost.Value != null)
            {
                Assembly assy = this.Entity as Assembly;
                if (assy != null)
                {
                    assy.TransportCost = (decimal)this.TransportCost.Value;
                }
                else
                {
                    Part part = this.Entity as Part;
                    if (part != null)
                    {
                        part.TransportCost = (decimal)this.TransportCost.Value;
                    }
                }

                this.needsToBeSaved = true;
                saveChangesTimer.Stop();
                saveChangesTimer.Start();
            }
        }

        /// <summary>
        /// Called when the value of packing cost changed, automatically saves the value in the database.
        /// </summary>
        private void OnPackagingCostChanged()
        {
            Assembly assy = this.Entity as Assembly;
            if (assy != null)
            {
                assy.PackagingCost = this.PackagingCost.Value;
            }
            else
            {
                Part part = this.Entity as Part;
                if (part != null)
                {
                    part.PackagingCost = this.PackagingCost.Value;
                }
            }

            this.needsToBeSaved = true;
            saveChangesTimer.Stop();
            saveChangesTimer.Start();
        }

        /// <summary>
        /// Called when the value of logistic cost changed, automatically saves the value in the database.
        /// </summary>
        private void OnLogisticCostChanged()
        {
            if (!this.CalculateLogisticCost.Value)
            {
                Assembly assy = this.Entity as Assembly;
                if (assy != null)
                {
                    assy.LogisticCost = (decimal)this.LogisticCost.Value;
                }
                else
                {
                    Part part = this.Entity as Part;
                    if (part != null)
                    {
                        part.LogisticCost = (decimal)this.LogisticCost.Value;
                    }
                }

                this.needsToBeSaved = true;
                saveChangesTimer.Stop();
                saveChangesTimer.Start();
            }
        }

        /// <summary>
        /// Called when the value indicating whether the logistic cost is calculated or not changed, automatically saves the value in the database and calculates the logistic cost, if case.
        /// </summary>
        private void OnCalculateLogisticCostChanged()
        {
            Assembly assy = this.Entity as Assembly;
            if (assy != null)
            {
                assy.CalculateLogisticCost = this.CalculateLogisticCost.Value;
            }
            else
            {
                Part part = this.Entity as Part;
                if (part != null)
                {
                    part.CalculateLogisticCost = this.CalculateLogisticCost.Value;
                }
            }

            this.needsToBeSaved = true;
            saveChangesTimer.Stop();
            saveChangesTimer.Start();
        }

        /// <summary>
        /// Called when the value of payment terms changed, automatically saves the value in the database.
        /// </summary>
        private void OnPaymentTermsChanged()
        {
            if (this.PaymentTerms.Value == null)
            {
                this.PaymentTerms.Value = 0;
            }

            Assembly assy = this.Entity as Assembly;
            if (assy != null)
            {
                assy.PaymentTerms = (decimal)this.PaymentTerms.Value;
            }
            else
            {
                Part part = this.Entity as Part;
                if (part != null)
                {
                    part.PaymentTerms = (decimal)this.PaymentTerms.Value;
                }
            }

            this.needsToBeSaved = true;
            saveChangesTimer.Stop();
            saveChangesTimer.Start();
        }

        /// <summary>
        /// Called when the additional costs notes changed, automatically saves the value in the database.
        /// </summary>
        private void OnAdditionalCostsNotesChanged()
        {
            Assembly assy = this.Entity as Assembly;
            if (assy != null)
            {
                assy.AdditionalCostsNotes = this.AdditionalCostsNotes.Value;
            }
            else
            {
                Part part = this.Entity as Part;
                if (part != null)
                {
                    part.AdditionalCostsNotes = this.AdditionalCostsNotes.Value;
                }
            }

            this.needsToBeSaved = true;
            saveChangesTimer.Stop();
            saveChangesTimer.Start();
        }

        /// <summary>
        /// Called when the value indicating whether the costs are par part or per parts total changed, recalculates the costs and automatically saves the values in the database.
        /// </summary>
        private void OnCostsPerPartsTotalChanged()
        {
            this.needsToBeSaved = false;
            Assembly assy = this.Entity as Assembly;
            if (assy != null)
            {
                assy.AreAdditionalCostsPerPartsTotal = this.AreCostsPerPartsTotal.Value;
            }
            else
            {
                Part part = this.Entity as Part;
                if (part != null)
                {
                    part.AreAdditionalCostsPerPartsTotal = this.AreCostsPerPartsTotal.Value;
                }
            }

            if (this.PartsTotalNo.Value != 0)
            {
                if (this.AreCostsPerPartsTotal.Value)
                {
                    var namesOfInvalidCostsBuilder = new StringBuilder();

                    // Switching from Per Part to Per Parts Total => we have to multiply the costs by the Total Number of Parts in order to maintain the Part's total cost
                    this.DevelopmentCost.Value = this.ValidateNumberValue(this.DevelopmentCost.Value * this.PartsTotalNo.Value, Constants.MaxNumberValueAllowedInDb, LocalizedResources.General_DevelopmentCost, namesOfInvalidCostsBuilder);
                    this.ProjectInvest.Value = this.ValidateNumberValue(this.ProjectInvest.Value * this.PartsTotalNo.Value, Constants.MaxNumberValueAllowedInDb, LocalizedResources.General_ProjectInvest, namesOfInvalidCostsBuilder);
                    this.TransportCost.Value = this.ValidateNumberValue(this.TransportCost.Value * this.PartsTotalNo.Value, Constants.MaxNumberValueAllowedInDb, LocalizedResources.General_TransportCost, namesOfInvalidCostsBuilder);
                    this.PackagingCost.Value = this.ValidateNumberValue(this.PackagingCost.Value * this.PartsTotalNo.Value, Constants.MaxNumberValueAllowedInDb, LocalizedResources.General_CostOfPackaging, namesOfInvalidCostsBuilder);
                    this.OtherCost.Value = this.ValidateNumberValue(this.OtherCost.Value * this.PartsTotalNo.Value, Constants.MaxNumberValueAllowedInDb, LocalizedResources.General_OtherCost, namesOfInvalidCostsBuilder);
                    if (!this.CalculateLogisticCost.Value)
                    {
                        this.LogisticCost.Value = this.ValidateNumberValue(this.LogisticCost.Value * this.PartsTotalNo.Value, Constants.MaxNumberValueAllowedInDb, LocalizedResources.General_LogisticCost, namesOfInvalidCostsBuilder);
                    }

                    var invalidCostsNames = namesOfInvalidCostsBuilder.Length > 2 ? namesOfInvalidCostsBuilder.ToString(0, namesOfInvalidCostsBuilder.Length - 2) : namesOfInvalidCostsBuilder.ToString();
                    if (!string.IsNullOrWhiteSpace(invalidCostsNames))
                    {
                        this.windowService.MessageDialogService.Show(string.Format(CultureInfo.CurrentUICulture, LocalizedResources.Error_InvalidAdditionalCostValues, invalidCostsNames), MessageDialogType.Warning);
                    }
                }
                else
                {
                    // Switching from Per Parts Total to Per Part => we have to divide the costs by the Total Number of Parts in order to maintain the Part's total cost
                    this.DevelopmentCost.Value = this.DevelopmentCost.Value / this.PartsTotalNo.Value;
                    this.ProjectInvest.Value = this.ProjectInvest.Value / this.PartsTotalNo.Value;
                    this.TransportCost.Value = this.TransportCost.Value / this.PartsTotalNo.Value;
                    this.PackagingCost.Value = this.PackagingCost.Value / this.PartsTotalNo.Value;
                    this.OtherCost.Value = this.OtherCost.Value / this.PartsTotalNo.Value;
                    if (!this.CalculateLogisticCost.Value)
                    {
                        this.LogisticCost.Value = this.LogisticCost.Value / this.PartsTotalNo.Value;
                    }
                }
            }

            this.needsToBeSaved = true;
            saveChangesTimer.Stop();
            saveChangesTimer.Start();
        }

        /// <summary>
        /// Called when the total costs of the entity changed, checks if the entity has costs or not.
        /// </summary>
        private void OnTotalCostChanged()
        {
            this.EntityHasCosts.Value = this.TotalCost.Value.HasValue && this.TotalCost.Value != 0m;
        }

        /// <summary>
        /// Called when the current chart mode changed, it notifies the sub view models to update their charts.
        /// </summary>
        private void OnChartModeChanged()
        {
            var message = new NotificationMessage<ChartModes>(Notification.ResultDetailsChartModeChanged, this.ChartMode.Value);
            this.messenger.Send<NotificationMessage<ChartModes>>(message, this.MessengerToken);
        }

        /// <summary>
        /// Validates the input value against the maximum value allowed; if the value exceeds the maximum, the cost name is appended to the list of invalid cost names.
        /// </summary>
        /// <param name="inputValue">The input value.</param>
        /// <param name="maxContentLimit">The maximum content limit.</param>
        /// <param name="costName">Name of the cost.</param>
        /// <param name="invalidCostNamesBuilder">The invalid cost names builder.</param>
        /// <returns>The valid value.</returns>
        private decimal ValidateNumberValue(decimal? inputValue, decimal maxContentLimit, string costName, StringBuilder invalidCostNamesBuilder)
        {
            var result = 0m;
            if (inputValue.HasValue)
            {
                if (inputValue.Value <= maxContentLimit)
                {
                    result = inputValue.Value;
                }
                else
                {
                    result = maxContentLimit;
                    invalidCostNamesBuilder.AppendFormat(CultureInfo.CurrentUICulture, @"{0}, ", costName);
                }
            }
            else
            {
                result = 0m;
            }

            return result;
        }

        #endregion
    }
}
