﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using ZPKTool.Data;

namespace ZPKTool.Gui.ViewModels.ResultDetails
{
    /// <summary>
    /// This class is used for data binding in the Part List data grid in the ResultDetails view.
    /// </summary>
    public class PartListBreakdownGridItem : IIdentifiable, INameable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PartListBreakdownGridItem"/> class.
        /// </summary>
        public PartListBreakdownGridItem()
        {
            this.ItemDetails = new List<PartListBreakdownRowDetailsItem>();
        }

        /// <summary>
        /// Gets or sets the entity behind the list item. It can be an Assembly or a Part.
        /// </summary>
        public object Entity { get; set; }

        /// <summary>
        /// Gets or sets the type of the entity represented by this item.
        /// </summary>
        public Type EntityType { get; set; }

        /// <summary>
        /// Gets or sets the icon displayed for the grid row corresponding to this instance.
        /// </summary>
        public ImageSource TypeIcon { get; set; }

        /// <summary>
        /// Gets or sets the type name that replaces the type icon when exporting this item to xls/pdf file.
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the object
        /// </summary>
        /// <value>The id of the object.</value>
        public Guid Guid { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name of the entity object.</value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the number.
        /// </summary>
        /// <value>The number.</value>
        public string Number { get; set; }

        /// <summary>
        /// Gets or sets the amount per assembly.
        /// </summary>
        /// <value>The amount per assembly.</value>
        public int? AmountPerAssembly { get; set; }

        /// <summary>
        /// Gets or sets the cost of 1 part/assembly.
        /// </summary>
        /// <value>The target cost.</value>
        public decimal? TargetCost { get; set; }

        /// <summary>
        /// Gets or sets the total cost.
        /// </summary>
        /// <value>The total cost.</value>
        public decimal? TotalCost { get; set; }

        /// <summary>
        /// Gets or sets the part description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the target price.
        /// </summary>
        public decimal? TargetPrice { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the Part represented by this instance is external.
        /// </summary>
        public bool IsExternal { get; set; }

        /// <summary>
        /// Gets or sets the value displayed as order number.
        /// </summary>
        /// <value>The order number display value.</value>
        public string OrderNumberDisplayValue { get; set; }

        /// <summary>
        /// Gets or sets the value used when sorting by order number.
        /// </summary>        
        public int OrderNumberSortValue { get; set; }

        /// <summary>
        /// Gets or sets the details of a data grid item.
        /// </summary>       
        public ICollection<PartListBreakdownRowDetailsItem> ItemDetails { get; set; }
    }
}
