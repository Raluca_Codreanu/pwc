﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Calculations.CostCalculation;

namespace ZPKTool.Gui.ViewModels.ResultDetails
{
    /// <summary>
    /// Item for data binding in the Assembling/Manufacturing data grid from the ResultDetails view.
    /// </summary>
    public class ProcessBreakdownGridItem
    {
        /// <summary>
        /// Gets or sets the process step cost displayed in the item.
        /// </summary>
        public ProcessStepCost StepCost { get; set; }

        /// <summary>
        /// Gets or sets the order number displayed in the "No." column.
        /// </summary>
        public string OrderNumber { get; set; }

        /// <summary>
        /// Gets or sets the unique number assigned for sorting.
        /// </summary>
        public int SortNumber { get; set; }
    }
}
