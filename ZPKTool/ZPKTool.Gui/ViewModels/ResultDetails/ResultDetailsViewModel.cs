﻿using System;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Windows;
using System.Windows.Input;
using Microsoft.Win32;
using ZPKTool.Business;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Reporting;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Utils;
using ZPKTool.Gui.ViewModels.ResultDetails;
using ZPKTool.Gui.Views.ResultDetails;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for the ResultDetails view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ResultDetailsViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Weak event listener for the PropertyChanged notification
        /// </summary>
        private WeakEventListener<PropertyChangedEventArgs> propertyChangedListener;

        /// <summary>
        /// The composition container.
        /// </summary>
        private CompositionContainer compositionContainer;

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The service that provides measurement units and currency data.
        /// </summary>
        private IUnitsService unitsService;

        /// <summary>
        /// The model browser helper service.
        /// </summary>
        private IModelBrowserHelperService modelBrowserHelperService;

        /// <summary>
        /// The view for selecting and creating an entity report.
        /// </summary>
        private ReportSelectionView reportSelection;

        /// <summary>
        /// The cost calculation result currently displayed by this view. Is either the normal or the enhanced result.
        /// </summary>
        private CalculationResult currentCalculationResult;

        /// <summary>
        /// The calculation result as it was originally calculated. Used to display the normal breakdown view and for creating reports.
        /// </summary>
        private CalculationResult normalCalculationResult;

        /// <summary>
        /// The enhanced calculation result created based on the normal calculation result. Used to display the enhanced breakdown view.
        /// </summary>
        private CalculationResult enhancedCalculationResult;

        /// <summary>
        /// The reports manager that handles reports creation.
        /// </summary>
        private ReportsManager reportsManager;

        /// <summary>
        /// Used for building chart items from cost calculation results.
        /// </summary>
        private CostChartBuilder chartBuilder;

        /// <summary>
        /// A value indicating whether the raw materials breakdown has been initialized or not.
        /// </summary>
        private bool isRawMaterialsBreakdownInitialized;

        /// <summary>
        /// The token for receiving message notifications from sub-view models.
        /// </summary>
        private Guid messengerToken;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ResultDetailsViewModel"/> class.
        /// </summary>
        /// <param name="compositionContainer">The composition container.</param>
        /// <param name="messenger">The messenger.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="unitsService">The units service.</param>
        /// <param name="modelBrowserHelperService">The model browser helper service.</param>
        [ImportingConstructor]
        public ResultDetailsViewModel(
            CompositionContainer compositionContainer,
            IMessenger messenger,
            IWindowService windowService,
            IUnitsService unitsService,
            IModelBrowserHelperService modelBrowserHelperService)
        {
            Argument.IsNotNull("compositionContainer", compositionContainer);
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("unitsService", unitsService);
            Argument.IsNotNull("modelBrowserHelperService", modelBrowserHelperService);

            this.compositionContainer = compositionContainer;
            this.messenger = messenger;
            this.windowService = windowService;
            this.unitsService = unitsService;
            this.modelBrowserHelperService = modelBrowserHelperService;

            this.InitializeCommands();

            // Register to notification messages
            this.messengerToken = Guid.NewGuid();
            this.messenger.Register<NotificationMessage>(this.HandleNotificationMessage, this.messengerToken);
            this.messenger.Register<NotificationMessage<ChartModes>>(this.HandleChartModeChangedNotification, this.messengerToken);
        }

        #endregion

        #region Generic Properties

        /// <summary>
        /// Gets or sets the entity for which to compute and display the costs.
        /// </summary>
        public object Entity { get; set; }

        /// <summary>
        /// Gets or sets the project to which the entity belongs.
        /// </summary>
        public Project ParentProject { get; set; }

        /// <summary>
        /// Gets or sets the data context associated with the entity.
        /// </summary>
        public IDataSourceManager DataSourceManager { get; set; }

        /// <summary>
        /// Gets the identifier of database manager.
        /// </summary>
        public VMProperty<DbIdentifier> DatabaseManagerId { get; private set; }

        /// <summary>
        /// Gets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter { get; private set; }

        /// <summary>
        /// Gets the extra information for data grid reports from breakdowns.
        /// </summary>
        public ExtendedDataGridAdditionalReportInformation AdditionalReportInformation { get; private set; }

        /// <summary>
        /// Gets the selected index of breakdown tab from the view.
        /// </summary>
        public VMProperty<int> SelectedTabIndex { get; private set; }

        /// <summary>
        /// Gets the current chart mode, common to all breakdowns.
        /// </summary>
        public VMProperty<ChartModes> ChartMode { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the version and last changed date of entity must be displayed or not (it's a user setting). 
        /// </summary>
        public VMProperty<bool> DisplayVersionAndTimestamp { get; private set; }

        /// <summary>
        /// Gets the entity's calculation variant.
        /// </summary>
        public VMProperty<string> CalculationVariant { get; private set; }

        /// <summary>
        /// Gets the entity's last modified date.
        /// </summary>
        public VMProperty<string> LastModifiedDate { get; private set; }

        /// <summary>
        /// Gets the entity's version.
        /// </summary>
        public VMProperty<string> Version { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the enhanced breakdown is enabled or not. 
        /// </summary>
        public VMProperty<bool> IsEnhancedBreakdownEnabled { get; private set; }

        /// <summary>
        /// Gets the visibility for the enhanced breakdown checkbox and breakdown section in reports generator view.
        /// </summary>
        public VMProperty<Visibility> EnhancedBreakdownVisibility { get; private set; }

        /// <summary>
        /// Gets the visibility for report creation status details.
        /// </summary>
        public VMProperty<Visibility> ReportStatusVisibility { get; private set; }

        /// <summary>
        /// Gets the path to last created report.
        /// </summary>
        public VMProperty<string> LastCreatedReportPath { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the entity has data check errors or not.
        /// </summary>
        public VMProperty<bool> EntityHasErrors { get; private set; }

        /// <summary>
        /// Gets the entity data check result with all entity errors.
        /// </summary>
        public VMProperty<EntityDataCheckResult> DataCheckResult { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the window with the data check results is opened or not.
        /// </summary>
        public VMProperty<bool> ShowEntityDataCheckResult { get; private set; }

        #endregion

        #region Breakdown Properties

        /// <summary>
        /// Gets the summary breakdown view model.
        /// </summary>
        [Import]
        public SummaryBreakdownViewModel SummaryBreakdown { get; private set; }

        /// <summary>
        /// Gets the overhead and margin breakdown view model.
        /// </summary>
        [Import]
        public OverheadAndMarginBreakdownViewModel OhAndMarginBreakdown { get; private set; }

        /// <summary>
        /// Gets the process breakdown view model.
        /// </summary>
        [Import]
        public ProcessBreakdownViewModel ProcessBreakdown { get; private set; }

        /// <summary>
        /// Gets the process steps breakdown view model.
        /// </summary>
        [Import]
        public ProcessStepsBreakdownViewModel ProcessStepsBreakdown { get; private set; }

        /// <summary>
        /// Gets the commodities breakdown view model.
        /// </summary>
        [Import]
        public CommoditiesBreakdownViewModel CommoditiesBreakdown { get; private set; }

        /// <summary>
        /// Gets the consumables breakdown view model.
        /// </summary>
        [Import]
        public ConsumablesBreakdownViewModel ConsumablesBreakdown { get; private set; }

        /// <summary>
        /// Gets the raw materials breakdown view model.
        /// </summary>
        [Import]
        public RawMaterialsBreakdownViewModel RawMaterialsBreakdown { get; private set; }

        /// <summary>
        /// Gets the part list breakdown view model.
        /// </summary>
        [Import]
        public PartListBreakdownViewModel PartListBreakdown { get; private set; }

        /// <summary>
        /// Gets the BOM breakdown view model.
        /// </summary>
        [Import]
        public BomBreakdownViewModel BomBreakdown { get; private set; }

        /// <summary>
        /// Gets the investment breakdown view model.
        /// </summary>
        [Import]
        public InvestmentBreakdownViewModel InvestmentBreakdown { get; private set; }

        #endregion

        #region Commands

        /// <summary>
        /// Gets the command to open the panel with calculation errors.
        /// </summary>
        public ICommand ShowEntityDataCheckResultCommand { get; private set; }

        /// <summary>
        /// Gets the command to open the reports selection view.
        /// </summary>
        public ICommand OpenReportSelectionCommand { get; private set; }

        /// <summary>
        /// Gets the command to close the reports selection view.
        /// </summary>
        public ICommand CloseReportSelectionCommand { get; private set; }

        /// <summary>
        /// Gets the command to create a regular Excel report.
        /// </summary>
        public ICommand RegularXlsReportCommand { get; private set; }

        /// <summary>
        /// Gets the command to create a regular PDF report.
        /// </summary>
        public ICommand RegularPdfReportCommand { get; private set; }

        /// <summary>
        /// Gets the command to create an enhanced Excel report.
        /// </summary>
        public ICommand EnhancedXlsReportCommand { get; private set; }

        /// <summary>
        /// Gets the command to create an enhanced PDF report.
        /// </summary>
        public ICommand EnhancedPdfReportCommand { get; private set; }

        /// <summary>
        /// Gets the command to open the last created report.
        /// </summary>
        public ICommand OpenLastCreatedReportCommand { get; private set; }

        #endregion

        #region Initialization

        /// <summary>
        /// Called after the view has been loaded, performing initialization of properties.
        /// <para />
        /// During unit tests this method must be manually called because there is no view to call it.
        /// </summary>
        public override void OnLoaded()
        {
            base.OnLoaded();
            this.CheckData();

            // Create instance of chart builder
            this.chartBuilder = CostChartBuilder.CreateInstanceForType(this.Entity.GetType());

            // Set properties related to data source manager
            this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(this.DataSourceManager);
            this.DatabaseManagerId.Value = this.DataSourceManager != null ? this.DataSourceManager.DatabaseId : DbIdentifier.NotSet;

            this.reportsManager = this.IsInViewerMode ? new ReportsManager(modelBrowserHelperService, this.MeasurementUnitsAdapter) : new ReportsManager(this.DatabaseManagerId.Value, this.MeasurementUnitsAdapter);

            // Set common properties related to entity
            this.AdditionalReportInformation = new ExtendedDataGridAdditionalReportInformation();
            this.AdditionalReportInformation.ProjectName += this.ParentProject != null ? " " + this.ParentProject.Name : string.Empty;

            string calculationVariant = null;

            Assembly assy = this.Entity as Assembly;
            if (assy != null)
            {
                this.AdditionalReportInformation.ParentName += " " + assy.Name;
                this.AdditionalReportInformation.Name += " " + LocalizedResources.General_ResultDetails + " (" + assy.Name + ")";
                this.AdditionalReportInformation.Version += " " + assy.Version;

                calculationVariant = assy.CalculationVariant;
                this.EnhancedBreakdownVisibility.Value = Visibility.Visible;
            }
            else
            {
                Part part = this.Entity as Part;
                if (part != null)
                {
                    this.AdditionalReportInformation.ParentName += " " + part.Name;
                    this.AdditionalReportInformation.Name += " " + LocalizedResources.General_ResultDetails + " (" + part.Name + ")";
                    this.AdditionalReportInformation.Version += " " + part.Version;

                    calculationVariant = part.CalculationVariant;
                    this.EnhancedBreakdownVisibility.Value = Visibility.Collapsed;
                }
            }

            this.CalculationVariant.Value = !string.IsNullOrEmpty(calculationVariant) ? calculationVariant : CostCalculatorFactory.OldestVersion;

            // Read settings from preferences for DisplayVersionAndTimestamp
            this.DisplayVersionAndTimestamp.Value = UserSettingsManager.Instance.DisplayVersionAndTimestamp;
            this.GetEntityVersionAndTimestamp();

            // Read settings from preferences for current chart mode
            this.GetDefaultChartMode();

            // Add listener to DisplayVersionAndTimestamp and DefaultChartViewMode user settings changed
            this.propertyChangedListener = new WeakEventListener<PropertyChangedEventArgs>(OnUserSettingsChanged);
            PropertyChangedEventManager.AddListener(UserSettingsManager.Instance, propertyChangedListener, ReflectionUtils.GetPropertyName(() => UserSettingsManager.Instance.DisplayVersionAndTimestamp));
            PropertyChangedEventManager.AddListener(UserSettingsManager.Instance, propertyChangedListener, ReflectionUtils.GetPropertyName(() => UserSettingsManager.Instance.DefaultChartViewMode));

            this.CalculateAndDisplayCost();
            this.InitializeBreakdowns();

            // Add handlers to value changed events
            this.IsEnhancedBreakdownEnabled.ValueChanged += (s, e) => OnEnhancedBreakdownChanged();
            this.ChartMode.ValueChanged += (s, e) => RefreshChartModeOnBreakdowns();
        }

        /// <summary>
        /// Initializes all breakdown view models.
        /// </summary>
        private void InitializeBreakdowns()
        {
            this.chartBuilder.CostCalculationResult = this.currentCalculationResult;

            PartCalculationAccuracy accuracy = PartCalculationAccuracy.FineCalculation;
            Assembly assy = this.Entity as Assembly;
            if (assy != null)
            {
                accuracy = EntityUtils.ConvertToEnum(assy.CalculationAccuracy, PartCalculationAccuracy.FineCalculation);
                this.RawMaterialsBreakdown.TabVisibility.Value = Visibility.Collapsed;
            }
            else
            {
                Part part = this.Entity as Part;
                if (part != null)
                {
                    accuracy = EntityUtils.ConvertToEnum(part.CalculationAccuracy, PartCalculationAccuracy.FineCalculation);
                }
            }

            this.InitializeSummaryBreakdown();

            if (accuracy != PartCalculationAccuracy.OfferOrExternalCalculation)
            {
                this.InitializeOhAndMarginBreakdown();
            }
            else
            {
                // For parts/assemblies with accuracy "Offer/External Calculation" the Overhead and Margin breakdown is always empty so there is no point in refreshing or showing it
                this.OhAndMarginBreakdown.TabVisibility.Value = Visibility.Collapsed;
            }

            if (accuracy == PartCalculationAccuracy.FineCalculation)
            {
                this.InitializeProcessBreakdown();
                this.InitializeProcessStepsBreakdown();
                this.InitializeCommoditiesBreakdown();
                this.InitializeConsumablesBreakdown();
                this.InitializeRawMaterialsBreakdown();
                this.InitializePartListBreakdown();
                this.InitializeBomBreakdown();
                this.InitializeInvestmentBreakdown();
            }
            else
            {
                // For parts/assemblies with accuracy not "Fine Calculation" the rest of the breakdowns are always empty so there is no point in refreshing or showing them
                this.ProcessBreakdown.TabVisibility.Value = Visibility.Collapsed;
                this.ProcessStepsBreakdown.TabVisibility.Value = Visibility.Collapsed;
                this.CommoditiesBreakdown.TabVisibility.Value = Visibility.Collapsed;
                this.ConsumablesBreakdown.TabVisibility.Value = Visibility.Collapsed;
                this.RawMaterialsBreakdown.TabVisibility.Value = Visibility.Collapsed;
                this.PartListBreakdown.TabVisibility.Value = Visibility.Collapsed;
                this.BomBreakdown.TabVisibility.Value = Visibility.Collapsed;
                this.InvestmentBreakdown.TabVisibility.Value = Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Initializes the summary breakdown view model.
        /// </summary>
        private void InitializeSummaryBreakdown()
        {
            this.SummaryBreakdown.DataSourceManager = this.DataSourceManager;
            this.SummaryBreakdown.MeasurementUnitsAdapter = this.MeasurementUnitsAdapter;
            this.SummaryBreakdown.Entity = this.Entity;
            this.SummaryBreakdown.ChartBuilder = this.chartBuilder;
            this.SummaryBreakdown.MessengerToken = this.messengerToken;
            this.SummaryBreakdown.ChartMode.Value = this.ChartMode.Value;
            this.SummaryBreakdown.IsInViewerMode = this.IsInViewerMode;
            this.SummaryBreakdown.IsReadOnly = this.IsReadOnly ? true : !SecurityManager.Instance.CurrentUserHasRight(Right.ProjectsAndCalculate);
            this.SummaryBreakdown.InitializeBreakdown();
        }

        /// <summary>
        /// Initializes the overhead and margin breakdown view model.
        /// </summary>
        private void InitializeOhAndMarginBreakdown()
        {
            this.OhAndMarginBreakdown.MeasurementUnitsAdapter = this.MeasurementUnitsAdapter;
            this.OhAndMarginBreakdown.Entity = this.Entity;
            this.OhAndMarginBreakdown.ChartBuilder = this.chartBuilder;
            this.OhAndMarginBreakdown.MessengerToken = this.messengerToken;
            this.OhAndMarginBreakdown.ChartMode.Value = this.ChartMode.Value;
            this.OhAndMarginBreakdown.IsInViewerMode = this.IsInViewerMode;
            this.OhAndMarginBreakdown.InitializeBreakdown();
        }

        /// <summary>
        /// Initializes the process breakdown view model.
        /// </summary>
        private void InitializeProcessBreakdown()
        {
            this.ProcessBreakdown.DatabaseManagerId = this.DatabaseManagerId.Value;
            this.ProcessBreakdown.MeasurementUnitsAdapter = this.MeasurementUnitsAdapter;
            this.ProcessBreakdown.Entity = this.Entity;
            this.ProcessBreakdown.ChartBuilder = this.chartBuilder;
            this.ProcessBreakdown.NormalCalculationResult = this.normalCalculationResult;
            this.ProcessBreakdown.AdditionalReportInformation = this.AdditionalReportInformation;
            this.ProcessBreakdown.MessengerToken = this.messengerToken;
            this.ProcessBreakdown.ChartMode.Value = this.ChartMode.Value;
            this.ProcessBreakdown.IsInViewerMode = this.IsInViewerMode;
            this.ProcessBreakdown.InitializeBreakdown();
        }

        /// <summary>
        /// Initializes the process steps breakdown view model.
        /// </summary>
        private void InitializeProcessStepsBreakdown()
        {
            this.ProcessStepsBreakdown.MeasurementUnitsAdapter = this.MeasurementUnitsAdapter;
            this.ProcessStepsBreakdown.Entity = this.Entity;
            this.ProcessStepsBreakdown.ChartBuilder = this.chartBuilder;
            this.ProcessStepsBreakdown.MessengerToken = this.messengerToken;
            this.ProcessStepsBreakdown.ChartMode.Value = this.ChartMode.Value;
            this.ProcessStepsBreakdown.InitializeBreakdown();
        }

        /// <summary>
        /// Initializes the commodities breakdown view model.
        /// </summary>
        private void InitializeCommoditiesBreakdown()
        {
            this.CommoditiesBreakdown.DatabaseManagerId = this.DatabaseManagerId.Value;
            this.CommoditiesBreakdown.MeasurementUnitsAdapter = this.MeasurementUnitsAdapter;
            this.CommoditiesBreakdown.Entity = this.Entity;
            this.CommoditiesBreakdown.ChartBuilder = this.chartBuilder;
            this.CommoditiesBreakdown.NormalCalculationResult = this.normalCalculationResult;
            this.CommoditiesBreakdown.AdditionalReportInformation = this.AdditionalReportInformation;
            this.CommoditiesBreakdown.MessengerToken = this.messengerToken;
            this.CommoditiesBreakdown.ChartMode.Value = this.ChartMode.Value;
            this.CommoditiesBreakdown.IsInViewerMode = this.IsInViewerMode;
            this.CommoditiesBreakdown.InitializeBreakdown();
        }

        /// <summary>
        /// Initializes the consumables breakdown view model.
        /// </summary>
        private void InitializeConsumablesBreakdown()
        {
            this.ConsumablesBreakdown.DatabaseManagerId = this.DatabaseManagerId.Value;
            this.ConsumablesBreakdown.MeasurementUnitsAdapter = this.MeasurementUnitsAdapter;
            this.ConsumablesBreakdown.Entity = this.Entity;
            this.ConsumablesBreakdown.ChartBuilder = this.chartBuilder;
            this.ConsumablesBreakdown.NormalCalculationResult = this.normalCalculationResult;
            this.ConsumablesBreakdown.AdditionalReportInformation = this.AdditionalReportInformation;
            this.ConsumablesBreakdown.MessengerToken = this.messengerToken;
            this.ConsumablesBreakdown.ChartMode.Value = this.ChartMode.Value;
            this.ConsumablesBreakdown.IsInViewerMode = this.IsInViewerMode;
            this.ConsumablesBreakdown.InitializeBreakdown();
        }

        /// <summary>
        /// Initializes the raw materials breakdown view model.
        /// </summary>
        private void InitializeRawMaterialsBreakdown()
        {
            if (this.Entity is Assembly && !this.IsEnhancedBreakdownEnabled.Value)
            {
                this.RawMaterialsBreakdown.TabVisibility.Value = Visibility.Collapsed;
                return;
            }

            this.RawMaterialsBreakdown.DatabaseManagerId = this.DatabaseManagerId.Value;
            this.RawMaterialsBreakdown.MeasurementUnitsAdapter = this.MeasurementUnitsAdapter;
            this.RawMaterialsBreakdown.Entity = this.Entity;
            this.RawMaterialsBreakdown.ChartBuilder = this.chartBuilder;
            this.RawMaterialsBreakdown.NormalCalculationResult = this.normalCalculationResult;
            this.RawMaterialsBreakdown.AdditionalReportInformation = this.AdditionalReportInformation;
            this.RawMaterialsBreakdown.MessengerToken = this.messengerToken;
            this.RawMaterialsBreakdown.ChartMode.Value = this.ChartMode.Value;
            this.RawMaterialsBreakdown.IsInViewerMode = this.IsInViewerMode;
            this.RawMaterialsBreakdown.InitializeBreakdown();

            this.isRawMaterialsBreakdownInitialized = true;
        }

        /// <summary>
        /// Initializes the part list breakdown view model.
        /// </summary>
        private void InitializePartListBreakdown()
        {
            if (this.Entity is Part)
            {
                this.PartListBreakdown.TabVisibility.Value = Visibility.Collapsed;
                return;
            }

            this.PartListBreakdown.DatabaseManagerId = this.DatabaseManagerId.Value;
            this.PartListBreakdown.MeasurementUnitsAdapter = this.MeasurementUnitsAdapter;
            this.PartListBreakdown.Entity = this.Entity;
            this.PartListBreakdown.ParentProject = this.ParentProject;
            this.PartListBreakdown.ChartBuilder = this.chartBuilder;
            this.PartListBreakdown.NormalCalculationResult = this.normalCalculationResult;
            this.PartListBreakdown.AdditionalReportInformation = this.AdditionalReportInformation;
            this.PartListBreakdown.MessengerToken = this.messengerToken;
            this.PartListBreakdown.ChartMode.Value = this.ChartMode.Value;
            this.PartListBreakdown.IsInViewerMode = this.IsInViewerMode;
            this.PartListBreakdown.InitializeBreakdown();
        }

        /// <summary>
        /// Initializes the BOM breakdown view model.
        /// </summary>
        private void InitializeBomBreakdown()
        {
            this.BomBreakdown.DatabaseManagerId = this.DatabaseManagerId.Value;
            this.BomBreakdown.MeasurementUnitsAdapter = this.MeasurementUnitsAdapter;
            this.BomBreakdown.Entity = this.Entity;
            this.BomBreakdown.ChartBuilder = this.chartBuilder;
            this.BomBreakdown.NormalCalculationResult = this.normalCalculationResult;
            this.BomBreakdown.AdditionalReportInformation = this.AdditionalReportInformation;
            this.BomBreakdown.MessengerToken = this.messengerToken;
            this.BomBreakdown.ChartMode.Value = this.ChartMode.Value;
            this.BomBreakdown.IsInViewerMode = this.IsInViewerMode;
            this.BomBreakdown.InitializeBreakdown();
        }

        /// <summary>
        /// Initializes the investment breakdown view model.
        /// </summary>
        private void InitializeInvestmentBreakdown()
        {
            this.InvestmentBreakdown.DatabaseManagerId = this.DatabaseManagerId.Value;
            this.InvestmentBreakdown.MeasurementUnitsAdapter = this.MeasurementUnitsAdapter;
            this.InvestmentBreakdown.Entity = this.Entity;
            this.InvestmentBreakdown.ChartBuilder = this.chartBuilder;
            this.InvestmentBreakdown.NormalCalculationResult = this.normalCalculationResult;
            this.InvestmentBreakdown.AdditionalReportInformation = this.AdditionalReportInformation;
            this.InvestmentBreakdown.MessengerToken = this.messengerToken;
            this.InvestmentBreakdown.ChartMode.Value = this.ChartMode.Value;
            this.InvestmentBreakdown.IsInViewerMode = this.IsInViewerMode;
            this.InvestmentBreakdown.InitializeBreakdown();
        }

        /// <summary>
        /// Initializes the commands.
        /// </summary>
        private void InitializeCommands()
        {
            this.ShowEntityDataCheckResultCommand = new DelegateCommand(() => this.ShowEntityDataCheckResult.Value = true);
            this.OpenReportSelectionCommand = new DelegateCommand(OpenReportSelectionWindow);
            this.CloseReportSelectionCommand = new DelegateCommand(CloseReportSelectionWindow);
            this.RegularXlsReportCommand = new DelegateCommand(CreateRegularXlsReport);
            this.RegularPdfReportCommand = new DelegateCommand(CreateRegularPdfReport);
            this.EnhancedXlsReportCommand = new DelegateCommand(CreateEnhancedXlsReport);
            this.EnhancedPdfReportCommand = new DelegateCommand(CreateEnhancedPdfReport);
            this.OpenLastCreatedReportCommand = new DelegateCommand(OpenLastCreatedReport);
        }

        /// <summary>
        /// Checks if some properties where properly initialized.
        /// </summary>
        private void CheckData()
        {
            // Check entity
            Argument.IsNotNull("Entity", this.Entity);

            var entityType = this.Entity.GetType();
            if (entityType != typeof(Assembly) && entityType != typeof(Part) && entityType != typeof(RawPart))
            {
                throw new ArgumentException("The entity type: " + entityType.FullName + " is not supported. Entity must be Assembly, Part or RawPart.");
            }

            // Check data source manager
            if (!this.IsInViewerMode && this.DataSourceManager == null)
            {
                throw new ArgumentNullException("DataSourceManager", "The data source manager cannot be null when view model is not in viewer mode.");
            }

            // Check initialization of sub view models
            Argument.IsNotNull("SummaryBreakdown", this.SummaryBreakdown);
            Argument.IsNotNull("OhAndMarginBreakdown", this.OhAndMarginBreakdown);
            Argument.IsNotNull("ProcessBreakdown", this.ProcessBreakdown);
            Argument.IsNotNull("ProcessStepsBreakdown", this.ProcessStepsBreakdown);
            Argument.IsNotNull("CommoditiesBreakdown", this.CommoditiesBreakdown);
            Argument.IsNotNull("ConsumablesBreakdown", this.ConsumablesBreakdown);
            Argument.IsNotNull("RawMaterialsBreakdown", this.RawMaterialsBreakdown);
            Argument.IsNotNull("PartListBreakdown", this.PartListBreakdown);
            Argument.IsNotNull("BomBreakdown", this.BomBreakdown);
            Argument.IsNotNull("InvestmentBreakdown", this.InvestmentBreakdown);
        }

        /// <summary>
        /// Gets the version and last modified date of entity.
        /// </summary>
        private void GetEntityVersionAndTimestamp()
        {
            if (!this.DisplayVersionAndTimestamp.Value)
            {
                return;
            }

            DateTime? lastChangeDate = null;
            decimal? version = null;

            Part part = this.Entity as Part;
            if (part != null)
            {
                lastChangeDate = part.LastChangeTimestamp;
                version = part.Version;
            }
            else
            {
                Assembly assy = this.Entity as Assembly;
                if (assy != null)
                {
                    lastChangeDate = assy.LastChangeTimestamp;
                    version = assy.Version;
                }
            }

            this.LastModifiedDate.Value = lastChangeDate.HasValue ? lastChangeDate.Value.ToString() : string.Empty;
            this.Version.Value = version.HasValue ? Formatter.FormatNumber(version.Value, 2) : string.Empty;
        }

        /// <summary>
        /// Gets the default chart mode from user settings.
        /// </summary>
        private void GetDefaultChartMode()
        {
            if ((ChartViewMode)UserSettingsManager.Instance.DefaultChartViewMode == ChartViewMode.PieView)
            {
                this.ChartMode.Value = ChartModes.Pie;
            }
            else if ((ChartViewMode)UserSettingsManager.Instance.DefaultChartViewMode == ChartViewMode.WatefallView)
            {
                this.ChartMode.Value = ChartModes.Watefall;
            }
        }

        /// <summary>
        /// Displays the entity calculation missing data on the screen.
        /// </summary>
        private void DisplayEntityDataCheckResults()
        {
            // If there is a data check result with errors, the entity has errors and the warning icon should be visible
            this.EntityHasErrors.Value = this.DataCheckResult.Value == null || !this.DataCheckResult.Value.HasErrors ? false : true;

            // If the entity has errors and the user setting for displaying warnings is set to true, the panel with errors should be displayed
            this.ShowEntityDataCheckResult.Value = this.EntityHasErrors.Value && UserSettingsManager.Instance.ShowWarningsInResultDetails;
        }

        #endregion

        #region Navigation

        /// <summary>
        /// Navigates to the cost type associated to a result details breakdown tab, if it's visible.
        /// </summary>
        /// <param name="itemType">The cost.</param>
        public void NavigateToCost(Type itemType)
        {
            if (itemType == typeof(CalculationResultSummary))
            {
                this.SelectedTabIndex.Value = 0;
            }
            else if (itemType == typeof(OverheadCost) && this.OhAndMarginBreakdown.TabVisibility.Value == Visibility.Visible)
            {
                this.SelectedTabIndex.Value = 1;
            }
            else if (itemType == typeof(ProcessCost) && this.ProcessBreakdown.TabVisibility.Value == Visibility.Visible)
            {
                this.SelectedTabIndex.Value = 2;
            }
            else if (itemType == typeof(CommoditiesCost) && this.CommoditiesBreakdown.TabVisibility.Value == Visibility.Visible)
            {
                this.SelectedTabIndex.Value = 4;
            }
            else if (itemType == typeof(ConsumablesCost) && this.ConsumablesBreakdown.TabVisibility.Value == Visibility.Visible)
            {
                this.SelectedTabIndex.Value = 5;
            }
            else if (itemType == typeof(RawMaterialsCost) && this.RawMaterialsBreakdown.TabVisibility.Value == Visibility.Visible)
            {
                this.SelectedTabIndex.Value = 6;
            }
            else if (itemType == typeof(InvestmentCost) && this.InvestmentBreakdown.TabVisibility.Value == Visibility.Visible)
            {
                this.SelectedTabIndex.Value = 9;
            }
        }

        #endregion

        #region Event and notification handlers

        /// <summary>
        /// Called when the enhanced breakdown is enabled or disabled, it sets the current calculation result and refreshed the calculations on breakdowns.
        /// </summary>
        private void OnEnhancedBreakdownChanged()
        {
            if (this.IsEnhancedBreakdownEnabled.Value)
            {
                this.currentCalculationResult = this.enhancedCalculationResult;
                if (!this.isRawMaterialsBreakdownInitialized)
                {
                    this.InitializeRawMaterialsBreakdown();
                }

                this.RawMaterialsBreakdown.TabVisibility.Value = Visibility.Visible;
            }
            else
            {
                this.currentCalculationResult = this.normalCalculationResult;
                this.RawMaterialsBreakdown.TabVisibility.Value = Visibility.Collapsed;
            }

            this.chartBuilder.CostCalculationResult = this.currentCalculationResult;
            this.chartBuilder.IsEnhancedCostCalculationResult = this.IsEnhancedBreakdownEnabled.Value;

            this.RefreshCalculationsOnBreakdowns();
        }

        /// <summary>
        /// Handles the notification message received with the current token.
        /// </summary>
        /// <param name="message">The message.</param>
        private void HandleNotificationMessage(NotificationMessage message)
        {
            if (message.Notification == Notification.ResultDetailsAdditionalCostsUpdated)
            {
                this.GetEntityVersionAndTimestamp();
                this.CalculateAndDisplayCost();
                this.RefreshCalculationsOnBreakdowns();
            }
        }

        /// <summary>
        /// Handles the chart mode changed notification message with the current token.
        /// </summary>
        /// <param name="message">The message.</param>
        private void HandleChartModeChangedNotification(NotificationMessage<ChartModes> message)
        {
            if (message.Notification == Notification.ResultDetailsChartModeChanged && message.Content != this.ChartMode.Value)
            {
                this.ChartMode.Value = message.Content;
                this.RefreshChartModeOnBreakdowns();
            }
        }

        /// <summary>
        /// Refreshes the current chart mode set on one breakdown, on all breakdowns.
        /// </summary>
        private void RefreshChartModeOnBreakdowns()
        {
            this.SummaryBreakdown.ChartMode.Value = this.ChartMode.Value;
            this.OhAndMarginBreakdown.ChartMode.Value = this.ChartMode.Value;
            this.ProcessBreakdown.ChartMode.Value = this.ChartMode.Value;
            this.ProcessStepsBreakdown.ChartMode.Value = this.ChartMode.Value;
            this.CommoditiesBreakdown.ChartMode.Value = this.ChartMode.Value;
            this.ConsumablesBreakdown.ChartMode.Value = this.ChartMode.Value;
            this.RawMaterialsBreakdown.ChartMode.Value = this.ChartMode.Value;
            this.PartListBreakdown.ChartMode.Value = this.ChartMode.Value;
            this.BomBreakdown.ChartMode.Value = this.ChartMode.Value;
            this.InvestmentBreakdown.ChartMode.Value = this.ChartMode.Value;
        }

        /// <summary>
        /// Handles the PropertyChanged event of a user setting.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void OnUserSettingsChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == ReflectionUtils.GetPropertyName(() => UserSettingsManager.Instance.DisplayVersionAndTimestamp))
            {
                this.DisplayVersionAndTimestamp.Value = UserSettingsManager.Instance.DisplayVersionAndTimestamp;
                this.GetEntityVersionAndTimestamp();
            }
            else if (e.PropertyName == ReflectionUtils.GetPropertyName(() => UserSettingsManager.Instance.DefaultChartViewMode))
            {
                this.GetDefaultChartMode();
            }
        }

        /// <summary>
        /// Called after the view has been unloaded from the parent or closed.
        /// </summary>
        public override void OnUnloaded()
        {
            base.OnUnloaded();
            this.messenger.Unregister(this);

            // Unload manually the sub-view models
            this.SummaryBreakdown.OnUnloaded();
            this.OhAndMarginBreakdown.OnUnloaded();
            this.ProcessBreakdown.OnUnloaded();
            this.ProcessStepsBreakdown.OnUnloaded();
            this.CommoditiesBreakdown.OnUnloaded();
            this.ConsumablesBreakdown.OnUnloaded();
            this.RawMaterialsBreakdown.OnUnloaded();
            this.PartListBreakdown.OnUnloaded();
            this.BomBreakdown.OnUnloaded();
            this.InvestmentBreakdown.OnUnloaded();
        }

        #endregion

        #region Calculations

        /// <summary>
        /// Calculates the cost of an entity and displays it in this control. Also displays the missing data pop-up if necessary.
        /// </summary>
        private void CalculateAndDisplayCost()
        {
            Part part = this.Entity as Part;
            if (part != null)
            {
                PartCostCalculationParameters costCalculationParameters = null;
                if (this.ParentProject != null)
                {
                    costCalculationParameters = CostCalculationHelper.CreatePartParamsFromProject(this.ParentProject);
                }
                else if (this.IsInViewerMode)
                {
                    costCalculationParameters = this.modelBrowserHelperService.GetPartCostCalculationParameters();
                }

                if (costCalculationParameters != null)
                {
                    ICostCalculator calculator = CostCalculatorFactory.GetCalculator(part.CalculationVariant);
                    this.normalCalculationResult = calculator.CalculatePartCost(part, costCalculationParameters);
                    this.currentCalculationResult = this.normalCalculationResult;

                    // Notify the application that the calculation results have changed
                    this.messenger.Send(new CurrentComponentCostChangedMessage(this.currentCalculationResult), this.IsInViewerMode ? GlobalMessengerTokens.ModelBrowserTargetToken : GlobalMessengerTokens.MainViewTargetToken);

                    this.DataCheckResult.Value = EntityDataChecker.CheckPartData(part);
                }
            }
            else
            {
                Assembly assembly = this.Entity as Assembly;
                if (assembly != null)
                {
                    AssemblyCostCalculationParameters costCalculationParameters = null;
                    if (this.ParentProject != null)
                    {
                        costCalculationParameters = CostCalculationHelper.CreateAssemblyParamsFromProject(this.ParentProject);
                    }
                    else if (this.IsInViewerMode)
                    {
                        costCalculationParameters = this.modelBrowserHelperService.GetAssemblyCostCalculationParameters();
                    }

                    if (costCalculationParameters != null)
                    {
                        ICostCalculator calculator = CostCalculatorFactory.GetCalculator(assembly.CalculationVariant);
                        var assemblyCost = calculator.CalculateAssemblyCost(assembly, costCalculationParameters);
                        this.DataCheckResult.Value = EntityDataChecker.CheckAssemblyData(assembly);

                        // Notify the application that the cost has changed
                        this.messenger.Send(new CurrentComponentCostChangedMessage(assemblyCost), this.IsInViewerMode ? GlobalMessengerTokens.ModelBrowserTargetToken : GlobalMessengerTokens.MainViewTargetToken);

                        this.normalCalculationResult = assemblyCost;
                        this.enhancedCalculationResult = ReportsHelper.CreateEnhancedCostBreakdown(assemblyCost);

                        this.currentCalculationResult = this.IsEnhancedBreakdownEnabled.Value ? this.enhancedCalculationResult : this.normalCalculationResult;
                    }
                }
            }

            // Order the process step costs by the step's index for the normal calculation result. For the enhanced result this is done while building it.
            if (this.normalCalculationResult != null)
            {
                this.normalCalculationResult.ProcessCost.SortStepCosts(s => s.StepIndex);
            }

            // Set the cost calculation result from chart builder to the current calculation result
            this.chartBuilder.CostCalculationResult = this.currentCalculationResult;

            // Check for entity data errors
            this.DisplayEntityDataCheckResults();
        }

        /// <summary>
        /// Refreshes the calculations on all visible breakdowns, so that the charts and data grids display the current costs.
        /// </summary>
        private void RefreshCalculationsOnBreakdowns()
        {
            this.SummaryBreakdown.RefreshBreakdown();

            if (this.OhAndMarginBreakdown.TabVisibility.Value == Visibility.Visible)
            {
                this.OhAndMarginBreakdown.RefreshBreakdown();
            }

            if (this.ProcessBreakdown.TabVisibility.Value == Visibility.Visible)
            {
                this.ProcessBreakdown.NormalCalculationResult = this.normalCalculationResult;
                this.ProcessBreakdown.RefreshBreakdown();
            }

            if (this.ProcessStepsBreakdown.TabVisibility.Value == Visibility.Visible)
            {
                this.ProcessStepsBreakdown.RefreshBreakdown();
            }

            if (this.CommoditiesBreakdown.TabVisibility.Value == Visibility.Visible)
            {
                this.CommoditiesBreakdown.NormalCalculationResult = this.normalCalculationResult;
                this.CommoditiesBreakdown.RefreshBreakdown();
            }

            if (this.ConsumablesBreakdown.TabVisibility.Value == Visibility.Visible)
            {
                this.ConsumablesBreakdown.NormalCalculationResult = this.normalCalculationResult;
                this.ConsumablesBreakdown.RefreshBreakdown();
            }

            if (this.RawMaterialsBreakdown.TabVisibility.Value == Visibility.Visible)
            {
                this.RawMaterialsBreakdown.NormalCalculationResult = this.normalCalculationResult;
                this.RawMaterialsBreakdown.RefreshBreakdown();
            }

            if (this.PartListBreakdown.TabVisibility.Value == Visibility.Visible)
            {
                this.PartListBreakdown.NormalCalculationResult = this.normalCalculationResult;
                this.PartListBreakdown.RefreshBreakdown();
            }

            if (this.BomBreakdown.TabVisibility.Value == Visibility.Visible)
            {
                this.BomBreakdown.NormalCalculationResult = this.normalCalculationResult;
                this.BomBreakdown.RefreshBreakdown();
            }

            if (this.InvestmentBreakdown.TabVisibility.Value == Visibility.Visible)
            {
                this.InvestmentBreakdown.NormalCalculationResult = this.normalCalculationResult;
                this.InvestmentBreakdown.RefreshBreakdown();
            }
        }

        #endregion

        #region Create reports

        /// <summary>
        /// Opens the window for generating reports.
        /// </summary>
        private void OpenReportSelectionWindow()
        {
            // Reset properties from report selection view
            this.ReportStatusVisibility.Value = Visibility.Collapsed;
            this.LastCreatedReportPath.Value = null;

            // Open view
            this.reportSelection = new ReportSelectionView();
            this.reportSelection.Owner = WindowHelper.DetermineWindowOwner(this.reportSelection);
            this.reportSelection.DataContext = this;
            this.reportSelection.ShowDialog();
        }

        /// <summary>
        /// Closes the window for generating reports.
        /// </summary>
        private void CloseReportSelectionWindow()
        {
            this.reportSelection.Close();
        }

        /// <summary>
        /// Creates the regular Excel report.
        /// </summary>
        private void CreateRegularXlsReport()
        {
            if (this.normalCalculationResult == null)
            {
                this.windowService.MessageDialogService.Show(LocalizedResources.Error_CantCreateReportInsufficientData, MessageDialogType.Error);
                return;
            }

            // Create and display the save file dialog
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.CheckPathExists = true;
            saveFileDialog.Filter = LocalizedResources.DialogFilter_ExcelDocs;
            saveFileDialog.OverwritePrompt = true;
            saveFileDialog.RestoreDirectory = true;

            // Suggest a file name for the report
            string suggestedFileName = ReportsHelper.GenerateReportFileName(this.Entity, false);
            if (!string.IsNullOrEmpty(suggestedFileName))
            {
                saveFileDialog.FileName = suggestedFileName;
            }

            bool? dialogResult = saveFileDialog.ShowDialog();
            if (dialogResult == true)
            {
                this.ReportStatusVisibility.Value = Visibility.Collapsed;

                // Convert the calculation result to use UI units
                var conversionFactors = this.MeasurementUnitsAdapter.GetUnitConversionFactors();
                var convertedResult = CurrencyConversionManager.ConvertFromBaseUnit(this.normalCalculationResult, conversionFactors);

                var part = this.Entity as Part;
                if (part != null)
                {
                    this.reportsManager.CreatePartReportXls(part, convertedResult, saveFileDialog.FileName);
                }
                else
                {
                    var assy = this.Entity as Assembly;
                    if (assy != null)
                    {
                        this.reportsManager.CreateAssemblyReportXls(assy, convertedResult, saveFileDialog.FileName);
                    }
                }

                this.LastCreatedReportPath.Value = saveFileDialog.FileName;
                this.ReportStatusVisibility.Value = Visibility.Visible;
            }
        }

        /// <summary>
        /// Creates the regular PDF report.
        /// </summary>
        private void CreateRegularPdfReport()
        {
            if (this.normalCalculationResult == null)
            {
                this.windowService.MessageDialogService.Show(LocalizedResources.Error_CantCreateReportInsufficientData, MessageDialogType.Error);
                return;
            }

            // Create and display the save file dialog
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.CheckPathExists = true;
            saveFileDialog.Filter = LocalizedResources.DialogFilter_PdfDocs;
            saveFileDialog.OverwritePrompt = true;
            saveFileDialog.RestoreDirectory = true;

            // Suggest a file name for the report
            string suggestedFileName = ReportsHelper.GenerateReportFileName(this.Entity, false);
            if (!string.IsNullOrEmpty(suggestedFileName))
            {
                saveFileDialog.FileName = suggestedFileName;
            }

            bool? dialogResult = saveFileDialog.ShowDialog();
            if (dialogResult == true)
            {
                this.ReportStatusVisibility.Value = Visibility.Collapsed;

                // Convert the calculation result to use UI units
                var conversionFactors = this.MeasurementUnitsAdapter.GetUnitConversionFactors();
                var convertedResult = CurrencyConversionManager.ConvertFromBaseUnit(this.normalCalculationResult, conversionFactors);

                var part = this.Entity as Part;
                if (part != null)
                {
                    this.reportsManager.CreatePartReportPdf(part, convertedResult, saveFileDialog.FileName);
                }
                else
                {
                    var assy = this.Entity as Assembly;
                    if (assy != null)
                    {
                        this.reportsManager.CreateAssemblyReportPdf(assy, convertedResult, saveFileDialog.FileName);
                    }
                }

                this.LastCreatedReportPath.Value = saveFileDialog.FileName;
                this.ReportStatusVisibility.Value = Visibility.Visible;
            }
        }

        /// <summary>
        /// Creates the enhanced Excel report.
        /// </summary>
        private void CreateEnhancedXlsReport()
        {
            if (!(this.Entity is Assembly))
            {
                return;
            }

            if (this.normalCalculationResult == null)
            {
                this.windowService.MessageDialogService.Show(LocalizedResources.Error_CantCreateReportInsufficientData, MessageDialogType.Error);
                return;
            }

            // Display the save file dialog and create the report
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.CheckPathExists = true;
            saveFileDialog.Filter = LocalizedResources.DialogFilter_ExcelDocs;
            saveFileDialog.OverwritePrompt = true;
            saveFileDialog.RestoreDirectory = true;

            // Suggest a report name            
            string suggestedFileName = ReportsHelper.GenerateReportFileName(this.Entity, true);
            if (!string.IsNullOrEmpty(suggestedFileName))
            {
                saveFileDialog.FileName = suggestedFileName;
            }

            bool? dialogResult = saveFileDialog.ShowDialog();
            if (dialogResult == true)
            {
                this.ReportStatusVisibility.Value = Visibility.Collapsed;

                // Convert the calculation result to use UI units
                var conversionFactors = this.MeasurementUnitsAdapter.GetUnitConversionFactors();
                var convertedResult = CurrencyConversionManager.ConvertFromBaseUnit(this.normalCalculationResult, conversionFactors);

                // Create the report
                this.reportsManager.CreateEnhancedAssemblyReportXls(convertedResult, saveFileDialog.FileName);

                this.LastCreatedReportPath.Value = saveFileDialog.FileName;
                this.ReportStatusVisibility.Value = Visibility.Visible;
            }
        }

        /// <summary>
        /// Creates the enhanced PDF report.
        /// </summary>
        private void CreateEnhancedPdfReport()
        {
            if (!(this.Entity is Assembly))
            {
                return;
            }

            if (this.normalCalculationResult == null)
            {
                this.windowService.MessageDialogService.Show(LocalizedResources.Error_CantCreateReportInsufficientData, MessageDialogType.Error);
                return;
            }

            // Create and display the save file dialog
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.CheckPathExists = true;
            saveFileDialog.Filter = LocalizedResources.DialogFilter_PdfDocs;
            saveFileDialog.OverwritePrompt = true;
            saveFileDialog.RestoreDirectory = true;

            // Suggest a file name for the report
            string suggestedFileName = ReportsHelper.GenerateReportFileName(this.Entity, true);
            if (!string.IsNullOrEmpty(suggestedFileName))
            {
                saveFileDialog.FileName = suggestedFileName;
            }

            bool? dialogResult = saveFileDialog.ShowDialog();
            if (dialogResult == true)
            {
                this.ReportStatusVisibility.Value = Visibility.Collapsed;

                // Convert the calculation result to use UI units
                var conversionFactors = this.MeasurementUnitsAdapter.GetUnitConversionFactors();
                var convertedResult = CurrencyConversionManager.ConvertFromBaseUnit(this.normalCalculationResult, conversionFactors);

                // Create the report
                this.reportsManager.CreateEnhancedAssemblyReportPdf(convertedResult, saveFileDialog.FileName);

                this.LastCreatedReportPath.Value = saveFileDialog.FileName;
                this.ReportStatusVisibility.Value = Visibility.Visible;
            }
        }

        /// <summary>
        /// Opens the last created report.
        /// </summary>
        private void OpenLastCreatedReport()
        {
            try
            {
                System.Diagnostics.Process.Start(this.LastCreatedReportPath.Value);
            }
            catch (Exception ex)
            {
                log.ErrorException("Error occurred while opening last created report.", ex);
                this.windowService.MessageDialogService.Show(LocalizedResources.Reports_LastCreatedReportOpenFail, MessageDialogType.Error);
            }
        }

        #endregion
    }
}
