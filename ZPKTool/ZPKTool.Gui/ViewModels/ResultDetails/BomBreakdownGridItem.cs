﻿using System;
using System.Windows.Media;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Data;
using ZPKTool.Gui.Resources;

namespace ZPKTool.Gui.ViewModels.ResultDetails
{
    /// <summary>
    /// This class represents a data object which will be added into the BOM view DataGrid.
    /// We need this wrapper to be able to insert into the dataGrid different type of objects.
    /// </summary>
    public class BomBreakdownGridItem : IIdentifiable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BomBreakdownGridItem"/> class.
        /// </summary>
        /// <param name="cost">The commodity cost.</param>
        /// <param name="parentName">Name of the commodity's parent.</param>
        public BomBreakdownGridItem(CommodityCost cost, string parentName)
        {
            this.Guid = cost.CommodityId;
            this.Name = cost.Name;
            this.TypeIcon = Images.CommodityIcon;
            this.TypeName = LocalizedResources.General_Commodity;
            this.Price = cost.Price;
            this.Overhead = cost.Overhead;
            this.Cost = cost.Cost;
            this.UnderylingCost = cost;
            this.TotalCost = cost.TotalCost;
            this.Amount = cost.Amount;
            this.AmountSymbol = LocalizedResources.General_Pieces;
            this.ParentName = parentName;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BomBreakdownGridItem" /> class.
        /// </summary>
        /// <param name="cost">The consumable cost.</param>
        /// <param name="parentName">Name of the consumable's parent.</param>
        public BomBreakdownGridItem(ConsumableCost cost, string parentName)
        {
            this.Guid = cost.ConsumableId;
            this.Name = cost.Name;
            this.TypeIcon = Images.ConsumableIcon;
            this.TypeName = LocalizedResources.General_Consumable;
            this.Price = cost.Price;
            this.Overhead = cost.Overhead;
            this.Cost = cost.Cost;
            this.UnderylingCost = cost;
            this.TotalCost = cost.TotalCost;
            this.Amount = cost.Amount;
            if (cost.AmountUnit != null)
            {
                this.AmountSymbol = cost.AmountUnit.Symbol;
            }

            this.ParentName = parentName;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BomBreakdownGridItem" /> class.
        /// </summary>
        /// <param name="cost">The raw material cost.</param>
        /// <param name="parentName">Name of the raw material's parent.</param>
        public BomBreakdownGridItem(RawMaterialCost cost, string parentName)
        {
            this.Guid = cost.RawMaterialId;
            this.Name = cost.Name;
            this.Overhead = cost.Overhead;
            this.Cost = cost.NetCost;
            this.UnderylingCost = cost;
            this.TotalCost = cost.TotalCost;
            this.ParentName = parentName;
            if (cost.IsRawPart)
            {
                this.TypeIcon = Images.RawPartIcon;
                this.TypeName = LocalizedResources.General_RawPart;
            }
            else
            {
                this.Amount = cost.MaterialWeight;
                this.AmountSymbol = cost.WeightUnit;
                this.Price = cost.Price;
                this.TypeIcon = Images.RawMaterialIcon;
                this.TypeName = LocalizedResources.General_RawMaterial;
            }
        }

        /// <summary>
        /// Gets or sets the identifier of the object.
        /// </summary>
        public Guid Guid { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the icon representing the type of the material.
        /// </summary>
        public ImageSource TypeIcon { get; set; }

        /// <summary>
        /// Gets or sets the type name that replaces the type icon when exporting this item to xls/pdf file.
        /// </summary>        
        public string TypeName { get; set; }

        /// <summary>
        /// Gets or sets the price.
        /// </summary>
        public decimal? Price { get; set; }

        /// <summary>
        /// Gets or sets the overhead.
        /// </summary>
        public decimal? Overhead { get; set; }

        /// <summary>
        /// Gets or sets the cost.
        /// </summary>
        public decimal? Cost { get; set; }

        /// <summary>
        /// Gets or sets the underlying cost object. It can be Commodity, Consumable or RawMaterial cost.
        /// </summary>
        public object UnderylingCost { get; set; }

        /// <summary>
        /// Gets or sets the total cost object.
        /// </summary>
        public decimal? TotalCost { get; set; }

        /// <summary>
        /// Gets or sets the amount of the object.
        /// </summary>
        public decimal? Amount { get; set; }

        /// <summary>
        /// Gets or sets the symbol unit for the amount of the object.
        /// </summary>
        public string AmountSymbol { get; set; }

        /// <summary>
        /// Gets or sets the name of the material's parent.
        /// </summary>        
        public string ParentName { get; set; }
    }
}
