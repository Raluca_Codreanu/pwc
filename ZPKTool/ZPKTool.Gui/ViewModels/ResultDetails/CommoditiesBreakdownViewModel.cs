﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using ZPKTool.Business;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Controls;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.ViewModels.ResultDetails;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for commodities breakdown tab from  result details view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class CommoditiesBreakdownViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The entity for which to compute and display the costs.
        /// </summary>
        private object entity;

        /// <summary>
        /// The measurement units adapter.
        /// </summary>
        private UnitsAdapter measurementUnitsAdapter;

        /// <summary>
        /// The chart builder, used for building chart items from cost calculation results.
        /// </summary>
        private CostChartBuilder chartBuilder;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="CommoditiesBreakdownViewModel"/> class.
        /// </summary>
        /// <param name="messenger">The messenger.</param>
        [ImportingConstructor]
        public CommoditiesBreakdownViewModel(IMessenger messenger)
        {
            Argument.IsNotNull("messenger", messenger);
            this.messenger = messenger;

            this.ChartItems.Value = new Collection<LabeledPieChartItem>();
            this.DataGridItems.Value = new Collection<CommodityBreakdownGridItem>();

            this.CurrentUserName = SecurityManager.Instance.CurrentUser != null ? SecurityManager.Instance.CurrentUser.Name : string.Empty;

            this.MouseDoubleClickCommand = new DelegateCommand(this.NavigateToItem);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the entity for which to compute and display the costs.
        /// </summary>
        public object Entity
        {
            get
            {
                return this.entity;
            }

            set
            {
                Argument.IsNotNull("Entity", value);
                if (this.entity != value)
                {
                    var entityType = value.GetType();
                    if (entityType != typeof(Assembly) && entityType != typeof(Part) && entityType != typeof(RawPart))
                    {
                        throw new InvalidOperationException("The entity type: " + entityType.FullName + " is not supported. Entity must be Assembly, Part or RawPart.");
                    }

                    this.entity = value;
                    OnPropertyChanged(() => this.Entity);
                }
            }
        }

        /// <summary>
        /// Gets or sets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter
        {
            get
            {
                return this.measurementUnitsAdapter;
            }

            set
            {
                if (this.measurementUnitsAdapter != value)
                {
                    this.measurementUnitsAdapter = value;
                    OnPropertyChanged(() => this.MeasurementUnitsAdapter);
                }
            }
        }

        /// <summary>
        /// Gets or sets the chart builder, used for building chart items from cost calculation results.
        /// </summary>
        public CostChartBuilder ChartBuilder
        {
            get
            {
                return this.chartBuilder;
            }

            set
            {
                Argument.IsNotNull("ChartBuilder", value);
                if (this.chartBuilder != value)
                {
                    this.chartBuilder = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the identifier of database manager.
        /// </summary>
        public DbIdentifier DatabaseManagerId { get; set; }

        /// <summary>
        /// Gets or sets the normal cost calculation result.
        /// </summary>
        public CalculationResult NormalCalculationResult { get; set; }

        /// <summary>
        /// Gets or sets the token for sending message notifications for parent view model.
        /// </summary>
        public Guid MessengerToken { get; set; }

        /// <summary>
        /// Gets the name of the current logged-in user.
        /// </summary>
        public string CurrentUserName { get; private set; }

        /// <summary>
        /// Gets or sets the extra information for data grid reports.
        /// </summary>
        public ExtendedDataGridAdditionalReportInformation AdditionalReportInformation { get; set; }

        /// <summary>
        /// Gets or sets the visibility of tab containing the breakdown.
        /// </summary>
        public VMProperty<Visibility> TabVisibility { get; set; }

        /// <summary>
        /// Gets or sets the current chart mode.
        /// </summary>
        public VMProperty<ChartModes> ChartMode { get; set; }

        /// <summary>
        /// Gets the items to display in chart.
        /// </summary>
        public VMProperty<ICollection<LabeledPieChartItem>> ChartItems { get; private set; }

        /// <summary>
        /// Gets the total cost of entity.
        /// </summary>
        public VMProperty<decimal?> TotalCost { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the entity has costs (total value > 0) or not.
        /// </summary>
        public VMProperty<bool> EntityHasCosts { get; private set; }

        /// <summary>
        /// Gets the cost items displayed in the data grid.
        /// </summary>
        public VMProperty<ICollection<CommodityBreakdownGridItem>> DataGridItems { get; private set; }

        /// <summary>
        /// Gets the selected cost item from the data grid.
        /// </summary>
        public VMProperty<CommodityBreakdownGridItem> SelectedDataGridItem { get; private set; }

        #endregion

        #region Commands

        /// <summary>
        /// Gets the command for the mouse double click event in commodities data grid.
        /// </summary>
        public ICommand MouseDoubleClickCommand { get; private set; }

        #endregion

        #region Implementation

        /// <summary>
        /// Initializes costs from the breakdown.
        /// </summary>
        public void InitializeBreakdown()
        {
            // Attach event handler for total cost changed before setting the cost, to raise event on initialization too
            this.TotalCost.ValueChanged += (s, e) => this.OnTotalCostChanged();

            this.SetupChart();
            this.SetupCommoditiesDataGrid();

            this.ChartMode.ValueChanged += (s, e) => this.OnChartModeChanged();
        }

        /// <summary>
        /// Refreshes costs from the breakdown (called after the costs have changed).
        /// </summary>
        public void RefreshBreakdown()
        {
            this.SetupChart();
            this.SetupCommoditiesDataGrid();
        }

        /// <summary>
        /// Gets the costs information displayed in the chart.
        /// </summary>
        private void SetupChart()
        {
            decimal totalCost;

            this.ChartItems.Value = this.ChartBuilder.GetCommoditiesCostChart(out totalCost);
            this.TotalCost.Value = totalCost;
        }

        /// <summary>
        /// Gets the items to display in the data grid.
        /// </summary>
        private void SetupCommoditiesDataGrid()
        {
            if (this.NormalCalculationResult != null)
            {
                string parentName = this.Entity is INameable ? ((INameable)this.Entity).Name : null;
                this.DataGridItems.Value = this.BuildCommoditiesDataGridItems(this.NormalCalculationResult, parentName);
            }
        }

        /// <summary>
        /// Builds the commodities data grid items source.
        /// </summary>
        /// <param name="result">The calculation result from which to build it.</param>
        /// <param name="parentName">Name of the commodities parent.</param>
        /// <returns>A list of objects for data binding.</returns>
        private List<CommodityBreakdownGridItem> BuildCommoditiesDataGridItems(CalculationResult result, string parentName)
        {
            var gridItems = new List<CommodityBreakdownGridItem>();
            foreach (CommodityCost cost in result.CommoditiesCost.CommodityCosts.OrderBy(c => c.Name))
            {
                gridItems.Add(new CommodityBreakdownGridItem()
                {
                    Cost = cost,
                    ParentName = parentName
                });
            }

            if (this.ChartBuilder.IsEnhancedCostCalculationResult)
            {
                foreach (PartCost partCost in result.PartsCost.PartCosts.OrderBy(c => c.PartIndex))
                {
                    var items = BuildCommoditiesDataGridItems(partCost.FullCalculationResult, partCost.Name);
                    gridItems.AddRange(items);
                }

                foreach (AssemblyCost assyCost in result.AssembliesCost.AssemblyCosts.OrderBy(c => c.AssemblyIndex))
                {
                    var items = BuildCommoditiesDataGridItems(assyCost.FullCalculationResult, assyCost.Name);
                    gridItems.AddRange(items);
                }
            }

            return gridItems;
        }

        /// <summary>
        /// Navigates in the project's tree, to the selected item from the data grid.
        /// </summary>
        private void NavigateToItem()
        {
            if (this.SelectedDataGridItem.Value != null)
            {
                var commodityCost = this.SelectedDataGridItem.Value.Cost;
                if (commodityCost != null)
                {
                    var navigationTarget = new Commodity() { Guid = commodityCost.CommodityId };

                    NavigateToEntityMessage navMessage = new NavigateToEntityMessage(navigationTarget, this.DatabaseManagerId);
                    this.messenger.Send(navMessage, this.IsInViewerMode ? GlobalMessengerTokens.ModelBrowserTargetToken : GlobalMessengerTokens.MainViewTargetToken);
                }
            }
        }

        /// <summary>
        /// Called when the current chart mode changed, notifies the parent view model that the chart mode changed, to refresh it in all breakdowns.
        /// </summary>
        private void OnChartModeChanged()
        {
            var message = new NotificationMessage<ChartModes>(Notification.ResultDetailsChartModeChanged, this.ChartMode.Value);
            this.messenger.Send<NotificationMessage<ChartModes>>(message, this.MessengerToken);
        }

        /// <summary>
        /// Called when the total costs of the entity changed, checks if the entity has costs or not.
        /// </summary>
        private void OnTotalCostChanged()
        {
            this.EntityHasCosts.Value = this.TotalCost.Value.HasValue && this.TotalCost.Value != 0m;
        }

        #endregion
    }
}
