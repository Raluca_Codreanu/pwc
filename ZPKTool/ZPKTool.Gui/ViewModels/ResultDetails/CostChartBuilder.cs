﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Common;
using ZPKTool.Controls;
using ZPKTool.Data;
using ZPKTool.Gui.Resources;

namespace ZPKTool.Gui.ViewModels.ResultDetails
{
    /// <summary>
    /// Offers some base functionality for classes responsible with building charts from the cost calculation results of different entities.
    /// </summary>
    public abstract class CostChartBuilder : INotifyPropertyChanged
    {
        #region Attributes

        /// <summary>
        /// The cost calculation result used to build the charts.
        /// </summary>
        private CalculationResult costCalculationResult;

        /// <summary>
        /// A value indicating whether the cost calculation result set in this instance is enhanced.
        /// </summary>        
        private bool isEnhancedCostCalculationResult;

        #endregion

        #region Events

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// When a property has been changed, this method notifies the GUI to update the contents.
        /// </summary>
        /// <param name="propertyName">The name of the property which has been changed.</param>
        private void RaisePropertyChangeEvent(string propertyName)
        {
            if (null != PropertyChanged)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the cost calculation result used to build the charts.
        /// </summary>
        public CalculationResult CostCalculationResult
        {
            get
            {
                return this.costCalculationResult;
            }

            set
            {
                if (this.costCalculationResult != value)
                {
                    this.costCalculationResult = value;
                    this.RaisePropertyChangeEvent(ReflectionUtils.GetPropertyName(() => this.CostCalculationResult));
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the cost calculation result set in this instance is enhanced.
        /// </summary>        
        public bool IsEnhancedCostCalculationResult
        {
            get
            {
                return this.isEnhancedCostCalculationResult;
            }

            set
            {
                if (this.isEnhancedCostCalculationResult != value)
                {
                    this.isEnhancedCostCalculationResult = value;
                    this.RaisePropertyChangeEvent(ReflectionUtils.GetPropertyName(() => this.IsEnhancedCostCalculationResult));
                }
            }
        }

        #endregion

        #region Implementation

        /// <summary>
        /// Creates an appropriate CostChartBuilder instance based on an entity type.
        /// Usually, different entities have different CostChartBuilder implementations.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <returns>
        /// An appropriate instance of CostChartBuilder.
        /// </returns>
        /// <exception cref="ArgumentNullException">entityType was null</exception>
        /// <exception cref="ArgumentException">entityType does not have a corresponding chart builder.</exception>
        public static CostChartBuilder CreateInstanceForType(Type entityType)
        {
            if (entityType == null)
            {
                throw new ArgumentNullException("entityType", "The type was null.");
            }

            if (entityType == typeof(Assembly))
            {
                return new AssemblyCostChartBuilder();
            }
            else if (entityType == typeof(Part) || entityType == typeof(RawPart))
            {
                return new PartCostChartBuilder();
            }
            else
            {
                throw new ArgumentException("The type " + entityType.FullName + " does not have a CostChartBuilder implementation.");
            }
        }

        /// <summary>
        /// Gets the items that should appear in the cost summary chart.
        /// </summary>
        /// <param name="totalCost">The total cost.</param>
        /// <returns>List of chart items.</returns>
        public abstract Collection<LabeledPieChartItem> GetCostSummaryChart(out decimal totalCost);

        /// <summary>
        /// Gets the items that should appear in the cost overhead chart.
        /// </summary>
        /// <param name="totalCost">The total overhead cost.</param>
        /// <returns>List of chart items.</returns>
        public abstract Collection<LabeledPieChartItem> GetOverheadCostChart(out decimal totalCost);

        /// <summary>
        /// Gets the items that should appear in the manufacturing/assembling cost chart.
        /// </summary>
        /// <param name="totalCost">The total manufacturing/assembling cost.</param>
        /// <returns>List of chart items.</returns>
        public abstract Collection<LabeledPieChartItem> GetProcessCostChart(out decimal totalCost);

        /// <summary>
        /// Gets the items that should appear in the part-list chart.
        /// </summary>
        /// <param name="totalCost">The total sub-parts cost.</param>
        /// <returns>List of chart items.</returns>
        public abstract Collection<LabeledPieChartItem> GetPartListChart(out decimal totalCost);

        /// <summary>
        /// Gets the chart items for displaying a process step cost.
        /// </summary>
        /// <param name="cost">The process step cost.</param>
        /// <param name="totalCost">The total cost to display under the chart legend.</param>
        /// <returns>List of chart items.</returns>
        public abstract Collection<LabeledPieChartItem> GetProcessStepCostChart(ProcessStepCost cost, out decimal totalCost);

        /// <summary>
        /// Gets the items that should appear in the commodities cost chart.
        /// </summary>
        /// <param name="totalCost">The total cost of commodities.</param>
        /// <returns>List of chart items.</returns>
        public virtual Collection<LabeledPieChartItem> GetCommoditiesCostChart(out decimal totalCost)
        {
            totalCost = 0m;
            if (this.CostCalculationResult == null || this.CostCalculationResult.CommoditiesCost == null)
            {
                return new Collection<LabeledPieChartItem>();
            }

            totalCost = this.CostCalculationResult.CommoditiesCost.TotalCostSum;
            Collection<LabeledPieChartItem> chartItems = new Collection<LabeledPieChartItem>();
            CommoditiesCost cost = this.CostCalculationResult.CommoditiesCost;

            chartItems.Add(new LabeledPieChartItem(LocalizedResources.Report_CommodityCost, cost.CostsSum));
            var rejectCostItem = new LabeledPieChartItem(LocalizedResources.General_SumRejectCost, cost.RejectCostSum);
            rejectCostItem.IsNotAffectingCost = true;
            chartItems.Add(rejectCostItem);
            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_SumOverheadCost, cost.OverheadSum));

            return chartItems;
        }

        /// <summary>
        /// Gets the chart items for displaying the cost of a commodity.
        /// </summary>
        /// <param name="cost">The commodity's cost.</param>
        /// <param name="totalCost">The total cost.</param>
        /// <returns>List of chart items.</returns>
        public virtual Collection<LabeledPieChartItem> GetCommodityCostChart(CommodityCost cost, out decimal totalCost)
        {
            totalCost = 0m;
            if (cost == null)
            {
                return new Collection<LabeledPieChartItem>();
            }

            totalCost = cost.TotalCost;
            Collection<LabeledPieChartItem> chartItems = new Collection<LabeledPieChartItem>();
            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_Cost, cost.Cost));
            var rejectCostItem = new LabeledPieChartItem(LocalizedResources.General_RejectCost, cost.RejectCost);
            rejectCostItem.IsNotAffectingCost = true;
            chartItems.Add(rejectCostItem);
            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_Overhead, cost.Overhead));

            return chartItems;
        }

        /// <summary>
        /// Gets the items that should appear in the consumables cost chart.
        /// </summary>
        /// <param name="totalCost">The total cost of consumables.</param>
        /// <returns>List of chart items.</returns>
        public virtual Collection<LabeledPieChartItem> GetConsumablesCostChart(out decimal totalCost)
        {
            totalCost = 0m;
            if (this.CostCalculationResult == null || this.CostCalculationResult.ConsumablesCost == null)
            {
                return new Collection<LabeledPieChartItem>();
            }

            totalCost = this.CostCalculationResult.ConsumablesCost.TotalCostSum;
            Collection<LabeledPieChartItem> chartItems = new Collection<LabeledPieChartItem>();
            ConsumablesCost cost = this.CostCalculationResult.ConsumablesCost;
            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_SumConsumableCosts, cost.CostsSum));
            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_SumOverheadOnConsumables, cost.OverheadSum));

            return chartItems;
        }

        /// <summary>
        /// Gets the chart items for displaying the cost of a consumable.
        /// </summary>
        /// <param name="cost">The cost of the consumable.</param>
        /// <param name="totalCost">The total cost.</param>
        /// <returns>List of chart items.</returns>
        public virtual Collection<LabeledPieChartItem> GetConsumableCostChart(ConsumableCost cost, out decimal totalCost)
        {
            totalCost = 0m;
            if (cost == null)
            {
                return new Collection<LabeledPieChartItem>();
            }

            totalCost = cost.TotalCost;
            Collection<LabeledPieChartItem> chartItems = new Collection<LabeledPieChartItem>();
            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_Price, cost.Price));
            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_Overhead, cost.Overhead));

            return chartItems;
        }

        /// <summary>
        /// Gets the items that should appear in the raw materials cost chart.
        /// </summary>
        /// <param name="totalCost">The total raw materials cost.</param>
        /// <returns>List of chart items.</returns>
        public virtual Collection<LabeledPieChartItem> GetRawMaterialsCostChart(out decimal totalCost)
        {
            totalCost = 0m;
            if (this.CostCalculationResult == null || this.CostCalculationResult.RawMaterialsCost == null)
            {
                return new Collection<LabeledPieChartItem>();
            }

            totalCost = this.CostCalculationResult.RawMaterialsCost.TotalCostSum;
            RawMaterialsCost cost = this.CostCalculationResult.RawMaterialsCost;
            Collection<LabeledPieChartItem> chartItems = new Collection<LabeledPieChartItem>();
            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_SumRawMaterialNetCost, cost.NetCostSum));
            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_SumOverheadOnRawMaterial, cost.OverheadSum));
            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_WIPCostTotal, cost.WIPCostSum));

            return chartItems;
        }

        /// <summary>
        /// Gets the chart items for displaying the cost of a raw material.
        /// </summary>
        /// <param name="cost">The cost of the raw material.</param>
        /// <param name="totalCost">The total cost.</param>
        /// <returns>List of chart items.</returns>
        public virtual Collection<LabeledPieChartItem> GetRawMaterialCostChart(RawMaterialCost cost, out decimal totalCost)
        {
            totalCost = 0m;
            if (cost == null)
            {
                return new Collection<LabeledPieChartItem>();
            }

            totalCost = cost.TotalCost;
            int netCostGroupId = 0;
            Collection<LabeledPieChartItem> chartItems = new Collection<LabeledPieChartItem>();

            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_BaseCost, cost.BaseCost, netCostGroupId));
            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_Scrap, cost.ScrapCost, netCostGroupId));
            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_Loss, cost.LossCost, netCostGroupId));
            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_RejectCost, cost.RejectCost, netCostGroupId));

            LabeledPieChartItem netCostItem = new LabeledPieChartItem(LocalizedResources.General_NetCost, cost.NetCost, false);
            netCostItem.GroupID = netCostGroupId;
            netCostItem.IsPrimaryInGroup = true;
            chartItems.Add(netCostItem);

            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_Overhead, cost.Overhead));
            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_WIPCost, cost.WIPCost));

            return chartItems;
        }

        /// <summary>
        /// Gets the items that should appear in the bill of materials chart.
        /// </summary>
        /// <param name="totalCost">The total cost of materials.</param>
        /// <returns>List of chart items.</returns>
        public virtual Collection<LabeledPieChartItem> GetBillOfMaterialsChart(out decimal totalCost)
        {
            totalCost = 0m;

            if (this.CostCalculationResult == null || this.CostCalculationResult.CommoditiesCost == null ||
                this.CostCalculationResult.ConsumablesCost == null || this.CostCalculationResult.RawMaterialsCost == null)
            {
                return new Collection<LabeledPieChartItem>();
            }

            Collection<LabeledPieChartItem> resultList = new Collection<LabeledPieChartItem>();
            CommoditiesCost commoditiesCost = this.CostCalculationResult.CommoditiesCost;
            ConsumablesCost consumablesCost = this.CostCalculationResult.ConsumablesCost;
            RawMaterialsCost rawMaterialsCost = this.CostCalculationResult.RawMaterialsCost;

            foreach (CommodityCost commodityCost in commoditiesCost.CommodityCosts)
            {
                resultList.Add(new LabeledPieChartItem(commodityCost.Name, commodityCost.TotalCost));
                totalCost += commodityCost.TotalCost;
            }

            foreach (ConsumableCost consumableCost in consumablesCost.ConsumableCosts)
            {
                resultList.Add(new LabeledPieChartItem(consumableCost.Name, consumableCost.TotalCost));
                totalCost += consumableCost.TotalCost;
            }

            foreach (RawMaterialCost rawMaterialCost in rawMaterialsCost.RawMaterialCosts)
            {
                resultList.Add(new LabeledPieChartItem(rawMaterialCost.Name, (decimal)rawMaterialCost.TotalCost));
                totalCost += rawMaterialCost.TotalCost;
            }

            return resultList;
        }

        /// <summary>
        /// Gets the items that should appear in the investment chart.
        /// </summary>
        /// <param name="totalCost">The total cost of Materials and Dies/Tools investment.</param>
        /// <returns>List of chart items.</returns>
        public virtual Collection<LabeledPieChartItem> GetInvestmentChart(out decimal totalCost)
        {
            Collection<LabeledPieChartItem> chartItems = new Collection<LabeledPieChartItem>();
            totalCost = 0;

            if (CostCalculationResult != null)
            {
                totalCost = this.CostCalculationResult.InvestmentCost.TotalInvestment;
                chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_MachineInvestment, this.CostCalculationResult.InvestmentCost.TotalMachinesInvestment));
                chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_DieToolingInvestment, this.CostCalculationResult.InvestmentCost.TotalDiesInvestment));
            }

            return chartItems;
        }

        /// <summary>
        /// Gets the items that should appear in the process steps chart.
        /// </summary>
        /// <param name="totalCost">The total cost of the steps.</param>
        /// <returns>List of chart items.</returns>
        public virtual Collection<LabeledPieChartItem> GetProcessStepsCostChart(out decimal totalCost)
        {
            Collection<LabeledPieChartItem> chartItems = new Collection<LabeledPieChartItem>();
            totalCost = 0;

            if (CostCalculationResult != null)
            {
                totalCost = this.CostCalculationResult.ProcessCost.TotalManufacturingCostSum;

                foreach (ProcessStepCost cost in this.CostCalculationResult.ProcessCost.StepCosts)
                {
                    LabeledPieChartItem chartItem = new LabeledPieChartItem(cost.StepName, cost.TotalManufacturingCost);
                    chartItems.Add(chartItem);
                }
            }

            return chartItems;
        }

        /// <summary>
        /// Computes the part of the tool/die cost that is included in the manufacturing/assembling cost and the part that is not.
        /// This method is used only for the enhanced breakdown, when the process breakdown includes the steps of all processes in the assembling hierarchy,
        /// and some of these processes may belong to parts/assemblies with SBM off. For the process of a single part/assembly the tool/die cost is either fully included
        /// or fully excluded.
        /// </summary>
        /// <param name="processCost">The process cost.</param>
        /// <param name="includedToolDieCost">The included tool die cost.</param>
        /// <param name="excludedToolDieCost">The excluded tool die cost.</param>
        protected void ComputeIncludedToolDieCost(ProcessCost processCost, out decimal includedToolDieCost, out decimal excludedToolDieCost)
        {
            includedToolDieCost = 0;
            excludedToolDieCost = 0;

            foreach (ProcessStepCost stepCost in processCost.StepCosts)
            {
                if (stepCost.SBMActive)
                {
                    includedToolDieCost += stepCost.ToolAndDieCost;
                }
                else
                {
                    excludedToolDieCost += stepCost.ToolAndDieCost;
                }
            }
        }

        #endregion
    }
}
