﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Controls;
using ZPKTool.Gui.Resources;

namespace ZPKTool.Gui.ViewModels.ResultDetails
{
    /// <summary>
    /// Builds charts that display the cost calculations of a part.
    /// </summary>
    public class PartCostChartBuilder : CostChartBuilder
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PartCostChartBuilder"/> class.
        /// </summary>
        public PartCostChartBuilder()
            : base()
        {
        }

        /// <summary>
        /// Gets the items that should appear in the cost summary chart.
        /// </summary>
        /// <param name="totalCost">The total cost.</param>
        /// <returns>List of chart items.</returns>
        public override Collection<LabeledPieChartItem> GetCostSummaryChart(out decimal totalCost)
        {
            Collection<LabeledPieChartItem> chartItems = new Collection<LabeledPieChartItem>();
            totalCost = 0m;

            if (this.CostCalculationResult == null || this.CostCalculationResult.Summary == null)
            {
                return chartItems;
            }

            totalCost = this.CostCalculationResult.Summary.TargetCost;
            CalculationResultSummary summary = this.CostCalculationResult.Summary;
            int subTotalID = 0;

            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_RawMaterial, summary.RawMaterialCost, subTotalID));
            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_WIPCostShort, summary.WIPCost, subTotalID));
            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_Commodities, summary.CommoditiesCost, subTotalID));
            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_Consumable, summary.ConsumableCost, subTotalID));
            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_TotalManufacturingCost, summary.TotalManufacturingCost, subTotalID));
            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_RejectCost, summary.ManufacturingRejectCost, subTotalID));
            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_TransportCostManufacturing, summary.ManufacturingTransportCost, subTotalID));

            LabeledPieChartItem subTotalItem = new LabeledPieChartItem(LocalizedResources.General_ProductionCost, summary.ProductionCost, false);
            subTotalItem.IsPrimaryInGroup = true;
            subTotalItem.GroupID = subTotalID;
            chartItems.Add(subTotalItem);

            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_DevelopmentCost, summary.DevelopmentCost));
            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_ProjectRelatedInvest, summary.ProjectInvest));
            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_OtherCost, summary.OtherCost));
            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_CostOfPackaging, summary.PackagingCost));
            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_LogisticCost, summary.LogisticCost));
            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_TransportCost, summary.TransportCost));
            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_OverheadAndMargin, summary.OverheadAndMarginCost));
            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_PaymentCost, summary.PaymentCost));

            return chartItems;
        }

        /// <summary>
        /// Gets the items that should appear in the cost overhead chart.
        /// </summary>
        /// <param name="totalCost">The total overhead cost.</param>
        /// <returns>List of chart items.</returns>
        public override Collection<LabeledPieChartItem> GetOverheadCostChart(out decimal totalCost)
        {
            totalCost = 0m;
            if (this.CostCalculationResult == null || this.CostCalculationResult.OverheadCost == null)
            {
                return new Collection<LabeledPieChartItem>();
            }

            totalCost = this.CostCalculationResult.OverheadCost.TotalSumOverheadAndMargin;
            Collection<LabeledPieChartItem> chartItems = new Collection<LabeledPieChartItem>();
            int overheadGroupID = 0;
            int marginGroupID = 1;
            OverheadCost cost = this.CostCalculationResult.OverheadCost;

            chartItems.Add(new LabeledPieChartItem(
                LocalizedResources.General_OverheadRawMaterial,
                cost.RawMaterialOverhead,
                cost.OverheadSettings != null,
                cost.OverheadSettings != null ? cost.OverheadSettings.MaterialOverhead : 0m,
                overheadGroupID));
            chartItems.Add(new LabeledPieChartItem(
                LocalizedResources.General_OverheadConsumable,
                cost.ConsumableOverhead,
                cost.OverheadSettings != null,
                cost.OverheadSettings != null ? cost.OverheadSettings.ConsumableOverhead : 0m,
                overheadGroupID));
            chartItems.Add(new LabeledPieChartItem(
                LocalizedResources.General_OverheadCommodity,
                cost.CommodityOverhead,
                cost.OverheadSettings != null,
                cost.OverheadSettings != null ? cost.OverheadSettings.CommodityOverhead : 0m,
                overheadGroupID));
            chartItems.Add(new LabeledPieChartItem(
                LocalizedResources.General_OverheadManufacturingCost,
                cost.ManufacturingOverhead,
                cost.OverheadSettings != null,
                cost.OverheadSettings != null ? cost.ManufacturingOverheadRateAverage : 0m,
                overheadGroupID));
            chartItems.Add(new LabeledPieChartItem(
                LocalizedResources.General_OverheadOtherCost,
                cost.OtherCostOverhead,
                cost.OverheadSettings != null,
                cost.OverheadSettings != null ? cost.OverheadSettings.OtherCostOHValue : 0m,
                overheadGroupID));
            chartItems.Add(new LabeledPieChartItem(
                LocalizedResources.General_OverheadPackaging,
                cost.PackagingOverhead,
                cost.OverheadSettings != null,
                cost.OverheadSettings != null ? cost.OverheadSettings.PackagingOHValue : 0m,
                overheadGroupID));
            chartItems.Add(new LabeledPieChartItem(
                LocalizedResources.General_OverheadLogistics,
                cost.LogisticOverhead,
                cost.OverheadSettings != null,
                cost.OverheadSettings != null ? cost.OverheadSettings.LogisticOHValue : 0m,
                overheadGroupID));

            chartItems.Add(new LabeledPieChartItem(
                LocalizedResources.Genaral_OverheadSalesAndAdmin,
                cost.SalesAndAdministrationOverhead,
                cost.OverheadSettings != null,
                cost.OverheadSettings != null ? cost.OverheadSettings.SalesAndAdministrationOHValue : 0m,
                overheadGroupID));

            chartItems.Add(new LabeledPieChartItem(
                LocalizedResources.OverheadSettings_CompanySurchargeOH,
                cost.CompanySurchargeOverhead,
                cost.OverheadSettings != null,
                cost.OverheadSettings != null ? cost.OverheadSettings.CompanySurchargeOverhead : 0m,
                overheadGroupID));

            chartItems.Add(new LabeledPieChartItem(
                LocalizedResources.OverheadSetting_ExternalWorkOverhead,
                cost.ExternalWorkOverhead,
                cost.OverheadSettings != null,
                cost.OverheadSettings != null ? cost.OverheadSettings.ExternalWorkOverhead : 0m,
                overheadGroupID));

            LabeledPieChartItem totalOverhead = new LabeledPieChartItem(LocalizedResources.General_SumOverheads, cost.TotalSumOverhead, false);
            totalOverhead.GroupID = overheadGroupID;
            totalOverhead.IsPrimaryInGroup = true;
            chartItems.Add(totalOverhead);

            chartItems.Add(new LabeledPieChartItem(
                LocalizedResources.General_MarginRawMaterial,
                cost.RawMaterialMargin,
                cost.OverheadSettings != null,
                cost.OverheadSettings != null ? cost.OverheadSettings.MaterialMargin : 0m,
                marginGroupID));
            chartItems.Add(new LabeledPieChartItem(
                LocalizedResources.General_MarginCommodity,
                cost.CommodityMargin,
                cost.OverheadSettings != null,
                cost.OverheadSettings != null ? cost.OverheadSettings.CommodityMargin : 0m,
                marginGroupID));
            chartItems.Add(new LabeledPieChartItem(
                LocalizedResources.General_MarginConsumable,
                cost.ConsumableMargin,
                cost.OverheadSettings != null,
                cost.OverheadSettings != null ? cost.OverheadSettings.ConsumableMargin : 0m,
                marginGroupID));
            chartItems.Add(new LabeledPieChartItem(
                LocalizedResources.General_MarginManufacturingCost,
                cost.ManufacturingMargin,
                cost.OverheadSettings != null,
                cost.OverheadSettings != null ? cost.OverheadSettings.ManufacturingMargin : 0m,
                marginGroupID));
            chartItems.Add(new LabeledPieChartItem(
                LocalizedResources.General_MarginExternalWork,
                cost.ExternalWorkMargin,
                cost.OverheadSettings != null,
                cost.OverheadSettings != null ? cost.OverheadSettings.ExternalWorkMargin : 0m,
                marginGroupID));

            LabeledPieChartItem totalMargin = new LabeledPieChartItem(LocalizedResources.General_SumMargin, cost.TotalSumMargin, false);
            totalMargin.GroupID = marginGroupID;
            totalMargin.IsPrimaryInGroup = true;
            chartItems.Add(totalMargin);

            return chartItems;
        }

        /// <summary>
        /// Gets the items that should appear in the manufacturing/assembling cost chart.
        /// </summary>
        /// <param name="totalCost">The total manufacturing cost.</param>
        /// <returns>List of chart items.</returns>
        public override Collection<LabeledPieChartItem> GetProcessCostChart(out decimal totalCost)
        {
            totalCost = 0m;
            if (this.CostCalculationResult == null || this.CostCalculationResult.ProcessCost == null)
            {
                return new Collection<LabeledPieChartItem>();
            }

            totalCost = this.CostCalculationResult.ProcessCost.TotalManufacturingCostSum;
            Collection<LabeledPieChartItem> chartItems = new Collection<LabeledPieChartItem>();
            int groupingId = 1;
            ProcessCost cost = this.CostCalculationResult.ProcessCost;
            decimal includedToolDieCost;
            decimal excludedToolDieCost;
            ComputeIncludedToolDieCost(cost, out includedToolDieCost, out excludedToolDieCost);

            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_SumMachineEquipmentCost, cost.MachineCostSum, groupingId));
            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_SumSetUpCost, cost.SetupCostSum, groupingId));
            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_SumDirectLabourCost, cost.DirectLabourCostSum, groupingId));

            // If the excluded tool/die cost is 0 it means that all cost is included, so display it even if it is 0
            if (includedToolDieCost > 0 || excludedToolDieCost <= 0)
            {
                chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_SumDieCost, includedToolDieCost, groupingId));
            }

            if (CostCalculatorFactory.IsNewer(this.CostCalculationResult.CalculatorVersion, "1.0"))
            {
                chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_SumMaintenanceDiesTools, cost.DiesMaintenanceCostSum, groupingId));
            }

            if (cost.EstimatedManufacturingCostSum != 0m)
            {
                chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_SumEstimatedManufacturingCost, cost.EstimatedManufacturingCostSum, groupingId));
            }

            LabeledPieChartItem item = new LabeledPieChartItem(LocalizedResources.General_SumManufacturingCost, cost.ManufacturingCostSum, groupingId);
            item.DisplayOnChart = false;
            item.IsPrimaryInGroup = true;
            chartItems.Add(item);

            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_SumOHOnManufacturing, cost.ManufacturingOverheadSum));
            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_SumRejectCost, cost.RejectCostSum));
            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_NTierTransportCostSum, cost.ManufacturingTransportCost));

            if (excludedToolDieCost > 0)
            {
                LabeledPieChartItem notIncludedItem = new LabeledPieChartItem(LocalizedResources.General_NotIncluded, null, false);
                notIncludedItem.FontWeight = FontWeights.Bold;
                chartItems.Add(notIncludedItem);
                chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_SumDieCost, excludedToolDieCost, false));
            }

            return chartItems;
        }

        /// <summary>
        /// Gets the items that should appear in the part-list chart.
        /// </summary>
        /// <param name="totalCost">The total sub-parts cost.</param>
        /// <returns>List of chart items.</returns>
        public override Collection<LabeledPieChartItem> GetPartListChart(out decimal totalCost)
        {
            throw new NotSupportedException("Parts-List chart is not supported for Part entities.");
        }

        /// <summary>
        /// Gets the chart items for displaying a process step cost.
        /// </summary>
        /// <param name="cost">The process step cost.</param>
        /// <param name="totalCost">The total cost to display under the chart legend.</param>
        /// <returns>List of chart items.</returns>
        public override Collection<LabeledPieChartItem> GetProcessStepCostChart(ProcessStepCost cost, out decimal totalCost)
        {
            totalCost = 0m;
            if (cost == null)
            {
                return new Collection<LabeledPieChartItem>();
            }

            totalCost = cost.TotalManufacturingCost;
            Collection<LabeledPieChartItem> chartItems = new Collection<LabeledPieChartItem>();
            int groupingId = 1;

            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_MachineEquipmentCost, cost.MachineCost, groupingId));
            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_SetUpCost, cost.SetupCost, groupingId));
            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_DirectLabourCost, cost.DirectLabourCost, groupingId));
            if (cost.SBMActive)
            {
                chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_ToolAndDieCost, cost.ToolAndDieCost, groupingId));
            }

            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_MaintenanceDiesTools, cost.DiesMaintenanceCost, groupingId));

            LabeledPieChartItem item = new LabeledPieChartItem(LocalizedResources.General_ManufacturingCost, cost.ManufacturingCost, groupingId);
            item.IsPrimaryInGroup = true;
            item.DisplayOnChart = false;
            chartItems.Add(item);

            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_OHOnManufacturing, cost.ManufacturingOverheadCost));
            chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_RejectCost, cost.RejectCost));

            if (!cost.SBMActive)
            {
                LabeledPieChartItem notIncludedItem = new LabeledPieChartItem(LocalizedResources.General_NotIncluded, null, false);
                notIncludedItem.FontWeight = FontWeights.Bold;
                chartItems.Add(notIncludedItem);
                chartItems.Add(new LabeledPieChartItem(LocalizedResources.General_ToolAndDieCost, cost.ToolAndDieCost, false));
            }

            return chartItems;
        }
    }
}
