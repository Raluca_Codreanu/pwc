﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Calculations.CostCalculation;

namespace ZPKTool.Gui.ViewModels.ResultDetails
{
    /// <summary>
    /// This class is used for data binding in the Commodities data grid in the ResultDetails view.
    /// </summary>
    public class CommodityBreakdownGridItem
    {
        /// <summary>
        /// Gets or sets the cost of the commodity.
        /// </summary>
        public CommodityCost Cost { get; set; }

        /// <summary>
        /// Gets or sets the name of the commodity's parent entity.
        /// </summary>
        public string ParentName { get; set; }
    }
}
