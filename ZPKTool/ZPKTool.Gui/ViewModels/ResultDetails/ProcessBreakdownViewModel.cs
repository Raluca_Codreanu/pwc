﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using ZPKTool.Business;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Controls;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.ViewModels.ResultDetails;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for process breakdown tab from result details view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ProcessBreakdownViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The data grid row background for items having external parent part.
        /// </summary>
        private static readonly SolidColorBrush GridRowBackground = new SolidColorBrush(Color.FromRgb(191, 191, 191));

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The entity for which to compute and display the costs.
        /// </summary>
        private object entity;

        /// <summary>
        /// The measurement units adapter.
        /// </summary>
        private UnitsAdapter measurementUnitsAdapter;

        /// <summary>
        /// The chart builder, used for building chart items from cost calculation results.
        /// </summary>
        private CostChartBuilder chartBuilder;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessBreakdownViewModel"/> class.
        /// </summary>
        /// <param name="messenger">The messenger.</param>
        [ImportingConstructor]
        public ProcessBreakdownViewModel(IMessenger messenger)
        {
            Argument.IsNotNull("messenger", messenger);
            this.messenger = messenger;

            this.ChartItems.Value = new Collection<LabeledPieChartItem>();
            this.DataGridItems.Value = new Collection<ProcessBreakdownGridItem>();

            this.CurrentUserName = SecurityManager.Instance.CurrentUser != null ? SecurityManager.Instance.CurrentUser.Name : string.Empty;

            this.MouseDoubleClickCommand = new DelegateCommand(this.NavigateToItem);
            this.LoadingRowCommand = new DelegateCommand<DataGridRowEventArgs>(this.OnDataGridRowLoading);

            GridRowBackground.Freeze();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the entity for which to compute and display the costs.
        /// </summary>
        public object Entity
        {
            get
            {
                return this.entity;
            }

            set
            {
                Argument.IsNotNull("Entity", value);
                if (this.entity != value)
                {
                    var entityType = value.GetType();
                    if (entityType != typeof(Assembly) && entityType != typeof(Part) && entityType != typeof(RawPart))
                    {
                        throw new InvalidOperationException("The entity type: " + entityType.FullName + " is not supported. Entity must be Assembly, Part or RawPart.");
                    }

                    this.entity = value;
                    OnPropertyChanged(() => this.Entity);
                }
            }
        }

        /// <summary>
        /// Gets or sets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter
        {
            get
            {
                return this.measurementUnitsAdapter;
            }

            set
            {
                if (this.measurementUnitsAdapter != value)
                {
                    this.measurementUnitsAdapter = value;
                    OnPropertyChanged(() => this.MeasurementUnitsAdapter);
                }
            }
        }

        /// <summary>
        /// Gets or sets the chart builder, used for building chart items from cost calculation results.
        /// </summary>
        public CostChartBuilder ChartBuilder
        {
            get
            {
                return this.chartBuilder;
            }

            set
            {
                Argument.IsNotNull("ChartBuilder", value);
                if (this.chartBuilder != value)
                {
                    this.chartBuilder = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the identifier of database manager.
        /// </summary>
        public DbIdentifier DatabaseManagerId { get; set; }

        /// <summary>
        /// Gets or sets the normal cost calculation result.
        /// </summary>
        public CalculationResult NormalCalculationResult { get; set; }

        /// <summary>
        /// Gets or sets the token for sending message notifications for parent view model.
        /// </summary>
        public Guid MessengerToken { get; set; }

        /// <summary>
        /// Gets the name of the current logged-in user.
        /// </summary>
        public string CurrentUserName { get; private set; }

        /// <summary>
        /// Gets or sets the extra information for data grid reports.
        /// </summary>
        public ExtendedDataGridAdditionalReportInformation AdditionalReportInformation { get; set; }

        /// <summary>
        /// Gets the title of breakdown.
        /// </summary>
        public VMProperty<string> Title { get; private set; }

        /// <summary>
        /// Gets or sets the visibility of tab containing the breakdown.
        /// </summary>
        public VMProperty<Visibility> TabVisibility { get; set; }

        /// <summary>
        /// Gets or sets the current chart mode.
        /// </summary>
        public VMProperty<ChartModes> ChartMode { get; set; }

        /// <summary>
        /// Gets the items to display in chart.
        /// </summary>
        public VMProperty<ICollection<LabeledPieChartItem>> ChartItems { get; private set; }

        /// <summary>
        /// Gets the total cost of entity.
        /// </summary>
        public VMProperty<decimal?> TotalCost { get; private set; }

        /// <summary>
        /// Gets the total cost's label of entity.
        /// </summary>
        public VMProperty<string> TotalCostLabel { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the entity has costs (total value > 0) or not.
        /// </summary>
        public VMProperty<bool> EntityHasCosts { get; private set; }

        /// <summary>
        /// Gets the cost items displayed in the data grid.
        /// </summary>
        public VMProperty<ICollection<ProcessBreakdownGridItem>> DataGridItems { get; private set; }

        /// <summary>
        /// Gets the selected cost item from the data grid.
        /// </summary>
        public VMProperty<ProcessBreakdownGridItem> SelectedDataGridItem { get; private set; }

        #endregion

        #region Commands

        /// <summary>
        /// Gets the command for the mouse double click event in process data grid.
        /// </summary>
        public ICommand MouseDoubleClickCommand { get; private set; }

        /// <summary>
        /// Gets the command for loading row event in process data grid.
        /// </summary>
        public ICommand LoadingRowCommand { get; private set; }

        #endregion

        #region Implementation

        /// <summary>
        /// Initializes costs from the breakdown.
        /// </summary>
        public void InitializeBreakdown()
        {
            if (this.Entity is Assembly)
            {
                this.Title.Value = LocalizedResources.General_Assembling;
            }
            else
            {
                this.Title.Value = LocalizedResources.General_Manufacturing;
            }

            // Attach event handler for total cost changed before setting the cost, to raise event on initialization too
            this.TotalCost.ValueChanged += (s, e) => this.OnTotalCostChanged();

            this.SetupChart();
            this.SetupProcessDataGrid();

            this.ChartMode.ValueChanged += (s, e) => this.OnChartModeChanged();
        }

        /// <summary>
        /// Refreshes costs from the breakdown (called after the costs have changed).
        /// </summary>
        public void RefreshBreakdown()
        {
            this.SetupChart();
            this.SetupProcessDataGrid();
        }

        /// <summary>
        /// Gets the costs information displayed in the chart.
        /// </summary>
        private void SetupChart()
        {
            decimal totalCost;

            this.ChartItems.Value = this.ChartBuilder.GetProcessCostChart(out totalCost);
            this.TotalCost.Value = totalCost;

            if (this.Entity is Part)
            {
                this.TotalCostLabel.Value = LocalizedResources.General_TotalSumManufacturingCost;
            }
            else if (this.Entity is Assembly)
            {
                this.TotalCostLabel.Value = LocalizedResources.General_TotalSumAssemblingCost;
            }
        }

        /// <summary>
        /// Gets the items to display in the data grid.
        /// </summary>
        private void SetupProcessDataGrid()
        {
            var processItems = new List<ProcessBreakdownGridItem>();
            int sortNumber = 1;

            if (this.ChartBuilder.CostCalculationResult != null)
            {
                foreach (ProcessStepCost cost in this.ChartBuilder.CostCalculationResult.ProcessCost.StepCosts.OrderBy(c => c.StepIndex))
                {
                    ProcessBreakdownGridItem item = new ProcessBreakdownGridItem();
                    item.StepCost = cost;

                    // Assigned order numbers only to the cost of the steps from the non-enhanced calculation. The steps from the enhanced calculation will
                    // be ordered by the OrderProcessDataGridSourceForEnhancedBreakdown call.
                    if (this.ChartBuilder.CostCalculationResult.ProcessCost.StepCosts.FirstOrDefault(stepCost => stepCost.StepId == cost.StepId) != null)
                    {
                        item.OrderNumber = sortNumber.ToString(CultureInfo.InvariantCulture);
                        item.SortNumber = sortNumber;
                        sortNumber++;
                    }

                    processItems.Add(item);
                }
            }

            if (this.ChartBuilder.IsEnhancedCostCalculationResult)
            {
                // Order the steps in an enhanced calculation on levels                
                OrderProcessItemsForEnhancedBreakdown(processItems, this.NormalCalculationResult, 2, ref sortNumber);
            }

            processItems.Sort(delegate(ProcessBreakdownGridItem x, ProcessBreakdownGridItem y)
            {
                if (x.SortNumber < y.SortNumber)
                {
                    return -1;
                }
                else if (x.SortNumber == y.SortNumber)
                {
                    return 0;
                }
                else
                {
                    return 1;
                }
            });

            this.DataGridItems.Value = processItems;
        }

        /// <summary>
        /// Assigns appropriate order number and sort number to all process steps items in a process data grid source list, for the enhanced breakdown.
        /// </summary>
        /// <param name="datagridSource">The process data grid source items.</param>
        /// <param name="originalResult">The original result from which the enhanced breakdown result was built.</param>
        /// <param name="level">The level on which to order (1 based). Non-recursive calls should set it to 1.</param>
        /// <param name="lastSortNumber">The last sort number assigned by this call. Used in recursive calls so set it to 1 in non-recursive calls.</param>
        /// <returns>The list of parts and assemblies costs based on which the process steps were ordered.</returns>
        private List<object> OrderProcessItemsForEnhancedBreakdown(List<ProcessBreakdownGridItem> datagridSource, CalculationResult originalResult, int level, ref int lastSortNumber)
        {
            List<object> parentCosts = new List<object>();

            // The current order number on the level
            int orderNumber = 1;

            string indent = string.Empty;
            for (int i = 0; i < level - 1; i++)
            {
                indent += "  ";
            }

            if (originalResult != null)
            {
                foreach (PartCost partCost in originalResult.PartsCost.PartCosts)
                {
                    // Count the steps because the step's index property gives the order but the indexes of steps are not necessarily consecutive or starting at 0
                    int stepNo = 1;
                    foreach (ProcessStepCost stepCost in partCost.FullCalculationResult.ProcessCost.StepCosts.OrderBy(s => s.StepIndex))
                    {
                        // Generate consecutive order numbers for each step
                        ProcessBreakdownGridItem stepItem = datagridSource.Where(item => item.StepCost.StepId == stepCost.StepId).FirstOrDefault();
                        if (stepItem != null && stepItem.StepCost != null)
                        {
                            stepItem.OrderNumber = indent + orderNumber + "." + stepNo;
                            stepItem.SortNumber = lastSortNumber;

                            lastSortNumber++;
                            stepNo++;
                        }
                    }

                    // Increase the order number only if the part's process step(s)
                    if (partCost.FullCalculationResult.ProcessCost.StepCosts.Count > 0)
                    {
                        orderNumber++;
                        parentCosts.Add(partCost);
                    }
                }

                foreach (AssemblyCost assyCost in originalResult.AssembliesCost.AssemblyCosts)
                {
                    // Count the steps because the step's index property gives the order but the indexes of steps are not necessarily consecutive or starting at 0
                    int stepNo = 1;

                    // Generate consecutive order numbers for each step                                
                    foreach (ProcessStepCost stepCost in assyCost.FullCalculationResult.ProcessCost.StepCosts.OrderBy(s => s.StepIndex))
                    {
                        ProcessBreakdownGridItem stepItem = datagridSource.Where(item => item.StepCost.StepId == stepCost.StepId).FirstOrDefault();
                        if (stepItem != null && stepItem.StepCost != null)
                        {
                            stepItem.OrderNumber = indent + orderNumber + "." + stepNo;
                            stepItem.SortNumber = lastSortNumber;

                            lastSortNumber++;
                            stepNo++;
                        }
                    }

                    // Increase the order number only if the assembly's process step(s)
                    if (assyCost.FullCalculationResult.ProcessCost.StepCosts.Count > 0)
                    {
                        orderNumber++;
                        parentCosts.Add(assyCost);
                    }

                    List<object> costs = OrderProcessItemsForEnhancedBreakdown(datagridSource, assyCost.FullCalculationResult, level + 1, ref lastSortNumber);
                    parentCosts.AddRange(costs);
                }
            }

            return parentCosts;
        }

        /// <summary>
        /// Navigates in the project's tree, to the selected item from the data grid.
        /// </summary>
        private void NavigateToItem()
        {
            if (this.SelectedDataGridItem.Value != null)
            {
                var stepCost = this.SelectedDataGridItem.Value.StepCost;
                if (stepCost != null)
                {
                    var navigationTarget = new AssemblyProcessStep() { Guid = stepCost.StepId };

                    NavigateToEntityMessage navMessage = new NavigateToEntityMessage(navigationTarget, this.DatabaseManagerId);
                    this.messenger.Send(navMessage, this.IsInViewerMode ? GlobalMessengerTokens.ModelBrowserTargetToken : GlobalMessengerTokens.MainViewTargetToken);
                }
            }
        }

        /// <summary>
        /// Called when the current chart mode changed, notifies the parent view model that the chart mode changed, to refresh it in all breakdowns.
        /// </summary>
        private void OnChartModeChanged()
        {
            var message = new NotificationMessage<ChartModes>(Notification.ResultDetailsChartModeChanged, this.ChartMode.Value);
            this.messenger.Send<NotificationMessage<ChartModes>>(message, this.MessengerToken);
        }

        /// <summary>
        /// Called when the total costs of the entity changed, checks if the entity has costs or not.
        /// </summary>
        private void OnTotalCostChanged()
        {
            this.EntityHasCosts.Value = this.TotalCost.Value.HasValue && this.TotalCost.Value != 0m;
        }

        /// <summary>
        /// Called when each row from data grid is loading, sets the background and tooltip for items having external parent part.
        /// </summary>
        /// <param name="e">The <see cref="DataGridRowEventArgs"/> instance containing the event data.</param>
        private void OnDataGridRowLoading(DataGridRowEventArgs e)
        {
            var item = e.Row.Item as ProcessBreakdownGridItem;
            if (item != null && item.StepCost.ParentPartIsExternal)
            {
                // The row corresponding to a step whose parent entity is external has a special background color
                e.Row.Background = GridRowBackground;
                e.Row.ToolTip = LocalizedResources.ResultDetails_ExternalStepDataGridItemTooltip;
            }
        }

        #endregion
    }
}