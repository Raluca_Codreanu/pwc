﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using ZPKTool.Business;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Reporting;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The kinds of cost reports that can be created.
    /// </summary>
    public enum CostReportType
    {
        /// <summary>
        /// A normal report.
        /// </summary>
        Normal = 0,

        /// <summary>
        /// An enhanced report.
        /// </summary>
        Enhanced = 1
    }

    /// <summary>
    /// Specifies the format of the generated report
    /// </summary>
    public enum CostReportFormat
    {
        /// <summary>
        /// Generated report is a xls file
        /// </summary>
        Excel = 0,

        /// <summary>
        /// Generated report is a pdf file
        /// </summary>
        Pdf = 1
    }

    /// <summary>
    /// The view-model for ...
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class BatchReportingViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The folder browser.
        /// </summary>
        private IFolderBrowserService folderBrowserService;

        /// <summary>
        /// The title displayed by the view.
        /// </summary>
        private string title;

        /// <summary>
        /// The entity for which to create the batch of reports.
        /// </summary>
        private object entity;

        /// <summary>
        /// The data source for the view.
        /// </summary>
        private ObservableCollection<BatchReportingDataSourceItem> dataSource;

        /// <summary>
        /// The type of the report to create.
        /// </summary>
        private CostReportType reportTypeToCreate;

        /// <summary>
        /// The format of the report to create.
        /// </summary>
        private CostReportFormat reportToCreateFormat;

        /// <summary>
        /// The path to the folder containing the last created batch of reports.
        /// </summary>
        private string lastCreatedReportsFolderPath;

        /// <summary>
        /// A value indicating whether this instance is creating the reports.
        /// </summary>
        private bool isCreatingReports;

        /// <summary>
        /// Contains the data source items for parts, which are removed when switching to enhanced report and added back when switching to normal report.
        /// </summary>
        private List<BatchReportingDataSourceItem> partItemsCache = new List<BatchReportingDataSourceItem>();

        /// <summary>
        /// A value indicating whether the source entity is being loaded.
        /// </summary>
        private bool isLoadingEntity;

        /// <summary>
        /// Contains the source item and the longest path length/maximum item length.
        /// </summary>
        private Dictionary<Guid, int> longestPaths = new Dictionary<Guid, int>();

        /// <summary>
        /// Contains the lengths of the folders from the longest path.
        /// </summary>
        private string longestPathFolderLengths;

        /// <summary>
        /// The maximum length of the current item.
        /// </summary>
        private int itemMaximumLength;

        /// <summary>
        /// The longest full path allowed to be created.
        /// </summary>
        private int longestFullPathAllowed = 240;

        /// <summary>
        /// A value indicating if the longest full path allowed to be created is exceeded.
        /// </summary>
        private bool isLongestFullPathAllowedExceeded;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        #endregion Attributes

        /// <summary>
        /// Initializes a new instance of the <see cref="BatchReportingViewModel"/> class.
        /// </summary>
        /// <param name="messenger">The messenger.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="folderBrowserService">The folder browser service.</param>
        /// <param name="unitsService">The units service.</param>
        [ImportingConstructor]
        public BatchReportingViewModel(
            IMessenger messenger,
            IWindowService windowService,
            IFolderBrowserService folderBrowserService,
            IUnitsService unitsService)
        {
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("folderBrowserService", folderBrowserService);
            Argument.IsNotNull("unitsService", unitsService);

            this.messenger = messenger;
            this.windowService = windowService;
            this.folderBrowserService = folderBrowserService;
            this.unitsService = unitsService;

            this.EntitySourceDb = DbIdentifier.NotSet;
            this.Title = LocalizedResources.General_BatchReporting;
            this.ReportTypeToCreate = CostReportType.Normal;
            this.ReportToCreateFormat = CostReportFormat.Pdf;

            this.CreateReportsCommand = new DelegateCommand(CreateReports);
            this.OpenReportsFolderCommand = new DelegateCommand(OpenReportsFolder);
            this.ChangeReportTypeCommand = new DelegateCommand<CostReportType>((param) =>
                {
                    var oldValue = this.ReportTypeToCreate;
                    this.ReportTypeToCreate = param;
                    if (oldValue != param)
                    {
                        RefreshDataSourceBasedOnReportType();
                    }
                });
            this.CheckAllCommand = new DelegateCommand<bool>((param) =>
            {
                if (this.DataSource != null)
                {
                    foreach (BatchReportingDataSourceItem item in this.DataSource)
                    {
                        item.IsChecked = param;
                    }
                }
            });
        }

        #region Commands

        /// <summary>
        /// Gets the create reports command.
        /// </summary>
        public ICommand CreateReportsCommand { get; private set; }

        /// <summary>
        /// Gets the command executed when the report type is changed.
        /// </summary>
        public ICommand ChangeReportTypeCommand { get; private set; }

        /// <summary>
        /// Gets the command that checks all items in DataSource.
        /// </summary>
        public ICommand CheckAllCommand { get; private set; }

        /// <summary>
        /// Gets the command that opens the folder containing the last created batch of reports.
        /// </summary>
        public ICommand OpenReportsFolderCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets or sets the title displayed by the view.
        /// </summary>
        public string Title
        {
            get
            {
                return this.title;
            }

            set
            {
                if (this.title != value)
                {
                    this.title = value;
                    OnPropertyChanged(() => this.Title);
                }
            }
        }

        /// <summary>
        /// Gets or sets the entity for which to create the batch of reports.
        /// The entities currently supported are Project and Assembly.
        /// </summary>      
        /// <exception cref="InvalidOperationException">The value is not supported.</exception>
        public object Entity
        {
            get
            {
                return this.entity;
            }

            set
            {
                if (!(value is Assembly) && !(value is Project))
                {
                    throw new InvalidOperationException("The provided entity is not supported for batch reporting creation.");
                }

                if (this.entity != value)
                {
                    this.entity = value;
                    OnPropertyChanged(() => this.Entity);
                    PopulateDataSource();

                    // Add the entity's name in the Title
                    var objName = EntityUtils.GetEntityName(this.entity);
                    if (!string.IsNullOrWhiteSpace(objName))
                    {
                        this.Title = LocalizedResources.General_BatchReporting + " - " + objName;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating the database where Entity comes from.
        /// It is mandatory to set it if Entity is a Project.
        /// </summary>
        public DbIdentifier EntitySourceDb { get; set; }

        /// <summary>
        /// Gets or sets the Entity's parent object, when the Entity is not a Project.
        /// </summary>
        public Project EntityParent { get; set; }

        /// <summary>
        /// Gets or sets the data source for the view.
        /// </summary>
        /// <value>The data source.</value>
        public ObservableCollection<BatchReportingDataSourceItem> DataSource
        {
            get
            {
                return this.dataSource;
            }

            set
            {
                if (this.dataSource != value)
                {
                    this.dataSource = value;
                    OnPropertyChanged(() => this.DataSource);
                }
            }
        }

        /// <summary>
        /// Gets or sets the type of the report to create.
        /// </summary>
        public CostReportType ReportTypeToCreate
        {
            get
            {
                return this.reportTypeToCreate;
            }

            set
            {
                if (this.reportTypeToCreate != value)
                {
                    this.reportTypeToCreate = value;
                    OnPropertyChanged(() => this.ReportTypeToCreate);
                }
            }
        }

        /// <summary>
        /// Gets or sets the format of the report to create
        /// </summary>
        public CostReportFormat ReportToCreateFormat
        {
            get
            {
                return this.reportToCreateFormat;
            }

            set
            {
                if (this.reportToCreateFormat != value)
                {
                    this.reportToCreateFormat = value;
                    OnPropertyChanged(() => this.ReportToCreateFormat);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is creating the reports.
        /// </summary>        
        public bool IsCreatingReports
        {
            get
            {
                return this.isCreatingReports;
            }

            set
            {
                if (this.isCreatingReports != value)
                {
                    this.isCreatingReports = value;
                    OnPropertyChanged(() => this.IsCreatingReports);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the source entity is being loaded.
        /// </summary>        
        public bool IsLoadingEntity
        {
            get
            {
                return this.isLoadingEntity;
            }

            set
            {
                if (this.isLoadingEntity != value)
                {
                    this.isLoadingEntity = value;
                    OnPropertyChanged(() => this.IsLoadingEntity);
                }
            }
        }

        /// <summary>
        /// Gets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter { get; private set; }

        #endregion Properties

        #region Populate and refresh the Data Source

        /// <summary>
        /// Populates the data source from the Entity.
        /// </summary>
        private void PopulateDataSource()
        {
            this.DataSource = null;
            this.partItemsCache.Clear();

            if (this.Entity == null)
            {
                return;
            }

            Assembly assy = this.Entity as Assembly;
            if (assy != null)
            {
                var item = CreateDataSourceItem(assy);
                this.DataSource = new ObservableCollection<BatchReportingDataSourceItem>(item.Children);
            }

            // otherwise the entity is a project which we have to load in the background and display a loading screen.
        }

        /// <summary>
        /// Creates a data source item for a data entity, recursively creating children for its sub-objects.
        /// </summary>
        /// <param name="dataEntity">The data entity.</param>
        /// <returns>The created data source item.</returns>
        private BatchReportingDataSourceItem CreateDataSourceItem(object dataEntity)
        {
            if (dataEntity == null)
            {
                return null;
            }

            BatchReportingDataSourceItem item = null;
            List<object> childEntities = new List<object>();

            Assembly assy = dataEntity as Assembly;
            if (assy != null)
            {
                item = new BatchReportingDataSourceItem();
                item.Entity = assy;
                item.DisplayName = assy.Name;
                item.TypeIcon = Images.AssemblyIcon;

                childEntities.AddRange(assy.Subassemblies);
                childEntities.AddRange(assy.Parts);
            }
            else
            {
                Part part = dataEntity as Part;
                if (part != null)
                {
                    // The parts are not displayed for enhanced reports
                    if (this.ReportTypeToCreate == CostReportType.Normal)
                    {
                        item = new BatchReportingDataSourceItem()
                            {
                                Entity = part,
                                DisplayName = part.Name,
                                TypeIcon = Images.PartIcon
                            };
                        this.partItemsCache.Add(item);
                    }
                }
                else
                {
                    Project project = dataEntity as Project;
                    if (project != null)
                    {
                        // The project is not displayed in the tree so we create just its children.
                        item = new BatchReportingDataSourceItem();
                        childEntities.AddRange(project.Assemblies);
                        childEntities.AddRange(project.Parts);
                    }
                }
            }

            if (item != null)
            {
                item.Children = new ObservableCollection<BatchReportingDataSourceItem>();
                foreach (Assembly subAssy in childEntities.OfType<Assembly>().Where(a => !a.IsDeleted).OrderBy(a => a.Name))
                {
                    BatchReportingDataSourceItem subItem = CreateDataSourceItem(subAssy);
                    if (subItem != null)
                    {
                        item.Children.Add(subItem);
                    }
                }

                foreach (Part subPart in childEntities.OfType<Part>().Where(a => !a.IsDeleted).OrderBy(a => a.Name))
                {
                    BatchReportingDataSourceItem subItem = CreateDataSourceItem(subPart);
                    if (subItem != null)
                    {
                        item.Children.Add(subItem);
                    }
                }
            }

            return item;
        }

        /// <summary>
        /// Refreshes the the data source to reflect the current report type.
        /// The parts are hidden for the enhanced report and shown for normal report.
        /// </summary>
        private void RefreshDataSourceBasedOnReportType()
        {
            if (this.dataSource == null)
            {
                return;
            }

            BatchReportingDataSourceItem tempItem = new BatchReportingDataSourceItem();
            tempItem.Entity = this.entity;
            tempItem.Children = this.dataSource;
            RefreshDataSourceItemChildren(tempItem);
        }

        /// <summary>
        /// Refreshes the data source item's children from its backing Entity.
        /// </summary>
        /// <param name="item">The item to refresh.</param>
        private void RefreshDataSourceItemChildren(BatchReportingDataSourceItem item)
        {
            if (this.ReportTypeToCreate == CostReportType.Enhanced)
            {
                // For enhanced report remove all parts from the item's graph
                foreach (var child in item.Children.ToList())
                {
                    if (child.Entity is Part)
                    {
                        item.Children.Remove(child);
                    }
                    else
                    {
                        RefreshDataSourceItemChildren(child);
                    }
                }
            }
            else if (this.ReportTypeToCreate == CostReportType.Normal)
            {
                // Add back the Part children that were removed for enhanced report.
                List<Part> subParts = new List<Part>();
                Assembly assy = item.Entity as Assembly;
                if (assy != null)
                {
                    subParts.AddRange(assy.Parts);
                }
                else
                {
                    Project proj = item.Entity as Project;
                    if (proj != null)
                    {
                        subParts.AddRange(proj.Parts);
                    }
                }

                foreach (Part subPart in subParts.Where(p => !p.IsDeleted).OrderBy(p => p.Name))
                {
                    var child = this.partItemsCache.FirstOrDefault(it => it.Entity is Part && ((Part)it.Entity).Guid == subPart.Guid);
                    if (child != null)
                    {
                        item.Children.Add(child);
                    }
                    else
                    {
                        item.Children.Add(new BatchReportingDataSourceItem()
                        {
                            Entity = subPart,
                            DisplayName = subPart.Name,
                            TypeIcon = Images.PartIcon
                        });
                    }
                }

                // Refresh the Assembly children.
                foreach (var child in item.Children.Where(c => c.Entity is Assembly))
                {
                    RefreshDataSourceItemChildren(child);
                }
            }
        }

        #endregion Populate and refresh the Data Source

        #region Create reports

        /// <summary>
        /// Opens the last created reports folder.
        /// </summary>
        private void OpenReportsFolder()
        {
            if (string.IsNullOrWhiteSpace(this.lastCreatedReportsFolderPath))
            {
                return;
            }

            try
            {
                string args = "/n,/e," + this.lastCreatedReportsFolderPath;
                System.Diagnostics.Process.Start("explorer.exe", args);
            }
            catch
            {
            }
        }

        /// <summary>
        /// Creates the reports for the selected data source items.
        /// </summary>
        private void CreateReports()
        {
            if (this.Entity == null
                || this.DataSource == null
                || (this.Entity is Assembly && this.EntityParent == null))
            {
                return;
            }

            if (this.Entity is Project && this.EntityParent == null)
            {
                // Necessary in order to successfully calculate Part costs.
                this.EntityParent = (Project)this.Entity;
            }

            // Get the source entity's name.
            string entityName = EntityUtils.GetEntityName(this.Entity);

            if (string.IsNullOrWhiteSpace(entityName))
            {
                log.Error("The name of the entity for which to create the reports was null or empty. Report creation will stop.");
                return;
            }

            // Select the folder where to create the reports
            this.folderBrowserService.Description = string.Format(LocalizedResources.BatchReporting_FolderBrowserDescription, entityName);
            bool ok = this.folderBrowserService.ShowFolderBrowserDialog();
            if (!ok)
            {
                return;
            }

            if (this.EntitySourceDb == DbIdentifier.NotSet)
            {
                log.Warn("The EntitySourceDb was not set => the reports will not contain pictures.");
            }

            // Create the reports in the background.
            string selectedPath = this.folderBrowserService.SelectedPath;

            // Determine if the longest full path allowed will be exceeded and if so determine the new maximum length of a folder.
            string entityFullFolderPath = System.IO.Path.Combine(selectedPath, PathUtils.SanitizeFolderPath(entityName));
            entityFullFolderPath = this.GetValidFolderPath(entityFullFolderPath);

            this.longestPaths.Clear();
            this.itemMaximumLength = this.longestFullPathAllowed;
            this.isLongestFullPathAllowedExceeded = false;

            // If there are no data source items determine the maximum length based on the entity, or else determine the maximum length based on the data source items.
            if (this.DataSource.Count == 0)
            {
                Assembly assy = this.Entity as Assembly;
                if (assy != null)
                {
                    BatchReportingDataSourceItem rootItem = new BatchReportingDataSourceItem();
                    rootItem.Entity = this.Entity;
                    rootItem.IsChecked = true;
                    rootItem.DisplayName = entityName;

                    this.ResolveFullPathLength(selectedPath, rootItem, assy.Guid);
                }
            }
            else
            {
                foreach (var sourceItem in this.DataSource)
                {
                    Assembly assy = sourceItem.Entity as Assembly;
                    if (assy != null)
                    {
                        this.ResolveFullPathLength(entityFullFolderPath, sourceItem, assy.Guid);
                    }
                    else
                    {
                        Part part = sourceItem.Entity as Part;
                        if (part != null)
                        {
                            this.ResolveFullPathLength(entityFullFolderPath, sourceItem, part.Guid);
                        }
                    }
                }
            }

            // If the full path allowed was exceeded ask the user what to do.
            if (this.isLongestFullPathAllowedExceeded)
            {
                if (this.itemMaximumLength <= 0)
                {
                    var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_TrimToZero, MessageDialogType.Info);
                    if (result == MessageDialogResult.OK)
                    {
                        this.CreateReports();

                        return;
                    }
                }
                else if (this.itemMaximumLength <= 3)
                {
                    var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_TrimToSmall, MessageDialogType.YesNoCancel);
                    if (result == MessageDialogResult.No)
                    {
                        this.CreateReports();
                        return;
                    }
                    else if (result == MessageDialogResult.Cancel)
                    {
                        return;
                    }
                }
                else
                {
                    var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_TrimItems, MessageDialogType.YesNoCancel);
                    if (result == MessageDialogResult.No)
                    {
                        this.CreateReports();
                        return;
                    }
                    else if (result == MessageDialogResult.Cancel)
                    {
                        return;
                    }
                }
            }

            Task.Factory.StartNew(() =>
            {
                this.IsCreatingReports = true;

                // Create reports for Assembly source entity.
                Assembly assembly = this.entity as Assembly;
                if (assembly != null)
                {
                    // Calculate the cost of the assembly.
                    ICostCalculator calculator = CostCalculatorFactory.GetCalculator(assembly.CalculationVariant);
                    var calculationParams = CostCalculationHelper.CreateAssemblyParamsFromProject(this.EntityParent);
                    CalculationResult cost = calculator.CalculateAssemblyCost(assembly, calculationParams);

                    // Convert the calculation result to use UI units.
                    var conversionFactors = this.MeasurementUnitsAdapter.GetUnitConversionFactors();
                    cost = CurrencyConversionManager.ConvertFromBaseUnit(cost, conversionFactors);

                    // Create the report for the Entity
                    BatchReportingDataSourceItem item = new BatchReportingDataSourceItem();
                    item.Entity = this.Entity;
                    item.IsChecked = true;

                    // root folder is built in the create reports method and returned to be used for the children
                    string entityFolderPath = CreateReports(item, cost, selectedPath);
                    if (entityFolderPath != null)
                    {
                        // Create the reports for all checked items.
                        foreach (var sourceItem in this.DataSource)
                        {
                            Assembly assy = sourceItem.Entity as Assembly;
                            if (assy != null)
                            {
                                this.itemMaximumLength = this.longestPaths[assy.Guid];
                            }
                            else
                            {
                                Part part = sourceItem.Entity as Part;
                                if (part != null)
                                {
                                    this.itemMaximumLength = this.longestPaths[part.Guid];
                                }
                            }

                            CalculationResult childCost = GetChildCost(sourceItem, cost);
                            if (childCost != null)
                            {
                                CreateReports(sourceItem, childCost, entityFolderPath);
                            }
                        }

                        this.lastCreatedReportsFolderPath = entityFolderPath;
                    }
                }
                else
                {
                    // Create reports for Project source entity.
                    Project project = this.entity as Project;
                    if (project != null)
                    {
                        string entityFolderPath = System.IO.Path.Combine(selectedPath, PathUtils.SanitizeFolderPath(entityName));
                        entityFolderPath = this.GetValidFolderPath(entityFolderPath);

                        foreach (var item in this.DataSource)
                        {
                            // Calculate the cost of the item.
                            CalculationResult cost = null;
                            Assembly subAssy = item.Entity as Assembly;
                            if (subAssy != null)
                            {
                                ICostCalculator calculator = CostCalculatorFactory.GetCalculator(subAssy.CalculationVariant);
                                var calculationParams = CostCalculationHelper.CreateAssemblyParamsFromProject(project);
                                cost = calculator.CalculateAssemblyCost(subAssy, calculationParams);

                                this.itemMaximumLength = this.longestPaths[subAssy.Guid];
                            }
                            else
                            {
                                Part subPart = item.Entity as Part;
                                if (subPart != null)
                                {
                                    ICostCalculator calculator = CostCalculatorFactory.GetCalculator(subPart.CalculationVariant);
                                    var calculationParams = CostCalculationHelper.CreatePartParamsFromProject(project);
                                    cost = calculator.CalculatePartCost(subPart, calculationParams);

                                    this.itemMaximumLength = this.longestPaths[subPart.Guid];
                                }
                            }

                            if (cost != null)
                            {
                                // Convert the calculation result to use UI units.
                                var conversionFactors = this.MeasurementUnitsAdapter.GetUnitConversionFactors();
                                cost = CurrencyConversionManager.ConvertFromBaseUnit(cost, conversionFactors);

                                // Create the reports for all checked items and their children.
                                CreateReports(item, cost, entityFolderPath);
                            }
                        }

                        this.lastCreatedReportsFolderPath = entityFolderPath;
                    }
                }
            })
            .ContinueWith((task) =>
            {
                this.IsCreatingReports = false;
                if (task.Exception != null)
                {
                    Exception error = task.Exception.InnerException;
                    log.ErrorException("Batch report creation error.", error);

                    if (error is System.IO.IOException)
                    {
                        this.windowService.MessageDialogService.Show(LocalizedResources.General_FileIOError, MessageDialogType.Error);
                    }
                    else if (error is System.UnauthorizedAccessException)
                    {
                        this.windowService.MessageDialogService.Show(LocalizedResources.General_FileAccessError, MessageDialogType.Error);
                    }

                    this.windowService.MessageDialogService.Show(error);
                }
            });
        }

        /// <summary>
        /// Creates the reports for a data source items and all its children, recursively.
        /// </summary>
        /// <param name="item">The item to create the reports for.</param>
        /// <param name="cost">The cost of the item.</param>
        /// <param name="folderPath">The path where to create the reports.</param>
        /// <returns>The folder path in which the report was created</returns>
        private string CreateReports(BatchReportingDataSourceItem item, CalculationResult cost, string folderPath)
        {
            ReportsManager reportsManager = new ReportsManager(this.EntitySourceDb, this.MeasurementUnitsAdapter);

            Assembly assy = item.Entity as Assembly;
            if (assy != null)
            {
                if (string.IsNullOrWhiteSpace(assy.Name))
                {
                    log.Error("The name of the assembly item for which to create the reports was null or empty. The reports for this item and its children will not be created.");
                    return null;
                }

                // An assembly report is placed in a folder named after the assembly.                
                string assyFolderPath = System.IO.Path.Combine(folderPath, ZPKTool.Common.PathUtils.SanitizeFolderPath(assy.Name.Length <= this.itemMaximumLength ? assy.Name : assy.Name.Substring(0, this.itemMaximumLength)));
                assyFolderPath = this.GetValidFolderPath(assyFolderPath);

                if (item.IsChecked == true)
                {
                    try
                    {
                        // Create report only for checked items.
                        System.IO.Directory.CreateDirectory(assyFolderPath);
                    }
                    catch (Exception ex)
                    {
                        log.ErrorException("Error occured while creating directory " + assyFolderPath, ex);
                        throw ZPKTool.Common.PathUtils.ParseFileRelatedException(ex);
                    }

                    if (this.ReportTypeToCreate == CostReportType.Normal)
                    {
                        string reportFileName = ReportsHelper.GenerateReportFileName(assy, false);
                        string reportFilePath = System.IO.Path.Combine(assyFolderPath, reportFileName);

                        if (this.ReportToCreateFormat == CostReportFormat.Excel)
                        {
                            reportFilePath = ZPKTool.Common.PathUtils.FixFilePathForCollisions(reportFilePath + ".xls");
                            reportsManager.CreateAssemblyReportXls(assy, cost, reportFilePath);
                        }
                        else if (this.ReportToCreateFormat == CostReportFormat.Pdf)
                        {
                            reportFilePath = ZPKTool.Common.PathUtils.FixFilePathForCollisions(reportFilePath + ".pdf");
                            reportsManager.CreateAssemblyReportPdf(assy, cost, reportFilePath);
                        }
                    }
                    else if (this.ReportTypeToCreate == CostReportType.Enhanced)
                    {
                        string reportFileName = ReportsHelper.GenerateReportFileName(assy, true);
                        string reportFilePath = System.IO.Path.Combine(assyFolderPath, reportFileName);

                        if (this.ReportToCreateFormat == CostReportFormat.Excel)
                        {
                            reportFilePath = ZPKTool.Common.PathUtils.FixFilePathForCollisions(reportFilePath + ".xls");
                            reportsManager.CreateEnhancedAssemblyReportXls(cost, reportFilePath);
                        }
                        else if (this.ReportToCreateFormat == CostReportFormat.Pdf)
                        {
                            reportFilePath = ZPKTool.Common.PathUtils.FixFilePathForCollisions(reportFilePath + ".pdf");
                            reportsManager.CreateEnhancedAssemblyReportPdf(cost, reportFilePath);
                        }
                    }
                }

                foreach (BatchReportingDataSourceItem child in item.Children)
                {
                    CalculationResult childCost = GetChildCost(child, cost);
                    if (childCost != null)
                    {
                        CreateReports(child, childCost, assyFolderPath);
                    }
                }

                return assyFolderPath;
            }
            else if (item.IsChecked == true)
            {
                Part part = item.Entity as Part;
                if (part != null)
                {
                    if (string.IsNullOrWhiteSpace(part.Name))
                    {
                        log.Error("The name of the part item for which to create the reports was null or empty. The report for this item will not be created.");
                        return null;
                    }

                    if (this.ReportTypeToCreate == CostReportType.Normal)
                    {
                        if (!System.IO.Directory.Exists(folderPath))
                        {
                            try
                            {
                                System.IO.Directory.CreateDirectory(folderPath);
                            }
                            catch (Exception ex)
                            {
                                log.ErrorException("Error occured while creating directory " + folderPath, ex);
                                throw PathUtils.ParseFileRelatedException(ex);
                            }
                        }

                        string reportFileName = ReportsHelper.GenerateReportFileName(part, false);
                        string filePath = System.IO.Path.Combine(folderPath, reportFileName);

                        if (this.reportToCreateFormat == CostReportFormat.Excel)
                        {
                            filePath = PathUtils.FixFilePathForCollisions(filePath + ".xls");
                            reportsManager.CreatePartReportXls(part, cost, filePath);
                        }
                        else if (this.ReportToCreateFormat == CostReportFormat.Pdf)
                        {
                            filePath = PathUtils.FixFilePathForCollisions(filePath + ".pdf");
                            reportsManager.CreatePartReportPdf(part, cost, filePath);
                        }
                    }
                }
            }

            return folderPath;
        }

        /// <summary>
        /// Gets the cost object corresponding to a child data source item from the cost object of its parent.
        /// </summary>
        /// <param name="child">The child.</param>
        /// <param name="parentCost">The parent's cost.</param>
        /// <returns>A cost object.</returns>
        private CalculationResult GetChildCost(BatchReportingDataSourceItem child, CalculationResult parentCost)
        {
            CalculationResult childCost = null;

            Assembly assyChild = child.Entity as Assembly;
            if (assyChild != null)
            {
                var assyCost = parentCost.AssembliesCost.AssemblyCosts.FirstOrDefault(a => a.AssemblyId == assyChild.Guid);
                if (assyCost != null)
                {
                    childCost = assyCost.FullCalculationResult;
                }
                else
                {
                    // The assy is not used in its parent's process so its parent's cost calculation does not contain its cost.
                    Project parentProject = this.EntityParent as Project;
                    if (parentProject != null)
                    {
                        ICostCalculator calculator = CostCalculatorFactory.GetCalculator(assyChild.CalculationVariant);
                        var calculationParams = CostCalculationHelper.CreateAssemblyParamsFromProject(parentProject);
                        childCost = calculator.CalculateAssemblyCost(assyChild, calculationParams);
                    }
                }
            }
            else
            {
                Part partChild = child.Entity as Part;
                if (partChild != null)
                {
                    var partCost = parentCost.PartsCost.PartCosts.FirstOrDefault(p => p.PartId == partChild.Guid);
                    if (partCost != null)
                    {
                        childCost = partCost.FullCalculationResult;
                    }
                    else
                    {
                        // The part is not used in its parent's process so its parent's cost calculation does not contain its cost.
                        Project parentProject = this.EntityParent as Project;
                        if (parentProject != null)
                        {
                            ICostCalculator calculator = CostCalculatorFactory.GetCalculator(partChild.CalculationVariant);
                            var calculationParams = CostCalculationHelper.CreatePartParamsFromProject(parentProject);
                            childCost = calculator.CalculatePartCost(partChild, calculationParams);
                        }
                    }
                }
            }

            return childCost;
        }

        #endregion Create reports

        /// <summary>
        /// Called before unloading the view from its parent.
        /// Returning false will cancel the view's unloading.
        /// </summary>
        /// <returns>
        /// True if the unloading process should continue and false if it should be canceled.
        /// </returns>
        public override bool OnUnloading()
        {
            // Cancel the unload if creating reports or loading data has not finished.
            if (this.IsCreatingReports || this.IsLoadingEntity)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Called after the view has been loaded into a parent.
        /// </summary>
        public override void OnLoaded()
        {
            base.OnLoaded();

            var dsm = DataAccessFactory.CreateDataSourceManager(this.EntitySourceDb);
            this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(dsm);

            Project project = this.entity as Project;
            if (project == null)
            {
                return;
            }

            if (this.EntitySourceDb == DbIdentifier.NotSet)
            {
                throw new InvalidOperationException("The EntitySourceDb property is mandatory for Project and was not set.");
            }

            // Load the project content in background and populate the Data Source.
            Task.Factory.StartNew(() =>
                {
                    this.IsLoadingEntity = true;

                    // Load the project.                    
                    Project fullProj = dsm.ProjectRepository.GetProjectFull(project.Guid);
                    this.entity = fullProj;

                    if (fullProj == null)
                    {
                        throw new DataAccessException(ErrorCodes.ItemNotFound);
                    }

                    // Create the data source
                    var item = CreateDataSourceItem(fullProj);
                    this.DataSource = new ObservableCollection<BatchReportingDataSourceItem>(item.Children);
                })
            .ContinueWith((task) =>
                {
                    this.IsLoadingEntity = false;
                    if (task.Exception != null)
                    {
                        Exception error = task.Exception.InnerException;
                        log.ErrorException("Project loading failed.", error);
                        this.windowService.MessageDialogService.Show(error);
                    }
                });
        }

        #region Helpers

        /// <summary>
        /// Determine the maximum length of a folder if the longest full path allowed is exceeded.
        /// </summary>
        /// <param name="entityFullFolderPath">The entity full folder path.</param>
        /// <param name="sourceItem">The source item.</param>
        /// <param name="entityId">The entity id.</param>
        private void ResolveFullPathLength(string entityFullFolderPath, BatchReportingDataSourceItem sourceItem, Guid entityId)
        {
            this.longestPaths.Add(entityId, 0);

            // Determine the longest path.
            this.DetermineLongestPath(entityId, sourceItem, string.Empty, 0, 0, 0);

            // Add to the longest paths length the length of the entity folder full path.
            this.longestPaths[entityId] = this.longestPaths[entityId] + entityFullFolderPath.Length + 1;

            // If the longest full path exceeds the allowed length determine the maximum length of a folder.
            if (this.longestPaths[entityId] > this.longestFullPathAllowed)
            {
                int[] itemsLength = Array.ConvertAll<string, int>(this.longestPathFolderLengths.Split('\\'), new Converter<string, int>(Convert.ToInt32));
                this.ShrinkPath(entityId, itemsLength.ToList(), this.longestPaths[entityId]);
                this.isLongestFullPathAllowedExceeded = true;
                if (this.longestPaths[entityId] < this.itemMaximumLength)
                {
                    this.itemMaximumLength = this.longestPaths[entityId];
                }
            }
        }

        /// <summary>
        /// Determine the longest path of the root.
        /// </summary>
        /// <param name="rootGuid">The Guid of the root.</param>
        /// <param name="item">The current item.</param>
        /// <param name="longestPath">The longest path in lengths.</param>
        /// <param name="longestPathLength">The longest path length.</param>
        /// <param name="lastFileNameLength">The name length of the last file.</param>
        /// <param name="currentFileNameLength">The name length of the current file.</param>
        private void DetermineLongestPath(Guid rootGuid, BatchReportingDataSourceItem item, string longestPath, int longestPathLength, int lastFileNameLength, int currentFileNameLength)
        {
            // Get the length of the file name.
            Assembly assy = item.Entity as Assembly;
            if (assy != null)
            {
                currentFileNameLength = LocalizedResources.General_Assembly.Length + 1 + assy.Name.Length + (!string.IsNullOrEmpty(assy.Number) ? 1 + assy.Number.Length : 0) + (assy.Version != null ? 2 + assy.Version.ToString().Length : 0);
                if (this.ReportTypeToCreate == CostReportType.Enhanced)
                {
                    currentFileNameLength += LocalizedResources.Reports_EReport.Length + 1;
                }
            }
            else
            {
                Part part = item.Entity as Part;
                if (part != null)
                {
                    currentFileNameLength = LocalizedResources.General_Part.Length + 1 + part.Name.Length + (!string.IsNullOrEmpty(part.Number) ? 1 + part.Number.Length : 0) + (part.Version != null ? 2 + part.Version.ToString().Length : 0);
                }
            }

            longestPath += item.DisplayName.Length + "\\";
            longestPathLength += item.DisplayName.Length + currentFileNameLength - lastFileNameLength + 1;
            lastFileNameLength = currentFileNameLength;

            if (item.IsChecked == true && longestPathLength > this.longestPaths[rootGuid])
            {
                this.longestPaths[rootGuid] = longestPathLength;
                this.longestPathFolderLengths = longestPath + "0";
            }

            foreach (BatchReportingDataSourceItem child in item.Children)
            {
                this.DetermineLongestPath(rootGuid, child, longestPath, longestPathLength, lastFileNameLength, currentFileNameLength);
            }
        }

        /// <summary>
        /// Determines the maximum length of the folder names that the root contains.
        /// </summary>
        /// <param name="rootGuid">The Guid of the root.</param>
        /// <param name="itemsLength">The length of each folder in the longest path.</param>
        /// <param name="itemMaximumLength">The maximum length of the folder names.</param>
        private void ShrinkPath(Guid rootGuid, List<int> itemsLength, int itemMaximumLength)
        {
            int currentLongestPath = itemsLength.Max();

            for (int i = 0; i < itemsLength.Count; i++)
            {
                if (itemsLength[i] == currentLongestPath)
                {
                    itemsLength[i] = currentLongestPath - 1;
                    itemMaximumLength--;
                }
            }

            if (itemMaximumLength > this.longestFullPathAllowed)
            {
                this.ShrinkPath(rootGuid, itemsLength, itemMaximumLength);
            }
            else
            {
                this.longestPaths[rootGuid] = itemsLength.Max();
            }
        }

        /// <summary>
        /// Returns the input if the folder path does not exist, otherwise it returns the first folder path which exists, input folder + (1), (2), etc.
        /// </summary>
        /// <param name="folderPath">The input folder path</param>
        /// <returns>The valid folder path, based on the input</returns>
        private string GetValidFolderPath(string folderPath)
        {
            string validFolderPath = folderPath;
            int index = 0;
            while (System.IO.Directory.Exists(validFolderPath))
            {
                index++;
                validFolderPath = folderPath + " (" + index + ")";
            }

            return validFolderPath;
        }

        #endregion Helpers
    }
}