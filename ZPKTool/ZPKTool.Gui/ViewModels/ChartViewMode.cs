﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Represents the view modes for a chart.
    /// </summary>
    public enum ChartViewMode
    {
        /// <summary>
        /// The pie view.
        /// </summary>
        PieView = 0,

        /// <summary>
        /// The waterfall view.
        /// </summary>
        WatefallView = 1,
    }
}
