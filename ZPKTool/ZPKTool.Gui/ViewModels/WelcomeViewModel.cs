﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Controls;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the Welcome view. 
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class WelcomeViewModel : ViewModel
    {
        /// <summary>
        /// The composition container.
        /// </summary>
        private CompositionContainer container;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// Flag specifying if the double click or enter commands can execute (is set to false after one execution to prevent multiple executions)
        /// </summary>
        private bool canNavigate = true;

        /// <summary>
        /// Indicates whether to show the start screen.
        /// </summary>
        private bool showStartScreen;

        /// <summary>
        /// Indicates the current Guid of the Folder that is open in Carousel
        /// </summary>
        private Guid curentFolderId = Guid.Empty;

        /// <summary>
        /// Contains the folder stack for the current location in Carousel
        /// It contains the current and the it's parent folder
        /// </summary>
        private Stack<Guid> folderNavigationStack = new Stack<Guid>();

        /// <summary>
        /// The last selected folder id in the carousel
        /// </summary>
        private Guid lastSelectedFolder = Guid.Empty;

        /// <summary>
        /// The currently selected item in the carousel.
        /// </summary>
        private CarouselItem carouselSelectedItem;

        /// <summary>
        /// The current visibility state of Go To Parent Folder button
        /// </summary>
        private Visibility goToParentFolderButtonVisibility;

        /// <summary>
        /// The current visibility state of the label for empty folder
        /// </summary>
        private Visibility emptyFolderLabelVisibility;

        /// <summary>
        /// The maximum number of folders and projects.
        /// The number is limited due to performance issues.
        /// </summary>
        private int maxNumOfFoldersAndProjects = 40;

        /// <summary>
        /// Initializes a new instance of the <see cref="WelcomeViewModel"/> class.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="messenger">The messenger service.</param>
        [ImportingConstructor]
        public WelcomeViewModel(CompositionContainer container, IWindowService windowService, IMessenger messenger)
        {
            this.container = container;
            this.windowService = windowService;
            this.messenger = messenger;

            this.GoToParentFolderCommand = new DelegateCommand(GoToParentFolder, () => folderNavigationStack.Count != 0);
            this.NewProjectCommand = new DelegateCommand(NewProjectAction, () => SecurityManager.Instance.CurrentUserHasRight(Right.ProjectsAndCalculate));
            this.ContinueCommand = new DelegateCommand(ContinueAction);
            this.CarouselKeyUpCommand = new DelegateCommand<KeyEventArgs>(CarouselKeyDownAction, (p) => this.canNavigate);
            this.CarouselDoubleClickCommand = new DelegateCommand<MouseButtonEventArgs>(CarouselDoubleClickAction, (p) => this.canNavigate);

            // set the visibility of the Go To Previous Folder Button from visible to hidden
            this.GoToParentFolderButtonVisibility = Visibility.Hidden;
            this.EmptyFolderLabelVisibility = Visibility.Hidden;

            this.ShowStartScreen = UserSettingsManager.Instance.ShowStartScreen;

            // Reset the calculations for the screen header
            CurrentComponentCostChangedMessage message = new CurrentComponentCostChangedMessage(null);
            this.messenger.Send<CurrentComponentCostChangedMessage>(message, GlobalMessengerTokens.MainViewTargetToken);

            if (SecurityManager.Instance.CurrentUser != null &&
              (SecurityManager.Instance.CurrentUserHasRole(Role.Admin) ||
               SecurityManager.Instance.CurrentUserHasRole(Role.KeyUser) ||
               SecurityManager.Instance.CurrentUserHasRole(Role.User)))
            {
                InitializeCarouselItems();
            }
        }

        #region Commands

        /// <summary>
        /// Gets the New Project command.
        /// </summary>
        public ICommand NewProjectCommand { get; private set; }

        /// <summary>
        /// Gets the Go To Parent Folder command.
        /// </summary>
        public ICommand GoToParentFolderCommand { get; private set; }

        /// <summary>
        /// Gets the Continue command.
        /// </summary>
        public ICommand ContinueCommand { get; private set; }

        /// <summary>
        /// Gets or the CarouselKeyDown command.
        /// </summary>        
        public ICommand CarouselKeyUpCommand { get; private set; }

        /// <summary>
        /// Gets the CarouselDoubleClick command.
        /// </summary>
        public ICommand CarouselDoubleClickCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets or sets the visibility of the Go To Parent Folder in Carousel.
        /// </summary>
        public Visibility GoToParentFolderButtonVisibility
        {
            get { return this.goToParentFolderButtonVisibility; }
            set { this.SetProperty(ref this.goToParentFolderButtonVisibility, value, () => this.GoToParentFolderButtonVisibility); }
        }

        /// <summary>
        /// Gets or sets the visibility of the label for empty folder.
        /// </summary>
        public Visibility EmptyFolderLabelVisibility
        {
            get { return this.emptyFolderLabelVisibility; }
            set { this.SetProperty(ref this.emptyFolderLabelVisibility, value, () => this.EmptyFolderLabelVisibility); }
        }

        /// <summary>
        /// Gets the carousel items.
        /// </summary>
        /// <remarks>Due to a bug in FluidKit you must not set a new value for this collection after its initialization (clear it when necessary and use AddRange).</remarks>
        public ObservableCollection<CarouselItem> CarouselItems { get; private set; }

        /// <summary>
        /// Gets or sets the currently selected item in the carousel.
        /// </summary>        
        public CarouselItem CarouselSelectedItem
        {
            get { return this.carouselSelectedItem; }
            set { this.SetProperty(ref this.carouselSelectedItem, value, () => this.CarouselSelectedItem); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to show the start/welcome screen.
        /// </summary>        
        public bool ShowStartScreen
        {
            get { return this.showStartScreen; }
            set { this.SetProperty(ref this.showStartScreen, value, () => this.ShowStartScreen, this.SaveSettingShowStartScreen); }
        }

        #endregion Properties

        /// <summary>
        /// Loads the carousel content for a specific folder using the folder id.
        /// </summary>
        /// <param name="folderId">The project folder id</param>
        private void LoadCarouselFolderContent(Guid folderId)
        {
            IDataSourceManager changeSet = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            IEnumerable<CarouselItem> projectItems = new List<CarouselItem>();
          
            // Get the folders from the folder that has the same id than folderId 
            Collection<ProjectFolder> myProjectFolders = changeSet.ProjectFolderRepository.GetSubFolders(folderId, SecurityManager.Instance.CurrentUser.Guid);
            var folderItems = myProjectFolders.Select(p => new CarouselItem(p));
            var numberOfFolders = myProjectFolders.Count();

            if (numberOfFolders < maxNumOfFoldersAndProjects)
            {
                // Get the projects from the folder that has the same id than folderId 
                Collection<Project> myProjects = changeSet.ProjectRepository.GetProjectsForWelcomeView(SecurityManager.Instance.CurrentUser.Guid, folderId, maxNumOfFoldersAndProjects - numberOfFolders);
                projectItems = myProjects.Select(p => new CarouselItem(p));
            }

            this.CarouselItems.Clear();
            this.CarouselItems.AddRange(folderItems);
            this.CarouselItems.AddRange(projectItems);

            // Set current folder id reference
            this.curentFolderId = folderId;

            this.GoToParentFolderButtonVisibility = Visibility.Visible;

            // Show a label that informs about an empty folder
            var subFolders = changeSet.ProjectFolderRepository.GetSubFolders(folderId, SecurityManager.Instance.CurrentUser.Guid);
            var projects = changeSet.ProjectRepository.GetProjectsOfFolder(folderId);
            if (subFolders.Count == 0 && projects.Count == 0)
            {
                this.EmptyFolderLabelVisibility = Visibility.Visible;
            }
            else
            {
                this.EmptyFolderLabelVisibility = Visibility.Hidden;
            }
        }

        /// <summary>
        /// Initializes the carousel items.
        /// </summary>
        private void InitializeCarouselItems()
        {
            curentFolderId = Guid.Empty;
            IEnumerable<CarouselItem> projectItems = new List<CarouselItem>();

            // set the visibility of the Go To Previous Folder Button from visible to hidden
            this.GoToParentFolderButtonVisibility = Visibility.Hidden;
            this.EmptyFolderLabelVisibility = Visibility.Hidden;

            IDataSourceManager changeSet = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
         
            // Select the root projects folders
            Collection<ProjectFolder> myProjectFolders = changeSet.ProjectFolderRepository.GetRootFolders(SecurityManager.Instance.CurrentUser.Guid, maxNumOfFoldersAndProjects);
            var folderItems = myProjectFolders.Select(p => new CarouselItem(p));
            var numberOfFolders = myProjectFolders.Count();

            if (numberOfFolders < maxNumOfFoldersAndProjects)
            {                
                // Select the projects that not have a folder
                Collection<Project> myProjects = changeSet.ProjectRepository.GetProjectsForWelcomeView(SecurityManager.Instance.CurrentUser.Guid, Guid.Empty, maxNumOfFoldersAndProjects - numberOfFolders);
                projectItems = myProjects.Select(p => new CarouselItem(p));              
            }

            if (this.CarouselItems == null)
            {
                this.CarouselItems = new ObservableCollection<CarouselItem>();
            }
            else
            {
                this.CarouselItems.Clear();
            }

            this.CarouselItems.AddRange(folderItems);
            this.CarouselItems.AddRange(projectItems);
        }

        /// <summary>
        /// The action performed by the Go To Parent Folder command.
        /// </summary>
        private void GoToParentFolder()
        {
            lastSelectedFolder = folderNavigationStack.Pop();
            Guid parentIdFolder = folderNavigationStack.Pop();

            if (parentIdFolder == Guid.Empty)
            {
                this.InitializeCarouselItems();
            }
            else
            {
                this.LoadCarouselFolderContent(parentIdFolder);
            }

            var foundItem = CarouselItems.FirstOrDefault(it => it.DataObject is ProjectFolder && ((ProjectFolder)it.DataObject).Guid == lastSelectedFolder);
            if (foundItem != null)
            {
                this.CarouselSelectedItem = foundItem;
            }
        }

        /// <summary>
        /// The action performed by the New Project command.
        /// </summary>
        private void NewProjectAction()
        {
            NavigateToMainView(true);

            ProjectViewModel viewModel = container.GetExportedValue<ProjectViewModel>();
            viewModel.DataSourceManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            viewModel.InitializeForProjectCreation(null);
            this.windowService.ShowViewInDialog(viewModel, "CreateProjectView");
        }

        /// <summary>
        /// The action performed by the Continue command.
        /// </summary>
        private void ContinueAction()
        {
            NavigateToMainView();
        }

        /// <summary>
        /// The action performed by the CarouselKeyDown command.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.Input.KeyEventArgs"/> instance containing the event data.</param>
        private void CarouselKeyDownAction(KeyEventArgs e)
        {
            if (e.Key == Key.Return || e.Key == Key.Enter)
            {
                var carouselElementFlow = e.Source as FluidKit.Controls.ElementFlow;
                if (carouselElementFlow != null)
                {
                    CarouselItem selItem = carouselElementFlow.SelectedItem as CarouselItem;
                    this.HandleCarouselItemSelection(selItem);
                }
            }

            if (e.Key == Key.Back && folderNavigationStack.Count != 0)
            {
                this.GoToParentFolder();
            }
        }

        /// <summary>
        /// The action performed by the CarouselDoubleClick command.
        /// </summary>    
        /// <param name="e">mouse event</param>
        private void CarouselDoubleClickAction(MouseButtonEventArgs e)
        {
            var originalSourceOnClick = e.OriginalSource as ModelUIElement3D;

            if (e.LeftButton == MouseButtonState.Pressed
                && originalSourceOnClick != null)
            {
                var carouselElementFlow = e.Source as FluidKit.Controls.ElementFlow;
                if (carouselElementFlow != null)
                {
                    ContainerUIElement3D modelContainer = carouselElementFlow.ModelContainer;
                    int selectedIndex = modelContainer.Children.IndexOf(originalSourceOnClick);

                    if (selectedIndex >= carouselElementFlow.ContainerCount || selectedIndex < 0)
                    {
                        selectedIndex = 0;
                    }

                    var selItem = carouselElementFlow.Items.GetItemAt(selectedIndex);
                    this.HandleCarouselItemSelection(selItem as CarouselItem);
                }
            }
        }

        /// <summary>
        /// Handle carousel item selection
        /// </summary>
        /// <param name="item">the carousel selection</param>
        private void HandleCarouselItemSelection(CarouselItem item)
        {
            if (item == null)
            {
                return;
            }

            Project project = item != null ? item.DataObject as Project : null;
            if (project != null)
            {
                this.canNavigate = false;
                this.NavigateToMainView(true);

                NavigateToEntityMessage message = new NavigateToEntityMessage(project, DbIdentifier.LocalDatabase);
                this.messenger.Send(message, IsInViewerMode ? GlobalMessengerTokens.ModelBrowserTargetToken : GlobalMessengerTokens.MainViewTargetToken);
            }
            else
            {
                ProjectFolder projectFolder = item != null ? item.DataObject as ProjectFolder : null;
                if (projectFolder != null)
                {
                    folderNavigationStack.Push(curentFolderId);
                    folderNavigationStack.Push(projectFolder.Guid);

                    // load project folder content
                    this.LoadCarouselFolderContent(projectFolder.Guid);
                }
            }
        }

        /// <summary>
        /// Navigates to main view.
        /// </summary>
        /// <param name="forceSelectProjectsTreeItem">if set to <c>true</c> [force select projects tree item].</param>
        private void NavigateToMainView(bool forceSelectProjectsTreeItem = false)
        {
            var mainViewModel = this.container.GetExportedValue<MainViewModel>();
            this.container.GetExportedValue<ShellViewModel>().Content = mainViewModel;

            if (forceSelectProjectsTreeItem)
            {
                var selectMsg = new SelectExplorerTreeRequestMessage(ExplorerTree.Projects);
                this.messenger.Send(selectMsg);
            }
        }

        /// <summary>
        /// Saves the setting show start screen.
        /// </summary>
        private void SaveSettingShowStartScreen()
        {
            UserSettingsManager.Instance.ShowStartScreen = this.ShowStartScreen;
            UserSettingsManager.Instance.Save();
        }

        #region Inner classes

        /// <summary>
        /// A carousel item definition
        /// </summary>
        public class CarouselItem
        {
            /// <summary>
            /// The image pixel width used for decoding images used on carousel
            /// </summary>
            private const int ImageDecodePixelWidth = 400;

            /// <summary>
            /// Initializes a new instance of the <see cref="CarouselItem"/> class
            /// </summary>
            /// <param name="project">the project item</param>
            public CarouselItem(Project project)
            {
                if (project == null)
                {
                    throw new ArgumentNullException("project", "The project can not be null.");
                }

                this.DataObject = project;
                this.Name = project.Name;

                Media media = project.Media.FirstOrDefault();
                if (media != null && media.Content != null)
                {
                    this.Image = MediaUtils.CreateImageForDisplay(media.Content, ImageDecodePixelWidth);
                }
                else
                {
                    this.Image = null;
                }
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="CarouselItem"/> class
            /// </summary>
            /// <param name="projectFolder">project folder</param>
            public CarouselItem(ProjectFolder projectFolder)
            {
                if (projectFolder == null)
                {
                    throw new ArgumentNullException("projectFolder", "The project Folder can not be null.");
                }

                this.DataObject = projectFolder;
                this.Name = projectFolder.Name;
                this.Image = Images.FolderIcon;
            }

            /// <summary>
            /// Gets or sets the dataObject of an carousel item
            /// </summary>
            public object DataObject { get; set; }

            /// <summary>
            /// Gets or sets the name of an carousel item
            /// </summary>
            public string Name { get; set; }

            /// <summary>
            /// Gets or sets the image of an carousel item
            /// </summary>
            public ImageSource Image { get; set; }
        }

        #endregion Inner classes
    }
}
