﻿using System;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Windows.Input;
using System.Windows.Media;
using ZPKTool.Common;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;
using ZPKTool.Updater;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the Update Application view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class UpdateApplicationViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The composition container.
        /// </summary>
        private CompositionContainer container;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// Enables the Download and install button
        /// </summary>
        private bool enableDownload = false;

        /// <summary>
        /// Enables the Check button
        /// </summary>
        private bool enableCheck = true;

        /// <summary>
        /// disables the check for update button if the download starts
        /// </summary>
        private bool downloadStarted = false;

        /// <summary>
        /// holds the result of the check for update operation
        /// </summary>
        private bool checkResult = false;

        /// <summary>
        /// progress percentage
        /// </summary>
        private double downloadProgress;

        /// <summary>
        /// string representing the message about the current version displayed on the screen
        /// </summary>
        private string currentVersionMessage;

        /// <summary>
        /// string representing the release notes
        /// </summary>
        private string releaseNotesMessage;

        /// <summary>
        /// string representing the message about the update version displayed on the screen
        /// </summary>
        private string statusMessage;

        /// <summary>
        /// solid color brush representing the color of the status text displayed on the screen (red for error, black for info)
        /// </summary>
        private SolidColorBrush statusMessageForeground;

        /// <summary>
        /// reference to the update manager from the shell controller
        /// </summary>
        private UpdateManager updateManager;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateApplicationViewModel"/> class.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="windowService">The window service.</param>
        [ImportingConstructor]
        public UpdateApplicationViewModel(CompositionContainer container, IWindowService windowService)
        {
            this.container = container;
            this.windowService = windowService;

            this.updateManager = this.container.GetExportedValue<ShellController>().UpdateManager;

            this.CheckForUpdate = new DelegateCommand(CheckForUpdateAction);
            this.DownloadAndUpdate = new DelegateCommand(DownloadAndUpdateAction);
            this.CloseScreen = new DelegateCommand(CloseScreenAction);

            string version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            this.CurrentVersionMessage = LocalizedResources.Update_CurrentVersion + " " + version;

            IMessenger messenger = container.GetExportedValue<IMessenger>();
            messenger.Send<NotificationMessage>(new NotificationMessage(Notification.HideUpdateAvailable));

            this.updateManager.CheckCanceled = false;
            this.CheckForUpdateAction();
        }

        #region Commands

        /// <summary>
        /// Gets the check for update command
        /// </summary>
        /// <value>The check for update.</value>
        public ICommand CheckForUpdate { get; private set; }

        /// <summary>
        /// Gets the download and update command
        /// </summary>
        /// <value>The download and update.</value>
        public ICommand DownloadAndUpdate { get; private set; }

        /// <summary>
        /// Gets the close screen command
        /// </summary>
        /// <value>The close screen.</value>
        public ICommand CloseScreen { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets or sets the current version message.
        /// </summary>
        /// <value>The current version message.</value>
        public string CurrentVersionMessage
        {
            get
            {
                return this.currentVersionMessage;
            }

            set
            {
                if (this.currentVersionMessage != value)
                {
                    this.currentVersionMessage = value;
                    OnPropertyChanged("CurrentVersionMessage");
                }
            }
        }

        /// <summary>
        /// Gets or sets the update message.
        /// </summary>
        /// <value>The update message.</value>
        public string StatusMessage
        {
            get
            {
                return this.statusMessage;
            }

            set
            {
                if (this.statusMessage != value)
                {
                    this.statusMessage = value;
                    OnPropertyChanged("StatusMessage");
                }
            }
        }

        /// <summary>
        /// Gets or sets the release notes message.
        /// </summary>
        /// <value>The release notes message.</value>
        public string ReleaseNotesMessage
        {
            get
            {
                return this.releaseNotesMessage;
            }

            set
            {
                if (this.releaseNotesMessage != value)
                {
                    this.releaseNotesMessage = value;
                    OnPropertyChanged("ReleaseNotesMessage");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether download is enabled
        /// </summary>
        public bool EnableDownload
        {
            get
            {
                return this.enableDownload;
            }

            set
            {
                if (this.enableDownload != value)
                {
                    this.enableDownload = value;
                    OnPropertyChanged("EnableDownload");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the check is enabled
        /// </summary>
        public bool EnableCheck
        {
            get
            {
                return this.enableCheck;
            }

            set
            {
                if (this.enableCheck != value)
                {
                    this.enableCheck = value;
                    OnPropertyChanged("EnableCheck");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether download started
        /// </summary>
        public bool DownloadStarted
        {
            get
            {
                return this.downloadStarted;
            }

            set
            {
                if (this.downloadStarted != value)
                {
                    this.downloadStarted = value;
                    OnPropertyChanged("DownloadStarted");
                }
            }
        }

        /// <summary>
        /// Gets or sets the download progress.
        /// </summary>
        public double DownloadProgress
        {
            get
            {
                return this.downloadProgress;
            }

            set
            {
                if (this.downloadProgress != value)
                {
                    this.downloadProgress = value;
                    OnPropertyChanged("DownloadProgress");
                }
            }
        }

        /// <summary>
        /// Gets or sets the color of the status message displayed on the screen
        /// </summary>
        public SolidColorBrush StatusMessageForeground
        {
            get
            {
                return this.statusMessageForeground;
            }

            set
            {
                if (this.statusMessageForeground != value)
                {
                    this.statusMessageForeground = value;
                    OnPropertyChanged("StatusMessageForeground");
                }
            }
        }

        #endregion Properties

        #region Command Actions

        /// <summary>
        /// Command handler for the check for update button
        /// </summary>
        private void CheckForUpdateAction()
        {
            this.EnableDownload = false;
            this.EnableCheck = false;
            this.StatusMessageForeground = new SolidColorBrush(Colors.Black);
            this.StatusMessage = LocalizedResources.Update_CheckingForUpdates;
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork +=
                delegate
                {
                    this.checkResult = updateManager.CheckForUpdate();
                };

            worker.RunWorkerAsync();
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(CheckCompleted);
        }

        /// <summary>
        /// Downloads the update and starts the setup process.
        /// </summary>
        private void DownloadAndUpdateAction()
        {
            this.EnableDownload = false;
            this.EnableCheck = false;
            this.DownloadStarted = true;
            PreDownloadCheck();
        }

        /// <summary>
        /// Command handler for closing the screen
        /// </summary>
        private void CloseScreenAction()
        {
            this.windowService.CloseViewWindow(this);
        }

        #endregion

        /// <summary>
        /// Populates the screen with update info, if the there was a check performed at some point after startup
        /// </summary>
        private void PopulateScreenWithUpdateInfo()
        {
            UpdateInfo versionInfo = updateManager.UpdateInfo;
            this.StatusMessageForeground = new SolidColorBrush(Colors.Black);
            this.StatusMessage = LocalizedResources.Update_FoundNewVersion + " " + versionInfo.Version;
            string releaseNotesMessage = string.Empty;

            if (versionInfo.Features.Count > 0)
            {
                releaseNotesMessage += LocalizedResources.Update_FeaturesAdded;
                foreach (Feature feature in versionInfo.Features)
                {
                    releaseNotesMessage += Environment.NewLine + " - " + feature.Title;
                }

                releaseNotesMessage += Environment.NewLine + Environment.NewLine;
            }

            if (versionInfo.Fixes.Count > 0)
            {
                releaseNotesMessage += LocalizedResources.Update_BugsFixed;
                foreach (Fix fix in versionInfo.Fixes)
                {
                    releaseNotesMessage += Environment.NewLine + " - " + fix.Title;
                }
            }

            this.ReleaseNotesMessage = releaseNotesMessage;
        }

        /// <summary>
        /// Handler for check operation completed
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.ComponentModel.RunWorkerCompletedEventArgs"/> instance containing the event data.</param>
        private void CheckCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.EnableCheck = true;

            if (e.Error != null)
            {
                Exception ex = e.Error as Exception;
                this.StatusMessage = string.Empty;
                this.ReleaseNotesMessage = string.Empty;
                this.EnableDownload = false;
                this.StatusMessageForeground = new SolidColorBrush(Colors.Red);

                if (ex is UpdateFileWrongFormatException)
                {
                    this.StatusMessage = LocalizedResources.Error_WrongXmlFormat;
                }
                else if (ex is UpdateServerOfflineException)
                {
                    this.StatusMessage = LocalizedResources.Error_ServerOffline;
                }
                else
                {
                    this.StatusMessage = LocalizedResources.Error_UpdateCheckFailed;
                }

                return;
            }

            if (this.checkResult)
            {
                this.EnableDownload = true;
                this.EnableCheck = true;
                this.PopulateScreenWithUpdateInfo();
            }
            else
            {
                this.StatusMessageForeground = new SolidColorBrush(Colors.Black);
                this.StatusMessage = LocalizedResources.Update_VersionUpToDate;
                this.ReleaseNotesMessage = string.Empty;
            }
        }

        /// <summary>
        /// Checks the server again, downloading the xml file and updating the download information before downloading the installer
        /// </summary>
        private void PreDownloadCheck()
        {
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork +=
                delegate
                {
                    this.checkResult = updateManager.CheckForUpdate();
                };

            worker.RunWorkerAsync();
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(PreDownloadCheckCompleted);
        }

        /// <summary>
        /// Handler for the pre download check operation completed
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.ComponentModel.RunWorkerCompletedEventArgs"/> instance containing the event data.</param>
        private void PreDownloadCheckCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Exception ex = e.Error as Exception;
                this.StatusMessage = string.Empty;
                this.ReleaseNotesMessage = string.Empty;
                this.DownloadStarted = false;
                this.EnableCheck = true;

                if (ex is UpdateFileWrongFormatException)
                {
                    this.windowService.MessageDialogService.Show(LocalizedResources.Error_WrongXmlFormat, MessageDialogType.Error);
                }
                else if (ex is UpdateServerOfflineException)
                {
                    this.windowService.MessageDialogService.Show(LocalizedResources.Error_ServerOffline, MessageDialogType.Error);
                }
                else
                {
                    this.windowService.MessageDialogService.Show(LocalizedResources.Error_UpdateFailed, MessageDialogType.Error);
                }

                return;
            }

            if (this.checkResult)
            {
                this.PopulateScreenWithUpdateInfo();

                if (GlobalSettingsManager.Instance.StartInViewerMode
                    && !GlobalSettingsManager.Instance.PCMViewerIsInstalled)
                {
                    this.CloseScreenAction();
                    this.updateManager.Cleanup();
                    System.Diagnostics.Process.Start("http://productcostmanager.com/");
                }
                else
                {
                    this.Download();
                }
            }
            else
            {
                this.StatusMessageForeground = new SolidColorBrush(Colors.Black);
                this.StatusMessage = LocalizedResources.Update_VersionUpToDate;
                this.ReleaseNotesMessage = string.Empty;
                this.DownloadStarted = false;
                this.EnableCheck = true;
            }
        }

        /// <summary>
        /// Command for the download button
        /// </summary>
        private void Download()
        {
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += (s, e) =>
                {
                    this.updateManager.DownloadProgress += this.DownloadProgressUpdate;
                    this.updateManager.Download();
                };

            worker.RunWorkerAsync();
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(DownloadCompleted);
        }

        /// <summary>
        /// Event handler for progress changed event
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="DownloadProgressEventArgs"/> instance containing the event data.</param>
        private void DownloadProgressUpdate(object sender, DownloadProgressEventArgs e)
        {
            this.DownloadProgress = e.ProgressPercentage;
        }

        /// <summary>
        /// Event handler for download completed event
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The instance containing the event data.</param>
        private void DownloadCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                log.ErrorException("Download stopped.", e.Error);
                this.windowService.MessageDialogService.Show(LocalizedResources.Error_UpdateFailed, MessageDialogType.Error);

                this.DownloadStarted = false;
                this.EnableDownload = true;
                this.EnableCheck = true;
                return;
            }

            if (!updateManager.DownloadCanceled)
            {
                CloseScreenAction();
                MessageDialogResult result = this.windowService.MessageDialogService.Show(
                    GlobalSettingsManager.Instance.StartInViewerMode ? LocalizedResources.UpdateViewer_InstallConfirmation : LocalizedResources.Update_InstallConfirmation, 
                    MessageDialogType.YesNo);
                if (result == MessageDialogResult.Yes)
                {
                    updateManager.StartSetup();
                    var messenger = this.container.GetExportedValue<IMessenger>();
                    messenger.Send(new NotificationMessage(Notifications.Notification.ShutdownApplication));
                }
                else
                {
                    updateManager.Cleanup();
                }
            }
        }

        /// <summary>
        /// Called after the view has been unloaded from the parent.
        /// </summary>
        public override void OnUnloaded()
        {
            base.OnUnloaded();

            this.updateManager.DownloadCanceled = true;
            this.updateManager.CheckCanceled = true;
            this.updateManager.DownloadProgress -= this.DownloadProgressUpdate;
        }
    }
}