﻿namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The master data operation.
    /// </summary>
    public enum MasterDataOperation
    {
        /// <summary>
        /// Import into master data.
        /// </summary>
        Import,

        /// <summary>
        /// Exports master data.
        /// </summary>
        Export
    }
}