﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using ZPKTool.Common;
using ZPKTool.Controls;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Views;
using ZPKTool.LicenseValidator;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for the Register view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class RegisterViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The composition container.
        /// </summary>
        private CompositionContainer container;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The file dialog service.
        /// </summary>
        private IFileDialogService fileDialogService;

        /// <summary>
        /// The license serial number.
        /// </summary>
        private string serialNumber;

        /// <summary>
        /// The license file path.
        /// </summary>
        private string filePath;

        /// <summary>
        /// The company information.
        /// </summary>
        private string company;

        /// <summary>
        /// The client information.
        /// </summary>
        private string user;

        /// <summary>
        /// The license type.
        /// </summary>
        private string licenseType;

        /// <summary>
        /// The license level.
        /// </summary>
        private string licenseLevel;

        /// <summary>
        /// The license level.
        /// </summary>
        private string applicationVersion;

        /// <summary>
        /// The license start and ending date.
        /// </summary>
        private string startEndDate;

        /// <summary>
        /// True if the license is expired, false otherwise.
        /// </summary>
        private bool licenseExpired;

        #endregion

        #region Contructor

        /// <summary>
        /// Initializes a new instance of the <see cref="RegisterViewModel"/> class.
        /// </summary>
        /// <param name="container">The composition container.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="fileDialogService">The file dialog service.</param>
        [ImportingConstructor]
        public RegisterViewModel(CompositionContainer container, IWindowService windowService, IFileDialogService fileDialogService)
        {
            this.container = container;
            this.windowService = windowService;
            this.fileDialogService = fileDialogService;
            this.SelectPathCommand = new DelegateCommand(SelectPath);
            this.ActivateLicenseCommand = new DelegateCommand(ActivateLicense);
            this.CopyToClipboardCommand = new DelegateCommand(CopyToClipboard);
            this.RequestLicenseByEmailCommand = new DelegateCommand(RequestLicenseByEmail);
            this.LicenseExpired = false;

            try
            {
                this.SerialNumber = LicenseInfo.FormatSerialNumber(SerialNumberManager.GenerateSerialNumber());

                System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
                object[] attrs = assembly.GetCustomAttributes(false);
                Version version = assembly.GetName().Version;

                LicenseValidation validator = new LicenseValidation(ZPKTool.Business.SecurityManager.LicensePublicKey, ZPKTool.Business.SecurityManager.LicenseFilePath);
                LicenseInfo licenseInfo = validator.ValidateLicense(version);

                this.Company = licenseInfo.CompanyName;
                this.User = licenseInfo.UserName;
                this.LicenseType = Enum.GetName(typeof(LicenseType), licenseInfo.Type);
                this.LicenseLevel = this.GetLanguageResourceLicenseLevel(licenseInfo.Level);
                this.StartEndDate = licenseInfo.StartingDate.ToShortDateString() + "-" + licenseInfo.ExpirationDate.ToShortDateString();
                this.ApplicationVersion = licenseInfo.ApplicationVersion;
                this.LicenseExpired = false;
            }
            catch (ZPKTool.LicenseValidator.LicenseException ex)
            {
                if (ex.ErrorCode == LicenseErrorCodes.ExpiredLicense)
                {
                    this.LicenseExpired = true;
                }
                else
                {
                    ClearLicenseInformationsView();
                }
            }
        }

        #endregion

        #region Commands

        /// <summary>
        /// Gets the select path command.
        /// </summary>
        public ICommand SelectPathCommand { get; private set; }

        /// <summary>
        /// Gets the validate license command.
        /// </summary>
        public ICommand ActivateLicenseCommand { get; private set; }

        /// <summary>
        /// Gets the copy to clipboard command.
        /// </summary>
        public ICommand CopyToClipboardCommand { get; private set; }

        /// <summary>
        /// Gets the request license by email command.
        /// </summary>
        /// <value>The request license by email command.</value>
        public ICommand RequestLicenseByEmailCommand { get; private set; }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether [license expired].
        /// </summary>
        /// <value><c>true</c> if [license expired]; otherwise, <c>false</c>.</value>
        public bool LicenseExpired
        {
            get
            {
                return this.licenseExpired;
            }

            set
            {
                if (this.licenseExpired != value)
                {
                    this.licenseExpired = value;
                    OnPropertyChanged("LicenseExpired");
                }
            }
        }

        /// <summary>
        /// Gets or sets the file path.
        /// </summary>
        /// <value>The file path.</value>
        public string FilePath
        {
            get
            {
                return this.filePath;
            }

            set
            {
                if (this.filePath != value)
                {
                    this.filePath = value;
                    OnPropertyChanged("FilePath");
                }
            }
        }

        /// <summary>
        /// Gets or sets the serial number.
        /// </summary>
        /// <value>The serial number.</value>
        public string SerialNumber
        {
            get
            {
                return this.serialNumber;
            }

            set
            {
                if (this.serialNumber != value)
                {
                    this.serialNumber = value;
                    OnPropertyChanged("SerialNumber");
                }
            }
        }

        /// <summary>
        /// Gets or sets the company.
        /// </summary>
        /// <value>The company.</value>
        public string Company
        {
            get
            {
                return this.company;
            }

            set
            {
                if (this.company != value)
                {
                    this.company = value;
                    OnPropertyChanged("Company");
                }
            }
        }

        /// <summary>
        /// Gets or sets the client.
        /// </summary>
        /// <value>The client.</value>
        public string User
        {
            get
            {
                return this.user;
            }

            set
            {
                if (this.user != value)
                {
                    this.user = value;
                    OnPropertyChanged("User");
                }
            }
        }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>The license type.</value>
        public string LicenseType
        {
            get
            {
                return this.licenseType;
            }

            set
            {
                if (this.licenseType != value)
                {
                    this.licenseType = value;
                    OnPropertyChanged("LicenseType");
                }
            }
        }

        /// <summary>
        /// Gets or sets the license level.
        /// </summary>
        /// <value>The license level.</value>
        public string LicenseLevel
        {
            get
            {
                return this.licenseLevel;
            }

            set
            {
                if (this.licenseLevel != value)
                {
                    this.licenseLevel = value;
                    OnPropertyChanged("LicenseLevel");
                }
            }
        }

        /// <summary>
        /// Gets or sets the start end date.
        /// </summary>
        /// <value>The start end date.</value>
        public string StartEndDate
        {
            get
            {
                return this.startEndDate;
            }

            set
            {
                if (this.startEndDate != value)
                {
                    this.startEndDate = value;
                    OnPropertyChanged("StartEndDate");
                }
            }
        }

        /// <summary>
        /// Gets or sets the application version.
        /// </summary>
        /// <value>The application version.</value>
        public string ApplicationVersion
        {
            get
            {
                return this.applicationVersion;
            }

            set
            {
                if (this.applicationVersion != value)
                {
                    this.applicationVersion = value;
                    OnPropertyChanged("ApplicationVersion");
                }
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Selects the file path.
        /// </summary>
        private void SelectPath()
        {
            // Configure open file dialog box        
            fileDialogService.Filter = LocalizedResources.LicenseFilesFileDialogFilter + " (*.lic)|*.lic";
            fileDialogService.Multiselect = false;

            // Show open file dialog box
            if (fileDialogService.ShowOpenFileDialog() == true)
            {
                try
                {
                    FileInfo fi = new FileInfo(fileDialogService.FileName);
                    if (!fi.Exists)
                    {
                        this.windowService.MessageDialogService.Show(LocalizedResources.Error_FileNotExists, MessageDialogType.Error);
                    }
                }
                catch (SecurityException)
                {
                    this.windowService.MessageDialogService.Show(LocalizedResources.Error_FilePermissionDenied, MessageDialogType.Error);
                }
                catch (UnauthorizedAccessException)
                {
                    this.windowService.MessageDialogService.Show(LocalizedResources.Error_FilePermissionDenied, MessageDialogType.Error);
                }
                catch (ArgumentException)
                {
                    this.windowService.MessageDialogService.Show(LocalizedResources.Error_InvalidFileName, MessageDialogType.Error);
                }
                catch (NotSupportedException)
                {
                    this.windowService.MessageDialogService.Show(LocalizedResources.Error_InvalidFileName, MessageDialogType.Error);
                }
                catch (PathTooLongException)
                {
                    this.windowService.MessageDialogService.Show(LocalizedResources.Error_FilePathTooLong, MessageDialogType.Error);
                }
                catch (Exception)
                {
                    this.windowService.MessageDialogService.Show(LocalizedResources.Error_Internal, MessageDialogType.Error);
                }

                this.FilePath = fileDialogService.FileName;
            }
        }

        /// <summary>
        /// Validates the license file.
        /// </summary>
        private void ActivateLicense()
        {
            try
            {
                if (File.Exists(this.FilePath))
                {
                    System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
                    object[] attrs = assembly.GetCustomAttributes(false);
                    Version version = assembly.GetName().Version;

                    LicenseValidation.RefreshCurrentDateTime();
                    LicenseValidation validator = new LicenseValidation(ZPKTool.Business.SecurityManager.LicensePublicKey, this.FilePath);
                    LicenseInfo licenseInfo = validator.ValidateLicense(version);

                    this.Company = licenseInfo.CompanyName;
                    this.User = licenseInfo.UserName;
                    this.LicenseType = Enum.GetName(typeof(LicenseType), licenseInfo.Type);
                    this.LicenseLevel = GetLanguageResourceLicenseLevel(licenseInfo.Level);
                    this.StartEndDate = licenseInfo.StartingDate.ToShortDateString() + "-" + licenseInfo.ExpirationDate.ToShortDateString();
                    this.ApplicationVersion = licenseInfo.ApplicationVersion;
                    this.LicenseExpired = false;

                    File.Copy(this.FilePath, ZPKTool.Business.SecurityManager.LicenseFilePath, true);

                    // Reset the last date in which a warning for license expiration was displayed.
                    UserSettingsManager.Instance.LastLicenseExpirationWarningDate = new DateTime(2012, 1, 1);
                    UserSettingsManager.Instance.Save();

                    this.windowService.MessageDialogService.Show(LocalizedResources.License_ActivatedSuccessfully, MessageDialogType.Info);
                    this.windowService.CloseViewWindow(this);
                }
                else
                {
                    this.windowService.MessageDialogService.Show(LocalizedResources.Error_InvalidFileName, MessageDialogType.Error);
                }
            }
            catch (ZPKTool.LicenseValidator.LicenseException ex)
            {
                this.windowService.MessageDialogService.Show(ex);
            }
            catch (SecurityException)
            {
                this.windowService.MessageDialogService.Show(LocalizedResources.Error_FilePermissionDenied, MessageDialogType.Error);
            }
            catch (UnauthorizedAccessException)
            {
                this.windowService.MessageDialogService.Show(LocalizedResources.Error_FilePermissionDenied, MessageDialogType.Error);
            }
            catch (NotSupportedException)
            {
                this.windowService.MessageDialogService.Show(LocalizedResources.Error_InvalidFileName, MessageDialogType.Error);
            }
            catch (PathTooLongException)
            {
                this.windowService.MessageDialogService.Show(LocalizedResources.Error_FilePathTooLong, MessageDialogType.Error);
            }
            catch (IOException)
            {
                this.windowService.MessageDialogService.Show(LocalizedResources.Error_FileCopyFailed, MessageDialogType.Error);
            }
        }

        /// <summary>
        /// Gets the corresponding string from language resource.
        /// </summary>
        /// <param name="licenseLevel">The license level.</param>
        /// <returns>A string representing the localized name of the license level.</returns>
        private string GetLanguageResourceLicenseLevel(LicenseLevel licenseLevel)
        {
            var name = licenseLevel.GetType();
            return LocalizedResources.ResourceManager.GetString(name.Name + "_" + licenseLevel);
        }

        /// <summary>
        /// Clears the view.
        /// </summary>
        private void ClearLicenseInformationsView()
        {
            this.Company = string.Empty;
            this.User = string.Empty;
            this.LicenseType = string.Empty;
            this.LicenseLevel = string.Empty;
            this.StartEndDate = string.Empty;
            this.ApplicationVersion = string.Empty;
        }

        /// <summary>
        /// Copies to clipboard.
        /// </summary>
        private void CopyToClipboard()
        {
            if (string.IsNullOrEmpty(this.SerialNumber))
            {
                return;
            }

            int attempts = 0;
            while (attempts < 5)
            {
                try
                {
                    Clipboard.SetText(this.SerialNumber);
                    break;
                }
                catch (COMException)
                {
                    Thread.Sleep(10);
                    attempts++;
                }
                catch
                {
                    break;
                }
            }
        }

        /// <summary>
        /// Closes this instance.
        /// </summary>
        protected override void Close()
        {
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Requests the license by email.
        /// </summary>
        private void RequestLicenseByEmail()
        {
            LicenseRequestViewModel requestLicenseViewModel = new LicenseRequestViewModel(this.windowService, this.SerialNumber);
            this.windowService.ShowViewInDialog(requestLicenseViewModel);
        }

        /// <summary>
        /// Called when the register view has been unloaded.
        /// </summary>
        public override void OnUnloaded()
        {
            // If the license is expired close the application if already opened.
            if (this.LicenseExpired && Application.Current != null)
            {
                Application.Current.Shutdown();
            }

            base.OnUnloaded();
        }

        #endregion
    }
}
