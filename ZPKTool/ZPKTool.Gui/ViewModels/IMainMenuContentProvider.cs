﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Provides the way to add content to the main menu.
    /// </summary>
    public interface IMainMenuContentProvider
    {
        /// <summary>
        /// Gets the items to be displayed in the main menu along with the default ones.        
        /// </summary>
        /// <returns>The list of menu items to appear in the main menu.</returns>
        Collection<SystemMenuItem> GetMainMenuItems();
    }
}
