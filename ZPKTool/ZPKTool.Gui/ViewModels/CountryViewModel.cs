﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Resources;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the CountryView.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class CountryViewModel : ViewModel<Country, IDataSourceManager>
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// A list containing the area items.
        /// </summary>
        private Collection<MeasurementUnit> areas;

        /// <summary>
        /// A list containing the lengths items.
        /// </summary>
        private Collection<MeasurementUnit> lengths;

        /// <summary>
        /// A list containing the times items.
        /// </summary>
        private Collection<MeasurementUnit> times;

        /// <summary>
        /// A list containing the volumes items.
        /// </summary>
        private Collection<MeasurementUnit> volumes;

        /// <summary>
        /// A list containing the weights items.
        /// </summary>
        private Collection<MeasurementUnit> weights;

        /// <summary>
        /// A list containing the currency items.
        /// </summary>
        private Collection<Currency> currencies;

        /// <summary>
        /// The country settings view model
        /// </summary>
        private CountrySettingsViewModel countrySettingsViewModel;

        #endregion Attributes

        /// <summary>
        /// Initializes a new instance of the <see cref="CountryViewModel" /> class.
        /// </summary>
        /// <param name="countrySettingsViewModel">The country settings view model.</param>
        /// <param name="windowService">The window service.</param>
        [ImportingConstructor]
        public CountryViewModel(
            CountrySettingsViewModel countrySettingsViewModel,
            IWindowService windowService)
        {
            Argument.IsNotNull("countrySettingsViewModel", countrySettingsViewModel);
            Argument.IsNotNull("windowService", windowService);

            this.CountrySettingsViewModel = countrySettingsViewModel;
            this.windowService = windowService;
            this.ShowCancelMessageFlag = true;

            this.InitializeUndoManager();
        }

        #region Properties

        /// <summary>
        /// Gets the name of the country.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Name", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Name")]
        public DataProperty<string> Name { get; private set; }

        /// <summary>
        /// Gets the floor (area) measurement unit.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Area", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty]
        [ExposesModelProperty("FloorMeasurementUnit")]
        public DataProperty<MeasurementUnit> FloorMeasurementUnit { get; private set; }

        /// <summary>
        /// Gets or sets the areas list.
        /// </summary>
        public Collection<MeasurementUnit> Areas
        {
            get { return this.areas; }
            set { this.SetProperty(ref this.areas, value, () => this.Areas); }
        }

        /// <summary>
        /// Gets the length measurement unit.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Length", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty]
        [ExposesModelProperty("LengthMeasurementUnit")]
        public DataProperty<MeasurementUnit> LengthMeasurementUnit { get; private set; }

        /// <summary>
        /// Gets or sets the lengths` list.
        /// </summary>
        public Collection<MeasurementUnit> Lengths
        {
            get { return this.lengths; }
            set { this.SetProperty(ref this.lengths, value, () => this.Lengths); }
        }

        /// <summary>
        /// Gets the time measurement unit.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Time", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty]
        [ExposesModelProperty("TimeMeasurementUnit")]
        public DataProperty<MeasurementUnit> TimeMeasurementUnit { get; private set; }

        /// <summary>
        /// Gets or sets the times` list.
        /// </summary>
        public Collection<MeasurementUnit> Times
        {
            get { return this.times; }
            set { this.SetProperty(ref this.times, value, () => this.Times); }
        }

        /// <summary>
        /// Gets the volume measurement unit.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Volume", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty]
        [ExposesModelProperty("VolumeMeasurementUnit")]
        public DataProperty<MeasurementUnit> VolumeMeasurementUnit { get; private set; }

        /// <summary>
        /// Gets or sets the volumes` list.
        /// </summary>
        public Collection<MeasurementUnit> Volumes
        {
            get { return this.volumes; }
            set { this.SetProperty(ref this.volumes, value, () => this.Volumes); }
        }

        /// <summary>
        /// Gets the weight measurement unit.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Weight", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty]
        [ExposesModelProperty("WeightMeasurementUnit")]
        public DataProperty<MeasurementUnit> WeightMeasurementUnit { get; private set; }

        /// <summary>
        /// Gets or sets the weights` list.
        /// </summary>
        public Collection<MeasurementUnit> Weights
        {
            get { return this.weights; }
            set { this.SetProperty(ref this.weights, value, () => this.Weights); }
        }

        /// <summary>
        /// Gets the currency.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Currency", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty]
        [ExposesModelProperty("Currency")]
        public DataProperty<Currency> Currency { get; private set; }

        /// <summary>
        /// Gets or sets the weights` list.
        /// </summary>
        public Collection<Currency> Currencies
        {
            get { return this.currencies; }
            set { this.SetProperty(ref this.currencies, value, () => this.Currencies); }
        }

        /// <summary>
        /// Gets the CountrySettingsViewModel
        /// </summary>
        public CountrySettingsViewModel CountrySettingsViewModel
        {
            get { return this.countrySettingsViewModel; }
            private set { this.SetProperty(ref this.countrySettingsViewModel, value, () => this.CountrySettingsViewModel); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the show cancel message will appear
        /// </summary>
        public bool ShowCancelMessageFlag { get; set; }

        #endregion Properties

        /// <summary>
        /// Initializes this instance for creating a new Country.
        /// </summary>
        public void InitializeForCreation()
        {
            var newCountry = new Country();
            newCountry.CountrySetting = new CountrySetting();

            this.EditMode = ViewModelEditMode.Create;
            this.Model = newCountry;
        }

        /// <summary>
        /// Initialize the undo manager.
        /// </summary>
        private void InitializeUndoManager()
        {
            this.CountrySettingsViewModel.UndoManager = this.UndoManager;
        }

        /// <summary>
        /// Called when the Model has changed.
        /// </summary>
        protected override void OnModelChanged()
        {
            this.CheckDataSource();
            this.LoadDataFromModel();

            this.CountrySettingsViewModel.SourceCountryName = this.Model.Name;
            this.CountrySettingsViewModel.DataSourceManager = this.DataSourceManager;

            if (this.Model.CountrySetting == null)
            {
                if (!this.IsReadOnly)
                {
                    throw new InvalidOperationException("The Country's Country Settings can not be null.");
                }
            }
            else
            {
                this.CountrySettingsViewModel.Model = this.Model.CountrySetting;
            }

            this.CountrySettingsViewModel.CheckForUpdate = false;

            this.IsChanged = false;
            this.UndoManager.Start();
            this.UndoManager.Reset();
        }

        /// <summary>
        /// Called when DataSourceManager has changed.
        /// </summary>
        protected override void OnDataSourceManagerChanged()
        {
            base.OnDataSourceManagerChanged();
            this.InitializeMeasurementUnits();
        }

        /// <summary>
        /// Initialize MeasurementUnits items.
        /// </summary>
        private void InitializeMeasurementUnits()
        {
            Collection<MeasurementUnit> mu = this.DataSourceManager.MeasurementUnitRepository.GetAll();

            Areas = new Collection<MeasurementUnit>();
            var areasList = mu.Where(u => u.Type == MeasurementUnitType.Area);
            Areas.AddRange(areasList);

            Lengths = new Collection<MeasurementUnit>();
            var lengthsList = mu.Where(u => u.Type == MeasurementUnitType.Length);
            Lengths.AddRange(lengthsList);

            Times = new Collection<MeasurementUnit>();
            var timesList = mu.Where(u => u.Type == MeasurementUnitType.Time);
            Times.AddRange(timesList);

            Volumes = new Collection<MeasurementUnit>();
            var volumesList = mu.Where(u => u.Type == MeasurementUnitType.Volume);
            Volumes.AddRange(volumesList);

            Weights = new Collection<MeasurementUnit>();
            var weightsList = mu.Where(u => u.Type == MeasurementUnitType.Weight);
            Weights.AddRange(weightsList);

            Currencies = new Collection<Currency>();
            Collection<Currency> currencies = this.DataSourceManager.CurrencyRepository.GetBaseCurrencies();
            Currencies.AddRange(currencies.OrderBy(n => n.Name));
        }

        #region Save/Cancel

        /// <summary>
        /// Determines whether this instance can perform the save operation. Executed by the SaveCommand.
        /// The default implementation allows the save to be performed if the input is valid.
        /// </summary>
        /// <returns>
        /// true if the save operation can be performed, false otherwise.
        /// </returns>
        protected override bool CanSave()
        {
            return base.CanSave() && this.CountrySettingsViewModel.SaveToModelCommand.CanExecute(null);
        }

        /// <summary>
        /// Performs the save operation. Executed by the SaveCommand.
        /// </summary>
        protected override void Save()
        {
            this.CheckModelAndDataSource();

            // When the fields are modified, but the name is the same, the changes can be made.
            bool countryExists = false;
            if (this.EditMode == ViewModelEditMode.Create || this.Name.IsChanged)
            {
                countryExists = this.DataSourceManager.CountryRepository.CheckIfExists(this.Name.Value);
            }

            if (countryExists)
            {
                this.windowService.MessageDialogService.Show(LocalizedResources.Country_Exists, MessageDialogType.Error);
            }
            else
            {
                this.SaveToModel();
                this.CountrySettingsViewModel.SetLastChangeTimestamp();
                this.CountrySettingsViewModel.SaveToModelCommand.Execute(null);
                if (this.SavesToDataSource)
                {
                    this.DataSourceManager.CountryRepository.Save(this.Model);
                    this.DataSourceManager.SaveChanges();
                }

                this.CountrySettingsViewModel.SetLastUpdateTime();
                this.windowService.CloseViewWindow(this);
            }
        }

        /// <summary>
        /// Determines whether the Cancel operation can be performed. Executed by the CancelCommand.
        /// </summary>
        /// <returns>
        /// true if the changes can be canceled, false otherwise.
        /// </returns>
        protected override bool CanCancel()
        {
            return base.CanCancel() && this.CountrySettingsViewModel.CancelCommand.CanExecute(null);
        }

        /// <summary>
        /// Cancels all changes. Executed by the CancelCommand.
        /// </summary>
        protected override void Cancel()
        {
            if (!this.CanCancel())
            {
                return;
            }

            if (this.IsChanged)
            {
                if (this.ShowCancelMessageFlag)
                {
                    var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_CancelChanges, MessageDialogType.YesNo);
                    if (result != MessageDialogResult.Yes)
                    {
                        // Don't cancel the changes and also don't close the view-model.
                        return;
                    }
                }

                using (this.UndoManager.StartBatch())
                {
                    // Cancel all changes.
                    base.Cancel();
                    this.CountrySettingsViewModel.CancelCommand.Execute(null);
                }
            }

            // Close the view-model when is displayed in a window.
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Called before unloading the view from its parent. Returning false will cancel the view's unloading.
        /// </summary>
        /// <returns>
        /// <c>True</c> if the unload was successful; <c>False</c> otherwise.
        /// </returns>
        public override bool OnUnloading()
        {
            // Do nothing if the view-model is read-only, in viewer mode or it was not changed.
            if (this.IsReadOnly || this.IsInViewerMode || !this.IsChanged)
            {
                return true;
            }

            if (this.EditMode == ViewModelEditMode.Create)
            {
                // Ask the user to confirm quitting
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_Quit, MessageDialogType.YesNo);
                if (result != MessageDialogResult.Yes)
                {
                    // The user chose to stay on the screen; return false to stop the screen unloading.
                    return false;
                }
                else
                {
                    this.IsChanged = false;
                }
            }
            else if (this.EditMode == ViewModelEditMode.Edit)
            {
                // Ask the user if he wants to save
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_UnsavedDataOnQuit, MessageDialogType.YesNoCancel);
                if (result == MessageDialogResult.Yes)
                {
                    // The user whishes to save but it is not possible because the input is not valid. Return false to stop the view-model from unloading.
                    if (!this.CanSave())
                    {
                        return false;
                    }

                    this.Save();
                }
                else if (result == MessageDialogResult.No)
                {
                    // The user does not want to save.                    
                    this.IsChanged = false;
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        #endregion Save/Cancel
    }
}
