﻿using System;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Windows.Input;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for playing back and managing the video of an entity.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class VideoViewModel : MediaHandlingViewModelBase
    {
        #region Attributes

        /// <summary>
        /// The current playback source for the video player.
        /// </summary>
        private string videoPlayerSource;

        /// <summary>
        /// A value indicating whether the media is currently playing.
        /// </summary>
        private bool? isPlayingMedia;

        /// <summary>
        /// A value indicating whether this instance is loading media.
        /// </summary>
        private bool isLoadingMedia;

        #endregion Attributes

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="VideoViewModel" /> class.
        /// </summary>
        /// <param name="messenger">The messenger.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="fileDialogService">The file dialog service.</param>
        [ImportingConstructor]
        public VideoViewModel(IMessenger messenger, IWindowService windowService, IFileDialogService fileDialogService) :
            base(messenger, windowService, fileDialogService)
        {
            this.InitializeOwnCommands();
        }

        #endregion Constructor

        #region Commands

        /// <summary>
        /// Gets the request video source command, which is invoked by the video player when it starts playback and its source is not set.
        /// </summary>
        public ICommand RequestVideoSourceCommand { get; private set; }

        /// <summary>
        /// Gets the command executed when the video player's action area is clicked.
        /// </summary>
        public ICommand VideoPlayerClickedCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets or sets the current playback source for the video player.
        /// </summary>
        public string VideoPlayerSource
        {
            get { return this.videoPlayerSource; }
            set { this.SetProperty(ref this.videoPlayerSource, value, () => this.VideoPlayerSource); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the media is currently playing.
        /// </summary>        
        public bool? IsPlayingMedia
        {
            get
            {
                return this.isPlayingMedia;
            }

            set
            {
                // No equality check here because this property sometimes is out of sync with the video player's IsPlaying property.
                this.isPlayingMedia = value;
                this.OnPropertyChanged(() => this.IsPlayingMedia);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is loading media.
        /// </summary>
        public bool IsLoadingMedia
        {
            get { return this.isLoadingMedia; }
            set { this.SetProperty(ref this.isLoadingMedia, value, () => this.IsLoadingMedia); }
        }

        #endregion Properties

        #region Initialization

        /// <summary>
        /// Initializes the commands.
        /// </summary>
        private void InitializeOwnCommands()
        {
            this.RequestVideoSourceCommand = new DelegateCommand(this.ExecuteVideoPlayerSourceRequestCommand);
            this.VideoPlayerClickedCommand = new DelegateCommand(this.ExecuteVideoPlayerClickedCommand, this.CanExecuteVideoPlayerClickedCommand);
        }

        #endregion Initialization

        #region Execute Commands

        /// <summary>
        /// Determines whether this instance [can execute video player clicked command].
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance [can execute video player clicked command]; otherwise, <c>false</c>.
        /// </returns>
        private bool CanExecuteVideoPlayerClickedCommand()
        {
            return !this.IsLoadingMedia; // todo: && !isreadonly, !isinviewermode
        }

        /// <summary>
        /// Executes the add media command.
        /// </summary>
        public void ExecuteVideoPlayerClickedCommand()
        {
            this.Messenger.Send(new NotificationMessage(Notification.AddMedia));
        }

        /// <summary>
        /// Executes the delete current media command.
        /// </summary>
        protected override void ExecuteDeleteCurrentMediaCommand()
        {
            var result = this.WindowService.MessageDialogService.Show(LocalizedResources.Question_DeletePicture, MessageDialogType.YesNo);
            if (result == MessageDialogResult.Yes)
            {
                var undoableItem = new MediaUndoableItem()
                {
                    AllSources = this.MediaSource.AllSources.ToList(),
                    Media = this.MediaSource.Media.ToList(),
                    MediaMetaData = this.MediaSource.MediaMetaData.ToList(),
                    MediaPaths = this.MediaSource.MediaPaths.ToList(),
                    MediaToBeAdded = this.MediaToBeAdded.ToList(),
                    MediaToBeDeleted = this.MediaToBeDeleted.ToList()
                };

                base.ExecuteDeleteCurrentMediaCommand();

                this.MarkAllMetaDataAsDeleted();
                this.MediaToBeAdded.Clear();
                this.MediaSource.Reset();
                this.RequestVideoSourceCommand.Execute(null);

                this.Messenger.Send(new NotificationMessage(undoableItem, Notification.MediaDeleted));
            }
        }

        #endregion Execute Commands

        #region Load media

        /// <summary>
        /// Executes the video player source request command.
        /// </summary>
        private void ExecuteVideoPlayerSourceRequestCommand()
        {
            // The media is loaded into the player only when attempting the playback.
            // Load the media from cache or database, if the source is media meta data.
            if (this.MediaSource.MediaMetaData.Count > 0)
            {
                // First check if the video is in the cache.
                var mediaMeta = this.MediaSource.MediaMetaData[0];
                var cachedMediaPath = this.GetMediaCachePath(mediaMeta);

                if (File.Exists(cachedMediaPath))
                {
                    this.VideoPlayerSource = cachedMediaPath;
                }
                else
                {
                    System.Threading.Tasks.Task.Factory.StartNew(() =>
                    {
                        this.IsLoadingMedia = true;
                        var mediaPath = this.LoadMediaFromDb(mediaMeta);

                        if (File.Exists(mediaPath))
                        {
                            this.VideoPlayerSource = mediaPath;
                        }
                    })
                    .ContinueWith(task =>
                    {
                        this.IsLoadingMedia = false;

                        if (task.Exception != null)
                        {
                            this.WindowService.MessageDialogService.Show(task.Exception.InnerException);
                        }
                    });
                }
            }
            else if (this.MediaSource.MediaPaths.Count > 0)
            {
                var mediaPath = this.MediaSource.MediaPaths[0];
                if (File.Exists(mediaPath))
                {
                    this.VideoPlayerSource = mediaPath;
                }
            }
            else if (this.MediaSource.Media.Count > 0)
            {
                var video = MediaSource.Media.FirstOrDefault(media => (MediaType)media.Type == MediaType.Video);
                if (video != null)
                {
                    var mediaCachePath = this.GetMediaCachePath(new MediaMetaData { Guid = video.Guid, OriginalFileName = video.OriginalFileName });
                    this.WriteMediaContentToDisk(mediaCachePath, video);
                    this.VideoPlayerSource = mediaCachePath;
                }
            }
            else
            {
                this.IsPlayingMedia = false;
            }
        }

        /// <summary>
        /// Loads the specified media from the database.
        /// </summary>
        /// <param name="mediaMeta">The media meta.</param>
        /// <returns>The path to the cache of the loaded media.</returns>
        private string LoadMediaFromDb(MediaMetaData mediaMeta)
        {
            Argument.IsNotNull("DataContext", this.DataContext);

            if (mediaMeta == null)
            {
                return null;
            }

            // Load the media from db and cache it on the disk for playback.
            var media = this.DataContext.MediaRepository.GetMedia(mediaMeta, true);
            this.CreateCacheFolder();
            var mediaCachePath = this.GetMediaCachePath(mediaMeta);
            this.WriteMediaContentToDisk(mediaCachePath, media);

            return mediaCachePath;
        }

        /// <summary>
        /// Writes the video to disk.
        /// </summary>
        /// <param name="filePath">The media cache path.</param>
        /// <param name="media">The media.</param>
        private void WriteMediaContentToDisk(string filePath, Media media)
        {
            if (media != null && media.Content != null)
            {
                try
                {
                    using (FileStream destFile = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None, 8192))
                    {
                        destFile.Write(media.Content, 0, media.Content.Length);
                    }
                }
                catch (Exception ex)
                {
                    Log.ErrorException("Failed to write file.", ex);
                    this.WindowService.MessageDialogService.Show(LocalizedResources.Error_FileWriteFailed, MessageDialogType.Error);
                }
            }
        }

        /// <summary>
        /// Return the complete path to a cached media, based of the media's meta data.
        /// </summary>
        /// <param name="media">Meta data information for the media.</param>
        /// <returns>The path to the cached video.</returns>
        private string GetMediaCachePath(MediaMetaData media)
        {
            return string.Format("{0}\\{1}{2}", Constants.VideoCacheFolderPath, media.Guid, media.OriginalFileName);
        }

        /// <summary>
        /// Makes sure that the folder where media is cached exists.
        /// </summary>
        private void CreateCacheFolder()
        {
            if (!Directory.Exists(Constants.VideoCacheFolderPath))
            {
                try
                {
                    Directory.CreateDirectory(Constants.VideoCacheFolderPath);
                }
                catch (Exception ex)
                {
                    Log.ErrorException(string.Format("Failed to create video cache folder: {0}", Constants.VideoCacheFolderPath), ex);
                    throw new UIException(ErrorCodes.FileWriteFail);
                }
            }
        }

        #endregion Load media

        #region Other Methods

        /// <summary>
        /// Obtain the binary representation of the media, having the source path as a parameter
        /// Works only for media under 2GB.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <returns>
        /// An array containing the media or null if an error occurred.
        /// </returns>
        /// <exception cref="ZPKException">Thrown when it fails to read the media file from the disk.</exception>
        public override byte[] GetMediaBytes(string filePath)
        {
            return MediaUtils.GetFileBytes(filePath);
        }

        /// <summary>
        /// Gets the content of the current media.
        /// </summary>
        /// <returns>The content of the current media.</returns>
        protected override byte[] GetCurrentMediaContent()
        {
            byte[] result = new byte[0];

            if (this.MediaSource.Media.Count > 0)
            {
                result = this.MediaSource.Media[0].Content;
            }
            else if (this.MediaSource.MediaMetaData.Count > 0)
            {
                var cachedPath = this.GetMediaCachePath(this.MediaSource.MediaMetaData[0]);

                if (File.Exists(cachedPath))
                {
                    result = File.ReadAllBytes(cachedPath);
                }
                else
                {
                    var mediaPath = this.LoadMediaFromDb(this.MediaSource.MediaMetaData[0]);

                    if (File.Exists(mediaPath))
                    {
                        result = File.ReadAllBytes(mediaPath);
                    }
                }
            }
            else if (this.MediaSource.MediaPaths.Count > 0)
            {
                if (File.Exists(this.MediaSource.MediaPaths[0]))
                {
                    result = File.ReadAllBytes(this.MediaSource.MediaPaths[0]);
                }
            }

            return result;
        }

        /// <summary>
        /// Gets the name of the current media file.
        /// </summary>
        /// <returns>The name of the current media file.</returns>
        protected override string GetCurrentMediaFileName()
        {
            var result = string.Empty;

            if (this.MediaSource.Media.Count > 0)
            {
                result = Path.GetFileName(this.MediaSource.Media[0].OriginalFileName);
            }
            else if (this.MediaSource.MediaMetaData.Count > 0)
            {
                result = Path.GetFileName(this.MediaSource.MediaMetaData[0].OriginalFileName);
            }
            else if (this.MediaSource.MediaPaths.Count > 0)
            {
                result = Path.GetFileName(this.MediaSource.MediaPaths[0]);
            }

            return result;
        }

        #endregion Other Methods
    }
}