﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for displaying and managing the pictures of an entity.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class PicturesViewModel : MediaHandlingViewModelBase
    {
        #region Attributes

        /// <summary>
        /// The default name of an image that have no name
        /// </summary>
        private const string DefaultPictureName = "Untitled.jpg";

        /// <summary>
        /// The default picture format for pictures from office editors
        /// </summary>
        private const string DefaultOfficePictureFormat = "PNG";

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The picture source.
        /// </summary>
        private ImageSource pictureSource;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The created images are saved in this dictionary, and thus will not be created when they will be displayed again.
        /// </summary>
        private Dictionary<object, ImageSource> cachedImages = new Dictionary<object, ImageSource>();

        #endregion Attributes

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PicturesViewModel" /> class.
        /// </summary>
        /// <param name="messenger">The messenger.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="fileDialogService">The file dialog service.</param>
        [ImportingConstructor]
        public PicturesViewModel(IMessenger messenger, IWindowService windowService, IFileDialogService fileDialogService) :
            base(messenger, windowService, fileDialogService)
        {
            this.windowService = windowService;
        }

        #endregion Constructor

        #region Properties

        /// <summary>
        /// Gets or sets the picture source.
        /// </summary>
        public ImageSource PictureSource
        {
            get { return this.pictureSource; }
            set { this.SetProperty(ref this.pictureSource, value, () => this.PictureSource); }
        }

        /// <summary>
        /// Gets the current source.
        /// </summary>
        public object CurrentSource
        {
            get
            {
                return this.MediaSource.AllSources.ElementAtOrDefault(this.CurrentMediaIndex);
            }
        }

        #endregion Properties

        #region Execute Commands

        /// <summary>
        /// Determines whether this instance [can execute next media command].
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance [can execute next media command]; otherwise, <c>false</c>.
        /// </returns>
        protected override bool CanExecuteNextMediaCommand()
        {
            return !IsPreviewModeOn && CurrentMediaIndex < MediaSource.AllSources.Count - 1;
        }

        /// <summary>
        /// Determines whether this instance [can execute preview media command].
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance [can execute preview media command]; otherwise, <c>false</c>.
        /// </returns>
        protected override bool CanExecutePreviewMediaCommand()
        {
            return !IsPreviewModeOn;
        }

        /// <summary>
        /// Determines whether this instance [can execute previous media command].
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance [can execute previous media command]; otherwise, <c>false</c>.
        /// </returns>
        protected override bool CanExecutePreviousMediaCommand()
        {
            return !IsPreviewModeOn && CurrentMediaIndex > 0;
        }

        /// <summary>
        /// Executes the next media command.
        /// </summary>
        protected override void ExecuteNextMediaCommand()
        {
            var previousMediapath = this.MediaSource.AllSources.ElementAt(CurrentMediaIndex + 1) as string;
            if (previousMediapath != null && !File.Exists(previousMediapath))
            {
                windowService.MessageDialogService.Show(LocalizedResources.Media_GetMediaErrorMessage, MessageDialogType.Error);
                return;
            }

            DisplayMediaAtIndex(CurrentMediaIndex + 1);
        }

        /// <summary>
        /// Executes the preview media command.
        /// </summary>
        protected override void ExecutePreviewMediaCommand()
        {
            this.TogglePreviewMode(true);
            PictureSource = null;
        }

        /// <summary>
        /// Executes the previous media command.
        /// </summary>
        protected override void ExecutePreviousMediaCommand()
        {
            var previousMediapath = this.MediaSource.AllSources.ElementAt(CurrentMediaIndex - 1) as string;
            if (previousMediapath != null && !File.Exists(previousMediapath))
            {
                windowService.MessageDialogService.Show(LocalizedResources.Media_GetMediaErrorMessage, MessageDialogType.Error);
                return;
            }

            DisplayMediaAtIndex(CurrentMediaIndex - 1);
        }

        #endregion Execute Commands

        #region Display/Get Media

        /// <summary>
        /// Displays the media at the given index
        /// </summary>
        /// <param name="mediaIndex">Index of the media.</param>
        public override void DisplayMediaAtIndex(int mediaIndex)
        {
            this.TogglePreviewMode(false);
            var source = this.MediaSource.AllSources.ElementAtOrDefault(mediaIndex);
            if (source == null)
            {
                return;
            }

            var result = source as ImageSource;
            if (this.cachedImages.ContainsKey(source))
            {
                result = this.cachedImages[source];
            }

            if (result == null)
            {
                var media = source as Media;
                if (media != null)
                {
                    result = MediaUtils.CreateImageForDisplay(media.Content);
                    if (!this.cachedImages.ContainsKey(source))
                    {
                        cachedImages.Add(source, result);
                    }
                }
                else
                {
                    var filePath = source as string;
                    if (filePath != null)
                    {
                        result = MediaUtils.CreateImageForDisplay(filePath);
                        if (!this.cachedImages.ContainsKey(source))
                        {
                            cachedImages.Add(source, result);
                        }
                    }
                }
            }

            CurrentMediaIndex = mediaIndex;
            PictureSource = result;
        }

        /// <summary>
        /// Get all Media ImageSources loaded.
        /// </summary>
        /// <returns>The media image sources.</returns>
        public Collection<ImageSource> GetAllImageSources()
        {
            var imgSources = new Collection<ImageSource>();
            foreach (var source in this.MediaSource.AllSources)
            {
                var imgSource = source as ImageSource;
                if (this.cachedImages.ContainsKey(source))
                {
                    imgSource = this.cachedImages[source];
                }

                if (imgSource == null)
                {
                    var imagePath = source as string;
                    if (imagePath != null)
                    {
                        imgSource = MediaUtils.CreateImageForDisplay(imagePath);
                        if (!this.cachedImages.ContainsKey(source))
                        {
                            cachedImages.Add(source, imgSource);
                        }
                    }
                    else
                    {
                        var mediaSource = source as Media;
                        if (mediaSource != null)
                        {
                            imgSource = MediaUtils.CreateImageForDisplay(mediaSource.Content);
                            if (!this.cachedImages.ContainsKey(source))
                            {
                                cachedImages.Add(source, imgSource);
                            }
                        }
                    }
                }

                if (imgSource != null)
                {
                    imgSources.Add(imgSource);
                }
            }

            return imgSources;
        }

        /// <summary>
        /// Toggles the preview mode.
        /// </summary>
        /// <param name="setPreviewModeOn">Indicates whether to set the preview mode on or off.</param>
        public override void TogglePreviewMode(bool setPreviewModeOn)
        {
            if (this.IsPreviewModeOn && setPreviewModeOn)
            {
                OnRefreshPreviewMode();
            }

            this.IsPreviewModeOn = setPreviewModeOn;
        }

        #endregion Display Media

        #region Other Methods

        /// <summary>
        /// Validate view-model media sources before attempting to display Media data on screen.
        /// </summary>
        public override void ValidateMediaToDisplay()
        {
            var unreachablePaths = 0;
            if (this.MediaSource.MediaPaths != null)
            {
                unreachablePaths = this.MediaSource.MediaPaths.Count(path => !File.Exists(path));
                var itemsToRemove = this.MediaSource.MediaPaths.Where(path => !File.Exists(path)).ToList();

                foreach (var itemToRemove in itemsToRemove)
                {
                    this.MediaSource.MediaPaths.Remove(itemToRemove);
                    this.MediaToBeAdded.Remove(itemToRemove);
                }
            }

            if (unreachablePaths > 0)
            {
                windowService.MessageDialogService.Show(
                    this.MediaSource.AllSources.Count > 0
                        ? LocalizedResources.Media_GetImagesErrorMessage
                        : LocalizedResources.Media_GetMediaErrorMessage,
                    MessageDialogType.Error);
            }
        }

        /// <summary>
        /// Obtain the binary representation of the media, having the source path as a parameter
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <returns>
        /// An array containing the media or null if an error occurred.
        /// </returns>
        /// <exception cref="ZPKException">Thrown when it fails to read the media file from the disk.</exception>
        public override byte[] GetMediaBytes(string filePath)
        {
            if (!string.IsNullOrEmpty(filePath))
            {
                var imageData = MediaUtils.GetFileBytes(filePath);
                if (imageData != null)
                {
                    return MediaUtils.GetMediaJPEGImageBytes(imageData);
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the name of the current media file.
        /// </summary>
        /// <returns>The name of the current media file.</returns>
        protected override string GetCurrentMediaFileName()
        {
            string sourcePath = null;
            if (this.MediaSource.AllSources[this.CurrentMediaIndex] is string)
            {
                sourcePath = this.MediaSource.AllSources[this.CurrentMediaIndex] as string;
            }

            if (!string.IsNullOrWhiteSpace(sourcePath))
            {
                return Path.GetFileName(sourcePath);
            }

            var sourceMedia = this.MediaSource.AllSources[this.CurrentMediaIndex] as Media;
            if (sourceMedia != null)
            {
                return Path.GetFileName(sourceMedia.OriginalFileName);
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the content of the current media.
        /// </summary>
        /// <returns>The content of the current media.</returns>
        protected override byte[] GetCurrentMediaContent()
        {
            string sourcePath = null;
            if (this.MediaSource.AllSources[this.CurrentMediaIndex] is string)
            {
                sourcePath = this.MediaSource.AllSources[this.CurrentMediaIndex] as string;
            }

            if (!string.IsNullOrWhiteSpace(sourcePath))
            {
                var imageData = MediaUtils.GetFileBytes(sourcePath);
                if (imageData != null)
                {
                    return MediaUtils.GetMediaJPEGImageBytes(imageData);
                }
            }

            var sourceMedia = this.MediaSource.AllSources[this.CurrentMediaIndex] as Media;
            if (sourceMedia != null)
            {
                return sourceMedia.Content;
            }

            return null;
        }

        /// <summary>
        /// Executes the delete current media command.
        /// </summary>
        protected override void ExecuteDeleteCurrentMediaCommand()
        {
            if (IsPreviewModeOn)
            {
                var result = this.WindowService.MessageDialogService.Show(LocalizedResources.Question_DeleteAllPictures, MessageDialogType.YesNo);
                if (result != MessageDialogResult.Yes)
                {
                    return;
                }

                var undoableItem = new MediaUndoableItem()
                {
                    AllSources = this.MediaSource.AllSources.ToList(),
                    Media = this.MediaSource.Media.ToList(),
                    MediaMetaData = this.MediaSource.MediaMetaData.ToList(),
                    MediaPaths = this.MediaSource.MediaPaths.ToList(),
                    MediaToBeAdded = this.MediaToBeAdded.ToList(),
                    MediaToBeDeleted = this.MediaToBeDeleted.ToList()
                };

                base.ExecuteDeleteCurrentMediaCommand();
                this.MediaToBeAdded.Clear();

                foreach (var media in MediaSource.Media)
                {
                    if (this.MediaSource.IsMediaFromMetaData(media, this.MediaSource.MediaMetaData))
                    {
                        this.MediaToBeDeleted.Add(this.MediaSource.MediaMetaData.First(p => p.Guid == media.Guid));
                    }
                    else
                    {
                        this.MediaToBeDeleted.Add(media);
                    }
                }

                this.MediaSource.Reset();
                this.OnRefreshPreviewMode();
                this.Messenger.Send(new NotificationMessage(undoableItem, Notification.MediaDeleted));
            }
            else
            {
                var result = this.WindowService.MessageDialogService.Show(LocalizedResources.Question_DeletePicture, MessageDialogType.YesNo);
                if (result != MessageDialogResult.Yes)
                {
                    return;
                }

                var undoableItem = new MediaUndoableItem()
                {
                    AllSources = this.MediaSource.AllSources.ToList(),
                    Media = this.MediaSource.Media.ToList(),
                    MediaMetaData = this.MediaSource.MediaMetaData.ToList(),
                    MediaPaths = this.MediaSource.MediaPaths.ToList(),
                    MediaToBeAdded = this.MediaToBeAdded.ToList(),
                    MediaToBeDeleted = this.MediaToBeDeleted.ToList()
                };

                base.ExecuteDeleteCurrentMediaCommand();

                this.cachedImages.Remove(CurrentSource);
                var filePath = CurrentSource as string;
                if (filePath != null)
                {
                    this.MediaToBeAdded.Remove(filePath);
                    this.MediaSource.MediaPaths.Remove(filePath);
                }
                else
                {
                    var media = CurrentSource as Media;
                    if (media != null)
                    {
                        this.MediaToBeAdded.Remove(media);
                        if (this.MediaSource.IsMediaFromMetaData(media, this.MediaSource.MediaMetaData))
                        {
                            this.MediaToBeDeleted.Add(this.MediaSource.MediaMetaData.First(p => p.Guid == media.Guid));
                        }
                        else
                        {
                            this.MediaToBeDeleted.Add(media);
                        }

                        this.MediaSource.Media.Remove(media);
                    }
                    else
                    {
                        throw new NotImplementedException("CurrentSource is not string nor media");
                    }
                }

                this.Messenger.Send(new NotificationMessage(undoableItem, Notification.MediaDeleted));
            }
        }

        /// <summary>
        /// Gets the pictures of the entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        ///  <param name="dataManager">The data source manager.</param>
        /// <returns>
        /// A list of Media objects containing images.
        /// </returns>
        public Collection<Media> GetPictures(object entity, IDataSourceManager dataManager)
        {
            var result = new Collection<Media>();
            if (IsInViewerMode)
            {
                return result;
            }

            var mediaManager = new MediaManager(dataManager);
            result.AddRange(mediaManager.GetPictures(entity));
            return result;
        }

        /// <summary>
        /// Gets the media from image byte array representation.
        /// </summary>
        /// <param name="source">The image byte array representation.</param>
        /// <returns>The created media object based on the byte array image representation passed as parameter.</returns>
        public Media GetMediaFromByteArray(byte[] source)
        {
            if (source != null)
            {
                // Get the byte stream of the image with the maximum width of MediaUtils.MaxImageWidth
                var resizedStream = MediaUtils.GetMediaJPEGImageBytes(source);
                if (resizedStream != null)
                {
                    Media mediaObject = new Media();
                    mediaObject.Type = (short)MediaType.Image;
                    mediaObject.Content = resizedStream;
                    mediaObject.Size = mediaObject.Content != null ? mediaObject.Content.Length : (int?)null;
                    mediaObject.OriginalFileName = DefaultPictureName;

                    return mediaObject;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the media representing a picture from from the Clipboard.
        /// </summary>
        /// <returns>The media object containing the picture</returns>
        public Media GetMediaFromClipboard()
        {
            byte[] array = null;

            // Check if the clipboard contains an image - for office versions after 2007
            BitmapSource img = null;
            try
            {
                img = Clipboard.GetImage();
                if (img != null)
                {
                    array = MediaUtils.EncodeImageAsJPEG(img, 0);
                }
                else
                {
                    // Check if the clipboard contains an PNG format image, a regular image format for office 2003 version
                    var stream = Clipboard.GetDataObject().GetData(DefaultOfficePictureFormat) as MemoryStream;
                    if (stream != null)
                    {
                        array = stream.ToArray();
                    }
                }

                if (array != null)
                {
                    return GetMediaFromByteArray(array);
                }
            }
            catch (OutOfMemoryException ex)
            {
                log.ErrorException("File size too large.", ex);
                this.windowService.MessageDialogService.Show(LocalizedResources.Media_FileSizeError, MessageDialogType.Error);
            }

            return null;
        }

        /// <summary>
        /// Gets the media from <see cref="IDataObject"/> object.
        /// </summary>
        /// <param name="obj">The data object containing the picture</param>
        /// <returns>The media object containing the picture</returns>
        public Media GetMediaFromDataObject(IDataObject obj)
        {
            byte[] array = null;
            var imgSource = obj.GetData(typeof(BitmapSource)) as BitmapSource;
            if (imgSource != null)
            {
                array = MediaUtils.EncodeImageAsJPEG(imgSource, 0);
                if (array != null)
                {
                    return GetMediaFromByteArray(array);
                }
            }

            return null;
        }

        /// <summary>
        /// Checks if the clipboard contains an image
        /// </summary>
        /// <param name="numberOfMaxAllowedPictures">The maximum allowed pictures</param>
        /// <returns>True if the clipboard contains a picture or false otherwise</returns>
        public bool CanPaste(int numberOfMaxAllowedPictures)
        {
            var clipboardObject = Clipboard.GetDataObject();
            if ((clipboardObject != null && clipboardObject.GetDataPresent(DefaultOfficePictureFormat)) || Clipboard.ContainsImage())
            {
                return this.CanAddImage(numberOfMaxAllowedPictures, false);
            }

            return false;
        }

        /// <summary>
        /// Checks if the <see cref="IDataObject"/> object parameter contains an image
        /// </summary>
        /// <param name="obj">The data object to be checked</param>
        /// <returns>True if the <see cref="IDataObject"/> object contains a picture or false otherwise</returns>
        public bool IsContainingImage(IDataObject obj)
        {
            var imageSource = obj.GetData(typeof(BitmapSource)) as BitmapSource;
            if (imageSource != null)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Checks if an picture can be added to view model
        /// It depends on the maximum pictures number allowed to be added
        /// It can show a message error or just return a Boolean value depending on it parameter value
        /// </summary>
        /// <param name="numberOfMaxAllowedPictures">The maximum number of pictures allowed</param>
        /// <param name="showMessage">A flag indicating whether to show an error or not</param>
        /// <returns>True if a picture can be added or false otherwise</returns>
        public bool CanAddImage(int numberOfMaxAllowedPictures, bool showMessage)
        {
            if (this.MediaSource.AllSources.Count + 1 > numberOfMaxAllowedPictures)
            {
                if (showMessage)
                {
                    this.WindowService.MessageDialogService.Show(string.Format(Thread.CurrentThread.CurrentUICulture, LocalizedResources.Media_ToManyPictures, numberOfMaxAllowedPictures), MessageDialogType.Info);
                }

                return false;
            }
            else
            {
                return true;
            }
        }

        #endregion Other Methods
    }
}