﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Windows.Input;
using ZPKTool.Common;
using ZPKTool.DataAccess;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Base class for the pictures and video view models.
    /// </summary>
    public abstract class MediaHandlingViewModelBase : ViewModel
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        protected static readonly NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The messenger service.
        /// </summary>
        protected readonly IMessenger Messenger;

        /// <summary>
        /// The window service.
        /// </summary>
        protected readonly IWindowService WindowService;

        /// <summary>
        /// The file dialog service.
        /// </summary>
        protected readonly IFileDialogService FileDialogService;

        /// <summary>
        /// Value indicating whether the media control is empty or not (i.e. there is no video)
        /// </summary>
        private bool isEmpty;

        /// <summary>
        /// A value indicating whether the view-model is during the process of toggling the full screen mode.
        /// </summary>
        private bool isTogglingFullScreen;

        /// <summary>
        /// A value indicating whether the preview mode is on or not.
        /// </summary>
        private bool isPreviewModeOn;

        /// <summary>
        /// A value indicating the index of the current media.
        /// </summary>
        private int currentMediaIndex;

        /// <summary>
        /// The current data manager.
        /// </summary>
        private IDataSourceManager dataManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="MediaHandlingViewModelBase" /> class.
        /// </summary>
        /// <param name="messenger">The messenger.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="fileDialogService">The file dialog service.</param>
        protected MediaHandlingViewModelBase(IMessenger messenger, IWindowService windowService, IFileDialogService fileDialogService)
        {
            this.Messenger = messenger;
            this.WindowService = windowService;
            this.FileDialogService = fileDialogService;

            this.MediaToBeAdded = new Collection<object>();
            this.MediaToBeDeleted = new Collection<object>();
            this.MediaSource = new MediaSource();
            this.MediaSource.AllSources.CollectionChanged += AllSourcesOnCollectionChanged;
            this.IsEmpty = true;
            this.IsPreviewModeOn = false;
            this.InitializeCommands();
        }

        /// <summary>
        /// The Refresh preview mode event handler.
        /// </summary>
        public event EventHandler RefreshPreviewMode;

        /// <summary>
        /// Gets or sets a value indicating whether the media control is empty or not.
        /// </summary>
        /// <value><c>true</c> if the media control is empty; otherwise, <c>false</c>.</value>
        public bool IsEmpty
        {
            get { return this.isEmpty; }
            set { this.SetProperty(ref this.isEmpty, value, () => this.IsEmpty); }
        }

        /// <summary>
        /// Gets or sets the object containing the media sources.
        /// </summary>
        public MediaSource MediaSource { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether the media control is during the process of toggling the full screen mode.
        /// </summary>
        public bool IsTogglingFullScreen
        {
            get { return this.isTogglingFullScreen; }
            set { this.SetProperty(ref this.isTogglingFullScreen, value, () => this.IsTogglingFullScreen); }
        }

        /// <summary>
        /// Gets the media to be added.
        /// The collection may contain strings, representing media's paths or media objects.
        /// </summary>
        public Collection<object> MediaToBeAdded { get; private set; }

        /// <summary>
        /// Gets the media to be deleted.
        /// </summary>
        public Collection<object> MediaToBeDeleted { get; private set; }

        /// <summary>
        /// Gets or sets the index of the current media.
        /// </summary>
        public int CurrentMediaIndex
        {
            get
            {
                return this.currentMediaIndex;
            }

            protected set
            {
                this.SetProperty(ref this.currentMediaIndex, value, () => this.CurrentMediaIndex);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the preview mode is on or not.
        /// </summary>
        public bool IsPreviewModeOn
        {
            get
            {
                return this.isPreviewModeOn;
            }

            protected set
            {
                this.SetProperty(ref this.isPreviewModeOn, value, () => this.IsPreviewModeOn);
            }
        }

        /// <summary>
        /// Gets or sets the data context to use for retrieving the media from the database.
        /// It is necessary only if the media source is MediaMetaData.
        /// </summary>        
        public IDataSourceManager DataContext
        {
            get
            {
                return this.dataManager;
            }

            set
            {
                this.SetProperty(ref this.dataManager, value, () => this.DataContext);
            }
        }

        /// <summary>
        /// Gets the previous media command.
        /// </summary>
        public ICommand PreviousMediaCommand { get; private set; }

        /// <summary>
        /// Gets the next media command.
        /// </summary>
        public ICommand NextMediaCommand { get; private set; }

        /// <summary>
        /// Gets the preview media command.
        /// </summary>
        public ICommand PreviewMediaCommand { get; private set; }

        /// <summary>
        /// Gets the save media as command.
        /// </summary>
        public ICommand SaveMediaAsCommand { get; private set; }

        /// <summary>
        /// Gets the delete current media command.
        /// </summary>
        public ICommand DeleteCurrentMediaCommand { get; private set; }

        /// <summary>
        /// Initializes the commands.
        /// </summary>
        private void InitializeCommands()
        {
            this.DeleteCurrentMediaCommand = new DelegateCommand(this.ExecuteDeleteCurrentMediaCommand);
            this.SaveMediaAsCommand = new DelegateCommand(this.ExecuteSaveMediaAsCommand, this.CanExecuteSaveMediaAsCommand);
            this.PreviousMediaCommand = new DelegateCommand(this.ExecutePreviousMediaCommand, this.CanExecutePreviousMediaCommand);
            this.NextMediaCommand = new DelegateCommand(this.ExecuteNextMediaCommand, this.CanExecuteNextMediaCommand);
            this.PreviewMediaCommand = new DelegateCommand(this.ExecutePreviewMediaCommand, this.CanExecutePreviewMediaCommand);
        }

        /// <summary>
        /// Obtain the binary representation of the media, having the source path as a parameter
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <returns>
        /// An array containing the media or null if an error occurred.
        /// </returns>
        /// <exception cref="ZPKException">Thrown when it fails to read the media file from the disk.</exception>
        public abstract byte[] GetMediaBytes(string filePath);

        /// <summary>
        /// Marks all meta data as deleted.
        /// </summary>
        public void MarkAllMetaDataAsDeleted()
        {
            this.MediaToBeDeleted.AddRange(this.MediaSource.MediaMetaData);
            this.MediaSource.MediaMetaData.Clear();
        }

        /// <summary>
        /// Toggles the preview mode on/off.
        /// </summary>
        /// <param name="setPreviewModeOn">Indicates whether to set the preview mode on or off.</param>
        public virtual void TogglePreviewMode(bool setPreviewModeOn)
        {
            // Note: overwrite this in the derived classes if needed. In the current version only the 
            // PicturesViewModel uses this method.
        }

        /// <summary>
        /// Displays the media at the given index.
        /// </summary>
        /// <param name="mediaIndex">Index of the media.</param>
        public virtual void DisplayMediaAtIndex(int mediaIndex)
        {
            // Note: overwrite this in the derived classes if needed. In the current version only the 
            // PicturesViewModel uses this method. The VideoViewModel contains only one video.
        }

        /// <summary>
        /// Validate view-model media sources before attempting to display Media data on screen.
        /// </summary>
        public virtual void ValidateMediaToDisplay()
        {
            // Note: overwrite this in the derived classes if needed. In the current version only the 
            // PicturesViewModel uses this method.
        }

        /// <summary>
        /// Called when [refresh preview mode].
        /// </summary>
        protected void OnRefreshPreviewMode()
        {
            var handler = this.RefreshPreviewMode;
            if (handler != null)
            {
                handler.Invoke(this, new EventArgs());
            }
        }

        /// <summary>
        /// Executes the previous media command.
        /// </summary>
        protected virtual void ExecutePreviousMediaCommand()
        {
            // Note: overwrite this in the derived classes if needed. In the current version only the 
            // PicturesViewModel uses this method.
        }

        /// <summary>
        /// Determines whether this instance [can execute previous media command].
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance [can execute previous media command]; otherwise, <c>false</c>.
        /// </returns>
        protected virtual bool CanExecutePreviousMediaCommand()
        {
            // Note: overwrite this in the derived classes if needed. In the current version only the 
            // PicturesViewModel uses this method.
            return false;
        }

        /// <summary>
        /// Executes the next media command.
        /// </summary>
        protected virtual void ExecuteNextMediaCommand()
        {
            // Note: overwrite this in the derived classes if needed. In the current version only the 
            // PicturesViewModel uses this method.
        }

        /// <summary>
        /// Determines whether this instance [can execute next media command].
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance [can execute next media command]; otherwise, <c>false</c>.
        /// </returns>
        protected virtual bool CanExecuteNextMediaCommand()
        {
            // Note: overwrite this in the derived classes if needed. In the current version only the 
            // PicturesViewModel uses this method.
            return false;
        }

        /// <summary>
        /// Executes the preview media command.
        /// </summary>
        protected virtual void ExecutePreviewMediaCommand()
        {
            // Note: overwrite this in the derived classes if needed. In the current version only the 
            // PicturesViewModel uses this method.
        }

        /// <summary>
        /// Determines whether this instance [can execute preview media command].
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance [can execute preview media command]; otherwise, <c>false</c>.
        /// </returns>
        protected virtual bool CanExecutePreviewMediaCommand()
        {
            // Note: overwrite this in the derived classes if needed. In the current version only the 
            // PicturesViewModel uses this method.
            return false;
        }

        /// <summary>
        /// Determines whether this instance [can execute save media as command].
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance [can execute save media as command]; otherwise, <c>false</c>.
        /// </returns>
        private bool CanExecuteSaveMediaAsCommand()
        {
            return !IsPreviewModeOn;
        }

        /// <summary>
        /// Executes the save media as command.
        /// </summary>
        private void ExecuteSaveMediaAsCommand()
        {
            var sourceFile = this.GetCurrentMediaFileName();
            var fileName = string.Empty;
            var extension = string.Empty;

            if (!string.IsNullOrEmpty(sourceFile))
            {
                fileName = Path.GetFileNameWithoutExtension(sourceFile);
                extension = Path.GetExtension(sourceFile);
            }

            if (string.IsNullOrEmpty(extension))
            {
                extension = "*.*|*.*";
            }
            else
            {
                extension = "*" + extension + "|*" + extension;
            }

            this.FileDialogService.Reset();
            this.FileDialogService.FileName = fileName;
            this.FileDialogService.Filter = extension;
            this.FileDialogService.FilterIndex = 0;

            var result = this.FileDialogService.ShowSaveFileDialog();

            if (result == true)
            {
                var mediaContent = GetCurrentMediaContent();

                try
                {
                    File.WriteAllBytes(this.FileDialogService.FileName, mediaContent);
                }
                catch (Exception ex)
                {
                    Log.ErrorException("An error occurred while trying to save the configuration.", ex);
                    throw PathUtils.ParseFileRelatedException(ex);
                }
            }
        }

        /// <summary>
        /// Gets the content of the current media.
        /// </summary>
        /// <returns>The content of the current media</returns>
        protected abstract byte[] GetCurrentMediaContent();

        /// <summary>
        /// Gets the name of the current media file.
        /// </summary>
        /// <returns>The name of the current media file.</returns>
        protected abstract string GetCurrentMediaFileName();

        /// <summary>
        /// Executes the delete current media command.
        /// </summary>
        protected virtual void ExecuteDeleteCurrentMediaCommand()
        {
        }

        /// <summary>
        /// Handles the OnCollectionChanged event of the AllSources control.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="notifyCollectionChangedEventArgs">The <see cref="NotifyCollectionChangedEventArgs" /> instance containing the event data.</param>
        private void AllSourcesOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
        {
            IsEmpty = this.MediaSource.AllSources.Count == 0;
        }
    }
}