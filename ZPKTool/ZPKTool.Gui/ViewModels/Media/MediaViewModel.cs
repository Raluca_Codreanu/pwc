﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for displaying and managing the media (images or video) of an entity.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class MediaViewModel : ViewModel<object, IDataSourceManager>, IUndoManager
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The list of image extensions supported by the application.
        /// </summary>
        private static readonly StringCollection SupportedImagesExtensions = new StringCollection { ".jpeg", ".jpg", ".gif", ".png", ".tiff", ".tif" };

        /// <summary>
        /// The list of video extensions supported by the application.
        /// </summary>
        private static readonly StringCollection SupportedVideosExtensions = new StringCollection { ".avi", ".wmv", ".asf", ".mpg" };

        /// <summary>
        /// The messenger service.
        /// </summary>
        private readonly IMessenger messenger;

        /// <summary>
        /// The messenger service used to privately communicate with the nested view-models.
        /// </summary>
        private readonly IMessenger privateMessenger;

        /// <summary>
        /// The model browser helper service.
        /// </summary>
        private readonly IModelBrowserHelperService modelBrowserHelperService;

        /// <summary>
        /// The window service.
        /// </summary>
        private readonly IWindowService windowService;

        /// <summary>
        /// The file dialog service.
        /// </summary>
        private readonly IFileDialogService fileDialog;

        /// <summary>
        /// The video view model.
        /// </summary>
        private readonly Lazy<VideoViewModel> videoViewModel;

        /// <summary>
        /// The pictures view model.
        /// </summary>
        private readonly Lazy<PicturesViewModel> picturesViewModel;

        /// <summary>
        /// The component that currently handles the entity's media.. Currently it can be either video or pictures view model.
        /// </summary>
        private MediaHandlingViewModelBase mediaHandler;

        /// <summary>
        /// The current media control mode.
        /// </summary>
        private MediaControlMode mode;

        /// <summary>
        /// The undo list.
        /// </summary>
        private ObservableCollection<MediaUndoableItem> undoStack;

        /// <summary>
        /// A value that determines whether the application logo should be displayed or not.
        /// </summary>
        private bool displayLogo;

        #endregion Attributes

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MediaViewModel"/> class.
        /// </summary>
        /// <param name="videoViewModel">The video view-model.</param>
        /// <param name="picturesViewModel">The pictures view-model.</param>
        /// <param name="messenger">The messenger.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="fileDialogService">The file dialog service.</param>
        /// <param name="modelBrowserHelperService">The model browser helper service.</param>
        [ImportingConstructor]
        public MediaViewModel(
            VideoViewModel videoViewModel,
            PicturesViewModel picturesViewModel,
            IMessenger messenger,
            IWindowService windowService,
            IFileDialogService fileDialogService,
            IModelBrowserHelperService modelBrowserHelperService)
        {
            this.messenger = messenger;
            this.windowService = windowService;
            this.modelBrowserHelperService = modelBrowserHelperService;
            this.fileDialog = fileDialogService;

            this.privateMessenger = new Messenger();
            this.privateMessenger.Register<NotificationMessage>(this.HandleNotificationMessage);

            this.PropertyChanged += OnPropertyChanged;

            InitializeCommands();

            this.videoViewModel = new Lazy<VideoViewModel>(() => new VideoViewModel(this.privateMessenger, this.windowService, this.fileDialog));
            this.picturesViewModel = new Lazy<PicturesViewModel>(() => new PicturesViewModel(this.privateMessenger, this.windowService, this.fileDialog));

            this.undoStack = new ObservableCollection<MediaUndoableItem>();
        }

        #endregion Constructor

        #region Commands

        /// <summary>
        /// Gets the add media command.
        /// </summary>
        public ICommand AddMediaCommand { get; private set; }

        /// <summary>
        /// Gets the DragOver command.
        /// </summary>
        public ICommand CheckDropTargetCommand { get; private set; }

        /// <summary>
        /// Gets the drop command.
        /// </summary>
        public ICommand DropCommand { get; private set; }

        /// <summary>
        /// Gets the drop command.
        /// </summary>
        public ICommand PasteCommand { get; private set; }

        /// <summary>
        /// Gets a command that informs the view model that the view is loaded, to retrieve the data to show (in this case the media pictures and videos).
        /// </summary>
        public ICommand ViewLoadedCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets or sets the component that currently handles the entity's media.
        /// </summary>
        public MediaHandlingViewModelBase MediaHandler
        {
            get
            {
                return this.mediaHandler;
            }

            set
            {
                if (this.mediaHandler != value)
                {
                    if (this.MediaHandler != null)
                    {
                        this.MediaHandler.PropertyChanged -= OnMediaHandlerPropertyChanged;
                    }

                    this.mediaHandler = value;
                    if (this.MediaHandler != null)
                    {
                        this.MediaHandler.PropertyChanged += OnMediaHandlerPropertyChanged;
                    }

                    this.SetLogoDisplayState();
                    OnPropertyChanged(() => this.MediaHandler);
                }
            }
        }

        /// <summary>
        /// Gets or sets the mode of the media view.
        /// </summary>
        public MediaControlMode Mode
        {
            get { return this.mode; }
            set { this.SetProperty(ref this.mode, value, () => this.Mode); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the application logo should be displayed or not.
        /// </summary>
        public bool DisplayLogo
        {
            get { return this.displayLogo; }
            set { this.SetProperty(ref this.displayLogo, value, () => this.DisplayLogo); }
        }

        #endregion Properties

        #region Execute Commands

        /// <summary>
        /// Determines whether this instance [can execute add media command] or [can execute paste media command].
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance [can execute add media command]; otherwise, <c>false</c>.
        /// </returns>
        public bool CanExecuteAddMediaCommand()
        {
            return !IsReadOnly; // todo: && !isLoading, && !isinviewermode
        }

        /// <summary>
        /// Adds the media.
        /// </summary>
        private void ExecuteAddMediaCommand()
        {
            switch (Mode)
            {
                case MediaControlMode.SingleImage:
                    AddSingleImage();

                    break;

                case MediaControlMode.MultipleImagesOrVideo:
                case MediaControlMode.MultipleImagesNoVideo:
                    AddMultipleMedia();

                    break;
            }
        }

        #endregion Execute Commands

        #region Initialization

        /// <summary>
        /// Initializes the commands.
        /// </summary>
        private void InitializeCommands()
        {
            this.AddMediaCommand = new DelegateCommand(this.ExecuteAddMediaCommand, this.CanExecuteAddMediaCommand);
            this.CheckDropTargetCommand = new DelegateCommand<DragEventArgs>(CheckDropTarget, (e) => !this.IsReadOnly);
            this.DropCommand = new DelegateCommand<DragEventArgs>(Drop, (e) => !this.IsReadOnly);
            this.PasteCommand = new DelegateCommand(Paste, this.CanPaste);
            this.ViewLoadedCommand = new DelegateCommand(() => LoadMediaFromModel(true));
        }

        #endregion Initialization

        #region Model handling

        /// <summary>
        /// Called when <see cref="ViewLoadedCommand" /> is executed when the view state loaded/unloaded is changed.
        /// </summary>
        /// <param name="loadAsynchronously">A parameter indicating whether to load data asynchronously or not.</param>
        /// <exception cref="System.InvalidOperationException">The Model must implement IEntityWithMedia or IEntityWithMediaCollection</exception>
        private void LoadMediaFromModel(bool loadAsynchronously)
        {
            if (this.IsLoaded || this.Model == null)
            {
                return;
            }

            this.CheckModelAndDataSource();

            if (!(this.Model is IEntityWithMedia) && !(this.Model is IEntityWithMediaCollection))
            {
                throw new InvalidOperationException("The Model must implement IEntityWithMedia or IEntityWithMediaCollection");
            }

            if (IsInViewerMode)
            {
                this.LoadMediaForViewerMode();
            }
            else
            {
                if (loadAsynchronously)
                {
                    var mediaLoadTask = new Task<IEnumerable>(this.LoadMediaData);
                    mediaLoadTask.ContinueWith(this.AsynchronousMediaLoadingFinished);
                    mediaLoadTask.Start();
                }
                else
                {
                    var mediaCollection = this.LoadMediaData();
                    this.DisplayMedia(mediaCollection);
                }
            }
        }

        /// <summary>
        /// Load media when the application run in view mode.
        /// </summary>
        private void LoadMediaForViewerMode()
        {
            var importedMedia = this.modelBrowserHelperService.GetMediaForEntity(Model);
            if (importedMedia.Any())
            {
                if (importedMedia.ElementAt(0).Type == MediaType.Video)
                {
                    this.videoViewModel.Value.DataContext = null;
                    this.videoViewModel.Value.MediaSource.Reset();
                    this.videoViewModel.Value.MediaSource.MediaPaths.Add(importedMedia.ElementAt(0).CachedFilePath);
                    this.MediaHandler = this.videoViewModel.Value;
                }
                else
                {
                    var pictures = importedMedia.Where(media => media.Type == MediaType.Image).ToList();

                    if (pictures.Any())
                    {
                        this.picturesViewModel.Value.DataContext = null;
                        this.picturesViewModel.Value.MediaSource.Reset();

                        foreach (var picture in pictures)
                        {
                            this.picturesViewModel.Value.MediaSource.MediaPaths.Add(picture.CachedFilePath);
                        }

                        this.MediaHandler = this.picturesViewModel.Value;

                        if (pictures.Count == 1)
                        {
                            this.picturesViewModel.Value.DisplayMediaAtIndex(0);
                        }
                        else
                        {
                            this.MediaHandler.TogglePreviewMode(true);
                        }
                    }
                }
            }

            this.IsLoaded = true;
        }

        /// <summary>
        /// Load media metadata from current model or from database.
        /// </summary>
        /// <returns>The media collection or the metadata collection, depending on where the media data is loaded.</returns>
        private IEnumerable LoadMediaData()
        {
            // First search in the entity for media because, in rare cases, the media may be attached to its owner entity.
            var mediaList = this.GetMediaFromModel();
            if (mediaList.Any())
            {
                return mediaList;
            }

            // Load the entity's media from the database.
            var mediaManager = new MediaManager(DataAccessFactory.CreateDataSourceManager(this.DataSourceManager.DatabaseId));
            var metaDataList = mediaManager.GetPictureOrVideoMetaData(this.Model);
            return metaDataList;
        }

        /// <summary>
        /// The logic necessary when loading media on screen, after the media collection was loaded asynchronously from the model or from the database.
        /// </summary>
        /// <param name="task">The asynchronous operation that just finished and that return the media collection.</param>
        private void AsynchronousMediaLoadingFinished(Task<IEnumerable> task)
        {
            if (task.Exception != null)
            {
                log.Error(task.Exception.InnerException);
                this.ClearMedia();
                return;
            }

            try
            {
                this.DisplayMedia(task.Result);
            }
            catch (Exception ex)
            {
                log.ErrorException("Exception occurred while trying to load/display media files", ex);
                this.ClearMedia();
            }
            finally
            {
                this.IsLoaded = true;
            }
        }

        /// <summary>
        /// Process and display media data.
        /// </summary>
        /// <param name="mediaCollection">The media collection or the metadata collection, depending on where the media data is loaded.</param>
        private void DisplayMedia(IEnumerable mediaCollection)
        {
            var mediaList = mediaCollection as ICollection<Media>;
            if (mediaList != null && mediaList.Count > 0)
            {
                if ((MediaType)mediaList.ElementAt(0).Type == MediaType.Video)
                {
                    this.videoViewModel.Value.DataContext = this.DataSourceManager;
                    this.videoViewModel.Value.MediaSource.Reset();
                    this.videoViewModel.Value.MediaSource.Media.Add(mediaList.ElementAt(0));
                    this.MediaHandler = this.videoViewModel.Value;
                }
                else
                {
                    var pictures = mediaList.Where(media => (MediaType)media.Type == MediaType.Image).ToList();
                    if (pictures.Any())
                    {
                        this.picturesViewModel.Value.DataContext = this.DataSourceManager;
                        this.picturesViewModel.Value.MediaSource.Reset();
                        this.picturesViewModel.Value.MediaSource.Media.AddRange(pictures);
                        this.MediaHandler = this.picturesViewModel.Value;

                        if (pictures.Count == 1)
                        {
                            this.MediaHandler.DisplayMediaAtIndex(0);
                        }
                        else
                        {
                            this.MediaHandler.TogglePreviewMode(true);
                        }
                    }
                }
            }
            else
            {
                var metaData = mediaCollection as ICollection<MediaMetaData>;
                if (metaData == null || metaData.Count == 0)
                {
                    this.MediaHandler = null;
                }
                else
                {
                    if ((MediaType)metaData.ElementAt(0).Type == MediaType.Video)
                    {
                        this.videoViewModel.Value.DataContext = this.DataSourceManager;
                        this.videoViewModel.Value.MediaSource.Reset();
                        this.videoViewModel.Value.MediaSource.MediaMetaData.AddRange(metaData);
                        this.MediaHandler = this.videoViewModel.Value;
                    }
                    else
                    {
                        this.picturesViewModel.Value.DataContext = this.DataSourceManager;
                        this.picturesViewModel.Value.MediaSource.Reset();
                        this.MediaHandler = this.picturesViewModel.Value;

                        var mediaPictures = this.picturesViewModel.Value.GetPictures(this.Model, DataAccessFactory.CreateDataSourceManager(this.DataSourceManager.DatabaseId));
                        if (mediaPictures.Any())
                        {
                            this.MediaHandler.MediaSource.Media.AddRange(mediaPictures);
                            foreach (var metaDataItem in metaData)
                            {
                                this.MediaHandler.MediaSource.MediaMetaData.Add(metaDataItem);
                            }

                            if (mediaPictures.Count == 1)
                            {
                                this.MediaHandler.DisplayMediaAtIndex(0);
                            }
                            else
                            {
                                this.MediaHandler.TogglePreviewMode(true);
                            }
                        }
                    }
                }
            }

            this.IsLoaded = true;
        }

        /// <summary>
        /// Saves the media.
        /// </summary>
        /// <param name="mediaObjectsToAdd">The file paths to add.</param>
        /// <param name="onlySaveToModel">If set to true, only saves to model; otherwise also saves to data source.</param>
        private void SaveMedia(IEnumerable<object> mediaObjectsToAdd, bool onlySaveToModel)
        {
            if (mediaObjectsToAdd == null)
            {
                throw new ArgumentNullException("mediaObjectsToAdd", "the list of media to add to the model is null.");
            }

            int currentMediaSize = 0;
            foreach (var mediaObjectToAdd in mediaObjectsToAdd.Where(m => m != null))
            {
                Media media = mediaObjectToAdd as Media;
                var filePathToAdd = mediaObjectToAdd as string;
                if (media == null && !string.IsNullOrWhiteSpace(filePathToAdd))
                {
                    media = GetMediaFromFilePath(filePathToAdd);
                    if (media != null)
                    {
                        switch (media.Type)
                        {
                            case MediaType.Image:
                                {
                                    this.picturesViewModel.Value.MediaSource.MediaPaths.Remove(filePathToAdd);
                                    this.picturesViewModel.Value.MediaSource.Media.Add(media);
                                    break;
                                }

                            case MediaType.Video:
                                {
                                    this.videoViewModel.Value.MediaSource.MediaPaths.Remove(filePathToAdd);
                                    this.videoViewModel.Value.MediaSource.Media.Add(media);
                                    break;
                                }

                            default:
                                break;
                        }
                    }
                }

                if (media != null)
                {
                    AddMediaToModel(media, filePathToAdd);

                    if (!onlySaveToModel)
                    {
                        currentMediaSize += media.Size ?? 0;
                        if (currentMediaSize >= Constants.MaxVideoSize)
                        {
                            this.DataSourceManager.SaveChanges();
                            currentMediaSize = 0;
                        }
                    }
                }
            }

            if (!onlySaveToModel
                && currentMediaSize > 0)
            {
                this.DataSourceManager.SaveChanges();
            }
        }

        /// <summary>
        /// Adds the media to model.
        /// </summary>
        /// <param name="media">The media to add.</param>
        /// <param name="filePathToAdd">The file path to add.</param>
        private void AddMediaToModel(Media media, string filePathToAdd)
        {
            IOwnedObject ownedObj = Model as IOwnedObject;
            media.Owner = ownedObj != null ? ownedObj.Owner : null;

            IMasterDataObject masterObj = Model as IMasterDataObject;
            media.IsMasterData = masterObj != null && masterObj.IsMasterData;

            var entityWithMediaCollection = Model as IEntityWithMediaCollection;
            if (entityWithMediaCollection != null)
            {
                entityWithMediaCollection.Media.Add(media);
            }
            else
            {
                var entityWithMedia = Model as IEntityWithMedia;
                if (entityWithMedia != null)
                {
                    entityWithMedia.Media = media;
                }
            }

            if (media.Type == (short)MediaType.Image)
            {
                var mediaMetaData = new MediaMetaData() { Guid = media.Guid };
                this.picturesViewModel.Value.MediaSource.MediaMetaData.Add(mediaMetaData);
                this.UpdateAddedMediaInUndoStack(filePathToAdd, media, mediaMetaData);
            }
            else if (media.Type == MediaType.Video)
            {
                var mediaMetaData = new MediaMetaData() { Guid = media.Guid };
                this.videoViewModel.Value.MediaSource.MediaMetaData.Add(new MediaMetaData() { Guid = media.Guid });
                this.UpdateAddedMediaInUndoStack(filePathToAdd, media, mediaMetaData);
            }
        }

        #endregion Model handling

        #region Load Media

        /// <summary>
        /// Load media to view-model.
        /// </summary>
        /// <param name="mediaList">The list of media.</param>
        public void LoadMedia(List<Media> mediaList)
        {
            this.Push(this.GenerateUndoableItem(this.MediaHandler));
            this.ClearMedia();
            this.IsChanged = true;

            if (mediaList.Count == 0)
            {
                return;
            }

            switch (this.Mode)
            {
                case MediaControlMode.SingleImage:
                    {
                        if ((MediaType)mediaList[0].Type == MediaType.Image)
                        {
                            this.AddSingleImageMedia(mediaList[0]);
                        }

                        break;
                    }

                case MediaControlMode.MultipleImagesOrVideo:
                    {
                        if ((MediaType)mediaList[0].Type == MediaType.Video)
                        {
                            this.AddVideoMedia(mediaList[0]);
                        }
                        else
                        {
                            var pictures = mediaList.Where(media => (MediaType)media.Type == MediaType.Image).ToList();
                            if (pictures.Count == 1)
                            {
                                this.AddSingleImageMedia(mediaList[0]);
                            }
                            else
                            {
                                this.AddMultipleImagesMedia(mediaList);
                            }
                        }

                        break;
                    }

                case MediaControlMode.MultipleImagesNoVideo:
                    {
                        var pictures = mediaList.Where(media => (MediaType)media.Type == MediaType.Image).ToList();
                        if (pictures.Count == 1)
                        {
                            this.AddSingleImageMedia(mediaList[0]);
                        }
                        else
                        {
                            this.AddMultipleImagesMedia(mediaList);
                        }

                        break;
                    }

                default:
                    break;
            }
        }

        /// <summary>
        /// Reset videoViewModel and picturesViewModel media.
        /// </summary>
        private void ClearMedia()
        {
            // If the video view-model is not created yet, it makes no sense to create it now only to reset (i.e. do nothing) its media source.
            if (this.videoViewModel.IsValueCreated)
            {
                foreach (var media in this.videoViewModel.Value.MediaSource.Media)
                {
                    if (!this.videoViewModel.Value.MediaSource.IsMediaFromMetaData(media, this.videoViewModel.Value.MediaSource.MediaMetaData))
                    {
                        this.videoViewModel.Value.MediaToBeDeleted.Add(media);
                    }
                }

                var sourcesCount = this.videoViewModel.Value.MediaSource.AllSources.Count;
                this.videoViewModel.Value.MarkAllMetaDataAsDeleted();
                this.videoViewModel.Value.MediaSource.Reset();
                this.videoViewModel.Value.MediaToBeAdded.Clear();

                if (sourcesCount > 0)
                {
                    this.MediaHandler = this.picturesViewModel.Value;
                }
            }

            // If the pictures view-model is not created yet, it makes no sense to create it now only to reset (i.e. do nothing) its media source.
            if (this.picturesViewModel.IsValueCreated)
            {
                foreach (var media in this.picturesViewModel.Value.MediaSource.Media)
                {
                    if (!this.picturesViewModel.Value.MediaSource.IsMediaFromMetaData(media, this.picturesViewModel.Value.MediaSource.MediaMetaData))
                    {
                        this.picturesViewModel.Value.MediaToBeDeleted.Add(media);
                    }
                }

                this.picturesViewModel.Value.MarkAllMetaDataAsDeleted();
                this.picturesViewModel.Value.MediaSource.Reset();
                this.picturesViewModel.Value.MediaToBeAdded.Clear();
                this.picturesViewModel.Value.DisplayMediaAtIndex(0);
            }
        }

        /// <summary>
        /// Add to MediaHandler a single image.
        /// </summary>
        /// <param name="media">The media containing the image.</param>
        private void AddSingleImageMedia(Media media)
        {
            if ((MediaType)media.Type != MediaType.Image)
            {
                throw new ArgumentException("This type of media is currently not handled: " + (MediaType)media.Type);
            }

            this.picturesViewModel.Value.DataContext = this.DataSourceManager;
            this.picturesViewModel.Value.MediaToBeAdded.Add(media);
            this.picturesViewModel.Value.MediaSource.Media.Add(media);
            this.IsChanged = true;

            this.MediaHandler = this.picturesViewModel.Value;
            this.picturesViewModel.Value.DisplayMediaAtIndex(0);
            MediaChangedMessage msg = new MediaChangedMessage()
            {
                Entity = this.Model,
                Media = this.MediaHandler.MediaSource.AllSources.ToList(),
                ChangeType = MediaChangeType.MediaAdded
            };
            this.messenger.Send(msg);
        }

        /// <summary>
        /// Add to MediaHandler multiple images.
        /// </summary>
        /// <param name="mediaList">The list of media pictures.</param>
        private void AddMultipleImagesMedia(List<Media> mediaList)
        {
            foreach (var media in mediaList)
            {
                if ((MediaType)media.Type != MediaType.Image)
                {
                    throw new ArgumentException("This type of media is currently not handled: " + (MediaType)media.Type);
                }
            }

            this.picturesViewModel.Value.DataContext = this.DataSourceManager;
            this.picturesViewModel.Value.MediaToBeAdded.AddRange(mediaList);
            this.picturesViewModel.Value.MediaSource.Media.AddRange(mediaList);
            this.IsChanged = true;

            this.MediaHandler = this.picturesViewModel.Value;
            this.MediaHandler.TogglePreviewMode(true);
            MediaChangedMessage msg = new MediaChangedMessage()
            {
                Entity = this.Model,
                Media = this.MediaHandler.MediaSource.AllSources.ToList(),
                ChangeType = MediaChangeType.MediaAdded
            };
            this.messenger.Send(msg);
        }

        /// <summary>
        /// Add to MediaHandler a video.
        /// </summary>
        /// <param name="media">The media containing the video.</param>
        private void AddVideoMedia(Media media)
        {
            if ((MediaType)media.Type != MediaType.Video)
            {
                throw new ArgumentException("This type of media is currently not handled: " + (MediaType)media.Type);
            }

            this.videoViewModel.Value.DataContext = this.DataSourceManager;
            this.videoViewModel.Value.MediaToBeAdded.Add(media);
            this.videoViewModel.Value.MediaSource.Media.Add(media);
            this.IsChanged = true;

            this.MediaHandler = this.videoViewModel.Value;
            this.videoViewModel.Value.RequestVideoSourceCommand.Execute(null);
            MediaChangedMessage msg = new MediaChangedMessage()
            {
                Entity = this.Model,
                Media = this.videoViewModel.Value.MediaSource.AllSources.ToList(),
                ChangeType = MediaChangeType.MediaAdded
            };
            this.messenger.Send(msg);
        }

        #endregion Load Media

        #region Add Media

        /// <summary>
        /// Displays the open file dialog box to browse for a new media source.
        /// </summary>
        private void AddSingleImage()
        {
            this.fileDialog.Filter = LocalizedResources.DialogFilter_Images;
            this.fileDialog.FilterIndex = 0;

            // Display the open file dialog
            var res = this.fileDialog.ShowOpenFileDialog();
            if (res == true)
            {
                // check for the file type (image/video)
                if (IsImageSupported(this.fileDialog.FileName))
                {
                    this.AddSingleImageFromObject(this.fileDialog.FileName);
                    MediaChangedMessage msg = new MediaChangedMessage()
                    {
                        Entity = this.Model,
                        Media = this.picturesViewModel.Value.MediaSource.AllSources.ToList(),
                        ChangeType = MediaChangeType.MediaAdded
                    };
                    this.messenger.Send(msg);
                }
                else
                {
                    if (IsVideoSupported(this.fileDialog.FileName))
                    {
                        this.windowService.MessageDialogService.Show(LocalizedResources.Media_VideoNotAllowed, MessageDialogType.Info);
                    }
                    else
                    {
                        this.windowService.MessageDialogService.Show(LocalizedResources.Media_FileImageNotSupported, MessageDialogType.Info);
                    }
                }
            }
        }

        /// <summary>
        /// Adds a new picture to picture model using a file path
        /// </summary>
        /// <param name="obj">The file path to source or the media of object</param>
        private void AddSingleImageFromObject(object obj)
        {
            this.Push(this.GenerateUndoableItem(this.picturesViewModel.Value));

            // If the video view model is not created yet, it makes no sense to create it now only to reset (i.e. do nothing) its media source.
            if (this.videoViewModel.IsValueCreated)
            {
                this.videoViewModel.Value.MarkAllMetaDataAsDeleted();
                this.videoViewModel.Value.MediaToBeAdded.Clear();
                this.videoViewModel.Value.MediaSource.Reset();
            }

            // Todo: MediaSource.Media should not be added into RemovedMedia?
            this.picturesViewModel.Value.DataContext = this.DataSourceManager;
            this.picturesViewModel.Value.MarkAllMetaDataAsDeleted();
            this.picturesViewModel.Value.MediaToBeAdded.Clear();
            this.picturesViewModel.Value.MediaToBeAdded.Add(obj);

            this.picturesViewModel.Value.MediaSource.Reset();

            var filePath = obj as string;
            if (filePath != null)
            {
                this.picturesViewModel.Value.MediaSource.MediaPaths.Add(filePath);
            }
            else
            {
                var media = obj as Media;
                if (media != null)
                {
                    this.picturesViewModel.Value.MediaSource.Media.Add(media);
                }
            }

            this.IsChanged = true;

            this.MediaHandler = this.picturesViewModel.Value;
            this.picturesViewModel.Value.DisplayMediaAtIndex(0);
        }

        /// <summary>
        /// Adds the multiple media.
        /// </summary>
        private void AddMultipleMedia()
        {
            fileDialog.Multiselect = true;
            fileDialog.FilterIndex = 0;
            fileDialog.Filter = this.Mode == MediaControlMode.MultipleImagesOrVideo ? LocalizedResources.DialogFilter_AllMedia : LocalizedResources.DialogFilter_Images;

            var res = fileDialog.ShowOpenFileDialog();
            if (res == true)
            {
                if (CheckSelection(fileDialog.FileNames, true))
                {
                    this.Push(this.GenerateUndoableItem(this.MediaHandler));

                    foreach (var fileName in fileDialog.FileNames)
                    {
                        if (IsImageSupported(fileName))
                        {
                            AddImage(fileName, true);
                            this.IsChanged = true;
                        }
                        else if (IsVideoSupported(fileName))
                        {
                            if (this.Mode == MediaControlMode.MultipleImagesOrVideo)
                            {
                                this.AddSingleVideo(fileName, true);
                                this.IsChanged = true;
                                MediaChangedMessage msg = new MediaChangedMessage()
                                {
                                    Entity = this.Model,
                                    Media = this.videoViewModel.Value.MediaSource.AllSources.ToList(),
                                    ChangeType = MediaChangeType.MediaAdded
                                };
                                this.messenger.Send(msg);
                            }
                            else
                            {
                                this.windowService.MessageDialogService.Show(LocalizedResources.Media_VideoNotAllowed, MessageDialogType.Info);
                            }
                        }
                        else
                        {
                            this.windowService.MessageDialogService.Show(LocalizedResources.Media_FileNotSupported, MessageDialogType.Info);
                        }
                    }

                    if (this.MediaHandler == this.picturesViewModel.Value)
                    {
                        if (this.MediaHandler.MediaSource.AllSources.Count > 1)
                        {
                            this.MediaHandler.TogglePreviewMode(true);
                        }
                        else
                        {
                            this.picturesViewModel.Value.DisplayMediaAtIndex(0);
                        }

                        MediaChangedMessage msg = new MediaChangedMessage()
                        {
                            Entity = this.Model,
                            Media = this.MediaHandler.MediaSource.AllSources.ToList(),
                            ChangeType = MediaChangeType.MediaAdded
                        };
                        this.messenger.Send(msg);
                    }
                }
            }
        }

        /// <summary>
        /// Add the image represented by the file path.
        /// </summary>
        /// <param name="obj">The path of the image or the media containing the image</param>
        /// <param name="showMessage">A bool value indicating whether to show or not the dialog message error.</param>
        private void AddImage(object obj, bool showMessage)
        {
            try
            {
                var filePath = obj as string;
                if (filePath != null)
                {
                    FileInfo fi = new FileInfo(filePath);
                    if (fi.Length > Constants.MaxVideoSize)
                    {
                        var msg = string.Format(LocalizedResources.Error_FileTooBig, (Constants.MaxVideoSize / 1024 / 1024).ToString());

                        if (showMessage)
                        {
                            this.windowService.MessageDialogService.Show(msg, MessageDialogType.Error);
                        }

                        return;
                    }
                }

                this.videoViewModel.Value.MarkAllMetaDataAsDeleted();
                this.picturesViewModel.Value.DataContext = this.DataSourceManager;
                this.picturesViewModel.Value.MediaToBeAdded.Add(obj);

                if (filePath != null)
                {
                    this.picturesViewModel.Value.MediaSource.MediaPaths.Add(filePath);
                }
                else
                {
                    var media = obj as Media;
                    if (media != null)
                    {
                        this.picturesViewModel.Value.MediaSource.Media.Add(media);
                    }
                }

                this.IsChanged = true;

                this.MediaHandler = this.picturesViewModel.Value;
            }
            catch (Exception ex)
            {
                Log.ErrorException("Error occurred while setting the image's source and additional info.", ex);
            }
        }

        /// <summary>
        /// Sets the video's source and additional info
        /// </summary>
        /// <param name="filePath">The path of the Video</param>
        /// <param name="showMessage">A bool value indicating whether to show or not the dialog message error.</param>
        private void AddSingleVideo(string filePath, bool showMessage)
        {
            try
            {
                FileInfo fi = new FileInfo(filePath);
                if (fi.Length > Constants.MaxVideoSize)
                {
                    var msg = string.Format(LocalizedResources.Error_FileTooBig, Constants.MaxVideoSize / 1024 / 1024);

                    // if the source is from drop action suppress the messages to user
                    if (showMessage)
                    {
                        this.windowService.MessageDialogService.Show(msg, MessageDialogType.Error);
                    }
                }
                else
                {
                    this.picturesViewModel.Value.MarkAllMetaDataAsDeleted();
                    this.picturesViewModel.Value.MediaToBeAdded.Clear();
                    this.picturesViewModel.Value.MediaSource.Reset();

                    this.videoViewModel.Value.DataContext = this.DataSourceManager;
                    this.videoViewModel.Value.MarkAllMetaDataAsDeleted();
                    this.videoViewModel.Value.MediaSource.Reset();
                    this.videoViewModel.Value.MediaToBeAdded.Clear();
                    this.videoViewModel.Value.MediaToBeAdded.Add(filePath);
                    this.MediaHandler = this.videoViewModel.Value;
                    this.videoViewModel.Value.MediaSource.MediaPaths.Add(filePath);
                    this.videoViewModel.Value.RequestVideoSourceCommand.Execute(null);
                }
            }
            catch (Exception ex)
            {
                Log.ErrorException("Error occurred while setting the video's source and additional info.", ex);
            }
        }

        /// <summary>
        /// Gets the media from file path.
        /// </summary>
        /// <param name="filePathToAdd">The file path to add.</param>
        /// <returns>The created media object based on the file path passed as parameter.</returns>
        private Media GetMediaFromFilePath(string filePathToAdd)
        {
            Media mediaObject = new Media();
            mediaObject.Type = MediaHandler is PicturesViewModel ? MediaType.Image : MediaType.Video;
            mediaObject.Content = this.MediaHandler.GetMediaBytes(filePathToAdd);
            mediaObject.Size = mediaObject.Content != null ? (int?)mediaObject.Content.Length : null;
            mediaObject.OriginalFileName = System.IO.Path.GetFileName(filePathToAdd);

            return mediaObject;
        }

        #endregion Add Media

        #region Delete Media

        /// <summary>
        /// Deletes the media object
        /// </summary>
        /// <param name="mediaData">The information regarding the media object to delete.</param>
        private void DeleteMedia(object mediaData)
        {
            if (mediaData == null)
            {
                throw new ArgumentNullException("metaData", "The 'metaData'param was null.");
            }

            if (this.DataSourceManager != null)
            {
                var mediaMetaData = mediaData as MediaMetaData;
                if (mediaMetaData != null)
                {
                    this.DataSourceManager.MediaRepository.DeleteMedia(mediaMetaData);
                }
                else
                {
                    var media = mediaData as Media;
                    if (media != null)
                    {
                        this.DataSourceManager.MediaRepository.RemoveAll(media);

                        // Remove the media from model if it's the case.
                        var mediaCollectionEntity = this.Model as IEntityWithMediaCollection;
                        if (mediaCollectionEntity != null)
                        {
                            if (mediaCollectionEntity.Media != null && mediaCollectionEntity.Media.Contains(media))
                            {
                                mediaCollectionEntity.Media.Remove(media);
                            }
                        }
                        else
                        {
                            var mediaEntity = this.Model as IEntityWithMedia;
                            if (mediaEntity != null)
                            {
                                mediaEntity.Media = null;
                            }
                        }
                    }
                }
            }
        }

        #endregion Delete Media

        #region Paste Media

        /// <summary>
        /// check paste from clipboard command, checks if any item exists in clipboard
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance [can execute paste command]; otherwise, <c>false</c>.
        /// </returns>
        public bool CanPaste()
        {
            if (this.IsReadOnly)
            {
                return false;
            }

            var canPasteImage = this.picturesViewModel.Value.CanPaste(Constants.NumberOfMaxAllowedPictures);
            if (canPasteImage)
            {
                return true;
            }
            else
            {
                var droppedData = Clipboard.GetFileDropList();
                if (droppedData != null)
                {
                    return this.CheckMediaFilePaths(droppedData.OfType<string>());
                }
            }

            return false;
        }

        /// <summary>
        /// Performs the paste operation
        /// </summary>
        private void Paste()
        {
            if (this.picturesViewModel.Value.CanPaste(Constants.NumberOfMaxAllowedPictures))
            {
                var media = this.picturesViewModel.Value.GetMediaFromClipboard();
                if (media != null)
                {
                    this.AddMedia(media);
                }
            }
            else
            {
                var sourceItem = Clipboard.GetFileDropList();
                if (sourceItem != null)
                {
                    this.AddMediaFromFiles(sourceItem.OfType<string>(), false);
                }
            }
        }

        #endregion Paste Media

        #region Drop Media

        /// <summary>
        /// Handler for the check drop target command, checks if the dragged item can be dropped
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        private void CheckDropTarget(DragEventArgs e)
        {
            e.Handled = true;

            if (this.IsReadOnly)
            {
                e.Effects = DragDropEffects.None;
                return;
            }

            bool isValid = false;

            // Check if the data to be dropped is an accepted format file
            var canDropPicture = this.picturesViewModel.Value.IsContainingImage(e.Data);
            if (canDropPicture)
            {
                isValid = true;
            }
            else
            {
                var droppedData = e.Data.GetData(DataFormats.FileDrop);
                var sourcePaths = droppedData as IEnumerable<string>;

                if (sourcePaths == null)
                {
                    var sourcePath = droppedData as string;
                    if (!string.IsNullOrWhiteSpace(sourcePath))
                    {
                        sourcePaths = new List<string>() { sourcePath };
                    }
                }

                if (sourcePaths != null)
                {
                    isValid = this.CheckMediaFilePaths(sourcePaths);
                }
            }

            if (isValid)
            {
                e.Effects = DragDropEffects.Copy;
            }
            else
            {
                e.Effects = DragDropEffects.None;
            }
        }

        /// <summary>
        /// Handler for the drop action, performs the drop operation
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        private void Drop(DragEventArgs e)
        {
            e.Handled = true;

            var canDropPicture = this.picturesViewModel.Value.IsContainingImage(e.Data);
            if (canDropPicture)
            {
                var media = this.picturesViewModel.Value.GetMediaFromDataObject(e.Data);
                if (media != null)
                {
                    this.AddMedia(media);
                }
            }
            else
            {
                var sourceItem = e.Data.GetData(DataFormats.FileDrop);

                var sourcePaths = sourceItem as IEnumerable<string>;
                if (sourcePaths != null)
                {
                    this.AddMediaFromFiles(sourcePaths, false);
                }
                else
                {
                    string sourcePath = sourceItem as string;
                    if (!string.IsNullOrWhiteSpace(sourcePath))
                    {
                        this.AddMediaFromFiles(new List<string>() { sourcePath }, false);
                    }
                }
            }

            // Forcing the CommandManager to raise the RequerySuggested event.
            CommandManager.InvalidateRequerySuggested();
        }

        #endregion Drop Media

        #region Other Methods

        /// <summary>
        /// Add media from files
        /// </summary>
        /// <param name="sourceItem">The files path</param>
        /// <param name="showMessage">A bool value indicating whether to show or not the dialog message error.</param>
        private void AddMediaFromFiles(IEnumerable<string> sourceItem, bool showMessage)
        {
            switch (Mode)
            {
                case MediaControlMode.SingleImage:
                    {
                        // find the first valid image and add it
                        foreach (var fileName in sourceItem)
                        {
                            if (IsImageSupported(fileName))
                            {
                                this.AddSingleImageFromObject(fileName);
                                MediaChangedMessage msg = new MediaChangedMessage()
                                {
                                    Entity = this.Model,
                                    Media = this.MediaHandler.MediaSource.AllSources.ToList(),
                                    ChangeType = MediaChangeType.MediaAdded
                                };
                                this.messenger.Send(msg);
                                break;
                            }
                        }
                    }

                    break;

                case MediaControlMode.MultipleImagesOrVideo:
                case MediaControlMode.MultipleImagesNoVideo:
                    {
                        int addedMediaCount = 0;
                        if (CheckSelection(sourceItem, showMessage))
                        {
                            var undoableItem = this.GenerateUndoableItem(this.MediaHandler);

                            foreach (var fileName in sourceItem)
                            {
                                if (IsImageSupported(fileName))
                                {
                                    addedMediaCount++;
                                    AddImage(fileName, showMessage);
                                    this.IsChanged = true;
                                }
                                else if (IsVideoSupported(fileName))
                                {
                                    if (this.Mode == MediaControlMode.MultipleImagesOrVideo)
                                    {
                                        addedMediaCount++;
                                        this.AddSingleVideo(fileName, showMessage);
                                        this.IsChanged = true;
                                    }
                                }
                            }

                            if (addedMediaCount > 0)
                            {
                                this.Push(undoableItem);
                                MediaChangedMessage msg = new MediaChangedMessage()
                                {
                                    Entity = this.Model,
                                    Media = this.MediaHandler.MediaSource.AllSources.ToList(),
                                    ChangeType = MediaChangeType.MediaAdded
                                };
                                this.messenger.Send(msg);
                            }

                            if (this.MediaHandler == this.picturesViewModel.Value)
                            {
                                if (this.MediaHandler.MediaSource.AllSources.Count > 1)
                                {
                                    this.MediaHandler.TogglePreviewMode(true);
                                }
                                else
                                {
                                    this.picturesViewModel.Value.DisplayMediaAtIndex(0);
                                }
                            }
                        }
                    }

                    break;
            }
        }

        /// <summary>
        /// Add a media object
        /// </summary>
        /// <param name="media">The media to be added</param>
        private void AddMedia(Media media)
        {
            switch (Mode)
            {
                case MediaControlMode.SingleImage:
                    {
                        this.AddSingleImageFromObject(media);
                        MediaChangedMessage msg = new MediaChangedMessage()
                        {
                            Entity = this.Model,
                            Media = this.MediaHandler.MediaSource.AllSources.ToList(),
                            ChangeType = MediaChangeType.MediaAdded
                        };
                        this.messenger.Send(msg);
                    }

                    break;

                case MediaControlMode.MultipleImagesOrVideo:
                case MediaControlMode.MultipleImagesNoVideo:
                    {
                        if (this.picturesViewModel.Value.CanAddImage(Constants.NumberOfMaxAllowedPictures, true))
                        {
                            this.Push(this.GenerateUndoableItem(this.MediaHandler));

                            AddImage(media, false);
                            this.IsChanged = true;

                            MediaChangedMessage msg = new MediaChangedMessage()
                            {
                                Entity = this.Model,
                                Media = this.MediaHandler.MediaSource.AllSources.ToList(),
                                ChangeType = MediaChangeType.MediaAdded
                            };
                            this.messenger.Send(msg);

                            if (this.MediaHandler == this.picturesViewModel.Value)
                            {
                                if (this.MediaHandler.MediaSource.AllSources.Count > 1)
                                {
                                    this.MediaHandler.TogglePreviewMode(true);
                                }
                                else
                                {
                                    this.picturesViewModel.Value.DisplayMediaAtIndex(0);
                                }
                            }
                        }
                    }

                    break;
            }
        }

        /// <summary>
        /// Checks the selection from the browse file
        /// </summary>
        /// <param name="fileNames">The file names.</param>
        /// <param name="showMessage">A bool value indicating whether to show or not the dialog message error.</param>
        /// <returns>true if the files selected can be added to the media control</returns>
        private bool CheckSelection(IEnumerable<string> fileNames, bool showMessage)
        {
            var numberOfVideos = 0;
            var numberOfPictures = 0;

            foreach (var fileName in fileNames)
            {
                if (IsVideoSupported(fileName))
                {
                    numberOfVideos++;
                }

                if (IsImageSupported(fileName))
                {
                    numberOfPictures++;
                }
            }

            if (numberOfPictures > Constants.NumberOfMaxAllowedPictures)
            {
                if (showMessage)
                {
                    this.windowService.MessageDialogService.Show(string.Format(Thread.CurrentThread.CurrentUICulture, LocalizedResources.Media_ToManyPictures, Constants.NumberOfMaxAllowedPictures), MessageDialogType.Info);
                }

                return false;
            }

            if (numberOfPictures > 0 && numberOfVideos > 0)
            {
                if (showMessage)
                {
                    this.windowService.MessageDialogService.Show(LocalizedResources.Media_VideosAndPicturesNotAllowed, MessageDialogType.Info);
                }

                return false;
            }

            if (numberOfVideos > 1)
            {
                if (showMessage)
                {
                    this.windowService.MessageDialogService.Show(LocalizedResources.Media_ToManyVideos, MessageDialogType.Info);
                }

                return false;
            }

            if (numberOfPictures > 0 && this.picturesViewModel.Value.MediaSource.AllSources.Count + numberOfPictures > Constants.NumberOfMaxAllowedPictures)
            {
                if (showMessage)
                {
                    this.windowService.MessageDialogService.Show(string.Format(Thread.CurrentThread.CurrentUICulture, LocalizedResources.Media_ToManyPictures, Constants.NumberOfMaxAllowedPictures), MessageDialogType.Info);
                }

                return false;
            }

            return true;
        }

        /// <summary>
        /// Handles messages from the messenger service.
        /// </summary>
        /// <param name="message">The message.</param>
        private void HandleNotificationMessage(NotificationMessage message)
        {
            if (message.Notification == Notification.AddMedia)
            {
                if (AddMediaCommand.CanExecute(null))
                {
                    AddMediaCommand.Execute(null);
                }
            }
            else if (message.Notification == Notification.MediaDeleted)
            {
                var undoItem = message.Sender as MediaUndoableItem;
                if (undoItem != null)
                {
                    this.Push(undoItem);
                }

                if (this.MediaHandler is VideoViewModel)
                {
                    if (this.picturesViewModel.IsValueCreated)
                    {
                        this.picturesViewModel.Value.MediaSource.Reset();
                    }

                    this.MediaHandler = this.picturesViewModel.Value;
                    this.MediaHandler.DisplayMediaAtIndex(0);
                }

                HandleMediaDeletedMessage();
            }
        }

        /// <summary>
        /// Handles the media deleted message.
        /// </summary>
        private void HandleMediaDeletedMessage()
        {
            if (MediaHandler != null)
            {
                IsChanged = true;
                this.MediaHandler.DisplayMediaAtIndex(0);
                MediaChangedMessage msg = new MediaChangedMessage()
                {
                    Entity = this.Model,
                    Media = this.MediaHandler.MediaSource.AllSources.ToList(),
                    ChangeType = MediaChangeType.MediaDeleted
                };
                this.messenger.Send(msg);
            }
        }

        /// <summary>
        /// Checks media file paths if are supported by media using a list of paths
        /// </summary>
        /// <param name="sourcePaths">The path list</param>
        /// <returns>True if at least one file is supported or False if non of files are supported</returns>
        private bool CheckMediaFilePaths(IEnumerable<string> sourcePaths)
        {
            bool isValid = false;
            switch (Mode)
            {
                case MediaControlMode.SingleImage:
                    {
                        // find the first valid image and add it
                        foreach (var fileName in sourcePaths)
                        {
                            if (IsImageSupported(fileName))
                            {
                                isValid = true;
                                break;
                            }
                        }
                    }

                    break;

                case MediaControlMode.MultipleImagesOrVideo:
                case MediaControlMode.MultipleImagesNoVideo:
                    {
                        foreach (var fileName in sourcePaths)
                        {
                            if (CheckSelection(sourcePaths, false))
                            {
                                if (IsImageSupported(fileName))
                                {
                                    isValid = true;
                                    break;
                                }
                                else if (IsVideoSupported(fileName))
                                {
                                    if (this.Mode == MediaControlMode.MultipleImagesOrVideo)
                                    {
                                        isValid = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    break;
            }

            return isValid;
        }

        #endregion Other Methods

        #region Save/Cancel

        /// <summary>
        /// Performs the save to model operation. Executed by the SaveToModelCommand command.
        /// </summary>
        protected override void SaveToModel()
        {
            this.InternalSave(true);
        }

        /// <summary>
        /// Performs the save operation. Executed by the SaveCommand command.
        /// </summary>
        protected override void Save()
        {
            this.InternalSave(false);
        }

        /// <summary>
        /// The internal Save.
        /// </summary>
        /// <param name="onlySaveToModel">If set to true, only saves to model; otherwise also saves to data source.</param>
        private void InternalSave(bool onlySaveToModel)
        {
            if (!this.IsLoaded)
            {
                return;
            }

            base.SaveToModel();

            if (this.picturesViewModel.IsValueCreated)
            {
                foreach (var mediaToDelete in this.picturesViewModel.Value.MediaToBeDeleted)
                {
                    var mediaMetaData = mediaToDelete as MediaMetaData;
                    if (mediaMetaData != null)
                    {
                        this.RemoveMediaMetaDataFromUndoStack(mediaToDelete as MediaMetaData);
                    }

                    DeleteMedia(mediaToDelete);
                }

                this.picturesViewModel.Value.MediaToBeDeleted.Clear();
            }

            if (this.videoViewModel.IsValueCreated)
            {
                foreach (var mediaToDelete in this.videoViewModel.Value.MediaToBeDeleted)
                {
                    this.RemoveMediaMetaDataFromUndoStack(mediaToDelete as MediaMetaData);
                    DeleteMedia(mediaToDelete);
                }

                this.videoViewModel.Value.MediaToBeDeleted.Clear();
            }

            if (this.MediaHandler != null)
            {
                SaveMedia(this.MediaHandler.MediaToBeAdded, onlySaveToModel);
                this.MediaHandler.MediaToBeAdded.Clear();
            }
        }

        /// <summary>
        /// Cancels this instance.
        /// </summary>
        protected override void Cancel()
        {
            if (!this.IsLoaded)
            {
                return;
            }

            var cancelledMedia = new List<object>();
            if (this.MediaHandler != null)
            {
                cancelledMedia = this.MediaHandler.MediaSource.AllSources.ToList();
            }

            var undoCancelledMediaItem = this.GenerateUndoableItem(this.MediaHandler);

            base.Cancel();
            this.IsLoaded = false;

            var addedMedia = 0;
            MediaChangedMessage msg;

            if (this.picturesViewModel.IsValueCreated)
            {
                addedMedia += this.picturesViewModel.Value.MediaToBeAdded.Count();
                this.picturesViewModel.Value.MediaToBeAdded.Clear();
                this.picturesViewModel.Value.MediaToBeDeleted.Clear();
                this.picturesViewModel.Value.MediaSource.Reset();
            }

            if (this.videoViewModel.IsValueCreated)
            {
                addedMedia += this.videoViewModel.Value.MediaToBeAdded.Count();
                this.videoViewModel.Value.MediaToBeAdded.Clear();
                this.videoViewModel.Value.MediaToBeDeleted.Clear();
                this.videoViewModel.Value.MediaSource.Reset();
            }

            if (addedMedia > 0)
            {
                msg = new MediaChangedMessage()
                {
                    Entity = this.Model,
                    Media = new List<object>(),
                    ChangeType = MediaChangeType.MediaDeleted
                };
                this.messenger.Send(msg);
            }

            // todo: maybe this is too much to do here.
            this.LoadMediaFromModel(false);

            var loadedMedia = this.picturesViewModel.IsValueCreated ?
                this.picturesViewModel.Value.MediaSource.AllSources.ToList() : this.videoViewModel.IsValueCreated ?
                this.videoViewModel.Value.MediaSource.AllSources.ToList() : new List<object>();

            if (!this.AreEquals(cancelledMedia, loadedMedia))
            {
                this.Push(undoCancelledMediaItem);
            }

            if (loadedMedia.Any())
            {
                msg = new MediaChangedMessage()
                {
                    Entity = this.Model,
                    Media = loadedMedia,
                    ChangeType = MediaChangeType.MediaAdded
                };
                this.messenger.Send(msg);
            }
        }

        /// <summary>
        /// Called after the view has been unloaded from the parent or closed.
        /// </summary>
        public override void OnUnloaded()
        {
            base.OnUnloaded();
            this.messenger.Unregister(this);
        }

        #endregion Save/Cancel

        #region Helper

        /// <summary>
        /// Gets the media objects from the Model instance without going to the database.
        /// </summary>
        /// <returns>A list of media objects which are attached with the current Model.</returns>
        private IEnumerable<Media> GetMediaFromModel()
        {
            var entityWithMediaCollection = this.Model as IEntityWithMediaCollection;
            if (entityWithMediaCollection != null)
            {
                return entityWithMediaCollection.Media;
            }

            var entityWithMedia = this.Model as IEntityWithMedia;
            if (entityWithMedia != null && entityWithMedia.Media != null)
            {
                return new Collection<Media> { entityWithMedia.Media };
            }

            return new Collection<Media>();
        }

        /// <summary>
        /// Verifies if a file is a supported video file.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <returns>True if is supported, false otherwise.</returns>
        private bool IsVideoSupported(string filePath)
        {
            var extension = Path.GetExtension(filePath);

            return !string.IsNullOrWhiteSpace(extension) && SupportedVideosExtensions.Contains(extension.ToLower());
        }

        /// <summary>
        /// Verifies if a file is a supported image file
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <returns>True if image is supported, false otherwise.</returns>
        private bool IsImageSupported(string filePath)
        {
            var extension = Path.GetExtension(filePath);

            return !string.IsNullOrWhiteSpace(extension) && SupportedImagesExtensions.Contains(extension.ToLower());
        }

        /// <summary>
        /// Check if two lists that contains media objects are equal. 
        /// ( Media object can be : Media, MediaMetaData, string - media path )
        /// </summary>
        /// <param name="firstList">The first media list.</param>
        /// <param name="secondList">The second media list.</param>
        /// <returns>True - if the list are equals / False - otherwise.</returns>
        private bool AreEquals(List<object> firstList, List<object> secondList)
        {
            if (firstList.Count != secondList.Count)
            {
                return false;
            }

            while (firstList.Any())
            {
                var mediaObject = firstList[0];
                var media = mediaObject as Media;
                var metaData = mediaObject as MediaMetaData;
                var mediaPath = mediaObject as string;
                if (media != null)
                {
                    var secondListMedia =
                        secondList.FirstOrDefault(m => m as Media != null && (m as Media).Guid == media.Guid);
                    if (secondListMedia != null)
                    {
                        secondList.Remove(secondListMedia);
                    }
                }
                else if (metaData != null)
                {
                    var secondListMetaData =
                        secondList.FirstOrDefault(m => m as MediaMetaData != null && (m as MediaMetaData).Guid == metaData.Guid);
                    if (secondListMetaData != null)
                    {
                        secondList.Remove(secondListMetaData);
                    }
                }
                else if (!string.IsNullOrWhiteSpace(mediaPath))
                {
                    var secondListMediaPath =
                        secondList.FirstOrDefault(m => !string.IsNullOrWhiteSpace(m as string) && m as string == mediaPath);
                    if (secondListMediaPath != null)
                    {
                        secondList.Remove(secondListMediaPath);
                    }
                }

                firstList.Remove(mediaObject);
            }

            return !firstList.Any() && !secondList.Any();
        }

        /// <summary>
        /// Handles the PropertyChanged event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void OnPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == ReflectionUtils.GetPropertyName(() => this.IsLoaded))
            {
                this.SetLogoDisplayState();
            }
        }

        /// <summary>
        /// Handles the MediaHandler PropertyChanged event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void OnMediaHandlerPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (this.MediaHandler != null &&
                e.PropertyName == ReflectionUtils.GetPropertyName(() => this.MediaHandler.IsEmpty))
            {
                this.SetLogoDisplayState();
            }
        }

        /// <summary>
        /// Determines whether the application logo should be displayed or not.
        /// </summary>
        private void SetLogoDisplayState()
        {
            this.DisplayLogo = this.IsLoaded &&
                               (this.MediaHandler == null || (this.MediaHandler != null && this.MediaHandler.IsEmpty));
        }

        #endregion Helper

        #region Undo Manager

        /// <summary>
        /// Actions can only be undone if there are items in the <see cref="undoStack"/>.
        /// </summary>
        /// <returns>True - if the action can be undone / False - otherwise.</returns>
        bool IUndoManager.CanUndo()
        {
            return this.undoStack.Count > 0;
        }

        /// <summary>
        /// Undo the last view-model change.
        /// </summary>
        void IUndoManager.Undo()
        {
            this.RefreshMedia(this.undoStack.Last());
            this.Pop();
            this.IsChanged = true;
        }

        /// <summary>
        /// Clear all items from the undo list.
        /// </summary>
        public void Reset()
        {
            this.undoStack.Clear();
        }

        /// <summary>
        /// Inserts an object at the top of the undoStack.
        /// </summary>
        /// <param name="item">The undoable item added.</param>
        private void Push(MediaUndoableItem item)
        {
            this.undoStack.Add(item);
            this.UndoManager.Push(new UndoableItem() { Instance = this, ActionType = UndoActionType.UndoManager, ViewModelInstance = this });
        }

        /// <summary>
        /// Removes the object at the top of the Stack.
        /// </summary>
        private void Pop()
        {
            this.undoStack.RemoveAt(undoStack.Count - 1);
        }

        /// <summary>
        /// Load media corresponding to the media undoable item passed as parameter.
        /// </summary>
        /// <param name="item">The media undoable item.</param>
        private void RefreshMedia(MediaUndoableItem item)
        {
            this.ClearMedia();

            if (item.AllSources == null)
            {
                return;
            }

            // Remove the media paths for which the corresponding images on disk does not exists anymore.
            var unreachablePaths = 0;
            if (item.MediaPaths != null)
            {
                unreachablePaths = item.MediaPaths.Count(path => !File.Exists(path));
                item.MediaPaths.RemoveAll(path => !File.Exists(path));
                item.AllSources.RemoveAll(source => source as string != null && !File.Exists(source as string));
                item.MediaToBeAdded.RemoveAll(source => source as string != null && !File.Exists(source as string));
            }

            if (item.AllSources.Count == 0)
            {
                if (unreachablePaths > 0)
                {
                    this.windowService.MessageDialogService.Show(LocalizedResources.Media_UndoMediaErrorMessage, MessageDialogType.Error);
                    return;
                }

                this.MediaHandler = this.picturesViewModel.Value;
                this.MediaHandler.DisplayMediaAtIndex(0);
            }
            else if (item.AllSources.Count > 1)
            {
                if (unreachablePaths > 0)
                {
                    this.windowService.MessageDialogService.Show(LocalizedResources.Media_UndoImagesErrorMessage, MessageDialogType.Error);
                }

                this.MediaHandler = this.picturesViewModel.Value;
                this.MediaHandler.MediaSource.AllSources.AddRange(item.AllSources);
                this.MediaHandler.TogglePreviewMode(true);
            }
            else
            {
                if (unreachablePaths > 0)
                {
                    this.windowService.MessageDialogService.Show(LocalizedResources.Media_UndoImagesErrorMessage, MessageDialogType.Error);
                }

                var mediaSource = item.AllSources[0];
                var media = mediaSource as Media;
                var metaData = mediaSource as MediaMetaData;
                if ((media != null && media.Type == MediaType.Video)
                    || (metaData != null && metaData.Type == MediaType.Video)
                    || this.IsVideoSupported(mediaSource as string))
                {
                    this.MediaHandler = this.videoViewModel.Value;
                    this.MediaHandler.MediaSource.AllSources.AddRange(item.AllSources);
                    this.videoViewModel.Value.RequestVideoSourceCommand.Execute(null);
                }

                if ((media != null && media.Type == (short)MediaType.Image)
                    || this.IsImageSupported(mediaSource as string))
                {
                    this.MediaHandler = this.picturesViewModel.Value;
                    this.MediaHandler.MediaSource.AllSources.AddRange(item.AllSources);
                    this.MediaHandler.DisplayMediaAtIndex(0);
                }
            }

            if (item.MediaToBeAdded != null && item.MediaToBeDeleted != null)
            {
                this.MediaHandler.MediaSource.PauseAllSourcesAutoUpdate();
                this.MediaHandler.MediaToBeAdded.Clear();
                this.MediaHandler.MediaToBeDeleted.Clear();
                this.MediaHandler.MediaSource.MediaMetaData.Clear();
                this.MediaHandler.MediaSource.Media.AddRange(item.Media);
                this.MediaHandler.MediaSource.MediaMetaData.AddRange(item.MediaMetaData);
                this.MediaHandler.MediaSource.MediaPaths.AddRange(item.MediaPaths);
                this.MediaHandler.MediaToBeAdded.AddRange(item.MediaToBeAdded);
                this.MediaHandler.MediaToBeDeleted.AddRange(item.MediaToBeDeleted);
                this.MediaHandler.MediaSource.ResumeAllSourcesAutoUpdate();
            }

            var msg = new MediaChangedMessage()
            {
                Entity = this.Model,
                Media = this.MediaHandler.MediaSource.AllSources.ToList(),
                ChangeType = MediaChangeType.MediaUpdated
            };
            this.messenger.Send(msg);
        }

        /// <summary>
        /// Update the undo stack after a media source is added to the model.
        /// </summary>
        /// <param name="filePath">The media file path, if exists.</param>
        /// <param name="media">The media object.</param>
        /// <param name="mediaMetaData">The media meta data.</param>
        private void UpdateAddedMediaInUndoStack(string filePath, Media media, MediaMetaData mediaMetaData)
        {
            var mediaHandling = this.GetAvailableMediaHandling((MediaType)mediaMetaData.Type);
            if (mediaHandling != null)
            {
                mediaHandling.MediaSource.PauseAllSourcesAutoUpdate();
            }

            foreach (var item in this.undoStack)
            {
                // Replace old source path with the media object stored in DB.
                var updatedItemIndex = item.AllSources.IndexOf(filePath);
                if (updatedItemIndex != -1)
                {
                    item.AllSources.RemoveAt(updatedItemIndex);
                    item.AllSources.Insert(updatedItemIndex, media);

                    item.MediaPaths.Remove(filePath);
                    item.Media.Add(media);
                    item.MediaMetaData.Add(mediaMetaData);
                }
                else
                {
                    updatedItemIndex = item.AllSources.IndexOf(media);
                    if (updatedItemIndex != -1)
                    {
                        item.MediaMetaData.Add(mediaMetaData);
                    }
                    else
                    {
                        // If the media source does not exist, it should be marked for deletion.
                        item.MediaToBeDeleted.Add(mediaMetaData);
                    }
                }

                item.MediaToBeAdded.Remove(filePath);
            }

            if (mediaHandling != null)
            {
                mediaHandling.MediaSource.ResumeAllSourcesAutoUpdate();
            }
        }

        /// <summary>
        /// Remove a mediaMetaData object from the undo stack.
        /// </summary>
        /// <param name="mediaMetaData">The media meta data.</param>
        private void RemoveMediaMetaDataFromUndoStack(MediaMetaData mediaMetaData)
        {
            var mediaHandling = this.GetAvailableMediaHandling((MediaType)mediaMetaData.Type);
            if (mediaHandling != null)
            {
                mediaHandling.MediaSource.PauseAllSourcesAutoUpdate();
            }

            foreach (var item in this.undoStack)
            {
                item.AllSources.RemoveAll(p => (p as Media != null) && ((p as Media).Guid == mediaMetaData.Guid));
                item.AllSources.RemoveAll(p => (p as MediaMetaData != null) && ((p as MediaMetaData).Guid == mediaMetaData.Guid));
                item.Media.RemoveAll(p => (p != null) && (p.Guid == mediaMetaData.Guid));
                item.MediaMetaData.RemoveAll(p => p.Guid == mediaMetaData.Guid);

                item.MediaToBeDeleted.RemoveAll(p => (p as MediaMetaData != null) && ((p as MediaMetaData).Guid == mediaMetaData.Guid));
            }

            if (mediaHandling != null)
            {
                mediaHandling.MediaSource.ResumeAllSourcesAutoUpdate();
            }
        }

        /// <summary>
        /// Generate a new media undoable item for media instance passed as parameter.
        /// </summary>
        /// <param name="mediaInstance">The MediaHandling view-model instance.</param>
        /// <returns>The created undoable item.</returns>
        private MediaUndoableItem GenerateUndoableItem(MediaHandlingViewModelBase mediaInstance)
        {
            var undoableItem = new MediaUndoableItem();
            if (mediaInstance != null && mediaInstance.MediaSource.AllSources.Count > 0)
            {
                undoableItem.AllSources = mediaInstance.MediaSource.AllSources.ToList();
                undoableItem.Media = mediaInstance.MediaSource.Media.ToList();
                undoableItem.MediaMetaData = mediaInstance.MediaSource.MediaMetaData.ToList();
                undoableItem.MediaPaths = mediaInstance.MediaSource.MediaPaths.ToList();
                undoableItem.MediaToBeAdded = mediaInstance.MediaToBeAdded.ToList();
                undoableItem.MediaToBeDeleted = mediaInstance.MediaToBeDeleted.ToList();
            }
            else
            {
                undoableItem.AllSources = new List<object>();
                undoableItem.MediaToBeAdded = new List<object>();
                undoableItem.MediaToBeDeleted = new List<object>();
                undoableItem.Media = new List<Media>();
                undoableItem.MediaMetaData = new List<MediaMetaData>();
                undoableItem.MediaPaths = new List<string>();
            }

            return undoableItem;
        }

        /// <summary>
        /// Get the used MediaHandlingViewModelBase.
        /// </summary>
        /// <param name="currentMediaType">Existing media type.</param>
        /// <returns>The available MediaHandlingViewModelBase instance.</returns>
        private MediaHandlingViewModelBase GetAvailableMediaHandling(MediaType currentMediaType)
        {
            if (this.MediaHandler != null)
            {
                return this.MediaHandler;
            }

            if (currentMediaType == MediaType.Image && this.picturesViewModel.IsValueCreated)
            {
                return this.picturesViewModel.Value;
            }

            if (currentMediaType == MediaType.Video && this.videoViewModel.IsValueCreated)
            {
                return this.videoViewModel.Value;
            }

            return null;
        }

        #endregion Undo Manager
    }
}