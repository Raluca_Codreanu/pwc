﻿namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The types of media controls available
    /// </summary>
    public enum MediaControlMode
    {
        /// <summary>
        /// The controls accepts only 1 image.
        /// </summary>
        SingleImage = 0,

        /// <summary>
        /// The control accepts multiple images or 1 video.
        /// </summary>
        MultipleImagesOrVideo = 1,

        /// <summary>
        /// The control accepts multiple images but no video.
        /// </summary>
        MultipleImagesNoVideo
    }
}
