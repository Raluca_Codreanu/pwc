﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Contains the data needed to perform an undo operation in Media View-Model.
    /// </summary>
    public class MediaUndoableItem
    {
        /// <summary>
        /// Gets or sets all source objects.
        /// </summary>
        public List<object> AllSources { get; set; }

        /// <summary>
        /// Gets or sets the media sources.
        /// </summary>
        public List<Media> Media { get; set; }

        /// <summary>
        /// Gets or sets the media meta data sources.
        /// </summary>
        public List<MediaMetaData> MediaMetaData { get; set; }

        /// <summary>
        /// Gets or sets the media path sources.
        /// </summary>
        public List<string> MediaPaths { get; set; }

        /// <summary>
        /// Gets or sets the media to be added.
        /// The collection may contain strings, representing media's paths or media objects.
        /// </summary>
        public List<object> MediaToBeAdded { get; set; }

        /// <summary>
        /// Gets or sets the media to be deleted.
        /// </summary>
        public List<object> MediaToBeDeleted { get; set; }
    }
}
