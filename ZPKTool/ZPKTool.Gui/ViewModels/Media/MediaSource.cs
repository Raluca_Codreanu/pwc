﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using ZPKTool.Data;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Contains the media sources for the video and pictures view-models. The sources can be a combination of Media, MediaMetaData and paths.
    /// </summary>
    public class MediaSource
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        protected static readonly NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// A value indicating whether the AllSources collection is updated when another MediaSource collection changes.
        /// </summary>
        private bool allowAllSourcesAutoUpdate = true;

        /// <summary>
        /// Initializes a new instance of the <see cref="MediaSource"/> class.
        /// </summary>
        public MediaSource()
        {
            this.Media = new ObservableCollection<Media>();
            this.Media.CollectionChanged += OnCollectionChanged;
            this.MediaMetaData = new ObservableCollection<MediaMetaData>();
            this.MediaMetaData.CollectionChanged += OnCollectionChanged;
            this.MediaPaths = new ObservableCollection<string>();
            this.MediaPaths.CollectionChanged += OnCollectionChanged;
            this.AllSources = new ObservableCollection<object>();
        }

        /// <summary>
        /// Gets the media sources.
        /// </summary>
        public ObservableCollection<Media> Media { get; private set; }

        /// <summary>
        /// Gets the media meta data sources.
        /// </summary>
        public ObservableCollection<MediaMetaData> MediaMetaData { get; private set; }

        /// <summary>
        /// Gets the media path sources.
        /// </summary>
        public ObservableCollection<string> MediaPaths { get; private set; }

        /// <summary>
        /// Gets all source objects.
        /// </summary>
        public ObservableCollection<object> AllSources { get; private set; }

        /// <summary>
        /// Resets this instance.
        /// </summary>
        public void Reset()
        {
            this.MediaMetaData.Clear();
            this.Media.Clear();
            this.MediaPaths.Clear();
            this.AllSources.Clear();
        }

        /// <summary>
        /// Block the AllSources collection update, when another MediaSource collection changes.
        /// </summary>
        public void PauseAllSourcesAutoUpdate()
        {
            this.allowAllSourcesAutoUpdate = false;
        }

        /// <summary>
        /// Allow the AllSources collection update, when another MediaSource collection changes.
        /// </summary>
        public void ResumeAllSourcesAutoUpdate()
        {
            this.allowAllSourcesAutoUpdate = true;
        }

        /// <summary>
        /// Check if current media is obtained from MediaMetaData.
        /// </summary>
        /// <param name="media">Current media entity.</param>
        /// <param name="mediaMetaData">The collection of MetaData.</param>
        /// <returns>True - if media is obtained from MediaMetaData / False - otherwise.</returns>
        public bool IsMediaFromMetaData(Media media, Collection<MediaMetaData> mediaMetaData)
        {
            foreach (var item in mediaMetaData)
            {
                if (item.Guid == media.Guid)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Called when [collection changed].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="NotifyCollectionChangedEventArgs" /> instance containing the event data.</param>
        private void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (!this.allowAllSourcesAutoUpdate)
            {
                return;
            }

            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (var newItem in e.NewItems)
                    {
                        var newMediaMetaData = newItem as MediaMetaData;
                        if (newMediaMetaData != null)
                        {
                            if (!this.CheckMediaExists((newItem as MediaMetaData).Guid, AllSources))
                            {
                                AllSources.Add(newItem);
                            }
                        }
                        else
                        {
                            AllSources.Add(newItem);
                        }
                    }

                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (var oldItem in e.OldItems)
                    {
                        AllSources.Remove(oldItem);
                    }

                    break;

                case NotifyCollectionChangedAction.Replace:
                case NotifyCollectionChangedAction.Move:
                    Log.Error("Replace and Move logic not implemented in the MediaSource - OnCollectionChanged");
                    break;

                case NotifyCollectionChangedAction.Reset:
                    if (e.OldItems != null)
                    {
                        foreach (var oldItem in e.OldItems)
                        {
                            AllSources.Remove(oldItem);
                        }
                    }

                    break;
            }
        }

        /// <summary>
        /// Check if exists in all sources a media with guid passed as parameter.
        /// </summary>
        /// <param name="guid">Media guid.</param>
        /// <param name="sources">MediaViewModel sources.</param>
        /// <returns>True if a media with the given Guid exists / False - otherwise.</returns>
        private bool CheckMediaExists(Guid guid, Collection<object> sources)
        {
            foreach (var source in AllSources)
            {
                var mediaSource = source as Media;
                if (mediaSource != null)
                {
                    if (guid == mediaSource.Guid)
                    {
                        return true;
                    }
                }

                var mediaMetaDataSource = source as MediaMetaData;
                if (mediaMetaDataSource != null)
                {
                    if (guid == mediaMetaDataSource.Guid)
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}