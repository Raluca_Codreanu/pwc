﻿using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Resources;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for editing and viewing currency.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class CurrencyViewModel : ViewModel<Currency, IDataSourceManager>
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        #endregion Attributes

        /// <summary>
        /// Initializes a new instance of the <see cref="CurrencyViewModel"/> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        [ImportingConstructor]
        public CurrencyViewModel(IWindowService windowService)
        {
            Argument.IsNotNull("windowService", windowService);

            this.windowService = windowService;
            this.ShowCancelMessageFlag = true;
        }

        #region Commands
        #endregion Commands

        #region Model related properties

        /// <summary>
        /// Gets the name of the currency.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Name", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Name")]
        public DataProperty<string> CurrencyName { get; private set; }

        /// <summary>
        /// Gets the symbol of the currency.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Symbol", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Symbol")]
        public DataProperty<string> CurrencySymbol { get; private set; }

        /// <summary>
        /// Gets the exchange rate of the currency.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_ExchangeRate", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("ExchangeRate")]
        public DataProperty<decimal?> CurrencyExchangeRate { get; private set; }

        /// <summary>
        /// Gets the ISO code of the currency.
        /// </summary>
        [CurrencyIsoCode(ErrorMessageResourceName = "Currency_IsoCode_InvalidFieldValue", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("IsoCode")]
        public DataProperty<string> IsoCode { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether the show cancel message will appear
        /// </summary>
        public bool ShowCancelMessageFlag { get; set; }

        #endregion Model related properties

        #region Property change handlers

        /// <summary>
        /// Called when the Model has changed.
        /// </summary>
        protected override void OnModelChanged()
        {
            this.CheckDataSource();
            base.OnModelChanged();
            if (this.EditMode == ViewModelEditMode.Create)
            {
                this.CurrencyExchangeRate.Value = null;
                this.CurrencyExchangeRate.AcceptChanges();
                this.IsChanged = false;
            }

            this.UndoManager.Start();
            this.UndoManager.Reset();
        }

        #endregion Property change handlers

        #region Save/Cancel

        /// <summary>
        /// Performs the save operation. Executed by the SaveCommand.
        /// </summary>
        protected override void Save()
        {
            this.CheckModelAndDataSource();

            // Save all changes back into the Model objects. The validity check of this operation is performed by the CanSave method.
            this.SaveToModel();

            if (this.SavesToDataSource)
            {
                // Update currency
                this.DataSourceManager.CurrencyRepository.Save(this.Model);
                this.DataSourceManager.SaveChanges();
            }

            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Cancels all changes. Executed by the CancelCommand.
        /// </summary>
        protected override void Cancel()
        {
            if (!this.CanCancel())
            {
                return;
            }

            if (this.IsChanged && this.ShowCancelMessageFlag)
            {
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_CancelChanges, MessageDialogType.YesNo);
                if (result != MessageDialogResult.Yes)
                {
                    // Don't cancel the changes and also don't close the view-model.
                    return;
                }
            }

            // Cancel all changes
            base.Cancel();

            // Close the view-model when is displayed in a window.
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Called before unloading the view from its parent. Returning false will cancel the view's unloading.
        /// </summary>
        /// <returns>
        /// True if the unloading process should continue and false if it should be canceled.
        /// </returns>
        public override bool OnUnloading()
        {
            // Do nothing if the view-model is read-only, in viewer mode or it was not changed.
            if (this.IsReadOnly || this.IsInViewerMode || !this.IsChanged)
            {
                return true;
            }

            if (this.EditMode == ViewModelEditMode.Create)
            {
                // Ask the user to confirm quitting
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_Quit, MessageDialogType.YesNo);
                if (result != MessageDialogResult.Yes)
                {
                    // The user chose to stay on the screen; return false to stop the screen unloading.
                    return false;
                }
                else
                {
                    this.IsChanged = false;
                }
            }
            else if (this.EditMode == ViewModelEditMode.Edit)
            {
                // Ask the user if he wants to save
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_UnsavedDataOnQuit, MessageDialogType.YesNoCancel);
                if (result == MessageDialogResult.Yes)
                {
                    // The user whishes to save but it is not possible because the input is not valid. Return false to stop the view-model from unloading.
                    if (!this.CanSave())
                    {
                        return false;
                    }

                    this.Save();
                }
                else if (result == MessageDialogResult.No)
                {
                    // The user does not want to save.                    
                    this.IsChanged = false;
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        #endregion Save/Cancel

        /// <summary>
        /// Attribute that validates a currency ISO code.
        /// </summary>
        public class CurrencyIsoCodeAttribute : ValidationAttribute
        {
            /// <summary>
            /// Validates the specified value with respect to the current validation attribute.
            /// </summary>
            /// <param name="value">The value to validate.</param>
            /// <param name="validationContext">The context information about the validation operation.</param>
            /// <returns>
            /// An instance of the <see cref="T:System.ComponentModel.DataAnnotations.ValidationResult" /> class.
            /// </returns>
            protected override ValidationResult IsValid(object value, ValidationContext validationContext)
            {
                var isoCode = value as string;
                if (isoCode != null && isoCode.Length >= 3 && isoCode.Length <= 6 && Regex.IsMatch(isoCode, @"^[a-zA-Z0-9]+$"))
                {
                    return ValidationResult.Success;
                }

                return new ValidationResult(this.ErrorMessageString);
            }
        }
    }
}
