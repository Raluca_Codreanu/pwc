﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Represents the data source for an item representing a project in the Projects Tree.
    /// </summary>
    public class ProjectTreeItem : TreeViewDataItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectTreeItem"/> class.
        /// </summary>
        /// <param name="project">The project backing the item.</param>
        /// <param name="dataContext">The data context from which the project comes from.</param>
        /// <param name="dataService">The data service.</param>
        public ProjectTreeItem(Project project, DbIdentifier dataContext, IProjectsExplorerDataService dataService)
            : base(dataService)
        {
            if (project == null)
            {
                throw new ArgumentNullException("project", "The project was null.");
            }

            this.DataObjectSource = dataContext;
            this.DataObject = project;
            this.IconResourceKey = Images.ProjectIconKey;
            this.AllowChildrenMultipleSelection = true;
            this.AllowMultipleSelection = true;

            if (dataContext == DbIdentifier.NotSet)
            {
                this.LazyLoad = TreeViewItemLazyLoadMode.None;
                this.AreChildrenLoaded = true;
                this.Refresh();
                this.ReadOnly = true;
            }
            else
            {
                this.RefreshLook();

                // Determine whether the Release, Un-release or none of those options should be displayed in the item's context menu.
                this.ShowReleaseOrUnreleaseContextMenuItem = null;
                if (project.IsReleased)
                {
                    // Only admin can un-release a project.
                    if (ZPKTool.Business.SecurityManager.Instance.CurrentUserHasRight(Right.AdminTasks))
                    {
                        this.ShowReleaseOrUnreleaseContextMenuItem = false;
                    }
                }
                else
                {
                    // Enabled Release Project for admin and user with release project right.
                    if (ZPKTool.Business.SecurityManager.Instance.CurrentUserHasRight(Right.AdminTasks) || ZPKTool.Business.SecurityManager.Instance.CurrentUserHasRight(Right.ReleaseProject))
                    {
                        this.ShowReleaseOrUnreleaseContextMenuItem = true;
                    }
                }

                this.AreChildrenLoaded = false;
                this.LazyLoad = TreeViewItemLazyLoadMode.OnSelectionOrExpansion;
            }
        }

        /// <summary>
        /// Gets a value indicating to show in this item's context menu the Release option (set it to true), Un-release option (set it to false)
        /// or none of the two options (set it to null).
        /// </summary>
        public bool? ShowReleaseOrUnreleaseContextMenuItem { get; private set; }

        /// <summary>
        /// Refreshes this item from the underlying data object, including its children collection.
        /// </summary>
        protected override void RefreshInternal()
        {
            this.RefreshLook();
            this.RefreshChildren(this.Children);
        }

        /// <summary>
        /// Implements the children loading logic for items that support lazy loading.
        /// This method is executed asynchronously so it should not manipulate UI elements.
        /// </summary>
        /// <returns>
        /// The loaded children.
        /// </returns>
        public override ICollection<TreeViewDataItem> LoadChildren()
        {
            var backingDataProject = this.DataObject as Project;
            if (backingDataProject == null)
            {
                return null;
            }

            var itemData = this.DataService.GetProject(backingDataProject.Guid, this.DataObjectSource);
            this.DataObject = itemData.Data.FirstOrDefault();
            this.DataObjectContext = itemData.DataManager;

            var children = new Collection<TreeViewDataItem>();
            this.RefreshChildren(children);

            return children;
        }

        /// <summary>
        /// Refreshes the item's look (label, icon(s), etc.)
        /// </summary>
        private void RefreshLook()
        {
            Project project = (Project)this.DataObject;
            this.Label = project.Name + (!string.IsNullOrEmpty(project.Number) ? " (" + project.Number + ")" : string.Empty);
            this.BottomRightIcon = project.IsOffline ? Images.OfflineItemIcon : null;
            this.BottomRightIconTooltip = project.IsOffline ? LocalizedResources.Synchronization_OfflineProjectItemToolTip : null;
        }

        /// <summary>
        /// Refreshes the children collection from the underlying data object.
        /// Can be used to both refresh and create the children collection.
        /// </summary>
        /// <param name="children">The children collection to refresh.</param>
        private void RefreshChildren(Collection<TreeViewDataItem> children)
        {
            var underlyingProject = (Project)this.DataObject;

            var lostAssemblies = from child in children
                                 where child.DataObject is Assembly
                                    && underlyingProject.Assemblies
                                        .FirstOrDefault(a => !a.IsDeleted && a.Guid == ((Assembly)child.DataObject).Guid) == null
                                 select child;

            foreach (var assyItem in lostAssemblies.ToList())
            {
                children.Remove(assyItem);
            }

            var lostParts = from child in children
                            where child.DataObject is Part
                                 && underlyingProject.Parts.FirstOrDefault(p => !p.IsDeleted && p.Guid == ((Part)child.DataObject).Guid) == null
                            select child;

            foreach (var project in lostParts.ToList())
            {
                children.Remove(project);
            }

            var newAssemblies = underlyingProject.Assemblies.Where(a => !a.IsDeleted && !this.HasChildForObject(a));

            foreach (var assy in newAssemblies)
            {
                var item = new AssemblyTreeItem(assy, this.DataObjectSource, true, this.DataService);
                item.ReadOnly = this.ReadOnly;
                item.GroupId = 0;
                children.Add(item);
            }

            var newParts = underlyingProject.Parts.Where(p => !p.IsDeleted && !this.HasChildForObject(p));

            foreach (var part in newParts)
            {
                var item = new PartTreeItem(part, this.DataObjectSource, true, this.DataService);
                item.ReadOnly = this.ReadOnly;
                item.GroupId = 1;
                children.Add(item);
            }
        }
    }
}