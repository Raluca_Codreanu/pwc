﻿namespace ZPKTool.Gui.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using ZPKTool.Data;
    using ZPKTool.Gui.Controls;
    using ZPKTool.Gui.Resources;

    /// <summary>
    /// The data source for an item representing a Raw Material in the Projects Tree.
    /// </summary>
    public class RawMaterialTreeItem : TreeViewDataItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RawMaterialTreeItem"/> class.
        /// </summary>
        /// <param name="rawMaterial">The raw material.</param>
        /// <param name="dataContext">The material's data context.</param>        
        public RawMaterialTreeItem(RawMaterial rawMaterial, ZPKTool.DataAccess.DbIdentifier dataContext)
        {
            if (rawMaterial == null)
            {
                throw new ArgumentNullException("rawMaterial", "The raw material was null.");
            }

            this.DataObjectSource = dataContext;
            this.DataObject = rawMaterial;
            this.IconResourceKey = Images.RawMaterialIconKey;
            this.AllowMultipleSelection = true;
            this.Label = rawMaterial.Name;

            if (dataContext == ZPKTool.DataAccess.DbIdentifier.NotSet)
            {
                ReadOnly = true;
            }
        }
    }
}
