﻿using System;
using ZPKTool.Data;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Resources;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The machine tree item.
    /// </summary>
    public class DieTreeItem : TreeViewDataItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DieTreeItem"/> class.
        /// </summary>
        /// <param name="die">The die.</param>
        /// <param name="dataContext">The die's data context.</param>
        public DieTreeItem(Die die, ZPKTool.DataAccess.DbIdentifier dataContext)
        {
            if (die == null)
            {
                throw new ArgumentNullException("die", "The die was null.");
            }

            this.DataObjectSource = dataContext;
            this.DataObject = die;
            this.IconResourceKey = Images.DieIconKey;
            this.TopRightIcon = Images.IsExternalIcon;

            if (dataContext == ZPKTool.DataAccess.DbIdentifier.NotSet)
            {
                ReadOnly = true;
            }

            this.Refresh();
        }
    }
}