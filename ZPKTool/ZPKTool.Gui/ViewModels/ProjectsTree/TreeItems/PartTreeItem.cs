﻿namespace ZPKTool.Gui.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using ZPKTool.Common;
    using ZPKTool.Data;
    using ZPKTool.DataAccess;
    using ZPKTool.Gui.Controls;
    using ZPKTool.Gui.Resources;
    using ZPKTool.Gui.Services;

    /// <summary>
    /// Represents the data source for an item representing a Part in the Projects Tree.
    /// </summary>
    public class PartTreeItem : TreeViewDataItem
    {
        /// <summary>
        /// The number of parts of the current data object used in his parent
        /// </summary>
        private int? numberOfPartsUsedInParent;

        /// <summary>
        /// Initializes a new instance of the <see cref="PartTreeItem" /> class.
        /// </summary>
        /// <param name="part">The part.</param>
        /// <param name="dataContext">The part's data context.</param>
        /// <param name="lazyLoadChildren">if set to true, the item's children are loaded only when the item is selected or expanded.</param>
        /// <param name="dataService">The data service.</param>
        /// <exception cref="System.ArgumentNullException">part;The part was null.</exception>
        public PartTreeItem(Part part, ZPKTool.DataAccess.DbIdentifier dataContext, bool lazyLoadChildren, IProjectsExplorerDataService dataService)
            : base(dataService)
        {
            if (part == null)
            {
                throw new ArgumentNullException("part", "The part was null.");
            }

            this.DataObjectSource = dataContext;
            this.DataObject = part;
            this.IconResourceKey = Images.PartIconKey;
            this.AllowMultipleSelection = true;
            this.SortDescriptions.Clear();

            if (dataContext == ZPKTool.DataAccess.DbIdentifier.NotSet)
            {
                this.LazyLoad = TreeViewItemLazyLoadMode.None;
                this.AreChildrenLoaded = true;
                this.AutoReleaseUnderlyingData = false;
                this.Refresh();
                this.ReadOnly = true;
            }
            else
            {
                if (lazyLoadChildren)
                {
                    this.AreChildrenLoaded = false;
                    this.LazyLoad = TreeViewItemLazyLoadMode.OnSelectionOrExpansion;
                    this.AutoReleaseUnderlyingData = true;

                    this.IsDataObjectReleased = true;
                }

                this.Refresh();
            }
        }

        /// <summary>
        /// Gets or sets the number of parts of the current data object used in his parent
        /// </summary>
        public int? NumberOfPartsUsedInParent
        {
            get { return this.numberOfPartsUsedInParent; }
            set { this.SetProperty(ref this.numberOfPartsUsedInParent, value, () => this.NumberOfPartsUsedInParent); }
        }

        /// <summary>
        /// Refreshes this item from the underlying data object, including its children collection.
        /// </summary>
        protected override void RefreshInternal()
        {
            this.RefreshLook();
            this.RefreshChildren(this.Children);
        }

        /// <summary>
        /// Refreshes the item's look (label, icon(s), etc.)
        /// </summary>
        public void RefreshLook()
        {
            Part part = (Part)this.DataObject;

            this.OrderId = part.Index ?? -1;
            this.Label = part.Name + (!string.IsNullOrWhiteSpace(part.Number) ? " (" + part.Number + ")" : string.Empty);
            this.TopRightIcon = part.IsExternal == true ? Images.IsExternalIcon : null;

            var accuracy = EntityUtils.ConvertToEnum(part.CalculationAccuracy, PartCalculationAccuracy.FineCalculation);
            switch (accuracy)
            {
                case PartCalculationAccuracy.Estimation:
                    this.BottomRightIcon = Images.EstimationIcon;
                    this.BottomRightIconTooltip = LocalizedResources.PartCalculationAccuracy_Estimation_Tooltip;
                    break;
                case PartCalculationAccuracy.RoughCalculation:
                    this.BottomRightIcon = Images.RoughCalculationIcon;
                    this.BottomRightIconTooltip = LocalizedResources.PartCalculationAccuracy_RoughCalculation_Tooltip;
                    break;
                case PartCalculationAccuracy.OfferOrExternalCalculation:
                    this.BottomRightIcon = Images.OfferExternalCalculationIcon;
                    this.BottomRightIconTooltip = LocalizedResources.PartCalculationAccuracy_OfferOrExternalCalculation_Tooltip;
                    break;
                default:
                    this.BottomRightIcon = null;
                    this.BottomRightIconTooltip = null;
                    break;
            }
        }

        /// <summary>
        /// Implements the children loading logic for items that support lazy loading.
        /// This method is executed asynchronously so it should not manipulate UI elements.
        /// </summary>
        /// <returns>
        /// The loaded children.
        /// </returns>
        public override ICollection<TreeViewDataItem> LoadChildren()
        {
            var backingDataPart = this.DataObject as Part;
            if (backingDataPart == null)
            {
                return null;
            }

            if (this.IsDataObjectReleased)
            {
                var itemData = this.DataService.GetPart(backingDataPart.Guid, this.DataObjectSource);
                this.DataObject = itemData.Data.FirstOrDefault();
                this.DataObjectContext = itemData.DataManager;

                // Set the data object as not released.
                this.IsDataObjectReleased = false;
            }

            var children = new Collection<TreeViewDataItem>();
            this.RefreshChildren(children);

            return children;
        }

        /// <summary>
        /// Refreshes the children collection from the underlying data object.
        /// Can be used to both refresh and create the children collection.
        /// </summary>
        /// <param name="children">The children collection to refresh.</param>
        private void RefreshChildren(Collection<TreeViewDataItem> children)
        {
            var part = this.DataObject as Part;
            if (part == null)
            {
                return;
            }

            // Determine whether to show the Materials and Process children based on the current calculation accuracy.            
            var accuracy = (PartCalculationAccuracy)part.CalculationAccuracy.GetValueOrDefault(PartCalculationAccuracy.FineCalculation);
            if (accuracy == PartCalculationAccuracy.FineCalculation)
            {
                // Display all children.
                // Obtain or create, if necessary, the Material child
                var materialsItem = (MaterialsTreeItem)children.FirstOrDefault(c => c is MaterialsTreeItem);
                if (materialsItem == null)
                {
                    materialsItem = new MaterialsTreeItem();
                    materialsItem.ReadOnly = this.ReadOnly;
                    children.Insert(0, materialsItem);
                }

                // Determine the raw materials and commodities that are no longer in the underlying Part and remove their items.
                var lostRawMaterials = from child in materialsItem.Children
                                       where child.DataObject is RawMaterial
                                          && part.RawMaterials
                                              .FirstOrDefault(mat => !mat.IsDeleted && mat.Guid == ((RawMaterial)child.DataObject).Guid) == null
                                       select child;
                foreach (var rawMaterialItem in lostRawMaterials.ToList())
                {
                    materialsItem.Children.Remove(rawMaterialItem);
                }

                var lostCommodities = from child in materialsItem.Children
                                      where child.DataObject is Commodity
                                        && part.Commodities.FirstOrDefault(c => !c.IsDeleted && c.Guid == ((Commodity)child.DataObject).Guid) == null
                                      select child;
                foreach (var commodity in lostCommodities.ToList())
                {
                    materialsItem.Children.Remove(commodity);
                }

                // Determine the raw materials and commodities newly added in the underlying part and add child items for them.
                var newRawMaterials = part.RawMaterials.Where(rm => !rm.IsDeleted && !materialsItem.HasChildForObject(rm));
                foreach (var material in newRawMaterials)
                {
                    var item = new RawMaterialTreeItem(material, this.DataObjectSource);
                    item.ReadOnly = this.ReadOnly;
                    item.GroupId = 0;
                    materialsItem.Children.Add(item);
                }

                var newCommodities = part.Commodities.Where(c => !c.IsDeleted && !materialsItem.HasChildForObject(c));
                foreach (var commodity in newCommodities)
                {
                    var item = new CommodityTreeItem(commodity, this.DataObjectSource);
                    item.ReadOnly = this.ReadOnly;
                    item.GroupId = 1;
                    materialsItem.Children.Add(item);
                }

                var rawPartTreeItem = materialsItem.Children.FirstOrDefault(item => item is RawPartTreeItem);

                if (part.RawPart == null || part.RawPart.IsDeleted)
                {
                    materialsItem.Children.Remove(rawPartTreeItem);
                }
                else if (rawPartTreeItem == null)
                {
                    var item = new RawPartTreeItem((RawPart)part.RawPart, this.DataObjectSource, false, this.DataService);
                    item.ReadOnly = this.ReadOnly;
                    item.GroupId = 2;
                    materialsItem.Children.Add(item);
                }

                // Create the Process child, if not already created
                if (part.Process != null && children.FirstOrDefault(c => c is ProcessTreeItem) == null)
                {
                    var processItem = new ProcessTreeItem(part.Process, this.DataObjectSource);
                    processItem.ReadOnly = this.ReadOnly;
                    children.Insert(1, processItem);
                }
            }
            else
            {
                // Remove the Materials and Process child items.
                children.Remove(children.FirstOrDefault(c => c is MaterialsTreeItem));
                children.Remove(children.FirstOrDefault(c => c is ProcessTreeItem));
            }

            // The Result Details child is not displayed if the part is from Master Data.
            if (!part.IsMasterData)
            {
                if (children.FirstOrDefault(c => c is ResultDetailsTreeItem) == null)
                {
                    var calculationResultsItem = new ResultDetailsTreeItem();
                    calculationResultsItem.ReadOnly = this.ReadOnly;
                    children.Add(calculationResultsItem);
                }
            }
            else
            {
                children.Remove(children.FirstOrDefault(c => c is ResultDetailsTreeItem));
            }
        }
    }
}