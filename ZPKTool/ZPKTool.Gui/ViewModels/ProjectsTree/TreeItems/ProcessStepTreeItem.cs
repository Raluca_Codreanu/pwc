﻿namespace ZPKTool.Gui.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using ZPKTool.Data;
    using ZPKTool.Gui.Controls;
    using ZPKTool.Gui.Resources;

    /// <summary>
    /// The data source for an item representing a Process Step in the Projects Tree.
    /// </summary>
    public class ProcessStepTreeItem : TreeViewDataItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessStepTreeItem"/> class.
        /// </summary>
        /// <param name="processStep">The process step backing this item.</param>
        /// <param name="dataContext">The process's data context.</param>
        public ProcessStepTreeItem(ProcessStep processStep, ZPKTool.DataAccess.DbIdentifier dataContext)
        {
            if (processStep == null)
            {
                throw new ArgumentNullException("processStep", "The process step was null.");
            }

            this.DataObjectSource = dataContext;
            this.DataObject = processStep;
            this.IconResourceKey = Images.ProcessStepIconKey;
            this.AllowMultipleSelection = true;

            this.Refresh();
        }

        /// <summary>
        /// Refreshes this item from the underlying data object, including its children collection.
        /// </summary>
        protected override void RefreshInternal()
        {
            ProcessStep step = this.DataObject as ProcessStep;
            if (step == null)
            {
                return;
            }

            this.Label = step.Name;
            this.OrderId = step.Index;

            ProcessCalculationAccuracy accuracy =
                (ProcessCalculationAccuracy)step.Accuracy.GetValueOrDefault((short)ProcessCalculationAccuracy.Calculated);
            switch (accuracy)
            {
                case ProcessCalculationAccuracy.Estimated:
                    this.BottomRightIcon = Images.EstimationIcon;
                    this.BottomRightIconTooltip = LocalizedResources.ProcessCalculationAccuracy_Estimated_Tooltip;
                    break;
                case ProcessCalculationAccuracy.Calculated:
                default:
                    this.BottomRightIcon = null;
                    this.BottomRightIconTooltip = null;
                    break;
            }
        }
    }
}
