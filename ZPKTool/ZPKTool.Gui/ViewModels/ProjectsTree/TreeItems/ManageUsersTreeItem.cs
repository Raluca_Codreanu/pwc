﻿namespace ZPKTool.Gui.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using ZPKTool.Gui.Controls;
    using ZPKTool.Gui.Resources;

    /// <summary>
    /// Represents the data source for the "Manage Users" item in the Projects Tree.
    /// </summary>
    public class ManageUsersTreeItem : TreeViewDataItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ManageUsersTreeItem"/> class.
        /// </summary>
        public ManageUsersTreeItem()
        {
            this.Label = LocalizedResources.General_ManageUsers;
            this.IconResourceKey = Images.ManageUsersIconKey;            
        }
    }
}
