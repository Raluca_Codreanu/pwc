﻿namespace ZPKTool.Gui.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using ZPKTool.Data;
    using ZPKTool.Gui.Controls;
    using ZPKTool.Gui.Resources;

    /// <summary>
    /// Represents the data source for an item representing a Process in the Projects Tree.
    /// </summary>
    public class ProcessTreeItem : TreeViewDataItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessTreeItem"/> class.
        /// </summary>
        /// <param name="process">The process backing this item.</param>
        /// <param name="dataContext">The process's data context.</param>
        public ProcessTreeItem(Process process, ZPKTool.DataAccess.DbIdentifier dataContext)
        {
            if (process == null)
            {
                throw new ArgumentNullException("part", "The part was null.");
            }

            this.DataObjectSource = dataContext;
            this.DataObject = process;
            this.IconResourceKey = Images.ProcessIconKey;
            this.Label = LocalizedResources.General_Process;
            this.AllowChildrenMultipleSelection = true;
            this.AutomationId = "ProcessNode";

            this.Refresh();
        }

        /// <summary>
        /// Refreshes this item from the underlying data object, including its children collection.
        /// </summary>
        protected override void RefreshInternal()
        {
            Process process = this.DataObject as Process;
            if (process == null)
            {
                return;
            }

            // Remove the children items representing steps that no longer exist
            var deadItems = from item in this.Children
                            where item.DataObject is ProcessStep
                                && process.Steps.FirstOrDefault(s => s.Guid == ((ProcessStep)item.DataObject).Guid) == null
                            select item;
            foreach (var item in deadItems.ToList())
            {
                this.Children.Remove(item);
            }

            // Add child items for all process steps that don't have one.
            var newSteps = process.Steps.Where(s => !this.HasChildForObject(s));
            foreach (ProcessStep newStep in newSteps)
            {
                ProcessStepTreeItem item = new ProcessStepTreeItem(newStep, this.DataObjectSource);
                item.ReadOnly = this.ReadOnly;
                this.Children.Add(item);
            }

            // Refresh the Children.
            foreach (TreeViewDataItem stepItem in this.Children.ToList())
            {
                stepItem.Refresh();
            }
        }
    }
}
