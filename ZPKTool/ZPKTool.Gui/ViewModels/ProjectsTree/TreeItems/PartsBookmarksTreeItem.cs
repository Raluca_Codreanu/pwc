﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using ZPKTool.Business;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.MvvmCore;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Represents the data source for the Parts item in the Bookmarks Tree.
    /// </summary>
    public class PartsBookmarksTreeItem : TreeViewDataItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PartsBookmarksTreeItem" /> class.
        /// </summary>
        /// <param name="dataService">The data service</param>
        public PartsBookmarksTreeItem(IProjectsExplorerDataService dataService)
            : base(dataService)
        {
            this.IconResourceKey = Images.PartsIconKey;
            this.BottomRightIcon = Images.BookmarksIcon;
            this.Label = LocalizedResources.General_Parts;

            this.AreChildrenLoaded = false;
            this.LazyLoad = TreeViewItemLazyLoadMode.OnSelectionOrExpansion;
            this.DataObjectSource = DbIdentifier.LocalDatabase;
        }

        /// <summary>
        /// Refreshes this item, including its children collection.
        /// </summary>
        protected override void RefreshInternal()
        {
            if (!this.AreChildrenLoaded)
            {
                // Nothing to refresh if the item was not loaded yet.
                return;
            }

            var crtUser = SecurityManager.Instance.CurrentUser;
            if (crtUser == null)
            {
                return;
            }

            var itemData = this.DataService.GetBookmarkedParts(this.DataObjectSource);
            this.DataObjectContext = itemData.DataManager;
            this.RefreshChildren(itemData.Data as IEnumerable<Part>, this.Children);
        }

        /// <summary>
        /// Implements the children loading logic for items that support lazy loading.
        /// This method is executed asynchronously so it should not manipulate UI elements.
        /// </summary>
        /// <returns>
        /// The loaded children.
        /// </returns>
        public override ICollection<TreeViewDataItem> LoadChildren()
        {
            var itemData = this.DataService.GetBookmarkedParts(this.DataObjectSource);
            this.DataObjectContext = itemData.DataManager;

            var children = new Collection<TreeViewDataItem>();
            this.RefreshChildren(itemData.Data, children);

            return children;
        }

        /// <summary>
        /// Refreshes the children collection from the specified input collections.
        /// Can be used to both refresh and create the children collection.
        /// </summary>
        /// <param name="parts">The parts to serve as the source for the refresh.</param>
        /// <param name="children">The children collection to refresh.</param>
        private void RefreshChildren(
            IEnumerable<Part> parts,
            Collection<TreeViewDataItem> children)
        {
            var lostParts = from child in children
                            where child.DataObject is Part
                                 && parts.FirstOrDefault(p => !p.IsDeleted && p.Guid == ((Part)child.DataObject).Guid) == null
                            select child;
            foreach (var part in lostParts.ToList())
            {
                children.Remove(part);
            }

            var newParts = parts.Where(p => !p.IsDeleted && !this.HasChildForObject(p));
            foreach (var newPart in newParts)
            {
                var item = newPart is RawPart ? new RawPartTreeItem((RawPart)newPart, this.DataObjectSource, true, this.DataService) : new PartTreeItem(newPart, this.DataObjectSource, true, this.DataService);
                item.ReadOnly = this.ReadOnly;
                children.Add(item);
            }
        }
    }
}