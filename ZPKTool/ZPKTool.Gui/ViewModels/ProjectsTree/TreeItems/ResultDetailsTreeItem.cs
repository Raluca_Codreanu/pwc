﻿namespace ZPKTool.Gui.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using ZPKTool.Gui.Controls;
    using ZPKTool.Gui.Resources;

    /// <summary>
    /// Represents the data source for the Result Details item in the Projects Tree.
    /// </summary>
    public class ResultDetailsTreeItem : TreeViewDataItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ResultDetailsTreeItem"/> class.
        /// </summary>
        public ResultDetailsTreeItem()
        {
            this.Label = LocalizedResources.General_ResultDetails;
            this.IconResourceKey = Images.ResultDetailsIconKey;
            this.AutomationId = "ResultDetailsNode";
        }
    }
}
