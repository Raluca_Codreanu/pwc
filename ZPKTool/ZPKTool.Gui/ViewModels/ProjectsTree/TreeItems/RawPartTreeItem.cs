﻿using System;
using ZPKTool.Data;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Represents the data source for an item representing a RawPart in the Projects Tree.
    /// </summary>
    public class RawPartTreeItem : PartTreeItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RawPartTreeItem" /> class.
        /// </summary>
        /// <param name="rawPart">The raw part.</param>
        /// <param name="dataContext">The data context.</param>
        /// <param name="lazyLoadChildren">if set to true, the item's children are loaded only when the item is selected or expanded.</param>
        /// <param name="dataService">The data service.</param>
        public RawPartTreeItem(RawPart rawPart, ZPKTool.DataAccess.DbIdentifier dataContext, bool lazyLoadChildren, IProjectsExplorerDataService dataService) :
            base(rawPart, dataContext, lazyLoadChildren, dataService)
        {
            this.IconResourceKey = Images.RawPartIconKey;
            this.AllowMultipleSelection = true;
        }
    }
}