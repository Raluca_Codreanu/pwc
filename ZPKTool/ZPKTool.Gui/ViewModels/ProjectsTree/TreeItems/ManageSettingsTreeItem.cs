﻿namespace ZPKTool.Gui.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using ZPKTool.Gui.Controls;
    using ZPKTool.Gui.Resources;

    /// <summary>
    /// Represents the data source for the "Manage Settings" item in the Projects Tree.
    /// </summary>
    public class ManageSettingsTreeItem : TreeViewDataItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ManageSettingsTreeItem"/> class.
        /// </summary>
        public ManageSettingsTreeItem()
        {
            this.Label = LocalizedResources.General_ManageSettings;
            this.IconResourceKey = Images.WrenchIconKey;
        }
    }
}
