﻿namespace ZPKTool.Gui.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using ZPKTool.Gui.Controls;

    /// <summary>
    /// Represents the data source for a basic Projects Tree item, without support asynchronous children loading.
    /// Should be used for static items, which have no children load or refresh logic.
    /// </summary>
    public class SimpleProjectsTreeItem : TreeViewDataItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SimpleProjectsTreeItem"/> class.
        /// </summary>
        public SimpleProjectsTreeItem()
        {
            this.AreChildrenLoaded = true;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SimpleProjectsTreeItem"/> class.
        /// </summary>
        /// <param name="dataObject">The data object associated with this item.</param>
        public SimpleProjectsTreeItem(object dataObject)
            : this()
        {
            this.DataObject = dataObject;
        }

        /// <summary>
        /// Gets or sets a value that uniquely identifies this item.
        /// </summary>        
        public ProjectsTreeItemUid Uid { get; set; }
    }
}
