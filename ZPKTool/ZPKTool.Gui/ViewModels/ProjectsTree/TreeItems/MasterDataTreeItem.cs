﻿namespace ZPKTool.Gui.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using ZPKTool.Gui.Controls;
    using ZPKTool.Gui.Resources;

    /// <summary>
    /// Represents the data source for the "Master Data" root item in the Projects Tree.
    /// </summary>
    public class MasterDataTreeItem : SimpleProjectsTreeItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MasterDataTreeItem"/> class.
        /// </summary>
        public MasterDataTreeItem()
        {
            this.Label = LocalizedResources.General_IsMasterData;
            this.IconResourceKey = Images.MasterDataIconKey;
            this.SortDescriptions.Clear();
            this.AutomationId = "Master Data";
            this.InitializeChildren();
        }

        /// <summary>
        /// Finds the child with the specified id. The search is performed in the entire children hierarchy.
        /// </summary>
        /// <param name="uid">The child's id.</param>
        /// <returns>The child if found; otherwise null.</returns>
        public SimpleProjectsTreeItem FindChild(ProjectsTreeItemUid uid)
        {
            SimpleProjectsTreeItem foundItem = null;
            Queue<TreeViewDataItem> searchQueue = new Queue<TreeViewDataItem>(this.Children);
            while (searchQueue.Count > 0)
            {
                var crtItem = searchQueue.Dequeue();

                SimpleProjectsTreeItem simpleItem = crtItem as SimpleProjectsTreeItem;
                if (simpleItem != null && simpleItem.Uid == uid)
                {
                    foundItem = simpleItem;
                    break;
                }

                searchQueue.Enqueue(crtItem.Children);
            }

            return foundItem;
        }

        /// <summary>
        /// Initializes the children of this item.
        /// </summary>
        private void InitializeChildren()
        {
            // Initialize the Entities sub-tree
            SimpleProjectsTreeItem entitiesItem = new SimpleProjectsTreeItem
            {
                Label = LocalizedResources.General_Entities,
                IconResourceKey = Images.MasterDataIconKey,
                Uid = ProjectsTreeItemUid.MasterDataEntitiesItem,
                AutomationId = "MDEntities"
            };
            entitiesItem.SortDescriptions.Clear();

            SimpleProjectsTreeItem assembliesItem = new SimpleProjectsTreeItem
            {
                Label = LocalizedResources.General_Assemblies,
                IconResourceKey = Images.AssembliesIconKey,
                Uid = ProjectsTreeItemUid.MasterDataAssemblies,
                AutomationId = "MDEntitiesAssemblies",
                AllowChildrenMultipleSelection = true
            };
            entitiesItem.Children.Add(assembliesItem);

            SimpleProjectsTreeItem partsItem = new SimpleProjectsTreeItem
            {
                Label = LocalizedResources.General_Parts,
                IconResourceKey = Images.PartsIconKey,
                Uid = ProjectsTreeItemUid.MasterDataParts,
                AutomationId = "MDEntitiesParts",
                AllowChildrenMultipleSelection = true
            };
            entitiesItem.Children.Add(partsItem);

            SimpleProjectsTreeItem machinesItem = new SimpleProjectsTreeItem
            {
                Label = LocalizedResources.General_Machines,
                IconResourceKey = Images.MachineIconKey,
                Uid = ProjectsTreeItemUid.MasterDataMachines,
                AutomationId = "MDEntitiesMachines"
            };
            entitiesItem.Children.Add(machinesItem);

            SimpleProjectsTreeItem rawMaterialsItem = new SimpleProjectsTreeItem
            {
                Label = LocalizedResources.General_RawMaterials,
                IconResourceKey = Images.RawMaterialIconKey,
                Uid = ProjectsTreeItemUid.MasterDataRawMaterials,
                AutomationId = "MDEntitiesRawMaterials"
            };
            entitiesItem.Children.Add(rawMaterialsItem);

            SimpleProjectsTreeItem consumablesItem = new SimpleProjectsTreeItem
            {
                Label = LocalizedResources.General_Consumables,
                IconResourceKey = Images.ConsumableIconKey,
                Uid = ProjectsTreeItemUid.MasterDataConsumables,
                AutomationId = "MDEntitiesConsumables"
            };
            entitiesItem.Children.Add(consumablesItem);

            SimpleProjectsTreeItem commoditiesItem = new SimpleProjectsTreeItem
            {
                Label = LocalizedResources.General_Commodities,
                IconResourceKey = Images.CommodityIconKey,
                Uid = ProjectsTreeItemUid.MasterDataCommodities,
                AutomationId = "MDEntitiesCommodities"
            };
            entitiesItem.Children.Add(commoditiesItem);

            SimpleProjectsTreeItem rawPartsItem = new SimpleProjectsTreeItem
            {
                Label = LocalizedResources.General_RawParts,
                IconResourceKey = Images.RawPartIconKey,
                Uid = ProjectsTreeItemUid.MasterDataRawParts,
                AutomationId = "MDEntitiesRawParts"
            };
            entitiesItem.Children.Add(rawPartsItem);

            SimpleProjectsTreeItem diesItem = new SimpleProjectsTreeItem
            {
                Label = LocalizedResources.General_Dies,
                IconResourceKey = Images.DieIconKey,
                Uid = ProjectsTreeItemUid.MasterDataDies,
                AutomationId = "MDEntitiesDies"
            };
            entitiesItem.Children.Add(diesItem);

            SimpleProjectsTreeItem manufacturersItem = new SimpleProjectsTreeItem
            {
                Label = LocalizedResources.General_Manufacturers,
                IconResourceKey = Images.ManufacturerIconKey,
                Uid = ProjectsTreeItemUid.MasterDataManufacturers,
                AutomationId = "MDEntitiesManufacturer"
            };
            entitiesItem.Children.Add(manufacturersItem);

            SimpleProjectsTreeItem suppliersItem = new SimpleProjectsTreeItem
            {
                Label = LocalizedResources.General_Suppliers,
                IconResourceKey = Images.SupplierIconKey,
                Uid = ProjectsTreeItemUid.MasterDataSuppliers,
                AutomationId = "MDEntitiesSuppliers"
            };
            entitiesItem.Children.Add(suppliersItem);

            // Initialize the Settings sub-tree
            SimpleProjectsTreeItem settingsItem = new SimpleProjectsTreeItem
            {
                Label = LocalizedResources.General_Settings,
                IconResourceKey = Images.WrenchIconKey,
                AutomationId = "MDSettings"
            };
            settingsItem.SortDescriptions.Clear();

            SimpleProjectsTreeItem basicSettingsItem = new SimpleProjectsTreeItem
            {
                Label = LocalizedResources.General_BasicSettings,
                IconResourceKey = Images.BasicSettingsIconKey,
                Uid = ProjectsTreeItemUid.MasterDataBasicSettings,
                AutomationId = "MDSettingsBasicSettings"
            };
            settingsItem.Children.Add(basicSettingsItem);

            SimpleProjectsTreeItem overheadSettingsItem = new SimpleProjectsTreeItem
            {
                Label = LocalizedResources.General_OverheadSettings,
                IconResourceKey = Images.OverheadSettingsIconKey,
                Uid = ProjectsTreeItemUid.MasterDataOverheadSettings,
                AutomationId = "MDSettingsOH"
            };
            settingsItem.Children.Add(overheadSettingsItem);

            SimpleProjectsTreeItem countriesItem = new SimpleProjectsTreeItem
            {
                Label = LocalizedResources.General_Countries,
                IconResourceKey = Images.CountriesIconKey,
                Uid = ProjectsTreeItemUid.MasterDataCountries,
                AutomationId = "MDSettingsCountries"
            };
            settingsItem.Children.Add(countriesItem);

            SimpleProjectsTreeItem currenciesItem = new SimpleProjectsTreeItem
            {
                Label = LocalizedResources.General_Currencies,
                IconResourceKey = Images.CurrencyIconKey,
                Uid = ProjectsTreeItemUid.MasterDataCurrencies,
                AutomationId = "MDSettingsCurrencies"
            };
            settingsItem.Children.Add(currenciesItem);

            this.Children.Add(entitiesItem);
            this.Children.Add(settingsItem);
        }
    }
}
