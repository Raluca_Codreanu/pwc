﻿using System;
using ZPKTool.Data;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Resources;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The machine tree item.
    /// </summary>
    public class MachineTreeItem : TreeViewDataItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MachineTreeItem"/> class.
        /// </summary>
        /// <param name="machine">The machine.</param>
        /// <param name="dataContext">The machine's data context.</param>
        public MachineTreeItem(Machine machine, ZPKTool.DataAccess.DbIdentifier dataContext)
        {
            if (machine == null)
            {
                throw new ArgumentNullException("machine", "The machine was null.");
            }

            this.DataObjectSource = dataContext;
            this.DataObject = machine;
            this.IconResourceKey = Images.MachineIconKey;
            this.TopRightIcon = Images.IsExternalIcon;

            if (dataContext == ZPKTool.DataAccess.DbIdentifier.NotSet)
            {
                ReadOnly = true;
            }

            this.Refresh();
        }
    }
}
