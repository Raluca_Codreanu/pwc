﻿namespace ZPKTool.Gui.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using ZPKTool.Business;
    using ZPKTool.Data;
    using ZPKTool.DataAccess;
    using ZPKTool.Gui.Controls;
    using ZPKTool.Gui.Resources;
    using ZPKTool.Gui.Services;

    /// <summary>
    /// Represents the data source for the My Projects item in the Projects Tree.
    /// </summary>
    public class MyProjectsTreeItem : TreeViewDataItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MyProjectsTreeItem" /> class.
        /// </summary>
        /// <param name="dataService">The data service.</param>
        public MyProjectsTreeItem(IProjectsExplorerDataService dataService)
            : base(dataService)
        {
            this.IconResourceKey = Images.MyProjectsIconKey;
            this.Label = LocalizedResources.General_MyProjects;

            this.AreChildrenLoaded = false;
            this.LazyLoad = TreeViewItemLazyLoadMode.OnSelectionOrExpansion;
            this.AutoReleaseUnderlyingData = true;
            this.DataObjectSource = DbIdentifier.LocalDatabase;
            this.AllowChildrenMultipleSelection = true;
        }

        /// <summary>
        /// Refreshes this item, including its children collection.
        /// </summary>
        protected override void RefreshInternal()
        {
            if (!this.AreChildrenLoaded)
            {
                // Nothing to refresh if the item was not loaded yet.
                return;
            }

            ICollection<ProjectFolder> folders;
            ICollection<Project> projects;

            var itemData = this.DataService.GetMyProjects(this.DataObjectSource, out folders, out projects);
            this.RefreshChildren(folders, projects, this.Children);
        }

        /// <summary>
        /// Implements the children loading logic for items that support lazy loading.
        /// This method is executed asynchronously so it should not manipulate UI elements.
        /// </summary>
        /// <returns>
        /// The loaded children.
        /// </returns>
        public override ICollection<TreeViewDataItem> LoadChildren()
        {
            ICollection<ProjectFolder> folders;
            ICollection<Project> projects;

            var itemData = this.DataService.GetMyProjects(this.DataObjectSource, out folders, out projects);
            this.DataObjectContext = itemData.DataManager;

            var children = new Collection<TreeViewDataItem>();
            this.RefreshChildren(folders, projects, children);

            return children;
        }

        /// <summary>
        /// Refreshes the children collection from the specified input collections.
        /// Can be used to both refresh and create the children collection.
        /// </summary>
        /// <param name="folders">The folders to serve as the source for the refresh.</param>
        /// <param name="projects">The projects to serve as the source for the refresh.</param>
        /// <param name="children">The children collection to refresh.</param>
        private void RefreshChildren(
            IEnumerable<ProjectFolder> folders,
            IEnumerable<Project> projects,
            Collection<TreeViewDataItem> children)
        {
            var lostFolders = from child in children
                              where child.DataObject is ProjectFolder
                                    && folders.FirstOrDefault(f => !f.IsDeleted && f.Guid == ((ProjectFolder)child.DataObject).Guid) == null
                              select child;
            foreach (var folderItem in lostFolders.ToList())
            {
                children.Remove(folderItem);
            }

            var lostProjects = from child in children
                               where child.DataObject is Project
                                    && projects.FirstOrDefault(p => !p.IsDeleted && p.Guid == ((Project)child.DataObject).Guid) == null
                               select child;
            foreach (var project in lostProjects.ToList())
            {
                children.Remove(project);
            }

            var newFolders = folders.Where(f => !f.IsDeleted && !this.HasChildForObject(f));
            foreach (var newFolder in newFolders)
            {
                var item = new ProjectFolderTreeItem(newFolder, this.DataObjectSource, this.DataService);
                item.GroupId = 0;
                item.ReadOnly = this.ReadOnly;
                children.Add(item);
            }

            var newProjects = projects.Where(p => !p.IsDeleted && !this.HasChildForObject(p));
            foreach (var newProject in newProjects)
            {
                var item = new ProjectTreeItem(newProject, this.DataObjectSource, this.DataService);
                item.GroupId = 1;
                item.ReadOnly = this.ReadOnly;
                children.Add(item);
            }
        }
    }
}