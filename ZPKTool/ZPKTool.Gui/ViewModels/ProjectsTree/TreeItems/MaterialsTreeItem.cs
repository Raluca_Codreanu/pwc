﻿namespace ZPKTool.Gui.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using ZPKTool.Gui.Controls;
    using ZPKTool.Gui.Resources;

    /// <summary>
    /// Represents the data source for the Materials item in the Projects Tree.
    /// </summary>
    public class MaterialsTreeItem : TreeViewDataItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MaterialsTreeItem"/> class.
        /// </summary>
        public MaterialsTreeItem()
        {
            this.IconResourceKey = Images.RawMaterialIconKey;
            this.Label = LocalizedResources.General_Materials;
            this.AutomationId = "MaterialsNode";
            this.AllowChildrenMultipleSelection = true;
        }
    }
}
