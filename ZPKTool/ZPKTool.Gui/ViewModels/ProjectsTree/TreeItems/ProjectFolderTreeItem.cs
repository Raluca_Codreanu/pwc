﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using ZPKTool.Business;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Represents the data source for an item representing a project folder in the Projects Tree.
    /// </summary>
    public class ProjectFolderTreeItem : TreeViewDataItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectFolderTreeItem"/> class.
        /// </summary>
        /// <param name="folder">The folder.</param>
        /// <param name="dataContext">The folder's data context. It is is "NotSet" the tree item will be used in view mode.</param>
        /// <param name="dataService">The data service.</param>
        public ProjectFolderTreeItem(ProjectFolder folder, DbIdentifier dataContext, IProjectsExplorerDataService dataService)
            : base(dataService)
        {
            if (folder == null)
            {
                throw new ArgumentNullException("folder", "The folder was null.");
            }

            this.DataObjectSource = dataContext;
            this.DataObject = folder;
            this.IconResourceKey = Images.FolderIconKey;
            this.AllowChildrenMultipleSelection = true;
            this.AllowMultipleSelection = true;

            if (dataContext == DbIdentifier.NotSet)
            {
                this.LazyLoad = TreeViewItemLazyLoadMode.None;
                this.AreChildrenLoaded = true;
                this.Refresh();
                ReadOnly = true;
            }
            else
            {
                this.RefreshLook();

                this.AreChildrenLoaded = false;
                this.LazyLoad = TreeViewItemLazyLoadMode.OnSelectionOrExpansion;
            }
        }

        /// <summary>
        /// Refreshes this item, including its children collection.
        /// </summary>
        protected override void RefreshInternal()
        {
            base.RefreshInternal();
            this.RefreshLook();
            this.RefreshChildren(this.Children);
        }

        /// <summary>
        /// Refreshes the item's look (label, icon(s), etc.)
        /// </summary>
        private void RefreshLook()
        {
            ProjectFolder folder = (ProjectFolder)this.DataObject;
            this.Label = folder.Name;

            this.BottomRightIcon = folder.IsOffline ? Images.OfflineItemIcon : null;
            this.BottomRightIconTooltip = LocalizedResources.Synchronization_OfflineProjectFolderItemToolTip;
        }

        /// <summary>
        /// Implements the children loading logic for items that support lazy loading.
        /// This method is executed asynchronously so it should not manipulate UI elements.
        /// </summary>
        /// <returns>
        /// The loaded children.
        /// </returns>
        public override ICollection<TreeViewDataItem> LoadChildren()
        {
            var backingDataFolder = this.DataObject as ProjectFolder;
            if (backingDataFolder == null)
            {
                return null;
            }

            var itemData = this.DataService.GetFolder(backingDataFolder.Guid, this.DataObjectSource);
            this.DataObject = itemData.Data.FirstOrDefault();
            this.DataObjectContext = itemData.DataManager;

            var children = new Collection<TreeViewDataItem>();
            this.RefreshChildren(children);

            return children;
        }

        /// <summary>
        /// Refreshes the children collection from the underlying data object.
        /// Can be used to both refresh and create the children collection.
        /// </summary>
        /// <param name="children">The children collection to refresh.</param>
        private void RefreshChildren(Collection<TreeViewDataItem> children)
        {
            var underlyingFolder = (ProjectFolder)this.DataObject;

            var lostSubFolders = from child in children
                                 where child.DataObject is ProjectFolder
                                    && underlyingFolder.ChildrenProjectFolders
                                        .FirstOrDefault(f => !f.IsDeleted && f.Guid == ((ProjectFolder)child.DataObject).Guid) == null
                                 select child;
            foreach (var folderItem in lostSubFolders.ToList())
            {
                children.Remove(folderItem);
            }

            var lostProjects = from child in children
                               where child.DataObject is Project
                                    && underlyingFolder.Projects.FirstOrDefault(p => !p.IsDeleted && p.Guid == ((Project)child.DataObject).Guid) == null
                               select child;
            foreach (var project in lostProjects.ToList())
            {
                children.Remove(project);
            }

            var newFolders = underlyingFolder.ChildrenProjectFolders.Where(f => !f.IsDeleted && !this.HasChildForObject(f));
            foreach (var newFolder in newFolders)
            {
                var item = new ProjectFolderTreeItem(newFolder, this.DataObjectSource, this.DataService);
                item.GroupId = 0;
                item.ReadOnly = this.ReadOnly;
                children.Add(item);
            }

            var newProjects = underlyingFolder.Projects.Where(p => !p.IsDeleted && !this.HasChildForObject(p));
            foreach (var newProject in newProjects)
            {
                var item = new ProjectTreeItem(newProject, this.DataObjectSource, this.DataService);
                item.GroupId = 1;
                item.ReadOnly = this.ReadOnly;
                children.Add(item);
            }
        }
    }
}