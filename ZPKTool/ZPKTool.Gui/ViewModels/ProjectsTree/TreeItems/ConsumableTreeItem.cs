﻿using System;
using ZPKTool.Data;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Resources;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The machine tree item.
    /// </summary>
    public class ConsumableTreeItem : TreeViewDataItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConsumableTreeItem"/> class.
        /// </summary>
        /// <param name="consumable">The consumable.</param>
        /// <param name="dataContext">The consumables's data context.</param>
        public ConsumableTreeItem(Consumable consumable, ZPKTool.DataAccess.DbIdentifier dataContext)
        {
            if (consumable == null)
            {
                throw new ArgumentNullException("consumable", "The consumable was null.");
            }

            this.DataObjectSource = dataContext;
            this.DataObject = consumable;
            this.IconResourceKey = Images.ConsumableIconKey;
            this.TopRightIcon = Images.IsExternalIcon;

            if (dataContext == ZPKTool.DataAccess.DbIdentifier.NotSet)
            {
                ReadOnly = true;
            }

            this.Refresh();
        }
    }
}