﻿namespace ZPKTool.Gui.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using ZPKTool.Business;
    using ZPKTool.Data;
    using ZPKTool.DataAccess;
    using ZPKTool.Gui.Controls;
    using ZPKTool.Gui.Resources;

    /// <summary>
    ///  Represents the data source for the Trash Bin item in the Projects Tree.
    /// </summary>
    public class TrashBinTreeItem : TreeViewDataItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TrashBinTreeItem"/> class.
        /// </summary>
        public TrashBinTreeItem()
        {
            this.ReadOnly = true;
            this.Label = LocalizedResources.General_TrashBin;
            this.AutomationId = "Trash Bin";
            this.Refresh();
        }

        /// <summary>
        /// Refreshes this item, including its children collection.
        /// </summary>
        protected override void RefreshInternal()
        {
            // Check if the trash bin is empty to set its icon accordingly.
            // TODO: do the check in the background.
            bool isTrashBinEmpty = false;
            try
            {
                TrashManager trashManager = new TrashManager(DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase));
                isTrashBinEmpty = trashManager.GetItemsCount() <= 0;
            }
            catch
            {
                // Error is ignored because it does not affect the working state of the program.
            }

            this.IconResourceKey = isTrashBinEmpty ? Images.TrashBinEmptyIconKey : Images.TrashBinIconKey;
        }
    }
}
