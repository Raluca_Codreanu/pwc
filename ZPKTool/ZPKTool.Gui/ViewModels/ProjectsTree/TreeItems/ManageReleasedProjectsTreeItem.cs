﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Resources;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Represents the data source for the "Manage Released Projects" item in the Projects Tree.
    /// </summary>
    public class ManageReleasedProjectsTreeItem : SimpleProjectsTreeItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ManageReleasedProjectsTreeItem"/> class.
        /// </summary>
        public ManageReleasedProjectsTreeItem()
            : this(null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ManageReleasedProjectsTreeItem"/> class.
        /// </summary>
        /// <param name="dataObject">The data object associated with this item.</param>
        public ManageReleasedProjectsTreeItem(object dataObject)
            : base(dataObject)
        {
            this.DataObjectSource = DbIdentifier.CentralDatabase;
            this.Label = LocalizedResources.General_ManageReleasedProjects;
            this.IconResourceKey = Images.ManageProjectsIconKey;
            this.AutomationId = "ManageReleasedProjects";
        }
    }
}