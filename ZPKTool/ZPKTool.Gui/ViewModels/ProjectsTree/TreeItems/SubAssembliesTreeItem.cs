﻿namespace ZPKTool.Gui.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using ZPKTool.Gui.Controls;
    using ZPKTool.Gui.Resources;

    /// <summary>
    /// Represents the data source for the Subassemblies sub-item of an Assembly item in the Projects Tree.
    /// </summary>
    public class SubAssembliesTreeItem : TreeViewDataItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SubAssembliesTreeItem"/> class.
        /// </summary>
        public SubAssembliesTreeItem()
        {
            this.IconResourceKey = Images.AssembliesIconKey;
            this.Label = LocalizedResources.General_Subassemblies;
            this.AutomationId = "SubassembliesNode";
            this.AllowChildrenMultipleSelection = true;
        }
    }
}
