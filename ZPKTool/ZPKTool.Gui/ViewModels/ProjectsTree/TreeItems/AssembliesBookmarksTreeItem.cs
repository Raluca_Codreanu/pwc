﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using ZPKTool.Business;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Represents the data source for the Assemblies item in the Bookmarks Tree.
    /// </summary>
    public class AssembliesBookmarksTreeItem : TreeViewDataItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AssembliesBookmarksTreeItem"/> class.
        /// </summary>
        /// <param name="dataService">The data service.</param>
        public AssembliesBookmarksTreeItem(IProjectsExplorerDataService dataService)
            : base(dataService)
        {
            this.IconResourceKey = Images.AssembliesIconKey;
            this.BottomRightIcon = Images.BookmarksIcon;
            this.Label = LocalizedResources.General_Assemblies;

            this.AreChildrenLoaded = false;
            this.LazyLoad = TreeViewItemLazyLoadMode.OnSelectionOrExpansion;
            this.DataObjectSource = DbIdentifier.LocalDatabase;
        }

        /// <summary>
        /// Refreshes this item, including its children collection.
        /// </summary>
        protected override void RefreshInternal()
        {
            if (!this.AreChildrenLoaded)
            {
                // Nothing to refresh if the item was not loaded yet.
                return;
            }

            var crtUser = SecurityManager.Instance.CurrentUser;
            if (crtUser == null)
            {
                return;
            }

            var itemData = this.DataService.GetBookmarkedAssemblies(this.DataObjectSource);
            this.DataObjectContext = itemData.DataManager;
            this.RefreshChildren(itemData.Data as IEnumerable<Assembly>, this.Children);
        }

        /// <summary>
        /// Implements the children loading logic for items that support lazy loading.
        /// This method is executed asynchronously so it should not manipulate UI elements.
        /// </summary>
        /// <returns>
        /// The loaded children.
        /// </returns>
        public override ICollection<TreeViewDataItem> LoadChildren()
        {
            var itemData = this.DataService.GetBookmarkedAssemblies(this.DataObjectSource);
            this.DataObjectContext = itemData.DataManager;

            var children = new Collection<TreeViewDataItem>();
            this.RefreshChildren(itemData.Data, children);

            return children;
        }

        /// <summary>
        /// Refreshes the children collection from the specified input collections.
        /// Can be used to both refresh and create the children collection.
        /// </summary>
        /// <param name="assemblies">The assemblies to serve as the source for the refresh.</param>
        /// <param name="children">The children collection to refresh.</param>
        private void RefreshChildren(
            IEnumerable<Assembly> assemblies,
            Collection<TreeViewDataItem> children)
        {
            var lostAssemblies = from child in children
                                 where child.DataObject is Assembly
                                      && assemblies.FirstOrDefault(p => !p.IsDeleted && p.Guid == ((Assembly)child.DataObject).Guid) == null
                                 select child;
            foreach (var assembly in lostAssemblies.ToList())
            {
                children.Remove(assembly);
            }

            var newAssemblies = assemblies.Where(p => !p.IsDeleted && !this.HasChildForObject(p));
            foreach (Assembly newAssembly in newAssemblies)
            {
                AssemblyTreeItem item = new AssemblyTreeItem(newAssembly, this.DataObjectSource, true, this.DataService);
                item.ReadOnly = this.ReadOnly;
                children.Add(item);
            }
        }
    }
}