﻿namespace ZPKTool.Gui.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using ZPKTool.Business;
    using ZPKTool.Data;
    using ZPKTool.DataAccess;
    using ZPKTool.Gui.Controls;
    using ZPKTool.Gui.Resources;
    using ZPKTool.Gui.Services;

    /// <summary>
    /// Represents the data source for the "Other Users'Projects" item in the Projects Tree.
    /// </summary>
    public class OtherUsersProjectsTreeItem : TreeViewDataItem
    {
        // TODO: if there's no connection when loading children maybe it we should not display the "No Connection" error message and instead display
        // the error by setting the top right icon to an error image with the error message as tooltip

        /// <summary>
        /// Initializes a new instance of the <see cref="OtherUsersProjectsTreeItem"/> class.
        /// </summary>
        /// <param name="dataService">The data service.</param>
        public OtherUsersProjectsTreeItem(IProjectsExplorerDataService dataService)
            : base(dataService)
        {
            this.ReadOnly = true;
            this.IconResourceKey = Images.OtherUsersProjectsIconKey;
            this.Label = LocalizedResources.General_OtherUsersProjects;

            this.AreChildrenLoaded = false;
            this.LazyLoad = TreeViewItemLazyLoadMode.OnExpansion;
            this.AutoReleaseUnderlyingData = true;
            this.DataObjectSource = DbIdentifier.CentralDatabase;
        }

        /// <summary>
        /// Implements the children loading logic for items that support lazy loading.
        /// This method is executed asynchronously so it should not manipulate UI elements.
        /// </summary>
        /// <returns>
        /// The loaded children.
        /// </returns>
        public override ICollection<TreeViewDataItem> LoadChildren()
        {
            var itemData = this.DataService.GetOtherUsers(this.DataObjectSource);
            this.DataObjectContext = itemData.DataManager;

            var children = new Collection<TreeViewDataItem>();
            foreach (var user in itemData.Data)
            {
                var item = new UserItem(user, this.DataObjectSource, this.DataService);
                item.ReadOnly = this.ReadOnly;
                children.Add(item);
            }

            return children;
        }

        /// <summary>
        /// Represents a User child item of OtherUsersProjectsTreeItem.
        /// </summary>
        private class UserItem : TreeViewDataItem
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="UserItem" /> class.
            /// </summary>
            /// <param name="user">The user represented by the item.</param>
            /// <param name="dataContext">The user's data context.</param>
            /// <param name="dataService">The data service.</param>
            public UserItem(User user, DbIdentifier dataContext, IProjectsExplorerDataService dataService)
                : base(dataService)
            {
                this.IconResourceKey = Images.UserIconKey;
                this.Label = user.Name;
                this.DataObject = user;
                this.DataObjectSource = dataContext;

                this.AreChildrenLoaded = false;
                this.LazyLoad = TreeViewItemLazyLoadMode.OnExpansion;
                this.AllowChildrenMultipleSelection = true;
            }

            /// <summary>
            /// Implements the children loading logic for items that support lazy loading.
            /// This method is executed asynchronously so it should not manipulate UI elements.
            /// </summary>
            /// <returns>
            /// The loaded children.
            /// </returns>
            public override ICollection<TreeViewDataItem> LoadChildren()
            {
                var user = (User)this.DataObject;

                // Load the projects of other users                
                Collection<Project> projects = null;
                var dataManager = DataAccessFactory.CreateDataSourceManager(this.DataObjectSource);
                if (SecurityManager.Instance.CurrentUserHasRole(Role.Admin))
                {
                    // An admin can see all projects of another user
                    projects = dataManager.ProjectRepository.GetOtherUserProjects(user.Guid, Guid.Empty);
                }
                else if (SecurityManager.Instance.CurrentUserHasRole(Role.User)
                        || SecurityManager.Instance.CurrentUserHasRole(Role.KeyUser)
                        || SecurityManager.Instance.CurrentUserHasRole(Role.ProjectLeader))
                {
                    // A normal user can see only the projects for which is leader or responsible calculator.
                    var currentUserId = SecurityManager.Instance.CurrentUser.Guid;
                    projects = dataManager.ProjectRepository.GetOtherUserProjects(user.Guid, currentUserId);
                }
                else
                {
                    // A viewer or data manager can't see any projects of other users.
                    projects = new Collection<Project>();
                }

                // Build the tree items corresponding to the loaded projects
                var children = new Collection<TreeViewDataItem>();

                // For each project, determine the folder path to the project and add a tree item for the folders that don't have one already created.
                foreach (var project in projects)
                {
                    if (project.ProjectFolder == null)
                    {
                        var projectItem = new ProjectTreeItem(project, this.DataObjectSource, this.DataService);
                        projectItem.ReadOnly = this.ReadOnly;
                        projectItem.GroupId = 1;
                        children.Add(projectItem);
                    }
                    else
                    {
                        var projectFolderPath = new List<ProjectFolder>();
                        var treeWalker = project.ProjectFolder;
                        while (treeWalker != null)
                        {
                            projectFolderPath.Insert(0, treeWalker);
                            treeWalker = treeWalker.ParentProjectFolder;
                        }

                        // Use a placeholder for this item because this item's Children collection should not be changed in this method.
                        var placeholderItem = new UserItem(user, this.DataObjectSource, this.DataService);
                        placeholderItem.Children.AddRange(children);

                        TreeViewDataItem parentItem = placeholderItem;
                        foreach (var folder in projectFolderPath)
                        {
                            var folderItem = parentItem.FindItemForDataObject(folder);
                            if (folderItem == null)
                            {
                                folderItem = new ProjectFolderTreeItem(folder, this.DataObjectSource, this.DataService)
                                {
                                    ReadOnly = this.ReadOnly
                                };

                                if (parentItem == placeholderItem)
                                {
                                    // Folder items should not be added into this item's placeholder, they should be put in the children collection.
                                    children.Add(folderItem);
                                }
                                else
                                {
                                    parentItem.Children.Add(folderItem);
                                }
                            }

                            parentItem = folderItem;
                        }

                        var projectItem = new ProjectTreeItem(project, this.DataObjectSource, this.DataService);
                        projectItem.ReadOnly = this.ReadOnly;
                        projectItem.GroupId = 1;
                        parentItem.Children.Add(projectItem);
                    }
                }

                return children;
            }
        }
    }
}