﻿namespace ZPKTool.Gui.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using ZPKTool.Data;
    using ZPKTool.DataAccess;
    using ZPKTool.Gui.Controls;
    using ZPKTool.Gui.Resources;
    using ZPKTool.Gui.Services;

    /// <summary>
    /// Represents the data source for the "Released Projects" item in the Projects Tree.
    /// </summary>
    public class ReleasedProjectsTreeItem : TreeViewDataItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReleasedProjectsTreeItem"/> class.
        /// </summary>
        /// <param name="dataService">The data service.</param>
        public ReleasedProjectsTreeItem(IProjectsExplorerDataService dataService)
            : base(dataService)
        {
            this.ReadOnly = true;
            this.IconResourceKey = Images.ProjectIconKey;
            this.Label = LocalizedResources.General_ReleasedProjects;
            this.AutomationId = "Released Projects";
            this.AreChildrenLoaded = false;
            this.LazyLoad = TreeViewItemLazyLoadMode.OnExpansion;
            this.AutoReleaseUnderlyingData = true;
            this.DataObjectSource = DbIdentifier.LocalDatabase;
            this.AllowChildrenMultipleSelection = true;
        }

        /// <summary>
        /// Implements the children loading logic for items that support lazy loading.
        /// This method is executed asynchronously so it should not manipulate UI elements.
        /// </summary>
        /// <returns>
        /// The loaded children.
        /// </returns>
        public override ICollection<TreeViewDataItem> LoadChildren()
        {
            ICollection<ProjectFolder> folders;
            ICollection<Project> projects;

            var itemData = this.DataService.GetReleasedProjects(this.DataObjectSource, out folders, out projects);
            this.DataObjectContext = itemData.DataManager;

            var children = new Collection<TreeViewDataItem>();
            foreach (var folder in folders)
            {
                var folderItem = new ProjectFolderTreeItem(folder, this.DataObjectSource, this.DataService);
                folderItem.ReadOnly = this.ReadOnly;
                children.Add(folderItem);
            }

            foreach (var project in projects)
            {
                var projectItem = new ProjectTreeItem(project, this.DataObjectSource, this.DataService);
                projectItem.ReadOnly = this.ReadOnly;
                children.Add(projectItem);
            }

            return children;
        }
    }
}