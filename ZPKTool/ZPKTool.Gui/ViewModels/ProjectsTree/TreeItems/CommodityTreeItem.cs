﻿namespace ZPKTool.Gui.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using ZPKTool.Data;
    using ZPKTool.Gui.Controls;
    using ZPKTool.Gui.Resources;

    /// <summary>
    /// The data source for an item representing a Commodity in the Projects Tree.
    /// </summary>
    public class CommodityTreeItem : TreeViewDataItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommodityTreeItem"/> class.
        /// </summary>
        /// <param name="commodity">The commodity.</param>
        /// <param name="dataContext">The commodity's data context.</param>
        public CommodityTreeItem(Commodity commodity, ZPKTool.DataAccess.DbIdentifier dataContext)
        {
            if (commodity == null)
            {
                throw new ArgumentNullException("commodity", "The commodity was null.");
            }

            this.DataObjectSource = dataContext;
            this.DataObject = commodity;
            this.IconResourceKey = Images.CommodityIconKey;
            this.AllowMultipleSelection = true;
            this.TopRightIcon = Images.IsExternalIcon;

            if (dataContext == ZPKTool.DataAccess.DbIdentifier.NotSet)
            {
                ReadOnly = true;
            }

            this.Refresh();
        }
    }
}
