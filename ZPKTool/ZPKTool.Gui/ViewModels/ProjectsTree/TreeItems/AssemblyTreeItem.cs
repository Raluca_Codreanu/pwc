﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Represents the data source for an item representing an Assembly in the Projects Tree.
    /// </summary>
    public class AssemblyTreeItem : TreeViewDataItem
    {
        /// <summary>
        /// Weak event listener for the PropertyChanged notification
        /// </summary>
        private WeakEventListener<PropertyChangedEventArgs> propertyChangedListener;

        /// <summary>
        /// The number of parts of the current data object used in his parent
        /// </summary>
        private int? numberOfPartsUsedInParent;

        /// <summary>
        /// Initializes a new instance of the <see cref="AssemblyTreeItem"/> class.
        /// </summary>
        /// <param name="assy">The assembly.</param>
        /// <param name="dataContext">The assembly's data context. If it is set to NotSet, this item will be used in ViewMode and the <see cref="lazyLoadChildren"/> parameter will be ignored.</param>
        /// <param name="lazyLoadChildren">if set to true, the item's children are loaded only when the item is selected or expanded.</param>
        /// <param name="dataService">The data service.</param>
        public AssemblyTreeItem(Assembly assy, ZPKTool.DataAccess.DbIdentifier dataContext, bool lazyLoadChildren, IProjectsExplorerDataService dataService)
            : base(dataService)
        {
            if (assy == null)
            {
                throw new ArgumentNullException("assy", "The assembly was null.");
            }

            this.DataObjectSource = dataContext;
            this.DataObject = assy;
            this.IconResourceKey = Images.AssemblyIconKey;
            this.AllowMultipleSelection = true;
            this.SortDescriptions.Clear();

            if (dataContext == ZPKTool.DataAccess.DbIdentifier.NotSet)
            {
                this.LazyLoad = TreeViewItemLazyLoadMode.None;
                this.AreChildrenLoaded = true;
                this.Refresh();
                this.ReadOnly = true;
            }
            else
            {
                if (lazyLoadChildren)
                {
                    this.AreChildrenLoaded = false;
                    this.LazyLoad = TreeViewItemLazyLoadMode.OnSelectionOrExpansion;
                    this.AutoReleaseUnderlyingData = true;

                    this.IsDataObjectReleased = true;
                }

                this.Refresh();
            }

            this.propertyChangedListener = new WeakEventListener<PropertyChangedEventArgs>(Settings_PropertyChanged);
            PropertyChangedEventManager.AddListener(UserSettingsManager.Instance, propertyChangedListener, ReflectionUtils.GetPropertyName(() => UserSettingsManager.Instance.HideEmptySubassembliesAndParts));
        }

        /// <summary>
        /// Gets or sets the number of parts of the current data object used in his parent
        /// </summary>
        public int? NumberOfPartsUsedInParent
        {
            get { return this.numberOfPartsUsedInParent; }
            set { this.SetProperty(ref this.numberOfPartsUsedInParent, value, () => this.NumberOfPartsUsedInParent); }
        }

        /// <summary>
        /// Refreshes this item from the underlying data object, including its children collection.
        /// </summary>
        protected override void RefreshInternal()
        {
            this.RefreshLook();
            this.RefreshChildren(this.Children);
        }

        /// <summary>
        /// Refreshes the item's look (label, icon(s), etc.)
        /// </summary>
        public void RefreshLook()
        {
            Assembly assembly = (Assembly)this.DataObject;

            // Refresh the item's label and icons
            this.OrderId = assembly.Index ?? -1;
            this.Label = assembly.Name + (!string.IsNullOrWhiteSpace(assembly.Number) ? " (" + assembly.Number + ")" : string.Empty);
            this.TopRightIcon = assembly.IsExternal == true ? Images.IsExternalIcon : null;

            var accuracy = EntityUtils.ConvertToEnum(assembly.CalculationAccuracy, PartCalculationAccuracy.FineCalculation);
            switch (accuracy)
            {
                case PartCalculationAccuracy.Estimation:
                    this.BottomRightIcon = Images.EstimationIcon;
                    this.BottomRightIconTooltip = LocalizedResources.PartCalculationAccuracy_Estimation_Tooltip;
                    break;
                case PartCalculationAccuracy.RoughCalculation:
                    this.BottomRightIcon = Images.RoughCalculationIcon;
                    this.BottomRightIconTooltip = LocalizedResources.PartCalculationAccuracy_RoughCalculation_Tooltip;
                    break;
                case PartCalculationAccuracy.OfferOrExternalCalculation:
                    this.BottomRightIcon = Images.OfferExternalCalculationIcon;
                    this.BottomRightIconTooltip = LocalizedResources.PartCalculationAccuracy_OfferOrExternalCalculation_Tooltip;
                    break;
                default:
                    this.BottomRightIcon = null;
                    this.BottomRightIconTooltip = null;
                    break;
            }
        }

        /// <summary>
        /// Implements the children loading logic for items that support lazy loading.
        /// This method is executed asynchronously so it should not manipulate UI elements.
        /// </summary>
        /// <returns>
        /// The loaded children.
        /// </returns>
        public override ICollection<TreeViewDataItem> LoadChildren()
        {
            var backingDataAssembly = this.DataObject as Assembly;
            if (backingDataAssembly == null)
            {
                return null;
            }

            if (this.IsDataObjectReleased)
            {
                var itemData = this.DataService.GetAssembly(backingDataAssembly.Guid, this.DataObjectSource);
                this.DataObject = itemData.Data.FirstOrDefault();
                this.DataObjectContext = itemData.DataManager;

                // Set the data object as not released.
                this.IsDataObjectReleased = false;
            }

            var children = new Collection<TreeViewDataItem>();
            this.RefreshChildren(children);

            return children;
        }

        /// <summary>
        /// Refreshes the children collection from the underlying data object.
        /// Can be used to both refresh and create the children collection.
        /// </summary>
        /// <param name="children">The children collection to refresh.</param>
        private void RefreshChildren(Collection<TreeViewDataItem> children)
        {
            var assembly = this.DataObject as Assembly;
            if (assembly == null)
            {
                return;
            }

            // Determine whether to show the sub-assemblies, sub-parts and process children based on the current calculation accuracy.            
            var accuracy = EntityUtils.ConvertToEnum(assembly.CalculationAccuracy, PartCalculationAccuracy.FineCalculation);

            if (accuracy == PartCalculationAccuracy.FineCalculation)
            {
                // Obtain or create if necessary the SubParts and SubAssemblies items
                var subassembliesItem = (SubAssembliesTreeItem)children.FirstOrDefault(c => c is SubAssembliesTreeItem);
                if (subassembliesItem == null)
                {
                    subassembliesItem = new SubAssembliesTreeItem();
                    subassembliesItem.ReadOnly = this.ReadOnly;
                }

                var subPartsItem = (SubPartsTreeItem)children.FirstOrDefault(c => c is SubPartsTreeItem);
                if (subPartsItem == null)
                {
                    subPartsItem = new SubPartsTreeItem();
                    subPartsItem.ReadOnly = this.ReadOnly;
                    subPartsItem.AutomationId = "PartsNode";
                }

                // Determine the sub-assemblies and sub-parts that are no longer in the underlying assembly and remove their items
                var lostAssemblies = from child in subassembliesItem.Children
                                     where child.DataObject is Assembly
                                        && assembly.Subassemblies
                                            .FirstOrDefault(a => !a.IsDeleted && a.Guid == ((Assembly)child.DataObject).Guid) == null
                                     select child;
                foreach (var assyItem in lostAssemblies.ToList())
                {
                    subassembliesItem.Children.Remove(assyItem);
                }

                // Refresh the "look" of the remaining sub-assembly items (their children should not be refreshed).
                foreach (var subAssyItem in subassembliesItem.Children.OfType<AssemblyTreeItem>())
                {
                    subAssyItem.RefreshLook();
                }

                var lostParts = from child in subPartsItem.Children
                                where child.DataObject is Part
                                     && assembly.Parts.FirstOrDefault(p => !p.IsDeleted && p.Guid == ((Part)child.DataObject).Guid) == null
                                select child;
                foreach (var part in lostParts.ToList())
                {
                    subPartsItem.Children.Remove(part);
                }

                // Refresh the "look" of the remaining sub-part items (their children should not be refreshed).
                foreach (var subPartItem in subPartsItem.Children.OfType<PartTreeItem>())
                {
                    subPartItem.RefreshLook();
                }

                // Determine the sub-assemblies and sub-parts newly added in the underlying assembly and add child items for them.
                var newAssemblies = assembly.Subassemblies.Where(a => !a.IsDeleted && !subassembliesItem.HasChildForObject(a));
                foreach (var assy in newAssemblies)
                {
                    var item = new AssemblyTreeItem(assy, this.DataObjectSource, false, this.DataService);
                    item.ReadOnly = this.ReadOnly;
                    subassembliesItem.Children.Add(item);
                }

                var newParts = assembly.Parts.Where(p => !p.IsDeleted && !subPartsItem.HasChildForObject(p));
                foreach (var part in newParts)
                {
                    var item = new PartTreeItem(part, this.DataObjectSource, false, this.DataService);
                    item.ReadOnly = this.ReadOnly;
                    subPartsItem.Children.Add(item);
                }

                // Refresh the label number,font size and color of assemblies and parts                
                foreach (var subassyItem in subassembliesItem.Children.OfType<AssemblyTreeItem>())
                {
                    var subassy = subassyItem.DataObject as Assembly;
                    int sum = 0;
                    if (assembly.Process != null)
                    {
                        sum = assembly.Process.Steps.Sum(step =>
                         {
                             var amount = step.AssemblyAmounts.FirstOrDefault(assyAmount => assyAmount.FindAssemblyId() == subassy.Guid);
                             return amount != null ? amount.Amount : 0;
                         });
                    }

                    subassyItem.NumberOfPartsUsedInParent = sum;
                }

                foreach (var subPartItem in subPartsItem.Children.OfType<PartTreeItem>())
                {
                    var subpart = subPartItem.DataObject as Part;
                    int sum = 0;
                    if (assembly.Process != null)
                    {
                        sum = assembly.Process.Steps.Sum(step =>
                         {
                             var amount = step.PartAmounts.FirstOrDefault(partAmount => partAmount.FindPartId() == subpart.Guid);
                             return amount != null ? amount.Amount : 0;
                         });
                    }

                    subPartItem.NumberOfPartsUsedInParent = sum;
                }

                // Create the Process child, if not already created
                if (assembly.Process != null && children.FirstOrDefault(c => c is ProcessTreeItem) == null)
                {
                    var processItem = new ProcessTreeItem(assembly.Process, this.DataObjectSource);
                    processItem.ReadOnly = this.ReadOnly;
                    children.Insert(0, processItem);
                }

                if (UserSettingsManager.Instance.HideEmptySubassembliesAndParts && subassembliesItem.Children.Count == 0)
                {
                    children.Remove(subassembliesItem);
                }
                else if (!children.Contains(subassembliesItem))
                {
                    children.Insert(0, subassembliesItem);
                }

                if (UserSettingsManager.Instance.HideEmptySubassembliesAndParts && subPartsItem.Children.Count == 0)
                {
                    children.Remove(subPartsItem);
                }
                else if (!children.Contains(subPartsItem))
                {
                    if (children.Contains(subassembliesItem))
                    {
                        children.Insert(1, subPartsItem);
                    }
                    else
                    {
                        children.Insert(0, subPartsItem);
                    }
                }
            }
            else
            {
                // Remove the sub-assemblies, sub-parts and Process child items.
                children.Remove(children.FirstOrDefault(c => c is SubAssembliesTreeItem));
                children.Remove(children.FirstOrDefault(c => c is SubPartsTreeItem));
                children.Remove(children.FirstOrDefault(c => c is ProcessTreeItem));
            }

            // The Result Details child is not displayed if the assembly is from Master Data.
            if (!assembly.IsMasterData)
            {
                if (children.FirstOrDefault(c => c is ResultDetailsTreeItem) == null)
                {
                    var calculationResultsItem = new ResultDetailsTreeItem();
                    calculationResultsItem.ReadOnly = this.ReadOnly;
                    children.Add(calculationResultsItem);
                }
            }
            else
            {
                children.Remove(children.FirstOrDefault(c => c is ResultDetailsTreeItem));
            }
        }

        /// <summary>
        /// Handles the PropertyChanged event of the Settings
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void Settings_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == ReflectionUtils.GetPropertyName(() => UserSettingsManager.Instance.HideEmptySubassembliesAndParts))
            {
                this.RefreshChildren(this.Children);
            }
        }
    }
}