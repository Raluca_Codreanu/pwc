﻿namespace ZPKTool.Gui.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using ZPKTool.Gui.Controls;
    using ZPKTool.Gui.Resources;

    /// <summary>
    /// Represents the data source for the Parts sub-item of an Assembly item in the Projects Tree.
    /// </summary>
    public class SubPartsTreeItem : TreeViewDataItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SubPartsTreeItem"/> class.
        /// </summary>
        public SubPartsTreeItem()
        {
            this.IconResourceKey = Images.PartsIconKey;
            this.Label = LocalizedResources.General_Parts;
            this.AllowChildrenMultipleSelection = true;
        }
    }
}
