﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using ZPKTool.Business;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Common;
using ZPKTool.Controls;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Managers;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model base for the Projects and Bookmarks Tree.
    /// </summary>
    public class ProjectsExplorerBaseViewModel : ViewModel, IExplorerDataReleaseProvider
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The Projects Tree items.
        /// </summary>
        private ObservableCollection<TreeViewDataItem> items = new ObservableCollection<TreeViewDataItem>();

        /// <summary>
        /// The screen coordinates where the current drag operation has started.
        /// </summary>
        private Point? dragStartPosition;

        #endregion Attributes

        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectsExplorerBaseViewModel" /> class.
        /// </summary>
        /// <param name="compositionContainer">The composition container.</param>
        /// <param name="messenger">The messenger.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="pleaseWaitService">The please wait service</param>
        /// <param name="unitsService">The units service.</param>
        /// <param name="modelBrowserHelperService">The model browser helper service.</param>
        /// <param name="dataService">The data service.</param>
        /// <param name="importService">The import service.</param>
        public ProjectsExplorerBaseViewModel(
            CompositionContainer compositionContainer,
            IMessenger messenger,
            IWindowService windowService,
            IPleaseWaitService pleaseWaitService,
            IUnitsService unitsService,
            IModelBrowserHelperService modelBrowserHelperService,
            IProjectsExplorerDataService dataService,
            IImportService importService)
        {
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("compositionContainer", compositionContainer);
            Argument.IsNotNull("pleaseWaitService", pleaseWaitService);
            Argument.IsNotNull("unitsService", unitsService);
            Argument.IsNotNull("modelBrowserHelperService", modelBrowserHelperService);
            Argument.IsNotNull("dataService", dataService);
            Argument.IsNotNull("importService", importService);

            this.Messenger = messenger;
            this.WindowService = windowService;
            this.CompositionContainer = compositionContainer;
            this.PleaseWaitService = pleaseWaitService;
            this.UnitsService = unitsService;
            this.ModelBrowserHelperService = modelBrowserHelperService;
            this.DataService = dataService;

            this.ProjectsExplorerCommandManager =
                new ProjectsExplorerCommandManager(this, compositionContainer, messenger, windowService, pleaseWaitService, importService, unitsService);
            this.NextTreeItemSelectionParams = new List<object>();

            this.InitializeCommands();
            this.RegisterMessageHandlers();
        }

        #region Commands

        /// <summary>
        /// Gets the command executed when the selected item of the tree view has changed.
        /// </summary>
        public ICommand SelectedTreeItemChangedCommand { get; private set; }

        /// <summary>
        /// Gets the command executed after the selected items collection is changed.
        /// </summary>
        public ICommand SelectedTreeItemsChangedCommand { get; private set; }

        /// <summary>
        /// Gets the command executed for the PreviewMouseMove event of the explorer tree view.
        /// </summary>
        public ICommand TreePreviewMouseMoveCommand { get; private set; }

        /// <summary>
        /// Gets the command executed for the DragLeave event of the explorer tree view.
        /// </summary>
        public ICommand TreeDragLeaveCommand { get; private set; }

        /// <summary>
        /// Gets the command executed for the QueryContinueDrag event of the explorer tree view.
        /// </summary>
        public ICommand TreeQueryContinueDragCommand { get; private set; }

        /// <summary>
        /// Gets the command executed before the selected item of the explorer tree view has changed.
        /// </summary>
        public ICommand TreePreviewSelectedItemChangedCommand { get; private set; }

        /// <summary>
        /// Gets the command executed for the PreviewMouseDown event of the explorer tree view.
        /// </summary>        
        public ICommand TreePreviewMouseDownCommand { get; private set; }

        /// <summary>
        /// Gets the command executed for the PreviewMouseUp event of the explorer tree view.
        /// </summary>
        public ICommand TreePreviewMouseUpCommand { get; private set; }

        /// <summary>
        /// Gets the command executed for the LoadingItem event of the explorer tree view.
        /// </summary>
        public ICommand TreeLoadingItemCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets the tree items.
        /// </summary>
        public ObservableCollection<TreeViewDataItem> Items
        {
            get { return this.items; }
            private set { this.SetProperty(ref this.items, value, () => this.Items); }
        }

        /// <summary>
        /// Gets the selected Tree item.
        /// </summary>
        public TreeViewDataItem SelectedTreeItem { get; private set; }

        /// <summary>
        /// Gets the selected tree items.
        /// </summary>
        public IList<object> SelectedTreeItems { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether the view associated with this instance is active in the UI.
        /// Active means that is focused and can receive user input.
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Gets the command manager used for the tree.
        /// </summary>
        public ProjectsExplorerCommandManager ProjectsExplorerCommandManager { get; private set; }

        /// <summary>
        /// Gets some parameters that will be passed to the screens loaded during the handling of the selected item's change (SelectedTreeItemChangedAction).
        /// It is used by the Navigation logic to perform some operation on the screen loaded when the entity to navigate to, or its parent,
        /// is selected in the projects tree.
        /// This list is reset every time the SelectedItem changes.
        /// </summary>
        protected IList<object> NextTreeItemSelectionParams { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether the content of the 
        /// selected item is displayed in readonly mode or not.
        /// </summary>
        public bool SelectedItemIsReadonly { get; set; }

        #endregion Properties

        #region Services

        /// <summary>
        /// Gets the composition container.
        /// </summary>
        protected CompositionContainer CompositionContainer { get; private set; }

        /// <summary>
        /// Gets the window service.
        /// </summary>
        public IWindowService WindowService { get; private set; }

        /// <summary>
        /// Gets the units service.
        /// </summary>
        public IUnitsService UnitsService { get; private set; }

        /// <summary>
        /// Gets the messenger service.
        /// </summary>
        protected IMessenger Messenger { get; private set; }

        /// <summary>
        /// Gets the token to be used when sending messages using the <see cref="Messenger"/>.
        /// </summary>
        protected object MessengerToken
        {
            get { return this.IsInViewerMode ? GlobalMessengerTokens.ModelBrowserTargetToken : GlobalMessengerTokens.MainViewTargetToken; }
        }

        /// <summary>
        /// Gets the PleaseWaitService reference.
        /// </summary>
        protected IPleaseWaitService PleaseWaitService { get; private set; }

        /// <summary>
        /// Gets the model browser helper service.
        /// </summary>
        protected IModelBrowserHelperService ModelBrowserHelperService { get; private set; }

        /// <summary>
        /// Gets the data service.
        /// </summary>
        protected IProjectsExplorerDataService DataService { get; private set; }

        #endregion Services

        /// <summary>
        /// Called after the view has been loaded. Usually it performs some initialization that needs the view to be loaded, like
        /// starting to load data from a database.
        /// <para />
        /// During unit tests this method must be manually called because there is no view to call it.
        /// </summary>
        public override void OnLoaded()
        {
            base.OnLoaded();
            this.DataService.RegisterProvider(this);
        }

        /// <summary>
        /// Called after the view has been unloaded from the parent or closed.
        /// </summary>
        public override void OnUnloaded()
        {
            base.OnUnloaded();
            this.Messenger.Unregister(this);
            this.DataService.UnregisterProvider(this);
        }

        #region Initialization

        /// <summary>
        /// Initializes the Project Explorer commands
        /// </summary>
        private void InitializeCommands()
        {
            this.SelectedTreeItemChangedCommand = new DelegateCommand<RoutedPropertyChangedEventArgs<object>>(this.OnSelectedTreeItemChanged);
            this.SelectedTreeItemsChangedCommand = new DelegateCommand<SelectedItemsChangedEventArgs>(this.OnSelectedTreeItemsChanged);
            this.TreePreviewMouseMoveCommand = new DelegateCommand<MouseEventArgs>(this.HandleTreePreviewMouseMove);
            this.TreeDragLeaveCommand = new DelegateCommand<DragEventArgs>(this.HandleTreeDragLeave);
            this.TreeQueryContinueDragCommand = new DelegateCommand<QueryContinueDragEventArgs>(this.HandleTreeQueryContinueDrag);
            this.TreePreviewSelectedItemChangedCommand = new DelegateCommand<RoutedPropertyChangedEventArgs<object>>(this.HandleTreePreviewSelectedItemChanged);
            this.TreePreviewMouseDownCommand = new DelegateCommand<MouseButtonEventArgs>(e => this.HandleTreePreviewMouseDown(e));
            this.TreePreviewMouseUpCommand = new DelegateCommand<MouseButtonEventArgs>(this.HandleTreePreviewMouseUp);
            this.TreeLoadingItemCommand = new DelegateCommand<TreeViewItemLoadingEventArgs>(this.HandleTreeLoadingItem);
        }

        /// <summary>
        /// Registers the necessary message handlers with the Messenger service.
        /// </summary>
        private void RegisterMessageHandlers()
        {
            this.Messenger.Register<NotificationMessage>(this.HandleNotificationMessage);
        }

        #endregion Initialization

        #region Data release

        /// <summary>
        /// Analyzes the project tree items for data release.
        /// </summary>
        /// <returns>Release related data.</returns>
        public ReleasableExplorerData AnalyzeDataForRelease()
        {
            var dataManagersSuitableForDataRelease = new List<IDataSourceManager>();
            var usedDataManagers = new List<IDataSourceManager>();
            foreach (var item in this.Items)
            {
                this.AnalyzeTreeItemsForDataRelease(item, dataManagersSuitableForDataRelease, usedDataManagers);
            }

            var releaseData = new ReleasableExplorerData();
            releaseData.ReleasableDataManagers.AddRange(dataManagersSuitableForDataRelease);
            releaseData.UnreleasableDataManagers.AddRange(usedDataManagers);

            return releaseData;
        }

        /// <summary>
        /// Analyzes the tree item for data release.
        /// </summary>
        /// <param name="item">The item that is currently analyzed.</param>
        /// <param name="dataManagersSuitableForDataRelease">The data managers that are suitable for data release.</param>
        /// <param name="usedDataManagers">The data managers that are currently used in the tree items.</param>
        private void AnalyzeTreeItemsForDataRelease(TreeViewDataItem item, List<IDataSourceManager> dataManagersSuitableForDataRelease, List<IDataSourceManager> usedDataManagers)
        {
            if (item == null)
            {
                return;
            }

            if (item.DataObjectContext != null)
            {
                // Determine if the item is suitable for release.
                var timeSpanSinceLastUnselectAndCollapse = DateTime.Now - item.LastUnselectAndCollapseDateTime;
                if (item.IsSelected
                    || item.IsExpanded
                    || (!item.IsExpanded && timeSpanSinceLastUnselectAndCollapse.TotalMilliseconds <= ProjectsExplorerDataService.ExplorerDataReleaseTimeout))
                {
                    if (!usedDataManagers.Contains(item.DataObjectContext))
                    {
                        usedDataManagers.Add(item.DataObjectContext);
                    }
                }
                else if (item.AutoReleaseUnderlyingData)
                {
                    dataManagersSuitableForDataRelease.Add(item.DataObjectContext);
                }
            }

            // Repeat the analysis for the item's children.  
            foreach (var child in item.Children)
            {
                this.AnalyzeTreeItemsForDataRelease(child, dataManagersSuitableForDataRelease, usedDataManagers);
            }
        }

        /// <summary>
        /// Determine and release the data where the specified data managers are used.
        /// </summary>
        /// <param name="dataManagersToRelease">The data managers to release.</param>
        public void ReleaseData(IEnumerable<IDataSourceManager> dataManagersToRelease)
        {
            if (dataManagersToRelease.Any())
            {
                foreach (var item in this.Items)
                {
                    this.ReleaseData(item, dataManagersToRelease.ToList());
                }
            }
        }

        /// <summary>
        /// Releases the underlying data of the tree items with the specified data managers.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="dataManagersToRelease">The data managers automatic release.</param>
        private void ReleaseData(TreeViewDataItem item, ICollection<IDataSourceManager> dataManagersToRelease)
        {
            if (dataManagersToRelease.Contains(item.DataObjectContext)
                && item.AutoReleaseUnderlyingData)
            {
                var inspectionQueue = new Queue<TreeViewDataItem>();
                inspectionQueue.Enqueue(item.Children);
                while (inspectionQueue.Count > 0)
                {
                    var crtItem = inspectionQueue.Dequeue();
                    dataManagersToRelease.Add(crtItem.DataObjectContext);
                    inspectionQueue.Enqueue(crtItem.Children);
                }

                item.ReleaseUnderlyingData();
            }
            else
            {
                foreach (var child in item.Children)
                {
                    this.ReleaseData(child, dataManagersToRelease);
                }
            }
        }

        #endregion Data release

        #region Tree Items selection

        /// <summary>
        /// The action performed when the SelectedItemChanged command is executed.
        /// </summary>
        /// <param name="e">The <see cref="object"/> instance containing the event data.</param>
        protected virtual void OnSelectedTreeItemChanged(RoutedPropertyChangedEventArgs<object> e)
        {
            // If the newly selected item is the same, it is not needed to recreate the view for its underlying object.
            var itemToSelect = e.NewValue as TreeViewDataItem;
            if (itemToSelect != this.SelectedTreeItem)
            {
                this.SelectedTreeItem = itemToSelect;

                // Handle the tree item selection only if the explorer view is currently active.
                if (this.IsActive)
                {
                    // IMPORTANT: this method absolutely must load new content in the main view because the current content has been unloaded at this point.
                    this.HandleTreeItemSelection(this.SelectedTreeItem);
                }
            }

            this.SelectedItemIsReadonly = this.IsReadOnly;
        }

        /// <summary>
        /// The action performed when the SelectedItemsChanged command is executed.
        /// </summary>
        /// <param name="e">The <see cref="object"/> instance containing the event data.</param>
        protected virtual void OnSelectedTreeItemsChanged(SelectedItemsChangedEventArgs e)
        {
            this.SelectedTreeItems = e.SelectedItems;
        }

        /// <summary>
        /// Handles the preview tree item changed.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void HandleTreePreviewSelectedItemChanged(RoutedPropertyChangedEventArgs<object> e)
        {
            // Signal the main view to notify its content that it will be unloaded.
            // IMPORTANT: if the unloading is not canceled by the content, then SelectedTreeItemChangedAction must load new content because the current one is considered to be unloaded.
            bool hasUnloaded = true;
            var unloadRequestMsg = new NotificationMessageWithAction<bool>(
                Notification.MainViewNotifyContentToUnload,
                (unloadSucceeded) => hasUnloaded = unloadSucceeded);
            this.Messenger.Send(unloadRequestMsg, this.IsInViewerMode ? GlobalMessengerTokens.ModelBrowserTargetToken : GlobalMessengerTokens.MainViewTargetToken);

            // If the unload is successful, set the Handled flag to false to allow the tree item selection to continue; otherwise set the flag to true.
            e.Handled = !hasUnloaded;
        }

        /// <summary>
        /// Loads into main view the content corresponding to the current selected tree item.
        /// <remarks>
        /// The old main view content is unloaded before loading the new one.
        /// </remarks>
        /// </summary>
        public void LoadSelectedTreeItemContent()
        {
            // Signal the main view to notify its content that it will be unloaded.
            bool hasUnloaded = true;
            var unloadRequestMsg = new NotificationMessageWithAction<bool>(
                Notification.MainViewNotifyContentToUnload,
                (unloadSucceeded) => hasUnloaded = unloadSucceeded);
            this.Messenger.Send(unloadRequestMsg, IsInViewerMode ? GlobalMessengerTokens.ModelBrowserTargetToken : GlobalMessengerTokens.MainViewTargetToken);

            if (hasUnloaded)
            {
                this.HandleTreeItemSelection(this.SelectedTreeItem);
            }
        }

        /// <summary>
        /// Handles the selection of a tree item.
        /// </summary>
        /// <param name="selectedItem">The selected item.</param>
        protected void HandleTreeItemSelection(TreeViewDataItem selectedItem)
        {
            if (selectedItem == null)
            {
                this.NextTreeItemSelectionParams.Clear();
                this.Messenger.Send(new LoadContentInMainViewMessage(null, string.Empty), this.MessengerToken);
                this.Messenger.Send(new CurrentComponentCostChangedMessage(null), this.MessengerToken);
                return;
            }

            try
            {
                var parentProjectItem = this.FindParentProjectItem(selectedItem);
                this.UpdateUnitServiceCurrencyData(parentProjectItem, selectedItem);

                // Load the Project edit screen
                ProjectTreeItem projectItem = selectedItem as ProjectTreeItem;
                if (projectItem != null)
                {
                    this.HandleProjectTreeItemSelection(projectItem);
                    return;
                }

                // Load the Assembly edit screen
                AssemblyTreeItem assyItem = selectedItem as AssemblyTreeItem;
                if (assyItem != null)
                {
                    this.HandleAssemblyTreeItemSelection(assyItem, parentProjectItem);
                    return;
                }

                // Load the Part edit screen
                PartTreeItem partItem = selectedItem as PartTreeItem;
                if (partItem != null)
                {
                    this.HandlePartTreeItemSelection(partItem, parentProjectItem);
                    return;
                }

                // Subassemblies screen
                SubAssembliesTreeItem subAssyItem = selectedItem as SubAssembliesTreeItem;
                if (subAssyItem != null)
                {
                    this.HandleSubAssembliesTreeItemSelection(subAssyItem);
                    return;
                }

                // Subparts screen
                SubPartsTreeItem subPartsItem = selectedItem as SubPartsTreeItem;
                if (subPartsItem != null)
                {
                    this.HandleSubPartsTreeItemSelection(subPartsItem);
                    return;
                }

                // Materials screen
                MaterialsTreeItem materialsItem = selectedItem as MaterialsTreeItem;
                if (materialsItem != null)
                {
                    this.HandleMaterialsTreeItemSelection(materialsItem);
                    return;
                }

                // Result Details screen
                ResultDetailsTreeItem resultDetailsItem = selectedItem as ResultDetailsTreeItem;
                if (resultDetailsItem != null)
                {
                    // In some cases is needed to go to different tabs corresponding to some cost types                     
                    object additionalData = this.NextTreeItemSelectionParams.FirstOrDefault();
                    this.HandleResultDetailsItemSelection(resultDetailsItem, additionalData);
                    return;
                }

                // Process screen
                ProcessTreeItem processItem = selectedItem as ProcessTreeItem;
                if (processItem != null)
                {
                    this.HandleProcessTreeItemSelection(processItem, parentProjectItem);
                    return;
                }

                // Process Step screen
                ProcessStepTreeItem stepItem = selectedItem as ProcessStepTreeItem;
                if (stepItem != null)
                {
                    object subItemToSelect = this.NextTreeItemSelectionParams.FirstOrDefault();
                    this.HandleProcessStepItemSelection(stepItem, subItemToSelect);
                    return;
                }

                // Raw Material screen
                RawMaterialTreeItem rawMaterialItem = selectedItem as RawMaterialTreeItem;
                if (rawMaterialItem != null)
                {
                    this.HandleRawMaterialTreeItemSelection(rawMaterialItem, parentProjectItem);
                    return;
                }

                // Commodity screen
                CommodityTreeItem commodityTreeItem = selectedItem as CommodityTreeItem;
                if (commodityTreeItem != null)
                {
                    this.HandleCommodityTreeItemSelection(commodityTreeItem, parentProjectItem);
                    return;
                }

                // Machine screen
                MachineTreeItem machineTreeItem = selectedItem as MachineTreeItem;
                if (machineTreeItem != null)
                {
                    this.HandleMachineTreeItemSelection(machineTreeItem, parentProjectItem);
                    return;
                }

                // Consumable screen
                ConsumableTreeItem consumableTreeItem = selectedItem as ConsumableTreeItem;
                if (consumableTreeItem != null)
                {
                    this.HandleConsumableTreeItemSelection(consumableTreeItem, parentProjectItem);
                    return;
                }

                // Die screen
                DieTreeItem dieTreeItem = selectedItem as DieTreeItem;
                if (dieTreeItem != null)
                {
                    this.HandleDieTreeItemSelection(dieTreeItem, parentProjectItem);
                    return;
                }

                if (this.HandleTreeItemSelectionHook(selectedItem))
                {
                    return;
                }

                // If the selected item has no handling just empty the main view content
                this.Messenger.Send(new LoadContentInMainViewMessage(null, string.Empty), this.MessengerToken);
                this.Messenger.Send(new CurrentComponentCostChangedMessage(null), this.MessengerToken);
            }
            catch (ZPKException ex)
            {
                if (ex.ErrorCode == ErrorCodes.ItemNotFound && selectedItem.Parent != null)
                {
                    selectedItem.Parent.Children.Remove(selectedItem);
                }

                throw;
            }
            finally
            {
                this.NextTreeItemSelectionParams.Clear();
            }
        }

        /// <summary>
        /// When overridden in a derived class, this method allows to add custom (extra) logic to the tree item selection handler (HandleTreeItemSelection method).
        /// This logic is executed after the base logic contained in HandleTreeItemSelection.
        /// </summary>
        /// <param name="selectedItem">The item for which to handle the selection.</param>
        /// <returns>True if the item selection was handled by the method; otherwise, false.</returns>
        protected virtual bool HandleTreeItemSelectionHook(TreeViewDataItem selectedItem)
        {
            return false;
        }

        /// <summary>
        /// Handles the selection of a tree item representing a Project.
        /// </summary>
        /// <param name="projectItem">The project tree item.</param>
        /// <exception cref="UIException">The item's Project was null.</exception>
        private void HandleProjectTreeItemSelection(ProjectTreeItem projectItem)
        {
            Project project = (Project)projectItem.DataObject;
            if (project != null)
            {
                ProjectViewModel projectViewModel = this.CompositionContainer.GetExportedValue<ProjectViewModel>();
                projectViewModel.EditMode = ViewModelEditMode.Edit;
                projectViewModel.IsReadOnly = projectItem.ReadOnly || this.IsReadOnly;
                projectViewModel.IsInViewerMode = this.IsInViewerMode;
                projectViewModel.DataSourceManager = projectItem.DataObjectContext;
                projectViewModel.Model = project;

                LoadContentInMainViewMessage msg = new LoadContentInMainViewMessage(
                    projectViewModel,
                    LocalizedResources.General_Project + " (" + project.Name + ")");
                this.Messenger.Send(msg, this.MessengerToken);
                this.Messenger.Send(new CurrentComponentCostChangedMessage(null), this.MessengerToken);
            }
            else
            {
                this.Messenger.Send(new LoadContentInMainViewMessage(null, string.Empty), this.MessengerToken);
                this.Messenger.Send(new CurrentComponentCostChangedMessage(null), this.MessengerToken);
                throw new UIException(ErrorCodes.ItemNotFound);
            }
        }

        /// <summary>
        /// Handles the selection of a tree item representing an Assembly.
        /// </summary>
        /// <param name="assyItem">The assembly tree item.</param>
        /// <param name="parentProjectItem">The project tree item that contains <paramref name="assyItem"/> in its sub-tree.</param>
        private void HandleAssemblyTreeItemSelection(AssemblyTreeItem assyItem, ProjectTreeItem parentProjectItem)
        {
            IDataSourceManager dataContext = assyItem.DataObjectContext;
            Assembly assembly = (Assembly)assyItem.DataObject;
            Project parentProject = null;

            if (parentProjectItem != null)
            {
                parentProject = (Project)parentProjectItem.DataObject;
            }
            else if (dataContext != null)
            {
                parentProject = dataContext.ProjectRepository.GetParentProject(assembly);
            }

            AssemblyViewModel assyVM = this.CompositionContainer.GetExportedValue<AssemblyViewModel>();
            assyVM.IsReadOnly = assyItem.ReadOnly || this.IsReadOnly;
            assyVM.IsInViewerMode = this.IsInViewerMode;
            assyVM.EditMode = ViewModelEditMode.Edit;
            assyVM.ParentProject = parentProject;
            assyVM.DataSourceManager = dataContext;
            assyVM.Model = assembly;

            string title = LocalizedResources.General_Assembly + " (" + assembly.Name + ")";
            this.Messenger.Send(new LoadContentInMainViewMessage(assyVM, title), this.MessengerToken);

            this.RefreshTreeItemCost(assyItem);
        }

        /// <summary>
        /// Handles the selection of a tree item representing a Part.
        /// </summary>
        /// <param name="partItem">The part tree item.</param>
        /// <param name="parentProjectItem">The project tree item that contains <paramref name="partItem"/> in its sub-tree.</param>
        private void HandlePartTreeItemSelection(PartTreeItem partItem, ProjectTreeItem parentProjectItem)
        {
            IDataSourceManager dataContext = partItem.DataObjectContext;
            Part part = (Part)partItem.DataObject;
            Project parentProject = null;

            if (parentProjectItem != null)
            {
                parentProject = (Project)parentProjectItem.DataObject;
            }
            else if (dataContext != null)
            {
                parentProject = dataContext.ProjectRepository.GetParentProject(part);
            }

            PartViewModel partVM = this.CompositionContainer.GetExportedValue<PartViewModel>();
            partVM.IsReadOnly = partItem.ReadOnly || this.IsReadOnly;
            partVM.IsInViewerMode = this.IsInViewerMode;
            partVM.EditMode = ViewModelEditMode.Edit;
            partVM.ParentProject = parentProject;
            partVM.DataSourceManager = dataContext;
            partVM.Model = part;

            string title = part is RawPart ? LocalizedResources.General_RawPart + " (" + part.Name + ")" : LocalizedResources.General_Part + " (" + part.Name + ")";
            this.Messenger.Send(new LoadContentInMainViewMessage(partVM, title), this.MessengerToken);
            this.RefreshTreeItemCost(partItem);
        }

        /// <summary>
        /// Handles the selection of a tree item representing a collection of sub-assemblies of an assembly.
        /// </summary>
        /// <param name="subAssyItem">The sub-assemblies tree item.</param>
        private void HandleSubAssembliesTreeItemSelection(SubAssembliesTreeItem subAssyItem)
        {
            Assembly parentAssembly = (Assembly)subAssyItem.Parent.DataObject;

            SubassembliesViewModel subassembliesViewModel = this.CompositionContainer.GetExportedValue<SubassembliesViewModel>();
            subassembliesViewModel.AssemblyDataContext = subAssyItem.DataObjectContext;
            subassembliesViewModel.IsReadOnly = subAssyItem.ReadOnly || this.IsReadOnly;
            subassembliesViewModel.IsInViewerMode = this.IsInViewerMode;
            subassembliesViewModel.ParentAssembly = parentAssembly;

            LoadContentInMainViewMessage msg = new LoadContentInMainViewMessage(
                subassembliesViewModel,
                LocalizedResources.General_Subassemblies + " (" + parentAssembly.Name + ")");
            this.Messenger.Send(msg, this.MessengerToken);

            this.RefreshTreeItemCost(subAssyItem.Parent);
        }

        /// <summary>
        /// Handles the selection of a tree item representing a collection of sub-parts of an assembly.
        /// </summary>
        /// <param name="subPartsItem">The sub-parts tree item.</param>
        private void HandleSubPartsTreeItemSelection(SubPartsTreeItem subPartsItem)
        {
            Assembly parentAssembly = (Assembly)subPartsItem.Parent.DataObject;

            PartsViewModel partsViewModel = this.CompositionContainer.GetExportedValue<PartsViewModel>();
            partsViewModel.PartDataContext = subPartsItem.DataObjectContext;
            partsViewModel.IsReadOnly = subPartsItem.ReadOnly || this.IsReadOnly;
            partsViewModel.IsInViewerMode = this.IsInViewerMode;
            partsViewModel.ParentAssembly = parentAssembly;

            LoadContentInMainViewMessage msg = new LoadContentInMainViewMessage(
                partsViewModel,
                LocalizedResources.General_Parts + " (" + parentAssembly.Name + ")");
            this.Messenger.Send(msg, this.MessengerToken);

            this.RefreshTreeItemCost(subPartsItem.Parent);
        }

        /// <summary>
        /// Handles the selection of a tree item representing a collection of materials of a Part.
        /// </summary>
        /// <param name="materialsItem">The materials tree item.</param>
        private void HandleMaterialsTreeItemSelection(MaterialsTreeItem materialsItem)
        {
            var parentItem = materialsItem.Parent;
            string title = LocalizedResources.General_Materials;

            MaterialsViewModel viewmodel = this.CompositionContainer.GetExportedValue<MaterialsViewModel>();
            viewmodel.PartDataContext = parentItem.DataObjectContext;
            viewmodel.IsInViewerMode = this.IsInViewerMode;
            viewmodel.ParentPart = parentItem.DataObject as Part;
            viewmodel.IsReadOnly = materialsItem.ReadOnly || this.IsReadOnly;

            this.Messenger.Send(new LoadContentInMainViewMessage(viewmodel, title), this.MessengerToken);

            this.RefreshTreeItemCost(parentItem);
        }

        /// <summary>
        /// Handles the selection of a tree item representing the process of a Part or Assembly.
        /// </summary>
        /// <param name="processItem">The process tree item.</param>
        /// <param name="parentProjectItem">The project tree item that contains <paramref name="processItem"/> in its sub-tree.</param>
        private void HandleProcessTreeItemSelection(ProcessTreeItem processItem, ProjectTreeItem parentProjectItem)
        {
            var parentItem = processItem.Parent;
            IDataSourceManager dataContext = processItem.DataObjectContext;

            string title = LocalizedResources.General_Process;
            var parentName = EntityUtils.GetEntityName(processItem.Parent.DataObject);
            if (!string.IsNullOrWhiteSpace(parentName))
            {
                title += " (" + parentName + ")";
            }

            Project parentProject = null;
            if (parentProjectItem != null)
            {
                parentProject = (Project)parentProjectItem.DataObject;
            }
            else if (dataContext != null)
            {
                parentProject = dataContext.ProjectRepository.GetParentProject(processItem.DataObject);
            }

            ProcessViewModel viewmodel = this.CompositionContainer.GetExportedValue<ProcessViewModel>();
            viewmodel.ParentProject = parentProject;
            viewmodel.Parent = processItem.Parent.DataObject;
            viewmodel.DataSourceManager = dataContext;
            viewmodel.IsInViewerMode = this.IsInViewerMode;
            viewmodel.IsReadOnly = processItem.ReadOnly || this.IsReadOnly;
            viewmodel.EditMode = ViewModelEditMode.Edit;
            viewmodel.Model = processItem.DataObject as Process;

            this.Messenger.Send(new LoadContentInMainViewMessage(viewmodel, title, false), this.MessengerToken);

            this.RefreshTreeItemCost(parentItem);
        }

        /// <summary>
        ///  Handles the selection of a result details tree item.
        /// </summary>
        /// <param name="resultDetailsItem">The result details item</param>
        /// <param name="additionalData">Additional data used to navigate in result details screen</param>
        private void HandleResultDetailsItemSelection(ResultDetailsTreeItem resultDetailsItem, object additionalData)
        {
            IDataSourceManager dataContext = resultDetailsItem.DataObjectContext;
            var parentProjectItem = this.FindParentProjectItem(resultDetailsItem);
            Project parentProject = null;
            if (parentProjectItem != null)
            {
                parentProject = (Project)parentProjectItem.DataObject;
            }
            else if (dataContext != null)
            {
                parentProject = dataContext.ProjectRepository.GetParentProject(resultDetailsItem.Parent.DataObject);
            }

            ResultDetailsViewModel viewModel = this.CompositionContainer.GetExportedValue<ResultDetailsViewModel>();
            viewModel.Entity = resultDetailsItem.Parent.DataObject;
            viewModel.ParentProject = parentProject;
            viewModel.DataSourceManager = dataContext;
            viewModel.IsReadOnly = resultDetailsItem.ReadOnly || this.IsReadOnly;
            viewModel.IsInViewerMode = this.IsInViewerMode;

            string title = LocalizedResources.General_ResultDetails;
            var parentName = EntityUtils.GetEntityName(resultDetailsItem.Parent.DataObject);
            if (!string.IsNullOrWhiteSpace(parentName))
            {
                title += " (" + parentName + ")";
            }

            // If additional data is provided and is valid then select the tab corresponding to the cost type provided
            if (additionalData != null && (additionalData is Type))
            {
                viewModel.NavigateToCost(additionalData as Type);
            }

            this.Messenger.Send(new LoadContentInMainViewMessage(viewModel, title, false), this.MessengerToken);
        }

        /// <summary>
        /// Handles the selection of a process step tree item.
        /// </summary>
        /// <param name="stepItem">The step item.</param>
        /// <param name="childToSelect">The child object to select after the process step view is loaded.</param>
        private void HandleProcessStepItemSelection(ProcessStepTreeItem stepItem, object childToSelect)
        {
            var step = (ProcessStep)stepItem.DataObject;

            var parentItem = stepItem.Parent;
            this.RefreshTreeItemCost(stepItem.Parent.Parent);
            var parentEntity = stepItem.Parent.Parent.DataObject;
            var parentProjectItem = this.FindParentProjectItem(stepItem);

            Project parentProject;
            if (parentProjectItem != null)
            {
                parentProject = (Project)parentProjectItem.DataObject;
            }
            else
            {
                parentProject = stepItem.DataObjectContext != null
                    ? stepItem.DataObjectContext.ProjectRepository.GetParentProject(stepItem.DataObject)
                    : null;
            }

            var processStepVM = this.CompositionContainer.GetExportedValue<ProcessStepEditorViewModel>();
            processStepVM.DataSourceManager = parentItem.DataObjectContext;
            processStepVM.IsInViewerMode = this.IsInViewerMode;
            processStepVM.IsReadOnly = parentItem.ReadOnly || this.IsReadOnly;
            processStepVM.ParentProject = parentProject;
            processStepVM.ProcessParent = parentEntity;
            processStepVM.Process = stepItem.Parent.DataObject as Process;
            processStepVM.ChildToSelect = childToSelect;
            processStepVM.Model = step;

            var title = LocalizedResources.General_ProcessStep + " (" + step.Name + ")";
            this.Messenger.Send(new LoadContentInMainViewMessage(processStepVM, title, false), this.MessengerToken);
        }

        /// <summary>
        /// Handles the selection of a tree item representing a Raw Material.
        /// </summary>
        /// <param name="rawMaterialItem">The raw material tree item.</param>
        /// <param name="parentProjectItem">The project tree item that contains <paramref name="rawMaterialItem" /> in its sub-tree.</param>
        private void HandleRawMaterialTreeItemSelection(RawMaterialTreeItem rawMaterialItem, ProjectTreeItem parentProjectItem)
        {
            string calculationVersion = null;
            if (rawMaterialItem.Parent != null && rawMaterialItem.Parent.Parent != null && rawMaterialItem.Parent.Parent.DataObject is Part)
            {
                calculationVersion = ((Part)rawMaterialItem.Parent.Parent.DataObject).CalculationVariant;
            }

            var rawMaterial = (RawMaterial)rawMaterialItem.DataObject;

            var dataContext = rawMaterialItem.DataObjectContext;
            Project parentProject = null;
            if (parentProjectItem != null)
            {
                parentProject = (Project)parentProjectItem.DataObject;
            }
            else if (dataContext != null)
            {
                parentProject = dataContext.ProjectRepository.GetParentProject(rawMaterial);
            }

            var rawMaterialVM = this.CompositionContainer.GetExportedValue<RawMaterialViewModel>();
            rawMaterialVM.EditMode = ViewModelEditMode.Edit;
            rawMaterialVM.DataSourceManager = dataContext;
            rawMaterialVM.IsReadOnly = rawMaterialItem.ReadOnly || this.IsReadOnly;
            rawMaterialVM.ParentProject = parentProject;
            rawMaterialVM.IsInViewerMode = this.IsInViewerMode;
            rawMaterialVM.CostCalculationVersion = calculationVersion;
            rawMaterialVM.Model = rawMaterial;

            this.Messenger.Send(new LoadContentInMainViewMessage(rawMaterialVM, LocalizedResources.General_RawMaterial), this.MessengerToken);

            if (rawMaterialItem.Parent != null)
            {
                this.RefreshTreeItemCost(rawMaterialItem.Parent.Parent);
            }
        }

        /// <summary>
        /// Handles the selection of a tree item representing a Die.
        /// </summary>
        /// <param name="dieTreeItem">The die tree item.</param>
        /// <param name="parentProjectItem">The project tree item that contains <paramref name="dieTreeItem" /> in its sub-tree.</param>
        private void HandleDieTreeItemSelection(DieTreeItem dieTreeItem, ProjectTreeItem parentProjectItem)
        {
            // Find the die's parent Assembly/Part.
            var currentParentItem = dieTreeItem.Parent;
            object parentEntity = null;
            while (currentParentItem != null)
            {
                if (currentParentItem.DataObject is Assembly
                    || currentParentItem.DataObject is Part)
                {
                    parentEntity = currentParentItem.DataObject;
                    break;
                }

                currentParentItem = currentParentItem.Parent;
            }

            var die = (Die)dieTreeItem.DataObject;

            var dataContext = dieTreeItem.DataObjectContext;
            Project parentProject = null;
            if (parentProjectItem != null)
            {
                parentProject = (Project)parentProjectItem.DataObject;
            }
            else if (dataContext != null)
            {
                parentProject = dataContext.ProjectRepository.GetParentProject(die);
            }

            var dieVM = this.CompositionContainer.GetExportedValue<DieViewModel>();
            dieVM.EditMode = ViewModelEditMode.Edit;
            dieVM.IsInViewerMode = this.IsInViewerMode;
            dieVM.IsReadOnly = dieTreeItem.ReadOnly || this.IsReadOnly;
            dieVM.ParentProject = parentProject;
            dieVM.DieParent = parentEntity;
            dieVM.Model = die;
            dieVM.DataSourceManager = dataContext;

            this.Messenger.Send(new LoadContentInMainViewMessage(dieVM, LocalizedResources.General_Die), this.MessengerToken);
        }

        /// <summary>
        /// Handles the selection of a tree item representing a Consumable.
        /// </summary>
        /// <param name="consumableTreeItem">The consumable tree item.</param>
        /// <param name="parentProjectItem">The project tree item that contains <paramref name="consumableTreeItem" /> in its sub-tree.</param>
        private void HandleConsumableTreeItemSelection(ConsumableTreeItem consumableTreeItem, ProjectTreeItem parentProjectItem)
        {
            // Find the consumable's parent Assembly/Part.
            var currentParentItem = consumableTreeItem.Parent;
            object parentEntity = null;
            while (currentParentItem != null)
            {
                if (currentParentItem.DataObject is Assembly
                    || currentParentItem.DataObject is Part)
                {
                    parentEntity = currentParentItem.DataObject;
                    break;
                }

                currentParentItem = currentParentItem.Parent;
            }

            var consumable = (Consumable)consumableTreeItem.DataObject;

            var dataContext = consumableTreeItem.DataObjectContext;
            Project parentProject = null;
            if (parentProjectItem != null)
            {
                parentProject = (Project)parentProjectItem.DataObject;
            }
            else if (dataContext != null)
            {
                parentProject = dataContext.ProjectRepository.GetParentProject(consumable);
            }

            var consumableVM = this.CompositionContainer.GetExportedValue<ConsumableViewModel>();
            consumableVM.EditMode = ViewModelEditMode.Edit;
            consumableVM.IsReadOnly = consumableTreeItem.ReadOnly || this.IsReadOnly;
            consumableVM.ParentProject = parentProject;
            consumableVM.ConsumableParent = parentEntity;
            consumableVM.IsInViewerMode = this.IsInViewerMode;
            consumableVM.DataSourceManager = dataContext;
            consumableVM.Model = consumable;

            this.Messenger.Send(new LoadContentInMainViewMessage(consumableVM, LocalizedResources.General_Consumable), this.MessengerToken);
        }

        /// <summary>
        /// Handles the selection of a tree item representing a Machine.
        /// </summary>
        /// <param name="machineTreeItem">The machine tree item.</param>
        /// <param name="parentProjectItem">The project tree item that contains <paramref name="machineTreeItem" /> in its sub-tree.</param>
        private void HandleMachineTreeItemSelection(MachineTreeItem machineTreeItem, ProjectTreeItem parentProjectItem)
        {
            // Find the machine's parent Assembly/Part.
            var currentParentItem = machineTreeItem.Parent;
            object parentEntity = null;
            while (currentParentItem != null)
            {
                if (currentParentItem.DataObject is Assembly
                    || currentParentItem.DataObject is Part)
                {
                    parentEntity = currentParentItem.DataObject;
                    break;
                }

                currentParentItem = currentParentItem.Parent;
            }

            var machine = (Machine)machineTreeItem.DataObject;

            var dataContext = machineTreeItem.DataObjectContext;
            Project parentProject = null;
            if (parentProjectItem != null)
            {
                parentProject = (Project)parentProjectItem.DataObject;
            }
            else if (dataContext != null)
            {
                parentProject = dataContext.ProjectRepository.GetParentProject(machine);
            }

            var machineVM = this.CompositionContainer.GetExportedValue<MachineViewModel>();
            machineVM.EditMode = ViewModelEditMode.Edit;
            machineVM.IsReadOnly = machineTreeItem.ReadOnly || this.IsReadOnly;
            machineVM.ParentProject = parentProject;
            machineVM.MachineParent = parentEntity;
            machineVM.IsInViewerMode = this.IsInViewerMode;
            machineVM.DataSourceManager = dataContext;
            machineVM.Model = machine;

            this.Messenger.Send(new LoadContentInMainViewMessage(machineVM, LocalizedResources.General_Machine), this.MessengerToken);
        }

        /// <summary>
        /// Handles the selection of a tree item representing a Commodity.
        /// </summary>
        /// <param name="commodityTreeItem">The commodity tree item.</param>
        /// <param name="parentProjectItem">The project tree item that contains <paramref name="commodityTreeItem" /> in its sub-tree.</param>
        private void HandleCommodityTreeItemSelection(CommodityTreeItem commodityTreeItem, ProjectTreeItem parentProjectItem)
        {
            // Find the commodity's parent Assembly/Part.
            var currentParentItem = commodityTreeItem.Parent;
            object parentEntity = null;
            while (currentParentItem != null)
            {
                if (currentParentItem.DataObject is Assembly
                    || currentParentItem.DataObject is Part)
                {
                    parentEntity = currentParentItem.DataObject;
                    break;
                }

                currentParentItem = currentParentItem.Parent;
            }

            var commodity = (Commodity)commodityTreeItem.DataObject;

            var dataContext = commodityTreeItem.DataObjectContext;
            Project parentProject = null;
            if (parentProjectItem != null)
            {
                parentProject = (Project)parentProjectItem.DataObject;
            }
            else if (dataContext != null)
            {
                parentProject = dataContext.ProjectRepository.GetParentProject(commodity);
            }

            var commodityVM = this.CompositionContainer.GetExportedValue<CommodityViewModel>();
            commodityVM.IsReadOnly = commodityTreeItem.ReadOnly || this.IsReadOnly;
            commodityVM.IsInViewerMode = this.IsInViewerMode;
            commodityVM.EditMode = ViewModelEditMode.Edit;
            commodityVM.ParentProject = parentProject;
            commodityVM.CommodityParent = parentEntity;
            commodityVM.DataSourceManager = dataContext;
            commodityVM.Model = commodity;

            this.Messenger.Send(new LoadContentInMainViewMessage(commodityVM, LocalizedResources.General_Commodity), this.MessengerToken);

            if (commodityTreeItem.Parent != null)
            {
                this.RefreshTreeItemCost(commodityTreeItem.Parent.Parent);
            }
        }

        /// <summary>
        /// Calculates the cost of an entity represented by a tree item with the purpose of refreshing the UI.
        /// </summary>
        /// <param name="item">The item to calculate the cost for.</param>
        private void RefreshTreeItemCost(TreeViewDataItem item)
        {
            if (item == null)
            {
                return;
            }

            if (item is PartTreeItem || item is AssemblyTreeItem)
            {
                var dataContext = item.DataObjectContext;
                var projectItem = this.FindParentProjectItem(item);
                var project = projectItem != null ? (Project)projectItem.DataObject : (dataContext != null ? dataContext.ProjectRepository.GetParentProject(item.DataObject) : null);

                if (item is PartTreeItem)
                {
                    Part part = (Part)item.DataObject;
                    PartCostCalculationParameters calculationParams = null;
                    if (project != null)
                    {
                        calculationParams = CostCalculationHelper.CreatePartParamsFromProject(project);
                    }
                    else if (this.IsInViewerMode)
                    {
                        calculationParams = this.ModelBrowserHelperService.GetPartCostCalculationParameters();
                    }
                    else
                    {
                        this.Messenger.Send(new CurrentComponentCostChangedMessage(null), this.MessengerToken);
                    }

                    if (calculationParams != null)
                    {
                        ICostCalculator calculator = CostCalculatorFactory.GetCalculator(part.CalculationVariant);
                        var calculationResult = calculator.CalculatePartCost(part, calculationParams);

                        // Notify the application that the calculation results have changed
                        CurrentComponentCostChangedMessage message = new CurrentComponentCostChangedMessage(calculationResult);
                        this.Messenger.Send(message, this.MessengerToken);
                    }
                }
                else if (item is AssemblyTreeItem)
                {
                    Assembly assembly = (Assembly)item.DataObject;
                    AssemblyCostCalculationParameters calculationParams = null;
                    if (project != null)
                    {
                        calculationParams = CostCalculationHelper.CreateAssemblyParamsFromProject(project);
                    }
                    else if (IsInViewerMode)
                    {
                        calculationParams = this.ModelBrowserHelperService.GetAssemblyCostCalculationParameters();
                    }
                    else
                    {
                        this.Messenger.Send(new CurrentComponentCostChangedMessage(null), this.MessengerToken);
                    }

                    if (calculationParams != null)
                    {
                        ICostCalculator calculator = CostCalculatorFactory.GetCalculator(assembly.CalculationVariant);
                        var calculationResult = calculator.CalculateAssemblyCost(assembly, calculationParams);

                        // Notify the application that the cost has changed
                        CurrentComponentCostChangedMessage message = new CurrentComponentCostChangedMessage(calculationResult);
                        this.Messenger.Send(message, this.MessengerToken);
                    }
                }
            }
        }

        /// <summary>
        /// Updates the currency data of the <see cref="UnitsService"/>.
        /// <para/>
        /// The data currency is retrieved from the database based on the parent project information. If the currency data is missing, EURO is used as the default currency.
        /// </summary>
        /// <param name="parentProjectItem">The parent project item.</param>
        /// <param name="selectedItem">The selected item.</param>
        private void UpdateUnitServiceCurrencyData(ProjectTreeItem parentProjectItem, TreeViewDataItem selectedItem)
        {
            if (this.IsInViewerMode)
            {
                return;
            }

            ICollection<Currency> currencies = null;
            Currency baseCurrency = null;

            if (parentProjectItem != null)
            {
                var parentProject = parentProjectItem.DataObject as Project;
                currencies = new Collection<Currency>(parentProject.Currencies.ToList());
                baseCurrency = parentProject.BaseCurrency;
            }

            if (baseCurrency == null)
            {
                CurrencyConversionManager.GetEntityCurrenciesAndBaseCurrency(
                    selectedItem.DataObject,
                    selectedItem.DataObjectContext,
                    out currencies,
                    out baseCurrency);
            }

            this.UnitsService.Currencies = currencies;
            this.UnitsService.BaseCurrency = baseCurrency;

            this.Messenger.Send(new RefreshUnitsAdapterMessage(selectedItem.DataObjectContext));
        }

        #endregion Tree tems selection

        #region Generic messages handling

        /// <summary>
        /// Handles some generic messages, of type NotificationMessage.
        /// </summary>
        /// <param name="message">The message.</param>
        protected virtual void HandleNotificationMessage(NotificationMessage message)
        {
            if (this.IsActive)
            {
                if (message.Notification == Notification.ReloadMainViewCurrentContent)
                {
                    // Reload the current screen by handling again the selection of the currently selected projects tree item.
                    this.HandleTreeItemSelection(this.SelectedTreeItem);
                }
                else if (message.Notification == Notification.SelectNextProcessStep
                    || message.Notification == Notification.SelectPreviousProcessStep)
                {
                    this.HandleProcessStepsSelectionMessage(message);
                }
            }
        }

        /// <summary>
        /// Handle the Process Step selection message sent when the Next or the Previous Step button from ProcessStep screen was clicked.
        /// </summary>
        /// <param name="message">The message.</param>
        private void HandleProcessStepsSelectionMessage(NotificationMessage message)
        {
            if (message.Notification == Notification.SelectNextProcessStep)
            {
                var step = (ProcessStep)this.SelectedTreeItem.DataObject;
                var parentProcessItem = this.SelectedTreeItem.Parent;

                // Determine the position of the step in the process.
                var children = parentProcessItem.Children.OrderBy(c => c.OrderId);
                var currentIndex = children.TakeWhile(child => child.DataObject != step).Count();

                if (currentIndex < parentProcessItem.Children.Count - 1)
                {
                    children.ElementAt(currentIndex + 1).IsSelected = true;
                }
            }
            else if (message.Notification == Notification.SelectPreviousProcessStep)
            {
                var step = (ProcessStep)this.SelectedTreeItem.DataObject;
                var parentProcessItem = this.SelectedTreeItem.Parent;

                // Determine the position of the step in the process.
                var children = parentProcessItem.Children.OrderBy(c => c.OrderId);
                var currentIndex = children.TakeWhile(child => child.DataObject != step).Count();

                if (currentIndex > 0)
                {
                    children.ElementAt(currentIndex - 1).IsSelected = true;
                }
            }
        }

        #endregion Generic messages handling

        #region Common Entity Changed Message Handling

        /// <summary>
        /// Contains the handling for the <see cref="EntityChangedMessage" /> message common to Projects and Bookmarks explorers.
        /// </summary>
        /// <param name="changedEntity">The changed entity.</param>
        /// <param name="parent">The parent of the changed entity.</param>
        /// <param name="changeType">The type of change.</param>
        /// <param name="rootItem">The root item of the changed entity.</param>
        /// <param name="selectItem">A value indicating whether to select or navigate to an item.</param>
        protected void HandleEntityChangedCommon(
            object changedEntity,
            object parent,
            EntityChangeType changeType,
            TreeViewDataItem rootItem,
            bool selectItem)
        {
            // TODO: the trash bin item refresh should take place here
            var trashBinItem = this.Items.FirstOrDefault(it => it is TrashBinTreeItem) as TrashBinTreeItem;

            if (!this.IsActive)
            {
                // If the current view model's view is not active no selection must take place.
                selectItem = false;
            }

            // The EntityDeleted message is handled the same for all entities.
            if (changeType == EntityChangeType.EntityDeleted)
            {
                // TODO: deleted entities should be detached.
                // Check if the deleted entity is a Process Step Amount which needs to be handled in another way as the other entities
                if (changedEntity is ProcessStepAssemblyAmount || changedEntity is ProcessStepPartAmount)
                {
                    // Refreshing the process step amount will delete it from the data context of its parent
                    this.RefreshProcessStepAmount(changedEntity, parent, rootItem);
                }
                else
                {
                    var item = rootItem.FindItemForDataObject(changedEntity);
                    if (item != null)
                    {
                        TreeViewDataItem parentItem = null;
                        if (parent == null
                            && (changedEntity is Project || changedEntity is ProjectFolder || changedEntity is Assembly || changedEntity is Part))
                        {
                            parentItem = rootItem;
                        }
                        else
                        {
                            parentItem = rootItem.FindItemForDataObject(parent);
                        }

                        if (parentItem != null)
                        {
                            if (parentItem.DataObject != null
                                && ((item != null && parentItem.DataObjectContext != item.DataObjectContext)
                                || changedEntity != null))
                            {
                                this.RefreshEntity(item.DataObject, parentItem.DataObjectContext);
                            }

                            this.RefreshParentProject(parentItem);
                            parentItem.Refresh();

                            // If the entity deleted is an assembly process step then refresh its parent assembly
                            // to refresh its children sub-parts and sub-assemblies labels
                            if (changedEntity is AssemblyProcessStep && parentItem.Parent != null)
                            {
                                parentItem.Parent.Refresh();
                            }
                        }
                    }

                    if (trashBinItem != null)
                    {
                        trashBinItem.Refresh();
                    }
                }

                return;
            }

            // The EntityReordered message is handled the same for all entities.
            if (changeType == EntityChangeType.EntityReordered)
            {
                var reorderedEntity = rootItem.FindItemForDataObject(changedEntity);

                // Refresh the child items and their view.
                if (reorderedEntity != null)
                {
                    reorderedEntity.Parent.RefreshChildrenView();
                }

                return;
            }

            // Handle changes to Project Folders.
            ProjectFolder changedFolder = changedEntity as ProjectFolder;
            if (changedFolder != null)
            {
                HandleProjectFolderChange(changedFolder, parent as ProjectFolder, changeType, rootItem, trashBinItem, selectItem);
                return;
            }

            // Handle changes to Projects.
            Project changedProject = changedEntity as Project;
            if (changedProject != null)
            {
                HandleProjectChange(changedProject, parent as ProjectFolder, changeType, rootItem, trashBinItem, selectItem);
                return;
            }

            // Handle changes to other objects (refresh logic is the same for all other objects)
            HandleOtherTreeEntityChange(changedEntity, parent, changeType, rootItem, trashBinItem, selectItem);
        }

        /// <summary>
        /// Handles a change made to a project folder in the projects tree.
        /// </summary>
        /// <param name="changedFolder">The changed folder.</param>
        /// <param name="parentFolder">The parent of the changed folder.</param>
        /// <param name="changeType">The type of change.</param>
        /// <param name="rootItem">The root item of the changed folder.</param>
        /// <param name="trashBinItem">The trash bin item.</param>
        /// <param name="selectItem">A value indicating whether to select or navigate to an item.</param>
        protected void HandleProjectFolderChange(
            ProjectFolder changedFolder,
            ProjectFolder parentFolder,
            EntityChangeType changeType,
            TreeViewDataItem rootItem,
            TrashBinTreeItem trashBinItem,
            bool selectItem)
        {
            if (changeType == EntityChangeType.EntityCreated
                || changeType == EntityChangeType.EntityMoved
                || changeType == EntityChangeType.EntityImported)
            {
                // Refresh the new parent's item so it adds the folder to its children.
                TreeViewDataItem parentItem = null;
                if (parentFolder == null)
                {
                    parentItem = rootItem;
                }
                else
                {
                    parentItem = rootItem.FindItemForDataObject(parentFolder);
                }

                if (changeType == EntityChangeType.EntityImported)
                {
                    var parent = parentItem.DataObject as ProjectFolder;
                    if (parent != null)
                    {
                        var parentDataContext = parentItem.DataObjectContext;
                        parentDataContext.ProjectFolderRepository.GetByKey(changedFolder.Guid);
                    }
                }

                if (parentItem != null)
                {
                    parentItem.Refresh();
                }

                var newFolderItem = parentItem.FindItemForDataObject(changedFolder);
                if (newFolderItem != null && selectItem)
                {
                    newFolderItem.Select();
                }
                else
                {
                    // My Projects is not loaded yet, so use the Navigation logic to select the new folder.
                    if (selectItem)
                    {
                        this.NavigateToEntityCommon(rootItem, changedFolder, DbIdentifier.LocalDatabase, null);
                    }
                }
            }
            else if (changeType == EntityChangeType.EntityRecovered)
            {
                if (parentFolder == null)
                {
                    rootItem.Refresh();
                }
                else
                {
                    TreeViewDataItem parentItem = rootItem.FindItemForDataObject(parentFolder);

                    if (parentItem != null)
                    {
                        this.RefreshEntity(changedFolder, parentItem.DataObjectContext);
                        parentItem.Refresh();
                    }
                }

                if (trashBinItem != null)
                {
                    trashBinItem.Refresh();
                }
            }
            else if (changeType == EntityChangeType.EntityPermanentlyDeleted)
            {
                if (trashBinItem != null)
                {
                    trashBinItem.Refresh();
                }
            }
            else
            {
                // Find the changed folder's tree item and refresh it.
                var folderItem = rootItem.FindItemForDataObject(changedFolder);
                if (folderItem != null)
                {
                    folderItem.Refresh();
                }
            }
        }

        /// <summary>
        /// Handles a change made to a project in the projects tree.
        /// </summary>
        /// <param name="changedProject">The changed project.</param>
        /// <param name="parentFolder">The parent of the changed folder.</param>
        /// <param name="changeType">Type of the change.</param>
        /// <param name="rootItem">The root item of the changed project.</param>
        /// <param name="trashBinItem">The trash bin item.</param>
        /// <param name="selectItem">A value indicating whether to select or navigate to an item.</param>
        protected void HandleProjectChange(
            Project changedProject,
            ProjectFolder parentFolder,
            EntityChangeType changeType,
            TreeViewDataItem rootItem,
            TrashBinTreeItem trashBinItem,
            bool selectItem)
        {
            if (changeType == EntityChangeType.EntityCreated
                || changeType == EntityChangeType.EntityMoved
                || changeType == EntityChangeType.EntityImported)
            {
                // Refresh the new parent's item so it adds the project to its children.
                TreeViewDataItem parentItem = null;
                if (parentFolder == null)
                {
                    parentItem = rootItem;
                }
                else
                {
                    parentItem = rootItem.FindItemForDataObject(parentFolder);
                }

                if (changeType == EntityChangeType.EntityImported)
                {
                    var parent = parentItem.DataObject as ProjectFolder;
                    if (parent != null)
                    {
                        var parentDataContext = parentItem.DataObjectContext;
                        parentDataContext.ProjectRepository.GetByKey(changedProject.Guid);
                    }
                }

                if (parentItem != null)
                {
                    parentItem.Refresh();

                    var newProjectItem = parentItem.FindItemForDataObject(changedProject);
                    if (newProjectItem != null && selectItem)
                    {
                        newProjectItem.Select();
                    }
                    else
                    {
                        // My Projects is not loaded yet, so use the Navigation logic to select the new project.
                        if (selectItem)
                        {
                            this.NavigateToEntityCommon(rootItem, changedProject, DbIdentifier.LocalDatabase, null);
                        }
                    }
                }
            }
            else if (changeType == EntityChangeType.EntityRecovered)
            {
                if (parentFolder == null)
                {
                    rootItem.Refresh();
                }
                else
                {
                    TreeViewDataItem parentItem = rootItem.FindItemForDataObject(parentFolder);

                    if (parentItem != null)
                    {
                        this.RefreshEntity(changedProject, parentItem.DataObjectContext);
                        parentItem.Refresh();
                    }
                }

                if (trashBinItem != null)
                {
                    trashBinItem.Refresh();
                }
            }
            else if (changeType == EntityChangeType.RefreshData)
            {
                this.RefreshProjectData(changedProject, rootItem);
            }
            else if (changeType == EntityChangeType.OverheadSettingsUpdated)
            {
                this.RefreshAfterProjectOverheadSettingsUpdate(changedProject, rootItem);
            }
            else if (changeType == EntityChangeType.EntityPermanentlyDeleted)
            {
                if (trashBinItem != null)
                {
                    trashBinItem.Refresh();
                }
            }
            else
            {
                // Find the changed project's tree item and refresh it.
                var projectItem = rootItem.FindItemForDataObject(changedProject);
                if (projectItem != null)
                {
                    projectItem.Refresh();
                }
            }
        }

        /// <summary>
        /// Handles a change made in the tree to an entity other than Project and ProjectFolder.
        /// </summary>
        /// <param name="changedEntity">The changed entity.</param>
        /// <param name="parent">The parent of the changed entity.</param>
        /// <param name="changeType">Type of the change.</param>
        /// <param name="rootItem">The root item of the changed entity.</param>
        /// <param name="trashBinItem">The trash bin item.</param>
        /// <param name="selectItem">A value indicating whether to select or navigate to an item.</param>
        protected void HandleOtherTreeEntityChange(
            object changedEntity,
            object parent,
            EntityChangeType changeType,
            TreeViewDataItem rootItem,
            TrashBinTreeItem trashBinItem,
            bool selectItem)
        {
            if (changedEntity == null)
            {
                return;
            }

            if (changeType == EntityChangeType.EntityDeleted)
            {
                // Handled in HandleTreeChangeMessage
                return;
            }

            // Permanently deleted handled here because the parent reference is null
            if (changeType == EntityChangeType.EntityPermanentlyDeleted)
            {
                this.RefreshPermanentlyDeletedEntity(changedEntity, rootItem);

                if (trashBinItem != null)
                {
                    trashBinItem.Refresh();
                }

                return;
            }

            TreeViewDataItem parentItem = rootItem.FindItemForDataObject(parent);

            if (changeType == EntityChangeType.EntityCreated
                || changeType == EntityChangeType.EntityImported
                || changeType == EntityChangeType.EntityMoved)
            {
                if (parentItem == null)
                {
                    return;
                }

                // In some cases the imported entity is added to database using a new data context
                // A new data context is used for entities that uses lazy loading (like projects)
                // To add the tree item corresponding to the imported entity it is loaded from the parent data context
                // We don't need the full object because when selected in the tree it loads the full data needed from database
                if (changeType == EntityChangeType.EntityImported)
                {
                    var parentProject = parentItem.DataObject as Project;
                    if (parentProject != null)
                    {
                        var parentDataContext = parentItem.DataObjectContext;

                        var changedAssy = changedEntity as Assembly;
                        if (changedAssy != null)
                        {
                            parentDataContext.AssemblyRepository.GetByKey(changedAssy.Guid);
                        }
                        else
                        {
                            var changedPart = changedEntity as Part;
                            if (changedPart != null)
                            {
                                parentDataContext.PartRepository.GetByKey(changedPart.Guid);
                            }
                        }
                    }
                }

                // Refresh the changed entity's parent tree item. This will create the tree item corresponding to the changed entity.
                parentItem.Refresh();
                this.RefreshParentProject(parentItem);

                // Select in the changed entity's tree item.
                // First search the item in its parent's children; this works if the parent children are loaded.
                var newItem = parentItem.FindItemForDataObject(changedEntity);
                if (newItem != null && selectItem)
                {
                    newItem.Select();
                }
                else
                {
                    // The parent item is not loaded or is a Process Step, so use the Navigation logic to select the changed entity.
                    if (selectItem)
                    {
                        this.NavigateToEntityCommon(rootItem, changedEntity, DbIdentifier.LocalDatabase, null);
                    }
                }
            }
            else if (changeType == EntityChangeType.EntityRecovered)
            {
                if (parentItem == null)
                {
                    Assembly assembly = changedEntity as Assembly;
                    if (assembly != null && rootItem is AssembliesBookmarksTreeItem)
                    {
                        rootItem.Refresh();
                    }
                    else
                    {
                        Part part = changedEntity as Part;
                        if (part != null && rootItem is PartsBookmarksTreeItem)
                        {
                            rootItem.Refresh();
                        }
                    }
                }
                else
                {
                    // Refresh the tree after an entity was recovered
                    this.RefreshAfterEntityRecover(changedEntity, parentItem);
                }

                if (trashBinItem != null)
                {
                    trashBinItem.Refresh();
                }
            }
            else if (changeType == EntityChangeType.EntityUpdated)
            {
                var itemToRefresh = rootItem.FindItemForDataObject(changedEntity);
                if (itemToRefresh != null)
                {
                    itemToRefresh.Refresh();
                }

                // Refresh its parent
                if (parentItem != null)
                {
                    parentItem.Refresh();
                }

                this.RefreshParentProject(itemToRefresh);

                if (trashBinItem != null)
                {
                    trashBinItem.Refresh();
                }
            }

            // Otherwise the change type is not handled.
        }

        /// <summary>
        /// Refresh the specified <paramref name="processStepAmount"/> process step amount
        /// If the process step amount is deleted from the database it will be deleted from the data context of its parent too
        /// </summary>
        /// <param name="processStepAmount">The process step amount</param>
        /// <param name="processStepParent">The process step parent of the process step amount</param>
        /// <param name="rootItem">The tree root item for the process step parent</param>
        private void RefreshProcessStepAmount(object processStepAmount, object processStepParent, TreeViewDataItem rootItem)
        {
            var partAmount = processStepAmount as ProcessStepPartAmount;
            var assemblyAmount = processStepAmount as ProcessStepAssemblyAmount;
            var item = rootItem.FindItemForDataObject(processStepParent);
            if (item != null)
            {
                var step = item.DataObject as ProcessStep;
                if (step != null)
                {
                    IIdentifiable amount = null;
                    if (partAmount != null)
                    {
                        amount = step.PartAmounts.FirstOrDefault(a => a.Guid == partAmount.Guid) as IIdentifiable;
                    }
                    else if (assemblyAmount != null)
                    {
                        amount = step.AssemblyAmounts.FirstOrDefault(a => a.Guid == assemblyAmount.Guid) as IIdentifiable;
                    }

                    if (amount != null)
                    {
                        this.RefreshEntity(amount, item.DataObjectContext);
                    }
                }
            }
        }

        /// <summary>
        /// Refreshes the parent project item of a specified tree item.
        /// </summary>
        /// <param name="treeViewDataItem">The tree view data item which's parent project should be refreshed.</param>
        private void RefreshParentProject(TreeViewDataItem treeViewDataItem)
        {
            var parentProjectItem = this.FindParentProjectItem(treeViewDataItem);
            if (parentProjectItem != null && parentProjectItem.DataObject != null)
            {
                Project parentProject = parentProjectItem.DataObject as Project;

                if (parentProject != null)
                {
                    var isDirectChildOfProject = false;

                    if (treeViewDataItem is AssemblyTreeItem)
                    {
                        isDirectChildOfProject = (treeViewDataItem.DataObject as Assembly).Project != null;
                    }
                    else
                    {
                        if (treeViewDataItem is PartTreeItem)
                        {
                            isDirectChildOfProject = (treeViewDataItem.DataObject as Part).Project != null;
                        }
                    }

                    // Note: If the data object of the passed tree view data item is a direct child of the parent project, then this
                    // project must be refreshed along with its references. Related Ticket #1666.
                    if (isDirectChildOfProject)
                    {
                        this.RefreshEntityAndRefrences(parentProjectItem.DataObject, parentProjectItem.DataObjectContext);
                    }
                    else
                    {
                        this.RefreshEntity(parentProjectItem.DataObject, parentProjectItem.DataObjectContext);
                    }

                    parentProjectItem.Refresh();
                }
            }
        }

        /// <summary>
        /// Refresh the project structure, reloading all its child, if there are not fully loaded yet.
        /// </summary>
        /// <param name="project">The project to refresh.</param>
        /// <param name="rootItem">The tree root item.</param>
        private void RefreshProjectData(Project project, TreeViewDataItem rootItem)
        {
            var projectItem = rootItem.FindItemForDataObject(project);
            projectItem.DataObjectContext.ProjectRepository.Refresh(project);

            // Each child of the project item is analyzed.
            foreach (var child in projectItem.Children)
            {
                var dataManager = child.DataObjectContext;
                var partItem = child as PartTreeItem;
                if (partItem != null && !partItem.IsDataObjectReleased)
                {
                    // If the part is loaded, reload all data from the db
                    var part = partItem.DataObject as Part;
                    if (part != null && dataManager != null)
                    {
                        dataManager.PartRepository.GetPartFull(part.Guid);
                    }
                }

                var assyItem = child as AssemblyTreeItem;
                if (assyItem != null && !assyItem.IsDataObjectReleased)
                {
                    // If the assembly is loaded, reload all data from the db
                    var assy = assyItem.DataObject as Assembly;
                    if (assy != null && dataManager != null)
                    {
                        dataManager.AssemblyRepository.GetAssemblyFull(assy.Guid);
                    }
                }
            }
        }

        /// <summary>
        /// Update the overhead settings for the project passed as parameter.
        /// </summary>
        /// <param name="updatedProject">The project to be updated overhead settings.</param>
        /// <param name="rootItem">The tree root item.</param>
        private void RefreshAfterProjectOverheadSettingsUpdate(Project updatedProject, TreeViewDataItem rootItem)
        {
            // The node of the updated project.
            var projectItem = rootItem.FindItemForDataObject(updatedProject) as ProjectTreeItem;

            // The overhead settings were updated on the project context.
            // We must take each sub-item of the project and refresh its overhead settings.
            foreach (var child in projectItem.Children)
            {
                var dataManager = child.DataObjectContext;
                var partItem = child as PartTreeItem;
                if (partItem != null && !partItem.IsDataObjectReleased)
                {
                    // If the part node is loaded, use its data context for the update.
                    var part = partItem.DataObject as Part;
                    if (part != null && part.OverheadSettings != null && dataManager != null)
                    {
                        dataManager.OverheadSettingsRepository.Refresh(part.OverheadSettings);
                    }
                }
                else
                {
                    var assemblyItem = child as AssemblyTreeItem;
                    if (assemblyItem != null && !assemblyItem.IsDataObjectReleased)
                    {
                        // If an assembly node is loaded, use its data context for the refresh and for the refresh of this children.
                        var assembly = assemblyItem.DataObject as Assembly;
                        var assembliesQueue = new Queue<Assembly>();
                        assembliesQueue.Enqueue(assembly);
                        while (assembliesQueue.Count > 0)
                        {
                            var assy = assembliesQueue.Dequeue();
                            if (assy.OverheadSettings != null && dataManager != null)
                            {
                                dataManager.OverheadSettingsRepository.Refresh(assy.OverheadSettings);
                            }

                            foreach (var part in assy.Parts)
                            {
                                if (part != null && part.OverheadSettings != null && dataManager != null)
                                {
                                    dataManager.OverheadSettingsRepository.Refresh(part.OverheadSettings);
                                }
                            }

                            foreach (var subAssy in assy.Subassemblies)
                            {
                                assembliesQueue.Enqueue(subAssy);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Refreshes the tree after the specified entity is recovered.
        /// </summary>
        /// <param name="recoveredEntity">The recovered entity.</param>
        /// <param name="parentItem">The tree item of the entity which is the direct parent of the recovered entity.</param>
        private void RefreshAfterEntityRecover(object recoveredEntity, TreeViewDataItem parentItem)
        {
            if (!parentItem.AreChildrenLoaded)
            {
                // If the parent item is not loaded, there is not need to refresh the tree.
                return;
            }

            // Refresh the recovered entity using the parent's data context so its IsDeleted flag is refreshed.
            // The recovery process is done using a different data context, so in the projects tree the entity is still seen as deleted.            
            IDataSourceManager dataManager = parentItem.DataObjectContext;
            if (dataManager != null)
            {
                Assembly recoveredAssembly = recoveredEntity as Assembly;
                if (recoveredAssembly != null)
                {
                    dataManager.AssemblyRepository.GetAssemblyFull(recoveredAssembly.Guid);
                }

                Part recoveredPart = recoveredEntity as Part;
                if (recoveredPart != null)
                {
                    dataManager.PartRepository.GetPartFull(recoveredPart.Guid);
                }

                RawMaterial recoveredMaterial = recoveredEntity as RawMaterial;
                if (recoveredMaterial != null)
                {
                    dataManager.RawMaterialRepository.GetById(recoveredMaterial.Guid);
                }

                Machine recoveredMachine = recoveredEntity as Machine;
                if (recoveredMachine != null)
                {
                    dataManager.MachineRepository.GetById(recoveredMachine.Guid);
                }

                Die recoveredDie = recoveredEntity as Die;
                if (recoveredDie != null)
                {
                    dataManager.DieRepository.GetById(recoveredDie.Guid);
                }

                Consumable recoveredConsumable = recoveredEntity as Consumable;
                if (recoveredConsumable != null)
                {
                    dataManager.ConsumableRepository.GetById(recoveredConsumable.Guid);
                }

                Commodity recoveredCommodity = recoveredEntity as Commodity;
                if (recoveredCommodity != null)
                {
                    dataManager.CommodityRepository.GetById(recoveredCommodity.Guid);
                }
            }

            // Refreshing the parent item will create an item for the changed (created/new) object.                
            parentItem.Refresh();
        }

        /// <summary>
        /// Refresh the parents of the old tree items after the underlying data objects were moved. 
        /// This will visually remove the old tree items from their old parents.
        /// </summary>
        /// <param name="rootItem">The tree root item.</param>
        /// <param name="entities">The entities to refresh.</param>
        protected void RefreshMovedTreeItemsParents(TreeViewDataItem rootItem, List<object> entities)
        {
            foreach (var item in entities)
            {
                // Remove the changed object item from its old parent.
                var oldTreeItem = rootItem.FindItemForDataObject(item);
                if (oldTreeItem != null && oldTreeItem.Parent != null)
                {
                    // Refresh the object on the data context of his old parent
                    // This will remove the object from old parent's children collection
                    IDataSourceManager parentDataContext = oldTreeItem.Parent.DataObjectContext;
                    var repository = parentDataContext.Repository(item.GetType());
                    var identifObject = item as IIdentifiable;
                    if (identifObject != null)
                    {
                        var obj = repository.GetByKey(identifObject.Guid);
                        if (obj != null)
                        {
                            repository.RefreshParentRelations(obj);
                        }
                    }

                    if (item is Project || item is ProjectFolder || oldTreeItem.Parent.DataObject != null)
                    {
                        // Also we refresh the node in order to visually remove the pasted node from his old place
                        oldTreeItem.Parent.Refresh();
                    }
                    else if (oldTreeItem.Parent.Parent != null)
                    {
                        // In this case the parent item is an decorative item that doesn't have data context (like subassemblies)
                        // Needs to refresh the parent of the items parent
                        oldTreeItem.Parent.Parent.Refresh();
                    }
                }
            }
        }

        #endregion Common Entity Changed Message Handling

        #region Common logic for navigation to specified tree items

        /// <summary>
        /// Implements the logic to navigate to an entity's tree item within a specified sub-tree of a project explorer (tree).
        /// This logic is common to all project explorer implementations (my projects, model viewer, bookmarks).
        /// <para/>
        /// In this context navigating means selecting the tree item.
        /// </summary>
        /// <param name="rootItem">The root item where to start the search.</param>
        /// <param name="entity">The entity to whose item to navigate.</param>
        /// <param name="entitySourceDb">A value indicating the entity's source database.</param>
        /// <param name="additionalData">Custom data used for navigation.</param>
        protected void NavigateToEntityCommon(TreeViewDataItem rootItem, object entity, DbIdentifier entitySourceDb, object additionalData)
        {
            if (rootItem.LazyLoad != TreeViewItemLazyLoadMode.None && !rootItem.AreChildrenLoaded)
            {
                // If the root item is not loaded, expand it so it loads and then retry this call.
                Action<TreeViewDataItem> postExpandAction = null;
                postExpandAction = (e) =>
                {
                    rootItem.ChildrenLoaded -= postExpandAction;
                    NavigateToEntityCommon(rootItem, entity, entitySourceDb, additionalData);
                };
                rootItem.ChildrenLoaded += postExpandAction;
                rootItem.IsExpanded = true;
                return;
            }

            // Search for the entity in the root item's children.
            // If no additional data then found tree item is selected
            var treeItem = rootItem.FindItemForDataObject(entity);
            if (treeItem != null)
            {
                if (additionalData == null)
                {
                    // If the item is already selected show the main view document content 
                    if (treeItem.IsSelected)
                    {
                        var message = new NotificationMessage(Notification.MainViewShowDocumentContent);
                        this.Messenger.Send(message);
                    }

                    treeItem.Select();
                }
                else
                {
                    this.NavigateToEntityInResultDetails(
                        treeItem,
                        additionalData,
                        entitySourceDb == DbIdentifier.NotSet ? GlobalMessengerTokens.ModelBrowserTargetToken : GlobalMessengerTokens.MainViewTargetToken);
                }
            }
            else
            {
                // If the entity is not a child of the root item, search it in the root item's Process item.
                bool foundInProcess = NavigateToEntityInProcesses(
                    rootItem,
                    entity,
                    entitySourceDb == DbIdentifier.NotSet ? GlobalMessengerTokens.ModelBrowserTargetToken : GlobalMessengerTokens.MainViewTargetToken);

                if (!foundInProcess)
                {
                    // If it was not found in the process either then it must belong to a child whose sub-tree is not loaded yet,
                    // like lazy-loaded parts and assemblies.                    
                    IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(entitySourceDb);
                    object parentEntity = dataContext.AssemblyRepository.GetTopParentAssembly(entity);
                    if (parentEntity == null)
                    {
                        parentEntity = dataContext.PartRepository.GetParentPart(entity);
                        if (parentEntity is RawPart)
                        {
                            parentEntity = dataContext.PartRepository.GetParentPart(parentEntity);
                        }
                    }

                    if (parentEntity == null)
                    {
                        parentEntity = dataContext.ProjectRepository.GetParentProject(entity);
                    }

                    if (parentEntity != null)
                    {
                        var parentEntityItem = rootItem.FindItemForDataObject(parentEntity);
                        if (parentEntityItem != null && parentEntityItem != rootItem)
                        {
                            NavigateToEntityCommon(parentEntityItem, entity, entitySourceDb, additionalData);
                            return;
                        }
                    }

                    log.Error("Could not find the parent assembly/part of an entity while trying to navigate to it (entity type: {0}).", entity.GetType());
                    this.WindowService.MessageDialogService.Show(LocalizedResources.Error_GoToError, MessageDialogType.Error);
                }
            }
        }

        /// <summary>
        /// Searches the specified entity in all processes from the sub-tree of the specified tree item and navigates to it if it is found.
        /// </summary>
        /// <param name="treeItem">The tree item in which to search.</param>
        /// <param name="entity">The entity to which to navigate.</param>
        /// <param name="messageToken">The message messageTokenToUse.</param>
        /// <returns>
        /// True if the entity was found and selected; otherwise false.
        /// </returns>
        private bool NavigateToEntityInProcesses(TreeViewDataItem treeItem, object entity, object messageToken)
        {
            if (treeItem == null || entity == null)
            {
                return false;
            }

            var pastedDataEntity = entity as PastedData;
            if (pastedDataEntity != null)
            {
                entity = pastedDataEntity.Entity;
            }

            IIdentifiable identifiableEntity = entity as IIdentifiable;
            if (identifiableEntity == null)
            {
                return false;
            }

            Guid entityId = identifiableEntity.Guid;
            bool entityFound = false;
            ProcessStepTreeItem stepItemToSelect = null;

            // Find the Process child item of the root item. Not all items have a Process child.
            ProcessTreeItem processItem = treeItem.Children.OfType<ProcessTreeItem>().FirstOrDefault();
            if (processItem != null)
            {
                foreach (ProcessStepTreeItem stepItem in processItem.Children.OfType<ProcessStepTreeItem>())
                {
                    ProcessStep step = stepItem.DataObject as ProcessStep;
                    if (step == null)
                    {
                        continue;
                    }

                    stepItemToSelect = stepItem;

                    var consumable = entity as Consumable;
                    if (consumable != null && (step.Consumables.Select(c => c.Guid).Contains(consumable.Guid) || consumable.ProcessStep == step))
                    {
                        entityFound = true;
                        break;
                    }

                    var commodity = entity as Commodity;
                    if (commodity != null && (step.Commodities.Select(c => c.Guid).Contains(commodity.Guid) || commodity.ProcessStep == step))
                    {
                        entityFound = true;
                        break;
                    }

                    var die = entity as Die;
                    if (die != null && (step.Dies.Select(d => d.Guid).Contains(die.Guid) || die.ProcessStep == step))
                    {
                        entityFound = true;
                        break;
                    }

                    var machine = entity as Machine;
                    if (machine != null && (step.Machines.Select(m => m.Guid).Contains(machine.Guid) || machine.ProcessStep == step))
                    {
                        entityFound = true;
                        break;
                    }
                }
            }

            // Select the entity if it was found
            if (entityFound && stepItemToSelect != null)
            {
                if (stepItemToSelect.IsSelected)
                {
                    // This is the callback for the message that requests the main view's content; it selects the entity on the edit process step screen.
                    Action<object> selector = (mainViewContent) =>
                    {
                        var processStepEditor = mainViewContent as ProcessStepEditorViewModel;
                        if (processStepEditor != null)
                        {
                            processStepEditor.SelectChild(entity);
                        }
                    };

                    var message = new NotificationMessageWithAction<object>(Notification.MainViewGetContent, selector);
                    this.Messenger.Send(message, messageToken);

                    // Notify the main view to show the main document content
                    var showDocumentMessage = new NotificationMessage(Notification.MainViewShowDocumentContent);
                    this.Messenger.Send(showDocumentMessage);
                }
                else
                {
                    // Add the entity to navigate to in the list of parameters for the next tree item selection handling. This will cause it to be selected when
                    // the view corresponding to stepItemToSelect will be loaded. See the HandleProcessStepItemSelection method for details.
                    this.NextTreeItemSelectionParams.Add(entity);

                    // Select the step item only if the current screen unload has succeeded
                    stepItemToSelect.Select();
                }
            }
            else if (!entityFound)
            {
                // Continue the search down the tree.
                foreach (var child in treeItem.Children)
                {
                    entityFound = this.NavigateToEntityInProcesses(child, entity, messageToken);
                    if (entityFound)
                    {
                        break;
                    }
                }
            }

            return entityFound;
        }

        /// <summary>
        /// Searches the result details item in the parent tree item and navigates to it if it is found.
        /// If founds it selects the tab corresponding to the cost type provided.
        /// </summary>
        /// <param name="treeItem">The parent tree item in which to search.</param>
        /// <param name="messageData">The message data containing the cost type used to navigate</param>
        /// <param name="messageToken">The message messageTokenToUse.</param>
        private void NavigateToEntityInResultDetails(TreeViewDataItem treeItem, object messageData, object messageToken)
        {
            if (treeItem == null || messageData == null)
            {
                return;
            }

            // Make sure treeItem is expanded and its children are loaded
            if (treeItem.LazyLoad != TreeViewItemLazyLoadMode.None && !treeItem.AreChildrenLoaded)
            {
                // If the root item is not loaded, expand it so it loads and then retry this call.
                Action<TreeViewDataItem> postExpandAction = null;
                postExpandAction = (e) =>
                {
                    treeItem.ChildrenLoaded -= postExpandAction;
                    NavigateToEntityInResultDetails(treeItem, messageData, messageToken);
                };
                treeItem.ChildrenLoaded += postExpandAction;
                treeItem.IsExpanded = true;
                return;
            }

            Type costType = messageData as Type;

            // Find the Result details child item of the root item.
            ResultDetailsTreeItem resultDetailsItem = treeItem.Children.OfType<ResultDetailsTreeItem>().FirstOrDefault();
            if (resultDetailsItem != null)
            {
                // If already selected select the tab for the cost type provided
                if (resultDetailsItem.IsSelected)
                {
                    Action<object> selector = (mainViewContent) =>
                    {
                        var resultDetails = mainViewContent as ResultDetailsViewModel;
                        if (resultDetails != null)
                        {
                            // Navigate to the right tab in result details view
                            resultDetails.NavigateToCost(costType);
                        }
                    };

                    var message = new NotificationMessageWithAction<object>(Notification.MainViewGetContent, selector);
                    this.Messenger.Send(message, messageToken);

                    // Notify the main view to show the main document content
                    var showDocumentMessage = new NotificationMessage(Notification.MainViewShowDocumentContent);
                    this.Messenger.Send(showDocumentMessage);
                }
                else
                {
                    // Add the cost to navigate to in the list of parameters for the next tree item selection handling. This will cause it to be selected when
                    // the view corresponding to ResultDetailsItemToSelect will be loaded. See the HandleResultDetailsItemSelection method for details.
                    this.NextTreeItemSelectionParams.Add(costType);

                    resultDetailsItem.Select();
                }
            }
        }

        #endregion Common logic for navigation to specified tree items

        #region Entity refresh

        /// <summary>
        /// Refreshes the entity in the specified data context.
        /// </summary>
        /// <param name="dataObject">The data object.</param>
        /// <param name="dataContext">The data context.</param>
        /// <param name="refreshChildren">if set to <c>true</c> [refresh children].</param>
        protected void RefreshEntity(object dataObject, IDataSourceManager dataContext, bool refreshChildren = false)
        {
            if (dataObject == null || dataContext == null)
            {
                return;
            }

            ProjectFolder folder = dataObject as ProjectFolder;
            if (folder != null)
            {
                var freshFolder = dataContext.ProjectFolderRepository.GetByKey(folder.Guid);
                if (freshFolder != null)
                {
                    dataContext.ProjectFolderRepository.Refresh(freshFolder);
                }

                return;
            }

            Project project = dataObject as Project;
            if (project != null)
            {
                var freshProject = dataContext.ProjectRepository.GetByKey(project.Guid);
                if (freshProject != null)
                {
                    dataContext.ProjectRepository.Refresh(freshProject);
                }

                return;
            }

            Assembly assembly = dataObject as Assembly;
            if (assembly != null)
            {
                var freshAssembly = dataContext.AssemblyRepository.GetByKey(assembly.Guid);
                if (freshAssembly != null)
                {
                    if (refreshChildren)
                    {
                        dataContext.AssemblyRepository.RefreshWithProcess(freshAssembly);
                    }
                    else
                    {
                        dataContext.AssemblyRepository.Refresh(freshAssembly);
                    }
                }

                return;
            }

            RawPart rawPart = dataObject as RawPart;
            if (rawPart != null)
            {
                var freshRawPart = dataContext.PartRepository.GetByKey(rawPart.Guid);
                if (freshRawPart != null)
                {
                    dataContext.PartRepository.Refresh(freshRawPart);
                }

                return;
            }

            Part part = dataObject as Part;
            if (part != null)
            {
                var freshPart = dataContext.PartRepository.GetByKey(part.Guid);
                if (freshPart != null)
                {
                    if (refreshChildren)
                    {
                        dataContext.PartRepository.RefreshWithProcess(freshPart);
                    }
                    else
                    {
                        dataContext.PartRepository.Refresh(freshPart);
                    }
                }

                return;
            }

            Machine machine = dataObject as Machine;
            if (machine != null)
            {
                var freshMachine = dataContext.MachineRepository.GetByKey(machine.Guid);
                if (freshMachine != null)
                {
                    dataContext.MachineRepository.Refresh(freshMachine);
                }

                return;
            }

            RawMaterial material = dataObject as RawMaterial;
            if (material != null)
            {
                var freshMaterial = dataContext.RawMaterialRepository.GetByKey(material.Guid);
                if (freshMaterial != null)
                {
                    dataContext.RawMaterialRepository.Refresh(freshMaterial);
                }

                return;
            }

            Commodity commodity = dataObject as Commodity;
            if (commodity != null)
            {
                var freshCommodity = dataContext.CommodityRepository.GetByKey(commodity.Guid);
                if (freshCommodity != null)
                {
                    dataContext.CommodityRepository.Refresh(freshCommodity);
                }

                return;
            }

            Consumable consumable = dataObject as Consumable;
            if (consumable != null)
            {
                var freshConsumable = dataContext.ConsumableRepository.GetByKey(consumable.Guid);
                if (freshConsumable != null)
                {
                    dataContext.ConsumableRepository.Refresh(freshConsumable);
                }

                return;
            }

            Die die = dataObject as Die;
            if (die != null)
            {
                var freshDie = dataContext.DieRepository.GetByKey(die.Guid);
                if (freshDie != null)
                {
                    dataContext.DieRepository.Refresh(freshDie);
                }

                return;
            }

            AssemblyProcessStep assemblyProcessStep = dataObject as AssemblyProcessStep;
            if (assemblyProcessStep != null)
            {
                var freshAssemblyProcessStep = dataContext.ProcessStepRepository.GetByKey(assemblyProcessStep.Guid);
                if (freshAssemblyProcessStep != null)
                {
                    dataContext.ProcessStepRepository.Refresh(freshAssemblyProcessStep);
                }

                return;
            }

            PartProcessStep partProcessStep = dataObject as PartProcessStep;
            if (partProcessStep != null)
            {
                var freshPartProcessStep = dataContext.ProcessStepRepository.GetByKey(partProcessStep.Guid);
                if (freshPartProcessStep != null)
                {
                    dataContext.ProcessStepRepository.Refresh(freshPartProcessStep);
                }

                return;
            }

            var processStepPartAmount = dataObject as ProcessStepPartAmount;
            if (processStepPartAmount != null)
            {
                var partAmountRepository = dataContext.Repository(typeof(ProcessStepPartAmount));
                var freshProcessStepPartAmount = partAmountRepository.GetByKey(processStepPartAmount.ProcessStepGuid, processStepPartAmount.PartGuid);
                if (freshProcessStepPartAmount != null)
                {
                    partAmountRepository.Refresh(freshProcessStepPartAmount);
                }

                return;
            }

            var processStepAssemblyAmount = dataObject as ProcessStepAssemblyAmount;
            if (processStepAssemblyAmount != null)
            {
                var assyAmountRepository = dataContext.Repository(typeof(ProcessStepAssemblyAmount));
                var freshProcessStepPartAmount = assyAmountRepository.GetByKey(processStepAssemblyAmount.ProcessStepGuid, processStepAssemblyAmount.AssemblyGuid);
                if (freshProcessStepPartAmount != null)
                {
                    assyAmountRepository.Refresh(freshProcessStepPartAmount);
                }

                return;
            }
        }

        /// <summary>
        /// Refreshes the entity which was permanently deleted from the trash bin and is still loaded in a context in the tree.
        /// </summary>
        /// <param name="entity">The entity which was permanently deleted from the db</param>
        /// <param name="rootItem">The root item of the entity.</param>
        protected void RefreshPermanentlyDeletedEntity(object entity, TreeViewDataItem rootItem)
        {
            List<IDataSourceManager> dataManagers = new List<IDataSourceManager>();
            Queue<TreeViewDataItem> queue = new Queue<TreeViewDataItem>();
            queue.Enqueue(rootItem.Children.Where(c => c.AreChildrenLoaded));

            while (queue.Count > 0)
            {
                TreeViewDataItem first = queue.Dequeue();

                if (first.DataObjectContext != null)
                {
                    dataManagers.Add(first.DataObjectContext);
                }

                if (first is ProjectFolderTreeItem || first is ProjectTreeItem)
                {
                    queue.Enqueue(first.Children.Where(c => c.AreChildrenLoaded));
                }
            }

            // refresh the entity on the context on which it is still present. this will erase all the references to that entity.
            foreach (IDataSourceManager dataManager in dataManagers)
            {
                var repository = dataManager.Repository(entity.GetType());
                object contextEntity = repository.GetByEntity(entity);
                if (contextEntity != null)
                {
                    // Detach the deleted entity from all data managers that contain it.
                    repository.DetachAll(contextEntity);
                }
            }
        }

        /// <summary>
        /// Refreshes the entity and references in the specified data context.
        /// </summary>
        /// <param name="dataObject">The data object.</param>
        /// <param name="dataContext">The data context.</param>
        protected void RefreshEntityAndRefrences(object dataObject, IDataSourceManager dataContext)
        {
            if (dataObject == null || dataContext == null)
            {
                return;
            }

            Project project = dataObject as Project;
            if (project != null)
            {
                dataContext.ProjectRepository.GetProjectIncludingTopLevelChildren(project.Guid);
                return;
            }

            Assembly assembly = dataObject as Assembly;
            if (assembly != null)
            {
                dataContext.AssemblyRepository.RefreshWithChildren(assembly);
                return;
            }

            Part part = dataObject as Part;
            if (part != null)
            {
                dataContext.PartRepository.RefreshWithChildren(part);
                return;
            }

            Process process = dataObject as Process;
            if (process != null)
            {
                dataContext.ProcessRepository.GetProcessWithSteps(process.Guid);
                return;
            }

            RawMaterial rawMaterial = dataObject as RawMaterial;
            if (rawMaterial != null)
            {
                dataContext.RawMaterialRepository.GetById(rawMaterial.Guid);
                return;
            }

            Commodity commodity = dataObject as Commodity;
            if (commodity != null)
            {
                dataContext.CommodityRepository.GetById(commodity.Guid);
                return;
            }

            Machine machine = dataObject as Machine;
            if (machine != null)
            {
                dataContext.MachineRepository.GetById(machine.Guid);
                return;
            }

            Consumable consumable = dataObject as Consumable;
            if (consumable != null)
            {
                dataContext.ConsumableRepository.GetById(consumable.Guid);
                return;
            }

            Die die = dataObject as Die;
            if (die != null)
            {
                dataContext.DieRepository.GetById(die.Guid);
                return;
            }

            var processStep = dataObject as ProcessStep;
            if (processStep != null)
            {
                dataContext.ProcessStepRepository.GetProcessStepFull(processStep.Guid);
                return;
            }
        }

        #endregion Entity refresh

        #region Explorer tree events handling

        /// <summary>
        /// Handler for the <see cref="TreePreviewMouseMoveCommand"/> command.
        /// </summary>
        /// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        private void HandleTreePreviewMouseMove(MouseEventArgs e)
        {
            this.TryStartDragOperation(e, this.SelectedTreeItem, this.dragStartPosition);
        }

        /// <summary>
        /// Handles the PreviewMouseDown event of the explorer tree view.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs" /> instance containing the event data.</param>
        /// <returns>
        /// The item over which the mouse down event occurred.
        /// </returns>
        protected virtual TreeViewDataItem HandleTreePreviewMouseDown(MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed && e.ClickCount == 1)
            {
                if (this.SelectedTreeItem != null)
                {
                    this.dragStartPosition = e.GetPosition(null);
                }
            }
            else if (e.ClickCount == 2)
            {
                this.dragStartPosition = null;
            }

            // If the expand button was clicked then the current main view screen is not unloaded so the user can
            // navigate down the tree
            DependencyObject source = e.OriginalSource as DependencyObject;
            if (source != null)
            {
                ToggleButton expander = UIHelper.FindParent<ToggleButton>(source);
                if (expander != null && expander.Name == "Expander")
                {
                    return null;
                }
            }

            // Find the clicked tree item and unload the current main view screen.
            TreeViewDataItem clickedItem = null;
            TreeViewItem parentItem = UIHelper.FindParent<TreeViewItem>(source);
            if (parentItem != null)
            {
                clickedItem = parentItem.DataContext as TreeViewDataItem;
            }

            // If the active window changed and the same item is selected handle the tree item selection again.
            if (!this.IsActive && (clickedItem == null || clickedItem == this.SelectedTreeItem))
            {
                // Signal the main view to notify its content that it will be unloaded.
                this.LoadSelectedTreeItemContent();
            }

            return clickedItem;
        }

        /// <summary>
        /// Handles the PreviewMouseUp event of the explorer tree.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void HandleTreePreviewMouseUp(MouseButtonEventArgs e)
        {
            this.dragStartPosition = null;
        }

        /// <summary>
        /// Handles the tree DragLeave event.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        private void HandleTreeDragLeave(DragEventArgs e)
        {
            var data = e.Data.GetData(typeof(DragAndDropData));
            if (data is DragAndDropTreeItemData)
            {
                DragAndDropHelper.RemoveInsertAdorner();
            }
            else
            {
                e.Effects = DragDropEffects.None;
            }

            e.Handled = true;
        }

        /// <summary>
        /// Handles the tree QueryContinueDrag event.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.QueryContinueDragEventArgs"/> instance containing the event data.</param>
        private void HandleTreeQueryContinueDrag(QueryContinueDragEventArgs e)
        {
            DragAndDropHelper.QueryContinueDrag(e);
        }

        /// <summary>
        /// Handles the TreeViewItemLoadingEvent event of the explorer tree.
        /// </summary>
        /// <param name="eventData">The <see cref="ZPKTool.Gui.Controls.TreeViewItemLoadingEventArgs"/> instance containing the event data.</param>
        private void HandleTreeLoadingItem(TreeViewItemLoadingEventArgs eventData)
        {
            if (eventData.IsLoading)
            {
                this.Messenger.Send(new NotificationMessage(this, Notification.DisableShellView));
            }
            else
            {
                this.Messenger.Send(new NotificationMessage(this, Notification.EnableShellView));
            }
        }

        #endregion Explorer tree events handling

        #region Drag and Drop

        /// <summary>
        /// Starts the drag operation for the tree item under the mouse, if it is possible or necessary.
        /// </summary>
        /// <param name="mouseMoveArgs">The <see cref="System.Windows.Input.MouseEventArgs"/> instance containing the event data from the PreviewMouseMove event.</param>
        /// <param name="selectedTreeItem">The selected tree item.</param>
        /// <param name="dragStartPosition">The drag start position.</param>
        private void TryStartDragOperation(MouseEventArgs mouseMoveArgs, TreeViewDataItem selectedTreeItem, Point? dragStartPosition)
        {
            // Check for the conditions necessary in order to start dragging the current tree item.

            // The left button must be pressed.
            if (mouseMoveArgs.LeftButton != MouseButtonState.Pressed)
            {
                return;
            }

            if (!dragStartPosition.HasValue)
            {
                return;
            }

            // The mouse must over a tree item, not other part of the tree view like a scroll bar.
            TreeViewItem parentItem = UIHelper.FindParent<TreeViewItem>(mouseMoveArgs.OriginalSource as DependencyObject);
            if (parentItem == null)
            {
                return;
            }

            // The selected item must be valid, not be read-only and its children must be loaded.
            if (selectedTreeItem == null
                || selectedTreeItem.DataObject == null
                || !selectedTreeItem.AreChildrenLoaded)
            {
                return;
            }

            // A minimum movement distance is required before starting the drag in order to avoid starting it when moving the mouse a few pixels during double click.
            Point currentMousePosition = mouseMoveArgs.GetPosition(null);
            double moveDeltaX = Math.Abs(currentMousePosition.X - dragStartPosition.Value.X);
            double moveDeltaY = Math.Abs(currentMousePosition.Y - dragStartPosition.Value.Y);
            if (moveDeltaX <= SystemParameters.MinimumHorizontalDragDistance + 8d
                && moveDeltaY <= SystemParameters.MinimumVerticalDragDistance + 8d)
            {
                return;
            }

            // The drag and drop of tree items is allowed only for the following items: Project, Assembly, Part, Process Step.
            // Note: in case of PartTreeItem the "is" was replaced with "GetType()" because "is" returns true for derived classes as well.
            if (selectedTreeItem is AssemblyTreeItem
                || selectedTreeItem.GetType() == typeof(PartTreeItem)
                || selectedTreeItem is ProcessStepTreeItem
                || selectedTreeItem is ProjectTreeItem)
            {
                // Start the drag & drop action.
                DragAndDropTreeItemData dragData = new DragAndDropTreeItemData(selectedTreeItem);
                DataObject data = new DataObject(typeof(DragAndDropData), dragData);
                DragDrop.DoDragDrop(parentItem, data, DragDropEffects.All);
            }
        }

        /// <summary>
        /// Handles the DragEnter event logic common to all explorer trees.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        /// <param name="parentItem">The parent item.</param>
        protected void HandleTreeDragEnterCommon(DragEventArgs e, TreeViewDataItem parentItem)
        {
            DragDropEffects operationEffects = DragDropEffects.None;
            bool eventHandled = true;

            DragAndDropData dataItem = e.Data.GetData(typeof(DragAndDropData)) as DragAndDropData;
            if (dataItem != null)
            {
                // Handle the dragging of tree items with the purpose of re-ordering them.
                DragAndDropTreeItemData treeItemDragData = dataItem as DragAndDropTreeItemData;
                if (treeItemDragData != null)
                {
                    ItemsControl dragAdornersTarget = null;
                    bool valid = this.ValidateTreeItemDragOperation(treeItemDragData, e, out dragAdornersTarget, parentItem);
                    if (valid)
                    {
                        DragAndDropHelper.InitializeInsertAdorner(dragAdornersTarget, e);
                        operationEffects = DragDropEffects.Copy;
                        eventHandled = false;
                    }
                }
                else
                {
                    // Handle the dragging of data over the tree with the purpose of adding it to a tree item.
                    bool dropAccepted = this.ValidateDataDragOperation(dataItem, e);
                    if (dropAccepted)
                    {
                        operationEffects = DragDropEffects.Copy;
                    }
                }
            }

            e.Effects = operationEffects;
            e.Handled = eventHandled;
        }

        /// <summary>
        /// Handles the DragOver event logic common to all explorer trees.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        /// <param name="parentItem">The parent item.</param>
        protected void HandleTreeDragOverCommon(DragEventArgs e, TreeViewDataItem parentItem)
        {
            DragDropEffects operationEffects = DragDropEffects.None;

            DragAndDropData dataItem = e.Data.GetData(typeof(DragAndDropData)) as DragAndDropData;
            if (dataItem != null)
            {
                // Handle the dragging of tree items with the purpose of re-ordering them.
                DragAndDropTreeItemData treeItemDragData = dataItem as DragAndDropTreeItemData;
                if (treeItemDragData != null)
                {
                    ItemsControl dragAdornersTarget = null;
                    bool valid = this.ValidateTreeItemDragOperation(treeItemDragData, e, out dragAdornersTarget, parentItem);
                    if (valid)
                    {
                        DragAndDropHelper.UpdateInsertAdorner(dragAdornersTarget, e);
                        DragAndDropHelper.HandleDragScrolling(dragAdornersTarget, e);
                        operationEffects = DragDropEffects.Copy;
                    }
                }
                else
                {
                    // Handle the dragging of data over the tree with the purpose of adding it to a tree item.
                    bool dropAccepted = this.ValidateDataDragOperation(dataItem, e);
                    if (dropAccepted)
                    {
                        operationEffects = DragDropEffects.Copy;
                    }
                }
            }

            e.Effects = operationEffects;
            e.Handled = true;
        }

        /// <summary>
        /// Handles the Drop event logic common to all explorer trees.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        /// <param name="selectedTreeItem">The selected tree item.</param>
        protected void HandleTreeDropCommon(DragEventArgs e, TreeViewDataItem selectedTreeItem)
        {
            e.Effects = DragDropEffects.None;
            e.Handled = true;
            DragAndDropHelper.RemoveAdorners();

            // Find the tree view item that received the drop, and its corresponding tree item.
            TreeViewItem targetTreeViewItem = UIHelper.FindParent<TreeViewItem>(e.OriginalSource as DependencyObject);
            if (targetTreeViewItem == null)
            {
                return;
            }

            TreeViewDataItem targetTreeItem = targetTreeViewItem.DataContext as TreeViewDataItem;
            if (targetTreeItem == null || targetTreeItem.ReadOnly)
            {
                return;
            }

            DragAndDropData droppedData = (DragAndDropData)e.Data.GetData(typeof(DragAndDropData));
            if (droppedData != null)
            {
                DragAndDropTreeItemData droppedTreeItem = droppedData as DragAndDropTreeItemData;
                if (droppedTreeItem != null)
                {
                    this.HandleDropForChildReordering(droppedTreeItem, targetTreeItem, targetTreeViewItem, e);
                }
                else
                {
                    this.HandleDataObjectDrop(selectedTreeItem, targetTreeItem, droppedData);
                }
            }
        }

        /// <summary>
        /// Validates the drag operation of a generic a data item.
        /// </summary>
        /// <param name="dataItem">The data item.</param>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the drag event data.</param>
        /// <returns>True if the drag operation is valid (the data item is accepted); otherwise, false.</returns>
        private bool ValidateDataDragOperation(DragAndDropData dataItem, DragEventArgs e)
        {
            if (dataItem == null || dataItem.DataObject == null)
            {
                return false;
            }

            TreeViewItem treeItem = UIHelper.FindParent<TreeViewItem>(e.OriginalSource as DependencyObject);
            if (treeItem == null)
            {
                return false;
            }

            TreeViewDataItem treeDataItem = treeItem.DataContext as TreeViewDataItem;
            if (treeDataItem == null || treeDataItem.ReadOnly)
            {
                return false;
            }

            IMasterDataObject masterObj = treeDataItem.DataObject as IMasterDataObject;
            if (masterObj != null && masterObj.IsMasterData)
            {
                return false;
            }

            if (treeDataItem.DataObject != null)
            {
                // Check if the data object is a process step and if its accuracy is not estimated
                // If the accuracy is estimated then the process step doesn't allow adding any entities
                var step = treeDataItem.DataObject as ProcessStep;
                if (step != null)
                {
                    var accuracy = EntityUtils.ConvertToEnum(step.Accuracy, ProcessCalculationAccuracy.Calculated);
                    if (accuracy != ProcessCalculationAccuracy.Calculated)
                    {
                        return false;
                    }
                }

                // Check if the tree item over which the drag is accepts the dragged data object.
                return ClipboardManager.Instance.CheckIfEntityAcceptsObject(treeDataItem.DataObject.GetType(), dataItem.DataObject.GetType());
            }
            else
            {
                var dataObjType = dataItem.DataObject.GetType();

                if ((treeDataItem is MyProjectsTreeItem && dataObjType == typeof(Project)) ||
                    (treeDataItem is SubAssembliesTreeItem && dataObjType == typeof(Assembly)) ||
                    (treeDataItem is SubPartsTreeItem && dataObjType == typeof(Part)) ||
                    (treeDataItem is MaterialsTreeItem && (dataObjType == typeof(RawMaterial) || dataObjType == typeof(Commodity) || dataObjType == typeof(RawPart))))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Validates the drag operation of a tree item.
        /// </summary>
        /// <param name="itemDragData">The item drag data.</param>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        /// <param name="dragAdornersTarget">The items control where to insert the drag adorners if the operation is valid; if the operation is invalid it is set to null.</param>
        /// <param name="parentItem">The parent item.</param>
        /// <returns>True if the drag operation is valid; otherwise returns false.</returns>
        private bool ValidateTreeItemDragOperation(DragAndDropTreeItemData itemDragData, DragEventArgs e, out ItemsControl dragAdornersTarget, TreeViewDataItem parentItem)
        {
            dragAdornersTarget = null;
            TreeView tree = e.Source as TreeView;

            if (itemDragData == null
                || itemDragData.DataObject == null
                || tree == null)
            {
                return false;
            }

            if (itemDragData.DataObject is ProcessStep
                || itemDragData.DataObject is Assembly
                || itemDragData.DataObject is Part)
            {
                // Find the tree item of the dragged data object.
                var treeItem = this.FindItemOfDataObject(itemDragData.DataObject, parentItem);
                if (treeItem != null && tree.Parent != null)
                {
                    // Find the TreeViewItem corresponding to the tree item's parent.
                    // This is necessary for determining the tree view item over which the drag operation is and for showing the drag associated adorners.
                    TreeViewItem parentTreeViewItem = this.FindContainerOfTreeItem(tree, treeItem.Parent);
                    if (parentTreeViewItem != null)
                    {
                        // Find the tree view item over which the drag operation is now.
                        TreeViewItem hoveredTreeViewItem = DragAndDropHelper.GetItemFromPoint(parentTreeViewItem, e.GetPosition(parentTreeViewItem));
                        if (hoveredTreeViewItem != null)
                        {
                            TreeViewDataItem hoveredTreeItem =
                                parentTreeViewItem.ItemContainerGenerator.ItemFromContainer(hoveredTreeViewItem) as TreeViewDataItem;
                            Type dropDataType = itemDragData.DataObject.GetType();

                            if (hoveredTreeItem != null)
                            {
                                bool itemIsInMyProjects = false;
                                if (parentItem is MyProjectsTreeItem)
                                {
                                    var walker = hoveredTreeItem;
                                    while (walker != null && !(itemIsInMyProjects = walker is MyProjectsTreeItem))
                                    {
                                        walker = walker.Parent;
                                    }
                                }
                                else
                                {
                                    itemIsInMyProjects = true;
                                }

                                if (itemIsInMyProjects
                                    && !hoveredTreeItem.ReadOnly
                                    && hoveredTreeItem.DataObject != null
                                    && hoveredTreeItem.DataObject.GetType() == dropDataType
                                    && parentTreeViewItem.Items.Contains(hoveredTreeItem))
                                {
                                    dragAdornersTarget = parentTreeViewItem;
                                    return true;
                                }
                            }
                        }
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Handles the drop of a data object on a tree item.
        /// </summary>
        /// <param name="selectedTreeItem">The selected tree item.</param>
        /// <param name="targetTreeItem">The target tree item.</param>
        /// <param name="droppedData">The dropped data.</param>
        private void HandleDataObjectDrop(TreeViewDataItem selectedTreeItem, TreeViewDataItem targetTreeItem, DragAndDropData droppedData)
        {
            if (targetTreeItem == null || droppedData == null)
            {
                return;
            }

            // Determine the data object that will receive the dropped data.
            // For tree items that have a backing data object it will be that object; for items representing groups it will be that backing data object of their parent item.
            object recipient = null;
            if (targetTreeItem is SubAssembliesTreeItem
                || targetTreeItem is SubPartsTreeItem
                || targetTreeItem is MaterialsTreeItem)
            {
                recipient = targetTreeItem.Parent.DataObject;
                targetTreeItem = targetTreeItem.Parent;
            }
            else if (targetTreeItem is MyProjectsTreeItem)
            {
                recipient = targetTreeItem;
            }
            else
            {
                recipient = targetTreeItem.DataObject;
            }

            if (recipient == null)
            {
                log.Error("Failed to determine the recipient data object for a tree data drop operation.");
                return;
            }

            IDataSourceManager dataContext = null;

            // Add the dropped object to the recipient through the Copy-Paste system.
            // Whether the dropped item is accepted by the recipient is decided when the drag entered the item so it is not necessary to check that here.
            ClipboardManager.Instance.Copy(droppedData.DataObject, droppedData.DataObjectContext);

            // Before adding the dropped object into an assembly, offer the user to upgrade its calculation version if is older that that of the assembly.
            bool upgradeToTargetCalculationVersion = false;
            if (targetTreeItem is AssemblyTreeItem)
            {
                Assembly targetAssy = (Assembly)targetTreeItem.DataObject;
                object clipboardObj = ClipboardManager.Instance.PeekClipboardObjects.First();
                bool clipboardObjHasCalculationVersion = false;
                string clipboardObjCalculationVersion = null;

                Part clipboardPart = clipboardObj as Part;
                if (clipboardPart != null)
                {
                    clipboardObjCalculationVersion = clipboardPart.CalculationVariant;
                    clipboardObjHasCalculationVersion = true;
                }
                else
                {
                    Assembly clipboardAssy = clipboardObj as Assembly;
                    if (clipboardAssy != null)
                    {
                        clipboardObjCalculationVersion = clipboardAssy.CalculationVariant;
                        clipboardObjHasCalculationVersion = true;
                    }
                }

                // If the clipboard obj. has a calculation version and the calculation version is null than the calculation version is set to the oldest version.
                if (clipboardObjHasCalculationVersion
                    && clipboardObjCalculationVersion == null)
                {
                    clipboardObjCalculationVersion = CostCalculatorFactory.OldestVersion;
                }

                if (clipboardObjHasCalculationVersion &&
                    CostCalculatorFactory.IsNewer(targetAssy.CalculationVariant, clipboardObjCalculationVersion))
                {
                    var answer = this.WindowService.MessageDialogService.Show(LocalizedResources.Paste_UpgradeCalculationVariant, MessageDialogType.YesNo);
                    if (answer == MessageDialogResult.Yes)
                    {
                        upgradeToTargetCalculationVersion = true;
                    }
                }
            }

            // The copy action is called depending on the drop target.
            PastedData pastedObject = null;
            Action<PleaseWaitService.WorkParams> work = (workParams) =>
            {
                bool addStepSubEntityToContext = targetTreeItem is ProcessStepTreeItem && targetTreeItem != selectedTreeItem;
                pastedObject = ClipboardManager.Instance.Paste(recipient, dataContext, upgradeToTargetCalculationVersion, addStepSubEntityToContext).First();

                // Save changes if you paste into a process step that is not selected and not loaded in the screen.
                if (addStepSubEntityToContext)
                {
                    dataContext.SaveChanges();
                }
            };

            Action<PleaseWaitService.WorkParams> workCompleted = (workParams) =>
            {
                if (workParams.Error != null)
                {
                    this.WindowService.MessageDialogService.Show(workParams.Error);
                }
                else if (pastedObject.Error != null)
                {
                    this.WindowService.MessageDialogService.Show(pastedObject.Error);
                }
                else
                {
                    var processStepRecipient = recipient as ProcessStep;
                    if (processStepRecipient != null)
                    {
                        // Send a message notifying a sub-entity of process step was created.
                        var stepMsg = new ProcessStepChangedMessage(this, recipient)
                        {
                            StepID = processStepRecipient.Guid,
                            SubEntity = pastedObject.Entity,
                            ChangeType = ProcessStepChangeType.SubEntityAdded
                        };
                        this.Messenger.Send(stepMsg, GlobalMessengerTokens.MainViewTargetToken);
                    }

                    // Notify the trees that an object has been created.
                    EntityChangedMessage msg = new EntityChangedMessage(Notification.MyProjectsEntityChanged, pastedObject.Entity, recipient, EntityChangeType.EntityCreated);
                    this.Messenger.Send(msg);
                }
            };

            // If the target item has lazy loading enabled and has not been loaded yet we should use a new data context because his will be recreated during child loading.
            if (targetTreeItem.LazyLoad != TreeViewItemLazyLoadMode.None && !targetTreeItem.AreChildrenLoaded)
            {
                // If the target node is not loaded yet we force the load by expanding it and we copy the dropped data after the loading is performed.
                // At this point the data context and the recipient can be taken from the target node.
                Action<TreeViewDataItem> loadedAction = null;
                loadedAction = (e) =>
                {
                    targetTreeItem.ChildrenLoaded -= loadedAction;
                    dataContext = targetTreeItem.DataObjectContext;

                    if (!(targetTreeItem is MyProjectsTreeItem))
                    {
                        recipient = targetTreeItem.DataObject;
                    }

                    this.PleaseWaitService.Show(LocalizedResources.General_WaitWhileDrop, work, workCompleted);
                };

                targetTreeItem.ChildrenLoaded += loadedAction;
                targetTreeItem.IsExpanded = true;
            }
            else
            {
                // Else the data context of the object is already loaded and we have to perform the copy on the current context.
                dataContext = targetTreeItem.DataObjectContext;

                this.PleaseWaitService.Show(LocalizedResources.General_WaitWhileDrop, work, workCompleted);
            }
        }

        /// <summary>
        /// Handles the drop of a tree item with the purpose of changing its position within its parent.
        /// </summary>
        /// <param name="droppedData">The data identifying the dropped tree item.</param>
        /// <param name="targetTreeItem">The tree item over which the drop occurred.</param>
        /// <param name="targetTreeViewItem">The tree view item over which the drop occurred (the tree view item corresponding to the targetTreeItem).</param>
        /// <param name="dropEventArgs">The <see cref="System.Windows.DragEventArgs"/> instance containing the drop event data.</param>
        private void HandleDropForChildReordering(
            DragAndDropTreeItemData droppedData,
            TreeViewDataItem targetTreeItem,
            TreeViewItem targetTreeViewItem,
            DragEventArgs dropEventArgs)
        {
            TreeViewDataItem droppedItem = droppedData.DraggedTreeItem;
            if (droppedItem == targetTreeItem)
            {
                return;
            }

            TreeViewDataItem parentItem = targetTreeItem.Parent;
            if (parentItem == null)
            {
                log.Error("Failed to determine the parent item in which the reordering should be performed.");
                return;
            }

            if (!parentItem.Children.Contains(droppedItem))
            {
                log.Error("The dropped tree item is not a child of the tree item in which the reordering takes place.");
                return;
            }

            TreeViewItem parentTreeViewItem = UIHelper.FindParent<TreeViewItem>(targetTreeViewItem);
            if (parentTreeViewItem == null)
            {
                log.Error("Failed to determine the parent tree view item in which the reordering should be performed.");
                return;
            }

            // Determine the position on which to put the dropped item.
            int? insertionIndex = DragAndDropHelper.FindInsertionIndex(parentTreeViewItem, dropEventArgs);
            if (!insertionIndex.HasValue)
            {
                log.Error("Failed to determine the position where the dropped tree item should be inserted.");
                return;
            }

            var parentDataContext = parentItem.DataObjectContext;
            List<TreeViewDataItem> updatedItems = new List<TreeViewDataItem>();
            int crtPosition = 0;

            // Refresh the order of the items that come before the insertion position.
            for (int i = 0; i < insertionIndex.Value; i++)
            {
                var itemContainer = parentTreeViewItem.ItemContainerGenerator.ContainerFromIndex(i);
                TreeViewDataItem item = parentTreeViewItem.ItemContainerGenerator.ItemFromContainer(itemContainer) as TreeViewDataItem;
                if (item == droppedItem)
                {
                    continue;
                }

                if (item != null)
                {
                    bool posSet = this.SetTreeItemPositionForChildReordering(item, crtPosition, item.DataObjectContext);
                    crtPosition++;
                    if (posSet)
                    {
                        updatedItems.Add(item);
                    }
                }
            }

            // Set the dropped item on the corresponding insertion position.
            this.SetTreeItemPositionForChildReordering(droppedItem, crtPosition, droppedItem.DataObjectContext);
            updatedItems.Add(droppedItem);
            crtPosition++;

            // Rebuild the order of items after the insertion position of the dropped item.
            for (int i = insertionIndex.Value; i < parentTreeViewItem.Items.Count; i++)
            {
                var itemContainer = parentTreeViewItem.ItemContainerGenerator.ContainerFromIndex(i);
                TreeViewDataItem item = parentTreeViewItem.ItemContainerGenerator.ItemFromContainer(itemContainer) as TreeViewDataItem;
                if (item == droppedItem)
                {
                    continue;
                }

                if (item != null)
                {
                    bool posSet = this.SetTreeItemPositionForChildReordering(item, crtPosition, item.DataObjectContext);
                    crtPosition++;
                    if (posSet)
                    {
                        updatedItems.Add(item);
                    }
                }
            }

            // Commit the position (index) changes made to the tree items underlying data objects.
            // First commit the changes made to data objects that belong to their parent's context.
            parentDataContext.SaveChanges();

            // Refresh the child items and their view.
            parentItem.RefreshChildrenView();

            // Next, commit the changes made to data objects that have their own data context and refresh their corresponding instances from the parent's data context.
            foreach (var item in updatedItems)
            {
                IDataSourceManager itemDataContext = item.DataObjectContext;
                if (itemDataContext != parentDataContext)
                {
                    itemDataContext.SaveChanges();
                    this.RefreshDataObjectForChildReordering(item.DataObject, itemDataContext);
                }
            }

            // The sent entity is the first children of the parent because the parent could have the data object null.
            // TODO: find a better way to detect where to send the message.
            if (this.GetType() == typeof(ProjectsTreeViewModel))
            {
                var msg = new EntityChangedMessage(Notification.MyProjectsEntityChanged, parentItem.Children[0].DataObject, null, EntityChangeType.EntityReordered);
                this.Messenger.Send<EntityChangedMessage, BookmarksTreeViewModel>(msg);
            }
            else
            {
                var msg = new EntityChangedMessage(Notification.MyProjectsEntityChanged, parentItem.Children[0].DataObject, null, EntityChangeType.EntityReordered);
                this.Messenger.Send(msg);
            }
        }

        /// <summary>
        /// Sets a new position for a tree item (and its underlying data object) in its parent.
        /// This method is used during the child reordering process triggered by a drop event.
        /// </summary>
        /// <param name="item">The item to reposition.</param>
        /// <param name="newPosition">The new position.</param>
        /// <param name="dataContext">The data context to be used for setting the position.</param>
        /// <returns>True if the item's position was set to the new position; otherwise, false ().</returns>
        private bool SetTreeItemPositionForChildReordering(TreeViewDataItem item, int newPosition, IDataSourceManager dataContext)
        {
            bool positionSet = false;

            Assembly assy = item.DataObject as Assembly;
            if (assy != null)
            {
                assy = dataContext.AssemblyRepository.GetByKey(assy.Guid);
                assy.Index = newPosition;

                // Set the new ref. as the item data object.
                item.DataObject = assy;
                positionSet = true;
            }
            else
            {
                Part part = item.DataObject as Part;
                if (part != null)
                {
                    part = dataContext.PartRepository.GetByKey(part.Guid);
                    part.Index = newPosition;

                    // Set the new ref. as the item data object.
                    item.DataObject = part;
                    positionSet = true;
                }
                else
                {
                    ProcessStep step = item.DataObject as ProcessStep;
                    if (step != null)
                    {
                        step = dataContext.ProcessStepRepository.GetByKey(step.Guid);
                        step.Index = newPosition;

                        // Set the new ref. as the item data object.
                        item.DataObject = step;
                        positionSet = true;
                    }
                }
            }

            return positionSet;
        }

        /// <summary>
        /// Refreshes the specified data object from the specified data context.
        /// This method is used during the child reordering process triggered by a drop event.
        /// </summary>
        /// <param name="dataObject">The data object to refresh.</param>
        /// <param name="dataContext">The data context.</param>
        private void RefreshDataObjectForChildReordering(object dataObject, IDataSourceManager dataContext)
        {
            Assembly assy = dataObject as Assembly;
            if (assy != null)
            {
                dataContext.AssemblyRepository.Refresh(assy);
            }
            else
            {
                Part part = dataObject as Part;
                if (part != null)
                {
                    dataContext.PartRepository.Refresh(part);
                }
                else
                {
                    ProcessStep step = dataObject as ProcessStep;
                    if (step != null)
                    {
                        dataContext.ProcessStepRepository.Refresh(step);
                    }
                }
            }
        }

        #endregion Drag and Drop

        #region Helpers

        /// <summary>
        /// Searches in the entire tree for the item corresponding to a specified data object.
        /// </summary>
        /// <param name="dataObject">The data object.</param>
        /// <param name="item">The item.</param>
        /// <returns>The item or null if not found.</returns>
        protected TreeViewDataItem FindItemOfDataObject(object dataObject, TreeViewDataItem item)
        {
            if (item == null)
            {
                return null;
            }

            TreeViewDataItem result = null;
            result = item.FindItemForDataObject(dataObject);
            return result;
        }

        /// <summary>
        /// Finds the Project item in whose sub-tree a specified item is located.
        /// </summary>
        /// <param name="item">The item for which to search for the parent project item.</param>
        /// <returns>The project item or null if it doesn't exist.</returns>
        private ProjectTreeItem FindParentProjectItem(TreeViewDataItem item)
        {
            if (item != null)
            {
                ProjectTreeItem projectItem = item as ProjectTreeItem;
                if (projectItem != null)
                {
                    return projectItem;
                }
                else if (item.Parent != null)
                {
                    return this.FindParentProjectItem(item.Parent);
                }
            }

            return null;
        }

        /// <summary>
        /// Searches for the visual container (TreeViewItem) of a specified tree item in the tree.
        /// </summary>
        /// <param name="tree">The tree.</param>
        /// <param name="item">The tree view data item.</param>
        /// <returns>The container of the item or null if it is not found.</returns>
        private TreeViewItem FindContainerOfTreeItem(ItemsControl tree, TreeViewDataItem item)
        {
            // List the parents of the items in hierarchical order up to the root.
            List<TreeViewDataItem> parentHierarchy = new List<TreeViewDataItem>();
            TreeViewDataItem walker = item.Parent;
            while (walker != null)
            {
                parentHierarchy.Insert(0, walker);
                walker = walker.Parent;
            }

            // Search the item's container from the root down by finding the container of each parent and continuing the search in that container.
            ItemsControl root = tree;
            while (parentHierarchy.Count > 0 && root != null)
            {
                root = root.ItemContainerGenerator.ContainerFromItem(parentHierarchy[0]) as ItemsControl;
                parentHierarchy.RemoveAt(0);
            }

            if (root != null)
            {
                return root.ItemContainerGenerator.ContainerFromItem(item) as TreeViewItem;
            }

            return null;
        }

        #endregion Helpers
    }
}