﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Data;
using ZPKTool.Gui.Controls;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The drag and drop data for an item dragged from the tree.
    /// </summary>    
    public class DragAndDropTreeItemData : DragAndDropData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DragAndDropTreeItemData"/> class.
        /// </summary>
        public DragAndDropTreeItemData()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DragAndDropTreeItemData"/> class.
        /// </summary>
        /// <param name="draggedItem">The dragged item.</param>
        public DragAndDropTreeItemData(TreeViewDataItem draggedItem)
        {
            if (draggedItem == null)
            {
                throw new ArgumentNullException("draggedItem", "The dragged item was null.");
            }

            this.DraggedTreeItem = draggedItem;
            this.DataObject = draggedItem.DataObject;
            this.DataObjectContext = draggedItem.DataObjectSource;
        }

        /// <summary>
        /// Gets the dragged tree item.
        /// </summary>
        public TreeViewDataItem DraggedTreeItem { get; private set; }
    }
}
