﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using ZPKTool.Business;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;
using ZPKTool.Synchronization;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for the Projects Tree.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ProjectsTreeViewModel : ProjectsExplorerBaseViewModel
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The online check service.
        /// </summary>
        private IOnlineCheckService onlineChecker;

        /// <summary>
        /// The security service.
        /// </summary>
        private ISecurityService securityService;

        /// <summary>
        /// The edited master data assembly that needs to be selected after the parent children have loaded.
        /// </summary>
        private Assembly editedMasterDataAssemblyToSelect;

        /// <summary>
        /// The text entered in the search TextBox
        /// </summary>
        private string textToSearchInTree;

        /// <summary>
        /// Gets or sets a Filter predicate, applied on TreeView Items.
        /// </summary>
        public VMProperty<Predicate<object>> TreeFilter { get; set; }

        #endregion Attributes

        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectsTreeViewModel" /> class.
        /// </summary>
        /// <param name="messenger">The messenger.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="compositionContainer">The composition container.</param>
        /// <param name="pleaseWaitService">The please wait service</param>
        /// <param name="unitsService">The units service.</param>
        /// <param name="modelBrowserHelperService">The model browser helper service.</param>
        /// <param name="dataService">The data service.</param>
        /// <param name="importService">The import service.</param>
        /// <param name="securityService">The security service.</param>
        [ImportingConstructor]
        public ProjectsTreeViewModel(
            IMessenger messenger,
            IWindowService windowService,
            CompositionContainer compositionContainer,
            IPleaseWaitService pleaseWaitService,
            IUnitsService unitsService,
            IModelBrowserHelperService modelBrowserHelperService,
            IProjectsExplorerDataService dataService,
            IImportService importService,
            ISecurityService securityService)
            : base(compositionContainer, messenger, windowService, pleaseWaitService, unitsService, modelBrowserHelperService, dataService, importService)
        {
            this.onlineChecker = compositionContainer.GetExportedValue<IOnlineCheckService>();
            this.securityService = securityService;

            this.InitializeProjectsTree();
            this.InitializeCommands();

            TreeFilter.Value = (item) =>
            {
                var treeItem = item as TreeViewDataItem;
                if (treeItem != null
                    && !string.IsNullOrWhiteSpace(this.TextToSearchInTree)
                    && (treeItem.Label == null || !treeItem.Label.StartsWith(this.TextToSearchInTree, StringComparison.CurrentCultureIgnoreCase)))
                {
                    return false;
                }

                return true;
            };
        }

        #region Commands

        /// <summary>
        /// Gets the command executed for the DragEnter event of the Projects Tree.
        /// </summary>
        public ICommand ProjectsTreeDragEnterCommand { get; private set; }

        /// <summary>
        /// Gets the command executed for the DragOver event of the Projects Tree.
        /// </summary>
        public ICommand ProjectsTreeDragOverCommand { get; private set; }

        /// <summary>
        /// Gets the command executed for the Drop event of the Projects Tree.
        /// </summary>
        public ICommand ProjectsTreeDropCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets or sets the text entered in the search TextBox.
        /// </summary>
        public string TextToSearchInTree
        {
            get
            {
                return this.textToSearchInTree;
            }

            set
            {
                var oldValue = this.textToSearchInTree;
                this.SetProperty(ref this.textToSearchInTree, value, () => this.TextToSearchInTree);
            }
        }

        #endregion Properties

        /// <summary>
        /// Called after the view has been loaded. Usually it performs some initialization that needs the view to be loaded, like
        /// starting to load data from a database.
        /// <para />
        /// During unit tests this method must be manually called because there is no view to call it.
        /// </summary>
        public override void OnLoaded()
        {
            base.OnLoaded();

            this.RegisterMessageHandlers();
        }

        #region Initialization

        /// <summary>
        /// Initializes the projects tree.
        /// </summary>
        private void InitializeProjectsTree()
        {
            // Initialize the pre-defined tree items.
            ObservableCollection<TreeViewDataItem> predefinedItems = new ObservableCollection<TreeViewDataItem>();

            // Initialize the Released Projects item
            ReleasedProjectsTreeItem releasedProjectsItem = new ReleasedProjectsTreeItem(this.DataService);
            predefinedItems.Add(releasedProjectsItem);

            // Initialize My Projects and Trash Bin items
            if (SecurityManager.Instance.CurrentUserHasRight(Right.ProjectsAndCalculate))
            {
                var myProjectsItem = new MyProjectsTreeItem(DataService);
                myProjectsItem.AutomationId = "MyProjectsTreeItem";
                predefinedItems.Add(myProjectsItem);

                TrashBinTreeItem trashBinItem = new TrashBinTreeItem();
                predefinedItems.Add(trashBinItem);
            }

            // Initialize the Other Users Projects item
            if (!(SecurityManager.Instance.CurrentUserHasRole(Role.Viewer) || SecurityManager.Instance.CurrentUserHasRole(Role.DataManager)))
            {
                OtherUsersProjectsTreeItem otherUsersProjectsItem = new OtherUsersProjectsTreeItem(this.DataService);
                otherUsersProjectsItem.AutomationId = "OthetUsersProjectsTreeItem";
                predefinedItems.Add(otherUsersProjectsItem);
            }

            // Initialize the Master Data item
            if (SecurityManager.Instance.CurrentUserHasRight(Right.EditMasterData))
            {
                MasterDataTreeItem masterDataItem = new MasterDataTreeItem();
                predefinedItems.Add(masterDataItem);
            }

            // Initialize the Admin item
            if (SecurityManager.Instance.CurrentUserHasRight(Right.AdminTasks))
            {
                // Add the Administration item and its sub-tree
                SimpleProjectsTreeItem adminItem = new SimpleProjectsTreeItem()
                {
                    Label = LocalizedResources.General_Admin,
                    IconResourceKey = Images.AdminIconKey,
                    Uid = ProjectsTreeItemUid.Admin,
                    AutomationId = "Admin"
                };
                adminItem.SortDescriptions.Clear();
                predefinedItems.Add(adminItem);

                ManageUsersTreeItem manageUsersItem = new ManageUsersTreeItem();
                manageUsersItem.AutomationId = "AdminManageUsers";
                adminItem.Children.Add(manageUsersItem);

                SimpleProjectsTreeItem manageProjectsItem = new SimpleProjectsTreeItem();
                manageProjectsItem.Label = LocalizedResources.General_ManageProjects;
                manageProjectsItem.IconResourceKey = Images.ManageProjectsIconKey;
                manageProjectsItem.Uid = ProjectsTreeItemUid.ManageProjects;
                manageProjectsItem.AutomationId = "AdminManageProjects";
                adminItem.Children.Add(manageProjectsItem);

                ManageReleasedProjectsTreeItem manageReleasedProjects = new ManageReleasedProjectsTreeItem();
                adminItem.Children.Add(manageReleasedProjects);

                SimpleProjectsTreeItem logsItem = new SimpleProjectsTreeItem();
                logsItem.Label = LocalizedResources.General_EventLog;
                logsItem.IconResourceKey = Images.EditIconKey;
                logsItem.Uid = ProjectsTreeItemUid.EventLog;
                logsItem.AutomationId = "AdminManageLogs";
                adminItem.Children.Add(logsItem);

                ManageSettingsTreeItem manageSettingsItem = new ManageSettingsTreeItem();
                adminItem.Children.Add(manageSettingsItem);
            }

            this.Items.AddRange(predefinedItems);

            // Set the initial online icon on the items that require that.
            this.HandleOnlineStateChanged(new OnlineStateChangedMessage(this.onlineChecker.IsOnline));
        }

        /// <summary>
        /// Initializes the commands.
        /// </summary>
        private void InitializeCommands()
        {
            this.ProjectsTreeDragEnterCommand = new DelegateCommand<DragEventArgs>(this.HandleProjectsTreeDragEnter);
            this.ProjectsTreeDragOverCommand = new DelegateCommand<DragEventArgs>(this.HandleProjectsTreeDragOver);
            this.ProjectsTreeDropCommand = new DelegateCommand<DragEventArgs>(this.HandleProjectsTreeTreeDrop);
        }

        /// <summary>
        /// Registers the necessary message handlers with the IMessenger service.
        /// </summary>
        private void RegisterMessageHandlers()
        {
            this.Messenger.Register<EntityChangedMessage>(this.HandleEntityChangedMessage);
            this.Messenger.Register<EntitiesChangedMessage>(this.HandleEntitiesChangedMessage);
            this.Messenger.Register<NavigateToEntityMessage>(this.HandleNavigateToEntityMessage, GlobalMessengerTokens.MainViewTargetToken);
            this.Messenger.Register<SynchronizationFinishedMessage>(this.HandleSynchronizationFinishedMessage);
            this.Messenger.Register<OnlineStateChangedMessage>(this.HandleOnlineStateChanged);
            this.Messenger.Register<EditMasterDataEntityMessage>(this.HandleEditMasterDataEntityMessage);
        }

        #endregion Initialization

        #region Selected item handling

        /// <summary>
        /// When overridden in a derived class, this method allows to add custom (extra) logic to the tree item selection handler (HandleTreeItemSelection method).
        /// This logic is executed after the base logic contained in HandleTreeItemSelection.
        /// </summary>
        /// <param name="selectedItem">The item for which to handle the selection.</param>
        /// <returns>
        /// True if the item selection was handled by the method; otherwise, false.
        /// </returns>
        protected override bool HandleTreeItemSelectionHook(TreeViewDataItem selectedItem)
        {
            // Trash Bin screen.
            var trashBinItem = selectedItem as TrashBinTreeItem;
            if (trashBinItem != null)
            {
                var content = this.CompositionContainer.GetExportedValue<TrashBinViewModel>();
                this.Messenger.Send(new LoadContentInMainViewMessage(content, LocalizedResources.General_TrashBin), this.MessengerToken);
                this.Messenger.Send(new CurrentComponentCostChangedMessage(null), this.MessengerToken);

                return true;
            }

            // Manage Users screen.
            var manageUsersItems = selectedItem as ManageUsersTreeItem;
            if (manageUsersItems != null)
            {
                object content = null;
                if (!this.onlineChecker.CheckNow())
                {
                    content = this.CompositionContainer.GetExportedValue<DatabaseServerOfflineViewModel>();
                }
                else if (this.securityService.CheckUserForRights(Right.AdminTasks))
                {
                    content = this.CompositionContainer.GetExportedValue<ManageUsersViewModel>();
                }

                this.Messenger.Send(new LoadContentInMainViewMessage(content, LocalizedResources.General_ManageUsers), this.MessengerToken);
                this.Messenger.Send(new CurrentComponentCostChangedMessage(null), this.MessengerToken);

                return true;
            }
           
            var simpleTreeItem = selectedItem as SimpleProjectsTreeItem;
            if (simpleTreeItem != null)
            {
                // Note: Check here the Uid of other SimpleProjectsTreeItems.
                if (simpleTreeItem.Uid == ProjectsTreeItemUid.ManageProjects)
                {
                    object content = null;
                    if (!this.onlineChecker.CheckNow())
                    {
                        content = this.CompositionContainer.GetExportedValue<DatabaseServerOfflineViewModel>();
                    }
                    else if (this.securityService.CheckUserForRights(Right.AdminTasks))
                    {
                        content = this.CompositionContainer.GetExportedValue<ManageProjectsViewModel>();
                    }

                    this.Messenger.Send(new LoadContentInMainViewMessage(content, LocalizedResources.General_ManageProjects, false), this.MessengerToken);
                    this.Messenger.Send(new CurrentComponentCostChangedMessage(null), this.MessengerToken);

                    return true;
                }
                else if (simpleTreeItem.Uid == ProjectsTreeItemUid.EventLog)
                {
                    object content = null;
                    if (!this.onlineChecker.CheckNow())
                    {
                        content = this.CompositionContainer.GetExportedValue<DatabaseServerOfflineViewModel>();
                    }
                    else if (this.securityService.CheckUserForRights(Right.AdminTasks))
                    {
                        content = this.CompositionContainer.GetExportedValue<LogEventsViewModel>();
                    }

                    this.Messenger.Send(new LoadContentInMainViewMessage(content, LocalizedResources.General_EventLog, false), this.MessengerToken);
                    this.Messenger.Send(new CurrentComponentCostChangedMessage(null), this.MessengerToken);

                    return true;
                }                
            }

            // Manage Released Projects screen.
            var manageReleasedProjectsItems = selectedItem as ManageReleasedProjectsTreeItem;
            if (manageReleasedProjectsItems != null)
            {
                object content = null;
                if (!this.onlineChecker.CheckNow())
                {
                    content = this.CompositionContainer.GetExportedValue<DatabaseServerOfflineViewModel>();
                }
                else if (this.securityService.CheckUserForRights(Right.AdminTasks))
                {
                    content = this.CompositionContainer.GetExportedValue<ManageReleasedProjectsViewModel>();
                }

                this.Messenger.Send(new LoadContentInMainViewMessage(content, LocalizedResources.General_ManageReleasedProjects), this.MessengerToken);
                this.Messenger.Send(new CurrentComponentCostChangedMessage(null), this.MessengerToken);

                return true;
            }

            // Manage Settings screen.
            var manageSettingsItems = selectedItem as ManageSettingsTreeItem;
            if (manageSettingsItems != null)
            {
                object content = null;
                if (!this.onlineChecker.CheckNow())
                {
                    content = this.CompositionContainer.GetExportedValue<DatabaseServerOfflineViewModel>();
                }
                else if (this.securityService.CheckUserForRights(Right.AdminTasks))
                {
                    content = this.CompositionContainer.GetExportedValue<ManageSettingsViewModel>();
                }

                this.Messenger.Send(new LoadContentInMainViewMessage(content, LocalizedResources.General_ManageSettings), this.MessengerToken);
                this.Messenger.Send(new CurrentComponentCostChangedMessage(null), this.MessengerToken);

                return true;
            }

            var masterDataItemHandled = this.HandleMasterDataTreeItemSelection(selectedItem);
            if (masterDataItemHandled)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Handles the selection of the items in the Master Data item's sub-tree.
        /// </summary>
        /// <param name="selectedItem">The selected item.</param>
        /// <returns>True if the selection was handled; otherwise false.</returns>
        private bool HandleMasterDataTreeItemSelection(TreeViewDataItem selectedItem)
        {
            SimpleProjectsTreeItem item = selectedItem as SimpleProjectsTreeItem;
            if (item == null)
            {
                return false;
            }

            if (item.Uid <= ProjectsTreeItemUid.None || item.Uid > ProjectsTreeItemUid.MasterDataCurrencies)
            {
                return false;
            }

            // If the central db is offline, show the "Offline" screen and return.
            if (!this.onlineChecker.CheckNow())
            {
                object content = this.CompositionContainer.GetExportedValue<DatabaseServerOfflineViewModel>();
                this.Messenger.Send(new LoadContentInMainViewMessage(content, string.Empty), GlobalMessengerTokens.MainViewTargetToken);
                this.Messenger.Send(new CurrentComponentCostChangedMessage(null), GlobalMessengerTokens.MainViewTargetToken);
                return true;
            }

            object contentToLoad = null;
            string title = string.Empty;
            Guid itemToSelectId = Guid.Empty;
            object param = this.NextTreeItemSelectionParams.FirstOrDefault();
            if (param is Guid)
            {
                itemToSelectId = (Guid)param;
            }

            switch (item.Uid)
            {
                case ProjectsTreeItemUid.MasterDataBasicSettings:
                    {
                        IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
                        BasicSetting masterBasicSettings = dataContext.BasicSettingsRepository.GetBasicSettings();

                        BasicSettingsViewModel basicsettingsVM = this.CompositionContainer.GetExportedValue<BasicSettingsViewModel>();
                        basicsettingsVM.DataSourceManager = dataContext;
                        basicsettingsVM.Model = masterBasicSettings;

                        contentToLoad = basicsettingsVM;
                        title = LocalizedResources.General_ManageBasicSettings;
                        break;
                    }

                case ProjectsTreeItemUid.MasterDataOverheadSettings:
                    {
                        IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
                        OverheadSetting masterOverheads = dataContext.OverheadSettingsRepository.GetMasterData();

                        OverheadSettingsViewModel ohsettingsVM = this.CompositionContainer.GetExportedValue<OverheadSettingsViewModel>();
                        ohsettingsVM.EditMode = ViewModelEditMode.Edit;
                        ohsettingsVM.DataSourceManager = dataContext;
                        ohsettingsVM.Model = masterOverheads;

                        contentToLoad = ohsettingsVM;
                        title = LocalizedResources.General_ManageOverheadSettings;
                        break;
                    }

                case ProjectsTreeItemUid.MasterDataCountries:
                    {
                        IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
                        ManageCountriesViewModel manageCountriesVM = this.CompositionContainer.GetExportedValue<ManageCountriesViewModel>();
                        manageCountriesVM.DataSourceManager = dataManager;

                        contentToLoad = manageCountriesVM;
                        title = LocalizedResources.General_ManageCountries;
                        break;
                    }

                case ProjectsTreeItemUid.MasterDataCurrencies:
                    {
                        contentToLoad = this.CompositionContainer.GetExportedValue<ManageCurrenciesViewModel>();
                        title = LocalizedResources.General_ManageCurrencies;
                        break;
                    }

                case ProjectsTreeItemUid.MasterDataAssemblies:
                    {
                        MasterDataManagerViewModel viewmodel = this.CompositionContainer.GetExportedValue<MasterDataManagerViewModel>();
                        viewmodel.MasterDataType = typeof(Assembly);
                        viewmodel.ItemToSelectId = itemToSelectId;
                        contentToLoad = viewmodel;
                        title = LocalizedResources.General_ManageMasterData;
                        break;
                    }

                case ProjectsTreeItemUid.MasterDataParts:
                    {
                        MasterDataManagerViewModel viewmodel = this.CompositionContainer.GetExportedValue<MasterDataManagerViewModel>();
                        viewmodel.MasterDataType = typeof(Part);
                        viewmodel.ItemToSelectId = itemToSelectId;
                        contentToLoad = viewmodel;
                        title = LocalizedResources.General_ManageMasterData;
                        break;
                    }

                case ProjectsTreeItemUid.MasterDataRawParts:
                    {
                        MasterDataManagerViewModel viewmodel = this.CompositionContainer.GetExportedValue<MasterDataManagerViewModel>();
                        viewmodel.MasterDataType = typeof(RawPart);
                        viewmodel.ItemToSelectId = itemToSelectId;
                        contentToLoad = viewmodel;
                        title = LocalizedResources.General_ManageMasterData;
                        break;
                    }

                case ProjectsTreeItemUid.MasterDataMachines:
                    {
                        MasterDataManagerViewModel viewmodel = this.CompositionContainer.GetExportedValue<MasterDataManagerViewModel>();
                        viewmodel.MasterDataType = typeof(Machine);
                        viewmodel.ItemToSelectId = itemToSelectId;
                        contentToLoad = viewmodel;
                        title = LocalizedResources.General_ManageMasterData;
                        break;
                    }

                case ProjectsTreeItemUid.MasterDataRawMaterials:
                    {
                        MasterDataManagerViewModel viewmodel = this.CompositionContainer.GetExportedValue<MasterDataManagerViewModel>();
                        viewmodel.MasterDataType = typeof(RawMaterial);
                        viewmodel.ItemToSelectId = itemToSelectId;
                        contentToLoad = viewmodel;
                        title = LocalizedResources.General_ManageMasterData;
                        break;
                    }

                case ProjectsTreeItemUid.MasterDataConsumables:
                    {
                        MasterDataManagerViewModel viewmodel = this.CompositionContainer.GetExportedValue<MasterDataManagerViewModel>();
                        viewmodel.MasterDataType = typeof(Consumable);
                        viewmodel.ItemToSelectId = itemToSelectId;
                        contentToLoad = viewmodel;
                        title = LocalizedResources.General_ManageMasterData;
                        break;
                    }

                case ProjectsTreeItemUid.MasterDataCommodities:
                    {
                        MasterDataManagerViewModel viewmodel = this.CompositionContainer.GetExportedValue<MasterDataManagerViewModel>();
                        viewmodel.MasterDataType = typeof(Commodity);
                        viewmodel.ItemToSelectId = itemToSelectId;
                        contentToLoad = viewmodel;
                        title = LocalizedResources.General_ManageMasterData;
                        break;
                    }

                case ProjectsTreeItemUid.MasterDataDies:
                    {
                        MasterDataManagerViewModel viewmodel = this.CompositionContainer.GetExportedValue<MasterDataManagerViewModel>();
                        viewmodel.MasterDataType = typeof(Die);
                        viewmodel.ItemToSelectId = itemToSelectId;
                        contentToLoad = viewmodel;
                        title = LocalizedResources.General_ManageMasterData;
                        break;
                    }

                case ProjectsTreeItemUid.MasterDataManufacturers:
                    {
                        MasterDataManagerViewModel viewmodel = this.CompositionContainer.GetExportedValue<MasterDataManagerViewModel>();
                        viewmodel.MasterDataType = typeof(Manufacturer);
                        viewmodel.ItemToSelectId = itemToSelectId;
                        contentToLoad = viewmodel;
                        title = LocalizedResources.General_ManageMasterData;
                        break;
                    }

                case ProjectsTreeItemUid.MasterDataSuppliers:
                    {
                        MasterDataManagerViewModel viewmodel = this.CompositionContainer.GetExportedValue<MasterDataManagerViewModel>();
                        viewmodel.MasterDataType = typeof(Customer);
                        viewmodel.ItemToSelectId = itemToSelectId;
                        contentToLoad = viewmodel;
                        title = LocalizedResources.General_ManageMasterData;
                        break;
                    }

                default:
                    break;
            }

            if (contentToLoad != null)
            {
                this.Messenger.Send(new LoadContentInMainViewMessage(contentToLoad, title), GlobalMessengerTokens.MainViewTargetToken);
                this.Messenger.Send(new CurrentComponentCostChangedMessage(null), GlobalMessengerTokens.MainViewTargetToken);
                return true;
            }

            return false;
        }

        #endregion Selected item handling

        #region EntityChangedMessage handling

        /// <summary>
        /// Dispatches the multiple entities change messages for the projects tree to the appropriate handlers.
        /// </summary>
        /// <param name="message">The message.</param>        
        private void HandleEntitiesChangedMessage(EntitiesChangedMessage message)
        {
            if (message == null)
            {
                return;
            }

            // Refresh the old tree items when a tree item is moved in order to visually remove them from their old parent.
            var myProjectsItem = this.Items.FirstOrDefault(it => it is MyProjectsTreeItem) as MyProjectsTreeItem;
            var entities = message.Changes.Where(msgChange => msgChange.ChangeType == EntityChangeType.EntityMoved).Select(msgChange => msgChange.Entity).ToList();
            this.RefreshMovedTreeItemsParents(myProjectsItem, entities);

            foreach (var msgChange in message.Changes)
            {
                this.HandleEntityChange(msgChange);
            }
        }

        /// <summary>
        /// Dispatches the entity change messages for the projects tree to the appropriate handlers.
        /// </summary>
        /// <param name="message">The message.</param>        
        private void HandleEntityChangedMessage(EntityChangedMessage message)
        {
            if (message.ChangeType == EntityChangeType.EntityMoved)
            {
                var myProjectsItem = this.Items.FirstOrDefault(it => it is MyProjectsTreeItem) as MyProjectsTreeItem;
                this.RefreshMovedTreeItemsParents(myProjectsItem, new List<object> { message.Entity });
            }

            this.HandleEntityChange(message);
        }

        /// <summary>
        /// Handles the notifications about changes of an entity.
        /// </summary>
        /// <param name="message">The message.</param>
        private void HandleEntityChange(EntityChangedMessage message)
        {
            if (message.Notification == Notification.MyProjectsEntityChanged)
            {
                var myProjectsItem = this.Items.FirstOrDefault(it => it is MyProjectsTreeItem) as MyProjectsTreeItem;
                var entity = message.Entity;
                var parent = message.Parent;

                if (!this.IsActive)
                {
                    var entityItem = this.FindItemOfDataObject(message.Entity, myProjectsItem);
                    var parentItem = this.FindItemOfDataObject(message.Parent, myProjectsItem);
                    if (parentItem == null && entityItem != null)
                    {
                        parentItem = entityItem.Parent is SubAssembliesTreeItem || entityItem.Parent is SubPartsTreeItem || entityItem.Parent is MaterialsTreeItem ? entityItem.Parent.Parent : entityItem.Parent;
                    }

                    entity = entityItem != null ? entityItem.DataObject : message.Entity;
                    parent = parentItem != null ? parentItem.DataObject : message.Parent;
                }

                this.HandleEntityChangedCommon(entity, parent, message.ChangeType, myProjectsItem, message.SelectEntity);
            }
            else if (message.Notification == Notification.MasterDataEntityChanged)
            {
                this.HandleMasterDataChangeMessage(message);
            }
        }

        /// <summary>
        /// Handles the notifications about changes in the master data section.
        /// </summary>
        /// <param name="message">The message.</param>
        private void HandleMasterDataChangeMessage(EntityChangedMessage message)
        {
            if (message.Notification != Notifications.Notification.MasterDataEntityChanged)
            {
                return;
            }

            // Get the master data item, entities, parts and raw parts items.
            var masterDataItem = this.Items.FirstOrDefault(it => it is MasterDataTreeItem);
            var entitiesItem = masterDataItem.Children.FirstOrDefault(it => it is SimpleProjectsTreeItem && (it as SimpleProjectsTreeItem).Uid == ProjectsTreeItemUid.MasterDataEntitiesItem);
            var partsItem = entitiesItem.Children.FirstOrDefault(it => it is SimpleProjectsTreeItem && (it as SimpleProjectsTreeItem).Uid == ProjectsTreeItemUid.MasterDataParts);
            var rawPartsItem = entitiesItem.Children.FirstOrDefault(it => it is SimpleProjectsTreeItem && (it as SimpleProjectsTreeItem).Uid == ProjectsTreeItemUid.MasterDataRawParts);

            if (message.ChangeType == EntityChangeType.EntityDeleted)
            {
                foreach (var entityItem in entitiesItem.Children)
                {
                    // Get the item and the parent item of the deleted entity from the current entity item.
                    var itemToRemove = entityItem.FindItemForDataObject(message.Entity);
                    var parentItem = itemToRemove != null && !(itemToRemove.Parent is SimpleProjectsTreeItem) ? itemToRemove.Parent : null;
                    if (parentItem is SubAssembliesTreeItem || parentItem is SubPartsTreeItem || parentItem is MaterialsTreeItem)
                    {
                        parentItem = parentItem.Parent;
                    }

                    if (itemToRemove != null && parentItem != null)
                    {
                        this.RefreshEntity(itemToRemove.DataObject, itemToRemove.DataObjectContext);

                        // Refresh the parent item.
                        parentItem.Refresh();
                    }
                    else if (itemToRemove != null)
                    {
                        // If the item that needs to be removed it's a node it needs to be removed directly from the parent.
                        itemToRemove.Parent.Children.Remove(itemToRemove);
                    }
                }

                // Remove the parts and rap parts items that no longer exist.
                if (partsItem != null)
                {
                    this.RemoveMasterDataDeletedParts(partsItem);
                }

                if (rawPartsItem != null)
                {
                    this.RemoveMasterDataDeletedParts(rawPartsItem);
                }
            }
            else if (message.ChangeType == EntityChangeType.EntityCreated ||
                message.ChangeType == EntityChangeType.EntityImported)
            {
                // Continue only if the entity was created from Projects tree.
                if (message.Parent == null)
                {
                    return;
                }

                foreach (var entityItem in entitiesItem.Children)
                {
                    // Get the parent item of the created/imported entity from the current entity item.
                    TreeViewDataItem parentItem = entityItem.FindItemForDataObject(message.Parent);
                    if (parentItem == null)
                    {
                        continue;
                    }

                    this.GetEntity(message.Entity, parentItem.DataObjectContext);
                    this.RefreshEntity(parentItem.DataObject, parentItem.DataObjectContext);

                    // Refresh the parent item.
                    parentItem.Refresh();
                }

                // Select the created item if selection is requested.
                if (message.SelectEntity)
                {
                    // First search the item in its parent's children; this works if the parent children are loaded.
                    var newItem = this.SelectedTreeItem.FindItemForDataObject(message.Entity);
                    if (newItem != null)
                    {
                        newItem.Select();
                    }
                    else
                    {
                        // The parent item is not loaded or is a Process Step, so use the Navigation logic to select the item.
                        this.NavigateToEntityCommon(this.SelectedTreeItem, message.Entity, DbIdentifier.CentralDatabase, null);
                    }
                }
            }
            else if (message.ChangeType == EntityChangeType.EntityUpdated)
            {
                foreach (var entityItem in entitiesItem.Children)
                {
                    // Get the item of the updated entity from the current entity item.
                    var itemToRefresh = entityItem.FindItemForDataObject(message.Entity);
                    if (itemToRefresh != null)
                    {
                        // Refresh the item.
                        itemToRefresh.Refresh();

                        // Refresh its parent
                        if (itemToRefresh.Parent != null)
                        {
                            itemToRefresh.Parent.Refresh();
                        }
                    }
                }
            }
            else if (message.ChangeType == EntityChangeType.EntityReordered)
            {
                var reorderedEntity = masterDataItem.FindItemForDataObject(message.Entity);

                // Refresh the child items and their view.
                if (reorderedEntity != null)
                {
                    reorderedEntity.Parent.RefreshChildrenView();
                }
            }

            return;
        }

        /// <summary>
        /// Gets the entity in the specified data context.
        /// </summary>
        /// <param name="dataObject">The data object.</param>
        /// <param name="dataContext">The data context.</param>
        /// <returns>The new object.</returns>
        private object GetEntity(object dataObject, IDataSourceManager dataContext)
        {
            if (dataObject == null || dataContext == null)
            {
                return null;
            }

            Project project = dataObject as Project;
            if (project != null)
            {
                return dataContext.ProjectRepository.GetProjectIncludingTopLevelChildren(project.Guid);
            }

            Assembly assembly = dataObject as Assembly;
            if (assembly != null)
            {
                return dataContext.AssemblyRepository.GetAssemblyFull(assembly.Guid);
            }

            Part part = dataObject as Part;
            if (part != null)
            {
                return dataContext.PartRepository.GetPartFull(part.Guid);
            }

            RawMaterial rawMaterial = dataObject as RawMaterial;
            if (rawMaterial != null)
            {
                return dataContext.RawMaterialRepository.GetById(rawMaterial.Guid);
            }

            Commodity commodity = dataObject as Commodity;
            if (commodity != null)
            {
                return dataContext.CommodityRepository.GetById(commodity.Guid);
            }

            Machine machine = dataObject as Machine;
            if (machine != null)
            {
                return dataContext.MachineRepository.GetById(machine.Guid);
            }

            Consumable consumable = dataObject as Consumable;
            if (consumable != null)
            {
                return dataContext.ConsumableRepository.GetById(consumable.Guid);
            }

            Die die = dataObject as Die;
            if (die != null)
            {
                return dataContext.DieRepository.GetById(die.Guid);
            }

            AssemblyProcessStep assemblyProcessStep = dataObject as AssemblyProcessStep;
            if (assemblyProcessStep != null)
            {
                return dataContext.ProcessStepRepository.GetProcessStepFull(assemblyProcessStep.Guid);
            }

            PartProcessStep partProcessStep = dataObject as PartProcessStep;
            if (partProcessStep != null)
            {
                return dataContext.ProcessStepRepository.GetProcessStepFull(partProcessStep.Guid);
            }

            return null;
        }

        #endregion EntityChangedMessage handling

        #region Navigation to specified tree items

        /// <summary>
        /// Handles NavigateToEntityMessage messages.
        /// </summary>
        /// <param name="message">The message.</param>
        private void HandleNavigateToEntityMessage(NavigateToEntityMessage message)
        {
            if (message.Entity == null)
            {
                return;
            }

            Action action = () => this.NavigateToEntity(message.Entity, message.DatabaseId, message.AdditionalData);
            this.Dispatcher.Invoke(action, DispatcherPriority.Loaded);
        }

        /// <summary>
        /// Navigates in the Projects Tree to the item corresponding to a specified entity and selects it.
        /// Use this method when you don't know the tree item in whose sub-tree the entity resides.
        /// </summary>
        /// <param name="entity">The entity to select to.</param>
        /// <param name="entitySource">The entity's data source.</param>
        /// <param name="additionalData">Additional data used for navigation</param>
        private void NavigateToEntity(object entity, DbIdentifier entitySource, object additionalData)
        {
            IMasterDataObject masterObj = entity as IMasterDataObject;
            bool isMasterData = masterObj != null ? masterObj.IsMasterData : false;
            if (isMasterData)
            {
                MasterDataTreeItem masterDataItem = this.Items.OfType<MasterDataTreeItem>().FirstOrDefault();
                this.NavigateToMasterDataEntity(entity, masterDataItem);
            }
            else
            {
                IDataSourceManager dataContext = DataAccessFactory.CreateDataSourceManager(entitySource);
                Project parentProject = dataContext.ProjectRepository.GetParentProject(entity);
                if (parentProject != null)
                {
                    // Determine the root item where to start the search.
                    TreeViewDataItem navRootItem = null;
                    if (parentProject.IsReleased)
                    {
                        // Search in Released Projects
                        navRootItem = this.Items.FirstOrDefault(it => it is ReleasedProjectsTreeItem);
                    }
                    else if (parentProject.Owner.Guid == SecurityManager.Instance.CurrentUser.Guid)
                    {
                        // Search in My Projects
                        navRootItem = this.Items.FirstOrDefault(it => it is MyProjectsTreeItem);
                    }
                    else if (parentProject.Owner.Guid != SecurityManager.Instance.CurrentUser.Guid)
                    {
                        // Search in Other Users Projects
                        navRootItem = this.Items.FirstOrDefault(it => it is OtherUsersProjectsTreeItem);
                    }

                    if (navRootItem != null)
                    {
                        // Determine the folders that must be expanded to reach the project to which the searched entity belongs.
                        var foldersToExpand = dataContext.ProjectRepository.GetProjectParentFolderHierarchy(parentProject.Guid);

                        List<Guid> treeItemHierarchy = new List<Guid>();

                        // if we are searching in other users project we must add the user id in the list in order to expand the user node
                        if (!parentProject.IsReleased && parentProject.Owner.Guid != SecurityManager.Instance.CurrentUser.Guid)
                        {
                            treeItemHierarchy.Add(parentProject.Owner.Guid);
                        }

                        // The order of folders in the collection is from project's parent to the root folder.
                        // We have to expand from root to the project's parent so we have to reverse the order of the items.
                        treeItemHierarchy.AddRange(foldersToExpand.Reverse());

                        // Expand all items and continue the search in the last expanded item.
                        Action<TreeViewDataItem> continueSearch = (treeItem) =>
                        {
                            // Search for the item corresponding to the parent project.
                            var parentProjectItem = treeItem.FindItemForDataObject(parentProject);
                            if (parentProjectItem == null)
                            {
                                log.Error("Could not find the parent project's item in the expanded folders hierarchy.");
                                this.WindowService.MessageDialogService.Show(LocalizedResources.Error_GoToError, MessageDialogType.Error);
                                return;
                            }

                            Project proj = entity as Project;
                            if (proj != null &&
                                proj.Guid == parentProject.Guid)
                            {
                                // If the item is already selected show the main document content
                                if (parentProjectItem.IsSelected == true)
                                {
                                    var message = new NotificationMessage(Notification.MainViewShowDocumentContent);
                                    this.Messenger.Send(message);
                                }

                                // The parent project is the searched entity, so select it.
                                parentProjectItem.Select();
                            }
                            else
                            {
                                // Continue the search in the parent project.
                                this.NavigateToEntityCommon(parentProjectItem, entity, entitySource, additionalData);
                            }
                        };

                        this.ExpandTreeItemsHierarchyForNavigation(navRootItem, treeItemHierarchy, continueSearch);
                    }
                    else
                    {
                        log.Error("Could not determine the Projects Tree root item where to start the navigation.");
                        this.WindowService.MessageDialogService.Show(LocalizedResources.Error_GoToError, MessageDialogType.Error);
                    }
                }
                else
                {
                    log.Error("Could not find the parent project of an entity while trying to navigate to it (entity type: {0}).", entity.GetType());
                    this.WindowService.MessageDialogService.Show(LocalizedResources.Error_GoToError, MessageDialogType.Error);
                }
            }
        }

        /// <summary>
        /// Expands each folder in the specified folders hierarchy.
        /// </summary>
        /// <param name="hierarchyRootItem">The root item in which the folders are.</param>
        /// <param name="treeItems">The folders to expand, in order top to bottom.</param>
        /// <param name="continuation">
        /// The action executed after the last folder in the hierarchy has been expanded. This action usually continues/performs a search in the last expanded folder.
        /// The action's parameter is the tree item of the last folder expanded.
        /// </param>
        private void ExpandTreeItemsHierarchyForNavigation(
            TreeViewDataItem hierarchyRootItem,
            List<Guid> treeItems,
            Action<TreeViewDataItem> continuation)
        {
            if (hierarchyRootItem.LazyLoad != TreeViewItemLazyLoadMode.None && !hierarchyRootItem.AreChildrenLoaded)
            {
                Action<TreeViewDataItem> postExpandAction = null;
                postExpandAction = (e) =>
                {
                    hierarchyRootItem.ChildrenLoaded -= postExpandAction;
                    this.ExpandTreeItemsHierarchyForNavigation(hierarchyRootItem, treeItems, continuation);
                };
                hierarchyRootItem.ChildrenLoaded += postExpandAction;
                hierarchyRootItem.IsExpanded = true;
                return;
            }

            if (treeItems.Count > 0)
            {
                Guid itemId = treeItems[0];
                treeItems.RemoveAt(0);

                var treeItem = hierarchyRootItem.FindItemForDataObject(itemId);
                if (treeItem != null)
                {
                    this.ExpandTreeItemsHierarchyForNavigation(treeItem, treeItems, continuation);
                }
                else
                {
                    log.Error("Could not find an item that should be expanded while navigating to an entity.");
                    this.WindowService.MessageDialogService.Show(LocalizedResources.Error_GoToError, MessageDialogType.Error);
                }
            }
            else if (continuation != null)
            {
                // Execute the logic that will continue the navigation after the folder expansion has finished.
                continuation(hierarchyRootItem);
            }
        }

        /// <summary>
        /// Navigates to the specified entity in the Projects Tree's Master Data sub-tree.
        /// </summary>
        /// <param name="entity">The entity to navigate to.</param>
        /// <param name="masterDataItem">The master data item.</param>
        private void NavigateToMasterDataEntity(object entity, MasterDataTreeItem masterDataItem)
        {
            if (!this.onlineChecker.CheckNow())
            {
                this.WindowService.MessageDialogService.Show(LocalizedResources.General_OfflineMessage, MessageDialogType.Error);
                return;
            }

            ProjectsTreeItemUid searchedItemUid = ProjectsTreeItemUid.None;
            if (entity.GetType() == typeof(Part))
            {
                searchedItemUid = ProjectsTreeItemUid.MasterDataParts;
            }
            else if (entity is RawPart)
            {
                searchedItemUid = ProjectsTreeItemUid.MasterDataRawParts;
            }
            else if (entity is Assembly)
            {
                searchedItemUid = ProjectsTreeItemUid.MasterDataAssemblies;
            }
            else if (entity is RawMaterial)
            {
                searchedItemUid = ProjectsTreeItemUid.MasterDataRawMaterials;
            }
            else if (entity is Commodity)
            {
                searchedItemUid = ProjectsTreeItemUid.MasterDataCommodities;
            }
            else if (entity is Consumable)
            {
                searchedItemUid = ProjectsTreeItemUid.MasterDataConsumables;
            }
            else if (entity is Machine)
            {
                searchedItemUid = ProjectsTreeItemUid.MasterDataMachines;
            }
            else if (entity is Customer)
            {
                searchedItemUid = ProjectsTreeItemUid.MasterDataSuppliers;
            }
            else if (entity is Manufacturer)
            {
                searchedItemUid = ProjectsTreeItemUid.MasterDataManufacturers;
            }
            else if (entity is Die)
            {
                searchedItemUid = ProjectsTreeItemUid.MasterDataDies;
            }

            if (searchedItemUid == ProjectsTreeItemUid.None)
            {
                log.Error("Could not find the master data entity tree item (for type {0}) while trying to navigate to a master data entity", entity.GetType());
                this.WindowService.MessageDialogService.Show(LocalizedResources.Error_GoToError, MessageDialogType.Error);
                return;
            }

            var itemToSelect = masterDataItem.FindChild(searchedItemUid);

            // Select the tree master data entity item corresponding to the master data category to which the object to navigate to belongs.            
            if (itemToSelect != null)
            {
                if (itemToSelect.IsSelected)
                {
                    // This is the callback for the message that requests the main view's content; it selects the entity on the the master data management screen.
                    Action<object> selector = (mainViewContent) =>
                    {
                        var masterDataManager = mainViewContent as MasterDataManagerViewModel;
                        if (masterDataManager != null)
                        {
                            masterDataManager.SelectedItems.Add(
                                masterDataManager.MasterDataItems.OfType<IIdentifiable>()
                                    .FirstOrDefault(it => it.Guid == ((IIdentifiable)entity).Guid));
                        }
                    };

                    var message = new NotificationMessageWithAction<object>(Notification.MainViewGetContent, selector);
                    this.Messenger.Send(message, GlobalMessengerTokens.MainViewTargetToken);

                    // Notify the main view to show the main document content
                    var showDocumentMessage = new NotificationMessage(Notification.MainViewShowDocumentContent);
                    this.Messenger.Send(showDocumentMessage);
                }
                else
                {
                    // Add the entity to navigate to in the list of parameters for the next tree item selection handling. This will cause it to be selected when
                    // the view corresponding to itemToSelect will be loaded. See HandleMasterDataTreeItemSelection method for details.
                    IIdentifiable identifiableEntity = entity as IIdentifiable;
                    if (identifiableEntity != null)
                    {
                        this.NextTreeItemSelectionParams.Add(identifiableEntity.Guid);
                    }

                    itemToSelect.Select();
                }
            }
        }

        #endregion Navigation to specified tree items

        #region Drag and Drop

        /// <summary>
        /// Handles the projects tree drag enter.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        private void HandleProjectsTreeDragEnter(DragEventArgs e)
        {
            var myProjectsItem = this.Items.FirstOrDefault(it => it is MyProjectsTreeItem);
            this.HandleTreeDragEnterCommon(e, myProjectsItem);
        }

        /// <summary>
        /// Handles the projects tree drag over.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        private void HandleProjectsTreeDragOver(DragEventArgs e)
        {
            var myProjectsItem = this.Items.FirstOrDefault(it => it is MyProjectsTreeItem);
            this.HandleTreeDragOverCommon(e, myProjectsItem);
        }

        /// <summary>
        /// Handles the projects tree tree drop.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        private void HandleProjectsTreeTreeDrop(DragEventArgs e)
        {
            var msg = new SelectExplorerTreeRequestMessage(ExplorerTree.Projects);
            this.Messenger.Send(msg);

            this.HandleTreeDropCommon(e, this.SelectedTreeItem);
        }

        #endregion Drag and Drop

        #region Message handling

        /// <summary>
        /// Handles the SynchronizationFinished message.
        /// </summary>
        /// <param name="message">The message.</param>
        private void HandleSynchronizationFinishedMessage(SynchronizationFinishedMessage message)
        {
            // Refresh the My Projects tree item, if the user can see it.
            if (message.CompletedTasks.Contains(SynchronizationTask.MyProjects)
                && (SecurityManager.Instance.CurrentUserHasRole(Role.Admin)
                    || SecurityManager.Instance.CurrentUserHasRole(Role.KeyUser)
                    || SecurityManager.Instance.CurrentUserHasRole(Role.User)))
            {
                var myProjectsItem = this.Items.OfType<MyProjectsTreeItem>().FirstOrDefault();

                // If My Projects is loaded, collapse and unselect it so the data is released and will be refreshed when is expanded again.
                if (myProjectsItem.AreChildrenLoaded)
                {
                    myProjectsItem.IsExpanded = false;
                    myProjectsItem.IsSelected = false;

                    this.DataService.ReleaseData(myProjectsItem.DataObjectContext);
                }
            }

            // Refresh the Released Projects tree item.
            if (message.CompletedTasks.Contains(SynchronizationTask.ReleasedProjects))
            {
                var releasedProjectsItem = this.Items.OfType<ReleasedProjectsTreeItem>().FirstOrDefault();

                // If Released Projects is loaded, collapse and unselect it so its data is released and it will be refreshed when is expanded again.
                if (releasedProjectsItem.AreChildrenLoaded)
                {
                    releasedProjectsItem.IsExpanded = false;
                    releasedProjectsItem.IsSelected = false;

                    this.DataService.ReleaseData(releasedProjectsItem.DataObjectContext);
                }
            }
        }

        /// <summary>
        /// Handles some generic messages, of type NotificationMessage.
        /// </summary>
        /// <param name="message">The message.</param>
        protected override void HandleNotificationMessage(NotificationMessage message)
        {
            base.HandleNotificationMessage(message);

            if (message.Notification == Notification.ProjectOwnerChanged)
            {
                this.HandleProjectOwnerChanged();
            }
        }

        /// <summary>
        /// Handles the messages of type OnlineStateChangedMessage.
        /// </summary>
        /// <param name="message">The message.</param>
        private void HandleOnlineStateChanged(OnlineStateChangedMessage message)
        {
            ImageSource icon = message.IsOnline ? Images.OnlineIcon : Images.OfflineIcon;

            // Update the Other Users' Projects item and its 1st level children
            var otherUsersProjectsItem = this.Items.OfType<OtherUsersProjectsTreeItem>().FirstOrDefault();
            if (otherUsersProjectsItem != null)
            {
                otherUsersProjectsItem.BottomRightIcon = icon;
                foreach (var child in otherUsersProjectsItem.Children)
                {
                    child.BottomRightIcon = icon;
                }
            }

            // Update the Master Data item and its first 2 levels of children
            var masterDataItem = this.Items.OfType<MasterDataTreeItem>().FirstOrDefault();
            if (masterDataItem != null)
            {
                masterDataItem.BottomRightIcon = icon;
                List<TreeViewDataItem> masterdataItemChildren = new List<TreeViewDataItem>();
                masterdataItemChildren.AddRange(masterDataItem.Children);
                foreach (var child in masterDataItem.Children)
                {
                    masterdataItemChildren.AddRange(child.Children);
                }

                foreach (var child in masterdataItemChildren)
                {
                    child.BottomRightIcon = icon;
                }
            }

            // Update the admin item and its 1st level children
            var adminItem = this.Items.OfType<SimpleProjectsTreeItem>().FirstOrDefault(it => it.Uid == ProjectsTreeItemUid.Admin);
            if (adminItem != null)
            {
                adminItem.BottomRightIcon = icon;
                foreach (var child in adminItem.Children)
                {
                    child.BottomRightIcon = icon;
                }
            }
        }

        /// <summary>
        /// Handles the messages of type EditMasterDataEntityMessage.
        /// </summary>
        /// <param name="message">The message.</param>
        private void HandleEditMasterDataEntityMessage(EditMasterDataEntityMessage message)
        {
            // Check to see if the central db is online.
            if (!this.onlineChecker.CheckNow())
            {
                this.WindowService.MessageDialogService.Show(LocalizedResources.General_OfflineMessage, MessageDialogType.Error);
                return;
            }

            //// TODO: this.SelectedTreeItem should not be used here and assumed that is the tree item representing the master data category item for the entity in the message.
            //// The correct root tree item into which the message entity should be placed should be determined by the entity's type.

            // If the edited entity is loaded select it.
            var itemToSelect = this.SelectedTreeItem.FindItemForDataObject(message.Entity);
            if (itemToSelect != null)
            {
                itemToSelect.Select();
                return;
            }
            else
            {
                Assembly assembly = message.Entity as Assembly;
                if (assembly != null)
                {
                    IDataSourceManager dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

                    // If a parent of the edited entity is loaded expand it.
                    var parentItem = this.GetFirstParentAssemblyItem(assembly.Guid, dsm);
                    if (parentItem != null)
                    {
                        parentItem.IsExpanded = true;
                        this.editedMasterDataAssemblyToSelect = assembly;

                        return;
                    }
                    else
                    {
                        // If a children of the edited entity is loaded remove it.
                        var editedAssemblyTopParentId = dsm.AssemblyRepository.GetTopParentAssemblyId(assembly.Guid);
                        TreeViewDataItem itemToRemove = null;
                        foreach (var currentItem in this.SelectedTreeItem.Children)
                        {
                            var identifiableItem = currentItem.DataObject as IIdentifiable;
                            if (identifiableItem != null)
                            {
                                var currentItemId = identifiableItem.Guid;
                                var currentAssemblyParent = dsm.AssemblyRepository.GetTopParentAssemblyId(currentItemId);
                                if (currentAssemblyParent == editedAssemblyTopParentId)
                                {
                                    itemToRemove = currentItem;

                                    break;
                                }
                            }
                        }

                        if (itemToRemove != null)
                        {
                            this.SelectedTreeItem.Children.Remove(itemToRemove);
                        }
                    }
                }

                // Create a new node for the edited entity.
                TreeViewDataItem newNode = null;
                if (assembly != null)
                {
                    newNode = new AssemblyTreeItem(assembly, DbIdentifier.CentralDatabase, true, this.DataService);
                    newNode.ChildrenLoaded += ChildrenLoadedHandler;
                }

                Part part = message.Entity as Part;
                if (part != null)
                {
                    newNode = part is RawPart ?
                        new RawPartTreeItem(part as RawPart, DbIdentifier.CentralDatabase, true, this.DataService) :
                        new PartTreeItem(part, DbIdentifier.CentralDatabase, true, this.DataService);
                }

                if (newNode != null)
                {
                    this.SelectedTreeItem.Children.Add(newNode);
                    newNode.Select();
                }
            }
        }

        /// <summary>
        /// Performs the necessary operations after a project owner is changed (refreshes, etc.).
        /// </summary>        
        private void HandleProjectOwnerChanged()
        {
            var otherUsersProjectsItem = this.Items.OfType<OtherUsersProjectsTreeItem>().FirstOrDefault();
            if (otherUsersProjectsItem != null)
            {
                // If Other Users Projects is loaded, collapse and unselect it so its data is released and it will be refreshed when is expanded again.
                if (otherUsersProjectsItem.AreChildrenLoaded)
                {
                    otherUsersProjectsItem.IsExpanded = false;
                    otherUsersProjectsItem.IsSelected = false;

                    this.DataService.ReleaseData(otherUsersProjectsItem.DataObjectContext);
                }
            }
        }

        #endregion Message handling

        #region Helpers

        /// <summary>
        /// Handles the ChildrenLoaded event of a master data assembly item.
        /// </summary>
        /// <param name="parentItem">The parent item that contains the edited item that needs to be selected.</param>
        private void ChildrenLoadedHandler(TreeViewDataItem parentItem)
        {
            if (this.editedMasterDataAssemblyToSelect != null
                && parentItem.DataObject is Assembly
                && parentItem.Parent is SimpleProjectsTreeItem
                && (parentItem.Parent as SimpleProjectsTreeItem).Uid == ProjectsTreeItemUid.MasterDataAssemblies)
            {
                var itemToSelect = parentItem.FindItemForDataObject(this.editedMasterDataAssemblyToSelect);
                if (itemToSelect != null)
                {
                    itemToSelect.Select();
                    this.editedMasterDataAssemblyToSelect = null;
                }
            }
        }

        /// <summary>
        /// Gets the first parent of the assembly with the specified Id from master data assemblies.
        /// </summary>
        /// <param name="entityId">The entity Id.</param>
        /// <param name="dsm">The data source manager.</param>
        /// <returns>The first parent item if it was found, otherwise null.</returns>
        private TreeViewDataItem GetFirstParentAssemblyItem(Guid entityId, IDataSourceManager dsm)
        {
            var parentEntityId = dsm.AssemblyRepository.GetParentAssemblyId(entityId);
            if (parentEntityId != Guid.Empty)
            {
                var parentItem = this.SelectedTreeItem.Children.FirstOrDefault(it => it.DataObject is IIdentifiable && ((IIdentifiable)it.DataObject).Guid == parentEntityId);
                if (parentItem != null)
                {
                    return parentItem;
                }
                else
                {
                    return this.GetFirstParentAssemblyItem(parentEntityId, dsm);
                }
            }

            return null;
        }

        /// <summary>
        /// Removes the deleted nodes from master data parts.
        /// </summary>
        /// <param name="parentItem">The parent item.</param>
        private void RemoveMasterDataDeletedParts(TreeViewDataItem parentItem)
        {
            IDataSourceManager dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            var itemsToRemove = new List<TreeViewDataItem>();

            foreach (var item in parentItem.Children)
            {
                IIdentifiable identifiableEntity = item.DataObject as IIdentifiable;
                if (identifiableEntity != null)
                {
                    // Check if the item exists in the db.
                    var entityExists = dsm.PartRepository.CheckIfExists(identifiableEntity.Guid);
                    if (!entityExists)
                    {
                        // If the item does not exists, add it to a list.
                        itemsToRemove.Add(item);
                    }
                }
            }

            // Remove from the parent item the items which data object was deleted.
            foreach (var item in itemsToRemove)
            {
                parentItem.Children.Remove(item);
            }
        }

        #endregion Helpers
    }
}