﻿using ZPKTool.Gui.Controls;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Uniquely identifies instances of <see cref="SimpleProjectsTreeItem"/>.
    /// This enumeration, coupled with the Uid property of <see cref="SimpleProjectsTreeItem"/>, is used to support the creation
    /// of Projects Tree items without creating new classes that implement <see cref="TreeViewDataItem"/>.
    /// </summary>    
    public enum ProjectsTreeItemUid
    {
        /// <summary>
        /// Represents a generic, non unique, item.
        /// </summary>
        None = 0,

        /// <summary>
        /// Represents the Master Data Assemblies item.
        /// </summary>
        MasterDataAssemblies = 1,

        /// <summary>
        /// Represents the Master Data Parts item.
        /// </summary>
        MasterDataParts = 2,

        /// <summary>
        /// Represents the Master Data Machines item.
        /// </summary>
        MasterDataMachines = 3,

        /// <summary>
        /// Represents the Master Data Raw Materials item.
        /// </summary>
        MasterDataRawMaterials = 4,

        /// <summary>
        /// Represents the Master Data Consumables item.
        /// </summary>
        MasterDataConsumables = 5,

        /// <summary>
        /// Represents the Master Data Commodities item.
        /// </summary>
        MasterDataCommodities = 6,

        /// <summary>
        /// Represents the Master Data Raw Parts item.
        /// </summary>
        MasterDataRawParts = 7,

        /// <summary>
        /// Represents the Master Data Dies item.
        /// </summary>
        MasterDataDies = 8,

        /// <summary>
        /// Represents the Master Data Manufacturers item.
        /// </summary>
        MasterDataManufacturers = 9,

        /// <summary>
        /// Represents the Master Data Suppliers item.
        /// </summary>
        MasterDataSuppliers = 10,

        /// <summary>
        /// Represents the Master Data Basic Settings item.
        /// </summary>            
        MasterDataBasicSettings = 11,

        /// <summary>
        /// Represents the Master Data Overhead Settings item.
        /// </summary>
        MasterDataOverheadSettings = 12,

        /// <summary>
        /// Represents the Master Data Countries item.
        /// </summary>
        MasterDataCountries = 13,

        /// <summary>
        /// Represents the Master Data Currencies item.
        /// </summary>
        MasterDataCurrencies = 14,

        /// <summary>
        /// Represents the Admin item from the projects tree root.
        /// </summary>
        Admin = 15,

        /// <summary>
        /// Represets the Manage Projects item.
        /// </summary>
        ManageProjects = 16,

        /// <summary>
        /// Represets the Released Project item from manage release projects.
        /// </summary>
        ManageReleasedProjectsReleasedProject = 17,

        /// <summary>
        /// Represets the Released Folder item from manage release projects.
        /// </summary>
        ManageReleasedProjectsReleasedFolder = 18,

        /// <summary>
        /// Represets the Root item from manage release projects.
        /// </summary>
        ManageReleasedProjectsRoot = 19,

        /// <summary>
        /// Represents the Master Data Entities item.
        /// </summary>
        MasterDataEntitiesItem = 20,

        /// <summary>
        /// Represents the Event Log item.
        /// </summary>
        EventLog = 21
    }
}
