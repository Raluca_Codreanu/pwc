﻿using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ZPKTool.Business;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;
using ZPKTool.Synchronization;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for the Bookmarks Tree.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class BookmarksTreeViewModel : ProjectsExplorerBaseViewModel
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The projects item.
        /// </summary>
        private ProjectsBookmarksTreeItem projectsItem;

        /// <summary>
        /// The assemblies item.
        /// </summary>
        private AssembliesBookmarksTreeItem assembliesItem;

        /// <summary>
        /// The parts item.
        /// </summary>
        private PartsBookmarksTreeItem partsItem;

        /// <summary>
        /// The no bookmarks set item.
        /// </summary>
        private TreeViewDataItem noBookmarkSetItem;

        /// <summary>
        /// The selected or the dragged over tree item's top parent.
        /// </summary>
        private TreeViewDataItem lastClickedItemTopParent;

        /// <summary>
        /// The number of projects that have a bookmark.
        /// </summary>
        private int bookmarkedProjects;

        /// <summary>
        /// The number of assemblies that have a bookmark.
        /// </summary>
        private int bookmarkedAssemblies;

        /// <summary>
        /// The number of parts that have a bookmark.
        /// </summary>
        private int bookmarkedParts;

        #endregion Attributes

        /// <summary>
        /// Initializes a new instance of the <see cref="BookmarksTreeViewModel" /> class.
        /// </summary>
        /// <param name="messenger">The messenger.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="compositionContainer">The composition container.</param>
        /// <param name="pleaseWaitService">The please wait service</param>
        /// <param name="unitsService">The units service.</param>
        /// <param name="modelBrowserHelperService">The model browser helper service.</param>
        /// <param name="dataService">The data service.</param>
        /// <param name="importService">The import service.</param>
        [ImportingConstructor]
        public BookmarksTreeViewModel(
            IMessenger messenger,
            IWindowService windowService,
            CompositionContainer compositionContainer,
            IPleaseWaitService pleaseWaitService,
            IUnitsService unitsService,
            IModelBrowserHelperService modelBrowserHelperService,
            IProjectsExplorerDataService dataService,
            IImportService importService)
            : base(compositionContainer, messenger, windowService, pleaseWaitService, unitsService, modelBrowserHelperService, dataService, importService)
        {
            this.InitializeBookmarksTree();
            this.InitializeCommands();
        }

        #region Commands

        /// <summary>
        /// Gets the command executed for the DragEnter event of the Bookmarks Tree.
        /// </summary>
        public ICommand BookmarksTreeDragEnterCommand { get; private set; }

        /// <summary>
        /// Gets the command executed for the DragOver event of the Bookmarks Tree.
        /// </summary>
        public ICommand BookmarksTreeDragOverCommand { get; private set; }

        /// <summary>
        /// Gets the command executed for the Drop event of the Bookmarks Tree.
        /// </summary>
        public ICommand BookmarksTreeDropCommand { get; private set; }

        #endregion Commands

        #region Properties

        #endregion Properties

        /// <summary>
        /// Called after the view has been loaded. Usually it performs some initialization that needs the view to be loaded, like
        /// starting to load data from a database.
        /// <para />
        /// During unit tests this method must be manually called because there is no view to call it.
        /// </summary>
        public override void OnLoaded()
        {
            base.OnLoaded();

            this.RegisterMessageHandlers();
        }

        #region Initialization

        /// <summary>
        /// Initializes the bookmarks tree.
        /// </summary>
        private void InitializeBookmarksTree()
        {
            // Initialize the Bookmarks tree.
            if (SecurityManager.Instance.CurrentUserHasRight(Right.ProjectsAndCalculate))
            {
                // Initialize the projects, assemblies and parts items.
                this.projectsItem = new ProjectsBookmarksTreeItem(this.DataService);
                this.assembliesItem = new AssembliesBookmarksTreeItem(this.DataService);
                this.partsItem = new PartsBookmarksTreeItem(this.DataService);
                this.projectsItem.AllowChildrenMultipleSelection = true;
                this.assembliesItem.AllowChildrenMultipleSelection = true;
                this.partsItem.AllowChildrenMultipleSelection = true;

                // Initialize no bookmarks set item.
                this.noBookmarkSetItem = new TreeViewDataItem();
                this.noBookmarkSetItem.Label = LocalizedResources.General_NoBookmarksSet;
                this.noBookmarkSetItem.IconResourceKey = Images.BookmarksIconKey;

                this.RefreshBookmarkedItemsCount();
                this.UpdateRootBookmarkNodesAvailability();
            }
        }

        /// <summary>
        /// Initializes the commands.
        /// </summary>
        private void InitializeCommands()
        {
            this.BookmarksTreeDragEnterCommand = new DelegateCommand<DragEventArgs>(this.HandleBookmarksTreeDragEnter);
            this.BookmarksTreeDragOverCommand = new DelegateCommand<DragEventArgs>(this.HandleBookmarksTreeDragOver);
            this.BookmarksTreeDropCommand = new DelegateCommand<DragEventArgs>(this.HandleBookmarksTreeTreeDrop);
        }

        /// <summary>
        /// Registers the necessary message handlers with the IMessenger service.
        /// </summary>
        private void RegisterMessageHandlers()
        {
            this.Messenger.Register<EntityChangedMessage>(this.HandleEntityChangedMessage);
            this.Messenger.Register<EntitiesChangedMessage>(this.HandleEntitiesChangedMessage);
            this.Messenger.Register<BookmarkChangedMessage>(this.HandleBookmarksChangedMessage);
            this.Messenger.Register<SynchronizationFinishedMessage>(this.HandleSynchronizationFinishedMessage);
        }

        #endregion Initialization

        #region Bookmarks Tree events handling

        /// <summary>
        /// Handles the PreviewMouseDown event of the explorer tree view.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs" /> instance containing the event data.</param>
        /// <returns>
        /// The item over which the mouse down event occurred.
        /// </returns>
        protected override TreeViewDataItem HandleTreePreviewMouseDown(MouseButtonEventArgs e)
        {
            var clickedItem = base.HandleTreePreviewMouseDown(e);
            if (clickedItem != null)
            {
                this.lastClickedItemTopParent = this.FindItemTopParent(clickedItem);
            }

            return clickedItem;
        }

        #endregion Bookmarks Tree events handling

        #region EntityChangedMessage handling

        /// <summary>
        /// Dispatches the entity change messages for the bookmarks tree to the appropriate handlers.
        /// </summary>
        /// <param name="message">The message.</param>
        private void HandleEntityChangedMessage(EntityChangedMessage message)
        {
            if (message.Notification != Notification.MyProjectsEntityChanged)
            {
                return;
            }

            if (message.ChangeType == EntityChangeType.EntityPermanentlyDeleted)
            {
                foreach (var child in this.Items)
                {
                    this.RefreshPermanentlyDeletedEntity(message.Entity, child);
                }

                return;
            }

            var dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            var entityParentProject = dataManager.ProjectRepository.GetParentProject(message.Entity);

            foreach (var rootNode in this.Items)
            {
                foreach (var rootChildItem in rootNode.Children)
                {
                    var entityItem = this.FindItemOfDataObject(message.Entity, rootChildItem);
                    var parentEntityItem = this.FindItemOfDataObject(message.Parent, rootChildItem);
                    if (parentEntityItem == null && entityItem != null)
                    {
                        parentEntityItem = entityItem.Parent is SubAssembliesTreeItem || entityItem.Parent is SubPartsTreeItem || entityItem.Parent is MaterialsTreeItem ? entityItem.Parent.Parent : entityItem.Parent;
                    }

                    // If the notification type is RefreshData and the entity that needs to be refreshed was not found
                    // check to see if the entity that was refreshed has the same parent project as the current entity, and if so refresh it.
                    if (message.ChangeType == EntityChangeType.RefreshData && entityItem == null)
                    {
                        var parentProject = dataManager.ProjectRepository.GetParentProject(rootChildItem.DataObject);
                        if (parentProject == entityParentProject)
                        {
                            this.RefreshEntityAndRefrences(rootChildItem.DataObject, rootChildItem.DataObjectContext);
                        }

                        continue;
                    }

                    if (entityItem == null && parentEntityItem == null)
                    {
                        continue;
                    }

                    // The entity's parent is not loaded so the refresh is not necessary.
                    if (parentEntityItem != null && !parentEntityItem.AreChildrenLoaded)
                    {
                        continue;
                    }

                    bool selectEntity = !this.IsActive || (this.IsActive && rootChildItem != this.lastClickedItemTopParent) ? false : message.SelectEntity;
                    this.HandleEntityChangedCommon(message.Entity, message.Parent, message.ChangeType, rootChildItem, selectEntity);
                }

                rootNode.Refresh();
            }

            // If the notification type is EntityRecovered or EntityDeleted and the entity is a project, assembly or part, update the bookmark nodes accordingly to the changes.
            if ((message.ChangeType == EntityChangeType.EntityRecovered || message.ChangeType == EntityChangeType.EntityDeleted)
                && (message.Entity is Project || message.Entity is Assembly || message.Entity is Part))
            {
                this.RefreshBookmarkedItemsCount();
                this.UpdateRootBookmarkNodesAvailability();

                // Refresh the root items from bookmark tree
                foreach (var item in this.Items)
                {
                    item.Refresh();
                }
            }
        }

        /// <summary>
        /// Handles the entitities change messages.
        /// </summary>
        /// <param name="message">The message.</param>  
        private void HandleEntitiesChangedMessage(EntitiesChangedMessage message)
        {
            if (message == null)
            {
                return;
            }

            foreach (var msgChange in message.Changes)
            {
                this.HandleEntityChangedMessage(msgChange);
            }
        }

        /// <summary>
        /// Handles a bookmark changed message for Bookmarks.
        /// </summary>
        /// <param name="message">The message.</param>
        private void HandleBookmarksChangedMessage(BookmarkChangedMessage message)
        {
            // The EntityBookmarked message is handled the same for all entities.
            // Depending on the entity type increment the number of entities of that kind that have a bookmark.
            if (message.ChangeType == BookmarkChangeType.BookmarkCreated)
            {
                var project = message.Entity as Project;
                if (project != null)
                {
                    var projectItem = new ProjectTreeItem(project, DbIdentifier.LocalDatabase, this.DataService);
                    this.projectsItem.Children.Add(projectItem);
                    this.bookmarkedProjects++;
                }
                else
                {
                    var assembly = message.Entity as Assembly;
                    if (assembly != null)
                    {
                        var assemblyItem = new AssemblyTreeItem(assembly, DbIdentifier.LocalDatabase, true, this.DataService);
                        this.assembliesItem.Children.Add(assemblyItem);
                        this.bookmarkedAssemblies++;
                    }
                    else
                    {
                        var part = message.Entity as Part;
                        if (part != null)
                        {
                            var rawPart = part as RawPart;
                            if (rawPart != null)
                            {
                                var rawPartItem = new RawPartTreeItem(rawPart, DbIdentifier.LocalDatabase, true, this.DataService);
                                this.partsItem.Children.Add(rawPartItem);
                            }
                            else
                            {
                                var partItem = new PartTreeItem(part, DbIdentifier.LocalDatabase, true, this.DataService);
                                this.partsItem.Children.Add(partItem);
                            }

                            this.bookmarkedParts++;
                        }
                    }
                }
            }
            else if (message.ChangeType == BookmarkChangeType.BookmarkDeleted)
            {
                // The BookmarkDeleted message is handled the same for all entities.
                // Depending on the entity type decrement the number of entities of that kind that have a bookmark.
                object entity = message.Entity;
                var project = entity as Project;
                if (project != null)
                {
                    var projectItem = this.projectsItem.Children.FirstOrDefault(item => item.DataObject is IIdentifiable && (item.DataObject as IIdentifiable).Guid == project.Guid);
                    if (projectItem != null)
                    {
                        this.projectsItem.Children.Remove(projectItem);
                    }

                    this.bookmarkedProjects--;
                }
                else
                {
                    var assembly = entity as Assembly;
                    if (assembly != null)
                    {
                        var assemblyItem = this.assembliesItem.Children.FirstOrDefault(item => item.DataObject is IIdentifiable && (item.DataObject as IIdentifiable).Guid == assembly.Guid);
                        if (assemblyItem != null)
                        {
                            this.assembliesItem.Children.Remove(assemblyItem);
                        }

                        this.bookmarkedAssemblies--;
                    }
                    else
                    {
                        var part = entity as Part;
                        if (part != null)
                        {
                            var partItem = this.partsItem.Children.FirstOrDefault(item => item.DataObject is IIdentifiable && (item.DataObject as IIdentifiable).Guid == part.Guid);
                            if (partItem != null)
                            {
                                this.partsItem.Children.Remove(partItem);
                            }

                            this.bookmarkedParts--;
                        }
                    }
                }
            }

            this.UpdateRootBookmarkNodesAvailability();
        }

        /// <summary>
        /// Refreshes the internal count of bookmarked items for each type of bookmarks (Projects, Parts, Assemblies, etc.).
        /// </summary>
        private void RefreshBookmarkedItemsCount()
        {
            using (var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase))
            {
                var bookmarkedItemTypesCount = dsm.BookmarkRepository.GetBookmarkedItemTypesCount(SecurityManager.Instance.CurrentUser.Guid);
                this.bookmarkedProjects = bookmarkedItemTypesCount.ContainsKey(BookmarkedItemType.Project) ? bookmarkedItemTypesCount[BookmarkedItemType.Project] : 0;
                this.bookmarkedAssemblies = bookmarkedItemTypesCount.ContainsKey(BookmarkedItemType.Assembly) ? bookmarkedItemTypesCount[BookmarkedItemType.Assembly] : 0;
                this.bookmarkedParts = bookmarkedItemTypesCount.ContainsKey(BookmarkedItemType.Part) ? bookmarkedItemTypesCount[BookmarkedItemType.Part] : 0;
            }
        }

        /// <summary>
        /// Updates the root bookmark nodes availability (hide the ones that are empty, show the ones that are no longer empty).
        /// </summary>
        private void UpdateRootBookmarkNodesAvailability()
        {
            // Bookmarked projects node.
            if (this.bookmarkedProjects == 0 && this.Items.Contains(this.projectsItem))
            {
                this.Items.Remove(this.projectsItem);
            }
            else if (this.bookmarkedProjects != 0 && !this.Items.Contains(this.projectsItem))
            {
                this.Items.Insert(0, this.projectsItem);
            }

            // Bookmarked assemblies node.
            if (this.bookmarkedAssemblies == 0 && this.Items.Contains(this.assembliesItem))
            {
                this.Items.Remove(this.assembliesItem);
            }
            else if (this.bookmarkedAssemblies != 0 && !this.Items.Contains(this.assembliesItem))
            {
                if (!this.Items.Contains(this.projectsItem))
                {
                    this.Items.Insert(0, this.assembliesItem);
                }
                else
                {
                    this.Items.Insert(1, this.assembliesItem);
                }
            }

            // Bookmarked parts node.
            if (this.bookmarkedParts == 0 && this.Items.Contains(this.partsItem))
            {
                this.Items.Remove(this.partsItem);
            }
            else if (this.bookmarkedParts != 0 && !this.Items.Contains(this.partsItem))
            {
                if (!this.Items.Contains(this.projectsItem) && !this.Items.Contains(this.assembliesItem))
                {
                    this.Items.Insert(0, this.partsItem);
                }
                else if (this.Items.Contains(this.projectsItem) && this.Items.Contains(this.assembliesItem))
                {
                    this.Items.Insert(2, this.partsItem);
                }
                else
                {
                    this.Items.Insert(1, this.partsItem);
                }
            }

            // No bookmarks set node.
            if (this.Items.Count == 0 && !this.Items.Contains(this.noBookmarkSetItem))
            {
                this.Items.Add(this.noBookmarkSetItem);
            }
            else
            {
                this.Items.Remove(this.noBookmarkSetItem);
            }
        }

        #endregion EntityChangedMessage handling

        #region Drag and Drop

        /// <summary>
        /// Handles the bookmarks tree drag enter.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        private void HandleBookmarksTreeDragEnter(DragEventArgs e)
        {
            var selectedItemTopParent = this.FindItemTopParent(this.SelectedTreeItem);
            this.HandleTreeDragEnterCommon(e, selectedItemTopParent);
        }

        /// <summary>
        /// Handles the bookmarks tree drag over.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        private void HandleBookmarksTreeDragOver(DragEventArgs e)
        {
            var selectedItemTopParent = this.FindItemTopParent(this.SelectedTreeItem);
            this.HandleTreeDragOverCommon(e, selectedItemTopParent);
        }

        /// <summary>
        /// Handles the bookmarks tree tree drop.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        private void HandleBookmarksTreeTreeDrop(DragEventArgs e)
        {
            TreeViewItem targetTreeViewItem = UIHelper.FindParent<TreeViewItem>(e.OriginalSource as DependencyObject);
            if (targetTreeViewItem != null)
            {
                TreeViewDataItem targetTreeItem = targetTreeViewItem.DataContext as TreeViewDataItem;
                this.lastClickedItemTopParent = this.FindItemTopParent(targetTreeItem);
            }

            // Request to be selected.
            var msg = new SelectExplorerTreeRequestMessage(ExplorerTree.Bookmarks);
            this.Messenger.Send(msg);

            this.HandleTreeDropCommon(e, this.SelectedTreeItem);
        }

        #endregion Drag and Drop

        #region Message handling

        /// <summary>
        /// Handles the SynchronizationFinished message.
        /// </summary>
        /// <param name="message">The message.</param>
        private void HandleSynchronizationFinishedMessage(SynchronizationFinishedMessage message)
        {
            // Refresh the Bookmarks tree item, if the user can see it.
            if (message.CompletedTasks.Contains(SynchronizationTask.MyProjects)
                && (SecurityManager.Instance.CurrentUserHasRole(Role.Admin)
                    || SecurityManager.Instance.CurrentUserHasRole(Role.KeyUser)
                    || SecurityManager.Instance.CurrentUserHasRole(Role.User)))
            {
                foreach (var node in this.Items)
                {
                    // If a node is loaded, collapse and unselect it so the data is released and will be refreshed when is expanded again.
                    if (node.AreChildrenLoaded)
                    {
                        node.IsExpanded = false;
                        node.IsSelected = false;

                        this.DataService.ReleaseData(node.DataObjectContext);
                    }
                }
            }
        }

        #endregion Message handling

        #region Helpers

        /// <summary>
        /// Searches for the top parent of an item.
        /// </summary>
        /// <param name="item">The tree view data item.</param>
        /// <returns>The parent of the hierarchy.</returns>
        private TreeViewDataItem FindItemTopParent(TreeViewDataItem item)
        {
            if (item == null)
            {
                return null;
            }

            TreeViewDataItem parent = new TreeViewDataItem();
            TreeViewDataItem walker = item;
            while (walker != null
                && !(walker.Parent is ProjectsBookmarksTreeItem)
                && !(walker.Parent is AssembliesBookmarksTreeItem)
                && !(walker.Parent is PartsBookmarksTreeItem))
            {
                parent = walker;
                walker = walker.Parent;
            }

            return walker;
        }

        #endregion Helpers
    }
}