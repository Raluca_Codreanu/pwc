﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Windows.Input;
using ZPKTool.Business;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for the ManageUsers view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ManageUsersViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The composition container.
        /// </summary>
        private CompositionContainer container;

        /// <summary>
        /// The data manager to work with.
        /// </summary>
        private IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

        /// <summary>
        /// The list of all users from database.
        /// </summary>
        private List<User> allUsers;

        /// <summary>
        /// The list of users to display in the data grid, after filtering.
        /// </summary>
        private ObservableCollection<User> users;

        /// <summary>
        /// The selected user from data grid.
        /// </summary>
        private User selectedUser;

        /// <summary>
        /// The value for filtering the users by name.
        /// </summary>
        private string filterValue;

        /// <summary>
        /// A value indicating whether the disabled users are displayed in data grid or not.
        /// </summary>
        private bool showDisabledUsers;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ManageUsersViewModel"/> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        /// <param name="container">The container.</param>
        [ImportingConstructor]
        public ManageUsersViewModel(
            IWindowService windowService,
            CompositionContainer container)
        {
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("container", container);

            this.windowService = windowService;
            this.container = container;

            this.AddUserCommand = new DelegateCommand(() => this.OpenUserViewModel(false));
            this.EditUserCommand = new DelegateCommand(() => this.OpenUserViewModel(true));
            this.DeleteUserCommand = new DelegateCommand(this.DeteleUser, () => !this.IsReadOnly && this.SelectedUser != null);

            this.LoadSourceData();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the list of users to display in the data grid, after filtering.
        /// </summary>
        public ObservableCollection<User> Users
        {
            get { return this.users; }
            set { this.SetProperty(ref this.users, value, () => this.Users); }
        }

        /// <summary>
        /// Gets or sets the selected user from data grid.
        /// </summary>
        public User SelectedUser
        {
            get { return this.selectedUser; }
            set { this.SetProperty(ref this.selectedUser, value, () => this.SelectedUser); }
        }

        /// <summary>
        /// Gets or sets the value for filtering the users by name.
        /// </summary>
        public string FilterValue
        {
            get { return this.filterValue; }
            set { this.SetProperty(ref this.filterValue, value, () => this.FilterValue, this.FilterDisplayedUsers); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the disabled users are displayed in data grid or not.
        /// </summary>
        public bool ShowDisabledUsers
        {
            get { return this.showDisabledUsers; }
            set { this.SetProperty(ref this.showDisabledUsers, value, () => this.ShowDisabledUsers, this.FilterDisplayedUsers); }
        }

        #endregion

        #region Commands

        /// <summary>
        /// Gets the command for the add new user button.
        /// </summary>
        public ICommand AddUserCommand { get; private set; }

        /// <summary>
        /// Gets the command for the edit user button.
        /// </summary>
        public ICommand EditUserCommand { get; private set; }

        /// <summary>
        /// Gets the command for the delete user button.
        /// </summary>
        public ICommand DeleteUserCommand { get; private set; }

        #endregion

        #region Implementation

        /// <summary>
        /// Loads the all the users from the data source.
        /// </summary>
        private void LoadSourceData()
        {
            this.allUsers = this.dataManager.UserRepository.GetAll(true).OrderBy(u => u.Name).ToList();
            this.FilterDisplayedUsers();
        }

        /// <summary>
        /// Filters the users to display in data grid by disabled and filter name options.
        /// </summary>
        private void FilterDisplayedUsers()
        {
            Predicate<User> filter = (item) =>
            {
                // Filter by disabled flag of a user
                if (!this.ShowDisabledUsers && item.Disabled)
                {
                    return false;
                }

                // Then filter by the searched string inputted
                if (!string.IsNullOrEmpty(this.FilterValue))
                {
                    // Match the searched string anywhere in the user's name
                    return item.Name.ToLower().Contains(this.FilterValue.ToLower());
                }

                return true;
            };

            this.Users = new ObservableCollection<User>(this.allUsers.FindAll(filter));
            if (this.SelectedUser == null)
            {
                this.SelectedUser = this.Users.FirstOrDefault();
            }
        }

        /// <summary>
        /// Deletes the selected user.
        /// </summary>
        private void DeteleUser()
        {
            if (this.SelectedUser == null)
            {
                return;
            }

            var message = string.Format(LocalizedResources.Question_Delete_Item, this.SelectedUser.Name);
            MessageDialogResult result = windowService.MessageDialogService.Show(message, MessageDialogType.YesNo);
            if (result == MessageDialogResult.No)
            {
                return;
            }

            var username = this.SelectedUser.Name;

            this.PermanentlyDeleteUserProjectsAndFolders();
            this.dataManager.UserRepository.RemoveAll(this.SelectedUser);
            this.dataManager.SaveChanges();
            this.Users.Remove(this.SelectedUser);
            this.SelectedUser = this.Users.FirstOrDefault();

            LoggingManager.Log(LogEventType.DeletedUser, username);
        }

        /// <summary>
        /// Opens the user view model in a new window in create mode or edit mode for adding / editing a user.
        /// </summary>
        /// <param name="isInEditMode">if set to <c>true</c> [is in edit mode].</param>
        private void OpenUserViewModel(bool isInEditMode)
        {
            var onlineChecker = this.container.GetExportedValue<IOnlineCheckService>();
            if (!onlineChecker.CheckNow())
            {
                this.windowService.MessageDialogService.Show(LocalizedResources.Database_NoConnection, MessageDialogType.Error);
            }
            else
            {
                var userVM = this.container.GetExportedValue<UserViewModel>();
                userVM.DataSourceManager = this.dataManager;
                userVM.EditMode = isInEditMode ? ViewModelEditMode.Edit : ViewModelEditMode.Create;
                userVM.Model = isInEditMode ? this.SelectedUser : new User();

                this.windowService.ShowViewInDialog(userVM, "UserWindowViewTemplate");

                this.LoadSourceData();
            }
        }

        /// <summary>
        /// Permanently deletes the projects and folders of a selected user.
        /// </summary>
        private void PermanentlyDeleteUserProjectsAndFolders()
        {
            var message = string.Format(LocalizedResources.Question_Delete_User, this.SelectedUser.Name);
            var messageResult = MessageDialogResult.None;
            var allProjectFoldersIds = dataManager.ProjectFolderRepository.GetProjectFoldersId(SelectedUser.Guid);
            if (allProjectFoldersIds.Any())
            {
                messageResult = windowService.MessageDialogService.Show(message, MessageDialogType.YesNo);
                if (messageResult == MessageDialogResult.Yes)
                {
                    foreach (var projectFolder in allProjectFoldersIds)
                    {
                        this.dataManager.ProjectFolderRepository.DeleteByStoredProcedure(new ProjectFolder() { Guid = projectFolder });
                    }
                }
            }

            var allProjectsIds = dataManager.ProjectRepository.GetProjectsId(SelectedUser.Guid);
            if (allProjectsIds.Any())
            {
                if (messageResult == MessageDialogResult.None)
                {
                    messageResult = windowService.MessageDialogService.Show(message, MessageDialogType.YesNo);
                    if (messageResult == MessageDialogResult.Yes)
                    {
                        foreach (var project in allProjectsIds)
                        {
                            this.dataManager.ProjectRepository.DeleteByStoredProcedure(new Project() { Guid = project });
                        }
                    }
                }
            }
        }

        #endregion
    }
}
