﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using ZPKTool.Common;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The serialized report generator configuration class that contains the configurations.
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "Configurations")]
    public class ReportGeneratorConfigurations
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReportGeneratorConfigurations" /> class.
        /// </summary>
        public ReportGeneratorConfigurations()
        {
            Configuration = new List<ReportGeneratorConfiguration>();
        }

        /// <summary>
        /// Gets or sets the last selected configuration's guid.
        /// </summary>
        [XmlAttribute]
        public Guid SelectedConfigurationUid { get; set; }

        /// <summary>
        /// Gets or sets the list containing the configurations.
        /// </summary>
        [XmlElement]
        public List<ReportGeneratorConfiguration> Configuration { get; set; }
    }
}
