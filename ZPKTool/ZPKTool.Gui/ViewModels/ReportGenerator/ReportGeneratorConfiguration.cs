﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Xml.Serialization;
using ZPKTool.Common;
using ZPKTool.Gui.Resources;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The configuration class that defines the data category types and their data fields when generating a report.
    /// </summary>
    [Serializable]
    public class ReportGeneratorConfiguration : ObservableObject, IEditableObject, ISerializable
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The unique identifier.
        /// </summary>
        private Guid uid;

        /// <summary>
        /// The name.
        /// </summary>
        private string name;

        /// <summary>
        /// The comment.
        /// </summary>
        private string comment;

        /// <summary>
        /// The date of last change.
        /// </summary>
        private DateTime dateChanged;

        /// <summary>
        /// The string for the serialized configuration data.
        /// </summary>
        private string serializedConfigurationData;

        /// <summary>
        /// The name before it is edited.
        /// </summary>
        private string nameBeforeEdit;

        /// <summary>
        /// The comment before it is edited.
        /// </summary>
        private string commentBeforeEdit;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportGeneratorConfiguration"/> class. 
        /// </summary>
        public ReportGeneratorConfiguration()
        {
            this.Uid = Guid.NewGuid();
            this.DateChanged = DateTime.Now;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportGeneratorConfiguration" /> class.
        /// </summary>
        /// <param name="info">The info.</param>
        /// <param name="context">The context.</param>
        public ReportGeneratorConfiguration(SerializationInfo info, StreamingContext context)
        {
            this.Name = (string)info.GetValue("Name", typeof(string));
            this.Comment = (string)info.GetValue("Comment", typeof(string));
            this.DateChanged = (DateTime)info.GetValue("DateChanged", typeof(DateTime));
            this.SerializedConfigurationData = (string)info.GetValue("SerializedConfigurationData", typeof(string));
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the unique identifier.
        /// </summary>
        [XmlElement("Uid")]
        public Guid Uid
        {
            get { return this.uid; }
            set { this.SetProperty(ref this.uid, value, () => this.Uid); }
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [XmlElement("Name")]
        public string Name
        {
            get { return this.name; }
            set { this.SetProperty(ref this.name, value, () => this.Name); }
        }

        /// <summary>
        /// Gets or sets the comment.
        /// </summary>
        [XmlElement("Comment")]
        public string Comment
        {
            get { return this.comment; }
            set { this.SetProperty(ref this.comment, value, () => this.Comment); }
        }

        /// <summary>
        /// Gets or sets the date of last change.
        /// </summary>
        [XmlElement("DateChanged")]
        public DateTime DateChanged
        {
            get { return this.dateChanged; }
            set { this.SetProperty(ref this.dateChanged, value, () => this.DateChanged); }
        }

        /// <summary>
        /// Gets or sets the string for the serialized configuration data.
        /// </summary>
        [XmlElement("SerializedConfigurationData")]
        public string SerializedConfigurationData
        {
            get
            {
                return this.serializedConfigurationData;
            }

            set
            {
                if (value != this.serializedConfigurationData)
                {
                    this.serializedConfigurationData = value;
                    if (this.serializedConfigurationData != null)
                    {
                        DeserializeConfigurationData(this.serializedConfigurationData);
                    }

                    this.OnPropertyChanged(() => this.SerializedConfigurationData);
                }
            }
        }

        /// <summary>
        /// Gets or sets the dictionary containing the configuration data.
        /// </summary>
        [XmlIgnore]
        public Dictionary<ReportGeneratorViewModel.DataCategoryType, List<string>> ConfigurationData { get; set; }

        #endregion

        #region ISerializable

        /// <summary>
        /// Populates a <see cref="T:System.Runtime.Serialization.SerializationInfo"/> with the data needed to serialize the target object.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo"/> to populate with data.</param>
        /// <param name="context">The destination (see <see cref="T:System.Runtime.Serialization.StreamingContext"/>) for this serialization.</param>
        /// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Name", this.Name);
            info.AddValue("Comment", this.Comment);
            info.AddValue("DateChanged", this.DateChanged);
            info.AddValue("SerializedConfigurationData", this.SerializedConfigurationData);
        }

        #endregion

        /// <summary>
        /// Performs a deep Copy of the object.
        /// </summary>
        /// <returns>The copied object.</returns>
        public ReportGeneratorConfiguration Clone()
        {
            try
            {
                using (MemoryStream stream = new MemoryStream())
                {
                    IFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(stream, this);
                    stream.Seek(0, SeekOrigin.Begin);
                    return (ReportGeneratorConfiguration)formatter.Deserialize(stream);
                }
            }
            catch (Exception ex)
            {
                log.ErrorException("Error while trying to create a configuration clone.", ex);
                return new ReportGeneratorConfiguration();
            }
        }

        /// <summary>
        /// Compares the configuration data contained in this instance with another configuration data.
        /// </summary>
        /// <param name="configurationDataCategories">The configuration data.</param>
        /// <returns>True if the configurations are equal, false otherwise.</returns>
        public bool CompareConfigurationData(Dictionary<ReportGeneratorViewModel.DataCategoryType, List<string>> configurationDataCategories)
        {
            if (this.ConfigurationData.Count == configurationDataCategories.Count)
            {
                foreach (var pair in this.ConfigurationData)
                {
                    if (configurationDataCategories.ContainsKey(pair.Key))
                    {
                        if (pair.Value.Count == configurationDataCategories[pair.Key].Count)
                        {
                            foreach (var value in pair.Value)
                            {
                                if (!configurationDataCategories[pair.Key].Contains(value))
                                {
                                    return false;
                                }
                            }
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// Begins an edit on an object.
        /// </summary>
        public void BeginEdit()
        {
            this.nameBeforeEdit = this.Name;
            this.commentBeforeEdit = this.Comment;
        }

        /// <summary>
        /// Discards changes since the last <see cref="M:System.ComponentModel.IEditableObject.BeginEdit"/> call.
        /// </summary>
        public void CancelEdit()
        {
        }

        /// <summary>
        /// Pushes changes since the last <see cref="M:System.ComponentModel.IEditableObject.BeginEdit"/> or <see cref="M:System.ComponentModel.IBindingList.AddNew"/> call into the underlying object.
        /// </summary>
        public void EndEdit()
        {
            if (string.IsNullOrWhiteSpace(this.Name))
            {
                // If the name is null or white space, it will be replaced with the name before edit.
                this.Name = this.nameBeforeEdit;
            }

            if (this.nameBeforeEdit != this.Name || this.commentBeforeEdit != this.Comment)
            {
                // If the name or comment are changed, the date of last change will be updated.
                this.DateChanged = DateTime.Now;
            }
        }

        /// <summary>
        /// Deserializes the dictionary containing the selection mappings and saves it into ConfigurationData.
        /// </summary>
        /// <param name="serializedData">The string containing the serialized configuration data.</param>
        private void DeserializeConfigurationData(string serializedData)
        {
            try
            {
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    byte[] dictionaryBytes = Convert.FromBase64String(serializedData);
                    memoryStream.Write(dictionaryBytes, 0, dictionaryBytes.Length);
                    memoryStream.Position = 0L;

                    BinaryFormatter bformatter = new BinaryFormatter();
                    this.ConfigurationData = bformatter.Deserialize(memoryStream) as Dictionary<ReportGeneratorViewModel.DataCategoryType, List<string>>;
                }
            }
            catch (Exception ex)
            {
                log.ErrorException("Error while trying to deserialize the configuration data.", ex);
                this.ConfigurationData = null;
            }
        }

        /// <summary>
        /// Serializes the configuration data dictionary containing the selection mappings and saves the string into SerializedConfigurationData.
        /// </summary>
        public void SerializeConfigurationData()
        {
            if (this.ConfigurationData != null)
            {
                try
                {
                    using (MemoryStream stream = new MemoryStream())
                    {
                        BinaryFormatter bformatter = new BinaryFormatter();
                        bformatter.Serialize(stream, this.ConfigurationData);

                        var streamArray = stream.ToArray();
                        this.SerializedConfigurationData = Convert.ToBase64String(streamArray);
                    }
                }
                catch (Exception ex)
                {
                    log.ErrorException("Error while trying to serialize the configuration data.", ex);
                    this.SerializedConfigurationData = null;
                }
            }
        }
    }
}
