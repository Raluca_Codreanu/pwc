﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;
using NPOI.HPSF;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using ZPKTool.Business;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Reporting;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;
using Reflection = System.Reflection;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for ReportGenerator.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class ReportGeneratorViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The default cell format.
        /// </summary>
        private const string DefaultCellFormat = "0";

        /// <summary>
        /// Rental Cost Column Width displays 19 
        /// </summary>
        private const int RentalCostColumnWidth = 19 * 256;

        /// <summary>
        /// The index of the row to write the data category into.
        /// </summary>
        private const int DataCategoryHeaderRowIndex = 10;

        /// <summary>
        /// The index of the row to write the field headers into.
        /// </summary>
        private const int FieldHeaderRowIndex = 11;

        /// <summary>
        /// The column index of parts.
        /// </summary>
        private const int AssembliesAndPartsColumnIndex = 0;

        /// <summary>
        /// The column index of quantity.
        /// </summary>
        private const int QuantityColumnIndex = 1;

        /// <summary>
        /// The column index of sub parts and process.
        /// </summary>
        private const int SubPartsAndProcessColumnIndex = 2;

        /// <summary>
        /// The path to the generate report template file.
        /// </summary>
        private const string GenerateAssemblyReportTemplateLocation = "ZPKTool.Gui.Resources.ExcelTemplates.ReportGeneratorTemplate.xls";

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The index of the Process color.
        /// </summary>
        private static readonly short ProcessColorIndex = IndexedColors.AQUA.Index;

        /// <summary>
        /// The index of the Process labour data color.
        /// </summary>
        private static readonly short ProcessLabourDataColorIndex = IndexedColors.BLUE.Index;

        /// <summary>
        /// The index of the Part color.
        /// </summary>
        private static readonly short PartColorIndex = IndexedColors.BLUE_GREY.Index;

        /// <summary>
        /// The index of the Assembly color.
        /// </summary>
        private static readonly short AssemblyColorIndex = IndexedColors.BRIGHT_GREEN.Index;

        /// <summary>
        /// The index of the Quantity color.
        /// </summary>
        private static readonly short QuantityColorIndex = IndexedColors.BROWN.Index;

        /// <summary>
        /// The index of the Machine color.
        /// </summary>
        private static readonly short MachineColorIndex = IndexedColors.CORAL.Index;

        /// <summary>
        /// The index of the Tooling color.
        /// </summary>
        private static readonly short ToolingColorIndex = IndexedColors.CORNFLOWER_BLUE.Index;

        /// <summary>
        /// The index of the Consumable color.
        /// </summary>
        private static readonly short ConsumableColorIndex = IndexedColors.DARK_BLUE.Index;

        /// <summary>
        /// The index of the Commodity color.
        /// </summary>
        private static readonly short CommodityColorIndex = IndexedColors.DARK_GREEN.Index;

        /// <summary>
        /// The index of the Material color.
        /// </summary>
        private static readonly short MaterialColorIndex = IndexedColors.DARK_RED.Index;

        /// <summary>
        /// The index of the Material color.
        /// </summary>
        private static readonly short SubPartsAndAssembliesColorIndex = IndexedColors.DARK_TEAL.Index;

        /// <summary>
        /// The index of the RawPart color.
        /// </summary>
        private static readonly short RawPartColorIndex = IndexedColors.DARK_YELLOW.Index;

        /// <summary>
        /// The index of the Process cost color.
        /// </summary>
        private static readonly short ProcessCostColorIndex = IndexedColors.GOLD.Index;

        /// <summary>
        /// The index of the Machine cost color.
        /// </summary>
        private static readonly short MachineCostColorIndex = IndexedColors.GREEN.Index;

        /// <summary>
        /// The index of the Tooling cost color.
        /// </summary>
        private static readonly short ToolingCostColorIndex = IndexedColors.INDIGO.Index;

        /// <summary>
        /// The index of the Consumable cost color.
        /// </summary>
        private static readonly short ConsumableCostColorIndex = IndexedColors.LAVENDER.Index;

        /// <summary>
        /// The index of the Commodity cost color.
        /// </summary>
        private static readonly short CommodityCostColorIndex = IndexedColors.LEMON_CHIFFON.Index;

        /// <summary>
        /// The index of the Raw material cost color.
        /// </summary>
        private static readonly short MaterialCostColorIndex = IndexedColors.LIGHT_BLUE.Index;

        /// <summary>
        /// The index of the Cost summary color.
        /// </summary>
        private static readonly short CostSummaryColorIndex = IndexedColors.LIGHT_CORNFLOWER_BLUE.Index;

        /// <summary>
        /// The index of the OverheadAndMargin settings color.
        /// </summary>
        private static readonly short OverheadAndMarginSettingsColorIndex = IndexedColors.LIGHT_GREEN.Index;

        /// <summary>
        /// The index of the Country settings cost color.
        /// </summary>
        private static readonly short CountrySettingsColorIndex = IndexedColors.LIGHT_ORANGE.Index;

        /// <summary>
        /// The index of the Calculation settings color.
        /// </summary>
        private static readonly short CalculationSettingsColorIndex = IndexedColors.LIGHT_TURQUOISE.Index;

        /// <summary>
        /// The index of the OverheadAndMargin costs color.
        /// </summary>
        private static readonly short OverheadAndMarginCostsColorIndex = IndexedColors.LIGHT_YELLOW.Index;

        /// <summary>
        /// Empty cell style.
        /// </summary>
        private static ICellStyle emptyCellStyle;

        /// <summary>
        /// Dictionary containing the list of <see cref="DataCategoryItem"/> for the supported types (e.g Assembly).
        /// </summary>
        private static Dictionary<Type, Collection<DataCategoryItem>> dataCategoriesDictionary;

        /// <summary>
        /// Contains the index of columns which hold labour data.
        /// </summary>
        private readonly List<int> labourDataColumnIndices;

        /// <summary>
        /// The window service.
        /// </summary>
        private readonly IWindowService windowService;

        /// <summary>
        /// The save file dialog.
        /// </summary>
        private readonly IFileDialogService fileDialogService;

        /// <summary>
        /// Mapping of color and <see cref="CellType"/> to <see cref="ICellStyle"/>.
        /// Tuple of colorIndex, cell format and cell type.
        /// </summary>
        private Dictionary<Tuple<short, string, CellType>, ICellStyle> cellStylesDictionary;

        /// <summary>
        /// The workbook to create the report into.
        /// </summary>
        private HSSFWorkbook workbook;

        /// <summary>
        /// The template sheet from the <see cref="workbook"/>.
        /// </summary>
        private ISheet templateWorksheet;

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The entity for which to generate the report.
        /// </summary>
        private object entity;

        /// <summary>
        /// The title displayed by the view.
        /// </summary>
        private string title;

        /// <summary>
        /// The data categories for the <see cref="Entity"/>.
        /// </summary>
        private Collection<DataCategoryItem> dataCategories;

        /// <summary>
        /// A value indicating whether this instance is creating the reports.
        /// </summary>
        private bool isCreatingReports;

        /// <summary>
        /// Indicates whether all fields are selected or not.
        /// </summary>
        private bool? areAllFieldSelected;

        /// <summary>
        /// The selected data category.
        /// </summary>
        private DataCategoryItem selectedDataCategory;

        /// <summary>
        /// The path of the last created report.
        /// </summary>
        private string lastCreatedReportPath;

        /// <summary>
        /// Indicates whether any cost category is selected or not.
        /// </summary>
        private bool isAnyCostCategorySelected;

        /// <summary>
        /// The data category item for the calculation settings.
        /// </summary>
        private DataCategoryItem calculationSettingsCategoryItem;

        /// <summary>
        /// The data categoryItem for the overhead and margin costs category item.
        /// </summary>
        private DataCategoryItem overheadAndMarginCostsCategoryItem;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        #endregion Attributes

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportGeneratorViewModel"/> class.
        /// </summary>
        /// <param name="messenger">The messenger.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="fileDialogService">The file dialog service.</param>
        /// <param name="unitsService">The units service.</param>
        [ImportingConstructor]
        public ReportGeneratorViewModel(
            IMessenger messenger,
            IWindowService windowService,
            IFileDialogService fileDialogService,
            IUnitsService unitsService)
        {
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("fileDialogService", fileDialogService);
            Argument.IsNotNull("unitsService", unitsService);

            this.messenger = messenger;
            this.windowService = windowService;
            this.fileDialogService = fileDialogService;
            this.unitsService = unitsService;

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
            this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(dsm);

            this.InitializeDataCategories();

            this.Configurations = new ObservableCollection<ReportGeneratorConfiguration>();
            this.DataCategories = new Collection<DataCategoryItem>();
            this.labourDataColumnIndices = new List<int>();
            this.Title = LocalizedResources.General_ReportGenerator;

            this.CreateReportCommand = new DelegateCommand(CreateReport);
            this.OpenLastCreatedReportCommand = new DelegateCommand(OpenLastCreatedReport);
            this.NewConfigurationCommand = new DelegateCommand(this.AddNewConfiguration, this.CanAddNewConfiguration);
            this.DuplicateConfigurationCommand = new DelegateCommand(this.DuplicateSelectedConfiguration, this.CanDuplicateSelectedConfiguration);
            this.DeleteConfigurationCommand = new DelegateCommand(this.DeleteSelectedConfiguration, this.CanDeleteSelectedConfiguration);
            this.ImportConfigurationCommand = new DelegateCommand(this.ImportConfiguration, this.CanImportConfiguration);
            this.ExportConfigurationCommand = new DelegateCommand(this.ExportSelectedConfiguration, this.CanExportSelectedConfiguration);
        }

        #region Enums

        /// <summary>
        /// The possible categories of the items in the DataCategories list.
        /// </summary>
        public enum DataCategoryType
        {
            /// <summary>
            /// Representing the ProcessStep data object
            /// </summary>
            Process = 0,

            /// <summary>
            /// Machine type
            /// </summary>
            Machine = 1,

            /// <summary>
            /// Tooling type
            /// </summary>
            Tooling = 2,

            /// <summary>
            /// Consumable type
            /// </summary>
            Consumable = 3,

            /// <summary>
            /// Commodity type
            /// </summary>
            Commodity = 4,

            /// <summary>
            /// Raw Material type
            /// </summary>
            Material = 5,

            /// <summary>
            /// Raw Part type
            /// </summary>
            RawPart = 6,

            /// <summary>
            /// Process cost type.
            /// </summary>
            ProcessCost = 7,

            /// <summary>
            /// Machine cost type.
            /// </summary>
            MachineCost = 8,

            /// <summary>
            /// Tooling cost type.
            /// </summary>
            ToolingCost = 9,

            /// <summary>
            /// Consumable cost type.
            /// </summary>
            ConsumableCost = 10,

            /// <summary>
            /// Commodity cost type.
            /// </summary>
            CommodityCost = 11,

            /// <summary>
            /// Material cost type.
            /// </summary>
            MaterialCost = 13,

            /// <summary>
            /// Cost summary (currently the 1st tab in the result details screen).
            /// </summary>
            CostSummary = 14,

            /// <summary>
            /// Overhead and margin costs.
            /// </summary>
            OverheadAndMarginCosts = 15,

            /// <summary>
            /// Overhead and margin settings.
            /// </summary>
            OverheadAndMarginSettings = 16,

            /// <summary>
            /// Country settings.
            /// </summary>
            CountrySettings = 17,

            /// <summary>
            /// Calculation settings.
            /// </summary>
            CalculationSettings = 18
        }

        /// <summary>
        /// ICell types.
        /// </summary>
        public enum CellType
        {
            /// <summary>
            /// Normal cell type.
            /// </summary>
            Normal,

            /// <summary>
            /// Small header cell type.
            /// </summary>
            SmallHeader,

            /// <summary>
            /// Big header cell type.
            /// </summary>
            BigHeader
        }

        #endregion

        #region Commands

        /// <summary>
        /// Gets the create report command.
        /// </summary>
        public ICommand CreateReportCommand { get; private set; }

        /// <summary>
        /// Gets the open last created report command.
        /// </summary>
        public ICommand OpenLastCreatedReportCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets or sets the data categories.
        /// </summary>
        /// <value>
        /// The data categories.
        /// </value>
        public Collection<DataCategoryItem> DataCategories
        {
            get { return this.dataCategories; }
            set { this.SetProperty(ref this.dataCategories, value, () => this.DataCategories); }
        }

        /// <summary>
        /// Gets or sets the last created report path.
        /// </summary>
        public string LastCreatedReportPath
        {
            get { return this.lastCreatedReportPath; }
            set { this.SetProperty(ref this.lastCreatedReportPath, value, () => this.LastCreatedReportPath); }
        }

        /// <summary>
        /// Gets or sets the title displayed by the view.
        /// </summary>
        public string Title
        {
            get { return this.title; }
            set { this.SetProperty(ref this.title, value, () => this.Title); }
        }

        /// <summary>
        /// Gets or sets the entity for which to create the batch of reports.
        /// The entity currently supported is the Assembly.
        /// </summary>      
        /// <exception cref="InvalidOperationException">The value is not supported.</exception>
        public object Entity
        {
            get
            {
                return this.entity;
            }

            set
            {
                if (!(value is Assembly))
                {
                    throw new InvalidOperationException("The provided entity is not supported for report generation.");
                }

                if (this.entity != value)
                {
                    this.entity = value;
                    OnPropertyChanged(() => this.Entity);
                    PopulateDataCategories();
                    LoadConfigurationsFromFile();

                    // Add the entity's name in the Title
                    var objName = EntityUtils.GetEntityName(this.entity);
                    if (!string.IsNullOrWhiteSpace(objName))
                    {
                        this.Title = LocalizedResources.General_ReportGenerator + " - " + objName;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the Entity's parent object, when the Entity is not a Project.
        /// </summary>
        public Project EntityParent { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is creating the reports.
        /// </summary>        
        public bool IsCreatingReports
        {
            get { return this.isCreatingReports; }
            set { this.SetProperty(ref this.isCreatingReports, value, () => this.IsCreatingReports); }
        }

        /// <summary>
        /// Gets or sets the are all fields selected.
        /// </summary>
        /// <value>
        /// The are all fields selected.
        /// </value>
        public bool? AreAllFieldsSelected
        {
            get
            {
                return this.areAllFieldSelected;
            }

            set
            {
                if (this.areAllFieldSelected != value)
                {
                    this.areAllFieldSelected = value;
                    this.OnPropertyChanged(() => this.AreAllFieldsSelected);
                    OnAreAllFieldsSelectedChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected data category.
        /// </summary>
        /// <value>
        /// The selected data category.
        /// </value>
        public DataCategoryItem SelectedDataCategory
        {
            get
            {
                return this.selectedDataCategory;
            }

            set
            {
                if (this.selectedDataCategory != value)
                {
                    var previousValue = selectedDataCategory;
                    this.selectedDataCategory = value;
                    this.OnPropertyChanged(() => this.SelectedDataCategory);
                    OnSelectedDataCategoryChanged(previousValue);
                }
            }
        }

        /// <summary>
        /// Gets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter { get; private set; }

        #endregion Properties

        /// <summary>
        /// Gets the number of used columns.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <returns>The number of used rows.</returns>
        private static int GetNumberOfUsedColumns(ISheet worksheet)
        {
            var numberOfUsedColumns = 0;
            var row = worksheet.GetRow(FieldHeaderRowIndex);

            if (row != null)
            {
                for (var columnIndex = 0; columnIndex < row.LastCellNum; columnIndex++)
                {
                    numberOfUsedColumns = row.LastCellNum;

                    if (row.GetCell(columnIndex, MissingCellPolicy.RETURN_BLANK_AS_NULL) == null)
                    {
                        numberOfUsedColumns = columnIndex;
                        break;
                    }
                }
            }

            return numberOfUsedColumns;
        }

        /// <summary>
        /// Generates the name of the report for a given entity
        /// </summary>
        /// <param name="entity">The entity</param>
        /// <param name="reportType">Type of the report.</param>
        /// <returns>The name of the report file.</returns>
        private static string GenerateReportFileName(object entity, string reportType)
        {
            var fileName = string.Empty;
            Assembly assy = entity as Assembly;

            if (assy != null)
            {
                fileName = LocalizedResources.ReportGenerator_RG + "_" + assy.Name + "_" + reportType;
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                fileName = PathUtils.SanitizeFileName(fileName);
            }

            return fileName;
        }

        /// <summary>
        /// Determines whether the specified object to check has property.
        /// </summary>
        /// <param name="objectToCheck">The object to check.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <returns><c>true</c> if the specified object to check has property; otherwise, <c>false</c>.</returns>
        private static bool HasProperty(object objectToCheck, string propertyName)
        {
            return objectToCheck.GetType().GetProperty(propertyName) != null;
        }

        /// <summary>
        /// Called after the view has been unloaded from the parent or closed.
        /// </summary>
        public override void OnUnloaded()
        {
            base.OnUnloaded();

            SaveConfigurationsToFile();
        }

        /// <summary>
        /// Initializes the data categories.
        /// </summary>
        private void InitializeDataCategories()
        {
            dataCategoriesDictionary = new Dictionary<Type, Collection<DataCategoryItem>>();
            Collection<DataCategoryItem> assemblyCategories = new Collection<DataCategoryItem>
            {
                new DataCategoryItem(DataCategoryType.Process, LocalizedResources.General_AssemblingData),
                new DataCategoryItem(DataCategoryType.ProcessCost, LocalizedResources.ReportGenerator_AssemblingCost),
                new DataCategoryItem(DataCategoryType.Machine, LocalizedResources.ReportGenerator_MachineData),
                new DataCategoryItem(DataCategoryType.MachineCost, LocalizedResources.ReportGenerator_MachineCost),
                new DataCategoryItem(DataCategoryType.Tooling, LocalizedResources.General_ToolingData),
                new DataCategoryItem(DataCategoryType.ToolingCost, LocalizedResources.ReportGenerator_ToolingCost),
                new DataCategoryItem(DataCategoryType.Consumable, LocalizedResources.General_ConsumableData),
                new DataCategoryItem(DataCategoryType.ConsumableCost, LocalizedResources.ReportGenerator_ConsumableCost),
                new DataCategoryItem(DataCategoryType.Commodity, LocalizedResources.General_CommodityData),
                new DataCategoryItem(DataCategoryType.CommodityCost, LocalizedResources.ReportGenerator_CommodityCost),
                new DataCategoryItem(DataCategoryType.Material, LocalizedResources.General_RawMaterialData),
                new DataCategoryItem(DataCategoryType.MaterialCost, LocalizedResources.ReportGenerator_RawMaterialCost), 
                new DataCategoryItem(DataCategoryType.CostSummary, LocalizedResources.General_CostSummary), 
                new DataCategoryItem(DataCategoryType.OverheadAndMarginSettings, LocalizedResources.General_OverheadAndMargin),
                new DataCategoryItem(DataCategoryType.CountrySettings, LocalizedResources.General_CountrySettings),
            };

            dataCategoriesDictionary.Add(typeof(Assembly), assemblyCategories);
        }

        /// <summary>
        /// Populates the data categories.
        /// </summary>
        private void PopulateDataCategories()
        {
            var entityType = this.Entity.GetType();

            if (dataCategoriesDictionary.ContainsKey(entityType))
            {
                DataCategories = dataCategoriesDictionary[entityType];

                if (DataCategories != null)
                {
                    SelectedDataCategory = DataCategories.FirstOrDefault();
                }
            }
            else
            {
                DataCategories = null;
                log.Error("There is no list of DataCategories defined for object type: " + Entity.GetType());
            }
        }

        /// <summary>
        /// Called when [are all fields selected changed].
        /// </summary>
        private void OnAreAllFieldsSelectedChanged()
        {
            if (AreAllFieldsSelected != null &&
                SelectedDataCategory != null &&
                SelectedDataCategory.DataFields != null)
            {
                if ((bool)AreAllFieldsSelected)
                {
                    SelectedDataCategory.IsChecked = true;
                }

                foreach (var dataFieldItem in SelectedDataCategory.DataFields)
                {
                    dataFieldItem.PropertyChanged -= DataFieldItemPropertyChanged;
                    dataFieldItem.IsChecked = (bool)AreAllFieldsSelected;
                    dataFieldItem.PropertyChanged += DataFieldItemPropertyChanged;
                }

                var noneIsSelected = SelectedDataCategory.DataFields.All(field => !field.IsChecked);
                if (noneIsSelected)
                {
                    SelectedDataCategory.IsChecked = false;
                }
            }
        }

        /// <summary>
        /// Called when [selected data category changed].
        /// </summary>
        /// <param name="previousDataCategory">The previous data category.</param>
        private void OnSelectedDataCategoryChanged(DataCategoryItem previousDataCategory)
        {
            if (previousDataCategory != null && previousDataCategory.DataFields != null)
            {
                foreach (var dataFieldItem in previousDataCategory.DataFields)
                {
                    dataFieldItem.PropertyChanged -= DataFieldItemPropertyChanged;
                }
            }

            if (SelectedDataCategory != null && SelectedDataCategory.DataFields != null)
            {
                foreach (var dataFieldItem in SelectedDataCategory.DataFields)
                {
                    dataFieldItem.PropertyChanged += DataFieldItemPropertyChanged;
                }

                SetAreAllFieldsSelectedState();
            }
        }

        /// <summary>
        /// Handles the PropertyChanged event of the dataFieldItem control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void DataFieldItemPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsChecked")
            {
                SetAreAllFieldsSelectedState();
            }
        }

        /// <summary>
        /// Sets the state of the are all fields selected.
        /// </summary>
        private void SetAreAllFieldsSelectedState()
        {
            if (SelectedDataCategory == null || SelectedDataCategory.DataFields == null)
            {
                return;
            }

            var areAllSelected = SelectedDataCategory.DataFields.All(field => field.IsChecked);
            if (areAllSelected)
            {
                AreAllFieldsSelected = true;
            }
            else
            {
                var noneIsSelected = SelectedDataCategory.DataFields.All(field => !field.IsChecked);
                if (noneIsSelected)
                {
                    AreAllFieldsSelected = false;
                }
                else
                {
                    AreAllFieldsSelected = null;
                }
            }
        }

        /// <summary>
        /// Called before unloading the view from its parent.
        /// Returning false will cancel the view's unloading.
        /// </summary>
        /// <returns>
        /// True if the unloading process should continue and false if it should be canceled.
        /// </returns>
        public override bool OnUnloading()
        {
            // Cancel the unload if creating reports.
            return !this.IsCreatingReports;
        }

        /// <summary>
        /// Opens the last created report.
        /// </summary>
        private void OpenLastCreatedReport()
        {
            if (File.Exists(LastCreatedReportPath))
            {
                try
                {
                    System.Diagnostics.Process.Start(LastCreatedReportPath);
                }
                catch (Exception ex)
                {
                    log.ErrorException("Error occurred while opening last created report.", ex);
                    this.windowService.MessageDialogService.Show(LocalizedResources.Reports_LastCreatedReportOpenFail, MessageDialogType.Error);
                }
            }
        }

        /// <summary>
        /// Creates the report.
        /// </summary>
        private void CreateReport()
        {
            if (Entity == null)
            {
                return;
            }

            // Get the source entity's name.
            string entityName = EntityUtils.GetEntityName(this.Entity);
            if (string.IsNullOrWhiteSpace(entityName))
            {
                log.Error("The name of the entity for which to create the reports was null or empty. Report creation will stop.");
                return;
            }

            // Create reports for Assembly source entity.
            Assembly assembly = this.entity as Assembly;

            var reportTypeString = GenerateReportType();
            var reportFileName = GenerateReportFileName(assembly, reportTypeString);

            reportFileName = PathUtils.FixFilePathForCollisions(reportFileName + ".xls");

            this.fileDialogService.FileName = reportFileName;
            this.fileDialogService.Filter = LocalizedResources.DialogFilter_ExcelDocs;

            string filePath = null;
            var result = this.fileDialogService.ShowSaveFileDialog();
            if (result == true)
            {
                filePath = this.fileDialogService.FileName;
            }

            if (string.IsNullOrEmpty(filePath))
            {
                return;
            }

            Task.Factory.StartNew(() =>
            {
                this.IsCreatingReports = true;

                if (assembly != null)
                {
                    WriteReportFile(filePath, reportTypeString);
                }
            }).ContinueWith(task =>
            {
                this.IsCreatingReports = false;

                if (task.Exception != null)
                {
                    LastCreatedReportPath = string.Empty;

                    var error = task.Exception.InnerException;
                    log.ErrorException("Report generator error.", error);

                    if (error is IOException)
                    {
                        this.windowService.MessageDialogService.Show(LocalizedResources.General_FileIOError, MessageDialogType.Error);
                    }
                    else if (error is UnauthorizedAccessException)
                    {
                        this.windowService.MessageDialogService.Show(LocalizedResources.General_FileAccessError, MessageDialogType.Error);
                    }

                    this.windowService.MessageDialogService.Show(error);
                }
            });
        }

        /// <summary>
        /// Generates the type of the report.
        /// </summary>
        /// <returns>The report type.</returns>
        private string GenerateReportType()
        {
            StringBuilder result = new StringBuilder();

            var checkedItems = DataCategories.Where(item => item.IsChecked).ToList();

            if (checkedItems.Count == 0)
            {
                result.Append(LocalizedResources.General_Empty);
            }
            else
            {
                if (checkedItems.Count == DataCategories.Count)
                {
                    result.Append(LocalizedResources.General_Full);
                }
                else
                {
                    if (checkedItems.Count == 1)
                    {
                        result.Append(checkedItems[0].DisplayName);
                    }
                    else
                    {
                        List<string> categoryNames = new List<string>();
                        foreach (var item in checkedItems)
                        {
                            if (item.CategoryType == DataCategoryType.CalculationSettings)
                            {
                                categoryNames.Add(LocalizedResources.ReportGenerator_CalculationSettings);
                            }
                            else if (item.CategoryType == DataCategoryType.Commodity || item.CategoryType == DataCategoryType.CommodityCost)
                            {
                                categoryNames.Add(LocalizedResources.General_Commodity);
                            }
                            else if (item.CategoryType == DataCategoryType.Consumable || item.CategoryType == DataCategoryType.ConsumableCost)
                            {
                                categoryNames.Add(LocalizedResources.General_Consumable);
                            }
                            else if (item.CategoryType == DataCategoryType.CostSummary)
                            {
                                categoryNames.Add(LocalizedResources.General_CostSummary);
                            }
                            else if (item.CategoryType == DataCategoryType.CountrySettings)
                            {
                                categoryNames.Add(LocalizedResources.General_CountrySettings);
                            }
                            else if (item.CategoryType == DataCategoryType.Machine || item.CategoryType == DataCategoryType.MachineCost)
                            {
                                categoryNames.Add(LocalizedResources.General_Machine);
                            }
                            else if (item.CategoryType == DataCategoryType.Material || item.CategoryType == DataCategoryType.MaterialCost)
                            {
                                categoryNames.Add(LocalizedResources.General_RawMaterial);
                            }
                            else if (item.CategoryType == DataCategoryType.OverheadAndMarginCosts || item.CategoryType == DataCategoryType.OverheadAndMarginSettings)
                            {
                                categoryNames.Add(LocalizedResources.General_OverheadAndMargin);
                            }
                            else if (item.CategoryType == DataCategoryType.Process || item.CategoryType == DataCategoryType.ProcessCost)
                            {
                                categoryNames.Add(LocalizedResources.General_Process);
                            }
                            else if (item.CategoryType == DataCategoryType.Tooling || item.CategoryType == DataCategoryType.ToolingCost)
                            {
                                categoryNames.Add(LocalizedResources.General_Tooling);
                            }
                        }

                        var distinctCategoryNames = categoryNames.Distinct().ToList();
                        if (distinctCategoryNames.Count > 0)
                        {
                            result.Append(distinctCategoryNames[0]);

                            for (int i = 1; i < distinctCategoryNames.Count - 1; i++)
                            {
                                result.AppendFormat(", {0}", distinctCategoryNames[i]);
                            }

                            if (distinctCategoryNames.Count > 1)
                            {
                                result.AppendFormat(" {0} {1}", LocalizedResources.General_And, distinctCategoryNames[distinctCategoryNames.Count - 1]);
                            }
                        }
                    }
                }
            }

            result.AppendFormat(" {0}", LocalizedResources.General_Report);

            return result.ToString();
        }

        /// <summary>
        /// Writes the report file.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="reportType">Type of the report.</param>
        private void WriteReportFile(string filePath, string reportType)
        {
            Assembly assy = Entity as Assembly;

            if (assy != null)
            {
                GenerateAssemblyReport(assy, filePath, reportType);
            }
        }

        /// <summary>
        /// Generates the assembly report.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <param name="reportFilePath">The report file path.</param>
        /// <param name="reportType">Type of the report.</param>
        public void GenerateAssemblyReport(Assembly assembly, string reportFilePath, string reportType)
        {
            if (assembly == null)
            {
                throw new ArgumentNullException("assembly", "The source assembly was null");
            }

            if (dataCategories == null)
            {
                throw new ArgumentNullException("dataCategories", "The collection of data categories was null");
            }

            if (string.IsNullOrWhiteSpace(reportFilePath))
            {
                throw new ArgumentException("The file path was null or empty", "reportFilePath");
            }

            try
            {
                using (Stream templateStream = Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(GenerateAssemblyReportTemplateLocation))
                {
                    if (templateStream == null)
                    {
                        log.Error("The template for the generate assembly report was null.");
                    }

                    workbook = new HSSFWorkbook(templateStream);
                }

                // The 1st sheet is the report sheet; set its name
                var worksheet = workbook.GetSheetAt(0);
                if (!string.IsNullOrEmpty(assembly.Name))
                {
                    workbook.SetSheetName(0, ReportsHelper.ValidateWorksheetName(assembly.Name));
                }

                // Get the template sheet. It's needed to get the proper colors.
                templateWorksheet = workbook.GetSheet("Template");

                InitializeCellStyles();

                // Fill the report
                FillGeneratedReport(worksheet, assembly, reportType);

                // Remove grouping hack and template sheet.
                worksheet.UngroupColumn(0, 0);
                workbook.RemoveSheetAt(1);

                // Save the report
                using (FileStream file = new FileStream(reportFilePath, FileMode.Create))
                {
                    workbook.Write(file);
                }

                LastCreatedReportPath = reportFilePath;
            }
            catch (IOException ex)
            {
                throw new BusinessException(ErrorCodes.ReportGenerationIOError, ex);
            }
            catch (UnauthorizedAccessException ex)
            {
                throw new BusinessException(FileErrorCode.UnauthorizedAccess, ex);
            }
            catch (Exception ex)
            {
                log.ErrorException("Error while creating the enhanced xls assembly report.", ex);
                throw new BusinessException(ErrorCodes.ExcelReportGenerationUnknownError, ex);
            }
        }

        /// <summary>
        /// Gets or creates a cell.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="rowIndex">Index of the row.</param>
        /// <param name="columnIndex">Index of the column.</param>
        /// <returns>The existing or the created cell having the given indices.</returns>
        private ICell GetOrCreateCell(ISheet worksheet, int rowIndex, int columnIndex)
        {
            var row = worksheet.GetRow(rowIndex) ?? worksheet.CreateRow(rowIndex);
            var cell = row.GetCell(columnIndex) ?? row.CreateCell(columnIndex);

            return cell;
        }

        /// <summary>
        /// Fills the generated report.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="assembly">The assembly.</param>
        /// <param name="reportType">Type of the report.</param>
        private void FillGeneratedReport(ISheet worksheet, Assembly assembly, string reportType)
        {
            AddMainCells(worksheet, assembly, reportType);
            var rowToWriteInto = 13;

            AddColumnHeaders(worksheet, 3);
            AddSeparatorLine(worksheet, 12);

            CalculationResult calculationResult = null;
            if (isAnyCostCategorySelected)
            {
                var costCalculator = CostCalculatorFactory.GetCalculator(assembly.CalculationVariant);
                var calculationParams = CostCalculationHelper.CreateAssemblyParamsFromProject(this.EntityParent);
                calculationResult = costCalculator.CalculateAssemblyCost(assembly, calculationParams);
            }

            WriteAssemblyInfo(worksheet, assembly, rowToWriteInto, calculationResult);

            // Add two empty rows at the end of the report to be able to place the report logo.
            var row1 = worksheet.CreateRow(worksheet.LastRowNum + 1);
            var row2 = worksheet.CreateRow(worksheet.LastRowNum + 1);
            for (var i = 0; i < GetNumberOfUsedColumns(worksheet); i++)
            {
                row1.CreateCell(i);
                row2.CreateCell(i);
            }

            ReportsHelper.AddReportBottomLogo(workbook, worksheet, 2, worksheet.LastRowNum);

            // Format all not used cells inside the filled area
            ApplyEmptyStyleOnNotUsedCells(worksheet);

            var summaryInfo = PropertySetFactory.CreateSummaryInformation();
            summaryInfo.Author = LocalizedResources.General_AppName;
            workbook.SummaryInformation = summaryInfo;
        }

        /// <summary>
        /// Applies the empty style on not used cells.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        private void ApplyEmptyStyleOnNotUsedCells(ISheet worksheet)
        {
            var numberOfColumns = GetNumberOfUsedColumns(worksheet);

            for (var columnIndex = 0; columnIndex < numberOfColumns; columnIndex++)
            {
                var cell = worksheet.GetRow(10).GetCell(columnIndex);

                if (cell != null)
                {
                    if ((cell.CellType == NPOI.SS.UserModel.CellType.STRING || cell.CellType == NPOI.SS.UserModel.CellType.BLANK) && string.IsNullOrWhiteSpace(cell.StringCellValue))
                    {
                        cell.CellStyle = emptyCellStyle;
                    }
                }
            }

            for (var rowIndex = 11; rowIndex < worksheet.PhysicalNumberOfRows; rowIndex++)
            {
                var row = worksheet.GetRow(rowIndex);

                if (row != null)
                {
                    for (var columnIndex = 0; columnIndex < numberOfColumns; columnIndex++)
                    {
                        if (row.GetCell(columnIndex) == null)
                        {
                            var emptyCell = GetOrCreateCell(worksheet, rowIndex, columnIndex);
                            emptyCell.CellStyle = emptyCellStyle;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Adds the main cells.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="assembly">The assembly.</param>
        /// <param name="reportType">Type of the report.</param>
        private void AddMainCells(ISheet worksheet, Assembly assembly, string reportType)
        {
            worksheet.GetRow(0).GetCell(0).SetCellValue(LocalizedResources.ReportGenerator_AssemblyName);
            worksheet.GetRow(2).GetCell(0).SetCellValue(assembly.Name);
            worksheet.GetRow(4).GetCell(0).SetCellValue(LocalizedResources.General_Report);
            worksheet.GetRow(4).GetCell(1).SetCellValue(reportType);
            worksheet.GetRow(6).GetCell(0).SetCellValue(LocalizedResources.Report_Author);
            worksheet.GetRow(6).GetCell(1).SetCellValue(SecurityManager.Instance.CurrentUser.Username);
            worksheet.GetRow(6).GetCell(3).SetCellValue(LocalizedResources.General_Currency);
            worksheet.GetRow(6).GetCell(4).SetCellValue(SecurityManager.Instance.UICurrency.Symbol);
            worksheet.GetRow(8).GetCell(0).SetCellValue(LocalizedResources.General_Version);
            worksheet.GetRow(8).GetCell(1).SetCellValue(assembly.Version);
            worksheet.GetRow(8).GetCell(3).SetCellValue(LocalizedResources.General_Date);
            worksheet.GetRow(8).GetCell(4).SetCellValue(DateTime.Now.ToShortDateString());

            worksheet.GetRow(10).GetCell(0).SetCellValue(LocalizedResources.General_Assemblies);
            worksheet.GetRow(10).GetCell(0).CellStyle.FillForegroundColor = AssemblyColorIndex;
            worksheet.GetRow(10).GetCell(2).SetCellValue(LocalizedResources.ReportGenerator_SubPartsAndAssemblies);
            worksheet.GetRow(10).GetCell(2).CellStyle.FillForegroundColor = SubPartsAndAssembliesColorIndex;
            worksheet.GetRow(11).GetCell(0).SetCellValue(LocalizedResources.General_Parts);
            worksheet.GetRow(11).GetCell(0).CellStyle.FillForegroundColor = PartColorIndex;
            worksheet.GetRow(11).GetCell(1).SetCellValue(LocalizedResources.General_QuantityAbbreviation);
            worksheet.GetRow(11).GetCell(1).CellStyle.FillForegroundColor = QuantityColorIndex;
            worksheet.GetRow(11).GetCell(2).SetCellValue(LocalizedResources.General_Process);
            worksheet.GetRow(11).GetCell(2).CellStyle.FillForegroundColor = ProcessColorIndex;
        }

        /// <summary>
        /// Writes the assembly info.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="assembly">The assembly.</param>
        /// <param name="rowToWriteInto">The row to write into.</param>
        /// <param name="calculationResult">The calculation result.</param>
        /// <returns>
        /// The last row it was written into.
        /// </returns>
        private int WriteAssemblyInfo(ISheet worksheet, Assembly assembly, int rowToWriteInto, CalculationResult calculationResult)
        {
            var originalRowToWriteInto = rowToWriteInto;
            AddMainAssemblyCells(worksheet, assembly, rowToWriteInto);
            AddMainPartAndAssemblyCostCells(worksheet, calculationResult, rowToWriteInto, false);

            WriteCostSummary(worksheet, calculationResult, assembly, rowToWriteInto);

            var assemblyOverheadSettings = assembly.OverheadSettings.Copy();
            if (calculationResult != null && calculationResult.OverheadCost != null)
            {
                // Put the average manufacturing rate in the report instead of the rate from the assembly level.
                assemblyOverheadSettings.ManufacturingOverhead = calculationResult.OverheadCost.ManufacturingOverheadRateAverage;
                WriteOverheadSettings(worksheet, assemblyOverheadSettings, rowToWriteInto);
                if (isAnyCostCategorySelected)
                {
                    WriteCalculationSettings(worksheet, assembly, rowToWriteInto);
                }
            }

            if (assembly.CountrySettings != null)
            {
                WriteCountrySettings(worksheet, assembly.CountrySettings, rowToWriteInto);
            }

            if (assembly.CalculationAccuracy.GetValueOrDefault(PartCalculationAccuracy.FineCalculation) == PartCalculationAccuracy.FineCalculation)
            {
                var numberOfAddedRows = WriteProcessStepValues(worksheet, assembly.Process.Steps.OrderBy(processStep => processStep.Index).ToList(), rowToWriteInto, calculationResult, assemblyOverheadSettings, assembly.CountrySettings);
                rowToWriteInto += numberOfAddedRows;

                var subassemblies = assembly.Subassemblies.Where(item => !item.IsDeleted).OrderBy(item => item.Name).ToList();

                if (numberOfAddedRows == 0 && subassemblies.Count > 0)
                {
                    rowToWriteInto++;
                }

                for (var assemblyIndex = 0; assemblyIndex < subassemblies.Count; assemblyIndex++)
                {
                    var subAssemblyAmount = 0;

                    foreach (var processStep in assembly.Process.Steps)
                    {
                        var assemblyAmount = processStep.AssemblyAmounts.FirstOrDefault(item => item.FindAssemblyId() == subassemblies[assemblyIndex].Guid);

                        if (assemblyAmount != null)
                        {
                            subAssemblyAmount += assemblyAmount.Amount;
                        }
                    }

                    var assemblyCell = GetOrCreateCell(worksheet, rowToWriteInto, AssembliesAndPartsColumnIndex);
                    ApplyAssemblyStyleOnCell(assemblyCell);

                    var quantityCell = GetOrCreateCell(worksheet, rowToWriteInto, QuantityColumnIndex);
                    quantityCell.SetCellValue(subAssemblyAmount);
                    ApplyQuantityStyleOnCell(quantityCell);

                    var subAssemblyCell = GetOrCreateCell(worksheet, rowToWriteInto, SubPartsAndProcessColumnIndex);
                    subAssemblyCell.SetCellValue(subassemblies[assemblyIndex].Name);
                    ApplyAssemblyStyleOnCell(subAssemblyCell);

                    var numberOfColumns = GetNumberOfUsedColumns(worksheet);

                    for (var columnIndex = SubPartsAndProcessColumnIndex + 1; columnIndex < numberOfColumns; columnIndex++)
                    {
                        ApplyAssemblyStyleOnCell(GetOrCreateCell(worksheet, rowToWriteInto, columnIndex));
                    }

                    // Advance with the rows only if this is not the last item.
                    if (assemblyIndex != subassemblies.Count - 1)
                    {
                        rowToWriteInto++;
                    }
                }

                var parts = assembly.Parts.Where(item => !item.IsDeleted).OrderBy(item => item.Name).ToList();

                if (parts.Count > 0)
                {
                    rowToWriteInto++;
                }

                for (var partIndex = 0; partIndex < parts.Count; partIndex++)
                {
                    var partAmount = 0;

                    foreach (var processStep in assembly.Process.Steps)
                    {
                        var stepPartAmount = processStep.PartAmounts.FirstOrDefault(item => item.FindPartId() == parts[partIndex].Guid);

                        if (stepPartAmount != null)
                        {
                            partAmount = stepPartAmount.Amount;
                        }
                    }

                    var assemblyCell = this.GetOrCreateCell(worksheet, rowToWriteInto, AssembliesAndPartsColumnIndex);
                    this.ApplyAssemblyStyleOnCell(assemblyCell);

                    var quantityCell = this.GetOrCreateCell(worksheet, rowToWriteInto, QuantityColumnIndex);
                    quantityCell.SetCellValue(partAmount);
                    this.ApplyQuantityStyleOnCell(quantityCell);

                    var partAssemblyCell = this.GetOrCreateCell(worksheet, rowToWriteInto, SubPartsAndProcessColumnIndex);
                    partAssemblyCell.SetCellValue(parts[partIndex].Name);
                    this.ApplyPartStyleOnCell(partAssemblyCell, parts[partIndex] is RawPart);

                    var numberOfColumns = GetNumberOfUsedColumns(worksheet);

                    for (var columnIndex = SubPartsAndProcessColumnIndex + 1; columnIndex < numberOfColumns; columnIndex++)
                    {
                        this.ApplyPartStyleOnCell(this.GetOrCreateCell(worksheet, rowToWriteInto, columnIndex), parts[partIndex] is RawPart);
                    }

                    // Advance with the rows only if this is not the last item.
                    if (partIndex != parts.Count - 1)
                    {
                        rowToWriteInto++;
                    }
                }

                if (subassemblies.Count != 0 || parts.Count != 0)
                {
                    rowToWriteInto++;
                }

                for (var partIndex = 0; partIndex < parts.Count; partIndex++)
                {
                    this.AddSeparatorLine(worksheet, rowToWriteInto++);
                    var currentPart = parts[partIndex];

                    if (calculationResult != null)
                    {
                        var partCost = calculationResult.PartsCost.PartCosts.FirstOrDefault(cost => cost.PartId == currentPart.Guid);
                        rowToWriteInto = this.WritePartInfo(worksheet, currentPart, rowToWriteInto, partCost != null ? partCost.FullCalculationResult : null);
                    }
                    else
                    {
                        rowToWriteInto = this.WritePartInfo(worksheet, currentPart, rowToWriteInto, null);
                    }

                    // Advance with the rows only if this is not the last item.
                    if (partIndex != parts.Count - 1)
                    {
                        rowToWriteInto++;
                    }
                }

                if (parts.Count != 0 && subassemblies.Count != 0)
                {
                    rowToWriteInto++;
                }

                for (var assemblyIndex = 0; assemblyIndex < subassemblies.Count; assemblyIndex++)
                {
                    this.AddSeparatorLine(worksheet, rowToWriteInto++);
                    var currentSubassembly = subassemblies[assemblyIndex];

                    if (calculationResult != null)
                    {
                        var subassemblyCost = calculationResult.AssembliesCost.AssemblyCosts.FirstOrDefault(cost => cost.AssemblyId == currentSubassembly.Guid);
                        rowToWriteInto = this.WriteAssemblyInfo(worksheet, currentSubassembly, rowToWriteInto, subassemblyCost != null ? subassemblyCost.FullCalculationResult : null);
                    }
                    else
                    {
                        rowToWriteInto = this.WriteAssemblyInfo(worksheet, currentSubassembly, rowToWriteInto, null);
                    }

                    // Advance with the rows only if this is not the last item.
                    if (assemblyIndex != subassemblies.Count - 1)
                    {
                        rowToWriteInto++;
                    }
                }

                // If there are no parts neither subassemblies, correct the +1 step made at the beginning.
                if (parts.Count == 0 && subassemblies.Count == 0 && originalRowToWriteInto != rowToWriteInto)
                {
                    rowToWriteInto--;
                }
            }

            return rowToWriteInto;
        }

        /// <summary>
        /// Writes the overhead settings.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="overheadSetting">The overhead setting.</param>
        /// <param name="rowToWriteInto">The row to write into.</param>
        private void WriteOverheadSettings(ISheet worksheet, OverheadSetting overheadSetting, int rowToWriteInto)
        {
            if (overheadSetting != null)
            {
                var overheadSettingsCategoryItem = DataCategories.FirstOrDefault(item => item.CategoryType == DataCategoryType.OverheadAndMarginSettings);

                if (overheadSettingsCategoryItem != null && overheadSettingsCategoryItem.IsChecked)
                {
                    foreach (var checkedDataFieldItem in overheadSettingsCategoryItem.DataFields.Where(field => field.IsChecked).ToList())
                    {
                        var dataFieldItemCell = GetOrCreateCell(worksheet, rowToWriteInto, checkedDataFieldItem.ColumnIndex);
                        ApplyDataCategoryBasedStyleOnCell(dataFieldItemCell, DataCategoryType.OverheadAndMarginSettings, GetFormat(overheadSetting, checkedDataFieldItem));
                        SetCellValue(overheadSetting, checkedDataFieldItem.Property, checkedDataFieldItem, dataFieldItemCell);
                    }
                }
            }
        }

        /// <summary>
        /// Writes the cost summary.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="calculationResult">The calculation result.</param>
        /// <param name="parentObject">The parent object.</param>
        /// <param name="rowToWriteInto">The row to write into.</param>
        private void WriteCostSummary(ISheet worksheet, CalculationResult calculationResult, object parentObject, int rowToWriteInto)
        {
            var costSummaryCategoryItem = DataCategories.FirstOrDefault(item => item.CategoryType == DataCategoryType.CostSummary);

            if (calculationResult != null && costSummaryCategoryItem != null && costSummaryCategoryItem.IsChecked)
            {
                var paymentTermsFieldName = "PaymentTerms";
                foreach (var checkedDataFieldItem in costSummaryCategoryItem.DataFields.Where(field => field.IsChecked && field.Property.Name != paymentTermsFieldName).ToList())
                {
                    var dataFieldItemCell = GetOrCreateCell(worksheet, rowToWriteInto, checkedDataFieldItem.ColumnIndex);
                    ApplyDataCategoryBasedStyleOnCell(dataFieldItemCell, DataCategoryType.CostSummary, GetFormat(calculationResult, checkedDataFieldItem));
                    SetCellValue(calculationResult, checkedDataFieldItem.Property, checkedDataFieldItem, dataFieldItemCell);
                }

                var paymentTermsFieldItem = costSummaryCategoryItem.DataFields.FirstOrDefault(field => field.Property.Name == paymentTermsFieldName);
                if (paymentTermsFieldItem != null && paymentTermsFieldItem.IsChecked)
                {
                    var dataFieldItemCell = GetOrCreateCell(worksheet, rowToWriteInto, paymentTermsFieldItem.ColumnIndex);
                    ApplyDataCategoryBasedStyleOnCell(dataFieldItemCell, DataCategoryType.CostSummary, GetFormat(parentObject, paymentTermsFieldItem, parentObject is Part));
                    SetCellValue(parentObject, parentObject is Part ? paymentTermsFieldItem.SecondProperty : paymentTermsFieldItem.Property, paymentTermsFieldItem, dataFieldItemCell);
                }
            }
        }

        /// <summary>
        /// Writes the Country settings.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="countrySetting">The country setting.</param>
        /// <param name="rowToWriteInto">The row to write into.</param>
        private void WriteCountrySettings(ISheet worksheet, CountrySetting countrySetting, int rowToWriteInto)
        {
            var countrySettingsCategoryItem = DataCategories.FirstOrDefault(item => item.CategoryType == DataCategoryType.CountrySettings);

            if (countrySettingsCategoryItem != null && countrySettingsCategoryItem.IsChecked)
            {
                foreach (var checkedDataFieldItem in countrySettingsCategoryItem.DataFields.Where(field => field.IsChecked).ToList())
                {
                    var dataFieldItemCell = GetOrCreateCell(worksheet, rowToWriteInto, checkedDataFieldItem.ColumnIndex);
                    ApplyDataCategoryBasedStyleOnCell(dataFieldItemCell, DataCategoryType.CountrySettings, GetFormat(countrySetting, checkedDataFieldItem));
                    SetCellValue(countrySetting, checkedDataFieldItem.Property, checkedDataFieldItem, dataFieldItemCell);
                }
            }
        }

        /// <summary>
        /// Adds the main part and assembly cost cells.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="calculationResult">The calculation result.</param>
        /// <param name="rowToWriteInto">The row to write into.</param>
        /// <param name="isForPart">Indicates whether the costs are generated for part or assembly.</param>
        private void AddMainPartAndAssemblyCostCells(ISheet worksheet, CalculationResult calculationResult, int rowToWriteInto, bool isForPart)
        {
            var processCostItem = DataCategories.FirstOrDefault(item => item.CategoryType == DataCategoryType.ProcessCost);
            if (processCostItem != null && processCostItem.IsChecked && calculationResult != null)
            {
                foreach (var checkedDataFieldItem in processCostItem.DataFields.Where(field => field.IsChecked).ToList())
                {
                    var dataFieldItemCell = GetOrCreateCell(worksheet, rowToWriteInto, checkedDataFieldItem.ColumnIndex);
                    ApplyDataCategoryBasedStyleOnCell(dataFieldItemCell, DataCategoryType.ProcessCost, GetFormat(calculationResult.ProcessCost, checkedDataFieldItem, true));
                    SetCellValue(calculationResult.ProcessCost, checkedDataFieldItem.SecondProperty, checkedDataFieldItem, dataFieldItemCell);
                }
            }

            var machineCostItem = DataCategories.FirstOrDefault(item => item.CategoryType == DataCategoryType.MachineCost);
            if (machineCostItem != null && machineCostItem.IsChecked && calculationResult != null)
            {
                foreach (var checkedDataFieldItem in machineCostItem.DataFields.Where(field => field.IsChecked && field.SecondProperty != null && field.SecondProperty.DeclaringType == typeof(InvestmentCost)).ToList())
                {
                    var dataFieldItemCell = GetOrCreateCell(worksheet, rowToWriteInto, checkedDataFieldItem.ColumnIndex);
                    ApplyDataCategoryBasedStyleOnCell(dataFieldItemCell, DataCategoryType.MachineCost, GetFormat(calculationResult.InvestmentCost, checkedDataFieldItem, true));
                    SetCellValue(calculationResult.InvestmentCost, checkedDataFieldItem.SecondProperty, checkedDataFieldItem, dataFieldItemCell);
                }
            }

            var toolingCostItem = DataCategories.FirstOrDefault(item => item.CategoryType == DataCategoryType.ToolingCost);
            if (toolingCostItem != null && toolingCostItem.IsChecked && calculationResult != null)
            {
                foreach (var checkedDataFieldItem in toolingCostItem.DataFields.Where(field => field.IsChecked && field.SecondProperty != null && field.SecondProperty.DeclaringType == typeof(InvestmentCost)).ToList())
                {
                    var dataFieldItemCell = GetOrCreateCell(worksheet, rowToWriteInto, checkedDataFieldItem.ColumnIndex);
                    ApplyDataCategoryBasedStyleOnCell(dataFieldItemCell, DataCategoryType.ToolingCost, GetFormat(calculationResult.InvestmentCost, checkedDataFieldItem));
                    SetCellValue(calculationResult.InvestmentCost, checkedDataFieldItem.SecondProperty, checkedDataFieldItem, dataFieldItemCell);
                }
            }

            var consumableCostItem = DataCategories.FirstOrDefault(item => item.CategoryType == DataCategoryType.ConsumableCost);
            if (consumableCostItem != null && consumableCostItem.IsChecked && calculationResult != null && calculationResult.ProcessCost != null)
            {
                foreach (var checkedDataFieldItem in consumableCostItem.DataFields.Where(field => field.IsChecked).ToList())
                {
                    var dataFieldItemCell = GetOrCreateCell(worksheet, rowToWriteInto, checkedDataFieldItem.ColumnIndex);
                    ApplyDataCategoryBasedStyleOnCell(dataFieldItemCell, DataCategoryType.ConsumableCost, GetFormat(calculationResult.ProcessCost.ConsumablesCost, checkedDataFieldItem, true));
                    SetCellValue(calculationResult.ProcessCost.ConsumablesCost, checkedDataFieldItem.SecondProperty, checkedDataFieldItem, dataFieldItemCell);
                }
            }

            var commoditiesCostItem = DataCategories.FirstOrDefault(item => item.CategoryType == DataCategoryType.CommodityCost);
            if (commoditiesCostItem != null && commoditiesCostItem.IsChecked && calculationResult != null && calculationResult.ProcessCost != null)
            {
                var commoditiesCost = isForPart ? calculationResult.CommoditiesCost : calculationResult.ProcessCost.CommoditiesCost;
                foreach (var checkedDataFieldItem in commoditiesCostItem.DataFields.Where(field => field.IsChecked).ToList())
                {
                    var dataFieldItemCell = GetOrCreateCell(worksheet, rowToWriteInto, checkedDataFieldItem.ColumnIndex);
                    ApplyDataCategoryBasedStyleOnCell(dataFieldItemCell, DataCategoryType.CommodityCost, GetFormat(commoditiesCost, checkedDataFieldItem, true));
                    SetCellValue(commoditiesCost, checkedDataFieldItem.SecondProperty, checkedDataFieldItem, dataFieldItemCell);
                }
            }

            if (overheadAndMarginCostsCategoryItem != null && calculationResult != null)
            {
                foreach (var checkedDataFieldItem in overheadAndMarginCostsCategoryItem.DataFields.Where(field => field.IsChecked).ToList())
                {
                    var dataFieldItemCell = GetOrCreateCell(worksheet, rowToWriteInto, checkedDataFieldItem.ColumnIndex);
                    ApplyDataCategoryBasedStyleOnCell(dataFieldItemCell, DataCategoryType.OverheadAndMarginCosts, GetFormat(calculationResult.OverheadCost, checkedDataFieldItem));
                    SetCellValue(calculationResult.OverheadCost, checkedDataFieldItem.Property, checkedDataFieldItem, dataFieldItemCell);
                }
            }

            if (isForPart)
            {
                var rawMaterialsCostItem = DataCategories.FirstOrDefault(item => item.CategoryType == DataCategoryType.MaterialCost);
                if (rawMaterialsCostItem != null && rawMaterialsCostItem.IsChecked && calculationResult != null)
                {
                    foreach (var checkedDataFieldItem in rawMaterialsCostItem.DataFields.Where(field => field.IsChecked).ToList())
                    {
                        if (checkedDataFieldItem.SecondProperty != null)
                        {
                            var dataFieldItemCell = GetOrCreateCell(worksheet, rowToWriteInto, checkedDataFieldItem.ColumnIndex);
                            ApplyDataCategoryBasedStyleOnCell(dataFieldItemCell, DataCategoryType.MaterialCost, GetFormat(calculationResult.RawMaterialsCost, checkedDataFieldItem, true));
                            SetCellValue(calculationResult.RawMaterialsCost, checkedDataFieldItem.SecondProperty, checkedDataFieldItem, dataFieldItemCell);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Writes the process step cost values.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="rowToWriteInto">The row to write into.</param>
        /// <param name="processStepCost">The process step cost.</param>
        private void WriteProcessStepCostValues(ISheet worksheet, int rowToWriteInto, ProcessStepCost processStepCost)
        {
            var processCostItem = DataCategories.FirstOrDefault(item => item.CategoryType == DataCategoryType.ProcessCost);

            if (processCostItem != null && processCostItem.IsChecked && processStepCost != null)
            {
                foreach (var checkedDataFieldItem in processCostItem.DataFields.Where(field => field.IsChecked).ToList())
                {
                    var dataFieldItemCell = GetOrCreateCell(worksheet, rowToWriteInto, checkedDataFieldItem.ColumnIndex);
                    ApplyDataCategoryBasedStyleOnCell(dataFieldItemCell, DataCategoryType.ProcessCost, GetFormat(processStepCost, checkedDataFieldItem));
                    SetCellValue(processStepCost, checkedDataFieldItem.Property, checkedDataFieldItem, dataFieldItemCell);
                }
            }
        }

        /// <summary>
        /// Writes the part info.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="part">The part.</param>
        /// <param name="rowToWriteInto">The row to write into.</param>
        /// <param name="calculationResult">The calculation result.</param>
        /// <returns>
        /// The last row it was written into.
        /// </returns>
        private int WritePartInfo(ISheet worksheet, Part part, int rowToWriteInto, CalculationResult calculationResult)
        {
            AddMainPartCells(worksheet, part, rowToWriteInto);
            AddMainPartAndAssemblyCostCells(worksheet, calculationResult, rowToWriteInto, true);
            WriteCostSummary(worksheet, calculationResult, part, rowToWriteInto);

            var partOverheadSettings = part.OverheadSettings.Copy();
            if (calculationResult != null && calculationResult.OverheadCost != null)
            {
                // Put the average manufacturing rate in the report instead of the rate from the part level.
                partOverheadSettings.ManufacturingOverhead = calculationResult.OverheadCost.ManufacturingOverheadRateAverage;
                WriteOverheadSettings(worksheet, partOverheadSettings, rowToWriteInto);
                if (isAnyCostCategorySelected)
                {
                    WriteCalculationSettings(worksheet, part, rowToWriteInto);
                }
            }

            if (part.CountrySettings != null)
            {
                WriteCountrySettings(worksheet, part.CountrySettings, rowToWriteInto);
            }

            if (part.CalculationAccuracy.GetValueOrDefault(PartCalculationAccuracy.FineCalculation) == PartCalculationAccuracy.FineCalculation)
            {
                var originalRowIndex = rowToWriteInto;
                var numberOfaddedRows = WriteProcessStepValues(worksheet, part.Process.Steps.OrderBy(processStep => processStep.Index).ToList(), rowToWriteInto, calculationResult, partOverheadSettings, part.CountrySettings);
                rowToWriteInto += numberOfaddedRows;

                // Add commodities values
                if (originalRowIndex == rowToWriteInto)
                {
                    rowToWriteInto++;
                }

                numberOfaddedRows = WriteCommoditiesValues(worksheet, rowToWriteInto, part, calculationResult);
                rowToWriteInto += numberOfaddedRows;

                // Add raw materials
                var rawMaterialAddedRows = WriteRawMaterialValues(worksheet, rowToWriteInto, part, calculationResult);
                rowToWriteInto += rawMaterialAddedRows - 1;

                if (part.RawPart != null && !part.RawPart.IsDeleted)
                {
                    this.AddSeparatorLine(worksheet, ++rowToWriteInto);
                    PartCost rawPartCalculationResult = null;

                    if (calculationResult != null)
                    {
                        rawPartCalculationResult = calculationResult.PartsCost.PartCosts.FirstOrDefault(cost => cost.PartId == part.RawPart.Guid);
                    }

                    rowToWriteInto = this.WritePartInfo(worksheet, part.RawPart, ++rowToWriteInto, rawPartCalculationResult == null ? null : rawPartCalculationResult.FullCalculationResult);
                }
            }

            return rowToWriteInto;
        }

        /// <summary>
        /// Adds the main part cells.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="part">The part.</param>
        /// <param name="rowToWriteInto">The row to write into.</param>
        private void AddMainPartCells(ISheet worksheet, Part part, int rowToWriteInto)
        {
            var mainPartNameCell = GetOrCreateCell(worksheet, rowToWriteInto, AssembliesAndPartsColumnIndex);
            mainPartNameCell.SetCellValue(part.Name);

            ApplyPartStyleOnCell(mainPartNameCell, part is RawPart);
            var numberOfColumns = GetNumberOfUsedColumns(worksheet);

            for (var columnIndex = 1; columnIndex < numberOfColumns; columnIndex++)
            {
                ApplyPartStyleOnCell(GetOrCreateCell(worksheet, rowToWriteInto, columnIndex), part is RawPart);
            }
        }

        /// <summary>
        /// Adds the main assembly cells.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="assembly">The assembly.</param>
        /// <param name="rowToWriteInto">The row to write into.</param>
        private void AddMainAssemblyCells(ISheet worksheet, Assembly assembly, int rowToWriteInto)
        {
            var mainAssemblyNameCell = GetOrCreateCell(worksheet, rowToWriteInto, AssembliesAndPartsColumnIndex);
            mainAssemblyNameCell.SetCellValue(assembly.Name);
            ApplyAssemblyStyleOnCell(mainAssemblyNameCell);
            var numberOfColumns = GetNumberOfUsedColumns(worksheet);

            for (var columnIndex = 1; columnIndex < numberOfColumns; columnIndex++)
            {
                ApplyAssemblyStyleOnCell(GetOrCreateCell(worksheet, rowToWriteInto, columnIndex));
            }
        }

        /// <summary>
        /// Creates the headers.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="columnToWriteInto">The column to write into.</param>
        private void AddColumnHeaders(ISheet worksheet, int columnToWriteInto)
        {
            isAnyCostCategorySelected = false;

            InitializeOverheadAndMarginCostsCategoryItem();

            // Create the header cells of the properties
            foreach (var dataCategoryItem in DataCategories)
            {
                columnToWriteInto = this.AddColumnsForCategory(worksheet, columnToWriteInto, dataCategoryItem);
            }

            if (isAnyCostCategorySelected)
            {
                Part part = new Part();
                Assembly assembly = new Assembly();

                calculationSettingsCategoryItem = new DataCategoryItem(DataCategoryType.CalculationSettings, LocalizedResources.ReportGenerator_CalculationSettings);
                calculationSettingsCategoryItem.ColumnIndex = columnToWriteInto;

                DataFieldItem calculationVariantFieldItem = new DataFieldItem(calculationSettingsCategoryItem, () => assembly.CalculationVariant, string.Empty, LocalizedResources.General_CalculationVariant, () => part.CalculationVariant);
                calculationSettingsCategoryItem.DataFields.Add(calculationVariantFieldItem);
                calculationVariantFieldItem.ColumnIndex = columnToWriteInto++;

                var typeMappingsString = string.Format(
                    CultureInfo.InvariantCulture,
                    "{0} = {1} {2} = {3} {4} = {5}",
                    GetNumberForType(typeof(Assembly)),
                    LocalizedResources.General_Assembly,
                       GetNumberForType(typeof(Part)),
                    LocalizedResources.General_Part,
                       GetNumberForType(typeof(RawPart)),
                    LocalizedResources.General_RawPart);

                // Note: The Name property is set only to avoid passing null. It is not used.
                DataFieldItem typeFieldItem = new DataFieldItem(calculationSettingsCategoryItem, () => part.Name, string.Empty, typeMappingsString);
                calculationSettingsCategoryItem.DataFields.Add(typeFieldItem);
                typeFieldItem.ColumnIndex = columnToWriteInto;

                var mainHeaderCell = GetOrCreateCell(worksheet, DataCategoryHeaderRowIndex, calculationSettingsCategoryItem.ColumnIndex);
                this.ApplyDataCategoryBasedStyleOnCell(mainHeaderCell, DataCategoryType.CalculationSettings, cellType: CellType.BigHeader);

                CellRangeAddress processDataCellRangeAddress = new CellRangeAddress(DataCategoryHeaderRowIndex, DataCategoryHeaderRowIndex, calculationSettingsCategoryItem.ColumnIndex, calculationSettingsCategoryItem.ColumnIndex + 1);
                worksheet.AddMergedRegion(processDataCellRangeAddress);

                foreach (var dataFieldItem in calculationSettingsCategoryItem.DataFields)
                {
                    var dataFieldCell = GetOrCreateCell(worksheet, FieldHeaderRowIndex, dataFieldItem.ColumnIndex);
                    dataFieldCell.SetCellValue(dataFieldItem.DisplayName);
                    this.ApplyDataCategoryBasedStyleOnCell(dataFieldCell, DataCategoryType.CalculationSettings, cellType: CellType.SmallHeader);
                }
            }
        }

        /// <summary>
        /// Initializes the overhead and margin costs category item.
        /// </summary>
        private void InitializeOverheadAndMarginCostsCategoryItem()
        {
            var overheadAndMarginSettingsCategoryItem = DataCategories.FirstOrDefault(item => item.CategoryType == DataCategoryType.OverheadAndMarginSettings);

            if (overheadAndMarginSettingsCategoryItem != null && overheadAndMarginSettingsCategoryItem.IsChecked)
            {
                var currencySymbol = SecurityManager.Instance.UICurrency.Symbol;
                overheadAndMarginCostsCategoryItem = new DataCategoryItem(DataCategoryType.OverheadAndMarginCosts, LocalizedResources.General_OverheadAndMarginCost);

                foreach (var checkedField in overheadAndMarginSettingsCategoryItem.DataFields.Where(item => item.IsChecked && item.SecondProperty != null).ToList())
                {
                    overheadAndMarginCostsCategoryItem.DataFields.Add(new DataFieldItem(overheadAndMarginCostsCategoryItem, checkedField.SecondProperty, currencySymbol, checkedField.DisplayName) { IsChecked = true, IsCostProperty = true });
                }
            }
        }

        /// <summary>
        /// Adds the columns for category.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="columnToWriteInto">The column to write into.</param>
        /// <param name="dataCategoryItem">The data category item.</param>
        /// <returns>The next column to write into.</returns>
        private int AddColumnsForCategory(ISheet worksheet, int columnToWriteInto, DataCategoryItem dataCategoryItem)
        {
            IEnumerable<DataFieldItem> checkedFields = dataCategoryItem.DataFields.Where(field => field.IsChecked).ToList();
            dataCategoryItem.NumberOfColumns = checkedFields.Count();

            if (dataCategoryItem.NumberOfColumns > 0)
            {
                // Add the overhead and margin costs if the overhead and margin settings are checked
                if (dataCategoryItem.CategoryType == DataCategoryType.OverheadAndMarginSettings)
                {
                    columnToWriteInto = AddColumnsForCategory(worksheet, columnToWriteInto, overheadAndMarginCostsCategoryItem);
                }

                dataCategoryItem.ColumnIndex = columnToWriteInto;
                var labourDataFieldCount = checkedFields.Count(field => field.IsProcessStepLabourDataField);

                var mainHeaderCell = this.GetOrCreateCell(worksheet, DataCategoryHeaderRowIndex, dataCategoryItem.ColumnIndex);
                this.ApplyDataCategoryBasedStyleOnCell(mainHeaderCell, dataCategoryItem.CategoryType, cellType: CellType.BigHeader);

                if (checkedFields.Count() > labourDataFieldCount)
                {
                    CellRangeAddress processDataCellRangeAddress = new CellRangeAddress(DataCategoryHeaderRowIndex, DataCategoryHeaderRowIndex, dataCategoryItem.ColumnIndex, dataCategoryItem.ColumnIndex + dataCategoryItem.NumberOfColumns - labourDataFieldCount - 1);
                    worksheet.AddMergedRegion(processDataCellRangeAddress);
                }

                switch (dataCategoryItem.CategoryType)
                {
                    case DataCategoryType.Process:
                        mainHeaderCell.SetCellValue(LocalizedResources.General_AssemblingData);

                        // Note: Labour data cells have separate header.
                        if (labourDataFieldCount > 0)
                        {
                            var labourDataHeaderCell = this.GetOrCreateCell(worksheet, DataCategoryHeaderRowIndex, dataCategoryItem.ColumnIndex + dataCategoryItem.NumberOfColumns - labourDataFieldCount);
                            CellRangeAddress labourDataCellRangeAddress = new CellRangeAddress(DataCategoryHeaderRowIndex, DataCategoryHeaderRowIndex, dataCategoryItem.ColumnIndex + dataCategoryItem.NumberOfColumns - labourDataFieldCount, dataCategoryItem.ColumnIndex + dataCategoryItem.NumberOfColumns - 1);
                            worksheet.AddMergedRegion(labourDataCellRangeAddress);
                            labourDataHeaderCell.SetCellValue(LocalizedResources.ReportGenerator_LabourData);
                            this.labourDataColumnIndices.Add(dataCategoryItem.ColumnIndex + dataCategoryItem.NumberOfColumns - labourDataFieldCount);
                            this.ApplyDataCategoryBasedStyleOnCell(labourDataHeaderCell, DataCategoryType.Process, cellType: CellType.BigHeader);
                        }

                        break;
                    case DataCategoryType.ProcessCost:
                        mainHeaderCell.SetCellValue(LocalizedResources.ReportGenerator_AssemblingCost);
                        this.isAnyCostCategorySelected = true;
                        break;
                    case DataCategoryType.Machine:
                        mainHeaderCell.SetCellValue(LocalizedResources.ReportGenerator_MachineData);
                        break;
                    case DataCategoryType.MachineCost:
                        mainHeaderCell.SetCellValue(LocalizedResources.ReportGenerator_MachineCost);
                        this.isAnyCostCategorySelected = true;
                        break;
                    case DataCategoryType.Tooling:
                        mainHeaderCell.SetCellValue(LocalizedResources.General_ToolingData);
                        break;
                    case DataCategoryType.ToolingCost:
                        mainHeaderCell.SetCellValue(LocalizedResources.ReportGenerator_ToolingCost);
                        this.isAnyCostCategorySelected = true;
                        break;
                    case DataCategoryType.Consumable:
                        mainHeaderCell.SetCellValue(LocalizedResources.General_ConsumableData);
                        break;
                    case DataCategoryType.ConsumableCost:
                        mainHeaderCell.SetCellValue(LocalizedResources.ReportGenerator_ConsumableCost);
                        this.isAnyCostCategorySelected = true;
                        break;
                    case DataCategoryType.Commodity:
                        mainHeaderCell.SetCellValue(LocalizedResources.General_CommodityData);
                        break;
                    case DataCategoryType.CommodityCost:
                        mainHeaderCell.SetCellValue(LocalizedResources.ReportGenerator_CommodityCost);
                        this.isAnyCostCategorySelected = true;
                        break;
                    case DataCategoryType.RawPart:
                        mainHeaderCell.SetCellValue(LocalizedResources.ReportGenerator_RawPartData);
                        break;
                    case DataCategoryType.Material:
                        mainHeaderCell.SetCellValue(LocalizedResources.General_RawMaterialData);
                        break;
                    case DataCategoryType.MaterialCost:
                        mainHeaderCell.SetCellValue(LocalizedResources.ReportGenerator_RawMaterialCost);
                        this.isAnyCostCategorySelected = true;
                        break;
                    case DataCategoryType.CostSummary:
                        mainHeaderCell.SetCellValue(LocalizedResources.General_CostSummary);
                        this.isAnyCostCategorySelected = true;
                        break;
                    case DataCategoryType.OverheadAndMarginSettings:
                        mainHeaderCell.SetCellValue(LocalizedResources.ReportGenerator_OverheadAndMarginSettings);
                        this.isAnyCostCategorySelected = true;
                        break;
                    case DataCategoryType.CountrySettings:
                        mainHeaderCell.SetCellValue(LocalizedResources.ReportGenerator_CountrySettings);
                        this.isAnyCostCategorySelected = true;
                        break;
                    case DataCategoryType.OverheadAndMarginCosts:
                        mainHeaderCell.SetCellValue(LocalizedResources.General_OverheadAndMarginCost);
                        break;
                    default:
                        var msg = string.Format("Handling of DataCategory type '{0}' is not implemented when adding column headers.", dataCategoryItem.CategoryType);
                        throw new ArgumentOutOfRangeException(msg);
                }

                foreach (var dataFieldItem in checkedFields)
                {
                    dataFieldItem.ColumnIndex = columnToWriteInto++;
                    var dataFieldCell = this.GetOrCreateCell(worksheet, FieldHeaderRowIndex, dataFieldItem.ColumnIndex);
                    dataFieldCell.SetCellValue(dataFieldItem.DisplayName);

                    if (dataFieldItem.IsProcessStepLabourDataField && !this.labourDataColumnIndices.Contains(dataFieldItem.ColumnIndex))
                    {
                        this.labourDataColumnIndices.Add(dataFieldItem.ColumnIndex);
                    }

                    this.ApplyDataCategoryBasedStyleOnCell(dataFieldCell, dataCategoryItem.CategoryType, cellType: CellType.SmallHeader);

                    if (dataCategoryItem.CategoryType == DataCategoryType.CountrySettings && (dataFieldItem.Property.Name == "ProductionAreaRentalCost" || dataFieldItem.Property.Name == "OfficeAreaRentalCost"))
                    {
                        worksheet.SetColumnWidth(dataFieldItem.ColumnIndex, RentalCostColumnWidth);
                    }
                }

                // Group the columns if needed.
                if (dataCategoryItem.NumberOfColumns > 3)
                {
                    worksheet.GroupColumn(dataCategoryItem.ColumnIndex + 3, dataCategoryItem.ColumnIndex + dataCategoryItem.NumberOfColumns - 1);
                    worksheet.SetColumnGroupCollapsed(dataCategoryItem.ColumnIndex + 3, false);
                }
            }

            return columnToWriteInto;
        }

        /// <summary>
        /// Writes the process step values.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="steps">The steps.</param>
        /// <param name="rowToWriteInto">The row to write into.</param>
        /// <param name="calculationResult">The calculation result.</param>
        /// <param name="overheadSetting">The overhead setting.</param>
        /// <param name="countrySetting">The country setting.</param>
        /// <returns>
        /// The total number of rows it was written into.
        /// </returns>
        private int WriteProcessStepValues(ISheet worksheet, IList<ProcessStep> steps, int rowToWriteInto, CalculationResult calculationResult, OverheadSetting overheadSetting, CountrySetting countrySetting)
        {
            var indexOfOriginalRow = rowToWriteInto;

            if (steps.Any())
            {
                rowToWriteInto++;
            }

            object parentObject = null;

            foreach (var processStep in steps)
            {
                // Add the Step
                var parentCell = GetOrCreateCell(worksheet, rowToWriteInto, AssembliesAndPartsColumnIndex);
                ProcessStepCost processStepCost = null;

                if (calculationResult != null)
                {
                    processStepCost = calculationResult.ProcessCost.StepCosts.FirstOrDefault(cost => cost.StepId == processStep.Guid);
                }

                if (processStep is AssemblyProcessStep)
                {
                    ApplyAssemblyStyleOnCell(parentCell);
                    parentObject = processStep.Process.Assemblies.FirstOrDefault();
                }
                else
                {
                    parentObject = processStep.Process.Parts.FirstOrDefault();
                    ApplyPartStyleOnCell(parentCell, parentObject is RawPart);
                }

                var processCell = GetOrCreateCell(worksheet, rowToWriteInto, SubPartsAndProcessColumnIndex);
                processCell.SetCellValue(processStep.Name);
                ApplyDataCategoryBasedStyleOnCell(processCell, DataCategoryType.Process);

                var processDataCategoryItem = DataCategories.FirstOrDefault(item => item.CategoryType == DataCategoryType.Process);

                if (processDataCategoryItem != null && processDataCategoryItem.IsChecked)
                {
                    WriteProcessStepValues(worksheet, rowToWriteInto, processStep, processDataCategoryItem.DataFields.Where(field => field.IsChecked));
                }

                if (calculationResult != null)
                {
                    WriteProcessStepCostValues(worksheet, rowToWriteInto, processStepCost);
                }

                var originalRowToWriteInto = rowToWriteInto;

                // Add machine info
                var numberOfMachineAddedRows = WriteMachineValues(worksheet, rowToWriteInto, processStep, processDataCategoryItem, processStepCost);

                // Add tooling info.
                var numberOfToolingAddedRows = WriteToolingValues(worksheet, originalRowToWriteInto, processStep, processDataCategoryItem, numberOfMachineAddedRows, processStepCost);
                numberOfMachineAddedRows = Math.Max(numberOfMachineAddedRows, numberOfMachineAddedRows + numberOfToolingAddedRows);

                // Add commodities
                var numberOfCommoditiesAddedRows = WriteCommoditiesValues(worksheet, originalRowToWriteInto, processStep, processDataCategoryItem, numberOfMachineAddedRows, calculationResult);
                numberOfMachineAddedRows = Math.Max(numberOfMachineAddedRows, numberOfMachineAddedRows + numberOfCommoditiesAddedRows);

                // Add consumables                
                var numberOfConsumablesAddedRows = WriteConsumablesValues(worksheet, originalRowToWriteInto, processStep, processDataCategoryItem, numberOfMachineAddedRows, calculationResult);
                numberOfMachineAddedRows = Math.Max(numberOfMachineAddedRows, numberOfMachineAddedRows + numberOfConsumablesAddedRows);

                rowToWriteInto += numberOfMachineAddedRows + 1;
            }

            return rowToWriteInto - indexOfOriginalRow;
        }

        /// <summary>
        /// Writes the calculation settings.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="parentObject">The parent object.</param>
        /// <param name="rowToWriteInto">The row to write into.</param>
        private void WriteCalculationSettings(ISheet worksheet, object parentObject, int rowToWriteInto)
        {
            var calculationVariantDataFieldItemCell = GetOrCreateCell(worksheet, rowToWriteInto, calculationSettingsCategoryItem.DataFields[0].ColumnIndex);
            ApplyDataCategoryBasedStyleOnCell(calculationVariantDataFieldItemCell, DataCategoryType.CalculationSettings, GetFormat(parentObject, calculationSettingsCategoryItem.DataFields[0], parentObject is Part));
            SetCellValue(
                parentObject,
                parentObject is Assembly ? calculationSettingsCategoryItem.DataFields[0].Property : calculationSettingsCategoryItem.DataFields[0].SecondProperty,
                calculationSettingsCategoryItem.DataFields[0],
                calculationVariantDataFieldItemCell);

            var itemTypeDataFieldItemCell = GetOrCreateCell(worksheet, rowToWriteInto, calculationSettingsCategoryItem.DataFields[1].ColumnIndex);

            if (parentObject is Assembly)
            {
                ApplyAssemblyStyleOnCell(itemTypeDataFieldItemCell);
            }
            else
            {
                ApplyPartStyleOnCell(itemTypeDataFieldItemCell, parentObject is RawPart);
            }

            itemTypeDataFieldItemCell.SetCellValue(GetNumberForType(parentObject.GetType()));
        }

        /// <summary>
        /// Gets the type of the number for.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>The number corresponding for the item type.</returns>
        private int GetNumberForType(Type type)
        {
            if (type == typeof(Assembly))
            {
                return 1;
            }

            if (type == typeof(Part))
            {
                return 2;
            }

            if (type == typeof(RawPart))
            {
                return 3;
            }

            return 0;
        }

        /// <summary>
        /// Writes the consumables values.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="originalRowToWriteInto">The original row to write into.</param>
        /// <param name="processStep">The process step.</param>
        /// <param name="processDataCategoryItem">The process data category item.</param>
        /// <param name="addedRows">The added rows.</param>
        /// <param name="calculationResult">The calculation result.</param>
        /// <returns>
        /// The number of rows added besides the default one to write consumable info into.
        /// </returns>
        private int WriteConsumablesValues(ISheet worksheet, int originalRowToWriteInto, ProcessStep processStep, DataCategoryItem processDataCategoryItem, int addedRows, CalculationResult calculationResult)
        {
            var additionalAddedRows = 0;
            var generateCost = false;
            var generateConsumableValues = false;
            var consumableCostCategoryItem = DataCategories.FirstOrDefault(item => item.CategoryType == DataCategoryType.ConsumableCost);
            if (consumableCostCategoryItem != null && consumableCostCategoryItem.IsChecked)
            {
                generateCost = true;

                if (calculationResult == null)
                {
                    log.Error("Consumable cost generation is checked but the calculation result is null.");
                    generateCost = false;
                }
            }

            var consumableCategoryItem = DataCategories.FirstOrDefault(item => item.CategoryType == DataCategoryType.Consumable);
            if (consumableCategoryItem != null && consumableCategoryItem.IsChecked)
            {
                generateConsumableValues = true;
            }

            if (generateCost || generateConsumableValues)
            {
                var rowToWriteInto = originalRowToWriteInto;
                var consumables = processStep.Consumables.Where(consumable => !consumable.IsDeleted).ToList();

                for (var consumableIndex = 0; consumableIndex < consumables.Count; consumableIndex++)
                {
                    if (generateConsumableValues)
                    {
                        foreach (var dataFieldItem in consumableCategoryItem.DataFields.Where(field => field.IsChecked))
                        {
                            var dataFieldItemCell = GetOrCreateCell(worksheet, rowToWriteInto, dataFieldItem.ColumnIndex);
                            ApplyDataCategoryBasedStyleOnCell(dataFieldItemCell, DataCategoryType.Consumable, GetFormat(consumables[consumableIndex], dataFieldItem));
                            SetCellValue(consumables[consumableIndex], dataFieldItem.Property, dataFieldItem, dataFieldItemCell);
                        }
                    }

                    if (generateCost)
                    {
                        var consumableCost = calculationResult.ConsumablesCost.ConsumableCosts.FirstOrDefault(cost => cost.ConsumableId == consumables[consumableIndex].Guid);

                        if (consumableCost != null)
                        {
                            foreach (var costItem in consumableCostCategoryItem.DataFields.Where(field => field.IsChecked))
                            {
                                var dataFieldItemCell = GetOrCreateCell(worksheet, rowToWriteInto, costItem.ColumnIndex);
                                ApplyDataCategoryBasedStyleOnCell(dataFieldItemCell, DataCategoryType.ConsumableCost, GetFormat(consumableCost, costItem));
                                SetCellValue(consumableCost, costItem.Property, costItem, dataFieldItemCell);
                            }
                        }
                    }

                    if (consumableIndex > addedRows)
                    {
                        additionalAddedRows++;
                        FillAdditionalAssemblyValues(worksheet, processStep, processDataCategoryItem, rowToWriteInto);
                    }

                    rowToWriteInto++;
                }
            }

            return additionalAddedRows;
        }

        /// <summary>
        /// Writes the commodities values.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="originalRowToWriteInto">The original row to write into.</param>
        /// <param name="processStep">The process step.</param>
        /// <param name="processDataCategoryItem">The process data category item.</param>
        /// <param name="addedRows">The added rows.</param>
        /// <param name="calculationResult">The calculation result.</param>
        /// <returns>
        /// The number of rows added besides the default one to write commodity info into.
        /// </returns>
        private int WriteCommoditiesValues(ISheet worksheet, int originalRowToWriteInto, ProcessStep processStep, DataCategoryItem processDataCategoryItem, int addedRows, CalculationResult calculationResult)
        {
            var additionalAddedRows = 0;
            var generateCost = false;
            var generateCommodityValues = false;
            var commodityCostCategoryItem = DataCategories.FirstOrDefault(item => item.CategoryType == DataCategoryType.CommodityCost);
            if (commodityCostCategoryItem != null && commodityCostCategoryItem.IsChecked)
            {
                generateCost = true;

                if (calculationResult == null)
                {
                    log.Error("Commodity cost generation is checked but the calculation result is null.");
                    generateCost = false;
                }
            }

            var commodityCategoryItem = DataCategories.FirstOrDefault(item => item.CategoryType == DataCategoryType.Commodity);
            if (commodityCategoryItem != null && commodityCategoryItem.IsChecked)
            {
                generateCommodityValues = true;
            }

            if (generateCost || generateCommodityValues)
            {
                var rowToWriteInto = originalRowToWriteInto;
                var commodities = processStep.Commodities.Where(commodity => !commodity.IsDeleted).ToList();

                for (var commodityIndex = 0; commodityIndex < commodities.Count; commodityIndex++)
                {
                    if (generateCommodityValues)
                    {
                        foreach (var dataFieldItem in commodityCategoryItem.DataFields.Where(field => field.IsChecked))
                        {
                            var dataFieldItemCell = GetOrCreateCell(worksheet, rowToWriteInto, dataFieldItem.ColumnIndex);
                            ApplyDataCategoryBasedStyleOnCell(dataFieldItemCell, DataCategoryType.Commodity, GetFormat(commodities[commodityIndex], dataFieldItem));
                            SetCellValue(commodities[commodityIndex], dataFieldItem.Property, dataFieldItem, dataFieldItemCell);
                        }
                    }

                    if (generateCost)
                    {
                        var commodityCost = calculationResult.CommoditiesCost.CommodityCosts.FirstOrDefault(cost => cost.CommodityId == commodities[commodityIndex].Guid);

                        if (commodityCost != null)
                        {
                            foreach (var costItem in commodityCostCategoryItem.DataFields.Where(field => field.IsChecked))
                            {
                                var dataFieldItemCell = GetOrCreateCell(worksheet, rowToWriteInto, costItem.ColumnIndex);
                                ApplyDataCategoryBasedStyleOnCell(dataFieldItemCell, DataCategoryType.CommodityCost, GetFormat(commodityCost, costItem));
                                SetCellValue(commodityCost, costItem.Property, costItem, dataFieldItemCell);
                            }
                        }
                    }

                    if (commodityIndex > addedRows)
                    {
                        additionalAddedRows++;
                        FillAdditionalAssemblyValues(worksheet, processStep, processDataCategoryItem, rowToWriteInto);
                    }

                    rowToWriteInto++;
                }
            }

            return additionalAddedRows;
        }

        /// <summary>
        /// Writes the commodities values.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="originalRowToWriteInto">The original row to write into.</param>
        /// <param name="part">The part.</param>
        /// <param name="calculationResult">The calculation result.</param>
        /// <returns>
        /// The number of rows added besides the default one to write commodity info into.
        /// </returns>
        private int WriteCommoditiesValues(ISheet worksheet, int originalRowToWriteInto, Part part, CalculationResult calculationResult)
        {
            var additionalAddedRows = 0;
            var generateCost = false;
            var generateCommodityValues = false;
            var commodityCostCategoryItem = DataCategories.FirstOrDefault(item => item.CategoryType == DataCategoryType.CommodityCost);

            if (commodityCostCategoryItem != null && commodityCostCategoryItem.IsChecked && calculationResult != null)
            {
                generateCost = true;
            }

            var commodityCategoryItem = DataCategories.FirstOrDefault(item => item.CategoryType == DataCategoryType.Commodity);
            if (commodityCategoryItem != null && commodityCategoryItem.IsChecked)
            {
                generateCommodityValues = true;
            }

            if (generateCost || generateCommodityValues)
            {
                var rowToWriteInto = originalRowToWriteInto;
                var commodities = part.Commodities.Where(commodity => !commodity.IsDeleted).ToList();

                foreach (var commodity in commodities)
                {
                    var partCell = this.GetOrCreateCell(worksheet, rowToWriteInto, AssembliesAndPartsColumnIndex);
                    this.ApplyPartStyleOnCell(partCell, part is RawPart);

                    if (generateCommodityValues)
                    {
                        foreach (var dataFieldItem in commodityCategoryItem.DataFields.Where(field => field.IsChecked))
                        {
                            var dataFieldItemCell = this.GetOrCreateCell(worksheet, rowToWriteInto, dataFieldItem.ColumnIndex);
                            ApplyDataCategoryBasedStyleOnCell(dataFieldItemCell, DataCategoryType.Commodity, this.GetFormat(commodity, dataFieldItem));
                            SetCellValue(commodity, dataFieldItem.Property, dataFieldItem, dataFieldItemCell);
                        }
                    }

                    if (generateCost)
                    {
                        var commodityCost = calculationResult.CommoditiesCost.CommodityCosts.FirstOrDefault(cost => cost.CommodityId == commodity.Guid);

                        if (commodityCost != null)
                        {
                            foreach (var costItem in commodityCostCategoryItem.DataFields.Where(field => field.IsChecked))
                            {
                                var dataFieldItemCell = GetOrCreateCell(worksheet, rowToWriteInto, costItem.ColumnIndex);
                                ApplyDataCategoryBasedStyleOnCell(dataFieldItemCell, DataCategoryType.CommodityCost, GetFormat(commodityCost, costItem));
                                SetCellValue(commodityCost, costItem.Property, costItem, dataFieldItemCell);
                            }
                        }
                    }

                    rowToWriteInto++;
                }

                additionalAddedRows = commodities.Count;
            }

            return additionalAddedRows;
        }

        /// <summary>
        /// Writes the tooling values.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="originalRowToWriteInto">The original row to write into.</param>
        /// <param name="processStep">The process step.</param>
        /// <param name="processDataCategoryItem">The process data category item.</param>
        /// <param name="addedRows">The added rows.</param>
        /// <param name="processStepCost">The process step cost.</param>
        /// <returns>
        /// The number of rows added besides the default one to write tooling info into.
        /// </returns>
        private int WriteToolingValues(
            ISheet worksheet,
            int originalRowToWriteInto,
            ProcessStep processStep,
            DataCategoryItem processDataCategoryItem,
            int addedRows,
            ProcessStepCost processStepCost)
        {
            var additionalAddedRows = 0;
            var generateCost = false;
            var generateToolingValues = false;
            var toolingCostCategoryItem = DataCategories.FirstOrDefault(item => item.CategoryType == DataCategoryType.ToolingCost);

            if (toolingCostCategoryItem != null && toolingCostCategoryItem.IsChecked && processStepCost != null)
            {
                generateCost = true;
            }

            var toolingCategoryItem = DataCategories.FirstOrDefault(item => item.CategoryType == DataCategoryType.Tooling);
            if (toolingCategoryItem != null && toolingCategoryItem.IsChecked)
            {
                generateToolingValues = true;
            }

            if (generateCost || generateToolingValues)
            {
                var rowToWriteInto = originalRowToWriteInto;
                var dies = processStep.Dies.Where(item => !item.IsDeleted).ToList();

                for (var dieIndex = 0; dieIndex < dies.Count; dieIndex++)
                {
                    var currentDie = dies[dieIndex];

                    if (generateToolingValues)
                    {
                        foreach (var dataFieldItem in toolingCategoryItem.DataFields.Where(field => field.IsChecked))
                        {
                            var dataFieldItemCell = GetOrCreateCell(worksheet, rowToWriteInto, dataFieldItem.ColumnIndex);
                            this.ApplyDataCategoryBasedStyleOnCell(dataFieldItemCell, DataCategoryType.Tooling, GetFormat(currentDie, dataFieldItem));
                            SetCellValue(currentDie, dataFieldItem.Property, dataFieldItem, dataFieldItemCell);
                        }
                    }

                    if (generateCost)
                    {
                        var dieCost = processStepCost.DieCosts.FirstOrDefault(c => c.DieId == currentDie.Guid);
                        if (dieCost != null)
                        {
                            foreach (var costItem in toolingCostCategoryItem.DataFields.Where(field => field.IsChecked && field.Property.DeclaringType != typeof(InvestmentCostSingle)).ToList())
                            {
                                var dataFieldItemCell = GetOrCreateCell(worksheet, rowToWriteInto, costItem.ColumnIndex);
                                this.ApplyDataCategoryBasedStyleOnCell(dataFieldItemCell, DataCategoryType.ToolingCost, GetFormat(dieCost, costItem));
                                SetCellValue(dieCost, costItem.Property, costItem, dataFieldItemCell);
                            }
                        }

                        var investmentCost = processStepCost.InvestmentCost.DieInvestments.FirstOrDefault(cost => cost.ParentId == currentDie.Guid);
                        if (investmentCost != null)
                        {
                            foreach (var investmentCostItem in toolingCostCategoryItem.DataFields.Where(field => field.IsChecked && field.Property.DeclaringType == typeof(InvestmentCostSingle)).ToList())
                            {
                                var dataFieldItemCell = GetOrCreateCell(worksheet, rowToWriteInto, investmentCostItem.ColumnIndex);
                                this.ApplyDataCategoryBasedStyleOnCell(dataFieldItemCell, DataCategoryType.ToolingCost, GetFormat(investmentCost, investmentCostItem));
                                SetCellValue(investmentCost, investmentCostItem.Property, investmentCostItem, dataFieldItemCell);
                            }
                        }
                    }

                    if (dieIndex > addedRows)
                    {
                        additionalAddedRows++;
                        FillAdditionalAssemblyValues(worksheet, processStep, processDataCategoryItem, rowToWriteInto);
                    }

                    rowToWriteInto++;
                }
            }

            return additionalAddedRows;
        }

        /// <summary>
        /// Fills the additional assembly values.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="processStep">The process step.</param>
        /// <param name="processDataCategoryItem">The process data category item.</param>
        /// <param name="rowToWriteInto">The row to write into.</param>
        private void FillAdditionalAssemblyValues(ISheet worksheet, ProcessStep processStep, DataCategoryItem processDataCategoryItem, int rowToWriteInto)
        {
            if (processDataCategoryItem != null && processDataCategoryItem.IsChecked)
            {
                var parentCell = GetOrCreateCell(worksheet, rowToWriteInto, AssembliesAndPartsColumnIndex);

                if (processStep is AssemblyProcessStep)
                {
                    ApplyAssemblyStyleOnCell(parentCell);
                }
                else
                {
                    ApplyPartStyleOnCell(parentCell, processStep.Process.Parts.FirstOrDefault() is RawPart);
                }

                foreach (var dataFieldItem in processDataCategoryItem.DataFields.Where(field => field.IsChecked))
                {
                    var dataFieldItemCell = GetOrCreateCell(worksheet, rowToWriteInto, dataFieldItem.ColumnIndex);
                    ApplyDataCategoryBasedStyleOnCell(dataFieldItemCell, DataCategoryType.Process, GetFormat(processStep, dataFieldItem));
                    SetCellValue(processStep, dataFieldItem.Property, dataFieldItem, dataFieldItemCell);
                }
            }
        }

        /// <summary>
        /// Writes the raw material values.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="rowToWriteInto">The row to write into.</param>
        /// <param name="part">The part.</param>
        /// <param name="calculationResult">The calculation result.</param>
        /// <returns>
        /// The number of added lines.
        /// </returns>
        private int WriteRawMaterialValues(ISheet worksheet, int rowToWriteInto, Part part, CalculationResult calculationResult)
        {
            var numberOfAddedRows = 0;
            var generateCost = false;
            var generateRawMaterialValues = false;
            var rawMaterialCostCategoryItem = DataCategories.FirstOrDefault(item => item.CategoryType == DataCategoryType.MaterialCost);

            if (rawMaterialCostCategoryItem != null && rawMaterialCostCategoryItem.IsChecked && calculationResult != null)
            {
                generateCost = true;
            }

            var rawMaterialCategoryItem = DataCategories.FirstOrDefault(item => item.CategoryType == DataCategoryType.Material);
            if (rawMaterialCategoryItem != null && rawMaterialCategoryItem.IsChecked)
            {
                generateRawMaterialValues = true;
            }

            if (generateCost || generateRawMaterialValues)
            {
                var rawMaterials = part.RawMaterials.Where(item => !item.IsDeleted).ToList();
                foreach (var rawMaterial in rawMaterials)
                {
                    var partCell = GetOrCreateCell(worksheet, rowToWriteInto, AssembliesAndPartsColumnIndex);
                    ApplyPartStyleOnCell(partCell, part is RawPart);

                    if (generateRawMaterialValues)
                    {
                        foreach (var dataFieldItem in rawMaterialCategoryItem.DataFields.Where(item => item.IsChecked).ToList())
                        {
                            var dataFieldItemCell = GetOrCreateCell(worksheet, rowToWriteInto, dataFieldItem.ColumnIndex);
                            this.ApplyDataCategoryBasedStyleOnCell(dataFieldItemCell, DataCategoryType.Material, GetFormat(rawMaterial, dataFieldItem));
                            SetCellValue(rawMaterial, dataFieldItem.Property, dataFieldItem, dataFieldItemCell);
                        }
                    }

                    if (generateCost)
                    {
                        var rawMaterialCost = calculationResult.RawMaterialsCost.RawMaterialCosts.FirstOrDefault(cost => cost.RawMaterialId == rawMaterial.Guid);

                        if (rawMaterialCost != null)
                        {
                            foreach (var dataFieldItem in rawMaterialCostCategoryItem.DataFields.Where(item => item.IsChecked).ToList())
                            {
                                var dataFieldItemCell = GetOrCreateCell(worksheet, rowToWriteInto, dataFieldItem.ColumnIndex);
                                this.ApplyDataCategoryBasedStyleOnCell(dataFieldItemCell, DataCategoryType.MaterialCost, GetFormat(rawMaterialCost, dataFieldItem));
                                SetCellValue(rawMaterialCost, dataFieldItem.Property, dataFieldItem, dataFieldItemCell);
                            }
                        }
                    }

                    rowToWriteInto++;
                }

                numberOfAddedRows = rawMaterials.Count;

                // Writing RawPart related info since it belongs to the raw materials.
                if (part.RawPart != null && !part.RawPart.IsDeleted)
                {
                    numberOfAddedRows++;

                    if (generateRawMaterialValues)
                    {
                        var partCell = GetOrCreateCell(worksheet, rowToWriteInto, AssembliesAndPartsColumnIndex);
                        ApplyPartStyleOnCell(partCell, part is RawPart);

                        var nameFieldItem = rawMaterialCategoryItem.DataFields.FirstOrDefault(item => item.Property.Name == "Name");

                        if (nameFieldItem != null)
                        {
                            var rawPartNameCell = GetOrCreateCell(worksheet, rowToWriteInto, nameFieldItem.ColumnIndex);
                            this.ApplyDataCategoryBasedStyleOnCell(rawPartNameCell, DataCategoryType.RawPart);
                            rawPartNameCell.SetCellValue(part.RawPart.Name);
                        }
                    }

                    if (generateCost)
                    {
                        var rawPartCost = calculationResult.RawMaterialsCost.RawMaterialCosts.FirstOrDefault(cost => cost.RawMaterialId == part.RawPart.Guid);

                        if (rawPartCost != null)
                        {
                            foreach (var dataFieldItem in rawMaterialCostCategoryItem.DataFields.Where(item => item.IsChecked).ToList())
                            {
                                var dataFieldItemCell = GetOrCreateCell(worksheet, rowToWriteInto, dataFieldItem.ColumnIndex);
                                this.ApplyDataCategoryBasedStyleOnCell(dataFieldItemCell, DataCategoryType.MaterialCost, GetFormat(rawPartCost, dataFieldItem));
                                SetCellValue(rawPartCost, dataFieldItem.Property, dataFieldItem, dataFieldItemCell);
                            }
                        }
                    }
                }
            }

            return numberOfAddedRows;
        }

        /// <summary>
        /// Writes the machine values.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="rowToWriteInto">The row to write into.</param>
        /// <param name="processStep">The process step.</param>
        /// <param name="processDataCategoryItem">The process data category item.</param>
        /// <param name="processStepCost">The process step cost.</param>
        /// <returns>
        /// The number of rows added besides the default one to write machine info into.
        /// </returns>
        private int WriteMachineValues(ISheet worksheet, int rowToWriteInto, ProcessStep processStep, DataCategoryItem processDataCategoryItem, ProcessStepCost processStepCost)
        {
            var numberOfRowsAdded = 0;
            var generateCost = false;
            var generateMachineValues = false;

            var machineCostCategoryItem = DataCategories.FirstOrDefault(item => item.CategoryType == DataCategoryType.MachineCost);
            if (machineCostCategoryItem != null && machineCostCategoryItem.IsChecked && processStepCost != null)
            {
                generateCost = true;
            }

            var machineCategoryItem = DataCategories.FirstOrDefault(item => item.CategoryType == DataCategoryType.Machine);
            if (machineCategoryItem != null && machineCategoryItem.IsChecked)
            {
                generateMachineValues = true;
            }

            if (generateCost || generateMachineValues)
            {
                var machines = processStep.Machines.Where(item => !item.IsDeleted).OrderBy(item => item.Index).ToList();

                for (var machineIndex = 0; machineIndex < machines.Count; machineIndex++)
                {
                    if (generateMachineValues)
                    {
                        foreach (var dataFieldItem in machineCategoryItem.DataFields.Where(field => field.IsChecked).ToList())
                        {
                            var dataFieldItemCell = GetOrCreateCell(worksheet, rowToWriteInto, dataFieldItem.ColumnIndex);
                            this.ApplyDataCategoryBasedStyleOnCell(dataFieldItemCell, DataCategoryType.Machine, GetFormat(machines[machineIndex], dataFieldItem));
                            SetCellValue(machines[machineIndex], dataFieldItem.Property, dataFieldItem, dataFieldItemCell);
                        }
                    }

                    if (generateCost)
                    {
                        var machineCost = processStepCost.MachineCosts.FirstOrDefault(cost => cost.MachineId == machines[machineIndex].Guid);

                        if (machineCost != null)
                        {
                            foreach (var machinCostItem in machineCostCategoryItem.DataFields.Where(field => field.IsChecked && field.Property.DeclaringType != typeof(InvestmentCostSingle)).ToList())
                            {
                                var dataFieldItemCell = GetOrCreateCell(worksheet, rowToWriteInto, machinCostItem.ColumnIndex);
                                this.ApplyDataCategoryBasedStyleOnCell(dataFieldItemCell, DataCategoryType.MachineCost, GetFormat(machineCost, machinCostItem));
                                SetCellValue(machineCost, machinCostItem.Property, machinCostItem, dataFieldItemCell);
                            }
                        }

                        var investmentCost = processStepCost.InvestmentCost.MachineInvestments.FirstOrDefault(cost => cost.ParentId == machines[machineIndex].Guid);

                        if (investmentCost != null)
                        {
                            foreach (var investmentCostItem in machineCostCategoryItem.DataFields.Where(field => field.IsChecked && field.Property.DeclaringType == typeof(InvestmentCostSingle)).ToList())
                            {
                                var dataFieldItemCell = GetOrCreateCell(worksheet, rowToWriteInto, investmentCostItem.ColumnIndex);
                                this.ApplyDataCategoryBasedStyleOnCell(dataFieldItemCell, DataCategoryType.MachineCost, GetFormat(investmentCost, investmentCostItem));
                                SetCellValue(investmentCost, investmentCostItem.Property, investmentCostItem, dataFieldItemCell);
                            }
                        }
                    }

                    if (machineIndex != machines.Count - 1)
                    {
                        rowToWriteInto++;
                        FillAdditionalAssemblyValues(worksheet, processStep, processDataCategoryItem, rowToWriteInto);
                    }
                }

                numberOfRowsAdded = machines.Count > 1 ? machines.Count - 1 : 0;
            }

            return numberOfRowsAdded;
        }

        /// <summary>
        /// Writes the process step values.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="rowToWriteInto">The row to write into.</param>
        /// <param name="processStep">The process step.</param>
        /// <param name="dataFields">The data fields.</param>
        private void WriteProcessStepValues(ISheet worksheet, int rowToWriteInto, ProcessStep processStep, IEnumerable<DataFieldItem> dataFields)
        {
            foreach (var dataFieldItem in dataFields)
            {
                var dataFieldItemCell = GetOrCreateCell(worksheet, rowToWriteInto, dataFieldItem.ColumnIndex);
                ApplyDataCategoryBasedStyleOnCell(dataFieldItemCell, DataCategoryType.Process, GetFormat(processStep, dataFieldItem));
                SetCellValue(processStep, dataFieldItem.Property, dataFieldItem, dataFieldItemCell);
            }
        }

        /// <summary>
        /// Sets the cell value.
        /// </summary>
        /// <param name="sourceObject">The source object.</param>
        /// <param name="propertyInfo">The property info.</param>
        /// <param name="theDataFieldItem">The data field item.</param>
        /// <param name="dataFieldItemCell">The data field item cell.</param>
        private void SetCellValue(object sourceObject, Reflection.PropertyInfo propertyInfo, DataFieldItem theDataFieldItem, ICell dataFieldItemCell)
        {
            if (theDataFieldItem != null && propertyInfo != null && HasProperty(sourceObject, propertyInfo.Name))
            {
                var value = propertyInfo.GetValue(sourceObject, null);

                if (value is bool)
                {
                    dataFieldItemCell.SetCellValue((bool)value ? LocalizedResources.General_Yes : LocalizedResources.General_No);
                }
                else
                {
                    if (theDataFieldItem.TargetEnumType != null)
                    {
                        var intValue = Convert.ToInt32(value);

                        if (Enum.IsDefined(theDataFieldItem.TargetEnumType, intValue))
                        {
                            var enumValue = Enum.ToObject(theDataFieldItem.TargetEnumType, intValue).ToString();
                            var localizedValue = LocalizedResources.ResourceManager.GetString(theDataFieldItem.TargetEnumType.Name + "_" + enumValue, LocalizedResources.Culture);
                            dataFieldItemCell.SetCellValue(localizedValue);
                        }
                        else
                        {
                            dataFieldItemCell.SetCellValue(value);
                        }
                    }
                    else
                    {
                        dataFieldItemCell.SetCellValue(value);

                        // If the property is the Price on a raw material object then the price value needs few adjustments
                        // The Price is depending on the weight unit on the UI and the weight unit on the object
                        var material = sourceObject as RawMaterial;
                        if (material != null && propertyInfo.Name == ReflectionUtils.GetPropertyName(() => material.Price))
                        {
                            var weightUnit = this.MeasurementUnitsAdapter.GetBaseMeasurementUnit(MeasurementUnitType.Weight);
                            var priceUnit = material.QuantityUnitBase ?? material.ParentWeightUnitBase;
                            var price = CostCalculationHelper.AdjustRawMaterialPriceToNewWeightUnit((decimal)dataFieldItemCell.NumericCellValue, priceUnit, weightUnit);
                            dataFieldItemCell.SetCellValue(price);
                        }

                        if (dataFieldItemCell.CellType == NPOI.SS.UserModel.CellType.NUMERIC)
                        {
                            if (theDataFieldItem.IsPercent)
                            {
                                dataFieldItemCell.SetCellValue(dataFieldItemCell.NumericCellValue * 100);
                            }
                            else
                            {
                                if (theDataFieldItem.UnitType != null)
                                {
                                    var measurementUnitType = (MeasurementUnitType)theDataFieldItem.UnitType;

                                    // Get the UI base measurement unit and convert the value. 
                                    // In some cases the database value is saved into another measurement system (saved in metric and shown on UI in imperial system)
                                    MeasurementUnit baseMeasurementUnit = this.MeasurementUnitsAdapter.GetBaseMeasurementUnit(measurementUnitType);
                                    if (baseMeasurementUnit != null)
                                    {
                                        dataFieldItemCell.SetCellValue((decimal)dataFieldItemCell.NumericCellValue / baseMeasurementUnit.ConversionRate);
                                    }
                                }
                                else
                                {
                                    if (theDataFieldItem.HasDynamicUnit)
                                    {
                                        var unitValue = theDataFieldItem.UnitPropertyInfo.GetValue(sourceObject, null);

                                        if (unitValue != null)
                                        {
                                            MeasurementUnit measurementUnit = unitValue as MeasurementUnit;

                                            if (measurementUnit != null)
                                            {
                                                dataFieldItemCell.SetCellValue((decimal)dataFieldItemCell.NumericCellValue / measurementUnit.ConversionRate);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (theDataFieldItem.IsCostProperty)
                                        {
                                            var convertedValue = CurrencyConversionManager.ConvertBaseValueToDisplayValue(
                                                                              (decimal)dataFieldItemCell.NumericCellValue,
                                                                              this.MeasurementUnitsAdapter.BaseCurrency.ExchangeRate,
                                                                              this.MeasurementUnitsAdapter.UICurrency.ConversionFactor);
                                            dataFieldItemCell.SetCellValue(convertedValue);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Adds the separator line.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="rowNumber">The row number.</param>
        private void AddSeparatorLine(ISheet worksheet, int rowNumber)
        {
            var row = worksheet.GetRow(rowNumber) ?? worksheet.CreateRow(rowNumber);

            row.Height = templateWorksheet.GetRow(6).Height;
            var numberOfColumns = GetNumberOfUsedColumns(worksheet);

            for (var columnIndex = 0; columnIndex < numberOfColumns; columnIndex++)
            {
                var cell = GetOrCreateCell(worksheet, rowNumber, columnIndex);
                cell.CellStyle = templateWorksheet.GetRow(6).GetCell(0).CellStyle;
            }
        }

        /// <summary>
        /// Applies the assembly style on cell.
        /// </summary>
        /// <param name="cell">The cell.</param>
        /// <param name="format">The format.</param>
        /// <param name="cellType">Type of the cell.</param>
        private void ApplyAssemblyStyleOnCell(ICell cell, string format = DefaultCellFormat, CellType cellType = CellType.Normal)
        {
            this.ApplyStyleOnCell(cell, format, cellType, AssemblyColorIndex);
        }

        /// <summary>
        /// Applies the part style on cell.
        /// </summary>
        /// <param name="cell">The cell.</param>
        /// <param name="isRawPart">if set to <c>true</c> [is raw part].</param>
        /// <param name="format">The format.</param>
        /// <param name="cellType">Type of the cell.</param>
        private void ApplyPartStyleOnCell(ICell cell, bool isRawPart, string format = DefaultCellFormat, CellType cellType = CellType.Normal)
        {
            var colorIndex = isRawPart ? RawPartColorIndex : PartColorIndex;
            this.ApplyStyleOnCell(cell, format, cellType, colorIndex);
        }

        /// <summary>
        /// Applies the style on cell.
        /// </summary>
        /// <param name="cell">The cell.</param>
        /// <param name="format">The format.</param>
        /// <param name="cellType">Type of the cell.</param>
        /// <param name="colorIndex">Index of the color.</param>
        /// <remarks>This method should not be called directly but from other set style methods, e.g. <see cref="ApplyDataCategoryBasedStyleOnCell"/></remarks>
        private void ApplyStyleOnCell(ICell cell, string format, CellType cellType, short colorIndex)
        {
            Tuple<short, string, CellType> key = new Tuple<short, string, CellType>(colorIndex, format, cellType);

            if (!this.cellStylesDictionary.ContainsKey(key))
            {
                this.InitializeCellStyle(cellType, colorIndex, format);
            }

            cell.CellStyle = this.cellStylesDictionary[new Tuple<short, string, CellType>(colorIndex, format, cellType)];
        }

        /// <summary>
        /// Applies the data category based style on cell.
        /// </summary>
        /// <param name="cell">The cell.</param>
        /// <param name="dataCategoryType">Type of the data category.</param>
        /// <param name="format">The format.</param>
        /// <param name="cellType">Type of the cell.</param>
        private void ApplyDataCategoryBasedStyleOnCell(ICell cell, DataCategoryType dataCategoryType, string format = DefaultCellFormat, CellType cellType = CellType.Normal)
        {
            switch (dataCategoryType)
            {
                case DataCategoryType.Process:
                    var colorIndex = ProcessColorIndex;

                    if (labourDataColumnIndices.Contains(cell.ColumnIndex))
                    {
                        colorIndex = ProcessLabourDataColorIndex;
                    }

                    this.ApplyStyleOnCell(cell, format, cellType, colorIndex);
                    break;
                case DataCategoryType.Machine:
                    this.ApplyStyleOnCell(cell, format, cellType, MachineColorIndex);
                    break;
                case DataCategoryType.Tooling:
                    this.ApplyStyleOnCell(cell, format, cellType, ToolingColorIndex);
                    break;
                case DataCategoryType.Consumable:
                    this.ApplyStyleOnCell(cell, format, cellType, ConsumableColorIndex);
                    break;
                case DataCategoryType.Commodity:
                    this.ApplyStyleOnCell(cell, format, cellType, CommodityColorIndex);
                    break;
                case DataCategoryType.Material:
                    this.ApplyStyleOnCell(cell, format, cellType, MaterialColorIndex);
                    break;
                case DataCategoryType.RawPart:
                    this.ApplyStyleOnCell(cell, format, cellType, RawPartColorIndex);
                    break;
                case DataCategoryType.ProcessCost:
                    this.ApplyStyleOnCell(cell, format, cellType, ProcessCostColorIndex);
                    break;
                case DataCategoryType.MachineCost:
                    this.ApplyStyleOnCell(cell, format, cellType, MachineCostColorIndex);
                    break;
                case DataCategoryType.ToolingCost:
                    this.ApplyStyleOnCell(cell, format, cellType, ToolingCostColorIndex);
                    break;
                case DataCategoryType.ConsumableCost:
                    this.ApplyStyleOnCell(cell, format, cellType, ConsumableCostColorIndex);
                    break;
                case DataCategoryType.CommodityCost:
                    this.ApplyStyleOnCell(cell, format, cellType, CommodityCostColorIndex);
                    break;
                case DataCategoryType.MaterialCost:
                    this.ApplyStyleOnCell(cell, format, cellType, MaterialCostColorIndex);
                    break;
                case DataCategoryType.CostSummary:
                    this.ApplyStyleOnCell(cell, format, cellType, CostSummaryColorIndex);
                    break;
                case DataCategoryType.OverheadAndMarginSettings:
                    this.ApplyStyleOnCell(cell, format, cellType, OverheadAndMarginSettingsColorIndex);
                    break;
                case DataCategoryType.OverheadAndMarginCosts:
                    this.ApplyStyleOnCell(cell, format, cellType, OverheadAndMarginCostsColorIndex);
                    break;
                case DataCategoryType.CountrySettings:
                    this.ApplyStyleOnCell(cell, format, cellType, CountrySettingsColorIndex);
                    break;
                case DataCategoryType.CalculationSettings:
                    this.ApplyStyleOnCell(cell, format, cellType, CalculationSettingsColorIndex);
                    break;
                default:
                    throw new ArgumentOutOfRangeException("dataCategoryType");
            }
        }

        /// <summary>
        /// Applies the quantity style on cell.
        /// </summary>
        /// <param name="cell">The cell.</param>
        /// <param name="format">The format.</param>
        /// <param name="cellType">Type of the cell.</param>
        private void ApplyQuantityStyleOnCell(ICell cell, string format = DefaultCellFormat, CellType cellType = CellType.Normal)
        {
            this.ApplyStyleOnCell(cell, format, cellType, QuantityColorIndex);
        }

        /// <summary>
        /// Initializes the cell styles.
        /// </summary>
        private void InitializeCellStyles()
        {
            if (workbook == null || templateWorksheet == null)
            {
                log.Error("The workbook and templateWorksheet need to be initialized prior creating cell styles.");
                return;
            }

            emptyCellStyle = workbook.CreateCellStyle();
            emptyCellStyle.CloneStyleFrom(templateWorksheet.GetRow(1).GetCell(0).CellStyle);

            InitializeColors();

            cellStylesDictionary = new Dictionary<Tuple<short, string, CellType>, ICellStyle>();

            InitializeStylesForCellType(CellType.Normal);
            InitializeStylesForCellType(CellType.SmallHeader);
            InitializeStylesForCellType(CellType.BigHeader);
        }

        /// <summary>
        /// Initializes (replaces) the colors.
        /// </summary>
        private void InitializeColors()
        {
            var palette = workbook.GetCustomPalette();

            palette.SetColorAtIndex(ProcessColorIndex, 234, 241, 221);
            palette.SetColorAtIndex(ProcessLabourDataColorIndex, 215, 228, 188);
            palette.SetColorAtIndex(PartColorIndex, 219, 229, 241);
            palette.SetColorAtIndex(AssemblyColorIndex, 216, 216, 216);
            palette.SetColorAtIndex(QuantityColorIndex, 155, 187, 89);
            palette.SetColorAtIndex(MachineColorIndex, 197, 190, 151);
            palette.SetColorAtIndex(ToolingColorIndex, 215, 228, 188);
            palette.SetColorAtIndex(ConsumableColorIndex, 196, 255, 145);
            palette.SetColorAtIndex(CommodityColorIndex, 170, 182, 255);
            palette.SetColorAtIndex(MaterialColorIndex, 117, 146, 60);
            palette.SetColorAtIndex(SubPartsAndAssembliesColorIndex, 255, 204, 153);
            palette.SetColorAtIndex(RawPartColorIndex, 216, 204, 255);
            palette.SetColorAtIndex(ToolingCostColorIndex, 193, 240, 255);
            palette.SetColorAtIndex(ConsumableCostColorIndex, 145, 206, 255);
            palette.SetColorAtIndex(MaterialCostColorIndex, 174, 232, 151);
            palette.SetColorAtIndex(OverheadAndMarginCostsColorIndex, 255, 208, 193);
        }

        /// <summary>
        /// Initializes the type of the styles for cell.
        /// </summary>
        /// <param name="cellType">Type of the cell.</param>
        private void InitializeStylesForCellType(CellType cellType)
        {
            foreach (var checkedDataCategoryItem in DataCategories.Where(item => item.IsChecked))
            {
                InitializeCellStyleBasedOnCategory(cellType, checkedDataCategoryItem);
            }

            // Note: these are styles which must be created regardless of data categories.
            InitializeCellStyle(cellType, PartColorIndex);
            InitializeCellStyle(cellType, AssemblyColorIndex);
            InitializeCellStyle(cellType, QuantityColorIndex);
        }

        /// <summary>
        /// Initializes the cell style.
        /// </summary>
        /// <param name="cellType">Type of the cell.</param>
        /// <param name="checkedDataCategory">The checked data category.</param>
        private void InitializeCellStyleBasedOnCategory(CellType cellType, DataCategoryItem checkedDataCategory)
        {
            switch (checkedDataCategory.CategoryType)
            {
                case DataCategoryType.Process:
                    InitializeCellStyle(cellType, ProcessColorIndex);
                    InitializeCellStyle(cellType, ProcessLabourDataColorIndex);
                    break;
                case DataCategoryType.ProcessCost:
                    InitializeCellStyle(cellType, ProcessCostColorIndex);
                    break;
                case DataCategoryType.Machine:
                    InitializeCellStyle(cellType, MachineColorIndex);
                    break;
                case DataCategoryType.MachineCost:
                    InitializeCellStyle(cellType, MachineCostColorIndex);
                    break;
                case DataCategoryType.Tooling:
                    InitializeCellStyle(cellType, ToolingColorIndex);
                    break;
                case DataCategoryType.ToolingCost:
                    InitializeCellStyle(cellType, ToolingCostColorIndex);
                    break;
                case DataCategoryType.Consumable:
                    InitializeCellStyle(cellType, ConsumableColorIndex);
                    break;
                case DataCategoryType.ConsumableCost:
                    InitializeCellStyle(cellType, ConsumableCostColorIndex);
                    break;
                case DataCategoryType.Commodity:
                    InitializeCellStyle(cellType, CommodityColorIndex);
                    break;
                case DataCategoryType.CommodityCost:
                    InitializeCellStyle(cellType, CommodityCostColorIndex);
                    break;
                case DataCategoryType.Material:
                    InitializeCellStyle(cellType, MaterialColorIndex);
                    break;
                case DataCategoryType.MaterialCost:
                    InitializeCellStyle(cellType, MaterialCostColorIndex);
                    break;
                case DataCategoryType.RawPart:
                    InitializeCellStyle(cellType, RawPartColorIndex);
                    break;
                case DataCategoryType.CostSummary:
                    InitializeCellStyle(cellType, CostSummaryColorIndex);
                    break;
                case DataCategoryType.OverheadAndMarginSettings:
                    InitializeCellStyle(cellType, OverheadAndMarginSettingsColorIndex);
                    break;
                case DataCategoryType.CountrySettings:
                    InitializeCellStyle(cellType, CountrySettingsColorIndex);
                    break;
                case DataCategoryType.CalculationSettings:
                    InitializeCellStyle(cellType, CalculationSettingsColorIndex);
                    break;
                case DataCategoryType.OverheadAndMarginCosts:
                    InitializeCellStyle(cellType, CalculationSettingsColorIndex);
                    break;
                default:
                    log.Error("ICell style is not defined for the category type: " + checkedDataCategory.CategoryType);
                    break;
            }
        }

        /// <summary>
        /// Gets the format.
        /// </summary>
        /// <param name="sourceObject">The source object.</param>
        /// <param name="dataFieldItem">The data field item.</param>
        /// <param name="useSecondProperty">if set to <c>true</c> [use second property].</param>
        /// <returns>The format of the cell to be used.</returns>
        private string GetFormat(object sourceObject, DataFieldItem dataFieldItem, bool useSecondProperty = false)
        {
            var format = DefaultCellFormat;
            var propertyInfo = useSecondProperty ? dataFieldItem.SecondProperty : dataFieldItem.Property;
            var formatTemplate = (dataFieldItem.IsCostProperty || dataFieldItem.IsPercent) ? "0.00\\ \"{0}\"" : "0\\ \"{0}\"";

            if (HasProperty(sourceObject, propertyInfo.Name))
            {
                if (dataFieldItem.HasUnit)
                {
                    var unitPropertyValue = string.Empty;

                    if (dataFieldItem.HasStaticUnit)
                    {
                        unitPropertyValue = dataFieldItem.Unit;
                    }
                    else
                    {
                        if (dataFieldItem.UnitType != null)
                        {
                            var measurementUnitType = (MeasurementUnitType)dataFieldItem.UnitType;

                            // Get the UI base measurement unit and convert the value. 
                            // In some cases the database value is saved into another measurement system (saved in metric and shown on UI in imperial system)
                            MeasurementUnit baseMeasurementUnit = this.MeasurementUnitsAdapter.GetBaseMeasurementUnit(measurementUnitType);

                            unitPropertyValue = baseMeasurementUnit != null ? baseMeasurementUnit.Symbol : string.Empty;
                        }
                        else
                        {
                            if (dataFieldItem.HasDynamicUnit)
                            {
                                var unitValue = dataFieldItem.UnitPropertyInfo.GetValue(sourceObject, null);

                                if (unitValue != null)
                                {
                                    MeasurementUnit measurementUnit = unitValue as MeasurementUnit;

                                    unitPropertyValue = measurementUnit != null ? measurementUnit.Symbol : unitValue.ToString();
                                }
                            }
                        }
                    }

                    format = string.Format(CultureInfo.InvariantCulture, formatTemplate, unitPropertyValue);
                }
            }

            return format;
        }

        /// <summary>
        /// Initializes the cell style.
        /// </summary>
        /// <param name="cellType">Type of the cell.</param>
        /// <param name="colorIndex">Index of the color.</param>
        /// <param name="format">The format.</param>
        private void InitializeCellStyle(CellType cellType, short colorIndex, string format = DefaultCellFormat)
        {
            int rowNumber;

            switch (cellType)
            {
                case CellType.Normal:
                    rowNumber = 1;
                    break;
                case CellType.SmallHeader:
                    rowNumber = 2;
                    break;
                case CellType.BigHeader:
                    rowNumber = 3;
                    break;
                default:
                    log.Error("Row number is not defined for the cellType: " + cellType);
                    return;
            }

            Tuple<short, string, CellType> key = new Tuple<short, string, CellType>(colorIndex, format, cellType);

            if (!cellStylesDictionary.ContainsKey(key))
            {
                var cellStyle = workbook.CreateCellStyle();
                cellStyle.CloneStyleFrom(templateWorksheet.GetRow(rowNumber).GetCell(0).CellStyle);
                cellStyle.FillForegroundColor = colorIndex;
                cellStyle.FillPattern = FillPatternType.SOLID_FOREGROUND;

                if (format != DefaultCellFormat)
                {
                    var dataFormat = workbook.CreateDataFormat();
                    cellStyle.DataFormat = dataFormat.GetFormat(format);
                }

                cellStylesDictionary.Add(key, cellStyle);
            }
        }

        #region Nested types

        /// <summary>
        /// Base class for the items displayed in the ReportGenerator window.
        /// </summary>
        public abstract class ReportGeneratorItemBase : ObservableObject
        {
            #region Attributes

            /// <summary>
            /// Indicates whether this item is checked or not.
            /// </summary>
            private bool isChecked;

            /// <summary>
            /// The display name of this item.
            /// </summary>
            private string displayName;

            /// <summary>
            /// The index of the column.
            /// </summary>
            private int columnIndex;

            #endregion Attributes

            /// <summary>
            /// Initializes a new instance of the <see cref="ReportGeneratorItemBase"/> class.
            /// </summary>
            protected ReportGeneratorItemBase()
            {
                IsChecked = false;
                DisplayName = string.Empty;
            }

            #region Properties

            /// <summary>
            /// Gets or sets a value indicating whether this instance is checked.
            /// </summary>
            /// <value><c>true</c> if this instance is checked; otherwise, <c>false</c>.</value>
            public bool IsChecked
            {
                get
                {
                    return this.isChecked;
                }

                set
                {
                    this.SetProperty(ref this.isChecked, value, () => this.IsChecked);
                    OnIsCheckedChanged();
                }
            }

            /// <summary>
            /// Gets or sets the display name.
            /// </summary>
            /// <value>
            /// The display name.
            /// </value>
            public string DisplayName
            {
                get { return this.displayName; }
                set { this.SetProperty(ref this.displayName, value, () => this.DisplayName); }
            }

            /// <summary>
            /// Gets or sets the index of the column.
            /// </summary>
            /// <value>
            /// The index of the column.
            /// </value>
            public int ColumnIndex
            {
                get { return this.columnIndex; }
                set { this.SetProperty(ref this.columnIndex, value, () => this.ColumnIndex); }
            }

            #endregion

            /// <summary>
            /// Called when [is checked changed].
            /// </summary>
            protected virtual void OnIsCheckedChanged()
            {
            }
        }

        /// <summary>
        /// Items of this type are displayed in the DataCategories list.
        /// </summary>
        public class DataCategoryItem : ReportGeneratorItemBase
        {
            /// <summary>
            /// The associated data fields.
            /// </summary>
            private Collection<DataFieldItem> dataFields;

            /// <summary>
            /// The type of the category.
            /// </summary>
            private DataCategoryType categoryType;

            /// <summary>
            /// The number of columns this category occupies with its fields.
            /// </summary>
            private int numberOfColumns;

            /// <summary>
            /// A value indicating whether the event raised when the property IsChecked is changed should be handled for the data category item or not.
            /// </summary>
            private bool handleOnIsCheckedChanged;

            /// <summary>
            /// Initializes a new instance of the <see cref="DataCategoryItem"/> class.
            /// </summary>
            /// <param name="dataCategoryType">Type of the data category.</param>
            /// <param name="displayName">The display name.</param>
            /// <remarks>
            /// Currently support types: Assembly.
            /// </remarks>
            public DataCategoryItem(DataCategoryType dataCategoryType, string displayName = null)
            {
                var resourceKey = "ReportGenerator_" + dataCategoryType.ToString();
                DisplayName = displayName ?? LocalizedResources.ResourceManager.GetString(resourceKey);

                if (string.IsNullOrWhiteSpace(DisplayName))
                {
                    log.Warn("Could not find localized name for key: " + resourceKey);
                    DisplayName = dataCategoryType.ToString();
                }

                HandleOnIsCheckedChanged = true;
                DataFields = new Collection<DataFieldItem>();
                CategoryType = dataCategoryType;
                PopulateDataFields(dataCategoryType);
                SetIcon(dataCategoryType);
            }

            /// <summary>
            /// Gets or sets the type of the category.
            /// </summary>
            /// <value>
            /// The type of the category.
            /// </value>
            public DataCategoryType CategoryType
            {
                get { return this.categoryType; }
                set { this.SetProperty(ref this.categoryType, value, () => this.CategoryType); }
            }

            /// <summary>
            /// Gets or sets the associated data fields.
            /// </summary>
            /// <value>
            /// The associated data fields.
            /// </value>
            public Collection<DataFieldItem> DataFields
            {
                get { return this.dataFields; }
                set { this.SetProperty(ref this.dataFields, value, () => this.DataFields); }
            }

            /// <summary>
            /// Gets or sets the number of columns.
            /// </summary>
            /// <value>
            /// The number of columns.
            /// </value>
            public int NumberOfColumns
            {
                get { return this.numberOfColumns; }
                set { this.SetProperty(ref this.numberOfColumns, value, () => this.NumberOfColumns); }
            }

            /// <summary>
            /// Gets or sets a value indicating whether the event raised when the property IsChecked is changed should be handled for the data category item or not.
            /// This stops the overflow caused by the dependency between a checked category and a checked item from this category.
            /// </summary>
            public bool HandleOnIsCheckedChanged
            {
                get { return this.handleOnIsCheckedChanged; }
                set { this.SetProperty(ref this.handleOnIsCheckedChanged, value, () => this.HandleOnIsCheckedChanged); }
            }

            /// <summary>
            /// Gets or sets the icon representing the type of the item.
            /// </summary>        
            public ImageSource TypeIcon { get; set; }

            /// <summary>
            /// Called when [is checked changed].
            /// </summary>
            protected override void OnIsCheckedChanged()
            {
                base.OnIsCheckedChanged();

                if (DataFields != null && HandleOnIsCheckedChanged)
                {
                    foreach (var dataFieldItem in DataFields)
                    {
                        dataFieldItem.IsChecked = IsChecked;
                    }
                }
            }

            /// <summary>
            /// Sets the icon.
            /// </summary>
            /// <param name="dataCategoryType">Type of the data category.</param>
            private void SetIcon(DataCategoryType dataCategoryType)
            {
                switch (dataCategoryType)
                {
                    case DataCategoryType.Process:
                    case DataCategoryType.ProcessCost:
                        TypeIcon = Images.ProcessIcon;
                        break;
                    case DataCategoryType.Machine:
                    case DataCategoryType.MachineCost:
                        TypeIcon = Images.MachineIcon;
                        break;
                    case DataCategoryType.Tooling:
                    case DataCategoryType.ToolingCost:
                        TypeIcon = Images.DieIcon;
                        break;
                    case DataCategoryType.Consumable:
                    case DataCategoryType.ConsumableCost:
                        TypeIcon = Images.ConsumableIcon;
                        break;
                    case DataCategoryType.Commodity:
                    case DataCategoryType.CommodityCost:
                        TypeIcon = Images.CommodityIcon;
                        break;
                    case DataCategoryType.Material:
                    case DataCategoryType.MaterialCost:
                        TypeIcon = Images.RawMaterialIcon;
                        break;
                    case DataCategoryType.RawPart:
                        TypeIcon = Images.RawPartIcon;
                        break;
                    case DataCategoryType.CostSummary:
                        TypeIcon = Images.ResultDetailsIcon;
                        break;
                    case DataCategoryType.OverheadAndMarginSettings:
                    case DataCategoryType.OverheadAndMarginCosts:
                        TypeIcon = Images.OverheadSettingsIcon;
                        break;
                    case DataCategoryType.CountrySettings:
                        TypeIcon = Images.CountriesIcon;
                        break;
                    case DataCategoryType.CalculationSettings:
                        break;
                    default:
                        var msg = string.Format("Icon for DataCategory type '{0}' is not defined", dataCategoryType);
                        throw new ArgumentOutOfRangeException("dataCategoryType", msg);
                }
            }

            /// <summary>
            /// Populates the data fields.
            /// </summary>
            /// <param name="dataCategoryType">Type of the data category.</param>
            private void PopulateDataFields(DataCategoryType dataCategoryType)
            {
                var currencySymbol = SecurityManager.Instance.UICurrency.Symbol;
                InvestmentCostSingle investmentCostSingle;
                InvestmentCost investmentCost;

                switch (dataCategoryType)
                {
                    case DataCategoryType.Process:
                        AssemblyProcessStep processStep = new AssemblyProcessStep();
                        dataFields.Add(new DataFieldItem(this, () => processStep.Name, string.Empty));
                        dataFields.Add(new DataFieldItem(this, () => processStep.Accuracy, string.Empty, theTargetEnumType: typeof(ProcessCalculationAccuracy)));
                        dataFields.Add(new DataFieldItem(this, () => processStep.CycleTime, () => processStep.CycleTimeUnit));
                        dataFields.Add(new DataFieldItem(this, () => processStep.ProcessTime, () => processStep.ProcessTimeUnit));
                        dataFields.Add(new DataFieldItem(this, () => processStep.PartsPerCycle, LocalizedResources.General_Pieces));
                        dataFields.Add(new DataFieldItem(this, () => processStep.ScrapAmount, LocalizedResources.General_PercentSymbol));
                        dataFields.Add(new DataFieldItem(this, () => processStep.ShiftsPerWeek, string.Empty));
                        dataFields.Add(new DataFieldItem(this, () => processStep.ProductionDaysPerWeek, string.Empty));
                        dataFields.Add(new DataFieldItem(this, () => processStep.SetupsPerBatch, string.Empty));
                        dataFields.Add(new DataFieldItem(this, () => processStep.SetupTime, () => processStep.SetupTimeUnit));
                        dataFields.Add(new DataFieldItem(this, () => processStep.MaxDownTime, () => processStep.MaxDownTimeUnit));
                        dataFields.Add(new DataFieldItem(this, () => processStep.BatchSize, LocalizedResources.General_Pieces));
                        dataFields.Add(new DataFieldItem(this, () => processStep.ManufacturingOverhead, LocalizedResources.General_PercentSymbol));
                        dataFields.Add(new DataFieldItem(this, () => processStep.HoursPerShift, string.Empty));
                        dataFields.Add(new DataFieldItem(this, () => processStep.ProductionWeeksPerYear, string.Empty));
                        dataFields.Add(new DataFieldItem(this, () => processStep.ExceedShiftCost, string.Empty));
                        dataFields.Add(new DataFieldItem(this, () => processStep.ExtraShiftsNumber, string.Empty));
                        dataFields.Add(new DataFieldItem(this, () => processStep.ShiftCostExceedRatio, LocalizedResources.General_PercentSymbol));
                        dataFields.Add(new DataFieldItem(this, () => processStep.Description, string.Empty));
                        dataFields.Add(new DataFieldItem(this, () => processStep.IsExternal, string.Empty));
                        dataFields.Add(new DataFieldItem(this, () => processStep.ProductionUnskilledLabour, string.Empty, isProcessStepLabourDataField: true, displayName: LocalizedResources.Labour_UnskilledLabourProduction));
                        dataFields.Add(new DataFieldItem(this, () => processStep.ProductionSkilledLabour, string.Empty, isProcessStepLabourDataField: true, displayName: LocalizedResources.Labour_SkilledLabourProduction));
                        dataFields.Add(new DataFieldItem(this, () => processStep.ProductionForeman, string.Empty, isProcessStepLabourDataField: true, displayName: LocalizedResources.Labour_ForemanProduction));
                        dataFields.Add(new DataFieldItem(this, () => processStep.ProductionTechnicians, string.Empty, isProcessStepLabourDataField: true, displayName: LocalizedResources.Labour_TechniciansProduction));
                        dataFields.Add(new DataFieldItem(this, () => processStep.ProductionEngineers, string.Empty, isProcessStepLabourDataField: true, displayName: LocalizedResources.Labour_EngineersProduction));
                        dataFields.Add(new DataFieldItem(this, () => processStep.SetupUnskilledLabour, string.Empty, isProcessStepLabourDataField: true, displayName: LocalizedResources.Labour_UnskilledLabourSetup));
                        dataFields.Add(new DataFieldItem(this, () => processStep.SetupSkilledLabour, string.Empty, isProcessStepLabourDataField: true, displayName: LocalizedResources.Labour_SkilledLabourSetup));
                        dataFields.Add(new DataFieldItem(this, () => processStep.SetupForeman, string.Empty, isProcessStepLabourDataField: true, displayName: LocalizedResources.Labour_ForemanSetup));
                        dataFields.Add(new DataFieldItem(this, () => processStep.SetupTechnicians, string.Empty, isProcessStepLabourDataField: true, displayName: LocalizedResources.Labour_TechniciansSetup));
                        dataFields.Add(new DataFieldItem(this, () => processStep.SetupEngineers, string.Empty, isProcessStepLabourDataField: true, displayName: LocalizedResources.Labour_EngineersSetup));
                        break;
                    case DataCategoryType.ProcessCost:
                        ProcessStepCost processStepCost = new ProcessStepCost();
                        ProcessCost processCost = new ProcessCost();
                        dataFields.Add(new DataFieldItem(this, () => processStepCost.MachineCost, currencySymbol, LocalizedResources.General_MachineEquipmentCost, () => processCost.MachineCostSum, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => processStepCost.SetupCost, currencySymbol, LocalizedResources.General_SetUpCost, () => processCost.SetupCostSum, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => processStepCost.DirectLabourCost, currencySymbol, LocalizedResources.General_DirectLabourCost, () => processCost.DirectLabourCostSum, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => processStepCost.DiesMaintenanceCost, currencySymbol, LocalizedResources.General_MaintenanceDiesTools, () => processCost.DiesMaintenanceCostSum, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => processStepCost.ToolAndDieCost, currencySymbol, LocalizedResources.General_ToolAndDieCost, () => processCost.ToolAndDieCostSum, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => processStepCost.ManufacturingCost, currencySymbol, LocalizedResources.General_AssemblingManufacturingCost, () => processCost.ManufacturingCostSum, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => processStepCost.ManufacturingOverheadCost, currencySymbol, LocalizedResources.General_AssemblingManufacturingOverhead, () => processCost.ManufacturingOverheadSum, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => processStepCost.RejectCost, currencySymbol, LocalizedResources.General_RejectCost, () => processCost.RejectCostSum, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => processStepCost.TransportCost, currencySymbol, LocalizedResources.General_TransportCost, () => processCost.ManufacturingTransportCost, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => processStepCost.TotalManufacturingCost, currencySymbol, LocalizedResources.General_TotalAssemblingCost, () => processCost.TotalManufacturingCostSum, isCostProperty: true));
                        break;
                    case DataCategoryType.Machine:
                        Machine machine = new Machine();
                        dataFields.Add(new DataFieldItem(this, () => machine.Name, string.Empty));
                        dataFields.Add(new DataFieldItem(this, () => machine.ManufacturingYear, string.Empty));
                        dataFields.Add(new DataFieldItem(this, () => machine.DepreciationPeriod, LocalizedResources.General_Years));
                        dataFields.Add(new DataFieldItem(this, () => machine.DepreciationRate, LocalizedResources.General_PercentSymbol));
                        dataFields.Add(new DataFieldItem(this, () => machine.RegistrationNumber, string.Empty));
                        dataFields.Add(new DataFieldItem(this, () => machine.DescriptionOfFunction, string.Empty));
                        dataFields.Add(new DataFieldItem(this, () => machine.FloorSize, UnitsService.GetCurrentUnitSymbol(MeasurementUnitType.Area)) { UnitType = MeasurementUnitType.Area });
                        dataFields.Add(new DataFieldItem(this, () => machine.WorkspaceArea, UnitsService.GetCurrentUnitSymbol(MeasurementUnitType.Area)) { UnitType = MeasurementUnitType.Area });
                        dataFields.Add(new DataFieldItem(this, () => machine.PowerConsumption, LocalizedResources.General_Kwh));
                        dataFields.Add(new DataFieldItem(this, () => machine.AirConsumption, UnitsService.GetCurrentUnitSymbol(MeasurementUnitType.Volume)) { UnitType = MeasurementUnitType.Volume });
                        dataFields.Add(new DataFieldItem(this, () => machine.WaterConsumption, UnitsService.GetCurrentUnitSymbol(MeasurementUnitType.Volume)) { UnitType = MeasurementUnitType.Volume });
                        dataFields.Add(new DataFieldItem(this, () => machine.FullLoadRate, LocalizedResources.General_PercentSymbol));
                        dataFields.Add(new DataFieldItem(this, () => machine.MaxCapacity, LocalizedResources.General_PiecesPerHour));
                        dataFields.Add(new DataFieldItem(this, () => machine.OEE, LocalizedResources.General_PercentSymbol));
                        dataFields.Add(new DataFieldItem(this, () => machine.Availability, LocalizedResources.General_PercentSymbol));
                        dataFields.Add(new DataFieldItem(this, () => machine.MachineInvestment, currencySymbol, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => machine.MachineLeaseCosts, currencySymbol, displayName: LocalizedResources.Machine_LeaseCosts, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => machine.SetupInvestment, currencySymbol, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => machine.AdditionalEquipmentInvestment, currencySymbol, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => machine.FundamentalSetupInvestment, currencySymbol, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => machine.InvestmentRemarks, string.Empty));
                        dataFields.Add(new DataFieldItem(this, () => machine.ExternalWorkCost, currencySymbol));
                        dataFields.Add(new DataFieldItem(this, () => machine.MaterialCost, currencySymbol));
                        dataFields.Add(new DataFieldItem(this, () => machine.ConsumablesCost, currencySymbol));
                        dataFields.Add(new DataFieldItem(this, () => machine.ManualConsumableCostPercentage, LocalizedResources.General_PercentSymbol));
                        dataFields.Add(new DataFieldItem(this, () => machine.RepairsCost, currencySymbol));
                        dataFields.Add(new DataFieldItem(this, () => machine.KValue, LocalizedResources.General_PercentSymbol));
                        dataFields.Add(new DataFieldItem(this, () => machine.CalculateWithKValue, string.Empty));
                        dataFields.Add(new DataFieldItem(this, () => machine.MountingCubeLength, LocalizedResources.MachiningCalculator_LengthUnit));
                        dataFields.Add(new DataFieldItem(this, () => machine.MountingCubeWidth, LocalizedResources.MachiningCalculator_LengthUnit));
                        dataFields.Add(new DataFieldItem(this, () => machine.MountingCubeHeight, LocalizedResources.MachiningCalculator_LengthUnit));
                        dataFields.Add(new DataFieldItem(this, () => machine.MaxDiameterRodPerChuckMaxThickness, LocalizedResources.MachiningCalculator_LengthUnit));
                        dataFields.Add(new DataFieldItem(this, () => machine.MaxPartsWeightPerCapacity, LocalizedResources.MachiningCalculator_Density));
                        dataFields.Add(new DataFieldItem(this, () => machine.LockingForce, LocalizedResources.MachiningCalculator_KiloNewton));
                        dataFields.Add(new DataFieldItem(this, () => machine.PressCapacity, LocalizedResources.MachiningCalculator_KiloNewton));
                        dataFields.Add(new DataFieldItem(this, () => machine.MaximumSpeed, LocalizedResources.MachiningCalculator_TurningSpeedUnit));
                        dataFields.Add(new DataFieldItem(this, () => machine.RapidFeedPerCuttingSpeed, LocalizedResources.MachiningCalculator_SpeedUnit));
                        dataFields.Add(new DataFieldItem(this, () => machine.StrokeRate, LocalizedResources.MachiningCalculator_StrokeRateUnit));
                        dataFields.Add(new DataFieldItem(this, () => machine.Other, string.Empty));
                        break;
                    case DataCategoryType.MachineCost:
                        MachineCost machineCost = new MachineCost();
                        investmentCostSingle = new InvestmentCostSingle();
                        investmentCost = new InvestmentCost();
                        dataFields.Add(new DataFieldItem(this, () => machineCost.DepreciationPerHour, currencySymbol, LocalizedResources.General_DeprecationPerHour, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => machineCost.MaintenancePerHour, currencySymbol, LocalizedResources.General_MaintenancePerHour, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => machineCost.EnergyCostPerHour, currencySymbol, LocalizedResources.General_EnergyCostPerHour, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => machineCost.FloorCost, currencySymbol, LocalizedResources.General_CostOfSpace, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => machineCost.FullCostPerHour, currencySymbol, LocalizedResources.General_FullMachineCostPerHour, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => investmentCostSingle.Investment, currencySymbol, LocalizedResources.General_Investment, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => investmentCostSingle.Amount, string.Empty, LocalizedResources.ResultDetails_NeededAmount, isCostProperty: false));
                        dataFields.Add(new DataFieldItem(this, () => investmentCostSingle.TotalInvestment, currencySymbol, LocalizedResources.ResultDetails_TotalInvestment, () => investmentCost.TotalMachinesInvestment, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => machineCost.MaintenanceNotes, currencySymbol, LocalizedResources.Machine_MaintenanceNotes));
                        break;
                    case DataCategoryType.Tooling:
                        Die die = new Die();
                        dataFields.Add(new DataFieldItem(this, () => die.Name, string.Empty));
                        dataFields.Add(new DataFieldItem(this, () => die.Type, string.Empty, theTargetEnumType: typeof(DieType)));
                        dataFields.Add(new DataFieldItem(this, () => die.Investment, currencySymbol, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => die.ReusableInvest, currencySymbol, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => die.Wear, currencySymbol, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => die.IsWearInPercentage, string.Empty));
                        dataFields.Add(new DataFieldItem(this, () => die.IsMaintenanceInPercentage, currencySymbol));
                        dataFields.Add(new DataFieldItem(this, () => die.LifeTime, LocalizedResources.General_Pieces));
                        dataFields.Add(new DataFieldItem(this, () => die.AllocationRatio, LocalizedResources.General_PercentSymbol));
                        dataFields.Add(new DataFieldItem(this, () => die.DiesetsNumberPaidByCustomer, string.Empty));
                        dataFields.Add(new DataFieldItem(this, () => die.CostAllocationBasedOnPartsPerLifeTime, string.Empty));
                        dataFields.Add(new DataFieldItem(this, () => die.CostAllocationNumberOfParts, string.Empty));
                        dataFields.Add(new DataFieldItem(this, () => die.Remark, string.Empty));
                        dataFields.Add(new DataFieldItem(this, () => die.AreAllPaidByCustomer, string.Empty));
                        break;
                    case DataCategoryType.ToolingCost:
                        DieCost dieCost = new DieCost();
                        investmentCostSingle = new InvestmentCostSingle();
                        investmentCost = new InvestmentCost();
                        dataFields.Add(new DataFieldItem(this, () => dieCost.MaintenanceCost, currencySymbol, LocalizedResources.Die_Maintenance, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => dieCost.Cost, currencySymbol, LocalizedResources.General_Cost, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => investmentCostSingle.Investment, currencySymbol, LocalizedResources.General_Investment, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => investmentCostSingle.Amount, string.Empty, LocalizedResources.ResultDetails_NeededAmount, isCostProperty: false));
                        dataFields.Add(new DataFieldItem(this, () => investmentCostSingle.TotalInvestment, currencySymbol, LocalizedResources.ResultDetails_TotalInvestment, () => investmentCost.TotalDiesInvestment, isCostProperty: true));
                        break;
                    case DataCategoryType.Consumable:
                        Consumable consumable = new Consumable();
                        dataFields.Add(new DataFieldItem(this, () => consumable.Name, string.Empty));
                        dataFields.Add(new DataFieldItem(this, () => consumable.Amount, () => consumable.AmountUnitBase));
                        dataFields.Add(new DataFieldItem(this, () => consumable.Price, currencySymbol, isCostProperty: true));
                        break;
                    case DataCategoryType.ConsumableCost:
                        ConsumableCost consumableCost = new ConsumableCost();
                        ConsumablesCost consumablesCost = new ConsumablesCost();
                        dataFields.Add(new DataFieldItem(this, () => consumableCost.Overhead, currencySymbol, LocalizedResources.General_Overhead, () => consumablesCost.OverheadSum, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => consumableCost.Cost, currencySymbol, LocalizedResources.General_Cost, () => consumablesCost.CostsSum, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => consumableCost.TotalCost, currencySymbol, LocalizedResources.General_TotalCost, () => consumablesCost.TotalCostSum, isCostProperty: true));
                        break;
                    case DataCategoryType.Commodity:
                        Commodity commodity = new Commodity();
                        dataFields.Add(new DataFieldItem(this, () => commodity.Name, string.Empty));
                        dataFields.Add(new DataFieldItem(this, () => commodity.PartNumber, string.Empty));
                        dataFields.Add(new DataFieldItem(this, () => commodity.Price, string.Format(CultureInfo.InvariantCulture, "{0}/{1}", currencySymbol, LocalizedResources.General_Pieces), isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => commodity.Amount, LocalizedResources.General_Pieces));
                        dataFields.Add(new DataFieldItem(this, () => commodity.RejectRatio, LocalizedResources.General_PercentSymbol));
                        dataFields.Add(new DataFieldItem(this, () => commodity.Weight, () => commodity.WeightUnitBase) { UnitType = MeasurementUnitType.Weight });
                        dataFields.Add(new DataFieldItem(this, () => commodity.Remarks, string.Empty));
                        break;
                    case DataCategoryType.CommodityCost:
                        CommodityCost commodityCost = new CommodityCost();
                        CommoditiesCost commoditiesCost = new CommoditiesCost();
                        dataFields.Add(new DataFieldItem(this, () => commodityCost.Overhead, currencySymbol, LocalizedResources.General_Overhead, () => commoditiesCost.OverheadSum, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => commodityCost.Cost, currencySymbol, LocalizedResources.General_Cost, () => commoditiesCost.CostsSum, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => commodityCost.TotalCost, currencySymbol, LocalizedResources.General_TotalCost, () => commoditiesCost.TotalCostSum, isCostProperty: true));
                        break;
                    case DataCategoryType.Material:
                        RawMaterial rawMaterial = new RawMaterial();
                        dataFields.Add(new DataFieldItem(this, () => rawMaterial.Name, string.Empty));
                        dataFields.Add(new DataFieldItem(this, () => rawMaterial.NameUK, string.Empty));
                        dataFields.Add(new DataFieldItem(this, () => rawMaterial.NameUS, string.Empty));
                        dataFields.Add(new DataFieldItem(this, () => rawMaterial.NormName, string.Empty));
                        dataFields.Add(new DataFieldItem(this, () => rawMaterial.DeliveryType, string.Empty));
                        dataFields.Add(new DataFieldItem(this, () => rawMaterial.Price, currencySymbol, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => rawMaterial.ParentWeight, () => rawMaterial.ParentWeightUnitBase) { UnitType = MeasurementUnitType.Weight });
                        dataFields.Add(new DataFieldItem(this, () => rawMaterial.Sprue, LocalizedResources.General_PercentSymbol));
                        dataFields.Add(new DataFieldItem(this, () => rawMaterial.Quantity, () => rawMaterial.QuantityUnitBase) { UnitType = MeasurementUnitType.Weight });
                        dataFields.Add(new DataFieldItem(this, () => rawMaterial.Loss, LocalizedResources.General_PercentSymbol));
                        dataFields.Add(new DataFieldItem(this, () => rawMaterial.RecyclingRatio, LocalizedResources.General_PercentSymbol, LocalizedResources.General_Recycling));
                        dataFields.Add(new DataFieldItem(this, () => rawMaterial.ScrapRefundRatio, LocalizedResources.General_PercentSymbol));
                        dataFields.Add(new DataFieldItem(this, () => rawMaterial.RejectRatio, LocalizedResources.General_PercentSymbol));
                        dataFields.Add(new DataFieldItem(this, () => rawMaterial.YieldStrength, LocalizedResources.Materials_StrengthUnit));
                        dataFields.Add(new DataFieldItem(this, () => rawMaterial.RuptureStrength, LocalizedResources.Materials_StrengthUnit));
                        dataFields.Add(new DataFieldItem(this, () => rawMaterial.Density, LocalizedResources.Materials_DensityUnit));
                        dataFields.Add(new DataFieldItem(this, () => rawMaterial.MaxElongation, LocalizedResources.General_PercentSymbol));
                        dataFields.Add(new DataFieldItem(this, () => rawMaterial.GlassTransitionTemperature, LocalizedResources.General_TemperatureUnit));
                        dataFields.Add(new DataFieldItem(this, () => rawMaterial.Rx, LocalizedResources.General_PercentSymbol));
                        dataFields.Add(new DataFieldItem(this, () => rawMaterial.Rm, LocalizedResources.Materials_StrengthUnit));
                        break;
                    case DataCategoryType.MaterialCost:
                        RawMaterialCost rawMaterialCost = new RawMaterialCost();
                        RawMaterialsCost rawMaterialsCost = new RawMaterialsCost();
                        dataFields.Add(new DataFieldItem(this, () => rawMaterialCost.BaseCost, currencySymbol, LocalizedResources.General_BaseCost, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => rawMaterialCost.ScrapCost, currencySymbol, LocalizedResources.General_Scrap, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => rawMaterialCost.ScrapRefund, currencySymbol, LocalizedResources.General_ScrapRefund, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => rawMaterialCost.LossCost, currencySymbol, LocalizedResources.RawMaterial_Loss, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => rawMaterialCost.RejectCost, currencySymbol, LocalizedResources.General_RejectCost, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => rawMaterialCost.NetCost, currencySymbol, LocalizedResources.General_NetCost, () => rawMaterialsCost.NetCostSum, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => rawMaterialCost.Overhead, currencySymbol, LocalizedResources.General_Overhead, () => rawMaterialsCost.OverheadSum, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => rawMaterialCost.TotalCost, currencySymbol, LocalizedResources.General_TotalCost, () => rawMaterialsCost.TotalCostSum, isCostProperty: true));
                        break;
                    case DataCategoryType.RawPart:
                        RawPart rawPart = new RawPart();
                        dataFields.Add(new DataFieldItem(this, () => rawPart.Name, string.Empty));
                        break;
                    case DataCategoryType.OverheadAndMarginSettings:
                        OverheadSetting overheadSetting = new OverheadSetting();
                        OverheadCost overheadCost = new OverheadCost();
                        dataFields.Add(new DataFieldItem(this, () => overheadSetting.MaterialOverhead, LocalizedResources.General_PercentSymbol, LocalizedResources.OverheadSetting_MaterialOverhead, () => overheadCost.RawMaterialOverhead, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => overheadSetting.ConsumableOverhead, LocalizedResources.General_PercentSymbol, LocalizedResources.OverheadSetting_ConsumableOverhead, () => overheadCost.ConsumableOverhead, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => overheadSetting.CommodityOverhead, LocalizedResources.General_PercentSymbol, LocalizedResources.OverheadSetting_CommodityOverhead, () => overheadCost.CommodityOverhead, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => overheadSetting.ExternalWorkOverhead, LocalizedResources.General_PercentSymbol, LocalizedResources.OverheadSetting_ExternalWorkOverhead, () => overheadCost.ExternalWorkOverhead, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => overheadSetting.ManufacturingOverhead, LocalizedResources.General_PercentSymbol, LocalizedResources.OverheadSetting_ManufacturingOverhead, () => overheadCost.ManufacturingOverhead, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => overheadSetting.OtherCostOHValue, LocalizedResources.General_PercentSymbol, LocalizedResources.OverheadSetting_OtherCostOHValue, () => overheadCost.OtherCostOverhead, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => overheadSetting.PackagingOHValue, LocalizedResources.General_PercentSymbol, LocalizedResources.OverheadSetting_PackagingOHValue, () => overheadCost.PackagingOverhead, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => overheadSetting.LogisticOHValue, LocalizedResources.General_PercentSymbol, LocalizedResources.OverheadSetting_LogisticOHValue, () => overheadCost.LogisticOverhead, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => overheadSetting.SalesAndAdministrationOHValue, LocalizedResources.General_PercentSymbol, LocalizedResources.OverheadSetting_SalesAndAdministrationOHValue, () => overheadCost.SalesAndAdministrationOverhead, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => overheadSetting.CompanySurchargeOverhead, LocalizedResources.General_PercentSymbol, LocalizedResources.OverheadSettings_CompanySurchargeOH, () => overheadCost.CompanySurchargeOverhead, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => overheadSetting.MaterialMargin, LocalizedResources.General_PercentSymbol, LocalizedResources.OverheadSetting_MaterialMargin, () => overheadCost.RawMaterialMargin, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => overheadSetting.ConsumableMargin, LocalizedResources.General_PercentSymbol, LocalizedResources.OverheadSetting_ConsumableMargin, () => overheadCost.ConsumableMargin, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => overheadSetting.CommodityMargin, LocalizedResources.General_PercentSymbol, LocalizedResources.OverheadSetting_CommodityMargin, () => overheadCost.CommodityMargin, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => overheadSetting.ExternalWorkMargin, LocalizedResources.General_PercentSymbol, LocalizedResources.OverheadSetting_ExternalWorkMargin, () => overheadCost.ExternalWorkMargin, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => overheadSetting.ManufacturingMargin, LocalizedResources.General_PercentSymbol, LocalizedResources.OverheadSetting_ManufacturingMargin, () => overheadCost.ManufacturingMargin, isCostProperty: true));
                        break;
                    case DataCategoryType.CountrySettings:
                        CountrySetting countrySetting = new CountrySetting();
                        dataFields.Add(new DataFieldItem(this, () => countrySetting.UnskilledLaborCost, string.Format(CultureInfo.InvariantCulture, "{0}/h", currencySymbol), LocalizedResources.General_UnskilledLabourCost, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => countrySetting.SkilledLaborCost, string.Format(CultureInfo.InvariantCulture, "{0}/h", currencySymbol), LocalizedResources.General_SkilledLabourCost, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => countrySetting.ForemanCost, string.Format(CultureInfo.InvariantCulture, "{0}/h", currencySymbol), LocalizedResources.General_ForemanCost, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => countrySetting.TechnicianCost, string.Format(CultureInfo.InvariantCulture, "{0}/h", currencySymbol), LocalizedResources.General_TechnicianCost, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => countrySetting.EngineerCost, string.Format(CultureInfo.InvariantCulture, "{0}/h", currencySymbol), LocalizedResources.General_EngineerCost, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => countrySetting.ShiftCharge1ShiftModel, LocalizedResources.General_PercentSymbol, LocalizedResources.CountrySetting_ShiftCharge1ShiftModel));
                        dataFields.Add(new DataFieldItem(this, () => countrySetting.ShiftCharge2ShiftModel, LocalizedResources.General_PercentSymbol, LocalizedResources.CountrySetting_ShiftCharge2ShiftModel));
                        dataFields.Add(new DataFieldItem(this, () => countrySetting.ShiftCharge3ShiftModel, LocalizedResources.General_PercentSymbol, LocalizedResources.CountrySetting_ShiftCharge3ShiftModel));
                        dataFields.Add(new DataFieldItem(this, () => countrySetting.LaborAvailability, LocalizedResources.General_PercentSymbol, LocalizedResources.CountrySetting_LabourAvailability));
                        dataFields.Add(new DataFieldItem(this, () => countrySetting.EnergyCost, string.Format(CultureInfo.InvariantCulture, "{0}/kWh", currencySymbol), LocalizedResources.General_EnergyCost, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => countrySetting.AirCost, string.Format(CultureInfo.InvariantCulture, "{0}/m³", currencySymbol), LocalizedResources.General_AirCost, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => countrySetting.WaterCost, string.Format(CultureInfo.InvariantCulture, "{0}/m³", currencySymbol), LocalizedResources.General_WaterCost, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => countrySetting.ProductionAreaRentalCost, string.Format(CultureInfo.InvariantCulture, "{0}/m² {1}", currencySymbol, LocalizedResources.General_PerMonth), LocalizedResources.General_ProductionAreaRentalCost, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => countrySetting.OfficeAreaRentalCost, string.Format(CultureInfo.InvariantCulture, "{0}/m² {1}", currencySymbol, LocalizedResources.General_PerMonth), LocalizedResources.General_OfficeAreaRentalCost, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => countrySetting.InterestRate, LocalizedResources.General_PercentSymbol, LocalizedResources.General_InterestRate));
                        break;
                    case DataCategoryType.CostSummary:
                        CalculationResult calculationResult = new CalculationResult();
                        Part part = new Part();
                        Assembly assembly = new Assembly();
                        dataFields.Add(new DataFieldItem(this, () => calculationResult.DevelopmentCost, currencySymbol, LocalizedResources.General_DevelopmentCost, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => calculationResult.PackagingCost, currencySymbol, LocalizedResources.General_PackagingCost, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => calculationResult.ProjectInvest, currencySymbol, LocalizedResources.General_ProjectInvest, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => calculationResult.LogisticCost, currencySymbol, LocalizedResources.General_LogisticCost, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => calculationResult.TransportCost, currencySymbol, LocalizedResources.General_TransportCost, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => calculationResult.OtherCost, currencySymbol, LocalizedResources.General_OtherCost, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => assembly.PaymentTerms, LocalizedResources.General_Days, LocalizedResources.General_PaymentTerms, () => part.PaymentTerms, isCostProperty: true));
                        dataFields.Add(new DataFieldItem(this, () => calculationResult.AdditionalCostsNotes, currencySymbol, LocalizedResources.General_AdditionalCostsNotes));
                        break;
                    case DataCategoryType.CalculationSettings:
                    case DataCategoryType.OverheadAndMarginCosts:
                        break;
                    default:
                        var msg = string.Format("Data fields for DataCategory type '{0}' are not defined", dataCategoryType);
                        throw new ArgumentOutOfRangeException(msg);
                }

                DataFields = dataFields;
            }
        }

        /// <summary>
        /// Items of this type are displayed in the DataFields list.
        /// </summary>
        public class DataFieldItem : ReportGeneratorItemBase
        {
            /// <summary>
            /// The property info this item represents.
            /// </summary>
            private Reflection.PropertyInfo property;

            /// <summary>
            /// The second property this item represents.
            /// </summary>
            private Reflection.PropertyInfo secondProperty;

            /// <summary>
            /// Value indicating whether this instance is process step labour data field.
            /// </summary>
            private bool isProcessStepLabourDataField;

            /// <summary>
            /// Initializes a new instance of the <see cref="DataFieldItem"/> class.
            /// </summary>
            /// <param name="parentItem">The parent item.</param>
            /// <param name="propertyInfo">The property info.</param>
            /// <param name="unit">The unit.</param>
            /// <param name="displayName">The display name.</param>
            /// <remarks>Use this in special cases only, e.g. overhead and margin costs.</remarks>
            public DataFieldItem(
                DataCategoryItem parentItem,
                Reflection.PropertyInfo propertyInfo,
                string unit,
                string displayName)
            {
                IsProcessStepLabourDataField = false;
                Property = propertyInfo;
                DisplayName = displayName;
                ParentDataCategory = parentItem;
                IsPercent = false;
                Unit = unit;
                TargetEnumType = null;
                IsCostProperty = false;
                this.UnitType = null;
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="DataFieldItem"/> class.
            /// </summary>
            /// <param name="parentItem">The parent item.</param>
            /// <param name="property">The property this item represents.</param>
            /// <param name="displayName">The display name.</param>
            /// <param name="secondProperty">The second property this item represents.</param>
            /// <param name="isProcessStepLabourDataField">if set to <c>true</c> [is process step labour data field].</param>
            /// <param name="theTargetEnumType">Type of the target enum.</param>
            /// <param name="isCostProperty">if set to <c>true</c> [is cost property].</param>
            private DataFieldItem(
                DataCategoryItem parentItem,
                Expression<Func<object>> property,
                string displayName = null,
                Expression<Func<object>> secondProperty = null,
                bool isProcessStepLabourDataField = false,
                Type theTargetEnumType = null,
                bool isCostProperty = false)
            {
                IsProcessStepLabourDataField = isProcessStepLabourDataField;
                Property = ReflectionUtils.GetProperty(property);
                DisplayName = displayName ?? UIUtils.GetLocalizedNameOfProperty(this.Property);
                ParentDataCategory = parentItem;
                IsPercent = false;
                TargetEnumType = theTargetEnumType;
                IsCostProperty = isCostProperty;
                this.UnitType = null;

                if (secondProperty != null)
                {
                    SecondProperty = ReflectionUtils.GetProperty(secondProperty);
                }
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="DataFieldItem"/> class.
            /// </summary>
            /// <param name="parentItem">The parent item.</param>
            /// <param name="property">The property this item represents.</param>
            /// <param name="unit">The unit of the value this object represents (if it's not changeable) or string.Empty if the property doesn't have a unit (e.g. Name or Description).</param>
            /// <param name="displayName">The display name.</param>
            /// <param name="secondProperty">The second property.</param>
            /// <param name="isProcessStepLabourDataField">if set to <c>true</c> [is process step labour data field].</param>
            /// <param name="theTargetEnumType">Type of the target enum.</param>
            /// <param name="isCostProperty">if set to <c>true</c> [is cost property].</param>
            public DataFieldItem(
                DataCategoryItem parentItem,
                Expression<Func<object>> property,
                string unit,
                string displayName = null,
                Expression<Func<object>> secondProperty = null,
                bool isProcessStepLabourDataField = false,
                Type theTargetEnumType = null,
                bool isCostProperty = false) :
                this(parentItem, property, displayName, secondProperty, isProcessStepLabourDataField, theTargetEnumType, isCostProperty)
            {
                Unit = unit;

                if (!string.IsNullOrWhiteSpace(unit))
                {
                    IsPercent = unit.Contains(LocalizedResources.General_PercentSymbol);
                }
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="DataFieldItem"/> class.
            /// </summary>
            /// <param name="parentItem">The parent item.</param>
            /// <param name="property">The property.</param>
            /// <param name="unitProperty">The unit property.</param>
            /// <param name="displayName">The display name.</param>
            /// <param name="secondProperty">The second property.</param>
            /// <param name="isProcessStepLabourDataField">if set to <c>true</c> [is process step labour data field].</param>
            public DataFieldItem(
                DataCategoryItem parentItem,
                Expression<Func<object>> property,
                Expression<Func<object>> unitProperty,
                string displayName = null,
                Expression<Func<object>> secondProperty = null,
                bool isProcessStepLabourDataField = false) :
                this(parentItem, property, displayName, secondProperty, isProcessStepLabourDataField)
            {
                UnitPropertyInfo = ReflectionUtils.GetProperty(unitProperty);
            }

            /// <summary>
            /// Gets a value indicating whether this instance has unit (static or dynamic).
            /// </summary>
            /// <value>
            ///   <c>true</c> if this instance has unit; otherwise, <c>false</c>.
            /// </value>
            public bool HasUnit
            {
                get
                {
                    return HasStaticUnit || HasDynamicUnit;
                }
            }

            /// <summary>
            /// Gets a value indicating whether this instance has static unit (static or dynamic).
            /// </summary>
            /// <value><c>true</c> if this instance has static unit; otherwise, <c>false</c>./// </value>
            public bool HasStaticUnit
            {
                get
                {
                    return !string.IsNullOrWhiteSpace(Unit);
                }
            }

            /// <summary>
            /// Gets a value indicating whether this instance has dynamic unit.
            /// </summary>
            /// <value><c>true</c> if this instance has dynamic unit; otherwise, <c>false</c>./// </value>
            public bool HasDynamicUnit
            {
                get
                {
                    return UnitPropertyInfo != null;
                }
            }

            /// <summary>
            /// Gets the unit property info.
            /// </summary>
            /// <value>
            /// The unit property info.
            /// </value>
            public Reflection.PropertyInfo UnitPropertyInfo
            {
                get;
                private set;
            }

            /// <summary>
            /// Gets a value indicating whether this instance is percent.
            /// </summary>
            /// <value><c>true</c> if this instance is percent; otherwise, <c>false</c>.</value>
            public bool IsPercent
            {
                get;
                private set;
            }

            /// <summary>
            /// Gets or sets a value indicating whether this instance is cost property.
            /// </summary>
            public bool IsCostProperty { get; set; }

            /// <summary>
            /// Gets the type of the target enum.
            /// </summary>
            public Type TargetEnumType { get; private set; }

            /// <summary>
            /// Gets the parent data category.
            /// </summary>
            public DataCategoryItem ParentDataCategory
            {
                get;
                private set;
            }

            /// <summary>
            /// Gets the unit of the value this property represents (if it's not changeable) or NULL if the property doesn't have a unit (e.g. Name or Description)
            /// </summary>
            /// <value>
            /// The unit of the value this property represents (if it's not changeable) or NULL if the property doesn't have a unit (e.g. Name or Description)
            /// </value>
            public string Unit
            {
                get;
                private set;
            }

            /// <summary>
            /// Gets or sets the type of the item measurement unit
            /// </summary>
            public MeasurementUnitType? UnitType { get; set; }

            /// <summary>
            /// Gets or sets the property info this item represents.
            /// </summary>
            /// <value>
            /// The property info this item represents.
            /// </value>
            public Reflection.PropertyInfo Property
            {
                get { return this.property; }
                set { this.SetProperty(ref this.property, value, () => this.Property); }
            }

            /// <summary>
            /// Gets or sets the second property this item represents.
            /// </summary>
            /// <value>
            /// The second property this item represents.
            /// </value>
            public Reflection.PropertyInfo SecondProperty
            {
                get { return this.secondProperty; }
                set { this.SetProperty(ref this.secondProperty, value, () => this.SecondProperty); }
            }

            /// <summary>
            /// Gets or sets a value indicating whether this instance is process step labour data field.
            /// </summary>
            /// <value><c>true</c> if this instance is process step labour data field; otherwise, <c>false</c>.</value>
            public bool IsProcessStepLabourDataField
            {
                get { return this.isProcessStepLabourDataField; }
                set { this.SetProperty(ref this.isProcessStepLabourDataField, value, () => this.IsProcessStepLabourDataField); }
            }

            /// <summary>
            /// Called when [is checked changed].
            /// </summary>
            protected override void OnIsCheckedChanged()
            {
                base.OnIsCheckedChanged();

                if (IsChecked)
                {
                    // Stop the event handling for OnIsCheckedChanged in the parent data category (if not, a checked category will check all its fields).
                    ParentDataCategory.HandleOnIsCheckedChanged = false;
                    ParentDataCategory.IsChecked = IsChecked;
                    ParentDataCategory.HandleOnIsCheckedChanged = true;
                }
            }
        }

        #endregion Nested types
    }
}