﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Windows.Controls;
using System.Windows.Input;
using System.Xml;
using System.Xml.Serialization;
using ZPKTool.Common;
using ZPKTool.Gui.Resources;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for ReportGenerator.
    /// It contains the logic for the ReportGenerator's configurations.
    /// </summary>
    public partial class ReportGeneratorViewModel
    {
        #region Attributes

        /// <summary>
        /// The path for the old settings file.
        /// </summary>
        private static readonly string SettingsFilePath = Path.Combine(ZPKTool.Common.Constants.ApplicationDataFolderPath, "ReportGeneratorSettings.dat");

        /// <summary>
        /// The path for the new xml file containing the configuration settings.
        /// </summary>
        private static readonly string ConfigurationsXmlFile = Path.Combine(ZPKTool.Common.Constants.ApplicationDataFolderPath, "ReportGeneratorConfigurations.xml");

        /// <summary>
        /// The guid of the default configuration.
        /// </summary>
        private static readonly Guid DefaultConfigurationGuid = new Guid("D21E0795-1CF9-4DBD-8C47-665E080F9693");

        /// <summary>
        /// The observable collection containing report generator configuration.
        /// </summary>
        private ObservableCollection<ReportGeneratorConfiguration> configurations;

        /// <summary>
        /// The selected configuration from data grid.
        /// </summary>
        private ReportGeneratorConfiguration selectedConfiguration;

        #endregion

        #region Commands

        /// <summary>
        /// Gets the new configuration command.
        /// </summary>
        public ICommand NewConfigurationCommand { get; private set; }

        /// <summary>
        /// Gets the duplicate configuration command.
        /// </summary>
        public ICommand DuplicateConfigurationCommand { get; private set; }

        /// <summary>
        /// Gets the delete configuration command.
        /// </summary>
        public ICommand DeleteConfigurationCommand { get; private set; }

        /// <summary>
        /// Gets the import configuration command.
        /// </summary>
        public ICommand ImportConfigurationCommand { get; private set; }

        /// <summary>
        /// Gets the export configuration command.
        /// </summary>
        public ICommand ExportConfigurationCommand { get; private set; }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the observable collection containing report generator configuration.
        /// </summary>
        public ObservableCollection<ReportGeneratorConfiguration> Configurations
        {
            get { return this.configurations; }
            set { this.SetProperty(ref this.configurations, value, () => this.Configurations); }
        }

        /// <summary>
        /// Gets or sets the observable collection containing report generator configuration.
        /// </summary>
        public ReportGeneratorConfiguration SelectedConfiguration
        {
            get
            {
                return this.selectedConfiguration;
            }

            set
            {
                if (value != this.selectedConfiguration)
                {
                    if (this.selectedConfiguration != null)
                    {
                        SaveSelectionMappings(this.selectedConfiguration);
                    }

                    this.selectedConfiguration = value;
                    this.OnPropertyChanged(() => this.SelectedConfiguration);
                    LoadSelectionMappings(this.selectedConfiguration);
                }
            }
        }

        #endregion

        /// <summary>
        /// Loads the report generator configurations from the file containing the settings.
        /// </summary>
        private void LoadConfigurationsFromFile()
        {
            try
            {
                if (File.Exists(SettingsFilePath))
                {
                    // This code will be executed only once, to read the old settings into the default configuration.
                    // The old settings file will be then deleted.
                    Dictionary<DataCategoryType, List<string>> selectionMappings = null;
                    try
                    {
                        using (Stream stream = File.Open(SettingsFilePath, FileMode.Open))
                        {
                            BinaryFormatter bformatter = new BinaryFormatter();
                            selectionMappings = bformatter.Deserialize(stream) as Dictionary<DataCategoryType, List<string>>;
                        }
                    }
                    catch (Exception ex)
                    {
                        log.ErrorException("Error while deserializing file " + SettingsFilePath, ex);
                    }

                    var defaultConfiguration = new ReportGeneratorConfiguration();
                    defaultConfiguration.Name = LocalizedResources.General_DefaultConfigurationName;
                    defaultConfiguration.Uid = DefaultConfigurationGuid;
                    defaultConfiguration.ConfigurationData = selectionMappings;

                    this.Configurations.Add(defaultConfiguration);
                    this.SelectedConfiguration = defaultConfiguration;
                    File.Delete(SettingsFilePath);
                }
                else if (File.Exists(ConfigurationsXmlFile))
                {
                    // Deserialize the xml file containing all the configurations.
                    ReportGeneratorConfigurations deserializedConfigurations = null;
                    try
                    {
                        using (Stream stream = new FileStream(ConfigurationsXmlFile, FileMode.Open))
                        {
                            XmlTextReader reader = new XmlTextReader(stream);
                            XmlSerializer serializer = new XmlSerializer(typeof(ReportGeneratorConfigurations));
                            deserializedConfigurations = (ReportGeneratorConfigurations)serializer.Deserialize(reader);
                        }
                    }
                    catch (Exception ex)
                    {
                        log.ErrorException("Error while deserializing file " + ConfigurationsXmlFile, ex);
                    }

                    if (deserializedConfigurations != null)
                    {
                        foreach (var configuration in deserializedConfigurations.Configuration)
                        {
                            this.Configurations.Add(configuration);
                        }

                        this.SelectedConfiguration = this.Configurations.FirstOrDefault(c => c.Uid == deserializedConfigurations.SelectedConfigurationUid);
                    }
                }
                else
                {
                    // If no file containing the configurations is found, then a new, default configuration is created without configuration data.
                    var defaultConfiguration = new ReportGeneratorConfiguration();
                    defaultConfiguration.Name = LocalizedResources.General_DefaultConfigurationName;
                    defaultConfiguration.Uid = DefaultConfigurationGuid;

                    this.Configurations.Add(defaultConfiguration);
                    this.SelectedConfiguration = defaultConfiguration;
                }
            }
            catch (Exception ex)
            {
                log.ErrorException("Error while loading report generator selections.", ex);
            }
        }

        /// <summary>
        /// Loads the configuration's selection mappings and checks all the data categories found in configuration data.
        /// </summary>
        /// <param name="configuration">The configuration from which the selection mappings are loaded.</param>
        private void LoadSelectionMappings(ReportGeneratorConfiguration configuration)
        {
            // Uncheck all the categories before loading another one.
            foreach (var category in this.DataCategories)
            {
                category.IsChecked = false;
                foreach (var field in category.DataFields)
                {
                    field.IsChecked = false;
                }
            }

            if (configuration != null && configuration.ConfigurationData != null)
            {
                // Mark each category item from the dictionary containing the configuration data as checked.
                foreach (var selectionMapping in configuration.ConfigurationData)
                {
                    var dataCategoryItem = DataCategories.FirstOrDefault(item => item.CategoryType == selectionMapping.Key);
                    if (dataCategoryItem != null)
                    {
                        foreach (var propertyName in selectionMapping.Value)
                        {
                            var dataFieldItem = dataCategoryItem.DataFields.FirstOrDefault(item => item.Property.Name == propertyName);
                            if (dataFieldItem != null)
                            {
                                dataFieldItem.IsChecked = true;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Saves all the report generator configurations into the xml file, on the Onunloaded event.
        /// </summary>
        private void SaveConfigurationsToFile()
        {
            try
            {
                if (this.Configurations.Count > 0)
                {
                    // Save the selection mappings for the selected configuration.
                    this.SaveSelectionMappings(this.SelectedConfiguration);
                }

                var configurationsToSerialize = new ReportGeneratorConfigurations();
                foreach (var item in this.Configurations)
                {
                    // Serialize the configuration data containing the selection mappings for each configuration.
                    item.SerializeConfigurationData();
                    configurationsToSerialize.Configuration.Add(item);
                }

                // Set the last selected configuration.
                configurationsToSerialize.SelectedConfigurationUid = this.SelectedConfiguration.Uid;

                // Serialize the configurations.
                using (Stream stream = new FileStream(ConfigurationsXmlFile, FileMode.Create))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(ReportGeneratorConfigurations));
                    serializer.Serialize(stream, configurationsToSerialize);
                }
            }
            catch (Exception ex)
            {
                log.ErrorException("Exception occurred while trying to create the report generator configurations.", ex);
            }
        }

        /// <summary>
        /// Saves the selection mappings for a configuration into configuration data.
        /// </summary>
        /// <param name="configuration">The configuration for which the selection mappings are saved.</param>
        private void SaveSelectionMappings(ReportGeneratorConfiguration configuration)
        {
            try
            {
                Dictionary<DataCategoryType, List<string>> selectionMappings = new Dictionary<DataCategoryType, List<string>>();
                foreach (var dataCategoryItem in DataCategories.Where(category => category.IsChecked).ToList())
                {
                    selectionMappings.Add(dataCategoryItem.CategoryType, dataCategoryItem.DataFields.Where(item => item.IsChecked).Select(item => item.Property.Name).ToList());
                }

                bool isConfigurationChanged = false;
                if (configuration.ConfigurationData != null && selectionMappings != null)
                {
                    // Check if the selection mappings from the selected configuration are changed.
                    isConfigurationChanged = !configuration.CompareConfigurationData(selectionMappings);
                }

                if (isConfigurationChanged)
                {
                    configuration.DateChanged = DateTime.Now;
                }

                configuration.ConfigurationData = selectionMappings;
            }
            catch (Exception ex)
            {
                log.ErrorException("Error while saving report generator selections.", ex);
            }
        }

        #region Add

        /// <summary>
        /// Determines whether this instance can execute the NewConfiguration command.
        /// </summary>
        /// <returns>True if the command can be executed; false otherwise.</returns>
        private bool CanAddNewConfiguration()
        {
            return true;
        }

        /// <summary>
        /// Adds a new configuration; executed by the NewConfiguration command.
        /// </summary>
        private void AddNewConfiguration()
        {
            var newConfiguration = new ReportGeneratorConfiguration();
            newConfiguration.Name = LocalizedResources.ReportGenerator_NewConfiguration;
            this.Configurations.Add(newConfiguration);
            this.SelectedConfiguration = newConfiguration;
        }

        #endregion

        #region Delete

        /// <summary>
        /// Determines whether this instance can execute the DeleteConfiguration command.
        /// </summary>
        /// <returns>True if the command can be executed; false otherwise.</returns>
        private bool CanDeleteSelectedConfiguration()
        {
            return this.SelectedConfiguration != null && this.SelectedConfiguration.Uid != DefaultConfigurationGuid;
        }

        /// <summary>
        /// Deletes a configuration; executed by the DeleteConfiguration command.
        /// </summary>
        private void DeleteSelectedConfiguration()
        {
            var questionMessage = string.Format(LocalizedResources.Question_Delete_Item, this.SelectedConfiguration.Name);
            MessageDialogResult result = windowService.MessageDialogService.Show(questionMessage, MessageDialogType.YesNo);
            if (result == MessageDialogResult.Yes)
            {
                var index = this.Configurations.IndexOf(this.SelectedConfiguration);
                this.Configurations.Remove(this.SelectedConfiguration);

                // Set the selected configuration according to the position of the deleted configuration.                
                if (this.Configurations.Count == 1)
                {
                    this.SelectedConfiguration = this.Configurations[0];
                }
                else if (index < this.Configurations.Count)
                {
                    this.SelectedConfiguration = this.Configurations[index];
                }
                else
                {
                    this.SelectedConfiguration = this.Configurations.Last();
                }
            }
        }

        #endregion

        #region Duplicate

        /// <summary>
        /// Determines whether this instance can execute the DuplicateConfiguration command.
        /// </summary>
        /// <returns>True if the command can be executed; false otherwise.</returns>
        private bool CanDuplicateSelectedConfiguration()
        {
            return this.SelectedConfiguration != null;
        }

        /// <summary>
        /// Creates a copy of the configuration; executed by the DuplicateConfiguration command.
        /// </summary>
        private void DuplicateSelectedConfiguration()
        {
            // Save and serialize the selection mappings for the selected configuration to duplicate.
            this.SaveSelectionMappings(this.SelectedConfiguration);
            this.SelectedConfiguration.SerializeConfigurationData();

            var clonedConfiguration = this.SelectedConfiguration.Clone();
            clonedConfiguration.Name = LocalizedResources.General_CopyOf + " " + clonedConfiguration.Name;
            clonedConfiguration.DateChanged = DateTime.Now;
            clonedConfiguration.Uid = Guid.NewGuid();

            this.Configurations.Add(clonedConfiguration);
            this.SelectedConfiguration = clonedConfiguration;
        }

        #endregion

        #region Import

        /// <summary>
        /// Determines whether this instance can execute the ImportConfiguration command.
        /// </summary>
        /// <returns>True if the command can be executed; false otherwise.</returns>
        private bool CanImportConfiguration()
        {
            return true;
        }

        /// <summary>
        /// Imports a configuration from a selected file; executed by the ImportConfiguration command.
        /// </summary>
        private void ImportConfiguration()
        {
            this.fileDialogService.Reset();
            this.fileDialogService.Filter = LocalizedResources.DialogFilter_XmlDocs;
            this.fileDialogService.Multiselect = false;

            if (fileDialogService.ShowOpenFileDialog() == false)
            {
                return;
            }

            var filePath = fileDialogService.FileName;
            try
            {
                using (StreamReader stream = new StreamReader(filePath))
                {
                    XmlTextReader reader = new XmlTextReader(stream);
                    XmlSerializer deserializer = new XmlSerializer(typeof(ReportGeneratorConfiguration));
                    ReportGeneratorConfiguration configuration = deserializer.Deserialize(reader) as ReportGeneratorConfiguration;
                    if (configuration != null)
                    {
                        this.Configurations.Add(configuration);
                        this.SelectedConfiguration = configuration;
                    }
                    else
                    {
                        log.Error("The deserialized configuration is null. FileName: " + filePath);
                    }
                }
            }
            catch (Exception ex)
            {
                log.ErrorException("An error occurred while trying to import a configuration.", ex);
                windowService.MessageDialogService.Show(LocalizedResources.General_ErrorImportReportGeneratorConfiguration, MessageDialogType.Error);
            }
        }

        #endregion

        #region Export

        /// <summary>
        /// Determines whether this instance can execute the ExportConfiguration command.
        /// </summary>
        /// <returns>True if the command can be executed; false otherwise.</returns>
        private bool CanExportSelectedConfiguration()
        {
            return this.SelectedConfiguration != null && !string.IsNullOrEmpty(this.SelectedConfiguration.Name) && this.SelectedConfiguration.Uid != DefaultConfigurationGuid;
        }

        /// <summary>
        /// Exports the selected configuration into a new file; executed by the ExportConfiguration command.
        /// </summary>
        private void ExportSelectedConfiguration()
        {
            try
            {
                this.fileDialogService.Reset();
                this.fileDialogService.Filter = LocalizedResources.DialogFilter_XmlDocs;
                this.fileDialogService.FileName = PathUtils.SanitizeFileName(this.SelectedConfiguration.Name);

                if (this.fileDialogService.ShowSaveFileDialog() == true)
                {
                    // Save and serialize the selection mappings for the selected configuration to export.
                    this.SaveSelectionMappings(this.SelectedConfiguration);
                    this.SelectedConfiguration.SerializeConfigurationData();

                    using (Stream stream = new FileStream(this.fileDialogService.FileName, FileMode.Create))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(ReportGeneratorConfiguration));
                        serializer.Serialize(stream, this.SelectedConfiguration);
                    }
                }
            }
            catch (Exception ex)
            {
                log.ErrorException("An error occurred while trying to export a configuration.", ex);
                throw PathUtils.ParseFileRelatedException(ex);
            }
        }

        #endregion
    }
}
