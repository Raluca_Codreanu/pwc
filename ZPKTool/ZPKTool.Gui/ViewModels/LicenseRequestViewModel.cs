﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Windows.Input;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Common.Mapi;
using ZPKTool.Gui.Resources;
using ZPKTool.LicenseValidator;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the IMyView view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class LicenseRequestViewModel : ViewModel
    {
        #region Members

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The window box service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The company name.
        /// </summary>
        private string companyName;

        /// <summary>
        /// The name of the user.
        /// </summary>
        private string userName;

        /// <summary>
        /// The user's phone information.
        /// </summary>
        private string userPhone;

        /// <summary>
        /// The user's fax information.
        /// </summary>
        private string userFax;

        /// <summary>
        /// The user's email information.
        /// </summary>
        private string userEmail;

        /// <summary>
        /// The user's street information.
        /// </summary>
        private string userStreet;

        /// <summary>
        /// The user's city information.
        /// </summary>
        private string userCity;

        /// <summary>
        /// The user's postcode.
        /// </summary>
        private string userPostcode;

        /// <summary>
        /// The user's country.
        /// </summary>
        private string userCountry;

        /// <summary>
        /// Represents a dynamic data collection with license types that provides notifications when items get added, removed, or when the whole list is refreshed.
        /// </summary>
        private ObservableCollection<Pair<string, LicenseType>> licenseTypes;

        /// <summary>
        /// Represents a dynamic data collection with license levels that provides notifications when items get added, removed, or when the whole list is refreshed.
        /// </summary>
        private ObservableCollection<Pair<string, LicenseLevel>> licenseLevels;

        /// <summary>
        /// The selected license type.
        /// </summary>
        private Pair<string, LicenseType> selectedLicenseType;

        /// <summary>
        /// The selected license level.
        /// </summary>
        private Pair<string, LicenseLevel> selectedLicenseLevel;

        /// <summary>
        /// The license application version.
        /// </summary>
        private string applicationVersion;

        /// <summary>
        /// The license starting date.
        /// </summary>
        private DateTime startingDate;

        /// <summary>
        /// The license expiration date.
        /// </summary>
        private DateTime expirationDate;

        /// <summary>
        /// The validation Errors Grid Visibility property.
        /// </summary>
        private System.Windows.Visibility validationErrorsGridVisibility;

        /// <summary>
        /// The validation error messages.
        /// </summary>
        private string validationErrorMessage;

        /// <summary>
        /// The navigate uri.
        /// </summary>
        private string navigateUriMailTo;

        /// <summary>
        /// The destination mail address.
        /// </summary>
        private string destinationMailAddress;

        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="LicenseRequestViewModel" /> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        /// <param name="serialNumber">The serial number.</param>
        [ImportingConstructor]
        public LicenseRequestViewModel(IWindowService windowService, string serialNumber)
        {
            this.windowService = windowService;

            // commands
            this.CloseLicenseRequestCommand = new DelegateCommand(CloseLicenseRequest);
            this.WindowClosingCommand = new DelegateCommand<CancelEventArgs>(WindowClosing);
            this.SendLicenseRequestCommand = new DelegateCommand(SendLicenseRequest);
            this.CloseErrorsCommand = new DelegateCommand(CloseErrors);
            this.CopyToClipboardCommand = new DelegateCommand(CopyToClipboard);
            this.NavigateUriCommand = new DelegateCommand(NavigateUri);

            var dbGlobalSettingsMgr = new DbGlobalSettingsManager(DataAccess.DbIdentifier.LocalDatabase);
            var dbGlobalSettings = dbGlobalSettingsMgr.Get();
            this.DestinationMailAddress = dbGlobalSettings.ContactEmail;

            this.NavigateUriMailTo = "mailto:" + this.DestinationMailAddress + "?Subject=" + LocalizedResources.General_LicenseRequest;
            this.SerialNumber = serialNumber;            

            // license Type
            this.LicenseTypes = new ObservableCollection<Pair<string, LicenseType>>();
            this.LicenseTypes.Add(new Pair<string, LicenseType>(LocalizedResources.LicenseType_Nodelocked, LicenseType.Nodelocked));
            this.SelectedLicenseType = this.LicenseTypes.FirstOrDefault();

            // license Level
            this.LicenseLevels = new ObservableCollection<Pair<string, LicenseLevel>>();
            this.LicenseLevels.Add(new Pair<string, LicenseLevel>(LocalizedResources.LicenseLevel_Trial, LicenseLevel.Trial));
            this.LicenseLevels.Add(new Pair<string, LicenseLevel>(LocalizedResources.LicenseLevel_Basic, LicenseLevel.Basic));
            this.LicenseLevels.Add(new Pair<string, LicenseLevel>(LocalizedResources.LicenseLevel_ProfessionalMech, LicenseLevel.ProfessionalMech));
            this.LicenseLevels.Add(new Pair<string, LicenseLevel>(LocalizedResources.LicenseLevel_ProfessionalMechIncMDUpdates, LicenseLevel.ProfessionalMechIncMDUpdates));
            this.SelectedLicenseLevel = this.LicenseLevels.FirstOrDefault();

            this.StartingDate = DateTime.Now;
            this.ExpirationDate = DateTime.Now;

            this.ValidationErrorsGridVisibility = System.Windows.Visibility.Collapsed;
            this.ValidationErrorMessage = string.Empty;
            Assembly assembly = Assembly.GetExecutingAssembly();
            this.ApplicationVersion = assembly.GetName().Version.ToString();
        }

        #endregion

        #region Commands

        /// <summary>
        /// Gets the close license request command.
        /// </summary>
        /// <value>The close license request command.</value>
        public ICommand CloseLicenseRequestCommand { get; private set; }

        /// <summary>
        /// Gets the window closing command.
        /// </summary>
        /// <value>The window closing.</value>
        public ICommand WindowClosingCommand { get; private set; }

        /// <summary>
        /// Gets the send license request command.
        /// </summary>
        /// <value>The send license request command.</value>
        public ICommand SendLicenseRequestCommand { get; private set; }

        /// <summary>
        /// Gets the close errors command.
        /// </summary>
        /// <value>The close errors command.</value>
        public ICommand CloseErrorsCommand { get; private set; }

        /// <summary>
        /// Gets the copy to clipboard command.
        /// </summary>
        /// <value>The copy to clipboard command.</value>
        public ICommand CopyToClipboardCommand { get; private set; }

        /// <summary>
        /// Gets the navigate URI command.
        /// </summary>
        /// <value>The navigate URI command.</value>
        public ICommand NavigateUriCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets or sets the name of the company.
        /// </summary>
        /// <value>The name of the company.</value>
        public string CompanyName
        {
            get
            {
                return this.companyName;
            }

            set
            {
                if (this.companyName != value)
                {
                    this.companyName = value;
                    OnPropertyChanged("CompanyName");
                }
            }
        }

        /// <summary>
        /// Gets or sets the name of the User.
        /// </summary>
        /// <value>The user name.</value>
        public string UserName
        {
            get
            {
                return this.userName;
            }

            set
            {
                if (this.userName != value)
                {
                    this.userName = value;
                    OnPropertyChanged("UserName");
                }
            }
        }

        /// <summary>
        /// Gets or sets the phone.
        /// </summary>
        /// <value>The phone.</value>
        public string UserPhone
        {
            get
            {
                return this.userPhone;
            }

            set
            {
                if (this.userPhone != value)
                {
                    this.userPhone = value;
                    OnPropertyChanged("UserPhone");
                }
            }
        }

        /// <summary>
        /// Gets or sets the fax.
        /// </summary>
        /// <value>The user's fax.</value>
        public string UserFax
        {
            get
            {
                return this.userFax;
            }

            set
            {
                if (this.userFax != value)
                {
                    this.userFax = value;
                    OnPropertyChanged("UserFax");
                }
            }
        }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The email.</value>
        public string UserEmail
        {
            get
            {
                return this.userEmail;
            }

            set
            {
                if (this.userEmail != value)
                {
                    this.userEmail = value;
                    OnPropertyChanged("UserEmail");
                }
            }
        }

        /// <summary>
        /// Gets or sets the street.
        /// </summary>
        /// <value>The street.</value>
        public string UserStreet
        {
            get
            {
                return this.userStreet;
            }

            set
            {
                if (this.userStreet != value)
                {
                    this.userStreet = value;
                    OnPropertyChanged("UserStreet");
                }
            }
        }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        /// <value>The user's city.</value>
        public string UserCity
        {
            get
            {
                return this.userCity;
            }

            set
            {
                if (this.userCity != value)
                {
                    this.userCity = value;
                    OnPropertyChanged("UserCity");
                }
            }
        }

        /// <summary>
        /// Gets or sets the post code.
        /// </summary>
        /// <value>The post code.</value>
        public string UserPostCode
        {
            get
            {
                return this.userPostcode;
            }

            set
            {
                if (this.userPostcode != value)
                {
                    this.userPostcode = value;
                    OnPropertyChanged("UserPostCode");
                }
            }
        }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        /// <value>The country.</value>
        public string UserCountry
        {
            get
            {
                return this.userCountry;
            }

            set
            {
                if (this.userCountry != value)
                {
                    this.userCountry = value;
                    OnPropertyChanged("UserCountry");
                }
            }
        }

        /// <summary>
        /// Gets or sets the serial number.
        /// </summary>
        /// <value>The serial number.</value>
        public string SerialNumber { get; set; }

        /// <summary>
        /// Gets or sets the license types.
        /// </summary>
        /// <value>The license types.</value>
        public ObservableCollection<Pair<string, LicenseType>> LicenseTypes
        {
            get
            {
                return this.licenseTypes;
            }

            set
            {
                if (this.licenseTypes != value)
                {
                    this.licenseTypes = value;
                    OnPropertyChanged("LicenseTypes");
                }
            }
        }

        /// <summary>
        /// Gets or sets the license levels.
        /// </summary>
        /// <value>The license levels.</value>
        public ObservableCollection<Pair<string, LicenseLevel>> LicenseLevels
        {
            get
            {
                return this.licenseLevels;
            }

            set
            {
                if (this.licenseLevels != value)
                {
                    this.licenseLevels = value;
                    OnPropertyChanged("LicenseLevels");
                }
            }
        }

        /// <summary>
        /// Gets or sets the type of the selected license.
        /// </summary>
        /// <value>The type of the selected license.</value>
        public Pair<string, LicenseType> SelectedLicenseType
        {
            get
            {
                return this.selectedLicenseType;
            }

            set
            {
                if (this.selectedLicenseType != value)
                {
                    this.selectedLicenseType = value;
                    OnPropertyChanged("SelectedLicenseType");
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected license level.
        /// </summary>
        /// <value>The selected license level.</value>
        public Pair<string, LicenseLevel> SelectedLicenseLevel
        {
            get
            {
                return this.selectedLicenseLevel;
            }

            set
            {
                if (this.selectedLicenseLevel != value)
                {
                    this.selectedLicenseLevel = value;
                    OnPropertyChanged("SelectedLicenseLevel");
                }
            }
        }

        /// <summary>
        /// Gets or sets the starting date.
        /// </summary>
        /// <value>The starting date.</value>
        public DateTime StartingDate
        {
            get
            {
                return this.startingDate;
            }

            set
            {
                if (value > ExpirationDate)
                {
                    this.ExpirationDate = value;
                }

                if (this.startingDate != value)
                {
                    this.startingDate = value;
                    OnPropertyChanged("StartingDate");
                }
            }
        }

        /// <summary>
        /// Gets or sets the expiration date.
        /// </summary>
        /// <value>The expiration date.</value>
        public DateTime ExpirationDate
        {
            get
            {
                return this.expirationDate;
            }

            set
            {
                if (this.expirationDate != value)
                {
                    this.expirationDate = value;
                    OnPropertyChanged("ExpirationDate");
                }
            }
        }

        /// <summary>
        /// Gets or sets the application version.
        /// </summary>
        /// <value>The application version.</value>
        public string ApplicationVersion
        {
            get
            {
                return this.applicationVersion;
            }

            set
            {
                if (this.applicationVersion != value)
                {
                    this.applicationVersion = value;
                    OnPropertyChanged("ApplicationVersion");
                }
            }
        }

        /// <summary>
        /// Gets or sets the validation error message.
        /// </summary>
        /// <value>The validation error message.</value>
        public string ValidationErrorMessage
        {
            get
            {
                return this.validationErrorMessage;
            }

            set
            {
                if (this.validationErrorMessage != value)
                {
                    this.validationErrorMessage = value;
                    OnPropertyChanged("ValidationErrorMessage");
                }
            }
        }

        /// <summary>
        /// Gets or sets the validation errors grid visibility.
        /// </summary>
        /// <value>The validation errors grid visibility.</value>
        public System.Windows.Visibility ValidationErrorsGridVisibility
        {
            get
            {
                return this.validationErrorsGridVisibility;
            }

            set
            {
                if (this.validationErrorsGridVisibility != value)
                {
                    this.validationErrorsGridVisibility = value;
                    OnPropertyChanged("ValidationErrorsGridVisibility");
                }
            }
        }

        /////// <summary>
        /////// Gets or sets the SMTP client.
        /////// </summary>
        /////// <value>The SMTP client.</value>
        ////private SmtpClient SmtpMailClient { get; set; }       

        /// <summary>
        /// Gets or sets the navigate URI.
        /// </summary>
        /// <value>The navigate URI.</value>
        public string NavigateUriMailTo
        {
            get
            {
                return this.navigateUriMailTo;
            }

            set
            {
                if (this.navigateUriMailTo != value)
                {
                    this.navigateUriMailTo = value;
                    OnPropertyChanged("NavigateUriMailTo");
                }
            }
        }

        /// <summary>
        /// Gets or sets the destination mail address.
        /// </summary>
        /// <value>The destination mail address.</value>
        public string DestinationMailAddress
        {
            get
            {
                return this.destinationMailAddress;
            }

            set
            {
                if (this.destinationMailAddress != value)
                {
                    this.destinationMailAddress = value;
                    OnPropertyChanged("DestinationMailAddress");
                }
            }
        }

        #endregion Properties

        #region Command Actions

        /// <summary>
        /// Closes the errors panel.
        /// </summary>
        private void CloseErrors()
        {
            this.ValidationErrorsGridVisibility = System.Windows.Visibility.Collapsed;
        }

        /// <summary>
        /// Sends the license request by email.
        /// </summary>
        private void SendLicenseRequest()
        {
            this.ValidationErrorsGridVisibility = System.Windows.Visibility.Collapsed;

            try
            {
                ValidateUserInput();

                string destinationAddress = this.DestinationMailAddress;
                if (string.IsNullOrWhiteSpace(destinationAddress))
                {
                    this.windowService.MessageDialogService.Show(LocalizedResources.RequestLicense_SendMailError, MessageDialogType.Error);
                    return;
                }

                MapiMail mail = new MapiMail();
                mail.Subject = LocalizedResources.RequestLicense_MailSubject;
                mail.Body = this.CreateMailBody();
                mail.Recipients.Add(new MapiRecipient(destinationAddress, MapiRecipientType.To));

                MapiMailer mailer = new MapiMailer();
                mailer.SendMail(mail);
            }
            catch (RequestLicenseValidationException ex)
            {
                this.ValidationErrorMessage = string.Empty;

                foreach (string error in ex.ErrorMessages)
                {
                    this.ValidationErrorMessage += error + Environment.NewLine;
                }

                this.ValidationErrorsGridVisibility = System.Windows.Visibility.Visible;
            }
            catch (MapiException ex)
            {
                log.ErrorException("Sending the license request email failed", ex);
                this.windowService.MessageDialogService.Show(LocalizedResources.RequestLicense_SendMailError, MessageDialogType.Error);
            }
        }

        /// <summary>
        /// Creates the mail body.
        /// </summary>
        /// <returns>The Email Message Body Content.</returns>
        private string CreateMailBody()
        {
            System.Text.StringBuilder body = new System.Text.StringBuilder();
            body.AppendLine(LocalizedResources.License_Company + ": " + this.CompanyName);
            body.AppendLine(LocalizedResources.License_User + ": " + this.UserName);
            body.AppendLine(LocalizedResources.RequestLicense_UserPhone + ": " + this.UserPhone);
            body.AppendLine(LocalizedResources.RequestLicense_UserFax + ": " + this.UserFax);
            body.AppendLine(LocalizedResources.RequestLicense_UserEmail + ": " + this.UserEmail);
            body.AppendLine(LocalizedResources.RequestLicense_UserStreet + ": " + this.UserStreet);
            body.AppendLine(LocalizedResources.RequestLicense_UserPostCode + ": " + this.UserPostCode);
            body.AppendLine(LocalizedResources.RequestLicense_UserCountry + ": " + this.UserCountry);
            body.AppendLine(LocalizedResources.RequestLicense_UserCity + ": " + this.UserCity);
            body.AppendLine(LocalizedResources.License_SerialNumber + ": " + this.SerialNumber);
            body.AppendLine(LocalizedResources.RequestLicense_LicenseType + ": " + this.SelectedLicenseType.First);
            body.AppendLine(LocalizedResources.RequestLicense_LicenseLevel + ": " + this.SelectedLicenseLevel.First);
            body.AppendLine(LocalizedResources.RequestLicense_LicenseStartDate + ": " + this.StartingDate.ToShortDateString());
            body.AppendLine(LocalizedResources.RequestLicense_LicenseEndDate + ": " + this.ExpirationDate.ToShortDateString());
            body.AppendLine(LocalizedResources.RequestLicense_LicenseAppVersion + ": " + this.ApplicationVersion);

            return body.ToString();
        }

        /// <summary>
        /// Validates the user input.
        /// </summary>
        private void ValidateUserInput()
        {
            List<string> errorList = new List<string>();

            if (this.CompanyName == null || (this.CompanyName != null && this.CompanyName.Trim().Equals(string.Empty)))
            {
                errorList.Add(LocalizedResources.RequestLicense_ValidationError_CompanyNameEmpty);
            }

            if (this.UserName == null || (this.UserName != null && this.UserName.Trim().Equals(string.Empty)))
            {
                errorList.Add(LocalizedResources.RequestLicense_ValidationError_UserNameEmpty);
            }

            if (string.IsNullOrWhiteSpace(this.UserEmail))
            {
                errorList.Add(LocalizedResources.RequestLicense_ValidationError_UserEmailIsInvalid);
            }
            else if (!ValidationUtils.IsEmail(this.UserEmail))
            {
                errorList.Add(LocalizedResources.RequestLicense_ValidationError_UserEmailIsInvalid);
            }

            if (this.StartingDate > this.ExpirationDate)
            {
                errorList.Add(LocalizedResources.RequestLicense_EndingDateMustBeLaterThanTheStartDate);
            }

            if (errorList.Count > 0)
            {
                throw new RequestLicenseValidationException("Validation Errors", errorList);
            }
        }

        /// <summary>
        /// Closes the license request window.
        /// </summary>
        private void CloseLicenseRequest()
        {
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Window closing action.
        /// </summary>
        /// <param name="arg">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        private void WindowClosing(CancelEventArgs arg)
        {
            if (this.IsRequestEdited())
            {
                MessageDialogResult messageBoxResult = this.windowService.MessageDialogService.Show(LocalizedResources.Question_Quit, MessageDialogType.YesNo);
                if (MessageDialogResult.No == messageBoxResult)
                {
                    arg.Cancel = true;
                }
            }
        }

        /// <summary>
        /// Navigates to the URI.
        /// </summary>
        private void NavigateUri()
        {
            Process.Start(this.NavigateUriMailTo);
        }

        #endregion

        #region Private Method        

        /// <summary>
        /// Determines whether [is request edited].
        /// </summary>
        /// <returns>
        /// <c>true</c> if [is request edited]; otherwise, <c>false</c>.
        /// </returns>
        private bool IsRequestEdited()
        {
            return false;
        }

        /// <summary>
        /// Copies to clipboard the mail body.
        /// </summary>
        private void CopyToClipboard()
        {
            this.ValidationErrorsGridVisibility = System.Windows.Visibility.Collapsed;

            try
            {
                this.ValidateUserInput();

                string mailBody = this.CreateMailBody();

                if (string.IsNullOrEmpty(mailBody))
                {
                    return;
                }

                int attempts = 0;
                while (attempts < 5)
                {
                    try
                    {
                        System.Windows.Clipboard.SetText(mailBody);
                        break;
                    }
                    catch (System.Runtime.InteropServices.COMException)
                    {
                        System.Threading.Thread.Sleep(10);
                        attempts++;
                    }
                    catch
                    {
                        break;
                    }
                }
            }
            catch (RequestLicenseValidationException ex)
            {
                this.ValidationErrorMessage = string.Empty;

                foreach (string error in ex.ErrorMessages)
                {
                    this.ValidationErrorMessage += error + Environment.NewLine;
                }

                this.ValidationErrorsGridVisibility = System.Windows.Visibility.Visible;
            }
        }

        #endregion

        /// <summary>
        /// Thrown if an error occurs while validating the form to request a license.
        /// </summary>
        private class RequestLicenseValidationException : Exception
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="RequestLicenseValidationException" /> class.
            /// </summary>
            /// <param name="errorCode">The error code.</param>
            /// <param name="errorMessages">The error list.</param>
            public RequestLicenseValidationException(string errorCode, IEnumerable<string> errorMessages)
                : base(string.Empty)
            {
                this.ErrorMessages = new List<string>(errorMessages);
            }

            /// <summary>
            /// Gets the error messages.
            /// </summary>
            public IEnumerable<string> ErrorMessages { get; private set; }
        }
    }
}