﻿using System.ComponentModel.Composition;
using System.Windows;
using ZPKTool.Controls;
using ZPKTool.Gui.Views;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// An interface for the service that navigates between the Moulding Wizard steps.
    /// </summary>
    public interface IMouldingModuleNavigationService
    {
        /// <summary>
        /// Starts the moulding wizard and displays the 1st step.
        /// </summary>
        /// <param name="viewModel">The moulding wizard's view model (necessary to be set as the data context of the moulding wizard views).</param>
        void StartWizard(MouldingModuleViewModel viewModel);

        /// <summary>
        /// Closes the wizard.
        /// </summary>
        void CloseWizard();

        /// <summary>
        /// Navigates to the next moulding step.
        /// </summary>
        void GoToNextStep();

        /// <summary>
        /// Navigates to the previous moulding step.
        /// </summary>
        void GoToPreviousStep();
    }

    /// <summary>
    /// The service that navigates between the Moulding Wizard steps.
    /// </summary>
    [Export(typeof(IMouldingModuleNavigationService))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class MouldingModuleNavigationService : IMouldingModuleNavigationService
    {
        /// <summary>
        /// The view of the currently displayed moulding wizard step.
        /// </summary>
        private OverlayWindow currentView;

        /// <summary>
        /// Initializes a new instance of the <see cref="MouldingModuleNavigationService"/> class.
        /// </summary>
        public MouldingModuleNavigationService()
        {
        }

        /// <summary>
        /// Starts the moulding wizard and displays the 1st step.
        /// </summary>
        /// <param name="viewModel">The moulding wizard's view model (necessary to be set as the data context of the moulding wizard views).</param>
        public void StartWizard(MouldingModuleViewModel viewModel)
        {
            MouldingTypeView startView = new MouldingTypeView();
            startView.Owner = Application.Current.MainWindow;
            startView.DataContext = viewModel;

            this.currentView = startView;
            startView.ShowDialog();
        }

        /// <summary>
        /// Closes the wizard.
        /// </summary>
        public void CloseWizard()
        {
            if (this.currentView != null)
            {
                this.currentView.Close();
            }
        }

        /// <summary>
        /// Navigates to the next moulding step.
        /// </summary>
        public void GoToNextStep()
        {
            var nextStep = MouldingWizardStep.None;

            if (this.currentView == null)
            {
                nextStep = MouldingWizardStep.MouldingType;
            }
            else if (this.currentView is MouldingTypeView)
            {
                nextStep = MouldingWizardStep.BasicValues;
            }
            else if (this.currentView is MouldingBasicValuesView)
            {
                nextStep = MouldingWizardStep.ResultingProcessValues;
            }

            this.DisplayStep(nextStep);
        }

        /// <summary>
        /// Navigates to the previous moulding step.
        /// </summary>
        public void GoToPreviousStep()
        {
            var previousStep = MouldingWizardStep.None;

            if (this.currentView is MouldingResultingProcessValuesView)
            {
                previousStep = MouldingWizardStep.BasicValues;
            }
            else if (this.currentView is MouldingBasicValuesView)
            {
                previousStep = MouldingWizardStep.MouldingType;
            }

            this.DisplayStep(previousStep);
        }

        /// <summary>
        /// Displays the specified step in the moulding wizard.
        /// </summary>
        /// <param name="step">The step to display.</param>
        private void DisplayStep(MouldingWizardStep step)
        {
            OverlayWindow stepView = null;
            switch (step)
            {
                case MouldingWizardStep.MouldingType:
                    stepView = new MouldingTypeView();
                    break;
                case MouldingWizardStep.BasicValues:
                    stepView = new MouldingBasicValuesView();
                    break;
                case MouldingWizardStep.ResultingProcessValues:
                    stepView = new MouldingResultingProcessValuesView();
                    break;
            }

            if (stepView != null)
            {
                stepView.Owner = this.currentView.Owner;
                stepView.DataContext = this.currentView.DataContext;

                this.currentView.Close();
                this.currentView = stepView;
                stepView.ShowDialog();
            }
        }
    }
}