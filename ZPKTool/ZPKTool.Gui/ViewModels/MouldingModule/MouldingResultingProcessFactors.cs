﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The moulding resulting process factors.
    /// </summary>
    public class MouldingResultingProcessFactors
    {
        /// <summary>
        /// Gets or sets the place insert parts factor.
        /// </summary>
        public decimal PlaceInsertPartsFactor { get; set; }

        /// <summary>
        /// Gets or sets the close moulding die factor.
        /// </summary>
        public decimal CloseMouldingDieFactor { get; set; }

        /// <summary>
        /// Gets or sets the close slider factor.
        /// </summary>
        public decimal CloseSliderFactor { get; set; }

        /// <summary>
        /// Gets or sets the inject mould factor.
        /// </summary>
        public decimal InjectMouldFactor { get; set; }

        /// <summary>
        /// Gets or sets the cooling factor.
        /// </summary>
        public decimal CoolingFactor { get; set; }

        /// <summary>
        /// Gets or sets the pull slider factor.
        /// </summary>
        public decimal PullSliderFactor { get; set; }

        /// <summary>
        /// Gets or sets the open moulding die factor.
        /// </summary>
        public decimal OpenMouldingDieFactor { get; set; }

        /// <summary>
        /// Gets or sets the pick parts.
        /// </summary>
        public decimal PickPartsFactor { get; set; }
    }
}