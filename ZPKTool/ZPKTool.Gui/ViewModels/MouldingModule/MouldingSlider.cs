﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Moulding Slider Type.
    /// </summary>
    public enum MouldingSlider
    {
        /// <summary>
        /// Hydraulic Slider.
        /// </summary>
        HydraulicSlider = 0,

        /// <summary>
        /// Mechanical Slider.
        /// </summary>
        MechanicalSlider = 1,

        /// <summary>
        /// No Slider.
        /// </summary>
        None = 2
    }
}
