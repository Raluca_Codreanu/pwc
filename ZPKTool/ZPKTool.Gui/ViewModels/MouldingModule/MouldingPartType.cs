﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Part Type Selector.
    /// </summary>
    public enum MouldingPartType
    {
        /// <summary>
        /// Plate Type.
        /// </summary>
        PlateType = 0,

        /// <summary>
        /// Volume Type.
        /// </summary>
        VolumeType = 1
    }
}