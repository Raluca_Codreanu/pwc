﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Windows.Input;
using ZPKTool.Business;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Managers;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the MouldingModule.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class MouldingModuleViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The default number of inserted parts.
        /// </summary>
        private const int DefaultNumberOfInsertedParts = 3;

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The composition container.
        /// </summary>
        private CompositionContainer container;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The online checker.
        /// </summary>
        private IOnlineCheckService onlineChecker;

        /// <summary>
        /// The parent part.
        /// </summary>
        private Part parentPart;

        /// <summary>
        /// The data source manager of the selected machines.
        /// </summary>
        private IDataSourceManager machinesDataSourceManager;

        /// <summary>
        /// The Part's data source manager.
        /// </summary>
        private IDataSourceManager partDataSourceManager;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        #endregion Attributes

        /// <summary>
        /// Initializes a new instance of the <see cref="MouldingModuleViewModel" /> class.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="onlineChecker">The online checker.</param>
        /// <param name="unitsService">The units service.</param>
        [ImportingConstructor]
        public MouldingModuleViewModel(
            CompositionContainer container,
            IWindowService windowService,
            IOnlineCheckService onlineChecker,
            IUnitsService unitsService)
        {
            Argument.IsNotNull("container", container);
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("onlineChecker", onlineChecker);
            Argument.IsNotNull("unitsService", unitsService);

            this.container = container;
            this.windowService = windowService;
            this.onlineChecker = onlineChecker;
            this.unitsService = unitsService;

            this.InitializeCommands();
            this.InitializeProperties();
        }

        #region Other properties

        /// <summary>
        /// Gets or sets the navigation service of the moulding wizard.
        /// </summary>
        [Import]
        public IMouldingModuleNavigationService NavigationService { get; set; }

        /// <summary>
        /// Gets or sets the parent part.
        /// </summary>
        public Part ParentPart
        {
            get
            {
                return this.parentPart;
            }

            set
            {
                if (this.parentPart != value)
                {
                    this.parentPart = value;

                    this.SetMainMaterials();

                    // Calculate the part weight in grams.
                    var calculator = CostCalculatorFactory.GetCalculator(value.CalculationVariant);
                    this.PartWeight.Value = calculator.CalculatePartWeight(value) * 1000m;
                    this.PartsPerYear = value.YearlyProductionQuantity.GetValueOrDefault();

                    this.CalculateNumberOfCavities();
                    this.CalculateCycleTimes();
                }
            }
        }

        /// <summary>
        /// Gets or sets a list containing the created steps.
        /// </summary>
        public List<ProcessStep> CreatedSteps { get; set; }

        /// <summary>
        /// Gets or sets the part's data source manager.
        /// </summary>
        public IDataSourceManager PartDataSourceManager
        {
            get
            {
                return this.partDataSourceManager;
            }

            set
            {
                if (this.partDataSourceManager != value)
                {
                    this.partDataSourceManager = value;
                    this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(value);
                }
            }
        }

        /// <summary>
        /// Gets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter { get; private set; }

        #endregion Other properties

        #region Moulding type propeties

        /// <summary>
        /// Gets or sets the type of the material.
        /// </summary>
        public VMProperty<MouldingMaterialType> MaterialType { get; set; }

        /// <summary>
        /// Gets or sets the quality demand.
        /// </summary>
        public VMProperty<MouldingQualityDemand> QualityDemand { get; set; }

        /// <summary>
        /// Gets or sets the technology.
        /// </summary>
        public VMProperty<MouldingTechnology> Technology { get; set; }

        /// <summary>
        /// Gets or sets the slider.
        /// </summary>
        public VMProperty<MouldingSlider> Slider { get; set; }

        /// <summary>
        /// Gets or sets the number of inserted parts.
        /// </summary>
        public VMProperty<int?> NumberOfInsertedParts { get; set; }

        #endregion Moulding type propeties

        #region Basic values properties

        /// <summary>
        /// Gets or sets the parts per year.
        /// </summary>
        private decimal PartsPerYear { get; set; }

        /// <summary>
        /// Gets or sets the part weight.
        /// </summary>
        public VMProperty<decimal> PartWeight { get; set; }

        /// <summary>
        /// Gets or sets the type of the selected part.
        /// </summary>
        public VMProperty<MouldingPartType> SelectedPartType { get; set; }

        /// <summary>
        /// Gets or sets the projected max area.
        /// </summary>
        public VMProperty<decimal?> ProjectedMaxArea { get; set; }

        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        public VMProperty<decimal?> Height { get; set; }

        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        public VMProperty<decimal?> Width { get; set; }

        /// <summary>
        /// Gets or sets the length.
        /// </summary>
        public VMProperty<decimal?> Length { get; set; }

        /// <summary>
        /// Gets or sets the proposed number of cavities.
        /// </summary>
        public VMProperty<int?> ProposedNumberOfCavities { get; set; }

        /// <summary>
        /// Gets or sets the inputted number of cavities.
        /// </summary>
        public VMProperty<int?> InputtedNumberOfCavities { get; set; }

        /// <summary>
        /// Gets or sets the proposed ratio of sprue and fillers.
        /// </summary>
        public VMProperty<decimal?> ProposedRatioOfSprueAndFillers { get; set; }

        /// <summary>
        /// Gets or sets the inputted ratio of sprue and fillers.
        /// </summary>
        public VMProperty<decimal?> InputtedRatioOfSprueAndFillers { get; set; }

        /// <summary>
        /// Gets or sets the max wall thickness.
        /// </summary>
        public VMProperty<decimal?> MaxWallThickness { get; set; }

        /// <summary>
        /// Gets or sets the max wall thickness max limit.
        /// </summary>
        public VMProperty<decimal?> MaxWallThicknessMaxLimit { get; set; }

        /// <summary>
        /// Gets or sets the main materials.
        /// </summary>
        private Collection<MouldingMainMaterial> MainMaterials { get; set; }

        /// <summary>
        /// Gets or sets the matching materials.
        /// </summary>
        public Collection<MouldingMainMaterial> MatchingMaterials { get; set; }

        /// <summary>
        /// Gets or sets the selected material.
        /// </summary>
        public VMProperty<MouldingMainMaterial> SelectedMaterial { get; set; }

        /// <summary>
        /// Gets or sets the fibres.
        /// </summary>
        public Collection<MouldingFibre> Fibres { get; set; }

        /// <summary>
        /// Gets or sets the selected fibre.
        /// </summary>
        public VMProperty<MouldingFibre> SelectedFibre { get; set; }

        /// <summary>
        /// Gets or sets the filler percentage.
        /// </summary>
        public VMProperty<decimal?> FillerPercentage { get; set; }

        /// <summary>
        /// Gets or sets the proposed inertial mould pressure.
        /// </summary>
        public VMProperty<decimal?> ProposedInertialMouldPressure { get; set; }

        /// <summary>
        /// Gets or sets the inputted inertial mould pressure.
        /// </summary>
        public VMProperty<decimal?> InputtedInertialMouldPressure { get; set; }

        #endregion Basic values properties

        #region Resulting process properties

        /// <summary>
        /// Gets or sets the duroplastics factors.
        /// </summary>
        private MouldingResultingProcessFactors DuroplasticsFactors { get; set; }

        /// <summary>
        /// Gets or sets the elastomers factors.
        /// </summary>
        private MouldingResultingProcessFactors ElastomersFactors { get; set; }

        /// <summary>
        /// Gets or sets the proposed necessary machine force.
        /// </summary>
        public VMProperty<decimal?> ProposedNecessaryMachineForce { get; set; }

        /// <summary>
        /// Gets or sets the inputted necessary machine force.
        /// </summary>
        public VMProperty<decimal?> InputtedNecessaryMachineForce { get; set; }

        /// <summary>
        /// Gets or sets the proposed die temperature.
        /// </summary>
        public VMProperty<decimal?> ProposedDieTemperature { get; set; }

        /// <summary>
        /// Gets or sets the inputted die temperature.
        /// </summary>
        public VMProperty<decimal?> InputtedDieTemperature { get; set; }

        /// <summary>
        /// Gets or sets the proposed place insert part time.
        /// </summary>
        public VMProperty<decimal?> ProposedPlaceInsertPartTime { get; set; }

        /// <summary>
        /// Gets or sets the inputted place insert part time.
        /// </summary>
        public VMProperty<decimal?> InputtedPlaceInsertPartTime { get; set; }

        /// <summary>
        /// Gets or sets the proposed close moulding die time.
        /// </summary>
        public VMProperty<decimal?> ProposedCloseMouldingDieTime { get; set; }

        /// <summary>
        /// Gets or sets the inputted close moulding die time.
        /// </summary>
        public VMProperty<decimal?> InputtedCloseMouldingDieTime { get; set; }

        /// <summary>
        /// Gets or sets the proposed close slider time.
        /// </summary>
        public VMProperty<decimal?> ProposedCloseSliderTime { get; set; }

        /// <summary>
        /// Gets or sets the inputted close slider time.
        /// </summary>
        public VMProperty<decimal?> InputtedCloseSliderTime { get; set; }

        /// <summary>
        /// Gets or sets the proposed inject mould time.
        /// </summary>
        public VMProperty<decimal?> ProposedInjectMouldTime { get; set; }

        /// <summary>
        /// Gets or sets the inputted inject mould time.
        /// </summary>
        public VMProperty<decimal?> InputtedInjectMouldTime { get; set; }

        /// <summary>
        /// Gets or sets the proposed cooling time.
        /// </summary>
        public VMProperty<decimal?> ProposedCoolingTime { get; set; }

        /// <summary>
        /// Gets or sets the inputted cooling time.
        /// </summary>
        public VMProperty<decimal?> InputtedCoolingTime { get; set; }

        /// <summary>
        /// Gets or sets the proposed pull slider time.
        /// </summary>
        public VMProperty<decimal?> ProposedPullSliderTime { get; set; }

        /// <summary>
        /// Gets or sets the inputted pull slider time.
        /// </summary>
        public VMProperty<decimal?> InputtedPullSliderTime { get; set; }

        /// <summary>
        /// Gets or sets the proposed open moulding die time.
        /// </summary>
        public VMProperty<decimal?> ProposedOpenMouldingDieTime { get; set; }

        /// <summary>
        /// Gets or sets the inputted open moulding die time.
        /// </summary>
        public VMProperty<decimal?> InputtedOpenMouldingDieTime { get; set; }

        /// <summary>
        /// Gets or sets the proposed pick parts time.
        /// </summary>
        public VMProperty<decimal?> ProposedPickPartsTime { get; set; }

        /// <summary>
        /// Gets or sets the inputted pick parts time.
        /// </summary>
        public VMProperty<decimal?> InputtedPickPartsTime { get; set; }

        /// <summary>
        /// Gets or sets the proposed resulting cycle time.
        /// </summary>
        public VMProperty<decimal?> ProposedResultingCycleTime { get; set; }

        /// <summary>
        /// Gets or sets the inputted resulting cycle time.
        /// </summary>
        public VMProperty<decimal?> InputtedResultingCycleTime { get; set; }

        #endregion Resulting process properties

        #region Commands

        /// <summary>
        /// Gets the command that advances the UI to the next step of the moulding module.
        /// </summary>
        public ICommand NextStepCommand { get; private set; }

        /// <summary>
        /// Gets the command that reverts the UI to the previous step of the moulding module.
        /// </summary>
        public ICommand PreviousStepCommand { get; private set; }

        /// <summary>
        /// Gets the command that finalizes the moulding wizard.
        /// </summary>
        public ICommand FinishCommand { get; private set; }

        /// <summary>
        /// Gets the command executed when material type is selected.
        /// </summary>
        public ICommand MaterialTypeSelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command executed when quality demand is selected.
        /// </summary>
        public ICommand QualityDemandSelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command executed when technology is selected.
        /// </summary>
        public ICommand TechnologySelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command executed when slider is selected.
        /// </summary>
        public ICommand SliderSelectedCommand { get; private set; }

        #endregion Commands

        #region Initializations

        /// <summary>
        /// Initializes the commands.
        /// </summary>
        private void InitializeCommands()
        {
            this.NextStepCommand = new DelegateCommand(this.GoToNextMouldingStep);
            this.PreviousStepCommand = new DelegateCommand(this.GoToPreviousMouldingStep);
            this.FinishCommand = new DelegateCommand(this.Finish);

            this.MaterialTypeSelectedCommand = new DelegateCommand<MouldingMaterialType>((type) => this.MaterialType.Value = type);
            this.QualityDemandSelectedCommand = new DelegateCommand<MouldingQualityDemand>((type) => this.QualityDemand.Value = type);
            this.TechnologySelectedCommand = new DelegateCommand<MouldingTechnology>((type) => this.Technology.Value = type);
            this.SliderSelectedCommand = new DelegateCommand<MouldingSlider>((type) => this.Slider.Value = type);
        }

        /// <summary>
        /// Initializes the properties of this instance.
        /// </summary>
        private void InitializeProperties()
        {
            this.NumberOfInsertedParts.Value = DefaultNumberOfInsertedParts;
            this.ProposedNecessaryMachineForce.Value = 0m;

            this.SelectedPartType.Value = MouldingPartType.PlateType;

            this.Slider.Value = MouldingSlider.None;

            this.SetFibres();
            this.SetMouldingProcessValuesFactors();

            this.NumberOfInsertedParts.ValueChanged += (s, e) => this.CalculateCycleTimes();
            this.ProposedNumberOfCavities.ValueChanged += (s, e) => this.CalculateRatioOfSprueAndFillers();
            this.InputtedNumberOfCavities.ValueChanged += (s, e) => this.CalculateRatioOfSprueAndFillers();
            this.ProjectedMaxArea.ValueChanged += (s, e) => this.CalculateNecessaryMachineForce();
            this.SelectedMaterial.ValueChanged += (s, e) => this.OnSelectedMaterialValueChanged();
            this.Technology.ValueChanged += (s, e) => this.OnSelectedTechnologyValueChanged();
            this.Slider.ValueChanged += (s, e) => this.CalculateCycleTimes();
            this.MaxWallThickness.ValueChanged += (s, e) => this.CalculateCycleTimes();
            this.PartWeight.ValueChanged += (s, e) => this.OnPartWeightValueChanged();
            this.SelectedPartType.ValueChanged += (s, e) => this.CalculateCycleTimes();
            this.MaterialType.ValueChanged += (s, e) => this.CalculateCycleTimes();
            this.InputtedNecessaryMachineForce.ValueChanged += (s, e) => this.CalculateCycleTimes();
            this.InputtedInertialMouldPressure.ValueChanged += (s, e) => this.CalculateNecessaryMachineForce();
            this.InputtedPlaceInsertPartTime.ValueChanged += (s, e) => this.CalculateCycleTimes();
            this.InputtedCloseMouldingDieTime.ValueChanged += (s, e) => this.CalculateCycleTimes();
            this.InputtedCloseSliderTime.ValueChanged += (s, e) => this.CalculateCycleTimes();
            this.InputtedInjectMouldTime.ValueChanged += (s, e) => this.CalculateCycleTimes();
            this.InputtedCoolingTime.ValueChanged += (s, e) => this.CalculateCycleTimes();
            this.InputtedPullSliderTime.ValueChanged += (s, e) => this.CalculateCycleTimes();
            this.InputtedOpenMouldingDieTime.ValueChanged += (s, e) => this.CalculateCycleTimes();
            this.InputtedPickPartsTime.ValueChanged += (s, e) => this.CalculateCycleTimes();

            this.CreatedSteps = new List<ProcessStep>();
        }

        #endregion Initializations

        #region Property change handlers

        /// <summary>
        /// Called when the SelectedMaterial property has changed.
        /// </summary>
        private void OnSelectedMaterialValueChanged()
        {
            this.CalculateInertialMouldPressure();
            this.CalculateNecessaryMachineForce();
            this.CalculateDieTemperature();
            this.CalculateCycleTimes();
        }

        /// <summary>
        /// Called when the Technology property has changed.
        /// </summary>
        private void OnSelectedTechnologyValueChanged()
        {
            this.CalculateRatioOfSprueAndFillers();
            this.CalculateCycleTimes();
        }

        /// <summary>
        /// Called when the PartWeight property has changed.
        /// </summary>
        private void OnPartWeightValueChanged()
        {
            this.SetMaxThicknessValueMaxLimit();
            this.CalculateCycleTimes();
        }

        #endregion Property change handlers

        #region Navigation

        /// <summary>
        /// Displays the view corresponding to the next moulding step.
        /// </summary>
        private void GoToNextMouldingStep()
        {
            this.NavigationService.GoToNextStep();
        }

        /// <summary>
        /// Displays the view corresponding to the previous moulding step.
        /// </summary>
        private void GoToPreviousMouldingStep()
        {
            this.NavigationService.GoToPreviousStep();
        }

        /// <summary>
        /// Finishes the moulding wizard.
        /// </summary>
        private void Finish()
        {
            this.CreateProcessSteps();
            this.NavigationService.CloseWizard();
        }

        #endregion Navigation

        #region Knowledge Base

        /// <summary>
        /// Gets the number of cavities based on cavity table.
        /// </summary>
        /// <param name="calculatedNumber">The calculated number.</param>
        /// <returns>Number of recommended cavities.</returns>
        private int GetNumberOfCavitiesBasedOnCavityTable(int calculatedNumber)
        {
            if (calculatedNumber < 2)
            {
                return 1;
            }
            else if (calculatedNumber < 4)
            {
                return 2;
            }
            else if (calculatedNumber < 9)
            {
                return 4;
            }
            else if (calculatedNumber < 16)
            {
                return 9;
            }
            else if (calculatedNumber < 25)
            {
                return 16;
            }
            else if (calculatedNumber < 36)
            {
                return 25;
            }
            else if (calculatedNumber < 49)
            {
                return 36;
            }
            else if (calculatedNumber < 64)
            {
                return 49;
            }
            else if (calculatedNumber < 81)
            {
                return 64;
            }
            else if (calculatedNumber < 100)
            {
                return 81;
            }
            else
            {
                return 100;
            }
        }

        /// <summary>
        /// Gets the dry run time based on the machine force range.
        /// </summary>
        /// <returns>Corresponding decimal value for the dry run time.</returns>
        private decimal GetDryRunTimeOnMachineForceRange()
        {
            var necessaryMachineForce = this.InputtedNecessaryMachineForce.Value.HasValue ? this.InputtedNecessaryMachineForce.Value.Value : this.ProposedNecessaryMachineForce.Value.GetValueOrDefault();

            if (this.MaterialType.Value == MouldingMaterialType.Thermoplastics)
            {
                if (this.Technology.Value == MouldingTechnology.Hotrunner)
                {
                    // High Runner Thermoplastic Injection Moulding.
                    if (necessaryMachineForce < 250)
                    {
                        return 0.8m;
                    }
                    else if (necessaryMachineForce < 500)
                    {
                        return 1.2m;
                    }
                    else if (necessaryMachineForce < 1000)
                    {
                        return 1.5m;
                    }
                    else if (necessaryMachineForce < 5000)
                    {
                        return 3m;
                    }
                    else if (necessaryMachineForce < 10000)
                    {
                        return 5m;
                    }
                    else
                    {
                        return 9m;
                    }
                }
                else
                {
                    // Standard Thermoplastic Injection Moulding.
                    if (necessaryMachineForce < 250)
                    {
                        return 1.5m;
                    }
                    else if (necessaryMachineForce < 500)
                    {
                        return 1.7m;
                    }
                    else if (necessaryMachineForce < 1000)
                    {
                        return 2.5m;
                    }
                    else if (necessaryMachineForce < 5000)
                    {
                        return 4m;
                    }
                    else if (necessaryMachineForce < 10000)
                    {
                        return 7m;
                    }
                    else
                    {
                        return 12m;
                    }
                }
            }
            else
            {
                // Elastomer or Duroplast Injection Moulding.
                if (necessaryMachineForce < 250)
                {
                    return 2.1m;
                }
                else if (necessaryMachineForce < 500)
                {
                    return 2.5m;
                }
                else if (necessaryMachineForce < 1000)
                {
                    return 3.4m;
                }
                else if (necessaryMachineForce < 5000)
                {
                    return 8m;
                }
                else if (necessaryMachineForce < 10000)
                {
                    return 11m;
                }
                else
                {
                    return 14m;
                }
            }
        }

        /// <summary>
        /// Returns true or false if a material is a polymer or not.
        /// </summary>
        /// <param name="material">The material.</param>
        /// <returns> Return true if the specified material is polymer.</returns>
        private bool IsPolymer(RawMaterial material)
        {
            if (material.MaterialsClassificationL1 != null && material.MaterialsClassificationL1.Guid == new Guid("f32882ee-c299-4991-9d6c-27449367aae6"))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Sets all main materials in the KB.
        /// </summary>
        private void SetMainMaterials()
        {
            var materialList = new Collection<MouldingMainMaterial>();

            var materialPCHT = new MouldingMainMaterial();
            materialPCHT.Type = "PC-HT";
            materialPCHT.MeltingTemperature = 105;
            materialPCHT.EffectiveThermalDiffusity = 0.08m;
            materialPCHT.MouldTemperature = 240;
            materialPCHT.MouldReleaseTemperature = 150;
            materialPCHT.RecommendedDieTemperature = 50;
            materialPCHT.DefaultPressure = 400;
            materialPCHT.MinPressure = 300;
            materialPCHT.MaxPressure = 500;
            materialList.Add(materialPCHT);

            var materialPCABS = new MouldingMainMaterial();
            materialPCABS.Type = "ABS+PC";
            materialPCABS.MeltingTemperature = 105;
            materialPCABS.EffectiveThermalDiffusity = 0.08m;
            materialPCABS.MouldTemperature = 240;
            materialPCABS.MouldReleaseTemperature = 110;
            materialPCABS.RecommendedDieTemperature = 50;
            materialPCABS.DefaultPressure = 325;
            materialPCABS.MinPressure = 250;
            materialPCABS.MaxPressure = 400;
            materialList.Add(materialPCABS);

            var materialTPU = new MouldingMainMaterial();
            materialTPU.Type = "TPU";
            materialTPU.MeltingTemperature = 105;
            materialTPU.EffectiveThermalDiffusity = 0.08m;
            materialTPU.MouldTemperature = 240;
            materialTPU.MouldReleaseTemperature = 90;
            materialTPU.RecommendedDieTemperature = 50;
            materialTPU.DefaultPressure = 500;
            materialTPU.MinPressure = 300;
            materialTPU.MaxPressure = 700;
            materialList.Add(materialTPU);

            var materialPA = new MouldingMainMaterial();
            materialPA.Type = "PA";
            materialPA.MeltingTemperature = 105;
            materialPA.EffectiveThermalDiffusity = 0.08m;
            materialPA.MouldTemperature = 240;
            materialPA.MouldReleaseTemperature = 100;
            materialPA.RecommendedDieTemperature = 50;
            materialPA.DefaultPressure = 525;
            materialPA.MinPressure = 350;
            materialPA.MaxPressure = 700;
            materialList.Add(materialPA);

            var materialABS = new MouldingMainMaterial();
            materialABS.Type = "ABS";
            materialABS.MeltingTemperature = 105;
            materialABS.EffectiveThermalDiffusity = 0.08m;
            materialABS.MouldTemperature = 240;
            materialABS.MouldReleaseTemperature = 90;
            materialABS.RecommendedDieTemperature = 50;
            materialABS.DefaultPressure = 300;
            materialABS.MinPressure = 250;
            materialABS.MaxPressure = 350;
            materialList.Add(materialABS);

            var materialSAN = new MouldingMainMaterial();
            materialSAN.Type = "SAN";
            materialSAN.MeltingTemperature = 105;
            materialSAN.EffectiveThermalDiffusity = 0.08m;
            materialSAN.MouldTemperature = 240;
            materialSAN.MouldReleaseTemperature = 90;
            materialSAN.RecommendedDieTemperature = 50;
            materialSAN.DefaultPressure = 300;
            materialSAN.MinPressure = 250;
            materialSAN.MaxPressure = 350;
            materialList.Add(materialSAN);

            var materialPC = new MouldingMainMaterial();
            materialPC.Type = "PC";
            materialPC.MeltingTemperature = 105;
            materialPC.EffectiveThermalDiffusity = 0.08m;
            materialPC.MouldTemperature = 240;
            materialPC.MouldReleaseTemperature = 130;
            materialPC.RecommendedDieTemperature = 50;
            materialPC.DefaultPressure = 400;
            materialPC.MinPressure = 300;
            materialPC.MaxPressure = 500;
            materialList.Add(materialPC);

            var materialPBT = new MouldingMainMaterial();
            materialPBT.Type = "PBT";
            materialPBT.MeltingTemperature = 105;
            materialPBT.EffectiveThermalDiffusity = 0.08m;
            materialPBT.MouldTemperature = 240;
            materialPBT.MouldReleaseTemperature = 130;
            materialPBT.RecommendedDieTemperature = 50;
            materialPBT.DefaultPressure = 475;
            materialPBT.MinPressure = 250;
            materialPBT.MaxPressure = 700;
            materialList.Add(materialPBT);

            var materialABSPA = new MouldingMainMaterial();
            materialABSPA.Type = "ABS+PA";
            materialABSPA.MeltingTemperature = 105;
            materialABSPA.EffectiveThermalDiffusity = 0.08m;
            materialABSPA.MouldTemperature = 240;
            materialABSPA.MouldReleaseTemperature = 90;
            materialABSPA.RecommendedDieTemperature = 50;
            materialABSPA.DefaultPressure = 375;
            materialABSPA.MinPressure = 250;
            materialABSPA.MaxPressure = 500;
            materialList.Add(materialABSPA);

            this.MainMaterials = materialList;
            this.MatchingMaterials = new Collection<MouldingMainMaterial>();

            var mainMaterialNames = materialList.Select(p => p.Type);

            foreach (var material in this.ParentPart.RawMaterials.Where(p => !p.IsDeleted))
            {
                if (this.IsPolymer(material))
                {
                    if (mainMaterialNames.Contains(material.Name))
                    {
                        var mainMaterial = materialList.Where(p => p.Type == material.Name).FirstOrDefault();
                        this.MatchingMaterials.Add(mainMaterial);
                    }
                    else
                    {
                        string[] splitName = material.Name.Split('+');
                        if (splitName.Length > 1)
                        {
                            for (int i = 0; i < splitName.Length; i++)
                            {
                                if (mainMaterialNames.Contains(splitName[i]))
                                {
                                    var mainMaterial = materialList.Where(p => p.Type == splitName[i]).FirstOrDefault();
                                    if (!this.MatchingMaterials.Contains(mainMaterial))
                                    {
                                        this.MatchingMaterials.Add(mainMaterial);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (this.MatchingMaterials.Count == 0)
            {
                this.MatchingMaterials = this.MainMaterials;
            }
        }

        /// <summary>
        /// Sets the fibres in the KB.
        /// </summary>
        private void SetFibres()
        {
            var fibreList = new Collection<MouldingFibre>();
            var fibreGlass = new MouldingFibre();
            fibreGlass.Type = "Glass";
            fibreList.Add(fibreGlass);

            this.Fibres = fibreList;
        }

        /// <summary>
        /// Sets the moulding process values factors in the KB.
        /// </summary>
        private void SetMouldingProcessValuesFactors()
        {
            this.DuroplasticsFactors = new MouldingResultingProcessFactors();
            this.DuroplasticsFactors.PlaceInsertPartsFactor = 1;
            this.DuroplasticsFactors.CloseMouldingDieFactor = 1;
            this.DuroplasticsFactors.CloseSliderFactor = 1;
            this.DuroplasticsFactors.InjectMouldFactor = 2;
            this.DuroplasticsFactors.CoolingFactor = 1.2m;
            this.DuroplasticsFactors.PullSliderFactor = 1;
            this.DuroplasticsFactors.OpenMouldingDieFactor = 1;
            this.DuroplasticsFactors.PickPartsFactor = 1;

            this.ElastomersFactors = new MouldingResultingProcessFactors();
            this.ElastomersFactors.PlaceInsertPartsFactor = 1;
            this.ElastomersFactors.CloseMouldingDieFactor = 1;
            this.ElastomersFactors.CloseSliderFactor = 1;
            this.ElastomersFactors.InjectMouldFactor = 3;
            this.ElastomersFactors.CoolingFactor = 2;
            this.ElastomersFactors.PullSliderFactor = 1;
            this.ElastomersFactors.OpenMouldingDieFactor = 1;
            this.ElastomersFactors.PickPartsFactor = 1;
        }

        #endregion

        #region Calculations

        /// <summary>
        /// Calculates the number of cavities based on the cavity table.
        /// </summary>
        private void CalculateNumberOfCavities()
        {
            // Number of cavities = ROUNDUP(1 / (partWeight / partsPerYear * 1000))
            // Proposed number based on cavity table, see method above.
            if (this.PartsPerYear != 0)
            {
                decimal ratio = this.PartWeight.Value / this.PartsPerYear;
                if (ratio != 0)
                {
                    decimal calculatedNumberDecimal = 1 / (ratio * 1000m);
                    int calculatedNumber = (int)Math.Ceiling(calculatedNumberDecimal);
                    this.ProposedNumberOfCavities.Value = this.GetNumberOfCavitiesBasedOnCavityTable(calculatedNumber);
                }
                else
                {
                    this.ProposedNumberOfCavities.Value = 0;
                }
            }
            else
            {
                this.ProposedNumberOfCavities.Value = 0;
            }
        }

        /// <summary>
        /// Calculates the ratio of sprue and fillers.
        /// </summary>
        private void CalculateRatioOfSprueAndFillers()
        {
            var numberOfCavities = this.InputtedNumberOfCavities.Value.HasValue ? this.InputtedNumberOfCavities.Value.Value : this.ProposedNumberOfCavities.Value.GetValueOrDefault();

            // If Technology = Hotrunner => 5%.
            // Else 15 / (1 + number of cavities / 100).
            if (this.Technology.Value == MouldingTechnology.Hotrunner)
            {
                this.ProposedRatioOfSprueAndFillers.Value = 0.05m;
            }
            else if (numberOfCavities != 0)
            {
                this.ProposedRatioOfSprueAndFillers.Value = (15 / (1 + (numberOfCavities / 100.0m))) / 100.0m; // Divide by 100 to obtain the base value for percentage.
            }
            else
            {
                this.ProposedRatioOfSprueAndFillers.Value = 0;
            }
        }

        /// <summary>
        /// Calculates the inertial mould pressure.
        /// </summary>
        private void CalculateInertialMouldPressure()
        {
            if (this.SelectedMaterial.Value != null)
            {
                this.ProposedInertialMouldPressure.Value = this.SelectedMaterial.Value.DefaultPressure;
            }
            else
            {
                this.ProposedInertialMouldPressure.Value = 0;
            }
        }

        /// <summary>
        /// Calculates the necessary machine force.
        /// </summary>
        private void CalculateNecessaryMachineForce()
        {
            var inertialMouldPressure = this.InputtedInertialMouldPressure.Value.HasValue ? this.InputtedInertialMouldPressure.Value.Value : this.ProposedInertialMouldPressure.Value.GetValueOrDefault();
            this.ProposedNecessaryMachineForce.Value = (this.ProjectedMaxArea.Value.GetValueOrDefault() * inertialMouldPressure) / 1000m;
        }

        /// <summary>
        /// Calculates the die temperature.
        /// </summary>
        private void CalculateDieTemperature()
        {
            if (this.SelectedMaterial.Value != null)
            {
                this.ProposedDieTemperature.Value = this.SelectedMaterial.Value.RecommendedDieTemperature;
            }
        }

        /// <summary>
        /// Calculates the cycle times.
        /// </summary>
        private void CalculateCycleTimes()
        {
            // Place insert parts.
            if (this.Technology.Value == MouldingTechnology.InsertedParts)
            {
                this.ProposedPlaceInsertPartTime.Value = 3m * this.NumberOfInsertedParts.Value.GetValueOrDefault();
            }
            else
            {
                this.ProposedPlaceInsertPartTime.Value = 9m;
            }

            // Close moulding die.
            this.ProposedCloseMouldingDieTime.Value = this.GetDryRunTimeOnMachineForceRange() * 0.5m;

            // Close slider.
            if (this.Slider.Value == MouldingSlider.HydraulicSlider)
            {
                this.ProposedCloseSliderTime.Value = 2;
            }
            else if (this.Slider.Value == MouldingSlider.MechanicalSlider)
            {
                this.ProposedCloseSliderTime.Value = 0.5m;
            }
            else
            {
                this.ProposedCloseSliderTime.Value = 0;
            }

            // Inject mould.
            try
            {
                double result = 0.2272 * Math.Exp(Convert.ToDouble((this.MaxWallThickness.Value.GetValueOrDefault() / 1000m) * this.PartWeight.Value));
                this.ProposedInjectMouldTime.Value = Math.Round((decimal)result, 2);
            }
            catch (OverflowException)
            {
                this.windowService.MessageDialogService.Show(LocalizedResources.Moulding_InjectionMouldTooLarge, MessageDialogType.Info);
                this.ProposedInjectMouldTime.Value = 0;
            }

            // Cooling.
            if (this.SelectedMaterial.Value != null)
            {
                decimal effectiveThermalDiffusity = this.SelectedMaterial.Value.EffectiveThermalDiffusity;
                decimal mouldTemperature = this.SelectedMaterial.Value.MouldTemperature;
                decimal mouldReleaseTemperature = this.SelectedMaterial.Value.MouldReleaseTemperature;
                decimal mouldRecommendedTemperature = this.SelectedMaterial.Value.RecommendedDieTemperature;
                decimal squarePI = (decimal)(Math.PI * Math.PI);
                decimal? firstPart = 0;
                decimal? secondPart = 0;
                if (this.SelectedPartType.Value == MouldingPartType.PlateType)
                {
                    if (effectiveThermalDiffusity != 0 && mouldReleaseTemperature != mouldRecommendedTemperature)
                    {
                        firstPart = this.MaxWallThickness.Value.GetValueOrDefault() / (squarePI * effectiveThermalDiffusity);
                        secondPart = (8m / squarePI) * ((mouldTemperature - mouldRecommendedTemperature) / (mouldReleaseTemperature - mouldRecommendedTemperature));
                        secondPart = (decimal)Math.Log((double)secondPart);
                    }
                }
                else
                {
                    if (effectiveThermalDiffusity != 0 && mouldReleaseTemperature != mouldRecommendedTemperature)
                    {
                        firstPart = this.MaxWallThickness.Value.GetValueOrDefault() / (23.14m * effectiveThermalDiffusity);
                        secondPart = 0.692m * ((mouldTemperature - mouldRecommendedTemperature) / (mouldReleaseTemperature - mouldRecommendedTemperature));
                        secondPart = (decimal)Math.Log((double)secondPart);
                    }
                }

                this.ProposedCoolingTime.Value = Math.Round(firstPart.Value * secondPart.Value, 2);
            }
            else
            {
                this.ProposedCoolingTime.Value = 0;
            }

            // Pull slider.
            if (this.Slider.Value == MouldingSlider.HydraulicSlider)
            {
                this.ProposedPullSliderTime.Value = 2;
            }
            else if (this.Slider.Value == MouldingSlider.MechanicalSlider)
            {
                this.ProposedPullSliderTime.Value = 0.5m;
            }
            else
            {
                this.ProposedPullSliderTime.Value = 0;
            }

            // Open moulding die.
            this.ProposedOpenMouldingDieTime.Value = this.GetDryRunTimeOnMachineForceRange() * 0.5m;

            // Pick parts.
            this.ProposedPickPartsTime.Value = Math.Round(5 + (this.PartWeight.Value / 250.0m), 2);

            // Applying factors.
            this.ApplyFactors();

            // Resulting cycle time.
            var placeInsertPartTime = this.InputtedPlaceInsertPartTime.Value.HasValue ? this.InputtedPlaceInsertPartTime.Value.Value : this.ProposedPlaceInsertPartTime.Value.GetValueOrDefault();
            var closeMouldingDieTime = this.InputtedCloseMouldingDieTime.Value.HasValue ? this.InputtedCloseMouldingDieTime.Value.Value : this.ProposedCloseMouldingDieTime.Value.GetValueOrDefault();
            var closeSliderTime = this.InputtedCloseSliderTime.Value.HasValue ? this.InputtedCloseSliderTime.Value.Value : this.ProposedCloseSliderTime.Value.GetValueOrDefault();
            var injectMouldTime = this.InputtedInjectMouldTime.Value.HasValue ? this.InputtedInjectMouldTime.Value.Value : this.ProposedInjectMouldTime.Value.GetValueOrDefault();
            var coolingTime = this.InputtedCoolingTime.Value.HasValue ? this.InputtedCoolingTime.Value.Value : this.ProposedCoolingTime.Value.GetValueOrDefault();
            var pullSliderTime = this.InputtedPullSliderTime.Value.HasValue ? this.InputtedPullSliderTime.Value.Value : this.ProposedPullSliderTime.Value.GetValueOrDefault();
            var openMouldingDieTime = this.InputtedOpenMouldingDieTime.Value.HasValue ? this.InputtedOpenMouldingDieTime.Value.Value : this.ProposedOpenMouldingDieTime.Value.GetValueOrDefault();
            var pickPartsTime = this.InputtedPickPartsTime.Value.HasValue ? this.InputtedPickPartsTime.Value.Value : this.ProposedPickPartsTime.Value.GetValueOrDefault();

            this.ProposedResultingCycleTime.Value = placeInsertPartTime + closeMouldingDieTime + closeSliderTime + injectMouldTime + coolingTime + pullSliderTime + openMouldingDieTime + pickPartsTime;
        }

        /// <summary>
        /// Applies the factors (for duroplastics and elastomers).
        /// </summary>
        private void ApplyFactors()
        {
            if (this.MaterialType.Value == MouldingMaterialType.Duroplastics)
            {
                this.ProposedPlaceInsertPartTime.Value = this.ProposedPlaceInsertPartTime.Value * this.DuroplasticsFactors.PlaceInsertPartsFactor;
                this.ProposedCloseMouldingDieTime.Value = this.ProposedCloseMouldingDieTime.Value * this.DuroplasticsFactors.CloseMouldingDieFactor;
                this.ProposedCloseSliderTime.Value = this.ProposedCloseSliderTime.Value * this.DuroplasticsFactors.CloseSliderFactor;
                this.ProposedInjectMouldTime.Value = this.ProposedInjectMouldTime.Value * this.DuroplasticsFactors.InjectMouldFactor;
                this.ProposedCoolingTime.Value = this.ProposedCoolingTime.Value * this.DuroplasticsFactors.CoolingFactor;
                this.ProposedPullSliderTime.Value = this.ProposedPullSliderTime.Value * this.DuroplasticsFactors.PullSliderFactor;
                this.ProposedOpenMouldingDieTime.Value = this.ProposedOpenMouldingDieTime.Value * this.DuroplasticsFactors.OpenMouldingDieFactor;
                this.ProposedPickPartsTime.Value = this.ProposedPickPartsTime.Value * this.DuroplasticsFactors.PickPartsFactor;
            }
            else if (this.MaterialType.Value == MouldingMaterialType.ThermoplasticElastomers)
            {
                this.ProposedPlaceInsertPartTime.Value = this.ProposedPlaceInsertPartTime.Value * this.ElastomersFactors.PlaceInsertPartsFactor;
                this.ProposedCloseMouldingDieTime.Value = this.ProposedCloseMouldingDieTime.Value * this.ElastomersFactors.CloseMouldingDieFactor;
                this.ProposedCloseSliderTime.Value = this.ProposedCloseSliderTime.Value * this.ElastomersFactors.CloseSliderFactor;
                this.ProposedInjectMouldTime.Value = this.ProposedInjectMouldTime.Value * this.ElastomersFactors.InjectMouldFactor;
                this.ProposedCoolingTime.Value = this.ProposedCoolingTime.Value * this.ElastomersFactors.CoolingFactor;
                this.ProposedPullSliderTime.Value = this.ProposedPullSliderTime.Value * this.ElastomersFactors.PullSliderFactor;
                this.ProposedOpenMouldingDieTime.Value = this.ProposedOpenMouldingDieTime.Value * this.ElastomersFactors.OpenMouldingDieFactor;
                this.ProposedPickPartsTime.Value = this.ProposedPickPartsTime.Value * this.ElastomersFactors.PickPartsFactor;
            }
            else
            {
                // Thermoplastics.
                return;
            }
        }

        /// <summary>
        /// Sets the value of the MaxWallThicknessMaxLimit. This is due to the exponential calculus and the decimal max value imposed.
        /// </summary>
        private void SetMaxThicknessValueMaxLimit()
        {
            double result = (Math.Log(999999999 / 0.2272) / (double)this.PartWeight.Value) * 1000;
            if ((result >= 0 && result < 1)
                || this.PartWeight.Value == 0)
            {
                this.MaxWallThicknessMaxLimit.Value = 0;
            }
            else
            {
                this.MaxWallThicknessMaxLimit.Value = (int)result - 1;
            }
        }

        #endregion Calculations

        #region Machine selection

        /// <summary>
        /// Returns the id of the handling robot machine.
        /// </summary>
        /// <returns>The id of the handling robot machine.</returns>
        private Guid GetHandlingRobotMachineGuid()
        {
            var necessaryMachineForce = this.InputtedNecessaryMachineForce.Value.HasValue ? this.InputtedNecessaryMachineForce.Value.Value : this.ProposedNecessaryMachineForce.Value.GetValueOrDefault();

            if (necessaryMachineForce <= 900)
            {
                // W801.
                return new Guid("a950d120-1eaf-46a0-8a5d-1550e3f397a4");
            }
            else if (necessaryMachineForce <= 5000)
            {
                // W811.
                return new Guid("687ef2f6-500e-4b17-9fce-1a3b1c77983f");
            }
            else if (necessaryMachineForce <= 10000)
            {
                // W832.
                return new Guid("81c015e7-0005-47ff-987b-b9f9650418d5");
            }
            else
            {
                // W873.
                return new Guid("5c9ae89b-dd69-4679-82f9-278b266ad7b3");
            }
        }

        /// <summary>
        /// Returns the id of the injection moulding machine.
        /// </summary>
        /// <returns>The id of the injection moulding machine.</returns>
        private Guid GetInjectionMouldingMachineGuid()
        {
            var necessaryMachineForce = this.InputtedNecessaryMachineForce.Value.HasValue ? this.InputtedNecessaryMachineForce.Value.Value : this.ProposedNecessaryMachineForce.Value.GetValueOrDefault();

            if (necessaryMachineForce <= 350)
            {
                // Plus 35.
                return new Guid("281fe3d9-c38c-46d3-a0c9-694a09ed3f2e");
            }
            else if (necessaryMachineForce <= 450)
            {
                // HM 45/210.
                return new Guid("a291ce01-ee93-4323-82ec-d927b0f0d6c6");
            }
            else if (necessaryMachineForce <= 650)
            {
                // HM 65/350.
                return new Guid("3098c08e-c719-4d8e-8f70-8095352805d7");
            }
            else if (necessaryMachineForce <= 900)
            {
                // HM 90/525.
                return new Guid("5121f05b-f3c1-4982-a549-b51b5c2c07d2");
            }
            else if (necessaryMachineForce <= 1100)
            {
                // HM 110/750.
                return new Guid("6e113347-e43c-4e68-be7f-0e853013863f");
            }
            else if (necessaryMachineForce <= 1500)
            {
                // HM 150/1000.
                return new Guid("1340dd6c-54ee-4a69-b3a5-00e66a3e89cf");
            }
            else if (necessaryMachineForce <= 1800)
            {
                // HM 180/1330.
                return new Guid("f7232e55-f576-4f57-94a0-c37ad3eb9aa9");
            }
            else if (necessaryMachineForce <= 2400)
            {
                // HM 240/1330.
                return new Guid("320aa7c2-fb1f-431d-acad-3c137fa9e7ff");
            }
            else if (necessaryMachineForce <= 3000)
            {
                // HM 300/2250.
                return new Guid("54826845-28b9-4f4f-ace4-9af23f4a67da");
            }
            else if (necessaryMachineForce <= 4000)
            {
                // HM 400/5100.
                return new Guid("95034970-a2f2-4ec7-940f-76f8a37c74e5");
            }
            else if (necessaryMachineForce <= 5000)
            {
                // HM 500/5100.
                return new Guid("e434eb0e-f8f4-419f-a8b1-a644ff16f37d");
            }
            else if (necessaryMachineForce <= 6500)
            {
                // HM 600/8800.
                return new Guid("718c4bce-f1cd-42f2-b7e5-0024c6d61ce2");
            }
            else if (necessaryMachineForce <= 8000)
            {
                // Macro Power 800/8800.
                return new Guid("a4eb4845-3da7-484a-a7f4-378da8b9ff2b");
            }
            else if (necessaryMachineForce <= 10000)
            {
                // Macro Power 1000/13800.
                return new Guid("95391c31-3454-41f2-bddf-a992b3f7e4b2");
            }
            else if (necessaryMachineForce <= 11000)
            {
                // Duo 7050/1100.
                return new Guid("DDED9754-49CF-4A64-8991-DC1AA58B89EF");
            }
            else if (necessaryMachineForce <= 13000)
            {
                // Duo 11050/1300.
                return new Guid("13849273-1F77-489F-A1D8-130C084D833E");
            }
            else if (necessaryMachineForce <= 15000)
            {
                // Duo 11050/1500.
                return new Guid("9406AB2E-DB7A-4904-9C81-001D8D2D0971");
            }
            else if (necessaryMachineForce <= 17000)
            {
                // Duo 11050/1700.
                return new Guid("45DEABBC-41D5-4982-8744-6CAEC8E436B6");
            }
            else if (necessaryMachineForce <= 20000)
            {
                // Duo 16050/2000.
                return new Guid("22E304C5-F566-4C5C-A0F3-886696DA0331");
            }
            else if (necessaryMachineForce <= 23000)
            {
                // Duo 16050/2300.
                return new Guid("D4CEC17E-320B-44EB-AE52-604620143002");
            }
            else if (necessaryMachineForce <= 25000)
            {
                // Duo 23050/2500.
                return new Guid("41B43DB3-4B6D-4EE7-87D0-5C0F6EF7E75D");
            }
            else if (necessaryMachineForce <= 27000)
            {
                // Duo 23050/2700.
                return new Guid("6A9049F8-038E-42AB-B8BC-12C3F6BD6DD4");
            }
            else if (necessaryMachineForce <= 30000)
            {
                // Duo 23050/3000.
                return new Guid("5DCE7C07-3795-41C3-8E06-00C96995BF02");
            }
            else if (necessaryMachineForce <= 32000)
            {
                // Duo 23050/3200.
                return new Guid("0C4137FA-2903-4105-84A4-71DFBB8BEA34");
            }
            else if (necessaryMachineForce <= 36000)
            {
                // Duo 35050/3600.
                return new Guid("C8D15F05-9C63-4770-BE88-090627DED387");
            }
            else if (necessaryMachineForce <= 40000)
            {
                // Duo 35050/4000.
                return new Guid("39CB518F-7244-467A-9A5D-1CD371691C42");
            }
            else if (necessaryMachineForce <= 50000)
            {
                // Duo 45050/5000.
                return new Guid("B4CDF02F-EB0D-4C59-B617-75BA28A332E8");
            }
            else
            {
                // Duo 45050/5500.
                return new Guid("38ED39FF-D301-4123-8CEF-4C30745C49B9");
            }
        }

        /// <summary>
        /// Returns the id of the dryer machine.
        /// </summary>
        /// <returns>The id of the dryer machine.</returns>
        private Guid GetDryerMachineGuid()
        {
            var necessaryMachineForce = this.InputtedNecessaryMachineForce.Value.HasValue ? this.InputtedNecessaryMachineForce.Value.Value : this.ProposedNecessaryMachineForce.Value.GetValueOrDefault();

            if (necessaryMachineForce <= 900)
            {
                // Drymax ES40/50.
                return new Guid("a35073ab-0920-4961-8ac7-f8d4c4866b61");
            }
            else if (necessaryMachineForce <= 5000)
            {
                // Drymax ES60/150.
                return new Guid("2c0803a3-ca33-408e-818b-55c0fff7826f");
            }
            else if (necessaryMachineForce <= 10000)
            {
                // Drymax ES100/200.
                return new Guid("f9c08394-185c-48a2-a2ee-87c669900aaf");
            }
            else
            {
                // Drymax ES100/400.
                return new Guid("79b1e426-e8a4-4181-9f1d-a8887dc01071");
            }
        }

        /// <summary>
        /// Returns the id of the cooling machine.
        /// </summary>
        /// <returns>The id of the cooling machine.</returns>
        private Guid GetCoolerMachineGuid()
        {
            var necessaryMachineForce = this.InputtedNecessaryMachineForce.Value.HasValue ? this.InputtedNecessaryMachineForce.Value.Value : this.ProposedNecessaryMachineForce.Value.GetValueOrDefault();

            if (necessaryMachineForce <= 900)
            {
                // Coolmax 30.
                return new Guid("5e26679e-ce94-4a37-9894-eff05da74a9b");
            }
            else if (necessaryMachineForce <= 5000)
            {
                // Coolmax 70.
                return new Guid("45749b27-c23d-4858-a98e-454d074307a5");
            }
            else if (necessaryMachineForce <= 10000)
            {
                // Coolmax 100.
                return new Guid("337834cc-a780-4489-ab72-f7d7e1110c1c");
            }
            else
            {
                // Coolmax 250.
                return new Guid("823f1177-a5bc-41f1-8124-9f7efe3ae854");
            }
        }

        /// <summary>
        /// Returns the id of the blender/mixer machine.
        /// </summary>
        /// <returns>The id of the blender/mixer machine.</returns>
        private Guid GetBlenderMixerMachineGuid()
        {
            var necessaryMachineForce = this.InputtedNecessaryMachineForce.Value.HasValue ? this.InputtedNecessaryMachineForce.Value.Value : this.ProposedNecessaryMachineForce.Value.GetValueOrDefault();

            if (necessaryMachineForce <= 900)
            {
                // Id: 1332 Name: Wittmann Gravimax GMX 14V.
                return new Guid("00c6b885-1408-4e70-a48d-cc752dcea2f0");
            }
            else if (necessaryMachineForce <= 5000)
            {
                // Id: 1333 Name: Wittmann Gravimax GMX 94.
                return new Guid("f1e86b14-84ed-459e-a2eb-3845b51d8b72");
            }
            else if (necessaryMachineForce <= 10000)
            {
                // Id: 1334 Name: Wittmann Gravimax GMX 184.
                return new Guid("cd369caf-1d1d-41b9-8d46-a417fd6c1ffb");
            }
            else
            {
                // Id: 1335 Name: Wittmann Gravimax GMX 274.
                return new Guid("1fdf53ff-6db8-45af-8618-c0e51c077f42");
            }
        }

        /// <summary>
        /// Gets the machines from the database.
        /// </summary>
        /// <returns>A collection of machines.</returns>
        private Collection<Machine> GetMachinesByForce()
        {
            var machineGuids = new List<Guid>();
            machineGuids.Add(this.GetBlenderMixerMachineGuid());
            machineGuids.Add(this.GetCoolerMachineGuid());
            machineGuids.Add(this.GetDryerMachineGuid());
            machineGuids.Add(this.GetInjectionMouldingMachineGuid());
            machineGuids.Add(this.GetHandlingRobotMachineGuid());

            var masterMachines = new Collection<Machine>();

            // If the central server is online load the machines from there.
            // Else load the machines from the local db.
            if (this.onlineChecker.IsOnline)
            {
                this.machinesDataSourceManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
                masterMachines = this.machinesDataSourceManager.MachineRepository.GetMasterDataMachines(machineGuids);
            }
            else
            {
                this.machinesDataSourceManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
                masterMachines = this.machinesDataSourceManager.MachineRepository.GetMasterDataMachines(machineGuids);
            }

            return masterMachines;
        }

        #endregion Machine selection

        #region Create the casting process

        /// <summary>
        /// Creates the process steps which will be added in the current process.
        /// </summary>
        private void CreateProcessSteps()
        {
            var resultingCycleTime = this.InputtedResultingCycleTime.Value.HasValue ? this.InputtedResultingCycleTime.Value.Value : this.ProposedResultingCycleTime.Value.GetValueOrDefault();

            // Material Conditioning (Mixing & Drying).
            var materialConditioningStep = new PartProcessStep();

            materialConditioningStep.Name = LocalizedResources.Moulding_MaterialConditioning;
            materialConditioningStep.CycleTime = resultingCycleTime;
            materialConditioningStep.ProcessTime = resultingCycleTime;
            materialConditioningStep.PartsPerCycle = 1;
            materialConditioningStep.Accuracy = (short)ProcessCalculationAccuracy.Calculated;
            materialConditioningStep.ProductionSkilledLabour = 0.5m;

            var newSteps = new List<ProcessStep>();
            newSteps.Add(materialConditioningStep);

            // Injection Molding.
            var injectionMoulding = new PartProcessStep();

            injectionMoulding.Name = LocalizedResources.Moulding_InjectionMoulding;
            injectionMoulding.CycleTime = resultingCycleTime;
            injectionMoulding.ProcessTime = resultingCycleTime;
            injectionMoulding.PartsPerCycle = 1;
            injectionMoulding.Accuracy = (short)ProcessCalculationAccuracy.Calculated;
            injectionMoulding.SetupTime = 3600;
            injectionMoulding.MaxDownTime = 3600;
            injectionMoulding.SetupSkilledLabour = 1;
            injectionMoulding.ProductionSkilledLabour = 0.5m;

            newSteps.Add(injectionMoulding);

            var secUnit = this.MeasurementUnitsAdapter.GetBaseMeasurementUnit(MeasurementUnitType.Time);
            var minUnit = this.MeasurementUnitsAdapter.GetMeasurementUnits(MeasurementUnitType.Time).FirstOrDefault(u => u.ScaleFactor == 60m);

            foreach (var step in newSteps)
            {
                step.ProcessTimeUnit = secUnit;
                step.CycleTimeUnit = secUnit;
                step.MaxDownTimeUnit = minUnit;
                step.SetupTimeUnit = minUnit;
            }

            var masterMachines = this.GetMachinesByForce();
            bool allMachines = true;

            if (this.machinesDataSourceManager != null && masterMachines.Count > 0)
            {
                try
                {
                    var cloneManager = new CloneManager(this.machinesDataSourceManager, this.PartDataSourceManager);

                    var blendermixer = masterMachines.Where(p => p.Guid == this.GetBlenderMixerMachineGuid()).FirstOrDefault();
                    if (blendermixer != null)
                    {
                        // Get a new machine, a copy of the master data machine which will be added in the step.
                        var clone = cloneManager.Clone(blendermixer);

                        // Sets the index for the machine. The index represents the order in which machines are added in the process step.              
                        this.SetNextIndexForMachine(materialConditioningStep, clone);
                        materialConditioningStep.Machines.Add(clone);
                        clone.ProcessStep = materialConditioningStep;
                    }
                    else
                    {
                        allMachines = false;
                    }

                    var cooler = masterMachines.Where(p => p.Guid == this.GetCoolerMachineGuid()).FirstOrDefault();
                    if (cooler != null)
                    {
                        // Get a new machine, a copy of the master data machine which will be added in the step.
                        var clone = cloneManager.Clone(cooler);

                        // Sets the index for the machine. The index represents the order in which machines are added in the process step.              
                        this.SetNextIndexForMachine(materialConditioningStep, clone);
                        materialConditioningStep.Machines.Add(clone);
                        clone.ProcessStep = materialConditioningStep;
                    }
                    else
                    {
                        allMachines = false;
                    }

                    var dryer = masterMachines.Where(p => p.Guid == this.GetDryerMachineGuid()).FirstOrDefault();
                    if (dryer != null)
                    {
                        // Get a new machine, a copy of the master data machine which will be added in the step
                        var clone = cloneManager.Clone(dryer);

                        // Sets the index for the machine. The index represents the order in which machines are added in the process step.              
                        this.SetNextIndexForMachine(materialConditioningStep, clone);
                        materialConditioningStep.Machines.Add(clone);
                        clone.ProcessStep = materialConditioningStep;
                    }
                    else
                    {
                        allMachines = false;
                    }

                    var injection = masterMachines.Where(p => p.Guid == this.GetInjectionMouldingMachineGuid()).FirstOrDefault();
                    if (injection != null)
                    {
                        // Get a new machine, a copy of the master data machine which will be added in the step.
                        var clone = cloneManager.Clone(injection);

                        // Sets the index for the machine. The index represents the order in which machines are added in the process step.              
                        this.SetNextIndexForMachine(injectionMoulding, clone);
                        injectionMoulding.Machines.Add(clone);
                        clone.ProcessStep = injectionMoulding;
                    }
                    else
                    {
                        allMachines = false;
                    }

                    var robot = masterMachines.Where(p => p.Guid == this.GetHandlingRobotMachineGuid()).FirstOrDefault();
                    if (robot != null)
                    {
                        // Get a new machine, a copy of the master data machine which will be added in the step.
                        var clone = cloneManager.Clone(robot);

                        // Sets the index for the machine. The index represents the order in which machines are added in the process step.              
                        this.SetNextIndexForMachine(injectionMoulding, clone);
                        injectionMoulding.Machines.Add(clone);
                        clone.ProcessStep = injectionMoulding;
                    }
                    else
                    {
                        allMachines = false;
                    }
                }
                catch (DataAccessException ex)
                {
                    log.ErrorException("Error while cloning the machines", ex);
                    this.windowService.MessageDialogService.Show(LocalizedResources.Moulding_MasterDataMachinesNotLoaded, MessageDialogType.Error);
                }
            }
            else
            {
                allMachines = false;
            }

            if (!allMachines)
            {
                this.windowService.MessageDialogService.Show(LocalizedResources.Moulding_SomeMasterDataMachinesNotLoaded, MessageDialogType.Info);
            }

            var basicSettings = this.PartDataSourceManager.BasicSettingsRepository.GetBasicSettings();

            foreach (var newStep in newSteps)
            {
                if (parentPart.IsMasterData)
                {
                    newStep.SetOwner(null);
                    newStep.SetIsMasterData(true);
                }
                else
                {
                    newStep.SetOwner(parentPart.Owner);
                    newStep.SetIsMasterData(false);
                }

                // Set default values for the of empty fields.
                ProcessStepViewModel.SetDefaultValuesForMissingStepFields(newStep, parentPart, basicSettings);
            }

            this.CreatedSteps.AddRange(newSteps);
        }

        /// <summary>
        /// Sets the next index for machine.
        /// The index represents the order in which machines are added in the process step.
        /// </summary>
        /// <param name="processStep">The process step.</param>
        /// <param name="machine">The machine.</param>
        private void SetNextIndexForMachine(ProcessStep processStep, Machine machine)
        {
            if (processStep != null && machine != null)
            {
                int? maxIndex = processStep.Machines.Max(m => m.Index);

                if (maxIndex == null)
                {
                    machine.Index = 1;
                }
                else
                {
                    machine.Index = maxIndex + 1;
                }
            }
        }

        #endregion Create the casting process
    }
}