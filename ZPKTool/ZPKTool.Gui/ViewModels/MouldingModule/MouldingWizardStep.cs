﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Identifies each step of the Moulding Wizard.
    /// </summary>
    public enum MouldingWizardStep
    {
        /// <summary>
        /// Default value that does not identify any step.
        /// </summary>
        None = 0,

        /// <summary>
        /// The moulding type step (1st).
        /// </summary>
        MouldingType = 1,

        /// <summary>
        /// The settings step (2nd).
        /// </summary>
        BasicValues = 2,

        /// <summary>
        /// The results step (3th and last).
        /// </summary>
        ResultingProcessValues = 3
    }
}