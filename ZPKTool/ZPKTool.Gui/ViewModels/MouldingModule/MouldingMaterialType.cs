﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Moulding Material Type.
    /// </summary>
    public enum MouldingMaterialType
    {
        /// <summary>
        /// Thermoplastics material type.
        /// </summary>
        Thermoplastics = 0,

        /// <summary>
        /// Thermoplastic Elastomers material type.
        /// </summary>
        ThermoplasticElastomers = 1,

        /// <summary>
        /// Duroplastic material type.
        /// </summary>
        Duroplastics = 2
    }
}
