﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Moulding Technology.
    /// </summary>
    public enum MouldingTechnology
    {
        /// <summary>
        /// 2C, 3C, 4C Mould.
        /// </summary>
        Mould2C3C4C = 0,

        /// <summary>
        /// Hotrunner Technology.
        /// </summary>
        Hotrunner = 1,

        /// <summary>
        /// Inserted Parts Technology.
        /// </summary>
        InsertedParts = 2
    }
}
