﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Represents a material from the knowledge base.
    /// </summary>
    public class MouldingMainMaterial
    {
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>The type.</value>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the melting temperature.
        /// </summary>
        public decimal MeltingTemperature { get; set; }

        /// <summary>
        /// Gets or sets the effective thermal diffusity.
        /// </summary>
        public decimal EffectiveThermalDiffusity { get; set; }

        /// <summary>
        /// Gets or sets the mould temperature.
        /// </summary>
        public decimal MouldTemperature { get; set; }

        /// <summary>
        /// Gets or sets the mould release temperature.
        /// </summary>
        public decimal MouldReleaseTemperature { get; set; }

        /// <summary>
        /// Gets or sets the recommended die temperature.
        /// </summary>
        public decimal RecommendedDieTemperature { get; set; }

        /// <summary>
        /// Gets or sets the default pressure.
        /// </summary>
        public decimal DefaultPressure { get; set; }

        /// <summary>
        /// Gets or sets the min pressure.
        /// </summary>
        public decimal MinPressure { get; set; }

        /// <summary>
        /// Gets or sets the max pressure.
        /// </summary>
        public decimal MaxPressure { get; set; }
    }
}