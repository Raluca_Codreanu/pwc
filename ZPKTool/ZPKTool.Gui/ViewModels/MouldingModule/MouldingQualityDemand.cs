﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Moulding Quality Demand.
    /// </summary>
    public enum MouldingQualityDemand
    {
        /// <summary>
        /// This item represents high quality demand.
        /// </summary>
        High = 0,

        /// <summary>
        /// This item represents medium quality demand.
        /// </summary>
        Mid = 1,

        /// <summary>
        /// This item represents low quality demand.
        /// </summary>
        Low = 2
    }
}
