﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Represents a method that will handle the selection of a master data entity from a MD browser window.
    /// The handler is responsible with copying/cloning the selected master data entity or parts of it.
    /// </summary>
    /// <param name="masterDataEntity">The selected master data entity.</param>
    /// <param name="databaseId">The database from where the master data entity was loaded.</param>
    public delegate void MasterDataEntitySelectionHandler(object masterDataEntity, DbIdentifier databaseId);

    /// <summary>
    /// The view-model for the master data Browser.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class MasterDataBrowserViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The index for Tree View.
        /// </summary>
        private const int TreeViewIndex = 0;

        /// <summary>
        /// The index for the DataGrid View.
        /// </summary>
        private const int GridViewIndex = 1;

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The window service.
        /// </summary>
        private readonly IWindowService windowService;

        /// <summary>
        /// The online check service.
        /// </summary>
        private readonly IOnlineCheckService onlineChecker;

        /// <summary>
        /// The composition container.
        /// </summary>
        private readonly CompositionContainer compositionContainer;

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// A value indicating whether this instance is loading data.
        /// </summary>
        private bool isLoading;

        /// <summary>
        /// The type of the master data to display in the browser.
        /// </summary>
        private Type masterDataType;

        /// <summary>
        /// The master data items to display.
        /// </summary>
        private Collection<TreeViewDataItem> masterDataTreeItems = new Collection<TreeViewDataItem>();

        /// <summary>
        /// The undo list containing the last [maxUndoLimit] TreeViewDataItems's selected.
        /// </summary>
        private Collection<TreeViewDataItem> undoSelectionItems = new Collection<TreeViewDataItem>();

        /// <summary>
        /// The redo list.
        /// </summary>
        private Collection<TreeViewDataItem> redoSelectionItems = new Collection<TreeViewDataItem>();

        /// <summary>
        /// The maximum size of the undo selection list.
        /// </summary>
        private int? maxUndoLimit = 11;

        /// <summary>
        /// A value indicating whether the current item selection is caused by an undo/redo command or not.
        /// </summary>
        private bool isUndoRedoAction = false;

        /// <summary>
        /// The header of the group box containing the tree view.
        /// </summary>
        private string treeViewGroupHeader;

        /// <summary>
        /// The data context from which to load the MasterData.
        /// </summary>
        private IDataSourceManager masterDataContext;

        /// <summary>
        /// The context from which the master data was loaded.
        /// </summary>
        private DbIdentifier masterDataSource;

        /// <summary>
        /// The title that should be displayed by the view.
        /// </summary>
        private string title = string.Empty;

        /// <summary>
        /// The selected master data item from the tree.
        /// </summary>
        private TreeViewDataItem selectedTreeItem;

        /// <summary>
        /// The selected master data item from the data grid row.
        /// </summary>
        private object selectedGridViewItem;

        /// <summary>
        /// The details of the selected master data item.
        /// </summary>
        private object selectedItemDetails;

        /// <summary>
        /// The selected view index from the TabControl.
        /// </summary>
        private int selectedViewIndex;

        /// <summary>
        /// The list of objects containing the loaded Master Data.
        /// </summary>
        private ICollection<object> loadedMasterData = new List<object>();

        /// <summary>
        /// The text used to filter the tree content.
        /// </summary>
        private string treeFilterText;

        /// <summary>
        /// The timer used to delay the tree filtering.
        /// </summary>
        private DispatcherTimer treeFilteringTimer = new DispatcherTimer();

        /// <summary>
        /// A bool value indicating whether the tree filtering should be delayed or not.
        /// </summary>
        private bool delayTreeFiltering = true;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        #endregion Attributes

        /// <summary>
        /// Initializes a new instance of the <see cref="MasterDataBrowserViewModel"/> class.
        /// </summary>
        /// <param name="compositionContainer">The composition container.</param>
        /// <param name="messenger">The messenger.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="onlineChecker">The online check service.</param>
        /// <param name="unitsService">The units service.</param>
        [ImportingConstructor]
        public MasterDataBrowserViewModel(
            CompositionContainer compositionContainer,
            IMessenger messenger,
            IWindowService windowService,
            IOnlineCheckService onlineChecker,
            IUnitsService unitsService)
        {
            Argument.IsNotNull("compositionContainer", compositionContainer);
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("onlineChecker", onlineChecker);
            Argument.IsNotNull("unitsService", unitsService);

            this.compositionContainer = compositionContainer;
            this.messenger = messenger;
            this.windowService = windowService;
            this.onlineChecker = onlineChecker;
            this.unitsService = unitsService;

            this.SelectedMasterDataTreeItemChangedCommand =
                new DelegateCommand<RoutedPropertyChangedEventArgs<object>>(e => this.SelectedTreeItem = e.NewValue as TreeViewDataItem);
            this.MasterDataTreeDoubleClickCommand = new DelegateCommand<MouseEventArgs>(e => this.Select());

            this.MasterDataDataGridViewRowDoubleClickCommand = new DelegateCommand<MouseEventArgs>(e => this.Select());

            this.SelectCommand = new DelegateCommand(this.Select, this.CanSelect);
            this.UndoSelectionCommand = new DelegateCommand(this.UndoSelection, this.CanUndoSelection);
            this.RedoSelectionCommand = new DelegateCommand(this.RedoSelection, this.CanRedoSelection);

            // Delay with 300 ms the tree filtering to prevent it to be applied after each key press, if the user input text is longer
            treeFilteringTimer.Interval = new TimeSpan(0, 0, 0, 0, 300);
            treeFilteringTimer.Tick += delegate(object sender, EventArgs e)
            {
                this.ApplyTreeFilter();
                treeFilteringTimer.Stop();
            };
        }

        /// <summary>
        /// Occurs when a master data object is selected by the user.
        /// </summary>
        public event MasterDataEntitySelectionHandler MasterDataSelected;

        #region Commands

        /// <summary>
        /// Gets the command executed when the currently selected master data item has changed.
        /// </summary>
        public ICommand SelectedMasterDataTreeItemChangedCommand { get; private set; }

        /// <summary>
        /// Gets the command executed when the master data tree is double clicked.
        /// </summary>
        public ICommand MasterDataTreeDoubleClickCommand { get; private set; }

        /// <summary>
        /// Gets the command executed when the master data tree is double clicked.
        /// </summary>
        public ICommand MasterDataDataGridViewRowDoubleClickCommand { get; private set; }

        /// <summary>
        /// Gets the command that selects the current master data item for the client.
        /// </summary>
        public ICommand SelectCommand { get; private set; }

        /// <summary>
        /// Gets the command responsible for selecting the previously selected item from MasterData TreeView.
        /// </summary>
        public ICommand UndoSelectionCommand { get; private set; }

        /// <summary>
        /// Gets the command that handles the MasterData TreeView selection redo action.
        /// </summary>
        public ICommand RedoSelectionCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether this instance is loading data.
        /// </summary>
        public bool IsLoading
        {
            get { return this.isLoading; }
            set { this.SetProperty(ref this.isLoading, value, () => this.IsLoading); }
        }

        /// <summary>
        /// Gets or sets the loaded master data
        /// </summary>
        public ICollection<object> LoadedMasterData
        {
            get { return this.loadedMasterData; }
            set { this.SetProperty(ref this.loadedMasterData, value, () => this.LoadedMasterData); }
        }

        /// <summary>
        /// Gets or sets the type of the master data to display in the browser.
        /// Setting this property triggers the master data loading process in the background.
        /// </summary>        
        public Type MasterDataType
        {
            get { return this.masterDataType; }
            set { this.SetProperty(ref this.masterDataType, value, () => this.MasterDataType, this.OnMasterDataTypeChanged); }
        }

        /// <summary>
        /// Gets or sets the master data items to display.
        /// </summary>
        public Collection<TreeViewDataItem> MasterDataTreeItems
        {
            get { return this.masterDataTreeItems; }
            set { this.SetProperty(ref this.masterDataTreeItems, value, () => this.MasterDataTreeItems); }
        }

        /// <summary>
        /// Gets or sets the header of the group box containing the tree view.
        /// </summary>
        public string TreeViewGroupHeader
        {
            get { return this.treeViewGroupHeader; }
            set { this.SetProperty(ref this.treeViewGroupHeader, value, () => this.TreeViewGroupHeader); }
        }

        /// <summary>
        /// Gets or sets the title that should be displayed by the view.
        /// </summary>        
        public string Title
        {
            get { return this.title; }
            set { this.SetProperty(ref this.title, value, () => this.Title); }
        }

        /// <summary>
        /// Gets or sets the selected master data tree item.
        /// </summary>   
        public TreeViewDataItem SelectedTreeItem
        {
            get { return this.selectedTreeItem; }
            set { this.SetProperty(ref this.selectedTreeItem, value, () => this.SelectedTreeItem, this.HandleSelectedItemChange); }
        }

        /// <summary>
        /// Gets or sets the selected master data grid item.
        /// </summary>
        public object SelectedGridViewItem
        {
            get { return this.selectedGridViewItem; }
            set { this.SetProperty(ref this.selectedGridViewItem, value, () => this.SelectedGridViewItem); }
        }

        /// <summary>
        /// Gets or sets the details for the selected master data item.
        /// </summary>
        public object SelectedItemDetails
        {
            get { return this.selectedItemDetails; }
            set { this.SetProperty(ref this.selectedItemDetails, value, () => this.SelectedItemDetails); }
        }

        /// <summary>
        /// Gets or sets the selected view index from TabControl.
        /// </summary>
        public int SelectedViewIndex
        {
            get { return this.selectedViewIndex; }
            set { this.SetProperty(ref this.selectedViewIndex, value, () => this.SelectedViewIndex); }
        }

        /// <summary>
        /// Gets or sets the text on which the tree content is filtered.
        /// </summary>
        public string TreeFilterText
        {
            get
            {
                return this.treeFilterText;
            }

            set
            {
                var oldValue = this.treeFilterText;
                this.SetProperty(ref this.treeFilterText, value, () => this.TreeFilterText);

                // Start the filtering or show all items when there is no filter text.
                if (this.treeFilterText != oldValue)
                {
                    this.treeFilteringTimer.Stop();

                    if (delayTreeFiltering)
                    {
                        this.treeFilteringTimer.Start();
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the DataGrid's persist ID, depending on the loaded items' type.
        /// </summary>
        public string DataGridPersistId { get; set; }

        /// <summary>
        /// Gets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter { get; private set; }

        #endregion Properties

        /// <summary>
        /// Called after the view has been loaded and starts loading the master data.
        /// <para/>
        /// During unit tests this method must be manually called because there is no view to call it.
        /// </summary>
        public override void OnLoaded()
        {
            this.SelectedItemDetails = null;
            this.StartLoadingMasterData();
        }

        /// <summary>
        /// Called when the view has been unloaded.
        /// </summary>
        public override void OnUnloaded()
        {
            this.LoadedMasterData = null;
            this.MasterDataTreeItems = null;
            this.TreeFilterText = null;
            this.undoSelectionItems.Clear();
            this.redoSelectionItems.Clear();
            this.treeFilteringTimer.Stop();
        }

        #region Master Data Loading

        /// <summary>
        /// Loads the master data on a background thread.
        /// </summary>
        private void StartLoadingMasterData()
        {
            if (this.MasterDataType == null)
            {
                return;
            }

            DbIdentifier loadLocation = DbIdentifier.NotSet;
            IEnumerable<object> loadedMasterData = null;
            var preferenceMasterDataBrowserDataSource = (DbLocation)UserSettingsManager.Instance.MasterDataBrowserDataSource;

            System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                log.Info("--- Started the master data loading task ---");
                this.IsLoading = true;

                if (preferenceMasterDataBrowserDataSource == DbLocation.Local)
                {
                    try
                    {
                        loadLocation = DbIdentifier.LocalDatabase;
                        loadedMasterData = LoadMasterData(this.MasterDataType, loadLocation);
                    }
                    catch (Exception ex1)
                    {
                        log.InfoException("Failed to load MD from the local db.", ex1);
                        throw;
                    }
                }
                else 
                {
                    try
                    {
                        // If the central db is online load the master data from it, otherwise load it from the local db                
                        if (this.onlineChecker.IsOnline)
                        {
                            loadLocation = DbIdentifier.CentralDatabase;
                            DatabaseHelper.ValidateDatabaseVersion(DbIdentifier.CentralDatabase);
                        }
                        else
                        {
                            loadLocation = DbIdentifier.LocalDatabase;
                        }

                        log.Info("The Master Data loading location is {0}.", loadLocation);
                        loadedMasterData = LoadMasterData(this.MasterDataType, loadLocation);
                    }
                    catch (Exception ex)
                    {
                        // If an error occurred while loading from the central db try to load from the local db.
                        if (loadLocation == DbIdentifier.CentralDatabase)
                        {
                            log.InfoException("Failed to load the Master Data from the central db. Trying to load it from the local db.", ex);
                            try
                            {
                                loadLocation = DbIdentifier.LocalDatabase;
                                loadedMasterData = LoadMasterData(this.MasterDataType, loadLocation);
                            }
                            catch (Exception ex1)
                            {
                                log.InfoException("Failed to load MD from the local db.", ex1);
                                throw;
                            }
                        }
                        else
                        {
                            log.InfoException("Failed to load MD from the local db.", ex);
                            throw;
                        }
                    }
                }

                var dataManager = DataAccessFactory.CreateDataSourceManager(loadLocation);
                ICollection<Currency> currencies = null;
                Currency baseCurrency = null;

                CurrencyConversionManager.GetEntityCurrenciesAndBaseCurrency(null, dataManager, out currencies, out baseCurrency);
                this.MeasurementUnitsAdapter = new UnitsAdapter(dataManager);
                this.MeasurementUnitsAdapter.Currencies = currencies;
                this.MeasurementUnitsAdapter.BaseCurrency = baseCurrency;
            })
            .ContinueWith(task =>
            {
                // Show an error message if needed
                if (task.Exception != null)
                {
                    this.windowService.MessageDialogService.Show(task.Exception.InnerException);
                }
                else if (loadedMasterData != null)
                {
                    // Display the loaded master data
                    if (loadLocation == DbIdentifier.CentralDatabase)
                    {
                        this.Title = LocalizedResources.General_MasterDataBrowser + " (" + LocalizedResources.General_CentralDb + ")";
                    }
                    else if (loadLocation == DbIdentifier.LocalDatabase)
                    {
                        this.Title = LocalizedResources.General_MasterDataBrowser + " (" + LocalizedResources.General_LocalDb + ")";
                    }

                    this.masterDataSource = loadLocation;

                    this.LoadedMasterData = loadedMasterData.ToList();
                    this.MasterDataTreeItems = this.PrepareMasterDataForDisplay(loadedMasterData);
                }

                this.IsLoading = false;

                log.Info("--- Finished the master data loading task ---");
            });
        }

        /// <summary>
        /// Loads the master data from the database.
        /// </summary>
        /// <param name="masterDataTypeToLoad">Type of the master data to load.</param>
        /// <param name="loadLocation">The db from where to load the master data.</param>
        /// <returns>A list of objects containing the loaded master data.</returns>
        private IEnumerable<object> LoadMasterData(Type masterDataTypeToLoad, DbIdentifier loadLocation)
        {
            this.masterDataContext = DataAccessFactory.CreateDataSourceManager(loadLocation);

            if (masterDataTypeToLoad == typeof(Part))
            {
                return this.masterDataContext.PartRepository.GetAllMasterDataPart().OrderBy(part => part.Name);
            }

            if (masterDataTypeToLoad == typeof(RawPart))
            {
                return this.masterDataContext.PartRepository.GetAllMasterDataRawPart().OrderBy(part => part.Name);
            }

            if (masterDataTypeToLoad == typeof(Assembly))
            {
                return this.masterDataContext.AssemblyRepository.GetAllMasterData().OrderBy(assy => assy.Name);
            }

            if (masterDataTypeToLoad == typeof(Die))
            {
                return this.masterDataContext.DieRepository.GetAllMasterData().OrderBy(die => die.Name);
            }

            if (masterDataTypeToLoad == typeof(Commodity))
            {
                return this.masterDataContext.CommodityRepository.GetAllMasterData().OrderBy(com => com.Name);
            }

            if (masterDataTypeToLoad == typeof(Machine))
            {
                return this.masterDataContext.MachineRepository.GetAllMasterData().OrderBy(m => m.Name);
            }

            if (masterDataTypeToLoad == typeof(Consumable))
            {
                return this.masterDataContext.ConsumableRepository.GetAllMasterData().OrderBy(cons => cons.Name);
            }

            if (masterDataTypeToLoad == typeof(RawMaterial))
            {
                return this.masterDataContext.RawMaterialRepository.GetAllMasterData().OrderBy(rawm => rawm.Name);
            }

            if (masterDataTypeToLoad == typeof(Customer))
            {
                return this.masterDataContext.SupplierRepository.FindAllMasterData().OrderBy(s => s.Name);
            }

            if (masterDataTypeToLoad == typeof(Manufacturer))
            {
                return this.masterDataContext.ManufacturerRepository.GetAllMasterData().OrderBy(man => man.Name);
            }

            return null;
        }

        #endregion Master Data Loading

        #region Tree Filtering

        /// <summary>
        /// Filters the tree content based on the text in the <see cref="TreeFilterText"/> property.
        /// </summary>
        private void ApplyTreeFilter()
        {
            if (this.LoadedMasterData == null)
            {
                if (this.MasterDataTreeItems != null)
                {
                    this.MasterDataTreeItems.Clear();
                }

                return;
            }

            var filterText = this.TreeFilterText ?? string.Empty;
            var oldSelectedItem = this.SelectedTreeItem;

            // Create a list of INameable MasterData items and remove the ones that are filtered out by the filter predicate.
            var filteredData = this.LoadedMasterData.Cast<INameable>().ToList();
            int unfilteredItemsCount = filteredData.Count; // the total number of master data objects (before filtering)

            Predicate<INameable> removeFilter = (obj) =>
                {
                    var machine = obj as Machine;
                    if (machine != null)
                    {
                        // A machine is filtered out if neither its name nor its manufacturer mach the filter text.
                        bool remove = string.IsNullOrWhiteSpace(machine.Name) || !machine.Name.StartsWith(filterText, StringComparison.CurrentCultureIgnoreCase);
                        if (machine.Manufacturer != null)
                        {
                            remove = remove
                                && (string.IsNullOrWhiteSpace(machine.Name) || !machine.Manufacturer.Name.StartsWith(filterText, StringComparison.CurrentCultureIgnoreCase));
                        }

                        return remove;
                    }
                    else
                    {
                        // Other objects are filtered out based only on their name.
                        return string.IsNullOrWhiteSpace(obj.Name)
                            || !obj.Name.StartsWith(filterText, StringComparison.CurrentCultureIgnoreCase);
                    }
                };
            filteredData.RemoveAll(removeFilter);

            this.MasterDataTreeItems = this.PrepareMasterDataForDisplay(filteredData);
            this.UpdateUndoRedoCollections();

            // Restore the selected item if it is present in the filtered tree content.
            // When the filter is cleared (the filter text is empty) the selected item should not be restored.
            if (oldSelectedItem != null && filteredData.Count != unfilteredItemsCount)
            {
                foreach (var item in this.MasterDataTreeItems)
                {
                    var searchedItem = item.FindItemForDataObject(oldSelectedItem.DataObject);

                    // If MasterData tree items contain the clicked item, expand the tree hierarchy for it and select it
                    if (searchedItem != null)
                    {
                        searchedItem.Select();
                        break;
                    }
                }
            }
        }

        #endregion Tree Filtering

        #region Prepare the loaded Master Data for display

        /// <summary>
        /// Sets the TreeViewGroupHeader and the DataGrid's persist ID according to the master data type.
        /// </summary>
        private void OnMasterDataTypeChanged()
        {
            if (MasterDataType == typeof(Machine))
            {
                this.TreeViewGroupHeader = LocalizedResources.General_Machines;
                this.DataGridPersistId = "5BDF570A-AD20-43B8-BB8D-575AB71D9D74";
            }

            if (MasterDataType == typeof(RawPart))
            {
                this.TreeViewGroupHeader = LocalizedResources.General_RawParts;
                this.DataGridPersistId = "33E42299-18B4-4721-A6F2-9D0BE9975933";
            }

            if (MasterDataType == typeof(Part))
            {
                this.TreeViewGroupHeader = LocalizedResources.General_Parts;
                this.DataGridPersistId = "0F831665-C8E2-43F6-B11D-505FCB39EEFF";
            }

            if (MasterDataType == typeof(Assembly))
            {
                this.TreeViewGroupHeader = LocalizedResources.General_Assemblies;
                this.DataGridPersistId = "8C9AF34D-525C-4958-8ED4-371BD90E3CEC";
            }

            if (MasterDataType == typeof(Die))
            {
                this.TreeViewGroupHeader = LocalizedResources.General_Dies;
                this.DataGridPersistId = "A70B27D9-B907-43D6-89FC-75F54171C968";
            }

            if (MasterDataType == typeof(Commodity))
            {
                this.TreeViewGroupHeader = LocalizedResources.General_Commodities;
                this.DataGridPersistId = "B5DAD772-2520-4C11-9C57-113E00C66371";
            }

            if (MasterDataType == typeof(Consumable))
            {
                this.TreeViewGroupHeader = LocalizedResources.General_Consumables;
                this.DataGridPersistId = "AA8A7718-25DA-4578-96B0-0D9ADE1122C6";
            }

            if (MasterDataType == typeof(RawMaterial))
            {
                this.TreeViewGroupHeader = LocalizedResources.General_RawMaterials;
                this.DataGridPersistId = "E0794481-BCAD-447D-BC4A-FBA8D8A4ABE4";
            }

            if (MasterDataType == typeof(Customer))
            {
                this.TreeViewGroupHeader = LocalizedResources.General_Suppliers;
                this.DataGridPersistId = "22690220-C11E-45A2-A323-59D350D7C672";
            }

            if (MasterDataType == typeof(Manufacturer))
            {
                this.TreeViewGroupHeader = LocalizedResources.General_Manufacturers;
                this.DataGridPersistId = "9A089FA2-7897-4CD7-8FFF-AB506781D9D4";
            }
        }

        /// <summary>
        /// Prepares the master data for being displayed in the view by transforming the data objects containing the master data into MasterDataBrowserItem instances.
        /// </summary>
        /// <param name="masterData">The master data.</param>
        /// <returns>A Collection of MasterDataBrowserItem objects representing the master data.</returns>
        private Collection<TreeViewDataItem> PrepareMasterDataForDisplay(IEnumerable<object> masterData)
        {
            if (masterData != null)
            {
                var machines = masterData.OfType<Machine>();
                if (machines.Any())
                {
                    return PrepareForDisplay(machines);
                }

                var rawParts = masterData.OfType<RawPart>();
                if (rawParts.Any())
                {
                    return PrepareForDisplay(rawParts);
                }

                var parts = masterData.OfType<Part>();
                if (parts.Any())
                {
                    return PrepareForDisplay(parts);
                }

                var assemblies = masterData.OfType<Assembly>();
                if (assemblies.Any())
                {
                    return PrepareForDisplay(assemblies);
                }

                var dies = masterData.OfType<Die>();
                if (dies.Any())
                {
                    return PrepareForDisplay(dies);
                }

                var commodities = masterData.OfType<Commodity>();
                if (commodities.Any())
                {
                    return PrepareForDisplay(commodities);
                }

                var consumables = masterData.OfType<Consumable>();
                if (consumables.Any())
                {
                    return PrepareForDisplay(consumables);
                }

                var materials = masterData.OfType<RawMaterial>();
                if (materials.Any())
                {
                    return PrepareForDisplay(materials);
                }

                var suppliers = masterData.OfType<Customer>();
                if (suppliers.Any())
                {
                    return PrepareForDisplay(suppliers);
                }

                var manufacturers = masterData.OfType<Manufacturer>();
                if (manufacturers.Any())
                {
                    return PrepareForDisplay(manufacturers);
                }
            }

            return new Collection<TreeViewDataItem>();
        }

        /// <summary>
        /// Prepares the master data machines for being displayed by the view.
        /// </summary>
        /// <param name="machines">The master data machines.</param>
        /// <returns>A Collection of MasterDataBrowserItem containing the hierarchic representation of the machines.</returns>
        private Collection<TreeViewDataItem> PrepareForDisplay(IEnumerable<Machine> machines)
        {
            // A temporary root item that makes it easier to search for children items.
            TreeViewDataItem rootItem = new TreeViewDataItem();

            // Add the machine classifications to the root item.
            rootItem.Children.AddRange(this.CreateMachinesClassificationTreeSource());

            // Create an item to hold the machines without classification. This item will be the last root item.
            TreeViewDataItem unclassifiedMachinesItem = new TreeViewDataItem();
            unclassifiedMachinesItem.Label = LocalizedResources.General_Unclassified;
            unclassifiedMachinesItem.IconResourceKey = Images.MaterialClassificationIconKey;

            // For each machine, add it into the appropriate classification item.
            foreach (Machine machine in machines.OrderBy(m => m.Name))
            {
                TreeViewDataItem machineItem = new TreeViewDataItem();
                machineItem.DataObject = machine;
                machineItem.IconResourceKey = Images.MachineIconKey;
                machineItem.BottomRightIcon = machine.IsStockMasterData ? Images.MasterDataIcon : Images.CustomMasterDataIcon;
                machineItem.ToolTip = machine.IsStockMasterData ? LocalizedResources.MasterData_StockMasterData : LocalizedResources.MasterData_CustomMasterData;
                machineItem.GroupId = 1;

                // If the machine has a manufacturer with a valid name the node label will be "<manufacturer name>, <machine name>".
                if (machine.Manufacturer != null && !string.IsNullOrEmpty(machine.Manufacturer.Name))
                {
                    machineItem.Label = machine.Manufacturer.Name + ", " + machine.Name;
                }
                else
                {
                    machineItem.Label = machine.Name;
                }

                // Check each of the machine's classifications and add the machine's item into the item of the last set classification.
                // The check order is the hierarchical order of classification: MainClassification, TypeClassification, SubClassification and ClassificationLevel4
                // If the classification does not correspond to the preloaded classification the item will be added in the last classification that exists, or else to unclassified items.
                if (machine.MainClassification == null)
                {
                    unclassifiedMachinesItem.Children.Add(machineItem);
                    continue;
                }

                var mainClsItem = rootItem.Children.FirstOrDefault(i => machine.MainClassification != null && i.DataObject as MachinesClassification == machine.MainClassification);
                if (mainClsItem == null)
                {
                    unclassifiedMachinesItem.Children.Add(machineItem);
                    continue;
                }

                var typeClsItem = mainClsItem.Children.FirstOrDefault(i => machine.TypeClassification != null && i.DataObject as MachinesClassification == machine.TypeClassification);
                if (typeClsItem == null)
                {
                    mainClsItem.Children.Add(machineItem);
                    continue;
                }

                var subClsItem = typeClsItem.Children.FirstOrDefault(i => machine.SubClassification != null && i.DataObject as MachinesClassification == machine.SubClassification);
                if (subClsItem == null)
                {
                    typeClsItem.Children.Add(machineItem);
                    continue;
                }

                var lvl4ClsItem = subClsItem.Children.FirstOrDefault(i => machine.ClassificationLevel4 != null && i.DataObject as MachinesClassification == machine.ClassificationLevel4);
                if (lvl4ClsItem == null)
                {
                    subClsItem.Children.Add(machineItem);
                    continue;
                }

                lvl4ClsItem.Children.Add(machineItem);
            }

            var classificationItemsWithNoChildren = new List<TreeViewDataItem>();
            this.SearchForClassificationItemsWithNoChildren(rootItem, ref classificationItemsWithNoChildren);

            // Remove the classification items with no children.
            foreach (var item in classificationItemsWithNoChildren)
            {
                var parentItem = item != null ? item.Parent : null;
                if (parentItem != null)
                {
                    parentItem.Children.Remove(item);
                    this.RemoveClassificationItemsWithNoChildren(parentItem);
                }
            }

            Collection<TreeViewDataItem> result = new Collection<TreeViewDataItem>(rootItem.Children.OrderBy(c => c.Label).ToList());
            if (unclassifiedMachinesItem.Children.Count > 0)
            {
                result.Add(unclassifiedMachinesItem);
            }

            return result;
        }

        /// <summary>
        /// Prepares the master data Parts for being displayed by the view.
        /// </summary>
        /// <param name="parts">The master data Parts.</param>
        /// <returns>
        /// A Collection of MasterDataBrowserItem containing the representation of the Parts.
        /// </returns>
        private Collection<TreeViewDataItem> PrepareForDisplay(IEnumerable<Part> parts)
        {
            Collection<TreeViewDataItem> result = new Collection<TreeViewDataItem>();

            foreach (Part part in parts.OrderBy(p => p.Name))
            {
                TreeViewDataItem item = new TreeViewDataItem
                {
                    DataObject = part,
                    IconResourceKey = part is RawPart ? Images.RawPartIconKey : Images.PartIconKey,
                    BottomRightIcon = part.IsStockMasterData ? Images.MasterDataIcon : Images.CustomMasterDataIcon,
                    ToolTip = part.IsStockMasterData ? LocalizedResources.MasterData_StockMasterData : LocalizedResources.MasterData_CustomMasterData,
                    Label = part.Name
                };
                result.Add(item);
            }

            return result;
        }

        /// <summary>
        /// Prepares the master data Assemblies for being displayed by the view.
        /// </summary>
        /// <param name="assemblies">The master data assemblies.</param>
        /// <returns>A Collection of MasterDataBrowserItem containing the representation of the assemblies.</returns>
        private Collection<TreeViewDataItem> PrepareForDisplay(IEnumerable<Assembly> assemblies)
        {
            Collection<TreeViewDataItem> result = new Collection<TreeViewDataItem>();

            foreach (Assembly assy in assemblies.OrderBy(a => a.Name))
            {
                TreeViewDataItem item = new TreeViewDataItem
                {
                    DataObject = assy,
                    IconResourceKey = Images.AssemblyIconKey,
                    BottomRightIcon = assy.IsStockMasterData ? Images.MasterDataIcon : Images.CustomMasterDataIcon,
                    ToolTip = assy.IsStockMasterData ? LocalizedResources.MasterData_StockMasterData : LocalizedResources.MasterData_CustomMasterData,
                    Label = assy.Name
                };
                result.Add(item);
            }

            return result;
        }

        /// <summary>
        /// Prepares the master data Dies for being displayed by the view.
        /// </summary>
        /// <param name="dies">The master data Dies.</param>
        /// <returns>A Collection of MasterDataBrowserItem containing the representation of the Dies.</returns>
        private Collection<TreeViewDataItem> PrepareForDisplay(IEnumerable<Die> dies)
        {
            Collection<TreeViewDataItem> result = new Collection<TreeViewDataItem>();

            foreach (Die die in dies.OrderBy(d => d.Name))
            {
                TreeViewDataItem item = new TreeViewDataItem
                {
                    DataObject = die,
                    IconResourceKey = Images.DieIconKey,
                    BottomRightIcon = die.IsStockMasterData ? Images.MasterDataIcon : Images.CustomMasterDataIcon,
                    ToolTip = die.IsStockMasterData ? LocalizedResources.MasterData_StockMasterData : LocalizedResources.MasterData_CustomMasterData,
                    Label = die.Name
                };
                result.Add(item);
            }

            return result;
        }

        /// <summary>
        /// Prepares the master data Commodities for being displayed by the view.
        /// </summary>
        /// <param name="commodities">The master data Commodities.</param>
        /// <returns>A Collection of MasterDataBrowserItem containing the representation of the Commodities.</returns>
        private Collection<TreeViewDataItem> PrepareForDisplay(IEnumerable<Commodity> commodities)
        {
            Collection<TreeViewDataItem> result = new Collection<TreeViewDataItem>();

            foreach (Commodity commodity in commodities.OrderBy(c => c.Name))
            {
                TreeViewDataItem item = new TreeViewDataItem
                {
                    DataObject = commodity,
                    IconResourceKey = Images.CommodityIconKey,
                    BottomRightIcon = commodity.IsStockMasterData ? Images.MasterDataIcon : Images.CustomMasterDataIcon,
                    ToolTip = commodity.IsStockMasterData ? LocalizedResources.MasterData_StockMasterData : LocalizedResources.MasterData_CustomMasterData,
                    Label = commodity.Name
                };
                result.Add(item);
            }

            return result;
        }

        /// <summary>
        /// Prepares the master data Consumables for being displayed by the view.
        /// </summary>
        /// <param name="consumables">The master data Consumables.</param>
        /// <returns>A Collection of MasterDataBrowserItem containing the representation of the Consumables.</returns>
        private Collection<TreeViewDataItem> PrepareForDisplay(IEnumerable<Consumable> consumables)
        {
            Collection<TreeViewDataItem> result = new Collection<TreeViewDataItem>();

            foreach (Consumable cons in consumables.OrderBy(c => c.Name))
            {
                TreeViewDataItem item = new TreeViewDataItem
                {
                    DataObject = cons,
                    IconResourceKey = Images.ConsumableIconKey,
                    BottomRightIcon = cons.IsStockMasterData ? Images.MasterDataIcon : Images.CustomMasterDataIcon,
                    ToolTip = cons.IsStockMasterData ? LocalizedResources.MasterData_StockMasterData : LocalizedResources.MasterData_CustomMasterData,
                    Label = cons.Name
                };
                result.Add(item);
            }

            return result;
        }

        /// <summary>
        /// Prepares the master data Raw Materials for being displayed by the view.
        /// </summary>
        /// <param name="materials">The master data Raw Materials.</param>
        /// <returns>A Collection of MasterDataBrowserItem containing the representation of the Raw Materials.</returns>
        private Collection<TreeViewDataItem> PrepareForDisplay(IEnumerable<RawMaterial> materials)
        {
            // A temporary root item that makes it easier to search for children items.
            TreeViewDataItem rootItem = new TreeViewDataItem();

            // Add the raw material classifications to the root item.
            rootItem.Children.AddRange(this.CreateRawMaterialsClassificationTreeSource());

            // Create an item to hold the materials without classification. This item will be the last root item.
            TreeViewDataItem unclassifiedMaterialsItem = new TreeViewDataItem();
            unclassifiedMaterialsItem.Label = LocalizedResources.General_Unclassified;
            unclassifiedMaterialsItem.IconResourceKey = Images.MaterialClassificationIconKey;

            // For each material, add it into the appropriate classification item.
            foreach (RawMaterial material in materials.OrderBy(m => m.Name))
            {
                TreeViewDataItem materialItem = new TreeViewDataItem();
                materialItem.DataObject = material;
                materialItem.IconResourceKey = Images.RawMaterialIconKey;
                materialItem.BottomRightIcon = material.IsStockMasterData ? Images.MasterDataIcon : Images.CustomMasterDataIcon;
                materialItem.ToolTip = material.IsStockMasterData ? LocalizedResources.MasterData_StockMasterData : LocalizedResources.MasterData_CustomMasterData;
                materialItem.GroupId = 1;
                materialItem.Label = material.Name;

                // Check each of the material's classifications and add the material's item into the item of the last set classification.
                // The check order is the hierarchical order of classification: MaterialsClassificationL1, MaterialsClassificationL2,
                // MaterialsClassificationL3 and MaterialsClassificationL4
                // If the classification does not correspond to the preloaded classification the item will be added in the last classification that exists, or else to unclassified items.
                if (material.MaterialsClassificationL1 == null)
                {
                    unclassifiedMaterialsItem.Children.Add(materialItem);
                    continue;
                }

                var lvl1ClsItem = rootItem.Children.FirstOrDefault(i => material.MaterialsClassificationL1 != null && i.DataObject as MaterialsClassification == material.MaterialsClassificationL1);
                if (lvl1ClsItem == null)
                {
                    unclassifiedMaterialsItem.Children.Add(materialItem);
                    continue;
                }

                var lvl2ClsItem = lvl1ClsItem.Children.FirstOrDefault(i => material.MaterialsClassificationL2 != null && i.DataObject as MaterialsClassification == material.MaterialsClassificationL2);
                if (lvl2ClsItem == null)
                {
                    lvl1ClsItem.Children.Add(materialItem);
                    continue;
                }

                var lvl3ClsItem = lvl2ClsItem.Children.FirstOrDefault(i => material.MaterialsClassificationL3 != null && i.DataObject as MaterialsClassification == material.MaterialsClassificationL3);
                if (lvl3ClsItem == null)
                {
                    lvl2ClsItem.Children.Add(materialItem);
                    continue;
                }

                var lvl4ClsItem = lvl3ClsItem.Children.FirstOrDefault(i => material.MaterialsClassificationL4 != null && i.DataObject as MaterialsClassification == material.MaterialsClassificationL4);
                if (lvl4ClsItem == null)
                {
                    lvl3ClsItem.Children.Add(materialItem);
                    continue;
                }

                lvl4ClsItem.Children.Add(materialItem);
            }

            var classificationItemsWithNoChildren = new List<TreeViewDataItem>();
            this.SearchForClassificationItemsWithNoChildren(rootItem, ref classificationItemsWithNoChildren);

            // Remove the classification items with no children.
            foreach (var item in classificationItemsWithNoChildren)
            {
                var parentItem = item != null ? item.Parent : null;
                if (parentItem != null)
                {
                    parentItem.Children.Remove(item);
                    this.RemoveClassificationItemsWithNoChildren(parentItem);
                }
            }

            Collection<TreeViewDataItem> result = new Collection<TreeViewDataItem>(rootItem.Children.OrderBy(c => c.Label).ToList());
            if (unclassifiedMaterialsItem.Children.Count > 0)
            {
                result.Add(unclassifiedMaterialsItem);
            }

            return result;
        }

        /// <summary>
        /// Prepares the master data Suppliers for being displayed by the view.
        /// </summary>
        /// <param name="suppliers">The master data Suppliers.</param>
        /// <returns>A Collection of MasterDataBrowserItem containing the representation of the Suppliers.</returns>
        private Collection<TreeViewDataItem> PrepareForDisplay(IEnumerable<Customer> suppliers)
        {
            Collection<TreeViewDataItem> result = new Collection<TreeViewDataItem>();
            foreach (Customer supplier in suppliers.OrderBy(s => s.Name))
            {
                TreeViewDataItem item = new TreeViewDataItem
                {
                    DataObject = supplier,
                    IconResourceKey = Images.SupplierIconKey,
                    BottomRightIcon = supplier.IsStockMasterData ? Images.MasterDataIcon : Images.CustomMasterDataIcon,
                    ToolTip = supplier.IsStockMasterData ? LocalizedResources.MasterData_StockMasterData : LocalizedResources.MasterData_CustomMasterData,
                    Label = supplier.Name
                };
                result.Add(item);
            }

            return result;
        }

        /// <summary>
        /// Prepares the master data Manufacturers for being displayed by the view.
        /// </summary>
        /// <param name="manufacturers">The master data Manufacturers.</param>
        /// <returns>A Collection of MasterDataBrowserItem containing the representation of the Manufacturers.</returns>
        private Collection<TreeViewDataItem> PrepareForDisplay(IEnumerable<Manufacturer> manufacturers)
        {
            Collection<TreeViewDataItem> result = new Collection<TreeViewDataItem>();
            foreach (Manufacturer manufacturer in manufacturers.OrderBy(m => m.Name))
            {
                TreeViewDataItem item = new TreeViewDataItem
                {
                    DataObject = manufacturer,
                    IconResourceKey = Images.ManufacturerIconKey,
                    BottomRightIcon = manufacturer.IsStockMasterData ? Images.MasterDataIcon : Images.CustomMasterDataIcon,
                    ToolTip = manufacturer.IsStockMasterData ? LocalizedResources.MasterData_StockMasterData : LocalizedResources.MasterData_CustomMasterData,
                    Label = manufacturer.Name
                };
                result.Add(item);
            }

            return result;
        }

        /// <summary>
        /// Creates the machines classification tree.
        /// </summary>
        /// <returns>
        /// A list containing the root tree items.
        /// </returns>
        private List<TreeViewDataItem> CreateMachinesClassificationTreeSource()
        {
            List<TreeViewDataItem> rootItems = new List<TreeViewDataItem>();
            try
            {
                var machinesClassification = this.masterDataContext.MachinesClassificationRepository.LoadClassificationTree();
                foreach (MachinesClassification classification in machinesClassification.OrderBy(n => n.Name).Where(cls => cls.Parent == null))
                {
                    TreeViewDataItem item = new TreeViewDataItem()
                    {
                        DataObject = classification,
                        Label = classification.Name,
                        IconResourceKey = Images.MachineClassificationIconKey
                    };
                    rootItems.Add(item);
                    this.CreateMachinesClassificationSubtree(item, classification.Children);
                }
            }
            catch (DataAccessException ex)
            {
                log.ErrorException("Caught a data exception.", ex);
            }

            return rootItems;
        }

        /// <summary>
        /// Create recursively the sub-tree of a root item in the machines classification tree
        /// </summary>
        /// <param name="rootItem">The root item.</param>
        /// <param name="classifications">The classifications to add.</param>
        private void CreateMachinesClassificationSubtree(TreeViewDataItem rootItem, IEnumerable<MachinesClassification> classifications)
        {
            foreach (MachinesClassification classification in classifications)
            {
                TreeViewDataItem item = new TreeViewDataItem()
                {
                    DataObject = classification,
                    Label = classification.Name,
                    IconResourceKey = Images.MachineClassificationIconKey
                };
                rootItem.Children.Add(item);
                this.CreateMachinesClassificationSubtree(item, classification.Children);
            }
        }

        /// <summary>
        /// Creates the raw materials classification tree.
        /// </summary>
        /// <returns>
        /// The root items (which contain their children).
        /// </returns>
        private List<TreeViewDataItem> CreateRawMaterialsClassificationTreeSource()
        {
            List<TreeViewDataItem> rootItems = new List<TreeViewDataItem>();
            try
            {
                var materialsClassification = this.masterDataContext.MaterialsClassificationRepository.LoadClassificationTree();
                foreach (MaterialsClassification classification in materialsClassification.OrderBy(n => n.Name).Where(cls => cls.Parent == null))
                {
                    TreeViewDataItem item = new TreeViewDataItem()
                    {
                        DataObject = classification,
                        Label = classification.Name,
                        IconResourceKey = Images.MaterialClassificationIconKey
                    };

                    rootItems.Add(item);
                    this.CreateRawMaterialsClassificationSubtree(item, classification.Children);
                }
            }
            catch (DataAccessException ex)
            {
                log.ErrorException("Caught a business exception.", ex);
            }

            return rootItems;
        }

        /// <summary>
        /// Create recursively the sub-tree of a root item in the raw materials classification tree
        /// </summary>
        /// <param name="rootItem">The root item.</param>
        /// <param name="classifications">The classifications to add.</param>
        private void CreateRawMaterialsClassificationSubtree(TreeViewDataItem rootItem, IEnumerable<MaterialsClassification> classifications)
        {
            foreach (MaterialsClassification classification in classifications)
            {
                TreeViewDataItem item = new TreeViewDataItem()
                {
                    DataObject = classification,
                    Label = classification.Name,
                    IconResourceKey = Images.MaterialClassificationIconKey
                };
                rootItem.Children.Add(item);
                this.CreateRawMaterialsClassificationSubtree(item, classification.Children);
            }
        }

        /// <summary>
        /// Recursively removes from the tree the classification items with no children.
        /// After removing an item it searches to see if the parent item also needs to be removed.
        /// </summary>
        /// <param name="leafItem">The leaf item.</param>
        private void RemoveClassificationItemsWithNoChildren(TreeViewDataItem leafItem)
        {
            if (leafItem.Children.Count() != 0)
            {
                return;
            }

            var parentItem = leafItem.Parent;
            if (leafItem.Parent != null)
            {
                parentItem.Children.Remove(leafItem);
                this.RemoveClassificationItemsWithNoChildren(parentItem);
            }
        }

        /// <summary>
        /// Recursively searches the tree for classification items that have no children.
        /// Used for machine and raw material classifications.
        /// </summary>
        /// <param name="parentItem">The parent item.</param>
        /// <param name="classificationItemsWithNoChildren">The reference list with tree view data items</param>
        private void SearchForClassificationItemsWithNoChildren(TreeViewDataItem parentItem, ref List<TreeViewDataItem> classificationItemsWithNoChildren)
        {
            foreach (var item in parentItem.Children)
            {
                if (item.DataObject is MachinesClassification
                    || item.DataObject is MaterialsClassification)
                {
                    // If the current item has no children add it to the list of items with no children or else search it's children.
                    if (!item.Children.Any())
                    {
                        classificationItemsWithNoChildren.Add(item);
                    }
                    else
                    {
                        this.SearchForClassificationItemsWithNoChildren(item, ref classificationItemsWithNoChildren);
                    }
                }
            }
        }

        #endregion Prepare the loaded Master Data for display

        /// <summary>
        /// Handles the change of the SelectedItem property.
        /// </summary>
        private void HandleSelectedItemChange()
        {
            if (this.SelectedTreeItem == null)
            {
                this.SelectedItemDetails = null;
                return;
            }

            if (!this.isUndoRedoAction)
            {
                this.undoSelectionItems.Add(this.SelectedTreeItem);
                this.redoSelectionItems.Clear();
                this.RefreshUndoStack();
            }

            var selectedDie = this.selectedTreeItem.DataObject as Die;
            if (selectedDie != null)
            {
                var dieVM = this.compositionContainer.GetExportedValue<DieViewModel>();
                dieVM.IsReadOnly = true;
                dieVM.DataSourceManager = this.masterDataContext;
                dieVM.MeasurementUnitsAdapter = this.MeasurementUnitsAdapter;
                dieVM.Model = selectedDie;
                this.SelectedItemDetails = dieVM;
                return;
            }

            var selectedCommodity = this.selectedTreeItem.DataObject as Commodity;
            if (selectedCommodity != null)
            {
                var commodityVM = this.compositionContainer.GetExportedValue<CommodityViewModel>();
                commodityVM.IsReadOnly = true;
                commodityVM.DataSourceManager = this.masterDataContext;
                commodityVM.MeasurementUnitsAdapter = this.MeasurementUnitsAdapter;
                commodityVM.Model = selectedCommodity;
                this.SelectedItemDetails = commodityVM;
                return;
            }

            var selectedAssembly = this.selectedTreeItem.DataObject as Assembly;
            if (selectedAssembly != null)
            {
                var assyVM = this.compositionContainer.GetExportedValue<AssemblyViewModel>();
                assyVM.IsReadOnly = true;
                assyVM.DataSourceManager = this.masterDataContext;
                assyVM.MeasurementUnitsAdapter = this.MeasurementUnitsAdapter;
                assyVM.Model = selectedAssembly;
                this.SelectedItemDetails = assyVM;

                return;
            }

            var selectedConsumable = this.selectedTreeItem.DataObject as Consumable;
            if (selectedConsumable != null)
            {
                var consumableVM = this.compositionContainer.GetExportedValue<ConsumableViewModel>();
                consumableVM.IsReadOnly = true;
                consumableVM.DataSourceManager = this.masterDataContext;
                consumableVM.MeasurementUnitsAdapter = this.MeasurementUnitsAdapter;
                consumableVM.Model = selectedConsumable;
                this.SelectedItemDetails = consumableVM;

                return;
            }

            var selectedMaterial = this.selectedTreeItem.DataObject as RawMaterial;
            if (selectedMaterial != null)
            {
                var rawMaterialVM = this.compositionContainer.GetExportedValue<RawMaterialViewModel>();
                rawMaterialVM.IsReadOnly = true;
                rawMaterialVM.DataSourceManager = this.masterDataContext;
                rawMaterialVM.MeasurementUnitsAdapter = this.MeasurementUnitsAdapter;
                rawMaterialVM.Model = selectedMaterial;
                this.SelectedItemDetails = rawMaterialVM;

                return;
            }

            var selectedMachine = this.selectedTreeItem.DataObject as Machine;
            if (selectedMachine != null)
            {
                var machineVM = this.compositionContainer.GetExportedValue<MachineViewModel>();
                machineVM.IsReadOnly = true;
                machineVM.DataSourceManager = this.masterDataContext;
                machineVM.MeasurementUnitsAdapter = this.MeasurementUnitsAdapter;
                machineVM.Model = selectedMachine;
                this.SelectedItemDetails = machineVM;

                return;
            }

            // Note: this includes RawParts as well.
            var selectedPart = this.selectedTreeItem.DataObject as Part;
            if (selectedPart != null)
            {
                var partVM = this.compositionContainer.GetExportedValue<PartViewModel>();
                partVM.IsReadOnly = true;
                partVM.DataSourceManager = this.masterDataContext;
                partVM.MeasurementUnitsAdapter = this.MeasurementUnitsAdapter;
                partVM.Model = selectedPart;
                this.SelectedItemDetails = partVM;

                return;
            }

            var selectedSupplier = this.selectedTreeItem.DataObject as Customer;
            if (selectedSupplier != null)
            {
                var supplierVM = this.compositionContainer.GetExportedValue<SupplierViewModel>();
                supplierVM.IsReadOnly = true;
                supplierVM.DataSourceManager = this.masterDataContext;
                supplierVM.Model = selectedSupplier;
                this.SelectedItemDetails = supplierVM;

                return;
            }

            var selectedManufacturer = this.selectedTreeItem.DataObject as Manufacturer;
            if (selectedManufacturer != null)
            {
                var manufacturerVM = this.SelectedItemDetails as ManufacturerViewModel;
                if (manufacturerVM == null)
                {
                    manufacturerVM = this.compositionContainer.GetExportedValue<ManufacturerViewModel>();
                }

                manufacturerVM.IsReadOnly = true;
                manufacturerVM.DataSourceManager = this.masterDataContext;
                manufacturerVM.Model = selectedManufacturer;
                this.SelectedItemDetails = manufacturerVM;

                return;
            }

            // Hide the details if selecting an item that is not a master data entity (like a classification level).
            this.SelectedItemDetails = null;
        }

        #region Commands handlers

        /// <summary>
        /// Determines whether the Select command can execute.
        /// </summary>
        /// <returns>
        /// true if the command can execute; otherwise it returns false.
        /// </returns>
        private bool CanSelect()
        {
            if (this.SelectedViewIndex == TreeViewIndex)
            {
                var treeItem = this.SelectedTreeItem;

                // The selection button is disabled if the selected item is MaterialsClassification, MachinesClassification,
                // or is Unclassified (which has no DataObject)
                return treeItem != null
                       && (treeItem.DataObject != null)
                       && !(treeItem.DataObject is MaterialsClassification)
                       && !(treeItem.DataObject is MachinesClassification);
            }
            else
            {
                var dataGridItem = this.SelectedGridViewItem;
                return dataGridItem != null
                       && !(dataGridItem is MaterialsClassification)
                       && !(dataGridItem is MachinesClassification);
            }
        }

        /// <summary>
        /// Notifies the client(s) that a master data item has been selected.
        /// </summary>
        private void Select()
        {
            if (!CanSelect())
            {
                return;
            }

            // Selects the item according to the current tab opened.
            object item;
            if (this.SelectedViewIndex == TreeViewIndex)
            {
                item = this.SelectedTreeItem.DataObject;
            }
            else if (this.SelectedViewIndex == GridViewIndex)
            {
                item = this.SelectedGridViewItem;
            }
            else
            {
                throw new InvalidOperationException("The TabControl SelectedIndex is not set.");
            }

            if (item != null
                && item.GetType() == this.MasterDataType)
            {
                var handler = this.MasterDataSelected;
                if (handler != null)
                {
                    handler(item, this.masterDataSource);
                }

                this.Close();
            }
        }

        /// <summary>
        /// Determines whether the UndoSelection command can execute.
        /// </summary>
        /// <returns>
        /// true if the command can execute; otherwise it returns false.
        /// </returns>
        private bool CanUndoSelection()
        {
            return this.undoSelectionItems != null && this.undoSelectionItems.Count > 1;
        }

        /// <summary>
        /// Select the previously selected item from MasterData TreeView.
        /// </summary>
        private void UndoSelection()
        {
            this.isUndoRedoAction = true;

            try
            {
                if (undoSelectionItems.Count > 1)
                {
                    // Get the previous tree item from the list.
                    var previousTreeItem = this.undoSelectionItems.ElementAt(this.undoSelectionItems.Count - 2);
                    if (!this.MasterDataTreeItems.ContainsItemWithDataObject(previousTreeItem.DataObject))
                    {
                        // Stop the delay on filtering.
                        this.delayTreeFiltering = false;
                        this.TreeFilterText = string.Empty;
                        this.MasterDataTreeItems = this.PrepareMasterDataForDisplay(this.LoadedMasterData.Cast<INameable>().ToList());
                        this.UpdateUndoRedoCollections();
                        previousTreeItem = this.undoSelectionItems.ElementAt(this.undoSelectionItems.Count - 2);
                    }

                    this.redoSelectionItems.Add(this.undoSelectionItems.Last());
                    this.undoSelectionItems.Remove(this.undoSelectionItems.Last());
                    this.SelectedTreeItem = previousTreeItem;
                    this.SelectedTreeItem.Select();

                    // Enable again the delay on filtering.
                    this.delayTreeFiltering = true;
                }
            }
            finally
            {
                this.isUndoRedoAction = false;
            }
        }

        /// <summary>
        /// Determines whether RedoSelection command can execute.
        /// </summary>
        /// <returns>
        /// true if the command can execute; otherwise it returns false.
        /// </returns>
        private bool CanRedoSelection()
        {
            return this.redoSelectionItems != null && this.redoSelectionItems.Any();
        }

        /// <summary>
        /// Revert the previously undo selection action in MasterData TreeView.
        /// </summary>
        private void RedoSelection()
        {
            this.isUndoRedoAction = true;

            try
            {
                if (this.redoSelectionItems.Any())
                {
                    var previousTreeItem = this.redoSelectionItems.Last();
                    if (!this.MasterDataTreeItems.ContainsItemWithDataObject(previousTreeItem.DataObject))
                    {
                        // Stop the delay on filtering.
                        this.delayTreeFiltering = false;
                        this.TreeFilterText = string.Empty;
                        this.MasterDataTreeItems = this.PrepareMasterDataForDisplay(this.LoadedMasterData.Cast<INameable>().ToList());
                        this.UpdateUndoRedoCollections();
                        previousTreeItem = this.redoSelectionItems.Last();
                    }

                    this.SelectedTreeItem = previousTreeItem;
                    this.undoSelectionItems.Add(this.SelectedTreeItem);
                    this.redoSelectionItems.Remove(this.SelectedTreeItem);
                    this.SelectedTreeItem.Select();

                    // Enable again the delay on filtering.
                    this.delayTreeFiltering = true;
                }
            }
            finally
            {
                this.isUndoRedoAction = false;
            }
        }

        /// <summary>
        /// Closes the view associated with this instance.
        /// </summary>
        protected override void Close()
        {
            this.windowService.CloseViewWindow(this);
        }

        #endregion Commands handlers

        #region Helpers

        /// <summary>
        /// Update undo and redo tree items collections.
        /// </summary>
        private void UpdateUndoRedoCollections()
        {
            foreach (var item in this.MasterDataTreeItems)
            {
                var index = 0;
                while (index < this.undoSelectionItems.Count)
                {
                    var newItem = item.FindItemForDataObject(this.undoSelectionItems[index].DataObject);
                    if (newItem != null && this.undoSelectionItems[index] != newItem)
                    {
                        this.undoSelectionItems[index] = newItem;
                    }

                    index++;
                }

                index = 0;
                while (index < this.redoSelectionItems.Count)
                {
                    var newItem = item.FindItemForDataObject(this.redoSelectionItems[index].DataObject);
                    if (newItem != null && this.redoSelectionItems[index] != newItem)
                    {
                        this.redoSelectionItems[index] = newItem;
                    }

                    index++;
                }
            }
        }

        /// <summary>
        /// Ensure that the undo list size does not get bigger than <see cref="maxUndoLimit"/>.
        /// </summary>
        private void RefreshUndoStack()
        {
            if (maxUndoLimit.HasValue && maxUndoLimit.Value > 0)
            {
                while (maxUndoLimit.Value < this.undoSelectionItems.Count)
                {
                    this.undoSelectionItems.RemoveAt(0);
                }
            }
        }

        #endregion Helpers        
    }
}