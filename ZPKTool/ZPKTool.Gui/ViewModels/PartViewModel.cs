﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Input;
using ZPKTool.Business;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for editing and viewing a Part.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class PartViewModel : ViewModel<Part, IDataSourceManager>
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The model browser helper service.
        /// </summary>
        private IModelBrowserHelperService modelBrowserHelperService;

        /// <summary>
        /// A weak event listener for changes in the "DisplayVersionAndTimestamp" application setting.
        /// </summary>
        private WeakEventListener<PropertyChangedEventArgs> timestampVisibilitySettingChangedListener;

        /// <summary>
        /// The parent part of the raw part.
        /// </summary>
        private Part parentForRawPart;

        /// <summary>
        /// The type of this part (normal part or RawPart).
        /// </summary>
        private Type partType;

        /// <summary>
        /// The assembly weight measurement unit.
        /// </summary>
        private MeasurementUnit weightUnit;

        /// <summary>
        /// A value indicating whether to update the part process steps batch size or not;
        /// </summary>
        private bool updateProcessStepsBatchSize = false;

        /// <summary>
        /// The part original batch size value.
        /// </summary>
        private int? originalBatchSize = null;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        /// <summary>
        /// The UnitsAdapter handler, used to perform certain operations when some UnitsAdapter properties are updated.
        /// </summary>
        private UnitsAdapterUpdateHandler unitsAdapterHandler;

        #endregion Attributes

        /// <summary>
        /// Initializes a new instance of the <see cref="PartViewModel"/> class.
        /// </summary>
        /// <param name="messenger">The messenger.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="modelBrowserHelperService">The model browser helper service.</param>
        /// <param name="costRecalculationCloneManager">The cost recalculation clone manager.</param>
        /// <param name="manufacturerViewModel">The manufacturer view model.</param>
        /// <param name="documentsViewModel">The documents view model.</param>
        /// <param name="ohsettingsViewModel">The overhead settings view model.</param>
        /// <param name="countrySettingsViewModel">The country settings view model.</param>
        /// <param name="countryBrowser">The country and supplier browser.</param>
        /// <param name="masterDataBrowser">The master data browser.</param>
        /// <param name="mediaViewModel">The media view model.</param>
        /// <param name="unitsService">The units service.</param>
        [ImportingConstructor]
        public PartViewModel(
            IMessenger messenger,
            IWindowService windowService,
            IModelBrowserHelperService modelBrowserHelperService,
            ICostRecalculationCloneManager costRecalculationCloneManager,
            ManufacturerViewModel manufacturerViewModel,
            EntityDocumentsViewModel documentsViewModel,
            OverheadSettingsViewModel ohsettingsViewModel,
            CountrySettingsViewModel countrySettingsViewModel,
            CountryAndSupplierBrowserViewModel countryBrowser,
            MasterDataBrowserViewModel masterDataBrowser,
            MediaViewModel mediaViewModel,
            IUnitsService unitsService)
        {
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("manufacturerViewModel", manufacturerViewModel);
            Argument.IsNotNull("documentsViewModel", documentsViewModel);
            Argument.IsNotNull("ohsettingsViewModel", ohsettingsViewModel);
            Argument.IsNotNull("countrySettingsViewModel", countrySettingsViewModel);
            Argument.IsNotNull("countryBrowser", countryBrowser);
            Argument.IsNotNull("masterDataBrowser", masterDataBrowser);
            Argument.IsNotNull("mediaViewModel", mediaViewModel);
            Argument.IsNotNull("unitsService", unitsService);
            Argument.IsNotNull("costRecalculationCloneManager", costRecalculationCloneManager);

            this.messenger = messenger;
            this.windowService = windowService;
            this.modelBrowserHelperService = modelBrowserHelperService;
            this.ManufacturerViewModel = manufacturerViewModel;
            this.DocumentsViewModel = documentsViewModel;
            this.OverheadSettingsViewModel = ohsettingsViewModel;
            this.CountrySettingsViewModel = countrySettingsViewModel;
            this.CountryBrowser = countryBrowser;
            this.MasterDataBrowser = masterDataBrowser;
            this.MediaViewModel = mediaViewModel;
            this.MediaViewModel.Mode = MediaControlMode.MultipleImagesOrVideo;
            this.MediaViewModel.IsChild = true;
            this.unitsService = unitsService;
            this.CloneManager = costRecalculationCloneManager;

            this.InitializeCommands();
            this.InitializeProperties();
            this.InitializeUndoManager();
        }

        #region Commands

        /// <summary>
        /// Gets the command that opens the Master Data Browser. The expected parameter is the Type of master data to browse.
        /// </summary>
        public ICommand BrowseMasterDataCommand { get; private set; }

        /// <summary>
        /// Gets the command that updates the calculation variant to latest (newest) variant.
        /// </summary>
        public ICommand UpdateToLatestCalculationVariantCommand { get; private set; }

        /// <summary>
        /// Gets the command that updates the batch size for part process steps.
        /// </summary>
        public ICommand UpdateBatchSizeCommand { get; private set; }

        /// <summary>
        /// Gets the command that cancel the batch size update for part process steps.
        /// </summary>
        public ICommand CancelUpdateBatchSizeCommand { get; private set; }

        /// <summary>
        /// Gets or sets the command that save all changes in the nested view models back into their models.
        /// This command aggregates the SaveToModel commands of the nested view models.
        /// </summary>
        private CompositeCommand SaveNestedViewModels { get; set; }

        /// <summary>
        /// Gets or sets the command that cancels all changes of the nested view models.
        /// This command aggregates the Cancel commands of the nested view models.
        /// </summary>
        private CompositeCommand CancelNestedViewModels { get; set; }

        #endregion Commands

        #region Model Properties

        /// <summary>
        /// Gets the name of the part.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Name", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Name")]
        public DataProperty<string> Name { get; private set; }

        /// <summary>
        /// Gets the number of the part.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Number")]
        public DataProperty<string> Number { get; private set; }

        /// <summary>
        /// Gets the version of the part.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Version")]
        public DataProperty<decimal?> Version { get; private set; }

        /// <summary>
        /// Gets the part's version date.
        /// </summary>
        [UndoableProperty]
        [ExposesModelProperty("VersionDate")]
        public DataProperty<DateTime?> VersionDate { get; private set; }

        /// <summary>
        /// Gets the description of the part.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Description")]
        public DataProperty<string> Description { get; private set; }

        /// <summary>
        /// Gets the manufacturing country of the part.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_CountryName", ErrorMessageResourceType = typeof(LocalizedResources))]
        [ExposesModelProperty("ManufacturingCountry")]
        [UndoableProperty]
        public DataProperty<string> ManufacturingCountry { get; private set; }

        /// <summary>
        /// Gets the manufacturing state of the part.
        /// </summary>
        [UndoableProperty]
        [ExposesModelProperty("ManufacturingSupplier")]
        public DataProperty<string> ManufacturingSupplier { get; private set; }

        /// <summary>
        /// Gets the calculation status of the part.
        /// </summary>
        [UndoableProperty]
        [ExposesModelProperty("CalculationStatus")]
        public DataProperty<PartCalculationStatus?> CalculationStatus { get; private set; }

        /// <summary>
        /// Gets the calculation accuracy of the part.
        /// </summary>
        [UndoableProperty]
        [ExposesModelProperty("CalculationAccuracy", AffectsCost = true)]
        public DataProperty<PartCalculationAccuracy?> CalculationAccuracy { get; private set; }

        /// <summary>
        /// Gets the estimated cost of the part.
        /// </summary>
        [Required(ErrorMessageResourceName = "Part_EstimatedCostMissing", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("EstimatedCost", AffectsCost = true)]
        public DataProperty<decimal?> EstimatedCost { get; private set; }

        /// <summary>
        /// Gets the weight of the part.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Weight", AffectsCost = true)]
        public DataProperty<decimal?> Weight { get; private set; }

        /// <summary>
        /// Gets the manufacturing ratio of the part.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_ManufacturingRatio", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("ManufacturingRatio", AffectsCost = true)]
        public DataProperty<decimal?> ManufacturingRatio { get; private set; }

        /// <summary>
        /// Gets the calculation approach of the part.
        /// </summary>
        [UndoableProperty]
        [ExposesModelProperty("CalculationApproach")]
        public DataProperty<PartCalculationApproach?> CalculationApproach { get; private set; }

        /// <summary>
        /// Gets the calculation variant of the part.
        /// </summary>
        [UndoableProperty]
        [ExposesModelProperty("CalculationVariant", AffectsCost = true)]
        public DataProperty<string> CalculationVariant { get; private set; }

        /// <summary>
        /// Gets the the batch size of the part.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("BatchSizePerYear", AffectsCost = true)]
        public DataProperty<int?> BatchSize { get; private set; }

        /// <summary>
        /// Gets the yearly production quantity of the part.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("YearlyProductionQuantity", AffectsCost = true)]
        public DataProperty<int?> YearlyProductionQuantity { get; private set; }

        /// <summary>
        /// Gets the life time of the part.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("LifeTime", AffectsCost = true)]
        public DataProperty<int?> LifeTime { get; private set; }

        /// <summary>
        /// Gets the purchase price of the part.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("PurchasePrice", AffectsCost = true)]
        public DataProperty<decimal?> PurchasePrice { get; private set; }

        /// <summary>
        /// Gets the target price of the part.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("TargetPrice", AffectsCost = true)]
        public DataProperty<decimal?> TargetPrice { get; private set; }

        /// <summary>
        /// Gets the type of the delivery of the part.
        /// </summary>
        [ExposesModelProperty("DelivertType")]
        [UndoableProperty]
        public DataProperty<PartDeliveryType?> DeliveryType { get; private set; }

        /// <summary>
        /// Gets the Tooling Active flag of the part.
        /// </summary>
        [UndoableProperty]
        [ExposesModelProperty("SBMActive", AffectsCost = true)]
        public DataProperty<bool> ToolingActive { get; private set; }

        /// <summary>
        /// Gets the asset rate of the part.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("AssetRate", AffectsCost = true)]
        public DataProperty<decimal?> AssetRate { get; private set; }

        /// <summary>
        /// Gets the IsExternal flag of the part.
        /// </summary>
        [UndoableProperty]
        [ExposesModelProperty("IsExternal", AffectsCost = true)]
        public DataProperty<bool> IsExternal { get; private set; }

        /// <summary>
        /// Gets a value indicating whether SG&amp;A is enabled if the part is external.
        /// </summary>
        [UndoableProperty]
        [ExposesModelProperty("ExternalSGA", AffectsCost = true)]
        public DataProperty<bool> ExternalSGA { get; private set; }

        /// <summary>
        /// Gets the additional remarks of the part.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("AdditionalRemarks")]
        public DataProperty<string> AdditionalRemarks { get; private set; }

        /// <summary>
        /// Gets the additional description of the part.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("AdditionalDescription")]
        public DataProperty<string> AdditionalDescription { get; private set; }

        /// <summary>
        /// Gets the manufacturer description at part level.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("ManufacturerDescription")]
        public DataProperty<string> ManufacturerDescription { get; private set; }

        /// <summary>
        /// Gets the last change date/time of the part.
        /// </summary>
        [ExposesModelProperty("LastChangeTimestamp")]
        public DataProperty<DateTime?> LastChangeTimestamp { get; private set; }

        /// <summary>
        /// Gets the Calculator user of the part.
        /// </summary>
        [UndoableProperty]
        [ExposesModelProperty("CalculatorUser")]
        public DataProperty<User> Calculator { get; private set; }

        /// <summary>
        /// Gets the part country id.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("ManufacturingCountryId")]
        public DataProperty<Guid?> ManufacturingCountryId { get; private set; }

        #endregion Model Properties

        #region View Models

        /// <summary>
        /// Gets the Manufacturer view model.
        /// </summary>
        public ManufacturerViewModel ManufacturerViewModel { get; private set; }

        /// <summary>
        /// Gets the documents management view model.
        /// </summary>
        public EntityDocumentsViewModel DocumentsViewModel { get; private set; }

        /// <summary>
        /// Gets the Overhead Settings view model.
        /// </summary>
        [SubViewModelProperty(ModelPropertyAssociated = "OverheadSettings")]
        public OverheadSettingsViewModel OverheadSettingsViewModel { get; private set; }

        /// <summary>
        /// Gets the Country Settings view model.
        /// </summary>
        [SubViewModelProperty(ModelPropertyAssociated = "CountrySettings")]
        public CountrySettingsViewModel CountrySettingsViewModel { get; private set; }

        /// <summary>
        /// Gets the country browser.
        /// </summary>
        public CountryAndSupplierBrowserViewModel CountryBrowser { get; private set; }

        /// <summary>
        /// Gets the master data browser.
        /// </summary>
        public MasterDataBrowserViewModel MasterDataBrowser { get; private set; }

        /// <summary>
        /// Gets the media view model.
        /// </summary>
        public MediaViewModel MediaViewModel { get; private set; }

        #endregion View Models

        #region Other Properties

        /// <summary>
        /// Gets or sets the project to which the Model Part belongs.
        /// This property is needed during cost calculations so it must be set when editing a Part (it can be omitted when creating a part).
        /// </summary>
        public Project ParentProject { get; set; }

        /// <summary>
        /// Gets or sets the type of the part.
        /// </summary>
        /// <value>
        /// The type of the part.
        /// </value>
        public Type PartType
        {
            get { return this.partType; }
            set { this.SetProperty(ref this.partType, value, () => this.PartType); }
        }

        /// <summary>
        /// Gets the set of available calculation variants.
        /// </summary>
        public Collection<string> CalculationVariants { get; private set; }

        /// <summary>
        /// Gets the text label associated with the EstimatedCost.
        /// </summary>
        public VMProperty<string> EstimatedCostLabel { get; private set; }

        /// <summary>
        /// Gets a value indicating whether to show the estimated cost field.
        /// </summary>
        public VMProperty<bool> ShowEstimatedCost { get; private set; }

        /// <summary>
        /// Gets a value indicating whether to show the weight field.
        /// </summary>
        public VMProperty<bool> ShowWeight { get; private set; }

        /// <summary>
        /// Gets a value indicating whether to show the manufacturing ratio field.
        /// </summary>
        public VMProperty<bool> ShowManufacturingRatio { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the current value of the CalculationVariant property represents the latest calculation variant.
        /// </summary>
        public VMProperty<bool> IsLatestCalculationVariant { get; private set; }

        /// <summary>
        /// Gets the target number of parts to be created over the lifetime of the Part.
        /// </summary>
        public VMProperty<decimal> PartsTotal { get; private set; }

        /// <summary>
        /// Gets the actual number of parts that will be created over the lifetime of the Part (including rejected ones).
        /// </summary>
        public VMProperty<decimal> NeededPartsTotal { get; private set; }

        /// <summary>
        /// Gets indicating whether to show the date/time of the last change applied to the Part.
        /// </summary>
        public VMProperty<bool> ShowLastChangeDateTime { get; private set; }

        /// <summary>
        /// Gets the list of users to display on the UI.
        /// </summary>
        public VMProperty<Collection<User>> Users { get; private set; }

        /// <summary>
        /// Gets indicating whether to show or not the batch size update warning.
        /// </summary>
        public VMProperty<bool> DisplayBatchSizeWarning { get; private set; }

        /// <summary>
        /// Gets the updated batch size value for the part process steps.
        /// </summary>
        public VMProperty<int?> BatchSizeUpdated { get; private set; }

        /// <summary>
        /// Gets or sets the assembly weight measurement unit.
        /// </summary>
        [UndoableProperty]
        public MeasurementUnit WeightUnit
        {
            get
            {
                return this.weightUnit;
            }

            set
            {
                if (this.weightUnit != value)
                {
                    OnPropertyChanging(() => this.WeightUnit);
                    this.weightUnit = value;
                    OnPropertyChanged(() => this.WeightUnit);
                }
            }
        }

        /// <summary>
        /// Gets or sets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter { get; set; }

        #endregion Other Properties

        #region Initialization

        /// <summary>
        /// Initializes the view-model's commands.
        /// </summary>
        private void InitializeCommands()
        {
            this.BrowseMasterDataCommand = new DelegateCommand<Type>(this.BrowseMasterData, this.CanBrowseMasterData);
            this.UpdateToLatestCalculationVariantCommand = new DelegateCommand(
                () => this.CalculationVariant.Value = CostCalculatorFactory.LatestVersion,
                () => !this.IsReadOnly);

            this.UpdateBatchSizeCommand = new DelegateCommand(this.UpdateBatchSizeAction);

            this.SaveNestedViewModels = new CompositeCommand();
            this.SaveNestedViewModels.RegisterCommand(this.ManufacturerViewModel.SaveToModelCommand);
            this.SaveNestedViewModels.RegisterCommand(this.DocumentsViewModel.SaveCommand);
            this.SaveNestedViewModels.RegisterCommand(this.OverheadSettingsViewModel.SaveToModelCommand);
            this.SaveNestedViewModels.RegisterCommand(this.CountrySettingsViewModel.SaveToModelCommand);

            this.CancelNestedViewModels = new CompositeCommand();
            this.CancelNestedViewModels.RegisterCommand(this.ManufacturerViewModel.CancelCommand);
            this.CancelNestedViewModels.RegisterCommand(this.DocumentsViewModel.CancelCommand);
            this.CancelNestedViewModels.RegisterCommand(this.OverheadSettingsViewModel.CancelCommand);
            this.CancelNestedViewModels.RegisterCommand(this.CountrySettingsViewModel.CancelCommand);
            this.CancelNestedViewModels.RegisterCommand(this.MediaViewModel.CancelCommand);
        }

        /// <summary>
        /// Initializes the properties of this instance.
        /// </summary>
        private void InitializeProperties()
        {
            // Initialize the child view-models
            this.ManufacturerViewModel.ShowSaveControls = false;
            this.ManufacturerViewModel.IsChild = true;

            this.OverheadSettingsViewModel.CheckForUpdate = false;
            this.OverheadSettingsViewModel.ShowSaveControls = false;
            this.OverheadSettingsViewModel.IsChild = true;

            this.CountrySettingsViewModel.ShowSaveControls = false;
            this.CountrySettingsViewModel.IsReadOnly = true; // Always read-only
            this.CountrySettingsViewModel.AllowToApplyUpdateWhenReadOnly = true;
            this.CountrySettingsViewModel.IsChild = true;

            // Initialize the other properties.
            this.CalculationVariants = new Collection<string>(CostCalculatorFactory.CalculationVersions);
            this.Users.Value = new Collection<User>();

            this.PropertyChanged += this.OnPropertyChanged;
            this.CalculationAccuracy.ValueChanged += (s, e) => this.OnCalculationAccuracyChanged();
            this.CalculationVariant.ValueChanged += (s, e) => this.OnCalculationVariantChanged();
            this.IsExternal.ValueChanged += (s, e) => this.OnIsExternalChanged();

            this.ShowLastChangeDateTime.Value = UserSettingsManager.Instance.DisplayVersionAndTimestamp;
            timestampVisibilitySettingChangedListener = new WeakEventListener<PropertyChangedEventArgs>((s, e) =>
            {
                this.ShowLastChangeDateTime.Value = UserSettingsManager.Instance.DisplayVersionAndTimestamp;
            });
            PropertyChangedEventManager.AddListener(UserSettingsManager.Instance, timestampVisibilitySettingChangedListener, ReflectionUtils.GetPropertyName(() => UserSettingsManager.Instance.DisplayVersionAndTimestamp));

            this.DisplayBatchSizeWarning.Value = false;
        }

        /// <summary>
        /// Initialize the undo manager.
        /// </summary>
        private void InitializeUndoManager()
        {
            this.ManufacturerViewModel.UndoManager = this.UndoManager;
            this.MediaViewModel.UndoManager = this.UndoManager;
            this.DocumentsViewModel.UndoManager = this.UndoManager;
            this.OverheadSettingsViewModel.UndoManager = this.UndoManager;
            this.CountrySettingsViewModel.UndoManager = this.UndoManager;
        }

        /// <summary>
        /// Initializes the view-model for the creation of a new Part.
        /// The ModelDataContext property must be set to a valid value before this call.
        /// </summary>
        /// <param name="isMasterData">If set to true the Part will be created in master data; in this case the parent should be set to null.</param>
        /// <param name="partParent">The parent object of the Part that will be created. If this is set the value of the <paramref name="isMasterData"/> argument is ignored and
        /// the parent's IsMasterData flag is used instead.</param>
        /// <param name="isRawPart">if set to <c>true</c> [is raw part].</param>
        /// <exception cref="ArgumentException">
        ///   <paramref name="partParent"/> is null and <paramref name="isMasterData"/> is false, or the type of<paramref name="partParent"/> is not supported.
        /// </exception>
        /// <exception cref="InvalidOperationException">ModelDataContext is not set.</exception>
        /// <remarks>
        /// This is a helper method for populating the model with default data for creation. It is not mandatory to use it; you can set the model to a new
        /// instance set up however you want.
        /// </remarks>
        public void InitializeForCreation(bool isMasterData, object partParent, bool isRawPart = false)
        {
            this.CheckDataSource();
            this.EditMode = ViewModelEditMode.Create;

            Part newPart = isRawPart ? new RawPart() : new Part();

            if (!isMasterData)
            {
                if (partParent == null)
                {
                    throw new ArgumentException("A non master data Part cannot be created without a parent.", "partParent");
                }

                // Extract from the parent the data to be used for defaults when creating the Part.
                string parentCountry = null;
                string parentState = null;
                Guid? parentCountryId = null;
                int newPartIndex = 0;

                Assembly parentAssy = partParent as Assembly;
                if (parentAssy != null)
                {
                    parentCountry = parentAssy.AssemblingCountry;
                    parentState = parentAssy.AssemblingSupplier;
                    parentCountryId = parentAssy.AssemblingCountryId ?? Guid.Empty;

                    newPart.YearlyProductionQuantity = parentAssy.YearlyProductionQuantity;
                    newPart.LifeTime = parentAssy.LifeTime;
                    isMasterData = parentAssy.IsMasterData;

                    newPart.Assembly = parentAssy;
                    if (parentAssy.OverheadSettings != null)
                    {
                        newPart.OverheadSettings = parentAssy.OverheadSettings.Copy();
                    }

                    // Determine the position of the part to create in its parent Assembly.
                    Assembly parentAssyWithChildren = this.DataSourceManager.AssemblyRepository.GetAssemblyWithTopParts(parentAssy.Guid);
                    if (parentAssyWithChildren != null)
                    {
                        newPartIndex = parentAssyWithChildren.Parts.Max(a => a.Index).GetValueOrDefault(-1) + 1;
                    }
                }
                else
                {
                    Project parentProj = partParent as Project;
                    if (parentProj != null)
                    {
                        if (parentProj.Customer != null)
                        {
                            parentCountry = parentProj.Customer.Country;
                            parentState = parentProj.Customer.State;
                        }

                        newPart.YearlyProductionQuantity = parentProj.YearlyProductionQuantity;
                        newPart.LifeTime = parentProj.LifeTime;
                        isMasterData = false;

                        newPart.Project = parentProj;
                        if (parentProj.OverheadSettings != null)
                        {
                            newPart.OverheadSettings = parentProj.OverheadSettings.Copy();
                        }

                        // Determine the position of the Part to create in its parent project.
                        Project parentProjWithChildren = this.DataSourceManager.ProjectRepository.GetProjectIncludingTopLevelChildren(parentProj.Guid, true);
                        if (parentProjWithChildren != null)
                        {
                            newPartIndex = parentProjWithChildren.Parts.Max(a => a.Index).GetValueOrDefault(-1) + 1;
                        }
                    }
                    else
                    {
                        parentForRawPart = partParent as Part;
                        if (parentForRawPart != null)
                        {
                            parentCountry = parentForRawPart.ManufacturingCountry;
                            parentState = parentForRawPart.ManufacturingSupplier;
                            parentCountryId = parentForRawPart.ManufacturingCountryId;

                            newPart.YearlyProductionQuantity = parentForRawPart.YearlyProductionQuantity;
                            newPart.LifeTime = parentForRawPart.LifeTime;
                            isMasterData = parentForRawPart.IsMasterData;
                            newPart.ParentOfRawPart.Add(parentForRawPart);

                            if (parentForRawPart.OverheadSettings != null)
                            {
                                newPart.OverheadSettings = parentForRawPart.OverheadSettings.Copy();
                            }
                        }
                        else
                        {
                            string message = string.Format("The type '{0}' is not supported as parent of a Part.", partParent.GetType().Name);
                            throw new ArgumentException(message, "partParent");
                        }
                    }
                }

                newPart.Index = newPartIndex;

                try
                {
                    // Use the country settings of the parent's country or state as default.
                    var country = this.DataSourceManager.CountryRepository.ResolveCountry(parentCountryId ?? Guid.Empty, parentCountry);
                    if (country != null)
                    {
                        newPart.ManufacturingCountry = country.Name;
                        newPart.ManufacturingCountryId = country.Guid;
                        if (string.IsNullOrWhiteSpace(parentState))
                        {
                            newPart.CountrySettings = country.CountrySetting.Copy();
                        }
                        else
                        {
                            CountryState state = country.States.FirstOrDefault(s => s.Name == parentState);
                            if (state != null)
                            {
                                newPart.ManufacturingSupplier = state.Name;
                                newPart.CountrySettings = state.CountrySettings.Copy();
                            }
                            else
                            {
                                newPart.ManufacturingSupplier = null;
                                newPart.CountrySettings = country.CountrySetting.Copy();
                            }
                        }
                    }
                    else
                    {
                        newPart.ManufacturingCountry = null;
                        newPart.ManufacturingSupplier = null;
                        newPart.ManufacturingCountryId = null;
                    }

                    // Convert the country setting from euro to the parent project's base currency.
                    if (newPart.CountrySettings != null)
                    {
                        CurrencyConversionManager.ConvertObject(newPart.CountrySettings, this.unitsService.BaseCurrency, CurrencyConversionManager.DefaultBaseCurrency);
                    }
                }
                catch (DataAccessException ex)
                {
                    log.ErrorException("A data access error occurred while initializing the creation of a Part.", ex);
                }
            }

            // Use the basic settings for some defaults.
            try
            {
                BasicSetting settings = this.DataSourceManager.BasicSettingsRepository.GetBasicSettings();
                newPart.BatchSizePerYear = settings.PartBatch;
                newPart.AssetRate = settings.AssetRate;
            }
            catch (DataAccessException ex)
            {
                log.ErrorException("Failed to load the basic settings.", ex);
            }

            // Initialize the properties for which no condition is required.
            newPart.CalculationAccuracy = PartCalculationAccuracy.FineCalculation;
            newPart.CalculationApproach = null;
            newPart.CalculationVariant = CostCalculatorFactory.LatestVersion;
            newPart.DevelopmentCost = 0m;
            newPart.ProjectInvest = 0m;
            newPart.OtherCost = 0m;
            newPart.PackagingCost = 0m;
            newPart.LogisticCost = 0m;
            newPart.TransportCost = 0m;
            newPart.SBMActive = true;
            newPart.Manufacturer = new Manufacturer();
            newPart.Process = new Process();

            if (newPart.OverheadSettings == null)
            {
                newPart.OverheadSettings = new OverheadSetting();
            }

            if (newPart.CountrySettings == null)
            {
                newPart.CountrySettings = new CountrySetting();
            }

            // Set the master data flag and the owner at the end so they are applied to all sub-objects.
            newPart.SetIsMasterData(isMasterData);
            if (!newPart.IsMasterData)
            {
                User owner = this.DataSourceManager.UserRepository.GetById(SecurityManager.Instance.CurrentUser.Guid, false);
                newPart.SetOwner(owner);
                newPart.CalculatorUser = owner;
            }

            this.Model = newPart;
        }

        #endregion Initialization

        #region Property changed handlers

        /// <summary>
        /// Called when the Model has changed.
        /// </summary>
        protected override void OnModelChanged()
        {
            this.CheckDataSource();
            this.LoadDataFromModel();

            if (this.IsInViewerMode)
            {
                this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(null);
            }

            this.MediaViewModel.Mode =
                this.Model.IsMasterData ? MediaControlMode.MultipleImagesNoVideo : MediaControlMode.MultipleImagesOrVideo;

            this.PartType = this.Model.GetType();

            this.ManufacturerViewModel.DataSourceManager = this.DataSourceManager;
            this.ManufacturerViewModel.Model = this.Model.Manufacturer ?? new Manufacturer();

            if (this.Model.OverheadSettings == null)
            {
                this.Model.OverheadSettings = new OverheadSetting();
            }

            this.OverheadSettingsViewModel.DataSourceManager = this.DataSourceManager;
            this.OverheadSettingsViewModel.Model = this.Model.OverheadSettings ?? new OverheadSetting();

            this.CountrySettingsViewModel.SourceCountryName = this.Model.ManufacturingCountry;
            this.CountrySettingsViewModel.SourceCountrySupplierName = this.Model.ManufacturingSupplier;
            this.CountrySettingsViewModel.DataSourceManager = this.DataSourceManager;
            this.CountrySettingsViewModel.CheckForUpdate = !this.IsReadOnly;
            this.CountrySettingsViewModel.Model = this.Model.CountrySettings ?? new CountrySetting();

            this.MediaViewModel.DataSourceManager = this.DataSourceManager;
            this.MediaViewModel.Model = this.Model;

            this.DocumentsViewModel.ModelDataSourceManager = this.DataSourceManager;
            this.DocumentsViewModel.Model = this.Model;

            // If the screen is read-only, the calculator is added to the users list in order to be selected in the combo boxes. There is no point in adding all
            // the users if the Calculator can't be changed.
            if (this.IsReadOnly)
            {
                if (this.Model.CalculatorUser != null)
                {
                    this.Users.Value.Add(this.Model.CalculatorUser);
                }
            }
            else
            {
                // else the users are retrieved from the database in order to populate the combo box.
                this.Users.Value = this.DataSourceManager.UserRepository.GetAll(false);
            }

            if (this.Model.MeasurementUnit != null)
            {
                this.WeightUnit = this.Model.MeasurementUnit;
            }
            else if (!IsInViewerMode)
            {
                // Set default unit of the weight in case it is missing.
                this.WeightUnit = this.MeasurementUnitsAdapter.GetBaseMeasurementUnit(MeasurementUnitType.Weight);
            }

            if (this.Model.BatchSizePerYear.GetValueOrDefault() != 0)
            {
                var modelBatchSize = Math.Ceiling(Convert.ToDecimal(this.Model.YearlyProductionQuantity.GetValueOrDefault()) / Convert.ToDecimal(this.Model.BatchSizePerYear.GetValueOrDefault()));
                this.originalBatchSize = modelBatchSize < int.MaxValue ? (int)modelBatchSize : int.MaxValue;
            }
            else
            {
                this.originalBatchSize = null;
            }

            this.BatchSize.ValueChanged += (s, e) => this.OnBatchSizeUpdated();
            this.YearlyProductionQuantity.ValueChanged += (s, e) => this.OnBatchSizeUpdated();

            this.CalculatePartsProductionTotal(this.Model);
            this.IsChanged = false;

            this.UndoManager.Reset();
            this.UndoManager.Start();

            this.CloneManager.Clone(this);
        }

        /// <summary>
        /// Called when the DataSourceManager changed.
        /// </summary>
        protected override void OnDataSourceManagerChanged()
        {
            base.OnDataSourceManagerChanged();

            this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(this.DataSourceManager);
            this.unitsAdapterHandler = new UnitsAdapterUpdateHandler(this.MeasurementUnitsAdapter);
            this.unitsAdapterHandler.PauseUndoOnUnitsAdapterUpdate(this.UndoManager);
        }

        /// <summary>
        /// Called when the CalculationAccuracy property has changed.
        /// </summary>
        private void OnCalculationAccuracyChanged()
        {
            this.StopRecalculationNotifications();
            using (this.UndoManager.StartBatch(includePreviousItem: true, reverseUndoOrder: true, navigateToBatchControls: true, undoEachBatch: true))
            {
                // If the calculation accuracy is not Fine Calculation, show the estimated cost and set its label according to the accuracy value.
                PartCalculationAccuracy accuracy = this.CalculationAccuracy.Value.GetValueOrDefault(PartCalculationAccuracy.FineCalculation);
                if (accuracy != PartCalculationAccuracy.FineCalculation)
                {
                    this.UndoManager.RegisterProperty(() => this.EstimatedCost, this);
                    if (this.EstimatedCost.Value == 0)
                    {
                        this.EstimatedCost.Value = null;
                    }

                    this.ShowEstimatedCost.Value = true;

                    if (accuracy == PartCalculationAccuracy.Estimation || accuracy == PartCalculationAccuracy.RoughCalculation)
                    {
                        this.EstimatedCostLabel.Value = LocalizedResources.General_EstimatedCost;
                    }
                    else if (accuracy == PartCalculationAccuracy.OfferOrExternalCalculation)
                    {
                        this.EstimatedCostLabel.Value = LocalizedResources.PartCalculationAccuracy_OfferOrExternalCalculation;
                    }
                }
                else
                {
                    this.EstimatedCost.Value = 0m;
                    this.EstimatedCost.AcceptChanges();
                    this.UndoManager.UnregisterProperty(() => this.EstimatedCost, this);

                    this.ShowEstimatedCost.Value = false;
                }

                this.ShowAdditionalFields();
            }

            this.ResumeRecalculationNotifications(true);
        }

        /// <summary>
        /// Called when the CalculationVariant property has changed.
        /// </summary>
        private void OnCalculationVariantChanged()
        {
            this.IsLatestCalculationVariant.Value = this.CalculationVariant.Value == CostCalculatorFactory.LatestVersion;

            this.ShowAdditionalFields();
        }

        /// <summary>
        /// Called when the IsExternal property has changed.
        /// </summary>
        private void OnIsExternalChanged()
        {
            if (!this.IsExternal.Value)
            {
                using (this.UndoManager.StartBatch(includePreviousItem: true, navigateToBatchControls: true, undoEachBatch: true))
                {
                    this.ExternalSGA.Value = false;
                }
            }
        }

        /// <summary>
        /// Shows or hides additional fields depending on the accuracy and calculation variant.
        /// </summary>
        private void ShowAdditionalFields()
        {
            PartCalculationAccuracy accuracy = this.CalculationAccuracy.Value.GetValueOrDefault(PartCalculationAccuracy.FineCalculation);

            // If the calculation accuracy is not Fine Calculation and the calculation variant is 1.3 or newer, show the weight.
            if (accuracy != PartCalculationAccuracy.FineCalculation && CostCalculatorFactory.IsNewer(this.CalculationVariant.Value, "1.2"))
            {
                this.ShowWeight.Value = true;
            }
            else
            {
                this.ShowWeight.Value = false;
            }

            // If the calculation accuracy is Estimation or Rough Calculation and the calculation variant is 1.1 or newer, show the manufacturing ratio.
            if ((accuracy == PartCalculationAccuracy.Estimation || accuracy == PartCalculationAccuracy.RoughCalculation) && CostCalculatorFactory.IsNewer(this.CalculationVariant.Value, "1.0"))
            {
                this.UndoManager.RegisterProperty(() => this.ManufacturingRatio, this);
                this.ShowManufacturingRatio.Value = true;
            }
            else
            {
                this.ManufacturingRatio.Value = Part.DefaultManufacturingRatio;
                this.ManufacturingRatio.AcceptChanges();
                this.UndoManager.UnregisterProperty(() => this.ManufacturingRatio, this);

                this.ShowManufacturingRatio.Value = false;
            }
        }

        /// <summary>
        /// Action performed when assembly batch size or yearly production quantity has changed. 
        /// Determine whether the batch size update warning should be displayed or not.
        /// </summary>
        private void OnBatchSizeUpdated()
        {
            if (this.EditMode != ViewModelEditMode.Create)
            {
                if (this.YearlyProductionQuantity.IsChanged)
                {
                    this.DisplayBatchSizeWarning.Value = true;
                }
                else
                {
                    this.DisplayBatchSizeWarning.Value = this.originalBatchSize == this.GetCurrentBatchSize() ||
                        (this.updateProcessStepsBatchSize && this.BatchSizeUpdated.Value == this.GetCurrentBatchSize()) ? false : true;
                }
            }
        }

        /// <summary>
        /// Handles the PropertyChanged event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsReadOnly")
            {
                this.CountrySettingsViewModel.AllowToApplyUpdateWhenReadOnly = !this.IsReadOnly;
                this.CountrySettingsViewModel.CheckForUpdate = !this.IsReadOnly;
                if (this.IsReadOnly)
                {
                    // Hide updates if the part view-model is read-only.
                    this.CountrySettingsViewModel.UpdateAvailable.Value = false;
                }
            }
        }

        #endregion Property changed handlers

        #region Save/Cancel

        /// <summary>
        /// Loads the data from the model.
        /// Each view-model property decorated with the ExposedModelProperty attribute is loaded from the associated Model property.
        /// </summary>
        /// <param name="model">The model instance.</param>
        public override void LoadDataFromModel(Part model)
        {
            base.LoadDataFromModel(model);
            this.ManufacturingCountryId.Value = model.ManufacturingCountryId;

            // If the estimated cost is not displayed, set a valid value in its property to avoid the validation error.
            if (this.CalculationAccuracy.Value.GetValueOrDefault(PartCalculationAccuracy.FineCalculation) == PartCalculationAccuracy.FineCalculation)
            {
                using (this.UndoManager.Pause())
                {
                    this.EstimatedCost.Value = 0m;
                }
            }

            // If the manufacturing ratio is not displayed, set the default value of the part in its property to avoid validation error.
            if (!this.ShowManufacturingRatio.Value)
            {
                using (this.UndoManager.Pause())
                {
                    this.ManufacturingRatio.Value = Part.DefaultManufacturingRatio;
                }
            }

            // If the model's Calculation Variant is invalid use the oldest variant for compatibility reasons.
            if (!CostCalculatorFactory.IsValidVersion(this.Model.CalculationVariant))
            {
                this.CalculationVariant.Value = CostCalculatorFactory.OldestVersion;
            }
        }

        /// <summary>
        /// Saves all changed back into the model and resets the changed status. This method is executed by the SaveToModelCommand command.
        /// </summary>
        protected override void SaveToModel()
        {
            this.CheckModel();

            this.LastChangeTimestamp.Value = DateTime.Now;

            if (this.ShowWeight.Value)
            {
                this.Model.MeasurementUnit = this.WeightUnit;
            }
            else
            {
                // Set the weight to null if the field is not displayed.                
                this.Weight.Value = null;
            }

            if (this.updateProcessStepsBatchSize)
            {
                this.originalBatchSize = this.BatchSizeUpdated.Value;
                foreach (var step in this.Model.Process.Steps)
                {
                    // Update process steps batch size.
                    step.BatchSize = this.BatchSizeUpdated.Value;
                }

                if (this.Model.RawPart != null)
                {
                    // Update the raw part's yearly production quantity.
                    this.Model.RawPart.YearlyProductionQuantity = this.YearlyProductionQuantity.Value;

                    foreach (var rawPartStep in this.Model.RawPart.Process.Steps)
                    {
                        // Set the new value for the raw part's process steps.
                        rawPartStep.BatchSize = this.BatchSizeUpdated.Value;
                    }
                }
            }

            base.SaveToModel();

            var manufacturer = this.ManufacturerViewModel.Model;
            if (manufacturer != null)
            {
                this.Model.Manufacturer = manufacturer;
            }

            var overheadSettings = this.OverheadSettingsViewModel.Model;
            if (overheadSettings != null)
            {
                this.Model.OverheadSettings = overheadSettings;
            }

            var countrySettings = this.CountrySettingsViewModel.Model;
            if (countrySettings != null)
            {
                this.Model.CountrySettings = countrySettings;
            }

            if (this.Model.CalculationAccuracy == PartCalculationAccuracy.FineCalculation)
            {
                // Set the estimated cost to null because it was set to zero to avoid its validation error.
                this.Model.EstimatedCost = null;
            }

            if (!this.ShowManufacturingRatio.Value)
            {
                // Set the manufacturing ratio in the model to null and the field to the default value of the part if the field is not displayed.
                this.Model.ManufacturingRatio = null;
                this.ManufacturingRatio.Value = Part.DefaultManufacturingRatio;
            }

            this.CalculatePartsProductionTotal(this.Model);
        }

        /// <summary>
        /// Determines whether this instance can perform the save operation. Executed by the SaveCommand.
        /// The default implementation allows the save to be performed if the input is valid.
        /// </summary>
        /// <returns>
        /// true if the save operation can be performed, false otherwise.
        /// </returns>
        protected override bool CanSave()
        {
            return base.CanSave() && this.SaveNestedViewModels.CanExecute(null);
        }

        /// <summary>
        /// Performs the save operation. Executed by the SaveCommand.
        /// </summary>
        protected override void Save()
        {
            this.CheckModelAndDataSource();

            // Determine if a new calculation variant was selected in order to set its new value in the raw part.
            bool calcVariantChanged = CalculationVariant.Value != this.Model.CalculationVariant;

            // Save all changes back into the Model objects. The validity check of this operation is performed by the CanSave method.
            this.SaveToModel();
            this.SaveNestedViewModels.Execute(null);

            // Set the new calculation variant in the raw part.
            if (calcVariantChanged)
            {
                this.Model.SetCalculationVariant(CalculationVariant.Value);
            }

            // Update the Part
            this.DataSourceManager.PartRepository.Save(this.Model);
            this.DataSourceManager.SaveChanges();

            // Save Media.
            this.MediaViewModel.SaveCommand.Execute(null);

            this.RefreshCalculations(this.Model);

            // Notify the other components that the Part was created/updated.
            EntityChangedMessage message = this.Model.IsMasterData ?
                new EntityChangedMessage(Notification.MasterDataEntityChanged) :
                new EntityChangedMessage(Notification.MyProjectsEntityChanged);

            message.ChangeType = this.EditMode == ViewModelEditMode.Create ? EntityChangeType.EntityCreated : EntityChangeType.EntityUpdated;
            message.Entity = this.Model;
            if (this.Model.Assembly != null)
            {
                message.Parent = this.Model.Assembly;
            }
            else
            {
                if (this.Model.Project != null)
                {
                    message.Parent = this.Model.Project;
                }
                else
                {
                    if (this.parentForRawPart != null)
                    {
                        message.Parent = parentForRawPart;
                    }
                }
            }

            this.messenger.Send(message);

            // Refresh the last update time.
            this.CountrySettingsViewModel.SetLastUpdateTime();

            // Close the view-model when is displayed in a window.
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Determines whether the Cancel operation can be performed. Executed by the CancelCommand.
        /// </summary>
        /// <returns>
        /// true if the changes can be canceled, false otherwise.
        /// </returns>
        protected override bool CanCancel()
        {
            return base.CanCancel() && this.CancelNestedViewModels.CanExecute(null) && this.DocumentsViewModel.IsLoaded;
        }

        /// <summary>
        /// Cancels all changes. Executed by the CancelCommand.
        /// </summary>
        protected override void Cancel()
        {
            if (!this.CanCancel())
            {
                return;
            }

            this.updateProcessStepsBatchSize = false;

            if (this.IsChanged)
            {
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_CancelChanges, MessageDialogType.YesNo);
                if (result != MessageDialogResult.Yes)
                {
                    // Don't cancel the changes and also don't close the view-model.
                    return;
                }
                else
                {
                    using (this.UndoManager.StartBatch())
                    {
                        // Cancel all changes
                        base.Cancel();
                        this.CancelNestedViewModels.Execute(null);
                    }
                }
            }

            // Close the view-model when is displayed in a window.
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Called before unloading the view from its parent. Returning false will cancel the view's unloading.
        /// </summary>
        /// <returns>
        /// True if the unloading process should continue and false if it should be canceled.
        /// </returns>
        public override bool OnUnloading()
        {
            // Do nothing if the view-model is read-only, in viewer mode, it was not changed or the model was deleted.
            if (this.IsReadOnly || this.IsInViewerMode || !this.IsChanged || this.Model.IsDeleted)
            {
                return true;
            }

            if (this.EditMode == ViewModelEditMode.Create)
            {
                // Ask the user to confirm quitting
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_Quit, MessageDialogType.YesNo);
                if (result != MessageDialogResult.Yes)
                {
                    // The user chose to stay on the screen; return false to stop the screen unloading.
                    return false;
                }
                else
                {
                    this.IsChanged = false;
                }
            }
            else if (this.EditMode == ViewModelEditMode.Edit)
            {
                // Ask the user if he wants to save
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_UnsavedDataOnQuit, MessageDialogType.YesNoCancel);
                if (result == MessageDialogResult.Yes)
                {
                    // The user whishes to save but it is not possible because the input is not valid. Return false to stop the view-model from unloading.
                    if (!this.CanSave())
                    {
                        return false;
                    }

                    this.Save();
                }
                else if (result == MessageDialogResult.No)
                {
                    // The user does not want to save.                    
                    this.IsChanged = false;
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Called after one or more model properties value changed in order to refresh the calculations.
        /// </summary>
        protected override void RefreshCalculation()
        {
            // Updates the total parts when the view model edit mode is <see cref="ViewModelEditMode.Create"/>
            // When creating a new part the recalculation mechanism is disabled because the part that is created
            // is not linked to any entities and the total part calculation must be made manually
            if (this.EditMode == ViewModelEditMode.Create)
            {
                // Create a part with the values that influence the total part calculation
                var tempPart = new Part();
                tempPart.CalculationVariant = this.CalculationVariant.Value;
                tempPart.BatchSizePerYear = this.BatchSize.Value;
                tempPart.YearlyProductionQuantity = this.YearlyProductionQuantity.Value;
                tempPart.LifeTime = this.LifeTime.Value;
                this.CalculatePartsProductionTotal(tempPart);

                return;
            }

            // Set the new calculation variant in the raw part.
            this.ModelClone.SetCalculationVariant(this.ModelClone.CalculationVariant);

            this.CalculatePartsProductionTotal(this.ModelClone);
            this.RefreshCalculations(this.ModelClone);
        }

        #endregion Save/Cancel

        /// <summary>
        /// Calculates the part and sends a message with the result.
        /// </summary>
        /// <param name="part">The part.</param>
        private void RefreshCalculations(Part part)
        {
            if (part == null)
            {
                return;
            }

            PartCostCalculationParameters calculationParams = null;
            if (this.ParentProject != null)
            {
                calculationParams = CostCalculationHelper.CreatePartParamsFromProject(this.ParentProject);
            }
            else if (IsInViewerMode)
            {
                // Note: theoretically this part is never reached because it's not possible to save in view mode.
                calculationParams = this.modelBrowserHelperService.GetPartCostCalculationParameters();
            }

            if (calculationParams != null)
            {
                var calculator = CostCalculatorFactory.GetCalculator(part.CalculationVariant);
                var result = calculator.CalculatePartCost(part, calculationParams);
                this.messenger.Send(new CurrentComponentCostChangedMessage(result), IsInViewerMode ? GlobalMessengerTokens.ModelBrowserTargetToken : GlobalMessengerTokens.MainViewTargetToken);
            }
        }

        #region Master Data Browse handling

        /// <summary>
        /// Determines whether this instance can browse the specified master data type.
        /// </summary>
        /// <param name="masterDataType">Type of the master data.</param>
        /// <returns>
        /// true if the specified master data type can be browsed; otherwise, false.
        /// </returns>
        private bool CanBrowseMasterData(Type masterDataType)
        {
            return !this.IsReadOnly;
        }

        /// <summary>
        /// Browses the specified type of master data using the master data browser.
        /// </summary>
        /// <param name="masterDataType">Type of the master data.</param>
        private void BrowseMasterData(Type masterDataType)
        {
            if (masterDataType == PartType)
            {
                this.MasterDataBrowser.MasterDataType = PartType;
                this.MasterDataBrowser.MasterDataSelected += this.OnMasterDataPartSelected;

                this.windowService.ShowViewInDialog(this.MasterDataBrowser);
                this.MasterDataBrowser.MasterDataSelected -= this.OnMasterDataPartSelected;
            }
            else if (masterDataType == typeof(Country))
            {
                this.CountryBrowser.CountryName = null;
                this.CountryBrowser.LoadLocation = this.DataSourceManager.DatabaseId;

                this.CountryBrowser.CountryOrSupplierSelected += this.OnCountryOrSupplierSelected;
                this.windowService.ShowViewInDialog(this.CountryBrowser);
                this.CountryBrowser.CountryOrSupplierSelected -= this.OnCountryOrSupplierSelected;
            }
            else if (masterDataType == typeof(CountryState))
            {
                if (!string.IsNullOrEmpty(this.ManufacturingCountry.Value))
                {
                    // check if the country name is not changed
                    string countryName = this.ManufacturingCountry.Value;
                    Guid? countryId = this.ManufacturingCountryId.Value;
                    var country = this.DataSourceManager.CountryRepository.ResolveCountry(countryId ?? Guid.Empty, countryName);
                    if (country != null && !countryName.Equals(country.Name))
                    {
                        this.CountryBrowser.CountryName = country.Name;
                    }
                    else
                    {
                        this.CountryBrowser.CountryName = this.ManufacturingCountry.Value;
                    }

                    this.CountryBrowser.LoadLocation = this.DataSourceManager.DatabaseId;
                    this.CountryBrowser.CountryOrSupplierSelected += this.OnCountryOrSupplierSelected;
                    this.windowService.ShowViewInDialog(this.CountryBrowser);
                    this.CountryBrowser.CountryOrSupplierSelected -= this.OnCountryOrSupplierSelected;
                }
                else
                {
                    this.windowService.MessageDialogService.Show(LocalizedResources.General_SelectCountryFirst, MessageDialogType.Error);
                }
            }
        }

        /// <summary>
        /// Called when a master data part is selected from the master data browser.
        /// </summary>
        /// <param name="masterDataEntity">The master data part selected in the browser.</param>
        /// <param name="databaseId">The source database of the selected master data.</param>
        private void OnMasterDataPartSelected(object masterDataEntity, DbIdentifier databaseId)
        {
            Part masterPart = masterDataEntity as Part;
            if (masterPart == null)
            {
                return;
            }

            this.StopRecalculationNotifications();
            using (this.UndoManager.StartBatch(undoEachBatch: true))
            {
                // Retrieve the media of the part master data.
                var dataManager = DataAccessFactory.CreateDataSourceManager(databaseId);
                MediaManager mediaManager = new MediaManager(dataManager);
                var allMedia = mediaManager.GetAllMedia(masterPart);
                var mediaList = new List<Media>();
                var documentsList = new List<Media>();
                foreach (var media in allMedia)
                {
                    if ((MediaType)media.Type != MediaType.Document)
                    {
                        mediaList.Add(media.Copy());
                    }
                    else
                    {
                        documentsList.Add(media.Copy());
                    }
                }

                // Load the part master data media in Media view-model.
                this.MediaViewModel.LoadMedia(mediaList);

                this.DocumentsViewModel.LoadDocuments(documentsList);
                this.DocumentsViewModel.IsChanged = true;

                // Convert the master data entity currency values.
                CurrencyConversionManager.ConvertObject(masterPart, this.MeasurementUnitsAdapter.BaseCurrency, CurrencyConversionManager.DefaultBaseCurrency);

                // Load the data from the master part and sub-objects in the corresponding view-models. Use empty data for null sub-objects.
                this.LoadDataFromModel(masterPart);

                // The calculator from the master part was loaded in this instance's Calculator property but it does not belong to the same data context as the Model.
                // We correct that by obtaining it from the data context associated with the Model.
                if (Calculator.Value != null)
                {
                    this.Calculator.Value = this.DataSourceManager.UserRepository.GetById(Calculator.Value.Guid, false);
                }

                this.ManufacturerViewModel.LoadDataFromModel(masterPart.Manufacturer ?? new Manufacturer());

                // If the master data part's Overhead Settings are empty use the ones of the parent.
                var overheadSettings = new OverheadSetting();
                if (masterPart.OverheadSettings.CompareValuesTo(overheadSettings))
                {
                    if (this.Model.Assembly != null)
                    {
                        overheadSettings = this.Model.Assembly.OverheadSettings;
                    }
                    else if (this.Model.Project != null)
                    {
                        overheadSettings = this.Model.Project.OverheadSettings;
                    }
                }
                else
                {
                    overheadSettings = masterPart.OverheadSettings;
                }

                this.OverheadSettingsViewModel.LoadDataFromModel(overheadSettings ?? new OverheadSetting());
                this.CountrySettingsViewModel.LoadDataFromModel(masterPart.CountrySettings ?? new CountrySetting());

                // Obtain the unit from the data context of the model.
                if (masterPart.MeasurementUnit != null)
                {
                    this.WeightUnit = this.DataSourceManager.MeasurementUnitRepository.GetByName(masterPart.MeasurementUnit.Name);
                }
            }

            this.ResumeRecalculationNotifications(true);
        }

        /// <summary>
        /// Called when a country or supplier is selected from the country browser.
        /// </summary>
        /// <param name="masterDataEntity">The master data entity selected in the browser.</param>
        /// <param name="databaseId">The source database of the selected master data.</param>
        private void OnCountryOrSupplierSelected(object masterDataEntity, DbIdentifier databaseId)
        {
            this.StopRecalculationNotifications();
            using (this.UndoManager.StartBatch(undoEachBatch: true))
            {
                Country country = masterDataEntity as Country;
                if (country != null)
                {
                    this.ManufacturingCountry.Value = country.Name;
                    this.ManufacturingCountryId.Value = country.Guid;
                    this.ManufacturingSupplier.Value = null;
                    if (country.CountrySetting != null)
                    {
                        // Convert the master data entity currency values.
                        CurrencyConversionManager.ConvertObject(country.CountrySetting, this.MeasurementUnitsAdapter.BaseCurrency, CurrencyConversionManager.DefaultBaseCurrency);

                        this.CountrySettingsViewModel.LoadDataFromModel(country.CountrySetting);
                    }
                    else
                    {
                        this.CountrySettingsViewModel.LoadDataFromModel(new CountrySetting());
                    }
                }
                else
                {
                    CountryState supplier = masterDataEntity as CountryState;
                    if (supplier != null)
                    {
                        this.ManufacturingSupplier.Value = supplier.Name;
                        if (supplier.CountrySettings != null)
                        {
                            // Convert the master data entity currency values.
                            CurrencyConversionManager.ConvertObject(supplier.CountrySettings, this.MeasurementUnitsAdapter.BaseCurrency, CurrencyConversionManager.DefaultBaseCurrency);

                            this.CountrySettingsViewModel.LoadDataFromModel(supplier.CountrySettings);
                        }
                        else
                        {
                            this.CountrySettingsViewModel.LoadDataFromModel(new CountrySetting());
                        }
                    }
                }
            }

            this.ResumeRecalculationNotifications(true);
        }

        #endregion Master Data Browse handling

        /// <summary>
        /// Calculates the target parts production and necessary parts production.
        /// </summary>
        /// <param name="part">The part.</param>
        private void CalculatePartsProductionTotal(Part part)
        {
            ICostCalculator calculator = CostCalculatorFactory.GetCalculator(part.CalculationVariant);

            this.PartsTotal.Value = calculator.CalculateNetLifetimeProductionQuantity(part);

            if (!part.IsMasterData
                && part.Process != null
                && part.Process.Steps.Count > 0
                && this.ParentProject != null)
            {
                var calculationParams = CostCalculationHelper.CreatePartParamsFromProject(this.ParentProject);
                this.NeededPartsTotal.Value = calculator.CalculateGrossLifetimeProductionQuantity(part, calculationParams);
            }
            else
            {
                this.NeededPartsTotal.Value = this.PartsTotal.Value;
            }
        }

        /// <summary>
        /// Generate a new batch size for the assembly process steps.
        /// </summary>
        private void UpdateBatchSizeAction()
        {
            using (this.UndoManager.StartBatch(navigateToBatchControls: true, undoEachBatch: true))
            {
                this.DisplayBatchSizeWarning.Value = false;
                this.updateProcessStepsBatchSize = true;
                this.BatchSizeUpdated.Value = this.GetCurrentBatchSize();
            }
        }

        /// <summary>
        /// Get the assembly process steps Batch Size value, based on current assembly batch size and yearly production quantity.
        /// </summary>
        /// <returns>The process steps new batch size.</returns>
        private int? GetCurrentBatchSize()
        {
            if (this.BatchSize.Value != null && this.BatchSize.Value != 0)
            {
                var batchSize = Math.Ceiling(Convert.ToDecimal(this.YearlyProductionQuantity.Value) / Convert.ToDecimal(this.BatchSize.Value));
                return batchSize < int.MaxValue ? (int)batchSize : int.MaxValue;
            }
            else
            {
                return null;
            }
        }
    }
}