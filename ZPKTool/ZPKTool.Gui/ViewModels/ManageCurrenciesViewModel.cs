﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Controls;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Managers;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for manage currencies
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ManageCurrenciesViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The composition container.
        /// </summary>
        private CompositionContainer container;

        /// <summary>
        /// The Window service
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The change set used to manage the currencies.
        /// </summary>
        private IDataSourceManager dataSourceManager;

        /// <summary>
        /// The currency view model
        /// </summary>
        private CurrencyViewModel currencyViewModel;

        /// <summary>
        /// The current selected currency
        /// </summary>
        private Currency selectedCurrency;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="ManageCurrenciesViewModel"/> class.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="messenger">The messenger.</param>
        /// <param name="windowService">The window service.</param>
        [ImportingConstructor]
        public ManageCurrenciesViewModel(
            CompositionContainer container,
            IMessenger messenger,
            IWindowService windowService)
        {
            this.container = container;
            this.Messenger = messenger;
            this.windowService = windowService;
            
            this.InitializeCommands();

            this.DataSourceManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            this.CurrencyViewModel = this.container.GetExportedValue<CurrencyViewModel>();
            this.CurrencyViewModel.DataSourceManager = this.DataSourceManager;
            this.RegisterMessageHandlers();
            this.LoadSourceData();
        }

        #region Commands

        /// <summary>
        /// Gets the command for the add currency button
        /// </summary>
        public ICommand AddCommand { get; private set; }

        /// <summary>
        /// Gets the command for the delete currency button
        /// </summary>
        public ICommand DeleteCommand { get; private set; }

        /// <summary>
        /// Gets the command for the import currency button.
        /// </summary>
        public ICommand ImportCommand { get; private set; }

        /// <summary>
        /// Gets the command for the export currency button.
        /// </summary>
        public ICommand ExportCommand { get; private set; }

        /// <summary>
        /// Gets the command for the PreviewMouseDown event
        /// </summary>
        public ICommand DataGridMouseDownCommand { get; private set; }

        /// <summary>
        /// Gets the command for the PreviewMouseUp event
        /// </summary>
        public ICommand DataGridKeyDownCommand { get; private set; }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the data source that contains the whole currencies
        /// </summary>
        public ObservableCollection<Currency> Currencies { get; private set; }

        /// <summary>
        /// Gets the messenger service.
        /// </summary>
        protected IMessenger Messenger { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether the view associated with this instance is active in the UI.
        /// Active means that is focused and can receive user input.
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Gets or sets the current selected currency from DataSource
        /// </summary>
        public Currency SelectedCurrency
        {
            get { return this.selectedCurrency; }
            set { this.SetProperty(ref this.selectedCurrency, value, () => this.SelectedCurrency, this.OnSelectedCurrencyChanged); }
        }

        /// <summary>
        /// Gets the CurrencyViewModel
        /// </summary>
        public CurrencyViewModel CurrencyViewModel
        {
            get { return this.currencyViewModel; }
            private set { this.SetProperty(ref this.currencyViewModel, value, () => this.CurrencyViewModel); }
        }

        /// <summary>
        /// Gets or sets the data manager used to perform data related operations.
        /// If a new data source manager is set, it must be changed in all its children
        /// </summary>
        public IDataSourceManager DataSourceManager
        {
            get
            {
                return this.dataSourceManager;
            }

            set
            {
                this.SetProperty(
                    ref this.dataSourceManager,
                    value,
                    () => this.DataSourceManager,
                    () =>
                    {
                        if (this.CurrencyViewModel != null)
                        {
                            this.CurrencyViewModel.DataSourceManager = this.DataSourceManager;
                        }
                    });
            }
        }

        #endregion

        /// <summary>
        /// Initializes the commands.
        /// </summary>
        private void InitializeCommands()
        {
            this.AddCommand = new DelegateCommand(this.AddCurrency, () => !this.IsReadOnly);
            this.ImportCommand = new DelegateCommand(this.ImportCurrency, () => !this.IsReadOnly);
            this.ExportCommand = new DelegateCommand(this.ExportCurrency, () => !this.IsReadOnly);
            this.DeleteCommand = new DelegateCommand(() => this.DeleteCurrency(), () => this.CanDeleteCurrency());
            this.DataGridMouseDownCommand = new DelegateCommand<object>(this.HandleCurrenciesListPreviewMouseDown);
            this.DataGridKeyDownCommand = new DelegateCommand<object>(this.HandleCurrenciesListPreviewKeyDown);
        }

        /// <summary>
        /// Loads the data from DB.
        /// </summary>
        private void LoadSourceData()
        {
            Collection<Currency> currencies = this.DataSourceManager.CurrencyRepository.GetBaseCurrencies();
            this.Currencies = new ObservableCollection<Currency>(currencies.OrderBy(c => c.Name));
            this.SelectedCurrency = this.Currencies.FirstOrDefault();
        }

        /// <summary>
        /// Sets the <see cref="CurrencyViewModel"/> property when the selected currency is changed
        /// </summary>
        private void OnSelectedCurrencyChanged()
        {
            var model = this.SelectedCurrency;
            if (model != null)
            {
                this.CurrencyViewModel.Model = model;

                // Check if the selected currency is the default application currency
                // The default currency can not be modified
                if (model.IsoCode.Equals(Constants.DefaultCurrencyIsoCode, StringComparison.InvariantCulture))
                {
                    this.CurrencyViewModel.IsReadOnly = true;
                    this.CurrencyViewModel.ShowSaveControls = false;
                }
                else
                {
                    this.CurrencyViewModel.IsReadOnly = false;
                    this.CurrencyViewModel.ShowSaveControls = true;
                }
            }
        }

        /// <summary>
        /// Handles the PreviewKeyDown event of the CurrenciesDataGrid control.
        /// </summary>
        /// <param name="parameter">The selected currency object</param>
        private void HandleCurrenciesListPreviewKeyDown(object parameter)
        {
            this.OnBeforeSelectedCurrencyChange(parameter);
        }

        /// <summary>
        /// Handles the PreviewMouseDown event of the CurrenciesDataGrid control.
        /// </summary>
        /// <param name="parameter">The command's parameter.</param>
        private void HandleCurrenciesListPreviewMouseDown(object parameter)
        {
            this.OnBeforeSelectedCurrencyChange(parameter);
        }

        /// <summary>
        /// Registers the necessary message handlers with the Messenger service.
        /// </summary>
        private void RegisterMessageHandlers()
        {
            this.Messenger.Register<EntityImportedMessage>(this.HandleNotificationMessage);
        }

        /// <summary>
        /// Handles some generic messages, of type EntityImportedMessage.
        /// </summary>
        /// <param name="message">The message.</param>
        protected virtual void HandleNotificationMessage(EntityImportedMessage message)
        {
            if (message.EntityType == ImportedEntityType.Currency)
            {
                this.LoadSourceData();
            }
        }

        /// <summary>
        /// Check if the model is changed before selected currency change
        /// </summary>
        /// <param name="parameter">The selected currency object</param>
        private void OnBeforeSelectedCurrencyChange(object parameter)
        {
            Currency selectedCurrency = parameter as Currency;
            if (selectedCurrency == null)
            {
                return;
            }

            if (this.CurrencyViewModel.IsChanged)
            {
                try
                {
                    // set the flag for the cancel window message from the CurrencyViewModel
                    this.currencyViewModel.ShowCancelMessageFlag = false;

                    MessageDialogResult result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_ChangedSave, MessageDialogType.YesNo);
                    if (result == MessageDialogResult.Yes)
                    {
                        if (this.CurrencyViewModel.SaveCommand.CanExecute(null))
                        {
                            this.CurrencyViewModel.SaveCommand.Execute(null);
                            this.SelectedCurrency = selectedCurrency;
                        }
                    }
                    else
                    {
                        if (this.CurrencyViewModel.CancelCommand.CanExecute(null))
                        {
                            this.CurrencyViewModel.CancelCommand.Execute(null);
                            this.SelectedCurrency = selectedCurrency;
                        }
                    }
                }
                finally
                {
                    // reset the flag for the cancel window message to default
                    this.currencyViewModel.ShowCancelMessageFlag = true;
                }
            }
        }

        /// <summary>
        /// Called before the control is removed from the main area.
        /// </summary>
        /// <returns>
        /// <c>True</c> if the unload was successful; <c>False</c> otherwise
        /// </returns>
        public override bool OnUnloading()
        {
            if (this.CurrencyViewModel.IsChanged)
            {
                // set show cancel Message from currency view model
                this.CurrencyViewModel.ShowCancelMessageFlag = false;

                try
                {
                    MessageDialogResult result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_UnsavedDataOnQuit, MessageDialogType.YesNoCancel);
                    if (result == MessageDialogResult.Yes)
                    {
                        if (this.CurrencyViewModel.SaveCommand.CanExecute(null))
                        {
                            this.CurrencyViewModel.SaveCommand.Execute(null);
                        }
                    }
                    else if (result == MessageDialogResult.No)
                    {
                        if (this.CurrencyViewModel.CancelCommand.CanExecute(null))
                        {
                            this.CurrencyViewModel.CancelCommand.Execute(null);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                finally
                {
                    this.CurrencyViewModel.ShowCancelMessageFlag = true;
                }
            }

            if (this.CurrencyViewModel.IsInputValid)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Open the create new currency window
        /// </summary>
        private void AddCurrency()
        {
            CurrencyViewModel viewModel = container.GetExportedValue<CurrencyViewModel>();
            viewModel.DataSourceManager = this.DataSourceManager;
            viewModel.EditMode = ViewModelEditMode.Create;
            viewModel.Model = new Currency() { IsMasterData = true };
            this.windowService.ShowViewInDialog(viewModel, "CreateCurrencyViewTemplate");

            var model = viewModel.Model;
            if (model != null && viewModel.Saved)
            {
                this.Currencies.Add(model);
            }
        }

        /// <summary>
        /// Imports the currency.
        /// </summary>
        private void ImportCurrency()
        {
            var viewModel = container.GetExportedValue<MasterDataImporterExporterViewModel>();
            viewModel.Operation = MasterDataOperation.Import;
            viewModel.EntityType = ImportedEntityType.Currency;
            viewModel.DataManager = this.DataSourceManager;
            this.windowService.ShowViewInDialog(viewModel);
        }

        /// <summary>
        /// Exports the currency.
        /// </summary>
        private void ExportCurrency()
        {
            var viewModel = container.GetExportedValue<MasterDataImporterExporterViewModel>();
            viewModel.Operation = MasterDataOperation.Export;
            viewModel.EntityType = ImportedEntityType.Currency;
            viewModel.DataManager = this.DataSourceManager;
            this.windowService.ShowViewInDialog(viewModel);
        }

        /// <summary>
        /// Deletes the selected currency
        /// </summary>
        private void DeleteCurrency()
        {
            if (this.SelectedCurrency == null)
            {
                return;
            }

            var message = string.Format(LocalizedResources.Question_Delete_Item, this.SelectedCurrency.Name);
            MessageDialogResult result = this.windowService.MessageDialogService.Show(message, MessageDialogType.YesNo);
            if (result == MessageDialogResult.No)
            {
                return;
            }

            // check if the selected currency is not assignee to any country
            bool checkCurrency = this.DataSourceManager.CurrencyRepository.CheckCurrencyHasCountry(this.SelectedCurrency.Guid);
            if (checkCurrency == true)
            {
                MessageDialogResult resultCheckCurrency = this.windowService.MessageDialogService.Show(LocalizedResources.Error_CantDeleteCurrency, MessageDialogType.Error);
                return;
            }

            this.DataSourceManager.CurrencyRepository.RemoveAll(this.SelectedCurrency);
            this.DataSourceManager.SaveChanges();
            this.Currencies.Remove(this.SelectedCurrency);
            this.SelectedCurrency = this.Currencies.FirstOrDefault();
        }

        /// <summary>
        /// Determines whether the Delete command can be execute.
        /// </summary>
        /// <returns>
        /// true if the command can be executed, false otherwise.
        /// </returns>
        private bool CanDeleteCurrency()
        {
            // Check if the selected currency is the default currency
            // The default currency can not be modified or deleted
            var isDefaultCurrency = false;
            if (this.SelectedCurrency != null)
            {
                isDefaultCurrency = this.SelectedCurrency.IsoCode.Equals(Constants.DefaultCurrencyIsoCode, StringComparison.InvariantCulture);
            }

            return !this.IsReadOnly && !isDefaultCurrency;
        }
    }
}
