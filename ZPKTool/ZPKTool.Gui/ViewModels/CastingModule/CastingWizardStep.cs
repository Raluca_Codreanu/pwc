﻿namespace ZPKTool.Gui.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// identifies each step of the Casting Wizard.
    /// </summary>
    public enum CastingWizardStep
    {
        /// <summary>
        /// Default value that does not identify any step.
        /// </summary>
        None = 0,

        /// <summary>
        /// The casting type step (1st).
        /// </summary>
        CastingType = 1,

        /// <summary>
        /// The settings step (2nd).
        /// </summary>
        Settings = 2,

        /// <summary>
        /// The core definitions step (3rd).
        /// </summary>
        CoreDefinition = 3,

        /// <summary>
        /// The results step (4th and last).
        /// </summary>
        ResultingProcessValues = 4
    }
}
