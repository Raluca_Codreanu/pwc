﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The types of casting processes.
    /// </summary>
    public enum CastingProcessType
    {
        /// <summary>
        /// Automatic process.
        /// </summary>
        Automatic = 0,

        /// <summary>
        /// Semi-automatic process
        /// </summary>
        SemiAutomatic = 1,

        /// <summary>
        /// Manual process
        /// </summary>
        Manual = 2
    }
}
