﻿namespace ZPKTool.Gui.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.Composition;
    using System.Linq;
    using System.Text;
    using System.Windows;
    using ZPKTool.Controls;
    using ZPKTool.Data;
    using ZPKTool.DataAccess;
    using ZPKTool.Gui.Views;

    /// <summary>
    /// An interface for the service that navigates between the Casting Wizard steps.
    /// </summary>
    public interface ICastingModuleNavigationService
    {
        /// <summary>
        /// Starts the casting wizard and displays the 1st step.
        /// </summary>
        /// <param name="viewModel">The casting wizard's view model (necessary to be set as the data context of the casting wizard views).</param>
        void StartWizard(CastingModuleViewModel viewModel);

        /// <summary>
        /// Closes the wizard.
        /// </summary>
        void CloseWizard();

        /// <summary>
        /// Determines whether it is possible to navigate to the next casting step.
        /// </summary>
        /// <returns>
        /// true if the navigation is possible; otherwise, false.
        /// </returns>
        bool CanGoToNextStep();

        /// <summary>
        /// Navigates to the next casting step.
        /// </summary>
        /// <param name="numberOfCores">The number of cores defined by the user.</param>
        void GoToNextStep(decimal numberOfCores);

        /// <summary>
        /// Determines the next casting step, without navigating to it.
        /// </summary>
        /// <param name="numberOfCores">The number of cores defined by the user..</param>
        /// <returns>A value identifying the next step.</returns>
        CastingWizardStep PredictNextStep(decimal numberOfCores);

        /// <summary>
        /// Determines whether it is possible to navigate to the previous casting step.
        /// </summary>
        /// <returns>
        /// true if the navigation is possible; otherwise, false.
        /// </returns>
        bool CanGoToPreviousStep();

        /// <summary>
        /// Navigates to the previous casting step.
        /// </summary>
        /// <param name="numberOfCores">The number of cores defined by the user..</param>
        void GoToPreviousStep(decimal numberOfCores);
    }

    /// <summary>
    /// The service that navigates between the Casting Wizard steps.
    /// </summary>
    [Export(typeof(ICastingModuleNavigationService))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class CastingModuleNavigationService : ICastingModuleNavigationService
    {
        /// <summary>
        /// The view of the currently displayed casting wizard step.
        /// </summary>
        private OverlayWindow currentView;

        /// <summary>
        /// Initializes a new instance of the <see cref="CastingModuleNavigationService"/> class.
        /// </summary>
        public CastingModuleNavigationService()
        {
        }

        /// <summary>
        /// Starts the casting wizard and displays the 1st step.
        /// </summary>
        /// <param name="viewModel">The casting wizard's view model (necessary to be set as the data context of the casting wizard views).</param>
        public void StartWizard(CastingModuleViewModel viewModel)
        {
            SelectCastingMethodView startView = new SelectCastingMethodView();
            startView.Owner = Application.Current.MainWindow;
            startView.DataContext = viewModel;

            this.currentView = startView;
            startView.ShowDialog();
        }

        /// <summary>
        /// Closes the wizard.
        /// </summary>
        public void CloseWizard()
        {
            if (this.currentView != null)
            {
                this.currentView.Close();
            }
        }

        /// <summary>
        /// Determines whether it is possible to navigate to the next casting step.
        /// </summary>
        /// <returns>
        /// true if the navigation is possible; otherwise, false.
        /// </returns>
        public bool CanGoToNextStep()
        {
            if (this.currentView == null
                || this.currentView is CastingResultingProcessValuesView)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Determines the next casting step, without navigating to it.
        /// </summary>
        /// <param name="numberOfCores">The number of cores defined by the user..</param>
        /// <returns>
        /// A value identifying the next step.
        /// </returns>
        public CastingWizardStep PredictNextStep(decimal numberOfCores)
        {
            CastingWizardStep nextStep = CastingWizardStep.None;

            if (this.currentView == null)
            {
                nextStep = CastingWizardStep.CastingType;
            }
            else if (this.currentView is SelectCastingMethodView)
            {
                nextStep = CastingWizardStep.Settings;
            }
            else if (this.currentView is CastingSettingsView && numberOfCores > 0)
            {
                nextStep = CastingWizardStep.CoreDefinition;
            }
            else if ((this.currentView is CastingSettingsView && numberOfCores <= 0)
                || this.currentView is CastingCoreDefinitionView)
            {
                nextStep = CastingWizardStep.ResultingProcessValues;
            }

            return nextStep;
        }

        /// <summary>
        /// Navigates to the next casting step.
        /// </summary>
        /// <param name="numberOfCores">The number of cores defined by the user.</param>
        public void GoToNextStep(decimal numberOfCores)
        {
            CastingWizardStep nextStep = this.PredictNextStep(numberOfCores);
            this.DisplayStep(nextStep);
        }

        /// <summary>
        /// Determines whether it is possible to navigate to the previous casting step.
        /// </summary>
        /// <returns>
        /// true if the navigation is possible; otherwise, false.
        /// </returns>
        public bool CanGoToPreviousStep()
        {
            if (this.currentView == null
                || this.currentView is SelectCastingMethodView)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Navigates to the previous casting step.
        /// </summary>
        /// <param name="numberOfCores">The number of cores defined by the user..</param>
        public void GoToPreviousStep(decimal numberOfCores)
        {
            CastingWizardStep previousStep = CastingWizardStep.None;
            if (this.currentView is CastingResultingProcessValuesView && numberOfCores > 0)
            {
                previousStep = CastingWizardStep.CoreDefinition;
            }
            else if ((this.currentView is CastingResultingProcessValuesView && numberOfCores <= 0)
                || this.currentView is CastingCoreDefinitionView)
            {
                previousStep = CastingWizardStep.Settings;
            }
            else if (this.currentView is CastingSettingsView)
            {
                previousStep = CastingWizardStep.CastingType;
            }

            this.DisplayStep(previousStep);
        }

        /// <summary>
        /// Displays the specified step in the casting wizard.
        /// </summary>
        /// <param name="step">The step to display.</param>
        private void DisplayStep(CastingWizardStep step)
        {
            OverlayWindow stepView = null;
            switch (step)
            {
                case CastingWizardStep.CastingType:
                    stepView = new SelectCastingMethodView();
                    break;
                case CastingWizardStep.Settings:
                    stepView = new CastingSettingsView();
                    break;
                case CastingWizardStep.CoreDefinition:
                    stepView = new CastingCoreDefinitionView();
                    break;
                case CastingWizardStep.ResultingProcessValues:
                    stepView = new CastingResultingProcessValuesView();
                    break;
            }

            if (stepView != null)
            {
                stepView.Owner = this.currentView.Owner;
                stepView.DataContext = this.currentView.DataContext;

                this.currentView.Close();
                this.currentView = stepView;
                stepView.ShowDialog();
            }
        }
    }
}
