﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The density demand types for casting.
    /// </summary>
    public enum CastingDensityDemand
    {
        /// <summary>
        /// Normal density
        /// </summary>
        Normal = 0,

        /// <summary>
        /// Pressure Tight Type
        /// </summary>
        Pressuretight = 1
    }
}
