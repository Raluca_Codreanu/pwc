﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The material types used in the casting module.
    /// </summary>
    public enum CastingMaterialType
    {
        /// <summary>
        /// Ferrous / Steel
        /// </summary>
        FerrousSteel = 0,

        /// <summary>
        /// Aluminum alloy
        /// </summary>
        AluminiumAlloy = 1,

        /// <summary>
        /// Zinc alloy
        /// </summary>
        ZinkAlloy = 2,

        /// <summary>
        /// Magnesium alloy
        /// </summary>
        MagnesiumAlloy = 3
    }
}
