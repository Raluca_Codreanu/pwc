﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using ZPKTool.Business;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Common;
using ZPKTool.Controls;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Utils;
using ZPKTool.Gui.Views;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the CastingModule.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class CastingModuleViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The data context of the selected machines
        /// </summary>
        private IDataSourceManager machinesSourceChangeSet;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The online check service.
        /// </summary>
        private IOnlineCheckService onlineChecker;

        /// <summary>
        /// The measurement units used when creating the casting process.
        /// </summary>
        private Collection<MeasurementUnit> measurementUnits = new Collection<MeasurementUnit>();

        /// <summary>
        /// The casting method.
        /// </summary>
        private CastingMethod castingMethod;

        /// <summary>
        /// The part weight
        /// </summary>
        private decimal? partWeight;

        /// <summary>
        /// The projected max area.
        /// </summary>
        private decimal? projectedMaxArea;

        /// <summary>
        /// The projected min area.
        /// </summary>
        private decimal? projectedMinArea;

        /// <summary>
        /// The number of cavities proposed by the wizard.
        /// </summary>
        private decimal? proposedNumberOfCavities;

        /// <summary>
        /// The number of cavities inputted by the user.
        /// </summary>
        private decimal? inputtedNumberOfCavities;

        /// <summary>
        /// The ratio of sprue and fillers proposed by the wizard.
        /// </summary>
        private decimal? proposedRatioOfSprueFillers;

        /// <summary>
        /// The ratio of sprue and fillers inputted by the user.
        /// </summary>
        private decimal? inputtedRatioOfSprueFillers;

        /// <summary>
        /// The maximum wall thickness.
        /// </summary>
        private decimal? maxWallThickness;

        /// <summary>
        /// The number of cores.
        /// </summary>
        private decimal? numberOfCores;

        /// <summary>
        /// The material type selection.
        /// </summary>
        private CastingMaterialType materialType;

        /// <summary>
        /// The density demand.
        /// </summary>
        private CastingDensityDemand densityDemand;

        /// <summary>
        /// The precision demand
        /// </summary>
        private CastingPrecisionDemand precisionDemand;

        /// <summary>
        /// The casting process.
        /// </summary>
        private CastingProcessType process;

        /// <summary>
        /// The proposed mold box volume.
        /// </summary>
        private decimal? proposedMoldBoxVolume;

        /// <summary>
        /// The inputted mold box volume.
        /// </summary>
        private decimal? inputtedMoldBoxVolume;

        /// <summary>
        /// The sand weight proposed by the casting wizard
        /// </summary>
        private decimal? proposedSandWeight;

        /// <summary>
        /// The sand weight inputted by the user.
        /// </summary>
        private decimal? inputtedSandWeight;

        /// <summary>
        /// The mold box setup time proposed by the casting wizard.
        /// </summary>
        private decimal? proposedMoldBoxSetupTime;

        /// <summary>
        /// The mold box setup time inputted by the user.
        /// </summary>
        private decimal? inputtedMoldBoxSetupTime;

        /// <summary>
        /// The melting time proposed by the casting wizard.
        /// </summary>
        private decimal? proposedMeltingTime;

        /// <summary>
        /// The inputted melting time.
        /// </summary>
        private decimal? inputtedMeltingTime;

        /// <summary>
        /// The proposed pouring time.
        /// </summary>
        private decimal? proposedPouringTime;

        /// <summary>
        /// The inputted pouring time.
        /// </summary>
        private decimal? inputtedPouringTime;

        /// <summary>
        /// the proposed cooling time.
        /// </summary>
        private decimal? proposedCoolingTime;

        /// <summary>
        /// the inputted cooling time.
        /// </summary>
        private decimal? inputtedCoolingTime;

        /// <summary>
        /// the proposed scrap.
        /// </summary>
        private decimal? proposedScrap;

        /// <summary>
        /// the inputted scrap.
        /// </summary>
        private decimal? inputtedScrap;

        /// <summary>
        /// The proposed solidification time.
        /// </summary>
        private decimal? proposedSolidificationTime;

        /// <summary>
        /// the inputted solidification time.
        /// </summary>
        private decimal? inputtedSolidificationTime;

        /// <summary>
        /// The part diameter.
        /// </summary>
        private decimal? partDiameter;

        /// <summary>
        /// The length of the part.
        /// </summary>
        private decimal? partLength;

        /// <summary>
        /// The inner die pressure proposed by the casting module.
        /// </summary>
        private decimal? innerDiePressureProposed;

        /// <summary>
        /// The inner die pressure inputted by the user.
        /// </summary>
        private decimal? innerDiePressureInputted;

        /// <summary>
        /// The Part's data context.
        /// </summary>
        private IDataSourceManager partDataContext;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        #endregion Attributes

        /// <summary>
        /// Initializes a new instance of the <see cref="CastingModuleViewModel"/> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        /// <param name="onlineChecker">The online check service.</param>
        /// <param name="unitsService">The units service.</param>
        [ImportingConstructor]
        public CastingModuleViewModel(
            IWindowService windowService,
            IOnlineCheckService onlineChecker,
            IUnitsService unitsService)
        {
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("onlineChecker", onlineChecker);
            Argument.IsNotNull("unitsService", unitsService);

            this.windowService = windowService;
            this.onlineChecker = onlineChecker;
            this.unitsService = unitsService;

            this.CoreDefinitions = new ObservableCollection<CastingCoreDefinition>();
            this.CastingMethod = CastingMethod.SandCastingFerrous;
            this.NumberOfCores = 0m;

            this.MaterialType = CastingMaterialType.FerrousSteel;
            this.DensityDemand = CastingDensityDemand.Normal;
            this.PrecisionDemand = CastingPrecisionDemand.Normal;
            this.Process = CastingProcessType.Automatic;

            this.CastingMethodSelectedCommand = new DelegateCommand<CastingMethod>((type) => this.CastingMethod = type);
            this.NextCastingStepCommand = new DelegateCommand(AdvanceToNextCastingStep, CanAdvanceToNextCastingStep);
            this.PreviousCastingStepCommand = new DelegateCommand(GoToPreviousCastingStep, CanGoToPreviousCastingStep);
            this.FinishCommand = new DelegateCommand(Finish);

            this.CreatedSteps = new List<ProcessStep>();
        }

        #region Commands

        /// <summary>
        /// Gets the command executed when casting method is selected.
        /// </summary>        
        public ICommand CastingMethodSelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command that advances the UI to the next step of the casting module.
        /// </summary>
        public ICommand NextCastingStepCommand { get; private set; }

        /// <summary>
        /// Gets the command that reverts the UI to the previous step of the casting module.
        /// </summary>
        public ICommand PreviousCastingStepCommand { get; private set; }

        /// <summary>
        /// Gets the command that finalizes the casting wizard.
        /// </summary>
        public ICommand FinishCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets or sets the navigation service of the casting wizard.
        /// </summary>
        [Import]
        public ICastingModuleNavigationService NavigationService { get; set; }

        /// <summary>
        /// Gets or sets the part for which the casting process is being created.
        /// </summary>
        /// <value>
        /// The part.
        /// </value>
        public Part Part { get; set; }

        /// <summary>
        /// Gets or sets the Part's data context.
        /// </summary>
        public IDataSourceManager PartDataContext
        {
            get
            {
                return this.partDataContext;
            }

            set
            {
                if (this.partDataContext != value)
                {
                    this.partDataContext = value;
                    this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(value);
                }
            }
        }

        /// <summary>
        /// Gets or sets the casting method.
        /// </summary>        
        public CastingMethod CastingMethod
        {
            get
            {
                return this.castingMethod;
            }

            set
            {
                if (this.castingMethod != value)
                {
                    this.castingMethod = value;
                    OnPropertyChanged("CastingMethod");
                }
            }
        }

        /// <summary>
        /// Gets the title of the cast settings view.
        /// </summary>        
        public string SettingsViewTitle
        {
            get { return GetSettingsViewTitle(); }
        }

        /// <summary>
        /// Gets or sets the part weight.
        /// </summary>        
        public decimal? PartWeight
        {
            get
            {
                return this.partWeight;
            }

            set
            {
                if (this.partWeight != value)
                {
                    this.partWeight = value;
                    OnPropertyChanged("PartWeight");
                }
            }
        }

        /// <summary>
        /// Gets or sets the projected max area.
        /// </summary>        
        public decimal? ProjectedMaxArea
        {
            get
            {
                return this.projectedMaxArea;
            }

            set
            {
                if (this.projectedMaxArea != value)
                {
                    this.projectedMaxArea = value;
                    OnPropertyChanged("ProjectedMaxArea");
                }
            }
        }

        /// <summary>
        /// Gets or sets the projected min area.
        /// </summary>
        public decimal? ProjectedMinArea
        {
            get
            {
                return this.projectedMinArea;
            }

            set
            {
                if (this.projectedMinArea != value)
                {
                    this.projectedMinArea = value;
                    OnPropertyChanged("ProjectedMinArea");
                }
            }
        }

        /// <summary>
        /// Gets or sets the number of cavities proposed by the wizard.
        /// </summary>        
        public decimal? ProposedNumberOfCavities
        {
            get
            {
                return this.proposedNumberOfCavities;
            }

            set
            {
                if (this.proposedNumberOfCavities != value)
                {
                    this.proposedNumberOfCavities = value;
                    OnPropertyChanged("ProposedNumberOfCavities");
                }
            }
        }

        /// <summary>
        /// Gets or sets the number of cavities inputted by the user.
        /// </summary>
        public decimal? InputtedNumberOfCavities
        {
            get
            {
                return this.inputtedNumberOfCavities;
            }

            set
            {
                if (this.inputtedNumberOfCavities != value)
                {
                    this.inputtedNumberOfCavities = value;
                    OnPropertyChanged("InputtedNumberOfCavities");
                }
            }
        }

        /// <summary>
        /// Gets or sets the ratio of sprue and fillers proposed by the wizard.
        /// </summary>        
        public decimal? ProposedRatioOfSprueFillers
        {
            get
            {
                return this.proposedRatioOfSprueFillers;
            }

            set
            {
                if (this.proposedRatioOfSprueFillers != value)
                {
                    this.proposedRatioOfSprueFillers = value;
                    OnPropertyChanged("ProposedRatioOfSprueFillers");
                }
            }
        }

        /// <summary>
        /// Gets or sets the ratio of sprue and fillers inputted by the user.
        /// </summary>        
        public decimal? InputtedRatioOfSprueFillers
        {
            get
            {
                return this.inputtedRatioOfSprueFillers;
            }

            set
            {
                if (this.inputtedRatioOfSprueFillers != value)
                {
                    this.inputtedRatioOfSprueFillers = value;
                    OnPropertyChanged("InputtedRatioOfSprueFillers");
                }
            }
        }

        /// <summary>
        /// Gets or sets the maximum wall thickness.
        /// </summary>        
        public decimal? MaxWallThickness
        {
            get
            {
                return this.maxWallThickness;
            }

            set
            {
                if (this.maxWallThickness != value)
                {
                    this.maxWallThickness = value;
                    OnPropertyChanged("MaxWallThickness");
                }
            }
        }

        /// <summary>
        /// Gets or sets the number of cores.
        /// </summary>        
        public decimal? NumberOfCores
        {
            get
            {
                return this.numberOfCores;
            }

            set
            {
                if (this.numberOfCores != value)
                {
                    this.numberOfCores = value;
                    OnPropertyChanged("NumberOfCores");
                }
            }
        }

        /// <summary>
        /// Gets or sets the material type selection.
        /// </summary>        
        public CastingMaterialType MaterialType
        {
            get
            {
                return this.materialType;
            }

            set
            {
                if (this.materialType != value)
                {
                    this.materialType = value;
                    OnPropertyChanged("MaterialType");
                }
            }
        }

        /// <summary>
        /// Gets or sets the density demand.
        /// </summary>        
        public CastingDensityDemand DensityDemand
        {
            get
            {
                return this.densityDemand;
            }

            set
            {
                if (this.densityDemand != value)
                {
                    this.densityDemand = value;
                    OnPropertyChanged("DensityDemand");
                    CalculateProposedSettings();
                }
            }
        }

        /// <summary>
        /// Gets or sets the precision demand.
        /// </summary>        
        public CastingPrecisionDemand PrecisionDemand
        {
            get
            {
                return this.precisionDemand;
            }

            set
            {
                if (this.precisionDemand != value)
                {
                    this.precisionDemand = value;
                    OnPropertyChanged("PrecisionDemand");
                    CalculateProposedSettings();
                }
            }
        }

        /// <summary>
        /// Gets or sets the casting process.
        /// </summary>
        public CastingProcessType Process
        {
            get
            {
                return this.process;
            }

            set
            {
                if (this.process != value)
                {
                    this.process = value;
                    OnPropertyChanged("Process");
                }
            }
        }

        /// <summary>
        /// Gets the core definitions.
        /// </summary>        
        public ObservableCollection<CastingCoreDefinition> CoreDefinitions { get; private set; }

        /// <summary>
        /// Gets or sets the proposed mold box volume.
        /// </summary>        
        public decimal? ProposedMoldBoxVolume
        {
            get
            {
                return this.proposedMoldBoxVolume;
            }

            set
            {
                if (this.proposedMoldBoxVolume != value)
                {
                    this.proposedMoldBoxVolume = value;
                    OnPropertyChanged("ProposedMoldBoxVolume");
                }
            }
        }

        /// <summary>
        /// Gets or sets the inputted mold box volume.
        /// </summary>
        public decimal? InputtedMoldBoxVolume
        {
            get
            {
                return this.inputtedMoldBoxVolume;
            }

            set
            {
                if (this.inputtedMoldBoxVolume != value)
                {
                    this.inputtedMoldBoxVolume = value;
                    OnPropertyChanged("InputtedMoldBoxVolume");
                    CalculateProposedResultValues();
                }
            }
        }

        /// <summary>
        /// Gets or sets the sand weight proposed by the casting wizard.
        /// </summary>
        public decimal? ProposedSandWeight
        {
            get
            {
                return this.proposedSandWeight;
            }

            set
            {
                if (this.proposedSandWeight != value)
                {
                    this.proposedSandWeight = value;
                    OnPropertyChanged("ProposedSandWeight");
                }
            }
        }

        /// <summary>
        /// Gets or sets the sand weight inputted by the user.
        /// </summary>        
        public decimal? InputtedSandWeight
        {
            get
            {
                return this.inputtedSandWeight;
            }

            set
            {
                if (this.inputtedSandWeight != value)
                {
                    this.inputtedSandWeight = value;
                    OnPropertyChanged("InputtedSandWeight");
                    CalculateProposedResultValues();
                }
            }
        }

        /// <summary>
        /// Gets or sets the mold box setup time proposed by the casting wizard.
        /// </summary>        
        public decimal? ProposedMoldBoxSetupTime
        {
            get
            {
                return this.proposedMoldBoxSetupTime;
            }

            set
            {
                if (this.proposedMoldBoxSetupTime != value)
                {
                    this.proposedMoldBoxSetupTime = value;
                    OnPropertyChanged("ProposedMoldBoxSetupTime");
                }
            }
        }

        /// <summary>
        /// Gets or sets the mold box setup time inputted by the user.
        /// </summary>
        public decimal? InputtedMoldBoxSetupTime
        {
            get
            {
                return this.inputtedMoldBoxSetupTime;
            }

            set
            {
                if (this.inputtedMoldBoxSetupTime != value)
                {
                    this.inputtedMoldBoxSetupTime = value;
                    OnPropertyChanged("InputtedMoldBoxSetupTime");
                }
            }
        }

        /// <summary>
        /// Gets or sets the melting time proposed by the casting wizard.
        /// </summary>
        public decimal? ProposedMeltingTime
        {
            get
            {
                return this.proposedMeltingTime;
            }

            set
            {
                if (this.proposedMeltingTime != value)
                {
                    this.proposedMeltingTime = value;
                    OnPropertyChanged("ProposedMeltingTime");
                }
            }
        }

        /// <summary>
        /// Gets or sets the inputted melting time.
        /// </summary>
        public decimal? InputtedMeltingTime
        {
            get
            {
                return this.inputtedMeltingTime;
            }

            set
            {
                if (this.inputtedMeltingTime != value)
                {
                    this.inputtedMeltingTime = value;
                    OnPropertyChanged("InputtedMeltingTime");
                }
            }
        }

        /// <summary>
        /// Gets or sets the proposed pouring time.
        /// </summary>
        public decimal? ProposedPouringTime
        {
            get
            {
                return this.proposedPouringTime;
            }

            set
            {
                if (this.proposedPouringTime != value)
                {
                    this.proposedPouringTime = value;
                    OnPropertyChanged("ProposedPouringTime");
                }
            }
        }

        /// <summary>
        /// Gets or sets the inputted pouring time.
        /// </summary>
        public decimal? InputtedPouringTime
        {
            get
            {
                return this.inputtedPouringTime;
            }

            set
            {
                if (this.inputtedPouringTime != value)
                {
                    this.inputtedPouringTime = value;
                    OnPropertyChanged("InputtedPouringTime");
                }
            }
        }

        /// <summary>
        /// Gets or sets the proposed solidification time.
        /// </summary>        
        public decimal? ProposedSolidificationTime
        {
            get
            {
                return this.proposedSolidificationTime;
            }

            set
            {
                if (this.proposedSolidificationTime != value)
                {
                    this.proposedSolidificationTime = value;
                    OnPropertyChanged("ProposedSolidificationTime");
                }
            }
        }

        /// <summary>
        /// Gets or sets the inputted solidification time.
        /// </summary>        
        public decimal? InputtedSolidificationTime
        {
            get
            {
                return this.inputtedSolidificationTime;
            }

            set
            {
                if (this.inputtedSolidificationTime != value)
                {
                    this.inputtedSolidificationTime = value;
                    OnPropertyChanged("InputtedSolidificationTime");
                }
            }
        }

        /// <summary>
        /// Gets or sets the proposed cooling time.
        /// </summary>
        public decimal? ProposedCoolingTime
        {
            get
            {
                return this.proposedCoolingTime;
            }

            set
            {
                if (this.proposedCoolingTime != value)
                {
                    this.proposedCoolingTime = value;
                    OnPropertyChanged("ProposedCoolingTime");
                }
            }
        }

        /// <summary>
        /// Gets or sets the inputted cooling time.
        /// </summary>
        public decimal? InputtedCoolingTime
        {
            get
            {
                return this.inputtedCoolingTime;
            }

            set
            {
                if (this.inputtedCoolingTime != value)
                {
                    this.inputtedCoolingTime = value;
                    OnPropertyChanged("InputtedCoolingTime");
                }
            }
        }

        /// <summary>
        /// Gets or sets the proposed scrap.
        /// </summary>
        public decimal? ProposedScrap
        {
            get
            {
                return this.proposedScrap;
            }

            set
            {
                if (this.proposedScrap != value)
                {
                    this.proposedScrap = value;
                    OnPropertyChanged("ProposedScrap");
                }
            }
        }

        /// <summary>
        /// Gets or sets the inputted scrap.
        /// </summary>
        public decimal? InputtedScrap
        {
            get
            {
                return this.inputtedScrap;
            }

            set
            {
                if (this.inputtedScrap != value)
                {
                    this.inputtedScrap = value;
                    OnPropertyChanged("InputtedScrap");
                }
            }
        }

        /// <summary>
        /// Gets or sets the part diameter.
        /// </summary>        
        public decimal? PartDiameter
        {
            get
            {
                return this.partDiameter;
            }

            set
            {
                if (this.partDiameter != value)
                {
                    this.partDiameter = value;
                    OnPropertyChanged("PartDiameter");
                }
            }
        }

        /// <summary>
        /// Gets or sets the length of the part.
        /// </summary>        
        public decimal? PartLength
        {
            get
            {
                return this.partLength;
            }

            set
            {
                if (this.partLength != value)
                {
                    this.partLength = value;
                    OnPropertyChanged("PartLength");
                }
            }
        }

        /// <summary>
        /// Gets or sets the inner die pressure proposed by the casting module.
        /// </summary>        
        public decimal? InnerDiePressureProposed
        {
            get
            {
                return this.innerDiePressureProposed;
            }

            set
            {
                if (this.innerDiePressureProposed != value)
                {
                    this.innerDiePressureProposed = value;
                    OnPropertyChanged("InnerDiePressureProposed");
                }
            }
        }

        /// <summary>
        /// Gets or sets the inner die pressure inputted by the user.
        /// </summary>
        public decimal? InnerDiePressureInputted
        {
            get
            {
                return this.innerDiePressureInputted;
            }

            set
            {
                if (this.innerDiePressureInputted != value)
                {
                    this.innerDiePressureInputted = value;
                    OnPropertyChanged("InnerDiePressureInputted");
                }
            }
        }

        /// <summary>
        /// Gets the list of created steps.
        /// </summary>
        public List<ProcessStep> CreatedSteps { get; private set; }

        /// <summary>
        /// Gets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter { get; private set; }

        #endregion Properties

        /// <summary>
        /// Determines whether it can advance to next casting step.
        /// </summary>
        /// <returns>true if it can advance, false if not.</returns>
        private bool CanAdvanceToNextCastingStep()
        {
            return this.NavigationService.CanGoToNextStep();
        }

        /// <summary>
        /// Advances to next casting step.
        /// </summary>
        private void AdvanceToNextCastingStep()
        {
            decimal noCores = this.NumberOfCores.GetValueOrDefault();
            CastingWizardStep nextStep = this.NavigationService.PredictNextStep(noCores);
            if (nextStep == CastingWizardStep.Settings)
            {
                // Calculate the proposed values for the Settings view.
                this.CalculateProposedSettings();
            }
            else if (nextStep == CastingWizardStep.CoreDefinition)
            {
                this.InitializeCoreDefinitions();
            }
            else if (nextStep == CastingWizardStep.ResultingProcessValues)
            {
                if (numberOfCores <= 0)
                {
                    // If the user entered x number of cores and then went back and entered 0, then we have to clear the created core definitions.
                    this.CoreDefinitions.Clear();
                }

                // Calculate the proposed values for the Resulting Process Values
                this.CalculateProposedResultValues();
            }

            this.NavigationService.GoToNextStep(noCores);
        }

        /// <summary>
        /// Determines whether it can go to previous casting step.
        /// </summary>
        /// <returns>true if it can, false if not.</returns>
        private bool CanGoToPreviousCastingStep()
        {
            return this.NavigationService.CanGoToPreviousStep();
        }

        /// <summary>
        /// Displays the view corresponding to the previous casting step.
        /// </summary>
        private void GoToPreviousCastingStep()
        {
            this.NavigationService.GoToPreviousStep(this.NumberOfCores.GetValueOrDefault());
        }

        /// <summary>
        /// Finishes the casting wizard.
        /// </summary>
        private void Finish()
        {
            this.CreateProcessSteps();
            this.NavigationService.CloseWizard();
        }

        /// <summary>
        /// Gets the title of the cast settings view.
        /// </summary>
        /// <returns>A string containing the title.</returns>
        private string GetSettingsViewTitle()
        {
            string title = LocalizedResources.CastingModule_Settings + " [";

            switch (this.CastingMethod)
            {
                case CastingMethod.CentrifugalCastingFerrous:
                    title += LocalizedResources.CastingMethod_CentrifugalCasting;
                    break;
                case CastingMethod.GravityDieCastingFerrous:
                case CastingMethod.GravityDieCastingNonFerrous:
                    title += LocalizedResources.CastingMethod_GravityDieCasting;
                    break;
                case CastingMethod.InvestmentCastingFerrous:
                case CastingMethod.InvestmentCastingNonFerrous:
                    title += LocalizedResources.CastingMethod_InvestmentCasting;
                    break;
                case CastingMethod.LowPressureCastNonFerrous:
                    title += LocalizedResources.CastingMethod_LowPressureCast;
                    break;
                case CastingMethod.LowPressureSandCastNonFerrous:
                    title += LocalizedResources.CastingMethod_LowPressureSandCast;
                    break;
                case CastingMethod.PressureDieCastNonFerrous:
                    title += LocalizedResources.CastingMethod_PressureDieCast;
                    break;
                case CastingMethod.SandCastingFerrous:
                case CastingMethod.SandCastingNonFerrous:
                    title += LocalizedResources.CastingMethod_SandCasting;
                    break;
            }

            title += ", ";

            if (this.CastingMethod == CastingMethod.SandCastingFerrous
                || this.CastingMethod == CastingMethod.CentrifugalCastingFerrous
                || this.CastingMethod == CastingMethod.GravityDieCastingFerrous
                || this.CastingMethod == CastingMethod.InvestmentCastingFerrous)
            {
                title += LocalizedResources.CastingModule_Ferrous;
            }
            else
            {
                title += LocalizedResources.CastingModule_NonFerrous;
            }

            title += "] ";
            return title;
        }

        /// <summary>
        /// Initializes the core definitions.
        /// </summary>
        private void InitializeCoreDefinitions()
        {
            int count = this.CoreDefinitions.Count;
            for (int i = count; i < this.NumberOfCores; i++)
            {
                CastingCoreDefinition def = new CastingCoreDefinition();
                def.Name = LocalizedResources.CastingModule_Core + " " + (i + 1).ToString();
                this.CoreDefinitions.Add(def);
            }

            if (this.CoreDefinitions.Count > this.NumberOfCores)
            {
                decimal extraCoreDefNumber = this.CoreDefinitions.Count - this.NumberOfCores.GetValueOrDefault();
                for (decimal i = 0; i < extraCoreDefNumber; i++)
                {
                    this.CoreDefinitions.RemoveAt(this.CoreDefinitions.Count - 1);
                }
            }
        }

        /// <summary>
        /// Checks if the Part inputted in the casting module and its data context are set.
        /// </summary>
        private void CheckPart()
        {
            if (this.Part == null)
            {
                throw new InvalidOperationException("The Part inputted into the casting module can't be null.");
            }

            if (this.PartDataContext == null)
            {
                throw new InvalidOperationException("The data context of the Part inputted into the casting module can't be null.");
            }
        }

        #region Calculations

        /// <summary>
        /// Calculates the proposed settings.
        /// </summary>
        private void CalculateProposedSettings()
        {
            this.CheckPart();

            // Calculate the part weight
            ICostCalculator calculator = CostCalculatorFactory.GetCalculator(this.Part.CalculationVariant);
            this.PartWeight = calculator.CalculatePartWeight(this.Part);

            int partsPerYear = this.Part.YearlyProductionQuantity.GetValueOrDefault();

            // Calculate the proposed number of cavities            
            this.ProposedNumberOfCavities = 0m;
            if (partsPerYear < 100000 || this.PartWeight > 10m)
            {
                this.ProposedNumberOfCavities = 1m;
            }
            else if (partsPerYear >= 100000 && this.PartWeight <= 10m && this.PartWeight > 5m)
            {
                this.ProposedNumberOfCavities = 2m;
            }
            else if (partsPerYear >= 100000 && this.PartWeight <= 5m && this.PartWeight > 2m)
            {
                this.ProposedNumberOfCavities = 4m;
            }
            else if (partsPerYear >= 100000 && this.PartWeight <= 2m && this.PartWeight > 1m)
            {
                this.ProposedNumberOfCavities = 6m;
            }
            else if (partsPerYear >= 100000
                && (this.CastingMethod == CastingMethod.InvestmentCastingFerrous || this.CastingMethod == CastingMethod.InvestmentCastingNonFerrous)
                && this.PartWeight <= 1m && this.PartWeight >= 0.5m)
            {
                this.ProposedNumberOfCavities = 10m;
            }
            else if (partsPerYear > 100000
                && (this.CastingMethod == CastingMethod.InvestmentCastingFerrous || this.CastingMethod == CastingMethod.InvestmentCastingNonFerrous)
                && this.PartWeight < 0.5m)
            {
                this.ProposedNumberOfCavities = 20m;
            }
            else
            {
                this.ProposedNumberOfCavities = 8m;
            }

            // Calculate the proposed ratio of sprue, fillers
            if (this.CastingMethod == CastingMethod.InvestmentCastingFerrous || this.CastingMethod == CastingMethod.InvestmentCastingNonFerrous)
            {
                this.ProposedRatioOfSprueFillers = 0.85m;
            }
            else if (this.CastingMethod == CastingMethod.PressureDieCastNonFerrous)
            {
                this.ProposedRatioOfSprueFillers = 0.65m;
            }
            else
            {
                this.ProposedRatioOfSprueFillers = 0.95m;
            }

            // Calculate the proposed process
            if (partsPerYear >= 100000)
            {
                this.Process = CastingProcessType.Automatic;
            }
            else if (partsPerYear >= 1000)
            {
                this.Process = CastingProcessType.SemiAutomatic;
            }
            else
            {
                this.Process = CastingProcessType.Manual;
            }

            // Calculate the inner die pressure (only for Pressure Die cast)
            if (this.CastingMethod == CastingMethod.PressureDieCastNonFerrous)
            {
                if (this.DensityDemand == CastingDensityDemand.Normal && this.PrecisionDemand == CastingPrecisionDemand.Normal)
                {
                    this.InnerDiePressureProposed = 300m;
                }
                else if ((this.DensityDemand == CastingDensityDemand.Pressuretight && this.PrecisionDemand == CastingPrecisionDemand.Normal)
                    || (this.DensityDemand == CastingDensityDemand.Normal && this.PrecisionDemand == CastingPrecisionDemand.TightTolerances))
                {
                    this.InnerDiePressureProposed = 600m;
                }
                else if (this.DensityDemand == CastingDensityDemand.Pressuretight && this.PrecisionDemand == CastingPrecisionDemand.TightTolerances)
                {
                    this.InnerDiePressureProposed = 900m;
                }
                else
                {
                    this.InnerDiePressureProposed = 0m;
                }
            }
        }

        /// <summary>
        /// Calculates the proposed result values.
        /// </summary>
        private void CalculateProposedResultValues()
        {
            // the material density is in g/cm^3
            decimal materialDensity = 0m;
            switch (this.MaterialType)
            {
                case CastingMaterialType.FerrousSteel:
                    materialDensity = 7.8m;
                    break;
                case CastingMaterialType.AluminiumAlloy:
                    materialDensity = 2.7m;
                    break;
                case CastingMaterialType.ZinkAlloy:
                    materialDensity = 6.4m;
                    break;
                case CastingMaterialType.MagnesiumAlloy:
                    materialDensity = 1.8m;
                    break;
                default:
                    materialDensity = 0m;
                    break;
            }

            decimal maxArea = this.ProjectedMaxArea.GetValueOrDefault();
            decimal partWeight = this.PartWeight.GetValueOrDefault();
            decimal maxThickness = this.MaxWallThickness.GetValueOrDefault();
            decimal sprue =
                this.InputtedRatioOfSprueFillers.HasValue ? this.InputtedRatioOfSprueFillers.Value : this.ProposedRatioOfSprueFillers.GetValueOrDefault();
            decimal cavities =
                this.InputtedNumberOfCavities.HasValue ? this.InputtedNumberOfCavities.Value : this.ProposedNumberOfCavities.GetValueOrDefault();

            // Calculate the Mold Box Volume
            if (materialDensity != 0m && maxArea != 0m)
            {
                this.ProposedMoldBoxVolume = maxArea * 1.5m * (partWeight * 1000m / (materialDensity * maxArea) * 2m);
            }

            // Calculate the Sand Weight
            if (materialDensity != 0m)
            {
                decimal moldBoxVolume = this.InputtedMoldBoxVolume.HasValue ? this.InputtedMoldBoxVolume.Value : this.ProposedMoldBoxVolume.GetValueOrDefault();
                decimal sumCoreVolumes = 0m;
                if (this.CoreDefinitions.Count > 0)
                {
                    sumCoreVolumes = this.CoreDefinitions.Sum(core => core.Volume.GetValueOrDefault());
                }

                this.ProposedSandWeight = (moldBoxVolume - sumCoreVolumes - (partWeight / materialDensity)) * 0.0012m;
            }

            // Calculate the Mold Box Set Up Time
            decimal sandWeight = this.InputtedSandWeight.HasValue ? this.InputtedSandWeight.Value : this.ProposedSandWeight.GetValueOrDefault();
            this.ProposedMoldBoxSetupTime = 67m * sandWeight / 100m;

            // Calculate the Melting Time
            if (this.CastingMethod == CastingMethod.GravityDieCastingNonFerrous || this.CastingMethod == CastingMethod.InvestmentCastingNonFerrous
                || this.CastingMethod == CastingMethod.LowPressureCastNonFerrous || this.CastingMethod == CastingMethod.LowPressureSandCastNonFerrous
                || this.CastingMethod == CastingMethod.PressureDieCastNonFerrous || this.CastingMethod == CastingMethod.SandCastingNonFerrous)
            {
                // For non ferrous casting
                this.ProposedMeltingTime = 3600m * (partWeight * (1m + sprue) * cavities) / 2000m;
            }
            else
            {
                // for ferrous casting
                this.ProposedMeltingTime = (partWeight * (1m + sprue) * cavities) / 9000m;
            }

            // Calculate the Pouring Time
            if (this.CastingMethod == CastingMethod.PressureDieCastNonFerrous)
            {
                this.ProposedPouringTime = 1m;
            }
            else if (materialDensity != 0 && maxArea != 0m)
            {
                this.ProposedPouringTime = partWeight * (1m + sprue) * cavities / (4m * materialDensity * (0.05m * maxArea / 100m));
            }

            // Calculate the Solidification Time
            if (this.CastingMethod == CastingMethod.PressureDieCastNonFerrous)
            {
                this.ProposedSolidificationTime = 0.0125m * maxThickness;
            }
            else
            {
                this.ProposedSolidificationTime = partWeight * (1m + sprue) * maxThickness / 20.22m;
            }

            // Calculate the Cooling Time
            this.ProposedCoolingTime = partWeight * (1m + sprue) * cavities * maxThickness * 1.7m;

            // The scrap
            this.ProposedScrap = 0.05m;
        }

        #endregion Calculations

        #region Create the casting process

        /// <summary>
        /// Creates the process steps for the casting process and adds them into the part.
        /// </summary>
        private void CreateProcessSteps()
        {
            this.CheckPart();

            DbIdentifier context = this.Part.IsMasterData ? DbIdentifier.CentralDatabase : DbIdentifier.LocalDatabase;

            try
            {
                this.measurementUnits = this.PartDataContext.MeasurementUnitRepository.GetAll();
            }
            catch
            {
                log.Warn("Casting Module, process creation: failed to get the measurement units.");
            }

            BasicSetting basicSettings = null;
            try
            {
                basicSettings = this.PartDataContext.BasicSettingsRepository.GetBasicSettings();
            }
            catch
            {
                log.Warn("Casting Module, process creation: failed to get the basic settings.");
            }

            List<PartProcessStep> newSteps = new List<PartProcessStep>();
            switch (this.CastingMethod)
            {
                case CastingMethod.SandCastingFerrous:
                case CastingMethod.SandCastingNonFerrous:
                    newSteps = CreateSandCastingProcessSteps();
                    break;
                case CastingMethod.InvestmentCastingFerrous:
                case CastingMethod.InvestmentCastingNonFerrous:
                    newSteps = CreateInvestmentCastingProcessSteps();
                    break;
                case CastingMethod.GravityDieCastingFerrous:
                case CastingMethod.GravityDieCastingNonFerrous:
                    newSteps = CreateGravityDieCastingProcessSteps();
                    break;
                case CastingMethod.CentrifugalCastingFerrous:
                    newSteps = CreateCentrifugalCastingProcessSteps();
                    break;
                case CastingMethod.LowPressureSandCastNonFerrous:
                    newSteps = CreateLowPressureSandCastProcessSteps();
                    break;
                case CastingMethod.LowPressureCastNonFerrous:
                    newSteps = CreateLowPressureCastProcessSteps();
                    break;
                case CastingMethod.PressureDieCastNonFerrous:
                    newSteps = CreatePressureDieCastProcessSteps();
                    break;
            }

            MeasurementUnit sec = this.measurementUnits.FirstOrDefault(u => u.Symbol.ToLower() == "s");
            foreach (ProcessStep step in newSteps)
            {
                if (this.Part.IsMasterData)
                {
                    step.SetIsMasterData(true);
                    step.SetOwner(null);
                }
                else
                {
                    step.SetIsMasterData(false);
                    step.SetOwner(this.Part.Owner);
                }

                step.Accuracy = (short)ProcessCalculationAccuracy.Calculated;

                // Set a default value for Parts per Cycle, Setup Time and Max Down time
                if (!step.PartsPerCycle.HasValue)
                {
                    step.PartsPerCycle = 1;
                }

                if (!step.SetupTime.HasValue)
                {
                    step.SetupTime = 0m;
                }

                if (!step.MaxDownTime.HasValue)
                {
                    step.MaxDownTime = 0m;
                }

                // Set all unset time units to second
                if (step.CycleTimeUnit == null)
                {
                    step.CycleTimeUnit = sec;
                }

                if (step.ProcessTimeUnit == null)
                {
                    step.ProcessTimeUnit = sec;
                }

                if (step.SetupTimeUnit == null)
                {
                    step.SetupTimeUnit = sec;
                }

                if (step.MaxDownTimeUnit == null)
                {
                    step.MaxDownTimeUnit = sec;
                }

                // Set default values for the of empty fields.
                ProcessStepViewModel.SetDefaultValuesForMissingStepFields(step, this.Part, basicSettings);
            }

            this.CreatedSteps.AddRange(newSteps);
        }

        /// <summary>
        /// Sets the next index for machine.
        /// The index represents the order in which machines are added in the process step.
        /// </summary>
        /// <param name="processStep">The process step.</param>
        /// <param name="machine">The machine.</param>
        private void SetNextIndexForMachine(ProcessStep processStep, Machine machine)
        {
            if (processStep != null && machine != null)
            {
                int? maxIndex = processStep.Machines.Max(m => m.Index);

                if (maxIndex == null)
                {
                    machine.Index = 1;
                }
                else
                {
                    machine.Index = maxIndex + 1;
                }
            }
        }

        /// <summary>
        /// Creates the process steps for the sand casting method.
        /// </summary>
        /// <returns>A list of process steps.</returns>
        private List<PartProcessStep> CreateSandCastingProcessSteps()
        {
            List<PartProcessStep> steps = new List<PartProcessStep>();

            PartProcessStep moldManufacturing = CreateMoldManufacturingStep();
            steps.Add(moldManufacturing);

            PartProcessStep coreManufacturing = null;
            PartProcessStep coreAndMoldAssembly = null;
            if (this.NumberOfCores > 0)
            {
                coreManufacturing = CreateCoreManufacturingStep();
                steps.Add(coreManufacturing);

                coreAndMoldAssembly = CreateCoreAndMoldAssemblyStepForSandCasting();
                steps.Add(coreAndMoldAssembly);
            }

            PartProcessStep melting = CreateMeltingStep();
            steps.Add(melting);

            PartProcessStep pouring = CreatePouringStep();
            steps.Add(pouring);

            PartProcessStep cooling = CreateCoolingStep();
            steps.Add(cooling);

            PartProcessStep castingRemoval = CreateCastingRemovalStep();
            steps.Add(castingRemoval);

            PartProcessStep deburring = CreateDeburringStep();
            steps.Add(deburring);

            PartProcessStep abrasiveBlasting = CreateAbrasiveBlastingStep();
            steps.Add(abrasiveBlasting);

            PartProcessStep xray = CreateXRayStep();
            steps.Add(xray);

            PartProcessStep checkAndPack = CreateCheckAndPackStep();
            steps.Add(checkAndPack);

            // Set the steps properties  
            Guid meltingMachGuid = SelectMeltingMachine();
            melting.ProductionSkilledLabour = 2m;
            melting.SetupSkilledLabour = 2m;

            List<Guid> moldManufacturingMachGuids = SelectMoldManufacturingMachines();
            List<Guid> coreManufacturingMachGuids = SelectCoreManufacturingMachines();
            Guid coreAndMoldAssemblyMachGuid = SelectCoreAndMoldAssemblyMachine();
            List<Guid> pouringMachinesGuid = SelectPouringMachinesForSandCast();
            List<Guid> coolingMachinesGuids = SelectCoolingMachines();
            List<Guid> castingRemovalMachinesGuids = SelectCastingRemovalMachines();
            List<Guid> deburringMachGuids = SelectDeburringMachines();
            List<Guid> abrasiveBlastingMachGuids = SelectAbrasiveBlastingMachines();
            List<Guid> xrayMachGuids = SelectXRayMachines();

            List<Guid> machinesGuids = new List<Guid>();
            machinesGuids.Add(meltingMachGuid);
            machinesGuids.AddRange(moldManufacturingMachGuids);
            machinesGuids.AddRange(pouringMachinesGuid);
            machinesGuids.AddRange(coolingMachinesGuids);
            machinesGuids.AddRange(castingRemovalMachinesGuids);
            machinesGuids.AddRange(deburringMachGuids);
            machinesGuids.AddRange(abrasiveBlastingMachGuids);
            machinesGuids.AddRange(xrayMachGuids);
            if (this.NumberOfCores > 0)
            {
                machinesGuids.AddRange(coreManufacturingMachGuids);
                machinesGuids.Add(coreAndMoldAssemblyMachGuid);
            }

            // Add the machines            
            Collection<Machine> masterMachines = GetMachines(machinesGuids);

            if (this.machinesSourceChangeSet != null && masterMachines.Count > 0)
            {
                try
                {
                    CloneManager cloneManager = new CloneManager(this.machinesSourceChangeSet, this.PartDataContext);

                    Machine meltingMach = masterMachines.FirstOrDefault(m => m.Guid == meltingMachGuid);
                    if (meltingMach != null)
                    {
                        Machine clone = cloneManager.Clone(meltingMach);
                        SetNextIndexForMachine(melting, clone);
                        melting.Machines.Add(clone);
                        clone.ProcessStep = melting;
                    }

                    foreach (Guid guid in moldManufacturingMachGuids)
                    {
                        Machine moldManufacturingMach = masterMachines.FirstOrDefault(m => m.Guid == guid);
                        if (moldManufacturingMach != null)
                        {
                            Machine clone = cloneManager.Clone(moldManufacturingMach);

                            // Sets the index for the machine. The index represents the order in which machines are added in the process step.              
                            SetNextIndexForMachine(moldManufacturing, clone);
                            moldManufacturing.Machines.Add(clone);
                            clone.ProcessStep = moldManufacturing;
                        }
                    }

                    foreach (Guid guid in coreManufacturingMachGuids)
                    {
                        Machine mach = masterMachines.FirstOrDefault(m => m.Guid == guid);
                        if (mach != null && coreManufacturing != null)
                        {
                            Machine clone = cloneManager.Clone(mach);
                            SetNextIndexForMachine(coreManufacturing, clone);
                            coreManufacturing.Machines.Add(clone);
                            clone.ProcessStep = coreManufacturing;
                        }
                    }

                    Machine coreAndMoldAssemblyMach = masterMachines.FirstOrDefault(m => m.Guid == coreAndMoldAssemblyMachGuid);
                    if (coreAndMoldAssemblyMach != null && coreAndMoldAssembly != null)
                    {
                        Machine clone = cloneManager.Clone(coreAndMoldAssemblyMach);
                        SetNextIndexForMachine(coreAndMoldAssembly, clone);
                        coreAndMoldAssembly.Machines.Add(clone);
                        clone.ProcessStep = coreAndMoldAssembly;
                    }

                    foreach (Guid guid in pouringMachinesGuid)
                    {
                        Machine mach = masterMachines.FirstOrDefault(m => m.Guid == guid);
                        if (mach != null)
                        {
                            Machine clone = cloneManager.Clone(mach);
                            SetNextIndexForMachine(pouring, clone);
                            pouring.Machines.Add(clone);
                            clone.ProcessStep = pouring;
                        }
                    }

                    foreach (Guid guid in coolingMachinesGuids)
                    {
                        Machine mach = masterMachines.FirstOrDefault(m => m.Guid == guid);
                        if (mach != null)
                        {
                            Machine clone = cloneManager.Clone(mach);
                            SetNextIndexForMachine(cooling, clone);
                            cooling.Machines.Add(clone);
                            clone.ProcessStep = cooling;
                        }
                    }

                    foreach (Guid guid in castingRemovalMachinesGuids)
                    {
                        Machine mach = masterMachines.FirstOrDefault(m => m.Guid == guid);
                        if (mach != null)
                        {
                            Machine clone = cloneManager.Clone(mach);
                            SetNextIndexForMachine(castingRemoval, clone);
                            castingRemoval.Machines.Add(clone);
                            clone.ProcessStep = castingRemoval;
                        }
                    }

                    foreach (Guid guid in deburringMachGuids)
                    {
                        Machine mach = masterMachines.FirstOrDefault(m => m.Guid == guid);
                        if (mach != null)
                        {
                            Machine clone = cloneManager.Clone(mach);
                            SetNextIndexForMachine(deburring, clone);
                            deburring.Machines.Add(clone);
                            clone.ProcessStep = deburring;
                        }
                    }

                    foreach (Guid guid in abrasiveBlastingMachGuids)
                    {
                        Machine mach = masterMachines.FirstOrDefault(m => m.Guid == guid);
                        if (mach != null)
                        {
                            Machine clone = cloneManager.Clone(mach);
                            SetNextIndexForMachine(abrasiveBlasting, clone);
                            abrasiveBlasting.Machines.Add(clone);
                            clone.ProcessStep = abrasiveBlasting;
                        }
                    }

                    foreach (Guid guid in xrayMachGuids)
                    {
                        Machine mach = masterMachines.FirstOrDefault(m => m.Guid == guid);
                        if (mach != null)
                        {
                            Machine clone = cloneManager.Clone(mach);
                            SetNextIndexForMachine(xray, clone);
                            xray.Machines.Add(clone);
                            clone.ProcessStep = xray;
                        }
                    }
                }
                catch (DataAccessException ex)
                {
                    log.ErrorException("Error while cloning the machines.", ex);
                }
            }

            if (masterMachines.Count() != machinesGuids.Distinct().Count())
            {
                this.windowService.MessageDialogService.Show(LocalizedResources.Moulding_SomeMasterDataMachinesNotLoaded, MessageDialogType.Info);
            }

            return steps;
        }

        /// <summary>
        /// Creates the process steps for gravity die casting.
        /// </summary>
        /// <returns>A list of steps.</returns>
        private List<PartProcessStep> CreateGravityDieCastingProcessSteps()
        {
            List<PartProcessStep> steps = new List<PartProcessStep>();

            PartProcessStep coreManufacturing = null;
            PartProcessStep coreAndMoldAssembly = null;
            if (this.NumberOfCores > 0)
            {
                coreManufacturing = CreateCoreManufacturingStep();
                steps.Add(coreManufacturing);

                coreAndMoldAssembly = new PartProcessStep();
                coreAndMoldAssembly.Name = LocalizedResources.CastingModule_ProcessStep_CoreAndMoldAssembly;
                steps.Add(coreAndMoldAssembly);
            }

            PartProcessStep melting = CreateMeltingStep();
            steps.Add(melting);

            PartProcessStep pouring = CreatePouringStep();
            steps.Add(pouring);

            PartProcessStep cooling = CreateCoolingStep();
            steps.Add(cooling);

            PartProcessStep deburring = CreateDeburringStep();
            steps.Add(deburring);

            PartProcessStep abrasiveBlasting = CreateAbrasiveBlastingStep();
            steps.Add(abrasiveBlasting);

            PartProcessStep xray = CreateXRayStep();
            steps.Add(xray);

            PartProcessStep checkAndPack = CreateCheckAndPackStep();
            steps.Add(checkAndPack);

            // Set the steps properties  
            Guid meltingMachGuid = SelectMeltingMachine();
            melting.ProductionSkilledLabour = 2m;
            melting.SetupSkilledLabour = 2m;

            // Core and Mold Assembly            
            if (coreAndMoldAssembly != null)
            {
                if (this.Process == CastingProcessType.Automatic)
                {
                    coreAndMoldAssembly.CycleTime = pouring.CycleTime;
                }
                else
                {
                    coreAndMoldAssembly.CycleTime = 300m;
                }

                coreAndMoldAssembly.ProcessTime = coreAndMoldAssembly.CycleTime;
                coreAndMoldAssembly.SetupSkilledLabour = 1m;
                if (this.Process == CastingProcessType.Manual)
                {
                    decimal partWeight = this.PartWeight.GetValueOrDefault();
                    if (partWeight <= 40m)
                    {
                        coreAndMoldAssembly.ProductionSkilledLabour = 1m;
                    }
                    else
                    {
                        coreAndMoldAssembly.ProductionSkilledLabour = 2m;
                    }
                }
                else
                {
                    coreAndMoldAssembly.ProductionSkilledLabour = 0.5m;
                }
            }

            List<Guid> coreManufacturingMachGuids = SelectCoreManufacturingMachines();
            Guid coreAndMoldAssemblyMachGuid = SelectCoreAndMoldAssemblyMachine();
            List<Guid> pouringMachGuids = SelectPouringMachinesForGravityDieCast();
            List<Guid> coolingMachinesGuids = SelectCoolingMachines();
            List<Guid> deburringMachGuids = SelectDeburringMachines();
            List<Guid> abrasiveBlastingMachGuids = SelectAbrasiveBlastingMachines();
            List<Guid> xrayMachGuids = SelectXRayMachines();

            List<Guid> machinesGuids = new List<Guid>();
            machinesGuids.Add(meltingMachGuid);
            machinesGuids.AddRange(pouringMachGuids);
            machinesGuids.AddRange(coolingMachinesGuids);
            machinesGuids.AddRange(deburringMachGuids);
            machinesGuids.AddRange(abrasiveBlastingMachGuids);
            machinesGuids.AddRange(xrayMachGuids);
            if (this.NumberOfCores > 0m)
            {
                machinesGuids.AddRange(coreManufacturingMachGuids);
                machinesGuids.Add(coreAndMoldAssemblyMachGuid);
            }

            // Add the machines            
            Collection<Machine> masterMachines = GetMachines(machinesGuids);

            if (this.machinesSourceChangeSet != null && masterMachines.Count > 0)
            {
                try
                {
                    CloneManager cloneManager = new CloneManager(this.machinesSourceChangeSet, this.PartDataContext);

                    Machine meltingMach = masterMachines.FirstOrDefault(m => m.Guid == meltingMachGuid);
                    if (meltingMach != null)
                    {
                        Machine clone = cloneManager.Clone(meltingMach);
                        SetNextIndexForMachine(melting, clone);
                        melting.Machines.Add(clone);
                        clone.ProcessStep = melting;
                    }

                    foreach (Guid guid in coreManufacturingMachGuids)
                    {
                        Machine mach = masterMachines.FirstOrDefault(m => m.Guid == guid);
                        if (mach != null && coreManufacturing != null)
                        {
                            Machine clone = cloneManager.Clone(mach);
                            SetNextIndexForMachine(coreManufacturing, clone);
                            coreManufacturing.Machines.Add(clone);
                            clone.ProcessStep = coreManufacturing;
                        }
                    }

                    Machine coreAndMoldAssemblyMach = masterMachines.FirstOrDefault(m => m.Guid == coreAndMoldAssemblyMachGuid);
                    if (coreAndMoldAssemblyMach != null && coreAndMoldAssembly != null)
                    {
                        Machine clone = cloneManager.Clone(coreAndMoldAssemblyMach);
                        SetNextIndexForMachine(coreAndMoldAssembly, clone);
                        coreAndMoldAssembly.Machines.Add(clone);
                        clone.ProcessStep = coreAndMoldAssembly;
                    }

                    foreach (Guid guid in pouringMachGuids)
                    {
                        Machine mach = masterMachines.FirstOrDefault(m => m.Guid == guid);
                        if (mach != null)
                        {
                            Machine clone = cloneManager.Clone(mach);
                            SetNextIndexForMachine(pouring, clone);
                            pouring.Machines.Add(clone);
                            clone.ProcessStep = pouring;
                        }
                    }

                    foreach (Guid guid in coolingMachinesGuids)
                    {
                        Machine mach = masterMachines.FirstOrDefault(m => m.Guid == guid);
                        if (mach != null)
                        {
                            Machine clone = cloneManager.Clone(mach);
                            SetNextIndexForMachine(cooling, clone);
                            cooling.Machines.Add(clone);
                            clone.ProcessStep = cooling;
                        }
                    }

                    foreach (Guid guid in deburringMachGuids)
                    {
                        Machine mach = masterMachines.FirstOrDefault(m => m.Guid == guid);
                        if (mach != null)
                        {
                            Machine clone = cloneManager.Clone(mach);
                            SetNextIndexForMachine(deburring, clone);
                            deburring.Machines.Add(clone);
                            clone.ProcessStep = deburring;
                        }
                    }

                    foreach (Guid guid in abrasiveBlastingMachGuids)
                    {
                        Machine mach = masterMachines.FirstOrDefault(m => m.Guid == guid);
                        if (mach != null)
                        {
                            Machine clone = cloneManager.Clone(mach);
                            SetNextIndexForMachine(abrasiveBlasting, clone);
                            abrasiveBlasting.Machines.Add(clone);
                            clone.ProcessStep = abrasiveBlasting;
                        }
                    }

                    foreach (Guid guid in xrayMachGuids)
                    {
                        Machine mach = masterMachines.FirstOrDefault(m => m.Guid == guid);
                        if (mach != null)
                        {
                            Machine clone = cloneManager.Clone(mach);
                            SetNextIndexForMachine(xray, clone);
                            xray.Machines.Add(clone);
                            clone.ProcessStep = xray;
                        }
                    }
                }
                catch (DataAccessException ex)
                {
                    log.ErrorException("Error while cloning the machines.", ex);
                }
            }

            if (masterMachines.Count() != machinesGuids.Distinct().Count())
            {
                this.windowService.MessageDialogService.Show(LocalizedResources.Moulding_SomeMasterDataMachinesNotLoaded, MessageDialogType.Info);
            }

            return steps;
        }

        /// <summary>
        /// Creates the process steps for low pressure cast.
        /// </summary>
        /// <returns>A list of process steps.</returns>
        private List<PartProcessStep> CreateLowPressureCastProcessSteps()
        {
            List<PartProcessStep> steps = new List<PartProcessStep>();

            PartProcessStep coreManufacturing = null;
            PartProcessStep coreAndMoldAssembly = null;
            if (this.NumberOfCores > 0m)
            {
                coreManufacturing = CreateCoreManufacturingStep();
                steps.Add(coreManufacturing);

                coreAndMoldAssembly = new PartProcessStep();
                coreAndMoldAssembly.Name = LocalizedResources.CastingModule_ProcessStep_CoreAndMoldAssembly;
                steps.Add(coreAndMoldAssembly);
            }

            PartProcessStep melting = CreateMeltingStep();
            steps.Add(melting);

            PartProcessStep pouring = CreatePouringStep();
            steps.Add(pouring);

            PartProcessStep cooling = CreateCoolingStep();
            steps.Add(cooling);

            PartProcessStep deburring = CreateDeburringStep();
            steps.Add(deburring);

            PartProcessStep abrasiveBlasting = CreateAbrasiveBlastingStep();
            steps.Add(abrasiveBlasting);

            PartProcessStep xray = CreateXRayStep();
            steps.Add(xray);

            PartProcessStep checkAndPack = CreateCheckAndPackStep();
            steps.Add(checkAndPack);

            // Set the steps properties  
            Guid meltingMachGuid = SelectMeltingMachine();
            melting.ProductionSkilledLabour = 2m;
            melting.SetupSkilledLabour = 2m;

            // Core and Mold Assembly            
            if (coreAndMoldAssembly != null)
            {
                if (this.Process == CastingProcessType.Automatic)
                {
                    coreAndMoldAssembly.CycleTime = pouring.CycleTime;
                }
                else
                {
                    coreAndMoldAssembly.CycleTime = 300m;
                }

                coreAndMoldAssembly.ProcessTime = coreAndMoldAssembly.CycleTime;
                coreAndMoldAssembly.SetupSkilledLabour = 1m;
                if (this.Process == CastingProcessType.Manual)
                {
                    decimal partWeight = this.PartWeight.GetValueOrDefault();
                    if (partWeight <= 40m)
                    {
                        coreAndMoldAssembly.ProductionSkilledLabour = 1m;
                    }
                    else
                    {
                        coreAndMoldAssembly.ProductionSkilledLabour = 2m;
                    }
                }
                else
                {
                    coreAndMoldAssembly.ProductionSkilledLabour = 0.5m;
                }
            }

            List<Guid> coreManufacturingMachGuids = SelectCoreManufacturingMachines();
            Guid coreAndMoldAssemblyMachGuid = SelectCoreAndMoldAssemblyMachine();
            List<Guid> pouringMachGuids = SelectPouringMachinesForLowPressureCast();
            List<Guid> coolingMachinesGuids = SelectCoolingMachines();
            List<Guid> deburringMachGuids = SelectDeburringMachines();
            List<Guid> abrasiveBlastingMachGuids = SelectAbrasiveBlastingMachines();
            List<Guid> xrayMachGuids = SelectXRayMachines();

            List<Guid> machinesGuids = new List<Guid>();
            machinesGuids.Add(meltingMachGuid);
            machinesGuids.AddRange(pouringMachGuids);
            machinesGuids.AddRange(coolingMachinesGuids);
            machinesGuids.AddRange(deburringMachGuids);
            machinesGuids.AddRange(abrasiveBlastingMachGuids);
            machinesGuids.AddRange(xrayMachGuids);
            if (this.NumberOfCores > 0)
            {
                machinesGuids.AddRange(coreManufacturingMachGuids);
                machinesGuids.Add(coreAndMoldAssemblyMachGuid);
            }

            // Add the machines            
            Collection<Machine> masterMachines = GetMachines(machinesGuids);

            if (this.machinesSourceChangeSet != null && masterMachines.Count > 0)
            {
                try
                {
                    CloneManager cloneManager = new CloneManager(this.machinesSourceChangeSet, this.PartDataContext);

                    Machine meltingMach = masterMachines.FirstOrDefault(m => m.Guid == meltingMachGuid);
                    if (meltingMach != null)
                    {
                        Machine clone = cloneManager.Clone(meltingMach);
                        SetNextIndexForMachine(melting, clone);
                        melting.Machines.Add(clone);
                        clone.ProcessStep = melting;
                    }

                    foreach (Guid guid in coreManufacturingMachGuids)
                    {
                        Machine mach = masterMachines.FirstOrDefault(m => m.Guid == guid);
                        if (mach != null && coreManufacturing != null)
                        {
                            Machine clone = cloneManager.Clone(mach);
                            SetNextIndexForMachine(coreManufacturing, clone);
                            coreManufacturing.Machines.Add(clone);
                            clone.ProcessStep = coreManufacturing;
                        }
                    }

                    Machine coreAndMoldAssemblyMach = masterMachines.FirstOrDefault(m => m.Guid == coreAndMoldAssemblyMachGuid);
                    if (coreAndMoldAssemblyMach != null && coreAndMoldAssembly != null)
                    {
                        Machine clone = cloneManager.Clone(coreAndMoldAssemblyMach);
                        SetNextIndexForMachine(coreAndMoldAssembly, clone);
                        coreAndMoldAssembly.Machines.Add(clone);
                        clone.ProcessStep = coreAndMoldAssembly;
                    }

                    foreach (Guid guid in pouringMachGuids)
                    {
                        Machine mach = masterMachines.FirstOrDefault(m => m.Guid == guid);
                        if (mach != null)
                        {
                            Machine clone = cloneManager.Clone(mach);
                            SetNextIndexForMachine(pouring, clone);
                            pouring.Machines.Add(clone);
                            clone.ProcessStep = pouring;
                        }
                    }

                    foreach (Guid guid in coolingMachinesGuids)
                    {
                        Machine mach = masterMachines.FirstOrDefault(m => m.Guid == guid);
                        if (mach != null)
                        {
                            Machine clone = cloneManager.Clone(mach);
                            SetNextIndexForMachine(cooling, clone);
                            cooling.Machines.Add(clone);
                            clone.ProcessStep = cooling;
                        }
                    }

                    foreach (Guid guid in deburringMachGuids)
                    {
                        Machine mach = masterMachines.FirstOrDefault(m => m.Guid == guid);
                        if (mach != null)
                        {
                            Machine clone = cloneManager.Clone(mach);
                            SetNextIndexForMachine(deburring, clone);
                            deburring.Machines.Add(clone);
                            clone.ProcessStep = deburring;
                        }
                    }

                    foreach (Guid guid in abrasiveBlastingMachGuids)
                    {
                        Machine mach = masterMachines.FirstOrDefault(m => m.Guid == guid);
                        if (mach != null)
                        {
                            Machine clone = cloneManager.Clone(mach);
                            SetNextIndexForMachine(abrasiveBlasting, clone);
                            abrasiveBlasting.Machines.Add(clone);
                            clone.ProcessStep = abrasiveBlasting;
                        }
                    }

                    foreach (Guid guid in xrayMachGuids)
                    {
                        Machine mach = masterMachines.FirstOrDefault(m => m.Guid == guid);
                        if (mach != null)
                        {
                            Machine clone = cloneManager.Clone(mach);
                            SetNextIndexForMachine(xray, clone);
                            xray.Machines.Add(clone);
                            clone.ProcessStep = xray;
                        }
                    }
                }
                catch (DataAccessException ex)
                {
                    log.ErrorException("Error while cloning the machines.", ex);
                }
            }

            if (masterMachines.Count() != machinesGuids.Distinct().Count())
            {
                this.windowService.MessageDialogService.Show(LocalizedResources.Moulding_SomeMasterDataMachinesNotLoaded, MessageDialogType.Info);
            }

            return steps;
        }

        /// <summary>
        /// Creates the process steps for low pressure sand cast.
        /// </summary>
        /// <returns>A list of process steps</returns>
        private List<PartProcessStep> CreateLowPressureSandCastProcessSteps()
        {
            List<PartProcessStep> steps = new List<PartProcessStep>();

            PartProcessStep moldManufacturing = CreateMoldManufacturingStep();
            steps.Add(moldManufacturing);

            PartProcessStep coreManufacturing = null;
            PartProcessStep coreAndMoldAssembly = null;
            if (this.NumberOfCores > 0)
            {
                coreManufacturing = CreateCoreManufacturingStep();
                steps.Add(coreManufacturing);

                coreAndMoldAssembly = CreateCoreAndMoldAssemblyStepForSandCasting();
                steps.Add(coreAndMoldAssembly);
            }

            PartProcessStep melting = CreateMeltingStep();
            steps.Add(melting);

            PartProcessStep pouring = CreatePouringStep();
            steps.Add(pouring);

            PartProcessStep cooling = CreateCoolingStep();
            steps.Add(cooling);

            PartProcessStep castingRemoval = CreateCastingRemovalStep();
            steps.Add(castingRemoval);

            PartProcessStep deburring = CreateDeburringStep();
            steps.Add(deburring);

            PartProcessStep abrasiveBlasting = CreateAbrasiveBlastingStep();
            steps.Add(abrasiveBlasting);

            PartProcessStep xray = CreateXRayStep();
            steps.Add(xray);

            PartProcessStep checkAndPack = CreateCheckAndPackStep();
            steps.Add(checkAndPack);

            // Set the steps properties  
            Guid meltingMachGuid = SelectMeltingMachine();
            melting.ProductionSkilledLabour = 2m;
            melting.SetupSkilledLabour = 2m;

            List<Guid> moldManufacturingMachGuids = SelectMoldManufacturingMachines();
            List<Guid> coreManufacturingMachGuids = SelectCoreManufacturingMachines();
            Guid coreAndMoldAssemblyMachGuid = SelectCoreAndMoldAssemblyMachine();
            List<Guid> pouringMachGuids = SelectPouringMachinesForLowPressureCast();
            List<Guid> coolingMachinesGuids = SelectCoolingMachines();
            List<Guid> castingRemovalMachinesGuids = SelectCastingRemovalMachines();
            List<Guid> deburringMachGuids = SelectDeburringMachines();
            List<Guid> abrasiveBlastingMachGuids = SelectAbrasiveBlastingMachines();
            List<Guid> xrayMachGuids = SelectXRayMachines();

            List<Guid> machinesGuids = new List<Guid>();
            machinesGuids.Add(meltingMachGuid);
            machinesGuids.AddRange(moldManufacturingMachGuids);
            machinesGuids.AddRange(pouringMachGuids);
            machinesGuids.AddRange(coolingMachinesGuids);
            machinesGuids.AddRange(castingRemovalMachinesGuids);
            machinesGuids.AddRange(deburringMachGuids);
            machinesGuids.AddRange(abrasiveBlastingMachGuids);
            machinesGuids.AddRange(xrayMachGuids);
            if (this.NumberOfCores > 0m)
            {
                machinesGuids.AddRange(coreManufacturingMachGuids);
                machinesGuids.Add(coreAndMoldAssemblyMachGuid);
            }

            // Add the machines            
            Collection<Machine> masterMachines = GetMachines(machinesGuids);

            if (this.machinesSourceChangeSet != null && masterMachines.Count > 0)
            {
                try
                {
                    CloneManager cloneManager = new CloneManager(this.machinesSourceChangeSet, this.PartDataContext);

                    Machine meltingMach = masterMachines.FirstOrDefault(m => m.Guid == meltingMachGuid);
                    if (meltingMach != null)
                    {
                        Machine clone = cloneManager.Clone(meltingMach);
                        SetNextIndexForMachine(melting, clone);
                        melting.Machines.Add(clone);
                        clone.ProcessStep = melting;
                    }

                    foreach (Guid guid in moldManufacturingMachGuids)
                    {
                        Machine moldManufacturingMach = masterMachines.FirstOrDefault(m => m.Guid == guid);
                        if (moldManufacturingMach != null)
                        {
                            Machine clone = cloneManager.Clone(moldManufacturingMach);
                            SetNextIndexForMachine(moldManufacturing, clone);
                            moldManufacturing.Machines.Add(clone);
                            clone.ProcessStep = moldManufacturing;
                        }
                    }

                    foreach (Guid guid in coreManufacturingMachGuids)
                    {
                        Machine mach = masterMachines.FirstOrDefault(m => m.Guid == guid);
                        if (mach != null && coreManufacturing != null)
                        {
                            Machine clone = cloneManager.Clone(mach);
                            SetNextIndexForMachine(coreManufacturing, clone);
                            coreManufacturing.Machines.Add(clone);
                            clone.ProcessStep = coreManufacturing;
                        }
                    }

                    Machine coreAndMoldAssemblyMach = masterMachines.FirstOrDefault(m => m.Guid == coreAndMoldAssemblyMachGuid);
                    if (coreAndMoldAssemblyMach != null && coreAndMoldAssembly != null)
                    {
                        Machine clone = cloneManager.Clone(coreAndMoldAssemblyMach);
                        SetNextIndexForMachine(coreAndMoldAssembly, clone);
                        coreAndMoldAssembly.Machines.Add(clone);
                        clone.ProcessStep = coreAndMoldAssembly;
                    }

                    foreach (Guid guid in pouringMachGuids)
                    {
                        Machine mach = masterMachines.FirstOrDefault(m => m.Guid == guid);
                        if (mach != null)
                        {
                            Machine clone = cloneManager.Clone(mach);
                            SetNextIndexForMachine(pouring, clone);
                            pouring.Machines.Add(clone);
                            clone.ProcessStep = pouring;
                        }
                    }

                    foreach (Guid guid in coolingMachinesGuids)
                    {
                        Machine mach = masterMachines.FirstOrDefault(m => m.Guid == guid);
                        if (mach != null)
                        {
                            Machine clone = cloneManager.Clone(mach);
                            SetNextIndexForMachine(cooling, clone);
                            cooling.Machines.Add(clone);
                            clone.ProcessStep = cooling;
                        }
                    }

                    foreach (Guid guid in castingRemovalMachinesGuids)
                    {
                        Machine mach = masterMachines.FirstOrDefault(m => m.Guid == guid);
                        if (mach != null)
                        {
                            Machine clone = cloneManager.Clone(mach);
                            SetNextIndexForMachine(castingRemoval, clone);
                            castingRemoval.Machines.Add(clone);
                            clone.ProcessStep = castingRemoval;
                        }
                    }

                    foreach (Guid guid in deburringMachGuids)
                    {
                        Machine mach = masterMachines.FirstOrDefault(m => m.Guid == guid);
                        if (mach != null)
                        {
                            Machine clone = cloneManager.Clone(mach);
                            SetNextIndexForMachine(deburring, clone);
                            deburring.Machines.Add(clone);
                            clone.ProcessStep = deburring;
                        }
                    }

                    foreach (Guid guid in abrasiveBlastingMachGuids)
                    {
                        Machine mach = masterMachines.FirstOrDefault(m => m.Guid == guid);
                        if (mach != null)
                        {
                            Machine clone = cloneManager.Clone(mach);
                            SetNextIndexForMachine(abrasiveBlasting, clone);
                            abrasiveBlasting.Machines.Add(clone);
                            clone.ProcessStep = abrasiveBlasting;
                        }
                    }

                    foreach (Guid guid in xrayMachGuids)
                    {
                        Machine mach = masterMachines.FirstOrDefault(m => m.Guid == guid);
                        if (mach != null)
                        {
                            Machine clone = cloneManager.Clone(mach);
                            SetNextIndexForMachine(xray, clone);
                            xray.Machines.Add(clone);
                            clone.ProcessStep = xray;
                        }
                    }
                }
                catch (DataAccessException ex)
                {
                    log.ErrorException("Error while cloning the machines.", ex);
                }
            }

            if (masterMachines.Count() != machinesGuids.Distinct().Count())
            {
                this.windowService.MessageDialogService.Show(LocalizedResources.Moulding_SomeMasterDataMachinesNotLoaded, MessageDialogType.Info);
            }

            return steps;
        }

        /// <summary>
        /// Creates the process steps for pressure die cast.
        /// </summary>
        /// <returns>A list of process steps.</returns>
        private List<PartProcessStep> CreatePressureDieCastProcessSteps()
        {
            List<PartProcessStep> steps = new List<PartProcessStep>();

            // Create the steps
            PartProcessStep melting = CreateMeltingStep();
            steps.Add(melting);

            PartProcessStep pouring = CreatePouringStep();
            pouring.Name = LocalizedResources.CastingModule_ProcessStep_PouringPressure;
            steps.Add(pouring);

            PartProcessStep cooling = CreateCoolingStep();
            steps.Add(cooling);

            PartProcessStep deburring = new PartProcessStep();
            deburring.Name = LocalizedResources.CastingModule_ProcessStep_Deburring;
            steps.Add(deburring);

            PartProcessStep abrasiveBlasting = new PartProcessStep();
            abrasiveBlasting.Name = LocalizedResources.CastingModule_ProcessStep_AbrasiveBlasting;
            steps.Add(abrasiveBlasting);

            PartProcessStep xray = new PartProcessStep();
            xray.Name = LocalizedResources.CastingModule_ProcessStep_XRay;
            steps.Add(xray);

            PartProcessStep checkAndPack = new PartProcessStep();
            checkAndPack.Name = LocalizedResources.CastingModule_ProcessStep_CheckAndPack;
            steps.Add(checkAndPack);

            // Set the steps properties            
            decimal innerDiePressure =
                this.InnerDiePressureInputted.HasValue ? this.InnerDiePressureInputted.Value : this.InnerDiePressureProposed.GetValueOrDefault();
            decimal clampingForce = this.ProjectedMaxArea.GetValueOrDefault() * innerDiePressure / 100m;

            Guid meltingMachineGuid = SelectMeltingMachineForPressureDieCasting(clampingForce);
            melting.PartsPerCycle = 1;
            melting.SetupTime = 0m;
            melting.MaxDownTime = 0m;
            melting.ProductionSkilledLabour = 1m;
            melting.SetupSkilledLabour = 1m;

            List<Guid> pouringMachinesGuids = SelectPouringMachinesForPressureDieCasting(clampingForce);
            pouring.PartsPerCycle = 1;
            pouring.SetupTime = 0m;
            pouring.MaxDownTime = 0m;
            pouring.ProductionSkilledLabour = 0.5m;
            pouring.SetupSkilledLabour = 1m;

            cooling.PartsPerCycle = 1;
            cooling.SetupTime = 0m;
            cooling.MaxDownTime = 0m;
            cooling.ProductionSkilledLabour = 1m;
            cooling.SetupSkilledLabour = 1m;

            Guid deburringMachGuid = SelectDeburringMachineForPressureDieCasting(clampingForce);
            deburring.PartsPerCycle = 1;
            deburring.CycleTime = pouring.CycleTime;
            deburring.CycleTimeUnit = pouring.CycleTimeUnit;
            deburring.ProcessTime = pouring.ProcessTime;
            deburring.ProcessTimeUnit = pouring.ProcessTimeUnit;
            deburring.SetupTime = 0m;
            deburring.MaxDownTime = 0m;
            deburring.ProductionSkilledLabour = 0.5m;
            deburring.SetupSkilledLabour = 1m;

            Guid abrasiveBlastingMachGuid = SelectAbrasiveBlastingMachineForPressureDieCasting(clampingForce);
            abrasiveBlasting.PartsPerCycle = 1;
            abrasiveBlasting.CycleTime = pouring.CycleTime;
            abrasiveBlasting.CycleTimeUnit = pouring.CycleTimeUnit;
            abrasiveBlasting.ProcessTime = pouring.ProcessTime;
            abrasiveBlasting.ProcessTimeUnit = pouring.ProcessTimeUnit;
            abrasiveBlasting.SetupTime = 600m;
            abrasiveBlasting.MaxDownTime = 600m;
            abrasiveBlasting.ProductionSkilledLabour = 0.5m;
            abrasiveBlasting.SetupSkilledLabour = 1m;

            Guid xrayGuid = new Guid("5b89de1b-8276-4010-b760-c752c91f7b95");
            xray.PartsPerCycle = 100;
            xray.CycleTime = pouring.CycleTime;
            xray.CycleTimeUnit = pouring.CycleTimeUnit;
            xray.ProcessTime = pouring.ProcessTime;
            xray.ProcessTimeUnit = pouring.ProcessTimeUnit;
            xray.SetupTime = 600m;
            xray.MaxDownTime = 600m;
            xray.ProductionSkilledLabour = 1m;
            xray.SetupSkilledLabour = 1m;

            checkAndPack.PartsPerCycle = 1;
            checkAndPack.CycleTime = pouring.CycleTime;
            checkAndPack.CycleTimeUnit = pouring.CycleTimeUnit;
            checkAndPack.ProcessTime = pouring.ProcessTime;
            checkAndPack.ProcessTimeUnit = pouring.ProcessTimeUnit;
            checkAndPack.SetupTime = 0m;
            checkAndPack.MaxDownTime = 0m;
            checkAndPack.ProductionSkilledLabour = 1m;
            checkAndPack.SetupSkilledLabour = 1m;

            List<Guid> machinesGuids = new List<Guid>();
            machinesGuids.Add(meltingMachineGuid);
            machinesGuids.AddRange(pouringMachinesGuids);
            machinesGuids.Add(deburringMachGuid);
            machinesGuids.Add(abrasiveBlastingMachGuid);
            machinesGuids.Add(xrayGuid);

            // Add the machines            
            Collection<Machine> masterMachines = GetMachines(machinesGuids);

            if (this.machinesSourceChangeSet != null && masterMachines.Count > 0)
            {
                try
                {
                    CloneManager cloneManager = new CloneManager(this.machinesSourceChangeSet, this.PartDataContext);

                    Machine meltingMach = masterMachines.FirstOrDefault(m => m.Guid == meltingMachineGuid);
                    if (meltingMach != null)
                    {
                        Machine clone = cloneManager.Clone(meltingMach);
                        SetNextIndexForMachine(melting, clone);
                        melting.Machines.Add(clone);
                        clone.ProcessStep = melting;
                    }

                    foreach (Guid machineGuid in pouringMachinesGuids)
                    {
                        Machine pouringMach = masterMachines.FirstOrDefault(m => m.Guid == machineGuid);
                        if (pouringMach != null)
                        {
                            Machine clone = cloneManager.Clone(pouringMach);
                            SetNextIndexForMachine(pouring, clone);
                            pouring.Machines.Add(clone);
                            clone.ProcessStep = pouring;
                        }
                    }

                    Machine deburringMach = masterMachines.FirstOrDefault(m => m.Guid == deburringMachGuid);
                    if (deburringMach != null)
                    {
                        Machine clone = cloneManager.Clone(deburringMach);
                        SetNextIndexForMachine(deburring, clone);
                        deburring.Machines.Add(clone);
                        clone.ProcessStep = deburring;
                    }

                    Machine abrasiveBlastingMach = masterMachines.FirstOrDefault(m => m.Guid == abrasiveBlastingMachGuid);
                    if (abrasiveBlastingMach != null)
                    {
                        Machine clone = cloneManager.Clone(abrasiveBlastingMach);
                        SetNextIndexForMachine(abrasiveBlasting, clone);
                        abrasiveBlasting.Machines.Add(clone);
                        clone.ProcessStep = abrasiveBlasting;
                    }

                    Machine xrayMach = masterMachines.FirstOrDefault(m => m.Guid == xrayGuid);
                    if (xrayMach != null)
                    {
                        Machine clone = cloneManager.Clone(xrayMach);
                        SetNextIndexForMachine(xray, clone);
                        xray.Machines.Add(clone);
                        clone.ProcessStep = xray;
                    }
                }
                catch (DataAccessException ex)
                {
                    log.ErrorException("Error while cloning the machines.", ex);
                }
            }

            if (masterMachines.Count() != machinesGuids.Distinct().Count())
            {
                this.windowService.MessageDialogService.Show(LocalizedResources.Moulding_SomeMasterDataMachinesNotLoaded, MessageDialogType.Info);
            }

            return steps;
        }

        /// <summary>
        /// Creates the process steps for centrifugal casting.
        /// </summary>
        /// <returns>A list of steps.</returns>
        private List<PartProcessStep> CreateCentrifugalCastingProcessSteps()
        {
            List<PartProcessStep> steps = new List<PartProcessStep>();

            // Create the steps
            PartProcessStep meltingAndFilling = new PartProcessStep();
            meltingAndFilling.Name = LocalizedResources.CastingModule_ProcessStep_MeltingAndFilling;
            steps.Add(meltingAndFilling);

            PartProcessStep preparationOfDie = new PartProcessStep();
            preparationOfDie.Name = LocalizedResources.CastingModule_ProcessStep_PreparationOfDie;
            steps.Add(preparationOfDie);

            PartProcessStep pouringAndSolidification = new PartProcessStep();
            pouringAndSolidification.Name = LocalizedResources.CastingModule_ProcessStep_PouringAndSolidification;
            steps.Add(pouringAndSolidification);

            PartProcessStep removalFromCast = new PartProcessStep();
            removalFromCast.Name = LocalizedResources.CastingModule_ProcessStep_RemovalFromCast;
            steps.Add(removalFromCast);

            PartProcessStep cooling = new PartProcessStep();
            cooling.Name = LocalizedResources.CastingModule_ProcessStep_Cooling;
            steps.Add(cooling);

            PartProcessStep cutOffEnds = new PartProcessStep();
            cutOffEnds.Name = LocalizedResources.CastingModule_ProcessStep_CutOffEnds;
            steps.Add(cutOffEnds);

            PartProcessStep blast = new PartProcessStep();
            blast.Name = LocalizedResources.CastingModule_ProcessStep_Blast;
            steps.Add(blast);

            PartProcessStep heatTreatement = new PartProcessStep();
            heatTreatement.Name = LocalizedResources.CastingModule_ProcessStep_HeatTreatment;
            steps.Add(heatTreatement);

            PartProcessStep cooling2 = new PartProcessStep();
            cooling2.Name = LocalizedResources.CastingModule_ProcessStep_Cooling;
            steps.Add(cooling2);

            PartProcessStep magneticParticleAnalysis = new PartProcessStep();
            magneticParticleAnalysis.Name = LocalizedResources.CastingModule_ProcessStep_MagneticParticleAnalysis;
            steps.Add(magneticParticleAnalysis);

            // Calculate the steps cycle & process time
            ICostCalculator calculator = CostCalculatorFactory.GetCalculator(this.Part.CalculationVariant);
            decimal processMaterial = calculator.CalculateProcessMaterial(this.Part);

            meltingAndFilling.CycleTime = (processMaterial / 650m) * 3600m;
            meltingAndFilling.ProcessTime = meltingAndFilling.CycleTime;
            meltingAndFilling.PartsPerCycle = 4;
            meltingAndFilling.ProductionUnskilledLabour = 0.25m;

            preparationOfDie.CycleTime = 1800m;
            preparationOfDie.ProcessTime = preparationOfDie.CycleTime;
            preparationOfDie.PartsPerCycle = 1;
            preparationOfDie.ProductionUnskilledLabour = 2m;
            preparationOfDie.SetupUnskilledLabour = 2m;

            pouringAndSolidification.CycleTime = (processMaterial / 650m) * 1.05m * 1069m;
            pouringAndSolidification.ProcessTime = pouringAndSolidification.CycleTime;
            pouringAndSolidification.ScrapAmount = 0.02m;
            pouringAndSolidification.PartsPerCycle = 1;
            pouringAndSolidification.ProductionUnskilledLabour = 2m;
            pouringAndSolidification.SetupUnskilledLabour = 2m;

            removalFromCast.CycleTime = 1800m;
            removalFromCast.ProcessTime = removalFromCast.CycleTime;
            removalFromCast.PartsPerCycle = 1;
            removalFromCast.ProductionUnskilledLabour = 2m;

            cooling.CycleTime = (processMaterial / 650m) * 0.92m * 32619m;
            cooling.ProcessTime = cooling.CycleTime;
            cooling.PartsPerCycle = 20;

            cutOffEnds.CycleTime = (processMaterial / 650m) * 0.7m * 750m;
            cutOffEnds.ProcessTime = cutOffEnds.CycleTime;
            cutOffEnds.ScrapAmount = 0.005m;
            cutOffEnds.PartsPerCycle = 1;
            cutOffEnds.ProductionUnskilledLabour = 0.5m;
            cutOffEnds.SetupUnskilledLabour = 1m;

            blast.CycleTime = 120m;
            blast.ProcessTime = blast.CycleTime;
            blast.PartsPerCycle = 1;
            blast.ProductionUnskilledLabour = 0.5m;

            heatTreatement.CycleTime = 32400m;
            heatTreatement.ProcessTime = heatTreatement.CycleTime;
            heatTreatement.PartsPerCycle = 10;
            heatTreatement.ProductionUnskilledLabour = 0.5m;
            heatTreatement.SetupUnskilledLabour = 1m;

            cooling2.CycleTime = (processMaterial / 650m) * 0.92m * 18000m;
            cooling2.PartsPerCycle = 20;
            cooling2.ProcessTime = cooling2.CycleTime;

            magneticParticleAnalysis.CycleTime = 1800m;
            magneticParticleAnalysis.ProcessTime = magneticParticleAnalysis.CycleTime;
            magneticParticleAnalysis.PartsPerCycle = 1;
            magneticParticleAnalysis.ProductionSkilledLabour = 2m;
            magneticParticleAnalysis.SetupSkilledLabour = 2m;

            // Add the tools
            Die cuttingTools = new Die();
            cuttingTools.Name = "Cutting Tools";
            cuttingTools.Type = DieType.Tools;
            cuttingTools.Investment = 4.038m;
            cuttingTools.ReusableInvest = 0m;
            cuttingTools.Wear = 0m;
            cuttingTools.Maintenance = 0m;
            cuttingTools.LifeTime = 1m;
            cuttingTools.AllocationRatio = 1m;
            cuttingTools.DiesetsNumberPaidByCustomer = 0;
            cuttingTools.CostAllocationBasedOnPartsPerLifeTime = true;
            cuttingTools.Manufacturer = new Manufacturer();
            cuttingTools.Manufacturer.Name = "Kenametal";
            cuttingTools.AreAllPaidByCustomer = false;

            cutOffEnds.Dies.Add(cuttingTools);

            // Add the consumables
            MeasurementUnit kg = this.measurementUnits.FirstOrDefault(u => u.Symbol.ToLower() == "kg");

            Consumable holcote = new Consumable();
            holcote.Name = "Holcote 110";
            holcote.Amount = 0.9m;
            holcote.AmountUnitBase = kg;
            holcote.Price = 2.3555m;
            holcote.PriceUnitBase = kg;
            holcote.Description = @"Refractory  (price based on request (Pre Mixed and homogenized)" + Environment.NewLine + "Baise Price Zi-Silcate: 0,8$/kg";
            holcote.Manufacturer = new Manufacturer();
            holcote.Manufacturer.Name = "Foseco";
            holcote.Manufacturer.Description = "Refractory on Zircon-Silicate Base";
            preparationOfDie.Consumables.Add(holcote);

            Consumable fluxPowder = new Consumable();
            fluxPowder.Name = "Flux  Powder";
            fluxPowder.Amount = 0.3m;
            fluxPowder.AmountUnitBase = kg;
            fluxPowder.Price = 2.0906m;
            fluxPowder.PriceUnitBase = kg;
            fluxPowder.Manufacturer = new Manufacturer();
            fluxPowder.Manufacturer.Name = "Interfflux";
            magneticParticleAnalysis.Consumables.Add(fluxPowder);

            // Add the machines
            List<Guid> machinesGuids = new List<Guid>();
            Guid meltingAndFillingMachGuid = new Guid("cdcf68dc-fc68-43b0-9315-03f2b8367b0c");
            Guid prepOfDieMachGuid = new Guid("9e282358-8a63-4848-91e4-f6db75521fb6");
            Guid pouringAndSolidMachGuid = new Guid("9e282358-8a63-4848-91e4-f6db75521fb6");
            Guid coolingMachGuid = new Guid("0198f482-e127-4723-b234-b22a8d1e5a28");
            Guid cutOffEndsMachGuid = new Guid("680b7d5e-4b74-499a-b605-b4eee7c680d4");
            Guid blastMachGuid = new Guid("cbb99cf4-7339-46fa-9ba9-77f117612866");
            Guid heatTreatmentMachGuid = new Guid("30a6b892-3151-48bf-9b2b-65368c53e9ab");
            Guid magParticleMachGuid = new Guid("790239d8-9176-44b8-a382-9e9510115619");

            machinesGuids.Add(meltingAndFillingMachGuid);
            machinesGuids.Add(prepOfDieMachGuid);
            machinesGuids.Add(pouringAndSolidMachGuid);
            machinesGuids.Add(coolingMachGuid);
            machinesGuids.Add(cutOffEndsMachGuid);
            machinesGuids.Add(blastMachGuid);
            machinesGuids.Add(heatTreatmentMachGuid);
            machinesGuids.Add(magParticleMachGuid);

            // Add the machines            
            Collection<Machine> masterMachines = GetMachines(machinesGuids);

            if (this.machinesSourceChangeSet != null && masterMachines.Count > 0)
            {
                try
                {
                    CloneManager cloneManager = new CloneManager(this.machinesSourceChangeSet, this.PartDataContext);

                    Machine meltingMach = masterMachines.FirstOrDefault(m => m.Guid == meltingAndFillingMachGuid);
                    if (meltingMach != null)
                    {
                        Machine clone = cloneManager.Clone(meltingMach);
                        SetNextIndexForMachine(meltingAndFilling, clone);
                        meltingAndFilling.Machines.Add(clone);
                        clone.ProcessStep = meltingAndFilling;
                    }

                    Machine prepOfDieMach = masterMachines.FirstOrDefault(m => m.Guid == prepOfDieMachGuid);
                    if (prepOfDieMach != null)
                    {
                        Machine clone = cloneManager.Clone(prepOfDieMach);
                        SetNextIndexForMachine(preparationOfDie, clone);
                        preparationOfDie.Machines.Add(clone);
                        clone.ProcessStep = preparationOfDie;
                    }

                    Machine pouringMach = masterMachines.FirstOrDefault(m => m.Guid == pouringAndSolidMachGuid);
                    if (pouringMach != null)
                    {
                        Machine clone = cloneManager.Clone(pouringMach);
                        SetNextIndexForMachine(pouringAndSolidification, clone);
                        pouringAndSolidification.Machines.Add(clone);
                        clone.ProcessStep = pouringAndSolidification;
                    }

                    Machine coolingMach = masterMachines.FirstOrDefault(m => m.Guid == coolingMachGuid);
                    if (coolingMach != null)
                    {
                        Machine clone = cloneManager.Clone(coolingMach);
                        SetNextIndexForMachine(cooling, clone);
                        cooling.Machines.Add(clone);
                        clone.ProcessStep = cooling;

                        Machine clone2 = cloneManager.Clone(coolingMach);
                        SetNextIndexForMachine(cooling2, clone2);
                        cooling2.Machines.Add(clone2);
                        clone2.ProcessStep = cooling2;
                    }

                    Machine cutOffEndsMach = masterMachines.FirstOrDefault(m => m.Guid == cutOffEndsMachGuid);
                    if (cutOffEndsMach != null)
                    {
                        Machine clone = cloneManager.Clone(cutOffEndsMach);
                        SetNextIndexForMachine(cutOffEnds, clone);
                        cutOffEnds.Machines.Add(clone);
                        clone.ProcessStep = cutOffEnds;
                    }

                    Machine blastMach = masterMachines.FirstOrDefault(m => m.Guid == blastMachGuid);
                    if (blastMach != null)
                    {
                        Machine clone = cloneManager.Clone(blastMach);
                        SetNextIndexForMachine(blast, clone);
                        blast.Machines.Add(clone);
                        clone.ProcessStep = blast;
                    }

                    Machine heatTreatmentMach = masterMachines.FirstOrDefault(m => m.Guid == heatTreatmentMachGuid);
                    if (heatTreatmentMach != null)
                    {
                        Machine clone = cloneManager.Clone(heatTreatmentMach);
                        SetNextIndexForMachine(heatTreatement, clone);
                        heatTreatement.Machines.Add(clone);
                        clone.ProcessStep = heatTreatement;
                    }

                    Machine magParticleMach = masterMachines.FirstOrDefault(m => m.Guid == magParticleMachGuid);
                    if (magParticleMach != null)
                    {
                        Machine clone = cloneManager.Clone(magParticleMach);
                        SetNextIndexForMachine(magneticParticleAnalysis, clone);
                        magneticParticleAnalysis.Machines.Add(clone);
                        clone.ProcessStep = magneticParticleAnalysis;
                    }
                }
                catch (DataAccessException ex)
                {
                    log.ErrorException("Error while cloning the machines.", ex);
                }
            }

            if (masterMachines.Count() != machinesGuids.Distinct().Count())
            {
                this.windowService.MessageDialogService.Show(LocalizedResources.Moulding_SomeMasterDataMachinesNotLoaded, MessageDialogType.Info);
            }

            return steps;
        }

        /// <summary>
        /// Creates the process steps for investment casting.
        /// </summary>
        /// <returns>A list of process steps.</returns>
        private List<PartProcessStep> CreateInvestmentCastingProcessSteps()
        {
            List<PartProcessStep> steps = new List<PartProcessStep>();

            PartProcessStep patternTreeAssembling = new PartProcessStep();
            patternTreeAssembling.Name = LocalizedResources.CastingModule_ProcessStep_PatternTreeAssembling;
            steps.Add(patternTreeAssembling);

            PartProcessStep moldCreation = new PartProcessStep();
            moldCreation.Name = LocalizedResources.CastingModule_ProcessStep_MoldCreation;
            steps.Add(moldCreation);

            PartProcessStep meltingOut = new PartProcessStep();
            meltingOut.Name = LocalizedResources.CastingModule_ProcessStep_MeltingOut;
            steps.Add(meltingOut);

            PartProcessStep burningMold = new PartProcessStep();
            burningMold.Name = LocalizedResources.CastingModule_ProcessStep_BurningMold;
            steps.Add(burningMold);

            PartProcessStep meltingAlloy = CreateMeltingStep();
            meltingAlloy.Name = LocalizedResources.CastingModule_ProcessStep_MeltingAlloy;
            steps.Add(meltingAlloy);

            PartProcessStep pouring = CreatePouringStep();
            steps.Add(pouring);

            PartProcessStep cooling = CreateCoolingStep();
            steps.Add(cooling);

            PartProcessStep castingRemoval = new PartProcessStep();
            castingRemoval.Name = LocalizedResources.CastingModule_ProcessStep_CastingRemoval;
            steps.Add(castingRemoval);

            PartProcessStep deburring = new PartProcessStep();
            deburring.Name = LocalizedResources.CastingModule_ProcessStep_Deburring;
            steps.Add(deburring);

            PartProcessStep abrasiveBlasting = new PartProcessStep();
            abrasiveBlasting.Name = LocalizedResources.CastingModule_ProcessStep_AbrasiveBlasting;
            steps.Add(abrasiveBlasting);

            PartProcessStep xray = new PartProcessStep();
            xray.Name = LocalizedResources.CastingModule_ProcessStep_XRay;
            steps.Add(xray);

            PartProcessStep checkAndPack = new PartProcessStep();
            checkAndPack.Name = LocalizedResources.CastingModule_ProcessStep_CheckAndPack;
            steps.Add(checkAndPack);

            return steps;
        }

        /// <summary>
        /// Selects the melting machine for Pressure Die Casting.
        /// </summary>
        /// <param name="clampingForce">The clamping force.</param>
        /// <returns>A machine guid.</returns>
        private Guid SelectMeltingMachineForPressureDieCasting(decimal clampingForce)
        {
            if (clampingForce <= 8000m)
            {
                return new Guid("fa63cd8c-25a7-413c-b54f-ce9fd67bd9fe");
            }
            else
            {
                return new Guid("b0b4f0ab-f6d4-4a31-862b-896d38cc6c27");
            }
        }

        /// <summary>
        /// Selects the pouring machines for Pressure Die Casting.
        /// </summary>
        /// <param name="clampingForce">The clamping force.</param>
        /// <returns>A list of machine guids.</returns>
        private List<Guid> SelectPouringMachinesForPressureDieCasting(decimal clampingForce)
        {
            List<Guid> machineIDs = new List<Guid>();

            // Dosing System
            if (clampingForce <= 3000m)
            {
                machineIDs.Add(new Guid("034E73A2-BF10-461E-B22C-D07C857870BC"));
            }
            else if (clampingForce <= 5000m)
            {
                machineIDs.Add(new Guid("F25D71AE-0BDA-4CC4-8F3E-94BE0710A573"));
            }
            else if (clampingForce <= 9000m)
            {
                machineIDs.Add(new Guid("E4C11C1B-798C-443E-9C38-17456C04F7CA"));
            }
            else if (clampingForce <= 14000m)
            {
                machineIDs.Add(new Guid("4907C4D8-534E-40A6-8FDA-40330D46CE77"));
            }
            else if (clampingForce <= 28000m)
            {
                machineIDs.Add(new Guid("E1E5DD8F-A5B7-4AD4-B9E6-4A80786CE116"));
            }
            else
            {
                machineIDs.Add(new Guid("0BC92DA1-3A35-490B-BF19-6D64112E3B18"));
            }

            // Die Casting Machine
            if (clampingForce <= 2000m)
            {
                machineIDs.Add(new Guid("789D1079-AA66-4754-9199-82D131AAB72A"));
            }
            else if (clampingForce <= 3000m)
            {
                machineIDs.Add(new Guid("5394ABD4-4859-4B52-908F-A18035F693CF"));
            }
            else if (clampingForce <= 4000m)
            {
                machineIDs.Add(new Guid("EEAEAE3C-58DD-4FEA-B6D3-CDFB5E8468F1"));
            }
            else if (clampingForce <= 5000m)
            {
                machineIDs.Add(new Guid("1878ACDB-AD81-49A1-8965-6910797F2D4A"));
            }
            else if (clampingForce <= 6000m)
            {
                machineIDs.Add(new Guid("A9C1DAFA-1AEA-4555-8277-F202BD4C592D"));
            }
            else if (clampingForce <= 8000m)
            {
                machineIDs.Add(new Guid("92B0B268-E616-4997-B4FB-C7D5246A2934"));
            }
            else if (clampingForce <= 9000m)
            {
                machineIDs.Add(new Guid("CD6DB657-725D-4B0D-8B93-736A087A3215"));
            }
            else if (clampingForce <= 10000m)
            {
                machineIDs.Add(new Guid("AEE81657-6A67-4D1C-85F0-286DB7421555"));
            }
            else if (clampingForce <= 14000m)
            {
                machineIDs.Add(new Guid("8CF96C87-8B8B-4317-9828-C1DD3301EF5A"));
            }
            else if (clampingForce <= 18000m)
            {
                machineIDs.Add(new Guid("1F4AE44E-C9DE-4BFA-901D-E219D7BD5D16"));
            }
            else if (clampingForce <= 22000m)
            {
                machineIDs.Add(new Guid("E77A625C-4219-4086-B254-D99DEE08D906"));
            }
            else if (clampingForce <= 28000m)
            {
                machineIDs.Add(new Guid("7FF657CA-B9B2-4710-9AEA-7326150505E5"));
            }
            else if (clampingForce <= 32000m)
            {
                machineIDs.Add(new Guid("2FEC4F19-560F-4701-AA59-82F3D97AB8E1"));
            }
            else
            {
                machineIDs.Add(new Guid("503B9CEF-F666-4FCF-B51F-1B8ADF2B2DC1"));
            }

            // Temperature Controller
            if (clampingForce <= 5000m)
            {
                machineIDs.Add(new Guid("6F2EFBA9-AAC2-45EA-BDD7-E25070BB6672"));
            }
            else if (clampingForce <= 28000m)
            {
                machineIDs.Add(new Guid("8AB8273A-0D34-4AB9-B7E6-F0E9BB1BD092"));
            }
            else
            {
                machineIDs.Add(new Guid("0DEB021D-B8AF-4A87-B209-433EA047247E"));
            }

            // Sprayer
            if (clampingForce <= 3000m)
            {
                machineIDs.Add(new Guid("E672B7DF-2CE0-40CE-8E17-DE923E0A630D"));
            }
            else if (clampingForce <= 5000m)
            {
                machineIDs.Add(new Guid("8B28ED62-9F6F-4FF3-B363-36F84BEEF99E"));
            }
            else if (clampingForce <= 6000m)
            {
                machineIDs.Add(new Guid("80A09BE2-6F8B-403D-9422-12B63CBBC209"));
            }
            else if (clampingForce <= 8000m)
            {
                machineIDs.Add(new Guid("A4CBD05D-68AB-4755-9502-CB0FD288BA81"));
            }
            else if (clampingForce <= 10000m)
            {
                machineIDs.Add(new Guid("95ED242F-5C87-4DFB-A8BE-00282473544C"));
            }
            else if (clampingForce <= 18000m)
            {
                machineIDs.Add(new Guid("82582380-AD2D-46F1-B8F3-F31FD155DB09"));
            }
            else if (clampingForce <= 28000m)
            {
                machineIDs.Add(new Guid("58CB193E-2F4D-44D6-BE79-DE72CE2BF40D"));
            }
            else
            {
                machineIDs.Add(new Guid("81C2FF56-9FC0-4ACC-A649-E77B965779E2"));
            }

            // Crop
            if (clampingForce <= 5000m)
            {
                machineIDs.Add(new Guid("AA3F965A-8A2C-4133-AE20-FB076BD9FFE9"));
            }
            else if (clampingForce <= 28000m)
            {
                machineIDs.Add(new Guid("8ADFE3CA-D77D-4226-A0F6-9044D35B63A8"));
            }
            else if (clampingForce <= 5000m)
            {
                machineIDs.Add(new Guid("91bef38c-feb0-462f-9843-5a3d430bed21"));
            }

            // Robot
            if (clampingForce <= 5000m)
            {
                machineIDs.Add(new Guid("B75D5209-9C56-4752-90A0-49FBAF0CC37A"));
            }
            else if (clampingForce <= 9000m)
            {
                machineIDs.Add(new Guid("FA151080-9CC0-4D25-B457-62D2188A3181"));
            }
            else if (clampingForce <= 18000m)
            {
                machineIDs.Add(new Guid("59119B9E-FA17-4D9A-99A5-8B582B8A08C5"));
            }
            else if (clampingForce <= 28000m)
            {
                machineIDs.Add(new Guid("5647C2F7-FC56-41CE-AB5F-1FB23364DDFC"));
            }
            else
            {
                machineIDs.Add(new Guid("CD82885C-6BDC-4298-81AF-4F97D982513C"));
            }

            return machineIDs;
        }

        /// <summary>
        /// Selects the deburring machine for Pressure Die Casting.
        /// </summary>
        /// <param name="clampingForce">The clamping force.</param>
        /// <returns>The machine guid.</returns>
        private Guid SelectDeburringMachineForPressureDieCasting(decimal clampingForce)
        {
            if (clampingForce <= 3000m)
            {
                return new Guid("59E49870-E878-4291-A611-97AAC8BF2668");
            }
            else if (clampingForce <= 5000m)
            {
                return new Guid("C699E870-B01B-4885-8699-BD62D9EA1E5E");
            }
            else if (clampingForce <= 8000m)
            {
                return new Guid("2BB92E6A-EF55-4A56-96F4-121377B4C215");
            }
            else if (clampingForce <= 10000m)
            {
                return new Guid("FEE5FA9F-9A9B-4C15-937B-E5481AD653D0");
            }
            else if (clampingForce <= 14000m)
            {
                return new Guid("5FDCEF42-4719-4B6B-AEB5-4252910EEEF5");
            }
            else if (clampingForce <= 18000m)
            {
                return new Guid("5F49DB37-2E48-4C12-A873-34800345D0DA");
            }
            else if (clampingForce <= 22000m)
            {
                return new Guid("5C982B58-896F-4934-81E3-A88E051F463A");
            }
            else if (clampingForce <= 28000m)
            {
                return new Guid("F8E7F8DF-9B56-4EF5-AE1E-4A9442D7AE72");
            }
            else
            {
                return new Guid("82F2300A-F7DA-4A58-B7BA-1FCE7415297A");
            }
        }

        /// <summary>
        /// Selects the abrasive blasting machine for Pressure Die Casting.
        /// </summary>
        /// <param name="clampingForce">The clamping force.</param>
        /// <returns>The machine guid.</returns>
        private Guid SelectAbrasiveBlastingMachineForPressureDieCasting(decimal clampingForce)
        {
            if (clampingForce <= 5000m)
            {
                return new Guid("0074FB06-D84D-4DB4-99F8-8E7B436FDDAB");
            }
            else if (clampingForce <= 14000m)
            {
                return new Guid("21D5B021-864A-41F6-B7F2-8A8761EE257E");
            }
            else if (clampingForce <= 28000m)
            {
                return new Guid("B8AA258D-FDC6-457E-875D-9AA9846FABC7");
            }
            else
            {
                return new Guid("D52695DD-BDD5-4D46-80BE-CB425EBE9011");
            }
        }

        /// <summary>
        /// Selects the machine for the Melting process step.
        /// </summary>
        /// <returns>The machine guid.</returns>
        private Guid SelectMeltingMachine()
        {
            if (IsFerrousCasting())
            {
                return new Guid("68b53515-435d-4524-b33e-1ed9bcda446f");
            }
            else
            {
                decimal partWeight = this.PartWeight.GetValueOrDefault();
                if (partWeight <= 25m)
                {
                    return new Guid("fa63cd8c-25a7-413c-b54f-ce9fd67bd9fe");
                }
                else
                {
                    return new Guid("b0b4f0ab-f6d4-4a31-862b-896d38cc6c27");
                }
            }
        }

        /// <summary>
        /// Selects the machines for the Mold Manufacturing process step.
        /// </summary>
        /// <returns>A list of machine guids.</returns>
        private List<Guid> SelectMoldManufacturingMachines()
        {
            decimal partWeight = this.PartWeight.GetValueOrDefault();
            List<Guid> guids = new List<Guid>();

            // Mold forming machine
            if (partWeight <= 15)
            {
                guids.Add(new Guid("87e9229c-43af-4e12-9f37-a9bf1a4f977c"));
                if (this.Process == CastingProcessType.Automatic)
                {
                    guids.Add(new Guid("5249A54C-2E2E-4597-A250-DD66D3376248"));
                }
            }
            else if (partWeight <= 40)
            {
                guids.Add(new Guid("87e9229c-43af-4e12-9f37-a9bf1a4f977c"));
                if (this.Process == CastingProcessType.Automatic)
                {
                    guids.Add(new Guid("41520904-814C-48B2-9852-98237780B3AC"));
                }
            }
            else if (partWeight <= 190)
            {
                guids.Add(new Guid("4abe1d3c-ab9e-48e5-958b-298b0065a0e2"));
                if (this.Process == CastingProcessType.Automatic)
                {
                    guids.Add(new Guid("41520904-814C-48B2-9852-98237780B3AC"));
                }
            }
            else
            {
                guids.Add(new Guid("4abe1d3c-ab9e-48e5-958b-298b0065a0e2"));
                guids.Add(new Guid("86105729-7D7C-4123-8D1A-8CE86EA6C503"));
            }

            // sand mixer
            if (partWeight <= 8)
            {
                guids.Add(new Guid("03070df0-a87a-4863-82dc-ee15f5daf96d"));
            }
            else if (partWeight <= 130)
            {
                guids.Add(new Guid("613ffd3e-0420-4daf-9088-ad643e3cb8f6"));
            }
            else
            {
                guids.Add(new Guid("4abe1d3c-ab9e-48e5-958b-298b0065a0e2"));
            }

            // sand cooler and recycler
            if (partWeight <= 15)
            {
                guids.Add(new Guid("74935ccb-b888-4bef-a5d7-1e7c76425a78"));
            }
            else if (partWeight <= 130)
            {
                guids.Add(new Guid("cb017021-f1c2-4533-9d43-ceeb24335625"));
            }
            else
            {
                guids.Add(new Guid("dd3e74cf-f98a-4dca-b19c-4f83f0dd55c6"));
            }

            return guids;
        }

        /// <summary>
        /// Selects the cycle time for the Mold Manufacturing process step.
        /// </summary>
        /// <returns>the cycle time</returns>
        private decimal SelectMoldManufacturingCycleTime()
        {
            decimal partWeight = this.PartWeight.GetValueOrDefault();

            if (partWeight <= 1)
            {
                return 15;
            }
            else if (partWeight <= 2)
            {
                return 17;
            }
            else if (partWeight <= 4)
            {
                return 19;
            }
            else if (partWeight <= 8)
            {
                return 21;
            }
            else if (partWeight <= 15)
            {
                return 23;
            }
            else if (partWeight <= 25)
            {
                return 25;
            }
            else if (partWeight <= 40)
            {
                return 26;
            }
            else if (partWeight <= 60)
            {
                return 28;
            }
            else if (partWeight <= 90)
            {
                return 30;
            }
            else if (partWeight <= 130)
            {
                return 32;
            }
            else if (partWeight <= 190)
            {
                return 35;
            }
            else if (partWeight <= 240)
            {
                return 40;
            }
            else if (partWeight <= 300)
            {
                return 45;
            }
            else
            {
                return 50;
            }
        }

        /// <summary>
        /// Selects the machines for the Core Manufacturing process step.
        /// </summary>
        /// <returns>A list of machine guids.</returns>
        private List<Guid> SelectCoreManufacturingMachines()
        {
            decimal partWeight = this.PartWeight.GetValueOrDefault();
            List<Guid> guids = new List<Guid>();

            if (partWeight <= 15)
            {
                guids.Add(new Guid("3aea325a-bca4-4267-81da-051f462d849a"));
                if (this.Process == CastingProcessType.Automatic)
                {
                    guids.Add(new Guid("5249A54C-2E2E-4597-A250-DD66D3376248"));
                }
            }
            else if (partWeight <= 40)
            {
                guids.Add(new Guid("3aea325a-bca4-4267-81da-051f462d849a"));
                if (this.Process == CastingProcessType.Automatic)
                {
                    guids.Add(new Guid("41520904-814C-48B2-9852-98237780B3AC"));
                }
            }
            else if (partWeight <= 190)
            {
                guids.Add(new Guid("8040a861-c317-437d-ac8a-f00ebe27db31"));
                if (this.Process == CastingProcessType.Automatic)
                {
                    guids.Add(new Guid("41520904-814C-48B2-9852-98237780B3AC"));
                }
            }
            else
            {
                guids.Add(new Guid("8040a861-c317-437d-ac8a-f00ebe27db31"));
                guids.Add(new Guid("86105729-7D7C-4123-8D1A-8CE86EA6C503"));
            }

            return guids;
        }

        /// <summary>
        /// Selects the cycle time for the Core Manufacturing process step.
        /// </summary>
        /// <returns>the cycle time</returns>
        private decimal SelectCoreManufacturingCycleTime()
        {
            decimal partWeight = this.PartWeight.GetValueOrDefault();

            if (partWeight <= 1)
            {
                return 15;
            }
            else if (partWeight <= 2)
            {
                return 17;
            }
            else if (partWeight <= 4)
            {
                return 19;
            }
            else if (partWeight <= 8)
            {
                return 21;
            }
            else if (partWeight <= 15)
            {
                return 23;
            }
            else if (partWeight <= 25)
            {
                return 25;
            }
            else if (partWeight <= 40)
            {
                return 26;
            }
            else if (partWeight <= 60)
            {
                return 28;
            }
            else if (partWeight <= 90)
            {
                return 30;
            }
            else if (partWeight <= 130)
            {
                return 32;
            }
            else if (partWeight <= 190)
            {
                return 35;
            }
            else if (partWeight <= 240)
            {
                return 40;
            }
            else if (partWeight <= 300)
            {
                return 45;
            }
            else
            {
                return 50;
            }
        }

        /// <summary>
        /// Selects the machine for the Core and Mold Assembly process step.
        /// </summary>
        /// <returns>the cycle time</returns>
        private Guid SelectCoreAndMoldAssemblyMachine()
        {
            decimal partWeight = this.PartWeight.GetValueOrDefault();

            if (this.Process == CastingProcessType.Automatic)
            {
                if (partWeight <= 15)
                {
                    return new Guid("fd202fd9-3799-4384-bc42-038707b6f602");
                }
                else if (partWeight <= 130)
                {
                    return new Guid("70f51f1b-3355-459e-8787-425cf50508be");
                }
                else
                {
                    return new Guid("426d63cf-8893-4f2c-890e-0500ecd4cb1a");
                }
            }
            else
            {
                return new Guid("10c21c07-1dde-464d-84f1-00f35dc3689f");
            }
        }

        /// <summary>
        /// Selects the cycle time for the Core and Mold Assembly process step of Sand Cast.
        /// </summary>
        /// <returns>the cycle time</returns>
        private decimal SelectCoreAndMoldAssemblyCycleTimeForSandCast()
        {
            if (this.Process == CastingProcessType.Automatic)
            {
                return SelectCoreManufacturingCycleTime();
            }
            else
            {
                decimal cycleTime = 0m;
                decimal partWeight = this.PartWeight.GetValueOrDefault();
                if (partWeight <= 8m)
                {
                    cycleTime = 300m;
                }
                else if (partWeight <= 90m)
                {
                    cycleTime = 600m;
                }
                else if (partWeight <= 240m)
                {
                    cycleTime = 900m;
                }
                else
                {
                    cycleTime = 1200m;
                }

                return cycleTime;
            }
        }

        /// <summary>
        /// Selects the pouring machines for Sand Cast.
        /// </summary>
        /// <returns>A list of machine guids.</returns>
        private List<Guid> SelectPouringMachinesForSandCast()
        {
            List<Guid> guids = new List<Guid>();
            decimal partWeight = this.PartWeight.GetValueOrDefault();
            if (partWeight <= 15)
            {
                guids.Add(new Guid("99C1FC8B-4653-493E-93E7-D203332A8CF5"));
                if (this.Process == CastingProcessType.Automatic)
                {
                    guids.Add(new Guid("5249A54C-2E2E-4597-A250-DD66D3376248"));
                }
            }
            else if (partWeight <= 130)
            {
                guids.Add(new Guid("A46B037A-1B36-4186-8AF3-3A652CB4CA9B"));
                if (this.Process == CastingProcessType.Automatic)
                {
                    guids.Add(new Guid("41520904-814C-48B2-9852-98237780B3AC"));
                }
            }
            else
            {
                guids.Add(new Guid("DAAD91A9-0537-429A-8ACC-45B4E5C3BB47"));
                guids.Add(new Guid("86105729-7D7C-4123-8D1A-8CE86EA6C503"));
            }

            return guids;
        }

        /// <summary>
        /// Selects the pouring machines for Gravity Die Cast.
        /// </summary>
        /// <returns>A list of machine guids.</returns>
        private List<Guid> SelectPouringMachinesForGravityDieCast()
        {
            decimal partWeight = this.PartWeight.GetValueOrDefault();
            List<Guid> guids = new List<Guid>();

            if (partWeight <= 15)
            {
                guids.Add(new Guid("F2EC59E8-76CD-419C-9EB4-B897A4147308"));
                if (this.Process == CastingProcessType.Automatic)
                {
                    guids.Add(new Guid("5249A54C-2E2E-4597-A250-DD66D3376248"));
                }
            }
            else if (partWeight <= 130)
            {
                guids.Add(new Guid("0BE495BE-000F-4F1E-9B7E-5D9787107ED4"));
                if (this.Process == CastingProcessType.Automatic)
                {
                    guids.Add(new Guid("41520904-814C-48B2-9852-98237780B3AC"));
                }
            }
            else
            {
                guids.Add(new Guid("D3A9F4F1-D841-4406-BFEE-0A07788609D8"));
                guids.Add(new Guid("86105729-7D7C-4123-8D1A-8CE86EA6C503"));
            }

            return guids;
        }

        /// <summary>
        /// Selects the pouring machine for Low Pressure Cast and Low Pressure Sand Cast.
        /// </summary>
        /// <returns>A list of machine guids.</returns>
        private List<Guid> SelectPouringMachinesForLowPressureCast()
        {
            List<Guid> guids = new List<Guid>();
            decimal partWeight = this.PartWeight.GetValueOrDefault();

            if (partWeight <= 15)
            {
                guids.Add(new Guid("6C7C93C6-E6D6-4132-85C7-E4F408434AB7"));
                if (this.Process == CastingProcessType.Automatic)
                {
                    guids.Add(new Guid("5249A54C-2E2E-4597-A250-DD66D3376248"));
                }
            }
            else if (partWeight <= 130)
            {
                guids.Add(new Guid("F32F0D21-A382-4AFE-9BDB-9264CF59B026"));
                if (this.Process == CastingProcessType.Automatic)
                {
                    guids.Add(new Guid("41520904-814C-48B2-9852-98237780B3AC"));
                }
            }
            else
            {
                guids.Add(new Guid("C013448C-4863-4729-BC55-0F1BBF91B725"));
                guids.Add(new Guid("86105729-7D7C-4123-8D1A-8CE86EA6C503"));
            }

            return guids;
        }

        /// <summary>
        /// Selects the machines for the Cooling process step.
        /// </summary>
        /// <returns>A list of machine guids.</returns>
        private List<Guid> SelectCoolingMachines()
        {
            decimal partWeight = this.PartWeight.GetValueOrDefault();
            List<Guid> guids = new List<Guid>();

            if (partWeight <= 15)
            {
                guids.Add(new Guid("72b0718e-700a-4280-b973-8a0fa01e474e"));
                if (this.Process == CastingProcessType.Automatic)
                {
                    guids.Add(new Guid("fd202fd9-3799-4384-bc42-038707b6f602"));
                }
            }
            else if (partWeight <= 130)
            {
                guids.Add(new Guid("6eaf3c5f-58ea-4e3e-a160-446ffd8dc8f6"));
                if (this.Process == CastingProcessType.Automatic)
                {
                    guids.Add(new Guid("70f51f1b-3355-459e-8787-425cf50508be"));
                }
            }
            else
            {
                guids.Add(new Guid("19cc9964-b88d-4ab8-b01e-caef79d7151c"));
                if (this.Process == CastingProcessType.Automatic)
                {
                    guids.Add(new Guid("426d63cf-8893-4f2c-890e-0500ecd4cb1a"));
                }
            }

            return guids;
        }

        /// <summary>
        /// Selects the Casting Removal machines.
        /// </summary>
        /// <returns>A list of machine guids.</returns>
        private List<Guid> SelectCastingRemovalMachines()
        {
            decimal partWeight = this.PartWeight.GetValueOrDefault();
            List<Guid> guids = new List<Guid>();

            if (partWeight <= 15)
            {
                guids.Add(new Guid("771e8274-b8ef-44db-8abd-015cd44fa9b1"));
                if (this.Process == CastingProcessType.Automatic)
                {
                    guids.Add(new Guid("fd202fd9-3799-4384-bc42-038707b6f602"));
                }
            }
            else if (partWeight <= 130)
            {
                guids.Add(new Guid("e70eea54-ad63-41db-94f3-0fccc3fd6f50"));
                if (this.Process == CastingProcessType.Automatic)
                {
                    guids.Add(new Guid("70f51f1b-3355-459e-8787-425cf50508be"));
                }
            }
            else
            {
                guids.Add(new Guid("ab209ae9-7bff-452f-a9e4-092065a66347"));
                if (this.Process == CastingProcessType.Automatic)
                {
                    guids.Add(new Guid("426d63cf-8893-4f2c-890e-0500ecd4cb1a"));
                }
            }

            return guids;
        }

        /// <summary>
        /// Selects the Deburring machines.
        /// </summary>
        /// <returns>A list of machine guids.</returns>
        private List<Guid> SelectDeburringMachines()
        {
            decimal partWeight = this.PartWeight.GetValueOrDefault();
            List<Guid> guids = new List<Guid>();

            if (partWeight <= 2)
            {
                guids.Add(new Guid("59E49870-E878-4291-A611-97AAC8BF2668"));
                if (this.Process == CastingProcessType.Automatic)
                {
                    guids.Add(new Guid("5249A54C-2E2E-4597-A250-DD66D3376248"));
                }
            }
            else if (partWeight <= 8)
            {
                guids.Add(new Guid("C699E870-B01B-4885-8699-BD62D9EA1E5E"));
                if (this.Process == CastingProcessType.Automatic)
                {
                    guids.Add(new Guid("5249A54C-2E2E-4597-A250-DD66D3376248"));
                }
            }
            else if (partWeight <= 15)
            {
                guids.Add(new Guid("2BB92E6A-EF55-4A56-96F4-121377B4C215"));
                if (this.Process == CastingProcessType.Automatic)
                {
                    guids.Add(new Guid("5249A54C-2E2E-4597-A250-DD66D3376248"));
                }
            }
            else if (partWeight <= 25)
            {
                guids.Add(new Guid("2BB92E6A-EF55-4A56-96F4-121377B4C215"));
                if (this.Process == CastingProcessType.Automatic)
                {
                    guids.Add(new Guid("41520904-814C-48B2-9852-98237780B3AC"));
                }
            }
            else if (partWeight <= 40)
            {
                guids.Add(new Guid("FEE5FA9F-9A9B-4C15-937B-E5481AD653D0"));
                if (this.Process == CastingProcessType.Automatic)
                {
                    guids.Add(new Guid("41520904-814C-48B2-9852-98237780B3AC"));
                }
            }
            else if (partWeight <= 90)
            {
                guids.Add(new Guid("5FDCEF42-4719-4B6B-AEB5-4252910EEEF5"));
                if (this.Process == CastingProcessType.Automatic)
                {
                    guids.Add(new Guid("41520904-814C-48B2-9852-98237780B3AC"));
                }
            }
            else if (partWeight <= 130)
            {
                guids.Add(new Guid("5F49DB37-2E48-4C12-A873-34800345D0DA"));
                if (this.Process == CastingProcessType.Automatic)
                {
                    guids.Add(new Guid("41520904-814C-48B2-9852-98237780B3AC"));
                }
            }
            else if (partWeight <= 190)
            {
                guids.Add(new Guid("5C982B58-896F-4934-81E3-A88E051F463A"));
                guids.Add(new Guid("86105729-7D7C-4123-8D1A-8CE86EA6C503"));
            }
            else if (partWeight <= 240)
            {
                guids.Add(new Guid("F8E7F8DF-9B56-4EF5-AE1E-4A9442D7AE72"));
                guids.Add(new Guid("86105729-7D7C-4123-8D1A-8CE86EA6C503"));
            }
            else
            {
                guids.Add(new Guid("82F2300A-F7DA-4A58-B7BA-1FCE7415297A"));
                guids.Add(new Guid("86105729-7D7C-4123-8D1A-8CE86EA6C503"));
            }

            return guids;
        }

        /// <summary>
        /// Selects the Deburring cycle time.
        /// </summary>
        /// <returns>the cycle time</returns>
        private decimal SelectDeburringCycleTime()
        {
            decimal partWeight = this.PartWeight.GetValueOrDefault();

            if (partWeight <= 15)
            {
                return 15;
            }
            else if (partWeight <= 40)
            {
                return 30;
            }
            else if (partWeight <= 90)
            {
                return 60;
            }
            else if (partWeight <= 190)
            {
                return 120;
            }
            else if (partWeight <= 240)
            {
                return 150;
            }
            else if (partWeight <= 300)
            {
                return 180;
            }
            else
            {
                return 210;
            }
        }

        /// <summary>
        /// Selects the Abrasive Blasting machines.
        /// </summary>
        /// <returns>A list of machine guids.</returns>
        private List<Guid> SelectAbrasiveBlastingMachines()
        {
            decimal partWeight = this.PartWeight.GetValueOrDefault();
            List<Guid> guids = new List<Guid>();

            if (partWeight <= 8)
            {
                guids.Add(new Guid("0074FB06-D84D-4DB4-99F8-8E7B436FDDAB"));
                if (this.Process == CastingProcessType.Automatic)
                {
                    guids.Add(new Guid("5249A54C-2E2E-4597-A250-DD66D3376248"));
                }
            }
            else if (partWeight <= 15)
            {
                guids.Add(new Guid("21D5B021-864A-41F6-B7F2-8A8761EE257E"));
                if (this.Process == CastingProcessType.Automatic)
                {
                    guids.Add(new Guid("5249A54C-2E2E-4597-A250-DD66D3376248"));
                }
            }
            else if (partWeight <= 90)
            {
                guids.Add(new Guid("21D5B021-864A-41F6-B7F2-8A8761EE257E"));
                if (this.Process == CastingProcessType.Automatic)
                {
                    guids.Add(new Guid("41520904-814C-48B2-9852-98237780B3AC"));
                }
            }
            else if (partWeight <= 130)
            {
                guids.Add(new Guid("B8AA258D-FDC6-457E-875D-9AA9846FABC7"));
                if (this.Process == CastingProcessType.Automatic)
                {
                    guids.Add(new Guid("41520904-814C-48B2-9852-98237780B3AC"));
                }
            }
            else if (partWeight <= 190)
            {
                guids.Add(new Guid("B8AA258D-FDC6-457E-875D-9AA9846FABC7"));
                guids.Add(new Guid("86105729-7D7C-4123-8D1A-8CE86EA6C503"));
            }
            else if (partWeight <= 300)
            {
                guids.Add(new Guid("D52695DD-BDD5-4D46-80BE-CB425EBE9011"));
                guids.Add(new Guid("86105729-7D7C-4123-8D1A-8CE86EA6C503"));
            }
            else
            {
                guids.Add(new Guid("D52695DD-BDD5-4D46-80BE-CB425EBE9011"));
                guids.Add(new Guid("32976350-10EA-4E3B-A489-19363CEFD6D0"));
            }

            return guids;
        }

        /// <summary>
        /// Selects the Abrasive Blasting cycle time.
        /// </summary>
        /// <returns>the cycle time</returns>
        private decimal SelectAbrasiveBlastingCycleTime()
        {
            decimal partWeight = this.PartWeight.GetValueOrDefault();

            if (partWeight <= 8)
            {
                return 180;
            }
            else if (partWeight <= 90)
            {
                return 220;
            }
            else
            {
                return 360;
            }
        }

        /// <summary>
        /// Selects the Abrasive Blasting parts per cycle value.
        /// </summary>
        /// <returns>The parts per cycle value.</returns>
        private int SelectAbrasiveBlastingPartsPerCycle()
        {
            decimal partWeight = this.PartWeight.GetValueOrDefault();

            if (partWeight <= 1)
            {
                return 10;
            }
            else if (partWeight <= 2)
            {
                return 8;
            }
            else if (partWeight <= 4)
            {
                return 7;
            }
            else if (partWeight <= 8)
            {
                return 6;
            }
            else if (partWeight <= 15)
            {
                return 5;
            }
            else if (partWeight <= 25)
            {
                return 4;
            }
            else if (partWeight <= 40)
            {
                return 3;
            }
            else if (partWeight <= 90)
            {
                return 2;
            }
            else
            {
                return 1;
            }
        }

        /// <summary>
        /// Selects the X-Ray machines.
        /// </summary>
        /// <returns>The machines guids</returns>
        private List<Guid> SelectXRayMachines()
        {
            decimal partWeight = this.PartWeight.GetValueOrDefault();
            List<Guid> guids = new List<Guid>();

            if (partWeight <= 15)
            {
                guids.Add(new Guid("5b89de1b-8276-4010-b760-c752c91f7b95"));
                if (this.Process == CastingProcessType.Automatic)
                {
                    guids.Add(new Guid("5249A54C-2E2E-4597-A250-DD66D3376248"));
                }
            }
            else if (partWeight <= 130)
            {
                guids.Add(new Guid("5b89de1b-8276-4010-b760-c752c91f7b95"));
                if (this.Process == CastingProcessType.Automatic)
                {
                    guids.Add(new Guid("41520904-814C-48B2-9852-98237780B3AC"));
                }
            }
            else
            {
                guids.Add(new Guid("5b89de1b-8276-4010-b760-c752c91f7b95"));
                guids.Add(new Guid("86105729-7D7C-4123-8D1A-8CE86EA6C503"));
            }

            return guids;
        }

        /// <summary>
        /// Selects the X-Ray cycle time.
        /// </summary>
        /// <returns>the cycle time</returns>
        private decimal SelectXRayCycleTime()
        {
            decimal partWeight = this.PartWeight.GetValueOrDefault();

            if (partWeight <= 1)
            {
                return 15;
            }
            else if (partWeight <= 2)
            {
                return 17;
            }
            else if (partWeight <= 4)
            {
                return 19;
            }
            else if (partWeight <= 8)
            {
                return 21;
            }
            else if (partWeight <= 15)
            {
                return 23;
            }
            else if (partWeight <= 25)
            {
                return 25;
            }
            else if (partWeight <= 40)
            {
                return 26;
            }
            else if (partWeight <= 60)
            {
                return 28;
            }
            else if (partWeight <= 90)
            {
                return 30;
            }
            else if (partWeight <= 130)
            {
                return 32;
            }
            else if (partWeight <= 190)
            {
                return 35;
            }
            else if (partWeight <= 240)
            {
                return 40;
            }
            else if (partWeight <= 300)
            {
                return 45;
            }
            else
            {
                return 50;
            }
        }

        /// <summary>
        /// Selects the Check and Pack cycle time.
        /// </summary>
        /// <returns>the cycle time</returns>
        private decimal SelectCheckAndPackCycleTime()
        {
            decimal partWeight = this.PartWeight.GetValueOrDefault();

            if (partWeight <= 1)
            {
                return 15;
            }
            else if (partWeight <= 2)
            {
                return 17;
            }
            else if (partWeight <= 4)
            {
                return 19;
            }
            else if (partWeight <= 8)
            {
                return 21;
            }
            else if (partWeight <= 15)
            {
                return 23;
            }
            else if (partWeight <= 25)
            {
                return 25;
            }
            else if (partWeight <= 40)
            {
                return 26;
            }
            else if (partWeight <= 60)
            {
                return 28;
            }
            else if (partWeight <= 90)
            {
                return 30;
            }
            else if (partWeight <= 130)
            {
                return 32;
            }
            else if (partWeight <= 190)
            {
                return 35;
            }
            else if (partWeight <= 240)
            {
                return 40;
            }
            else if (partWeight <= 300)
            {
                return 45;
            }
            else
            {
                return 50;
            }
        }

        /// <summary>
        /// Adds the calculated sand quantity (SandWeight) to a step.
        /// </summary>
        /// <param name="step">The step to add the sand to.</param>
        private void AddSandToStep(PartProcessStep step)
        {
            decimal sandWeight = this.InputtedSandWeight.HasValue ? this.InputtedSandWeight.Value : this.ProposedSandWeight.GetValueOrDefault();
            if (sandWeight > 0)
            {
                Consumable sand = new Consumable();
                sand.Name = LocalizedResources.CastingModule_SandAndBinder;
                sand.Price = 0.025m;
                sand.Amount = sandWeight;
                step.Consumables.Add(sand);

                sand.PriceUnitBase = this.measurementUnits.FirstOrDefault(u => u.Symbol.ToLower() == "kg");
                sand.AmountUnitBase = sand.PriceUnitBase;
            }
        }

        /// <summary>
        /// Creates the melting process step.
        /// </summary>
        /// <returns>A process step.</returns>
        private PartProcessStep CreateMeltingStep()
        {
            PartProcessStep melting = new PartProcessStep();
            melting.Name = LocalizedResources.CastingModule_ProcessStep_Melting;
            melting.CycleTime = this.InputtedMeltingTime.HasValue ? this.InputtedMeltingTime.Value : this.ProposedMeltingTime.GetValueOrDefault();
            melting.ProcessTime = melting.CycleTime;
            melting.CycleTimeUnit = this.measurementUnits.FirstOrDefault(u => u.Symbol.ToLower() == "s");
            melting.ProcessTimeUnit = melting.CycleTimeUnit;

            return melting;
        }

        /// <summary>
        /// Creates the pouring process step.
        /// </summary>
        /// <returns>A process step.</returns>
        private PartProcessStep CreatePouringStep()
        {
            PartProcessStep pouring = new PartProcessStep();
            pouring.Name = LocalizedResources.CastingModule_ProcessStep_Pouring;

            decimal pouringTime =
                this.InputtedPouringTime.HasValue ? this.InputtedPouringTime.Value : this.ProposedPouringTime.GetValueOrDefault();
            decimal solidificationTime =
                this.InputtedSolidificationTime.HasValue ? this.InputtedSolidificationTime.Value : this.ProposedSolidificationTime.GetValueOrDefault();
            pouring.CycleTime = pouringTime + solidificationTime;
            pouring.ProcessTime = pouring.CycleTime;
            pouring.ScrapAmount = this.InputtedScrap.HasValue ? this.InputtedScrap.Value : this.ProposedScrap.GetValueOrDefault();
            pouring.CycleTimeUnit = this.measurementUnits.FirstOrDefault(u => u.Symbol.ToLower() == "s");
            pouring.ProcessTimeUnit = pouring.CycleTimeUnit;
            pouring.SetupTime = 600m;
            pouring.MaxDownTime = 600m;
            pouring.ProductionSkilledLabour = 1m;
            pouring.SetupSkilledLabour = 1m;

            return pouring;
        }

        /// <summary>
        /// Creates the Cooling process step.
        /// </summary>
        /// <returns>A process step.</returns>
        private PartProcessStep CreateCoolingStep()
        {
            PartProcessStep cooling = new PartProcessStep();
            cooling.Name = LocalizedResources.CastingModule_ProcessStep_Cooling;
            cooling.CycleTime = this.InputtedCoolingTime.HasValue ? this.InputtedCoolingTime.Value : this.ProposedCoolingTime.GetValueOrDefault();
            cooling.ProcessTime = cooling.CycleTime;
            cooling.CycleTimeUnit = this.measurementUnits.FirstOrDefault(u => u.Symbol.ToLower() == "s");
            cooling.ProcessTimeUnit = cooling.CycleTimeUnit;
            cooling.SetupTime = 0m;
            cooling.MaxDownTime = 0m;
            cooling.ProductionSkilledLabour = 0m;
            cooling.SetupSkilledLabour = 0m;

            return cooling;
        }

        /// <summary>
        /// Gets the machines with the GUID in the specified list.
        /// </summary>
        /// <param name="machineGuids">The machine guids.</param>
        /// <returns>The machines.</returns>
        private Collection<Machine> GetMachines(List<Guid> machineGuids)
        {
            if (machineGuids == null)
            {
                return new Collection<Machine>();
            }

            Collection<Machine> masterMachines = new Collection<Machine>();

            // Check if central server is online and try to load the machines from there. If loading from there fails (the central server online state
            // is refreshed periodically and for short durations it may not reflect the actual state), load from the local database.
            bool loadFromLocal = true;
            if (this.onlineChecker.IsOnline)
            {
                loadFromLocal = false;
                try
                {
                    machinesSourceChangeSet = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
                    masterMachines = machinesSourceChangeSet.MachineRepository.GetMasterDataMachines(machineGuids);
                }
                catch (Exception ex)
                {
                    log.DebugException("Casting module: no central database connection or load fail when getting machines.", ex);
                    loadFromLocal = true;
                }
            }

            // Load the machines from the local database (the central database is offline or loading from it has failed).
            if (loadFromLocal)
            {
                try
                {
                    machinesSourceChangeSet = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
                    masterMachines = machinesSourceChangeSet.MachineRepository.GetMasterDataMachines(machineGuids);
                }
                catch (DataAccessException exc)
                {
                    log.ErrorException("Casting module: no local database connection when getting machines.", exc);
                    this.windowService.MessageDialogService.Show(LocalizedResources.Moulding_MasterDataMachinesNotLoaded, MessageDialogType.Error);
                }
            }

            return masterMachines;
        }

        /// <summary>
        /// Determines whether the casting process is ferrous or non ferrous.
        /// </summary>
        /// <returns>true if is ferrous and false if is non ferrous.</returns>
        private bool IsFerrousCasting()
        {
            if (this.CastingMethod == ViewModels.CastingMethod.CentrifugalCastingFerrous ||
                this.CastingMethod == ViewModels.CastingMethod.GravityDieCastingFerrous ||
                this.CastingMethod == ViewModels.CastingMethod.InvestmentCastingFerrous ||
                this.CastingMethod == ViewModels.CastingMethod.SandCastingFerrous)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Creates the Core and Mold Assembly step for Sand Casting.
        /// </summary>
        /// <returns>The process step.</returns>
        private PartProcessStep CreateCoreAndMoldAssemblyStepForSandCasting()
        {
            PartProcessStep coreAndMoldAssembly = new PartProcessStep();
            coreAndMoldAssembly.Name = LocalizedResources.CastingModule_ProcessStep_CoreAndMoldAssembly;

            coreAndMoldAssembly.CycleTime = SelectCoreAndMoldAssemblyCycleTimeForSandCast();
            coreAndMoldAssembly.ProcessTime = coreAndMoldAssembly.CycleTime;

            coreAndMoldAssembly.SetupSkilledLabour = 1m;
            if (this.Process == CastingProcessType.Manual)
            {
                decimal partWeight = this.PartWeight.GetValueOrDefault();
                if (partWeight <= 40m)
                {
                    coreAndMoldAssembly.ProductionSkilledLabour = 1m;
                }
                else
                {
                    coreAndMoldAssembly.ProductionSkilledLabour = 2m;
                }
            }
            else
            {
                coreAndMoldAssembly.ProductionSkilledLabour = 0.5m;
            }

            return coreAndMoldAssembly;
        }

        /// <summary>
        /// Creates the Mold Manufacturing step.
        /// </summary>
        /// <returns>The process step.</returns>
        private PartProcessStep CreateMoldManufacturingStep()
        {
            PartProcessStep moldManufacturing = new PartProcessStep();
            moldManufacturing.Name = LocalizedResources.CastingModule_ProcessStep_MoldManufacturing;
            moldManufacturing.CycleTime = SelectMoldManufacturingCycleTime();
            moldManufacturing.ProcessTime = moldManufacturing.CycleTime;
            moldManufacturing.SetupTime = 1800m;
            moldManufacturing.MaxDownTime = 1800m;
            moldManufacturing.ProductionSkilledLabour = 2m;
            moldManufacturing.SetupSkilledLabour = 2m;

            AddSandToStep(moldManufacturing);

            return moldManufacturing;
        }

        /// <summary>
        /// Creates the Core Manufacturing step.
        /// </summary>
        /// <returns>The process step.</returns>
        private PartProcessStep CreateCoreManufacturingStep()
        {
            PartProcessStep coreManufacturing = new PartProcessStep();
            coreManufacturing.Name = LocalizedResources.CastingModule_ProcessStep_CoreManufacturing;
            coreManufacturing.CycleTime = SelectCoreManufacturingCycleTime();
            coreManufacturing.ProcessTime = coreManufacturing.CycleTime;
            coreManufacturing.SetupTime = 1800m;
            coreManufacturing.MaxDownTime = 1800m;
            coreManufacturing.ProductionSkilledLabour = 0.5m;
            coreManufacturing.SetupSkilledLabour = 1m;

            return coreManufacturing;
        }

        /// <summary>
        /// Creates the casting removal step.
        /// </summary>
        /// <returns>The process step.</returns>
        private PartProcessStep CreateCastingRemovalStep()
        {
            PartProcessStep castingRemoval = new PartProcessStep();
            castingRemoval.Name = LocalizedResources.CastingModule_ProcessStep_CastingRemoval;
            castingRemoval.SetupTime = 1800m;
            castingRemoval.MaxDownTime = 1800m;
            castingRemoval.ProductionSkilledLabour = 1m;
            castingRemoval.SetupSkilledLabour = 1m;
            castingRemoval.CycleTime = SelectCoreAndMoldAssemblyCycleTimeForSandCast();
            castingRemoval.ProcessTime = castingRemoval.CycleTime;

            return castingRemoval;
        }

        /// <summary>
        /// Creates the Deburring step.
        /// </summary>
        /// <returns>The process step.</returns>
        private PartProcessStep CreateDeburringStep()
        {
            PartProcessStep deburring = new PartProcessStep();
            deburring.Name = LocalizedResources.CastingModule_ProcessStep_Deburring;
            deburring.CycleTime = SelectDeburringCycleTime();
            deburring.ProcessTime = deburring.CycleTime;
            deburring.SetupTime = 0m;
            deburring.MaxDownTime = 0m;
            deburring.ProductionSkilledLabour = 0.5m;
            deburring.SetupSkilledLabour = 1m;

            return deburring;
        }

        /// <summary>
        /// Creates the Abrasive Blasting step.
        /// </summary>
        /// <returns>The process step.</returns>
        private PartProcessStep CreateAbrasiveBlastingStep()
        {
            PartProcessStep abrasiveBlasting = new PartProcessStep();
            abrasiveBlasting.Name = LocalizedResources.CastingModule_ProcessStep_AbrasiveBlasting;
            abrasiveBlasting.CycleTime = SelectAbrasiveBlastingCycleTime();
            abrasiveBlasting.ProcessTime = abrasiveBlasting.CycleTime;
            abrasiveBlasting.PartsPerCycle = SelectAbrasiveBlastingPartsPerCycle();
            abrasiveBlasting.SetupTime = 600m;
            abrasiveBlasting.MaxDownTime = 600m;
            abrasiveBlasting.ProductionSkilledLabour = 0.5m;
            abrasiveBlasting.SetupSkilledLabour = 1m;

            return abrasiveBlasting;
        }

        /// <summary>
        /// Creates the X-Ray step.
        /// </summary>
        /// <returns>The process step.</returns>
        private PartProcessStep CreateXRayStep()
        {
            PartProcessStep xray = new PartProcessStep();
            xray.Name = LocalizedResources.CastingModule_ProcessStep_XRay;
            xray.PartsPerCycle = 100;
            xray.CycleTime = SelectXRayCycleTime();
            xray.ProcessTime = xray.CycleTime;
            xray.SetupTime = 600m;
            xray.MaxDownTime = 600m;
            xray.ProductionSkilledLabour = 1m;
            xray.SetupSkilledLabour = 1m;

            return xray;
        }

        /// <summary>
        /// Creates the Check and Pack step.
        /// </summary>
        /// <returns>The process step.</returns>
        private PartProcessStep CreateCheckAndPackStep()
        {
            PartProcessStep checkAndPack = new PartProcessStep();
            checkAndPack.Name = LocalizedResources.CastingModule_ProcessStep_CheckAndPack;
            checkAndPack.CycleTime = SelectCheckAndPackCycleTime();
            checkAndPack.ProcessTime = checkAndPack.CycleTime;
            checkAndPack.SetupTime = 0m;
            checkAndPack.MaxDownTime = 0m;
            checkAndPack.ProductionSkilledLabour = 1m;
            checkAndPack.SetupSkilledLabour = 1m;

            return checkAndPack;
        }

        #endregion Create the casting process
    }
}