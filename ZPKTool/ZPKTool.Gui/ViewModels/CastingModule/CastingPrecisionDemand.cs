﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The precision demands for casting.
    /// </summary>
    public enum CastingPrecisionDemand
    {
        /// <summary>
        /// Normal precision
        /// </summary>
        Normal = 0,

        /// <summary>
        /// Tight Tolerances Type
        /// </summary>
        TightTolerances = 1
    }
}
