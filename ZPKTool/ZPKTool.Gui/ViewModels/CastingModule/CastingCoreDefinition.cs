﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.Views
{
    /// <summary>
    /// The definition of a casting core.
    /// </summary>
    public class CastingCoreDefinition : INotifyPropertyChanged
    {
        /// <summary>
        /// The weight of the core.
        /// </summary>
        private decimal? weight;

        /// <summary>
        /// The volume of the core.
        /// </summary>
        private decimal? volume;

        /// <summary>
        /// Initializes a new instance of the <see cref="CastingCoreDefinition"/> class.
        /// </summary>
        public CastingCoreDefinition()
        {
        }

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the volume of the core.
        /// </summary>
        public decimal? Volume
        {
            get
            {
                return this.volume;
            }

            set
            {
                if (this.volume != value)
                {
                    this.volume = value;
                    RaisePropertyChanged("Volume");
                    CalculateCoreWeight();
                }
            }
        }

        /// <summary>
        /// Gets or sets the weight of the core.
        /// </summary>
        public decimal? Weight
        {
            get
            {
                return this.weight;
            }

            set
            {
                if (this.weight != value)
                {
                    this.weight = value;
                    RaisePropertyChanged("Weight");
                }
            }
        }

        /// <summary>
        /// Raises the PropertyChanged event.
        /// </summary>
        /// <param name="propertyName">Name of the property that changed.</param>
        private void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// Calculates the weight of the core.
        /// </summary>
        private void CalculateCoreWeight()
        {
            this.Weight = this.Volume * 0.0014m;
        }
    }
}
