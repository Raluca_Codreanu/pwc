﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The casting methods.
    /// </summary>
    public enum CastingMethod
    {
        /// <summary>
        /// Sand casting, ferrous
        /// </summary>
        SandCastingFerrous = 0,

        /// <summary>
        /// Sand casting, non ferrous
        /// </summary>
        SandCastingNonFerrous = 1,

        /// <summary>
        /// Investment casting, ferrous
        /// </summary>
        InvestmentCastingFerrous = 2,

        /// <summary>
        /// Investment casting, non ferrous
        /// </summary>
        InvestmentCastingNonFerrous = 3,

        /// <summary>
        /// Gravity die casting, ferrous
        /// </summary>
        GravityDieCastingFerrous = 4,

        /// <summary>
        /// Gravity die casting, non ferrous
        /// </summary>
        GravityDieCastingNonFerrous = 5,

        /// <summary>
        /// Centrifugal casting
        /// </summary>
        CentrifugalCastingFerrous = 6,

        /// <summary>
        /// Low pressure sand cast
        /// </summary>
        LowPressureSandCastNonFerrous = 7,

        /// <summary>
        /// Low pressure cast
        /// </summary>
        LowPressureCastNonFerrous = 8,

        /// <summary>
        /// Pressure die cast
        /// </summary>
        PressureDieCastNonFerrous = 9
    }
}
