﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.Gui.Resources;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for manage viewer currencies.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ManageViewerCurrenciesViewModel : ViewModel
    {
        #region Fields

        /// <summary>
        /// The composition container.
        /// </summary>
        private CompositionContainer container;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The currency view model.
        /// </summary>
        private CurrencyViewModel currencyViewModel;

        /// <summary>
        /// The currencies exchange rate before edit.
        /// </summary>
        private Dictionary<Guid, decimal> currenciesExchangeRateBeforeEdit;

        /// <summary>
        /// The selected currency.
        /// </summary>
        private Currency selectedCurrency;

        #endregion Fields

        /// <summary>
        /// Initializes a new instance of the <see cref="ManageViewerCurrenciesViewModel" /> class.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="windowService">The window service.</param>
        [ImportingConstructor]
        public ManageViewerCurrenciesViewModel(
            CompositionContainer container,
            IWindowService windowService)
        {
            Argument.IsNotNull("container", container);
            Argument.IsNotNull("windowService", windowService);

            this.container = container;
            this.windowService = windowService;

            this.CurrencyViewModel = this.container.GetExportedValue<CurrencyViewModel>();
            this.CurrencyViewModel.IsInViewerMode = true;
            this.CurrencyViewModel.ShowSaveControls = false;
        }

        #region Properties

        /// <summary>
        /// Gets the currencies.
        /// </summary>
        public ICollection<Currency> Currencies { get; private set; }

        /// <summary>
        /// Gets the CurrencyViewModel
        /// </summary>
        public CurrencyViewModel CurrencyViewModel
        {
            get { return this.currencyViewModel; }
            private set { this.SetProperty(ref this.currencyViewModel, value, () => this.CurrencyViewModel); }
        }

        /// <summary>
        /// Gets or sets the selected currency.
        /// </summary>
        public Currency SelectedCurrency
        {
            get { return this.selectedCurrency; }
            set { this.SetProperty(ref this.selectedCurrency, value, () => this.SelectedCurrency, this.OnSelectedCurrencyChanged); }
        }

        #endregion Properties

        /// <summary>
        /// Initializes the currencies.
        /// </summary>
        /// <param name="currencies">The currencies.</param>
        public void InitializeCurrencies(IEnumerable<Currency> currencies)
        {
            this.Currencies = new List<Currency>(currencies);
            this.SelectedCurrency = this.Currencies.FirstOrDefault();

            // Save the exchange rate of the currencies before editing.
            this.currenciesExchangeRateBeforeEdit = new Dictionary<Guid, decimal>();
            foreach (var currency in currencies)
            {
                this.currenciesExchangeRateBeforeEdit.Add(currency.Guid, currency.ExchangeRate);
            }
        }

        /// <summary>
        /// Determines whether this instance can perform the save operation. Executed by the SaveCommand.
        /// </summary>
        /// <returns>
        /// true if the save operation can be performed, false otherwise.
        /// </returns>
        protected override bool CanSave()
        {
            return base.CanSave() && this.CurrencyViewModel.SaveCommand.CanExecute(null);
        }

        /// <summary>
        /// Performs the save operation. Executed by the SaveCommand.
        /// </summary>
        protected override void Save()
        {
            base.Save();
            this.CurrencyViewModel.SaveToModelCommand.Execute(null);
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Performs the cancel operation. Executed by the CancelCommand.
        /// </summary>
        protected override void Cancel()
        {
            bool isChanged = false;

            // Determine if any exchange rate has changed.
            foreach (var currency in this.Currencies)
            {
                if (this.currenciesExchangeRateBeforeEdit[currency.Guid] != currency.ExchangeRate)
                {
                    isChanged = true;
                    continue;
                }
            }

            if (isChanged)
            {
                // If there are changes made, ask the user to confirm cancelling.
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_CancelChanges, MessageDialogType.YesNo);
                if (result == MessageDialogResult.No)
                {
                    // Don't cancel the changes and also don't close the view-model.
                    return;
                }
            }

            // Set the original exchange rates.
            base.Cancel();
            foreach (var currency in this.Currencies)
            {
                currency.ExchangeRate = this.currenciesExchangeRateBeforeEdit[currency.Guid];
            }

            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Sets the <see cref="CurrencyViewModel"/> property when the selected currency is changed
        /// </summary>
        private void OnSelectedCurrencyChanged()
        {
            if (this.CurrencyViewModel.Model != null)
            {
                this.CurrencyViewModel.SaveToModelCommand.Execute(null);
            }

            if (this.selectedCurrency != null)
            {
                this.CurrencyViewModel.Model = this.selectedCurrency;

                // Check if the selected currency is the default application currency
                // The default currency can not be modified
                if (this.selectedCurrency.IsoCode.Equals(Constants.DefaultCurrencyIsoCode, StringComparison.InvariantCulture))
                {
                    this.CurrencyViewModel.IsReadOnly = true;
                }
                else
                {
                    this.CurrencyViewModel.IsReadOnly = false;
                }
            }
        }
    }
}