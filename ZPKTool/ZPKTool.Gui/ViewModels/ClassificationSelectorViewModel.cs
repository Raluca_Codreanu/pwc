﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view model for the classification selection view.
    /// </summary>
    /// <remarks>
    /// In order to initialize the view model the following properties must be set:
    /// - DataContext
    /// - ClassificationType
    /// - ClassificationLevels can be set if you have initial levels, but it is optional.
    /// </remarks>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ClassificationSelectorViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The type of the classification tree to display.
        /// </summary>
        private Type classificationType;

        /// <summary>
        /// The data context used for data related operations.
        /// </summary>
        private IDataSourceManager dataAccessContext;

        /// <summary>
        /// The classification levels displayed by the view.
        /// </summary>
        private ObservableCollection<object> classificationLevels;

        /// <summary>
        /// The display value for the 1st classification level.
        /// </summary>
        private string classificationLevel1;

        /// <summary>
        /// The display value for the 2nd classification level.
        /// </summary>
        private string classificationLevel2;

        /// <summary>
        /// The display value for the 3rd classification level.
        /// </summary>
        private string classificationLevel3;

        /// <summary>
        /// The display value for the 4th classification level.
        /// </summary>
        private string classificationLevel4;

        /// <summary>
        /// The data source for the classification tree.
        /// </summary>
        private IList<TreeViewDataItem> classificationTreeSource;

        /// <summary>
        /// The tree item currently selected in the classification tree.
        /// </summary>
        private TreeViewDataItem selectedClassificationTreeItem;

        /// <summary>
        /// A value indicating whether the classification tree popup is open
        /// </summary>
        private bool isClassificationTreePopupOpen;

        /// <summary>
        /// A value indicating whether to show only the first two levels.
        /// </summary>
        private bool showOnlyTwoLevels;

        /// <summary>
        /// A value indicating OpenClassificationButton string content.
        /// </summary>
        private string openClassificationButtonContent;

        /// <summary>
        /// Indicates whether the classification tree content is loaded.
        /// </summary>
        private bool isClassificationTreeLoaded;

        /// <summary>
        /// The listener for PropertyChanged weak event.
        /// </summary>
        private WeakEventListener<PropertyChangedEventArgs> zoomLevelPropertyChangedListener;

        #endregion Attributes

        /// <summary>
        /// Initializes a new instance of the <see cref="ClassificationSelectorViewModel"/> class.
        /// </summary>
        [ImportingConstructor]
        public ClassificationSelectorViewModel()
        {
            this.OpenClassificationTreeCommand = new DelegateCommand(this.OpenClassificationTree, () => !this.IsReadOnly);
            this.ClearClassificationsCommand = new DelegateCommand(this.ClearClassifications, () => !this.IsReadOnly);
            this.CloseClassificationTreeCommand = new DelegateCommand(() => this.IsClassificationTreePopupOpen = false);
            this.SelectedClassificationTreeItemChangedCommand = new DelegateCommand<RoutedPropertyChangedEventArgs<object>>(this.ClassificationTreeSelectedItemChanged);
            this.SelectedClassificationTreeDoubleClickCommand = new DelegateCommand<MouseButtonEventArgs>(this.ClassificationTreeDoubleClick);
            this.SelectClassificationCommand = new DelegateCommand(this.SelectClassification);

            this.ClassificationLevel1 = LocalizedResources.General_NotAvailable;
            this.ClassificationLevel2 = LocalizedResources.General_NotAvailable;
            this.ClassificationLevel3 = LocalizedResources.General_NotAvailable;
            this.ClassificationLevel4 = LocalizedResources.General_NotAvailable;

            // Close the tree popup when the zoom changes because its placement target's position will change but its position won't.
            this.zoomLevelPropertyChangedListener = new WeakEventListener<PropertyChangedEventArgs>((s, e) => this.IsClassificationTreePopupOpen = false);
            PropertyChangedEventManager.AddListener(
                UserSettingsManager.Instance,
                zoomLevelPropertyChangedListener,
                ReflectionUtils.GetPropertyName(() => UserSettingsManager.Instance.ZoomLevel));

            this.PropertyChanged += OnPropertyChanged;
        }

        #region Commands

        /// <summary>
        /// Gets the command that opens the classification tree popup.
        /// </summary>
        public ICommand OpenClassificationTreeCommand { get; private set; }

        /// <summary>
        /// Gets the command that clears the current classifications.
        /// </summary>
        public ICommand ClearClassificationsCommand { get; private set; }

        /// <summary>
        /// Gets the command that closes the classification tree.
        /// </summary>
        public ICommand CloseClassificationTreeCommand { get; private set; }

        /// <summary>
        /// Gets the command executed when the selected item of the classification tree has changed.
        /// </summary>
        public ICommand SelectedClassificationTreeItemChangedCommand { get; private set; }

        /// <summary>
        /// Gets the command executed when the user has double-clicked the classification tree.
        /// </summary>
        public ICommand SelectedClassificationTreeDoubleClickCommand { get; private set; }

        /// <summary>
        /// Gets the command executed when the user saves the selection of the classification tree.
        /// </summary>
        public ICommand SelectClassificationCommand { get; private set; }

        #endregion Commands

        #region Data Properties

        /// <summary>
        /// Gets or sets the type of the classification tree to display.
        /// <para />
        /// The currently supported types are MachinesClassification, MaterialsClassification and ProcessStepsClassification.
        /// </summary>
        public Type ClassificationType
        {
            get { return this.classificationType; }
            set { this.SetProperty(ref this.classificationType, value, () => this.ClassificationType, this.OnClassificationTypeChanged); }
        }

        /// <summary>
        /// Gets or sets the data context used for data related operations (like populate the classification tree).
        /// </summary>        
        public IDataSourceManager DataAccessContext
        {
            get
            {
                return this.dataAccessContext;
            }

            set
            {
                this.SetProperty(
                    ref this.dataAccessContext,
                    value,
                    () => this.DataAccessContext,
                    () =>
                    {
                        this.UnloadClassificationTree();
                        this.IsClassificationTreePopupOpen = false;
                    });
            }
        }

        /// <summary>
        /// Gets or sets the classification levels displayed by the view. Thy are displayed in the order from the collection.
        /// <para />
        /// If the object(s) in the collection are not INameble their display name will be the result of the ToString method.
        /// <para/>
        /// Manipulating individual elements of the collection (add, remove, etc.) has no effect on the view model.
        /// Usually you set the collection reference with or without content and then iterate it when you need the selected classifications.
        /// </summary>
        [UndoableProperty]
        public ObservableCollection<object> ClassificationLevels
        {
            get { return this.classificationLevels; }
            set { this.SetProperty(ref this.classificationLevels, value, () => this.ClassificationLevels); }
        }

        /// <summary>
        /// Gets or sets the display value for the 1st classification level.
        /// </summary>
        public string ClassificationLevel1
        {
            get { return this.classificationLevel1; }
            set { this.SetProperty(ref this.classificationLevel1, value, () => this.ClassificationLevel1); }
        }

        /// <summary>
        /// Gets or sets the display value for the 2nd classification level.
        /// </summary>
        public string ClassificationLevel2
        {
            get { return this.classificationLevel2; }
            set { this.SetProperty(ref this.classificationLevel2, value, () => this.ClassificationLevel2); }
        }

        /// <summary>
        /// Gets or sets the display value for the 3rd classification level.
        /// </summary>        
        public string ClassificationLevel3
        {
            get { return this.classificationLevel3; }
            set { this.SetProperty(ref this.classificationLevel3, value, () => this.ClassificationLevel3); }
        }

        /// <summary>
        /// Gets or sets the display value for the 4th classification level.
        /// </summary>
        public string ClassificationLevel4
        {
            get { return this.classificationLevel4; }
            set { this.SetProperty(ref this.classificationLevel4, value, () => this.ClassificationLevel4); }
        }

        /// <summary>
        /// Gets the data source for the classification tree.
        /// </summary>
        public IList<TreeViewDataItem> ClassificationTreeSource
        {
            get { return this.classificationTreeSource; }
            private set { this.SetProperty(ref this.classificationTreeSource, value, () => this.ClassificationTreeSource); }
        }

        #endregion Data Properties

        #region State & Other Properties

        /// <summary>
        /// Gets or sets a value indicating whether the classification tree popup is open.
        /// </summary>        
        public bool IsClassificationTreePopupOpen
        {
            get
            {
                return this.isClassificationTreePopupOpen;
            }

            set
            {
                this.SetProperty(ref this.isClassificationTreePopupOpen, value, () => this.IsClassificationTreePopupOpen);
                if (value)
                {
                    this.OpenClassificationButtonContent = LocalizedResources.General_Close;
                }
                else
                {
                    this.OpenClassificationButtonContent = LocalizedResources.General_Select;
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to show only the first two levels.
        /// </summary>        
        public bool ShowOnlyTwoLevels
        {
            get { return this.showOnlyTwoLevels; }
            set { this.SetProperty(ref this.showOnlyTwoLevels, value, () => this.ShowOnlyTwoLevels); }
        }

        /// <summary>
        /// Gets or sets a value indicating OpenClassificationButton string content.
        /// </summary>        
        public string OpenClassificationButtonContent
        {
            get { return this.openClassificationButtonContent; }
            set { this.SetProperty(ref this.openClassificationButtonContent, value, () => this.OpenClassificationButtonContent); }
        }

        #endregion State & Other Properties

        /// <summary>
        /// Cancels all changes. Executed by the CancelCommand.
        /// </summary>
        protected override void Cancel()
        {
            base.Cancel();
            this.IsChanged = false;
        }

        /// <summary>
        /// Populates the classification level properties from the list of classification levels.
        /// </summary>
        private void PopulateLevels()
        {
            if (this.ClassificationLevels == null || this.ClassificationLevels.Count == 0)
            {
                this.ClassificationLevel1 = LocalizedResources.General_NotAvailable;
                this.ClassificationLevel2 = LocalizedResources.General_NotAvailable;
                this.ClassificationLevel3 = LocalizedResources.General_NotAvailable;
                this.ClassificationLevel4 = LocalizedResources.General_NotAvailable;

                return;
            }

            var lvl1 = this.ClassificationLevels[0] as INameable;
            if (lvl1 != null)
            {
                this.ClassificationLevel1 = lvl1.Name;
            }
            else
            {
                this.ClassificationLevel1 = this.ClassificationLevels[0].ToString();
            }

            if (this.ClassificationLevels.Count > 1)
            {
                var lvl2 = this.ClassificationLevels[1] as INameable;
                if (lvl2 != null)
                {
                    this.ClassificationLevel2 = lvl2.Name;
                }
                else
                {
                    this.ClassificationLevel2 = this.ClassificationLevels[1].ToString();
                }
            }

            if (this.ClassificationLevels.Count > 2)
            {
                var lvl3 = this.ClassificationLevels[2] as INameable;
                if (lvl3 != null)
                {
                    this.ClassificationLevel3 = lvl3.Name;
                }
                else
                {
                    this.ClassificationLevel3 = this.ClassificationLevels[2].ToString();
                }
            }

            if (this.ClassificationLevels.Count > 3)
            {
                var lvl4 = this.ClassificationLevels[3] as INameable;
                if (lvl4 != null)
                {
                    this.ClassificationLevel4 = lvl4.Name;
                }
                else
                {
                    this.ClassificationLevel4 = this.ClassificationLevels[3].ToString();
                }
            }

            switch (this.ClassificationLevels.Count)
            {
                case 1:
                    this.ClassificationLevel2 = LocalizedResources.General_NotAvailable;
                    this.ClassificationLevel3 = LocalizedResources.General_NotAvailable;
                    this.ClassificationLevel4 = LocalizedResources.General_NotAvailable;
                    break;
                case 2:
                    this.ClassificationLevel3 = LocalizedResources.General_NotAvailable;
                    this.ClassificationLevel4 = LocalizedResources.General_NotAvailable;
                    break;
                case 3:
                    this.ClassificationLevel4 = LocalizedResources.General_NotAvailable;
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Called when the ClassificationType property has changed.
        /// </summary>
        private void OnClassificationTypeChanged()
        {
            var clsType = this.ClassificationType;
            if (clsType != typeof(MachinesClassification)
                && clsType != typeof(MaterialsClassification)
                && clsType != typeof(ProcessStepsClassification))
            {
                throw new InvalidOperationException("The provided classification type is not supported.");
            }

            this.ShowOnlyTwoLevels = this.ClassificationType == typeof(ProcessStepsClassification) ? true : false;
            this.UnloadClassificationTree();
            this.IsClassificationTreePopupOpen = false;
        }

        /// <summary>
        /// Handles the PropertyChanged event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == ReflectionUtils.GetPropertyName(() => this.UndoManager))
            {
                this.ClassificationLevels = new ObservableCollection<object>();
                this.ClassificationLevels.CollectionChanged += HandleClassificationLevelsCollectionChanged;
            }
        }

        /// <summary>
        /// Handle the ClassificationLevels collection changed event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Collections.Specialized.NotifyCollectionChangedEventArgs"/> instance containing the event data.</param>
        private void HandleClassificationLevelsCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            this.PopulateLevels();
            this.IsChanged = true;
        }

        #region Command Actions

        /// <summary>
        /// Opens the classification tree popup.
        /// </summary>
        private void OpenClassificationTree()
        {
            if (!this.IsClassificationTreePopupOpen)
            {
                this.LoadClassificationTree();
                this.IsClassificationTreePopupOpen = true;
            }
            else
            {
                this.IsClassificationTreePopupOpen = false;
            }
        }

        /// <summary>
        /// Clears the selected classifications and closes the classification tree popup.
        /// </summary>
        private void ClearClassifications()
        {
            using (this.UndoManager.StartBatch(includePreviousItem: false, reverseUndoOrder: false, navigateToBatchControls: true))
            {
                this.IsClassificationTreePopupOpen = false;

                while (this.ClassificationLevels.Count > 0)
                {
                    this.ClassificationLevels.RemoveAt(0);
                }

                this.PopulateLevels();
                this.IsChanged = true;
            }
        }

        /// <summary>
        /// Saves the classifications currently selected in the classification tree.
        /// </summary>
        private void SelectClassification()
        {
            if (this.selectedClassificationTreeItem == null)
            {
                return;
            }

            using (this.UndoManager.StartBatch(includePreviousItem: false, reverseUndoOrder: false, navigateToBatchControls: true))
            {
                while (this.ClassificationLevels.Count > 0)
                {
                    this.ClassificationLevels.RemoveAt(0);
                }

                this.ClassificationLevels.Add(this.selectedClassificationTreeItem.DataObject);

                var parentItem = this.selectedClassificationTreeItem.Parent;
                while (parentItem != null)
                {
                    this.ClassificationLevels.Insert(0, parentItem.DataObject);
                    parentItem = parentItem.Parent;
                }

                this.IsChanged = true;
                this.PopulateLevels();
                this.IsClassificationTreePopupOpen = false;
            }
        }

        /// <summary>
        /// Saves internally the classification tree's currently selected item.
        /// Executed by the SelectedClassificationTreeItemChangedCommand.
        /// </summary>
        /// <param name="args">The <see cref="System.Windows.RoutedPropertyChangedEventArgs&lt;System.Object&gt;"/> instance containing the event data.</param>
        private void ClassificationTreeSelectedItemChanged(RoutedPropertyChangedEventArgs<object> args)
        {
            this.selectedClassificationTreeItem = args.NewValue as TreeViewDataItem;
        }

        /// <summary>
        /// Executed when the user has double-clicked the classification tree.
        /// </summary>
        /// <param name="args">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void ClassificationTreeDoubleClick(MouseButtonEventArgs args)
        {
            var treeItem = this.selectedClassificationTreeItem;
            if (treeItem == null || treeItem.Children.Count > 0)
            {
                return;
            }

            var clickSource = args.OriginalSource as DependencyObject;
            if (clickSource != null
                && UIHelper.FindParent<TreeViewItem>(clickSource) == null)
            {
                // The double click was outside any item.
                return;
            }

            this.SelectClassification();
        }

        #endregion Command Actions

        #region Classification Tree Load

        /// <summary>
        /// Loads the classification tree content.
        /// </summary>
        private void LoadClassificationTree()
        {
            if (this.isClassificationTreeLoaded)
            {
                return;
            }

            if (classificationType == typeof(MachinesClassification))
            {
                this.ClassificationTreeSource = this.CreateMachinesClassificationTreeSource();
            }
            else if (classificationType == typeof(MaterialsClassification))
            {
                this.ClassificationTreeSource = this.CreateRawMaterialsClassificationTreeSource();
            }
            else if (classificationType == typeof(ProcessStepsClassification))
            {
                this.ClassificationTreeSource = this.CreateProcessStepsClassificationTreeSource();
            }
            else
            {
                this.ClassificationTreeSource = null;
            }

            this.isClassificationTreeLoaded = true;
        }

        /// <summary>
        /// Unloads the classification tree content.
        /// </summary>
        private void UnloadClassificationTree()
        {
            this.ClassificationTreeSource = null;
            this.isClassificationTreeLoaded = false;
        }

        /// <summary>
        /// Creates the machines classification tree.
        /// </summary>
        /// <returns>
        /// A list containing the root tree items.
        /// </returns>
        private List<TreeViewDataItem> CreateMachinesClassificationTreeSource()
        {
            List<TreeViewDataItem> rootItems = new List<TreeViewDataItem>();
            try
            {
                var machinesClassification = this.DataAccessContext.MachinesClassificationRepository.LoadClassificationTree();
                foreach (MachinesClassification classification in machinesClassification.OrderBy(n => n.Name).Where(cls => cls.Parent == null))
                {
                    TreeViewDataItem item = new TreeViewDataItem()
                    {
                        DataObject = classification,
                        Label = classification.Name,
                        IconResourceKey = Images.MachineClassificationIconKey
                    };
                    rootItems.Add(item);
                    this.CreateMachinesClassificationSubtree(item, classification.Children);
                }
            }
            catch (DataAccessException ex)
            {
                log.ErrorException("Caught a data exception.", ex);
            }

            return rootItems;
        }

        /// <summary>
        /// Create recursively the sub-tree of a root item in the machines classification tree
        /// </summary>
        /// <param name="rootItem">The root item.</param>
        /// <param name="classifications">The classifications to add.</param>
        private void CreateMachinesClassificationSubtree(TreeViewDataItem rootItem, IEnumerable<MachinesClassification> classifications)
        {
            foreach (MachinesClassification classification in classifications)
            {
                TreeViewDataItem item = new TreeViewDataItem()
                {
                    DataObject = classification,
                    Label = classification.Name,
                    IconResourceKey = Images.MachineClassificationIconKey
                };
                rootItem.Children.Add(item);
                this.CreateMachinesClassificationSubtree(item, classification.Children);
            }
        }

        /// <summary>
        /// Creates the raw materials classification tree.
        /// </summary>
        /// <returns>
        /// The root items (which contain their children).
        /// </returns>
        private List<TreeViewDataItem> CreateRawMaterialsClassificationTreeSource()
        {
            List<TreeViewDataItem> rootItems = new List<TreeViewDataItem>();
            try
            {
                var materialsClassification = this.DataAccessContext.MaterialsClassificationRepository.LoadClassificationTree();
                foreach (MaterialsClassification classification in materialsClassification.OrderBy(n => n.Name).Where(cls => cls.Parent == null))
                {
                    TreeViewDataItem item = new TreeViewDataItem()
                    {
                        DataObject = classification,
                        Label = classification.Name,
                        IconResourceKey = Images.MaterialClassificationIconKey
                    };

                    rootItems.Add(item);
                    this.CreateRawMaterialsClassificationSubtree(item, classification.Children);
                }
            }
            catch (DataAccessException ex)
            {
                log.ErrorException("Caught a business exception.", ex);
            }

            return rootItems;
        }

        /// <summary>
        /// Create recursively the sub-tree of a root item in the raw materials classification tree
        /// </summary>
        /// <param name="rootItem">The root item.</param>
        /// <param name="classifications">The classifications to add.</param>
        private void CreateRawMaterialsClassificationSubtree(TreeViewDataItem rootItem, IEnumerable<MaterialsClassification> classifications)
        {
            foreach (MaterialsClassification classification in classifications)
            {
                TreeViewDataItem item = new TreeViewDataItem()
                {
                    DataObject = classification,
                    Label = classification.Name,
                    IconResourceKey = Images.MaterialClassificationIconKey
                };
                rootItem.Children.Add(item);
                this.CreateRawMaterialsClassificationSubtree(item, classification.Children);
            }
        }

        /// <summary>
        /// Creates the Process Step classification tree
        /// </summary>
        /// <returns>
        /// The root items of the tree; these items contain their children.
        /// </returns>
        private List<TreeViewDataItem> CreateProcessStepsClassificationTreeSource()
        {
            List<TreeViewDataItem> rootItems = new List<TreeViewDataItem>();
            try
            {
                var processStepsClassification = this.DataAccessContext.ProcessStepsClassificationRepository.LoadClassificationTree();
                foreach (ProcessStepsClassification classification in processStepsClassification.OrderBy(n => n.Name).Where(cls => cls.Parent == null))
                {
                    TreeViewDataItem item = new TreeViewDataItem()
                    {
                        DataObject = classification,
                        Label = classification.Name,
                        IconResourceKey = Images.ProcessStepsClassificationIconKey
                    };
                    rootItems.Add(item);
                    this.CreateProcessStepsClassificationSubtree(item, classification.Children);
                }
            }
            catch (DataAccessException ex)
            {
                log.ErrorException("Caught a data exception.", ex);
            }

            return rootItems;
        }

        /// <summary>
        /// Create recursively the sub-tree of a root item in the ProcessStep classification tree
        /// </summary>
        /// <param name="rootItem">The root item.</param>
        /// <param name="classifications">The classifications to add items for in the root item.</param>
        private void CreateProcessStepsClassificationSubtree(TreeViewDataItem rootItem, IEnumerable<ProcessStepsClassification> classifications)
        {
            foreach (ProcessStepsClassification classification in classifications)
            {
                TreeViewDataItem item = new TreeViewDataItem()
                {
                    DataObject = classification,
                    Label = classification.Name,
                    IconResourceKey = Images.ProcessStepsClassificationIconKey
                };

                rootItem.Children.Add(item);
                this.CreateProcessStepsClassificationSubtree(item, classification.Children);
            }
        }

        #endregion Classification Tree Load
    }
}