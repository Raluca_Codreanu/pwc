﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using AvalonDock;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Utils;
using ZPKTool.Gui.Views;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the IMainView view.
    /// </summary>
    //// TODO: refactor the way tools are loaded and unloaded for the main view.
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class MainViewModel : ViewModel, IMainMenuContentProvider
    {
        #region Attributes

        /// <summary>
        /// Constant representing the name of bookmark tree content name
        /// </summary>
        private const string BookmarksTreeContentName = "BookmarksTreeContent";

        /// <summary>
        /// Constant representing the name of search tool content name
        /// </summary>
        private const string SearchToolName = "SearchToolContent";

        /// <summary>
        /// Constant representing the name of compare tool content name
        /// </summary>
        private const string CompareToolContentName = "CompareToolContent";

        /// <summary>
        /// Constant representing the name of main tree content name
        /// </summary>
        private const string MainTreeContentName = "MainTreeContent";

        /// <summary>
        /// Constant representing the name of main document content name
        /// </summary>
        private const string MainDocumentContentName = "MainDocumentContent";

        /// <summary>
        /// Constant representing the name of advanced search content name
        /// </summary>
        private const string AdvancedSearchContentName = "AdvancedSearchContent";

        /// <summary>
        /// Constant representing the name of readonly document content name
        /// </summary>
        private const string ReadonlyDocumentContentName = "ReadonlyDocumentContent";

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The composition container.
        /// </summary>
        private CompositionContainer container;

        /// <summary>
        /// the window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The PleaseWaitService reference.
        /// </summary>
        private IPleaseWaitService pleaseWaitService;

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The import service
        /// </summary>
        private IImportService importService;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        /// <summary>
        /// The content in the central part of the main view.
        /// </summary>
        private object content;

        /// <summary>
        /// The read only content from the central part of the main view.
        /// </summary>
        private object readonlyContent;

        /// <summary>
        /// The title of the docking manager's main document (the document containing the main view content).
        /// </summary>
        private string mainDocumentTitle;

        /// <summary>
        /// The title of the docking manager's readonly document .
        /// </summary>
        private string readonlyDocumentTitle;

        /// <summary>
        /// The visibility of the content's vertical scroll bar.
        /// </summary>
        private ScrollBarVisibility contentVerticalScrollBarVisibility;

        /// <summary>
        /// The search tool.
        /// </summary>
        private SearchToolViewModel searchTool;

        /// <summary>
        /// The compare tool.
        /// </summary>
        private CompareToolViewModel compareTool;

        /// <summary>
        /// The Projects Tree view model.
        /// </summary>
        private ProjectsTreeViewModel projectsTreeViewModel;

        /// <summary>
        /// The Bookmarks Tree view model.
        /// </summary>
        private BookmarksTreeViewModel bookmarksTreeViewModel;

        /// <summary>
        /// The menu items displayed in the main menu when the main view is loaded.
        /// </summary>
        private Collection<SystemMenuItem> mainMenuItems;

        #endregion Attributes

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel" /> class.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="messenger">The messenger service.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="pleaseWaitService">The please wait service.</param>
        /// <param name="importService">The import service</param>
        /// <param name="unitsService">The units service.</param>
        [ImportingConstructor]
        public MainViewModel(
            CompositionContainer container,
            IMessenger messenger,
            IWindowService windowService,
            IPleaseWaitService pleaseWaitService,
            IImportService importService,
            IUnitsService unitsService)
        {
            this.container = container;
            this.messenger = messenger;
            this.WindowService = windowService;
            this.pleaseWaitService = pleaseWaitService;
            this.importService = importService;
            this.unitsService = unitsService;

            // Initialize commands
            InitializeCommands();

            // Initialize properties            
            this.ContentVerticalScrollBarVisibility = ScrollBarVisibility.Auto;
            SelectedExplorerTree = ExplorerTree.None;

            // Register to receive messages
            this.messenger.Register<NotificationMessage>(this.HandleNotificationMessage);
            this.messenger.Register<EntityChangedMessage>(this.HandleEntityChange);
            this.messenger.Register<EntitiesChangedMessage>(this.HandleEntitiesChangedMessage);
            this.messenger.Register<LoadContentInMainViewMessage>(this.HandleLoadContentRequests, GlobalMessengerTokens.MainViewTargetToken);
            this.messenger.Register<NotificationMessageWithCallback>(this.HandleNotificationsWithCallback, GlobalMessengerTokens.MainViewTargetToken, true);
            this.messenger.Register<SelectExplorerTreeRequestMessage>(this.HandleSelectEntityTreeRequest);

            // Activate mainTreeContent when navigate to an entity from ProjectsTree
            this.messenger.Register<NavigateToEntityMessage>(
                msg =>
                {
                    if (this.DockingManager != null)
                    {
                        var mainTreeContent = this.DockingManager.DockableContents.FirstOrDefault(d => d.Name == MainTreeContentName);
                        if (mainTreeContent != null &&
                            !mainTreeContent.IsActiveContent)
                        {
                            mainTreeContent.Activate();
                        }
                    }
                },
                GlobalMessengerTokens.MainViewTargetToken);

            this.messenger.Register<UpdateReadonlyDocumentContentMessage>(
                this.HandleReadonlyDocumentContentUpdate,
                GlobalMessengerTokens.MainViewTargetToken);
        }

        #region Commands

        /// <summary>
        /// Gets the command executed when the Docking Manager has been loaded.
        /// </summary>
        public ICommand DockingManagerLoadedCommand { get; private set; }

        /// <summary>
        /// Gets the command executed when Docking Manager Layout has been updated.
        /// </summary>
        public ICommand DockingManagerLayoutUpdatedCommand { get; private set; }

        /// <summary>
        /// Gets the command executed when the Docking Manager Active Content has changed.
        /// </summary>
        public ICommand DockingManagerActiveContentChangedCommand { get; private set; }

        /// <summary>
        /// Gets the Compare command.
        /// </summary>
        public ICommand CompareCommand { get; private set; }

        /// <summary>
        /// Gets the "Copy to Master Data" command.
        /// </summary>
        public ICommand CopyToMasterDataCommand { get; private set; }

        /// <summary>
        /// Gets the open advanced search command.
        /// </summary>
        public ICommand OpenAdvancedSearchCommand { get; private set; }

        /// <summary>
        /// Gets the command for the mouse double click event
        /// </summary>
        public ICommand IsActiveDocumentChangedCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets or sets the docking manager.
        /// </summary>
        private DockingManager DockingManager { get; set; }

        /// <summary>
        /// Gets the content in the central part of the main view.
        /// </summary>
        public object Content
        {
            get
            {
                return this.content;
            }

            private set
            {
                if (this.content != value)
                {
                    this.content = value;
                    OnPropertyChanged("Content");
                }
            }
        }

        /// <summary>
        /// Gets the readonly tab content from the central part of the main view.
        /// </summary>
        public object ReadonlyContent
        {
            get
            {
                return this.readonlyContent;
            }

            private set
            {
                if (this.readonlyContent != value)
                {
                    this.readonlyContent = value;
                    OnPropertyChanged("ReadonlyContent");
                }
            }
        }

        /// <summary>
        /// Gets or sets the title of the docking manager's main document (the document containing the main view content).
        /// </summary>
        public string MainDocumentTitle
        {
            get
            {
                return this.mainDocumentTitle;
            }

            set
            {
                if (this.mainDocumentTitle != value)
                {
                    this.mainDocumentTitle = value;
                    OnPropertyChanged("MainDocumentTitle");
                }
            }
        }

        /// <summary>
        /// Gets or sets the title of the docking manager's readonly document.
        /// </summary>
        public string ReadonlyDocumentTitle
        {
            get
            {
                return this.readonlyDocumentTitle;
            }

            set
            {
                if (this.readonlyDocumentTitle != value)
                {
                    this.readonlyDocumentTitle = value;
                    OnPropertyChanged("ReadonlyDocumentTitle");
                }
            }
        }

        /// <summary>
        /// Gets or sets the visibility of the content's vertical scroll bar.
        /// </summary>
        public ScrollBarVisibility ContentVerticalScrollBarVisibility
        {
            get
            {
                return this.contentVerticalScrollBarVisibility;
            }

            set
            {
                if (this.contentVerticalScrollBarVisibility != value)
                {
                    this.contentVerticalScrollBarVisibility = value;
                    OnPropertyChanged("ContentVerticalScrollBarVisibility");
                }
            }
        }

        /// <summary>
        /// Gets or sets the search tool view-model.
        /// </summary>
        [Import]
        public SearchToolViewModel SearchTool
        {
            get { return this.searchTool; }
            set { this.SetProperty(ref this.searchTool, value, () => this.SearchTool); }
        }

        /// <summary>
        /// Gets or sets the compare tool.
        /// </summary>
        [Import]
        public CompareToolViewModel CompareTool
        {
            get
            {
                return this.compareTool;
            }

            set
            {
                if (this.compareTool != value)
                {
                    this.compareTool = value;
                    OnPropertyChanged("CompareTool");
                }
            }
        }

        /// <summary>
        /// Gets or sets the window service.
        /// </summary>
        public IWindowService WindowService
        {
            get
            {
                return this.windowService;
            }

            set
            {
                if (this.windowService != value)
                {
                    this.windowService = value;
                    OnPropertyChanged(() => this.WindowService);
                }
            }
        }

        /// <summary>
        /// Gets or sets the Projects Tree view model.
        /// </summary>
        [Import]
        public ProjectsTreeViewModel ProjectsTreeViewModel
        {
            get { return this.projectsTreeViewModel; }
            set { this.SetProperty(ref this.projectsTreeViewModel, value, () => this.ProjectsTreeViewModel); }
        }

        /// <summary>
        /// Gets or sets the Bookmarks Tree view model.
        /// </summary>
        [Import]
        public BookmarksTreeViewModel BookmarksTreeViewModel
        {
            get { return this.bookmarksTreeViewModel; }
            set { this.SetProperty(ref this.bookmarksTreeViewModel, value, () => this.BookmarksTreeViewModel); }
        }

        /// <summary>
        /// Gets the currently selected (focused) explorer tree view (Projects Tree or Bookmarks Tree).
        /// </summary>
        public ExplorerTree SelectedExplorerTree { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the ReadOnly DocumentContent is selected or not.
        /// </summary>
        public bool IsReadonlyTabSelected
        {
            get
            {
                var readonlyDoc = this.DockingManager.Documents.FirstOrDefault(p => p.Name.Equals(ReadonlyDocumentContentName));
                return readonlyDoc != null && readonlyDoc.IsActiveDocument;
            }
        }

        #endregion Properties

        /// <summary>
        /// Called after the view has been loaded. Usually it performs some initialization that needs the view to be loaded, like
        /// starting to load data from a database.
        /// <para />
        /// During unit tests this method must be manually called because there is no view to call it.
        /// </summary>
        public override void OnLoaded()
        {
            base.OnLoaded();

            this.ProjectsTreeViewModel.OnLoaded();
            this.BookmarksTreeViewModel.OnLoaded();
        }

        /// <summary>
        /// Called when the main view has been unloaded.
        /// </summary>
        public override void OnUnloaded()
        {
            base.OnUnloaded();
            this.messenger.Unregister(this);
            this.SearchTool.OnUnloaded();
            this.CompareTool.OnUnloaded();
            this.ProjectsTreeViewModel.OnUnloaded();
            this.BookmarksTreeViewModel.OnUnloaded();
        }

        /// <summary>
        /// Called before the main view is unloaded.
        /// </summary>
        /// <returns>False if the main view unload should be canceled, true if it should continue.</returns>
        public override bool OnUnloading()
        {
            this.SaveAdvancedSearchConfiguration();
            this.SaveDockingManagerLayout();
            return this.UnloadMainDocumentContent();
        }

        #region Initialization

        /// <summary>
        /// Initializes the commands.
        /// </summary>
        private void InitializeCommands()
        {
            this.DockingManagerLoadedCommand = new DelegateCommand<RoutedEventArgs>(this.HandleDockingManagerLoaded);
            this.DockingManagerLayoutUpdatedCommand = new DelegateCommand<RoutedEventArgs>(this.HandleDockingManagerLayoutUpdated);
            this.DockingManagerActiveContentChangedCommand = new DelegateCommand<RoutedEventArgs>(this.HandleDockingManagerActiveContentChanged);

            this.CompareCommand = new DelegateCommand<object>(this.Compare, this.CanCompare);
            this.CopyToMasterDataCommand = new DelegateCommand<object>(this.CopyToMasterData, this.CanCopyToMasterData);
            this.OpenAdvancedSearchCommand = new DelegateCommand<object>(this.OpenAdvancedSearch);
            this.IsActiveDocumentChangedCommand = new DelegateCommand<EventArgs>(this.IsActiveDocumentChanged);
        }

        #endregion Initialization

        #region Content management

        /// <summary>
        /// Handles requests to load content into the main view.
        /// </summary>
        /// <param name="loadRequest">The load request.</param>
        private void HandleLoadContentRequests(LoadContentInMainViewMessage loadRequest)
        {
            this.ContentVerticalScrollBarVisibility =
                loadRequest.AllowVerticalScrolling
                    ? ScrollBarVisibility.Auto
                    : ScrollBarVisibility.Disabled;
            this.LoadMainDocumentContent(loadRequest.Content, loadRequest.Title);
        }

        /// <summary>
        /// Loads the specified content in the Main Document.
        /// Use this method if the previous content was already removed.
        /// </summary>
        /// <param name="newContent">The content to load.</param>
        /// <param name="title">The document title.</param>
        private void LoadMainDocumentContent(object newContent, string title)
        {
            /*
             * Determines whether the new content will be loaded into the readonly tab or not.
             */
            var viewModel = newContent as ViewModel;
            var selectReadonlyTab = this.projectsTreeViewModel.IsReadOnly &&
                                    ((viewModel != null && viewModel.IsReadOnly) ||
                                     newContent is ResultDetailsViewModel);

            if (selectReadonlyTab)
            {
                /*
                 * Set the MainDocument "readonly" content.
                 */
                this.ReadonlyContent = newContent;
                var lfcMngr = this.ReadonlyContent as ILifeCycleManager;
                if (lfcMngr != null)
                {
                    lfcMngr.OnLoaded();
                }

                /*
                 * Set the title for the MainDocument readonly tab.
                 */
                if (!string.IsNullOrEmpty(title))
                {
                    this.ReadonlyDocumentTitle =
                        string.Format(
                            "[{0}] {1}",
                            LocalizedResources.General_ReadonlyTabTitle,
                            title);
                }
                else
                {
                    this.ReadonlyDocumentTitle = string.Empty;
                }

                /*
                 * Load (refresh) the readonly document tab.
                 */
                this.UpdateReadonlyDocumentContent(this.ReadonlyContent, this.ReadonlyDocumentTitle);
            }
            else
            {
                /*
                 * Set the MainDocument content and title.
                 */
                this.Content = newContent;
                var lfcMngr = this.Content as ILifeCycleManager;
                if (lfcMngr != null)
                {
                    lfcMngr.OnLoaded();
                }

                this.MainDocumentTitle = title;
            }

            this.SelectMainDocumentContent(selectReadonlyTab);
        }

        /// <summary>
        /// Triggers the "Unloading"/"OnUnloading" event of the current content of the Main Document.
        /// </summary>
        /// <remarks>DO NOT CALL THIS METHOD UNLESS YOU FULLY UNDERSTAND WHAT UNLOADING A VIEW-MODEL OR EDIT CONTROL MEANS</remarks>
        /// <returns>True if the unload was successful; otherwise false.</returns>
        private bool UnloadMainDocumentContent()
        {
            // DO NOT CALL THIS METHOD UNLESS YOU FULLY UNDERSTAND WHAT UNLOADING A VIEW-MODEL OR EDIT CONTROL MEANS
            bool isUnloadSuccessfull = true;

            ILifeCycleManager lfcMngr = this.Content as ILifeCycleManager;
            if (lfcMngr != null)
            {
                isUnloadSuccessfull = lfcMngr.OnUnloading();

                if (isUnloadSuccessfull)
                {
                    lfcMngr.OnUnloaded();
                }
            }

            return isUnloadSuccessfull;
        }

        /// <summary>
        /// Displays the compare view in a document pane.
        /// </summary>
        /// <param name="itemsToCompare">The items to compare.</param>
        private void DisplayCompareView(IList<CompareToolItem> itemsToCompare)
        {
            CompareViewModel compViewModel = this.container.GetExportedValue<CompareViewModel>();
            string title = LocalizedResources.General_Compare + " ";

            foreach (var item in itemsToCompare)
            {
                string name = item.Name;
                if (!string.IsNullOrWhiteSpace(name))
                {
                    name = name.Trim();

                    if (name.Length > 15)
                    {
                        title += name.Substring(0, 14);
                    }
                    else
                    {
                        title += name;
                    }

                    title += "-";
                }
            }

            title = title.Substring(0, title.Length - 1);

            if (title.Length > 60)
            {
                title = title.Substring(0, 58);
            }

            compViewModel.CompareToolItemCollection.AddRange(itemsToCompare);

            DocumentContent compareDocumentContent = new DocumentContent();
            compareDocumentContent.Content = new ContentPresenter() { Content = compViewModel };
            compareDocumentContent.IsCloseable = true;
            compareDocumentContent.IsFloatingAllowed = false;
            compareDocumentContent.Title = title;
            compareDocumentContent.HorizontalAlignment = HorizontalAlignment.Stretch;
            this.DockingManager.MainDocumentPane.Items.Add(compareDocumentContent);
            compareDocumentContent.Show(this.DockingManager);
            this.DockingManager.MainDocumentPane.SelectedItem = compareDocumentContent;
            compareDocumentContent.Activate();
        }

        /// <summary>
        /// Selects the content of the main document.
        /// </summary>
        /// <param name="selectReadonlyTab">A value indicating whether to select the readonly tab or not.</param>
        private void SelectMainDocumentContent(bool selectReadonlyTab = false)
        {
            this.UpdateProjectsTreeReadonlyState(selectReadonlyTab);
            var documents = this.DockingManager.MainDocumentPane.Items.OfType<DocumentContent>();
            var documentContentName = this.projectsTreeViewModel.IsReadOnly
                ? ReadonlyDocumentContentName
                : MainDocumentContentName;
            var documentContent = documents.FirstOrDefault(p => string.Equals(p.Name, documentContentName));
            this.DockingManager.MainDocumentPane.SelectedItem = documentContent;
        }

        /// <summary>
        /// Shows the compare tool dockable pane.
        /// </summary>
        private void ShowCompareToolDockablePane()
        {
            var dockableContents = this.DockingManager.DockableContents.OfType<DockableContent>();
            DockableContent compareToolDockableCont = dockableContents.FirstOrDefault(d => d.Name.Equals(CompareToolContentName));
            if (compareToolDockableCont != null)
            {
                if (compareToolDockableCont.State != DockableContentState.Hidden)
                {
                    compareToolDockableCont.Activate();
                }
                else
                {
                    compareToolDockableCont.Show(this.DockingManager);
                }
            }
        }

        #endregion Content management

        #region IMainMenuContentProvider Members

        /// <summary>
        /// Gets the items to be displayed in the main menu along with the default ones.
        /// </summary>
        /// <returns>
        /// The list of menu items to appear in the main menu.
        /// </returns>
        public Collection<SystemMenuItem> GetMainMenuItems()
        {
            if (this.mainMenuItems != null)
            {
                return this.mainMenuItems;
            }

            this.mainMenuItems = new Collection<SystemMenuItem>();

            // Create the Edit Menu and its content            
            if (SecurityManager.Instance.CurrentUserHasRight(Right.ProjectsAndCalculate))
            {
                var editMenuItem = new SystemMenuItem() { HeaderText = LocalizedResources.General_Edit, Index = 1, IsTopLevel = true, AutomationId = "EditMenuItem" };
                this.mainMenuItems.Add(editMenuItem);

                var cutMenuItem = new SystemMenuItem() { IconResourceKey = Images.CutIconKey, Command = MainViewCommands.Cut, AutomationId = "CutMenuItem" };
                var copyMenuItem = new SystemMenuItem() { IconResourceKey = Images.CopyIconKey, Command = MainViewCommands.Copy, AutomationId = "CopyMenuItem" };
                var pasteMenuItem = new SystemMenuItem() { IconResourceKey = Images.PasteIconKey, Command = MainViewCommands.Paste, AutomationId = "PasteMenuItem" };
                var deleteMenuItem = new SystemMenuItem() { IconResourceKey = Images.DeleteIconKey, Command = MainViewCommands.Delete, AutomationId = "DeleteMenuItem" };

                editMenuItem.Items.Add(cutMenuItem);
                editMenuItem.Items.Add(copyMenuItem);
                editMenuItem.Items.Add(pasteMenuItem);
                editMenuItem.Items.Add(deleteMenuItem);
            }

            // Create the Projects Menu and its content            
            if (SecurityManager.Instance.CurrentUserHasRight(Right.ProjectsAndCalculate))
            {
                var projectMenuItem = new SystemMenuItem() { HeaderText = LocalizedResources.General_Project, Index = 2, IsTopLevel = true, AutomationId = "ProjectMenuItem" };
                this.mainMenuItems.Add(projectMenuItem);

                var createProjectMenuItem = new SystemMenuItem() { IconResourceKey = Images.ProjectIconKey, Command = MainViewCommands.CreateProject, AutomationId = "CreateProjectMenuItem" };
                var createFolderMenuItem = new SystemMenuItem() { IconResourceKey = Images.ProjectFolderNewIconKey, Command = MainViewCommands.CreateProjectFolder, AutomationId = "CreateFolderMenuItem" };
                var createAssemblyMenuItem = new SystemMenuItem() { IconResourceKey = Images.AssemblyIconKey, Command = MainViewCommands.CreateAssembly };
                var createPartMenuItem = new SystemMenuItem() { IconResourceKey = Images.PartIconKey, Command = MainViewCommands.CreatePart, AutomationId = "CreatePartMenuItem" };

                var createMaterialMenuItem = new SystemMenuItem() { HeaderText = LocalizedResources.General_CreateMaterial };
                var createRawMaterialMenuItem = new SystemMenuItem() { IconResourceKey = Images.RawMaterialIconKey, Command = MainViewCommands.CreateRawMaterial };
                var createCommodityMenuItem = new SystemMenuItem() { IconResourceKey = Images.CommodityIconKey, Command = MainViewCommands.CreateCommodity };
                var createRawPartMenuItem = new SystemMenuItem() { IconResourceKey = Images.RawPartIconKey, Command = MainViewCommands.CreateRawPart };
                createMaterialMenuItem.Items.Add(createRawMaterialMenuItem);
                createMaterialMenuItem.Items.Add(createCommodityMenuItem);
                createMaterialMenuItem.Items.Add(createRawPartMenuItem);

                var createConsumableMenuItem = new SystemMenuItem() { IconResourceKey = Images.ConsumableIconKey, Command = MainViewCommands.CreateConsumable };
                var createMachineMenuItem = new SystemMenuItem() { IconResourceKey = Images.MachineIconKey, Command = MainViewCommands.CreateMachine };
                var createDieMenuItem = new SystemMenuItem() { IconResourceKey = Images.DieIconKey, Command = MainViewCommands.CreateDie };
                var addBookmarkMenuItem = new SystemMenuItem() { IconResourceKey = Images.AddBookmarkIconKey, Command = MainViewCommands.AddBookmark };
                var removeBookmarkMenuItem = new SystemMenuItem() { IconResourceKey = Images.RemoveBookmarkIconKey, Command = MainViewCommands.RemoveBookmark };
                var importMenuItem = new SystemMenuItem() { HeaderText = LocalizedResources.General_Import, IconResourceKey = Images.ImportIconKey };
                var exportMenuItem = new SystemMenuItem() { IconResourceKey = Images.ExportIconKey, Command = MainViewCommands.Export, AutomationId = "ExportMenuItem" };

                var importProjectFolderMenuItem = new SystemMenuItem()
                {
                    HeaderText = LocalizedResources.General_ProjectFolder,
                    IconResourceKey = Images.FolderIconKey,
                    Command = MainViewCommands.Import,
                    CommandParameter = typeof(ProjectFolder)
                };

                var importProjectMenuItem = new SystemMenuItem()
                {
                    HeaderText = LocalizedResources.General_Project,
                    IconResourceKey = Images.ProjectIconKey,
                    Command = MainViewCommands.Import,
                    CommandParameter = typeof(Project)
                };

                var importAssemblyMenuItem = new SystemMenuItem()
                {
                    HeaderText = LocalizedResources.General_Assembly,
                    IconResourceKey = Images.AssemblyIconKey,
                    Command = MainViewCommands.Import,
                    CommandParameter = typeof(Assembly)
                };

                var importPartMenuItem = new SystemMenuItem()
                {
                    HeaderText = LocalizedResources.General_Part,
                    IconResourceKey = Images.PartIconKey,
                    Command = MainViewCommands.Import,
                    CommandParameter = typeof(Part)
                };

                var importRawPartMenuItem = new SystemMenuItem()
                {
                    HeaderText = LocalizedResources.General_RawPart,
                    IconResourceKey = Images.RawPartIconKey,
                    Command = MainViewCommands.Import,
                    CommandParameter = typeof(RawPart)
                };

                var importRawMaterialMenuItem = new SystemMenuItem()
                {
                    HeaderText = LocalizedResources.General_RawMaterial,
                    IconResourceKey = Images.RawMaterialIconKey,
                    Command = MainViewCommands.Import,
                    CommandParameter = typeof(RawMaterial)
                };

                var importCommodityMenuItem = new SystemMenuItem()
                {
                    HeaderText = LocalizedResources.General_Commodity,
                    IconResourceKey = Images.CommodityIconKey,
                    Command = MainViewCommands.Import,
                    CommandParameter = typeof(Commodity)
                };

                var importConsumableMenuItem = new SystemMenuItem()
                {
                    HeaderText = LocalizedResources.General_Consumable,
                    IconResourceKey = Images.ConsumableIconKey,
                    Command = MainViewCommands.Import,
                    CommandParameter = typeof(Consumable)
                };

                var importMachineMenuItem = new SystemMenuItem()
                {
                    HeaderText = LocalizedResources.General_Machine,
                    IconResourceKey = Images.MachineIconKey,
                    Command = MainViewCommands.Import,
                    CommandParameter = typeof(Machine)
                };

                var importDieMenuItem = new SystemMenuItem()
                {
                    HeaderText = LocalizedResources.General_Die,
                    IconResourceKey = Images.DieIconKey,
                    Command = MainViewCommands.Import,
                    CommandParameter = typeof(Die)
                };

                importMenuItem.Items.Add(importProjectFolderMenuItem);
                importMenuItem.Items.Add(importProjectMenuItem);
                importMenuItem.Items.Add(importAssemblyMenuItem);
                importMenuItem.Items.Add(importPartMenuItem);
                importMenuItem.Items.Add(importRawPartMenuItem);
                importMenuItem.Items.Add(importRawMaterialMenuItem);
                importMenuItem.Items.Add(importCommodityMenuItem);
                importMenuItem.Items.Add(importConsumableMenuItem);
                importMenuItem.Items.Add(importMachineMenuItem);
                importMenuItem.Items.Add(importDieMenuItem);

                projectMenuItem.Items.Add(createProjectMenuItem);
                projectMenuItem.Items.Add(createFolderMenuItem);
                projectMenuItem.Items.Add(createAssemblyMenuItem);
                projectMenuItem.Items.Add(createPartMenuItem);
                projectMenuItem.Items.Add(createMaterialMenuItem);
                projectMenuItem.Items.Add(createConsumableMenuItem);
                projectMenuItem.Items.Add(createMachineMenuItem);
                projectMenuItem.Items.Add(createDieMenuItem);
                projectMenuItem.Items.Add(new SystemMenuItem() { IsSeparator = true });
                projectMenuItem.Items.Add(addBookmarkMenuItem);
                projectMenuItem.Items.Add(removeBookmarkMenuItem);
                projectMenuItem.Items.Add(new SystemMenuItem() { IsSeparator = true });
                projectMenuItem.Items.Add(importMenuItem);
                projectMenuItem.Items.Add(exportMenuItem);
            }

            // Create the Data Menu and its content
            var dataMenuItem = new SystemMenuItem() { HeaderText = LocalizedResources.General_Data, Index = 3, AutomationId = "dataMenuItem" };
            var massDataUpdateMenuItem = new SystemMenuItem() { IconResourceKey = Images.MassDataUpdateIconKey, Command = MainViewCommands.MassDataUpdate, AutomationId = "MassDataUpdateMenuItem" };
            var synchronizeDataMenuItem = new SystemMenuItem()
            {
                IconResourceKey = Images.SyncIconKey,
                Command = MainViewCommands.Synchronize,
                AutomationId = "SyncDataMenuItem"
            };

            this.mainMenuItems.Add(dataMenuItem);
            dataMenuItem.Items.Add(massDataUpdateMenuItem);
            dataMenuItem.Items.Add(synchronizeDataMenuItem);

            if (SecurityManager.Instance.CurrentUserHasRight(Right.ViewAndReports))
            {
                var reportsItem = new SystemMenuItem() { HeaderText = LocalizedResources.General_Reports, Index = 4 };
                var batchReportingItem = new SystemMenuItem() { IconResourceKey = Images.BatchReportingIconKey, Command = MainViewCommands.BatchReporting };
                var reportGeneratorItem = new SystemMenuItem() { IconResourceKey = Images.ReportGeneratorIconKey, Command = MainViewCommands.ReportGenerator };

                this.mainMenuItems.Add(reportsItem);
                reportsItem.Items.Add(batchReportingItem);
                reportsItem.Items.Add(reportGeneratorItem);
            }

            // Create the View Menu and its content
            if (SecurityManager.Instance.CurrentUserHasRight(Right.ViewAndReports))
            {
                var viewItem = new SystemMenuItem() { HeaderText = LocalizedResources.General_View, Index = 6, AutomationId = "ViewMenuItem" };
                var bookmarkItem = new SystemMenuItem()
                {
                    IconResourceKey = Images.BookmarksIconKey,
                    HeaderText = LocalizedResources.General_Bookmarks,
                    Command = MainViewCommands.ShowToolPane,
                    AutomationId = "BookmarkViewMenuItem",
                    CommandParameter = BookmarksTreeContentName
                };
                var searchItem = new SystemMenuItem()
                {
                    IconResourceKey = Images.SearchIconKey,
                    HeaderText = LocalizedResources.General_Search,
                    Command = MainViewCommands.ShowToolPane,
                    AutomationId = "SearchViewMenuItem",
                    CommandParameter = SearchToolName
                };
                var advancedSearchItem = new SystemMenuItem()
                {
                    IconResourceKey = Images.SearchIconKey,
                    HeaderText = LocalizedResources.General_AdvancedSearch,
                    Command = MainViewCommands.AdvancedSearch,
                    AutomationId = "AdvancedSearchViewMenuItem",
                    CommandParameter = SearchToolName
                };
                var compareItem = new SystemMenuItem()
                {
                    IconResourceKey = Images.CompareIconKey,
                    HeaderText = LocalizedResources.General_Compare,
                    Command = MainViewCommands.ShowToolPane,
                    AutomationId = "CompareViewMenuItem",
                    CommandParameter = CompareToolContentName
                };

                this.mainMenuItems.Add(viewItem);
                viewItem.Items.Add(bookmarkItem);
                viewItem.Items.Add(searchItem);
                viewItem.Items.Add(advancedSearchItem);
                viewItem.Items.Add(compareItem);
            }

            return this.mainMenuItems;
        }

        #endregion IMainMenuContentProvider Members

        #region Docking Manager

        /// <summary>
        /// Handles the loading of the Docking Manager.
        /// </summary>
        /// <param name="eventArgs">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void HandleDockingManagerLoaded(RoutedEventArgs eventArgs)
        {
            this.DockingManager = eventArgs.Source as DockingManager;
            if (this.DockingManager == null)
            {
                throw new InvalidOperationException("The Docking manager instance was null.");
            }

            this.LoadDockingManagerLayout();
        }

        /// <summary>
        /// Handles the layout update of the Docking Manager.
        /// </summary>
        /// <param name="eventArgs">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void HandleDockingManagerLayoutUpdated(EventArgs eventArgs)
        {
            foreach (var window in DockingManager.FloatingWindows)
            {
                var frameworkElem = window.Content as FrameworkElement;
                if (frameworkElem != null)
                {
                    var binding = new System.Windows.Data.Binding("ZoomLevel");
                    binding.Source = UserSettingsManager.Instance;

                    ScaleTransform scaleTransform = new ScaleTransform();
                    BindingOperations.SetBinding(scaleTransform, ScaleTransform.ScaleXProperty, binding);
                    BindingOperations.SetBinding(scaleTransform, ScaleTransform.ScaleYProperty, binding);
                    frameworkElem.LayoutTransform = scaleTransform;
                }
            }
        }

        /// <summary>
        /// Handles the active content changed of the Docking Manager.
        /// </summary>
        /// <param name="eventArgs">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void HandleDockingManagerActiveContentChanged(EventArgs eventArgs)
        {
            if (this.DockingManager.ActiveContent == null)
            {
                return;
            }

            if (this.SelectedExplorerTree != ExplorerTree.Projects && this.DockingManager.ActiveContent.Name == MainTreeContentName)
            {
                this.SelectProjectsTree();
                this.ProjectsTreeViewModel.LoadSelectedTreeItemContent();
            }
            else if (this.SelectedExplorerTree != ExplorerTree.Bookmarks && this.DockingManager.ActiveContent.Name == BookmarksTreeContentName)
            {
                this.SelectBookmarksTree();
                this.BookmarksTreeViewModel.LoadSelectedTreeItemContent();
            }
        }

        /// <summary>
        /// Selects the bookmarks tree.
        /// </summary>
        private void SelectBookmarksTree()
        {
            this.SelectedExplorerTree = ExplorerTree.Bookmarks;
            this.projectsTreeViewModel.IsActive = false;
            this.bookmarksTreeViewModel.IsActive = true;
        }

        /// <summary>
        /// Selects the projects tree.
        /// </summary>
        private void SelectProjectsTree()
        {
            this.SelectedExplorerTree = ExplorerTree.Projects;
            this.projectsTreeViewModel.IsActive = true;
            this.bookmarksTreeViewModel.IsActive = false;
        }

        /// <summary>
        /// Sets the entity tree selection to none, meaning no entity tree is selected.
        /// </summary>
        private void SelectNoEntityTree()
        {
            this.SelectedExplorerTree = ExplorerTree.None;
            this.projectsTreeViewModel.IsActive = false;
            this.bookmarksTreeViewModel.IsActive = false;
        }

        /// <summary>
        /// Loads the docking manager's layout from the file where it was previously saved or from a default layout file if it was never saved.
        /// </summary>
        private void LoadDockingManagerLayout()
        {
            if (this.DockingManager == null)
            {
                return;
            }

            bool layoutRestored = false;
            try
            {
                // Restore the main view layout from the app's layout configuration file, if it exists.
                string layoutFile = Constants.ApplicationDataFolderPath + "\\UILayout.xml";
                if (File.Exists(layoutFile))
                {
                    ManagedContentCollection<DockableContent> dockableContents = this.DockingManager.DockableContents;
                    DockableContent compareToolContent = dockableContents.FirstOrDefault(d => d.Name == CompareToolContentName);
                    DockableContent bookmarksTreeContent = dockableContents.FirstOrDefault(d => d.Name == BookmarksTreeContentName);
                    DockableContent mainTreeContent = dockableContents.FirstOrDefault(d => d.Name == MainTreeContentName);

                    this.DockingManager.RestoreLayout(layoutFile);
                    layoutRestored = true;

                    if (!this.DockingManager.DockableContents.Contains(compareToolContent))
                    {
                        // Workaround to show the Docked CompareToolContent.
                        // It seems to be a reported (avalon dock discussions:261652). ShowAsFloatingWindow() should be removed.
                        compareToolContent.ShowAsFloatingWindow(this.DockingManager, true);
                        compareToolContent.Show(this.DockingManager, AnchorStyle.Right);
                        compareToolContent.Activate();
                        compareToolContent.ToggleAutoHide();
                    }

                    if (!this.DockingManager.DockableContents.Contains(bookmarksTreeContent))
                    {
                        // Workaround to show the BookmarksTreeContent next to the MainTreeContent.
                        bookmarksTreeContent.ContainerPane.Items.Remove(bookmarksTreeContent);
                        mainTreeContent.ContainerPane.Items.Add(bookmarksTreeContent);
                        mainTreeContent.ContainerPane.SelectedIndex = 0;
                    }

                    // Find the selected tree content and set focus on it
                    // This will enable the navigation using the keyboard
                    var treeContentPane = mainTreeContent.Parent as DockablePane;
                    if (treeContentPane != null)
                    {
                        var selectedContent = treeContentPane.SelectedItem as DockableContent;
                        if (selectedContent != null)
                        {
                            UIUtils.FocusElement(selectedContent);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.WarnException("Failed to restore the docking manager layout from the app layout file.", ex);
            }

            if (!layoutRestored)
            {
                try
                {
                    // If the layout was not restored because configuration file does not exist or its format was not OK, restore the default layout.
                    using (Stream layoutXmlStream =
                        System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("ZPKTool.Gui.Resources.DefaultDockingManagerLayout.xml"))
                    {
                        this.DockingManager.RestoreLayout(layoutXmlStream);
                    }
                }
                catch (Exception ex)
                {
                    log.WarnException("Failed to restore the docking manager layout from the default layout file.", ex);
                }
            }
        }

        /// <summary>
        /// Saves the layout of the docking manager in order to be restored later.
        /// </summary>
        private void SaveDockingManagerLayout()
        {
            try
            {
                // Save the main screen layout
                if (this.DockingManager != null)
                {
                    string layoutFile = Constants.ApplicationDataFolderPath + "\\UILayout.xml";
                    this.DockingManager.SaveLayout(layoutFile);
                }
            }
            catch (Exception ex)
            {
                log.WarnException("Failed to save the main screen layout.", ex);
            }
        }

        /// <summary>
        /// Handle the UpdateReadonlyDocumentContentMessage request.
        /// </summary>
        /// <param name="msg">The UpdateReadonlyDocumentContent message.</param>
        private void HandleReadonlyDocumentContentUpdate(UpdateReadonlyDocumentContentMessage msg)
        {
            if (this.DockingManager == null || msg == null || msg.Content == null)
            {
                return;
            }

            this.UpdateReadonlyDocumentContent(msg.Content, msg.Title);
        }

        /// <summary>
        /// Updates the content of the MainDocumentPane ReadonlyDocument.
        /// </summary>
        /// <param name="newContent">The ReadonlyDocument content.</param>
        /// <param name="title">The ReadonlyDocument title.</param>
        private void UpdateReadonlyDocumentContent(object newContent, string title)
        {
            var docContent = this.DockingManager.Documents.FirstOrDefault(p => p.Name.Equals(ReadonlyDocumentContentName));
            if (docContent == null)
            {
                docContent = new DocumentContent();
                docContent.Name = ReadonlyDocumentContentName;
                docContent.IsCloseable = true;
                docContent.IsFloatingAllowed = false;
                docContent.HorizontalAlignment = HorizontalAlignment.Stretch;
                docContent.IsActiveDocumentChanged += ReadonlyDocument_IsActiveDocumentChanged;
                this.DockingManager.MainDocumentPane.Items.Add(docContent);
            }

            docContent.Content = new ContentPresenter() { Content = newContent, Margin = new Thickness(10) };
            var titlePrefix = string.Format("[{0}]", LocalizedResources.General_ReadonlyTabTitle);
            docContent.Title = this.GetDocumentTitle(newContent, title, titlePrefix);

            docContent.Show(this.DockingManager);
            if (this.DockingManager.MainDocumentPane.Items.Contains(docContent))
            {
                this.DockingManager.MainDocumentPane.SelectedItem = docContent;
            }

            docContent.Activate();

            this.UpdateProjectsTreeReadonlyState(true);
        }

        /// <summary>
        /// Handles the IsActiveDocumentChanged event of the MainView ReadonlyDocument control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void ReadonlyDocument_IsActiveDocumentChanged(object sender, EventArgs e)
        {
            this.UpdateProjectsTreeReadonlyState(this.IsReadonlyTabSelected);
        }

        /// <summary>
        /// Update projectsTreeViewModel ReadOnly state, 
        /// based on the value passed as parameter.
        /// </summary>
        /// <param name="isReadOnly">A value indicating whether the ProjectsTree 
        /// is in Readonly mode or not.</param>
        private void UpdateProjectsTreeReadonlyState(bool isReadOnly)
        {
            this.projectsTreeViewModel.IsReadOnly = isReadOnly;
            var myProjectsTreeItem = 
                this.projectsTreeViewModel.Items.OfType<MyProjectsTreeItem>().FirstOrDefault();
            if (myProjectsTreeItem != null)
            {
                myProjectsTreeItem.ReadOnly = isReadOnly;
            }
        }

        #endregion Docking Manager

        #region Message handling

        /// <summary>
        /// Handles the messages of type NotificationMessage.
        /// </summary>
        /// <param name="message">The message.</param>
        private void HandleNotificationMessage(NotificationMessage message)
        {
            if (message.Notification == Notification.ReloadMainViewLayout)
            {
                this.LoadDockingManagerLayout();
            }
            else if (message.Notification == Notification.DisplayCompareView)
            {
                this.DisplayCompareView(this.CompareTool.CompareCollection);
            }
            else if (message.Notification == Notification.MainViewShowDocumentContent)
            {
                this.SelectMainDocumentContent(this.projectsTreeViewModel.SelectedItemIsReadonly);
            }
        }

        /// <summary>
        /// Handles the messages of type EntityChangedMessage.
        /// </summary>
        /// <param name="message">The message.</param>
        private void HandleEntityChange(EntityChangedMessage message)
        {
            var selectedItem = this.GetSelectedTreeItem();
            if (selectedItem == null)
            {
                return;
            }

            // Update the title of the document in which the changed entity is opened.
            IIdentifiable selectedEntity = selectedItem.DataObject as IIdentifiable;
            IIdentifiable changedEntity = message.Entity as IIdentifiable;
            if (selectedEntity != null
                && changedEntity != null
                && selectedEntity.Guid == changedEntity.Guid)
            {
                this.SetMainDocumentTitle(message.Entity);
            }
        }

        /// <summary>
        /// Handles the entities change messages.
        /// </summary>
        /// <param name="message">The message.</param>  
        private void HandleEntitiesChangedMessage(EntitiesChangedMessage message)
        {
            if (message == null)
            {
                return;
            }

            foreach (var msgChange in message.Changes)
            {
                this.HandleEntityChange(msgChange);
            }
        }

        /// <summary>
        /// Handles the messages of type NotificationMessageWithCallback and derived types (like NotificationMessageWithAction).
        /// </summary>
        /// <param name="message">The message.</param>
        private void HandleNotificationsWithCallback(NotificationMessageWithCallback message)
        {
            if (message.Notification == Notification.MainViewNotifyContentToUnload)
            {
                // Trigger the unload of the main document's content and return the result to the sender.
                bool unloaded = this.UnloadMainDocumentContent();
                message.ExecuteCallback(unloaded);
            }
            else if (message.Notification == Notification.MainViewGetContent)
            {
                // Return the main document's content to the sender.
                var content = this.Content;
                message.ExecuteCallback(content);
            }
        }

        /// <summary>
        /// Handles messages of type SelectEntityTreeRequestMessage.
        /// </summary>
        /// <param name="message">The message.</param>
        private void HandleSelectEntityTreeRequest(SelectExplorerTreeRequestMessage message)
        {
            switch (message.TreeToSelect)
            {
                case ExplorerTree.Projects:
                    this.SelectProjectsTree();
                    break;

                case ExplorerTree.Bookmarks:
                    this.SelectBookmarksTree();
                    break;

                default:
                    this.SelectNoEntityTree();
                    break;
            }
        }

        #endregion Message handling

        #region Menu commands handling

        /// <summary>
        /// Determines whether this instance can compare the specified parameter.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        ///   <c>true</c> if this instance can compare the specified parameter; otherwise, <c>false</c>.
        /// </returns>
        private bool CanCompare(object parameter)
        {
            var inputData = parameter as IEnumerable<EntityInfo>;
            if (inputData == null || inputData.Count() == 0)
            {
                return false;
            }

            // Check also if all items have same type
            Type firstItemType = null;
            var firstItem = inputData.FirstOrDefault();
            if (firstItem.Entity != null)
            {
                firstItemType = firstItem.Entity.GetType();
            }

            foreach (var item in inputData)
            {
                if (item.Entity == null
                    || !(item.Entity is Project
                    || item.Entity is Assembly
                    || item.Entity is Part)
                    || firstItemType != item.Entity.GetType())
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Contains the logic associated with the Compare command.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void Compare(object parameter)
        {
            var inputData = parameter as IList<EntityInfo>;
            if (inputData == null || inputData.Count == 0)
            {
                return;
            }

            // Check also if all items have same type
            Type firstItemType = null;
            var firstItem = inputData.FirstOrDefault();
            if (firstItem.Entity != null)
            {
                firstItemType = firstItem.Entity.GetType();
            }

            foreach (var item in inputData)
            {
                if (item.Entity == null || firstItemType != item.Entity.GetType())
                {
                    return;
                }
            }

            if (inputData.Count == 1)
            {
                // If are multiple items to compare then do not add items to compare tool,
                var item = inputData.FirstOrDefault();
                this.CompareTool.AddItemToCompare(item.Entity, item.DbIdentifier);
                this.ShowCompareToolDockablePane();
            }
            else if (inputData.Count > 1)
            {
                // If are multiple items to compare then do not add items to compare tool,
                // they should be compared immediately.
                var itemsToCompare = new List<CompareToolItem>();
                foreach (var item in inputData)
                {
                    var name = string.Empty;
                    var nameableObject = item.Entity as INameable;
                    if (nameableObject != null)
                    {
                        name = nameableObject.Name;
                    }

                    var id = Guid.Empty;
                    var identifiableObject = item.Entity as IIdentifiable;
                    if (identifiableObject != null)
                    {
                        id = identifiableObject.Guid;
                    }

                    var compareItem = new CompareToolItem(name, id, item.Entity.GetType(), item.DbIdentifier);
                    itemsToCompare.Add(compareItem);
                }

                this.DisplayCompareView(itemsToCompare);
            }
        }

        /// <summary>
        /// Determines whether the Copy to Master Data command can be executed.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        /// true if the command can be executed; otherwise, false.
        /// </returns>
        private bool CanCopyToMasterData(object parameter)
        {
            if (!SecurityManager.Instance.CurrentUserHasRight(Right.EditMasterData))
            {
                return false;
            }

            var inputData = parameter as IEnumerable<EntityInfo>;
            if (inputData == null || inputData.Count() == 0)
            {
                return false;
            }

            foreach (var item in inputData)
            {
                if (EntityUtils.IsMasterData(item.Entity)
                    || !(item.Entity is Assembly
                    || item.Entity is Part
                    || item.Entity is Machine
                    || item.Entity is RawMaterial
                    || item.Entity is Commodity
                    || item.Entity is Consumable
                    || item.Entity is Die
                    || item.Entity is Manufacturer
                    || item.Entity is Customer))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// The logic associated with the Copy to Master Data command.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void CopyToMasterData(object parameter)
        {
            if (!SecurityManager.Instance.CurrentUserHasRight(Right.EditMasterData))
            {
                return;
            }

            var inputData = parameter as IEnumerable<EntityInfo>;
            if (inputData == null || inputData.Count() == 0)
            {
                return;
            }

            foreach (var item in inputData)
            {
                if (EntityUtils.IsMasterData(item.Entity)
                    || item.Entity == null)
                {
                    return;
                }
            }

            // Ask for confirmation
            var answer = this.WindowService.MessageDialogService.Show(LocalizedResources.General_ConfirmationCopyToMasterData, MessageDialogType.YesNo);
            if (answer != MessageDialogResult.Yes)
            {
                return;
            }

            Action<PleaseWaitService.WorkParams> work = (workParams) =>
            {
                // Check if the central database is online.
                DatabaseHelper.IsCentralDatabaseOnline();

                foreach (var item in inputData)
                {
                    // Get the base currency of the parent project.
                    var dataManager = DataAccessFactory.CreateDataSourceManager(item.DbIdentifier);
                    IDataSourceManager centralDbDataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

                    var objectToCopyProject = dataManager.ProjectRepository.GetParentProject(DataAccess.Utils.GetDBEntity(item.Entity, dataManager));
                    var objectToCopyProjectBaseCurrency = dataManager.CurrencyRepository.GetProjectBaseCurrency(objectToCopyProject.Guid);

                    CloneManager cloneManager = new CloneManager(dataManager, centralDbDataManager);
                    var clone = this.CloneObjectToCopyToMasterData(item.Entity, dataManager, cloneManager);
                    if (clone != null)
                    {
                        var masterDataclone = clone as IMasterDataObject;
                        masterDataclone.SetIsMasterData(true);

                        // If the object is not a IOwnedObject object means that has no owner and no set is required.
                        var ownedClone = clone as IOwnedObject;
                        if (ownedClone != null)
                        {
                            ownedClone.SetOwner(null);
                        }

                        // Convert the clone currencies data.
                        CurrencyConversionManager.ConvertObject(clone, CurrencyConversionManager.DefaultBaseCurrency, objectToCopyProjectBaseCurrency);

                        var cloneRepository = centralDbDataManager.Repository(clone.GetType());
                        cloneRepository.Add(clone);
                        centralDbDataManager.SaveChanges();
                    }
                }
            };

            Action<PleaseWaitService.WorkParams> workCompleted = (workParams) =>
            {
                if (workParams.Error != null)
                {
                    this.windowService.MessageDialogService.Show(workParams.Error);
                }
            };

            this.pleaseWaitService.Show(LocalizedResources.General_CopyingToMasterData, work, workCompleted);
        }

        /// <summary>
        /// The logic associated with the OpenAdvancedSearch command.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void OpenAdvancedSearch(object parameter)
        {
            if (this.DockingManager == null)
            {
                return;
            }

            var advancedSearchDocumentContent = this.DockingManager.MainDocumentPane.Items.OfType<DocumentContent>().FirstOrDefault(d => d.Name.Equals(AdvancedSearchContentName));
            if (advancedSearchDocumentContent != null)
            {
                if (!advancedSearchDocumentContent.IsActiveDocument)
                {
                    advancedSearchDocumentContent.Activate();
                }

                return;
            }

            var advancedSearchViewModel = this.container.GetExportedValue<AdvancedSearchViewModel>();
            advancedSearchViewModel.OnLoaded();

            advancedSearchDocumentContent = new DocumentContent();
            advancedSearchDocumentContent.Name = AdvancedSearchContentName;
            advancedSearchDocumentContent.Closing += new EventHandler<CancelEventArgs>(AdvancedSearchDocumentContent_Closing);
            advancedSearchDocumentContent.Content = new ContentPresenter { Content = advancedSearchViewModel };
            advancedSearchDocumentContent.IsCloseable = true;
            advancedSearchDocumentContent.IsFloatingAllowed = false;
            advancedSearchDocumentContent.Title = LocalizedResources.General_AdvancedSearch;
            advancedSearchDocumentContent.HorizontalAlignment = HorizontalAlignment.Stretch;
            this.DockingManager.MainDocumentPane.Items.Add(advancedSearchDocumentContent);
            advancedSearchDocumentContent.Show(this.DockingManager);
            this.DockingManager.MainDocumentPane.SelectedItem = advancedSearchDocumentContent;
            advancedSearchDocumentContent.Activate();
        }

        #endregion Menu commands handling

        #region Commands handling

        /// <summary>
        /// Handles the Closing event of the advanced search document content.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        private void AdvancedSearchDocumentContent_Closing(object sender, CancelEventArgs e)
        {
            var documentContent = sender as DocumentContent;
            if (documentContent != null)
            {
                var content = documentContent.Content as ContentPresenter;
                if (content != null)
                {
                    var viewModel = content.Content as AdvancedSearchViewModel;
                    if (viewModel != null)
                    {
                        viewModel.OnUnloaded();
                    }
                }
            }
        }

        /// <summary>
        /// Determines whether the MainDocumentPane active document has changed.
        /// </summary>
        /// <param name="obj">The <see cref="EventArgs"/> instance containing 
        /// the event data.</param>
        private void IsActiveDocumentChanged(EventArgs obj)
        {
            this.UpdateProjectsTreeReadonlyState(this.IsReadonlyTabSelected);
        }

        #endregion Commands handling

        #region Helpers

        /// <summary>
        /// Sets the main document title according to a provided entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        private void SetMainDocumentTitle(object entity)
        {
            var title = GetTitleFromModel(entity);
            if (title != null)
            {
                this.MainDocumentTitle = title;
            }
        }

        /// <summary>
        /// Generate the document title.
        /// </summary>
        /// <param name="docContent">The content.</param>
        /// <param name="title">The default title. [if set will be used as document title]</param>
        /// <param name="titlePrefix">The title prefix.</param>
        /// <returns>The generated title corresponding to the content passed as parameter.</returns>
        private string GetDocumentTitle(object docContent, string title, string titlePrefix)
        {
            if (docContent == null)
            {
                return string.Empty;
            }

            string contentTitle = string.Empty;
            if (!string.IsNullOrWhiteSpace(title))
            {
                contentTitle = title;
            }
            else
            {
                var model = this.GetModelFrom(docContent);
                if (model != null)
                {
                    string defaultTitle = string.Empty;
                    var subassembliesVm = docContent as SubassembliesViewModel;
                    if (subassembliesVm != null)
                    {
                        defaultTitle = LocalizedResources.General_Subassemblies + " (" +
                                       subassembliesVm.ParentAssembly.Name + ")";
                    }

                    var partsVm = docContent as PartsViewModel;
                    if (partsVm != null)
                    {
                        defaultTitle = LocalizedResources.General_Parts + " (" + partsVm.ParentAssembly.Name + ")";
                    }

                    var materialsVm = docContent as MaterialsViewModel;
                    if (materialsVm != null)
                    {
                        defaultTitle = LocalizedResources.General_Materials + " (" + materialsVm.ParentPart.Name + ")";
                    }

                    var resultDetailsVm = docContent as ResultDetailsViewModel;
                    if (resultDetailsVm != null && resultDetailsVm.Entity is INameable)
                    {
                        defaultTitle = LocalizedResources.General_ResultDetails + " (" +
                                       (resultDetailsVm.Entity as INameable).Name + ")";
                    }

                    if (string.IsNullOrEmpty(defaultTitle))
                    {
                        defaultTitle = this.GetTitleFromModel(model);
                    }

                    contentTitle = string.IsNullOrWhiteSpace(titlePrefix)
                        ? defaultTitle
                        : string.Format("{0} {1}", titlePrefix, defaultTitle);
                }
            }

            return contentTitle;
        }

        /// <summary>
        /// Get the default title, based on the model passed as paramaeter.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>The title generated by entity type.</returns>
        private string GetTitleFromModel(object entity)
        {
            var project = entity as Project;
            if (project != null)
            {
                return LocalizedResources.General_Project + " (" + project.Name + ")";
            }

            var assembly = entity as Assembly;
            if (assembly != null)
            {
                return LocalizedResources.General_Assembly + " (" + assembly.Name + ")";
            }

            var rawPart = entity as RawPart;
            if (rawPart != null)
            {
                return LocalizedResources.General_RawPart + " (" + rawPart.Name + ")";
            }

            var part = entity as Part;
            if (part != null)
            {
                return LocalizedResources.General_Part + " (" + part.Name + ")";
            }

            var rawMaterial = entity as RawMaterial;
            if (rawMaterial != null)
            {
                return LocalizedResources.General_RawMaterial + " (" + rawMaterial.Name + ")";
            }

            var commodity = entity as Commodity;
            if (commodity != null)
            {
                return LocalizedResources.General_Commodity + " (" + commodity.Name + ")";
            }

            var process = entity as Process;
            if (process != null)
            {
                object parentEntity = null;
                if (process.Assemblies.Count == 1)
                {
                    parentEntity = process.Assemblies.First();
                }

                if (process.Parts.Count == 1)
                {
                    parentEntity = process.Parts.First();
                }

                var parentName = EntityUtils.GetEntityName(parentEntity);
                if (!string.IsNullOrWhiteSpace(parentName))
                {
                    return LocalizedResources.General_Process + " (" + parentName + ")";
                }
            }

            var step = entity as ProcessStep;
            if (step != null)
            {
                return LocalizedResources.General_ProcessStep + " (" + step.Name + ")";
            }

            return null;
        }

        /// <summary>
        /// Gets the selected tree item from the selected tree.
        /// </summary>
        /// <returns>The selected tree view data item.</returns>
        private TreeViewDataItem GetSelectedTreeItem()
        {
            TreeViewDataItem item = null;
            if (this.ProjectsTreeViewModel.IsActive)
            {
                item = this.ProjectsTreeViewModel.SelectedTreeItem;
            }
            else if (this.BookmarksTreeViewModel.IsActive)
            {
                item = this.BookmarksTreeViewModel.SelectedTreeItem;
            }

            return item;
        }

        /// <summary>
        /// Saves the advanced search configuration.
        /// </summary>
        private void SaveAdvancedSearchConfiguration()
        {
            try
            {
                if (this.DockingManager != null)
                {
                    var documentContent = this.DockingManager.MainDocumentPane.Items.OfType<DocumentContent>().FirstOrDefault(d => d.Name.Equals(AdvancedSearchContentName));
                    if (documentContent != null)
                    {
                        var contentPresenter = documentContent.Content as ContentPresenter;
                        if (contentPresenter != null)
                        {
                            var viewModel = contentPresenter.Content as AdvancedSearchViewModel;
                            if (viewModel != null)
                            {
                                viewModel.OnUnloaded();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.WarnException("Failed to save the advanced search configurations.", ex);
            }
        }

        /// <summary>
        /// Clones the object provided so it can be copied to master data
        /// </summary>
        /// <param name="objectToClone">The object to clone</param>
        /// <param name="dataManager">The data source manager used to load the object before cloning it.</param>
        /// <param name="cloneManager">The clone manager</param>
        /// <returns>The cloned object or null if the object type is not supported</returns>
        private object CloneObjectToCopyToMasterData(object objectToClone, IDataSourceManager dataManager, CloneManager cloneManager)
        {
            Assembly assy = objectToClone as Assembly;
            if (assy != null)
            {
                var assyFull = dataManager.AssemblyRepository.GetAssemblyFull(assy.Guid);
                return cloneManager.Clone(assyFull, false, false);
            }

            Part part = objectToClone as Part;
            if (part != null)
            {
                var partFull = dataManager.PartRepository.GetPartFull(part.Guid);
                return cloneManager.Clone(partFull, false, false);
            }

            Machine machine = objectToClone as Machine;
            if (machine != null)
            {
                var machineFull = dataManager.MachineRepository.GetById(machine.Guid);
                return cloneManager.Clone(machineFull);
            }

            RawMaterial material = objectToClone as RawMaterial;
            if (material != null)
            {
                var materialFull = dataManager.RawMaterialRepository.GetById(material.Guid);
                return cloneManager.Clone(materialFull);
            }

            Commodity commodity = objectToClone as Commodity;
            if (commodity != null)
            {
                var commodityFull = dataManager.CommodityRepository.GetById(commodity.Guid);
                return cloneManager.Clone(commodityFull);
            }

            Consumable consumable = objectToClone as Consumable;
            if (consumable != null)
            {
                var consumableFull = dataManager.ConsumableRepository.GetById(consumable.Guid);
                return cloneManager.Clone(consumableFull);
            }

            Die die = objectToClone as Die;
            if (die != null)
            {
                var dieFull = dataManager.DieRepository.GetById(die.Guid);
                return cloneManager.Clone(dieFull);
            }

            Manufacturer manufacturer = objectToClone as Manufacturer;
            if (manufacturer != null)
            {
                var manufacturerFull = dataManager.ManufacturerRepository.GetById(manufacturer.Guid);
                return cloneManager.Clone(manufacturerFull);
            }

            Customer customer = objectToClone as Customer;
            if (customer != null)
            {
                var customerFull = dataManager.SupplierRepository.FindById(customer.Guid);
                return cloneManager.Clone(customerFull);
            }

            throw new InvalidOperationException(string.Format("Copy to Master Data is not implemented for type {0}.", objectToClone.GetType()));
        }

        /// <summary>
        /// Gets the Model property from a parent entity.
        /// </summary>
        /// <param name="entity">The entity that may contain a Model.</param>
        /// <returns>The entity Model property value.</returns>
        private object GetModelFrom(object entity)
        {
            if (entity == null)
            {
                return null;
            }

            var type = entity.GetType().BaseType;
            if (type != null && type.IsGenericType
                && (type.GetGenericTypeDefinition() == typeof(ViewModel<>)
                    || type.GetGenericTypeDefinition() == typeof(ViewModel<,>)))
            {
                var tabModel = entity.GetType().GetProperty("Model");
                if (tabModel != null)
                {
                    return tabModel.GetValue(entity, null);
                }
            }

            var subassembliesVm = entity as SubassembliesViewModel;
            if (subassembliesVm != null)
            {
                return subassembliesVm.ParentAssembly;
            }

            var partsVm = entity as PartsViewModel;
            if (partsVm != null)
            {
                return partsVm.ParentAssembly;
            }

            var materialsVm = entity as MaterialsViewModel;
            if (materialsVm != null)
            {
                return materialsVm.ParentPart;
            }

            var resultDetails = entity as ResultDetailsViewModel;
            if (resultDetails != null)
            {
                return resultDetails.Entity;
            }

            return null;
        }

        #endregion Helpers
    }
}