﻿using System;
using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Input;
using ZPKTool.Business;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for editing and viewing a CountrySetting instance.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class CountrySettingsViewModel : ViewModel<CountrySetting, IDataSourceManager>
    {
        #region Fields

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// A value indicating whether to check if there are any updates for the country settings model.
        /// </summary>
        private bool checkForUpdate;

        /// <summary>
        /// The available latest country settings obtained during the last update check.
        /// </summary>
        private CountrySetting latestCountrySettings;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        /// <summary>
        /// The UnitsAdapter handler, used to perform certain operations when some UnitsAdapter properties are updated.
        /// </summary>
        private UnitsAdapterUpdateHandler unitsAdapterHandler;

        #endregion Fields

        /// <summary>
        /// Initializes a new instance of the <see cref="CountrySettingsViewModel"/> class.
        /// </summary>
        /// <param name="unitsService">The units service.</param>
        [ImportingConstructor]
        public CountrySettingsViewModel(IUnitsService unitsService)
        {
            Argument.IsNotNull("unitsService", unitsService);

            this.unitsService = unitsService;

            this.ApplyUpdatedCountrySettingsCommand = new DelegateCommand(this.ApplyUpdate, this.CanApplyUpdate);

            // Update the shift model costs when data related to their calculations changes
            this.OneShiftModelCostRatio.ValueChanged += (s, e) =>
                this.OneShiftModelCost.Value = this.CalculateShiftModelCosts(this.OneShiftModelCostRatio.Value);
            this.TwoShiftsModelCostRatio.ValueChanged += (s, e) =>
                this.TwoShiftsModelCost.Value = this.CalculateShiftModelCosts(this.TwoShiftsModelCostRatio.Value);
            this.ThreeShiftsModelCostRatio.ValueChanged += (s, e) =>
                this.ThreeShiftsModelCost.Value = this.CalculateShiftModelCosts(this.ThreeShiftsModelCostRatio.Value);
            this.LaborAvailability.ValueChanged += (s, e) => this.CalculateAllShiftModelCosts();
            this.UnskilledLaborCost.ValueChanged += (s, e) => this.CalculateAllShiftModelCosts();
            this.SkilledLaborCost.ValueChanged += (s, e) => this.CalculateAllShiftModelCosts();
            this.ForemanCost.ValueChanged += (s, e) => this.CalculateAllShiftModelCosts();
            this.TechnicianCost.ValueChanged += (s, e) => this.CalculateAllShiftModelCosts();
            this.EngineerCost.ValueChanged += (s, e) => this.CalculateAllShiftModelCosts();

            this.UpdateAvailable.ValueChanged += (s, e) =>
            {
                if (!this.UpdateAvailable.Value)
                {
                    this.ShowUpdatedSettings.Value = false;
                }
            };
        }

        #region Commands

        /// <summary>
        /// Gets the command that applies the updated country settings, when they are available.
        /// </summary>        
        public ICommand ApplyUpdatedCountrySettingsCommand { get; private set; }

        #endregion Commands

        #region Model Related Properties

        /// <summary>
        /// Gets the cost ratio for the 1 shift model.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Cost1Shift", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("ShiftCharge1ShiftModel", AffectsCost = true)]
        public DataProperty<decimal?> OneShiftModelCostRatio { get; private set; }

        /// <summary>
        /// Gets the updated cost ratio for the 1 shift model.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public VMProperty<decimal?> UpdatedOneShiftModelCostRatio { get; private set; }

        /// <summary>
        /// Gets the cost ratio for the 2 shifts model.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Cost2Shift", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("ShiftCharge2ShiftModel", AffectsCost = true)]
        public DataProperty<decimal?> TwoShiftsModelCostRatio { get; private set; }

        /// <summary>
        /// Gets the updated cost ratio for the 2 shifts model.
        /// </summary>
        public VMProperty<decimal?> UpdatedTwoShiftsModelCostRatio { get; private set; }

        /// <summary>
        /// Gets the cost ratio for the 3 shifts model.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Cost3Shift", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("ShiftCharge3ShiftModel", AffectsCost = true)]
        public DataProperty<decimal?> ThreeShiftsModelCostRatio { get; private set; }

        /// <summary>
        /// Gets the updated cost ratio for the 3 shifts model.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public VMProperty<decimal?> UpdatedThreeShiftsModelCostRatio { get; private set; }

        /// <summary>
        /// Gets the unskilled labor cost.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_UnskilledLabourCost", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("UnskilledLaborCost", AffectsCost = true)]
        public DataProperty<decimal?> UnskilledLaborCost { get; private set; }

        /// <summary>
        /// Gets the updated unskilled labor cost.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public VMProperty<decimal?> UpdatedUnskilledLaborCost { get; private set; }

        /// <summary>
        /// Gets the skilled labor cost.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_SkilledLabourCost", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("SkilledLaborCost", AffectsCost = true)]
        public DataProperty<decimal?> SkilledLaborCost { get; private set; }

        /// <summary>
        /// Gets the updated skilled labor cost.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public VMProperty<decimal?> UpdatedSkilledLaborCost { get; private set; }

        /// <summary>
        /// Gets the foreman cost.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_ForemanCost", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("ForemanCost", AffectsCost = true)]
        public DataProperty<decimal?> ForemanCost { get; private set; }

        /// <summary>
        /// Gets the updated foreman cost.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public VMProperty<decimal?> UpdatedForemanCost { get; private set; }

        /// <summary>
        /// Gets the technician cost.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_TechnicianCost", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("TechnicianCost", AffectsCost = true)]
        public DataProperty<decimal?> TechnicianCost { get; private set; }

        /// <summary>
        /// Gets the updated technician cost.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public VMProperty<decimal?> UpdatedTechnicianCost { get; private set; }

        /// <summary>
        /// Gets the engineer cost.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_EngineerCost", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("EngineerCost", AffectsCost = true)]
        public DataProperty<decimal?> EngineerCost { get; private set; }

        /// <summary>
        /// Gets the updated engineer cost.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public VMProperty<decimal?> UpdatedEngineerCost { get; private set; }

        /// <summary>
        /// Gets the labor availability.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_LabourAvailability", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("LaborAvailability", AffectsCost = true)]
        public DataProperty<decimal?> LaborAvailability { get; private set; }

        /// <summary>
        /// Gets the updated labor availability.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public VMProperty<decimal?> UpdatedLaborAvailability { get; private set; }

        /// <summary>
        /// Gets the energy cost.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_EnergyCost", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("EnergyCost", AffectsCost = true)]
        public DataProperty<decimal?> EnergyCost { get; private set; }

        /// <summary>
        /// Gets the updated energy cost.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public VMProperty<decimal?> UpdatedEnergyCost { get; private set; }

        /// <summary>
        /// Gets the air cost.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_AirCost", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("AirCost", AffectsCost = true)]
        public DataProperty<decimal?> AirCost { get; private set; }

        /// <summary>
        /// Gets the updated air cost.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public VMProperty<decimal?> UpdatedAirCost { get; private set; }

        /// <summary>
        /// Gets the water cost.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_WaterCost", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("WaterCost", AffectsCost = true)]
        public DataProperty<decimal?> WaterCost { get; private set; }

        /// <summary>
        /// Gets the updated water cost.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public VMProperty<decimal?> UpdatedWaterCost { get; private set; }

        /// <summary>
        /// Gets the production area rental cost.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_ProductionAreaRentalCost", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("ProductionAreaRentalCost", AffectsCost = true)]
        public DataProperty<decimal?> ProductionAreaRentalCost { get; private set; }

        /// <summary>
        /// Gets the updated production area rental cost.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public VMProperty<decimal?> UpdatedProductionAreaRentalCost { get; private set; }

        /// <summary>
        /// Gets the office area rental cost.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_OfficeAreaRentalCost", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("OfficeAreaRentalCost", AffectsCost = true)]
        public DataProperty<decimal?> OfficeAreaRentalCost { get; private set; }

        /// <summary>
        /// Gets the updated office area rental cost.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public VMProperty<decimal?> UpdatedOfficeAreaRentalCost { get; private set; }

        /// <summary>
        /// Gets the interest rate.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_InterestRate", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("InterestRate", AffectsCost = true)]
        public DataProperty<decimal?> InterestRate { get; private set; }

        /// <summary>
        /// Gets the updated interest rate.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public VMProperty<decimal?> UpdatedInterestRate { get; private set; }

        /// <summary>
        /// Gets the last change timestamp.
        /// </summary>
        [ExposesModelProperty("LastChangeTimestamp")]
        public DataProperty<DateTime?> LastChangeTimestamp { get; private set; }

        #endregion Model Related Properties

        #region Other Properties

        /// <summary>
        /// Gets a value indicating whether to show the updated country settings.
        /// </summary>        
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public VMProperty<bool> ShowUpdatedSettings { get; private set; }

        /// <summary>
        /// Gets the labor costs associated with the current 1-shift model cost ratio.
        /// </summary>        
        public VMProperty<ShiftModelLaborCosts> OneShiftModelCost { get; private set; }

        /// <summary>
        /// Gets the labor costs associated with the current 2-shifts model cost ratio.
        /// </summary>
        public VMProperty<ShiftModelLaborCosts> TwoShiftsModelCost { get; private set; }

        /// <summary>
        /// Gets the labor costs associated with the current 2-shifts model cost ratio.
        /// </summary>
        public VMProperty<ShiftModelLaborCosts> ThreeShiftsModelCost { get; private set; }

        /// <summary>
        /// Gets a value indicating whether an update is available for the current country settings.
        /// </summary>        
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public VMProperty<bool> UpdateAvailable { get; private set; }

        /// <summary>
        /// Gets the last update date.
        /// </summary>  
        public VMProperty<DateTime?> LastUpdateTime { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether to check if there are any updates for the country settings model.
        /// The default value is false.
        /// </summary>         
        public bool CheckForUpdate
        {
            get { return this.checkForUpdate; }
            set { this.SetProperty(ref this.checkForUpdate, value, () => this.CheckForUpdate, this.PerformCheckForUpdate); }
        }

        /// <summary>
        /// Gets or sets the name of the country from which the original values of the Model came from.
        /// This property should be set before setting the Model.
        /// </summary>
        public string SourceCountryName { get; set; }

        /// <summary>
        /// Gets or sets the name of the country supplier from which the original values of the Model came from.
        /// When setting this property you also must set the SourceCountryName to the name of the supplier's country.
        /// This property should be set before setting the Model.
        /// </summary>
        public string SourceCountrySupplierName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to allow to apply an available country settings update when the view-model is in read-only mode.
        /// The default value is false.
        /// </summary>        
        public bool AllowToApplyUpdateWhenReadOnly { get; set; }

        /// <summary>
        /// Gets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter { get; private set; }

        #endregion Other Properties

        /// <summary>
        /// Called when the Model changed.
        /// </summary>
        protected override void OnModelChanged()
        {
            this.CheckDataSource();
            base.OnModelChanged();

            if (this.IsInViewerMode)
            {
                this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(null);
            }

            this.PerformCheckForUpdate();
            this.SetLastUpdateTime();
            this.UndoManager.Start();
        }

        /// <summary>
        /// Called when DataSourceManager has changed.
        /// </summary>
        protected override void OnDataSourceManagerChanged()
        {
            base.OnDataSourceManagerChanged();

            this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(this.DataSourceManager);
            this.unitsAdapterHandler = new UnitsAdapterUpdateHandler(this.MeasurementUnitsAdapter);
            this.unitsAdapterHandler.PauseUndoOnUnitsAdapterUpdate(this.UndoManager);
        }

        /// <summary>
        /// Loads the data from the specified model instance.
        /// Each view-model property decorated with the ExposedModelProperty attribute is loaded from the associated Model property.
        /// </summary>
        /// <param name="model">The model instance.</param>
        /// <exception cref="InvalidOperationException">The Model property name declared by a view-model property is invalid (a property with that name does not exist on the Model).</exception>
        public override void LoadDataFromModel(CountrySetting model)
        {
            base.LoadDataFromModel(model);

            // Model cost ratios should default to zero if they are null in the model.
            if (this.OneShiftModelCostRatio.Value == null)
            {
                this.OneShiftModelCostRatio.Value = 0m;
            }

            if (this.TwoShiftsModelCostRatio.Value == null)
            {
                this.TwoShiftsModelCostRatio.Value = 0m;
            }

            if (this.ThreeShiftsModelCostRatio.Value == null)
            {
                this.ThreeShiftsModelCostRatio.Value = 0m;
            }

            // Model labor availability should default to 1 (100%) if it is null in the model.
            if (this.LaborAvailability.Value == null)
            {
                this.LaborAvailability.Value = 1m;
            }

            // Recalculate the shift model costs after all model data has been loaded
            this.CalculateAllShiftModelCosts();
        }

        /// <summary>
        /// Performs the save operation. Executed by the SaveCommand.
        /// </summary>
        protected override void Save()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Cancels all changes. Executed by the CancelCommand.
        /// </summary>
        protected override void Cancel()
        {
            base.Cancel();
            this.PerformCheckForUpdate();
        }

        /// <summary>
        /// Calculates the shift model costs for all shift models (1-shift, 2-shifts and 3-shifts models).
        /// </summary>
        private void CalculateAllShiftModelCosts()
        {
            this.OneShiftModelCost.Value = this.CalculateShiftModelCosts(this.OneShiftModelCostRatio.Value);
            this.TwoShiftsModelCost.Value = this.CalculateShiftModelCosts(this.TwoShiftsModelCostRatio.Value);
            this.ThreeShiftsModelCost.Value = this.CalculateShiftModelCosts(this.ThreeShiftsModelCostRatio.Value);
        }

        /// <summary>
        /// Calculates the costs of a shift model.
        /// </summary>
        /// <param name="shiftModelCostRatio">The shift model cost ratio.</param>
        /// <returns>The costs.</returns>
        private ShiftModelLaborCosts CalculateShiftModelCosts(decimal? shiftModelCostRatio)
        {
            decimal costRatio = 1m + shiftModelCostRatio.GetValueOrDefault();
            decimal availability = 0;

            if (this.LaborAvailability.Value.HasValue && this.LaborAvailability.Value != 0)
            {
                availability = 1 / this.LaborAvailability.Value.Value;
            }

            ShiftModelLaborCosts cost = new ShiftModelLaborCosts();
            cost.UnskilledLaborCost = this.UnskilledLaborCost.Value * costRatio * availability;
            cost.SkilledLaborCost = this.SkilledLaborCost.Value * costRatio * availability;
            cost.ForemanCost = this.ForemanCost.Value * costRatio * availability;
            cost.TechnicianCost = this.TechnicianCost.Value * costRatio * availability;
            cost.EngineerCost = this.EngineerCost.Value * costRatio * availability;

            return cost;
        }

        /// <summary>
        /// Sets the last update time as the greatest timestamp from country settings value history for the current country setting.
        /// </summary>
        public void SetLastUpdateTime()
        {
            if (!this.IsInViewerMode && DataSourceManager != null)
            {
                var setting = DataSourceManager.CountrySettingsValueHistoryRepository.GetLastModifiedByCountrySettingGuid(this.Model.Guid);
                this.LastUpdateTime.Value = (setting != null) ? setting.Timestamp : (DateTime?)null;
            }
        }

        /// <summary>
        /// Sets the last change timestamp as the current date time.
        /// </summary>
        public void SetLastChangeTimestamp()
        {
            if (this.IsChanged)
            {
                this.LastChangeTimestamp.Value = DateTime.Now;
            }
        }

        #region Country Settings Update

        /// <summary>
        /// Checks to see if an update for the country settings is available.
        /// </summary>
        private void PerformCheckForUpdate()
        {
            // check model, data context
            if (!this.CheckForUpdate
                || this.Model == null
                || this.DataSourceManager == null
                || string.IsNullOrWhiteSpace(this.SourceCountryName))
            {
                this.UpdateAvailable.Value = false;
                this.ShowUpdatedSettings.Value = false;
                return;
            }

            try
            {
                // If the cached settings are not available get the latest settings from the database.
                if (this.latestCountrySettings == null)
                {
                    var selectedCountry = this.DataSourceManager.CountryRepository.GetByName(this.SourceCountryName, true);
                    if (selectedCountry != null)
                    {
                        if (!string.IsNullOrWhiteSpace(this.SourceCountrySupplierName))
                        {
                            var selectedState = selectedCountry.States.FirstOrDefault(s => s.Name == this.SourceCountrySupplierName);
                            if (selectedState != null)
                            {
                                this.latestCountrySettings = selectedState.CountrySettings;
                                CurrencyConversionManager.ConvertObject(this.latestCountrySettings, this.MeasurementUnitsAdapter.BaseCurrency, CurrencyConversionManager.DefaultBaseCurrency);
                            }
                        }

                        if (this.latestCountrySettings == null)
                        {
                            this.latestCountrySettings = selectedCountry.CountrySetting;
                            CurrencyConversionManager.ConvertObject(this.latestCountrySettings, this.MeasurementUnitsAdapter.BaseCurrency, CurrencyConversionManager.DefaultBaseCurrency);
                        }
                    }
                }
            }
            catch (DataAccessException ex)
            {
                log.ErrorException("A data access error occurred while checking for country settings update.", ex);
            }

            if (this.latestCountrySettings != null)
            {
                // To determine if an update exists compare the timestamp of the current and latest settings.
                if (this.Model.LastChangeTimestamp == this.latestCountrySettings.LastChangeTimestamp)
                {
                    // No update available.
                    this.UpdateAvailable.Value = false;
                    this.ShowUpdatedSettings.Value = false;
                }
                else
                {
                    // Load the updated country settings values into the UI.
                    this.UpdatedAirCost.Value = this.latestCountrySettings.AirCost;
                    this.UpdatedEnergyCost.Value = this.latestCountrySettings.EnergyCost;
                    this.UpdatedEngineerCost.Value = this.latestCountrySettings.EngineerCost;
                    this.UpdatedForemanCost.Value = this.latestCountrySettings.ForemanCost;
                    this.UpdatedInterestRate.Value = this.latestCountrySettings.InterestRate;
                    this.UpdatedLaborAvailability.Value = this.latestCountrySettings.LaborAvailability;
                    this.UpdatedOfficeAreaRentalCost.Value = this.latestCountrySettings.OfficeAreaRentalCost;
                    this.UpdatedProductionAreaRentalCost.Value = this.latestCountrySettings.ProductionAreaRentalCost;
                    this.UpdatedSkilledLaborCost.Value = this.latestCountrySettings.SkilledLaborCost;
                    this.UpdatedTechnicianCost.Value = this.latestCountrySettings.TechnicianCost;
                    this.UpdatedUnskilledLaborCost.Value = this.latestCountrySettings.UnskilledLaborCost;
                    this.UpdatedWaterCost.Value = this.latestCountrySettings.WaterCost;
                    this.UpdatedOneShiftModelCostRatio.Value = this.latestCountrySettings.ShiftCharge1ShiftModel;
                    this.UpdatedTwoShiftsModelCostRatio.Value = this.latestCountrySettings.ShiftCharge2ShiftModel;
                    this.UpdatedThreeShiftsModelCostRatio.Value = this.latestCountrySettings.ShiftCharge3ShiftModel;

                    // Trigger the display of the update availability messages.
                    this.UpdateAvailable.Value = true;
                }
            }
            else
            {
                log.Error("The updated country settings obtained from the database were null.");
            }
        }

        /// <summary>
        /// Determines whether the ApplyUpdatedCountrySettingsCommand command can be executed.
        /// </summary>
        /// <returns>
        /// true if the command can execute; otherwise, false.
        /// </returns>
        private bool CanApplyUpdate()
        {
            return this.UpdateAvailable.Value
                && (this.AllowToApplyUpdateWhenReadOnly || !this.IsReadOnly)
                && this.latestCountrySettings != null;
        }

        /// <summary>
        /// Applies the country settings update, if available.
        /// </summary>
        private void ApplyUpdate()
        {
            if (this.latestCountrySettings == null)
            {
                return;
            }

            using (this.UndoManager.StartBatch(navigateToBatchControls: true))
            {
                this.LoadDataFromModel(this.latestCountrySettings);
                this.ShowUpdatedSettings.Value = false;
                this.UpdateAvailable.Value = false;
            }

            this.IsChanged = true;
        }

        #endregion Country Settings Update

        #region Inner classes

        /// <summary>
        /// Contains the labor costs for a shift model.
        /// </summary>
        public struct ShiftModelLaborCosts
        {
            /// <summary>
            /// Gets or sets the unskilled labor cost.
            /// </summary>            
            public decimal? UnskilledLaborCost { get; set; }

            /// <summary>
            /// Gets or sets the skilled labor cost.
            /// </summary>            
            public decimal? SkilledLaborCost { get; set; }

            /// <summary>
            /// Gets or sets the foreman cost.
            /// </summary>            
            public decimal? ForemanCost { get; set; }

            /// <summary>
            /// Gets or sets the technician cost.
            /// </summary>            
            public decimal? TechnicianCost { get; set; }

            /// <summary>
            /// Gets or sets the engineer cost.
            /// </summary>            
            public decimal? EngineerCost { get; set; }
        }

        #endregion Inner classes
    }
}
