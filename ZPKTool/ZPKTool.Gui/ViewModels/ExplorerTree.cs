﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Represents the types of the explorer tree views.
    /// </summary>
    public enum ExplorerTree
    {
        /// <summary>
        /// No tree.
        /// </summary>
        None = 0,

        /// <summary>
        /// The item represents a Projects tree.
        /// </summary>
        Projects = 1,

        /// <summary>
        /// The item represents a Bookmarks tree.
        /// </summary>
        Bookmarks = 2
    }
}
