﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using ZPKTool.Business;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Views;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for the <see cref="ManageProjectsView"/>.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ManageProjectsViewModel : ViewModel
    {
        /// <summary>
        /// The delay to display the loading screen when getting the projects and users.
        /// </summary>
        private const int DisplayDelay = 1000;

        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The data manager to work with.
        /// </summary>
        private readonly IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The PleaseWaitService reference.
        /// </summary>
        private IPleaseWaitService pleaseWaitService;

        /// <summary>
        /// The selected user.
        /// </summary>
        private User selectedUser;

        /// <summary>
        /// Contains the user to select after the save operation if needed.
        /// </summary>
        private User userToSelectAfterSave;

        #endregion Attributes

        /// <summary>
        /// Initializes a new instance of the <see cref="ManageProjectsViewModel"/> class.
        /// </summary>
        /// <param name="messenger">The messenger.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="pleaseWaitService">The please wait service.</param>
        [ImportingConstructor]
        public ManageProjectsViewModel(
            IMessenger messenger,
            IWindowService windowService,
            IPleaseWaitService pleaseWaitService)
        {
            this.messenger = messenger;
            this.windowService = windowService;
            this.pleaseWaitService = pleaseWaitService;

            Users.Value = new List<User>();
            Projects.Value = new ObservableCollection<ProjectOwnerWrapper>();

            messenger.Register<SynchronizationFinishedMessage>(this.HandleSynchronizationFinishedMessage);
        }

        #region Properties

        /// <summary>
        /// Gets the projects and owners.
        /// </summary>
        public VMProperty<ObservableCollection<ProjectOwnerWrapper>> Projects { get; private set; }

        /// <summary>
        /// Gets or sets the selected user.
        /// </summary>
        public User SelectedUser
        {
            get
            {
                return selectedUser;
            }

            set
            {
                userToSelectAfterSave = value;
                var areUnsavedChanges = SolveUnsavedChanges(LocalizedResources.Question_UnsavedDataOnSelectedUserChange, MessageDialogType.YesNo);

                if (!areUnsavedChanges)
                {
                    SetSelectedUser(value);
                }
            }
        }

        /// <summary>
        /// Gets or sets the window service.
        /// </summary>
        public IWindowService WindowService
        {
            get
            {
                return this.windowService;
            }

            set
            {
                if (this.windowService != value)
                {
                    this.windowService = value;
                    OnPropertyChanged(() => this.WindowService);
                }
            }
        }

        #endregion Properties

        /// <summary>
        /// Gets the users.
        /// </summary>
        public VMProperty<IEnumerable<User>> Users { get; private set; }

        /// <summary>
        /// Cancels all changes. Executed by the CancelCommand.
        /// </summary>
        protected override void Cancel()
        {
            base.Cancel();

            foreach (var wrapper in Projects.Value)
            {
                wrapper.OwnerId = wrapper.Project.Owner.Guid;
            }
        }

        /// <summary>
        /// Performs the save operation. Executed by the SaveCommand.
        /// </summary>
        protected override void Save()
        {
            base.Save();

            Action<PleaseWaitService.WorkParams> work = (workParams) =>
            {
                foreach (var wrapper in Projects.Value)
                {
                    if (wrapper.OwnerId != wrapper.Project.Owner.Guid)
                    {
                        var fullProject = dataManager.ProjectRepository.GetProjectFull(wrapper.Project.Guid);
                        CloneManager cloneManager = new CloneManager(dataManager, dataManager);
                        var clonedProject = cloneManager.Clone(fullProject);

                        clonedProject.SetOwner(Users.Value.FirstOrDefault(user => user.Guid == wrapper.OwnerId));
                        dataManager.ProjectRepository.Save(clonedProject);

                        this.dataManager.ProjectRepository.DeleteByStoredProcedure(fullProject);
                    }
                }

                dataManager.SaveChanges();
            };

            Action<PleaseWaitService.WorkParams> workCompleted = (workParams) =>
            {
                if (workParams.Error != null)
                {
                    this.windowService.MessageDialogService.Show(workParams.Error);
                }
                else
                {
                    for (var projectIndex = Projects.Value.Count - 1; projectIndex >= 0; projectIndex--)
                    {
                        if (Projects.Value[projectIndex].OwnerId != SelectedUser.Guid)
                        {
                            Projects.Value.Remove(Projects.Value[projectIndex]);
                        }
                    }

                    if (userToSelectAfterSave != null)
                    {
                        SetSelectedUser(userToSelectAfterSave);
                        userToSelectAfterSave = null;
                    }
                }
            };

            this.pleaseWaitService.Show(LocalizedResources.General_WaitAssignProjects, work, workCompleted);

            this.messenger.Send(new NotificationMessage(Notification.ProjectOwnerChanged));
        }

        /// <summary>
        /// Called before unloading the view from its parent or closing it. Returning false will cancel the view's unloading or closing.
        /// </summary>
        /// <returns>
        /// True if the unloading process should continue and false if it should be canceled.
        /// </returns>
        public override bool OnUnloading()
        {
            bool canUnload = base.OnUnloading();
            var areUnsavedChanges = SolveUnsavedChanges(LocalizedResources.Question_UnsavedDataOnQuit, MessageDialogType.YesNoCancel);

            return canUnload && !areUnsavedChanges;
        }

        /// <summary>
        /// Sets the selected user.
        /// </summary>
        /// <param name="userToSet">The user to set.</param>
        private void SetSelectedUser(User userToSet)
        {
            if (this.selectedUser != userToSet)
            {
                this.selectedUser = userToSet;
                this.PopulateProjects();
                OnPropertyChanged("SelectedUser");
            }
        }

        /// <summary>
        /// Populates the projects.
        /// </summary>
        private void PopulateProjects()
        {
            Projects.Value.Clear();

            if (SelectedUser != null)
            {
                IEnumerable<Project> projects = null;

                Action<PleaseWaitService.WorkParams> work = (workParams) =>
                {
                    projects = dataManager.ProjectRepository.GetOtherUserProjects(SelectedUser.Guid, Guid.Empty);
                };

                Action<PleaseWaitService.WorkParams> workCompleted = (workParams) =>
                {
                    if (workParams.Error != null)
                    {
                        windowService.MessageDialogService.Show(workParams.Error);
                    }
                    else
                    {
                        Projects.Value = GetProjectWrappers(projects);
                    }
                };

                this.pleaseWaitService.Show(LocalizedResources.General_LoadingProjects, DisplayDelay, work, workCompleted);
            }
        }

        /// <summary>
        /// Solves the unsaved changes.
        /// </summary>
        /// <param name="question">The question.</param>
        /// <param name="messageType">The message dialog type.</param>
        /// <returns>
        /// A value indicating whether there are unsaved changes or not.
        /// </returns>
        private bool SolveUnsavedChanges(string question, MessageDialogType messageType)
        {
            var areUnsavedChanges = false;

            if (Projects.Value != null)
            {
                areUnsavedChanges = Projects.Value.Any(wrapper => wrapper.OwnerId != wrapper.Project.Owner.Guid);
            }

            if (areUnsavedChanges)
            {
                var dialogResult = windowService.MessageDialogService.Show(question, messageType);
                if (dialogResult == MessageDialogResult.Yes)
                {
                    Save();
                }
                else if (dialogResult == MessageDialogResult.No)
                {
                    areUnsavedChanges = false;
                }
            }

            return areUnsavedChanges;
        }

        /// <summary>
        /// Method called after the view is loaded
        /// </summary>
        public override void OnLoaded()
        {
            IEnumerable<User> users = null;

            Action<PleaseWaitService.WorkParams> work = (workParams) =>
            {
                users = this.dataManager.UserRepository.GetAll(false);
            };

            Action<PleaseWaitService.WorkParams> workCompleted = (workParams) =>
            {
                if (workParams.Error != null)
                {
                    Users.Value = new List<User>();
                    windowService.MessageDialogService.Show(workParams.Error);
                }
                else
                {
                    // Add the loaded data to the data source collection for the view.
                    Users.Value = users.OrderBy(user => user.Username);
                    SelectedUser = Users.Value.FirstOrDefault();
                }
            };

            this.pleaseWaitService.Show(LocalizedResources.General_LoadingUsers, DisplayDelay, work, workCompleted);
        }

        /// <summary>
        /// Gets the project wrappers.
        /// </summary>
        /// <param name="projects">The projects.</param>
        /// <returns>The collection of project wrappers.</returns>
        private ObservableCollection<ProjectOwnerWrapper> GetProjectWrappers(IEnumerable<Project> projects)
        {
            ObservableCollection<ProjectOwnerWrapper> wrappers = new ObservableCollection<ProjectOwnerWrapper>();

            if (projects != null)
            {
                projects = projects.OrderBy(project => project.Name);
                foreach (var project in projects)
                {
                    wrappers.Add(new ProjectOwnerWrapper(project));
                }
            }

            return wrappers;
        }

        /// <summary>
        /// Handles the SynchronizationFinished message.
        /// </summary>
        /// <param name="message">The message.</param>
        private void HandleSynchronizationFinishedMessage(SynchronizationFinishedMessage message)
        {
            this.Dispatcher.BeginInvoke(this.PopulateProjects);
        }

        #region Nested classes

        /// <summary>
        /// Wraps the projects and their owners to edit the owner of projects in the gui without having to modify the model directly.
        /// </summary>
        public class ProjectOwnerWrapper : INotifyPropertyChanged
        {
            /// <summary>
            /// The id of the owner of the project
            /// </summary>
            private Guid ownerId;

            /// <summary>
            /// The wrapped project
            /// </summary>
            private Project project;

            /// <summary>
            /// Initializes a new instance of the <see cref="ProjectOwnerWrapper"/> class.
            /// </summary>
            /// <param name="soureProject">The soure project.</param>
            public ProjectOwnerWrapper(Project soureProject)
            {
                OwnerId = soureProject.Owner.Guid;
                Project = soureProject;
            }

            /// <summary>
            /// Occurs when a property value changes.
            /// </summary>
            public event PropertyChangedEventHandler PropertyChanged;

            /// <summary>
            /// Gets or sets the owner id.
            /// </summary>
            /// <value>
            /// The owner id.
            /// </value>
            public Guid OwnerId
            {
                get
                {
                    return ownerId;
                }

                set
                {
                    if (ownerId != value)
                    {
                        ownerId = value;
                        RaisePropertyChanged("OwnerId");
                    }
                }
            }

            /// <summary>
            /// Gets or sets the project.
            /// </summary>
            /// <value>
            /// The project.
            /// </value>
            public Project Project
            {
                get
                {
                    return project;
                }

                set
                {
                    if (project != value)
                    {
                        project = value;
                        RaisePropertyChanged("Project");
                    }
                }
            }

            /// <summary>
            /// Raises the property changed.
            /// </summary>
            /// <param name="propertyName">Name of the property.</param>
            private void RaisePropertyChanged(string propertyName)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
            }
        }

        #endregion Nested classes
    }
}