﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Windows.Input;
using ZPKTool.Business;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the TransportCostCalculator view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class TransportCostCalculatorViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The composition container.
        /// </summary>
        private CompositionContainer compositionContainer;

        /// <summary>
        /// The PleaseWaitService reference.
        /// </summary>
        private IPleaseWaitService pleaseWaitService;

        /// <summary>
        /// The name of the parent part for which the route is calculated.
        /// </summary>
        private string parentName;

        /// <summary>
        /// The number of the parent part for which the route is calculated.
        /// </summary>
        private string parentNumber;

        /// <summary>
        /// The supplier of the parent part for which the route is calculated.
        /// </summary>
        private string parentSupplier;

        /// <summary>
        /// The weight of the parent part for which the route is calculated.
        /// </summary>
        private decimal? parentWeight;

        /// <summary>
        /// The country of the parent part for which the route is calculated.
        /// </summary>
        private string parentCountry;

        /// <summary>
        /// The country's guid of the parent part for which the route is calculated.
        /// </summary>
        private Guid? parentCountryId;

        /// <summary>
        /// The parent part (entity) for which the route is calculated.
        /// </summary>
        private object parentEntity;

        /// <summary>
        /// The calculation items (routes).
        /// </summary>
        private ObservableCollection<TransportCostCalculationViewModel> calculationItems;

        /// <summary>
        /// The view model items (the items source for the calculator's tab control).
        /// </summary>
        private ObservableCollection<ViewModel> viewModelItems;

        /// <summary>
        /// The deleted calculations (routes).
        /// </summary>
        private List<TransportCostCalculationViewModel> deletedCalculations;

        /// <summary>
        /// The initial calculations (routes).
        /// </summary>
        private ObservableCollection<TransportCostCalculationViewModel> initialCalculations;

        /// <summary>
        /// The selected view model (the selected tab from the calculator's tab control).
        /// </summary>
        private ViewModel selectedViewModelItem;

        /// <summary>
        /// The latest transport cost calculation setting from the database of the current user.
        /// </summary>
        private TransportCostCalculationSetting latestSettingFromRepository;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        /// <summary>
        /// The data source manager.
        /// </summary>
        private IDataSourceManager dataSourceManager;

        /// <summary>
        /// The UnitsAdapter handler, used to perform certain operations when some UnitsAdapter properties are updated.
        /// </summary>
        private UnitsAdapterUpdateHandler unitsAdapterHandler;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="TransportCostCalculatorViewModel"/> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        /// <param name="messenger">The messenger.</param>
        /// <param name="unitsService">The units service.</param>
        /// <param name="compositionContainer">The composition container.</param>
        /// <param name="pleaseWaitService">The please wait service.</param>
        [ImportingConstructor]
        public TransportCostCalculatorViewModel(
            IWindowService windowService,
            IMessenger messenger,
            IUnitsService unitsService,
            CompositionContainer compositionContainer,
            IPleaseWaitService pleaseWaitService)
        {
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("unitsService", unitsService);
            Argument.IsNotNull("compositionContainer", compositionContainer);
            Argument.IsNotNull("pleaseWaitService", pleaseWaitService);

            this.windowService = windowService;
            this.messenger = messenger;
            this.unitsService = unitsService;
            this.compositionContainer = compositionContainer;
            this.pleaseWaitService = pleaseWaitService;

            this.InitializeUndoManager();

            using (this.UndoManager.Pause())
            {
                // Initialize the collections and lists.
                this.CalculationItems = new ObservableCollection<TransportCostCalculationViewModel>();
                this.ViewModelItems = new ObservableCollection<ViewModel>();
                this.deletedCalculations = new List<TransportCostCalculationViewModel>();
                this.initialCalculations = new ObservableCollection<TransportCostCalculationViewModel>();

                this.DeleteCalculationCommand = new DelegateCommand<ViewModel>(this.DeleteCalculation, this.CanDeleteCalculation);

                // Create the summary view model.
                this.SummaryViewModel = new TransportCostCalculatorSummaryViewModel(this.CalculationItems);
                this.SummaryViewModel.AddCalculationCommand = new DelegateCommand(this.AddNewCalculation);
                this.SummaryViewModel.DeleteCalculationCommand = this.DeleteCalculationCommand;
                this.ViewModelItems.Add(this.SummaryViewModel);
            }

            this.messenger.Register<NotificationMessage>(this.HandleNotificationMessage);
            this.SaveAllCommand = new CompositeCommand();
            this.ShowCancelMessageFlag = true;
        }

        #region Properties

        /// <summary>
        /// Gets or sets the data source manager.
        /// </summary>
        public IDataSourceManager DataSourceManager
        {
            get
            {
                return this.dataSourceManager;
            }

            set
            {
                if (this.dataSourceManager != value)
                {
                    this.dataSourceManager = value;
                }

                this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(value);
                this.SummaryViewModel.MeasurementUnitsAdapter = this.MeasurementUnitsAdapter;
                this.unitsAdapterHandler = new UnitsAdapterUpdateHandler(this.MeasurementUnitsAdapter);
                this.unitsAdapterHandler.PauseUndoOnUnitsAdapterUpdate(this.UndoManager);
            }
        }

        /// <summary>
        /// Gets or sets the parent part (entity) for which the route is calculated.
        /// </summary>
        public object ParentEntity
        {
            get { return this.parentEntity; }
            set { this.SetProperty(ref this.parentEntity, value, () => this.ParentEntity, this.OnParentEntityChanged); }
        }

        /// <summary>
        /// Gets or sets the calculation items (routes).
        /// </summary>
        [UndoableProperty]
        public ObservableCollection<TransportCostCalculationViewModel> CalculationItems
        {
            get { return this.calculationItems; }
            set { this.SetProperty(ref this.calculationItems, value, () => this.CalculationItems); }
        }

        /// <summary>
        /// Gets or sets the view model items (the items source for the calculator's tab control).
        /// This collection contains the summary view model and the view models for each calculation.
        /// </summary>
        public ObservableCollection<ViewModel> ViewModelItems
        {
            get { return this.viewModelItems; }
            set { this.SetProperty(ref this.viewModelItems, value, () => this.ViewModelItems); }
        }

        /// <summary>
        /// Gets or sets the selected view model (the selected tab from the calculator's tab control).
        /// </summary>
        public ViewModel SelectedViewModelItem
        {
            get { return this.selectedViewModelItem; }
            set { this.SetProperty(ref this.selectedViewModelItem, value, () => this.SelectedViewModelItem); }
        }

        /// <summary>
        /// Gets the transport cost calculator summary view model.
        /// </summary>
        public TransportCostCalculatorSummaryViewModel SummaryViewModel { get; private set; }

        /// <summary>
        /// Gets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the cancel message will appear or not.
        /// </summary>
        public bool ShowCancelMessageFlag { get; private set; }

        #endregion

        #region Commands

        /// <summary>
        /// Gets or sets the command that saves all changes.
        /// </summary>        
        public CompositeCommand SaveAllCommand { get; set; }

        /// <summary>
        /// Gets or sets the command that deletes the selected calculation route from transport calculator.
        /// </summary>
        public ICommand DeleteCalculationCommand { get; set; }

        #endregion

        /// <summary>
        /// Initialize the undo manager.
        /// </summary>
        private void InitializeUndoManager()
        {
            this.UndoManager.RegisterPropertyAction(() => this.CalculationItems, this.HandleUndoOnCollection, this, true);
            this.UndoManager.Start();
        }

        /// <summary>
        /// Handles the parent entity changed event.
        /// </summary>
        private void OnParentEntityChanged()
        {
            using (this.UndoManager.Pause())
            {
                Assembly parentAssembly = this.ParentEntity as Assembly;
                if (parentAssembly != null)
                {
                    // Set the default values for a new calculation.
                    this.parentName = parentAssembly.Name;
                    this.parentNumber = parentAssembly.Number;
                    this.parentSupplier = parentAssembly.AssemblingSupplier;
                    this.parentCountry = parentAssembly.AssemblingCountry;
                    this.parentCountryId = parentAssembly.AssemblingCountryId;

                    foreach (var calculation in parentAssembly.TransportCostCalculations.OrderBy(i => i.Index))
                    {
                        // Add each calculation from the parent (assembly) into the calculations list.
                        this.AddCalculationIntoList(calculation);
                    }
                }
                else
                {
                    Part parentPart = this.ParentEntity as Part;
                    if (parentPart != null)
                    {
                        // Set the default values for a new calculation.
                        this.parentName = parentPart.Name;
                        this.parentNumber = parentPart.Number;
                        this.parentSupplier = parentPart.ManufacturingSupplier;
                        this.parentWeight = parentPart.RawMaterials.Sum(it => it.ParentWeight);
                        this.parentCountry = parentPart.ManufacturingCountry;
                        this.parentCountryId = parentPart.ManufacturingCountryId;

                        foreach (var calculation in parentPart.TransportCostCalculations.OrderBy(i => i.Index))
                        {
                            // Add each calculation from the parent (part) into the calculations list.
                            this.AddCalculationIntoList(calculation);
                        }
                    }
                }

                // Copy the original calculation routes into a list.
                this.initialCalculations.AddRange(this.CalculationItems);

                this.SelectedViewModelItem = this.ViewModelItems.FirstOrDefault();
                this.UpdateTotalCosts();

                foreach (var item in this.CalculationItems)
                {
                    item.IsChanged = false;
                }

                this.IsChanged = false;
            }
        }

        /// <summary>
        /// Adds a new calculation (route). Executed by the AddCalculationCommand from summary view model.
        /// </summary>
        private void AddNewCalculation()
        {
            TransportCostCalculation calculation = new TransportCostCalculation();
            TransportCostCalculationSetting currentSetting = null;
            Collection<TransportCostCalculationSetting> settings = new Collection<TransportCostCalculationSetting>();

            if (this.CalculationItems.Count > 0)
            {
                // If there are items into the list, add all their settings into a list.
                foreach (var item in this.CalculationItems)
                {
                    settings.Add(item.Model.TransportCostCalculationSetting);
                }
            }

            if (this.latestSettingFromRepository == null)
            {
                // Get the latest calculation setting saved into database.
                this.latestSettingFromRepository = this.DataSourceManager.TransportCostCalculationSettingRepository.GetLatestSetting(SecurityManager.Instance.CurrentUser.Guid);
            }

            var settingFromRepositoryId = Guid.Empty;
            if (this.latestSettingFromRepository != null)
            {
                // Add the latest setting into the list.
                settings.Add(this.latestSettingFromRepository);
                settingFromRepositoryId = this.latestSettingFromRepository.Guid;
            }

            if (settings.Count > 0)
            {
                // Order descending all the settings, to get the last saved setting and clone it.
                currentSetting = settings.OrderByDescending(i => i.Timestamp).FirstOrDefault();

                CloneManager cloneManager = new CloneManager();
                calculation.TransportCostCalculationSetting = cloneManager.Clone(currentSetting);

                if (Guid.Equals(currentSetting.Guid, settingFromRepositoryId))
                {
                    // If the setting used for the calculation is the one got from repository, it's currency values have to be converted
                    // from the parent project currency exchange rate into the current project currency exchange rate.
                    var parentProjectId = this.DataSourceManager.TransportCostCalculationSettingRepository.GetParentProjectId(currentSetting);
                    var oldProjectBaseCurrency = this.DataSourceManager.CurrencyRepository.GetProjectBaseCurrency(parentProjectId);
                    CurrencyConversionManager.ConvertObject(calculation.TransportCostCalculationSetting, this.MeasurementUnitsAdapter.BaseCurrency, oldProjectBaseCurrency);
                }
            }
            else
            {
                // If there are no settings into the list and repository, create a new one.
                currentSetting = new TransportCostCalculationSetting();
                currentSetting.DaysPerYear = 245;
                currentSetting.AverageLoadTarget = 0.25m;
                currentSetting.VanTypeCost = 0.7m;
                currentSetting.TruckTrailerCost = 0.5m;
                currentSetting.MegaTrailerCost = 0.5m;
                currentSetting.Truck7tCost = 0.5m;
                currentSetting.Truck3_5tCost = 0.5m;

                calculation.TransportCostCalculationSetting = currentSetting;
                CurrencyConversionManager.ConvertObject(calculation.TransportCostCalculationSetting, this.MeasurementUnitsAdapter.BaseCurrency, CurrencyConversionManager.DefaultBaseCurrency);
            }

            this.AddCalculationIntoList(calculation);
        }

        /// <summary>
        /// Adds a calculation into the calculations list and makes the settings for it.
        /// </summary>
        /// <param name="item">The calculation to add.</param>
        private void AddCalculationIntoList(TransportCostCalculation item)
        {
            TransportCostCalculationViewModel calculationVM = new TransportCostCalculationViewModel(windowService, messenger, unitsService, compositionContainer, pleaseWaitService);
            this.SaveAllCommand.RegisterCommand(calculationVM.SaveCommand);

            calculationVM.IsReadOnly = this.IsReadOnly;
            calculationVM.IsInViewerMode = this.IsInViewerMode;
            calculationVM.DataSourceManager = this.DataSourceManager;
            calculationVM.UndoManager = this.UndoManager;

            this.UndoManager.RegisterInstanceAction(this.HandleUndo, calculationVM);

            this.CalculationItems.Add(calculationVM);

            using (this.UndoManager.Pause())
            {
                calculationVM.Model = item;
                if (calculationVM.Index.Value == null)
                {
                    // If we are in the process of adding a new calculation item, some default values must be set.
                    calculationVM.Index.Value = this.CalculationItems.Count;
                    calculationVM.PartName.Value = this.parentName;
                    calculationVM.PartNumber.Value = this.parentNumber;
                    calculationVM.Supplier.Value = this.parentSupplier;
                    calculationVM.PartWeight.Value = this.parentWeight;
                }

                // Country and country guid are necessary for the supplier browser.
                calculationVM.PartCountry.Value = this.parentCountry;
                calculationVM.PartCountryId.Value = this.parentCountryId;

                // The selected item from the summary' data grid is the new added one.
                this.SummaryViewModel.SelectedCalculationItem = calculationVM;

                // Add the item into the view models' list.
                this.ViewModelItems.Add(calculationVM);

                // Select the tab containing the new added calculation.
                this.SelectedViewModelItem = calculationVM;
            }
        }

        /// <summary>
        /// Determines whether the selected calculation can be deleted or not.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>
        /// True if the calculation can be deleted; otherwise, false.
        /// </returns>
        private bool CanDeleteCalculation(ViewModel item)
        {
            if (item != null && item is TransportCostCalculationViewModel)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Deletes a calculation (route) from calculator. Executed by the DeleteCalculationCommand from summary view model.
        /// </summary>
        /// <param name="viewModel">The view model containing the item to delete.</param>
        private void DeleteCalculation(ViewModel viewModel)
        {
            TransportCostCalculationViewModel calculationViewModel = viewModel as TransportCostCalculationViewModel;
            if (calculationViewModel != null)
            {
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_DeleteSelectedObjects, MessageDialogType.YesNo);
                if (result == MessageDialogResult.No)
                {
                    return;
                }

                this.CalculationItems.Remove(calculationViewModel);
                this.ViewModelItems.Remove(calculationViewModel);
                this.deletedCalculations.Add(calculationViewModel);

                this.SaveAllCommand.UnregisterCommand(calculationViewModel.SaveCommand);

                using (this.UndoManager.Pause())
                {
                    foreach (var calculation in this.CalculationItems)
                    {
                        if (calculation.Index.Value > calculationViewModel.Index.Value)
                        {
                            // Decrease the index of the items with a greater index than the deleted one.
                            calculation.Index.Value -= 1;
                        }
                    }
                }

                this.SelectCalculation(calculationViewModel);
                this.UpdateTotalCosts();
            }
        }

        /// <summary>
        /// Sets the selected calculation according to the position of the deleted item.
        /// </summary>
        /// <param name="calculationViewModel">The deleted calculation view model.</param>
        private void SelectCalculation(TransportCostCalculationViewModel calculationViewModel)
        {
            var index = calculationViewModel.Index.Value.HasValue ? calculationViewModel.Index.Value.Value : 0;
            if (this.CalculationItems.Count == 1)
            {
                this.SummaryViewModel.SelectedCalculationItem = this.CalculationItems[0];
            }
            else if (index < this.CalculationItems.Count)
            {
                this.SummaryViewModel.SelectedCalculationItem = this.CalculationItems[index - 1];
            }
            else if (index >= this.CalculationItems.Count && this.CalculationItems.Count != 0)
            {
                this.SummaryViewModel.SelectedCalculationItem = this.CalculationItems.Last();
            }
            else
            {
                this.SummaryViewModel.SelectedCalculationItem = null;
            }
        }

        /// <summary>
        /// Handles the notification message.
        /// </summary>
        /// <param name="message">The message.</param>
        private void HandleNotificationMessage(NotificationMessage message)
        {
            if (message.Notification == "UpdateTotalCosts")
            {
                this.UpdateTotalCosts();
            }
        }

        /// <summary>
        /// Updates the total costs for all the routes.
        /// </summary>
        private void UpdateTotalCosts()
        {
            this.SummaryViewModel.TotalPackagingCost.Value = this.CalculationItems.Sum(i => i.PackagingCost.Value);
            this.SummaryViewModel.TotalTransportCost.Value = this.CalculationItems.Sum(i => i.TransportCost.Value);
            this.SummaryViewModel.TotalPackagingCostPerPcs = this.CalculationItems.Sum(i => i.PackagingCostPerPcs.Value);
            this.SummaryViewModel.TotalTransportCostPerPcs = this.CalculationItems.Sum(i => i.TransportCostPerPcs.Value);
        }

        /// <summary>
        /// Determines whether this instance can perform the save operation. Executed by the SaveCommand.
        /// The default implementation allows the save to be performed if the input is valid.
        /// </summary>
        /// <returns>
        /// True if the save operation can be performed, false otherwise.
        /// </returns>
        protected override bool CanSave()
        {
            return base.CanSave()
                && this.SaveAllCommand.CanExecute(null)
                && string.IsNullOrWhiteSpace(this.SummaryViewModel.Error);
        }

        /// <summary>
        /// Performs the save operation. Executed by the SaveCommand.
        /// </summary>
        protected override void Save()
        {
            foreach (TransportCostCalculationViewModel itemToSave in this.CalculationItems)
            {
                var calculation = itemToSave.Model;
                if (calculation != null)
                {
                    Assembly parentAssembly = this.ParentEntity as Assembly;
                    if (parentAssembly != null && !parentAssembly.TransportCostCalculations.Contains(calculation))
                    {
                        // If the calculation is not attached to the assembly, attach it and set it's owner.
                        calculation.TransportCostCalculationSetting.SetOwner(parentAssembly.Owner);
                        calculation.SetOwner(parentAssembly.Owner);
                        parentAssembly.TransportCostCalculations.Add(calculation);
                    }
                    else
                    {
                        Part parentPart = this.ParentEntity as Part;
                        if (parentPart != null && !parentPart.TransportCostCalculations.Contains(calculation))
                        {
                            // If the calculation is not attached to the part, attach it and set it's owner.
                            calculation.TransportCostCalculationSetting.SetOwner(parentPart.Owner);
                            calculation.SetOwner(parentPart.Owner);
                            parentPart.TransportCostCalculations.Add(calculation);
                        }
                    }
                }
            }

            foreach (TransportCostCalculationViewModel itemToDelete in this.deletedCalculations)
            {
                // Delete the calculation from the repository.
                var deletedCalculation = itemToDelete.Model;
                this.DataSourceManager.TransportCostCalculationRepository.RemoveAll(deletedCalculation);
            }

            this.SaveAllCommand.Execute(null);
            this.DataSourceManager.SaveChanges();
            this.ShowCancelMessageFlag = false;

            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Cancels all changes. Executed by the CancelCommand.
        /// </summary>
        protected override void Cancel()
        {
            var anyItemChanged = this.CalculationItems.Any(i => i.IsChanged);
            if (this.IsChanged || anyItemChanged || this.deletedCalculations.Count > 0)
            {
                // If there are changes made, ask the user to confirm canceling.
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_CancelChanges, MessageDialogType.YesNo);
                if (result == MessageDialogResult.No)
                {
                    // Don't cancel the changes and also don't close the view-model.
                    return;
                }

                // If the changes are canceled, the calculations list will contain the initial calculations.
                this.CalculationItems = this.initialCalculations;

                // Refresh the total costs for the current calculations.
                this.UpdateTotalCosts();
            }

            base.Cancel();
            this.deletedCalculations.Clear();
            this.IsChanged = false;
            this.ShowCancelMessageFlag = false;

            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Called before unloading the view from its parent. Returning false will cancel the view's unloading.
        /// </summary>
        /// <returns>
        /// True if the unloading process should continue and false if it should be canceled.
        /// </returns>
        public override bool OnUnloading()
        {
            if (this.ShowCancelMessageFlag)
            {
                var anyItemChanged = this.CalculationItems.Any(it => it.IsChanged);
                if (this.IsChanged && (anyItemChanged || this.deletedCalculations.Count > 0))
                {
                    // If there are changes made, ask the user to confirm quitting.
                    var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_Quit, MessageDialogType.YesNo);
                    if (result == MessageDialogResult.No)
                    {
                        // Don't quit.
                        return false;
                    }
                    else
                    {
                        // If the changes are canceled, the calculations list will contain the initial calculations.
                        this.CalculationItems = this.initialCalculations;

                        // Refresh the total costs for the current calculations.
                        this.UpdateTotalCosts();
                    }
                }
            }

            this.messenger.Unregister(this);
            return true;
        }

        #region UndoManager handling

        /// <summary>
        /// Handle the Undo on CalculationViewModelItems.
        /// </summary>
        /// <param name="undoItem">The undoable item.</param>
        private void HandleUndoOnCollection(UndoableItem undoItem)
        {
            if (undoItem == null)
            {
                return;
            }

            switch (undoItem.ActionType)
            {
                case UndoActionType.Insert:
                    {
                        foreach (var item in undoItem.Values)
                        {
                            var itemToAdd = item as TransportCostCalculationViewModel;
                            if (itemToAdd != null)
                            {
                                using (this.UndoManager.Pause())
                                {
                                    foreach (var calculationItem in this.CalculationItems)
                                    {
                                        if (calculationItem.Index.Value >= itemToAdd.Index.Value)
                                        {
                                            calculationItem.Index.Value++;
                                        }
                                    }
                                }

                                this.InsertItemIntoCollections(itemToAdd);

                                this.SaveAllCommand.RegisterCommand(itemToAdd.SaveCommand);
                                this.SelectedViewModelItem = itemToAdd;
                                this.SummaryViewModel.SelectedCalculationItem = itemToAdd;
                            }

                            if (deletedCalculations.Contains(itemToAdd))
                            {
                                this.deletedCalculations.Remove(itemToAdd);
                            }
                        }

                        break;
                    }

                case UndoActionType.Delete:
                    {
                        foreach (var item in undoItem.Values)
                        {
                            var itemToDelete = item as TransportCostCalculationViewModel;
                            if (itemToDelete != null)
                            {
                                this.SaveAllCommand.UnregisterCommand(itemToDelete.SaveCommand);
                                this.CalculationItems.Remove(itemToDelete);
                                this.ViewModelItems.Remove(itemToDelete);

                                using (this.UndoManager.Pause())
                                {
                                    foreach (var calculation in this.CalculationItems)
                                    {
                                        if (calculation.Index.Value > itemToDelete.Index.Value)
                                        {
                                            // Decrease the index of the items with a greater index than the deleted one.
                                            calculation.Index.Value -= 1;
                                        }
                                    }
                                }

                                this.SelectCalculation(itemToDelete);
                            }
                        }

                        break;
                    }
            }

            this.UpdateTotalCosts();
        }

        /// <summary>
        /// Handles undo when items are changed in different calculation tabs.
        /// </summary>
        /// <param name="undoItem">The undo item.</param>
        private void HandleUndo(UndoableItem undoItem)
        {
            if (undoItem == null)
            {
                return;
            }

            // Select the appropriate calculation for which the undo action is performed.
            var undoCalculation = undoItem.ViewModelInstance as TransportCostCalculationViewModel;
            if (undoCalculation != null)
            {
                this.SelectedViewModelItem = undoCalculation;
            }
        }

        /// <summary>
        /// Inserts the calculation item into the CalculationItems and ViewModelItems collections, in its original place.
        /// </summary>
        /// <param name="item">The item to insert.</param>
        private void InsertItemIntoCollections(TransportCostCalculationViewModel item)
        {
            var index = item.Index.Value.GetValueOrDefault();
            if (index > 0 && index <= this.CalculationItems.Count)
            {
                // If the index is in range, insert the item into its original place in the calculation items collection.
                this.CalculationItems.Insert(index - 1, item);
            }
            else
            {
                this.CalculationItems.Add(item);
            }

            if (index >= 0 && index < this.ViewModelItems.Count)
            {
                // If the index is in range, insert the item into its original place in the view model items collection.
                this.ViewModelItems.Insert(index, item);
            }
            else
            {
                this.ViewModelItems.Add(item);
            }
        }

        #endregion
    }
}
