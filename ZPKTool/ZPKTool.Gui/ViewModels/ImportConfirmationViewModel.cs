﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Resources;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view model of the model browser.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ImportConfirmationViewModel : ViewModel
    {
        #region Atributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The MEF composition container.
        /// </summary>
        private readonly CompositionContainer compositionContainer;

        /// <summary>
        /// Weak event listener for the PropertyChanged notification
        /// </summary>
        private readonly WeakEventListener<PropertyChangedEventArgs> propertyChangedListener;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The data manager on which the master data entities are attached
        /// </summary>
        private IDataSourceManager dataSourceManager;

        /// <summary>
        /// Reference to the user control loaded in the details section of the screen
        /// </summary>
        private object content;

        /// <summary>
        /// The entity loaded model
        /// </summary>
        private object model;

        /// <summary>
        /// Indicates if the import can continue or not
        /// </summary>
        private bool canImportContinue;

        /// <summary>
        /// Indicates whether to show the import confirmation screen.
        /// </summary>
        private bool dontShowImportConfirmationScreen;

        #endregion Atributes

        /// <summary>
        /// Initializes a new instance of the <see cref="ImportConfirmationViewModel" /> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        /// <param name="compositionContainer">The composition container.</param>
        [ImportingConstructor]
        public ImportConfirmationViewModel(IWindowService windowService, CompositionContainer compositionContainer)
        {
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("compositionContainer", compositionContainer);

            this.windowService = windowService;
            this.compositionContainer = compositionContainer;
            this.Content = null;

            this.DontShowImportConfirmationScreen = UserSettingsManager.Instance.DontShowImportConfirmation;

            this.CanImportContinue = false;
            this.ImportCommand = new DelegateCommand(this.Import);
            this.CancelImportCommand = new DelegateCommand(this.CancelImport);

            this.propertyChangedListener = new WeakEventListener<PropertyChangedEventArgs>((s, e) => { this.DontShowImportConfirmationScreen = UserSettingsManager.Instance.DontShowImportConfirmation; });
            PropertyChangedEventManager.AddListener(UserSettingsManager.Instance, propertyChangedListener, ReflectionUtils.GetPropertyName(() => UserSettingsManager.Instance.DontShowImportConfirmation));
        }

        #region Commands

        /// <summary>
        /// Gets the command for the import button
        /// </summary>
        public ICommand ImportCommand { get; private set; }

        /// <summary>
        /// Gets the command for the cancel button
        /// </summary>
        public ICommand CancelImportCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets or sets the user control loaded in the details content area of the screen
        /// </summary>
        public object Content
        {
            get { return this.content; }
            set { this.SetProperty(ref this.content, value, () => this.Content); }
        }

        /// <summary>
        /// Gets or sets the entity loaded model
        /// </summary>
        public object Model
        {
            get { return this.model; }
            set { this.SetProperty(ref this.model, value, () => this.Model, () => this.LoadEntityDetails(this.model)); }
        }

        /// <summary>
        /// Gets or sets the entity in which the model may be imported.
        /// </summary>
        public object ModelParent { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether if the import can continue or not
        /// </summary>
        public bool CanImportContinue
        {
            get { return this.canImportContinue; }
            set { this.SetProperty(ref this.canImportContinue, value, () => this.CanImportContinue); }
        }

        /// <summary>
        /// Gets or sets the data manager on which the master data entities are attached
        /// </summary>
        public IDataSourceManager DataSourceManager
        {
            get { return this.dataSourceManager; }
            set { this.SetProperty(ref this.dataSourceManager, value, () => this.DataSourceManager); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to show the Import Confirmation screen.
        /// </summary> 
        public bool DontShowImportConfirmationScreen
        {
            get { return this.dontShowImportConfirmationScreen; }
            set { this.SetProperty(ref this.dontShowImportConfirmationScreen, value, () => this.DontShowImportConfirmationScreen, this.SaveSettingShowImportConfirmationScreen); }
        }

        #endregion

        /// <summary>
        /// Loads the details screen of an entity based on the entity type
        /// </summary>
        /// <param name="model">The object model that is loaded</param>
        private void LoadEntityDetails(object model)
        {
            if (model == null)
            {
                return;
            }

            var typeOfModel = model.GetType();
            if (typeOfModel == typeof(Project))
            {
                ProjectViewModel projVM = this.compositionContainer.GetExportedValue<ProjectViewModel>();
                projVM.IsReadOnly = true;
                projVM.DataSourceManager = this.DataSourceManager;
                projVM.Model = model as Project;

                this.Content = projVM;
            }
            else if (typeOfModel == typeof(Assembly))
            {
                AssemblyViewModel assyVM = this.compositionContainer.GetExportedValue<AssemblyViewModel>();
                assyVM.IsReadOnly = true;
                assyVM.DataSourceManager = this.DataSourceManager;
                assyVM.Model = model as Assembly;

                this.Content = assyVM;
            }
            else if (typeOfModel == typeof(Part) || typeOfModel == typeof(RawPart))
            {
                PartViewModel partVM = this.compositionContainer.GetExportedValue<PartViewModel>();
                partVM.IsReadOnly = true;
                partVM.DataSourceManager = this.DataSourceManager;
                partVM.Model = model as Part;

                this.Content = partVM;
            }
            else if (typeOfModel == typeof(Machine))
            {
                var machineVM = this.compositionContainer.GetExportedValue<MachineViewModel>();
                machineVM.IsReadOnly = true;
                machineVM.DataSourceManager = this.DataSourceManager;
                machineVM.CostCalculationVersion = this.GetCalculationVariant(this.ModelParent);
                machineVM.Model = model as Machine;
                this.Content = machineVM;
            }
            else if (typeOfModel == typeof(RawMaterial))
            {
                RawMaterialViewModel rawMaterialVM = this.compositionContainer.GetExportedValue<RawMaterialViewModel>();
                rawMaterialVM.IsReadOnly = true;
                rawMaterialVM.DataSourceManager = this.DataSourceManager;
                rawMaterialVM.CostCalculationVersion = this.GetCalculationVariant(this.ModelParent);
                rawMaterialVM.Model = model as RawMaterial;
                this.Content = rawMaterialVM;
            }
            else if (typeOfModel == typeof(Consumable))
            {
                ConsumableViewModel consumableVM = this.compositionContainer.GetExportedValue<ConsumableViewModel>();
                consumableVM.IsReadOnly = true;
                consumableVM.DataSourceManager = this.DataSourceManager;
                consumableVM.Model = model as Consumable;
                this.Content = consumableVM;
            }
            else if (typeOfModel == typeof(Commodity))
            {
                CommodityViewModel commodityVM = this.compositionContainer.GetExportedValue<CommodityViewModel>();
                commodityVM.IsReadOnly = true;
                commodityVM.DataSourceManager = this.DataSourceManager;
                commodityVM.Model = model as Commodity;
                this.Content = commodityVM;
            }
            else if (typeOfModel == typeof(Die))
            {
                DieViewModel dieVM = this.compositionContainer.GetExportedValue<DieViewModel>();
                dieVM.IsReadOnly = true;
                dieVM.DataSourceManager = this.DataSourceManager;
                dieVM.Model = model as Die;
                this.Content = dieVM;
            }
            else if (typeOfModel == typeof(Manufacturer))
            {
                var manufacturer = model as Manufacturer;
                if (manufacturer != null)
                {
                    var manufacturerVM = this.Content as ManufacturerViewModel;
                    if (manufacturerVM == null)
                    {
                        manufacturerVM = this.compositionContainer.GetExportedValue<ManufacturerViewModel>();
                    }

                    manufacturerVM.IsReadOnly = true;
                    manufacturerVM.DataSourceManager = this.dataSourceManager;
                    manufacturerVM.Model = manufacturer;
                    this.Content = manufacturerVM;
                }
                else
                {
                    this.Content = null;
                }
            }
            else if (typeOfModel == typeof(Customer))
            {
                SupplierViewModel supplierVM = this.compositionContainer.GetExportedValue<SupplierViewModel>();
                supplierVM.IsReadOnly = true;
                supplierVM.DataSourceManager = this.DataSourceManager;
                supplierVM.Model = model as Customer;

                this.Content = supplierVM;
            }
            else if (typeOfModel == typeof(ProjectFolder))
            {
                var folder = model as ProjectFolder;
                var projects = folder.Projects;

                var items = new List<object>();
                foreach (var project in folder.ChildrenProjectFolders.OrderBy(f => f.Name))
                {
                    items.Add(new { Name = project.Name, IconKey = Images.FolderIconKey });
                }

                foreach (var project in folder.Projects.OrderBy(p => p.Name))
                {
                    var formatedName = project.Name + (!string.IsNullOrEmpty(project.Number) ? " (" + project.Number + ")" : string.Empty);

                    items.Add(new { Name = formatedName, IconKey = Images.ProjectIconKey });
                }

                this.Content = items;
            }
        }

        /// <summary>
        /// Confirm the import of the entity
        /// </summary>
        private void Import()
        {
            this.CanImportContinue = true;

            // Close the view-model when is displayed in a window.
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Cancel the import of the entity
        /// </summary>
        private void CancelImport()
        {
            this.CanImportContinue = false;

            // Close the view-model when is displayed in a window.
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Saves the setting show import confirmation screen.
        /// </summary>
        private void SaveSettingShowImportConfirmationScreen()
        {
            UserSettingsManager.Instance.DontShowImportConfirmation = this.DontShowImportConfirmationScreen;
            UserSettingsManager.Instance.Save();
        }

        /// <summary>
        /// Called when the view has been unloaded.
        /// </summary>
        public override void OnUnloaded()
        {
            // Clear all data
            this.Model = null;
            this.ModelParent = null;
            this.Content = null;
            base.OnUnloaded();
        }

        /// <summary>
        /// Gets the calculation variant of a specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>The calculation variant or null if the entity does not have one.</returns>
        private string GetCalculationVariant(object entity)
        {
            var processStep = entity as ProcessStep;
            if (processStep != null && processStep.Process != null)
            {
                if (processStep.Process.Assemblies.Count > 0)
                {
                    entity = processStep.Process.Assemblies.First();
                }
                else if (processStep.Process.Parts.Count > 0)
                {
                    entity = processStep.Process.Parts.First();
                }
            }

            var part = entity as Part;
            if (part != null)
            {
                return part.CalculationVariant;
            }
            else
            {
                var assy = entity as Assembly;
                if (assy != null)
                {
                    return assy.CalculationVariant;
                }
            }

            return null;
        }
    }
}
