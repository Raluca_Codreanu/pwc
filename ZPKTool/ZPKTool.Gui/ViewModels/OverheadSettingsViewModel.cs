﻿using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations;
using System.Windows;
using System.Windows.Input;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the IMyView view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class OverheadSettingsViewModel : ViewModel<OverheadSetting, IDataSourceManager>
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// A value indicating whether to check if the overhead settings can be updated.
        /// </summary>
        private bool checkForUpdate;

        /// <summary>
        /// A value indicating whether an update is available for the current model.
        /// </summary>
        private bool updateAvailable;

        /// <summary>
        /// A value indicating whether an update was applied.
        /// </summary>
        private bool updateApplied;

        /// <summary>
        /// The visibility of the updated values.
        /// </summary>
        private Visibility updatedValuesVisibility;

        /// <summary>
        /// The master data overhead settings.
        /// </summary>
        private OverheadSetting masterOHSettings;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        #endregion Attributes

        /// <summary>
        /// Initializes a new instance of the <see cref="OverheadSettingsViewModel"/> class.
        /// </summary>
        /// <param name="messenger">The messenger.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="unitsService">The units service.</param>
        [ImportingConstructor]
        public OverheadSettingsViewModel(
            IMessenger messenger,
            IWindowService windowService,
            IUnitsService unitsService)
        {
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("unitsService", unitsService);

            this.messenger = messenger;
            this.windowService = windowService;
            this.unitsService = unitsService;

            this.CheckForUpdate = false;
            this.IsChanged = false;
            this.IsUpdateEnabled = true;

            this.UpdateOverheadSettingsCommand = new DelegateCommand(UpdateOverheadSettings, () => this.CanUpdateOverheadSettings());
            this.PropertyChanged += OnPropertyChanged;
        }

        #region Commands

        /// <summary>
        /// Gets the command that applies the updated overhead settings, if available.
        /// </summary>
        public ICommand UpdateOverheadSettingsCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether to check if the overhead settings can be updated.
        /// The update possibility is determined by comparing the overhead settings with the master data overhead settings.
        /// </summary>
        public bool CheckForUpdate
        {
            get { return this.checkForUpdate; }
            set { this.SetProperty(ref this.checkForUpdate, value, () => this.CheckForUpdate, this.PerformCheckForUpdate); }
        }

        /// <summary>
        /// Gets or sets the visibility of the updated values.
        /// </summary>
        public Visibility UpdatedValuesVisibility
        {
            get { return this.updatedValuesVisibility; }
            set { this.SetProperty(ref this.updatedValuesVisibility, value, () => this.UpdatedValuesVisibility); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether an update is available for the current model.
        /// </summary>
        public bool UpdateAvailable
        {
            get { return this.updateAvailable; }
            set { this.SetProperty(ref this.updateAvailable, value, () => this.UpdateAvailable); }
        }

        /// <summary>
        /// Gets the material overhead.
        /// </summary>
        [Required(ErrorMessageResourceName = "OverheadSettings_MaterialOHValidationError", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("MaterialOverhead", AffectsCost = true)]
        public DataProperty<decimal?> MaterialOverhead { get; private set; }

        /// <summary>
        /// Gets the updated material overhead.
        /// </summary>
        public VMProperty<decimal?> UpdatedMaterialOverhead { get; private set; }

        /// <summary>
        /// Gets the consumable overhead.
        /// </summary>
        [Required(ErrorMessageResourceName = "OverheadSettings_ConsumableOHValidationError", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("ConsumableOverhead", AffectsCost = true)]
        public DataProperty<decimal?> ConsumableOverhead { get; private set; }

        /// <summary>
        /// Gets the updated consumable overhead.
        /// </summary>
        public VMProperty<decimal?> UpdatedConsumableOverhead { get; private set; }

        /// <summary>
        /// Gets the commodity overhead.
        /// </summary>
        [Required(ErrorMessageResourceName = "OverheadSettings_CommodityOHValidationError", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("CommodityOverhead", AffectsCost = true)]
        public DataProperty<decimal?> CommodityOverhead { get; private set; }

        /// <summary>
        /// Gets the updated commodity overhead.
        /// </summary>
        public VMProperty<decimal?> UpdatedCommodityOverhead { get; private set; }

        /// <summary>
        /// Gets the external work overhead.
        /// </summary>
        [Required(ErrorMessageResourceName = "OverheadSettings_ExternalWorkOHValidationError", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("ExternalWorkOverhead", AffectsCost = true)]
        public DataProperty<decimal?> ExternalWorkOverhead { get; private set; }

        /// <summary>
        /// Gets the updated external work overhead.
        /// </summary>
        public VMProperty<decimal?> UpdatedExternalWorkOverhead { get; private set; }

        /// <summary>
        /// Gets the manufacturing overhead.
        /// </summary>
        [Required(ErrorMessageResourceName = "OverheadSettings_ManufacturingOHValidationError", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("ManufacturingOverhead", AffectsCost = true)]
        public DataProperty<decimal?> ManufacturingOverhead { get; private set; }

        /// <summary>
        /// Gets the updated manufacturing overhead.
        /// </summary>
        public VMProperty<decimal?> UpdatedManufacturingOverhead { get; private set; }

        /// <summary>
        /// Gets the other cost overhead.
        /// </summary>
        [Required(ErrorMessageResourceName = "OverheadSettings_OtherCostOHValidationError", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("OtherCostOHValue", AffectsCost = true)]
        public DataProperty<decimal?> OtherCostOverhead { get; private set; }

        /// <summary>
        /// Gets the updated other cost overhead.
        /// </summary>
        public VMProperty<decimal?> UpdatedOtherCostOverhead { get; private set; }

        /// <summary>
        /// Gets the packaging overhead.
        /// </summary>
        [Required(ErrorMessageResourceName = "OverheadSettings_PackagingOHValidationError", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("PackagingOHValue", AffectsCost = true)]
        public DataProperty<decimal?> PackagingOverhead { get; private set; }

        /// <summary>
        /// Gets the updated packaging overhead.
        /// </summary>
        public VMProperty<decimal?> UpdatedPackagingOverhead { get; private set; }

        /// <summary>
        /// Gets the logistic overhead.
        /// </summary>
        [Required(ErrorMessageResourceName = "OverheadSettings_LogisticOHValidationError", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("LogisticOHValue", AffectsCost = true)]
        public DataProperty<decimal?> LogisticOverhead { get; private set; }

        /// <summary>
        /// Gets the updated logistic overhead.
        /// </summary>
        public VMProperty<decimal?> UpdatedLogisticOverhead { get; private set; }

        /// <summary>
        /// Gets the sales and administration overhead.
        /// </summary>
        [Required(ErrorMessageResourceName = "OverheadSettings_SalesAndAdminOHValidationError", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("SalesAndAdministrationOHValue", AffectsCost = true)]
        public DataProperty<decimal?> SalesAndAdministrationOverhead { get; private set; }

        /// <summary>
        /// Gets the updated sales and administration overhead.
        /// </summary>
        public VMProperty<decimal?> UpdatedSalesAndAdministrationOverhead { get; private set; }

        /// <summary>
        /// Gets the company surcharge overhead.
        /// </summary>
        [Required(ErrorMessageResourceName = "OverheadSettings_CompanySurchargeOHValidationError", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("CompanySurchargeOverhead", AffectsCost = true)]
        public DataProperty<decimal?> CompanySurchargeOverhead { get; private set; }

        /// <summary>
        /// Gets the updated company surcharge overhead.
        /// </summary>
        public VMProperty<decimal?> UpdatedCompanySurchargeOverhead { get; private set; }

        /// <summary>
        /// Gets the material margin.
        /// </summary>
        [Required(ErrorMessageResourceName = "OverheadSettings_MaterialMarginValidationError", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("MaterialMargin", AffectsCost = true)]
        public DataProperty<decimal?> MaterialMargin { get; private set; }

        /// <summary>
        /// Gets the updated material margin.
        /// </summary>
        public VMProperty<decimal?> UpdatedMaterialMargin { get; private set; }

        /// <summary>
        /// Gets the consumable margin.
        /// </summary>
        [Required(ErrorMessageResourceName = "OverheadSettings_ConsumableMarginValidationError", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("ConsumableMargin", AffectsCost = true)]
        public DataProperty<decimal?> ConsumableMargin { get; private set; }

        /// <summary>
        /// Gets the updated consumable margin.
        /// </summary>
        public VMProperty<decimal?> UpdatedConsumableMargin { get; private set; }

        /// <summary>
        /// Gets the commodity margin.
        /// </summary>
        [Required(ErrorMessageResourceName = "OverheadSettings_CommodityMarginValidationError", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("CommodityMargin", AffectsCost = true)]
        public DataProperty<decimal?> CommodityMargin { get; private set; }

        /// <summary>
        /// Gets the updated commodity margin.
        /// </summary>
        public VMProperty<decimal?> UpdatedCommodityMargin { get; private set; }

        /// <summary>
        /// Gets the external work margin.
        /// </summary>
        [Required(ErrorMessageResourceName = "OverheadSettings_ExternalWorkMarginValidationError", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("ExternalWorkMargin", AffectsCost = true)]
        public DataProperty<decimal?> ExternalWorkMargin { get; private set; }

        /// <summary>
        /// Gets the updated external work margin.
        /// </summary>
        public VMProperty<decimal?> UpdatedExternalWorkMargin { get; private set; }

        /// <summary>
        /// Gets the manufacturing margin.
        /// </summary>
        [Required(ErrorMessageResourceName = "OverheadSettings_ManufacturingMarginValidationError", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("ManufacturingMargin", AffectsCost = true)]
        public DataProperty<decimal?> ManufacturingMargin { get; private set; }

        /// <summary>
        /// Gets the updated manufacturing margin.
        /// </summary>
        public VMProperty<decimal?> UpdatedManufacturingMargin { get; private set; }

        /// <summary>
        /// Gets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether an update was applied.
        /// </summary>
        public bool UpdateApplied
        {
            get
            {
                return this.updateApplied;
            }

            set
            {
                if (this.updateApplied != value)
                {
                    this.updateApplied = value;
                    if (this.updateApplied)
                    {
                        this.UpdateAvailable = false;
                        this.UpdatedValuesVisibility = Visibility.Collapsed;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the overhead settings update is enabled or not
        /// </summary>
        public bool IsUpdateEnabled { get; set; }

        #endregion Properties

        /// <summary>
        /// Called when the Model property has changed.
        /// </summary>
        protected override void OnModelChanged()
        {
            this.CheckDataSource();
            this.LoadDataFromModel();

            if (this.IsInViewerMode)
            {
                this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(null);
            }

            this.PerformCheckForUpdate();
            this.UndoManager.Start();
        }

        /// <summary>
        /// Called when DataSourceManager has changed.
        /// </summary>
        protected override void OnDataSourceManagerChanged()
        {
            base.OnDataSourceManagerChanged();

            this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(this.DataSourceManager);
        }

        /// <summary>
        /// Saves all changed back into the model.
        /// </summary>
        protected override void SaveToModel()
        {
            base.SaveToModel();

            if (this.UpdateApplied &&
                this.masterOHSettings != null)
            {
                this.Model.LastUpdateTime = this.masterOHSettings.LastUpdateTime;
            }
            else if (this.Model.IsMasterData)
            {
                // If the overhead setting was modified from master data settings store the local time
                // Do not change the time if the user is changing the overhead settings manually from the project overhead setting section
                // to not interfere with the overhead settings update notification mechanism
                this.Model.LastUpdateTime = System.DateTime.Now;
            }
        }

        /// <summary>
        /// Performs the save operation. Executed by the SaveCommand.
        /// </summary>
        protected override void Save()
        {
            this.CheckModelAndDataSource();

            this.SaveToModel();
            this.DataSourceManager.OverheadSettingsRepository.Save(this.Model);
            this.DataSourceManager.SaveChanges();
        }

        /// <summary>
        /// Cancels all changes. Executed by the CancelCommand.
        /// </summary>
        protected override void Cancel()
        {
            if (this.IsChanged)
            {
                var result = MessageDialogResult.Yes;
                if (!this.IsChild)
                {
                    result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_CancelChanges, MessageDialogType.YesNo);
                }

                if (result != MessageDialogResult.Yes)
                {
                    // Don't cancel the changes and also don't close the view-model.
                    return;
                }
                else
                {
                    // Cancel all changes
                    base.Cancel();
                }
            }

            this.UpdateApplied = false;
            this.PerformCheckForUpdate();

            // Close the view-model when is displayed in a window.
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Called before unloading the view from its parent. Returning false will cancel the view's unloading.
        /// </summary>
        /// <returns>
        /// True if the unloading process should continue and false if it should be canceled.
        /// </returns>
        public override bool OnUnloading()
        {
            // Do nothing if the view-model is read-only, in viewer mode or it was not changed.
            if (this.IsReadOnly
                || this.IsInViewerMode
                || !this.IsChanged)
            {
                return true;
            }

            if (this.EditMode == ViewModelEditMode.Edit)
            {
                // Ask the user if he wants to save
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_UnsavedDataOnQuit, MessageDialogType.YesNoCancel);
                if (result == MessageDialogResult.Yes)
                {
                    // The user wishes to save but it is not possible because the input is not valid. Return false to stop the view-model from unloading.
                    if (!this.CanSave())
                    {
                        return false;
                    }

                    this.Save();
                }
                else if (result == MessageDialogResult.No)
                {
                    // The user does not want to save.                    
                    this.IsChanged = false;
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Checks to see if an overhead settings update is available.
        /// </summary>
        private void PerformCheckForUpdate()
        {
            if (this.Model == null
                || !this.checkForUpdate
                || this.IsReadOnly)
            {
                this.UpdateAvailable = false;
                this.UpdatedValuesVisibility = Visibility.Collapsed;

                return;
            }

            try
            {
                this.masterOHSettings = this.DataSourceManager.OverheadSettingsRepository.GetMasterData();
                if (masterOHSettings != null
                    && masterOHSettings.LastUpdateTime != this.Model.LastUpdateTime
                    && !masterOHSettings.CompareValuesTo(this.Model))
                {
                    this.LoadUpdatedValues(this.masterOHSettings);
                    this.UpdateAvailable = true;
                }
                else
                {
                    this.UpdateAvailable = false;
                }
            }
            catch (DataAccessException ex)
            {
                // Don't show an error as it does not have much impact on the user's ability to use the view.
                log.ErrorException("Encountered an error while checking for overhead settings update.", ex);
                this.UpdateAvailable = false;
            }
        }

        /// <summary>
        /// Loads the values from the specified overhead settings into the updated values.
        /// </summary>
        /// <param name="overheadSettings">The overhead settings.</param>
        private void LoadUpdatedValues(OverheadSetting overheadSettings)
        {
            this.UpdatedCommodityMargin.Value = overheadSettings.CommodityMargin;
            this.UpdatedCommodityOverhead.Value = overheadSettings.CommodityOverhead;
            this.UpdatedConsumableMargin.Value = overheadSettings.ConsumableMargin;
            this.UpdatedConsumableOverhead.Value = overheadSettings.ConsumableOverhead;
            this.UpdatedExternalWorkMargin.Value = overheadSettings.ExternalWorkMargin;
            this.UpdatedExternalWorkOverhead.Value = overheadSettings.ExternalWorkOverhead;
            this.UpdatedLogisticOverhead.Value = overheadSettings.LogisticOHValue;
            this.UpdatedManufacturingMargin.Value = overheadSettings.ManufacturingMargin;
            this.UpdatedManufacturingOverhead.Value = overheadSettings.ManufacturingOverhead;
            this.UpdatedMaterialMargin.Value = overheadSettings.MaterialMargin;
            this.UpdatedMaterialOverhead.Value = overheadSettings.MaterialOverhead;
            this.UpdatedOtherCostOverhead.Value = overheadSettings.OtherCostOHValue;
            this.UpdatedPackagingOverhead.Value = overheadSettings.PackagingOHValue;
            this.UpdatedSalesAndAdministrationOverhead.Value = overheadSettings.SalesAndAdministrationOHValue;
            this.UpdatedCompanySurchargeOverhead.Value = overheadSettings.CompanySurchargeOverhead;
        }

        /// <summary>
        /// Determines whether this instance can perform the update overhead settings operation. Executed by the UpdateOverheadSettingsCommand.
        /// </summary>
        /// <returns>
        /// True if the update operation can be performed, False otherwise.
        /// </returns>
        private bool CanUpdateOverheadSettings()
        {
            return this.UpdateAvailable && this.IsUpdateEnabled;
        }

        /// <summary>
        /// Requests for a save all command at first, if the command can be executed, it updates the overhead settings.
        /// </summary>
        private void UpdateOverheadSettings()
        {
            if (this.masterOHSettings == null)
            {
                return;
            }

            using (this.UndoManager.Pause())
            {
                this.LoadDataFromModel(this.masterOHSettings);
                var requestUpdateMessage = new NotificationMessage(this, Notification.RequestUpdateOverheadSettings);
                this.messenger.Send(requestUpdateMessage);
            }
        }

        /// <summary>
        /// Handles the PropertyChanged event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void OnPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsReadOnly")
            {
                this.PerformCheckForUpdate();
            }
        }
    }
}