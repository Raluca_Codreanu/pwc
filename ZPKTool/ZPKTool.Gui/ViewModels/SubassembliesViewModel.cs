﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using ZPKTool.Business;
using ZPKTool.Business.Export;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Managers;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the Subassemblies view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class SubassembliesViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The service used to create open/save file dialogs.
        /// </summary>
        private IFileDialogService fileDialogService;

        /// <summary>
        /// The service used to brows for folders.
        /// </summary>
        private IFolderBrowserService folderBrowserService;

        /// <summary>
        /// The PleaseWaitService reference.
        /// </summary>
        private IPleaseWaitService pleaseWaitService;

        /// <summary>
        /// The composition container.
        /// </summary>
        private CompositionContainer container;

        /// <summary>
        /// The collection of objects which are used in the binding on the data grid
        /// </summary>
        private DispatchedObservableCollection<Assembly> subassemblies = new DispatchedObservableCollection<Assembly>();

        /// <summary>
        /// Reference to the user control loaded in the details section of the screen
        /// </summary>
        private object assemblyDetails;

        /// <summary>
        /// The parent Assembly.
        /// </summary>
        private Assembly parentAssembly;

        /// <summary>
        /// The data context.
        /// </summary>
        private IDataSourceManager assemblyDataContext;

        /// <summary>
        /// Extra information for the report.
        /// </summary>
        private ExtendedDataGridAdditionalReportInformation additionalReportInformation;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        /// <summary>
        /// The currently selected items in the grid
        /// </summary>
        private ObservableCollection<object> selectedItems = new ObservableCollection<object>();

        /// <summary>
        /// The currently selected entities info data.
        /// </summary>
        private ObservableCollection<EntityInfo> selectedEntitiesInfo = new ObservableCollection<EntityInfo>();

        #endregion Attributes

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SubassembliesViewModel"/> class.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="messenger">The messenger.</param>
        /// <param name="fileDialogService">The file dialog service.</param>
        /// <param name="folderBrowserService">The folder browser service.</param>
        /// <param name="pleaseWaitService">The please wait service.</param>
        /// <param name="unitsService">The units service.</param>
        [ImportingConstructor]
        public SubassembliesViewModel(
            CompositionContainer container,
            IWindowService windowService,
            IMessenger messenger,
            IFileDialogService fileDialogService,
            IFolderBrowserService folderBrowserService,
            IPleaseWaitService pleaseWaitService,
            IUnitsService unitsService)
        {
            Argument.IsNotNull("container", container);
            Argument.IsNotNull("container", container);
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("fileDialogService", fileDialogService);
            Argument.IsNotNull("folderBrowserService", folderBrowserService);
            Argument.IsNotNull("pleaseWaitService", pleaseWaitService);
            Argument.IsNotNull("unitsService", unitsService);

            this.container = container;
            this.WindowService = windowService;
            this.messenger = messenger;
            this.fileDialogService = fileDialogService;
            this.folderBrowserService = folderBrowserService;
            this.pleaseWaitService = pleaseWaitService;
            this.unitsService = unitsService;

            this.AddCommand = new DelegateCommand(this.Add, () => !this.IsReadOnly);
            this.DeleteCommand = new DelegateCommand(
                this.Delete,
                () => !this.IsReadOnly && this.SelectedItems != null && this.SelectedItems.Count > 0);
            this.EditCommand = new DelegateCommand(
                this.Edit,
                () => !this.IsReadOnly && this.SelectedItems != null && this.SelectedItems.Count == 1);
            this.MouseDoubleClickCommand = new DelegateCommand<MouseButtonEventArgs>(this.MouseDoubleClicked);
            this.DragOverCommand = new DelegateCommand<DragEventArgs>(this.CheckDropTarget, (e) => !this.IsReadOnly);
            this.DragEnterCommand = new DelegateCommand<DragEventArgs>(this.CheckDropTarget, (e) => !this.IsReadOnly);
            this.DropCommand = new DelegateCommand<DragEventArgs>(this.Drop, (e) => !this.IsReadOnly);
            this.CopyCommand = new DelegateCommand<object>(this.ExecuteCopy, this.CanCopy);
            this.PasteCommand = new DelegateCommand<object>(this.ExecutePaste, this.CanPaste);
            this.ImportCommand = new DelegateCommand<object>(this.ExecuteImport, (o) => !this.IsReadOnly);
            this.ExportCommand = new DelegateCommand<object>(this.ExecuteExport, this.CanExport);
            this.SelectedItems.CollectionChanged += this.SelectedItemsOnCollectionChanged;
        }

        #endregion

        #region Commands

        /// <summary>
        /// Gets the command for the add button
        /// </summary>
        public ICommand AddCommand { get; private set; }

        /// <summary>
        /// Gets the command for the edit button
        /// </summary>
        public ICommand EditCommand { get; private set; }

        /// <summary>
        /// Gets the command for the delete button
        /// </summary>
        public ICommand DeleteCommand { get; private set; }

        /// <summary>
        /// Gets the command for the mouse DragEnter event
        /// </summary>
        public ICommand DragEnterCommand { get; private set; }

        /// <summary>
        /// Gets the DragOver command.
        /// </summary>
        public ICommand DragOverCommand { get; private set; }

        /// <summary>
        /// Gets the drop command.
        /// </summary>
        public ICommand DropCommand { get; private set; }

        /// <summary>
        /// Gets the mouse double click command.
        /// </summary>
        public ICommand MouseDoubleClickCommand { get; private set; }

        /// <summary>
        /// Gets the copy command.
        /// </summary>
        public ICommand CopyCommand { get; private set; }

        /// <summary>
        /// Gets the paste command.
        /// </summary>
        public ICommand PasteCommand { get; private set; }

        /// <summary>
        /// Gets the import command.
        /// </summary>
        public ICommand ImportCommand { get; private set; }

        /// <summary>
        /// Gets the export command.
        /// </summary>
        public ICommand ExportCommand { get; private set; }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the window service.
        /// </summary>
        public IWindowService WindowService
        {
            get
            {
                return this.windowService;
            }

            set
            {
                if (this.windowService != value)
                {
                    this.windowService = value;
                    OnPropertyChanged(() => this.WindowService);
                }
            }
        }

        /// <summary>
        /// Gets or sets the collection of assemblies displayed in the data grid
        /// </summary>
        public DispatchedObservableCollection<Assembly> Subassemblies
        {
            get
            {
                return this.subassemblies;
            }

            set
            {
                if (this.subassemblies != value)
                {
                    this.subassemblies = value;
                    OnPropertyChanged(() => this.Subassemblies);
                }
            }
        }

        /// <summary>
        /// Gets or sets the currently selected items in the grid.
        /// </summary>
        public ObservableCollection<object> SelectedItems
        {
            get
            {
                return this.selectedItems;
            }

            set
            {
                if (!Equals(this.selectedItems, value))
                {
                    this.selectedItems = value;
                    OnPropertyChanged(() => this.SelectedItems);
                }
            }
        }

        /// <summary>
        /// Gets or sets the currently selected entities info data.
        /// </summary>
        public ObservableCollection<EntityInfo> SelectedEntitiesInfo
        {
            get
            {
                return this.selectedEntitiesInfo;
            }

            set
            {
                if (!Equals(this.selectedEntitiesInfo, value))
                {
                    this.selectedEntitiesInfo = value;
                    OnPropertyChanged(() => this.SelectedEntitiesInfo);
                }
            }
        }

        /// <summary>
        /// Gets or sets the user control loaded in the details content area of the screen
        /// </summary>
        public object AssemblyDetails
        {
            get
            {
                return this.assemblyDetails;
            }

            set
            {
                if (this.assemblyDetails != value)
                {
                    this.assemblyDetails = value;
                    OnPropertyChanged(() => this.AssemblyDetails);
                }
            }
        }

        /// <summary>
        /// Gets or sets the data context.
        /// </summary>
        /// <value>
        /// The data context.
        /// </value>
        public IDataSourceManager AssemblyDataContext
        {
            get
            {
                return this.assemblyDataContext;
            }

            set
            {
                if (this.assemblyDataContext != value)
                {
                    this.assemblyDataContext = value;
                    OnPropertyChanged(() => this.AssemblyDataContext);
                }
            }
        }

        /// <summary>
        /// Gets or sets the parent assembly.
        /// </summary>
        /// <value>
        /// The parent assembly.
        /// </value>
        public Assembly ParentAssembly
        {
            get
            {
                return this.parentAssembly;
            }

            set
            {
                if (this.parentAssembly != value)
                {
                    this.parentAssembly = value;
                    OnPropertyChanged(() => this.IsReadOnly);

                    this.Subassemblies = new DispatchedObservableCollection<Assembly>();
                    this.AdditionalReportInformation = new ExtendedDataGridAdditionalReportInformation();

                    if (this.parentAssembly != null)
                    {
                        this.Subassemblies =
                            new DispatchedObservableCollection<Assembly>(
                                this.parentAssembly.Subassemblies.Where(s => !s.IsDeleted).OrderBy(s => s.Index));
                        var parentProject = !IsInViewerMode
                            ? this.AssemblyDataContext.ProjectRepository.GetParentProject(this.ParentAssembly)
                            : EntityUtils.GetParentProject(this.ParentAssembly);

                        this.AdditionalReportInformation.ProjectName += parentProject != null ? " " + parentProject.Name : string.Empty;
                        this.AdditionalReportInformation.ParentName += " " + this.ParentAssembly.Name;
                        this.AdditionalReportInformation.Name += " " + LocalizedResources.General_Subassemblies + " (" + this.ParentAssembly.Name + ")";
                        this.AdditionalReportInformation.Version += " " + this.ParentAssembly.Version;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the extra report information.
        /// </summary>
        public ExtendedDataGridAdditionalReportInformation AdditionalReportInformation
        {
            get
            {
                return this.additionalReportInformation;
            }

            set
            {
                if (this.additionalReportInformation != value)
                {
                    this.additionalReportInformation = value;
                    OnPropertyChanged(() => this.AdditionalReportInformation);
                }
            }
        }

        #endregion

        /// <summary>
        /// Adds this instance.
        /// </summary>
        private void Add()
        {
            var assyVm = this.container.GetExportedValue<AssemblyViewModel>();
            assyVm.DataSourceManager = this.AssemblyDataContext;
            assyVm.InitializeForAssemblyCreation(false, this.ParentAssembly);

            this.windowService.ShowViewInDialog(assyVm, "CreateAssemblyViewTemplate");

            var newAssembly = assyVm.Model;
            if (newAssembly != null && assyVm.Saved)
            {
                this.Subassemblies.Add(newAssembly);
            }
        }

        /// <summary>
        /// Deletes the selected assemblies.
        /// </summary>
        private void Delete()
        {
            var selectedAssemblies = this.SelectedItems.Cast<Assembly>().ToList();
            if (selectedAssemblies.Count == 0)
            {
                this.windowService.MessageDialogService.Show(
                            LocalizedResources.Error_NoItemSelected,
                            MessageDialogType.Info);
                return;
            }

            var permanentlyDelete = Keyboard.Modifiers == ModifierKeys.Shift;
            var isMasterData = selectedAssemblies.Any(EntityUtils.IsMasterData);
            string questionMessage;

            /* 
             * If the Shift modifier is not pressed or the selected assemblies 
             * are not Masterdata then the deletion should be made to the 
             * trash bin else should be deleted permanently.
             */
            if (permanentlyDelete || isMasterData)
            {
                questionMessage = LocalizedResources.Question_PermanentlyDeleteSelectedItems;
            }
            else
            {
                questionMessage = LocalizedResources.Question_DeleteSelectedObjects;
            }

            var result = this.windowService.MessageDialogService.Show(questionMessage, MessageDialogType.YesNo);

            /*
             * If the answer is Yes, delete the selected items.
             */
            if (result == MessageDialogResult.Yes)
            {
                Action<PleaseWaitService.WorkParams> work = (workParams) =>
                    {
                        var assemblyIndex = 1;
                        foreach (var selectedAssembly in selectedAssemblies)
                        {
                            string waitMessage;
                            if (selectedItems.Count == 1)
                            {
                                waitMessage = LocalizedResources.General_WaitItemDelete;
                            }
                            else
                            {
                                waitMessage = string.Format(
                                    LocalizedResources.General_WaitDeleteObjects,
                                    assemblyIndex,
                                    selectedItems.Count);
                            }

                            this.pleaseWaitService.Message = waitMessage;
                            assemblyIndex++;

                            var trashManager = new TrashManager(this.AssemblyDataContext);
                            if (isMasterData || permanentlyDelete)
                            {
                                trashManager.DeleteByStoreProcedure(selectedAssembly);
                            }
                            else
                            {
                                trashManager.DeleteToTrashBin(selectedAssembly);
                                this.AssemblyDataContext.SaveChanges();
                            }
                        }
                    };

                Action<PleaseWaitService.WorkParams> workCompleted = (workParams) =>
                {
                    if (workParams.Error != null)
                    {
                        this.windowService.MessageDialogService.Show(workParams.Error);
                    }
                    else
                    {
                        foreach (var selectedAssembly in selectedAssemblies)
                        {
                            var notification = this.ParentAssembly.IsMasterData
                                                   ? Notification.MasterDataEntityChanged
                                                   : Notification.MyProjectsEntityChanged;
                            var message = new EntityChangedMessage(notification);
                            message.Entity = selectedAssembly;
                            message.Parent = this.ParentAssembly;
                            message.ChangeType = EntityChangeType.EntityDeleted;
                            this.messenger.Send(message);

                            this.Subassemblies.Remove(selectedAssembly);
                        }
                    }
                };

                this.pleaseWaitService.Show(LocalizedResources.General_WaitItemDelete, work, workCompleted);
            }
        }

        /// <summary>
        /// Edits this instance.
        /// </summary>
        private void Edit()
        {
            var selectedAssemblies = this.SelectedItems.Cast<Assembly>().ToList();
            if (selectedAssemblies.Count == 1)
            {
                this.SelectAssembly(selectedAssemblies.First());
            }
        }

        /// <summary>
        /// Loads the selected assembly details.
        /// </summary>
        private void LoadSelectedAssemblyDetails()
        {
            var selectedAssemblies = this.SelectedItems.Cast<Assembly>().ToList();
            if (selectedAssemblies.Count != 1)
            {
                this.AssemblyDetails = null;
                return;
            }

            var assyVm = this.container.GetExportedValue<AssemblyViewModel>();
            assyVm.IsReadOnly = true;
            assyVm.IsInViewerMode = IsInViewerMode;
            assyVm.DataSourceManager = this.AssemblyDataContext;
            assyVm.Model = selectedAssemblies.First();

            this.AssemblyDetails = assyVm;
        }

        /// <summary>
        /// Selects a specified assembly in the projects tree.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        private void SelectAssembly(Assembly assembly)
        {
            if (assembly == null)
            {
                return;
            }

            this.messenger.Send(
                new NavigateToEntityMessage(
                    assembly,
                    this.AssemblyDataContext != null ? this.AssemblyDataContext.DatabaseId : DbIdentifier.NotSet),
                IsInViewerMode ? GlobalMessengerTokens.ModelBrowserTargetToken : GlobalMessengerTokens.MainViewTargetToken);
        }

        #region Mouse events

        /// <summary>
        /// DataGrid mouse double click event.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void MouseDoubleClicked(MouseButtonEventArgs e)
        {
            this.Edit();
        }

        /// <summary>
        /// Handles the CheckDropTarget event of the SubassembliesDataGrid control.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        private void CheckDropTarget(DragEventArgs e)
        {
            e.Handled = true;

            var sourceItem = (DragAndDropData)e.Data.GetData(typeof(DragAndDropData));
            if (sourceItem != null && sourceItem.DataObject is Assembly)
            {
                e.Effects = DragDropEffects.Copy;
            }
            else
            {
                e.Effects = DragDropEffects.None;
            }
        }

        /// <summary>
        /// Handles the Drop event of the SubassembliesDataGrid control.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        private void Drop(DragEventArgs e)
        {
            e.Handled = true;

            var sourceItem = (DragAndDropData)e.Data.GetData(typeof(DragAndDropData));
            if (sourceItem.DataObject is Assembly)
            {
                // Add the pasted item into its parent
                ClipboardManager.Instance.Copy(sourceItem.DataObject, sourceItem.DataObjectContext);
                var pastedObject = ClipboardManager.Instance.Paste(this.ParentAssembly, this.AssemblyDataContext).First();
                if (pastedObject.Error != null)
                {
                    this.windowService.MessageDialogService.Show(pastedObject.Error);
                }
                else
                {
                    var pastedAssembly = pastedObject.Entity as Assembly;
                    if (pastedAssembly != null)
                    {
                        this.Subassemblies.Add(pastedAssembly);

                        var message = new EntityChangedMessage(Notification.MyProjectsEntityChanged);
                        message.Entity = this.ParentAssembly;
                        message.Parent = null;
                        message.ChangeType = EntityChangeType.EntityUpdated;
                        this.messenger.Send(message);

                        this.SelectedItems.Add(pastedAssembly);
                    }
                }
            }
        }

        #endregion

        /// <summary>
        /// Determines whether this instance can paste the specified parameter.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        /// true if the command can be executed, false otherwise.
        /// </returns>
        private bool CanPaste(object parameter)
        {
            var clipboardObjTypes = ClipboardManager.Instance.PeekTypes.ToList();
            if (this.IsReadOnly
                || clipboardObjTypes.Count == 0
                || clipboardObjTypes.Any(type => type != typeof(Assembly)))
            {
                return false;
            }

            if (ClipboardManager.Instance.Operation == ClipboardOperation.Cut)
            {
                // An item can not be moved into a master data.
                if (this.ParentAssembly.IsMasterData)
                {
                    return false;
                }

                var clipboardObjects = ClipboardManager.Instance.PeekClipboardObjects;
                foreach (var clpObject in clipboardObjects)
                {
                    // An assembly can not be moved if: object to move are released or are master data,
                    // already exists in the destination collection or moved into one of its children
                    var releaseableObj = clpObject as IReleasable;
                    var assemblyObj = clpObject as Assembly;
                    if ((releaseableObj != null && releaseableObj.IsReleased)
                        || assemblyObj == null
                        || EntityUtils.IsMasterData(clpObject)
                        || this.ParentAssembly.Guid == assemblyObj.Guid
                        || ClipboardManager.Instance.CheckAssemblyIfParentOf(assemblyObj, this.ParentAssembly)
                        || this.ParentAssembly.Subassemblies.Any(assy => assy.Guid == assemblyObj.Guid))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Performs the logic associated with the Paste command.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void ExecutePaste(object parameter)
        {
            var clipboardObjTypes = ClipboardManager.Instance.PeekTypes.ToList();
            if (this.IsReadOnly
                || clipboardObjTypes.Count == 0
                || clipboardObjTypes.Any(type => type != typeof(Assembly)))
            {
                return;
            }

            var operation = ClipboardManager.Instance.Operation;
            var clipboardObjects = ClipboardManager.Instance.PeekClipboardObjects.ToList();
            string displayedMessage;

            /*
             * If the operation is Cut, the message displayed is "Moving object(s)..."; else, 
             * the operation is Copy, and the message displayed is "Copying object(s)..."
             */
            if (clipboardObjects.Count == 1)
            {
                var objName = EntityUtils.GetEntityName(clipboardObjects.First());
                if (!string.IsNullOrWhiteSpace(objName))
                {
                    objName = "\"" + objName + "\"";
                }

                displayedMessage =
                    string.Format(
                        operation == ClipboardOperation.Cut
                            ? LocalizedResources.General_MovingItem
                            : LocalizedResources.General_CopyingItem,
                        objName);
            }
            else
            {
                displayedMessage = operation == ClipboardOperation.Cut
                                       ? LocalizedResources.General_MovingObjects
                                       : LocalizedResources.General_CopyingObjects;
            }

            var pastedObjects = new List<PastedData>();
            Action<PleaseWaitService.WorkParams> work = (workParams) =>
            {
                pastedObjects = ClipboardManager.Instance.Paste(this.ParentAssembly, this.AssemblyDataContext).ToList();
            };

            Action<PleaseWaitService.WorkParams> workCompleted = (workparams) =>
            {
                if (workparams.Error != null)
                {
                    Log.Error("An error occurred while executing the paste operation.", workparams.Error);
                    this.windowService.MessageDialogService.Show(workparams.Error);
                }
                else
                {
                    // Display errors appeared during the paste process.
                    var errorMessage = ClipboardManager.Instance.GetErrorMessageToDisplay(pastedObjects);
                    if (!string.IsNullOrWhiteSpace(errorMessage))
                    {
                        this.windowService.MessageDialogService.Show(errorMessage, MessageDialogType.Error);
                    }

                    var lastPastedObject = pastedObjects.LastOrDefault(o => o.Error == null);
                    var messages = new Collection<EntityChangedMessage>();
                    this.SelectedItems.Clear();
                    foreach (var obj in pastedObjects.Where(o => o.Error == null))
                    {
                        var assy = obj.Entity as Assembly;
                        if (assy != null)
                        {
                            this.Subassemblies.Add(assy);
                            this.SelectedItems.Add(assy);

                            if (obj == lastPastedObject)
                            {
                                var notification = assy.IsMasterData
                                    ? Notification.MasterDataEntityChanged
                                    : Notification.MyProjectsEntityChanged;

                                var message = new EntityChangedMessage(notification);
                                message.Entity = assy;
                                message.Parent = this.ParentAssembly;
                                message.SelectEntity = true;
                                message.ChangeType = ClipboardManager.Instance.Operation == ClipboardOperation.Cut
                                    ? EntityChangeType.EntityMoved
                                    : EntityChangeType.EntityCreated;

                                messages.Add(message);
                            }
                        }
                    }

                    if (messages.Count == 1)
                    {
                        this.messenger.Send(messages.FirstOrDefault());
                    }
                    else if (messages.Any())
                    {
                        var msg = new EntitiesChangedMessage(messages);
                        this.messenger.Send(msg);
                    }
                }
            };

            this.pleaseWaitService.Show(displayedMessage, work, workCompleted);
        }

        /// <summary>
        /// Determines whether this instance can copy the specified parameter.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        ///   <c>true</c> if this instance can copy the specified parameter; otherwise, <c>false</c>.
        /// </returns>
        private bool CanCopy(object parameter)
        {
            if (this.SelectedItems == null
                || this.SelectedItems.Count == 0
                || !SecurityManager.Instance.CurrentUserHasRight(Right.ProjectsAndCalculate)
                || this.IsReadOnly)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Executes the copy.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void ExecuteCopy(object parameter)
        {
            var selectedAssemblies = this.SelectedItems.Cast<Assembly>().ToList();
            if (selectedAssemblies.Count == 0)
            {
                return;
            }

            var itemsToCopy = selectedAssemblies
                .Select(item => new ClipboardObject()
                {
                    Entity = item,
                    DbSource = this.AssemblyDataContext.DatabaseId
                });

            ClipboardManager.Instance.Copy(itemsToCopy);
        }

        /// <summary>
        /// Contains the logic associated with the Import command.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void ExecuteImport(object parameter)
        {
            /*
             * Set the file dialog service.
             */
            this.fileDialogService.Filter = UIUtils.GetDialogFilterForExportImport(typeof(Assembly));
            this.fileDialogService.Multiselect = true;
            if (this.fileDialogService.ShowOpenFileDialog() != true)
            {
                return;
            }

            var filePaths = new List<string>(this.fileDialogService.FileNames);

            Action<PleaseWaitService.WorkParams, IEnumerable<ImportedData<object>>> importCompleted = (workParams, importedData) =>
            {
                // Perform action after the import is completed.
                foreach (var entityData in importedData.Where(imp => imp.Entity != null))
                {
                    var importedObjectIsMasterData = EntityUtils.IsMasterData(entityData.Entity);
                    var notification = importedObjectIsMasterData ? Notification.MasterDataEntityChanged : Notification.MyProjectsEntityChanged;

                    // Only the last imported item will be selected.
                    var selectItem = entityData == importedData.LastOrDefault(i => i.Entity != null);

                    var item = entityData.Entity as Assembly;
                    this.Subassemblies.Add(item);
                    if (selectItem)
                    {
                        this.SelectedItems.Add(item);
                    }

                    var msg = new EntityChangedMessage(notification)
                    {
                        // Create a message to notify that a new item has been imported.
                        Entity = entityData.Entity,
                        Parent = this.ParentAssembly,
                        ChangeType = EntityChangeType.EntityImported,
                        SelectEntity = selectItem
                    };
                    this.messenger.Send(msg);
                }
            };

            var importService = this.container.GetExportedValue<IImportService>();
            importService.Import(filePaths, this.ParentAssembly, this.AssemblyDataContext, true, importCompleted);
        }

        /// <summary>
        /// Determines whether this instance can export the specified parameter.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>
        ///   <c>true</c> if this instance can export the specified parameter; otherwise, <c>false</c>.
        /// </returns>
        private bool CanExport(object parameter)
        {
            if (this.SelectedItems == null
                || this.SelectedItems.Count == 0
                || this.IsInViewerMode)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Executes the export.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void ExecuteExport(object parameter)
        {
            var selectedAssemblies = this.SelectedItems.Cast<Assembly>().ToList();
            if (selectedAssemblies.Count == 0)
            {
                return;
            }

            string filePath = null;
            if (this.SelectedItems.Count == 1)
            {
                var assemblyToExport = selectedAssemblies.First();
                var defaultFileName = EntityUtils.GetEntityName(assemblyToExport);
                if (!string.IsNullOrWhiteSpace(defaultFileName))
                {
                    defaultFileName = PathUtils.SanitizeFileName(defaultFileName);
                }

                this.fileDialogService.Reset();
                this.fileDialogService.FileName = defaultFileName;
                this.fileDialogService.Filter = UIUtils.GetDialogFilterForExportImport(assemblyToExport.GetType());

                var result = this.fileDialogService.ShowSaveFileDialog();
                if (result == true)
                {
                    filePath = this.fileDialogService.FileName;
                }
            }
            else
            {
                this.folderBrowserService.Description = LocalizedResources.General_ExportFolderBrowserDescription;
                var result = folderBrowserService.ShowFolderBrowserDialog();
                if (result)
                {
                    filePath = folderBrowserService.SelectedPath;
                }
            }

            if (string.IsNullOrEmpty(filePath))
            {
                return;
            }

            foreach (var selectedAssembly in selectedAssemblies)
            {
                var pathToExport = filePath;

                /*
                 * If there are more objects to be exported, we must get 
                 * for each object the path where it will be exported.
                 * Also we must check for file name collisions before export.
                 */
                if (selectedItems.Count > 1)
                {
                    pathToExport = Path.Combine(pathToExport, UIUtils.GetExportObjectFileName(selectedAssembly));
                    pathToExport = PathUtils.FixFilePathForCollisions(pathToExport);
                }

                ExportManager.Instance.Export(
                    selectedAssembly,
                    this.AssemblyDataContext,
                    pathToExport,
                    this.unitsService.Currencies,
                    this.unitsService.BaseCurrency);
            }

            this.WindowService.MessageDialogService.Show(
                selectedAssemblies.Count > 1
                    ? LocalizedResources.ExportImport_ExportItemsWithSuccess
                    : LocalizedResources.ExportImport_ExportSuccess,
                MessageDialogType.Info);
        }

        /// <summary>
        /// Handle the SelectedItems collection changed event.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Collections.Specialized.NotifyCollectionChangedEventArgs"/> 
        /// instance containing the event data.</param>
        private void SelectedItemsOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            this.LoadSelectedAssemblyDetails();

            if (this.IsInViewerMode)
            {
                return;
            }

            /*
             * Synchronizes the SelectedEntitiesInfo collection, 
             * every time an item from subassemblies datagrid is selected/deselected.
             */
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        foreach (var newEntity in e.NewItems)
                        {
                            this.SelectedEntitiesInfo.Add(new EntityInfo(newEntity, this.AssemblyDataContext.DatabaseId));
                        }

                        break;
                    }

                case NotifyCollectionChangedAction.Remove:
                    {
                        foreach (var oldEntity in e.OldItems)
                        {
                            var index = 0;
                            var founded = false;
                            while (!founded && index < this.SelectedEntitiesInfo.Count)
                            {
                                if (this.SelectedEntitiesInfo[index].Entity == oldEntity)
                                {
                                    founded = true;
                                    this.SelectedEntitiesInfo.RemoveAt(index);
                                }

                                index++;
                            }
                        }

                        break;
                    }

                case NotifyCollectionChangedAction.Reset:
                    {
                        this.SelectedEntitiesInfo.Clear();
                        break;
                    }
            }
        }
    }
}