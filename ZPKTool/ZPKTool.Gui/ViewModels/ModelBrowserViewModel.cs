﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using AvalonDock;
using ZPKTool.Business.Export;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Common;
using ZPKTool.Controls;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;
using ZPKTool.Updater;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view model of the model browser.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ModelBrowserViewModel : ProjectsExplorerBaseViewModel
    {
        #region Const and static readonly values

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The list of supported file extensions.
        /// </summary>
        private static readonly List<string> SupportedFileExtensions = new List<string>
        {
            ".part", ".folder", ".project", ".assembly", ".rawpart"
        };

        #endregion

        #region Private members

        /// <summary>
        /// The file dialog service.
        /// </summary>
        private readonly IFileDialogService fileDialogService;

        /// <summary>
        /// Weak event listener for the PropertyChanged notification
        /// </summary>
        private WeakEventListener<PropertyChangedEventArgs> propertyChangedListener;

        /// <summary>
        /// The model file path to browse.
        /// </summary>
        private string modelFilePathToBrowse;

        /// <summary>
        /// Exception in case if the model to be browsed has issues (e.g. it's not supported, invalid,...).
        /// </summary>
        private Exception modelOpeningError;

        /// <summary>
        /// The content in the central part of the main view.
        /// </summary>
        private object content;

        /// <summary>
        /// The title of the docking manager's main document (the document containing the main view content).
        /// </summary>
        private string mainDocumentTitle;

        /// <summary>
        /// The visibility of the content's vertical scroll bar.
        /// </summary>
        private ScrollBarVisibility contentVerticalScrollBarVisibility = ScrollBarVisibility.Auto;

        /// <summary>
        /// The model browser tree title.
        /// </summary>
        private string modelBrowserTreeTitle;

        /// <summary>
        /// The model browser tree icon.
        /// </summary>
        private ImageSource modelBrowserTreeIcon;

        /// <summary>
        /// The weight.
        /// </summary>
        private decimal? weight;

        /// <summary>
        /// The investment.
        /// </summary>
        private decimal? investment;

        /// <summary>
        /// The cost calculation result.
        /// </summary>
        private decimal? cost;

        /// <summary>
        /// The purchase price
        /// </summary>
        private decimal? purchasePrice;

        /// <summary>
        /// The target price
        /// </summary>
        private decimal? targetPrice;

        /// <summary>
        /// A value indicating whether this instance is started directly or from another view.
        /// </summary>
        private bool isStartedDirectly = true;

        /// <summary>
        /// A value indicating whether to display the update available popup in the screen header
        /// </summary>
        private bool showUpdateAvailable = false;

        /// <summary>
        /// True if entity is loading, false otherwise.
        /// </summary>
        private bool isLoadingEntity;

        /// <summary>
        /// The cost visibility.
        /// </summary>
        private Visibility costVisibility;

        /// <summary>
        /// The weight visibility.
        /// </summary>
        private Visibility weightVisibility;

        /// <summary>
        /// The investment visibility.
        /// </summary>
        private Visibility investmentVisibility;

        /// <summary>
        /// A value indicating whether to display the purchase price.
        /// </summary>
        private Visibility purchasePriceVisibility;

        /// <summary>
        /// A value indicating whether to display the target price.
        /// </summary>
        private Visibility targetPriceVisibility;

        /// <summary>
        /// The media provider service.
        /// </summary>
        private IModelBrowserHelperService modelBrowserHelperService;

        /// <summary>
        /// The update manager.
        /// </summary>        
        private UpdateManager updateManager;

        /// <summary>
        /// The default currencies to use when a model does not contain any currency data.
        /// </summary>
        private Collection<Currency> defaultCurrencies;

        /// <summary>
        /// The UI currencies.
        /// </summary>
        private ObservableCollection<Currency> currencies = new ObservableCollection<Currency>();

        /// <summary>
        /// The selected UI currency.
        /// </summary>
        private Currency selectedUICurrency;

        /// <summary>
        /// The current zoom level.
        /// </summary>
        private double selectedZoomLevel;

        /// <summary>
        /// The selected Units System.
        /// </summary>
        private UnitsSystem selectedUnitsSystem;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="ModelBrowserViewModel" /> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        /// <param name="messenger">The messenger.</param>
        /// <param name="compositionContainer">The composition container.</param>
        /// <param name="fileDialogService">The file dialog service.</param>
        /// <param name="modelBrowserHelperService">The media provider service.</param>
        /// <param name="pleaseWaitService">The please wait service</param>
        /// <param name="unitsService">The units service.</param>
        /// <param name="dataService">The data service.</param>
        /// <param name="importService">The import service.</param>
        [ImportingConstructor]
        public ModelBrowserViewModel(
            IWindowService windowService,
            IMessenger messenger,
            CompositionContainer compositionContainer,
            IFileDialogService fileDialogService,
            IModelBrowserHelperService modelBrowserHelperService,
            IPleaseWaitService pleaseWaitService,
            IUnitsService unitsService,
            IProjectsExplorerDataService dataService,
            IImportService importService)
            : base(compositionContainer, messenger, windowService, pleaseWaitService, unitsService, modelBrowserHelperService, dataService, importService)
        {
            this.fileDialogService = fileDialogService;
            this.modelBrowserHelperService = modelBrowserHelperService;
            this.updateManager = this.CompositionContainer.GetExportedValue<ShellController>().UpdateManager;

            // Initialize properties
            this.IsInViewerMode = true;
            this.IsActive = true;
            this.MeasurementUnitsAdapter = this.UnitsService.GetUnitsAdapter(null);

            this.InitializeCommands();
            this.RegisterForEventsAndMessages();
            this.InitializeZoomLevel();

            // Initialize weight and investment display
            this.ComputeHeaderInformationVisibility();

            // Perform the update check.
            if (GlobalSettingsManager.Instance.StartInViewerMode)
            {
                var worker = new BackgroundWorker();
                worker.DoWork += (s, e) => this.updateManager.AutoCheck();
                worker.RunWorkerAsync();
            }
        }

        #region Commands

        /// <summary>
        /// Gets the about command.
        /// </summary>
        public ICommand AboutCommand { get; private set; }

        /// <summary>
        /// Gets the website link command.
        /// </summary>
        public ICommand WebsiteLinkCommand { get; private set; }

        /// <summary>
        /// Gets the open update command.
        /// </summary>
        public ICommand OpenUpdateCommand { get; private set; }

        /// <summary>
        /// Gets the DragOver command.
        /// </summary>
        public ICommand CheckDropTargetCommand { get; private set; }

        /// <summary>
        /// Gets the drop command.
        /// </summary>
        public ICommand DropCommand { get; private set; }

        /// <summary>
        /// Gets the open model command.
        /// </summary>
        public ICommand OpenModelCommand { get; private set; }

        /// <summary>
        /// Gets the hide popup command
        /// </summary>        
        public ICommand HidePopup { get; private set; }

        /// <summary>
        /// Gets the handle manage viewer currencies command.
        /// </summary>
        public ICommand HandleManageViewerCurrenciesCommand { get; private set; }

        /// <summary>
        /// Gets the command executed when the Docking Manager has been loaded.
        /// </summary>
        public ICommand DockingManagerLoadedCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether this instance is loading entity.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is loading entity; otherwise, <c>false</c>.
        /// </value>
        public bool IsLoadingEntity
        {
            get
            {
                return this.isLoadingEntity;
            }

            set
            {
                if (this.isLoadingEntity != value)
                {
                    this.isLoadingEntity = value;
                    OnPropertyChanged(() => this.IsLoadingEntity);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is started directly or from another view.
        /// </summary>
        /// <value><c>true</c> if this instance is started directly; otherwise, <c>false</c>.</value>
        public bool IsStartedDirectly
        {
            get { return this.isStartedDirectly; }
            set { this.SetProperty(ref this.isStartedDirectly, value, () => this.IsStartedDirectly); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to display the update available popup
        /// </summary>
        public bool ShowUpdateAvailable
        {
            get { return this.showUpdateAvailable; }
            set { this.SetProperty(ref this.showUpdateAvailable, value, () => this.ShowUpdateAvailable); }
        }

        /// <summary>
        /// Gets or sets the model file path to browse.
        /// </summary>
        /// <value>
        /// The model file path to browse.
        /// </value>
        public string ModelFilePathToBrowse
        {
            get
            {
                return this.modelFilePathToBrowse;
            }

            set
            {
                this.SetProperty(ref this.modelFilePathToBrowse, value, () => this.ModelFilePathToBrowse);
                this.VerifyAndOpenModel();
            }
        }

        /// <summary>
        /// Gets or sets the model browser tree title.
        /// </summary>
        /// <value>
        /// The model browser tree title.
        /// </value>
        public string ModelBrowserTreeTitle
        {
            get { return this.modelBrowserTreeTitle; }
            set { this.SetProperty(ref this.modelBrowserTreeTitle, value, () => this.ModelBrowserTreeTitle); }
        }

        /// <summary>
        /// Gets or sets the model browser tree icon.
        /// </summary>
        /// <value>
        /// The model browser tree icon.
        /// </value>
        public ImageSource ModelBrowserTreeIcon
        {
            get { return this.modelBrowserTreeIcon; }
            set { this.SetProperty(ref this.modelBrowserTreeIcon, value, () => this.ModelBrowserTreeIcon); }
        }

        /// <summary>
        /// Gets or sets the weight.
        /// </summary>
        /// <value>
        /// The weight.
        /// </value>
        public decimal? Weight
        {
            get
            {
                return this.weight;
            }

            set
            {
                this.SetProperty(ref this.weight, value, () => this.Weight);
                ComputeHeaderInformationVisibility();
            }
        }

        /// <summary>
        /// Gets or sets the cost.
        /// </summary>
        /// <value>
        /// The cost.
        /// </value>
        public decimal? Cost
        {
            get
            {
                return this.cost;
            }

            set
            {
                this.SetProperty(ref this.cost, value, () => this.Cost);
                ComputeHeaderInformationVisibility();
            }
        }

        /// <summary>
        /// Gets or sets the investment.
        /// </summary>
        /// <value>
        /// The investment.
        /// </value>
        public decimal? Investment
        {
            get
            {
                return this.investment;
            }

            set
            {
                this.SetProperty(ref this.investment, value, () => this.Investment);
                ComputeHeaderInformationVisibility();
            }
        }

        /// <summary>
        /// Gets or sets the cost visibility.
        /// </summary>
        /// <value>
        /// The cost visibility.
        /// </value>
        public Visibility CostVisibility
        {
            get { return this.costVisibility; }
            set { this.SetProperty(ref this.costVisibility, value, () => this.CostVisibility); }
        }

        /// <summary>
        /// Gets or sets the weight visibility.
        /// </summary>
        /// <value>
        /// The weight visibility.
        /// </value>
        public Visibility WeightVisibility
        {
            get { return this.weightVisibility; }
            set { this.SetProperty(ref this.weightVisibility, value, () => this.WeightVisibility); }
        }

        /// <summary>
        /// Gets or sets the investment visibility.
        /// </summary>
        /// <value>
        /// The investment visibility.
        /// </value>
        public Visibility InvestmentVisibility
        {
            get { return this.investmentVisibility; }
            set { this.SetProperty(ref this.investmentVisibility, value, () => this.InvestmentVisibility); }
        }

        /// <summary>
        /// Gets or sets the purchase price
        /// </summary>
        public decimal? PurchasePrice
        {
            get { return this.purchasePrice; }
            set { this.SetProperty(ref this.purchasePrice, value, () => this.PurchasePrice, this.ComputeHeaderInformationVisibility); }
        }

        /// <summary>
        /// Gets or sets the target price
        /// </summary>
        public decimal? TargetPrice
        {
            get { return this.targetPrice; }
            set { this.SetProperty(ref this.targetPrice, value, () => this.TargetPrice, this.ComputeHeaderInformationVisibility); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to display the purchase price.
        /// </summary>
        public Visibility PurchasePriceVisibility
        {
            get { return this.purchasePriceVisibility; }
            set { this.SetProperty(ref this.purchasePriceVisibility, value, () => this.PurchasePriceVisibility); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to display the target price.
        /// </summary>
        public Visibility TargetPriceVisibility
        {
            get { return this.targetPriceVisibility; }
            set { this.SetProperty(ref this.targetPriceVisibility, value, () => this.TargetPriceVisibility); }
        }

        /// <summary>
        /// Gets the content in the central part of the main view.
        /// </summary>
        public object Content
        {
            get
            {
                return this.content;
            }

            private set
            {
                if (this.content != value)
                {
                    this.content = value;
                    OnPropertyChanged("Content");
                }
            }
        }

        /// <summary>
        /// Gets or sets the main document title.
        /// </summary>
        /// <value>
        /// The main document title.
        /// </value>
        public string MainDocumentTitle
        {
            get { return this.mainDocumentTitle; }
            set { this.SetProperty(ref this.mainDocumentTitle, value, () => this.MainDocumentTitle); }
        }

        /// <summary>
        /// Gets or sets the content vertical scroll bar visibility.
        /// </summary>
        /// <value>
        /// The content vertical scroll bar visibility.
        /// </value>
        public ScrollBarVisibility ContentVerticalScrollBarVisibility
        {
            get { return this.contentVerticalScrollBarVisibility; }
            set { this.SetProperty(ref this.contentVerticalScrollBarVisibility, value, () => this.ContentVerticalScrollBarVisibility); }
        }

        /// <summary>
        /// Gets or sets the docking manager.
        /// </summary>
        private DockingManager DockingManager { get; set; }

        /// <summary>
        /// Gets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter { get; private set; }

        /// <summary>
        /// Gets or sets the UI currencies.
        /// </summary>
        public ObservableCollection<Currency> UICurrencies
        {
            get { return this.currencies; }
            set { this.SetProperty(ref this.currencies, value, () => this.UICurrencies); }
        }

        /// <summary>
        /// Gets or sets the selected UI currency.
        /// </summary>
        public Currency SelectedUICurrency
        {
            get
            {
                return this.selectedUICurrency;
            }

            set
            {
                if (this.selectedUICurrency != value)
                {
                    this.selectedUICurrency = value;
                    this.OnPropertyChanged(() => this.SelectedUICurrency);

                    // Save the viewer UI currency into the settings.
                    UserSettingsManager.Instance.ViewerUICurrencyIsoCode = value != null ? value.IsoCode : string.Empty;
                    UserSettingsManager.Instance.Save();
                }
            }
        }

        /// <summary>
        /// Gets the items to be listed in the zooming menu.
        /// </summary>
        public ObservableCollection<Tuple<string, double>> ZoomLevels { get; private set; }

        /// <summary>
        /// Gets or sets the selected units system.
        /// </summary>
        public UnitsSystem SelectedUnitsSystem
        {
            get
            {
                return this.selectedUnitsSystem;
            }

            set
            {
                if (this.selectedUnitsSystem != value)
                {
                    this.selectedUnitsSystem = value;
                    this.OnPropertyChanged(() => this.SelectedUnitsSystem);

                    UserSettingsManager.Instance.UnitsSystem = value;
                    UserSettingsManager.Instance.Save();
                }
            }
        }

        /// <summary>
        /// Gets or sets the current zoom level.
        /// </summary>
        public double SelectedZoomLevel
        {
            get
            {
                return this.selectedZoomLevel;
            }

            set
            {
                if (value != this.selectedZoomLevel)
                {
                    this.selectedZoomLevel = value;
                    this.OnPropertyChanged(() => this.SelectedZoomLevel);
                    this.OnZoomLevelChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the current zoom level text.
        /// </summary>
        public string SelectedZoomLevelText
        {
            get
            {
                return this.SelectedZoomLevel.ToString("### %", System.Globalization.CultureInfo.InvariantCulture);
            }

            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    double zoomLevel;
                    if (double.TryParse(value.Replace("%", string.Empty).Trim(), out zoomLevel))
                    {
                        zoomLevel = zoomLevel / 100d;
                        double minZoom = this.ZoomLevels.Min(z => z.Item2);
                        double maxZoom = this.ZoomLevels.Max(z => z.Item2);

                        if (zoomLevel < minZoom)
                        {
                            zoomLevel = minZoom;
                        }

                        if (zoomLevel > maxZoom)
                        {
                            zoomLevel = maxZoom;
                        }

                        this.SelectedZoomLevel = zoomLevel;
                    }
                }
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Called after the view has been loaded. Usually it performs some initialization that needs the view to be loaded, like
        /// starting to load data from a database.
        /// <para/>
        /// During unit tests this method must be manually called because there is no view to call it.
        /// </summary>
        public override void OnLoaded()
        {
            base.OnLoaded();
            DisplayErrorMessage();
        }

        /// <summary>
        /// Called after the view has been unloaded from the parent or closed.
        /// </summary>
        public override void OnUnloaded()
        {
            base.OnUnloaded();

            this.Messenger.Unregister(this);
            this.CleanViewerModeCacheFolder();

            if (IsStartedDirectly)
            {
                Application.Current.Shutdown();
            }
            else
            {
                WindowService.CloseViewWindow(this);
            }
        }

        /// <summary>
        /// Called before the main view is unloaded.
        /// </summary>
        /// <returns>False if the main view unload should be canceled, true if it should continue.</returns>
        public override bool OnUnloading()
        {
            return this.UnloadMainDocumentContent();
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Initializes the application zoom.
        /// </summary>
        private void InitializeZoomLevel()
        {
            this.ZoomLevels = new ObservableCollection<Tuple<string, double>>();
            this.ZoomLevels.Add(new Tuple<string, double>(" 50 %", 0.50d));
            this.ZoomLevels.Add(new Tuple<string, double>(" 75 %", 0.75d));
            this.ZoomLevels.Add(new Tuple<string, double>(" 90 %", 0.90d));
            this.ZoomLevels.Add(new Tuple<string, double>(" 95 %", 0.95d));
            this.ZoomLevels.Add(new Tuple<string, double>("100 %", 1.00d));
            this.ZoomLevels.Add(new Tuple<string, double>("105 %", 1.05d));
            this.ZoomLevels.Add(new Tuple<string, double>("110 %", 1.10d));
            this.ZoomLevels.Add(new Tuple<string, double>("125 %", 1.25d));
            this.ZoomLevels.Add(new Tuple<string, double>("150 %", 1.50d));
            this.ZoomLevels.Add(new Tuple<string, double>("200 %", 2.00d));

            double minZoom = this.ZoomLevels.Min(z => z.Item2);
            double maxZoom = this.ZoomLevels.Max(z => z.Item2);

            if (UserSettingsManager.Instance.ZoomLevel < minZoom)
            {
                UserSettingsManager.Instance.ZoomLevel = minZoom;
            }
            else if (UserSettingsManager.Instance.ZoomLevel > maxZoom)
            {
                UserSettingsManager.Instance.ZoomLevel = maxZoom;
            }

            this.SelectedZoomLevel = UserSettingsManager.Instance.ZoomLevel != 0d ? UserSettingsManager.Instance.ZoomLevel : 1.0d;
        }

        /// <summary>
        /// Called when the SelectedZoomLevel property has changed.
        /// </summary>
        private void OnZoomLevelChanged()
        {
            // save selected zoom level in settings
            UserSettingsManager.Instance.ZoomLevel = this.SelectedZoomLevel;

            UserSettingsManager.Instance.Save();
        }

        /// <summary>
        /// Handles messages from the messenger service.
        /// </summary>
        /// <param name="message">The message.</param>
        private void ReceiveMessage(NotificationMessage message)
        {
            if (message.Notification == Notification.ShowUpdateAvailable
                && GlobalSettingsManager.Instance.StartInViewerMode)
            {
                this.ShowUpdateAvailable = true;
            }
            else if (message.Notification == Notification.HideUpdateAvailable)
            {
                this.ShowUpdateAvailable = false;
            }
            else if (message.Notification == Notification.LoadModelInViewer)
            {
                var modelPathUpdatedMessage = (NotificationMessage<string>)message;
                if (!string.IsNullOrWhiteSpace(modelPathUpdatedMessage.Content))
                {
                    if (!string.IsNullOrWhiteSpace(this.ModelFilePathToBrowse))
                    {
                        MessageDialogResult result = this.WindowService.MessageDialogService.Show(LocalizedResources.Question_ReplacePCMViewerModel, MessageDialogType.YesNo);
                        if (result != MessageDialogResult.Yes)
                        {
                            return;
                        }
                    }

                    this.ModelFilePathToBrowse = modelPathUpdatedMessage.Content;
                }
            }
        }

        /// <summary>
        /// Handler for the check drop target command, checks if the dragged item can be dropped.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        private void CheckModelBrowserDropTarget(DragEventArgs e)
        {
            e.Handled = true;
            var sourcePath = string.Empty;

            var droppedData = e.Data.GetData(DataFormats.FileDrop);
            var sourcePaths = droppedData as IEnumerable<string>;

            sourcePath = sourcePaths == null ? droppedData as string :
                sourcePaths.Count().Equals(1) ? sourcePaths.ElementAt(0) : string.Empty;

            e.Effects = !string.IsNullOrEmpty(sourcePath) && this.IsFileExtensionSupported(sourcePath) ? DragDropEffects.Copy : DragDropEffects.None;
        }

        /// <summary>
        /// Handler for the drop action, performs the drop operation.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
        private void HandleModelBrowserDrop(DragEventArgs e)
        {
            e.Handled = true;
            var sourcePath = string.Empty;

            var droppedData = e.Data.GetData(DataFormats.FileDrop);
            var sourcePaths = droppedData as IEnumerable<string>;

            sourcePath = sourcePaths == null ? droppedData as string :
                sourcePaths.Count().Equals(1) ? sourcePaths.ElementAt(0) : string.Empty;

            if (!string.IsNullOrEmpty(sourcePath) && this.IsFileExtensionSupported(sourcePath))
            {
                if (!string.IsNullOrEmpty(this.ModelFilePathToBrowse))
                {
                    MessageDialogResult result = this.WindowService.MessageDialogService.Show(LocalizedResources.Question_ReplacePCMViewerModel, MessageDialogType.YesNo);
                    if (result != MessageDialogResult.Yes)
                    {
                        return;
                    }
                }

                this.ModelFilePathToBrowse = sourcePath;
            }
        }

        /// <summary>
        /// Cleans the media folder.
        /// </summary>
        private void CleanViewerModeCacheFolder()
        {
            try
            {
                if (Directory.Exists(Constants.ViewerModeCacheFolderPath))
                {
                    Directory.Delete(Constants.ViewerModeCacheFolderPath, true);
                }
            }
            catch (Exception ex)
            {
                Log.WarnException("Error occurred while clearing the media folder.", ex);
            }
        }

        /// <summary>
        /// Handles the PropertyChanged event of the Settings
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void SettingsPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "HeaderDisplayWeight" || e.PropertyName == "HeaderDisplayInvestment" || e.PropertyName == "HeaderDisplayCalculationResult")
            {
                ComputeHeaderInformationVisibility();
            }
        }

        /// <summary>
        /// Refreshes the cost, weight and investment.
        /// </summary>
        /// <param name="result">The result.</param>
        private void RefreshCost(CalculationResult result)
        {
            if (result == null)
            {
                this.Cost = null;
                this.Weight = null;
                this.Investment = null;
                this.PurchasePrice = null;
                this.TargetPrice = null;
            }
            else
            {
                this.Cost = result.TotalCost;
                this.Weight = result.Weight;
                this.Investment = result.InvestmentCostCumulated;
                this.TargetPrice = result.TargetPrice;
                this.PurchasePrice = result.PurchasePrice;
            }
        }

        /// <summary>
        /// Computes whether to display the weight and investment.
        /// </summary>
        private void ComputeHeaderInformationVisibility()
        {
            this.WeightVisibility = UserSettingsManager.Instance.HeaderDisplayWeight && this.Weight.HasValue ? Visibility.Visible : Visibility.Collapsed;
            this.InvestmentVisibility = UserSettingsManager.Instance.HeaderDisplayInvestment && this.Investment.HasValue ? Visibility.Visible : Visibility.Collapsed;
            this.CostVisibility = UserSettingsManager.Instance.HeaderDisplayCalculationResult && this.Cost.HasValue ? Visibility.Visible : Visibility.Collapsed;
            this.TargetPriceVisibility = UserSettingsManager.Instance.HeaderDisplaysTargetPrice && this.TargetPrice.HasValue ? Visibility.Visible : Visibility.Collapsed;
            this.PurchasePriceVisibility = UserSettingsManager.Instance.HeaderDisplaysPurchasePrice && this.PurchasePrice.HasValue ? Visibility.Visible : Visibility.Collapsed;
        }

        /// <summary>
        /// Displays the error message.
        /// </summary>
        private void DisplayErrorMessage()
        {
            if (this.modelOpeningError != null && IsLoaded)
            {
                this.WindowService.MessageDialogService.Show(this.modelOpeningError);
                this.modelOpeningError = null;
            }
        }

        /// <summary>
        /// Handles the loading of the Docking Manager.
        /// </summary>
        /// <param name="eventArgs">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void HandleDockingManagerLoaded(RoutedEventArgs eventArgs)
        {
            this.DockingManager = eventArgs.Source as DockingManager;

            if (this.DockingManager == null)
            {
                throw new InvalidOperationException("The Docking manager instance was null.");
            }

            this.HandleTreeItemSelection(this.Items.FirstOrDefault());
        }

        /// <summary>
        /// Handles requests to load content into the main view.
        /// </summary>
        /// <param name="loadRequest">The load request.</param>
        private void HandleLoadContentRequests(LoadContentInMainViewMessage loadRequest)
        {
            ContentVerticalScrollBarVisibility = loadRequest.AllowVerticalScrolling ? ScrollBarVisibility.Auto : ScrollBarVisibility.Disabled;
            LoadMainDocumentContent(loadRequest.Content);

            if (loadRequest.Title != null)
            {
                MainDocumentTitle = loadRequest.Title;
            }
        }

        /// <summary>
        /// Loads the specified content in the Main Document.
        /// Use this method if the previous content was already removed.
        /// </summary>
        /// <param name="newContent">The content to load.</param>
        private void LoadMainDocumentContent(object newContent)
        {
            // Add the new content
            this.Content = newContent;

            ILifeCycleManager lfcMngr = this.Content as ILifeCycleManager;
            if (lfcMngr != null)
            {
                lfcMngr.OnLoaded();
            }

            this.SelectMainDocumentContent();
        }

        /// <summary>
        /// Triggers the "Unloading"/"OnUnloading" event of the current content of the Main Document.
        /// </summary>
        /// <remarks>DO NOT CALL THIS METHOD UNLESS YOU FULLY UNDERSTAND WHAT UNLOADING A VIEW-MODEL OR EDIT CONTROL MEANS</remarks>
        /// <returns>True if the unload was successful; otherwise false.</returns>
        private bool UnloadMainDocumentContent()
        {
            // DO NOT CALL THIS METHOD UNLESS YOU FULLY UNDERSTAND WHAT UNLOADING A VIEW-MODEL OR EDIT CONTROL MEANS
            var isUnloadSuccessfull = true;

            ILifeCycleManager lfcMngr = this.Content as ILifeCycleManager;
            if (lfcMngr != null)
            {
                isUnloadSuccessfull = lfcMngr.OnUnloading();
            }

            return isUnloadSuccessfull;
        }

        /// <summary>
        /// Selects the content of the main document.
        /// </summary>
        private void SelectMainDocumentContent()
        {
            var documents = this.DockingManager.MainDocumentPane.Items.OfType<DocumentContent>();
            var mainDocumentContent = documents.FirstOrDefault(p => p.Name.Equals("MainDocumentContent"));
            this.DockingManager.MainDocumentPane.SelectedItem = mainDocumentContent;
        }

        /// <summary>
        /// Handles the open model.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void HandleOpenModel(RoutedEventArgs e)
        {
            this.fileDialogService.Reset();
            this.fileDialogService.Filter = LocalizedResources.DialogFilter_ModelFiles;
            this.fileDialogService.Multiselect = true;
            this.fileDialogService.FilterIndex = 0;
            var result = this.fileDialogService.ShowOpenFileDialog();

            if (result == true)
            {
                ModelFilePathToBrowse = fileDialogService.FileName;
            }
        }

        /// <summary>
        /// Determines whether the specified file path has a supported file extension.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <returns>
        ///   <c>true</c> if the specified file path has a supported file extension; otherwise, <c>false</c>.
        /// </returns>
        private bool IsFileExtensionSupported(string filePath)
        {
            var fileExtension = Path.GetExtension(filePath);
            if (!string.IsNullOrWhiteSpace(filePath))
            {
                foreach (var supportedFileExtension in SupportedFileExtensions)
                {
                    // Check if the file extension is equal to one of the supported extensions (ignoring the case).
                    if (fileExtension.Equals(supportedFileExtension, StringComparison.InvariantCultureIgnoreCase))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Verifies the model.
        /// </summary>
        private void VerifyAndOpenModel()
        {
            IsLoadingEntity = true;
            ImportedData<object> importedData = null;

            Task.Factory.StartNew(() =>
            {
                // Note: if the application reached this point it means that the file exists since this check is made in the App.xaml.cs.
                if (IsFileExtensionSupported(ModelFilePathToBrowse))
                {
                    importedData = ExportManager.Instance.ImportModelInMemory(ModelFilePathToBrowse);
                }
                else
                {
                    modelOpeningError = new ZPKException(ErrorCodes.ImportedEntityNotSupportedInView);
                    Log.Warn("The file type is not supported to be viewed. File: " + ModelFilePathToBrowse);
                }
            }).ContinueWith(
                task =>
                {
                    IsLoadingEntity = false;

                    if (importedData != null)
                    {
                        modelOpeningError = importedData.Error;

                        if (modelOpeningError == null && importedData.Entity != null)
                        {
                            // Adjust the imported model's yearly production quantity to be the yearly production quantity exported with it
                            // Initialize the model browser helper service to use the imported model data.
                            this.AdjustModelYearlyProductionQuantity(importedData);
                            this.modelBrowserHelperService.SetCurrentModelData(importedData);

                            // Initialize the default currencies to undo any changes made to them when a previous model was opened/used.
                            this.InitializeDefaultCurrencies();

                            Currency baseCurrency = null;
                            ICollection<Currency> currencies = null;
                            if (importedData.Currencies.Count > 0)
                            {
                                currencies = importedData.Currencies;
                                baseCurrency = importedData.BaseCurrency;
                                if (baseCurrency == null)
                                {
                                    baseCurrency = currencies.FirstOrDefault(c => c.IsoCode == Constants.DefaultCurrencyIsoCode);
                                }
                            }
                            else
                            {
                                currencies = this.defaultCurrencies;
                                baseCurrency = currencies.FirstOrDefault(c => c.IsoCode == Constants.DefaultCurrencyIsoCode);
                            }

                            this.MeasurementUnitsAdapter.Currencies = currencies;
                            this.MeasurementUnitsAdapter.BaseCurrency = baseCurrency;

                            this.UnitsService.Currencies = currencies;
                            this.UnitsService.BaseCurrency = baseCurrency;

                            var selectedCurrencyCode = this.SelectedUICurrency != null ? this.SelectedUICurrency.IsoCode : UserSettingsManager.Instance.ViewerUICurrencyIsoCode;
                            this.UICurrencies.Clear();
                            this.UICurrencies.AddRange(currencies.OrderBy(c => c.Name));

                            // If the previously selected UI currency still exists select it, else select the default currency.                            
                            this.SelectedUICurrency = this.UICurrencies.FirstOrDefault(c => c.IsoCode == selectedCurrencyCode);
                            if (this.SelectedUICurrency == null)
                            {
                                this.SelectedUICurrency = this.UICurrencies.FirstOrDefault(c => c.IsoCode == Constants.DefaultCurrencyIsoCode);
                            }

                            var rootTreeItem = GetTreeItemForModel(importedData.Entity);
                            if (rootTreeItem != null)
                            {
                                Items.Clear();
                                Items.Add(rootTreeItem);

                                rootTreeItem.IsExpanded = true;
                                rootTreeItem.IsSelected = true;
                            }
                            else
                            {
                                Log.Error("Could not create tree item for object type: " + importedData.Entity.GetType());
                            }
                        }
                    }

                    DisplayErrorMessage();
                },
                TaskScheduler.FromCurrentSynchronizationContext());
        }

        /// <summary>
        /// Copies the YearlyProductionQuantity from the imported data metadata into the imported model, if the value is available (this value was exported with the model),
        /// in order to maintain the same cost calculation results.
        /// <remarks>
        /// This is necessary in order to obtain the same cost the model had in the application when the model had a parent model in the application and in
        /// the parent's process, this model had a positive rejection cost (a number of model pieces were rejected). This caused the model's
        /// YearlyProductionQuantity to increase beyond the target value (ex: if the target was 10.000 pcs, the value including rejected parts may be 10.500 pcs).
        /// The YearlyProductionQuantity including rejected pieces was exported with the model and we have to use it here to obtain the same calculation result as in the application.
        /// </remarks>
        /// </summary>
        /// <param name="importedModelData">The imported model's data.</param>
        private void AdjustModelYearlyProductionQuantity(ImportedData<object> importedModelData)
        {
            if (importedModelData.CostCalculationData != null
                && importedModelData.CostCalculationData.YearlyProductionQuantity.HasValue)
            {
                try
                {
                    var yearlyProductionQty = Convert.ToInt32(importedModelData.CostCalculationData.YearlyProductionQuantity.Value);

                    var assy = importedModelData.Entity as Assembly;
                    if (assy != null)
                    {
                        assy.YearlyProductionQuantity = yearlyProductionQty;
                    }
                    else
                    {
                        var part = importedModelData.Entity as Part;
                        if (part != null)
                        {
                            part.YearlyProductionQuantity = yearlyProductionQty;
                        }
                        else
                        {
                            // Handle other models that have a Yearly Production Quantity.
                        }
                    }
                }
                catch (OverflowException)
                {
                    Log.Error("An overflow error occurred when converting the exported YearlyProductionQuantity from decimal to int. The exported YearlyProductionQuantity will not be used.");
                }
            }
        }

        /// <summary>
        /// Gets the tree item for model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns>The specific <see cref="TreeViewDataItem"/> for the given object, or NULL if it was NULL.</returns>
        private TreeViewDataItem GetTreeItemForModel(object model)
        {
            TreeViewDataItem resultTreeViewDataItem = null;

            if (model == null)
            {
                return null;
            }

            var typeOfModel = model.GetType();

            if (typeOfModel == typeof(ProjectFolder))
            {
                resultTreeViewDataItem = new ProjectFolderTreeItem((ProjectFolder)model, DbIdentifier.NotSet, null);
                ModelBrowserTreeIcon = Images.FolderIcon;
                ModelBrowserTreeTitle = LocalizedResources.General_ProjectFolder;
            }
            else if (typeOfModel == typeof(Project))
            {
                resultTreeViewDataItem = new ProjectTreeItem((Project)model, DbIdentifier.NotSet, null);
                ModelBrowserTreeIcon = Images.ProjectIcon;
                ModelBrowserTreeTitle = LocalizedResources.General_Project;
            }
            else if (typeOfModel == typeof(Assembly))
            {
                resultTreeViewDataItem = new AssemblyTreeItem((Assembly)model, DbIdentifier.NotSet, false, null);
                ModelBrowserTreeIcon = Images.AssemblyIcon;
                ModelBrowserTreeTitle = LocalizedResources.General_Assembly;
            }
            else if (typeOfModel == typeof(Part))
            {
                resultTreeViewDataItem = new PartTreeItem((Part)model, DbIdentifier.NotSet, false, null);
                ModelBrowserTreeIcon = Images.PartIcon;
                ModelBrowserTreeTitle = LocalizedResources.General_Part;
            }
            else if (typeOfModel == typeof(RawPart))
            {
                resultTreeViewDataItem = new RawPartTreeItem((RawPart)model, DbIdentifier.NotSet, false, null);
                ModelBrowserTreeIcon = Images.RawPartIcon;
                ModelBrowserTreeTitle = LocalizedResources.General_RawPart;
            }
            else if (typeOfModel == typeof(RawMaterial))
            {
                resultTreeViewDataItem = new RawMaterialTreeItem((RawMaterial)model, DbIdentifier.NotSet);
                ModelBrowserTreeIcon = Images.RawMaterialIcon;
                ModelBrowserTreeTitle = LocalizedResources.General_RawMaterial;
            }
            else if (typeOfModel == typeof(Commodity))
            {
                resultTreeViewDataItem = new CommodityTreeItem((Commodity)model, DbIdentifier.NotSet);
                ModelBrowserTreeIcon = Images.CommodityIcon;
                ModelBrowserTreeTitle = LocalizedResources.General_Commodity;
            }
            else if (typeOfModel == typeof(Machine))
            {
                resultTreeViewDataItem = new MachineTreeItem((Machine)model, DbIdentifier.NotSet);
                ModelBrowserTreeIcon = Images.MachineIcon;
                ModelBrowserTreeTitle = LocalizedResources.General_Machine;
            }
            else if (typeOfModel == typeof(Consumable))
            {
                resultTreeViewDataItem = new ConsumableTreeItem((Consumable)model, DbIdentifier.NotSet);
                ModelBrowserTreeIcon = Images.ConsumableIcon;
                ModelBrowserTreeTitle = LocalizedResources.General_Consumable;
            }
            else if (typeOfModel == typeof(Die))
            {
                resultTreeViewDataItem = new DieTreeItem((Die)model, DbIdentifier.NotSet);
                ModelBrowserTreeIcon = Images.DieIcon;
                ModelBrowserTreeTitle = LocalizedResources.General_Die;
            }
            else
            {
                Log.Warn(string.Format(CultureInfo.InvariantCulture, "Could not create tree item for object type {0} in the ModelBrowserViewModel.", model.GetType()));
            }

            return resultTreeViewDataItem;
        }

        /// <summary>
        /// Handles the messages of type NotificationMessageWithCallback and derived types (like NotificationMessageWithAction).
        /// </summary>
        /// <param name="message">The message.</param>
        private void HandleNotificationsWithCallback(NotificationMessageWithCallback message)
        {
            if (message.Notification == Notification.MainViewNotifyContentToUnload)
            {
                // Trigger the unload of the main document's content and return the result to the sender.
                var isUnloaded = this.UnloadMainDocumentContent();
                message.ExecuteCallback(isUnloaded);
            }
            else if (message.Notification == Notification.MainViewGetContent)
            {
                // Return the main document's content to the sender.
                message.ExecuteCallback(Content);
            }
        }

        /// <summary>
        /// Handles NavigateToEntityMessage messages.
        /// </summary>
        /// <param name="message">The message.</param>
        private void HandleNavigateToEntityMessage(NavigateToEntityMessage message)
        {
            if (message.Entity == null)
            {
                return;
            }

            Action action = () => this.NavigateToEntity(message.Entity);
            this.Dispatcher.Invoke(action, DispatcherPriority.Loaded);
        }

        /// <summary>
        /// Navigates in the Projects Tree to the item corresponding to a specified entity and selects it.
        /// </summary>
        /// <param name="entity">The entity to select to.</param>
        private void NavigateToEntity(object entity)
        {
            // Determine the root item where to start the search.
            var navRootItem = Items.FirstOrDefault();

            if (navRootItem != null)
            {
                this.NavigateToEntityCommon(navRootItem, entity, DbIdentifier.NotSet, null);
            }
            else
            {
                Log.Error("Could not determine the Projects Tree root item where to start the navigation.");
                this.WindowService.MessageDialogService.Show(LocalizedResources.Error_GoToError, MessageDialogType.Error);
            }
        }

        /// <summary>
        /// The action performed by the About command.
        /// </summary>
        private void AboutAction()
        {
            var viewModel = new AboutViewModel(this.WindowService);
            this.WindowService.ShowViewInDialog(viewModel);
        }

        /// <summary>
        /// Opens the website link.
        /// </summary>
        private void OpenWebsiteLink()
        {
            System.Diagnostics.Process.Start("http://productcostmanager.com/");
        }

        /// <summary>
        /// The action performed by the OpenUpdate command.
        /// </summary>
        private void OpenUpdateAction()
        {
            var viewModel = this.CompositionContainer.GetExportedValue<UpdateApplicationViewModel>();
            this.WindowService.ShowViewInDialog(viewModel);
        }

        /// <summary>
        /// Handle the UI currencies.
        /// </summary>
        private void HandleUICurrencies()
        {
            var calculatorVM = this.CompositionContainer.GetExportedValue<ManageViewerCurrenciesViewModel>();
            calculatorVM.InitializeCurrencies(this.UICurrencies);
            this.WindowService.ShowViewInDialog(calculatorVM, "ManageViewerCurrenciesTemplate");

            // Set the modified currencies into the adapter and units service.
            this.UnitsService.Currencies = this.UICurrencies;
            this.UnitsService.BaseCurrency = this.UICurrencies.FirstOrDefault(c => c.IsSameAs(this.UnitsService.BaseCurrency));
            this.MeasurementUnitsAdapter.Currencies = this.UICurrencies;
            this.MeasurementUnitsAdapter.BaseCurrency = this.UICurrencies.FirstOrDefault(c => c.IsSameAs(this.MeasurementUnitsAdapter.BaseCurrency));

            // Populate the currency selector with the modified currencies.
            var selectedUICurrencyCode = this.SelectedUICurrency.IsoCode;
            this.UICurrencies = new ObservableCollection<Currency>(this.UICurrencies);
            this.SelectedUICurrency = null;
            this.SelectedUICurrency = this.UICurrencies.FirstOrDefault(c => c.IsoCode == selectedUICurrencyCode);
        }

        #endregion

        #region Initialization

        /// <summary>
        /// Initializes the commands.
        /// </summary>
        private void InitializeCommands()
        {
            this.DockingManagerLoadedCommand = new DelegateCommand<RoutedEventArgs>(this.HandleDockingManagerLoaded);
            this.OpenModelCommand = new DelegateCommand<RoutedEventArgs>(HandleOpenModel);
            this.HidePopup = new DelegateCommand(() => this.ShowUpdateAvailable = false);
            this.AboutCommand = new DelegateCommand(this.AboutAction);
            this.WebsiteLinkCommand = new DelegateCommand(this.OpenWebsiteLink);
            this.OpenUpdateCommand = new DelegateCommand(this.OpenUpdateAction);
            this.CheckDropTargetCommand = new DelegateCommand<DragEventArgs>(this.CheckModelBrowserDropTarget);
            this.DropCommand = new DelegateCommand<DragEventArgs>(this.HandleModelBrowserDrop);
            this.HandleManageViewerCurrenciesCommand = new DelegateCommand(this.HandleUICurrencies);
        }

        /// <summary>
        /// Registers event listeners and message handlers for the events and messages that this instance must handle from its creation.
        /// </summary>
        private void RegisterForEventsAndMessages()
        {
            this.Messenger.Register<NotificationMessage>(ReceiveMessage);
            this.Messenger.Register<LoadContentInMainViewMessage>(this.HandleLoadContentRequests, GlobalMessengerTokens.ModelBrowserTargetToken);
            this.Messenger.Register<NotificationMessageWithCallback>(this.HandleNotificationsWithCallback, GlobalMessengerTokens.ModelBrowserTargetToken, true);
            this.Messenger.Register<NavigateToEntityMessage>(this.HandleNavigateToEntityMessage, GlobalMessengerTokens.ModelBrowserTargetToken);
            this.Messenger.Register<CurrentComponentCostChangedMessage>(msg => RefreshCost(msg.Content), GlobalMessengerTokens.ModelBrowserTargetToken);

            this.propertyChangedListener = new WeakEventListener<PropertyChangedEventArgs>(SettingsPropertyChanged);
            PropertyChangedEventManager.AddListener(UserSettingsManager.Instance, propertyChangedListener, ReflectionUtils.GetPropertyName(() => UserSettingsManager.Instance.HeaderDisplayWeight));
            PropertyChangedEventManager.AddListener(UserSettingsManager.Instance, propertyChangedListener, ReflectionUtils.GetPropertyName(() => UserSettingsManager.Instance.HeaderDisplayInvestment));
            PropertyChangedEventManager.AddListener(UserSettingsManager.Instance, propertyChangedListener, ReflectionUtils.GetPropertyName(() => UserSettingsManager.Instance.HeaderDisplayCalculationResult));
        }

        /// <summary>
        /// Initializes the default currencies that are used when a model does not have currency data.
        /// </summary>
        private void InitializeDefaultCurrencies()
        {
            this.defaultCurrencies = new Collection<Currency>();
            this.defaultCurrencies.Add(new Currency { Name = "Euro", IsoCode = "EUR", Symbol = "€", ExchangeRate = 1m });
            this.defaultCurrencies.Add(new Currency { Name = "British Pound", IsoCode = "GBP", Symbol = "£", ExchangeRate = 0.84195m });
            this.defaultCurrencies.Add(new Currency { Name = "US Dollar", IsoCode = "USD", Symbol = "$", ExchangeRate = 1.31025m });
            this.defaultCurrencies.Add(new Currency { Name = "Chinese Yuan", IsoCode = "CNY", Symbol = "CNY", ExchangeRate = 8.13389m });
            this.defaultCurrencies.Add(new Currency { Name = "Indian rupees", IsoCode = "INR", Symbol = "INR", ExchangeRate = 70.3714m });
            this.defaultCurrencies.Add(new Currency { Name = "Japanese yean", IsoCode = "JPY", Symbol = "JPY", ExchangeRate = 130.03m });
            this.defaultCurrencies.Add(new Currency { Name = "Russian Ruble", IsoCode = "RUB", Symbol = "RUB", ExchangeRate = 40.6864m });
            this.defaultCurrencies.Add(new Currency { Name = "South Korean Won", IsoCode = "KRW", Symbol = "KRW", ExchangeRate = 1422.79m });
            this.defaultCurrencies.Add(new Currency { Name = "Swedish Krona", IsoCode = "SEK", Symbol = "SEK", ExchangeRate = 8.54048m });
        }

        #endregion Initialization

        #region Message handling
        #endregion Message handling
    }
}