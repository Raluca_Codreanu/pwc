﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Input;
using ZPKTool.Business;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the IMyView view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class CommodityViewModel : ViewModel<Commodity, IDataSourceManager>
    {
        #region Attributes

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The messenger.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The model browser helper service.
        /// </summary>
        private IModelBrowserHelperService modelBrowserHelperService;

        /// <summary>
        /// The commodity cost
        /// </summary>
        private decimal commodityCost;

        /// <summary>
        /// The parent of the commodity model
        /// </summary>
        private object commodityParent;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        /// <summary>
        /// The UnitsAdapter handler, used to perform certain operations when some UnitsAdapter properties are updated.
        /// </summary>
        private UnitsAdapterUpdateHandler unitsAdapterHandler;

        #endregion Attributes

        /// <summary>
        /// Initializes a new instance of the <see cref="CommodityViewModel"/> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        /// <param name="messenger">The messenger.</param>
        /// <param name="modelBrowserHelperService">The model browser helper service.</param>
        /// <param name="costRecalculationCloneManager">The cost recalculation clone manager.</param>
        /// <param name="manufacturerViewModel">The manufacturer view model.</param>
        /// <param name="masterDataBrowser">The master data browser.</param>
        /// <param name="mediaViewModel">The media view model.</param>
        /// <param name="unitsService">The units service.</param>
        [ImportingConstructor]
        public CommodityViewModel(
            IWindowService windowService,
            IMessenger messenger,
            IModelBrowserHelperService modelBrowserHelperService,
            ICostRecalculationCloneManager costRecalculationCloneManager,
            ManufacturerViewModel manufacturerViewModel,
            MasterDataBrowserViewModel masterDataBrowser,
            MediaViewModel mediaViewModel,
            IUnitsService unitsService)
        {
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("manufacturerViewModel", manufacturerViewModel);
            Argument.IsNotNull("masterDataBrowser", masterDataBrowser);
            Argument.IsNotNull("mediaViewModel", mediaViewModel);
            Argument.IsNotNull("unitsService", unitsService);
            Argument.IsNotNull("costRecalculationCloneManager", costRecalculationCloneManager);

            this.windowService = windowService;
            this.messenger = messenger;
            this.modelBrowserHelperService = modelBrowserHelperService;
            this.ManufacturerViewModel = manufacturerViewModel;
            this.MasterDataBrowser = masterDataBrowser;
            this.MediaViewModel = mediaViewModel;
            this.MediaViewModel.Mode = MediaControlMode.SingleImage;
            this.MediaViewModel.IsChild = true;
            this.unitsService = unitsService;
            this.CloneManager = costRecalculationCloneManager;

            // Initialize the manufacturer view-model
            this.ManufacturerViewModel.ShowSaveControls = false;
            this.ManufacturerViewModel.IsChild = true;

            this.Amount.ValueChanged += (s, e) => this.OnAmountChanged();
            this.Price.ValueChanged += (s, e) => this.OnPriceChanged();

            // Initialize the commands
            this.SaveNestedViewModels = new CompositeCommand();
            this.SaveNestedViewModels.RegisterCommand(this.ManufacturerViewModel.SaveToModelCommand);
            this.SaveNestedViewModels.RegisterCommand(this.MediaViewModel.SaveToModelCommand);

            this.CancelNestedViewModels = new CompositeCommand();
            this.CancelNestedViewModels.RegisterCommand(this.ManufacturerViewModel.CancelCommand);
            this.CancelNestedViewModels.RegisterCommand(this.MediaViewModel.CancelCommand);
            this.BrowseMasterDataCommand = new DelegateCommand(BrowseMasterData);

            this.ManufacturerViewModel.UndoManager = this.UndoManager;
            this.MediaViewModel.UndoManager = this.UndoManager;
        }

        #region Commands

        /// <summary>
        /// Gets the browse master data command.
        /// </summary>
        public ICommand BrowseMasterDataCommand { get; private set; }

        /// <summary>
        /// Gets or sets the command that save all changes in the nested view models back into their models.
        /// This command aggregates the SaveToModel commands of the nested view models.
        /// </summary>
        private CompositeCommand SaveNestedViewModels { get; set; }

        /// <summary>
        /// Gets or sets the command that cancels all changes of the nested view models.
        /// This command aggregates the Cancel commands of the nested view models.
        /// </summary>
        private CompositeCommand CancelNestedViewModels { get; set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets the name of the commodity.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Name", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Name")]
        public DataProperty<string> Name { get; private set; }

        /// <summary>
        /// Gets the commodity part number.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("PartNumber")]
        public DataProperty<string> PartNumber { get; private set; }

        /// <summary>
        /// Gets the commodity price.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Price", AffectsCost = true)]
        public DataProperty<decimal?> Price { get; private set; }

        /// <summary>
        /// Gets the commodity reject ratio.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("RejectRatio", AffectsCost = true)]
        public DataProperty<decimal?> RejectRatio { get; private set; }

        /// <summary>
        /// Gets the commodity amount.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Amount", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Amount", AffectsCost = true)]
        public DataProperty<int?> Amount { get; private set; }

        /// <summary>
        /// Gets the commodity remarks.
        /// </summary>
        /// <value>The remarks.</value>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Remarks")]
        public DataProperty<string> Remarks { get; private set; }

        /// <summary>
        /// Gets the commodity weight.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Weight", AffectsCost = true)]
        public DataProperty<decimal?> Weight { get; private set; }

        /// <summary>
        /// Gets the commodity weight measurement unit.
        /// </summary>
        [UndoableProperty]
        [ExposesModelProperty("WeightUnitBase")]
        public DataProperty<MeasurementUnit> WeightUnit { get; private set; }

        /// <summary>
        /// Gets the manufacturer view model.
        /// </summary>
        public ManufacturerViewModel ManufacturerViewModel { get; private set; }

        /// <summary>
        /// Gets the master data browser.
        /// </summary>
        public MasterDataBrowserViewModel MasterDataBrowser { get; private set; }

        /// <summary>
        /// Gets the media view model.
        /// </summary>
        public MediaViewModel MediaViewModel { get; private set; }

        /// <summary>
        /// Gets or sets the commodity cost
        /// </summary>
        public decimal CommodityCost
        {
            get { return this.commodityCost; }
            set { this.SetProperty(ref this.commodityCost, value, () => this.CommodityCost); }
        }

        /// <summary>
        /// Gets or sets the commodity parent
        /// </summary>
        public object CommodityParent
        {
            get { return this.commodityParent; }
            set { this.SetProperty(ref this.commodityParent, value, () => this.CommodityParent); }
        }

        /// <summary>
        /// Gets or sets the project to which the Model belongs.
        /// This property is needed during cost calculations so it must be set when editing (it can be omitted when creating).
        /// </summary>
        public Project ParentProject { get; set; }

        /// <summary>
        /// Gets or sets the message token to be used when sending messages with token 
        /// </summary>
        public string MessageToken { get; set; }

        /// <summary>
        /// Gets or sets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter { get; set; }

        #endregion Properties

        /// <summary>
        /// Initializes the view-model for the creation of a new commodity.
        /// The ModelDataContext property must be set to a valid value before this call.
        /// </summary>
        /// <param name="isMasterData">If set to true the commodity will be created in master data; in this case the parent should be set to null.</param>
        /// <param name="parent">
        /// The parent object of the commodity that will be created. If this is set, the value of the <paramref name="isMasterData"/> argument is ignored and
        /// the parent's IsMasterData flag is used instead.
        /// </param>
        /// <remarks>
        /// This is a helper method for populating the model with default data for creation. It is not mandatory to use it; you can set the model to a new
        /// instance set up however you want.
        /// </remarks>
        /// <exception cref="ArgumentException">
        /// <paramref name="parent"/> is null and <paramref name="isMasterData"/> is false, or the type of<paramref name="parent"/> is not supported.
        /// </exception>
        /// <exception cref="InvalidOperationException">The provided <paramref name="parent"/> object type is not supported.</exception>
        public void InitializeForCreation(bool isMasterData, object parent)
        {
            this.CheckDataSource();
            this.EditMode = ViewModelEditMode.Create;

            Commodity newCommodity = new Commodity();

            if (!isMasterData)
            {
                if (parent == null)
                {
                    throw new ArgumentException("A non master data commodity cannot be created without a parent", "parent");
                }

                Part parentPart = parent as Part;
                if (parentPart != null)
                {
                    newCommodity.Part = parentPart;
                    isMasterData = parentPart.IsMasterData;
                }
                else
                {
                    AssemblyProcessStep parentStep = parent as AssemblyProcessStep;
                    if (parentStep != null)
                    {
                        newCommodity.ProcessStep = parentStep;
                        isMasterData = parentStep.IsMasterData;
                    }
                    else
                    {
                        throw new InvalidOperationException(string.Format("An object of type '{0}' is not supported as commodity parent.", parent.GetType().Name));
                    }
                }
            }

            newCommodity.Manufacturer = new Manufacturer();

            // Set the master data flag and the owner at the end so they are applied to all sub-objects.
            newCommodity.SetIsMasterData(isMasterData);
            if (!newCommodity.IsMasterData)
            {
                User owner = this.DataSourceManager.UserRepository.GetById(SecurityManager.Instance.CurrentUser.Guid, false);
                newCommodity.SetOwner(owner);
            }

            this.Model = newCommodity;
        }

        /// <summary>
        /// Called when the Model has changed.
        /// </summary>
        protected override void OnModelChanged()
        {
            this.CheckDataSource();
            base.OnModelChanged();

            if (this.IsInViewerMode)
            {
                this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(null);
            }

            if (this.Model.WeightUnitBase == null)
            {
                // Set default unit of the weight in case it is missing.
                this.WeightUnit.Value = this.MeasurementUnitsAdapter.GetBaseMeasurementUnit(MeasurementUnitType.Weight);

                // Reset the changed status.
                this.WeightUnit.AcceptChanges();
                this.IsChanged = false;
            }

            this.ManufacturerViewModel.DataSourceManager = this.DataSourceManager;
            this.ManufacturerViewModel.Model = this.Model.Manufacturer ?? new Manufacturer();

            this.MediaViewModel.DataSourceManager = this.DataSourceManager;
            this.MediaViewModel.Model = this.Model;

            this.CalculateConsumableCost();
            this.UndoManager.Start();

            if (!this.IsChild)
            {
                this.CloneManager.Clone(this);
            }
        }

        /// <summary>
        /// Called when the DataSourceManager changed.
        /// </summary>
        protected override void OnDataSourceManagerChanged()
        {
            base.OnDataSourceManagerChanged();

            this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(this.DataSourceManager);
            this.unitsAdapterHandler = new UnitsAdapterUpdateHandler(this.MeasurementUnitsAdapter);
            this.unitsAdapterHandler.PauseUndoOnUnitsAdapterUpdate(this.UndoManager);
        }

        #region Property change handlers

        /// <summary>
        /// Called when the Amount property has changed.
        /// </summary>
        private void OnAmountChanged()
        {
            this.CalculateConsumableCost();
        }

        /// <summary>
        /// Called when the Price property has changed.
        /// </summary>
        private void OnPriceChanged()
        {
            this.CalculateConsumableCost();
        }

        #endregion

        /// <summary>
        /// Opens the master data browser to select a commodity.
        /// </summary>
        private void BrowseMasterData()
        {
            this.MasterDataBrowser.MasterDataType = typeof(Commodity);
            this.MasterDataBrowser.MasterDataSelected += this.OnMasterDataCommoditySelected;
            this.windowService.ShowViewInDialog(this.MasterDataBrowser);
            this.MasterDataBrowser.MasterDataSelected -= this.OnMasterDataCommoditySelected;
        }

        /// <summary>
        /// Called when a master data commodity is selected from the master data browser.
        /// </summary>
        /// <param name="masterDataEntity">The master data entity selected in the browser.</param>
        /// <param name="databaseId">The source database of the selected master data.</param>
        private void OnMasterDataCommoditySelected(object masterDataEntity, DbIdentifier databaseId)
        {
            Commodity masterCommodity = masterDataEntity as Commodity;
            if (masterCommodity == null)
            {
                return;
            }

            this.StopRecalculationNotifications();
            using (this.UndoManager.StartBatch(undoEachBatch: true))
            {
                // Retrieve the media of the commodity master data.
                var dsm = DataAccessFactory.CreateDataSourceManager(databaseId);
                MediaManager mediaManager = new MediaManager(dsm);
                var mediaList = new List<Media>();
                var media = mediaManager.GetPictureOrVideo(masterCommodity);
                if (media != null && (MediaType)media.Type != MediaType.Document)
                {
                    mediaList.Add(media.Copy());
                }

                // Load the commodity master data media in Media view-model.
                this.MediaViewModel.LoadMedia(mediaList);

                // Convert the master data entity currency values.
                var currencies = dsm.CurrencyRepository.GetBaseCurrencies();
                var baseCurrencyFromMD = currencies.FirstOrDefault(c => c.IsSameAs(this.MeasurementUnitsAdapter.BaseCurrency));
                CurrencyConversionManager.ConvertObject(masterCommodity, baseCurrencyFromMD, CurrencyConversionManager.DefaultBaseCurrency);

                // Load the data from the master commodity and sub-objects in the corresponding view-models. Use empty data for null sub-objects.
                this.LoadDataFromModel(masterCommodity);

                // Obtain the unit from the data context of the model.
                if (this.WeightUnit.Value != null)
                {
                    this.WeightUnit.Value = this.DataSourceManager.MeasurementUnitRepository.GetByName(this.WeightUnit.Value.Name);
                }

                this.ManufacturerViewModel.LoadDataFromModel(masterCommodity.Manufacturer ?? new Manufacturer());
            }

            this.ResumeRecalculationNotifications(true);
        }

        #region Cost handling

        /// <summary>
        /// Calculate commodity Cost.
        /// </summary>
        private void CalculateConsumableCost()
        {
            Commodity updatedCommodity = new Commodity();
            updatedCommodity.Price = this.Price.Value;
            updatedCommodity.Amount = this.Amount.Value;

            string costCalculationVersion = null;
            if (this.CommodityParent is Part)
            {
                Part part = (Part)this.CommodityParent;
                costCalculationVersion = part.CalculationVariant;
            }
            else if (this.CommodityParent is Assembly)
            {
                Assembly assy = (Assembly)this.CommodityParent;
                costCalculationVersion = assy.CalculationVariant;
            }
            else
            {
                costCalculationVersion = CostCalculatorFactory.LatestVersion;
            }

            ICostCalculator calculator = CostCalculatorFactory.GetCalculator(costCalculationVersion);
            CommodityCost cost = calculator.CalculateCommodityCost(updatedCommodity, new CommodityCostCalculationParameters());

            this.CommodityCost = cost.Cost;
        }

        /// <summary>
        /// Refreshes the commodity parent's cost and notifies listeners.
        /// </summary>
        private void RefreshParentCost()
        {
            this.RefreshCalculations(this.CommodityParent);
        }

        #endregion

        #region Save/Cancel

        /// <summary>
        /// Saves all changes back into the model.
        /// </summary>
        protected override void SaveToModel()
        {
            this.CheckModel();

            base.SaveToModel();

            var manufacturer = this.ManufacturerViewModel.Model;
            if (manufacturer != null)
            {
                this.Model.Manufacturer = manufacturer;
            }
        }

        /// <summary>
        /// Determines whether this instance can perform the save operation. Executed by the SaveCommand.
        /// The default implementation allows the save to be performed if the input is valid.
        /// </summary>
        /// <returns>
        /// true if the save operation can be performed, false otherwise.
        /// </returns>
        protected override bool CanSave()
        {
            return base.CanSave() && this.SaveNestedViewModels.CanExecute(null);
        }

        /// <summary>
        /// Performs the save operation. Executed by the SaveCommand.
        /// </summary>
        protected override void Save()
        {
            this.CheckModelAndDataSource();

            // Save all changes back into the Model objects. The validity check of this operation is performed by the CanSave method.
            this.SaveToModel();
            this.SaveNestedViewModels.Execute(null);

            if (this.SavesToDataSource)
            {
                // Update commodity
                this.DataSourceManager.CommodityRepository.Save(this.Model);
                this.DataSourceManager.SaveChanges();

                // Notify the other components that the commodity was created/updated.
                EntityChangedMessage message = this.Model.IsMasterData ?
                    new EntityChangedMessage(Notification.MasterDataEntityChanged) :
                    new EntityChangedMessage(Notification.MyProjectsEntityChanged);

                message.ChangeType = this.EditMode == ViewModelEditMode.Create ? EntityChangeType.EntityCreated : EntityChangeType.EntityUpdated;
                message.Entity = this.Model;
                if (this.Model.Part != null)
                {
                    message.Parent = this.Model.Part;
                }
                else
                {
                    message.Parent = this.Model.ProcessStep;
                }

                this.SendMessage(message, false);
            }

            this.RefreshParentCost();

            // Close the view-model when is displayed in a window.            
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Determines whether the Cancel operation can be performed. Executed by the CancelCommand.
        /// </summary>
        /// <returns>
        /// true if the changes can be canceled, false otherwise.
        /// </returns>
        protected override bool CanCancel()
        {
            return base.CanCancel() && this.CancelNestedViewModels.CanExecute(null);
        }

        /// <summary>
        /// Cancels all changes. Executed by the CancelCommand.
        /// </summary>
        protected override void Cancel()
        {
            if (!this.CanCancel())
            {
                return;
            }

            if (this.IsChanged)
            {
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_CancelChanges, MessageDialogType.YesNo);
                if (result != MessageDialogResult.Yes)
                {
                    // Don't cancel the changes and also don't close the view-model.
                    return;
                }
                else
                {
                    using (this.UndoManager.StartBatch())
                    {
                        // Cancel all changes
                        base.Cancel();
                        this.CancelNestedViewModels.Execute(null);
                    }
                }
            }

            // Close the view-model when is displayed in a window.            
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Called before unloading the view from its parent. Returning false will cancel the view's unloading.
        /// </summary>
        /// <returns>
        /// True if the unloading process should continue and false if it should be canceled.
        /// </returns>
        public override bool OnUnloading()
        {
            // Do nothing if the view-model is read-only, in viewer mode, it was not changed or the model was deleted.
            if (this.IsReadOnly || this.IsInViewerMode || !this.IsChanged || this.Model.IsDeleted)
            {
                return true;
            }

            if (this.EditMode == ViewModelEditMode.Create)
            {
                // Ask the user to confirm quitting
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_Quit, MessageDialogType.YesNo);
                if (result != MessageDialogResult.Yes)
                {
                    // The user chose to stay on the screen; return false to stop the screen unloading.
                    return false;
                }
                else
                {
                    this.IsChanged = false;
                }
            }
            else if (this.EditMode == ViewModelEditMode.Edit)
            {
                // Ask the user if he wants to save
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_UnsavedDataOnQuit, MessageDialogType.YesNoCancel);
                if (result == MessageDialogResult.Yes)
                {
                    // The user whishes to save but it is not possible because the input is not valid. Return false to stop the view-model from unloading.
                    if (!this.CanSave())
                    {
                        return false;
                    }

                    this.Save();
                }
                else if (result == MessageDialogResult.No)
                {
                    // The user does not want to save.                    
                    this.IsChanged = false;
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Called after one or more model properties value changed in order to refresh the calculations.
        /// </summary>
        protected override void RefreshCalculation()
        {
            if (this.EditMode == ViewModelEditMode.Create)
            {
                return;
            }

            var parenPart = this.ModelParentClone as Part;
            if (parenPart != null)
            {
                this.RefreshCalculations(parenPart);
            }
            else
            {
                var parentAssembly = this.ModelParentClone as Assembly;
                if (parentAssembly != null)
                {
                    this.RefreshCalculations(parentAssembly);
                }
            }
        }

        #endregion

        /// <summary>
        /// Calculates the parent and sends a message with the result.
        /// </summary>
        /// <param name="entityParent">The entity parent.</param>
        private void RefreshCalculations(object entityParent)
        {
            if (entityParent == null)
            {
                return;
            }

            var parentPart = entityParent as Part;
            if (parentPart != null)
            {
                PartCostCalculationParameters calculationParams = null;
                if (this.ParentProject != null)
                {
                    calculationParams = CostCalculationHelper.CreatePartParamsFromProject(this.ParentProject);
                }
                else if (IsInViewerMode)
                {
                    // Note: theoretically this part is never reached because it's not possible to save in view mode.
                    calculationParams = this.modelBrowserHelperService.GetPartCostCalculationParameters();
                }

                if (calculationParams != null)
                {
                    var calculator = CostCalculatorFactory.GetCalculator(parentPart.CalculationVariant);
                    var result = calculator.CalculatePartCost(parentPart, calculationParams);
                    this.SendMessage(new CurrentComponentCostChangedMessage(result), true);
                }
            }
            else
            {
                var parentAssembly = entityParent as Assembly;
                if (parentAssembly != null)
                {
                    AssemblyCostCalculationParameters calculationParams = null;
                    if (this.ParentProject != null)
                    {
                        calculationParams = CostCalculationHelper.CreateAssemblyParamsFromProject(this.ParentProject);
                    }
                    else if (IsInViewerMode)
                    {
                        // Note: theoretically this part is never reached because it's not possible to save in view mode.
                        calculationParams = this.modelBrowserHelperService.GetAssemblyCostCalculationParameters();
                    }

                    if (calculationParams != null)
                    {
                        var calculator = CostCalculatorFactory.GetCalculator(parentAssembly.CalculationVariant);
                        var result = calculator.CalculateAssemblyCost(parentAssembly, calculationParams);
                        this.SendMessage(new CurrentComponentCostChangedMessage(result), true);
                    }
                }
            }
        }

        /// <summary>
        /// Sends a message with or without a token
        /// </summary>
        /// <typeparam name="TMessage">The message type</typeparam>
        /// <param name="message">The message to send</param>
        /// <param name="sendWithToken">A value indicating whether to send the message with a token or send it globally without one</param>
        private void SendMessage<TMessage>(TMessage message, bool sendWithToken)
        {
            if (sendWithToken)
            {
                if (this.MessageToken == null)
                {
                    this.messenger.Send(message, this.IsInViewerMode ? GlobalMessengerTokens.ModelBrowserTargetToken : GlobalMessengerTokens.MainViewTargetToken);
                }
                else
                {
                    this.messenger.Send(message, this.MessageToken);
                }
            }
            else
            {
                this.messenger.Send(message);
            }
        }
    }
}