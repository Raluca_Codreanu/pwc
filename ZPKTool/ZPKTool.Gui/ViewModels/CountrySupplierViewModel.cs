﻿using System;
using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Resources;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the SuppliersView.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]

    public class CountrySupplierViewModel : ViewModel<CountryState, IDataSourceManager>
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The country settings view model.
        /// </summary>
        private CountrySettingsViewModel countrySettingsViewModel;

        #endregion Attributes

        /// <summary>
        /// Initializes a new instance of the <see cref="CountrySupplierViewModel" /> class.
        /// </summary>
        /// <param name="countrySettingsViewModel">The country settings view model.</param>
        /// <param name="windowService">The window service.</param>
        [ImportingConstructor]
        public CountrySupplierViewModel(
            CountrySettingsViewModel countrySettingsViewModel,
            IWindowService windowService)
        {
            Argument.IsNotNull("countrySettingsViewModel", countrySettingsViewModel);
            Argument.IsNotNull("windowService", windowService);

            this.CountrySettingsViewModel = countrySettingsViewModel;
            this.windowService = windowService;
            this.ShowCancelMessageFlag = true;

            this.CountrySettingsViewModel.UndoManager = this.UndoManager;
        }

        #region Properties

        /// <summary>
        /// Gets the supplier`s name.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Name", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Name")]
        public DataProperty<string> Name { get; private set; }

        /// <summary>
        /// Gets the CountrySettingsViewModel.
        /// </summary>
        public CountrySettingsViewModel CountrySettingsViewModel
        {
            get { return this.countrySettingsViewModel; }
            private set { this.SetProperty(ref this.countrySettingsViewModel, value, () => this.CountrySettingsViewModel); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the show cancel message will appear.
        /// </summary>
        public bool ShowCancelMessageFlag { get; set; }

        #endregion Properties

        /// <summary>
        /// Initializes this instance for creating a new Country Supplier.
        /// </summary>
        /// <param name="parentCountry">The country for which the supplier will be created.</param>
        /// <exception cref="System.ArgumentNullException">Argument 'parentCountry' can not be null.</exception>
        public void InitializeForCreation(Country parentCountry)
        {
            if (parentCountry == null)
            {
                throw new ArgumentNullException("parentCountry", "Argument 'parentCountry' can not be null.");
            }

            var supplier = new CountryState();
            supplier.CountrySettings = parentCountry.CountrySetting.Copy();
            supplier.Country = parentCountry;

            this.EditMode = ViewModelEditMode.Create;
            this.Model = supplier;
        }

        /// <summary>
        /// Called when the Model has changed.
        /// </summary>        
        protected override void OnModelChanged()
        {
            this.CheckDataSource();
            base.OnModelChanged();

            this.CountrySettingsViewModel.SourceCountryName = this.Model.Name;
            this.CountrySettingsViewModel.CheckForUpdate = false;            
            this.CountrySettingsViewModel.DataSourceManager = this.DataSourceManager;
            this.CountrySettingsViewModel.Model = this.Model.CountrySettings;

            this.IsChanged = false;
            this.UndoManager.Start();
            this.UndoManager.Reset();
        }

        #region Save/Cancel

        /// <summary>
        /// Determines whether this instance can perform the save operation. Executed by the SaveCommand.
        /// The default implementation allows the save to be performed if the input is valid.
        /// </summary>
        /// <returns>
        /// true if the save operation can be performed, false otherwise.
        /// </returns>
        protected override bool CanSave()
        {
            return base.CanSave() && this.CountrySettingsViewModel.SaveToModelCommand.CanExecute(null);
        }

        /// <summary>
        /// Performs the save operation. Executed by the SaveCommand.
        /// </summary>
        protected override void Save()
        {
            this.CheckModelAndDataSource();

            // When the fields are modified, but the name is the same, the changes can be made.
            bool supplierExists = false;
            if (this.EditMode == ViewModelEditMode.Create || this.Name.IsChanged)
            {
                supplierExists = this.DataSourceManager.CountrySupplierRepository.CheckIfExists(this.Name.Value);
            }

            if (supplierExists)
            {
                this.windowService.MessageDialogService.Show(LocalizedResources.Supplier_Exists, MessageDialogType.Error);
            }
            else
            {
                this.SaveToModel();
                this.CountrySettingsViewModel.SetLastChangeTimestamp();
                this.CountrySettingsViewModel.SaveToModelCommand.Execute(null);

                if (this.SavesToDataSource)
                {
                    this.DataSourceManager.CountrySupplierRepository.Save(this.Model);
                    this.DataSourceManager.SaveChanges();
                }

                this.windowService.CloseViewWindow(this);
            }
        }

        /// <summary>
        /// Determines whether the Cancel operation can be performed. Executed by the CancelCommand.
        /// </summary>
        /// <returns>
        /// true if the changes can be canceled, false otherwise.
        /// </returns>
        protected override bool CanCancel()
        {
            return base.CanCancel() && this.CountrySettingsViewModel.CancelCommand.CanExecute(null);
        }

        /// <summary>
        /// Cancels all changes. Executed by the CancelCommand.
        /// </summary>
        protected override void Cancel()
        {
            if (!this.CanCancel())
            {
                return;
            }

            if (this.IsChanged && this.ShowCancelMessageFlag)
            {
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_CancelChanges, MessageDialogType.YesNo);
                if (result != MessageDialogResult.Yes)
                {
                    // Don't cancel the changes and also don't close the view-model.
                    return;
                }
                else
                {
                    using (this.UndoManager.StartBatch())
                    {
                        // Cancel all changes.
                        base.Cancel();
                        this.CountrySettingsViewModel.CancelCommand.Execute(null);
                    }
                }
            }

            // Close the view-model when is displayed in a window.
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Called before unloading the view from its parent. Returning false will cancel the view's unloading.
        /// </summary>
        /// <returns>
        /// <c>True</c> if the unload was successful; <c>False</c> otherwise.
        /// </returns>
        public override bool OnUnloading()
        {
            // Do nothing if the view-model is read-only, in viewer mode or it was not changed.
            if (this.IsReadOnly || this.IsInViewerMode || !this.IsChanged)
            {
                return true;
            }

            if (this.EditMode == ViewModelEditMode.Create)
            {
                // Ask the user to confirm quitting
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_Quit, MessageDialogType.YesNo);
                if (result != MessageDialogResult.Yes)
                {
                    // The user chose to stay on the screen; return false to stop the screen unloading.
                    return false;
                }
                else
                {
                    this.IsChanged = false;
                }
            }
            else if (this.EditMode == ViewModelEditMode.Edit)
            {
                // Ask the user if he wants to save
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_UnsavedDataOnQuit, MessageDialogType.YesNoCancel);
                if (result == MessageDialogResult.Yes)
                {
                    // The user whishes to save but it is not possible because the input is not valid. Return false to stop the view-model from unloading.
                    if (!this.CanSave())
                    {
                        return false;
                    }

                    this.Save();
                }
                else if (result == MessageDialogResult.No)
                {
                    // The user does not want to save.                    
                    this.IsChanged = false;
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        #endregion Save/Cancel
    }
}
