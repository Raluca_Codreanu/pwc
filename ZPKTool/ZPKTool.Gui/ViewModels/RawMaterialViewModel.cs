﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Windows.Input;
using ZPKTool.Business;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for Raw Material management.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class RawMaterialViewModel : ViewModel<RawMaterial, IDataSourceManager>
    {
        #region Attributes

        /// <summary>
        /// The units system changed listener.
        /// </summary>
        private readonly WeakEventListener<PropertyChangedEventArgs> unitsSystemChangedListener;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The messenger.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The model browser helper service.
        /// </summary>
        private IModelBrowserHelperService modelBrowserHelperService;

        /// <summary>
        /// The calculation version to use when calculating for display the raw material's cost.
        /// </summary>
        private string costCalculationVersion;

        /// <summary>
        /// A value indicating the collection of RawMaterialDeliveryTypes available.
        /// </summary>
        private Collection<RawMaterialDeliveryType> deliveryTypes;

        /// <summary>
        ///  A value indicating the RawMaterial cost. 
        /// </summary>
        private decimal? cost;

        /// <summary>
        /// The RawMaterial Price Symbol.
        /// </summary>
        private string priceSymbol;

        /// <summary>
        /// A value indicating RawMaterial PartWeight MaxDecimalPlaces.
        /// </summary>
        private int partWeightMaxDecimalPlaces;

        /// <summary>
        /// A value indicating RawMaterial Quantity MaxDecimalPlaces.
        /// </summary>
        private int quantityMaxDecimalPlaces;

        /// <summary>
        /// A value indicating whether or not to store stock.
        /// </summary>
        private bool stockKeepingState;

        /// <summary>
        /// A value indicating RawMaterial Sprue MaxContentLimit.
        /// </summary>
        private decimal sprueMaxContentLimit;

        /// <summary>
        /// The RawMaterial ClassificationSelectorViewModel.
        /// </summary>
        private ClassificationSelectorViewModel materialClassificationsView;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        /// <summary>
        /// The UnitsAdapter handler, used to perform certain operations when some UnitsAdapter properties are updated.
        /// </summary>
        private UnitsAdapterUpdateHandler unitsAdapterHandler;

        #endregion Attributes

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="RawMaterialViewModel"/> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        /// <param name="messenger">The messenger.</param>
        /// <param name="modelBrowserHelperService">The model browser helper service.</param>
        /// <param name="costRecalculationCloneManager">The cost recalculation clone manager.</param>
        /// <param name="manufacturerViewModel">The manufacturer view model.</param>
        /// <param name="masterDataBrowser">The master data browser.</param>
        /// <param name="mediaViewModel">The media view model.</param>
        /// <param name="classificationSelectorVM">The ClassificationSelector view model.</param>
        /// <param name="unitsService">The units service.</param>
        [ImportingConstructor]
        public RawMaterialViewModel(
            IWindowService windowService,
            IMessenger messenger,
            IModelBrowserHelperService modelBrowserHelperService,
            ICostRecalculationCloneManager costRecalculationCloneManager,
            ManufacturerViewModel manufacturerViewModel,
            MasterDataBrowserViewModel masterDataBrowser,
            MediaViewModel mediaViewModel,
            ClassificationSelectorViewModel classificationSelectorVM,
            IUnitsService unitsService)
        {
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("manufacturerViewModel", manufacturerViewModel);
            Argument.IsNotNull("masterDataBrowser", masterDataBrowser);
            Argument.IsNotNull("mediaViewModel", mediaViewModel);
            Argument.IsNotNull("unitsService", unitsService);
            Argument.IsNotNull("costRecalculationCloneManager", costRecalculationCloneManager);

            this.windowService = windowService;
            this.messenger = messenger;
            this.modelBrowserHelperService = modelBrowserHelperService;
            this.ManufacturerViewModel = manufacturerViewModel;
            this.MasterDataBrowser = masterDataBrowser;
            this.MaterialClassificationsView = classificationSelectorVM;
            this.MaterialClassificationsView.ClassificationType = typeof(MaterialsClassification);
            this.MediaViewModel = mediaViewModel;
            this.MediaViewModel.Mode = MediaControlMode.SingleImage;
            this.MediaViewModel.IsChild = true;
            this.unitsService = unitsService;
            this.CloneManager = costRecalculationCloneManager;

            // Initialize the manufacturer view-model
            this.ManufacturerViewModel.ShowSaveControls = false;
            this.ManufacturerViewModel.IsChild = true;

            // Initialize the commands
            this.SaveNestedViewModels = new CompositeCommand();
            this.SaveNestedViewModels.RegisterCommand(this.ManufacturerViewModel.SaveToModelCommand);
            this.SaveNestedViewModels.RegisterCommand(this.MediaViewModel.SaveToModelCommand);
            this.CancelNestedViewModels = new CompositeCommand();
            this.CancelNestedViewModels.RegisterCommand(this.ManufacturerViewModel.CancelCommand);
            this.CancelNestedViewModels.RegisterCommand(this.MediaViewModel.CancelCommand);
            this.CancelNestedViewModels.RegisterCommand(this.MaterialClassificationsView.CancelCommand);
            this.BrowseMasterDataCommand = new DelegateCommand(BrowseMasterData);

            this.InitializeProperties();
            this.InitializeUndoManager();

            this.unitsSystemChangedListener = new WeakEventListener<PropertyChangedEventArgs>(this.HandleUnitsSystemChanged);
            PropertyChangedEventManager.AddListener(
                UserSettingsManager.Instance,
                unitsSystemChangedListener,
                ReflectionUtils.GetPropertyName(() => UserSettingsManager.Instance.UnitsSystem));
        }

        #endregion Constructor

        #region Commands

        /// <summary>
        /// Gets the browse master data command.
        /// </summary>
        public ICommand BrowseMasterDataCommand { get; private set; }

        /// <summary>
        /// Gets or sets the command that save all changes in the nested view models back into their models.
        /// This command aggregates the SaveToModel commands of the nested view models.
        /// </summary>
        private CompositeCommand SaveNestedViewModels { get; set; }

        /// <summary>
        /// Gets or sets the command that cancels all changes of the nested view models.
        /// This command aggregates the Cancel commands of the nested view models.
        /// </summary>
        private CompositeCommand CancelNestedViewModels { get; set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets the name of the RawMaterial.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Name", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Name")]
        public DataProperty<string> Name { get; private set; }

        /// <summary>
        /// Gets the uk name of the RawMaterial.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("NameUK")]
        public DataProperty<string> NameUK { get; private set; }

        /// <summary>
        /// Gets the us name of the RawMaterial.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("NameUS")]
        public DataProperty<string> NameUS { get; private set; }

        /// <summary>
        /// Gets the Norm Name of the RawMaterial.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("NormName")]
        public DataProperty<string> NormName { get; private set; }

        /// <summary>
        /// Gets the RawMaterial price.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Price", AffectsCost = true)]
        public DataProperty<decimal?> Price { get; private set; }

        /// <summary>
        /// Gets the RawMaterial quantity.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Quantity", AffectsCost = true)]
        [MaterialWeightValidator]
        public DataProperty<decimal?> Quantity { get; private set; }

        /// <summary>
        /// Gets the RawMaterial sprue.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Sprue", AffectsCost = true)]
        public DataProperty<decimal?> Sprue { get; private set; }

        /// <summary>
        /// Gets the RawMaterial sprue value.
        /// </summary>
        public decimal SprueValue
        {
            get
            {
                return this.PartWeight.Value.GetValueOrDefault() * this.Sprue.Value.GetValueOrDefault();
            }
        }

        /// <summary>
        /// Gets the RawMaterial loss.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Loss", AffectsCost = true)]
        public DataProperty<decimal?> Loss { get; private set; }

        /// <summary>
        /// Gets the RawMaterial Loss value.
        /// </summary>
        public decimal LossValue
        {
            get
            {
                return this.Quantity.Value.GetValueOrDefault() * this.Loss.Value.GetValueOrDefault();
            }
        }

        /// <summary>
        /// Gets the RawMaterial stock keeping.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("StockKeeping", AffectsCost = true)]
        public DataProperty<decimal?> StockKeeping { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether to store stock or not.
        /// </summary>
        [UndoableProperty]
        public bool StockKeepingState
        {
            get
            {
                return this.stockKeepingState;
            }

            set
            {
                if (this.stockKeepingState != value)
                {
                    OnPropertyChanging(() => this.StockKeepingState);
                    using (this.UndoManager.StartBatch(includePreviousItem: true, reverseUndoOrder: false, navigateToBatchControls: true))
                    {
                        if (!value)
                        {
                            // Reset the stock keeping value if the stock keeping is disabled.
                            this.StopRecalculationNotifications();
                            this.StockKeeping.Value = null;
                            this.ResumeRecalculationNotifications(true);
                        }
                    }

                    this.stockKeepingState = value;
                    OnPropertyChanged(() => this.StockKeepingState);
                }
            }
        }

        /// <summary>
        /// Gets the RawMaterial scrap refund ratio.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("ScrapRefundRatio", AffectsCost = true)]
        public DataProperty<decimal?> ScrapRefundRatio { get; private set; }

        /// <summary>
        /// Gets the RawMaterial reject ratio.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("RejectRatio", AffectsCost = true)]
        public DataProperty<decimal?> RejectRatio { get; private set; }

        /// <summary>
        /// Gets the RawMaterial recycling ratio.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("RecyclingRatio", AffectsCost = true)]
        public DataProperty<decimal?> RecyclingRatio { get; private set; }

        /// <summary>
        /// Gets the RawMaterial remarks.
        /// </summary>
        /// <value>The remarks.</value>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Remarks")]
        public DataProperty<string> Remarks { get; private set; }

        /// <summary>
        /// Gets the RawMaterial parent weight.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("ParentWeight", AffectsCost = true)]
        public DataProperty<decimal?> PartWeight { get; private set; }

        /// <summary>
        /// Gets the RawMaterial delivery type.
        /// </summary>
        [UndoableProperty]
        [ExposesModelProperty("DeliveryType")]
        public DataProperty<RawMaterialDeliveryType> DeliveryType { get; private set; }

        /// <summary>
        /// Gets or sets the collection of RawMaterialDeliveryTypes available.
        /// </summary>
        public Collection<RawMaterialDeliveryType> DeliveryTypes
        {
            get { return this.deliveryTypes; }
            set { this.SetProperty(ref this.deliveryTypes, value, () => this.DeliveryTypes); }
        }

        /// <summary>
        /// Gets the RawMaterial ScrapCalculation type.
        /// </summary>
        [UndoableProperty]
        [ExposesModelProperty("ScrapCalculationType")]
        public DataProperty<ScrapCalculationType> ScrapCalculationSelectedType { get; private set; }

        /// <summary>
        /// Gets the RawMaterial yield strength.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("YieldStrength")]
        public DataProperty<string> YieldStrength { get; private set; }

        /// <summary>
        /// Gets the RawMaterial rupture strength.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("RuptureStrength")]
        public DataProperty<string> RuptureStrength { get; private set; }

        /// <summary>
        /// Gets the RawMaterial density.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Density")]
        public DataProperty<string> Density { get; private set; }

        /// <summary>
        /// Gets the RawMaterial max elongation.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("MaxElongation")]
        public DataProperty<string> MaxElongation { get; private set; }

        /// <summary>
        /// Gets the RawMaterial glass transition temperature.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("GlassTransitionTemperature")]
        public DataProperty<string> GlassTransitionTemperature { get; private set; }

        /// <summary>
        /// Gets the RawMaterial Rx.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Rx")]
        public DataProperty<string> Rx { get; private set; }

        /// <summary>
        /// Gets the RawMaterial Rm.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Rm")]
        public DataProperty<string> Rm { get; private set; }

        /// <summary>
        /// Gets the RawMaterial Weight Unit.
        /// </summary>
        [UndoableProperty]
        public VMProperty<MeasurementUnit> MaterialWeightUnit { get; private set; }

        /// <summary>
        /// Gets the RawMaterial Quantity WeightUnit.
        /// </summary>
        [UndoableProperty]
        public VMProperty<MeasurementUnit> QuantityWeightUnit { get; private set; }

        /// <summary>
        /// Gets or sets the RawMaterial Classifications view-model.
        /// </summary>
        public ClassificationSelectorViewModel MaterialClassificationsView
        {
            get { return this.materialClassificationsView; }
            set { this.SetProperty(ref this.materialClassificationsView, value, () => this.MaterialClassificationsView); }
        }

        /// <summary>
        /// Gets or sets the RawMaterial PartWeightMaxDecimalPlaces.
        /// </summary>
        public int PartWeightMaxDecimalPlaces
        {
            get { return this.partWeightMaxDecimalPlaces; }
            set { this.SetProperty(ref this.partWeightMaxDecimalPlaces, value, () => this.PartWeightMaxDecimalPlaces); }
        }

        /// <summary>
        /// Gets or sets the RawMaterial QuantityMaxDecimalPlaces.
        /// </summary>
        public int QuantityMaxDecimalPlaces
        {
            get { return this.quantityMaxDecimalPlaces; }
            set { this.SetProperty(ref this.quantityMaxDecimalPlaces, value, () => this.QuantityMaxDecimalPlaces); }
        }

        /// <summary>
        /// Gets or sets the RawMaterial Sprue MaxContentLimit.
        /// </summary>
        public decimal SprueMaxContentLimit
        {
            get { return this.sprueMaxContentLimit; }
            set { this.SetProperty(ref this.sprueMaxContentLimit, value, () => this.SprueMaxContentLimit); }
        }

        /// <summary>
        /// Gets or sets the RawMaterial cost.
        /// </summary>
        public decimal? Cost
        {
            get { return this.cost; }
            set { this.SetProperty(ref this.cost, value, () => this.Cost); }
        }

        /// <summary>
        /// Gets or sets the calculation version.
        /// </summary>
        /// <value>The calculation version.</value>
        public string CostCalculationVersion
        {
            get
            {
                return this.costCalculationVersion;
            }

            set
            {
                this.costCalculationVersion = value;
                this.UpdateCost();
            }
        }

        /// <summary>
        /// Gets or sets the Price symbol.
        /// </summary>
        public string PriceSymbol
        {
            get { return this.priceSymbol; }
            set { this.SetProperty(ref this.priceSymbol, value, () => this.PriceSymbol); }
        }

        /// <summary>
        /// Gets the RawMaterial process material.
        /// </summary>
        public decimal ProcessMaterial
        {
            get
            {
                return this.Quantity.Value.GetValueOrDefault() + this.LossValue;
            }
        }

        /// <summary>
        /// Gets the RawMaterial ScrapCalculationTypes.
        /// </summary>
        public EnumerationDescription<ScrapCalculationType> ScrapCalculationTypes { get; private set; }

        /// <summary>
        /// Gets the manufacturer view model.
        /// </summary>
        public ManufacturerViewModel ManufacturerViewModel { get; private set; }

        /// <summary>
        /// Gets the master data browser.
        /// </summary>
        public MasterDataBrowserViewModel MasterDataBrowser { get; private set; }

        /// <summary>
        /// Gets the media view model.
        /// </summary>
        public MediaViewModel MediaViewModel { get; private set; }

        /// <summary>
        /// Gets or sets the project to which the Model belongs.
        /// This property is needed during cost calculations so it must be set when editing (it can be omitted when creating).
        /// </summary>
        public Project ParentProject { get; set; }

        /// <summary>
        /// Gets or sets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter { get; set; }

        #endregion Properties

        #region Initialization

        /// <summary>
        /// Initializes the view-model for the creation of a new RawMaterial.
        /// The ModelDataContext property must be set to a valid value before this call.
        /// </summary>
        /// <param name="isMasterData">If set to true the RawMaterial will be created in master data; in this case the parent should be set to null.</param>
        /// <param name="parent">
        /// The parent object of the RawMaterial that will be created. If this is set, the value of the <paramref name="isMasterData"/> argument is ignored and
        /// the parent's IsMasterData flag is used instead.
        /// </param>
        /// <param name="costCalculationVersion">The cost calculation version to use when calculating the material's cost for display.</param>
        /// <remarks>
        /// This is a helper method for populating the model with default data for creation. It is not mandatory to use it; you can set the model to a new
        /// instance set up however you want.
        /// </remarks>
        /// <exception cref="ArgumentException">
        /// <paramref name="parent"/> is null and <paramref name="isMasterData"/> is false, or the type of<paramref name="parent"/> is not supported.
        /// </exception>
        /// <exception cref="InvalidOperationException">The provided <paramref name="parent"/> object type is not supported.</exception>
        public void InitializeForCreation(bool isMasterData, object parent, string costCalculationVersion)
        {
            this.CheckDataSource();
            this.EditMode = ViewModelEditMode.Create;

            RawMaterial newRawMaterial = new RawMaterial();

            if (!isMasterData)
            {
                if (parent == null)
                {
                    throw new ArgumentException("A non master data RawMaterial cannot be created without a parent", "parent");
                }

                Part parentPart = parent as Part;
                if (parentPart != null)
                {
                    newRawMaterial.Part = parentPart;
                    isMasterData = parentPart.IsMasterData;
                }
                else
                {
                    throw new InvalidOperationException(string.Format("An object of type '{0}' is not supported as RawMaterial parent.", parent.GetType().Name));
                }
            }

            newRawMaterial.Manufacturer = new Manufacturer();

            // Set the master data flag and the owner at the end so they are applied to all sub-objects.
            newRawMaterial.SetIsMasterData(isMasterData);
            if (!newRawMaterial.IsMasterData
                && !this.IsInViewerMode)
            {
                User owner = this.DataSourceManager.UserRepository.GetById(SecurityManager.Instance.CurrentUser.Guid, false);
                newRawMaterial.SetOwner(owner);
            }

            this.CostCalculationVersion = costCalculationVersion;

            this.Model = newRawMaterial;
        }

        /// <summary>
        /// Initializes the properties of this instance.
        /// </summary>
        private void InitializeProperties()
        {
            this.Price.ValueChanged += (s, e) => this.OnPropertyInvolvedInCostChanged();
            this.PartWeight.ValueChanged += (s, e) => this.OnPartWeightChanged();
            this.Quantity.ValueChanged += (s, e) => this.OnQuantityChanged();
            this.Sprue.ValueChanged += (s, e) => this.OnSprueChanged();
            this.Loss.ValueChanged += (s, e) => this.OnLossChanged();
            this.RejectRatio.ValueChanged += (s, e) => this.OnPropertyInvolvedInCostChanged();
            this.RecyclingRatio.ValueChanged += (s, e) => this.OnPropertyInvolvedInCostChanged();
            this.ScrapRefundRatio.ValueChanged += (s, e) => this.OnPropertyInvolvedInCostChanged();
            this.MaterialWeightUnit.ValueChanged += (s, e) => this.OnMaterialWeightUnitChanged();
            this.QuantityWeightUnit.ValueChanged += (s, e) => this.OnQuantityWeightUnitChanged();

            this.SprueMaxContentLimit = 9999;
            this.PartWeightMaxDecimalPlaces = 4;
            this.QuantityMaxDecimalPlaces = 4;

            this.ScrapCalculationTypes = new EnumerationDescription<ScrapCalculationType>();
        }

        /// <summary>
        /// Initialize the undo manager.
        /// </summary>
        private void InitializeUndoManager()
        {
            this.MediaViewModel.UndoManager = this.UndoManager;
            this.ManufacturerViewModel.UndoManager = this.UndoManager;
            this.MaterialClassificationsView.UndoManager = this.UndoManager;
        }

        #endregion Initialization

        #region Model handling

        /// <summary>
        /// Loads the data from the model.
        /// Each view-model property decorated with the ExposedModelProperty attribute is loaded from the associated Model property.
        /// </summary>
        /// <param name="model">The model instance.</param>
        public override void LoadDataFromModel(RawMaterial model)
        {
            base.LoadDataFromModel(model);

            // If in viewer mode or if model is released then add the raw material delivery type manually to the view model delivery types
            // In this case the delivery type from model can not be accessed from the database (not available in viewer mode)
            // or is different than the values from the database if model is released.
            if (this.Model.DeliveryType != null
                && (this.IsInViewerMode || this.Model.DeliveryType.IsReleased))
            {
                this.DeliveryTypes = new Collection<RawMaterialDeliveryType>();
                this.DeliveryTypes.Add(this.Model.DeliveryType);
            }

            if (this.DeliveryType.Value != null && !this.DeliveryTypes.Contains(this.DeliveryType.Value))
            {
                // DeliveryType instance is not from the view model's DataSourceManager.
                this.DeliveryType.Value = this.DeliveryTypes.FirstOrDefault(t => t.Guid == this.DeliveryType.Value.Guid);
            }

            var crtUIWeightUnit = this.MeasurementUnitsAdapter.GetCurrentUnit(UnitType.Weight);
            var materialUnit = model.QuantityUnitBase != null ? model.QuantityUnitBase : model.ParentWeightUnitBase;

            if (this.IsInViewerMode)
            {
                this.MaterialWeightUnit.Value = materialUnit;
            }
            else
            {
                if (materialUnit != null
                    && materialUnit.ScaleID == crtUIWeightUnit.Scale)
                {
                    // Use the material's weight unit if is from the same measurement units scale as the current UI weight unit.
                    this.MaterialWeightUnit.Value = this.DataSourceManager.MeasurementUnitRepository.GetByName(model.ParentWeightUnitBase.Name);
                }
                else
                {
                    // If the material's weight unit is null or if is not from the same measurement units scale as the current UI weight unit,
                    // use the current UI weight unit as the material's weight unit.
                    // In the case the material's weight unit is not null, using the current UI weight unit syncs the MaterialWeightUnit property value with its
                    // corresponding text box's unit -> this property sync is done through two way binding but at screen load time, in the case the material unit is
                    // kg and the UI unit is lb, the sync does not occur from the text box to the property.
                    this.MaterialWeightUnit.Value = this.DataSourceManager.MeasurementUnitRepository.GetBaseMeasurementUnit(crtUIWeightUnit.Scale);
                }
            }

            if (model.Sprue != null && model.ParentWeight != null)
            {
                // if the sprue and part weight are set, compute the quantity based on them
                this.SetMaterialWeightBasedOnPartWeightAndSprue();
            }
            else
            {
                // else take the quantity from the object
                this.Quantity.Value = model.Quantity;
            }

            // "Dispose" option is not available for Calculation Version 1.0
            if (this.CostCalculationVersion == "1.0")
            {
                var disposeItem = this.ScrapCalculationTypes.FirstOrDefault(p => p.Value == ScrapCalculationType.Dispose);
                if (disposeItem != null)
                {
                    this.ScrapCalculationTypes.Remove(disposeItem);
                }

                this.ScrapCalculationSelectedType.Value = ScrapCalculationType.Yield;
            }

            this.StockKeepingState = model.StockKeeping.HasValue ? true : false;

            this.LoadMaterialClassifications(model);
            this.MaterialClassificationsView.IsChanged = false;

            // Adjust the material's Price to be proportional to the current UI unit for weight.
            // The raw material does not have a property to store the price unit so we take it from the Material Weight (QuantityUnitBase property),
            // or Part Weight (ParentWeightUnitBase property), if the previous is not set.
            var priceUnit = model.QuantityUnitBase ?? model.ParentWeightUnitBase;
            this.Price.Value = this.AdjustRawMaterialPriceToCurrentUIWeightUnit(model.Price, priceUnit);

            this.UpdateCost();
        }

        /// <summary>
        /// Called when the Model has changed.
        /// </summary>
        protected override void OnModelChanged()
        {
            if (this.IsInViewerMode)
            {
                this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(null);
            }

            this.CheckDataSource();
            base.OnModelChanged();

            this.ManufacturerViewModel.DataSourceManager = this.DataSourceManager;

            if (this.Model.Manufacturer != null)
            {
                this.ManufacturerViewModel.Model = this.Model.Manufacturer;
            }
            else
            {
                this.ManufacturerViewModel.Model = new Manufacturer();
            }

            this.MediaViewModel.DataSourceManager = this.DataSourceManager;
            this.MediaViewModel.Model = this.Model;

            this.UndoManager.Start();

            this.CloneManager.Clone(this);
        }

        /// <summary>
        /// Called when DataSourceManager has changed.
        /// </summary>
        protected override void OnDataSourceManagerChanged()
        {
            base.OnDataSourceManagerChanged();

            this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(this.DataSourceManager);
            this.unitsAdapterHandler = new UnitsAdapterUpdateHandler(this.MeasurementUnitsAdapter);
            unitsAdapterHandler.PauseUndoOnUnitsAdapterUpdate(this.UndoManager);

            this.DeliveryTypes = this.DataSourceManager.RawMaterialDeliveryTypeRepository.FindAll();
            this.MaterialClassificationsView.DataAccessContext = this.DataSourceManager;
        }

        #endregion Model handling

        #region Property change handlers

        /// <summary>
        /// Called when some property involved in cost calculation has changed.
        /// </summary>
        private void OnPropertyInvolvedInCostChanged()
        {
            this.UpdateCost();
        }

        /// <summary>
        /// Called when the PartWeight property has changed.
        /// </summary>
        private void OnPartWeightChanged()
        {
            using (this.UndoManager.StartBatch(includePreviousItem: true, reverseUndoOrder: true, navigateToBatchControls: true))
            {
                this.StopRecalculationNotifications();
                this.SetMaterialWeightBasedOnPartWeightAndSprue();
                this.ResumeRecalculationNotifications(true);
            }
        }

        /// <summary>
        /// Called when the Quantity property has changed.
        /// </summary>
        private void OnQuantityChanged()
        {
            using (this.UndoManager.StartBatch(includePreviousItem: false, reverseUndoOrder: false, navigateToBatchControls: true))
            {
                this.StopRecalculationNotifications();
                if (this.Quantity.Value != null && this.PartWeight.Value != null)
                {
                    decimal? sprueRatio;
                    if (this.PartWeight.Value != 0)
                    {
                        sprueRatio = (this.Quantity.Value / this.PartWeight.Value) - 1;
                    }
                    else
                    {
                        sprueRatio = 0;
                    }

                    if (sprueRatio >= 0 && sprueRatio <= SprueMaxContentLimit / 100)
                    {
                        this.Sprue.Value = sprueRatio;
                    }
                    else
                    {
                        this.Sprue.Value = null;
                    }
                }

                this.OnPropertyChanged(() => this.LossValue);
                this.OnPropertyChanged(() => this.ProcessMaterial);

                this.UpdateCost();
                this.ResumeRecalculationNotifications(true);
            }
        }

        /// <summary>
        /// Called when the Sprue property has changed.
        /// </summary>
        private void OnSprueChanged()
        {
            using (this.UndoManager.StartBatch(includePreviousItem: true, reverseUndoOrder: false, navigateToBatchControls: true))
            {
                this.StopRecalculationNotifications();
                this.SetMaterialWeightBasedOnPartWeightAndSprue();
                this.ResumeRecalculationNotifications(true);
            }
        }

        /// <summary>
        /// Called when the Loss property has changed.
        /// </summary>
        private void OnLossChanged()
        {
            using (this.UndoManager.StartBatch(includePreviousItem: false, reverseUndoOrder: false, navigateToBatchControls: true))
            {
                this.StopRecalculationNotifications();
                this.UpdateCost();
                this.OnPropertyChanged(() => this.LossValue);
                this.OnPropertyChanged(() => this.ProcessMaterial);
                this.ResumeRecalculationNotifications(true);
            }
        }

        /// <summary>
        /// Called when the Material WeightUnit property has changed.
        /// </summary>
        private void OnMaterialWeightUnitChanged()
        {
            using (this.UndoManager.Pause())
            {
                if (this.QuantityWeightUnit.Value == null ||
                    (this.QuantityWeightUnit.Value != null && this.QuantityWeightUnit.Value != this.MaterialWeightUnit.Value))
                {
                    this.QuantityWeightUnit.Value = this.MaterialWeightUnit.Value;
                }

                this.SetMaterialWeightBasedOnPartWeightAndSprue();
            }
        }

        /// <summary>
        /// Called when the QuantityUnit property has changed.
        /// </summary>
        private void OnQuantityWeightUnitChanged()
        {
            using (this.UndoManager.Pause())
            {
                if (this.MaterialWeightUnit.Value == null ||
                    (this.MaterialWeightUnit.Value != null && this.QuantityWeightUnit.Value != this.MaterialWeightUnit.Value))
                {
                    this.MaterialWeightUnit.Value = this.QuantityWeightUnit.Value;
                }

                this.SetMaterialWeightBasedOnPartWeightAndSprue();
            }
        }

        /// <summary>
        /// Handles the system units change.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void HandleUnitsSystemChanged(object sender, PropertyChangedEventArgs e)
        {
            bool changedState = this.IsChanged;

            using (this.UndoManager.Pause())
            {
                var priceUnit = this.MeasurementUnitsAdapter.GetPreviousBaseMeasurementUnit(MeasurementUnitType.Weight);
                this.Price.Value = this.AdjustRawMaterialPriceToCurrentUIWeightUnit(this.Price.Value, priceUnit);
            }

            // Restore the change state of the view model in case the price adjustment has changed it.
            this.IsChanged = changedState;
        }

        #endregion Property change handlers

        #region Other private methods

        /// <summary>
        /// Load Material classifications.
        /// </summary>
        /// <param name="rawMaterial">RawMaterial entity.</param>
        private void LoadMaterialClassifications(RawMaterial rawMaterial)
        {
            using (this.UndoManager.StartBatch(navigateToBatchControls: true))
            {
                if (rawMaterial.MaterialsClassificationL1 != null)
                {
                    if (this.IsInViewerMode)
                    {
                        this.MaterialClassificationsView.ClassificationLevels.Add(rawMaterial.MaterialsClassificationL1);
                    }
                    else
                    {
                        var classificationLevel1 = this.DataSourceManager.MaterialsClassificationRepository.GetByName(rawMaterial.MaterialsClassificationL1.Name);
                        if (classificationLevel1 != null)
                        {
                            if (this.MaterialClassificationsView.ClassificationLevels.Count > 0)
                            {
                                if (this.MaterialClassificationsView.ClassificationLevels[0] != classificationLevel1)
                                {
                                    while (this.MaterialClassificationsView.ClassificationLevels.Count > 0)
                                    {
                                        this.MaterialClassificationsView.ClassificationLevels.RemoveAt(0);
                                    }

                                    this.MaterialClassificationsView.ClassificationLevels.Add(classificationLevel1);
                                }
                            }
                            else
                            {
                                this.MaterialClassificationsView.ClassificationLevels.Add(classificationLevel1);
                            }
                        }
                    }
                }
                else if (this.MaterialClassificationsView.ClassificationLevels.Count > 0)
                {
                    while (this.MaterialClassificationsView.ClassificationLevels.Count > 0)
                    {
                        this.MaterialClassificationsView.ClassificationLevels.RemoveAt(0);
                    }
                }

                if (rawMaterial.MaterialsClassificationL2 != null)
                {
                    if (this.IsInViewerMode)
                    {
                        this.MaterialClassificationsView.ClassificationLevels.Add(rawMaterial.MaterialsClassificationL2);
                    }
                    else
                    {
                        var classificationLevel2 = this.DataSourceManager.MaterialsClassificationRepository.GetByName(rawMaterial.MaterialsClassificationL2.Name);
                        if (classificationLevel2 != null)
                        {
                            if (this.MaterialClassificationsView.ClassificationLevels.Count > 1)
                            {
                                if (this.MaterialClassificationsView.ClassificationLevels[1] != classificationLevel2)
                                {
                                    while (this.MaterialClassificationsView.ClassificationLevels.Count > 1)
                                    {
                                        this.MaterialClassificationsView.ClassificationLevels.RemoveAt(1);
                                    }

                                    this.MaterialClassificationsView.ClassificationLevels.Add(classificationLevel2);
                                }
                            }
                            else
                            {
                                this.MaterialClassificationsView.ClassificationLevels.Add(classificationLevel2);
                            }
                        }
                    }
                }
                else if (this.MaterialClassificationsView.ClassificationLevels.Count > 1)
                {
                    while (this.MaterialClassificationsView.ClassificationLevels.Count > 1)
                    {
                        this.MaterialClassificationsView.ClassificationLevels.RemoveAt(1);
                    }
                }

                if (rawMaterial.MaterialsClassificationL3 != null)
                {
                    if (this.IsInViewerMode)
                    {
                        this.MaterialClassificationsView.ClassificationLevels.Add(rawMaterial.MaterialsClassificationL3);
                    }
                    else
                    {
                        var classificationLevel3 = this.DataSourceManager.MaterialsClassificationRepository.GetByName(rawMaterial.MaterialsClassificationL3.Name);
                        if (classificationLevel3 != null)
                        {
                            if (this.MaterialClassificationsView.ClassificationLevels.Count > 2)
                            {
                                if (this.MaterialClassificationsView.ClassificationLevels[2] != classificationLevel3)
                                {
                                    while (this.MaterialClassificationsView.ClassificationLevels.Count > 2)
                                    {
                                        this.MaterialClassificationsView.ClassificationLevels.RemoveAt(2);
                                    }

                                    this.MaterialClassificationsView.ClassificationLevels.Add(classificationLevel3);
                                }
                            }
                            else
                            {
                                this.MaterialClassificationsView.ClassificationLevels.Add(classificationLevel3);
                            }
                        }
                    }
                }
                else if (this.MaterialClassificationsView.ClassificationLevels.Count > 2)
                {
                    while (this.MaterialClassificationsView.ClassificationLevels.Count > 2)
                    {
                        this.MaterialClassificationsView.ClassificationLevels.RemoveAt(2);
                    }
                }

                if (rawMaterial.MaterialsClassificationL4 != null)
                {
                    if (this.IsInViewerMode)
                    {
                        this.MaterialClassificationsView.ClassificationLevels.Add(rawMaterial.MaterialsClassificationL4);
                    }
                    else
                    {
                        var classificationLevel4 = this.DataSourceManager.MaterialsClassificationRepository.GetByName(rawMaterial.MaterialsClassificationL4.Name);
                        if (classificationLevel4 != null)
                        {
                            if (this.MaterialClassificationsView.ClassificationLevels.Count > 3)
                            {
                                if (this.MaterialClassificationsView.ClassificationLevels[3] != classificationLevel4)
                                {
                                    this.MaterialClassificationsView.ClassificationLevels.RemoveAt(3);
                                    this.MaterialClassificationsView.ClassificationLevels.Add(classificationLevel4);
                                }
                            }
                            else
                            {
                                this.MaterialClassificationsView.ClassificationLevels.Add(classificationLevel4);
                            }
                        }
                    }
                }
                else if (this.MaterialClassificationsView.ClassificationLevels.Count > 3)
                {
                    this.MaterialClassificationsView.ClassificationLevels.RemoveAt(3);
                }
            }
        }

        /// <summary>
        /// Sets the classifications.
        /// </summary>
        /// <param name="rawMaterial">The raw material.</param>
        private void SetClassifications(RawMaterial rawMaterial)
        {
            if (MaterialClassificationsView != null)
            {
                var classifications = MaterialClassificationsView.ClassificationLevels;

                if (classifications != null)
                {
                    if (classifications.Count > 0)
                    {
                        MaterialsClassification cls = classifications[0] as MaterialsClassification;
                        if (rawMaterial.MaterialsClassificationL1 != cls)
                        {
                            rawMaterial.MaterialsClassificationL1 = cls;
                        }
                    }
                    else
                    {
                        if (rawMaterial.MaterialsClassificationL1 != null)
                        {
                            rawMaterial.MaterialsClassificationL1 = null;
                        }
                    }

                    if (classifications.Count > 1)
                    {
                        MaterialsClassification cls = classifications[1] as MaterialsClassification;
                        if (rawMaterial.MaterialsClassificationL2 != cls)
                        {
                            rawMaterial.MaterialsClassificationL2 = cls;
                        }
                    }
                    else
                    {
                        if (rawMaterial.MaterialsClassificationL2 != null)
                        {
                            rawMaterial.MaterialsClassificationL2 = null;
                        }
                    }

                    if (classifications.Count > 2)
                    {
                        MaterialsClassification cls = classifications[2] as MaterialsClassification;
                        if (rawMaterial.MaterialsClassificationL3 != cls)
                        {
                            rawMaterial.MaterialsClassificationL3 = cls;
                        }
                    }
                    else
                    {
                        if (rawMaterial.MaterialsClassificationL3 != null)
                        {
                            rawMaterial.MaterialsClassificationL3 = null;
                        }
                    }

                    if (classifications.Count > 3)
                    {
                        MaterialsClassification cls = classifications[3] as MaterialsClassification;
                        if (rawMaterial.MaterialsClassificationL4 != cls)
                        {
                            rawMaterial.MaterialsClassificationL4 = cls;
                        }
                    }
                    else
                    {
                        if (rawMaterial.MaterialsClassificationL4 != null)
                        {
                            rawMaterial.MaterialsClassificationL4 = null;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Calculates and sets the material weight based on the part weight and sprue value.
        /// </summary>
        private void SetMaterialWeightBasedOnPartWeightAndSprue()
        {
            if (this.Sprue.Value != null)
            {
                this.Quantity.Value = this.PartWeight.Value * (1 + this.Sprue.Value);
            }

            this.OnPropertyChanged(() => this.SprueValue);

            this.UpdateCost();
        }

        /// <summary>
        /// Gets the number of decimals to show.
        /// </summary>
        /// <param name="sourceValue">The source value.</param>
        /// <param name="numberOfParentDecimalPlaces">The number of parent decimal places.</param>
        /// <returns> The number of decimals to show. </returns>
        private int GetNumberOfDecimalsToShow(decimal sourceValue, int numberOfParentDecimalPlaces)
        {
            // Note: start with 2 because the "for" below count's only the number of leading zeros before any valuable information.
            // If the sourceValue doesn't contain any decimal places, it's not a problem to return 2 because the formatter will ignore it anyway.
            var numberOfDecimals = 2;
            var valueAsString = sourceValue.ToString(CultureInfo.InvariantCulture);
            var indexOfDecimalPoint = valueAsString.IndexOf(CultureInfo.InvariantCulture.NumberFormat.NumberDecimalSeparator, StringComparison.InvariantCultureIgnoreCase);

            if (indexOfDecimalPoint != -1)
            {
                for (var index = indexOfDecimalPoint + 1; index < valueAsString.Length; index++)
                {
                    if (valueAsString[index] == '0')
                    {
                        numberOfDecimals++;
                        continue;
                    }

                    break;
                }
            }

            return Math.Max(numberOfDecimals, numberOfParentDecimalPlaces);
        }

        #endregion Other private methods

        #region Master Data Browse handling

        /// <summary>
        /// Opens the master data browser to select a RawMaterial.
        /// </summary>
        private void BrowseMasterData()
        {
            this.MasterDataBrowser.MasterDataType = typeof(RawMaterial);
            this.MasterDataBrowser.MasterDataSelected += this.OnMasterDataRawMaterialSelected;
            this.windowService.ShowViewInDialog(this.MasterDataBrowser);
            this.MasterDataBrowser.MasterDataSelected -= this.OnMasterDataRawMaterialSelected;
        }

        /// <summary>
        /// Called when a master data RawMaterial is selected from the master data browser.
        /// </summary>
        /// <param name="masterDataEntity">The master data entity selected in the browser.</param>
        /// <param name="databaseId">The source database of the selected master data.</param>
        private void OnMasterDataRawMaterialSelected(object masterDataEntity, DbIdentifier databaseId)
        {
            RawMaterial masterRawMaterial = masterDataEntity as RawMaterial;
            if (masterRawMaterial == null)
            {
                return;
            }

            this.StopRecalculationNotifications();
            using (this.UndoManager.StartBatch(undoEachBatch: true))
            {
                // Retrieve the media of the raw material master data.
                var dataManager = DataAccessFactory.CreateDataSourceManager(databaseId);
                MediaManager mediaManager = new MediaManager(dataManager);
                var mediaList = new List<Media>();
                var media = mediaManager.GetPictureOrVideo(masterDataEntity);
                if (media != null && (MediaType)media.Type != MediaType.Document)
                {
                    mediaList.Add(media.Copy());
                }

                // Load the raw material master data media in Media view-model.
                this.MediaViewModel.LoadMedia(mediaList);

                // Convert the master data entity currency values.
                var currencies = dataManager.CurrencyRepository.GetBaseCurrencies();
                var baseCurrencyFromMD = currencies.FirstOrDefault(c => c.IsSameAs(this.MeasurementUnitsAdapter.BaseCurrency));
                CurrencyConversionManager.ConvertObject(masterRawMaterial, baseCurrencyFromMD, CurrencyConversionManager.DefaultBaseCurrency);

                // Load the data from the master RawMaterial and sub-objects in the corresponding view-models. Use empty data for null sub-objects.
                this.LoadDataFromModel(masterRawMaterial);

                if (this.DeliveryType.Value != null)
                {
                    this.DeliveryType.Value = this.DataSourceManager.RawMaterialDeliveryTypeRepository.FindById(this.DeliveryType.Value.Guid);
                }

                if (masterRawMaterial.Manufacturer != null)
                {
                    this.ManufacturerViewModel.LoadDataFromModel(masterRawMaterial.Manufacturer);
                }
                else
                {
                    this.ManufacturerViewModel.LoadDataFromModel(new Manufacturer());
                }
            }

            this.ResumeRecalculationNotifications(true);
        }

        #endregion Master Data Browse handling

        #region Cost handling

        /// <summary>
        /// Refreshes the RawMaterial parent's cost and notifies listeners.
        /// </summary>
        private void RefreshParentCost()
        {
            this.RefreshCalculations(this.Model.Part);
        }

        /// <summary>
        /// Calculates the cost and displays it.
        /// </summary>
        private void UpdateCost()
        {
            RawMaterial tmpMaterial = new RawMaterial();
            tmpMaterial.Quantity = this.Quantity.Value;
            tmpMaterial.Price = this.Price.Value;
            tmpMaterial.ParentWeight = this.PartWeight.Value;
            tmpMaterial.ScrapRefundRatio = this.ScrapRefundRatio.Value;
            tmpMaterial.Loss = this.Loss.Value;
            tmpMaterial.RejectRatio = this.RejectRatio.Value;
            tmpMaterial.RecyclingRatio = this.RecyclingRatio.Value;
            tmpMaterial.StockKeeping = this.StockKeeping.Value;
            tmpMaterial.ScrapCalculationType = this.ScrapCalculationSelectedType.Value;

            if (this.MaterialWeightUnit.Value != null)
            {
                // Make a copy of the unit so the temp material is not associated with the data access manager (by using the MaterialWeightUnit value which is associated with it).
                var weightUnit = this.MaterialWeightUnit.Value.Copy();
                tmpMaterial.QuantityUnitBase = weightUnit;
                tmpMaterial.ParentWeightUnitBase = weightUnit;
            }

            ICostCalculator calculator = CostCalculatorFactory.GetCalculator(this.CostCalculationVersion);
            RawMaterialCost cost = calculator.CalculateRawMaterialCost(tmpMaterial, null);
            this.Cost = cost.NetCost;
        }

        /// <summary>
        /// Adjust the price of a raw material from the measurement unit in which is currently saved to the UI measurement unit in which is displayed.
        /// <para/>
        /// This adjustment is necessary to keep constant the cost of the material when the weight unit changes to another scale. The cost is price * quantity,
        /// so when the scale changes the quantity increases or decreases, directly affecting the cost even though you have the same basic quantity.
        /// </summary>
        /// <param name="price">The price of the material.</param>
        /// <param name="priceUnit">The measurement unit in which the price is given. If is null, kg is used as default.</param>
        /// <returns>
        /// The adjusted price.
        /// </returns>        
        /// <remarks>
        /// The price is always in euro but the price unit can be kilograms or pounds.
        /// If you have a price of 1 euro/kg and a material quantity of 1 kg the cost is 1 euro (1 euro * 1kg). If you switch the UI to pounds and don't do this
        /// adjustment, you get a cost of 2.20462 (1 euro * 2.20462 lb; 2.20462 is the quantity converted to pounds). With the adjustment you get a price
        /// of 0.4535929 euro/lb; the cost is the same 1 euro (0.4535929 * 2.20462).
        /// </remarks>
        private decimal? AdjustRawMaterialPriceToCurrentUIWeightUnit(decimal? price, MeasurementUnit priceUnit)
        {
            var weightUnit = this.MeasurementUnitsAdapter.GetBaseMeasurementUnit(MeasurementUnitType.Weight);
            return CostCalculationHelper.AdjustRawMaterialPriceToNewWeightUnit(price ?? 0m, priceUnit, weightUnit);
        }

        #endregion Cost handling

        #region Save/Cancel

        /// <summary>
        /// Saves all changes back into the model.
        /// </summary>
        protected override void SaveToModel()
        {
            this.CheckModel();

            base.SaveToModel();

            this.Model.ParentWeightUnitBase = this.MaterialWeightUnit.Value;
            this.Model.QuantityUnitBase = this.MaterialWeightUnit.Value;

            // Attach the manufacturer from model.
            var manufacturer = this.ManufacturerViewModel.Model;
            if (manufacturer != null)
            {
                this.Model.Manufacturer = manufacturer;
            }
        }

        /// <summary>
        /// Determines whether this instance can perform the save operation. Executed by the SaveCommand.
        /// The default implementation allows the save to be performed if the input is valid.
        /// </summary>
        /// <returns>
        /// true if the save operation can be performed, false otherwise.
        /// </returns>
        protected override bool CanSave()
        {
            return base.CanSave() && this.SaveNestedViewModels.CanExecute(null);
        }

        /// <summary>
        /// Performs the save operation. Executed by the SaveCommand.
        /// </summary>
        protected override void Save()
        {
            this.CheckModelAndDataSource();

            this.SetClassifications(this.Model);

            // Save all changes back into the Model objects. The validity check of this operation is performed by the CanSave method.
            this.SaveToModel();
            this.SaveNestedViewModels.Execute(null);

            if (this.SavesToDataSource)
            {
                // Update RawMaterial
                this.DataSourceManager.RawMaterialRepository.Save(this.Model);
                this.DataSourceManager.SaveChanges();

                // Notify the other components that the RawMaterial was created/updated.
                EntityChangedMessage message = this.Model.IsMasterData ?
                    new EntityChangedMessage(Notification.MasterDataEntityChanged) :
                    new EntityChangedMessage(Notification.MyProjectsEntityChanged);

                message.ChangeType = this.EditMode == ViewModelEditMode.Create ? EntityChangeType.EntityCreated : EntityChangeType.EntityUpdated;
                message.Entity = this.Model;
                if (this.Model.Part != null)
                {
                    message.Parent = this.Model.Part;
                }

                this.messenger.Send(message);
            }

            this.RefreshParentCost();

            this.MaterialClassificationsView.IsChanged = false;

            // Close the view-model when is displayed in a window.
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Determines whether the Cancel operation can be performed. Executed by the CancelCommand.
        /// </summary>
        /// <returns>
        /// true if the changes can be canceled, false otherwise.
        /// </returns>
        protected override bool CanCancel()
        {
            return base.CanCancel() && this.CancelNestedViewModels.CanExecute(null);
        }

        /// <summary>
        /// Cancels all changes. Executed by the CancelCommand.
        /// </summary>
        protected override void Cancel()
        {
            if (!this.CanCancel())
            {
                return;
            }

            if (this.IsChanged)
            {
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_CancelChanges, MessageDialogType.YesNo);
                if (result != MessageDialogResult.Yes)
                {
                    // Don't cancel the changes and also don't close the view-model.
                    return;
                }
                else
                {
                    using (this.UndoManager.StartBatch())
                    {
                        // Cancel all changes
                        base.Cancel();
                        this.CancelNestedViewModels.Execute(null);
                    }
                }
            }

            // Close the view-model when is displayed in a window.
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Called before unloading the view from its parent. Returning false will cancel the view's unloading.
        /// </summary>
        /// <returns>
        /// True if the unloading process should continue and false if it should be canceled.
        /// </returns>
        public override bool OnUnloading()
        {
            // Do nothing if the view-model is read-only, in viewer mode, it was not changed or the model was deleted.
            if (this.IsReadOnly || this.IsInViewerMode || !this.IsChanged || this.Model.IsDeleted)
            {
                return true;
            }

            if (this.EditMode == ViewModelEditMode.Create)
            {
                // Ask the user to confirm quitting
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_Quit, MessageDialogType.YesNo);
                if (result != MessageDialogResult.Yes)
                {
                    // The user chose to stay on the screen; return false to stop the screen unloading.
                    return false;
                }
                else
                {
                    this.IsChanged = false;
                }
            }
            else if (this.EditMode == ViewModelEditMode.Edit)
            {
                // Ask the user if he wants to save
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_UnsavedDataOnQuit, MessageDialogType.YesNoCancel);
                if (result == MessageDialogResult.Yes)
                {
                    // The user whishes to save but it is not possible because the input is not valid. Return false to stop the view-model from unloading.
                    if (!this.CanSave())
                    {
                        return false;
                    }

                    this.Save();
                }
                else if (result == MessageDialogResult.No)
                {
                    // The user does not want to save.                    
                    this.IsChanged = false;
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Called after one or more model properties value changed in order to refresh the calculations.
        /// </summary>
        protected override void RefreshCalculation()
        {
            if (this.EditMode == ViewModelEditMode.Create)
            {
                return;
            }

            this.RefreshCalculations((Part)this.ModelParentClone);
        }

        #endregion Save/Cancel

        /// <summary>
        /// Calculates the parent and sends a message with the result.
        /// </summary>
        /// <param name="part">The part.</param>
        private void RefreshCalculations(Part part)
        {
            if (part == null)
            {
                return;
            }

            PartCostCalculationParameters calculationParams = null;
            if (this.ParentProject != null)
            {
                calculationParams = CostCalculationHelper.CreatePartParamsFromProject(this.ParentProject);
            }
            else if (this.IsInViewerMode)
            {
                // Note: theoretically this part is never reached because it's not possible to save in view mode.
                calculationParams = this.modelBrowserHelperService.GetPartCostCalculationParameters();
            }

            if (calculationParams != null)
            {
                var calculator = CostCalculatorFactory.GetCalculator(part.CalculationVariant);
                var result = calculator.CalculatePartCost(part, calculationParams);
                messenger.Send(new CurrentComponentCostChangedMessage(result), IsInViewerMode ? GlobalMessengerTokens.ModelBrowserTargetToken : GlobalMessengerTokens.MainViewTargetToken);
            }
        }

        #region Inner Classes

        /// <summary>
        /// MaterialWeight Validator inner class.
        /// </summary>
        private class MaterialWeightValidator : ValidationAttribute
        {
            /// <summary>
            /// Validates the specified value with respect to the current validation attribute.
            /// </summary>
            /// <param name="value">The value to validate.</param>
            /// <param name="validationContext">The context information about the validation operation.</param>
            /// <returns>
            /// An instance of the <see cref="T:System.ComponentModel.DataAnnotations.ValidationResult"/> class.
            /// </returns>
            protected override ValidationResult IsValid(object value, ValidationContext validationContext)
            {
                object viewModel;
                validationContext.Items.TryGetValue("ViewModel", out viewModel);

                bool isValid = true;
                var materialWeightValue = value as decimal?;
                var rawMaterialVM = viewModel as RawMaterialViewModel;

                if (materialWeightValue != null && rawMaterialVM != null)
                {
                    var partWeight = rawMaterialVM.PartWeight;
                    if (partWeight.Value != null
                        && materialWeightValue < partWeight.Value)
                    {
                        isValid = false;
                    }
                }

                return isValid ? ValidationResult.Success : new ValidationResult(LocalizedResources.RawMaterial_MaterialWeightInvalidData);
            }
        }

        #endregion Inner Classes
    }
}