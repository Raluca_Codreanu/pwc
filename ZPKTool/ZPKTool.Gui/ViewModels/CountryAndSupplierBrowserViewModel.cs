﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using ZPKTool.Business;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Views;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the IMyView view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class CountryAndSupplierBrowserViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The text written on the window border
        /// </summary>
        private string titleText = string.Empty;

        /// <summary>
        /// The text written in the group box header
        /// </summary>
        private string headerText = string.Empty;

        /// <summary>
        /// The collection of Tree Items which is displayed in the group box
        /// </summary>
        private ObservableCollection<TreeViewDataItem> treeItems;

        /// <summary>
        /// A value indicating whether this instance is loading data (the countries or suppliers).
        /// </summary>
        private bool isLoadingData;

        /// <summary>
        /// The thread on which the master data loading runs.
        /// </summary>
        private BackgroundWorker loadMDWorker = new BackgroundWorker();

        /// <summary>
        /// The selected item in the tree
        /// </summary>
        private TreeViewDataItem selectedTreeItem;

        /// <summary>
        /// the window service
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The text entered in the search TextBox.
        /// </summary>
        private string textToSearchInTree;

        /// <summary>
        /// The list of objects containing the loaded data.
        /// </summary>
        private IEnumerable<object> loadedData = new List<object>();

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="CountryAndSupplierBrowserViewModel"/> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        [ImportingConstructor]
        public CountryAndSupplierBrowserViewModel(IWindowService windowService)
        {
            this.windowService = windowService;
            this.TreeItems = new ObservableCollection<TreeViewDataItem>();

            this.CloseScreen = new DelegateCommand(CloseScreenAction);
            this.SelectItem = new DelegateCommand(SelectItemAction);
            this.KeyDownCommand = new DelegateCommand<KeyEventArgs>(HandleKeyDown);
            this.MouseDoubleClickCommand = new DelegateCommand<MouseEventArgs>((e) => this.DoubleClickAction(e));
            this.SelectedTreeItemChangedCommand = new DelegateCommand<RoutedPropertyChangedEventArgs<object>>((e) => this.selectedTreeItem = e.NewValue as TreeViewDataItem);

            this.loadMDWorker.DoWork += new DoWorkEventHandler(LoadMDWorker_DoWork);
            this.loadMDWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(LoadMDWorker_RunWorkerCompleted);

            TreeFilter.Value = (item) =>
            {
                var treeItem = item as TreeViewDataItem;
                if (treeItem != null
                    && !string.IsNullOrWhiteSpace(this.TextToSearchInTree)
                    && (treeItem.Label == null || !treeItem.Label.StartsWith(this.TextToSearchInTree, StringComparison.CurrentCultureIgnoreCase)))
                {
                    return false;
                }

                return true;
            };
        }

        /// <summary>
        /// Occurs when a master data object is selected by the user.
        /// </summary>
        public event MasterDataEntitySelectionHandler CountryOrSupplierSelected;

        #region Commands

        /// <summary>
        /// Gets the close screen command
        /// </summary>
        public ICommand CloseScreen { get; private set; }

        /// <summary>
        /// Gets the select item command
        /// </summary>
        public ICommand SelectItem { get; private set; }

        /// <summary>
        /// Gets the command triggered by the selection change
        /// </summary>
        public ICommand SelectedTreeItemChangedCommand { get; private set; }

        /// <summary>
        /// Gets the command triggered by the double click event
        /// </summary>
        public ICommand MouseDoubleClickCommand { get; private set; }

        /// <summary>
        /// Gets the key down command
        /// </summary>
        public ICommand KeyDownCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets or sets the name of the country for which the browser displays the suppliers
        /// </summary>
        public string CountryName { get; set; }

        /// <summary>
        /// Gets or sets the load location for the country and suppliers
        /// </summary>
        public DbIdentifier LoadLocation { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is loading data (the countries or suppliers).
        /// </summary>        
        public bool IsLoadingData
        {
            get { return this.isLoadingData; }
            set { this.SetProperty(ref this.isLoadingData, value, () => this.IsLoadingData); }
        }

        /// <summary>
        /// Gets or sets the title text.
        /// </summary>
        public string TitleText
        {
            get
            {
                return this.titleText;
            }

            set
            {
                if (this.titleText != value)
                {
                    this.titleText = value;
                    OnPropertyChanged("TitleText");
                }
            }
        }

        /// <summary>
        /// Gets or sets the header text.
        /// </summary>
        public string HeaderText
        {
            get
            {
                return this.headerText;
            }

            set
            {
                if (this.headerText != value)
                {
                    this.headerText = value;
                    OnPropertyChanged("HeaderText");
                }
            }
        }

        /// <summary>
        /// Gets or sets the tree items.
        /// </summary>
        public ObservableCollection<TreeViewDataItem> TreeItems
        {
            get
            {
                return this.treeItems;
            }

            set
            {
                if (this.treeItems != value)
                {
                    this.treeItems = value;
                    OnPropertyChanged("TreeItems");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to load the suppliers or not.
        /// </summary>
        public bool LoadSuppliers { get; set; }

        /// <summary>
        /// Gets or sets the text entered in the search TextBox.
        /// </summary>
        public string TextToSearchInTree
        {
            get
            {
                return this.textToSearchInTree;
            }

            set
            {
                var oldValue = this.textToSearchInTree;
                this.SetProperty(ref this.textToSearchInTree, value, () => this.TextToSearchInTree);
            }
        }

        /// <summary>
        /// Gets or sets the loaded data.
        /// </summary>
        public IEnumerable<object> LoadedData
        {
            get { return this.loadedData; }
            set { this.SetProperty(ref this.loadedData, value, () => this.LoadedData); }
        }

        /// <summary>
        /// Gets or sets a Filter predicate, applied on TreeView Items.
        /// </summary>
        public VMProperty<Predicate<object>> TreeFilter { get; set; }

        #endregion Properties

        #region Actions

        /// <summary>
        /// Called when the view has been loaded and starts loading the countries/suppliers from the database.
        /// <para/>        
        /// During unit tests this method must be manually called  because there is no view to call it.
        /// </summary>
        public override void OnLoaded()
        {
            this.TreeItems = null;
            this.TitleText = string.Empty;
            this.HeaderText = null;
            this.loadMDWorker.RunWorkerAsync();
        }

        /// <summary>
        /// Called when the view has been unloaded.
        /// </summary>
        public override void OnUnloaded()
        {
            this.LoadedData = null;
            this.TreeItems = null;
            this.TextToSearchInTree = null;
        }

        /// <summary>
        /// Closes the screen
        /// </summary>
        private void CloseScreenAction()
        {
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Selects an item and closes the screen
        /// </summary>
        private void SelectItemAction()
        {
            if (this.CountryOrSupplierSelected == null)
            {
                return;
            }

            if (this.selectedTreeItem != null && this.selectedTreeItem.DataObject != null)
            {
                this.CountryOrSupplierSelected(this.selectedTreeItem.DataObject, this.LoadLocation);
            }

            this.CloseScreenAction();
        }

        /// <summary>
        /// Handles the key up event
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.Input.KeyEventArgs"/> instance containing the event data.</param>
        private void HandleKeyDown(KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                SelectItemAction();
                e.Handled = true;
            }
            else if (e.Key == Key.Escape)
            {
                CloseScreenAction();
                e.Handled = true;
            }
        }

        /// <summary>
        /// Handles the double click event
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.Input.MouseEventArgs"/> instance containing the event data.</param>
        private void DoubleClickAction(MouseEventArgs e)
        {
            e.Handled = true;
            SelectItemAction();
        }

        #endregion

        #region Load Data

        /// <summary>
        /// Handles the DoWork event of the LoadMDWorker control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.DoWorkEventArgs"/> instance containing the event data.</param>
        private void LoadMDWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            this.IsLoadingData = true;
            object data = null;

            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(this.LoadLocation);
            if (this.CountryName == null
                && !this.LoadSuppliers)
            {
                data = dataManager.CountryRepository.GetAll();
            }
            else if (this.CountryName == null
                && this.LoadSuppliers)
            {
                data = dataManager.CountrySupplierRepository.FindAll();
            }
            else
            {
                var selectedCountry = dataManager.CountryRepository.GetByName(this.CountryName);
                if (selectedCountry != null)
                {
                    data = selectedCountry.States;
                }
            }

            this.LoadedData = data as IEnumerable<object>;
            e.Result = data;
        }

        /// <summary>
        /// Handles the RunWorkerCompleted event of the LoadMDWorker control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.RunWorkerCompletedEventArgs"/> instance containing the event data.</param>
        private void LoadMDWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.IsLoadingData = false;

            // Show an error message if needed
            if (e.Error != null)
            {
                this.windowService.MessageDialogService.Show(e.Error);
            }
            else
            {
                object result = e.Result;
                if (result != null)
                {
                    // Display the loaded master data
                    if (this.LoadLocation == DbIdentifier.CentralDatabase)
                    {
                        this.TitleText = LocalizedResources.General_CentralDb;
                    }
                    else if (this.LoadLocation == DbIdentifier.LocalDatabase)
                    {
                        this.TitleText = LocalizedResources.General_LocalDb;
                    }

                    this.DisplayLoadedData(result);
                }
            }
        }

        /// <summary>
        /// Prepares the data countries or states for being displayed by the view.
        /// </summary>
        /// <param name="data">The data countries or country states.</param>
        /// <returns>A Collection of TreeViewDataItem containing the representation of the countries or country states.</returns>
        private ObservableCollection<TreeViewDataItem> PrepareDataForDisplay(object data)
        {
            if (data is IEnumerable<Country>)
            {
                IEnumerable<Country> countries = data as IEnumerable<Country>;
                this.HeaderText = LocalizedResources.General_Countries;
                if (countries == null)
                {
                    return new ObservableCollection<TreeViewDataItem>();
                }

                List<TreeViewDataItem> items = new List<TreeViewDataItem>();
                foreach (Country country in countries.OrderBy(p => p.Name))
                {
                    TreeViewDataItem item = new TreeViewDataItem() { DataObject = country, Label = country.Name, IconResourceKey = Images.OtherUsersProjectsIconKey };
                    items.Add(item);
                }

                return new ObservableCollection<TreeViewDataItem>(items);
            }
            else if (data is IEnumerable<CountryState>)
            {
                IEnumerable<CountryState> states = data as IEnumerable<CountryState>;
                this.HeaderText = LocalizedResources.General_States;
                if (states == null)
                {
                    return new ObservableCollection<TreeViewDataItem>();
                }

                List<TreeViewDataItem> items = new List<TreeViewDataItem>();
                foreach (CountryState state in states)
                {
                    TreeViewDataItem item = new TreeViewDataItem() { DataObject = state, Label = state.Name, IconResourceKey = Images.SupplierIconKey };
                    items.Add(item);
                }

                return new ObservableCollection<TreeViewDataItem>(items);
            }
            else
            {
                return new ObservableCollection<TreeViewDataItem>();
            }
        }

        /// <summary>
        /// Displays for browsing the data that was loaded
        /// </summary>
        /// <param name="data">The data to display.</param>
        private void DisplayLoadedData(object data)
        {
            this.TreeItems = this.PrepareDataForDisplay(data);

            // Select the 1st item.
            var firstItem = this.TreeItems.FirstOrDefault();
            if (firstItem != null)
            {
                firstItem.IsSelected = true;
            }
        }

        #endregion Load Data
    }
}