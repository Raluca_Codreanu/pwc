﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Windows.Input;
using ZPKTool.Controls;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for the ManageCountries view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ManageCountriesViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The composition container.
        /// </summary>
        private CompositionContainer container;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The currently selected country.
        /// </summary>
        private Country selectedCountry;

        /// <summary>
        /// The currently selected country supplier.
        /// </summary>
        private CountryState selectedCountrySupplier;

        /// <summary>
        /// The country view model
        /// </summary>
        private CountryViewModel countryViewModel;

        /// <summary>
        /// The country supplier view model
        /// </summary>
        private CountrySupplierViewModel countrySupplierViewModel;

        /// <summary>
        /// The backup for the valid  country supplier view model
        /// </summary>
        private CountrySupplierViewModel countrySupplierViewModelBackup;

        /// <summary>
        /// A list of countries.
        /// </summary>
        private ObservableCollection<Country> countries;

        /// <summary>
        /// A list of the country states.
        /// </summary>
        private ObservableCollection<CountryState> countryStates;

        /// <summary>
        /// The data source manager to use for data operations involving the Model.
        /// </summary>
        private IDataSourceManager dataSourceManager;

        #endregion Attributes

        /// <summary>
        /// Initializes a new instance of the <see cref="ManageCountriesViewModel"/> class.
        /// </summary>
        /// <param name="countryViewModel">The country view model.</param>
        /// <param name="suppliersViewModel">The suppliers view model.</param>
        /// <param name="messenger">The messenger.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="container">The container.</param>
        [ImportingConstructor]
        public ManageCountriesViewModel(
            CountryViewModel countryViewModel,
            CountrySupplierViewModel suppliersViewModel,
            IMessenger messenger,
            IWindowService windowService,
            CompositionContainer container)
        {
            this.Messenger = messenger;
            this.windowService = windowService;
            this.container = container;

            Argument.IsNotNull("countryViewModel", countryViewModel);
            Argument.IsNotNull("suppliersViewModel", suppliersViewModel);

            this.CountryViewModel = countryViewModel;
            this.CountrySupplierViewModel = suppliersViewModel;
            this.countrySupplierViewModelBackup = suppliersViewModel;

            this.InitializeCommands();
            this.RegisterMessageHandlers();
        }

        #region Commands

        /// <summary>
        /// Gets the command for the add country button
        /// </summary>
        public ICommand AddCountryCommand { get; private set; }

        /// <summary>
        /// Gets the command for the add supplier button
        /// </summary>
        public ICommand AddSupplierCommand { get; private set; }

        /// <summary>
        /// Gets the command for the delete country button
        /// </summary>
        public ICommand DeleteCountryCommand { get; private set; }

        /// <summary>
        /// Gets the command for the delete supplier button
        /// </summary>
        public ICommand DeleteSupplierCommand { get; private set; }

        /// <summary>
        /// Gets the command for the import countries button.
        /// </summary>
        public ICommand ImportCountrySettingsCommand { get; private set; }

        /// <summary>
        /// Gets the command for the export countries button.
        /// </summary>
        public ICommand ExportCountrySettingsCommand { get; private set; }

        /// <summary>
        /// Gets the command for the PreviewMouseDown event in CountriesDataGrid
        /// </summary>
        public ICommand CountriesDataGridMouseDownCommand { get; private set; }

        /// <summary>
        /// Gets the command for the PreviewMouseUp event in CountriesDataGrid
        /// </summary>
        public ICommand CountriesDataGridKeyDownCommand { get; private set; }

        /// <summary>
        /// Gets the command for the PreviewMouseDown event in StatesDataGrid
        /// </summary>
        public ICommand StatesDataGridMouseDownCommand { get; private set; }

        /// <summary>
        /// Gets the command for the PreviewMouseUp event in StatesDataGrid
        /// </summary>
        public ICommand StatesDataGridKeyDownCommand { get; private set; }

        /// <summary>
        /// Gets the command for the SelectionChanged event
        /// </summary>
        public ICommand SelectionChangedCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets the messenger service.
        /// </summary>
        protected IMessenger Messenger { get; private set; }

        /// <summary>
        /// Gets the data source that contains the whole countries.
        /// </summary>
        public ObservableCollection<Country> Countries
        {
            get { return this.countries; }
            private set { this.SetProperty(ref this.countries, value, () => Countries); }
        }

        /// <summary>
        /// Gets or sets the country states.
        /// </summary>                                        
        public ObservableCollection<CountryState> CountryStates
        {
            get { return this.countryStates; }
            set { this.SetProperty(ref this.countryStates, value, () => this.CountryStates); }
        }

        /// <summary>
        /// Gets or sets the current selected country from the countries` grid, for displaying the country`s information.
        /// </summary>
        public Country SelectedCountry
        {
            get { return this.selectedCountry; }
            set { this.SetProperty(ref this.selectedCountry, value, () => this.SelectedCountry, this.OnSelectedCountryChanged); }
        }

        /// <summary>
        /// Gets or sets the current selected country supplier from the supplier's grid.
        /// </summary>
        public CountryState SelectedCountrySupplier
        {
            get { return this.selectedCountrySupplier; }
            set { this.SetProperty(ref this.selectedCountrySupplier, value, () => this.SelectedCountrySupplier, this.OnSelectedCountryStateChanged); }
        }

        /// <summary>
        /// Gets the CountryViewModel
        /// </summary>
        public CountryViewModel CountryViewModel
        {
            get { return this.countryViewModel; }
            private set { this.SetProperty(ref this.countryViewModel, value, () => this.CountryViewModel); }
        }

        /// <summary>
        /// Gets the CountrySupplierViewModel
        /// </summary>
        public CountrySupplierViewModel CountrySupplierViewModel
        {
            get { return this.countrySupplierViewModel; }
            private set { this.SetProperty(ref this.countrySupplierViewModel, value, () => this.CountrySupplierViewModel); }
        }

        /// <summary>
        /// Gets or sets the data manager used to perform data related operations.
        /// </summary>        
        public IDataSourceManager DataSourceManager
        {
            get { return this.dataSourceManager; }
            set { this.SetProperty(ref this.dataSourceManager, value, () => this.DataSourceManager, this.OnDataSourceManagerChanged); }
        }

        #endregion

        /// <summary>
        /// Initializes commands.
        /// </summary>
        private void InitializeCommands()
        {
            this.AddCountryCommand = new DelegateCommand(this.AddCountry, () => !this.IsReadOnly);
            this.AddSupplierCommand = new DelegateCommand(this.AddSupplier, () => !this.IsReadOnly && this.SelectedCountry != null);
            this.DeleteCountryCommand = new DelegateCommand(this.DeleteCountry, () => !this.IsReadOnly && this.SelectedCountry != null);
            this.DeleteSupplierCommand = new DelegateCommand(this.DeleteSupplier, () => !this.IsReadOnly && this.SelectedCountry != null && this.CountryStates != null && this.CountryStates.Count > 0);
            this.ImportCountrySettingsCommand = new DelegateCommand(this.ImportCountrySettings, () => !this.IsReadOnly);
            this.ExportCountrySettingsCommand = new DelegateCommand(this.ExportCountrySettings, () => !this.IsReadOnly);
            this.CountriesDataGridMouseDownCommand = new DelegateCommand<object>(this.HandleCountriesListPreviewMouseDown);
            this.CountriesDataGridKeyDownCommand = new DelegateCommand<object>(this.HandleCountriesListPreviewKeyDown);
            this.StatesDataGridMouseDownCommand = new DelegateCommand<object>(this.HandleStatesListPreviewMouseDown);
            this.StatesDataGridKeyDownCommand = new DelegateCommand<object>(this.HandleStatesListPreviewKeyDown);
        }

        /// <summary>
        /// Loads the data from DB.
        /// </summary>
        private void LoadSourceData()
        {
            Collection<Country> countries = this.DataSourceManager.CountryRepository.GetAll();
            this.Countries = new ObservableCollection<Country>(countries.OrderBy(c => c.Name));
            this.SelectedCountry = this.Countries.FirstOrDefault();
        }

        /// <summary>
        /// Registers the necessary message handlers with the Messenger service.
        /// </summary>
        private void RegisterMessageHandlers()
        {
            this.Messenger.Register<EntityImportedMessage>(this.HandleNotificationMessage);
        }

        /// <summary>
        /// Handles some generic messages, of type EntityImportedMessage.
        /// </summary>
        /// <param name="message">The message.</param>
        protected virtual void HandleNotificationMessage(EntityImportedMessage message)
        {
            if (message.EntityType == ImportedEntityType.CountrySetting)
            {
                this.LoadSourceData();
            }
        }

        /// <summary>
        /// Sets the selected country
        /// </summary>
        private void OnSelectedCountryChanged()
        {
            var model = this.SelectedCountry;
            if (model == null)
            {
                return;
            }

            this.CountryViewModel.EditMode = ViewModelEditMode.Edit;
            this.CountryViewModel.Model = model;
            this.CountryStates = new ObservableCollection<CountryState>(model.States.OrderBy(c => c.Name));

            // If the new selected country supplier is null, set the country supplier VM as null (it has no data);
            // Else, if the country supplier VM is null, set the backup VM for it.
            var newSelectedSupplier = this.CountryStates.FirstOrDefault();
            if (newSelectedSupplier == null)
            {
                this.CountrySupplierViewModel = null;
            }
            else
            {
                if (this.CountrySupplierViewModel == null)
                {
                    this.CountrySupplierViewModel = this.countrySupplierViewModelBackup;
                }
            }

            this.SelectedCountrySupplier = newSelectedSupplier;
        }

        /// <summary>
        /// Sets the selected country supplier
        /// </summary>
        private void OnSelectedCountryStateChanged()
        {
            var model = this.SelectedCountrySupplier;
            if (model != null)
            {
                // If the VM is null, set the backup VM for it.
                if (this.CountrySupplierViewModel == null)
                {
                    this.CountrySupplierViewModel = this.countrySupplierViewModelBackup;
                }

                this.CountrySupplierViewModel.EditMode = ViewModelEditMode.Edit;
                this.CountrySupplierViewModel.Model = model;
            }
        }

        /// <summary>
        /// Called when DataSourceManager has changed.
        /// </summary>
        private void OnDataSourceManagerChanged()
        {
            this.CountryViewModel.DataSourceManager = this.DataSourceManager;
            if (this.CountrySupplierViewModel != null)
            {
                this.CountrySupplierViewModel.DataSourceManager = this.DataSourceManager;
            }

            this.LoadSourceData();
        }

        /// <summary>
        /// Called before the control is removed from the main area.
        /// </summary>
        /// <returns>
        ///   <c>True</c> if the unload was successful; <c>False</c> otherwise
        /// </returns>
        public override bool OnUnloading()
        {
            if (this.CountryViewModel.IsChanged || (this.CountrySupplierViewModel != null && this.CountrySupplierViewModel.IsChanged))
            {
                this.CountryViewModel.ShowCancelMessageFlag = false;

                if (this.CountrySupplierViewModel != null)
                {
                    this.CountrySupplierViewModel.ShowCancelMessageFlag = false;
                }

                try
                {
                    MessageDialogResult result = windowService.MessageDialogService.Show(LocalizedResources.Question_UnsavedDataOnQuit, MessageDialogType.YesNoCancel);
                    if (result == MessageDialogResult.Yes)
                    {
                        if (this.CountryViewModel.SaveCommand.CanExecute(null)
                            && (this.CountrySupplierViewModel == null || (this.CountrySupplierViewModel != null && this.CountrySupplierViewModel.SaveCommand.CanExecute(null))))
                        {
                            this.CountryViewModel.SaveCommand.Execute(null);

                            if (this.CountrySupplierViewModel != null)
                            {
                                this.CountrySupplierViewModel.SaveCommand.Execute(null);
                            }
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else if (result == MessageDialogResult.No)
                    {
                        if (this.CountryViewModel.CancelCommand.CanExecute(null))
                        {
                            this.CountryViewModel.CancelCommand.Execute(null);

                            if (this.CountrySupplierViewModel != null && this.CountrySupplierViewModel.CancelCommand.CanExecute(null))
                            {
                                this.CountrySupplierViewModel.CancelCommand.Execute(null);
                            }
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                finally
                {
                    this.CountryViewModel.ShowCancelMessageFlag = true;

                    if (this.CountrySupplierViewModel != null)
                    {
                        this.CountrySupplierViewModel.ShowCancelMessageFlag = true;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Open the create new country window
        /// </summary>
        private void AddCountry()
        {
            CountryViewModel viewModel = container.GetExportedValue<CountryViewModel>();
            viewModel.DataSourceManager = this.DataSourceManager;
            viewModel.InitializeForCreation();
            this.windowService.ShowViewInDialog(viewModel, "CreateCountryViewTemplate");

            if (viewModel.Saved)
            {
                var newCountry = viewModel.Model;
                this.Countries.Add(newCountry);
            }
        }

        /// <summary>
        /// Open the create new supplier window
        /// </summary>
        private void AddSupplier()
        {
            var country = this.SelectedCountry;
            if (country == null)
            {
                return;
            }

            CountrySupplierViewModel viewModel = container.GetExportedValue<CountrySupplierViewModel>();
            viewModel.DataSourceManager = this.DataSourceManager;
            viewModel.InitializeForCreation(country);
            this.windowService.ShowViewInDialog(viewModel, "CreateCountrySupplierViewTemplate");

            var newSupplier = viewModel.Model;
            if (viewModel.Saved && country.States.Contains(newSupplier))
            {
                this.CountryStates.Add(newSupplier);
                this.SelectedCountrySupplier = newSupplier;
            }
        }

        /// <summary>
        /// Deletes the selected country
        /// </summary>
        private void DeleteCountry()
        {
            if (this.SelectedCountry == null)
            {
                return;
            }

            var message = string.Format(LocalizedResources.Question_Delete_Item, this.SelectedCountry.Name);
            MessageDialogResult result = windowService.MessageDialogService.Show(message, MessageDialogType.YesNo);
            if (result == MessageDialogResult.No)
            {
                return;
            }

            this.DataSourceManager.CountryRepository.RemoveAll(this.SelectedCountry);
            this.DataSourceManager.SaveChanges();
            this.Countries.Remove(this.SelectedCountry);
            this.SelectedCountry = this.Countries.FirstOrDefault();
        }

        /// <summary>
        /// Deletes the selected supplier
        /// </summary>
        private void DeleteSupplier()
        {
            if (this.SelectedCountrySupplier == null)
            {
                return;
            }

            var message = string.Format(LocalizedResources.Question_Delete_Item, this.SelectedCountrySupplier.Name);
            MessageDialogResult result = windowService.MessageDialogService.Show(message, MessageDialogType.YesNo);
            if (result == MessageDialogResult.No)
            {
                return;
            }

            this.DataSourceManager.CountrySupplierRepository.RemoveAll(this.SelectedCountrySupplier);
            this.DataSourceManager.SaveChanges();
            this.CountryStates.Remove(this.SelectedCountrySupplier);

            // If the deleted supplier is the last one from the grid, set the country supplier VM as null (it has no data).
            var newSelectedSupplier = this.CountryStates.FirstOrDefault();
            if (newSelectedSupplier == null)
            {
                this.CountrySupplierViewModel = null;
            }

            this.SelectedCountrySupplier = newSelectedSupplier;
        }

        /// <summary>
        /// Imports the country settings.
        /// </summary>
        private void ImportCountrySettings()
        {
            var viewModel = container.GetExportedValue<MasterDataImporterExporterViewModel>();
            viewModel.Operation = MasterDataOperation.Import;
            viewModel.EntityType = ImportedEntityType.CountrySetting;
            viewModel.DataManager = this.DataSourceManager;
            this.windowService.ShowViewInDialog(viewModel);
        }

        /// <summary>
        /// Exports the country settings.
        /// </summary>
        private void ExportCountrySettings()
        {
            var viewModel = container.GetExportedValue<MasterDataImporterExporterViewModel>();
            viewModel.Operation = MasterDataOperation.Export;
            viewModel.EntityType = ImportedEntityType.CountrySetting;
            viewModel.DataManager = this.DataSourceManager;
            this.windowService.ShowViewInDialog(viewModel);
        }

        #region Events for CountriesDataGrid

        /// <summary>
        /// Handles the PreviewKeyDown event of the CountriesDataGrid control.
        /// </summary>
        /// <param name="parameter">The command's parameter.</param>
        private void HandleCountriesListPreviewKeyDown(object parameter)
        {
            this.OnBeforeSelectedCountryChange(parameter);
        }

        /// <summary>
        /// Handles the PreviewMouseDown event of the CountriesDataGrid control.
        /// </summary>
        /// <param name="parameter">The command's parameter.</param>
        private void HandleCountriesListPreviewMouseDown(object parameter)
        {
            this.OnBeforeSelectedCountryChange(parameter);
        }

        /// <summary>
        /// Check if the model is changed before selected country change
        /// </summary>
        /// <param name="parameter">The command's parameter.</param>
        private void OnBeforeSelectedCountryChange(object parameter)
        {
            Country selectedCountry = parameter as Country;

            if (selectedCountry == null)
            {
                return;
            }

            if (this.CountryViewModel.IsChanged)
            {
                try
                {
                    // set the flag for the cancel window message from the CountryViewModel
                    this.countryViewModel.ShowCancelMessageFlag = false;

                    MessageDialogResult result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_ChangedSave, MessageDialogType.YesNo);
                    if (result == MessageDialogResult.Yes)
                    {
                        if (this.CountryViewModel.SaveCommand.CanExecute(null))
                        {
                            this.CountryViewModel.SaveCommand.Execute(null);
                            this.SelectedCountry = selectedCountry;
                        }
                    }
                    else
                    {
                        if (this.CountryViewModel.CancelCommand.CanExecute(null))
                        {
                            this.CountryViewModel.CancelCommand.Execute(null);
                            this.SelectedCountry = selectedCountry;
                        }
                    }
                }
                finally
                {
                    // reset the flag for the cancel window message to default
                    this.countryViewModel.ShowCancelMessageFlag = true;
                }
            }
        }

        #endregion

        #region Events for StatesDataGrid
        /// <summary>
        /// Handles the PreviewKeyDown event of the StatesDataGrid control.
        /// </summary>
        /// <param name="parameter">The command's parameter.</param>
        private void HandleStatesListPreviewKeyDown(object parameter)
        {
            this.OnBeforeSelectedCountrySupplierChange(parameter);
        }

        /// <summary>
        /// Handles the PreviewMouseDown event of the StatesDataGrid control.
        /// </summary>
        /// <param name="parameter">The command's parameter.</param>
        private void HandleStatesListPreviewMouseDown(object parameter)
        {
            this.OnBeforeSelectedCountrySupplierChange(parameter);
        }

        /// <summary>
        /// Check if the model is changed before selected country supplier change
        /// </summary>
        /// <param name="parameter">The command's parameter.</param>
        private void OnBeforeSelectedCountrySupplierChange(object parameter)
        {
            CountryState selectedCountrySupplier = parameter as CountryState;

            // If the clicked row doesn't contain a valid country supplier object or if it is the currently selected,
            // simply return.
            if (selectedCountrySupplier == null)
            {
                return;
            }

            if (this.CountrySupplierViewModel != null && this.CountrySupplierViewModel.IsChanged)
            {
                try
                {
                    // set the flag for the cancel window message from the CountrySupplierViewModel
                    this.countrySupplierViewModel.ShowCancelMessageFlag = false;

                    MessageDialogResult result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_ChangedSave, MessageDialogType.YesNo);
                    if (result == MessageDialogResult.Yes)
                    {
                        if (this.CountrySupplierViewModel.SaveCommand.CanExecute(null))
                        {
                            this.CountrySupplierViewModel.SaveCommand.Execute(null);
                            this.SelectedCountrySupplier = selectedCountrySupplier;
                        }
                    }
                    else
                    {
                        if (this.CountrySupplierViewModel.CancelCommand.CanExecute(null))
                        {
                            this.CountrySupplierViewModel.CancelCommand.Execute(null);
                            this.SelectedCountrySupplier = selectedCountrySupplier;
                        }
                    }
                }
                finally
                {
                    // reset the flag for the cancel window message to default
                    if (this.CountrySupplierViewModel != null)
                    {
                        this.countrySupplierViewModel.ShowCancelMessageFlag = true;
                    }
                }
            }

            #endregion
        }
    }
}
