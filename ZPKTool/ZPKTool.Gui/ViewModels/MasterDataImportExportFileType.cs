﻿namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Supported import\export file types.
    /// </summary>
    public enum MasterDataImportExportFileType
    {
        /// <summary>
        /// File Type Not Set.
        /// </summary>
        None,

        /// <summary>
        /// Excel 2007 File Format.
        /// </summary>
        Xlsx,

        /// <summary>
        /// Excel 2003 File Format.
        /// </summary>
        Xls
    }
}