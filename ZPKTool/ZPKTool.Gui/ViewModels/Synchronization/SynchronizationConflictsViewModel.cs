﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;
using ZPKTool.Synchronization;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view model of the SynchronizationConflictsView.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class SynchronizationConflictsViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The synchronization service.
        /// </summary>
        private readonly ISyncService syncService;

        /// <summary>
        /// The window service.
        /// </summary>
        private readonly IWindowService windowService;

        /// <summary>
        /// The messenger.
        /// </summary>
        private readonly IMessenger messenger;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SynchronizationConflictsViewModel"/> class.
        /// </summary>
        /// <param name="syncService">The synchronize service.</param>
        /// <param name="windowService">The window service.</param>
        /// <param name="messenger">The messenger.</param>
        [ImportingConstructor]
        public SynchronizationConflictsViewModel(
            ISyncService syncService,
            IWindowService windowService,
            IMessenger messenger)
        {
            Argument.IsNotNull("syncService", syncService);
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("messenger", messenger);

            this.windowService = windowService;
            this.syncService = syncService;
            this.messenger = messenger;

            this.InitializeProperties();

            this.ResolveConflictsCommand = new DelegateCommand(this.ResolveConflicts);
            this.OpenConflictDetailsPopupCommand = new DelegateCommand<SynchronizationConflictGridItem>(this.ShowConflictDetails);
            this.CloseConflictDetailsPopupCommand = new DelegateCommand(() => this.IsConflictDetailsPopupOpen.Value = false);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the sync conflict items to display in data grid.
        /// </summary>
        public VMProperty<ObservableCollection<SynchronizationConflictGridItem>> ConflictItems { get; private set; }

        /// <summary>
        /// Gets the selected conflict resolution.
        /// </summary>
        public VMProperty<SynchronizationConflictResolution> SelectedConflictResolution { get; private set; }

        /// <summary>
        /// Gets the detailed conflict values to display in popup, with side by side values of conflict.
        /// </summary>
        public VMProperty<List<ConflictDetails>> ConflictDetails { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the resolve conflicts button should be enabled or not.
        /// </summary>
        public VMProperty<bool> IsResolveButtonEnabled { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the popup with detailed values of conflict is opened or not.
        /// </summary>
        public VMProperty<bool> IsConflictDetailsPopupOpen { get; private set; }

        #endregion

        #region Commands

        /// <summary>
        /// Gets the command to resolve conflicts.
        /// </summary>
        public ICommand ResolveConflictsCommand { get; private set; }

        /// <summary>
        /// Gets the command to open the conflict detailed values popup.
        /// </summary>
        public ICommand OpenConflictDetailsPopupCommand { get; private set; }

        /// <summary>
        /// Gets the command to close the conflict detailed values popup.
        /// </summary>
        public ICommand CloseConflictDetailsPopupCommand { get; private set; }

        #endregion

        #region Implementation

        /// <summary>
        /// Initializes the properties of this instance.
        /// </summary>
        private void InitializeProperties()
        {
            this.ConflictItems.Value = new ObservableCollection<SynchronizationConflictGridItem>(this.syncService.SyncManager.Conflicts.Select(conflict => this.InitializeConflictGridItem(conflict)));
            this.IsResolveButtonEnabled.Value = this.ConflictItems.Value.Count > 0;

            this.SelectedConflictResolution.ValueChanged += (s, e) => this.OnConflictResolutionChanged();
        }

        /// <summary>
        /// Called when the selected resolution for all conflicts changed.
        /// </summary>
        private void OnConflictResolutionChanged()
        {
            foreach (var conflict in this.ConflictItems.Value)
            {
                conflict.Resolution = this.SelectedConflictResolution.Value;
            }
        }

        /// <summary>
        /// Opens the popup to display the detailed values of a conflict, side by side.
        /// </summary>
        /// <param name="conflict">The conflict.</param>
        private void ShowConflictDetails(SynchronizationConflictGridItem conflict)
        {
            this.GenerateConflictDetailedValues(conflict);
            this.IsConflictDetailsPopupOpen.Value = true;
        }

        /// <summary>
        /// Resolves the conflicts by starting the synchronization for the current conflicts.
        /// </summary>
        private void ResolveConflicts()
        {
            foreach (var conflict in this.syncService.SyncManager.Conflicts)
            {
                conflict.IsResolutionSetByUser = true;
            }

            // Send a message to notify that the sync started
            this.messenger.Send(new SynchronizationStartedMessage());

            Task.Factory.StartNew(() =>
            {
                this.syncService.SyncManager.ResynchronizeAndResolveConflicts(SecurityManager.Instance.CurrentUser.Guid);
            });

            this.Close();
        }

        /// <summary>
        /// Closes this instance.
        /// </summary>
        protected override void Close()
        {
            this.windowService.CloseViewWindow(this);
        }

        #endregion

        #region Conflict grid item related methods

        /// <summary>
        /// Creates a data grid item for the conflict and sets it properties.
        /// </summary>
        /// <param name="conflict">The conflict.</param>
        /// <returns>The conflict grid item</returns>
        private SynchronizationConflictGridItem InitializeConflictGridItem(SynchronizationConflict conflict)
        {
            if (conflict == null)
            {
                return null;
            }

            var gridItem = new SynchronizationConflictGridItem(conflict);
            gridItem.SyncTask = this.GetGridItemSyncTask(conflict.SyncTask);
            gridItem.Type = this.GetGridItemType(conflict.EntityType);
            gridItem.Name = this.GetGridItemName(conflict);

            return gridItem;
        }

        /// <summary>
        /// Gets synchronize task to display for conflict data grid item.
        /// </summary>
        /// <param name="syncTask">The synchronize task.</param>
        /// <returns>The string for conflict sync task.</returns>
        private string GetGridItemSyncTask(SyncType syncTask)
        {
            string task = string.Empty;
            switch (syncTask)
            {
                case SyncType.StaticDataAndSettings:
                    task = LocalizedResources.Synchronization_SDAndSettings;
                    break;
                case SyncType.ReleasedProjects:
                    task = LocalizedResources.General_ReleasedProjects;
                    break;
                case SyncType.MasterData:
                    task = LocalizedResources.General_IsMasterData;
                    break;
                case SyncType.MyProjects:
                    task = LocalizedResources.General_MyProjects;
                    break;
            }

            return task;
        }

        /// <summary>
        /// Gets the entity type to display for conflict data grid item.
        /// </summary>
        /// <param name="entityType">The entity type.</param>
        /// <returns>The string for conflict entity type.</returns>
        private string GetGridItemType(string entityType)
        {
            string type = string.Empty;
            switch (entityType)
            {
                case "AccessRights":
                    type = LocalizedResources.General_AccessRight;
                    break;
                case "Assemblies":
                    type = LocalizedResources.General_Assembly;
                    break;
                case "AssemblyMedia":
                    type = LocalizedResources.General_AssemblyMedia;
                    break;
                case "BasicSettings":
                    type = LocalizedResources.General_BasicSettings;
                    break;
                case "Commodities":
                    type = LocalizedResources.General_Commodity;
                    break;
                case "CommoditiesPriceHistory":
                    type = LocalizedResources.General_CommodityPriceHistory;
                    break;
                case "Consumables":
                    type = LocalizedResources.General_Consumable;
                    break;
                case "ConsumablesPriceHistory":
                    type = LocalizedResources.General_ConsumablePriceHistory;
                    break;
                case "Countries":
                    type = LocalizedResources.General_Country;
                    break;
                case "CountrySettings":
                    type = LocalizedResources.General_CountrySettings;
                    break;
                case "CountrySettingsValueHistory":
                    type = LocalizedResources.General_CountrySettingsValueHistory;
                    break;
                case "CountryStates":
                    type = LocalizedResources.General_Supplier;
                    break;
                case "Currencies":
                    type = LocalizedResources.General_Currency;
                    break;
                case "Customers":
                    type = LocalizedResources.General_Supplier;
                    break;
                case "CycleTimeCalculations":
                    type = LocalizedResources.General_CycleTime;
                    break;
                case "Dies":
                    type = LocalizedResources.General_Die;
                    break;
                case "Machines":
                    type = LocalizedResources.General_Machine;
                    break;
                case "MachinesClassification":
                    type = LocalizedResources.General_MachineClassification;
                    break;
                case "Manufacturers":
                    type = LocalizedResources.General_Manufacturer;
                    break;
                case "MaterialsClassification":
                    type = LocalizedResources.General_MaterialClassification;
                    break;
                case "MeasurementUnits":
                    type = LocalizedResources.General_MeasurementUnit;
                    break;
                case "Media":
                    type = LocalizedResources.General_Media;
                    break;
                case "OverheadSettings":
                    type = LocalizedResources.General_OverheadSettings;
                    break;
                case "PartMedia":
                    type = LocalizedResources.General_PartMedia;
                    break;
                case "Parts":
                    type = LocalizedResources.General_Part;
                    break;
                case "Processes":
                    type = LocalizedResources.General_Process;
                    break;
                case "ProcessStepAssemblyAmounts":
                    type = LocalizedResources.General_AssemblyAmounts;
                    break;
                case "ProcessStepPartAmounts":
                    type = LocalizedResources.General_PartAmounts;
                    break;
                case "ProcessSteps":
                    type = LocalizedResources.General_ProcessStep;
                    break;
                case "ProcessStepsClassification":
                    type = LocalizedResources.General_ProcessStepClassification;
                    break;
                case "ProjectFolders":
                    type = LocalizedResources.General_ProjectFolder;
                    break;
                case "ProjectMedia":
                    type = LocalizedResources.General_ProjectMedia;
                    break;
                case "Projects":
                    type = LocalizedResources.General_Project;
                    break;
                case "RawMaterials":
                    type = LocalizedResources.General_RawMaterial;
                    break;
                case "RawMaterialsPriceHistory":
                    type = LocalizedResources.General_RawMaterialDeliveryType;
                    break;
                case "RoleAccessRights":
                    type = LocalizedResources.General_RoleAccessRight;
                    break;
                case "Roles":
                    type = LocalizedResources.General_Role;
                    break;
                case "TrashBinItems":
                    type = LocalizedResources.General_TrashBinItem;
                    break;
                case "UserRoles":
                    type = LocalizedResources.General_UserRole;
                    break;
                case "Users":
                    type = LocalizedResources.General_User;
                    break;
            }

            return type;
        }

        /// <summary>
        /// Gets synchronize task to display for conflict data grid item.
        /// </summary>
        /// <param name="conflict">The conflict.</param>
        /// <returns>The string for conflict name.</returns>
        private string GetGridItemName(SynchronizationConflict conflict)
        {
            string localItemName = string.Empty;
            var localConflictData = conflict.LocalEntityVersion as DataRow;
            if (localConflictData != null)
            {
                if (localConflictData.Table.Columns.Contains("Name"))
                {
                    localItemName += localConflictData["Name"].ToString();
                }
            }
            else
            {
                localItemName += "(" + LocalizedResources.Synchronization_Conflicts_EntityDeleted + ")";
            }

            string centralItemName = string.Empty;
            var centralConflictData = conflict.CentralEntityVersion as DataRow;
            if (centralConflictData != null)
            {
                if (centralConflictData.Table.Columns.Contains("Name"))
                {
                    centralItemName += centralConflictData["Name"].ToString();
                }
            }
            else
            {
                centralItemName += "(" + LocalizedResources.Synchronization_Conflicts_EntityDeleted + ")";
            }

            if (string.IsNullOrWhiteSpace(localItemName) && string.IsNullOrWhiteSpace(centralItemName))
            {
                return string.Empty;
            }

            return string.Join(" / ", localItemName, centralItemName);
        }

        /// <summary>
        /// Generates the conflict detailed values.
        /// </summary>
        /// <param name="gridItem">The grid item.</param>
        private void GenerateConflictDetailedValues(SynchronizationConflictGridItem gridItem)
        {
            var conflictDetails = new List<ConflictDetails>();

            var localVersionDataRow = gridItem.Conflict.LocalEntityVersion as DataRow;
            var centralVersionDataRow = gridItem.Conflict.CentralEntityVersion as DataRow;

            if (localVersionDataRow != null &&
                centralVersionDataRow != null &&
                localVersionDataRow.Table != null &&
                !string.IsNullOrEmpty(localVersionDataRow.Table.TableName))
            {
                MapEntity(gridItem.Type, localVersionDataRow, centralVersionDataRow, conflictDetails);
            }

            this.ConflictDetails.Value = conflictDetails;
        }

        /// <summary>
        /// Maps the field.
        /// </summary>
        /// <param name="columnName">Name of the column.</param>
        /// <param name="displayName">The display name.</param>
        /// <param name="localRow">The local row.</param>
        /// <param name="centralRow">The central row.</param>
        /// <param name="conflictDetailedValues">The conflict detailed values list.</param>
        /// <param name="propertyType">The type of the entity.</param>
        private void MapField(string columnName, string displayName, DataRow localRow, DataRow centralRow, List<ConflictDetails> conflictDetailedValues, Type propertyType)
        {
            if (localRow.Table.Columns.Contains(columnName) && centralRow.Table.Columns.Contains(columnName))
            {
                string localVersion = null;
                string centralVersion = null;

                ConvertDisplayValueByType(
                    columnName,
                    localRow,
                    centralRow,
                    ref localVersion,
                    ref centralVersion,
                    propertyType);

                if (!(string.IsNullOrEmpty(localVersion) && string.IsNullOrEmpty(centralVersion)))
                {
                    var conflictDetails = new ConflictDetails();
                    conflictDetails.ColumnName = displayName;
                    conflictDetails.LocalColumnValue = localVersion;
                    conflictDetails.CentralColumnValue = centralVersion;

                    conflictDetailedValues.Add(conflictDetails);
                }
            }
        }

        /// <summary>
        /// Converts the display type of the value by.
        /// </summary>
        /// <param name="columnName">Name of the column.</param>
        /// <param name="localRow">The local row.</param>
        /// <param name="centralRow">The central row.</param>
        /// <param name="localVersion">The local version.</param>
        /// <param name="centralVersion">The central version.</param>
        /// <param name="type">The type of the entity.</param>
        private void ConvertDisplayValueByType(string columnName, DataRow localRow, DataRow centralRow, ref string localVersion, ref string centralVersion, Type type)
        {
            object localObject = localRow.ItemArray[localRow.Table.Columns.IndexOf(columnName)];
            object centralObject = centralRow.ItemArray[centralRow.Table.Columns.IndexOf(columnName)];

            if (type == typeof(string))
            {
                if (NumericUtils.IsNumber(localObject))
                {
                    localVersion = Formatter.FormatNumber(localObject, 4);
                    centralVersion = Formatter.FormatNumber(centralObject, 4);
                }
                else
                {
                    localVersion = localObject.ToString();
                    centralVersion = centralObject.ToString();
                }
            }
            else if (type == typeof(int))
            {
                localVersion = localObject.ToString();
                centralVersion = centralObject.ToString();
            }
            else if (type == typeof(decimal))
            {
                localVersion = Formatter.FormatNumber(localObject, 4);
                centralVersion = Formatter.FormatNumber(centralObject, 4);
            }
            else if (type == typeof(bool))
            {
                bool localValueBool;
                if (bool.TryParse(localObject.ToString(), out localValueBool))
                {
                    if (localValueBool)
                    {
                        localVersion = LocalizedResources.General_True;
                    }
                    else
                    {
                        localVersion = LocalizedResources.General_False;
                    }
                }

                bool centralValueBool;
                if (bool.TryParse(centralObject.ToString(), out centralValueBool))
                {
                    if (centralValueBool)
                    {
                        centralVersion = LocalizedResources.General_True;
                    }
                    else
                    {
                        centralVersion = LocalizedResources.General_False;
                    }
                }
            }
            else if (type.IsEnum)
            {
                if (localObject != DBNull.Value)
                {
                    localVersion = Enum.GetValues(type).GetValue((short)localObject).ToString();
                }

                if (centralObject != DBNull.Value)
                {
                    centralVersion = Enum.GetValues(type).GetValue((short)centralObject).ToString();
                }
            }
            else if (typeof(DateTime) == type)
            {
                if (localObject != DBNull.Value)
                {
                    localVersion = ((DateTime)localObject).ToShortDateString();
                }

                if (centralObject != DBNull.Value)
                {
                    centralVersion = ((DateTime)centralObject).ToShortDateString();
                }
            }
        }

        /// <summary>
        /// Gets the name to display.
        /// </summary>
        /// <param name="columnName">Name of the column.</param>
        /// <param name="entityName">Name of the entity.</param>
        /// <returns>The name to display.</returns>
        private string GetDisplayName(string columnName, string entityName)
        {
            string value = null;

            // Find the resource using the name of the entity.
            value = LocalizedResources.ResourceManager.GetString(entityName + "_" + columnName);
            if (string.IsNullOrEmpty(value))
            {
                // Find the resource using 'General'.
                value = LocalizedResources.ResourceManager.GetString("General" + "_" + columnName);
                if (string.IsNullOrEmpty(value))
                {
                    // If the resource is not found use the name of the column.
                    value = columnName;
                }
            }

            return value;
        }

        /// <summary>
        /// Maps the entity.
        /// </summary>
        /// <param name="entityName">The entity name.</param>
        /// <param name="localRow">The local row.</param>
        /// <param name="centralRow">The central row.</param>
        /// <param name="conflictDetailedValues">The conflict detailed values list.</param>
        private void MapEntity(string entityName, DataRow localRow, DataRow centralRow, List<ConflictDetails> conflictDetailedValues)
        {
            MappingData mappingProperty = new MappingData();
            var propertiesMapping = MappingUtils.GetModelTypeMappings(entityName);

            foreach (DataColumn column in localRow.Table.Columns)
            {
                if (column.DataType != typeof(System.Guid))
                {
                    var displayName = GetDisplayName(column.ColumnName, entityName);
                    mappingProperty = propertiesMapping.FirstOrDefault(p => p.ColumnName == column.ColumnName);

                    if (mappingProperty != null && displayName != null)
                    {
                        MapField(column.ColumnName, displayName, localRow, centralRow, conflictDetailedValues, mappingProperty.EntityType);
                    }
                }
            }
        }

        #endregion
    }
}
