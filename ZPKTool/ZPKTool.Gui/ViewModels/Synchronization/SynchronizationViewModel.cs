﻿namespace ZPKTool.Gui.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.Composition;
    using System.ComponentModel.Composition.Hosting;
    using System.Globalization;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Threading;
    using ZPKTool.Business;
    using ZPKTool.Common;
    using ZPKTool.Controls;
    using ZPKTool.Data;
    using ZPKTool.Gui.Notifications;
    using ZPKTool.Gui.Resources;
    using ZPKTool.Gui.Services;
    using ZPKTool.MvvmCore;
    using ZPKTool.MvvmCore.Commands;
    using ZPKTool.MvvmCore.Services;
    using ZPKTool.Synchronization;

    /// <summary>
    /// The view-model of the Synchronization view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class SynchronizationViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Weak event listener for the Synchronization progress notification.
        /// </summary>
        private static WeakEventListener<SynchronizationProgressEventArgs> syncProgressListener;

        /// <summary>
        /// The window service.
        /// </summary>
        private readonly IWindowService windowService;

        /// <summary>
        /// The composition container.
        /// </summary>
        private readonly CompositionContainer container;

        /// <summary>
        /// The messenger.
        /// </summary>
        private readonly IMessenger messenger;

        /// <summary>
        /// The synchronization manager.
        /// </summary>
        private readonly ISynchronizationManager syncManager;

        /// <summary>
        /// The PleaseWaitService reference.
        /// </summary>
        private readonly IPleaseWaitService pleaseWaitService;

        /// <summary>
        /// The synchronization process is executed by this task on a separate thread.
        /// </summary>
        private Task syncTask;

        /// <summary>
        /// Synchronization duration timer.
        /// </summary>
        private DispatcherTimer dispatcherTimer;

        /// <summary>
        /// A value indicating whether the synchronization will run 
        /// in the background after closing the window or not.
        /// </summary>
        private bool isReadyToRunInBackground;

        #endregion Attributes

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SynchronizationViewModel"/> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        /// <param name="container">The composition container.</param>
        /// <param name="pleaseWaitService">The please wait service.</param>
        /// <param name="syncService">The synchronization service.</param>
        /// <param name="messenger">The messenger.</param>
        [ImportingConstructor]
        public SynchronizationViewModel(
            IWindowService windowService,
            CompositionContainer container,
            IPleaseWaitService pleaseWaitService,
            ISyncService syncService,
            IMessenger messenger)
        {
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("container", container);
            Argument.IsNotNull("pleaseWaitService", pleaseWaitService);
            Argument.IsNotNull("messenger", messenger);

            this.windowService = windowService;
            this.container = container;
            this.pleaseWaitService = pleaseWaitService;
            this.messenger = messenger;
            this.syncManager = syncService.SyncManager;

            this.SynchronizationCommand = new DelegateCommand(this.SynchronizationAction);
            this.CheckAgainCommand = new DelegateCommand(this.CheckAgainAction);
            this.AbortCommand = new DelegateCommand(this.AbortAction);
            this.ViewConflictsCommand = new DelegateCommand(this.ViewConflictsAction);
            this.RunInBackgroundCommand = new DelegateCommand(
                this.RunInBackgroundAction,
                () => this.syncManager != null &&
                    this.syncManager.CurrentStatus.State == SyncState.Running);

            this.InitializeProperties();

            this.messenger.Register<SynchronizationStartedMessage>(s => this.InitializeSyncUi());
        }

        #endregion Constructor

        #region Commands

        /// <summary>
        /// Gets the synchronization command.
        /// </summary>
        public ICommand SynchronizationCommand { get; private set; }

        /// <summary>
        /// Gets the synch check again command.
        /// </summary>
        public ICommand CheckAgainCommand { get; private set; }

        /// <summary>
        /// Gets the synchronization abort command.
        /// </summary>
        public ICommand AbortCommand { get; private set; }

        /// <summary>
        /// Gets the view conflicts command.
        /// </summary>
        public ICommand ViewConflictsCommand { get; private set; }

        /// <summary>
        /// Gets the Run in background command.
        /// </summary>
        public ICommand RunInBackgroundCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets a value indicating whether the synchronization is visible or not.
        /// </summary>
        public VMProperty<bool> IsSynchronizationVisible { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the central server is offline or not.
        /// </summary>
        public VMProperty<bool> IsCentralOffline { get; private set; }

        /// <summary>
        /// Gets a value indicating whether to synchronize the static data and settings or not.
        /// </summary>
        public VMProperty<bool> SynchronizeStaticDataAndSettings { get; private set; }

        /// <summary>
        /// Gets a value indicating whether to synchronize the released projects or not.
        /// </summary>
        public VMProperty<bool> SynchronizeReleasedProjects { get; private set; }

        /// <summary>
        /// Gets a value indicating whether to synchronize my projects or not.
        /// </summary>
        public VMProperty<bool> SynchronizeMyProjects { get; private set; }

        /// <summary>
        /// Gets a value indicating whether to synchronize the masterdata or not.
        /// </summary>
        public VMProperty<bool> SynchronizeMasterData { get; private set; }

        /// <summary>
        /// Gets a value indicating whether my projects synchronization is available or not.
        /// </summary>
        public VMProperty<bool> SynchronizeMyProjectsAvailability { get; private set; }

        /// <summary>
        /// Gets a value indicating whether masterdata synchronization is available or not.
        /// </summary>
        public VMProperty<bool> SynchronizeMasterDataAvailability { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the synchronization is in progress or not.
        /// </summary>
        public VMProperty<bool> IsSyncInProgress { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the abort action is available.
        /// </summary>
        public VMProperty<bool> AbortAvailability { get; private set; }

        /// <summary>
        /// Gets the current task.
        /// </summary>
        public VMProperty<string> CurrentTask { get; private set; }

        /// <summary>
        /// Gets the current task status.
        /// </summary>
        public VMProperty<string> CurrentTaskStatus { get; private set; }

        /// <summary>
        /// Gets the duration of the synchronization.
        /// </summary>
        public VMProperty<string> SynchronizationDuration { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the synchronization duration is visible or not.
        /// </summary>
        public VMProperty<bool> SynchronizationDurationVisibility { get; private set; }

        /// <summary>
        /// Gets a value indicating whether during the synchronization occurred any conflict.
        /// </summary>
        public VMProperty<bool> IsAnyConflict { get; private set; }

        /// <summary>
        /// Gets the abort message.
        /// </summary>
        public VMProperty<string> AbortMessage { get; private set; }

        /// <summary>
        /// Gets the current progress.
        /// </summary>
        public VMProperty<double> CurrentProgress { get; private set; }

        /// <summary>
        /// Gets the current progress maximum value.
        /// </summary>
        public VMProperty<double> CurrentProgressMaximum { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the current progress is indeterminate or not.
        /// </summary>
        public VMProperty<bool> CurrentProgressIsIndeterminate { get; private set; }

        /// <summary>
        /// Gets the total progress.
        /// </summary>
        public VMProperty<double> TotalProgress { get; private set; }

        /// <summary>
        /// Gets the total progress maximum value.
        /// </summary>
        public VMProperty<double> TotalProgressMaximum { get; private set; }

        /// <summary>
        /// Gets the summary total items.
        /// </summary>
        public VMProperty<int> SummaryTotalItems { get; private set; }

        /// <summary>
        /// Gets the summary successes operations count.
        /// </summary>
        public VMProperty<int> SummarySuccessesCount { get; private set; }

        /// <summary>
        /// Gets the summary conflicts count.
        /// </summary>
        public VMProperty<int> SummaryConflictsCount { get; private set; }

        /// <summary>
        /// Gets a value indicating whether to display sync summary or not.
        /// </summary>
        public VMProperty<bool> DisplaySyncSummary { get; private set; }

        #endregion Properties

        #region Initialization

        /// <summary>
        /// Initializes some view-model properties.
        /// </summary>
        private void InitializeProperties()
        {
            this.AbortMessage.Value = LocalizedResources.General_Abort;
            this.SynchronizeMyProjectsAvailability.Value =
                SecurityManager.Instance.CurrentUserHasRight(Right.ProjectsAndCalculate);
            this.SynchronizeMasterDataAvailability.Value =
                SecurityManager.Instance.CurrentUserHasRight(Right.ProjectsAndCalculate)
                || SecurityManager.Instance.CurrentUserHasRight(Right.EditMasterData);

            this.RefreshUi(false, false);

            /*
             * Send the notification through the global messenger.
             */
            this.messenger.Send(new NotificationMessage(Notification.CancelSyncReminder));
        }

        /// <summary>
        /// Initializes some UI properties when the synchronization is running.
        /// </summary>
        private void InitializeSyncUi()
        {
            this.IsSyncInProgress.Value = true;
            this.AbortAvailability.Value = true;
            this.SynchronizationDurationVisibility.Value = true;

            /*
             * Start the synchronization timer 
             */
            this.dispatcherTimer = new DispatcherTimer();
            this.dispatcherTimer.Tick += this.DispatcherTimerTick;
            this.dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
            this.dispatcherTimer.Start();

            var syncStatus = this.syncManager.CurrentStatus;
            if (syncStatus.State == SyncState.Running)
            {
                this.UpdateSyncTaskStatus(
                    syncStatus.CurrentTask,
                    syncStatus.CurrentTaskStatus,
                    syncStatus.ProgressData);

                this.SynchronizeStaticDataAndSettings.Value = syncStatus.SyncStaticDataAndSettings;
                this.SynchronizeReleasedProjects.Value = syncStatus.SyncReleasedProjects;
                this.SynchronizeMasterData.Value = syncStatus.SyncMasterData;
                this.SynchronizeMyProjects.Value = syncStatus.SyncMyProjects;

                if (syncStatus.ProgressData != null)
                {
                    this.CurrentProgress.Value = syncStatus.ProgressData.PercentageCompleted;
                }

                if (syncStatus.CurrentTaskStatus == SynchronizationTaskStatus.ApplyingChanges
                    && syncStatus.ProgressData != null)
                {
                    var totalProgress = ((100 / this.syncManager.CurrentStatus.TasksCount) * this.syncManager.CurrentStatus.TasksCompletedCount)
                                + ((double)syncStatus.ProgressData.ProcessedItems * (100 / this.syncManager.CurrentStatus.TasksCount) / syncStatus.ProgressData.TotalItemsNumber);
                    this.TotalProgress.Value = totalProgress < 98 ? totalProgress : 98;
                }
                else
                {
                    this.TotalProgress.Value = (this.syncManager.CurrentStatus.TasksCompletedCount * 100) / this.syncManager.CurrentStatus.TasksCount;
                }
            }
            else
            {
                this.CurrentTask.Value = string.Empty;
                this.CurrentTaskStatus.Value = string.Empty;
                this.TotalProgress.Value = 0;
            }

            this.ShowSyncSummary(false, false);
        }

        #endregion Initialization

        #region Commands handlers

        /// <summary>
        /// Synchronizations the action.
        /// </summary>
        private void SynchronizationAction()
        {
            if (this.syncManager.CurrentStatus.State == SyncState.Running
                || this.syncManager.CurrentStatus.State == SyncState.Aborting
                || (!this.SynchronizeReleasedProjects.Value &&
                    !this.SynchronizeMasterData.Value &&
                    !this.SynchronizeMyProjects.Value &&
                    !this.SynchronizeStaticDataAndSettings.Value))
            {
                return;
            }

            /*
             * Start the synchronization process.
             */
            this.StartSynchronization(false);
        }

        /// <summary>
        /// Checks the again action.
        /// </summary>
        private void CheckAgainAction()
        {
            this.IsCentralOffline.Value = false;
            this.CheckCentralServerConnectionAsync();
        }

        /// <summary>
        /// Aborts the action.
        /// </summary>
        private void AbortAction()
        {
            var result = this.windowService.MessageDialogService.Show(
                LocalizedResources.Synchronization_AbortConfirmationMessage,
                MessageDialogType.YesNo);
            if (result == MessageDialogResult.Yes
                && this.syncManager.CurrentStatus.State == SyncState.Running)
            {
                this.AbortMessage.Value =
                    string.Format("{0}{1}", LocalizedResources.General_Aborting, "...");
                this.AbortAvailability.Value = false;
                this.syncManager.AbortSynchronization();
            }
        }

        /// <summary>
        /// Views the conflicts action.
        /// </summary>
        private void ViewConflictsAction()
        {
            var syncConflictsVM = this.container.GetExportedValue<SynchronizationConflictsViewModel>();
            this.windowService.ShowViewInDialog(syncConflictsVM);
        }

        /// <summary>
        /// Handle the synchronization RunInBackground command.
        /// </summary>
        private void RunInBackgroundAction()
        {
            this.isReadyToRunInBackground = true;
            this.windowService.CloseViewWindow(this);
        }

        #endregion Commands handlers

        #region Events handlers

        /// <summary>
        /// Called after the view has been loaded. Usually it performs some initialization 
        /// that needs the view to be loaded, like starting to load data from a database.
        /// <para />
        /// During unit tests this method must be manually called because there is no view to call it.
        /// </summary>
        public override void OnLoaded()
        {
            base.OnLoaded();
            this.AddSyncProgressListener();
            this.CheckCentralServerConnectionAsync();
        }

        /// <summary>
        /// Called after the view has been unloaded from the parent or closed.
        /// </summary>
        public override void OnUnloaded()
        {
            this.RemoveSyncProgressListener();
            base.OnUnloaded();
        }

        /// <summary>
        /// Called before unloading the view from its parent or closing it. 
        /// Returning false will cancel the view's unloading or closing.
        /// </summary>
        /// <returns>
        /// True if the unloading process should continue and false if it should be canceled.
        /// </returns>
        public override bool OnUnloading()
        {
            /*
             * Do nothing if is read-only.
             */
            if (this.IsReadOnly || this.IsInViewerMode)
            {
                return true;
            }

            if (this.syncManager.CurrentStatus.State == SyncState.Running
                && !this.isReadyToRunInBackground)
            {
                var result = this.windowService.MessageDialogService.Show(
                    LocalizedResources.Synchronization_RunInBackgroundConfirmationMessage,
                    MessageDialogType.YesNo);
                return result == MessageDialogResult.Yes;
            }

            /*
             * Do not unload the view-model as long as
             * the sync abort command is running.
             */
            return this.syncManager.CurrentStatus.State != SyncState.Aborting;
        }

        /// <summary>
        /// Closes this instance.
        /// <para />
        /// Called by the CloseCommand command.
        /// </summary>
        protected override void Close()
        {
            base.Close();
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Handles the Tick event of the dispatcherTimer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void DispatcherTimerTick(object sender, EventArgs e)
        {
            var currentSyncDuration = DateTime.Now - this.syncManager.CurrentStatus.StartTime;
            var currentSyncDurationDateTime = DateTime.MinValue.Add(currentSyncDuration);
            var durationString = new StringBuilder();
            durationString.AppendFormat(
                "({0} {1})",
                LocalizedResources.Synchronization_Duration,
                currentSyncDurationDateTime.ToString("HH:mm:ss"));
            this.SynchronizationDuration.Value = durationString.ToString();
        }

        #endregion Events handlers

        #region Do Synchronization

        /// <summary>
        /// Starts the synchronization process.
        /// </summary>
        /// <param name="resolveConflicts">A flag indicating whether to resolve the conflicts or not.</param>
        public void StartSynchronization(bool resolveConflicts)
        {
            /*
             * Check if the synchronization is running and if any option is selected.
             */
            if (this.syncManager.CurrentStatus.State == SyncState.Running ||
                this.syncManager.CurrentStatus.State == SyncState.Aborting)
            {
                return;
            }

            if (!this.SynchronizeMyProjects.Value &&
                !this.SynchronizeReleasedProjects.Value &&
                !this.SynchronizeMasterData.Value &&
                !this.SynchronizeStaticDataAndSettings.Value)
            {
                return;
            }

            /*
             * Determine what to synchronize.
             */
            var threadProcParams = new SyncThreadProcParams();
            threadProcParams.SyncReleasedProjects = this.SynchronizeReleasedProjects.Value;
            threadProcParams.SyncMyProjects = this.SynchronizeMyProjects.Value;
            threadProcParams.SyncMasterData = this.SynchronizeMasterData.Value;
            threadProcParams.SyncStaticDataAndSettings = this.SynchronizeStaticDataAndSettings.Value;
            threadProcParams.ResolveConflicts = resolveConflicts;

            /*
             * Signal that the synchronization has started.
             */
            this.messenger.Send(new SynchronizationStartedMessage());

            /*
             * Start the synchronization.
             */
            this.syncTask = Task.Factory.StartNew(() =>
            {
                var userGuid = SecurityManager.Instance.CurrentUser.Guid;
                this.syncManager.Synchronize(
                    threadProcParams.SyncStaticDataAndSettings,
                    threadProcParams.SyncReleasedProjects,
                    threadProcParams.SyncMasterData,
                    threadProcParams.SyncMyProjects,
                    userGuid,
                    threadProcParams.ResolveConflicts);
            });
        }

        /// <summary>
        /// Handles the synchronize completion.
        /// </summary>
        /// <param name="error">The error.</param>
        private void HandleSyncCompletion(Exception error)
        {
            if (dispatcherTimer != null)
            {
                dispatcherTimer.Stop();
            }

            /*
             * Update the synchronization window UI
             */
            this.IsSyncInProgress.Value = false;
            this.AbortAvailability.Value = false;

            this.AbortMessage.Value = LocalizedResources.General_Abort;

            if (error != null)
            {
                Log.ErrorException("An error occurred during synchronization.", error);

                var tmpEx = error;
                var showDialog = true;
                while (tmpEx != null)
                {
                    var exception = tmpEx as SynchronizationException;
                    if (exception != null)
                    {
                        if (exception.ErrorCode == DatabaseErrorCode.NoConnection)
                        {
                            showDialog = false;
                            AddRemoveOfflineGrid(true);
                        }
                        else if (exception.ErrorCode == SyncErrorCodes.MinSqlServerVersionError)
                        {
                            showDialog = true;
                        }
                    }

                    tmpEx = tmpEx.InnerException;
                }

                if (showDialog)
                {
                    this.windowService.MessageDialogService.Show(error);
                }
            }
        }

        /// <summary>
        /// Handles the synchronization progress.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="eventArgs">The <see><cref>ZPKTool.Business.Managers.SynchronizationProgressEventArgs</cref></see>
        /// instance containing the event data.</param>
        private void SynchronizationProgressHandler(object sender, SynchronizationProgressEventArgs eventArgs)
        {
            this.UpdateSyncTaskStatus(eventArgs.CurrentTask, eventArgs.CurrentTaskStatus, eventArgs.Data);
        }

        /// <summary>
        /// Updates the synchronization current task and total progress.
        /// </summary>
        /// <param name="currentTask">The current task.</param>
        /// <param name="currentTaskStatus">The current task status.</param>
        /// <param name="progressData">The progress data.</param>
        private void UpdateSyncTaskStatus(
            SynchronizationTask currentTask,
            SynchronizationTaskStatus currentTaskStatus,
            SynchronizationProgressData progressData)
        {
            this.CurrentTask.Value = currentTask.ToString();

            /*
             * The sync task has started.
             */
            if (currentTaskStatus == SynchronizationTaskStatus.Initializing)
            {
                this.CurrentProgressIsIndeterminate.Value = true;
                this.CurrentProgress.Value = 0;

                switch (currentTask)
                {
                    case SynchronizationTask.MasterData:
                        this.CurrentTaskStatus.Value = ": " + LocalizedResources.Synchronization_DetectingChanges;
                        this.CurrentTask.Value = LocalizedResources.General_IsMasterData;
                        break;

                    case SynchronizationTask.MyProjects:
                        this.CurrentTaskStatus.Value = ": " + LocalizedResources.Synchronization_DetectingChanges;
                        this.CurrentTask.Value = LocalizedResources.General_MyProjects;
                        break;

                    case SynchronizationTask.ReleasedProjects:
                        this.CurrentTaskStatus.Value = ": " + LocalizedResources.Synchronization_DetectingChanges;
                        this.CurrentTask.Value = LocalizedResources.General_ReleasedProjects;
                        break;

                    case SynchronizationTask.StaticDataAndSettings:
                        this.CurrentTaskStatus.Value = ": " + LocalizedResources.Synchronization_DetectingChanges;
                        this.CurrentTask.Value = LocalizedResources.Synchronization_SDAndSettings;
                        break;

                    case SynchronizationTask.ProcessConflicts:
                        this.CurrentTaskStatus.Value = ": " + LocalizedResources.Synchronization_DetectingChanges;
                        this.CurrentTask.Value = LocalizedResources.Synchronization_ProcessingConflicts;
                        break;

                    case SynchronizationTask.Initialising:
                        this.CurrentTask.Value = LocalizedResources.Synchronization_Initializing;
                        break;
                }
            }
            else if (currentTaskStatus == SynchronizationTaskStatus.SelectingChanges)
            {
                this.CurrentTaskStatus.Value = ": " + LocalizedResources.Synchronization_DetectingChanges;
                this.CurrentProgressIsIndeterminate.Value = true;
                this.CurrentProgress.Value = 0;
            }
            else if (currentTaskStatus == SynchronizationTaskStatus.ApplyingChanges && progressData != null)
            {
                /*
                 * A sync task has advanced
                 */
                this.CurrentTaskStatus.Value = ": " + LocalizedResources.Synchronization_Synchronizing;
                this.CurrentProgressIsIndeterminate.Value = false;

                if (this.CurrentProgress.Value < 96)
                {
                    this.CurrentProgress.Value = progressData.PercentageCompleted;
                }

                if (this.TotalProgress.Value < 98)
                {
                    if (progressData.TotalItemsNumber > 0)
                    {
                        this.TotalProgress.Value = ((100 / this.syncManager.CurrentStatus.TasksCount) * this.syncManager.CurrentStatus.TasksCompletedCount)
                            + ((double)progressData.ProcessedItems * (100 / this.syncManager.CurrentStatus.TasksCount) / (double)progressData.TotalItemsNumber);
                    }
                }
            }
            else if (currentTaskStatus == SynchronizationTaskStatus.Aborted)
            {
                this.CurrentTaskStatus.Value = ": " + LocalizedResources.Synchronization_Task_Status_Aborted;
                this.CurrentProgressIsIndeterminate.Value = false;

                this.CurrentProgress.Value = this.CurrentProgressMaximum.Value;
                this.TotalProgress.Value = this.TotalProgressMaximum.Value;
            }

            /*
             * Count the total number of processed items across all tasks 
             * (event data contains the total items only for a task)
             */
            if (currentTaskStatus == SynchronizationTaskStatus.Finished)
            {
                if (progressData != null)
                {
                    this.CurrentProgressIsIndeterminate.Value = false;
                    this.CurrentProgress.Value = this.CurrentProgressMaximum.Value;

                    this.TotalProgress.Value = (this.syncManager.CurrentStatus.TasksCompletedCount * 100) / this.syncManager.CurrentStatus.TasksCount;
                }
            }

            /*
             * The synchronization process has completed (all tasks have finished)
             */
            if (currentTask == SynchronizationTask.CompletedAllTasks)
            {
                this.CurrentTaskStatus.Value = string.Empty;
                this.CurrentProgress.Value = this.CurrentProgressMaximum.Value;
                this.CurrentProgressIsIndeterminate.Value = false;

                this.TotalProgress.Value = TotalProgressMaximum.Value;

                if (this.syncManager.CurrentStatus.State == SyncState.Aborted)
                {
                    this.CurrentTask.Value = LocalizedResources.General_Aborted;
                }

                /*
                 * Handled the synchronization completion and Refresh de SyncView UI.
                 */
                this.HandleSyncCompletion(this.syncManager.CurrentStatus.Error);
                this.RefreshUi(true, this.syncManager.CurrentStatus.State == SyncState.Aborted);
            }
        }

        /// <summary>
        /// Adds a listener to handle the synchronization progress.
        /// </summary>
        private void AddSyncProgressListener()
        {
            /*
             * Removes the listener that handles the synchronization progress, 
             * if it was already added.
             */
            if (syncProgressListener != null)
            {
                SynchronizationProgressEventManager.RemoveListener(this.syncManager, syncProgressListener);
            }

            syncProgressListener =
                new WeakEventListener<SynchronizationProgressEventArgs>(SynchronizationProgressHandler);
            SynchronizationProgressEventManager.AddListener(this.syncManager, syncProgressListener);
        }

        /// <summary>
        /// Removes the listener that handles the synchronization progress.
        /// </summary>
        private void RemoveSyncProgressListener()
        {
            if (syncProgressListener != null)
            {
                SynchronizationProgressEventManager.RemoveListener(this.syncManager, syncProgressListener);
            }
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Checks the central server connection async.
        /// </summary>
        private void CheckCentralServerConnectionAsync()
        {
            this.IsSyncInProgress.Value = true;

            Action<PleaseWaitService.WorkParams> work = (workParams) =>
            {
                var onlineChecker = this.container.GetExportedValue<IOnlineCheckService>();
                workParams.Result = onlineChecker.CheckNow();
            };

            Action<PleaseWaitService.WorkParams> workCompleted = (workParams) =>
            {
                if (workParams.Result != null)
                {
                    var online = (bool)workParams.Result;
                    this.AddRemoveOfflineGrid(!online);

                    if (this.syncManager.CurrentStatus.State == SyncState.Running)
                    {
                        this.InitializeSyncUi();
                    }
                }
            };

            this.pleaseWaitService.Show(
                LocalizedResources.SynchronizationStatusCheck,
                1000,
                work,
                workCompleted);
        }

        /// <summary>
        /// Updates the UI after the synchronization has finished.
        /// </summary>
        /// <param name="showSyncSummary">if set to <c>true</c> show the synchronization summary, 
        /// otherwise hide it.</param>
        /// <param name="aborted">if set to <c>true</c> the sync process was aborted.</param>
        private void RefreshUi(bool showSyncSummary, bool aborted)
        {
            if (showSyncSummary)
            {
                this.CurrentTask.Value = LocalizedResources.Synchronization_Finished;
            }
            else
            {
                this.CurrentTask.Value = LocalizedResources.Synchronization_NotStarted;
            }

            this.CurrentProgressMaximum.Value = 100;
            this.TotalProgressMaximum.Value = 100;
            this.ShowSyncSummary(showSyncSummary, aborted);
        }

        /// <summary>
        /// Display the synchronization summary.
        /// </summary>
        /// <param name="show">if set to <c>true</c> display the summary; otherwise hide it..</param>
        /// <param name="aborted">if set to <c>true</c> the sync process was aborted.</param>
        private void ShowSyncSummary(bool show, bool aborted)
        {
            if (show)
            {
                this.SummaryTotalItems.Value = this.syncManager.CurrentStatus.TotalItemsCount;
                if (!aborted)
                {
                    this.SummarySuccessesCount.Value = this.syncManager.CurrentStatus.TotalItemsCount - this.syncManager.CurrentStatus.TotalConflictsCount;
                }
                else
                {
                    this.SummarySuccessesCount.Value = this.syncManager.CurrentStatus.ProcessedItemsCount;
                    this.CurrentTask.Value = LocalizedResources.General_Aborted;
                }

                this.SummaryConflictsCount.Value = this.syncManager.CurrentStatus.TotalConflictsCount;
                this.IsAnyConflict.Value = this.syncManager.CurrentStatus.TotalConflictsCount > 0;
                this.DisplaySyncSummary.Value = true;
            }
            else
            {
                this.DisplaySyncSummary.Value = false;
            }
        }

        /// <summary>
        /// Adds or removes the offline grid.
        /// </summary>
        /// <param name="addorder">if set to <c>true</c> [addorder].</param>
        private void AddRemoveOfflineGrid(bool addorder)
        {
            if (addorder)
            {
                this.IsSyncInProgress.Value = true;
                this.AbortAvailability.Value = true;
                this.IsCentralOffline.Value = true;
            }
            else
            {
                this.IsSyncInProgress.Value = false;
                this.AbortAvailability.Value = false;
                this.IsCentralOffline.Value = false;
            }
        }

        #endregion Private methods

        #region Inner Classes

        /// <summary>
        /// Contains the values that must be passed as parameters to the synchronization method.
        /// </summary>
        private struct SyncThreadProcParams
        {
            /// <summary>
            /// This value tells whether to synchronize static data and settings.
            /// </summary>
            public bool SyncStaticDataAndSettings;

            /// <summary>
            /// This value tells whether to synchronize the released projects.
            /// </summary>
            public bool SyncReleasedProjects;

            /// <summary>
            /// This value tells whether to synchronize master data.
            /// </summary>
            public bool SyncMasterData;

            /// <summary>
            /// This value tells whether to synchronize my projects.
            /// </summary>
            public bool SyncMyProjects;

            /// <summary>
            /// Resolve conflicts.
            /// </summary>
            public bool ResolveConflicts;
        }

        #endregion Inner Classes
    }
}
