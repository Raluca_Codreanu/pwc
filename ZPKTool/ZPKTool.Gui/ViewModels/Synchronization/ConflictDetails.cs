﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// A class containing properties of sync conflict: field name and local vs central values (to be displayed side by side).
    /// </summary>
    public class ConflictDetails
    {
        /// <summary>
        /// Gets or sets the name of the column.
        /// </summary>
        /// <value>
        /// The name of the column.
        /// </value>
        public string ColumnName { get; set; }

        /// <summary>
        /// Gets or sets the local column value.
        /// </summary>
        /// <value>
        /// The local column value.
        /// </value>
        public string LocalColumnValue { get; set; }

        /// <summary>
        /// Gets or sets the central column value.
        /// </summary>
        /// <value>
        /// The central column value.
        /// </value>
        public string CentralColumnValue { get; set; }
    }
}
