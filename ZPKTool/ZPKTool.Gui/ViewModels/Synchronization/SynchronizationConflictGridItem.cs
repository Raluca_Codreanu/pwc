﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using ZPKTool.Common;
using ZPKTool.Synchronization;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Wraps a synchronization conflict object with the purpose of displaying it in a DataGrid.
    /// </summary>
    public class SynchronizationConflictGridItem : ObservableObject
    {
        /// <summary>
        /// The conflict wrapped.
        /// </summary>
        private SynchronizationConflict conflict;

        /// <summary>
        /// Initializes a new instance of the <see cref="SynchronizationConflictGridItem"/> class.
        /// </summary>
        /// <param name="conflict">The conflict.</param>
        public SynchronizationConflictGridItem(SynchronizationConflict conflict)
        {
            this.conflict = conflict;
        }

        /// <summary>
        /// Gets the wrapped conflict.
        /// </summary>
        public SynchronizationConflict Conflict
        {
            get { return conflict; }
        }

        /// <summary>
        /// Gets or sets the localized name of the synchronization task during which the conflict appeared.
        /// </summary>
        public string SyncTask { get; set; }

        /// <summary>
        /// Gets or sets what to display in the Type column.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets what to display in the Name column.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the resolution for the conflict.
        /// </summary>
        public SynchronizationConflictResolution Resolution
        {
            get
            {
                return conflict.Resolution;
            }

            set
            {
                conflict.Resolution = value;
                this.OnPropertyChanged(() => this.Resolution);
            }
        }
    }
}
