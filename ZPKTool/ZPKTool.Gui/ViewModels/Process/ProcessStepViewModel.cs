﻿namespace ZPKTool.Gui.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.ComponentModel.Composition;
    using System.ComponentModel.Composition.Hosting;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using ZPKTool.Business;
    using ZPKTool.Business.Export;
    using ZPKTool.Calculations.CostCalculation;
    using ZPKTool.Common;
    using ZPKTool.Data;
    using ZPKTool.DataAccess;
    using ZPKTool.Gui.Managers;
    using ZPKTool.Gui.Notifications;
    using ZPKTool.Gui.Resources;
    using ZPKTool.Gui.Services;
    using ZPKTool.Gui.Utils;
    using ZPKTool.Gui.ViewModels.ProcessInfrastructure;
    using ZPKTool.MvvmCore;
    using ZPKTool.MvvmCore.Commands;
    using ZPKTool.MvvmCore.Services;

    /// <summary>
    /// The view-model for editing and viewing a Process step.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ProcessStepViewModel : ViewModel<ProcessStep, IDataSourceManager>
    {
        #region Attributes

        /// <summary>
        /// The composition container reference which is used to get other view-models.
        /// </summary>
        private CompositionContainer compositionContainer;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The messenger.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The model browser helper service.
        /// </summary>
        private IModelBrowserHelperService modelBrowserHelperService;

        /// <summary>
        /// The PleaseWaitService reference.
        /// </summary>
        private IPleaseWaitService pleaseWaitService;

        /// <summary>
        /// The collection of machines which are used in the binding on the data grid
        /// </summary>
        private DispatchedObservableCollection<MachineDataGridItem> stepMachines;

        /// <summary>
        /// The collection of DieDataGridItems which are used in the binding on the data grid.
        /// </summary>
        private DispatchedObservableCollection<DieDataGridItem> stepDieItems;

        /// <summary>
        /// The collection of Consumables which are used in the binding on the data grid.
        /// </summary>
        private DispatchedObservableCollection<ConsumableDataGridItem> stepConsumables;

        /// <summary>
        /// The collection of Commodities which are used in the binding on the data grid.
        /// </summary>
        private DispatchedObservableCollection<CommodityDataGridItem> stepCommodities;

        /// <summary>
        /// The collection of PartsGridItem which are used in the binding on the data grid.
        /// </summary>
        private DispatchedObservableCollection<PartsGridItem> stepParts = new DispatchedObservableCollection<PartsGridItem>();

        /// <summary>
        /// The cost calculation version of the process' parent entity, which is used internally for cost calculations.
        /// </summary>
        private string costCalculationVersion;

        /// <summary>
        /// A bool value indicating whether the model is loading or not.
        /// </summary>
        private bool isModelLoading = false;

        /// <summary>
        /// The clone manager.
        /// </summary>
        private CloneManager cloneManager = new CloneManager();

        /// <summary>
        /// The cycle time calculator image source.
        /// </summary>
        private ImageSource calculatorIcon;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        /// <summary>
        /// The cache of costs for the sub-parts and sub-assemblies displayed in the step's Parts List.
        /// </summary>
        private Dictionary<Guid, CalculationResult> subpartsCostCache;

        /// <summary>
        /// Weak event listener for the PropertyChanged notification
        /// </summary>
        private WeakEventListener<PropertyChangedEventArgs> propertyChangedListener;

        /// <summary>
        /// A value indicating whether the capacity utilization value can be shown
        /// </summary>
        private bool canShowCapacityUtilization;

        /// <summary>
        /// The UnitsAdapter handler, used to perform certain operations when some UnitsAdapter properties are updated.
        /// </summary>
        private UnitsAdapterUpdateHandler unitsAdapterHandler;

        /// <summary>
        /// A list containing the id of the entities that are marked for permanent deletion
        /// </summary>
        private ICollection<Guid> permanentlyDeletedEntityIds = new List<Guid>();

        /// <summary>
        /// A list containing the objects that have been moved (after cut - paste operation).
        /// </summary>
        private IList<object> movedItems = new List<object>();

        #endregion Attributes

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessStepViewModel" /> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        /// <param name="messenger">The messenger.</param>
        /// <param name="container">The composition container.</param>
        /// <param name="pleaseWaitService">The please wait service.</param>
        /// <param name="modelBrowserHelperService">The model browser helper service.</param>
        /// <param name="costRecalculationCloneManager">The cost recalculation clone manager.</param>
        /// <param name="mediaViewModel">The media view model.</param>
        /// <param name="typeSelectorViewModel">The ClassificationSelector view model.</param>
        /// <param name="unitsService">The units service.</param>
        [ImportingConstructor]
        public ProcessStepViewModel(
            IWindowService windowService,
            IMessenger messenger,
            CompositionContainer container,
            IPleaseWaitService pleaseWaitService,
            IModelBrowserHelperService modelBrowserHelperService,
            ICostRecalculationCloneManager costRecalculationCloneManager,
            MediaViewModel mediaViewModel,
            ClassificationSelectorViewModel typeSelectorViewModel,
            IUnitsService unitsService)
        {
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("container", container);
            Argument.IsNotNull("pleaseWaitService", pleaseWaitService);
            Argument.IsNotNull("modelBrowserHelperService", modelBrowserHelperService);
            Argument.IsNotNull("mediaViewModel", mediaViewModel);
            Argument.IsNotNull("typeSelectorViewModel", typeSelectorViewModel);
            Argument.IsNotNull("unitsService", unitsService);
            Argument.IsNotNull("costRecalculationCloneManager", costRecalculationCloneManager);

            this.windowService = windowService;
            this.messenger = messenger;
            this.compositionContainer = container;
            this.pleaseWaitService = pleaseWaitService;
            this.modelBrowserHelperService = modelBrowserHelperService;
            this.TypeSelectorViewModel = typeSelectorViewModel;
            this.TypeSelectorViewModel.ClassificationType = typeof(ProcessStepsClassification);
            this.MediaViewModel = mediaViewModel;
            this.MediaViewModel.Mode = MediaControlMode.SingleImage;
            this.MediaViewModel.IsChild = true;
            this.unitsService = unitsService;
            this.CloneManager = costRecalculationCloneManager;

            this.InitializeProperties();
            this.InitializeCommands();
            this.InitializeUndoManager();
            this.InitializeCollections();
            this.CanShowCapacityUtilization = UserSettingsManager.Instance.DisplayCapacityUtilization;

            this.messenger.Register<ProcessStepChangedMessage>(this.HandleProcessStepChange, GlobalMessengerTokens.MainViewTargetToken);

            this.propertyChangedListener = new WeakEventListener<PropertyChangedEventArgs>(Settings_PropertyChanged);
            PropertyChangedEventManager.AddListener(UserSettingsManager.Instance, propertyChangedListener, ReflectionUtils.GetPropertyName(() => UserSettingsManager.Instance.DisplayCapacityUtilization));
        }

        #endregion Constructor

        #region Commands

        /// <summary>
        /// Gets the command for the add button
        /// </summary>
        public ICommand AddCommand { get; private set; }

        /// <summary>
        /// Gets the command for the edit button
        /// </summary>
        public ICommand EditCommand { get; private set; }

        /// <summary>
        /// Gets the command for the delete button
        /// </summary>
        public ICommand DeleteCommand { get; private set; }

        /// <summary>
        /// Gets the Cut command.
        /// </summary>
        public ICommand CutCommand { get; private set; }

        /// <summary>
        /// Gets the Copy command.
        /// </summary>
        public ICommand CopyCommand { get; private set; }

        /// <summary>
        /// Gets the Paste command.
        /// </summary>
        public ICommand PasteCommand { get; private set; }

        /// <summary>
        /// Gets the Drop command.
        /// </summary>
        public ICommand DropCommand { get; private set; }

        /// <summary>
        /// Gets the Import command.
        /// </summary>
        public ICommand ImportCommand { get; private set; }

        /// <summary>
        /// Gets the Export command.
        /// </summary>
        public ICommand ExportCommand { get; private set; }

        /// <summary>
        /// Gets the command for the mouse double click event
        /// </summary>
        public ICommand MouseDoubleClickCommand { get; private set; }

        /// <summary>
        /// Gets the command that open the CyCleTimeCalculator.
        /// </summary>
        public ICommand HandleCyCleTimeCalculatorCommand { get; private set; }

        /// <summary>
        /// Gets or sets the command that cancels all changes of the nested view models.
        /// This command aggregates the Cancel commands of the nested view models.
        /// </summary>
        private CompositeCommand CancelNestedViewModels { get; set; }

        #endregion Commands

        #region Model related properties

        /// <summary>
        /// Gets the name of the process step.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Name", ErrorMessageResourceType = typeof(LocalizedResources))]
        [ExposesModelProperty("Name")]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public DataProperty<string> Name { get; private set; }

        /// <summary>
        /// Gets the process step accuracy.
        /// </summary>
        [ExposesModelProperty("Accuracy", AffectsCost = true)]
        [UndoableProperty]
        public DataProperty<ProcessCalculationAccuracy?> Accuracy { get; private set; }

        /// <summary>
        /// Gets the price of the process step.
        /// </summary>
        [Required(ErrorMessageResourceName = "Part_EstimatedCostMissing", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Price", AffectsCost = true)]
        public DataProperty<decimal?> Price { get; private set; }

        /// <summary>
        /// Gets the process time of the process step.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("ProcessTime", AffectsCost = true)]
        public DataProperty<decimal?> ProcessTime { get; private set; }

        /// <summary>
        /// Gets the process time unit of the process step.
        /// </summary>
        [ExposesModelProperty("ProcessTimeUnit")]
        [UndoableProperty]
        public DataProperty<MeasurementUnit> ProcessTimeUnit { get; private set; }

        /// <summary>
        /// Gets the cycle time of the process step.
        /// </summary>
        [ExposesModelProperty("CycleTime", AffectsCost = true)]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public DataProperty<decimal?> CycleTime { get; private set; }

        /// <summary>
        /// Gets the cycle time unit of the process step.
        /// </summary>
        [ExposesModelProperty("CycleTimeUnit")]
        [UndoableProperty]
        public DataProperty<MeasurementUnit> CycleTimeUnit { get; private set; }

        /// <summary>
        /// Gets the parts/cycle of the process step.
        /// </summary>
        [ExposesModelProperty("PartsPerCycle", AffectsCost = true)]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public DataProperty<int?> PartsPerCycle { get; private set; }

        /// <summary>
        /// Gets the scrap amount of the process step.
        /// </summary>
        [ExposesModelProperty("ScrapAmount", AffectsCost = true)]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public DataProperty<decimal?> ScrapAmount { get; private set; }

        /// <summary>
        /// Gets the price of the process step.
        /// </summary>
        [ExposesModelProperty("SetupsPerBatch", AffectsCost = true)]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public DataProperty<int?> SetupsPerBatch { get; private set; }

        /// <summary>
        /// Gets the max downtime of the process step.
        /// </summary>
        [ExposesModelProperty("MaxDownTime", AffectsCost = true)]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public DataProperty<decimal?> MaxDownTime { get; private set; }

        /// <summary>
        /// Gets the max downtime unit of the process step.
        /// </summary>
        [ExposesModelProperty("MaxDownTimeUnit")]
        [UndoableProperty]
        public DataProperty<MeasurementUnit> MaxDownTimeUnit { get; private set; }

        /// <summary>
        /// Gets the setup time of the process step.
        /// </summary>
        [ExposesModelProperty("SetupTime", AffectsCost = true)]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public DataProperty<decimal?> SetupTime { get; private set; }

        /// <summary>
        /// Gets the setup time unit of the process step.
        /// </summary>
        [ExposesModelProperty("SetupTimeUnit")]
        [UndoableProperty]
        public DataProperty<MeasurementUnit> SetupTimeUnit { get; private set; }

        /// <summary>
        /// Gets the batch size of the process step.
        /// </summary>
        [ExposesModelProperty("BatchSize", AffectsCost = true)]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public DataProperty<int?> BatchSize { get; private set; }

        /// <summary>
        /// Gets the manufacturing overhead of the process step.
        /// </summary>
        [ExposesModelProperty("ManufacturingOverhead", AffectsCost = true)]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public DataProperty<decimal?> ManufacturingOverhead { get; private set; }

        /// <summary>
        /// Gets the description of the process step.
        /// </summary>
        [ExposesModelProperty("Description")]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public DataProperty<string> Description { get; private set; }

        /// <summary>
        /// Gets the shifts per week of the process step.
        /// </summary>
        [ExposesModelProperty("ShiftsPerWeek", AffectsCost = true)]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public DataProperty<decimal?> ShiftsPerWeek { get; private set; }

        /// <summary>
        /// Gets the hours per shift of the process step.
        /// </summary>
        [ExposesModelProperty("HoursPerShift", AffectsCost = true)]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public DataProperty<decimal?> HoursPerShift { get; private set; }

        /// <summary>
        /// Gets the production days per week of the process step.
        /// </summary>
        [ExposesModelProperty("ProductionDaysPerWeek", AffectsCost = true)]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public DataProperty<decimal?> ProductionDaysPerWeek { get; private set; }

        /// <summary>
        /// Gets the production weeks per year of the process step.
        /// </summary>
        [ExposesModelProperty("ProductionWeeksPerYear", AffectsCost = true)]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public DataProperty<decimal?> ProductionWeeksPerYear { get; private set; }

        /// <summary>
        /// Gets the exceed shift cost of the process step.
        /// </summary>
        [ExposesModelProperty("ExceedShiftCost")]
        [UndoableProperty]
        public DataProperty<bool> ExceedShiftCost { get; private set; }

        /// <summary>
        /// Gets the extra shifts number of the process step.
        /// </summary>
        [ExposesModelProperty("ExtraShiftsNumber", AffectsCost = true)]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public DataProperty<decimal?> ExtraShiftsNumber { get; private set; }

        /// <summary>
        /// Gets the shift cost exceed ratio of the process step.
        /// </summary>
        [ExposesModelProperty("ShiftCostExceedRatio", AffectsCost = true)]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public DataProperty<decimal?> ShiftCostExceedRatio { get; private set; }

        /// <summary>
        /// Gets the production unskilled labour of the process step.
        /// </summary>
        [ExposesModelProperty("ProductionUnskilledLabour", AffectsCost = true)]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public DataProperty<decimal?> ProductionUnskilledLabour { get; private set; }

        /// <summary>
        /// Gets the production skilled labour of the process step.
        /// </summary>
        [ExposesModelProperty("ProductionSkilledLabour", AffectsCost = true)]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public DataProperty<decimal?> ProductionSkilledLabour { get; private set; }

        /// <summary>
        /// Gets the production foreman of the process step.
        /// </summary>
        [ExposesModelProperty("ProductionForeman", AffectsCost = true)]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public DataProperty<decimal?> ProductionForeman { get; private set; }

        /// <summary>
        /// Gets the production technicians of the process step.
        /// </summary>
        [ExposesModelProperty("ProductionTechnicians", AffectsCost = true)]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public DataProperty<decimal?> ProductionTechnicians { get; private set; }

        /// <summary>
        /// Gets the production engineers of the process step.
        /// </summary>
        [ExposesModelProperty("ProductionEngineers", AffectsCost = true)]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public DataProperty<decimal?> ProductionEngineers { get; private set; }

        /// <summary>
        /// Gets the setup unskilled labour of the process step.
        /// </summary>
        [ExposesModelProperty("SetupUnskilledLabour", AffectsCost = true)]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public DataProperty<decimal?> SetupUnskilledLabour { get; private set; }

        /// <summary>
        /// Gets the setup skilled labour of the process step.
        /// </summary>
        [ExposesModelProperty("SetupSkilledLabour", AffectsCost = true)]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public DataProperty<decimal?> SetupSkilledLabour { get; private set; }

        /// <summary>
        /// Gets the setup foreman of the process step.
        /// </summary>
        [ExposesModelProperty("SetupForeman", AffectsCost = true)]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public DataProperty<decimal?> SetupForeman { get; private set; }

        /// <summary>
        /// Gets the setup technicians of the process step.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("SetupTechnicians", AffectsCost = true)]
        public DataProperty<decimal?> SetupTechnicians { get; private set; }

        /// <summary>
        /// Gets the setup engineers of the process step.
        /// </summary>
        [ExposesModelProperty("SetupEngineers", AffectsCost = true)]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public DataProperty<decimal?> SetupEngineers { get; private set; }

        /// <summary>
        /// Gets the IsExternal state of the process step.
        /// </summary>
        [ExposesModelProperty("IsExternal")]
        [UndoableProperty]
        public DataProperty<bool?> IsExternal { get; private set; }

        /// <summary>
        /// Gets the transport cost of the process step.
        /// </summary>
        [ExposesModelProperty("TransportCost", AffectsCost = true)]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public DataProperty<decimal?> TransportCost { get; private set; }

        /// <summary>
        /// Gets the transport cost type of the process step.
        /// </summary>
        [ExposesModelProperty("TransportCostType", AffectsCost = true)]
        [UndoableProperty]
        public DataProperty<ProcessTransportCostType?> TransportCostType { get; private set; }

        /// <summary>
        /// Gets the transport cost quantity of the process step.
        /// </summary>
        [Required(ErrorMessageResourceName = "ProcessStep_TransportCostQtyMissing", ErrorMessageResourceType = typeof(LocalizedResources))]
        [ExposesModelProperty("TransportCostQty", AffectsCost = true)]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public DataProperty<int?> TransportCostQty { get; private set; }

        #endregion Model related properties

        #region Other Properties

        /// <summary>
        /// Gets the media view model.
        /// </summary>
        public MediaViewModel MediaViewModel { get; private set; }

        /// <summary>
        /// Gets the view-model for the view that allows to select the step's type classification.
        /// </summary>
        public ClassificationSelectorViewModel TypeSelectorViewModel { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the accuracy is calculated or estimated.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public VMProperty<bool> IsAccuracyCalculated { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the accuracy is calculated or estimated.
        /// </summary>
        [UndoableProperty(IsUnselectable = true, GroupConsecutiveChanges = true)]
        public VMProperty<bool> IsExceededShiftCost { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the transport cost is enabled or not.
        /// </summary>
        public VMProperty<bool> IsTransportCostEnabled { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the transport cost type selected is per qty.
        /// </summary>
        public VMProperty<bool> IsTransportPerQtySelected { get; private set; }

        /// <summary>
        /// Gets a value indicating whether a process step process parent is Assembly or not (Part).
        /// </summary>
        public VMProperty<bool> IsAssemblyProcessStep { get; private set; }

        /// <summary>
        /// Gets or sets the collection of MachineDataGridItem displayed in the machines data grid.
        /// </summary>
        [UndoableProperty]
        public DispatchedObservableCollection<MachineDataGridItem> StepMachines
        {
            get
            {
                return this.stepMachines;
            }

            set
            {
                if (this.stepMachines != value)
                {
                    this.stepMachines = value;
                    OnPropertyChanged(() => this.StepMachines);
                }
            }
        }

        /// <summary>
        /// Gets the selected machine data grid item in the entity data grid.
        /// </summary>
        public VMProperty<MachineDataGridItem> SelectedMachineItem { get; private set; }

        /// <summary>
        /// Gets or sets the collection of DieDataGridItem displayed in the Dies data grid.
        /// </summary>
        [UndoableProperty]
        public DispatchedObservableCollection<DieDataGridItem> StepDieItems
        {
            get
            {
                return this.stepDieItems;
            }

            set
            {
                if (this.stepDieItems != value)
                {
                    this.stepDieItems = value;
                    OnPropertyChanged(() => this.StepDieItems);
                }
            }
        }

        /// <summary>
        /// Gets the selected die in the step dies data grid.
        /// </summary>
        public VMProperty<DieDataGridItem> SelectedDieItem { get; private set; }

        /// <summary>
        /// Gets or sets the collection of consumables displayed in the consumables data grid.
        /// </summary>
        [UndoableProperty]
        public DispatchedObservableCollection<ConsumableDataGridItem> StepConsumables
        {
            get
            {
                return this.stepConsumables;
            }

            set
            {
                if (this.stepConsumables != value)
                {
                    this.stepConsumables = value;
                    OnPropertyChanged(() => this.StepConsumables);
                }
            }
        }

        /// <summary>
        /// Gets the selected consumable in the step consumables data grid.
        /// </summary>
        public VMProperty<ConsumableDataGridItem> SelectedConsumable { get; private set; }

        /// <summary>
        /// Gets or sets the collection of commodities displayed in the commodities data grid.
        /// </summary>
        [UndoableProperty]
        public DispatchedObservableCollection<CommodityDataGridItem> StepCommodities
        {
            get
            {
                return this.stepCommodities;
            }

            set
            {
                if (this.stepCommodities != value)
                {
                    this.stepCommodities = value;
                    OnPropertyChanged(() => this.StepCommodities);
                }
            }
        }

        /// <summary>
        /// Gets the selected commodity in the step commodities data grid.
        /// </summary>
        public VMProperty<CommodityDataGridItem> SelectedCommodity { get; private set; }

        /// <summary>
        /// Gets or sets the collection of parts items displayed in the Parts data grid.
        /// </summary>
        public DispatchedObservableCollection<PartsGridItem> StepParts
        {
            get
            {
                return this.stepParts;
            }

            set
            {
                if (this.stepParts != value)
                {
                    this.stepParts = value;
                    OnPropertyChanged(() => this.StepParts);
                }
            }
        }

        /// <summary>
        /// Gets the selected commodity in the step commodities data grid.
        /// </summary>
        public VMProperty<PartsGridItem> SelectedPartItem { get; private set; }

        /// <summary>
        /// Gets or sets the project to which the Model belongs.
        /// This property is needed during cost calculations so it must be set when editing (it can be omitted when creating).
        /// </summary>
        public Project ParentProject { get; set; }

        /// <summary>
        /// Gets or sets the Process Parent. (assembly or part)
        /// </summary>
        public object ProcessParent { get; set; }

        /// <summary>
        /// Gets a value indicating whether the Information tab contains data or not.
        /// </summary>
        public VMProperty<bool> InformationTabHasData { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the Labour Settings tab contains data or not.
        /// </summary>
        public VMProperty<bool> LabourSettingsTabHasData { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the Additional Settings tab contains data or not.
        /// </summary>
        public VMProperty<bool> AdditionalSettingsTabHasData { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the Machines tab contains data or not.
        /// </summary>
        public VMProperty<bool> MachinesTabHasData { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the Tooling tab contains data or not.
        /// </summary>
        public VMProperty<bool> ToolingTabHasData { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the Consumables tab contains data or not.
        /// </summary>
        public VMProperty<bool> ConsumablesTabHasData { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the Commodities tab contains data or not.
        /// </summary>
        public VMProperty<bool> CommoditiesTabHasData { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the Parts tab contains data or not.
        /// </summary>
        public VMProperty<bool> PartsTabHasData { get; private set; }

        /// <summary>
        /// Gets a value indicating the selected tab index.
        /// </summary>
        public VMProperty<int> SelectedTabIndex { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the External SGA is available or not.
        /// </summary>
        public VMProperty<bool> IsExternalSGAAvailable { get; private set; }

        /// <summary>
        /// Gets a value indicating the Manufacturing overhead watermark.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public VMProperty<string> ManufacturingOHWatermark { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether the show cancel message will appear.
        /// </summary>
        public bool ShowCancelMessageFlag { get; set; }

        /// <summary>
        /// Gets or sets the calculation result for the process step parent.
        /// </summary>
        public CalculationResult ParentCost { get; set; }

        /// <summary>
        /// Gets or sets the cycle time calculator image source.
        /// </summary>
        public ImageSource CalculatorIcon
        {
            get { return this.calculatorIcon; }
            set { this.SetProperty(ref this.calculatorIcon, value, () => this.CalculatorIcon); }
        }

        /// <summary>
        /// Gets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter { get; private set; }

        /// <summary>
        /// Gets or sets the cache of costs for the sub-parts and sub-assemblies displayed in the step's Parts List.
        /// </summary>
        public Dictionary<Guid, CalculationResult> SubpartsCostCache
        {
            get { return this.subpartsCostCache; }
            set { this.SetProperty(ref this.subpartsCostCache, value, () => this.SubpartsCostCache); }
        }

        /// <summary>
        /// Gets or sets the message token to be used when sending messages from this instance. 
        /// </summary>
        public string MessageToken { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the capacity utilization value can be shown
        /// </summary>
        public bool CanShowCapacityUtilization
        {
            get { return this.canShowCapacityUtilization; }
            set { this.SetProperty(ref this.canShowCapacityUtilization, value, () => this.CanShowCapacityUtilization); }
        }

        #endregion Other Properties

        #region Helper

        /// <summary>
        /// Sets the default values for the missing fields of a process step.
        /// </summary>
        /// <param name="step">The process step.</param>
        /// <param name="processParent">The process parent.</param>
        /// <param name="basicSettings">The basic settings.</param>        
        public static void SetDefaultValuesForMissingStepFields(ProcessStep step, object processParent, BasicSetting basicSettings)
        {
            if (step == null)
            {
                return;
            }

            if (!step.Accuracy.HasValue)
            {
                step.Accuracy = (short)ProcessCalculationAccuracy.Calculated;
            }

            if (!step.SetupsPerBatch.HasValue)
            {
                step.SetupsPerBatch = ProcessStep.DefaultSetupsPerBatch;
            }

            if (!step.SetupTime.HasValue)
            {
                step.SetupTime = ProcessStep.DefaultSetupTime;
            }

            if (!step.MaxDownTime.HasValue)
            {
                step.MaxDownTime = ProcessStep.DefaultMaxDowntime;
            }

            if (!step.ScrapAmount.HasValue)
            {
                step.ScrapAmount = ProcessStep.DefaultScrapAmount;
            }

            if (!step.Rework.HasValue)
            {
                step.Rework = ProcessStep.DefaultReworkRatio;
            }

            if (!step.ShiftsPerWeek.HasValue)
            {
                step.ShiftsPerWeek = basicSettings != null ? basicSettings.ShiftsPerWeek : 0m;
            }

            if (!step.HoursPerShift.HasValue)
            {
                step.HoursPerShift = basicSettings != null ? basicSettings.HoursPerShift : 0m;
            }

            if (!step.ProductionDaysPerWeek.HasValue)
            {
                step.ProductionDaysPerWeek = basicSettings != null ? basicSettings.ProdDays : 0m;
            }

            if (!step.ProductionWeeksPerYear.HasValue)
            {
                step.ProductionWeeksPerYear = basicSettings != null ? basicSettings.ProdWeeks : 0m;
            }

            if (!step.BatchSize.HasValue)
            {
                int? batchSizePerYear = null;
                int? yearlyProdQty = null;

                var parentAssembly = processParent as Assembly;
                if (parentAssembly != null)
                {
                    batchSizePerYear = parentAssembly.BatchSizePerYear;
                    yearlyProdQty = parentAssembly.YearlyProductionQuantity;
                }
                else
                {
                    var parentPart = processParent as Part;
                    if (parentPart != null)
                    {
                        batchSizePerYear = parentPart.BatchSizePerYear;
                        yearlyProdQty = parentPart.YearlyProductionQuantity;
                    }
                }

                if (yearlyProdQty.HasValue && batchSizePerYear.HasValue && batchSizePerYear != 0)
                {
                    double batchSize = Math.Ceiling(Convert.ToDouble(yearlyProdQty.Value) / Convert.ToDouble(batchSizePerYear.Value));
                    if (batchSize <= int.MaxValue)
                    {
                        step.BatchSize = Convert.ToInt32(batchSize);
                    }
                }
            }

            if (!step.ProductionUnskilledLabour.HasValue)
            {
                step.ProductionUnskilledLabour = 0m;
            }

            if (!step.ProductionSkilledLabour.HasValue)
            {
                step.ProductionSkilledLabour = 0m;
            }

            if (!step.ProductionForeman.HasValue)
            {
                step.ProductionForeman = 0m;
            }

            if (!step.ProductionTechnicians.HasValue)
            {
                step.ProductionTechnicians = 0m;
            }

            if (!step.ProductionEngineers.HasValue)
            {
                step.ProductionEngineers = 0m;
            }

            if (!step.SetupUnskilledLabour.HasValue)
            {
                step.SetupUnskilledLabour = 0m;
            }

            if (!step.SetupSkilledLabour.HasValue)
            {
                step.SetupSkilledLabour = 0m;
            }

            if (!step.SetupForeman.HasValue)
            {
                step.SetupForeman = 0m;
            }

            if (!step.SetupTechnicians.HasValue)
            {
                step.SetupTechnicians = 0m;
            }

            if (!step.SetupEngineers.HasValue)
            {
                step.SetupEngineers = 0m;
            }
        }

        #endregion Helper

        #region Initialization

        /// <summary>
        /// Initializes the view-model for the creation of a new process step.
        /// The ModelDataContext property must be set to a valid value before this call.
        /// </summary>
        public void InitializeForCreation()
        {
            using (this.UndoManager.Pause())
            {
                this.StopRecalculationNotifications();

                this.CheckDataSource();
                this.EditMode = ViewModelEditMode.Create;

                // Initialize Information Tab.
                this.Accuracy.Value = ProcessCalculationAccuracy.Calculated;
                this.SetupsPerBatch.Value = ProcessStep.DefaultSetupsPerBatch;
                this.SetupTime.Value = ProcessStep.DefaultSetupTime;
                this.MaxDownTime.Value = ProcessStep.DefaultMaxDowntime;

                // Initialize the default measurement units.
                var measurementUnit = this.MeasurementUnitsAdapter.GetBaseMeasurementUnit(MeasurementUnitType.Time);
                this.ProcessTimeUnit.Value = measurementUnit;
                this.CycleTimeUnit.Value = measurementUnit;

                measurementUnit = this.MeasurementUnitsAdapter.GetMeasurementUnits(MeasurementUnitType.Time).FirstOrDefault(u => u.ScaleFactor == 60m);
                this.MaxDownTimeUnit.Value = measurementUnit;
                this.SetupTimeUnit.Value = measurementUnit;

                this.ScrapAmount.Value = ProcessStep.DefaultScrapAmount;

                var basicSettings = this.DataSourceManager.BasicSettingsRepository.GetBasicSettings();

                if (this.ParentProject != null)
                {
                    if (this.ParentProject.ShiftsPerWeek.HasValue)
                    {
                        this.ShiftsPerWeek.Value = this.ParentProject.ShiftsPerWeek.Value;
                    }
                    else if (basicSettings != null)
                    {
                        this.ShiftsPerWeek.Value = basicSettings.ShiftsPerWeek;
                    }

                    if (this.ParentProject.HoursPerShift.HasValue)
                    {
                        this.HoursPerShift.Value = this.ParentProject.HoursPerShift.Value;
                    }
                    else if (basicSettings != null)
                    {
                        this.HoursPerShift.Value = basicSettings.HoursPerShift;
                    }

                    if (this.ParentProject.ProductionDaysPerWeek.HasValue)
                    {
                        this.ProductionDaysPerWeek.Value = this.ParentProject.ProductionDaysPerWeek.Value;
                    }
                    else if (basicSettings != null)
                    {
                        this.ProductionDaysPerWeek.Value = basicSettings.ProdDays;
                    }

                    if (this.ParentProject.ProductionWeeksPerYear.HasValue)
                    {
                        this.ProductionWeeksPerYear.Value = this.ParentProject.ProductionWeeksPerYear;
                    }
                    else if (basicSettings != null)
                    {
                        this.ProductionWeeksPerYear.Value = basicSettings.ProdWeeks;
                    }
                }

                int? batchSizePerYear = null;
                int? yearlyProdQty = null;
                if (this.ProcessParent is Part)
                {
                    Part part = (Part)this.ProcessParent;
                    batchSizePerYear = part.BatchSizePerYear;
                    yearlyProdQty = part.YearlyProductionQuantity;
                }
                else if (this.ProcessParent is Assembly)
                {
                    Assembly assy = (Assembly)this.ProcessParent;
                    batchSizePerYear = assy.BatchSizePerYear;
                    yearlyProdQty = assy.YearlyProductionQuantity;
                }

                if (yearlyProdQty.HasValue && batchSizePerYear.HasValue && batchSizePerYear != 0)
                {
                    decimal batchSize = Math.Ceiling(Convert.ToDecimal(yearlyProdQty.Value) / Convert.ToDecimal(batchSizePerYear.Value));
                    this.BatchSize.Value = batchSize < int.MaxValue ? (int)batchSize : int.MaxValue;
                }
                else
                {
                    this.BatchSize.Value = null;
                }

                // Initialize Labor Settings.
                this.ProductionUnskilledLabour.Value = 0;
                this.ProductionSkilledLabour.Value = 0;
                this.ProductionForeman.Value = 0;
                this.ProductionTechnicians.Value = 0;
                this.ProductionEngineers.Value = 0;
                this.SetupUnskilledLabour.Value = 0;
                this.SetupSkilledLabour.Value = 0;
                this.SetupForeman.Value = 0;
                this.SetupTechnicians.Value = 0;
                this.SetupEngineers.Value = 0;

                // Initialize Additional Settings.
                this.TransportCostType.Value = ProcessTransportCostType.PerPart;

                this.ResumeRecalculationNotifications(false);
            }
        }

        /// <summary>
        /// Initializes the properties of this instance.
        /// </summary>
        private void InitializeProperties()
        {
            // Initialize the other properties.
            this.ShowCancelMessageFlag = true;
            this.IsExceededShiftCost.Value = this.Accuracy.Value == ProcessCalculationAccuracy.Calculated && this.ExceedShiftCost.Value;
            this.IsTransportCostEnabled.Value = !this.IsReadOnly && (this.IsExternal.Value ?? false);

            this.ExceedShiftCost.ValueChanged += (s, e) => this.OnExceededShiftCostChanged();
            this.Accuracy.ValueChanged += (s, e) => this.OnAccuracyChanged();
            this.IsExternal.ValueChanged += (s, e) => { this.IsTransportCostEnabled.Value = !this.IsReadOnly && (this.IsExternal.Value ?? false); };

            this.Price.ValueChanged += (s, e) => this.CheckInformationTabData();
            this.ProcessTime.ValueChanged += (s, e) => this.CheckInformationTabData();
            this.CycleTime.ValueChanged += (s, e) => this.CheckInformationTabData();
            this.PartsPerCycle.ValueChanged += (s, e) => this.CheckInformationTabData();
            this.ScrapAmount.ValueChanged += (s, e) => this.CheckInformationTabData();
            this.SetupsPerBatch.ValueChanged += (s, e) => this.CheckInformationTabData();
            this.MaxDownTime.ValueChanged += (s, e) => this.CheckInformationTabData();
            this.SetupTime.ValueChanged += (s, e) => this.CheckInformationTabData();
            this.BatchSize.ValueChanged += (s, e) => this.CheckInformationTabData();
            this.ManufacturingOverhead.ValueChanged += (s, e) => this.CheckInformationTabData();
            this.ShiftsPerWeek.ValueChanged += (s, e) => this.CheckInformationTabData();
            this.HoursPerShift.ValueChanged += (s, e) => this.CheckInformationTabData();
            this.ProductionDaysPerWeek.ValueChanged += (s, e) => this.CheckInformationTabData();
            this.ProductionWeeksPerYear.ValueChanged += (s, e) => this.CheckInformationTabData();
            this.ExtraShiftsNumber.ValueChanged += (s, e) => this.CheckInformationTabData();
            this.ShiftCostExceedRatio.ValueChanged += (s, e) => this.CheckInformationTabData();
            this.Description.ValueChanged += (s, e) => this.CheckInformationTabData();
            this.Name.ValueChanged += (s, e) => this.OnNameChanged();

            this.messenger.Register<MediaChangedMessage>((msg) => this.CheckInformationTabData());

            this.ProductionUnskilledLabour.ValueChanged += (s, e) => this.CheckLabourSettingsTabData();
            this.ProductionSkilledLabour.ValueChanged += (s, e) => this.CheckLabourSettingsTabData();
            this.ProductionForeman.ValueChanged += (s, e) => this.CheckLabourSettingsTabData();
            this.ProductionEngineers.ValueChanged += (s, e) => this.CheckLabourSettingsTabData();
            this.ProductionTechnicians.ValueChanged += (s, e) => this.CheckLabourSettingsTabData();
            this.SetupUnskilledLabour.ValueChanged += (s, e) => this.CheckLabourSettingsTabData();
            this.SetupSkilledLabour.ValueChanged += (s, e) => this.CheckLabourSettingsTabData();
            this.SetupForeman.ValueChanged += (s, e) => this.CheckLabourSettingsTabData();
            this.SetupEngineers.ValueChanged += (s, e) => this.CheckLabourSettingsTabData();
            this.SetupTechnicians.ValueChanged += (s, e) => this.CheckLabourSettingsTabData();

            this.IsTransportCostEnabled.ValueChanged += (s, e) => this.OnIsTransportCostEnabledChanged();
            this.TransportCost.ValueChanged += (s, e) => this.CheckAdditionalSettingsTabsData();
            this.IsTransportPerQtySelected.ValueChanged += (s, e) => this.CheckAdditionalSettingsTabsData();
            this.TransportCostQty.ValueChanged += (s, e) => this.CheckAdditionalSettingsTabsData();
            this.TransportCostType.ValueChanged += (s, e) => this.OnTransportCostTypeChanged();

            this.StepParts.CollectionChanged += (s, e) => this.OnStepPartsCollectionChanged();
        }

        /// <summary>
        /// Initialize the ObservableCollections used in Undo. [ need to be initialized after undo manager is initialized ]
        /// </summary>
        public void InitializeCollections()
        {
            this.StepMachines = new DispatchedObservableCollection<MachineDataGridItem>();
            this.StepDieItems = new DispatchedObservableCollection<DieDataGridItem>();
            this.StepConsumables = new DispatchedObservableCollection<ConsumableDataGridItem>();
            this.StepCommodities = new DispatchedObservableCollection<CommodityDataGridItem>();

            this.StepMachines.CollectionChanged += this.HandleStepMachinesCollectionChanged;
            this.StepDieItems.CollectionChanged += this.HandleStepDiesCollectionChanged;
            this.StepConsumables.CollectionChanged += this.HandleStepConsumablesCollectionChanged;
            this.StepCommodities.CollectionChanged += this.HandleStepCommoditiesCollectionChanged;
        }

        /// <summary>
        /// Initializes the view-model's commands.
        /// </summary>
        private void InitializeCommands()
        {
            this.AddCommand = new DelegateCommand<object>(this.AddEntity, this.CanAddEntity);
            this.EditCommand = new DelegateCommand<object>(this.EditEntity);
            this.DeleteCommand = new DelegateCommand<object>(this.ExecuteDelete, this.CanExecuteDelete);
            this.CutCommand = new DelegateCommand<object>(this.ExecuteCut, this.CanExecuteCut);
            this.CopyCommand = new DelegateCommand<object>(this.ExecuteCopy, this.CanExecuteCopy);
            this.PasteCommand = new DelegateCommand<object>(this.ExecutePaste, this.CanExecutePaste);
            this.DropCommand = new DelegateCommand<object>(this.ExecuteDrop, (o) => !this.IsReadOnly);
            this.ImportCommand = new DelegateCommand<object>(this.ExecuteImport, this.CanExecuteImport);
            this.ExportCommand = new DelegateCommand<object>(this.ExecuteExport, this.CanExecuteExport);
            this.MouseDoubleClickCommand = new DelegateCommand<MouseButtonEventArgs>(this.MouseDoubleClicked);
            this.HandleCyCleTimeCalculatorCommand = new DelegateCommand(this.HandleCycleTimeCalculator);

            this.CancelNestedViewModels = new CompositeCommand();
            this.CancelNestedViewModels.RegisterCommand(this.MediaViewModel.CancelCommand);
            this.CancelNestedViewModels.RegisterCommand(this.TypeSelectorViewModel.CancelCommand);
        }

        /// <summary>
        /// Initialize the undo manager.
        /// </summary>
        public void InitializeUndoManager()
        {
            this.MediaViewModel.UndoManager = this.UndoManager;
            this.TypeSelectorViewModel.UndoManager = this.UndoManager;

            this.UndoManager.RegisterPropertyAction(() => this.StepMachines, this.HandleUndoOnStepCollections, this);
            this.UndoManager.RegisterPropertyAction(() => this.StepDieItems, this.HandleUndoOnStepCollections, this);
            this.UndoManager.RegisterPropertyAction(() => this.StepConsumables, this.HandleUndoOnStepCollections, this);
            this.UndoManager.RegisterPropertyAction(() => this.StepCommodities, this.HandleUndoOnStepCollections, this);
        }

        #endregion Initialization

        #region Model handling

        /// <summary>
        /// Loads the data from the model.
        /// Each view-model property decorated with the ExposedModelProperty attribute is loaded from the associated Model property.
        /// </summary>
        /// <param name="model">The model instance.</param>
        public override void LoadDataFromModel(ProcessStep model)
        {
            try
            {
                this.isModelLoading = true;
                base.LoadDataFromModel(model);

                using (this.UndoManager.Pause())
                {
                    this.LoadProcessStepData();
                    this.LoadProcessStepParts(this.SubpartsCostCache);
                }
            }
            finally
            {
                this.isModelLoading = false;
            }

            this.CheckTabsForData();
        }

        /// <summary>
        /// Called when the Model has changed.
        /// </summary>
        protected override void OnModelChanged()
        {
            this.CheckDataSource();
            this.LoadDataFromModel();

            if (this.IsInViewerMode)
            {
                this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(null);
            }

            this.TypeSelectorViewModel.DataAccessContext = this.DataSourceManager;

            this.MediaViewModel.DataSourceManager = this.DataSourceManager;
            this.MediaViewModel.Model = this.Model;

            if (!this.IsChild)
            {
                this.UndoManager.Start();
            }
        }

        /// <summary>
        /// Called when the DataSourceManager changed.
        /// </summary>
        protected override void OnDataSourceManagerChanged()
        {
            base.OnDataSourceManagerChanged();

            this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(this.DataSourceManager);
            this.unitsAdapterHandler = new UnitsAdapterUpdateHandler(this.MeasurementUnitsAdapter);
            this.unitsAdapterHandler.PauseUndoOnUnitsAdapterUpdate(this.UndoManager);
        }

        /// <summary>
        /// Load in view-model process step data.
        /// </summary>
        private void LoadProcessStepData()
        {
            this.StepMachines.Clear();
            this.StepDieItems.Clear();
            this.StepCommodities.Clear();
            this.StepConsumables.Clear();
            this.StepParts.Clear();

            OverheadSetting ohsettings = this.GetOverheadSettingsOfProcessParent();
            if (ohsettings != null)
            {
                // Set the ManufacturingOHTextBox's watermark to be the parent's manufacturing overhead because that value is used in calculations
                // if the step's manufacturing overhead is not set.
                this.ManufacturingOHWatermark.Value = Formatter.FormatNumber(ohsettings.ManufacturingOverhead * 100m);
            }

            var orderedMachines = this.Model.Machines.Where(m => !m.IsDeleted).OrderBy(m => m.Index);
            foreach (var machine in orderedMachines)
            {
                var machineItem = new MachineDataGridItem() { Machine = machine, UndoManager = this.UndoManager };
                machineItem.PropertyChanged += new PropertyChangedEventHandler(this.OnStepMachineChanged);
                this.StepMachines.Add(machineItem);
            }

            this.SelectedMachineItem.Value = this.StepMachines.FirstOrDefault();

            var dies = this.Model.Dies.Where(d => !d.IsDeleted);
            foreach (var die in dies)
            {
                var dieItem = new DieDataGridItem() { Die = die };
                this.StepDieItems.Add(dieItem);
            }

            this.SelectedDieItem.Value = this.StepDieItems.FirstOrDefault();

            var consumables = this.Model.Consumables.Where(d => !d.IsDeleted);
            foreach (var consumable in consumables)
            {
                var consumableItem = new ConsumableDataGridItem(consumable);
                this.StepConsumables.Add(consumableItem);
            }

            this.SelectedConsumable.Value = this.StepConsumables.FirstOrDefault();

            var commodities = this.Model.Commodities.Where(d => !d.IsDeleted);
            foreach (var commodity in commodities)
            {
                var commodityItem = new CommodityDataGridItem() { Commodity = commodity };
                this.StepCommodities.Add(commodityItem);
            }

            this.SelectedCommodity.Value = this.StepCommodities.FirstOrDefault();

            // Show/hide some controls based on the entity whose this process is.
            if (this.ProcessParent is Part)
            {
                this.IsAssemblyProcessStep.Value = false;
                this.costCalculationVersion = ((Part)this.ProcessParent).CalculationVariant;
            }
            else if (this.ProcessParent is Assembly)
            {
                this.IsAssemblyProcessStep.Value = true;
                this.costCalculationVersion = ((Assembly)this.ProcessParent).CalculationVariant;
            }

            // SG&A on external parts and assemblies is enabled starting with calculation variant 1.4
            this.IsExternalSGAAvailable.Value = CostCalculatorFactory.IsOlder(this.costCalculationVersion, "1.4");

            using (this.UndoManager.StartBatch(navigateToBatchControls: true))
            {
                if (this.Model.Type != null)
                {
                    if (this.TypeSelectorViewModel.ClassificationLevels.Count > 0)
                    {
                        if (this.TypeSelectorViewModel.ClassificationLevels[0] != this.Model.Type)
                        {
                            while (this.TypeSelectorViewModel.ClassificationLevels.Count > 0)
                            {
                                this.TypeSelectorViewModel.ClassificationLevels.RemoveAt(0);
                            }

                            this.TypeSelectorViewModel.ClassificationLevels.Add(this.Model.Type);
                        }
                    }
                    else
                    {
                        this.TypeSelectorViewModel.ClassificationLevels.Add(this.Model.Type);
                    }
                }
                else
                {
                    while (this.TypeSelectorViewModel.ClassificationLevels.Count > 0)
                    {
                        this.TypeSelectorViewModel.ClassificationLevels.RemoveAt(0);
                    }
                }

                if (this.Model.SubType != null)
                {
                    if (this.TypeSelectorViewModel.ClassificationLevels.Count > 1)
                    {
                        if (this.TypeSelectorViewModel.ClassificationLevels[1] != this.Model.SubType)
                        {
                            this.TypeSelectorViewModel.ClassificationLevels.RemoveAt(1);
                            this.TypeSelectorViewModel.ClassificationLevels.Add(this.Model.SubType);
                        }
                    }
                    else
                    {
                        this.TypeSelectorViewModel.ClassificationLevels.Add(this.Model.SubType);
                    }
                }
                else if (this.TypeSelectorViewModel.ClassificationLevels.Count > 1)
                {
                    this.TypeSelectorViewModel.ClassificationLevels.RemoveAt(1);
                }
            }

            TypeSelectorViewModel.ClassificationLevels.CollectionChanged += (s, e) => this.CheckInformationTabData();
            this.TypeSelectorViewModel.IsChanged = false;

            this.IsTransportPerQtySelected.Value = this.TransportCostType.Value == ProcessTransportCostType.PerQty;
            if (!this.IsTransportPerQtySelected.Value)
            {
                this.TransportCostQty.Value = 1;
            }

            this.IsAccuracyCalculated.Value = this.Accuracy.Value == ProcessCalculationAccuracy.Calculated;
            if (this.IsAccuracyCalculated.Value)
            {
                this.Price.Value = 0;
            }

            if (!this.IsChild)
            {
                this.RefreshCalculation();
            }
            else
            {
                this.UpdateSubEntitiesCost(this.ParentCost);
            }

            if (this.Accuracy.Value == ProcessCalculationAccuracy.Estimated
                && this.SelectedTabIndex.Value != (int)SelectedProcessStepTab.PartsTab)
            {
                this.SelectedTabIndex.Value = (int)SelectedProcessStepTab.InformationTab;
            }

            this.CalculatorIcon = this.Model.CycleTimeCalculations.Count > 0 ? Images.CalculatorCheckedIcon : Images.CalculatorIcon;
        }

        /// <summary>
        /// Load Process Step parts items.
        /// </summary>
        /// <param name="subObjectsCostCache">The cache of costs for the sub-parts and sub-assemblies displayed in the step's Parts List.
        /// Set it to null if caching is not needed (the costs will be computed here).</param>
        private void LoadProcessStepParts(Dictionary<Guid, CalculationResult> subObjectsCostCache)
        {
            // *** Set the data in the Parts tab *********************************
            if (this.ModelParentClone is Assembly && this.ModelClone is AssemblyProcessStep)
            {
                var assembly = this.ModelParentClone as Assembly;

                // Add all parent's parts to the list                
                foreach (Part part in assembly.Parts.Where(p => !p.IsDeleted))
                {
                    // Determine the cost of the part by getting it from the costs cache, if caching is available, or computing it otherwise.
                    decimal? partCost = null;
                    bool costFoundInCache = false;
                    if (subObjectsCostCache != null)
                    {
                        // get the cost from the cache
                        CalculationResult partCalcResult = null;
                        costFoundInCache = subObjectsCostCache.TryGetValue(part.Guid, out partCalcResult);
                        if (partCalcResult != null)
                        {
                            partCost = partCalcResult.Summary.TargetCost;
                        }
                    }

                    if (!costFoundInCache)
                    {
                        PartCostCalculationParameters calculationParams = null;
                        if (this.ParentProject != null)
                        {
                            calculationParams = CostCalculationHelper.CreatePartParamsFromProject(this.ParentProject);
                        }
                        else if (this.IsInViewerMode)
                        {
                            calculationParams = this.modelBrowserHelperService.GetPartCostCalculationParameters();
                        }

                        if (calculationParams != null)
                        {
                            // If the cost was not found in the cache or the cache is not available calculate it.
                            ICostCalculator calculator = CostCalculatorFactory.GetCalculator(part.CalculationVariant);
                            var result = calculator.CalculatePartCost(part, calculationParams);
                            partCost = result.Summary.TargetCost;

                            // Add the cost to the cache if caching is available
                            if (subObjectsCostCache != null)
                            {
                                subObjectsCostCache.Add(part.Guid, result);
                            }
                        }
                    }

                    var amount = this.ModelClone.PartAmounts.FirstOrDefault(partAmount => partAmount.PartGuid == part.Guid || partAmount.Part == part);

                    int amountSum = 0;
                    foreach (var step in assembly.Process.Steps)
                    {
                        if (step != this.ModelClone && step.PartAmounts.Count > 0)
                        {
                            var currentAmount = step.PartAmounts.FirstOrDefault(a => a.Part.Guid == part.Guid);
                            if (currentAmount != null)
                            {
                                amountSum += currentAmount.Amount;
                            }
                        }
                    }

                    var item = new PartsGridPartItem(part, partCost, amount, this.ModelClone, assembly.OverheadSettings);
                    item.AmountSum = amountSum;
                    item.UndoManager = this.UndoManager;

                    this.StepParts.Add(item);
                }

                // Add all parent's assemblies to the list                
                foreach (Assembly subassembly in assembly.Subassemblies.Where(s => !s.IsDeleted))
                {
                    // Determine the cost of the part by getting it from the costs cache, if caching is available, or computing it otherwise.
                    decimal? subAssyCost = null;
                    bool costFoundInCache = false;
                    if (subObjectsCostCache != null)
                    {
                        // get the cost from the cache
                        CalculationResult partCalcResult = null;
                        costFoundInCache = subObjectsCostCache.TryGetValue(subassembly.Guid, out partCalcResult);
                        if (partCalcResult != null)
                        {
                            subAssyCost = partCalcResult.Summary.TargetCost;
                        }
                    }

                    if (!costFoundInCache)
                    {
                        AssemblyCostCalculationParameters calculationParams = null;
                        if (this.ParentProject != null)
                        {
                            calculationParams = CostCalculationHelper.CreateAssemblyParamsFromProject(this.ParentProject);
                        }
                        else if (this.IsInViewerMode)
                        {
                            calculationParams = this.modelBrowserHelperService.GetAssemblyCostCalculationParameters();
                        }

                        if (calculationParams != null)
                        {
                            // If the cost was not found in the cache or the cache is not available calculate it.
                            ICostCalculator calculator = CostCalculatorFactory.GetCalculator(subassembly.CalculationVariant);
                            var result = calculator.CalculateAssemblyCost(subassembly, calculationParams);
                            subAssyCost = result.Summary.TargetCost;

                            // Add the cost to the cache if caching is available
                            if (subObjectsCostCache != null)
                            {
                                subObjectsCostCache.Add(subassembly.Guid, result);
                            }
                        }
                    }

                    var subAssyAmount = this.ModelClone.AssemblyAmounts.FirstOrDefault(amount => amount.AssemblyGuid == subassembly.Guid || amount.Assembly == subassembly);

                    int amountSum = 0;
                    foreach (var step in assembly.Process.Steps)
                    {
                        if (step != this.ModelClone && step.AssemblyAmounts.Count > 0)
                        {
                            var currentAmount = step.AssemblyAmounts.FirstOrDefault(a => a.Assembly.Guid == subassembly.Guid);
                            if (currentAmount != null)
                            {
                                amountSum += currentAmount.Amount;
                            }
                        }
                    }

                    var item = new PartsGridAssemblyItem(subassembly, subAssyCost, subAssyAmount, this.ModelClone, assembly.OverheadSettings);
                    item.AmountSum = amountSum;
                    item.UndoManager = this.UndoManager;

                    this.StepParts.Add(item);
                }
            }
        }

        #endregion Model handling

        #region Commands handling

        /// <summary>
        /// Checks if any new entities can be added into the process step
        /// Entities can be added just if the accuracy is <see cref="ProcessCalculationAccuracy.Calculated"/>
        /// </summary>
        /// <param name="entityType">The added entity type.</param>
        /// <returns>True if process step accepts adding new entities and false otherwise</returns>
        private bool CanAddEntity(object entityType)
        {
            if (this.Accuracy.Value != null && this.Accuracy.Value != ProcessCalculationAccuracy.Estimated)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Performs the add operation, creating a new entity and adding it in the data grid based on the type of master data displayed
        /// </summary>
        /// <param name="parameter">The added entity type.</param>
        private void AddEntity(object parameter)
        {
            var entityType = parameter as Type;
            if (entityType == null)
            {
                return;
            }

            if (entityType == typeof(Machine))
            {
                this.AddMachine();
                return;
            }

            if (entityType == typeof(Die))
            {
                this.AddDie();
                return;
            }

            if (entityType == typeof(Consumable))
            {
                this.AddConsumable();
                return;
            }

            if (entityType == typeof(Commodity))
            {
                this.AddCommodity();
                return;
            }
        }

        /// <summary>
        /// Display the "Create Machine" window and add the created machine, if any, to the current step.
        /// </summary>
        private void AddMachine()
        {
            var machineVM = this.compositionContainer.GetExportedValue<MachineViewModel>();
            machineVM.Title = LocalizedResources.General_CreateMachine;
            machineVM.DataSourceManager = this.DataSourceManager;
            machineVM.SavesToDataSource = false;
            machineVM.IsInViewerMode = this.IsInViewerMode;
            machineVM.IsReadOnly = this.IsReadOnly;
            machineVM.MachineCalculationParams = this.GetMachineCostCalcParameter();
            machineVM.CostCalculationVersion = this.costCalculationVersion;
            machineVM.MessageToken = this.MessageToken;
            machineVM.InitializeForCreation(false, this.Model);

            this.windowService.ShowViewInDialog(machineVM, "MachineWindowViewTemplate");

            var newMachine = machineVM.Model;
            if (newMachine != null && machineVM.Saved)
            {
                using (this.UndoManager.StartBatch(navigateToBatchControls: true))
                {
                    var machineItem = new MachineDataGridItem() { Machine = newMachine };
                    this.StepMachines.Add(machineItem);

                    // Navigate to the machine only if the Process calculation accuracy is calculated.
                    if (this.Accuracy.Value == ProcessCalculationAccuracy.Calculated)
                    {
                        this.SelectedTabIndex.Value = (int)SelectedProcessStepTab.MachinesTab;
                        this.SelectedMachineItem.Value = machineItem;
                    }
                }
            }
        }

        /// <summary>
        /// Display the "Create Die" window and add the created die, if any, to the current step.
        /// </summary>
        private void AddDie()
        {
            var dieVM = this.compositionContainer.GetExportedValue<DieViewModel>();
            dieVM.DataSourceManager = this.DataSourceManager;
            dieVM.IsReadOnly = this.IsReadOnly;
            dieVM.IsInViewerMode = false;
            dieVM.DieParent = this.ProcessParent;
            dieVM.SavesToDataSource = false;
            dieVM.Title = LocalizedResources.General_CreateDie;
            dieVM.EditMode = ViewModelEditMode.Create;
            dieVM.MessageToken = this.MessageToken;
            dieVM.InitializeForCreation(false, this.Model);

            this.windowService.ShowViewInDialog(dieVM, "DieViewTemplate");

            var newDie = dieVM.Model;
            if (newDie != null && dieVM.Saved)
            {
                using (this.UndoManager.StartBatch(navigateToBatchControls: true))
                {
                    var dieItem = new DieDataGridItem() { Die = newDie };
                    this.StepDieItems.Add(dieItem);

                    // Navigate to the machine only if the Process calculation accuracy is calculated.
                    if (this.Accuracy.Value == ProcessCalculationAccuracy.Calculated)
                    {
                        this.SelectedTabIndex.Value = (int)SelectedProcessStepTab.DiesTab;
                        this.SelectedDieItem.Value = dieItem;
                    }
                }
            }
        }

        /// <summary>
        /// Display the "Create Consumable" window and add the created consumable, if any, to the current step.
        /// </summary>
        private void AddConsumable()
        {
            var consumableVM = this.compositionContainer.GetExportedValue<ConsumableViewModel>();
            consumableVM.DataSourceManager = this.DataSourceManager;
            consumableVM.InitializeForCreation(false, this.Model);
            consumableVM.SavesToDataSource = false;
            consumableVM.ConsumableParent = this.ProcessParent;
            consumableVM.Title = LocalizedResources.General_CreateConsumable;
            consumableVM.MessageToken = this.MessageToken;

            this.windowService.ShowViewInDialog(consumableVM, "ConsumableWindowViewTemplate");

            var newConsumable = consumableVM.Model;
            if (newConsumable != null && consumableVM.Saved)
            {
                using (this.UndoManager.StartBatch(navigateToBatchControls: true))
                {
                    var consumableItem = new ConsumableDataGridItem(newConsumable);
                    this.StepConsumables.Add(consumableItem);

                    // Navigate to the machine only if the Process calculation accuracy is calculated.
                    if (this.Accuracy.Value == ProcessCalculationAccuracy.Calculated)
                    {
                        this.SelectedTabIndex.Value = (int)SelectedProcessStepTab.ConsumablesTab;
                        this.SelectedConsumable.Value = consumableItem;
                    }
                }
            }
        }

        /// <summary>
        /// Display the "Create Commodity" window and add the created commodity, if any, to the current step.
        /// </summary>
        private void AddCommodity()
        {
            var commodityVM = this.compositionContainer.GetExportedValue<CommodityViewModel>();
            commodityVM.DataSourceManager = this.DataSourceManager;
            commodityVM.InitializeForCreation(false, this.Model);
            commodityVM.SavesToDataSource = false;
            commodityVM.CommodityParent = this.ProcessParent;
            commodityVM.Title = LocalizedResources.General_CreateCommodity;
            commodityVM.MessageToken = this.MessageToken;

            this.windowService.ShowViewInDialog(commodityVM, "CommodityWindowViewTemplate");

            var newCommodity = commodityVM.Model;
            if (newCommodity != null && commodityVM.Saved)
            {
                using (this.UndoManager.StartBatch(navigateToBatchControls: true))
                {
                    var commodityItem = new CommodityDataGridItem() { Commodity = newCommodity };
                    this.StepCommodities.Add(commodityItem);

                    // Navigate to the machine only if the Process calculation accuracy is calculated.
                    if (this.Accuracy.Value == ProcessCalculationAccuracy.Calculated)
                    {
                        this.SelectedTabIndex.Value = (int)SelectedProcessStepTab.CommoditiesTab;
                        this.SelectedCommodity.Value = commodityItem;
                    }
                }
            }
        }

        /// <summary>
        /// Handles the double click event on the data grid
        /// </summary>
        /// <param name="e">The mouse button event args</param>
        private void MouseDoubleClicked(MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                // Try to find the clicked one item.
                DependencyObject source = (DependencyObject)e.OriginalSource;
                var row = UIHelper.FindParent<DataGridRow>(source);
                if (row != null)
                {
                    var machineItem = row.Item as MachineDataGridItem;
                    if (machineItem != null)
                    {
                        this.EditEntity(machineItem.Machine);
                        return;
                    }

                    var dieItem = row.Item as DieDataGridItem;
                    if (dieItem != null)
                    {
                        this.EditEntity(dieItem.Die);
                        return;
                    }

                    var consumableItem = row.Item as ConsumableDataGridItem;
                    if (consumableItem != null)
                    {
                        this.EditEntity(consumableItem.Consumable);
                        return;
                    }

                    var commodityItem = row.Item as CommodityDataGridItem;
                    if (commodityItem != null)
                    {
                        this.EditEntity(commodityItem.Commodity);
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// Shows the edit screen popup for the selected item in the data grid.
        /// </summary>
        /// <param name="parameter">The entity which will populate the edit screen.</param>
        private void EditEntity(object parameter)
        {
            if (parameter == null)
            {
                return;
            }

            var selectedMachine = parameter as Machine;
            if (selectedMachine != null)
            {
                this.EditMachine(selectedMachine);
                return;
            }

            var selectedDie = parameter as Die;
            if (selectedDie != null)
            {
                this.EditDie(selectedDie);
                return;
            }

            var selectedConsumable = parameter as Consumable;
            if (selectedConsumable != null)
            {
                this.EditConsumable(selectedConsumable);
                return;
            }

            var selectedCommodity = parameter as Commodity;
            if (selectedCommodity != null)
            {
                this.EditCommodity(selectedCommodity);
                return;
            }
        }

        /// <summary>
        /// Edits the machine.
        /// </summary>
        /// <param name="machine">The machine to edit.</param>
        private void EditMachine(Machine machine)
        {
            if (machine == null)
            {
                return;
            }

            string initialName = machine.Name;

            var machineVM = this.compositionContainer.GetExportedValue<MachineViewModel>();
            machineVM.Title = this.IsReadOnly ? LocalizedResources.General_ViewMachine : LocalizedResources.General_EditMachine;
            machineVM.EditMode = ViewModelEditMode.Edit;
            machineVM.DataSourceManager = this.DataSourceManager;
            machineVM.SavesToDataSource = false;
            machineVM.IsInViewerMode = this.IsInViewerMode;
            machineVM.IsReadOnly = this.IsReadOnly;
            machineVM.ParentProject = this.ParentProject;
            machineVM.MachineParent = this.ProcessParent;
            machineVM.CostCalculationVersion = this.costCalculationVersion;
            machineVM.MachineCalculationParams = this.GetMachineCostCalcParameter();
            machineVM.IsChild = true;
            machineVM.ModelClone = this.ModelClone.Machines.FirstOrDefault(m => m.Guid == machine.Guid);
            machineVM.ModelParentClone = this.ModelParentClone;
            machineVM.MessageToken = this.MessageToken;
            machineVM.Model = machine;

            this.windowService.ShowViewInDialog(machineVM, "MachineWindowViewTemplate");

            if (machineVM.Saved)
            {
                // Recalculate the machine cost after editing.
                this.IsChanged = true;
                this.RefreshCalculation();

                if (initialName != machine.Name)
                {
                    var msg = new ProcessStepChangedMessage(this, this.Model)
                    {
                        StepID = this.Model.Guid,
                        SubEntity = machine,
                        ChangeType = ProcessStepChangeType.SubEntityNameUpdated
                    };
                    this.SendMessage(msg, true);
                }
            }
        }

        /// <summary>
        /// Edits the die.
        /// </summary>
        /// <param name="die">The die to edit.</param>
        private void EditDie(Die die)
        {
            if (die == null)
            {
                return;
            }

            var dieVM = this.compositionContainer.GetExportedValue<DieViewModel>();
            dieVM.IsReadOnly = this.IsReadOnly;
            dieVM.IsInViewerMode = this.IsInViewerMode;
            dieVM.EditMode = ViewModelEditMode.Edit;
            dieVM.ParentProject = this.ParentProject;
            dieVM.DieParent = this.ProcessParent;
            dieVM.DataSourceManager = this.DataSourceManager;
            dieVM.SavesToDataSource = false;
            dieVM.IsChild = true;
            dieVM.ModelClone = this.ModelClone.Dies.FirstOrDefault(d => d.Guid == die.Guid);
            dieVM.ModelParentClone = this.ModelParentClone;
            dieVM.MessageToken = this.MessageToken;
            dieVM.Model = die;
            dieVM.Title = this.IsReadOnly ? LocalizedResources.General_ViewDie : LocalizedResources.General_EditDie;

            this.windowService.ShowViewInDialog(dieVM, "DieViewTemplate");

            if (dieVM.Saved)
            {
                // Recalculate the die wear, maintenance and cost after editing.
                this.RefreshCalculation();
                this.IsChanged = true;
            }
        }

        /// <summary>
        /// Edits the consumable.
        /// </summary>
        /// <param name="consumable">The consumable to edit.</param>
        private void EditConsumable(Consumable consumable)
        {
            if (consumable == null)
            {
                return;
            }

            var consumableVM = this.compositionContainer.GetExportedValue<ConsumableViewModel>();
            consumableVM.DataSourceManager = this.DataSourceManager;
            consumableVM.SavesToDataSource = false;
            consumableVM.EditMode = ViewModelEditMode.Edit;
            consumableVM.Title = this.IsReadOnly ? LocalizedResources.General_ViewConsumable : LocalizedResources.General_EditConsumable;
            consumableVM.IsInViewerMode = this.IsInViewerMode;
            consumableVM.ParentProject = this.ParentProject;
            consumableVM.ConsumableParent = this.ProcessParent;
            consumableVM.IsReadOnly = this.IsReadOnly;
            consumableVM.IsChild = true;
            consumableVM.ModelClone = this.ModelClone.Consumables.FirstOrDefault(c => c.Guid == consumable.Guid);
            consumableVM.ModelParentClone = this.ModelParentClone;
            consumableVM.MessageToken = this.MessageToken;
            consumableVM.Model = consumable;

            this.windowService.ShowViewInDialog(consumableVM, "ConsumableWindowViewTemplate");

            if (consumableVM.Saved)
            {
                this.IsChanged = true;
                this.RefreshCalculation();
            }
        }

        /// <summary>
        /// Edits the commodity.
        /// </summary>
        /// <param name="commodity">The commodity to edit.</param>
        private void EditCommodity(Commodity commodity)
        {
            if (commodity == null)
            {
                return;
            }

            var commodityVM = this.compositionContainer.GetExportedValue<CommodityViewModel>();
            commodityVM.SavesToDataSource = false;
            commodityVM.EditMode = ViewModelEditMode.Edit;
            commodityVM.Title = this.IsReadOnly ? LocalizedResources.General_ViewCommodity : LocalizedResources.General_EditCommodity;
            commodityVM.IsInViewerMode = this.IsInViewerMode;
            commodityVM.ParentProject = this.ParentProject;
            commodityVM.CommodityParent = this.ProcessParent;
            commodityVM.IsReadOnly = this.IsReadOnly;
            commodityVM.DataSourceManager = this.DataSourceManager;
            commodityVM.IsChild = true;
            commodityVM.ModelClone = this.ModelClone.Commodities.FirstOrDefault(c => c.Guid == commodity.Guid);
            commodityVM.ModelParentClone = this.ModelParentClone;
            commodityVM.MessageToken = this.MessageToken;
            commodityVM.Model = commodity;

            windowService.ShowViewInDialog(commodityVM, "CommodityWindowViewTemplate");

            if (commodityVM.Saved)
            {
                this.IsChanged = true;
                this.RefreshCalculation();
            }
        }

        /// <summary>
        /// Determines whether this instance can execute the Delete command.
        /// </summary>
        /// <param name="parameter">The command parameter.</param>
        /// <returns>
        /// true if the command can be executed; otherwise returns false.
        /// </returns>
        private bool CanExecuteDelete(object parameter)
        {
            if (parameter == null || this.IsReadOnly)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Executes the logic associated with the Delete command.
        /// </summary>
        /// <param name="parameter">The command parameter.</param>
        private void ExecuteDelete(object parameter)
        {
            if (parameter == null)
            {
                return;
            }

            // If the Shift modifier is not pressed then the deletion should be made to the trash bin
            // else should be deleted permanently
            var permanentlyDelete = Keyboard.Modifiers == ModifierKeys.Shift;

            var objectName = EntityUtils.GetEntityName(parameter);

            var message = string.Empty;
            if (permanentlyDelete || EntityUtils.IsMasterData(parameter))
            {
                message = string.Format(LocalizedResources.Question_PermanentlyDeleteItem, objectName);
            }
            else
            {
                message = string.Format(LocalizedResources.Question_Delete_Item, objectName);
            }

            var result = this.windowService.MessageDialogService.Show(message, MessageDialogType.YesNo);
            if (result == MessageDialogResult.Yes)
            {
                var machine = parameter as Machine;
                if (machine != null)
                {
                    this.StepMachines.Remove(this.StepMachines.FirstOrDefault(item => item.Machine.Guid == machine.Guid));
                    if (permanentlyDelete)
                    {
                        this.permanentlyDeletedEntityIds.Add(machine.Guid);
                    }

                    return;
                }

                var die = parameter as Die;
                if (die != null)
                {
                    this.StepDieItems.Remove(this.StepDieItems.FirstOrDefault(item => item.Die.Guid == die.Guid));
                    if (permanentlyDelete)
                    {
                        this.permanentlyDeletedEntityIds.Add(die.Guid);
                    }

                    return;
                }

                var commodity = parameter as Commodity;
                if (commodity != null)
                {
                    this.StepCommodities.Remove(this.StepCommodities.FirstOrDefault(item => item.Commodity.Guid == commodity.Guid));
                    if (permanentlyDelete)
                    {
                        this.permanentlyDeletedEntityIds.Add(commodity.Guid);
                    }

                    return;
                }

                var consumable = parameter as Consumable;
                if (consumable != null)
                {
                    this.StepConsumables.Remove(this.StepConsumables.FirstOrDefault(item => item.Consumable.Guid == consumable.Guid));
                    if (permanentlyDelete)
                    {
                        this.permanentlyDeletedEntityIds.Add(consumable.Guid);
                    }

                    return;
                }
            }
        }

        /// <summary>
        /// Determines whether this instance can execute the Cut command.
        /// </summary>
        /// <param name="parameter">The command parameter.</param>
        /// <returns>
        /// true if the command can be executed; otherwise returns false.
        /// </returns>
        private bool CanExecuteCut(object parameter)
        {
            if (parameter == null)
            {
                return false;
            }

            return !this.IsInViewerMode && !this.IsReadOnly;
        }

        /// <summary>
        /// Executes the logic associated with the Copy command.
        /// </summary>
        /// <param name="parameter">The command parameter.</param>
        private void ExecuteCut(object parameter)
        {
            if (parameter == null)
            {
                return;
            }

            var itemsToMove = new List<ClipboardObject>
            {
                new ClipboardObject() { Entity = parameter, DbSource = this.DataSourceManager.DatabaseId }
            };

            ClipboardManager.Instance.Cut(itemsToMove);
        }

        /// <summary>
        /// Determines whether this instance can execute the Copy command.
        /// </summary>
        /// <param name="parameter">The command parameter.</param>
        /// <returns>
        /// true if the command can be executed; otherwise returns false.
        /// </returns>
        private bool CanExecuteCopy(object parameter)
        {
            if (parameter == null)
            {
                return false;
            }

            return !this.IsInViewerMode;
        }

        /// <summary>
        /// Executes the logic associated with the Copy command.
        /// </summary>
        /// <param name="parameter">The command parameter.</param>
        private void ExecuteCopy(object parameter)
        {
            if (parameter == null)
            {
                return;
            }

            ClipboardManager.Instance.Copy(parameter, this.DataSourceManager.DatabaseId, true);
        }

        /// <summary>
        /// Determines whether this instance can execute the Paste command.
        /// </summary>
        /// <param name="parameter">The command parameter.</param>
        /// <returns>
        /// true if the command can be executed; otherwise returns false.
        /// </returns>
        private bool CanExecutePaste(object parameter)
        {
            if (parameter == null || this.IsReadOnly)
            {
                return false;
            }

            // The parameter is either the type of the objects allowed to be pasted or an object of the type allowed to be pasted.
            var targetType = parameter as Type ?? parameter.GetType();
            var clipboardObjTypes = ClipboardManager.Instance.PeekTypes.ToList();

            if (!IsTargetValidForPaste(targetType))
            {
                return false;
            }

            return clipboardObjTypes.Count > 0 && clipboardObjTypes.All(type => type == targetType)
                   && ClipboardManager.Instance.CheckIfEntityAcceptsClipboardObjects(this.Model.GetType());
        }

        /// <summary>
        /// Executes the logic associated with the Paste command.
        /// </summary>
        /// <param name="parameter">The command parameter.</param>
        private void ExecutePaste(object parameter)
        {
            var pastedObjects = ClipboardManager.Instance.Paste(this.Model, this.DataSourceManager);

            // Display errors appeared during the paste process.
            string errorMessage = ClipboardManager.Instance.GetErrorMessageToDisplay(pastedObjects);
            if (!string.IsNullOrWhiteSpace(errorMessage))
            {
                this.windowService.MessageDialogService.Show(errorMessage, MessageDialogType.Error);
            }

            var lastPastedObject = pastedObjects.LastOrDefault(o => o.Error == null);
            foreach (var pastedObject in pastedObjects.Where(o => o.Error == null))
            {
                var pastedMachine = pastedObject.Entity as Machine;
                if (pastedMachine != null)
                {
                    var machineItem = new MachineDataGridItem() { Machine = pastedMachine };
                    this.StepMachines.Add(machineItem);
                    if (pastedObject == lastPastedObject)
                    {
                        this.SelectedMachineItem.Value = machineItem;
                    }
                }

                var pastedDie = pastedObject.Entity as Die;
                if (pastedDie != null)
                {
                    var dieItem = new DieDataGridItem() { Die = pastedDie };
                    this.StepDieItems.Add(dieItem);
                    if (pastedObject == lastPastedObject)
                    {
                        this.SelectedDieItem.Value = dieItem;
                    }
                }

                var pastedConsumable = pastedObject.Entity as Consumable;
                if (pastedConsumable != null)
                {
                    var consumableItem = new ConsumableDataGridItem(pastedConsumable);
                    this.StepConsumables.Add(consumableItem);
                    if (pastedObject == lastPastedObject)
                    {
                        this.SelectedConsumable.Value = consumableItem;
                    }
                }

                var pastedCommodity = pastedObject.Entity as Commodity;
                if (pastedCommodity != null)
                {
                    var commodityItem = new CommodityDataGridItem() { Commodity = pastedCommodity };
                    this.StepCommodities.Add(commodityItem);
                    if (pastedObject == lastPastedObject)
                    {
                        this.SelectedCommodity.Value = commodityItem;
                    }
                }
            }
        }

        /// <summary>
        /// Determines whether this instance can execute the Drop command.
        /// </summary>
        /// <param name="parameter">The command parameter.</param>
        /// <returns>
        /// true if the command can be executed; otherwise returns false.
        /// </returns>
        private bool CanExecuteDrop(object parameter)
        {
            if (parameter == null || this.IsReadOnly)
            {
                return false;
            }

            // The parameter must be an object of the type allowed to be drop.
            Type targetType = parameter.GetType();

            if (ClipboardManager.Instance.CheckIfEntityAcceptsObject(this.Model.GetType(), targetType))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Executes the logic associated with the Drop command.
        /// </summary>
        /// <param name="parameter">The command parameter.</param>
        private void ExecuteDrop(object parameter)
        {
            if (parameter == null || !this.CanExecuteDrop(parameter))
            {
                return;
            }

            var newMachine = parameter as Machine;
            if (newMachine != null)
            {
                var machine = this.cloneManager.Clone(newMachine, false);
                if (machine != null)
                {
                    using (this.UndoManager.StartBatch(navigateToBatchControls: true))
                    {
                        this.Model.Machines.Add(machine);
                        var machineItem = new MachineDataGridItem() { Machine = machine };
                        this.StepMachines.Add(machineItem);

                        // Navigate to the machine only if the Process calculation accuracy is calculated.
                        if (this.Accuracy.Value == ProcessCalculationAccuracy.Calculated)
                        {
                            this.SelectedTabIndex.Value = (int)SelectedProcessStepTab.MachinesTab;
                            this.SelectedMachineItem.Value = machineItem;
                        }
                    }
                }
            }

            var newDie = parameter as Die;
            if (newDie != null)
            {
                var die = this.cloneManager.Clone(newDie, false);
                if (die != null)
                {
                    using (this.UndoManager.StartBatch(navigateToBatchControls: true))
                    {
                        this.Model.Dies.Add(die);
                        var dieItem = new DieDataGridItem() { Die = die };
                        this.StepDieItems.Add(dieItem);

                        // Navigate to the machine only if the Process calculation accuracy is calculated.
                        if (this.Accuracy.Value == ProcessCalculationAccuracy.Calculated)
                        {
                            this.SelectedTabIndex.Value = (int)SelectedProcessStepTab.DiesTab;
                            this.SelectedDieItem.Value = dieItem;
                        }
                    }
                }
            }

            var newConsumable = parameter as Consumable;
            if (newConsumable != null)
            {
                var consumable = this.cloneManager.Clone(newConsumable, false);
                if (consumable != null)
                {
                    using (this.UndoManager.StartBatch(navigateToBatchControls: true))
                    {
                        this.Model.Consumables.Add(consumable);
                        var consumableItem = new ConsumableDataGridItem(consumable);
                        this.StepConsumables.Add(consumableItem);

                        // Navigate to the machine only if the Process calculation accuracy is calculated.
                        if (this.Accuracy.Value == ProcessCalculationAccuracy.Calculated)
                        {
                            this.SelectedTabIndex.Value = (int)SelectedProcessStepTab.ConsumablesTab;
                            this.SelectedConsumable.Value = consumableItem;
                        }
                    }
                }
            }

            var newCommodity = parameter as Commodity;
            if (newCommodity != null)
            {
                var commodity = this.cloneManager.Clone(newCommodity, false);
                if (commodity != null)
                {
                    using (this.UndoManager.StartBatch(navigateToBatchControls: true))
                    {
                        this.Model.Commodities.Add(commodity);
                        var commodityItem = new CommodityDataGridItem() { Commodity = commodity };
                        this.StepCommodities.Add(commodityItem);

                        // Navigate to the machine only if the Process calculation accuracy is calculated.
                        if (this.Accuracy.Value == ProcessCalculationAccuracy.Calculated)
                        {
                            this.SelectedTabIndex.Value = (int)SelectedProcessStepTab.CommoditiesTab;
                            this.SelectedCommodity.Value = commodityItem;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Determines whether this instance can execute the Import command.
        /// </summary>
        /// <param name="parameter">The command parameter.</param>
        /// <returns>
        /// true if the command can be executed; otherwise returns false.
        /// </returns>
        private bool CanExecuteImport(object parameter)
        {
            if (parameter == null || this.IsReadOnly)
            {
                return false;
            }

            // The parameter is either the type of the object to be imported or an object of the type to be imported.
            Type type = parameter as Type;
            if (type == null)
            {
                type = parameter.GetType();
            }

            if (ClipboardManager.Instance.CheckIfEntityAcceptsObject(this.Model.GetType(), type))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Executes the logic associated with the Import command.
        /// </summary>
        /// <param name="parameter">The command parameter.</param>
        private void ExecuteImport(object parameter)
        {
            if (parameter == null)
            {
                return;
            }

            // The parameter is either the type of the object to be imported or an object of the type to be imported.
            Type typeToImport = parameter as Type;
            if (typeToImport == null)
            {
                typeToImport = parameter.GetType();

                var machineItem = parameter as MachineDataGridItem;
                if (machineItem != null)
                {
                    typeToImport = typeof(Machine);
                }
                else
                {
                    var dieItem = parameter as DieDataGridItem;
                    if (dieItem != null)
                    {
                        typeToImport = typeof(Die);
                    }
                }
            }

            var fileDialogService = this.compositionContainer.GetExportedValue<IFileDialogService>();
            fileDialogService.Filter = UIUtils.GetDialogFilterForExportImport(typeToImport);
            fileDialogService.Multiselect = true;
            if (fileDialogService.ShowOpenFileDialog() != true)
            {
                return;
            }

            var filePaths = new List<string>(fileDialogService.FileNames);

            Action<PleaseWaitService.WorkParams, IEnumerable<ImportedData<object>>> workCompleted = (workParams, importedData) =>
            {
                foreach (var entityData in importedData.Where(imp => imp.Entity != null))
                {
                    if (typeToImport == typeof(Machine))
                    {
                        var importedMachine = entityData.Entity as Machine;
                        if (importedMachine != null)
                        {
                            var machineItem = new MachineDataGridItem() { Machine = importedMachine };
                            this.StepMachines.Add(machineItem);
                            this.SelectedMachineItem.Value = machineItem;
                        }
                    }

                    if (typeToImport == typeof(Die))
                    {
                        var importedDie = entityData.Entity as Die;
                        if (importedDie != null)
                        {
                            var dieItem = new DieDataGridItem() { Die = importedDie };
                            this.StepDieItems.Add(dieItem);
                            this.SelectedDieItem.Value = dieItem;
                        }
                    }

                    if (typeToImport == typeof(Consumable))
                    {
                        var importedConsumable = entityData.Entity as Consumable;
                        if (importedConsumable != null)
                        {
                            var consumableItem = new ConsumableDataGridItem(importedConsumable);
                            this.StepConsumables.Add(consumableItem);
                            this.SelectedConsumable.Value = consumableItem;
                        }
                    }

                    if (typeToImport == typeof(Commodity))
                    {
                        var importedCommodity = entityData.Entity as Commodity;
                        if (importedCommodity != null)
                        {
                            var commodityItem = new CommodityDataGridItem() { Commodity = importedCommodity };
                            this.StepCommodities.Add(commodityItem);
                            this.SelectedCommodity.Value = commodityItem;
                        }
                    }
                }
            };

            var importService = this.compositionContainer.GetExportedValue<IImportService>();
            importService.Import(filePaths, this.Model, this.DataSourceManager, false, workCompleted);
        }

        /// <summary>
        /// Determines whether this instance can execute the Export command.
        /// </summary>
        /// <param name="parameter">The command parameter.</param>
        /// <returns>
        /// true if the command can be executed; otherwise returns false.
        /// </returns>
        private bool CanExecuteExport(object parameter)
        {
            if (parameter == null || this.IsInViewerMode)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Executes the logic associated with the Export command.
        /// </summary>
        /// <param name="parameter">The command parameter.</param>
        private void ExecuteExport(object parameter)
        {
            if (parameter == null)
            {
                return;
            }

            // The object to export is defined by the parameter.
            object objectToExport = parameter;

            var filename = EntityUtils.GetEntityName(objectToExport);
            if (!string.IsNullOrWhiteSpace(filename))
            {
                filename = PathUtils.SanitizeFileName(filename);
            }

            var fileDialogService = this.compositionContainer.GetExportedValue<IFileDialogService>();
            fileDialogService.FileName = filename;
            fileDialogService.Filter = UIUtils.GetDialogFilterForExportImport(objectToExport.GetType());

            string filePath = null;
            bool? result = fileDialogService.ShowSaveFileDialog();
            if (result == true)
            {
                filePath = fileDialogService.FileName;
            }

            if (string.IsNullOrEmpty(filePath))
            {
                return;
            }

            ZPKTool.Business.Export.ExportManager.Instance.Export(objectToExport, this.DataSourceManager, filePath, this.unitsService.Currencies, this.unitsService.BaseCurrency);
            this.windowService.MessageDialogService.Show(LocalizedResources.ExportImport_ExportSuccess, MessageDialogType.Info);
        }

        /// <summary>
        /// Handle the CycleTime calculator.
        /// </summary>
        private void HandleCycleTimeCalculator()
        {
            CycleTimeCalculatorViewModel calculatorVM = this.compositionContainer.GetExportedValue<CycleTimeCalculatorViewModel>();
            calculatorVM.IsReadOnly = this.IsReadOnly;
            calculatorVM.IsInViewerMode = this.IsInViewerMode;
            calculatorVM.DataManager = this.DataSourceManager;
            calculatorVM.TimeUnits = this.IsInViewerMode ? new List<MeasurementUnit>() : new List<MeasurementUnit>(this.MeasurementUnitsAdapter.GetMeasurementUnits(MeasurementUnitType.Time));
            calculatorVM.CrtTimeUnit = this.ProcessTimeUnit.Value;
            calculatorVM.ProcessStep = this.Model;

            this.windowService.ShowViewInDialog(calculatorVM, "CycleTimeCalculatorViewTemplate");

            if (!this.IsInViewerMode && calculatorVM.Saved)
            {
                using (this.UndoManager.StartBatch(navigateToBatchControls: true, undoEachBatch: true))
                {
                    // Save the new values read from calculator only if the changes from calculator were not canceled.
                    MeasurementUnit cycleTimeUnit = calculatorVM.SelectedTimeUnit;
                    this.CycleTimeUnit.Value = cycleTimeUnit;
                    this.ProcessTimeUnit.Value = cycleTimeUnit;
                    this.CycleTime.Value = calculatorVM.CycleTime * cycleTimeUnit.ConversionRate;
                    this.ProcessTime.Value = calculatorVM.CycleTime * cycleTimeUnit.ConversionRate;
                }
            }

            this.CalculatorIcon = this.Model.CycleTimeCalculations.Count > 0 ? Images.CalculatorCheckedIcon : Images.CalculatorIcon;
        }

        #endregion Commands handling

        #region Property change handlers

        /// <summary>
        /// Called when the process step Name property has changed.
        /// </summary>
        private void OnNameChanged()
        {
            this.CheckInformationTabData();
            if (!this.isModelLoading)
            {
                if (this.Name.Value != null)
                {
                    this.ModelClone.Name = this.Name.Value;
                }

                var msg = new ProcessStepChangedMessage(this, this.Model)
                {
                    StepID = this.Model.Guid,
                    Value = this.Name.Value,
                    ChangeType = ProcessStepChangeType.NameUpdated
                };
                this.SendMessage(msg, true);
            }
        }

        /// <summary>
        /// Called when the process step Accuracy property has changed.
        /// </summary>
        private void OnAccuracyChanged()
        {
            using (this.UndoManager.StartBatch(includePreviousItem: true, reverseUndoOrder: true, navigateToBatchControls: true, undoEachBatch: true))
            {
                this.StopRecalculationNotifications();
                this.IsAccuracyCalculated.Value = this.Accuracy.Value == ProcessCalculationAccuracy.Calculated;
                if (this.IsAccuracyCalculated.Value)
                {
                    this.Price.Value = 0;
                    this.Price.AcceptChanges();
                }
                else
                {
                    this.Price.Value = null;
                }

                this.IsExceededShiftCost.Value = this.Accuracy.Value == ProcessCalculationAccuracy.Calculated && this.ExceedShiftCost.Value;
                this.ResumeRecalculationNotifications(true);
            }
        }

        /// <summary>
        /// Called when the process step ExceededShiftCost property has changed.
        /// </summary>
        private void OnExceededShiftCostChanged()
        {
            using (this.UndoManager.StartBatch(includePreviousItem: true, reverseUndoOrder: true, navigateToBatchControls: true))
            {
                this.IsExceededShiftCost.Value = this.Accuracy.Value == ProcessCalculationAccuracy.Calculated && this.ExceedShiftCost.Value;

                if (!this.ExceedShiftCost.Value)
                {
                    this.ExtraShiftsNumber.Value = null;
                    this.ShiftCostExceedRatio.Value = null;
                }
            }
        }

        /// <summary>
        /// Called when IsTransportCostEnabled property has changed.
        /// </summary>
        private void OnIsTransportCostEnabledChanged()
        {
            this.CheckAdditionalSettingsTabsData();
            if (this.TransportCostQty.Value == null)
            {
                this.TransportCostQty.Value = 1;
                this.AcceptChanges();
            }
        }

        /// <summary>
        /// Called when the process step TransportCostType property has changed.
        /// </summary>
        private void OnTransportCostTypeChanged()
        {
            this.IsTransportPerQtySelected.Value = this.TransportCostType.Value == ProcessTransportCostType.PerQty;
            if (!this.IsTransportPerQtySelected.Value)
            {
                if (this.TransportCostQty.Value == null)
                {
                    this.TransportCostQty.Value = 1;
                    this.AcceptChanges();
                }
            }

            this.CheckAdditionalSettingsTabsData();
        }

        /// <summary>
        /// Called when the StepParts collection has changed.
        /// </summary>
        private void OnStepPartsCollectionChanged()
        {
            if (!this.isModelLoading)
            {
                this.PartsTabHasData.Value = this.StepParts.Any(item => item.Amount > 0);
                foreach (var item in StepParts)
                {
                    item.PropertyChanged -= this.OnStepPartChanged;
                    item.PropertyChanged += this.OnStepPartChanged;
                }
            }
        }

        /// <summary>
        /// Handles the step part item PropertyChanged event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void OnStepPartChanged(object sender, PropertyChangedEventArgs e)
        {
            var gridItem = sender as PartsGridItem;
            if (gridItem == null)
            {
                return;
            }

            if (e.PropertyName == ReflectionUtils.GetPropertyName(() => gridItem.Amount))
            {
                if (gridItem.Amount == null)
                {
                    return;
                }

                this.PartsTabHasData.Value = this.StepParts.Any(i => i.Amount > 0);
            }

            if (e.PropertyName == ReflectionUtils.GetPropertyName(() => gridItem.Amount)
                || e.PropertyName == ReflectionUtils.GetPropertyName(() => gridItem.IsExternal)
                || e.PropertyName == ReflectionUtils.GetPropertyName(() => gridItem.HasExternalSGA))
            {
                this.IsChanged = true;
                this.RefreshCalculation();

                var msg = new ProcessStepChangedMessage()
                {
                    StepID = this.Model.Guid,
                    ChangeType = ProcessStepChangeType.StepItemUpdated
                };

                var assyItem = sender as PartsGridAssemblyItem;
                if (assyItem != null)
                {
                    msg.SubEntity = assyItem;
                }
                else
                {
                    var partitem = sender as PartsGridPartItem;
                    if (partitem != null)
                    {
                        msg.SubEntity = partitem;
                    }
                }

                this.SendMessage(msg, true);
            }
        }

        /// <summary>
        /// Handles the step machine item PropertyChanged event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void OnStepMachineChanged(object sender, PropertyChangedEventArgs e)
        {
            var machineItem = sender as MachineDataGridItem;
            if (machineItem != null)
            {
                var machineClone = this.ModelClone.Machines.FirstOrDefault(m => m.Guid == machineItem.Machine.Guid);
                if (machineClone != null)
                {
                    if (e.PropertyName == ReflectionUtils.GetPropertyName(() => machineItem.MachineAmount))
                    {
                        machineClone.Amount = machineItem.MachineAmount;

                        this.IsChanged = true;
                        this.RefreshCalculation();
                    }

                    if (e.PropertyName == ReflectionUtils.GetPropertyName(() => machineItem.Machine.IsProjectSpecific))
                    {
                        machineClone.IsProjectSpecific = machineItem.IsProjectSpecific;

                        this.IsChanged = true;
                        this.RefreshCalculation();
                    }
                }
            }
        }

        /// <summary>
        /// Handle the StepMachines collection changed event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Collections.Specialized.NotifyCollectionChangedEventArgs"/> instance containing the event data.</param>
        private void HandleStepMachinesCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            this.SetStepMachinesIndex();
            if (this.isModelLoading)
            {
                return;
            }

            switch (e.Action)
            {
                case System.Collections.Specialized.NotifyCollectionChangedAction.Add:
                    {
                        var addedMachineItem = e.NewItems[0] as MachineDataGridItem;
                        if (addedMachineItem != null)
                        {
                            var machineClone = this.cloneManager.Clone(addedMachineItem.Machine, true);
                            if (machineClone != null && this.ModelClone != null)
                            {
                                this.ModelClone.Machines.Add(machineClone);
                                addedMachineItem.UndoManager = this.UndoManager;

                                // Send a notification that a process step sub-entity was created.
                                var msg = new ProcessStepChangedMessage(this, this.Model)
                                {
                                    StepID = this.Model.Guid,
                                    SubEntity = machineClone,
                                    ChangeType = ProcessStepChangeType.SubEntityAdded
                                };
                                this.SendMessage(msg, true);
                            }

                            addedMachineItem.PropertyChanged += new PropertyChangedEventHandler(this.OnStepMachineChanged);
                        }

                        break;
                    }

                case System.Collections.Specialized.NotifyCollectionChangedAction.Remove:
                    {
                        var deletedMachineItem = e.OldItems[0] as MachineDataGridItem;
                        if (deletedMachineItem != null)
                        {
                            var deletedMachineClone = this.ModelClone.Machines.FirstOrDefault(c => c.Guid == deletedMachineItem.Machine.Guid);
                            if (deletedMachineClone != null && this.ModelClone != null)
                            {
                                this.ModelClone.Machines.Remove(deletedMachineClone);

                                // Send a notification that a process step sub-entity was deleted.
                                var msg = new ProcessStepChangedMessage(this, this.Model)
                                {
                                    StepID = this.Model.Guid,
                                    SubEntity = deletedMachineClone,
                                    ChangeType = ProcessStepChangeType.SubEntityDeleted
                                };
                                this.SendMessage(msg, true);
                            }

                            deletedMachineItem.PropertyChanged -= new PropertyChangedEventHandler(this.OnStepMachineChanged);
                        }

                        break;
                    }

                case System.Collections.Specialized.NotifyCollectionChangedAction.Reset:
                    {
                        if (this.ModelClone != null)
                        {
                            this.ModelClone.Machines.Clear();

                            foreach (var machineItem in this.StepMachines)
                            {
                                machineItem.PropertyChanged -= new PropertyChangedEventHandler(this.OnStepMachineChanged);
                            }
                        }

                        break;
                    }

                default:
                    break;
            }

            this.MachinesTabHasData.Value = this.StepMachines.Count > 0;
            this.RefreshCalculation();
            this.IsChanged = true;
        }

        /// <summary>
        /// Handle the StepDies collection changed event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Collections.Specialized.NotifyCollectionChangedEventArgs"/> instance containing the event data.</param>
        private void HandleStepDiesCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (this.isModelLoading)
            {
                return;
            }

            switch (e.Action)
            {
                case System.Collections.Specialized.NotifyCollectionChangedAction.Add:
                    {
                        var addedDieItem = e.NewItems[0] as DieDataGridItem;
                        if (addedDieItem != null)
                        {
                            var dieClone = this.cloneManager.Clone(addedDieItem.Die, true);
                            if (dieClone != null && this.ModelClone != null)
                            {
                                this.ModelClone.Dies.Add(dieClone);
                            }
                        }

                        break;
                    }

                case System.Collections.Specialized.NotifyCollectionChangedAction.Remove:
                    {
                        var deletedDieItem = e.OldItems[0] as DieDataGridItem;
                        if (deletedDieItem != null)
                        {
                            var deletedDieClone = this.ModelClone.Dies.FirstOrDefault(c => c.Guid == deletedDieItem.Die.Guid);
                            if (deletedDieClone != null && this.ModelClone != null)
                            {
                                this.ModelClone.Dies.Remove(deletedDieClone);
                            }
                        }

                        break;
                    }

                case System.Collections.Specialized.NotifyCollectionChangedAction.Reset:
                    {
                        if (this.ModelClone != null)
                        {
                            this.ModelClone.Dies.Clear();
                        }

                        break;
                    }

                default:
                    break;
            }

            this.ToolingTabHasData.Value = this.StepDieItems.Count > 0;
            this.RefreshCalculation();
            this.IsChanged = true;
        }

        /// <summary>
        /// Handle the StepConsumables collection changed event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Collections.Specialized.NotifyCollectionChangedEventArgs"/> instance containing the event data.</param>
        private void HandleStepConsumablesCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (this.isModelLoading)
            {
                return;
            }

            switch (e.Action)
            {
                case System.Collections.Specialized.NotifyCollectionChangedAction.Add:
                    {
                        var addedConsumableItem = e.NewItems[0] as ConsumableDataGridItem;
                        if (addedConsumableItem != null)
                        {
                            var consumableClone = this.cloneManager.Clone(addedConsumableItem.Consumable, true);
                            if (consumableClone != null && this.ModelClone != null)
                            {
                                this.ModelClone.Consumables.Add(consumableClone);
                            }
                        }

                        break;
                    }

                case System.Collections.Specialized.NotifyCollectionChangedAction.Remove:
                    {
                        var deletedConsumableItem = e.OldItems[0] as ConsumableDataGridItem;
                        if (deletedConsumableItem != null)
                        {
                            var deletedConsumableClone = this.ModelClone.Consumables.FirstOrDefault(c => c.Guid == deletedConsumableItem.Consumable.Guid);
                            if (deletedConsumableClone != null && this.ModelClone != null)
                            {
                                this.ModelClone.Consumables.Remove(deletedConsumableClone);
                            }
                        }

                        break;
                    }

                case System.Collections.Specialized.NotifyCollectionChangedAction.Reset:
                    {
                        if (this.ModelClone != null)
                        {
                            this.ModelClone.Consumables.Clear();
                        }

                        break;
                    }

                default:
                    break;
            }

            this.ConsumablesTabHasData.Value = this.StepConsumables.Count > 0;
            this.RefreshCalculation();
            this.IsChanged = true;
        }

        /// <summary>
        /// Handle the StepCommodities collection changed event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Collections.Specialized.NotifyCollectionChangedEventArgs"/> instance containing the event data.</param>
        private void HandleStepCommoditiesCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (this.isModelLoading)
            {
                return;
            }

            switch (e.Action)
            {
                case System.Collections.Specialized.NotifyCollectionChangedAction.Add:
                    {
                        var addedCommodityItem = e.NewItems[0] as CommodityDataGridItem;
                        if (addedCommodityItem != null)
                        {
                            var commodityClone = this.cloneManager.Clone(addedCommodityItem.Commodity, true);
                            if (commodityClone != null && this.ModelClone != null)
                            {
                                this.ModelClone.Commodities.Add(commodityClone);
                            }
                        }

                        break;
                    }

                case System.Collections.Specialized.NotifyCollectionChangedAction.Remove:
                    {
                        var deletedCommodityItem = e.OldItems[0] as CommodityDataGridItem;
                        if (deletedCommodityItem != null)
                        {
                            var deletedCommodityClone = this.ModelClone.Commodities.FirstOrDefault(c => c.Guid == deletedCommodityItem.Commodity.Guid);
                            if (deletedCommodityClone != null && this.ModelClone != null)
                            {
                                this.ModelClone.Commodities.Remove(deletedCommodityClone);
                            }
                        }

                        break;
                    }

                case System.Collections.Specialized.NotifyCollectionChangedAction.Reset:
                    {
                        if (this.ModelClone != null)
                        {
                            this.ModelClone.Commodities.Clear();
                        }

                        break;
                    }

                default:
                    break;
            }

            this.CommoditiesTabHasData.Value = this.StepCommodities.Count > 0;
            this.RefreshCalculation();
            this.IsChanged = true;
        }

        /// <summary>
        /// Handle a change made in process step.
        /// </summary>
        /// <param name="msg">The ProcessStepChanged message.</param>
        private void HandleProcessStepChange(ProcessStepChangedMessage msg)
        {
            if (this.IsChild)
            {
                return;
            }

            var stepTarget = msg != null ? msg.Target as ProcessStep : null;
            if (stepTarget != null && this.Model != null && stepTarget.Guid == this.Model.Guid
                && !(msg.Sender is ProcessStepViewModel) &&
                (msg.ChangeType == ProcessStepChangeType.SubEntityAdded || msg.ChangeType == ProcessStepChangeType.SubEntityMoved))
            {
                if (this.Model != null && msg.StepID == this.Model.Guid)
                {
                    bool isEntityMoved = msg.ChangeType == ProcessStepChangeType.SubEntityMoved;

                    var newMachine = msg.SubEntity as Machine;
                    if (newMachine != null)
                    {
                        this.StepMachines.Add(new MachineDataGridItem() { Machine = newMachine });

                        if (isEntityMoved)
                        {
                            this.movedItems.Add(newMachine);
                        }
                    }

                    var newDie = msg.SubEntity as Die;
                    if (newDie != null)
                    {
                        this.StepDieItems.Add(new DieDataGridItem() { Die = newDie });

                        if (isEntityMoved)
                        {
                            this.movedItems.Add(newDie);
                        }
                    }

                    var newConsumable = msg.SubEntity as Consumable;
                    if (newConsumable != null)
                    {
                        this.StepConsumables.Add(new ConsumableDataGridItem(newConsumable));

                        if (isEntityMoved)
                        {
                            this.movedItems.Add(newConsumable);
                        }
                    }

                    var newCommodity = msg.SubEntity as Commodity;
                    if (newCommodity != null)
                    {
                        var commodityItem = new CommodityDataGridItem() { Commodity = newCommodity };
                        this.StepCommodities.Add(commodityItem);

                        foreach (var additionalItem in msg.AdditionalEntities)
                        {
                            var additionalCommodity = additionalItem as Commodity;
                            if (additionalCommodity != null)
                            {
                                this.StepCommodities.Add(new CommodityDataGridItem() { Commodity = additionalCommodity });
                            }
                        }

                        if (isEntityMoved)
                        {
                            this.movedItems.Add(newCommodity);
                            this.movedItems.AddRange(msg.AdditionalEntities);
                            this.SelectedCommodity.Value = commodityItem;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Handles the PropertyChanged event of the Settings
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void Settings_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "DisplayCapacityUtilization")
            {
                this.CanShowCapacityUtilization = UserSettingsManager.Instance.DisplayCapacityUtilization;
            }
        }

        #endregion Property change handlers

        #region Cost handling

        /// <summary>
        /// Update the costs of items representing sub-entities of the Model (machines, Dies, etc.)
        /// </summary>
        /// <param name="result">The calculation result from which to get the new costs.</param>
        private void UpdateSubEntitiesCost(CalculationResult result)
        {
            if (result == null)
            {
                return;
            }

            var stepCost = result.ProcessCost.StepCosts.FirstOrDefault(s => s.StepId == this.Model.Guid);
            if (stepCost != null)
            {
                foreach (var machineItem in this.StepMachines)
                {
                    machineItem.Cost = stepCost.MachineCosts.FirstOrDefault(m => m.MachineId == machineItem.Machine.Guid);
                }

                foreach (var dieItem in this.StepDieItems)
                {
                    dieItem.Cost = stepCost.DieCosts.FirstOrDefault(d => d.DieId == dieItem.Die.Guid);
                }
            }

            foreach (var consumableItem in this.StepConsumables)
            {
                var consumableCost = result.ConsumablesCost.ConsumableCosts.FirstOrDefault(c => c.ConsumableId == consumableItem.Consumable.Guid);
                if (consumableCost != null)
                {
                    consumableItem.Cost = consumableCost.Cost;
                }
            }

            foreach (var commodityItem in this.stepCommodities)
            {
                var commodityCost = result.CommoditiesCost.CommodityCosts.FirstOrDefault(c => c.CommodityId == commodityItem.Commodity.Guid);
                if (commodityCost != null)
                {
                    commodityItem.Cost = commodityCost.Cost;
                }
            }
        }

        /// <summary>
        /// Gets the machine cost calculation parameters.
        /// </summary>
        /// <returns>The machine cost calculation parameters</returns>
        private MachineCostCalculationParameters GetMachineCostCalcParameter()
        {
            var machineCalcParams = new MachineCostCalculationParameters();
            bool isForMasterData = this.Model.IsMasterData;

            CountrySetting countrySettings = GetCountrySettingsOfProcessParent();
            if (countrySettings != null)
            {
                machineCalcParams.CountrySettingsAirCost = countrySettings.AirCost;
                machineCalcParams.CountrySettingsEnergyCost = countrySettings.EnergyCost;
                machineCalcParams.CountrySettingsWaterCost = countrySettings.WaterCost;
                machineCalcParams.CountrySettingsRentalCostProductionArea = countrySettings.ProductionAreaRentalCost;
            }

            if (!isForMasterData && this.ParentProject != null)
            {
                machineCalcParams.ProjectDepreciationPeriod = this.ParentProject.DepreciationPeriod.GetValueOrDefault();
                machineCalcParams.ProjectDepreciationRate = this.ParentProject.DepreciationRate.GetValueOrDefault();
                machineCalcParams.ProjectCreateDate = CostCalculationHelper.GetProjectCreateDate(this.ParentProject);
            }

            machineCalcParams.ProcessStepHoursPerShift = this.HoursPerShift.Value ?? 0;
            machineCalcParams.ProcessStepProductionWeeksPerYear = this.ProductionWeeksPerYear.Value ?? 0;
            machineCalcParams.ProcessStepShiftsPerWeek = this.ShiftsPerWeek.Value ?? 0;

            if (this.ExceedShiftCost.Value)
            {
                machineCalcParams.ProcessStepExtraShiftsPerWeek = this.ExtraShiftsNumber.Value ?? 0;
            }

            return machineCalcParams;
        }

        /// <summary>
        /// Gets the country settings of the entity to which the process belongs.
        /// Don't be lazy to save the its result in a variable for reuse, especially if using it in loops.
        /// </summary>
        /// <returns>The parent's CountrSetting instance.</returns>
        private CountrySetting GetCountrySettingsOfProcessParent()
        {
            CountrySetting countrySettings = null;
            if (this.ProcessParent is Part)
            {
                countrySettings = ((Part)this.ProcessParent).CountrySettings;
            }
            else if (this.ProcessParent is Assembly)
            {
                countrySettings = ((Assembly)this.ProcessParent).CountrySettings;
            }
            else
            {
                throw new InvalidOperationException("This parent entity is not handled: " + this.ProcessParent.GetType().FullName);
            }

            return countrySettings;
        }

        /// <summary>
        /// Called after one or more model properties value changed in order to refresh the calculations.
        /// </summary>
        protected override void RefreshCalculation()
        {
            this.SendMessage(new NotificationMessage(Notification.ProcessStepChanged), true);
            this.RefreshCalculations(this.ModelParentClone);
        }

        /// <summary>
        /// Calculates the parent and sends a message with the result.
        /// </summary>
        /// <param name="entityParent">The entity parent.</param>
        private void RefreshCalculations(object entityParent)
        {
            if (entityParent == null)
            {
                return;
            }

            Part part = entityParent as Part;
            if (part != null)
            {
                PartCostCalculationParameters calculationParams = null;
                if (this.ParentProject != null)
                {
                    calculationParams = CostCalculationHelper.CreatePartParamsFromProject(this.ParentProject);
                }
                else if (this.IsInViewerMode)
                {
                    calculationParams = this.modelBrowserHelperService.GetPartCostCalculationParameters();
                }

                if (calculationParams != null)
                {
                    var calculator = CostCalculatorFactory.GetCalculator(part.CalculationVariant);
                    this.ParentCost = calculator.CalculatePartCost(part, calculationParams);
                    this.SendMessage(new CurrentComponentCostChangedMessage(this.ParentCost), true);
                    this.UpdateSubEntitiesCost(this.ParentCost);
                }
            }
            else
            {
                Assembly assy = entityParent as Assembly;
                if (assy != null)
                {
                    AssemblyCostCalculationParameters calculationParams = null;
                    if (this.ParentProject != null)
                    {
                        calculationParams = CostCalculationHelper.CreateAssemblyParamsFromProject(this.ParentProject);
                    }
                    else if (this.IsInViewerMode)
                    {
                        calculationParams = this.modelBrowserHelperService.GetAssemblyCostCalculationParameters();
                    }

                    if (calculationParams != null)
                    {
                        var calculator = CostCalculatorFactory.GetCalculator(assy.CalculationVariant);
                        this.ParentCost = calculator.CalculateAssemblyCost(assy, calculationParams);
                        this.SendMessage(new CurrentComponentCostChangedMessage(this.ParentCost), true);
                        this.UpdateSubEntitiesCost(this.ParentCost);
                    }
                }
            }
        }

        #endregion Cost handling

        #region Undo handling

        /// <summary>
        /// Handle the undo performed on Step collections. (Machines, Dies, Consumables and Commodities)
        /// </summary>
        /// <param name="undoItem">The undoable item executed.</param>
        private void HandleUndoOnStepCollections(UndoableItem undoItem)
        {
            if (undoItem == null)
            {
                return;
            }

            switch (undoItem.ActionType)
            {
                case UndoActionType.Insert:
                    {
                        foreach (var collectionItem in undoItem.Values)
                        {
                            var machineItem = collectionItem as MachineDataGridItem;
                            if (machineItem != null && machineItem.Machine != null)
                            {
                                if (machineItem.Machine.IsDeleted)
                                {
                                    this.DataSourceManager.TrashBinRepository.RestoreFromTrashBin(machineItem.Machine);
                                }

                                this.SelectedMachineItem.Value = machineItem;
                                if (this.permanentlyDeletedEntityIds.Contains(machineItem.Machine.Guid))
                                {
                                    this.permanentlyDeletedEntityIds.Remove(machineItem.Machine.Guid);
                                }
                            }

                            var dieItem = collectionItem as DieDataGridItem;
                            if (dieItem != null && dieItem.Die != null)
                            {
                                if (dieItem.Die.IsDeleted)
                                {
                                    this.DataSourceManager.TrashBinRepository.RestoreFromTrashBin(dieItem.Die);
                                }

                                this.SelectedDieItem.Value = dieItem;
                                if (this.permanentlyDeletedEntityIds.Contains(dieItem.Die.Guid))
                                {
                                    this.permanentlyDeletedEntityIds.Remove(dieItem.Die.Guid);
                                }
                            }

                            var consumableItem = collectionItem as ConsumableDataGridItem;
                            if (consumableItem != null && consumableItem.Consumable != null)
                            {
                                if (consumableItem.Consumable.IsDeleted)
                                {
                                    this.DataSourceManager.TrashBinRepository.RestoreFromTrashBin(consumableItem.Consumable);
                                }

                                this.SelectedConsumable.Value = consumableItem;
                                if (this.permanentlyDeletedEntityIds.Contains(consumableItem.Consumable.Guid))
                                {
                                    this.permanentlyDeletedEntityIds.Remove(consumableItem.Consumable.Guid);
                                }
                            }

                            var commodityItem = collectionItem as CommodityDataGridItem;
                            if (commodityItem != null && commodityItem.Commodity != null)
                            {
                                if (commodityItem.Commodity.IsDeleted)
                                {
                                    this.DataSourceManager.TrashBinRepository.RestoreFromTrashBin(commodityItem.Commodity);
                                }

                                this.SelectedCommodity.Value = commodityItem;
                                if (this.permanentlyDeletedEntityIds.Contains(commodityItem.Commodity.Guid))
                                {
                                    this.permanentlyDeletedEntityIds.Remove(commodityItem.Commodity.Guid);
                                }
                            }
                        }

                        break;
                    }

                case UndoActionType.Delete:
                    {
                        foreach (var collectionItem in undoItem.Values)
                        {
                            object deletedEntity = null;
                            var machineItem = collectionItem as MachineDataGridItem;
                            if (machineItem != null && machineItem.Machine != null && this.Model.Machines.Contains(machineItem.Machine))
                            {
                                deletedEntity = machineItem.Machine;
                            }

                            var dieItem = collectionItem as DieDataGridItem;
                            if (dieItem != null && dieItem.Die != null && this.Model.Dies.Contains(dieItem.Die))
                            {
                                deletedEntity = dieItem.Die;
                            }

                            var consumableItem = collectionItem as ConsumableDataGridItem;
                            if (consumableItem != null && consumableItem.Consumable != null && this.Model.Consumables.Contains(consumableItem.Consumable))
                            {
                                deletedEntity = consumableItem.Consumable;
                            }

                            var commodityItem = collectionItem as CommodityDataGridItem;
                            if (commodityItem != null && commodityItem.Commodity != null && this.Model.Commodities.Contains(commodityItem.Commodity))
                            {
                                deletedEntity = commodityItem.Commodity;
                            }

                            if (deletedEntity != null)
                            {
                                TrashManager trashManager = new TrashManager(this.DataSourceManager);
                                trashManager.DeleteToTrashBin(deletedEntity);
                            }

                            var msg = new ProcessStepChangedMessage(this, this.Model)
                            {
                                StepID = this.Model.Guid,
                                SubEntity = deletedEntity,
                                ChangeType = ProcessStepChangeType.SubEntityDeleted
                            };
                            this.SendMessage(msg, true);
                        }

                        break;
                    }

                default:
                    break;
            }
        }

        #endregion Undo handling

        #region Save/Cancel

        /// <summary>
        /// Saves all changed back into the model and resets the changed status. This method is executed by the SaveToModelCommand command.
        /// </summary>
        protected override void SaveToModel()
        {
            // HACK: (#2410) This call must be performed before base.SaveToModel() because it used the TrashBinManager for deletions, which in turn loads
            // the object to be deleted from database, which causes the parent step (this step) to be reloaded (overwritten) from db. This workaround will no 
            // longer apply when the merge option PreserveChanges is removed from all Get entity methods.
            this.SyncSubEntitiesWithModel();

            base.SaveToModel();
            this.SaveClassificationToModel();

            this.TypeSelectorViewModel.IsChanged = false;
        }

        /// <summary>
        /// Saves the step classification(s) from the view-model into the Model.
        /// </summary>
        private void SaveClassificationToModel()
        {
            int selectedLevelsCount = this.TypeSelectorViewModel.ClassificationLevels.Count;
            if (selectedLevelsCount > 0)
            {
                this.Model.Type = this.TypeSelectorViewModel.ClassificationLevels[0] as ProcessStepsClassification;

                if (selectedLevelsCount > 1)
                {
                    this.Model.SubType = this.TypeSelectorViewModel.ClassificationLevels[1] as ProcessStepsClassification;
                }
            }
            else
            {
                this.Model.Type = null;
                this.Model.SubType = null;
            }
        }

        /// <summary>
        /// Syncs the view-model's and Model's sub-entities collections.
        /// Basically, pushes the newly added entities into the Model and removes from it the deleted entities. For some entities that can be modified in a data grid
        /// (like Machine - amount, or Part/Assembly amounts, for example) it also pushes those changes into the entities.
        /// <para />
        /// The sub-entities synced are Machines, Dies, Consumables, Commodities, Assembly and Part Amounts (ProcessStepAssemblyAmount, ProcessStepPartAmount),
        /// Cycle Time Calculations.
        /// </summary>
        private void SyncSubEntitiesWithModel()
        {
            TrashManager trashManager = new TrashManager(this.DataSourceManager);

            // Sync the machines
            var deletedMachines = this.Model.Machines.Where(mach => !mach.IsDeleted && !this.StepMachines.Any(item => item.Machine == mach)).ToList();
            foreach (var machine in deletedMachines)
            {
                this.DeleteObject(trashManager, machine);
            }

            foreach (var machineItem in this.StepMachines)
            {
                machineItem.SaveDataToModel();
            }

            var addedMachineItems = this.StepMachines.Where(item => !this.Model.Machines.Contains(item.Machine));
            foreach (var machineItem in addedMachineItems)
            {
                this.Model.Machines.Add(machineItem.Machine);
            }

            // Sync the dies
            var deletedDies = this.Model.Dies.Where(die => !die.IsDeleted && !this.StepDieItems.Any(item => item.Die == die)).ToList();
            foreach (var die in deletedDies)
            {
                this.DeleteObject(trashManager, die);
            }

            var addedDieItems = this.StepDieItems.Where(item => !this.Model.Dies.Contains(item.Die));
            foreach (var dieItem in addedDieItems)
            {
                this.Model.Dies.Add(dieItem.Die);
            }

            // Sync the consumables
            var deletedConsumables = this.Model.Consumables.Where(consumable => !consumable.IsDeleted && !this.StepConsumables.Any(item => item.Consumable == consumable)).ToList();
            foreach (var consumable in deletedConsumables)
            {
                this.DeleteObject(trashManager, consumable);
            }

            var addedConsumableItems = this.StepConsumables.Where(item => !this.Model.Consumables.Contains(item.Consumable));
            foreach (var consumableItem in addedConsumableItems)
            {
                this.Model.Consumables.Add(consumableItem.Consumable);
            }

            // Sync the commodities
            var deletedCommodities = this.Model.Commodities.Where(commodity => !commodity.IsDeleted && !this.StepCommodities.Any(item => item.Commodity == commodity)).ToList();
            foreach (var commodity in deletedCommodities)
            {
                this.DeleteObject(trashManager, commodity);
            }

            var addedCommodityItems = this.StepCommodities.Where(item => !this.Model.Commodities.Contains(item.Commodity));
            foreach (var commodityItem in addedCommodityItems)
            {
                this.Model.Commodities.Add(commodityItem.Commodity);
            }

            // Sync the Part and Assembly Amounts. This is done only for the assembling process.
            var parentAssembly = this.ProcessParent as Assembly;
            if (parentAssembly != null)
            {
                // Sync the Part Amounts and Part properties changed in the Parts data grid
                foreach (var partAmountItem in this.StepParts.OfType<PartsGridPartItem>())
                {
                    // The part to which the amount is linked.
                    var part = parentAssembly.Parts.First(p => p.Guid == partAmountItem.Part.Guid);

                    // partAmountItem.PartAmount is null before the 1st edit is made to it on a newly created step
                    var partAmount =
                        this.Model.PartAmounts.FirstOrDefault(a => partAmountItem.PartAmount != null && a.Part.Guid == partAmountItem.PartAmount.FindPartId());
                    if (partAmount == null)
                    {
                        // Create the Amount instance
                        partAmount = new ProcessStepPartAmount();
                        partAmount.Part = part;
                        this.Model.PartAmounts.Add(partAmount);

                        partAmount.SetIsMasterData(this.Model.IsMasterData);
                        partAmount.SetOwner(this.Model.Owner);
                    }

                    partAmount.Amount = partAmountItem.Amount.Value;

                    // Update the Part.
                    part.IsExternal = partAmountItem.IsExternal;
                    part.ExternalSGA = partAmountItem.HasExternalSGA;

                    partAmountItem.RefreshInitialValues();
                }

                // Sync the Assembly Amounts and Assembly properties changed in the Parts data grid
                foreach (var assyAmountItem in this.StepParts.OfType<PartsGridAssemblyItem>())
                {
                    // The assembly to which the amount is linked.
                    var assy = parentAssembly.Subassemblies.First(p => p.Guid == assyAmountItem.Assembly.Guid);

                    // assyAmountItem.AssemblyAmount is null before the 1st edit is made to it on a newly created step
                    var assyAmount =
                        this.Model.AssemblyAmounts.FirstOrDefault(a => assyAmountItem.AssemblyAmount != null && a.Assembly.Guid == assyAmountItem.AssemblyAmount.FindAssemblyId());
                    if (assyAmount == null)
                    {
                        // Create the amount instance.
                        assyAmount = new ProcessStepAssemblyAmount();
                        assyAmount.Assembly = assy;
                        this.Model.AssemblyAmounts.Add(assyAmount);

                        assyAmount.SetIsMasterData(this.Model.IsMasterData);
                        assyAmount.SetOwner(this.Model.Owner);
                    }

                    assyAmount.Amount = assyAmountItem.Amount.Value;

                    // Update the Assembly
                    assy.IsExternal = assyAmountItem.IsExternal;
                    assy.ExternalSGA = assyAmountItem.HasExternalSGA;

                    assyAmountItem.RefreshInitialValues();
                }
            }

            // Sync Cycle Time Calculations
        }

        /// <summary>
        /// Performs the save operation. Executed by the SaveCommand.
        /// </summary>
        protected override void Save()
        {
            this.CheckModelAndDataSource();

            // Determine all modified part/assembly amounts and create messages that will be sent after the SaveChanges call below.
            // This must happen before the data is saved into the model because that resets the state of the part amounts to not changed.
            var modifiedPartsMessages = this.CreateMessagesForModifiedPartAmounts();

            // Save all changes back into the Model objects. The validity check of this operation is performed by the CanSave method.
            this.SaveToModel();

            // Update the process step.
            this.DataSourceManager.ProcessStepRepository.Save(this.Model);
            this.DataSourceManager.SaveChanges();

            // Save Media.
            this.MediaViewModel.SaveCommand.Execute(null);

            if (this.movedItems.Count == 1)
            {
                var firstMovedItem = this.movedItems.FirstOrDefault();

                // Send a message to notify the parent of moved items.
                var entityMovedMsg =
                    new EntityChangedMessage(
                        EntityUtils.IsMasterData(firstMovedItem)
                            ? Notification.MasterDataEntityChanged
                            : Notification.MyProjectsEntityChanged);

                entityMovedMsg.Entity = firstMovedItem;
                entityMovedMsg.Parent = this.Model;
                entityMovedMsg.ChangeType = EntityChangeType.EntityMoved;
                entityMovedMsg.SelectEntity = false;

                this.messenger.Send(entityMovedMsg);
            }
            else if (this.movedItems.Count > 1)
            {
                var messages = new Collection<EntityChangedMessage>();
                foreach (var movedItem in movedItems)
                {
                    // Send a message to notify the parent of moved items.
                    var entityMovedMsg =
                        new EntityChangedMessage(
                            EntityUtils.IsMasterData(movedItem)
                                ? Notification.MasterDataEntityChanged
                                : Notification.MyProjectsEntityChanged);

                    entityMovedMsg.Entity = movedItem;
                    entityMovedMsg.Parent = this.Model;
                    entityMovedMsg.ChangeType = EntityChangeType.EntityMoved;
                    entityMovedMsg.SelectEntity = false;
                    messages.Add(entityMovedMsg);
                }

                var msg = new EntitiesChangedMessage(messages);
                this.messenger.Send(msg);
            }

            var notification = this.Model.IsMasterData ? Notification.MasterDataEntityChanged : Notification.MyProjectsEntityChanged;
            var entityUpdatedMsg = new EntityChangedMessage(notification)
            {
                Entity = this.Model,
                Parent = null,
                ChangeType = EntityChangeType.EntityUpdated
            };

            // Send a global message for save action
            this.SendMessage(entityUpdatedMsg, false);

            // Send the messages for the modified parts/assemblies
            foreach (var message in modifiedPartsMessages)
            {
                this.SendMessage(message, false);
            }

            // Close the view-model when is displayed in a window.
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Creates the messages to be sent for each part/assembly amount that was modified by the user.
        /// </summary>
        /// <returns>A list of messages.</returns>
        private List<EntityChangedMessage> CreateMessagesForModifiedPartAmounts()
        {
            var messages = new List<EntityChangedMessage>();
            foreach (var dirtyPartAmountItem in this.StepParts.Where(item => item.IsDirty))
            {
                var assyItem = dirtyPartAmountItem as PartsGridAssemblyItem;
                if (assyItem != null)
                {
                    var assembly = assyItem.Assembly;

                    var notification = assembly.IsMasterData ? Notification.MasterDataEntityChanged : Notification.MyProjectsEntityChanged;
                    var msg = new EntityChangedMessage(notification)
                    {
                        Entity = assembly,
                        Parent = (object)assembly.Project ?? (object)assembly.ParentAssembly,
                        ChangeType = EntityChangeType.EntityUpdated
                    };
                    messages.Add(msg);
                }
                else
                {
                    var partItem = dirtyPartAmountItem as PartsGridPartItem;
                    if (partItem != null)
                    {
                        var part = partItem.Part;
                        object parent = null;
                        if (part.Project != null)
                        {
                            parent = part.Project;
                        }
                        else if (part.Assembly != null)
                        {
                            parent = part.Assembly;
                        }
                        else if (part is RawPart)
                        {
                            parent = part.ParentOfRawPart;
                        }

                        string notification = part.IsMasterData ? Notification.MasterDataEntityChanged : Notification.MyProjectsEntityChanged;
                        var msg = new EntityChangedMessage(notification)
                        {
                            Entity = part,
                            Parent = parent,
                            ChangeType = EntityChangeType.EntityUpdated
                        };
                        messages.Add(msg);
                    }
                }
            }

            return messages;
        }

        /// <summary>
        /// Determines whether the Cancel operation can be performed. Executed by the CancelCommand.
        /// </summary>
        /// <returns>
        /// true if the changes can be canceled, false otherwise.
        /// </returns>
        protected override bool CanCancel()
        {
            return base.CanCancel() && this.CancelNestedViewModels.CanExecute(null);
        }

        /// <summary>
        /// Cancels all changes. Executed by the CancelCommand.
        /// </summary>
        protected override void Cancel()
        {
            if (this.IsChanged)
            {
                if (this.ShowCancelMessageFlag)
                {
                    var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_CancelChanges, MessageDialogType.YesNo);
                    if (result != MessageDialogResult.Yes)
                    {
                        // Don't cancel the changes and also don't close the view-model.
                        return;
                    }
                }

                // Refresh the ModelClone if the view-model is not loaded from Process screen.
                if (!this.IsChild)
                {
                    this.CloneManager.Clone(this);
                }

                using (this.UndoManager.StartBatch())
                {
                    // Cancel the changes that were made to sub-entities through their own edit screens. Those changes were directly saved into the sub-entities.
                    // TODO: this cancels all changes in the DataSourceManager instance. When a Revert/Undo changes method is implemented in ProcessStepRepository
                    // that reverts only the step graph (its owned entities and nothing else) it should replace the general revert changes here.
                    this.DataSourceManager.RevertChanges();

                    // Cancel all changes
                    base.Cancel();
                    this.CancelNestedViewModels.Execute(null);
                }

                this.UndoManager.Reset();
            }

            // Close the view-model when is displayed in a window.
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Called before unloading the view from its parent or closing it. Returning false will cancel the view's unloading or closing.
        /// </summary>
        /// <returns>
        /// True if the unloading process should continue and false if it should be canceled.
        /// </returns>
        public override bool OnUnloading()
        {
            // Do nothing if the view-model is read-only, in viewer mode, it was not changed or the parent of the process was deleted.
            var parent = this.ProcessParent as ITrashable;
            if (this.IsReadOnly || this.IsInViewerMode || !this.IsChanged || (parent != null && parent.IsDeleted))
            {
                return true;
            }

            // Do nothing if the process step was deleted.
            var modelExists = this.DataSourceManager.ProcessStepRepository.CheckIfExists(this.Model.Guid);
            if (!modelExists)
            {
                return true;
            }

            if (this.EditMode == ViewModelEditMode.Create)
            {
                // Ask the user to confirm quitting
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_Quit, MessageDialogType.YesNo);
                if (result != MessageDialogResult.Yes)
                {
                    // The user chose to stay on the screen; return false to stop the screen unloading.
                    return false;
                }
                else
                {
                    this.IsChanged = false;
                }
            }
            else if (this.EditMode == ViewModelEditMode.Edit)
            {
                // Ask the user if he wants to save
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_UnsavedDataOnQuit, MessageDialogType.YesNoCancel);
                if (result == MessageDialogResult.Yes)
                {
                    // The user whishes to save but it is not possible because the input is not valid. Return false to stop the view-model from unloading.
                    if (!this.CanSave())
                    {
                        return false;
                    }

                    this.Save();
                }
                else if (result == MessageDialogResult.No)
                {
                    // The user does not want to save.                    
                    if (!this.CanCancel())
                    {
                        return false;
                    }

                    this.ShowCancelMessageFlag = false;
                    this.Cancel();
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        #endregion Save/Cancel

        #region Tabs data

        /// <summary>
        /// Check if process step tabs contains data.
        /// </summary>
        private void CheckTabsForData()
        {
            this.CheckInformationTabData();
            this.CheckLabourSettingsTabData();
            this.CheckAdditionalSettingsTabsData();
            this.MachinesTabHasData.Value = this.StepMachines.Count > 0;
            this.ToolingTabHasData.Value = this.StepDieItems.Count > 0;
            this.ConsumablesTabHasData.Value = this.StepConsumables.Count > 0;
            this.CommoditiesTabHasData.Value = this.StepCommodities.Count > 0;
            this.PartsTabHasData.Value = this.StepParts.Any(item => item.Amount > 0);
            foreach (var item in StepParts)
            {
                item.PropertyChanged -= this.OnStepPartChanged;
                item.PropertyChanged += this.OnStepPartChanged;
            }
        }

        /// <summary>
        /// Check if the Information tab contains data or not.
        /// </summary>
        private void CheckInformationTabData()
        {
            if (!this.isModelLoading)
            {
                this.InformationTabHasData.Value = !string.IsNullOrWhiteSpace(this.Name.Value)
                    || !string.IsNullOrWhiteSpace(this.Description.Value)
                    || (this.Accuracy.Value == ProcessCalculationAccuracy.Calculated &&
                       ((this.ProcessTime.Value != null && this.ProcessTime.Value != 0)
                        || (this.CycleTime.Value != null && this.CycleTime.Value != 0) || (this.PartsPerCycle.Value != null && this.PartsPerCycle.Value != 0)
                        || (this.ScrapAmount.Value != null && this.ScrapAmount.Value != 0) || (this.SetupsPerBatch.Value != null && this.SetupsPerBatch.Value != 0)
                        || (this.MaxDownTime.Value != null && this.MaxDownTime.Value != 0) || (this.SetupTime.Value != null && this.SetupTime.Value != 0)
                        || (this.BatchSize.Value != null && this.BatchSize.Value != 0) || (this.ManufacturingOverhead.Value != null && this.ManufacturingOverhead.Value != 0)
                        || (this.ShiftsPerWeek.Value != null && this.ShiftsPerWeek.Value != 0) || (this.HoursPerShift.Value != null && this.HoursPerShift.Value != 0)
                        || (this.ProductionDaysPerWeek.Value != null && this.ProductionDaysPerWeek.Value != 0) || (this.ProductionWeeksPerYear.Value != null && this.ProductionWeeksPerYear.Value != 0)
                        || (this.ExtraShiftsNumber.Value != null && this.ProductionWeeksPerYear.Value != 0) || (this.ShiftCostExceedRatio.Value != null && this.ShiftCostExceedRatio.Value != 0)))
                    || (this.Accuracy.Value == ProcessCalculationAccuracy.Estimated && (this.Price.Value != 0 && this.Price.Value != null));

                if (this.MediaViewModel != null && this.MediaViewModel.MediaHandler != null)
                {
                    this.InformationTabHasData.Value = this.InformationTabHasData.Value || this.MediaViewModel.MediaHandler.MediaSource.AllSources.Count > 0;
                }

                if (this.TypeSelectorViewModel != null && this.TypeSelectorViewModel.ClassificationLevels != null)
                {
                    this.InformationTabHasData.Value = this.InformationTabHasData.Value || this.TypeSelectorViewModel.ClassificationLevels.Count > 0;
                }
            }
        }

        /// <summary>
        /// Check if the Labour Settings tab contains data or not.
        /// </summary>
        private void CheckLabourSettingsTabData()
        {
            if (!this.isModelLoading)
            {
                this.LabourSettingsTabHasData.Value = (this.ProductionUnskilledLabour.Value != null && this.ProductionUnskilledLabour.Value != 0)
                    || (this.ProductionSkilledLabour.Value != null && this.ProductionSkilledLabour.Value != 0)
                    || (this.ProductionForeman.Value != null && this.ProductionForeman.Value != 0) || (this.ProductionEngineers.Value != null && this.ProductionEngineers.Value != 0)
                    || (this.ProductionTechnicians.Value != null && this.ProductionTechnicians.Value != 0) || (this.SetupUnskilledLabour.Value != null && this.SetupUnskilledLabour.Value != 0)
                    || (this.SetupSkilledLabour.Value != null && this.SetupSkilledLabour.Value != 0) || (this.SetupForeman.Value != null && this.SetupForeman.Value != 0)
                    || (this.SetupEngineers.Value != null && this.SetupEngineers.Value != 0) || (this.SetupTechnicians.Value != null && this.SetupTechnicians.Value != 0);
            }
        }

        /// <summary>
        /// Checl if the Additional Settings tab contains data or not.
        /// </summary>
        private void CheckAdditionalSettingsTabsData()
        {
            if (!this.isModelLoading)
            {
                this.AdditionalSettingsTabHasData.Value = (this.IsTransportCostEnabled.Value && this.TransportCost.Value != null && this.TransportCost.Value != 0)
                    || (this.IsTransportCostEnabled.Value && this.IsTransportPerQtySelected.Value && this.TransportCostQty.Value != null && this.TransportCostQty.Value != 0);
            }
        }

        #endregion Tabs data

        #region Other private methods

        /// <summary>
        /// Determine if the target is valid for pasting the current items from clipboard into it.
        /// </summary>
        /// <param name="targetType">
        /// The target Type for Paste command.
        /// </param>
        /// <returns>
        /// True if the target is valid / False otherwise.
        /// </returns>
        private bool IsTargetValidForPaste(Type targetType)
        {
            if (ClipboardManager.Instance.Operation == ClipboardOperation.Cut)
            {
                // Verify if the target process step does not contain already the item to paste.
                foreach (var clipboardObject in ClipboardManager.Instance.PeekClipboardObjects)
                {
                    var machineToPaste = clipboardObject as Machine;
                    if (targetType == typeof(Machine) && machineToPaste != null)
                    {
                        var machine = this.StepMachines.FirstOrDefault(m => m.Machine.Guid == machineToPaste.Guid);
                        if (machine != null)
                        {
                            return false;
                        }
                    }

                    var dieToPaste = clipboardObject as Die;
                    if (targetType == typeof(Die) && dieToPaste != null)
                    {
                        var die = this.StepDieItems.FirstOrDefault(d => d.Die.Guid == dieToPaste.Guid);
                        if (die != null)
                        {
                            return false;
                        }
                    }

                    var consumableToPaste = clipboardObject as Consumable;
                    if (targetType == typeof(Consumable) && consumableToPaste != null)
                    {
                        var consumable = this.StepConsumables.FirstOrDefault(m => m.Consumable.Guid == consumableToPaste.Guid);
                        if (consumable != null)
                        {
                            return false;
                        }
                    }

                    var commodityToPaste = clipboardObject as Commodity;
                    if (targetType == typeof(Commodity) && commodityToPaste != null)
                    {
                        var commodity = this.StepCommodities.FirstOrDefault(c => c.Commodity.Guid == commodityToPaste.Guid);
                        if (commodity != null)
                        {
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Set the step machines index.
        /// </summary>
        private void SetStepMachinesIndex()
        {
            foreach (var machineItem in this.StepMachines)
            {
                int index = this.StepMachines.IndexOf(machineItem) + 1;

                if (machineItem.OrderNumber != index)
                {
                    machineItem.OrderNumber = index;
                }

                if (machineItem.Machine.Index != index)
                {
                    machineItem.Machine.Index = index;
                }
            }
        }

        /// <summary>
        /// Gets the overhead settings of the entity to which the process belongs.
        /// </summary>
        /// <returns>The parent's overhead settings.</returns>
        private OverheadSetting GetOverheadSettingsOfProcessParent()
        {
            OverheadSetting ohsettings = null;

            Part part = this.ProcessParent as Part;
            if (part != null)
            {
                ohsettings = part.OverheadSettings;
            }
            else
            {
                Assembly assy = this.ProcessParent as Assembly;
                if (assy != null)
                {
                    ohsettings = assy.OverheadSettings;
                }
                else
                {
                    throw new InvalidOperationException("This parent entity is not handled: " + this.ProcessParent.GetType().FullName);
                }
            }

            return ohsettings;
        }

        /// <summary>
        /// Sends a message with or without a token
        /// </summary>
        /// <typeparam name="TMessage">The message type</typeparam>
        /// <param name="message">The message to send</param>
        /// <param name="sendWithToken">A value indicating whether to send the message with a token or send it globally without one</param>
        private void SendMessage<TMessage>(TMessage message, bool sendWithToken)
        {
            if (sendWithToken)
            {
                if (this.MessageToken == null)
                {
                    this.messenger.Send(message, this.IsInViewerMode ? GlobalMessengerTokens.ModelBrowserTargetToken : GlobalMessengerTokens.MainViewTargetToken);
                }
                else
                {
                    this.messenger.Send(message, this.MessageToken);
                }
            }
            else
            {
                this.messenger.Send(message);
            }
        }

        /// <summary>
        /// Delete the provided object from the model.
        /// </summary>
        /// <param name="trashManager">The trash manager instance used for deletion.</param>
        /// <param name="entity">The object to delete</param>
        private void DeleteObject(TrashManager trashManager, IIdentifiable entity)
        {
            if (this.permanentlyDeletedEntityIds.Contains(entity.Guid) || EntityUtils.IsMasterData(entity))
            {
                this.permanentlyDeletedEntityIds.Remove(entity.Guid);
                trashManager.DeleteByStoreProcedure(entity);
            }
            else
            {
                trashManager.DeleteToTrashBin(entity);
            }
        }

        #endregion Other private methods
    }
}
