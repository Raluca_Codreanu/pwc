﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using ZPKTool.Business.MachiningCalculator;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the MachiningCalculatorMilling view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class MachiningCalculatorMillingViewModel : ViewModel<CycleTimeCalculationItemViewModel>
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The machining calculator.
        /// </summary>
        private MachiningCalculator machiningCalculator;

        /// <summary>
        /// The selected milling material.
        /// </summary>
        private MillingMaterialToBeMachined selectedMaterial;

        /// <summary>
        /// The process description.
        /// </summary>
        private string processDescription;

        /// <summary>
        /// The selected milling tool type.
        /// </summary>
        private MillingToolType selectedMillingToolType;

        /// <summary>
        /// The machine feed speed.
        /// </summary>
        private decimal? feedSpeed;

        /// <summary>
        /// The selected milling type.
        /// </summary>
        private MillingType selectedMillingType;

        /// <summary>
        /// The cut depth.
        /// </summary>
        private decimal? cutDepth;

        /// <summary>
        /// The number of teeth.
        /// </summary>
        private decimal? teethNumber;

        /// <summary>
        /// The start stop time for machine feed.
        /// </summary>
        private decimal? startStopTime;

        /// <summary>
        /// The machine feed length.
        /// </summary>
        private decimal? feedLength;

        /// <summary>
        /// The milling tool diameter.
        /// </summary>
        private decimal? millingToolDiameter;

        /// <summary>
        /// The milling tool width.
        /// </summary>
        private decimal? millingToolWidth;

        /// <summary>
        /// The milling length.
        /// </summary>
        private decimal? millingLength;

        /// <summary>
        /// The milling width.
        /// </summary>
        private decimal? millingWidth;

        /// <summary>
        /// The milling depth.
        /// </summary>
        private decimal? millingDepth;

        /// <summary>
        /// The tool change time.
        /// </summary>
        private decimal? toolChangeTime;

        /// <summary>
        /// The calculated turning speed displayed in tooltip.
        /// </summary>
        private string calculatedTurningSpeed;

        /// <summary>
        /// The turning speed.
        /// </summary>
        private decimal? turningSpeed;

        /// <summary>
        /// The calculated feed rate displayed in tooltip.
        /// </summary>
        private string calculatedFeedRate;

        /// <summary>
        /// The feed rate.
        /// </summary>
        private decimal? feedRate;

        /// <summary>
        /// The resulting milling time.
        /// </summary>
        private decimal? resultingMillingTime;

        /// <summary>
        /// The resulting gross process time.
        /// </summary>
        private decimal? resultingCycleTime;

        /// <summary>
        /// A value indicating whether the machining calculator is in expert mode or not.
        /// </summary>
        private bool isExpertMode;

        /// <summary>
        /// A value indicating whether the tool width textbox is read only or not.
        /// </summary>
        private bool isToolWidthTextBoxEnabled;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="MachiningCalculatorMillingViewModel" /> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        /// <param name="messenger">The messenger.</param>
        [ImportingConstructor]
        public MachiningCalculatorMillingViewModel(IWindowService windowService, IMessenger messenger)
        {
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("messenger", messenger);

            this.windowService = windowService;
            this.messenger = messenger;

            InitializeProperties();
        }

        #region Properties

        /// <summary>
        /// Gets the list of milling materials.
        /// </summary>
        public List<MillingMaterialToBeMachined> MillingMaterials { get; private set; }

        /// <summary>
        /// Gets or sets the selected milling material.
        /// </summary>
        [UndoableProperty]
        public MillingMaterialToBeMachined SelectedMaterial
        {
            get { return this.selectedMaterial; }
            set { this.SetProperty(ref this.selectedMaterial, value, () => this.SelectedMaterial, this.UpdateMillingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the process description.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public string ProcessDescription
        {
            get { return this.processDescription; }
            set { this.SetProperty(ref this.processDescription, value, () => this.ProcessDescription); }
        }

        /// <summary>
        /// Gets or sets the selected milling tool type.
        /// </summary>
        [UndoableProperty]
        public MillingToolType SelectedMillingToolType
        {
            get { return this.selectedMillingToolType; }
            set { this.SetProperty(ref this.selectedMillingToolType, value, () => this.SelectedMillingToolType, this.OnMillingToolTypeChanged); }
        }

        /// <summary>
        /// Gets or sets the feed speed.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_FeedSpeed", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? FeedSpeed
        {
            get { return this.feedSpeed; }
            set { this.SetProperty(ref this.feedSpeed, value, () => this.FeedSpeed, this.UpdateMillingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the selected milling type.
        /// </summary>
        [UndoableProperty]
        public MillingType SelectedMillingType
        {
            get { return this.selectedMillingType; }
            set { this.SetProperty(ref this.selectedMillingType, value, () => this.SelectedMillingType, this.UpdateMillingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the cut depth.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_CutDepth", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? CutDepth
        {
            get { return this.cutDepth; }
            set { this.SetProperty(ref this.cutDepth, value, () => this.CutDepth, this.UpdateMillingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the number of teeth.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Milling_TeethNumber", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? TeethNumber
        {
            get { return this.teethNumber; }
            set { this.SetProperty(ref this.teethNumber, value, () => this.TeethNumber, this.UpdateMillingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the milling tool diameter.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_ToolDiameter", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? MillingToolDiameter
        {
            get { return this.millingToolDiameter; }
            set { this.SetProperty(ref this.millingToolDiameter, value, () => this.MillingToolDiameter, this.UpdateMillingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the milling tool width.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_ToolWidth", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? MillingToolWidth
        {
            get { return this.millingToolWidth; }
            set { this.SetProperty(ref this.millingToolWidth, value, () => this.MillingToolWidth, this.UpdateMillingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the start stop time for machine feed.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_StartStopTime", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? StartStopTime
        {
            get { return this.startStopTime; }
            set { this.SetProperty(ref this.startStopTime, value, () => this.StartStopTime, this.UpdateMillingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the machine feed length.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_FeedLength", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? FeedLength
        {
            get { return this.feedLength; }
            set { this.SetProperty(ref this.feedLength, value, () => this.FeedLength, this.UpdateMillingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the milling length.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_MillingLength", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? MillingLength
        {
            get { return this.millingLength; }
            set { this.SetProperty(ref this.millingLength, value, () => this.MillingLength, this.UpdateMillingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the milling width.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_MillingWidth", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? MillingWidth
        {
            get { return this.millingWidth; }
            set { this.SetProperty(ref this.millingWidth, value, () => this.MillingWidth, this.UpdateMillingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the milling depth.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_MillingDepth", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? MillingDepth
        {
            get { return this.millingDepth; }
            set { this.SetProperty(ref this.millingDepth, value, () => this.MillingDepth, this.UpdateMillingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the tool change time.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_ToolChangeTime", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? ToolChangeTime
        {
            get { return this.toolChangeTime; }
            set { this.SetProperty(ref this.toolChangeTime, value, () => this.ToolChangeTime, this.UpdateMillingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the calculated turning speed displayed in tooltip.
        /// </summary>
        public string CalculatedTurningSpeed
        {
            get { return this.calculatedTurningSpeed; }
            set { this.SetProperty(ref this.calculatedTurningSpeed, value, () => this.CalculatedTurningSpeed); }
        }

        /// <summary>
        /// Gets or sets the turning speed.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_TurningSpeed", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? TurningSpeed
        {
            get
            {
                return this.turningSpeed;
            }

            set
            {
                if (value != this.turningSpeed)
                {
                    var oldValue = this.turningSpeed;
                    this.turningSpeed = value.HasValue ? Math.Round(value.Value) : (decimal?)null;
                    this.OnPropertyChanged(() => this.TurningSpeed);

                    if (oldValue != this.turningSpeed)
                    {
                        this.UpdateMillingCalculationResult();
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the calculated feed rate displayed in tooltip.
        /// </summary>
        public string CalculatedFeedRate
        {
            get { return this.calculatedFeedRate; }
            set { this.SetProperty(ref this.calculatedFeedRate, value, () => this.CalculatedFeedRate); }
        }

        /// <summary>
        /// Gets or sets the feed rate.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_FeedRate", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? FeedRate
        {
            get { return this.feedRate; }
            set { this.SetProperty(ref this.feedRate, value, () => this.FeedRate, this.UpdateMillingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the resulting milling time.
        /// </summary>
        public decimal? ResultingMillingTime
        {
            get { return this.resultingMillingTime; }
            set { this.SetProperty(ref this.resultingMillingTime, value, () => this.ResultingMillingTime); }
        }

        /// <summary>
        /// Gets or sets the resulting cycle time.
        /// </summary>
        public decimal? ResultingCycleTime
        {
            get { return this.resultingCycleTime; }
            set { this.SetProperty(ref this.resultingCycleTime, value, () => this.ResultingCycleTime); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the machining calculator is in expert mode or not.
        /// </summary>
        public bool IsExpertMode
        {
            get { return this.isExpertMode; }
            set { this.SetProperty(ref this.isExpertMode, value, () => this.IsExpertMode, this.UpdateMillingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the tool width textbox is read only or not.
        /// </summary>
        public bool IsToolWidthTextBoxEnabled
        {
            get { return this.isToolWidthTextBoxEnabled; }
            set { this.SetProperty(ref this.isToolWidthTextBoxEnabled, value, () => this.IsToolWidthTextBoxEnabled); }
        }

        /// <summary>
        /// Gets or sets the token used for sending messages to machining calculator.
        /// </summary>
        public Guid MessengerTokenForMachining { get; set; }

        /// <summary>
        /// Gets or sets the UndoManager from CycleTime parent window.
        /// </summary>
        public UndoManager ParentUndoManager { get; set; }

        #endregion

        /// <summary>
        /// Called when the Model has changed.
        /// </summary>
        protected override void OnModelChanged()
        {
            base.OnModelChanged();

            if ((this.Model != null) && this.Model.MachiningType.Value == MachiningType.Milling && this.EditMode == ViewModelEditMode.Edit)
            {
                this.LoadMillingResultDataFromCalculationToEdit();
            }
        }

        /// <summary>
        /// Initializes the properties of this instance.
        /// </summary>
        private void InitializeProperties()
        {
            this.machiningCalculator = new MachiningCalculator();
            this.MillingMaterials = new List<MillingMaterialToBeMachined>(this.machiningCalculator.Knowledgebase.MillingData.MaterialsToMachine);

            this.SelectedMaterial = this.MillingMaterials.FirstOrDefault();
            this.SelectedMillingToolType = MillingToolType.CylindricalCutter;
            this.SelectedMillingType = MillingType.RoughMachining;
            this.FeedSpeed = this.machiningCalculator.Knowledgebase.MillingData.DefaultMachineFeedSpeed;
            this.CutDepth = this.machiningCalculator.Knowledgebase.MillingData.DefaultCutDepth;
            this.TeethNumber = this.machiningCalculator.Knowledgebase.MillingData.DefaultTeethNumber;
            this.MillingToolDiameter = this.machiningCalculator.Knowledgebase.MillingData.DefaultMillingToolDiameter;
            this.MillingToolWidth = this.machiningCalculator.Knowledgebase.MillingData.DefaultMillingToolWidth;
            this.StartStopTime = this.machiningCalculator.Knowledgebase.MillingData.DefaultStartStopTimeForMachineFeed;
            this.FeedLength = this.machiningCalculator.Knowledgebase.MillingData.DefaultMachineFeedLength;
            this.MillingLength = this.machiningCalculator.Knowledgebase.MillingData.DefaultMillingLength;
            this.MillingWidth = this.machiningCalculator.Knowledgebase.MillingData.DefaultMillingWidth;
            this.MillingDepth = this.machiningCalculator.Knowledgebase.MillingData.DefaultMillingDepth;
            this.ToolChangeTime = this.machiningCalculator.Knowledgebase.MillingData.DefaultToolChangeTime;
        }

        /// <summary>
        /// Loads the milling result data from the object that the screen is editing.
        /// </summary>
        private void LoadMillingResultDataFromCalculationToEdit()
        {
            if (this.Model.MachiningCalculationData == null ||
                this.Model.MachiningCalculationData.Value == null)
            {
                return;
            }

            UnicodeEncoding encoder = new UnicodeEncoding();
            string data = encoder.GetString(this.Model.MachiningCalculationData.Value);

            if (!string.IsNullOrEmpty(data))
            {
                this.ProcessDescription = this.Model.ToleranceType.Value;

                string[] parts = data.Split(';');
                if (parts.Length > 0)
                {
                    int materialId;
                    if (int.TryParse(parts[0], out materialId))
                    {
                        this.SelectedMaterial = this.MillingMaterials.FirstOrDefault(material => material.MaterialID == materialId);
                        if (this.SelectedMaterial == null)
                        {
                            this.windowService.MessageDialogService.Show(LocalizedResources.MachiningCalculator_MaterialUnavailable, MessageDialogType.Error);
                        }
                    }
                    else
                    {
                        this.SelectedMaterial = null;
                    }
                }

                if (parts.Length > 1)
                {
                    MillingToolType millingToolType;
                    if (Enum.TryParse(parts[1], out millingToolType))
                    {
                        this.SelectedMillingToolType = millingToolType;
                    }
                }

                if (parts.Length > 3)
                {
                    MillingType millingType;
                    if (Enum.TryParse(parts[3], out millingType))
                    {
                        this.SelectedMillingType = millingType;
                    }
                }

                if (parts.Length > 2)
                {
                    this.FeedSpeed = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[2]);
                }

                if (parts.Length > 4)
                {
                    this.CutDepth = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[4]);
                }

                if (parts.Length > 5)
                {
                    this.TeethNumber = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[5]);
                }

                if (parts.Length > 6)
                {
                    this.MillingToolDiameter = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[6]);
                }

                if (parts.Length > 7)
                {
                    this.MillingToolWidth = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[7]);
                }

                if (parts.Length > 8)
                {
                    this.StartStopTime = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[8]);
                }

                if (parts.Length > 9)
                {
                    this.FeedLength = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[9]);
                }

                if (parts.Length > 10)
                {
                    this.MillingLength = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[10]);
                }

                if (parts.Length > 11)
                {
                    this.MillingWidth = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[11]);
                }

                if (parts.Length > 12)
                {
                    this.MillingDepth = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[12]);
                }

                bool expertMode = false;
                if (parts.Length > 13)
                {
                    bool.TryParse(parts[13], out expertMode);
                    this.IsExpertMode = expertMode;

                    if (this.IsExpertMode)
                    {
                        var message = new NotificationMessage<CycleTimeCalculationItemViewModel>(MachiningCalculatorViewModel.CalculatorNotificationMessages.ExpertModeChanged, this.Model);
                        this.messenger.Send<NotificationMessage<CycleTimeCalculationItemViewModel>>(message, this.MessengerTokenForMachining);
                    }
                }

                if (expertMode && parts.Length > 14)
                {
                    this.FeedRate = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[14]);
                }

                if (parts.Length > 15)
                {
                    this.ToolChangeTime = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[15]);
                }

                if (expertMode && parts.Length > 16)
                {
                    this.TurningSpeed = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[16]);
                }
            }
        }

        /// <summary>
        /// Handles the SelectionChanged event of the selected milling tool type.
        /// </summary>
        private void OnMillingToolTypeChanged()
        {
            if (this.SelectedMillingToolType == MillingToolType.CylindricalCutter || this.SelectedMillingToolType == MillingToolType.SideMillCutter)
            {
                this.IsToolWidthTextBoxEnabled = false;
            }
            else
            {
                this.IsToolWidthTextBoxEnabled = true;
            }

            this.UpdateMillingCalculationResult();
        }

        /// <summary>
        /// Performs the milling operation calculation and updates the result on the UI.
        /// </summary>
        private void UpdateMillingCalculationResult()
        {
            using (this.UndoManager.Pause())
            {
                MillingCalculationResult result = CalculateMillingOperation();
                this.ResultingMillingTime = result.MillingTime;
                this.ResultingCycleTime = result.GrossProcessTime;

                if (this.IsExpertMode)
                {
                    var resultedTurningSpeed = result.TurningSpeed.HasValue ? Math.Round(result.TurningSpeed.Value) : (decimal?)null;
                    this.CalculatedTurningSpeed = Formatter.FormatNumber(resultedTurningSpeed);

                    var resultedFeedRate = result.FeedRatePerTooth.HasValue ? Math.Round(result.FeedRatePerTooth.Value, 2) : (decimal?)null;
                    this.CalculatedFeedRate = Formatter.FormatNumber(resultedFeedRate);
                }
                else
                {
                    this.TurningSpeed = result.TurningSpeed;
                    this.FeedRate = result.FeedRatePerTooth;
                }
            }
        }

        /// <summary>
        /// Performs the save operation. Executed by the SaveCommand.
        /// </summary>
        protected override void Save()
        {
            if (this.EditMode == ViewModelEditMode.Create)
            {
                // If the view model is not in edit mode, it does not have a model attached, so we have to create it.
                var calculation = new CycleTimeCalculation();
                var calculationItemVM = new CycleTimeCalculationItemViewModel(windowService, messenger);
                calculationItemVM.EditMode = ViewModelEditMode.Create;

                this.Model = calculationItemVM;
                calculationItemVM.Model = calculation;
            }

            this.SaveToModel();

            if (this.EditMode == ViewModelEditMode.Create)
            {
                var message = new NotificationMessage<CycleTimeCalculationItemViewModel>(MachiningCalculatorViewModel.CalculatorNotificationMessages.AddMachiningCalculator, this.Model);
                this.messenger.Send<NotificationMessage<CycleTimeCalculationItemViewModel>>(message, this.MessengerTokenForMachining);
            }
            else
            {
                var message = new NotificationMessage<CycleTimeCalculationItemViewModel>(MachiningCalculatorViewModel.CalculatorNotificationMessages.UpdateMachiningCalculator, this.Model);
                this.messenger.Send<NotificationMessage<CycleTimeCalculationItemViewModel>>(message, this.MessengerTokenForMachining);
            }
        }

        /// <summary>
        /// Executes the logic associated with the SaveToModelCommand command.
        /// </summary>
        protected override void SaveToModel()
        {
            if (this.ParentUndoManager != null)
            {
                using (this.ParentUndoManager.StartBatch(navigateToBatchControls: true))
                {
                    this.SaveMillingData();
                }
            }
            else
            {
                this.SaveMillingData();
            }
        }

        /// <summary>
        /// Saves all changed back into the model and resets the changed status. 
        /// </summary>
        private void SaveMillingData()
        {
            MillingCalculationResult result = CalculateMillingOperation();

            this.Model.Description.Value = LocalizedResources.MachiningCalculator_Milling + " - " + this.SelectedMillingToolType;
            this.Model.MachiningVolume.Value = result.MachiningVolume.HasValue ? result.MachiningVolume.Value : 0;
            this.Model.MachiningType.Value = MachiningType.Milling;

            decimal? toolChangeTime = this.ToolChangeTime;
            decimal? transportTime = result.GrossProcessTime - result.MillingTime - toolChangeTime;
            this.Model.TransportClampingTime.Value = transportTime.GetValueOrDefault();
            this.Model.MachiningTime.Value = result.MillingTime.GetValueOrDefault();
            this.Model.ToolChangeTime.Value = toolChangeTime.GetValueOrDefault();
            this.Model.ToleranceType.Value = this.ProcessDescription;

            this.SaveMillingResultDataToBinary();
        }

        /// <summary>
        /// Calculates the milling operation.
        /// </summary>
        /// <returns>The result.</returns>
        private MillingCalculationResult CalculateMillingOperation()
        {
            MillingCalculationInputData input = new MillingCalculationInputData();

            input.Material = this.SelectedMaterial;
            input.ToolType = this.SelectedMillingToolType;
            input.MachineFeedSpeed = this.FeedSpeed;
            input.MillingType = this.SelectedMillingType;
            input.CutDepth = this.CutDepth;
            input.TeethNumber = this.TeethNumber;
            input.MillingToolDiameter = this.MillingToolDiameter;
            input.MillingToolWidth = this.MillingToolWidth;
            input.StartStopTimeForMachineFeed = this.StartStopTime;
            input.MachineFeedLength = this.FeedLength;
            input.MillingLength = this.MillingLength;
            input.MillingWidth = this.MillingWidth;
            input.MillingDepth = this.MillingDepth;
            input.ToolChangeTime = this.ToolChangeTime;

            if (this.IsExpertMode)
            {
                input.ExpertModeCalculation = true;
                input.FeedRatePerTooth = this.FeedRate;
                input.TurningSpeed = this.TurningSpeed;
            }
            else
            {
                input.ExpertModeCalculation = false;
            }

            return this.machiningCalculator.CalculateMillingOperation(input);
        }

        /// <summary>
        /// Saves the current milling result data into the machining calculation data.
        /// </summary>
        private void SaveMillingResultDataToBinary()
        {
            // The data will be saved as a comma separated list.
            string data = string.Empty;

            string millingMaterial = string.Empty;
            MillingMaterialToBeMachined material = this.SelectedMaterial;
            if (material != null)
            {
                millingMaterial = material.MaterialID.ToString();
            }

            string millingToolType = ((int)this.SelectedMillingToolType).ToString();

            string millingType = ((int)this.SelectedMillingType).ToString();

            data += millingMaterial + ";";
            data += millingToolType + ";";
            data += (this.FeedSpeed.HasValue ? this.FeedSpeed.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += millingType + ";";
            data += (this.CutDepth.HasValue ? this.CutDepth.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += (this.TeethNumber.HasValue ? this.TeethNumber.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += (this.MillingToolDiameter.HasValue ? this.MillingToolDiameter.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += (this.MillingToolWidth.HasValue ? this.MillingToolWidth.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += (this.StartStopTime.HasValue ? this.StartStopTime.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += (this.FeedLength.HasValue ? this.FeedLength.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += (this.MillingLength.HasValue ? this.MillingLength.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += (this.MillingWidth.HasValue ? this.MillingWidth.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += (this.MillingDepth.HasValue ? this.MillingDepth.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += this.IsExpertMode + ";";
            data += (this.FeedRate.HasValue ? this.FeedRate.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += (this.ToolChangeTime.HasValue ? this.ToolChangeTime.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += this.TurningSpeed.HasValue ? this.TurningSpeed.Value.ToString(CultureInfo.InvariantCulture) : string.Empty;

            UnicodeEncoding encoding = new UnicodeEncoding();
            this.Model.MachiningCalculationData.Value = encoding.GetBytes(data);
        }
    }
}
