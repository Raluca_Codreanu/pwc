﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using ZPKTool.Business.MachiningCalculator;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the MachiningCalculatorDrilling view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class MachiningCalculatorDrillingViewModel : ViewModel<CycleTimeCalculationItemViewModel>
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The machining calculator.
        /// </summary>
        private MachiningCalculator machiningCalculator;

        /// <summary>
        /// The selected drilling material.
        /// </summary>
        private DrillingMaterialToBeMachined selectedMaterial;

        /// <summary>
        /// The process description.
        /// </summary>
        private string processDescription;

        /// <summary>
        /// The selected drilling type.
        /// </summary>
        private DrillingType selectedDrillingType;

        /// <summary>
        /// The machine feed speed.
        /// </summary>
        private decimal? feedSpeed;

        /// <summary>
        /// The start stop time for machine feed.
        /// </summary>
        private decimal? startStopTime;

        /// <summary>
        /// The drilling/thread diameter.
        /// </summary>
        private decimal? drillingDiameter;

        /// <summary>
        /// The drilling depth.
        /// </summary>
        private decimal? drillingDepth;

        /// <summary>
        /// The number of holes.
        /// </summary>
        private int? holesNumber;

        /// <summary>
        /// The average distance between holes.
        /// </summary>
        private decimal? avgHoleDistance;

        /// <summary>
        /// The tool change time.
        /// </summary>
        private decimal? toolChangeTime;

        /// <summary>
        /// The calculated turning speed displayed in tooltip.
        /// </summary>
        private string calculatedTurningSpeed;

        /// <summary>
        /// The turning speed.
        /// </summary>
        private decimal? turningSpeed;

        /// <summary>
        /// The resulting drilling time.
        /// </summary>
        private decimal? resultingDrillingTime;

        /// <summary>
        /// The resulting gross process time.
        /// </summary>
        private decimal? resultingCycleTime;

        /// <summary>
        /// A value indicating whether the boring is checked or not.
        /// </summary>
        private bool isBoringChecked;

        /// <summary>
        /// A value indicating whether the machining calculator is in expert mode or not.
        /// </summary>
        private bool isExpertMode = false;

        /// <summary>
        /// The boring checkbox visibility. 
        /// </summary>
        private Visibility boringVisibility;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="MachiningCalculatorDrillingViewModel" /> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        /// <param name="messenger">The messenger.</param>
        [ImportingConstructor]
        public MachiningCalculatorDrillingViewModel(IWindowService windowService, IMessenger messenger)
        {
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("messenger", messenger);

            this.windowService = windowService;
            this.messenger = messenger;

            InitializeProperties();
        }

        #region Properties

        /// <summary>
        /// Gets the list of drilling materials.
        /// </summary>
        public List<DrillingMaterialToBeMachined> DrillingMaterials { get; private set; }

        /// <summary>
        /// Gets or sets the selected drilling material.
        /// </summary>
        [UndoableProperty]
        public DrillingMaterialToBeMachined SelectedMaterial
        {
            get { return this.selectedMaterial; }
            set { this.SetProperty(ref this.selectedMaterial, value, () => this.SelectedMaterial, this.UpdateDrillingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the process description.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public string ProcessDescription
        {
            get { return this.processDescription; }
            set { this.SetProperty(ref this.processDescription, value, () => this.ProcessDescription); }
        }

        /// <summary>
        /// Gets or sets the selected drilling type.
        /// </summary>
        [UndoableProperty]
        public DrillingType SelectedDrillingType
        {
            get { return this.selectedDrillingType; }
            set { this.SetProperty(ref this.selectedDrillingType, value, () => this.SelectedDrillingType, this.OnSelectedTypeChanged); }
        }

        /// <summary>
        /// Gets or sets the feed speed.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_FeedSpeed", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? FeedSpeed
        {
            get { return this.feedSpeed; }
            set { this.SetProperty(ref this.feedSpeed, value, () => this.FeedSpeed, this.UpdateDrillingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the start stop time for machine feed.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_StartStopTime", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? StartStopTime
        {
            get { return this.startStopTime; }
            set { this.SetProperty(ref this.startStopTime, value, () => this.StartStopTime, this.UpdateDrillingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the drilling/thread diameter.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Diameter", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? DrillingDiameter
        {
            get { return this.drillingDiameter; }
            set { this.SetProperty(ref this.drillingDiameter, value, () => this.DrillingDiameter, this.UpdateDrillingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the drilling depth.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Depth", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? DrillingDepth
        {
            get { return this.drillingDepth; }
            set { this.SetProperty(ref this.drillingDepth, value, () => this.DrillingDepth, this.UpdateDrillingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the number of holes.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_HolesNumber", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public int? HolesNumber
        {
            get { return this.holesNumber; }
            set { this.SetProperty(ref this.holesNumber, value, () => this.HolesNumber, this.UpdateDrillingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the average distance between holes.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_AvgHoleDistance", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? AvgHoleDistance
        {
            get { return this.avgHoleDistance; }
            set { this.SetProperty(ref this.avgHoleDistance, value, () => this.AvgHoleDistance, this.UpdateDrillingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the tool change time.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_ToolChangeTime", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? ToolChangeTime
        {
            get { return this.toolChangeTime; }
            set { this.SetProperty(ref this.toolChangeTime, value, () => this.ToolChangeTime, this.UpdateDrillingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the calculated turning speed displayed in tooltip.
        /// </summary>
        public string CalculatedTurningSpeed
        {
            get { return this.calculatedTurningSpeed; }
            set { this.SetProperty(ref this.calculatedTurningSpeed, value, () => this.CalculatedTurningSpeed); }
        }

        /// <summary>
        /// Gets or sets the turning speed.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_TurningSpeed", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? TurningSpeed
        {
            get
            {
                return this.turningSpeed;
            }

            set
            {
                if (value != this.turningSpeed)
                {
                    var oldValue = this.turningSpeed;
                    this.turningSpeed = value.HasValue ? Math.Round(value.Value) : (decimal?)null;
                    this.OnPropertyChanged(() => this.TurningSpeed);

                    if (oldValue != this.turningSpeed)
                    {
                        this.UpdateDrillingCalculationResult();
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the resulting drilling time.
        /// </summary>
        public decimal? ResultingDrillingTime
        {
            get { return this.resultingDrillingTime; }
            set { this.SetProperty(ref this.resultingDrillingTime, value, () => this.ResultingDrillingTime); }
        }

        /// <summary>
        /// Gets or sets the resulting cycle time.
        /// </summary>
        public decimal? ResultingCycleTime
        {
            get { return this.resultingCycleTime; }
            set { this.SetProperty(ref this.resultingCycleTime, value, () => this.ResultingCycleTime); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the boring is checked or not.
        /// </summary>
        [UndoableProperty]
        public bool IsBoringChecked
        {
            get { return this.isBoringChecked; }
            set { this.SetProperty(ref this.isBoringChecked, value, () => this.IsBoringChecked, this.UpdateDrillingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the machining calculator is in expert mode or not.
        /// </summary>
        public bool IsExpertMode
        {
            get { return this.isExpertMode; }
            set { this.SetProperty(ref this.isExpertMode, value, () => this.IsExpertMode, this.UpdateDrillingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the boring checkbox visibility.
        /// </summary>
        public Visibility BoringVisibility
        {
            get { return this.boringVisibility; }
            set { this.SetProperty(ref this.boringVisibility, value, () => this.BoringVisibility); }
        }

        /// <summary>
        /// Gets or sets the token used for sending messages to machining calculator.
        /// </summary>
        public Guid MessengerTokenForMachining { get; set; }

        /// <summary>
        /// Gets or sets the UndoManager from CycleTime parent window.
        /// </summary>
        public UndoManager ParentUndoManager { get; set; }

        #endregion

        /// <summary>
        /// Called when the Model has changed.
        /// </summary>
        protected override void OnModelChanged()
        {
            base.OnModelChanged();

            if ((this.Model != null) && this.Model.MachiningType.Value == MachiningType.Drilling && this.EditMode == ViewModelEditMode.Edit)
            {
                this.LoadDrillingResultDataFromCalculationToEdit();
            }
        }

        /// <summary>
        /// Initializes the properties of this instance.
        /// </summary>
        private void InitializeProperties()
        {
            this.machiningCalculator = new MachiningCalculator();
            this.DrillingMaterials = new List<DrillingMaterialToBeMachined>(this.machiningCalculator.Knowledgebase.DrillingData.MaterialsToMachine);

            this.SelectedMaterial = this.DrillingMaterials.FirstOrDefault();
            this.SelectedDrillingType = DrillingType.ThroughHole;
            this.FeedSpeed = this.machiningCalculator.Knowledgebase.DrillingData.DefaultMachineFeedSpeed;
            this.StartStopTime = this.machiningCalculator.Knowledgebase.DrillingData.DefaultStartStopTimeForMachineFeed;
            this.DrillingDiameter = this.machiningCalculator.Knowledgebase.DrillingData.DefaultDrillingDiameter;
            this.DrillingDepth = this.machiningCalculator.Knowledgebase.DrillingData.DefaultDrillingDepth;
            this.HolesNumber = this.machiningCalculator.Knowledgebase.DrillingData.DefaultHolesNumber;
            this.AvgHoleDistance = this.machiningCalculator.Knowledgebase.DrillingData.DefaultAvgHolesDistance;
            this.ToolChangeTime = this.machiningCalculator.Knowledgebase.DrillingData.DefaultToolChangeTime;
        }

        /// <summary>
        /// Loads the drilling result data from the machining calculation data of calculation to edit.
        /// </summary>
        private void LoadDrillingResultDataFromCalculationToEdit()
        {
            if (this.Model.MachiningCalculationData == null ||
                this.Model.MachiningCalculationData.Value == null)
            {
                return;
            }

            UnicodeEncoding encoder = new UnicodeEncoding();
            string data = encoder.GetString(this.Model.MachiningCalculationData.Value);

            if (!string.IsNullOrEmpty(data))
            {
                this.ProcessDescription = this.Model.ToleranceType.Value;

                string[] parts = data.Split(';');
                if (parts.Length > 0)
                {
                    int materialId;
                    if (int.TryParse(parts[0], out materialId))
                    {
                        this.SelectedMaterial = this.DrillingMaterials.FirstOrDefault(material => material.MaterialID == materialId);
                        if (this.SelectedMaterial == null)
                        {
                            this.windowService.MessageDialogService.Show(LocalizedResources.MachiningCalculator_MaterialUnavailable, MessageDialogType.Error);
                        }
                    }
                    else
                    {
                        this.SelectedMaterial = null;
                    }
                }

                if (parts.Length > 1)
                {
                    DrillingType drillingType;
                    if (Enum.TryParse(parts[1], out drillingType))
                    {
                        this.SelectedDrillingType = drillingType;
                    }
                }

                if (parts.Length > 2)
                {
                    this.FeedSpeed = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[2]);
                }

                if (parts.Length > 3)
                {
                    this.StartStopTime = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[3]);
                }

                if (parts.Length > 4)
                {
                    this.DrillingDiameter = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[4]);
                }

                if (parts.Length > 5)
                {
                    this.DrillingDepth = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[5]);
                }

                if (parts.Length > 6)
                {
                    this.HolesNumber = MachiningCalculatorHelper.ConvertIntFromInvariantNumberRepresentation(parts[6]);
                }

                if (parts.Length > 7)
                {
                    this.AvgHoleDistance = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[7]);
                }

                if (parts.Length > 8)
                {
                    this.ToolChangeTime = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[8]);
                }

                if (parts.Length > 9)
                {
                    bool boring;
                    if (!bool.TryParse(parts[9], out boring))
                    {
                        boring = false;
                    }

                    this.IsBoringChecked = boring;
                }

                if (parts.Length > 10)
                {
                    bool expertMode = false;
                    bool.TryParse(parts[10], out expertMode);
                    this.IsExpertMode = expertMode;
                    if (expertMode)
                    {
                        var message = new NotificationMessage<CycleTimeCalculationItemViewModel>(MachiningCalculatorViewModel.CalculatorNotificationMessages.ExpertModeChanged, this.Model);
                        this.messenger.Send<NotificationMessage<CycleTimeCalculationItemViewModel>>(message, this.MessengerTokenForMachining);

                        if (parts.Length > 11)
                        {
                            this.TurningSpeed = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[11]);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Updates the drilling calculation result.
        /// </summary>
        private void UpdateDrillingCalculationResult()
        {
            using (this.UndoManager.Pause())
            {
                DrillingCalculationResult result = CalculateDrillingOperationResult();
                this.ResultingDrillingTime = result.DrillingTime;
                this.ResultingCycleTime = result.GrossProcessTime;

                if (this.IsExpertMode)
                {
                    var resultedTurningSpeed = result.TurningSpeed.HasValue ? Math.Round(result.TurningSpeed.Value) : (decimal?)null;
                    this.CalculatedTurningSpeed = Formatter.FormatNumber(resultedTurningSpeed);
                }
                else
                {
                    this.TurningSpeed = result.TurningSpeed;
                }
            }
        }

        /// <summary>
        /// Handles the selected type changed event.
        /// </summary>
        private void OnSelectedTypeChanged()
        {
            this.UpdateDrillingCalculationResult();

            if (this.SelectedDrillingType == DrillingType.BlindHole || this.SelectedDrillingType == DrillingType.ThroughHole)
            {
                this.BoringVisibility = Visibility.Visible;
            }
            else
            {
                this.BoringVisibility = Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Performs the save operation. Executed by the SaveCommand.
        /// </summary>
        protected override void Save()
        {
            if (this.EditMode == ViewModelEditMode.Create)
            {
                // If the view model is not in edit mode, it does not have a model attached, so we have to create it.
                var calculation = new CycleTimeCalculation();
                var calculationItemVM = new CycleTimeCalculationItemViewModel(windowService, messenger);
                calculationItemVM.EditMode = ViewModelEditMode.Create;

                this.Model = calculationItemVM;
                calculationItemVM.Model = calculation;
            }

            this.SaveToModel();

            if (this.EditMode == ViewModelEditMode.Create)
            {
                var message = new NotificationMessage<CycleTimeCalculationItemViewModel>(MachiningCalculatorViewModel.CalculatorNotificationMessages.AddMachiningCalculator, this.Model);
                this.messenger.Send<NotificationMessage<CycleTimeCalculationItemViewModel>>(message, this.MessengerTokenForMachining);
            }
            else
            {
                var message = new NotificationMessage<CycleTimeCalculationItemViewModel>(MachiningCalculatorViewModel.CalculatorNotificationMessages.UpdateMachiningCalculator, this.Model);
                this.messenger.Send<NotificationMessage<CycleTimeCalculationItemViewModel>>(message, this.MessengerTokenForMachining);
            }
        }

        /// <summary>
        /// Executes the logic associated with the SaveToModelCommand command.
        /// </summary>
        protected override void SaveToModel()
        {
            if (this.ParentUndoManager != null)
            {
                using (this.ParentUndoManager.StartBatch(navigateToBatchControls: true))
                {
                    this.SaveDrillingData();
                }
            }
            else
            {
                this.SaveDrillingData();
            }
        }

        /// <summary>
        /// Saves all changed back into the model and resets the changed status. 
        /// </summary>
        private void SaveDrillingData()
        {
            DrillingCalculationResult result = this.CalculateDrillingOperationResult();

            this.Model.Description.Value = LocalizedResources.MachiningCalculator_Drilling + " - " + this.SelectedDrillingType;
            this.Model.MachiningVolume.Value = result.MachiningVolume.HasValue ? result.MachiningVolume.Value : 0;
            this.Model.MachiningType.Value = MachiningType.Drilling;

            decimal? toolChangeTime = this.ToolChangeTime;
            decimal? transportTime = result.GrossProcessTime - result.DrillingTime - toolChangeTime;
            this.Model.TransportClampingTime.Value = transportTime.GetValueOrDefault();
            this.Model.MachiningTime.Value = result.DrillingTime.GetValueOrDefault();
            this.Model.ToolChangeTime.Value = toolChangeTime.GetValueOrDefault();
            this.Model.ToleranceType.Value = this.ProcessDescription;

            this.SaveDrillingResultDataToBinary();
        }

        /// <summary>
        /// Calculates the drilling operation result.
        /// </summary>
        /// <returns>The result</returns>
        private DrillingCalculationResult CalculateDrillingOperationResult()
        {
            DrillingCalculationInputData input = new DrillingCalculationInputData();

            input.Material = this.SelectedMaterial;
            input.DrillingType = this.SelectedDrillingType;
            input.AvgHolesDistance = this.AvgHoleDistance;
            input.DrillingDepth = this.DrillingDepth;
            input.DrillingDiameter = this.DrillingDiameter;
            input.HolesNumber = this.HolesNumber;
            input.MachineFeedSpeed = this.FeedSpeed;
            input.StartStopTimeForMachineFeed = this.StartStopTime;
            input.ToolChangeTime = this.ToolChangeTime;
            input.Boring = this.IsBoringChecked;

            if (this.IsExpertMode)
            {
                input.ExpertModeCalculation = true;
                input.TurningSpeed = this.TurningSpeed;
            }
            else
            {
                input.ExpertModeCalculation = false;
            }

            return this.machiningCalculator.CalculateDrillingOperation(input);
        }

        /// <summary>
        /// Saves the current drilling result data into the machining calculation data.
        /// </summary>
        private void SaveDrillingResultDataToBinary()
        {
            // The data will be saved as a comma separated list.
            string data = string.Empty;

            string drillingMaterial = string.Empty;
            DrillingMaterialToBeMachined material = this.SelectedMaterial;
            if (material != null)
            {
                drillingMaterial = material.MaterialID.ToString();
            }

            string drillingType = ((int)this.SelectedDrillingType).ToString();

            data += drillingMaterial + ";";
            data += drillingType + ";";
            data += (this.FeedSpeed.HasValue ? this.FeedSpeed.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += (this.StartStopTime.HasValue ? this.StartStopTime.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += (this.DrillingDiameter.HasValue ? this.DrillingDiameter.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += (this.DrillingDepth.HasValue ? this.DrillingDepth.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += (this.HolesNumber.HasValue ? this.HolesNumber.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += (this.AvgHoleDistance.HasValue ? this.AvgHoleDistance.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += (this.ToolChangeTime.HasValue ? this.ToolChangeTime.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += this.IsBoringChecked.ToString() + ";";
            data += this.IsExpertMode.ToString() + ";";
            data += this.TurningSpeed.HasValue ? this.TurningSpeed.Value.ToString(CultureInfo.InvariantCulture) : string.Empty;

            UnicodeEncoding encoding = new UnicodeEncoding();
            this.Model.MachiningCalculationData.Value = encoding.GetBytes(data);
        }
    }
}
