﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Windows.Data;
using System.Windows.Input;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Controls;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Resources;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the CycleTimeCalculator view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class CycleTimeCalculatorViewModel : ViewModel
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The composition container.
        /// </summary>
        private CompositionContainer container;

        /// <summary>
        /// The process step for which the calculations are made.
        /// </summary>
        private ProcessStep processStep;

        /// <summary>
        /// The selected time unit from the time units combobox.
        /// </summary>
        private MeasurementUnit selectedTimeUnit;

        /// <summary>
        /// The calculated cycle time in seconds.
        /// </summary>
        private decimal calculatedCycleTime;

        /// <summary>
        /// Represents the list of deleted calculations.
        /// </summary>
        private List<CycleTimeCalculationItemViewModel> deletedCalculations = new List<CycleTimeCalculationItemViewModel>();

        /// <summary>
        /// Represents the list of initial calculations.
        /// </summary>
        private List<CycleTimeCalculationItemViewModel> initialCalculations = new List<CycleTimeCalculationItemViewModel>();

        /// <summary>
        /// The item from the clipboard, it has value after executing a copy command on a cycle time calculation item.
        /// </summary>
        private CycleTimeCalculationItemViewModel clipboardItem;

        /// <summary>
        /// The list of calculation view model items.
        /// </summary>
        private ObservableCollection<CycleTimeCalculationItemViewModel> calculationViewModelItems;

        /// <summary>
        /// The cycle time.
        /// </summary>
        private decimal? cycleTime;

        /// <summary>
        /// The token object used for receiving messages from the view model's children.
        /// </summary>
        private Guid messengerToken;

        /// <summary>
        /// A value indicating whether any calculation item is editing or not.
        /// </summary>
        private bool isEditedAnyCalculationItem = false;

        /// <summary>
        /// The count of calculation items that are editing.
        /// </summary>
        private int editedCalculationItemsCount = 0;

        /// <summary>
        /// The selected calculation from data grid.
        /// </summary>
        private CycleTimeCalculationItemViewModel dataGridSelectedCalculation;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="CycleTimeCalculatorViewModel" /> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        /// <param name="messenger">The messenger.</param>
        /// <param name="container">The container.</param>
        [ImportingConstructor]
        public CycleTimeCalculatorViewModel(IWindowService windowService, IMessenger messenger, CompositionContainer container)
        {
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("container", container);

            this.windowService = windowService;
            this.messenger = messenger;
            this.container = container;

            // Initialize the commands. 
            this.CopyCommand = new DelegateCommand<object>(Copy, CanCopy);
            this.PasteCommand = new DelegateCommand<object>(Paste, CanPaste);
            this.AddCommand = new DelegateCommand(AddCalculationItem);
            this.DeleteCommand = new DelegateCommand<CycleTimeCalculationItemViewModel>(this.DeleteCalculationItem, this.CanDeleteCalculationItem);
            this.EditCommand = new DelegateCommand<CycleTimeCalculationItemViewModel>(this.EditCalculationItem, this.CanEditCalculationItem);
            this.MoveUpCommand = new DelegateCommand(this.MoveUp, () => !this.IsReadOnly);
            this.MoveDownCommand = new DelegateCommand(this.MoveDown, () => !this.IsReadOnly);
            this.SaveAllCommand = new CompositeCommand();
            this.MachiningCalculatorCommand = new DelegateCommand(OpenMachiningCalculator);

            // Set the undo manager.
            this.UndoManager.RegisterPropertyAction(() => this.CalculationViewModelItems, this.HandleUndo, this);
            this.CalculationViewModelItems = new ObservableCollection<CycleTimeCalculationItemViewModel>();
            this.UndoManager.Start();

            // Register to notification messages.
            messengerToken = Guid.NewGuid();
            this.messenger.Register<NotificationMessage>(this.HandleNotificationMessageToUpdateTotalTime);
            this.messenger.Register<NotificationMessage<CycleTimeCalculationItemViewModel>>(this.HandleNotificationMessageToAddCalculation, messengerToken);

            this.ShowCancelMessageFlag = true;
        }

        #region Commands

        /// <summary>
        /// Gets the menu copy command.
        /// </summary>        
        public ICommand CopyCommand { get; private set; }

        /// <summary>
        /// Gets the menu paste command.
        /// </summary>        
        public ICommand PasteCommand { get; private set; }

        /// <summary>
        /// Gets the add command.
        /// </summary>        
        public ICommand AddCommand { get; private set; }

        /// <summary>
        /// Gets the delete command.
        /// </summary>        
        public ICommand DeleteCommand { get; private set; }

        /// <summary>
        /// Gets the edit command.
        /// </summary>        
        public ICommand EditCommand { get; private set; }

        /// <summary>
        /// Gets the move up command.
        /// </summary>        
        public ICommand MoveUpCommand { get; private set; }

        /// <summary>
        /// Gets the move down command.
        /// </summary>        
        public ICommand MoveDownCommand { get; private set; }

        /// <summary>
        /// Gets or sets the command that saves all changes.
        /// </summary>        
        public CompositeCommand SaveAllCommand { get; set; }

        /// <summary>
        /// Gets or sets the command that opens the machining calculator.
        /// </summary>        
        public ICommand MachiningCalculatorCommand { get; set; }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the process step.
        /// </summary>
        public ProcessStep ProcessStep
        {
            get { return this.processStep; }
            set { this.SetProperty(ref this.processStep, value, () => this.ProcessStep, this.OnProcessStepChanged); }
        }

        /// <summary>
        /// Gets or sets the selected time unit.
        /// </summary>
        [UndoableProperty]
        public MeasurementUnit SelectedTimeUnit
        {
            get { return this.selectedTimeUnit; }
            set { this.SetProperty(ref this.selectedTimeUnit, value, () => this.SelectedTimeUnit, this.OnSelectedTimeUnitChanged); }
        }

        /// <summary>
        /// Gets or sets the list of calculation view model items.
        /// </summary>
        [UndoableProperty]
        public ObservableCollection<CycleTimeCalculationItemViewModel> CalculationViewModelItems
        {
            get { return this.calculationViewModelItems; }
            set { this.SetProperty(ref this.calculationViewModelItems, value, () => this.CalculationViewModelItems); }
        }

        /// <summary>
        /// Gets or sets the selected calculation from data grid.
        /// </summary>
        public CycleTimeCalculationItemViewModel DataGridSelectedCalculation
        {
            get { return this.dataGridSelectedCalculation; }
            set { this.SetProperty(ref this.dataGridSelectedCalculation, value, () => this.DataGridSelectedCalculation); }
        }

        /// <summary>
        /// Gets or sets the list of time measurement units.
        /// </summary>
        public List<MeasurementUnit> TimeUnits { get; set; }

        /// <summary>
        /// Gets or sets the current time measurement unit.
        /// </summary>
        public MeasurementUnit CrtTimeUnit { get; set; }

        /// <summary>
        /// Gets or sets the current time measurement unit.
        /// </summary>
        public MeasurementUnit ProcessTimeUnit { get; set; }

        /// <summary>
        /// Gets or sets the cycle time.
        /// </summary>
        public decimal? CycleTime
        {
            get
            {
                return this.cycleTime;
            }

            set
            {
                if (value != this.cycleTime)
                {
                    this.cycleTime = value.HasValue ? Math.Round(value.Value, 4) : (decimal?)null;
                    this.OnPropertyChanged(() => this.CycleTime);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the cancel message will appear or not.
        /// </summary>
        public bool ShowCancelMessageFlag { get; set; }

        /// <summary>
        /// Gets or sets the data source manager.
        /// </summary>
        public IDataSourceManager DataManager { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether some calculation item is editing or not.
        /// </summary>
        public bool IsEditedAnyCalculationItem
        {
            get { return this.isEditedAnyCalculationItem; }
            set { this.SetProperty(ref this.isEditedAnyCalculationItem, value, () => this.IsEditedAnyCalculationItem); }
        }

        #endregion

        /// <summary>
        /// Determines whether the calculation is supported for the specified machining type.
        /// </summary>
        /// <param name="type">The machining type.</param>
        /// <returns>
        /// true if the calculation is supported; otherwise, false.
        /// </returns>
        public static bool IsCalculationSupported(MachiningType type)
        {
            if (type == MachiningType.PartPositioning || type == MachiningType.Drilling ||
                type == MachiningType.Turning || type == MachiningType.Milling ||
                type == MachiningType.GearHobbing || type == MachiningType.Grinding)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Initializes the properties of this instance.
        /// </summary>
        public void InitializeProperties()
        {
            using (this.UndoManager.Pause())
            {
                if (this.IsInViewerMode)
                {
                    this.TimeUnits.Add(this.CrtTimeUnit);
                }

                if (CrtTimeUnit != null)
                {
                    var selectedUnit = this.TimeUnits.FirstOrDefault(u => u.Symbol == this.CrtTimeUnit.Symbol);
                    if (selectedUnit != null)
                    {
                        this.SelectedTimeUnit = selectedUnit;
                    }
                }
                else
                {
                    this.SelectedTimeUnit = this.TimeUnits.FirstOrDefault();
                }

                // Add each cycle time calculation item of the current process step into view.
                foreach (CycleTimeCalculation calc in this.ProcessStep.CycleTimeCalculations.OrderBy(a => a.Index))
                {
                    this.AddCycleTimeEntry(calc, false);
                    UpdateCycleTime();
                }

                // Copy the calculations into an original items list.
                this.initialCalculations.AddRange(this.CalculationViewModelItems);

                this.IsChanged = false;
            }
        }

        /// <summary>
        /// Determines whether this instance can copy the specified parameter.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>True if this instance can copy the specified parameter; otherwise, false.</returns>
        private bool CanCopy(object parameter)
        {
            if (this.DataGridSelectedCalculation == null || this.IsReadOnly)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Executes Copy.
        /// </summary>
        /// <param name="parameter">The selected item on data grid.</param>
        private void Copy(object parameter)
        {
            this.clipboardItem = parameter as CycleTimeCalculationItemViewModel;
        }

        /// <summary>
        /// Determines whether this instance can paste the specified parameter.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>True if the command can be executed, false otherwise.</returns>
        private bool CanPaste(object parameter)
        {
            if (this.clipboardItem == null || this.IsReadOnly)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Executes Paste.
        /// </summary>
        /// <param name="parameter">The selected item on data grid.</param>
        private void Paste(object parameter)
        {
            if (this.clipboardItem != null)
            {
                this.clipboardItem.SaveToModelCommand.Execute(null);
                CycleTimeCalculation clipboardItemCalculation = this.clipboardItem.Model;
                CycleTimeCalculation calculationCopy = CloneManager.Clone(clipboardItemCalculation);
                if (calculationCopy != null)
                {
                    calculationCopy.Index = null;
                    AddCycleTimeEntry(calculationCopy, true);
                }

                this.clipboardItem = null;
            }
        }

        /// <summary>
        /// Adds a new calculation view model item.
        /// </summary>
        private void AddCalculationItem()
        {
            CycleTimeCalculation calculation = new CycleTimeCalculation();
            AddCycleTimeEntry(calculation, true);
        }

        /// <summary>
        /// Adds each cycle time calculation item, attaching the model for its view model.
        /// </summary>
        /// <param name="calculation">The calculation.</param>
        /// <param name="selectItemAdded">a value indicating whether the new item added should be selected or not</param>
        private void AddCycleTimeEntry(CycleTimeCalculation calculation, bool selectItemAdded)
        {
            if (calculation == null)
            {
                return;
            }

            // Attach the calculation as model.
            CycleTimeCalculationItemViewModel cycleTimeCalculationItem = new CycleTimeCalculationItemViewModel(windowService, messenger);
            cycleTimeCalculationItem.Model = calculation;
            cycleTimeCalculationItem.EditMode = ViewModelEditMode.Create;

            this.SetupCycleTimeCalculationItem(cycleTimeCalculationItem);

            if (selectItemAdded)
            {
                this.DataGridSelectedCalculation = cycleTimeCalculationItem;
            }
        }

        /// <summary>
        /// Makes the setup for the item.
        /// </summary>
        /// <param name="item">The item which will be setup.</param>
        private void SetupCycleTimeCalculationItem(CycleTimeCalculationItemViewModel item)
        {
            if (this.CalculationViewModelItems.Contains(item))
            {
                return;
            }

            item.IsReadOnly = this.IsReadOnly;
            item.IsInViewerMode = this.IsInViewerMode;
            item.DataSourceManager = this.DataManager;

            // Register the view model's save command.
            this.SaveAllCommand.RegisterCommand(item.SaveCommand);

            if (item.Index.Value == null)
            {
                // If we are in the process of adding a new calculation item with null index, set its value.
                item.Index.Value = this.CalculationViewModelItems.Count + 1;
            }

            this.CalculationViewModelItems.Add(item);

            this.UpdateCycleTime();

            // Set the undo manager for each cycle time calculation item.
            item.UndoManager = this.UndoManager;
        }

        /// <summary>
        /// Determines whether this instance can execute the Delete command.
        /// </summary>
        /// <param name="parameter">The command parameter.</param>
        /// <returns>
        /// true if the command can be executed; otherwise returns false.
        /// </returns>
        private bool CanDeleteCalculationItem(CycleTimeCalculationItemViewModel parameter)
        {
            return parameter != null && parameter.EditMode != ViewModelEditMode.Edit && !this.IsReadOnly;
        }

        /// <summary>
        /// Deletes a calculation view model item.
        /// </summary>
        /// <param name="calculationItemToDelete">The item that will be deleted.</param>
        private void DeleteCalculationItem(CycleTimeCalculationItemViewModel calculationItemToDelete)
        {
            // Ask the user to confirm deleting an item.
            MessageDialogResult result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_DeleteSelectedObjects, MessageDialogType.YesNo);
            if (result == MessageDialogResult.No)
            {
                return;
            }

            // Remove it from the calculation list and unregister the view model's save command.
            this.CalculationViewModelItems.Remove(calculationItemToDelete);
            this.SaveAllCommand.UnregisterCommand(calculationItemToDelete.SaveCommand);

            // Add the calculation to deleted calculation list.
            this.deletedCalculations.Add(calculationItemToDelete);

            foreach (var calculation in this.CalculationViewModelItems)
            {
                if (calculation.Index.Value > calculationItemToDelete.Index.Value)
                {
                    // Decrease the index of the items with a grater index than the deleted one.
                    calculation.Index.Value -= 1;
                }
            }

            UpdateCycleTime();
        }

        /// <summary>
        /// Updates the calculator's cycle time value.
        /// </summary>
        private void UpdateCycleTime()
        {
            using (this.UndoManager.Pause())
            {
                this.calculatedCycleTime = 0;
                foreach (CycleTimeCalculationItemViewModel calculationViewModel in this.CalculationViewModelItems)
                {
                    // Sum the total time value from all the calculation items.
                    this.calculatedCycleTime += calculationViewModel.TotalTime;
                }

                this.CycleTime = this.calculatedCycleTime / SelectedTimeUnit.ScaleFactor;
            }
        }

        /// <summary>
        /// Moves up the selected calculation in data grid.
        /// </summary>
        private void MoveUp()
        {
            if (this.DataGridSelectedCalculation != null)
            {
                int selectedCalculation = this.CalculationViewModelItems.IndexOf(this.DataGridSelectedCalculation);
                if (selectedCalculation != 0)
                {
                    this.CalculationViewModelItems.Move(selectedCalculation, selectedCalculation - 1);
                }

                this.MoveUpAction(this.DataGridSelectedCalculation);
            }
        }

        /// <summary>
        /// Moves up the cycle time calculation item passed as parameter.
        /// </summary>
        /// <param name="movedItem">The CycleTime Calculation item.</param>
        private void MoveUpAction(CycleTimeCalculationItemViewModel movedItem)
        {
            CycleTimeCalculationItemViewModel changedCalculation = null;
            foreach (CycleTimeCalculationItemViewModel item in this.CalculationViewModelItems)
            {
                if (item.Index.Value == movedItem.Index.Value - 1)
                {
                    // Invert the index of the selected item to move and the item for which will take the place. 
                    item.Index.Value = movedItem.Index.Value;
                    changedCalculation = item;

                    movedItem.Index.Value -= 1;
                    break;
                }
            }
        }

        /// <summary>
        /// Moves down the selected calculation in data grid.
        /// </summary>
        private void MoveDown()
        {
            if (DataGridSelectedCalculation != null)
            {
                int selectedCalculation = this.CalculationViewModelItems.IndexOf(this.DataGridSelectedCalculation);
                if (selectedCalculation != this.CalculationViewModelItems.Count - 1)
                {
                    this.CalculationViewModelItems.Move(selectedCalculation, selectedCalculation + 1);
                }

                this.MoveDownAction(this.DataGridSelectedCalculation);
            }
        }

        /// <summary>
        /// Moves down the cycle time calculation item passed as parameter.
        /// </summary>
        /// <param name="movedItem">The CycleTime Calculation item.</param>
        private void MoveDownAction(CycleTimeCalculationItemViewModel movedItem)
        {
            CycleTimeCalculationItemViewModel changedCalculation = null;
            foreach (CycleTimeCalculationItemViewModel item in this.CalculationViewModelItems)
            {
                if (item.Index.Value == movedItem.Index.Value + 1)
                {
                    // Invert the index of the selected item to move and the item for which will take the place. 
                    item.Index.Value = movedItem.Index.Value;
                    changedCalculation = item;

                    movedItem.Index.Value += 1;
                    break;
                }
            }
        }

        /// <summary>
        /// Handles the notification message.
        /// </summary>
        /// <param name="message">The message.</param>
        private void HandleNotificationMessageToUpdateTotalTime(NotificationMessage message)
        {
            if (message.Notification == MachiningCalculatorViewModel.CalculatorNotificationMessages.TotalTimeChanged)
            {
                // If the total time changed for one of the calculation items, the calculator's cycle time will be updated too.
                this.UpdateCycleTime();
            }
        }

        /// <summary>
        /// Handles the notification message.
        /// </summary>
        /// <param name="message">The message.</param>
        private void HandleNotificationMessageToAddCalculation(NotificationMessage<CycleTimeCalculationItemViewModel> message)
        {
            if (message.Notification == MachiningCalculatorViewModel.CalculatorNotificationMessages.AddCalculationInCycleTime)
            {
                // If a machining calculator item was added by machining calculator view model, it is added in calculator.
                var calculationItem = message.Content;
                this.SetupCycleTimeCalculationItem(calculationItem);

                // Select the item that was added               
                this.DataGridSelectedCalculation = calculationItem;
            }
        }

        /// <summary>
        /// Handles the selected time unit changed event.
        /// </summary>
        private void OnSelectedTimeUnitChanged()
        {
            this.CycleTime = this.calculatedCycleTime / SelectedTimeUnit.ScaleFactor;
            this.IsChanged = true;
        }

        /// <summary>
        /// Handles the process step changed event.
        /// </summary>
        private void OnProcessStepChanged()
        {
            this.InitializeProperties();
        }

        #region Undo/Save/Cancel

        /// <summary>
        /// Determines whether this instance can perform the undo operation. Executed by the UndoCommand.
        /// </summary>
        /// <returns>
        /// true if the undo operation can be performed, false otherwise.
        /// </returns>
        protected override bool CanUndo()
        {
            return base.CanUndo() && !this.IsEditedAnyCalculationItem;
        }

        /// <summary>
        /// Determines whether this instance can perform the save operation. Executed by the SaveCommand.
        /// The default implementation allows the save to be performed if the input is valid.
        /// </summary>
        /// <returns>
        /// True if the save operation can be performed, false otherwise.
        /// </returns>
        protected override bool CanSave()
        {
            return base.CanSave() && this.SaveAllCommand.CanExecute(null);
        }

        /// <summary>
        /// Performs the save operation. Executed by the SaveCommand.
        /// </summary>
        protected override void Save()
        {
            this.SaveAllCommand.Execute(null);

            foreach (CycleTimeCalculationItemViewModel viewModel in this.CalculationViewModelItems)
            {
                var calculation = viewModel.Model;
                if (calculation != null)
                {
                    // If the calculation's machining volume has a value grater than MaxContentLimit of it's text box, it is not displayed in GUI.
                    // It must be reset to null, otherwise it's value cannot be inserted into database.
                    calculation.MachiningVolume = calculation.MachiningVolume < Constants.MaxNumberValue ? calculation.MachiningVolume.Value : (decimal?)null;

                    if (!this.ProcessStep.CycleTimeCalculations.Contains(calculation))
                    {
                        // Attach the new created items with the context.
                        calculation.IsMasterData = this.ProcessStep.IsMasterData;
                        calculation.SetOwner(this.ProcessStep.Owner);
                        this.ProcessStep.CycleTimeCalculations.Add(calculation);
                    }
                }
            }

            foreach (CycleTimeCalculationItemViewModel item in this.deletedCalculations)
            {
                // Delete the calculation from repository and process step's calculations.
                var deletedCalculation = item.Model;
                this.DataManager.CycleTimeCalculationRepository.RemoveAll(deletedCalculation);
                this.ProcessStep.CycleTimeCalculations.Remove(deletedCalculation);
            }

            this.ShowCancelMessageFlag = false;
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Cancels all changes. Executed by the CancelCommand.
        /// </summary>
        protected override void Cancel()
        {
            var anyItemChanged = CalculationViewModelItems.Any(it => it.IsChanged);
            if (this.IsChanged || anyItemChanged || this.deletedCalculations.Count > 0)
            {
                // If there are changes made, ask the user to confirm cancelling.
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_CancelChanges, MessageDialogType.YesNo);
                if (result == MessageDialogResult.No)
                {
                    // Don't cancel the changes and also don't close the view-model.
                    return;
                }

                // If the changes are canceled, the calculations' list will contain the initial calculations.
                this.CalculationViewModelItems.Clear();
                this.CalculationViewModelItems.AddRange(this.initialCalculations);

                // Refresh the cycle time value for the current calculations.
                UpdateCycleTime();

                // Restore the selected time unit.
                this.SelectedTimeUnit = this.CrtTimeUnit;
            }

            base.Cancel();
            this.ShowCancelMessageFlag = false;
            this.IsChanged = false;
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Called before unloading the view from its parent. Returning false will cancel the view's unloading.
        /// </summary>
        /// <returns>
        /// True if the unloading process should continue and false if it should be canceled.
        /// </returns>
        public override bool OnUnloading()
        {
            if (this.ShowCancelMessageFlag)
            {
                var anyItemChanged = CalculationViewModelItems.Any(it => it.IsChanged);
                if (this.IsChanged || anyItemChanged || this.deletedCalculations.Count > 0)
                {
                    // If there are changes made, ask the user to confirm quitting.
                    var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_Quit, MessageDialogType.YesNo);
                    if (result == MessageDialogResult.No)
                    {
                        // Don't quit.
                        return false;
                    }
                    else
                    {
                        // If the changes are canceled, the calculations' list will contain the initial calculations.
                        this.CalculationViewModelItems.Clear();
                        this.CalculationViewModelItems.AddRange(this.initialCalculations);

                        // Refresh the cycle time value for the current calculations.
                        UpdateCycleTime();

                        // Restore the selected time unit.
                        this.SelectedTimeUnit = this.CrtTimeUnit;
                    }
                }
            }

            return true;
        }

        #endregion Undo/Save/Cancel

        #region Machining Calculator

        /// <summary>
        /// Opens the machining calculator and creates a new calculation view model item for it.
        /// </summary>
        private void OpenMachiningCalculator()
        {
            MachiningCalculatorViewModel machiningCalculatorVM = container.GetExportedValue<MachiningCalculatorViewModel>();
            machiningCalculatorVM.EditMode = ViewModelEditMode.Create;
            machiningCalculatorVM.MessengerTokenForCycleTime = this.messengerToken;

            this.windowService.ShowViewInWindow(machiningCalculatorVM, "MachiningCalculatorViewTemplate");
        }

        /// <summary>
        /// Determines whether this instance can execute the Edit command.
        /// </summary>
        /// <param name="parameter">The command parameter.</param>
        /// <returns>
        /// true if the command can be executed; otherwise returns false.
        /// </returns>
        private bool CanEditCalculationItem(CycleTimeCalculationItemViewModel parameter)
        {
            return parameter != null && parameter.EditMode != ViewModelEditMode.Edit;
        }

        /// <summary>
        /// Edits a calculation view model item.
        /// </summary>
        /// <param name="calculationItemToEdit">The item that will be edited.</param>
        private void EditCalculationItem(CycleTimeCalculationItemViewModel calculationItemToEdit)
        {
            if (calculationItemToEdit.MachiningType.Value != null &&
                IsCalculationSupported((MachiningType)calculationItemToEdit.MachiningType.Value))
            {
                MachiningCalculatorViewModel machiningCalculatorVM = container.GetExportedValue<MachiningCalculatorViewModel>();
                machiningCalculatorVM.EditMode = ViewModelEditMode.Edit;
                machiningCalculatorVM.PartName.Value = calculationItemToEdit.RawPartDescription.Value;
                machiningCalculatorVM.ParentUndoManager = this.UndoManager;
                machiningCalculatorVM.Model = calculationItemToEdit;
                calculationItemToEdit.EditMode = ViewModelEditMode.Edit;
                this.IsEditedAnyCalculationItem = true;
                this.editedCalculationItemsCount++;

                var viewWindowClosedHandler = new Action(() =>
                    {
                        calculationItemToEdit.EditMode = ViewModelEditMode.None;
                        this.editedCalculationItemsCount--;
                        if (editedCalculationItemsCount == 0)
                        {
                            this.IsEditedAnyCalculationItem = false;
                        }
                    });

                this.windowService.ShowViewInWindow(machiningCalculatorVM, "MachiningCalculatorViewTemplate", windowClosedAction: viewWindowClosedHandler);
            }
        }

        #endregion

        #region UndoManager handling

        /// <summary>
        /// Handle the Undo on CalculationViewModelItems.
        /// </summary>
        /// <param name="undoItem">The undoable item.</param>
        public void HandleUndo(UndoableItem undoItem)
        {
            if (undoItem == null)
            {
                return;
            }

            switch (undoItem.ActionType)
            {
                case UndoActionType.Insert:
                    {
                        this.calculatedCycleTime = 0;
                        foreach (var item in undoItem.Values)
                        {
                            var calculationVMItem = item as CycleTimeCalculationItemViewModel;
                            if (calculationVMItem != null)
                            {
                                calculatedCycleTime += calculationVMItem.TotalTime;

                                // Register the view model's save command.
                                this.SaveAllCommand.RegisterCommand(calculationVMItem.SaveCommand);
                                foreach (var calc in this.CalculationViewModelItems)
                                {
                                    if (calc.Index.Value >= calculationVMItem.Index.Value)
                                    {
                                        // Increase the index of the items with a grater index than the deleted one.
                                        calc.Index.Value += 1;
                                    }
                                }

                                // Add the calculation to deleted calculation list.
                                if (deletedCalculations.Contains(calculationVMItem))
                                {
                                    this.deletedCalculations.Remove(calculationVMItem);
                                }
                            }
                        }

                        // Update CycleTime value.
                        foreach (CycleTimeCalculationItemViewModel calculationViewModel in this.CalculationViewModelItems)
                        {
                            if (!undoItem.Values.Contains(calculationViewModel))
                            {
                                // Sum the total time value from all the calculation items.
                                this.calculatedCycleTime += calculationViewModel.TotalTime;
                            }
                        }

                        this.CycleTime = this.calculatedCycleTime / SelectedTimeUnit.ScaleFactor;
                        break;
                    }

                case UndoActionType.Delete:
                    {
                        foreach (var item in undoItem.Values)
                        {
                            var calculationVMItem = item as CycleTimeCalculationItemViewModel;
                            if (calculationVMItem != null)
                            {
                                this.SaveAllCommand.UnregisterCommand(calculationVMItem.SaveCommand);

                                foreach (var calc in this.CalculationViewModelItems)
                                {
                                    if (calc.Index.Value > calculationVMItem.Index.Value)
                                    {
                                        // Decrease the index of the items with a grater index than the deleted one.
                                        calc.Index.Value -= 1;
                                    }
                                }
                            }
                        }

                        // Update CycleTime value.
                        this.calculatedCycleTime = 0;
                        foreach (CycleTimeCalculationItemViewModel calculationViewModel in this.CalculationViewModelItems)
                        {
                            if (!undoItem.Values.Contains(calculationViewModel))
                            {
                                // Sum the total time value from all the calculation items.
                                this.calculatedCycleTime += calculationViewModel.TotalTime;
                            }
                        }

                        this.CycleTime = this.calculatedCycleTime / SelectedTimeUnit.ScaleFactor;
                        break;
                    }

                case UndoActionType.Move:
                    {
                        if (undoItem.OldStartingIndex < this.CalculationViewModelItems.Count
                            && undoItem.NewStartingIndex < this.CalculationViewModelItems.Count)
                        {
                            var movedItem = this.CalculationViewModelItems.ElementAt(undoItem.NewStartingIndex);
                            if (undoItem.OldStartingIndex == undoItem.NewStartingIndex + 1)
                            {
                                this.MoveDownAction(movedItem);
                            }

                            if (undoItem.OldStartingIndex + 1 == undoItem.NewStartingIndex)
                            {
                                this.MoveUpAction(movedItem);
                            }
                        }

                        break;
                    }

                default:
                    break;
            }
        }

        #endregion UndoManager handling
    }
}