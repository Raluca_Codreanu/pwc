﻿using System;
using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using ZPKTool.Business.MachiningCalculator;
using ZPKTool.Data;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the MachiningCalculatorSetupTime view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class MachiningCalculatorSetupTimeViewModel : ViewModel<CycleTimeCalculationItemViewModel>
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The machining calculator.
        /// </summary>
        private MachiningCalculator machiningCalculator;

        /// <summary>
        /// The selected setup description.
        /// </summary>
        private SetupType selectedDescription;

        /// <summary>
        /// The remarks.
        /// </summary>
        private string remarks;

        /// <summary>
        /// The setup time in the machine.
        /// </summary>
        private decimal? setupTimeInMachine;

        /// <summary>
        /// The setup time outside the machine.
        /// </summary>
        private decimal? setupTimeOutsideMachine;

        /// <summary>
        /// The net setup time.
        /// </summary>
        private decimal? netSetupTime;

        /// <summary>
        /// The gross setup time.
        /// </summary>
        private decimal? grossSetupTime;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="MachiningCalculatorSetupTimeViewModel" /> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        /// <param name="messenger">The messenger.</param>
        [ImportingConstructor]
        public MachiningCalculatorSetupTimeViewModel(IWindowService windowService, IMessenger messenger)
        {
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("messenger", messenger);

            this.windowService = windowService;
            this.messenger = messenger;

            InitializeProperties();
        }

        #region Properties

        /// <summary>
        /// Gets or sets the selected setup description.
        /// </summary>
        [UndoableProperty]
        public SetupType SelectedDescription
        {
            get { return this.selectedDescription; }
            set { this.SetProperty(ref this.selectedDescription, value, () => this.SelectedDescription); }
        }

        /// <summary>
        /// Gets or sets the remarks.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public string Remarks
        {
            get { return this.remarks; }
            set { this.SetProperty(ref this.remarks, value, () => this.Remarks); }
        }

        /// <summary>
        /// Gets or sets the setup time in the machine.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_SetupTimeInMachine", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? SetupTimeInMachine
        {
            get { return this.setupTimeInMachine; }
            set { this.SetProperty(ref this.setupTimeInMachine, value, () => this.SetupTimeInMachine, UpdateSetupTime); }
        }

        /// <summary>
        /// Gets or sets the setup time outside the machine.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_SetupTimeOutsideMachine", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? SetupTimeOutsideMachine
        {
            get { return this.setupTimeOutsideMachine; }
            set { this.SetProperty(ref this.setupTimeOutsideMachine, value, () => this.SetupTimeOutsideMachine, UpdateSetupTime); }
        }

        /// <summary>
        /// Gets or sets the net setup time.
        /// </summary>
        public decimal? NetSetupTime
        {
            get { return this.netSetupTime; }
            set { this.SetProperty(ref this.netSetupTime, value, () => this.NetSetupTime); }
        }

        /// <summary>
        /// Gets or sets the gross setup time.
        /// </summary>
        public decimal? GrossSetupTime
        {
            get { return this.grossSetupTime; }
            set { this.SetProperty(ref this.grossSetupTime, value, () => this.GrossSetupTime); }
        }

        /// <summary>
        /// Gets or sets the token used for sending messages to machining calculator.
        /// </summary>
        public Guid MessengerTokenForMachining { get; set; }

        /// <summary>
        /// Gets or sets the UndoManager from CycleTime parent window.
        /// </summary>
        public UndoManager ParentUndoManager { get; set; }

        #endregion

        /// <summary>
        /// Called when the Model has changed.
        /// </summary>
        protected override void OnModelChanged()
        {
            base.OnModelChanged();

            if ((this.Model != null) && this.Model.MachiningType.Value == MachiningType.PartPositioning && this.EditMode == ViewModelEditMode.Edit)
            {
                this.LoadSetupTimeDataFromCalculationToEdit();
            }
        }

        /// <summary>
        /// Initializes the properties of this instance.
        /// </summary>
        private void InitializeProperties()
        {
            this.machiningCalculator = new MachiningCalculator();

            this.SelectedDescription = SetupType.PartPositioning;
            this.SetupTimeInMachine = this.machiningCalculator.Knowledgebase.SetupTimeData.DefaultSetupTimeInTheMachine;
            this.SetupTimeOutsideMachine = this.machiningCalculator.Knowledgebase.SetupTimeData.DefaultSetupTimeOutsideTheMachine;
        }

        /// <summary>
        /// Updates the net and gross setup time.
        /// </summary>
        private void UpdateSetupTime()
        {
            using (this.UndoManager.Pause())
            {
                this.NetSetupTime = this.SetupTimeInMachine;
                this.GrossSetupTime = this.SetupTimeInMachine + this.SetupTimeOutsideMachine;
            }
        }

        /// <summary>
        /// Loads the setup time result data from the machining calculation data of calculation to edit.
        /// </summary>
        private void LoadSetupTimeDataFromCalculationToEdit()
        {
            if (this.Model.MachiningCalculationData == null ||
                this.Model.MachiningCalculationData.Value == null)
            {
                return;
            }

            UnicodeEncoding encoder = new UnicodeEncoding();
            string data = encoder.GetString(this.Model.MachiningCalculationData.Value);

            if (!string.IsNullOrEmpty(data))
            {
                string[] parts = data.Split(';');
                if (parts.Length == 4)
                {
                    SetupType descriptionNo;
                    if (Enum.TryParse(parts[0], out descriptionNo))
                    {
                        this.SelectedDescription = descriptionNo;
                    }

                    this.Remarks = parts[1];
                    this.SetupTimeInMachine = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[2]);
                    this.SetupTimeOutsideMachine = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[3]);
                }
            }
        }

        /// <summary>
        /// Performs the save operation. Executed by the SaveCommand.
        /// </summary>
        protected override void Save()
        {
            if (this.EditMode == ViewModelEditMode.Create)
            {
                // If the view model is not in edit mode, it does not have a model attached, so we have to create it.
                var calculation = new CycleTimeCalculation();
                var calculationItemVM = new CycleTimeCalculationItemViewModel(windowService, messenger);
                calculationItemVM.EditMode = ViewModelEditMode.Create;

                this.Model = calculationItemVM;
                calculationItemVM.Model = calculation;
            }

            this.SaveToModel();

            if (this.EditMode == ViewModelEditMode.Create)
            {
                var message = new NotificationMessage<CycleTimeCalculationItemViewModel>(MachiningCalculatorViewModel.CalculatorNotificationMessages.AddMachiningCalculator, this.Model);
                this.messenger.Send<NotificationMessage<CycleTimeCalculationItemViewModel>>(message, this.MessengerTokenForMachining);
            }
            else
            {
                var message = new NotificationMessage<CycleTimeCalculationItemViewModel>(MachiningCalculatorViewModel.CalculatorNotificationMessages.UpdateMachiningCalculator, this.Model);
                this.messenger.Send<NotificationMessage<CycleTimeCalculationItemViewModel>>(message, this.MessengerTokenForMachining);
            }
        }

        /// <summary>
        /// Executes the logic associated with the SaveToModelCommand command.
        /// </summary>
        protected override void SaveToModel()
        {
            if (this.ParentUndoManager != null)
            {
                using (this.ParentUndoManager.StartBatch(navigateToBatchControls: true))
                {
                    this.SaveSetupTimeData();
                }
            }
            else
            {
                this.SaveSetupTimeData();
            }
        }

        /// <summary>
        /// Saves all changed back into the model and resets the changed status.
        /// </summary>
        private void SaveSetupTimeData()
        {
            string description = this.SelectedDescription.ToString();

            if (!string.IsNullOrEmpty(this.Remarks))
            {
                description += " - " + this.Remarks;
            }

            this.Model.Description.Value = description;
            this.Model.MachiningVolume.Value = 0;
            this.Model.MachiningType.Value = MachiningType.PartPositioning;
            this.Model.TransportClampingTime.Value = 0;
            this.Model.MachiningTime.Value = 0;
            this.Model.ToolChangeTime.Value = this.GrossSetupTime.HasValue ? this.GrossSetupTime.Value : 0;

            SaveSetupTimeDataToBinary();
        }

        /// <summary>
        /// Saves the current setup time result data into the machining calculation data.
        /// </summary>
        private void SaveSetupTimeDataToBinary()
        {
            // The data will be saved as a comma separated list.
            string data = string.Empty;

            data += ((int)this.SelectedDescription).ToString() + ";";
            data += this.Remarks + ";";
            data += (this.SetupTimeInMachine.HasValue ? this.SetupTimeInMachine.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += this.SetupTimeOutsideMachine.HasValue ? this.SetupTimeOutsideMachine.Value.ToString(CultureInfo.InvariantCulture) : string.Empty;

            UnicodeEncoding encoding = new UnicodeEncoding();
            this.Model.MachiningCalculationData.Value = encoding.GetBytes(data);
        }
    }
}
