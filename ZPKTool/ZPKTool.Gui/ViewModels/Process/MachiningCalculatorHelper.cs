﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Contains helper methods for the MachiningCalculator classes.
    /// </summary>
    public static class MachiningCalculatorHelper
    {
        /// <summary>
        /// Converts a string representing a number in invariant culture format to the decimal representation of the number in the current culture format.
        /// This should be used when displaying numbers that were stored as strings.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <returns>The decimal representation of the number in the current culture.</returns>
        public static decimal? ConvertFromInvariantNumberRepresentation(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return null;
            }

            decimal number;
            if (!decimal.TryParse(value, NumberStyles.Any, CultureInfo.InvariantCulture, out number))
            {
                return null;
            }

            return number;
        }

        /// <summary>
        /// Converts a string representing a number in invariant culture format to the integer representation of the number in the current culture format.
        /// This should be used when displaying numbers that were stored as strings.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <returns>The integer representation of the number in the current culture.</returns>
        public static int? ConvertIntFromInvariantNumberRepresentation(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return null;
            }

            int number;
            if (!int.TryParse(value, NumberStyles.Any, CultureInfo.InvariantCulture, out number))
            {
                return null;
            }

            return number;
        }
    }
}
