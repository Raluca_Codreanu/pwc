﻿using System;
using System.ComponentModel.Composition;
using ZPKTool.Data;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    #region Enum

    /// <summary>
    /// The existing Machining Calculation Tabs.
    /// </summary>
    public enum SelectedMachiningCalculationTab
    {
        /// <summary>
        /// The Setup Time Tab.
        /// </summary>
        SetupTimeTab = 1,

        /// <summary>
        /// The Drilling Tab.
        /// </summary>
        DrillingTab = 2,

        /// <summary>
        /// The Turning Tab.
        /// </summary>
        TurningTab = 3,

        /// <summary>
        /// The Milling Tab.
        /// </summary>
        MillingTab = 4,

        /// <summary>
        /// The Gear Hobbing Tab.
        /// </summary>
        GearHobbingTab = 5,

        /// <summary>
        /// The Grinding Tab.
        /// </summary>
        GrindingTab = 6,
    }

    #endregion

    /// <summary>
    /// The view-model of the MachiningCalculator view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class MachiningCalculatorViewModel : ViewModel<CycleTimeCalculationItemViewModel>
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// A value indicating whether the machining calculator is in expert mode or not.
        /// </summary>
        private bool isExpertMode = false;

        /// <summary>
        /// The token object used for receiving messages from the view model's children.
        /// </summary>
        private Guid messengerToken;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="MachiningCalculatorViewModel" /> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        /// <param name="messenger">The messenger.</param>
        [ImportingConstructor]
        public MachiningCalculatorViewModel(IWindowService windowService, IMessenger messenger)
        {
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("messenger", messenger);

            this.windowService = windowService;
            this.messenger = messenger;

            this.SelectedTabIndex.Value = 0;

            messengerToken = Guid.NewGuid();
            this.messenger.Register<NotificationMessage<CycleTimeCalculationItemViewModel>>(this.HandleNotificationMessage, messengerToken);
        }

        #region Properties

        /// <summary>
        /// Gets or sets the MachiningCalculatorSetupTimeViewModel
        /// </summary>
        [Import]
        public MachiningCalculatorSetupTimeViewModel SetupTimeViewModel { get; set; }

        /// <summary>
        /// Gets or sets the MachiningCalculatorDrillingViewModel
        /// </summary>
        [Import]
        public MachiningCalculatorDrillingViewModel DrillingViewModel { get; set; }

        /// <summary>
        /// Gets or sets the MachiningCalculatorTurningViewModel
        /// </summary>
        [Import]
        public MachiningCalculatorTurningViewModel TurningViewModel { get; set; }

        /// <summary>
        /// Gets or sets the MachiningCalculatorMillingViewModel
        /// </summary>
        [Import]
        public MachiningCalculatorMillingViewModel MillingViewModel { get; set; }

        /// <summary>
        /// Gets or sets the MachiningCalculatorGearHobbingViewModel
        /// </summary>
        [Import]
        public MachiningCalculatorGearHobbingViewModel GearHobbingViewModel { get; set; }

        /// <summary>
        /// Gets or sets the MachiningCalculatorGrindingViewModel
        /// </summary>
        [Import]
        public MachiningCalculatorGrindingViewModel GrindingViewModel { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the machining calculator is in expert mode or not.
        /// </summary>
        [UndoableProperty]
        public bool IsExpertMode
        {
            get { return this.isExpertMode; }
            set { this.SetProperty(ref this.isExpertMode, value, () => this.IsExpertMode, OnExpertModeChanged); }
        }

        /// <summary>
        /// Gets or sets the part name.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public VMProperty<string> PartName { get; set; }

        /// <summary>
        /// Gets or sets the selected tab index.
        /// </summary>
        public VMProperty<int> SelectedTabIndex { get; set; }

        /// <summary>
        /// Gets or sets the token used for sending messages to cycle time calculator view model.
        /// </summary>
        public Guid MessengerTokenForCycleTime { get; set; }

        /// <summary>
        /// Gets or sets the UndoManager from CycleTime parent window.
        /// </summary>
        public UndoManager ParentUndoManager { get; set; }

        #endregion

        /// <summary>
        /// Called when the view has been loaded and starts loading the countries/suppliers from the database.
        /// <para/>        
        /// During unit tests this method must be manually called  because there is no view to call it.
        /// </summary>
        public override void OnLoaded()
        {
            base.OnLoaded();

            SetupTimeViewModel.EditMode = this.EditMode;
            SetupTimeViewModel.MessengerTokenForMachining = messengerToken;

            DrillingViewModel.IsExpertMode = this.IsExpertMode;
            DrillingViewModel.EditMode = this.EditMode;
            DrillingViewModel.MessengerTokenForMachining = messengerToken;

            TurningViewModel.IsExpertMode = this.IsExpertMode;
            TurningViewModel.EditMode = this.EditMode;
            TurningViewModel.MessengerTokenForMachining = messengerToken;

            MillingViewModel.IsExpertMode = this.IsExpertMode;
            MillingViewModel.EditMode = this.EditMode;
            MillingViewModel.MessengerTokenForMachining = messengerToken;

            GearHobbingViewModel.IsExpertMode = this.IsExpertMode;
            GearHobbingViewModel.EditMode = this.EditMode;
            GearHobbingViewModel.MessengerTokenForMachining = messengerToken;

            GrindingViewModel.IsExpertMode = this.IsExpertMode;
            GrindingViewModel.EditMode = this.EditMode;
            GrindingViewModel.MessengerTokenForMachining = messengerToken;

            this.InitializeUndoManager();
        }

        /// <summary>
        /// Called when the Model has changed.
        /// </summary>
        protected override void OnModelChanged()
        {
            if (this.EditMode == ViewModelEditMode.Edit)
            {
                SetupTimeViewModel.Model = this.Model;
                DrillingViewModel.Model = this.Model;
                TurningViewModel.Model = this.Model;
                MillingViewModel.Model = this.Model;
                GearHobbingViewModel.Model = this.Model;
                GrindingViewModel.Model = this.Model;

                EditCycleTimeCalculation(this.Model);
            }
        }

        /// <summary>
        /// Initializes the undo manager.
        /// </summary>
        private void InitializeUndoManager()
        {
            this.SetupTimeViewModel.UndoManager = this.UndoManager;
            this.DrillingViewModel.UndoManager = this.UndoManager;
            this.TurningViewModel.UndoManager = this.UndoManager;
            this.MillingViewModel.UndoManager = this.UndoManager;
            this.GearHobbingViewModel.UndoManager = this.UndoManager;
            this.GrindingViewModel.UndoManager = this.UndoManager;

            this.SetupTimeViewModel.ParentUndoManager = this.ParentUndoManager;
            this.DrillingViewModel.ParentUndoManager = this.ParentUndoManager;
            this.TurningViewModel.ParentUndoManager = this.ParentUndoManager;
            this.MillingViewModel.ParentUndoManager = this.ParentUndoManager;
            this.GearHobbingViewModel.ParentUndoManager = this.ParentUndoManager;
            this.GrindingViewModel.ParentUndoManager = this.ParentUndoManager;

            this.UndoManager.Start();
        }

        /// <summary>
        /// Called when the expert mode has changed.
        /// </summary>
        private void OnExpertModeChanged()
        {
            using (this.UndoManager.Pause())
            {
                DrillingViewModel.IsExpertMode = this.IsExpertMode;
                TurningViewModel.IsExpertMode = this.IsExpertMode;
                MillingViewModel.IsExpertMode = this.IsExpertMode;
                GearHobbingViewModel.IsExpertMode = this.IsExpertMode;
                GrindingViewModel.IsExpertMode = this.IsExpertMode;
            }
        }

        /// <summary>
        /// Sets the selected tab index according to the calculation's machining type.
        /// </summary>
        /// <param name="calculation">The calculation to edit.</param>
        private void EditCycleTimeCalculation(CycleTimeCalculationItemViewModel calculation)
        {
            if (calculation == null || calculation.MachiningType == null)
            {
                return;
            }

            try
            {
                MachiningType type = (MachiningType)calculation.MachiningType.Value;
                if (type == MachiningType.PartPositioning)
                {
                    this.SelectedTabIndex.Value = (int)SelectedMachiningCalculationTab.SetupTimeTab;
                }
                else if (type == MachiningType.Drilling)
                {
                    this.SelectedTabIndex.Value = (int)SelectedMachiningCalculationTab.DrillingTab;
                }
                else if (type == MachiningType.Turning)
                {
                    this.SelectedTabIndex.Value = (int)SelectedMachiningCalculationTab.TurningTab;
                }
                else if (type == MachiningType.Milling)
                {
                    this.SelectedTabIndex.Value = (int)SelectedMachiningCalculationTab.MillingTab;
                }
                else if (type == MachiningType.GearHobbing)
                {
                    this.SelectedTabIndex.Value = (int)SelectedMachiningCalculationTab.GearHobbingTab;
                }
                else if (type == MachiningType.Grinding)
                {
                    this.SelectedTabIndex.Value = (int)SelectedMachiningCalculationTab.GrindingTab;
                }
            }
            catch (Exception ex)
            {
                log.ErrorException("Error while trying to set the selected tab index for Machining Calculator's tab control", ex);
            }
        }

        /// <summary>
        /// Handles the notification message.
        /// </summary>
        /// <param name="message">The message.</param>
        private void HandleNotificationMessage(NotificationMessage<CycleTimeCalculationItemViewModel> message)
        {
            if (message.Notification == CalculatorNotificationMessages.AddMachiningCalculator)
            {
                // If one of the children view models added a new item, this model will contain that item and send it to cycle time calculator.
                this.Model = message.Content;
                this.Model.RawPartDescription.Value = this.PartName.Value;

                var msg = new NotificationMessage<CycleTimeCalculationItemViewModel>(MachiningCalculatorViewModel.CalculatorNotificationMessages.AddCalculationInCycleTime, this.Model);
                this.messenger.Send<NotificationMessage<CycleTimeCalculationItemViewModel>>(msg, this.MessengerTokenForCycleTime);
            }
            else if (message.Notification == CalculatorNotificationMessages.UpdateMachiningCalculator)
            {
                // If one of the children view models updated a new item, the item's raw part description will also be updated.
                this.Model.RawPartDescription.Value = this.PartName.Value;
            }
            else if (message.Notification == CalculatorNotificationMessages.ExpertModeChanged)
            {
                // If an item to edit has the expert mode set to true, this property is changed for the parent view model too.
                this.IsExpertMode = true;
            }
        }

        /// <summary>
        /// Closes the view associated with this instance.
        /// </summary>
        protected override void Close()
        {
            this.windowService.CloseViewWindow(this);
        }

        /// <summary>
        /// Called before unloading the view from its parent. Returning false will cancel the view's unloading.
        /// </summary>
        /// <returns>
        /// True if the unloading process should continue and false if it should be canceled.
        /// </returns>
        public override bool OnUnloading()
        {
            this.messenger.Unregister<NotificationMessage<CycleTimeCalculationItemViewModel>>(this.HandleNotificationMessage, messengerToken);
            return true;
        }

        #region Inner Classes

        /// <summary>
        /// The class that contains the notification messages sent by the children view models to the parent calculator.
        /// </summary>
        public static class CalculatorNotificationMessages
        {
            /// <summary>
            /// A value indicating that a new machining calculator item is added.
            /// </summary>
            public const string AddMachiningCalculator = "AddMachiningCalculator";

            /// <summary>
            /// A value indicating that a new calculation item is added in cycle time calculator.
            /// </summary>
            public const string AddCalculationInCycleTime = "AddCalculationInCycleTime";

            /// <summary>
            /// A value indicating that a machining calculator item is updated.
            /// </summary>
            public const string UpdateMachiningCalculator = "UpdateMachiningCalculator";

            /// <summary>
            /// A value indicating that the expert mode for an edited item is changed.
            /// </summary>
            public const string ExpertModeChanged = "ExpertModeChanged";

            /// <summary>
            /// A value indicating that the total time is changed.
            /// </summary>
            public const string TotalTimeChanged = "TotalTimeChanged";
        }

        #endregion
    }
}
