﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Windows.Input;
using System.Windows.Media;
using ZPKTool.Business;
using ZPKTool.Calculations.CO2Emissions;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Common;
using ZPKTool.Controls;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.ViewModels.ProcessInfrastructure;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for the process.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ProcessViewModel : ViewModel<Process, IDataSourceManager>
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The object instance used to synchronize threads using <see cref="lock"/> in screen.
        /// </summary>
        private readonly object lockObj = new object();

        /// <summary>
        /// The UI currency changed listener.
        /// </summary>
        private readonly WeakEventListener<UICurrencyChangedArgs> uiCurrencyChangedListener;

        /// <summary>
        /// The viewer UI currency changed listener.
        /// </summary>
        private readonly WeakEventListener<PropertyChangedEventArgs> viewerUICurrencyChangedListener;

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The model browser helper service.
        /// </summary>
        private IModelBrowserHelperService modelBrowserHelperService;

        /// <summary>
        /// The composition container.
        /// </summary>
        private CompositionContainer container;

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The reject overview cost of the model' cost
        /// </summary>
        private ProcessRejectOverview rejectOverviewCost;

        /// <summary>
        /// The project parent of the model
        /// </summary>
        private Project parentProject;

        /// <summary>
        /// The parent (assembly/part) of the model
        /// </summary>
        private object parent;

        /// <summary>
        /// It indicates if the raw parts cost can be shown or not
        /// </summary>
        private bool canShowRawPartsCost;

        /// <summary>
        /// It indicates if the raw materials cost can be shown or not
        /// </summary>
        private bool canShowRawMaterialsCost;

        /// <summary>
        /// It indicates if the parts cost can be shown or not
        /// </summary>
        private bool canShowPartsCost;

        /// <summary>
        /// It indicates if the scrap reject view can be shown or not
        /// The scrap reject view is not available for the versions older then 1.1.1
        /// </summary>
        private bool canShowScrapRejectView;

        /// <summary>
        /// The total material grid source items
        /// </summary>
        private DispatchedObservableCollection<ScrapViewTotalMaterialItem> totalMaterialItems;

        /// <summary>
        /// The scrap view source items
        /// Used to draw the scrap items
        /// </summary>
        private DispatchedObservableCollection<ScrapViewItem> scrapViewItems;

        /// <summary>
        /// The CO2 emission data grid source items
        /// </summary>
        private DispatchedObservableCollection<CO2EmissionDataGridItem> processCo2EmissionItems;

        /// <summary>
        /// The CO2 emission per part chart source items
        /// </summary>
        private Collection<LabeledPieChartItem> co2EmissionsPerPartItems;

        /// <summary>
        /// The total emission quantity for the CO2 emission per part chart
        /// </summary>
        private decimal co2EmissionsPerPartTotal;

        /// <summary>
        /// The CO2 emission chart fossil source items
        /// </summary>
        private DispatchedObservableCollection<CO2ChartItem> co2ChartFossilItems;

        /// <summary>
        /// The CO2 emission chart renewable source items
        /// </summary>
        private DispatchedObservableCollection<CO2ChartItem> co2ChartRenewableItems;

        /// <summary>
        /// It indicates if the casting and moulding wizards is available or not
        /// </summary>
        private bool canShowWizards;

        /// <summary>
        /// It indicates if are calculation errors or not
        /// </summary>
        private bool existsCalculationErrors;

        /// <summary>
        /// The selected item from the CO2 emission data grid
        /// </summary>
        private CO2EmissionDataGridItem selectedProcessCo2EmissionItem;

        /// <summary>
        /// The selected item from the CO2 emission by process chart
        /// </summary>
        private CO2ChartItem seletedCO2EmissionsByProcessChartItem;

        /// <summary>
        /// The process step widgets source items
        /// </summary>
        private DispatchedObservableCollection<ProcessStepWidgetViewModel> processStepWidgetItems;

        /// <summary>
        /// The process step view models source items
        /// </summary>
        private DispatchedObservableCollection<ProcessStepViewModel> processStepViewModelItems;

        /// <summary>
        /// The current process step widget that is selected
        /// </summary>
        private ProcessStepWidgetViewModel selectedProcessStepWidget;

        /// <summary>
        /// The current process step view model that is selected
        /// </summary>
        private ProcessStepViewModel selectedProcessStepViewModel;

        /// <summary>
        /// The cost for the process step that is selected
        /// </summary>
        private decimal selectedProcessStepCost;

        /// <summary>
        /// The result on data check
        /// </summary>
        private EntityDataCheckResult dataCheckResult;

        /// <summary>
        /// A value indicating whether the panel with the data check results is opened or not.
        /// </summary>
        private bool showEntityDataCheckResult;

        /// <summary>
        /// The current calculation result of the process
        /// </summary>
        private CalculationResult internalCalculationResult;

        /// <summary>
        /// The current selected process step tab on process step view model
        /// </summary>
        private int currentSelectedProcessStepTab;

        /// <summary>
        /// The process steps media from process.
        /// </summary>
        private Dictionary<Guid, Media> stepsMedia = new Dictionary<Guid, Media>();

        /// <summary>
        /// Contains the cost of each part and assembly found in the process's parent object.
        /// It is used to display the cost of the parts/assemblies in the Parts list.
        /// It is populated only if the parent object is an Assembly (there is no use for it in case of Parts).
        /// </summary>
        private Dictionary<Guid, CalculationResult> subpartsCostCache = new Dictionary<Guid, CalculationResult>();

        /// <summary>
        /// The internal message token used to communicate with and between subcomponents
        /// </summary>
        private string internalMessageToken;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        /// <summary>
        /// The please wait service.
        /// </summary>
        private IPleaseWaitService pleaseWaitService;

        /// <summary>
        /// A value indicating whether the process data is loaded.
        /// </summary>
        private bool isDataLoaded;

        #endregion Attributes

        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessViewModel"/> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        /// <param name="messenger">The messenger.</param>
        /// <param name="container">The composition container.</param>
        /// <param name="modelBrowserHelperService">The model browser helper service.</param>
        /// <param name="costRecalculationCloneManager">The cost recalculation clone manager.</param>
        /// <param name="unitsService">The units service.</param>
        /// <param name="pleaseWaitService">The please wait service.</param>
        [ImportingConstructor]
        public ProcessViewModel(
            IWindowService windowService,
            IMessenger messenger,
            CompositionContainer container,
            IModelBrowserHelperService modelBrowserHelperService,
            ICostRecalculationCloneManager costRecalculationCloneManager,
            IUnitsService unitsService,
            IPleaseWaitService pleaseWaitService)
        {
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("container", container);
            Argument.IsNotNull("modelBrowserHelperService", modelBrowserHelperService);
            Argument.IsNotNull("unitsService", unitsService);
            Argument.IsNotNull("pleaseWaitService", pleaseWaitService);
            Argument.IsNotNull("costRecalculationCloneManager", costRecalculationCloneManager);

            this.windowService = windowService;
            this.messenger = messenger;
            this.container = container;
            this.modelBrowserHelperService = modelBrowserHelperService;
            this.unitsService = unitsService;
            this.pleaseWaitService = pleaseWaitService;
            this.CloneManager = costRecalculationCloneManager;

            this.InitializeUndoManager();
            this.InitializeCommands();
            this.InitializeProperties();

            this.internalMessageToken = Guid.NewGuid().ToString();
            this.messenger.Register<CurrentComponentCostChangedMessage>(this.CurrentProcessStepCostChanged, this.internalMessageToken);
            this.messenger.Register<ProcessStepChangedMessage>(this.HandleProcessStepChanged, this.internalMessageToken);
            this.messenger.Register<NotificationMessage>(this.HandleNotificationMessage, this.internalMessageToken);

            this.currentSelectedProcessStepTab = (int)SelectedProcessStepTab.InformationTab;

            this.uiCurrencyChangedListener = new WeakEventListener<UICurrencyChangedArgs>(this.HandleUICurrencyChanged);
            UICurrencyChangedEventManager.AddListener(SecurityManager.Instance, uiCurrencyChangedListener);

            this.viewerUICurrencyChangedListener = new WeakEventListener<PropertyChangedEventArgs>(this.HandleViewerUICurrencyChanged);
            PropertyChangedEventManager.AddListener(
                UserSettingsManager.Instance,
                viewerUICurrencyChangedListener,
                ReflectionUtils.GetPropertyName(() => UserSettingsManager.Instance.ViewerUICurrencyIsoCode));
        }

        #region Commands

        /// <summary>
        /// Gets or sets the command that save all changes in the nested view models back into their models.
        /// This command aggregates the SaveToModel commands of the nested view models.
        /// </summary>
        private CompositeCommand SaveNestedViewModels { get; set; }

        /// <summary>
        /// Gets or sets the command that cancels all changes of the nested view models.
        /// This command aggregates the Cancel commands of the nested view models.
        /// </summary>
        private CompositeCommand CancelNestedViewModels { get; set; }

        /// <summary>
        /// Gets or sets the command for adding a new process step
        /// </summary>
        public ICommand AddStepCommand { get; set; }

        /// <summary>
        /// Gets or sets the command for removing the selected process step
        /// </summary>
        public ICommand RemoveStepCommand { get; set; }

        /// <summary>
        /// Gets or sets the command to add moulding
        /// </summary>
        public ICommand AddMouldingCommand { get; set; }

        /// <summary>
        /// Gets or sets the command to add casting
        /// </summary>
        public ICommand AddCastingCommand { get; set; }

        /// <summary>
        /// Gets or sets the command for the process step drop event
        /// </summary>
        public ICommand DropProcessStepCommand { get; set; }

        /// <summary>
        /// Gets or sets the command for the process step drop before target process step.
        /// </summary>
        public ICommand DropBeforeProcessStepCommand { get; set; }

        /// <summary>
        /// Gets or sets the command for the process step drop before target process step.
        /// </summary>
        public ICommand DropEntityOnProcessStepCommand { get; set; }

        /// <summary>
        /// Gets or sets the command for opening the panel containing data check results.
        /// </summary>
        public ICommand ShowEntityDataCheckResultCommand { get; set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets or sets the reject overview cost of the model' cost
        /// </summary>
        public ProcessRejectOverview RejectOverviewCost
        {
            get { return this.rejectOverviewCost; }
            set { this.SetProperty(ref this.rejectOverviewCost, value, () => this.RejectOverviewCost); }
        }

        /// <summary>
        /// Gets or sets the project parent of the model
        /// </summary>
        public Project ParentProject
        {
            get { return this.parentProject; }
            set { this.SetProperty(ref this.parentProject, value, () => this.ParentProject); }
        }

        /// <summary>
        /// Gets or sets the parent assembly/part to which the Model assembly belongs.
        /// </summary>
        public object Parent
        {
            get { return this.parent; }
            set { this.SetProperty(ref this.parent, value, () => this.Parent); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the raw materials cost can be shown or not
        /// </summary>
        public bool CanShowRawMaterialsCost
        {
            get { return this.canShowRawMaterialsCost; }
            set { this.SetProperty(ref this.canShowRawMaterialsCost, value, () => this.CanShowRawMaterialsCost); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the raw part cost can be shown or not
        /// </summary>
        public bool CanShowRawPartsCost
        {
            get { return this.canShowRawPartsCost; }
            set { this.SetProperty(ref this.canShowRawPartsCost, value, () => this.CanShowRawPartsCost); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the parts cost can be shown or not
        /// </summary>
        public bool CanShowPartsCost
        {
            get { return this.canShowPartsCost; }
            set { this.SetProperty(ref this.canShowPartsCost, value, () => this.CanShowPartsCost); }
        }

        /// <summary>
        /// Gets or sets the total material grid source items
        /// </summary>
        public DispatchedObservableCollection<ScrapViewTotalMaterialItem> TotalMaterialItems
        {
            get { return this.totalMaterialItems; }
            set { this.SetProperty(ref this.totalMaterialItems, value, () => this.TotalMaterialItems); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the scrap reject view can be shown or not
        /// The scrap reject view is not available for the versions older then 1.1.1
        /// </summary>
        public bool CanShowScrapRejectView
        {
            get { return this.canShowScrapRejectView; }
            set { this.SetProperty(ref this.canShowScrapRejectView, value, () => this.CanShowScrapRejectView); }
        }

        /// <summary>
        /// Gets or sets the scrap view source items
        /// </summary>
        public DispatchedObservableCollection<ScrapViewItem> ScrapViewItems
        {
            get { return this.scrapViewItems; }
            set { this.SetProperty(ref this.scrapViewItems, value, () => this.ScrapViewItems); }
        }

        /// <summary>
        /// Gets or sets the CO2 emission data grid source items
        /// </summary>
        public DispatchedObservableCollection<CO2EmissionDataGridItem> ProcessCo2EmissionItems
        {
            get { return this.processCo2EmissionItems; }
            set { this.SetProperty(ref this.processCo2EmissionItems, value, () => this.ProcessCo2EmissionItems); }
        }

        /// <summary>
        /// Gets or sets the CO2 emission per part chart's source items
        /// </summary>
        public Collection<LabeledPieChartItem> Co2EmissionsPerPartItems
        {
            get { return this.co2EmissionsPerPartItems; }
            set { this.SetProperty(ref this.co2EmissionsPerPartItems, value, () => this.Co2EmissionsPerPartItems); }
        }

        /// <summary>
        /// Gets or sets the CO2 emission per part chart's total value
        /// </summary>
        public decimal Co2EmissionsPerPartTotal
        {
            get { return this.co2EmissionsPerPartTotal; }
            set { this.SetProperty(ref this.co2EmissionsPerPartTotal, value, () => this.Co2EmissionsPerPartTotal); }
        }

        /// <summary>
        /// Gets or sets the CO2 emission fossil chart's source items
        /// </summary>
        public DispatchedObservableCollection<CO2ChartItem> CO2ChartFossilItems
        {
            get { return this.co2ChartFossilItems; }
            set { this.SetProperty(ref this.co2ChartFossilItems, value, () => this.CO2ChartFossilItems); }
        }

        /// <summary>
        /// Gets or sets the CO2 emission renewable chart's source items
        /// </summary>
        public DispatchedObservableCollection<CO2ChartItem> CO2ChartRenewableItems
        {
            get { return this.co2ChartRenewableItems; }
            set { this.SetProperty(ref this.co2ChartRenewableItems, value, () => this.CO2ChartRenewableItems); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the casting and moulding wizards are available or not
        /// </summary>
        public bool CanShowWizards
        {
            get { return this.canShowWizards; }
            set { this.SetProperty(ref this.canShowWizards, value, () => this.CanShowWizards); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether are calculation errors or not
        /// </summary>
        public bool ExistsCalculationErrors
        {
            get { return this.existsCalculationErrors; }
            set { this.SetProperty(ref this.existsCalculationErrors, value, () => this.ExistsCalculationErrors); }
        }

        /// <summary>
        /// Gets or sets the selected item from the CO2 emission data grid
        /// </summary>
        public CO2EmissionDataGridItem SelectedProcessCo2EmissionItem
        {
            get { return this.selectedProcessCo2EmissionItem; }
            set { this.SetProperty(ref this.selectedProcessCo2EmissionItem, value, () => this.SelectedProcessCo2EmissionItem, () => this.SelectedCO2EmissionDataGridItemChanged()); }
        }

        /// <summary>
        /// Gets or sets the selected item from the CO2 emission by process chart
        /// </summary>
        public CO2ChartItem SeletedCO2EmissionsByProcessChartItem
        {
            get { return this.seletedCO2EmissionsByProcessChartItem; }
            set { this.SetProperty(ref this.seletedCO2EmissionsByProcessChartItem, value, () => this.SeletedCO2EmissionsByProcessChartItem, () => this.SeletedCO2EmissionsByProcessChartItemChanged()); }
        }

        /// <summary>
        /// Gets or sets the process step widgets source items.
        /// </summary>
        public DispatchedObservableCollection<ProcessStepWidgetViewModel> ProcessStepWidgetItems
        {
            get { return this.processStepWidgetItems; }
            set { this.SetProperty(ref this.processStepWidgetItems, value, () => this.ProcessStepWidgetItems); }
        }

        /// <summary>
        /// Gets or sets the process step view models source items
        /// </summary>
        [UndoableProperty]
        public DispatchedObservableCollection<ProcessStepViewModel> ProcessStepViewModelItems
        {
            get { return this.processStepViewModelItems; }
            set { this.SetProperty(ref this.processStepViewModelItems, value, () => this.ProcessStepViewModelItems); }
        }

        /// <summary>
        /// Gets or sets the current process step widget that is selected
        /// </summary>
        public ProcessStepWidgetViewModel SelectedProcessStepWidget
        {
            get
            {
                return this.selectedProcessStepWidget;
            }

            set
            {
                if (this.selectedProcessStepWidget != value)
                {
                    this.OnPropertyChanging(() => this.SelectedProcessStepWidget);

                    // Unselect the old process step widget selection
                    if (this.selectedProcessStepWidget != null)
                    {
                        this.selectedProcessStepWidget.IsSelected = false;
                    }

                    this.selectedProcessStepWidget = value;

                    // Select the new widget
                    if (this.selectedProcessStepWidget != null)
                    {
                        this.selectedProcessStepWidget.IsSelected = true;
                    }

                    // Synchronize the widget selection with the selected process step view model
                    this.SynchronizeProcessStepSelection(value);

                    this.OnPropertyChanged(() => this.SelectedProcessStepWidget);
                }
            }
        }

        /// <summary>
        /// Gets or sets the current process step view model that is selected
        /// </summary>
        public ProcessStepViewModel SelectedProcessStepViewModel
        {
            get { return this.selectedProcessStepViewModel; }
            set { this.SetProperty(ref this.selectedProcessStepViewModel, value, () => this.SelectedProcessStepViewModel); }
        }

        /// <summary>
        /// Gets or sets the cost for the process step that is selected
        /// </summary>
        public decimal SelectedProcessStepCost
        {
            get { return this.selectedProcessStepCost; }
            set { this.SetProperty(ref this.selectedProcessStepCost, value, () => this.SelectedProcessStepCost); }
        }

        /// <summary>
        /// Gets or sets the result on data check
        /// </summary>
        public EntityDataCheckResult DataCheckResult
        {
            get { return this.dataCheckResult; }
            set { this.SetProperty(ref this.dataCheckResult, value, () => this.DataCheckResult); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the panel with the data check results is opened or not.
        /// </summary>
        public bool ShowEntityDataCheckResult
        {
            get { return this.showEntityDataCheckResult; }
            set { this.SetProperty(ref this.showEntityDataCheckResult, value, () => this.ShowEntityDataCheckResult); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the process data is loaded.
        /// </summary>
        public bool IsDataLoaded
        {
            get { return this.isDataLoaded; }
            set { this.SetProperty(ref this.isDataLoaded, value, () => this.IsDataLoaded); }
        }

        /// <summary>
        /// Gets or sets the process' steps. This collection is used internally in all operations involving process steps instead of the Model's Steps collection.
        /// It is synced with the Model's Steps collection in SaveToModel method.
        /// <para />
        /// It should not be reset to a new instance; instead it should be emptied and re-populated when necessary.
        /// </summary>
        private List<ProcessStep> ProcessSteps { get; set; }

        /// <summary>
        /// Gets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter { get; private set; }

        #endregion Properties

        #region Initialization

        /// <summary>
        /// Initializes the view-model's commands.
        /// </summary>
        private void InitializeCommands()
        {
            this.AddStepCommand = new DelegateCommand(this.AddStep, () => !this.IsReadOnly);
            this.RemoveStepCommand = new DelegateCommand(this.RemoveSelectedStep, () => !this.IsReadOnly);
            this.AddMouldingCommand = new DelegateCommand(this.AddMoulding, () => !this.IsReadOnly);
            this.AddCastingCommand = new DelegateCommand(this.AddCasting, () => !this.IsReadOnly);
            this.DropProcessStepCommand = new DelegateCommand<Tuple<object, object>>((obj) => this.DropProcessStep(obj, false), (t) => !this.IsReadOnly);
            this.DropBeforeProcessStepCommand = new DelegateCommand<Tuple<object, object>>((obj) => this.DropProcessStep(obj, true), (t) => !this.IsReadOnly);
            this.DropEntityOnProcessStepCommand = new DelegateCommand<Tuple<object, object>>((obj) => this.DropEntityOnProcessStep(obj), (t) => !this.IsReadOnly);
            this.ShowEntityDataCheckResultCommand = new DelegateCommand(() => this.ShowEntityDataCheckResult = true);

            this.SaveNestedViewModels = new CompositeCommand();
            this.CancelNestedViewModels = new CompositeCommand();
        }

        /// <summary>
        /// Initializes the properties of this instance.
        /// </summary>
        private void InitializeProperties()
        {
            this.ProcessStepWidgetItems = new DispatchedObservableCollection<ProcessStepWidgetViewModel>();
            this.ProcessStepViewModelItems = new DispatchedObservableCollection<ProcessStepViewModel>();
            this.CO2ChartFossilItems = new DispatchedObservableCollection<CO2ChartItem>();
            this.CO2ChartRenewableItems = new DispatchedObservableCollection<CO2ChartItem>();
            this.ScrapViewItems = new DispatchedObservableCollection<ScrapViewItem>();
            this.ProcessCo2EmissionItems = new DispatchedObservableCollection<CO2EmissionDataGridItem>();
            this.Co2EmissionsPerPartItems = new Collection<LabeledPieChartItem>();
            this.ProcessSteps = new List<ProcessStep>();
        }

        /// <summary>
        /// Initialize the undo manager.
        /// </summary>
        private void InitializeUndoManager()
        {
            this.UndoManager.RegisterPropertyAction(() => this.ProcessStepViewModelItems, this.HandleUndoOnProcessStepsCollection, this, true);
        }

        #endregion Initialization

        #region Message Handling

        /// <summary>
        /// Handle the CurrentComponentCostChanged message and refreshes the costs
        /// </summary>
        /// <param name="msg">The message</param>
        private void CurrentProcessStepCostChanged(CurrentComponentCostChangedMessage msg)
        {
            // Need synchronize threads because this method is triggered by multiple threads when the cancel command is executed
            // because the nested view models are refreshing their calculation after executing the cancel command
            lock (lockObj)
            {
                var result = msg.Content;
                if (result != null && this.SelectedProcessStepViewModel != null && this.SelectedProcessStepViewModel.Model != null)
                {
                    this.internalCalculationResult = result;
                    var cost = result.ProcessCost.StepCosts.FirstOrDefault(c => c.StepId == this.SelectedProcessStepViewModel.Model.Guid);
                    if (cost != null)
                    {
                        this.SelectedProcessStepCost = cost.TotalManufacturingCost;
                    }

                    this.RefreshScrapViewTab();
                    this.RefreshCO2EmissionsTab();

                    // Notify that the cost is changed
                    this.messenger.Send(msg, this.IsInViewerMode ? GlobalMessengerTokens.ModelBrowserTargetToken : GlobalMessengerTokens.MainViewTargetToken);
                }
            }
        }

        /// <summary>
        /// Handle the ProcessStepChanged message and refreshes the costs
        /// </summary>
        /// <param name="msg">The message</param>
        private void HandleProcessStepChanged(ProcessStepChangedMessage msg)
        {
            // Check if the step that has changed belongs to this process.
            if (msg == null || this.ProcessSteps.FirstOrDefault(s => s.Guid == msg.StepID) == null)
            {
                return;
            }

            using (this.UndoManager.Pause())
            {
                if (msg.ChangeType == ProcessStepChangeType.NameUpdated)
                {
                    var newStepName = string.Empty;

                    if (msg.Value != null && msg.Value is string)
                    {
                        newStepName = msg.Value as string;
                    }

                    var stepId = msg.StepID;

                    var scrapItem = this.ScrapViewItems.FirstOrDefault((s) => s.StepId == stepId);
                    if (scrapItem != null)
                    {
                        scrapItem.StepName = newStepName;
                    }

                    var co2DataGridItem = this.ProcessCo2EmissionItems.FirstOrDefault((s) => s.ProcessStepId == stepId);
                    if (co2DataGridItem != null)
                    {
                        co2DataGridItem.ProcessStepName = newStepName;
                    }

                    var co2FossilItem = this.CO2ChartFossilItems.FirstOrDefault((f) => f.Id == stepId);
                    if (co2FossilItem != null)
                    {
                        co2FossilItem.Name = newStepName;
                    }

                    var co2RenewableItem = this.CO2ChartRenewableItems.FirstOrDefault((f) => f.Id == stepId);
                    if (co2RenewableItem != null)
                    {
                        co2RenewableItem.Name = newStepName;
                    }

                    this.AnalyzeDataCheckErrors();
                }
                else if (msg.ChangeType == ProcessStepChangeType.StepItemUpdated)
                {
                    var assyItem = msg.SubEntity as PartsGridAssemblyItem;
                    if (assyItem != null)
                    {
                        foreach (var stepVM in this.ProcessStepViewModelItems)
                        {
                            if (stepVM.ModelClone.Guid != msg.StepID)
                            {
                                var gridItem = stepVM.StepParts.FirstOrDefault(s => s.GetType() == typeof(PartsGridAssemblyItem) && ((PartsGridAssemblyItem)s).Assembly.Guid == assyItem.Assembly.Guid);
                                if (gridItem != null)
                                {
                                    gridItem.IsRaisePropertyChangedSuspended = true;

                                    gridItem.IsExternal = assyItem.IsExternal;
                                    gridItem.HasExternalSGA = assyItem.HasExternalSGA;
                                    gridItem.AmountSum = gridItem.AmountSum - assyItem.AmountBeforeEdit + assyItem.Amount.Value;

                                    gridItem.IsRaisePropertyChangedSuspended = false;
                                }
                            }
                        }
                    }
                    else
                    {
                        var partitem = msg.SubEntity as PartsGridPartItem;
                        if (partitem != null)
                        {
                            foreach (var stepVM in this.ProcessStepViewModelItems)
                            {
                                if (stepVM.ModelClone.Guid != msg.StepID)
                                {
                                    var gridItem = stepVM.StepParts.FirstOrDefault(s => s.GetType() == typeof(PartsGridPartItem) && ((PartsGridPartItem)s).Part.Guid == partitem.Part.Guid);
                                    if (gridItem != null)
                                    {
                                        gridItem.IsRaisePropertyChangedSuspended = true;

                                        gridItem.IsExternal = partitem.IsExternal;
                                        gridItem.HasExternalSGA = partitem.HasExternalSGA;
                                        gridItem.AmountSum = gridItem.AmountSum - partitem.AmountBeforeEdit + partitem.Amount.Value;

                                        gridItem.IsRaisePropertyChangedSuspended = false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Handle the NavigateToEntityIntoProcessScreen message and tries to navigates to the entity from message
        /// </summary>
        /// <param name="msg">The message</param>
        private void HandleNavigateToEntityMessage(NotificationMessage<object> msg)
        {
            if (msg.Content == null)
            {
                return;
            }

            if (msg.Notification == Notification.NavigateToEntityIntoProcessScreen)
            {
                var processStep = msg.Content as ProcessStep;
                if (processStep != null)
                {
                    this.TrySelectProcessStep(processStep);
                }
                else
                {
                    this.NavigateToEntityMessage(msg.Content);
                }
            }
        }

        /// <summary>
        /// Handles some generic messages, of type NotificationMessage.
        /// </summary>
        /// <param name="msg">The message.</param>
        private void HandleNotificationMessage(NotificationMessage msg)
        {
            // If the process step changed then check the data for errors
            if (msg.Notification == Notification.ProcessStepChanged)
            {
                this.AnalyzeDataCheckErrors();
            }
        }

        #endregion Message Handling

        #region Property change handlers

        /// <summary>
        /// Called when the Model has changed.
        /// </summary>
        protected override void OnModelChanged()
        {
            this.IsDataLoaded = false;

            if (this.IsInViewerMode)
            {
                this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(null);
            }

            Action<PleaseWaitService.WorkParams> work = (workParams) =>
            {
                this.CheckDataSource();

                this.LoadDataFromModel();
                this.CloneManager.Clone(this);

                this.internalCalculationResult = CalculateParentCost();
                this.CalculateAndCacheSubpartsCost();

                // Get the process step and its machines media
                if (!this.IsInViewerMode)
                {
                    MediaManager mediaManager = new MediaManager(this.DataSourceManager);
                    this.stepsMedia = mediaManager.GetPicturesForProcessSteps(this.Model.Steps.OrderBy(s => s.Index));
                }

                this.RefreshInternal();

                this.messenger.Register<NotificationMessage<object>>(this.HandleNavigateToEntityMessage, this.IsInViewerMode ? GlobalMessengerTokens.ModelBrowserTargetToken : GlobalMessengerTokens.MainViewTargetToken);
                this.AnalyzeDataCheckErrors();

                this.UndoManager.Reset();
                this.UndoManager.Start();
            };

            Action<PleaseWaitService.WorkParams> workCompleted = (workParams) =>
                {
                    if (workParams.Error != null)
                    {
                        this.windowService.MessageDialogService.Show(workParams.Error);
                    };

                    this.IsDataLoaded = true;
                };

            this.pleaseWaitService.Show(LocalizedResources.General_WaitLoadingProcess, (int)TimeSpan.FromSeconds(1).TotalMilliseconds, work, workCompleted);
        }

        /// <summary>
        /// Called when the DataSourceManager changed.
        /// </summary>
        protected override void OnDataSourceManagerChanged()
        {
            base.OnDataSourceManagerChanged();

            this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(this.DataSourceManager);
        }

        /// <summary>
        /// Refreshes the model clone of the process and its process steps
        /// </summary>
        private void RefreshModelClone()
        {
            this.CloneManager.Clone(this);

            foreach (var item in this.ProcessStepViewModelItems)
            {
                var processStep = this.ModelClone.Steps.FirstOrDefault(ps => ps.Guid == item.Model.Guid);
                if (processStep != null)
                {
                    // Refresh all the process step view model clones to remove any changes done to them because are all used in calculations
                    item.ModelClone = processStep;
                    item.ModelParentClone = this.ModelParentClone;
                }
            }
        }

        /// <summary>
        /// Called after the CO2 emission data grid selection changes
        /// It synchronizes the selection from the data grid with the CO2 emission by process chart 
        /// </summary>
        private void SelectedCO2EmissionDataGridItemChanged()
        {
            if (this.SelectedProcessCo2EmissionItem != null)
            {
                var newChartSelectedItem = this.CO2ChartFossilItems.FirstOrDefault(p => p.Id == this.SelectedProcessCo2EmissionItem.ProcessStepId);
                if (newChartSelectedItem != null)
                {
                    this.SeletedCO2EmissionsByProcessChartItem = newChartSelectedItem;
                }
            }
        }

        /// <summary>
        /// Called after the CO2 emission by process chart selection changes
        /// It synchronizes the selection from the chart with the CO2 emission data grid 
        /// </summary>
        private void SeletedCO2EmissionsByProcessChartItemChanged()
        {
            if (this.SeletedCO2EmissionsByProcessChartItem != null)
            {
                var newDataGridSelectedItem = this.ProcessCo2EmissionItems.FirstOrDefault(p => p.ProcessStepId == this.SeletedCO2EmissionsByProcessChartItem.Id);
                if (newDataGridSelectedItem != null)
                {
                    this.SelectedProcessCo2EmissionItem = newDataGridSelectedItem;
                }
            }
        }

        /// <summary>
        /// Called after the selected process step widget changes
        /// It synchronizes the selected process step widget with its corresponding process step view model
        /// </summary>
        /// <param name="value">The process step widget view model to synchronize with</param>
        private void SynchronizeProcessStepSelection(ProcessStepWidgetViewModel value)
        {
            // Save the current selected process step tab on SelectedProcessStepViewModel
            if (this.SelectedProcessStepViewModel != null)
            {
                this.currentSelectedProcessStepTab = this.SelectedProcessStepViewModel.SelectedTabIndex.Value;
            }

            // Synchronize the widget selection with the selected process step view model
            if (value != null)
            {
                this.SelectedProcessStepViewModel = this.ProcessStepViewModelItems.FirstOrDefault(v => v.Model.Guid == value.Model.Guid);

                if (this.currentSelectedProcessStepTab != (int)SelectedProcessStepTab.InformationTab
                    && this.currentSelectedProcessStepTab != (int)SelectedProcessStepTab.PartsTab
                    && this.SelectedProcessStepViewModel.Accuracy.Value == ProcessCalculationAccuracy.Estimated)
                {
                    this.SelectedProcessStepViewModel.SelectedTabIndex.Value = (int)SelectedProcessStepTab.InformationTab;
                }
                else
                {
                    this.SelectedProcessStepViewModel.SelectedTabIndex.Value = this.currentSelectedProcessStepTab;
                }
            }
            else
            {
                this.SelectedProcessStepViewModel = null;
            }

            // Refresh the cost of the selected process step
            if (this.SelectedProcessStepViewModel != null)
            {
                if (this.internalCalculationResult == null)
                {
                    this.internalCalculationResult = CalculateParentCost();
                }

                if (this.internalCalculationResult != null)
                {
                    var cost = this.internalCalculationResult.ProcessCost.StepCosts.FirstOrDefault(c => c.StepId == this.SelectedProcessStepViewModel.Model.Guid);
                    if (cost != null)
                    {
                        this.SelectedProcessStepCost = cost.TotalManufacturingCost;
                    }
                }
            }
            else
            {
                this.SelectedProcessStepCost = 0m;
            }
        }

        /// <summary>
        /// Handles the UI currency change.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void HandleUICurrencyChanged(object sender, UICurrencyChangedArgs e)
        {
            if (e.NewCurrency != null
                && e.NewCurrency != e.OldCurrency)
            {
                this.RefreshScrapViewTab();
            }
        }

        /// <summary>
        /// Handles the viewer UI currency change.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void HandleViewerUICurrencyChanged(object sender, PropertyChangedEventArgs e)
        {
            this.RefreshScrapViewTab();
        }

        #endregion Property change handlers

        #region Command handling

        /// <summary>
        /// Add a new process step
        /// </summary>
        private void AddStep()
        {
            // Initialize a new process step and set its owner.
            ProcessStep newStep = null;

            var assy = this.Parent as Assembly;
            if (assy != null)
            {
                newStep = new AssemblyProcessStep();
                newStep.IsMasterData = assy.IsMasterData;
                if (!assy.IsMasterData)
                {
                    var owner = this.DataSourceManager.UserRepository.GetById(SecurityManager.Instance.CurrentUser.Guid, false);
                    newStep.Owner = owner;
                }
            }
            else
            {
                var part = this.Parent as Part;
                if (part != null)
                {
                    newStep = new PartProcessStep();
                    newStep.IsMasterData = part.IsMasterData;
                    if (!part.IsMasterData)
                    {
                        var owner = this.DataSourceManager.UserRepository.GetById(SecurityManager.Instance.CurrentUser.Guid, false);
                        newStep.Owner = owner;
                    }
                }
                else
                {
                    throw new NotImplementedException("Operation not implemented for process of " + this.Parent.GetType().Name);
                }
            }

            this.InitializeSteps(new List<ProcessStep>() { newStep }, true);
        }

        /// <summary>
        /// Initializes the process steps.
        /// </summary>
        /// <param name="steps">The steps.</param>
        /// <param name="initializeSteps">if set to true the steps will be initialized.</param>
        private void InitializeSteps(IEnumerable<ProcessStep> steps, bool initializeSteps)
        {
            this.AddNewSteps(steps, null, this.ProcessSteps.Count);

            this.ComputeProcessViewData(steps, initializeSteps, false);
            this.RefreshScrapViewTab();
            this.RefreshCO2EmissionsTab();
        }

        /// <summary>
        /// Adds the specified newly created steps to the process's collection of steps.
        /// </summary>
        /// <param name="steps">The new steps.</param>
        /// <param name="stepClones">A collection containing the clones of the new steps.</param>
        /// <param name="startIndex">The index from which to start inserting.</param>
        private void AddNewSteps(IEnumerable<ProcessStep> steps, IEnumerable<ProcessStep> stepClones, int startIndex)
        {
            if (steps == null || steps.Count() == 0)
            {
                return;
            }

            var cloneManager = new CloneManager();

            foreach (var step in steps)
            {
                // Set the new step's index & update Model steps index.
                if (this.ProcessSteps.Count < startIndex)
                {
                    startIndex = 0;
                }

                this.ProcessSteps.Add(step);
                step.Process = this.Model;
                step.Index = startIndex;

                startIndex++;

                Type parentType = null;
                if (this.Parent is Assembly)
                {
                    parentType = typeof(AssemblyProcessStep);
                }
                else if (this.Parent is Part)
                {
                    parentType = typeof(PartProcessStep);
                }

                ProcessStep stepClone = null;
                if (stepClones != null)
                {
                    stepClone = stepClones.FirstOrDefault(s => s.Guid == step.Guid);
                }

                if (stepClone == null)
                {
                    stepClone = cloneManager.Clone(step, parentType, true);
                }

                if (stepClone != null)
                {
                    this.ModelClone.Steps.Add(stepClone);
                }
            }

            this.IsChanged = true;

            // Refresh cost
            this.internalCalculationResult = CalculateParentCost();

            this.AnalyzeDataCheckErrors();
        }

        /// <summary>
        /// Remove the selected process step
        /// </summary>
        private void RemoveSelectedStep()
        {
            if (this.SelectedProcessStepWidget == null)
            {
                return;
            }

            var questionMessage = string.Format(LocalizedResources.Question_Delete_Item, this.SelectedProcessStepWidget.ProcessStepName);
            MessageDialogResult result = this.windowService.MessageDialogService.Show(questionMessage, MessageDialogType.YesNo);
            if (result != MessageDialogResult.Yes)
            {
                return;
            }

            if (this.SelectedProcessStepWidget != null)
            {
                int selectedItemIndex = this.ProcessStepWidgetItems.IndexOf(this.SelectedProcessStepWidget);
                if (selectedItemIndex < 0 || selectedItemIndex >= this.ProcessStepWidgetItems.Count)
                {
                    return;
                }

                var processStep = this.SelectedProcessStepWidget.Model;
                this.ProcessSteps.Remove(processStep);

                // Remove the step from the model clone.
                var stepToRemove = this.ModelClone.Steps.FirstOrDefault(s => s.Guid == processStep.Guid);
                if (stepToRemove != null)
                {
                    this.ModelClone.Steps.Remove(stepToRemove);
                }

                ProcessStepWidgetViewModel nextWidgetToSelect = null;
                if (selectedItemIndex == this.ProcessStepWidgetItems.Count - 1)
                {
                    nextWidgetToSelect = this.ProcessStepWidgetItems.ElementAtOrDefault(selectedItemIndex - 1);
                }
                else
                {
                    nextWidgetToSelect = this.ProcessStepWidgetItems.ElementAtOrDefault(selectedItemIndex + 1);
                }

                this.RemoveProcessStep(processStep, this.SelectedProcessStepWidget);

                this.SelectedProcessStepWidget = nextWidgetToSelect;

                this.RefreshWidgetsFirstLastFlags();
            }

            this.IsChanged = true;
            this.AnalyzeDataCheckErrors();
        }

        /// <summary>
        /// Show the casting wizard
        /// </summary>
        private void AddCasting()
        {
            var part = this.Parent as Part;
            if (part == null)
            {
                throw new InvalidOperationException("Casting module could not start because the process parent is not a Part.");
            }

            using (this.UndoManager.StartBatch(navigateToBatchControls: true))
            {
                // Initialize the casting module view-model.
                var castingViewModel = this.container.GetExportedValue<ViewModels.CastingModuleViewModel>();
                castingViewModel.Part = part;
                castingViewModel.PartDataContext = this.DataSourceManager;
                castingViewModel.NavigationService.StartWizard(castingViewModel);

                this.InitializeSteps(castingViewModel.CreatedSteps, false);
                this.AnalyzeDataCheckErrors();
            }
        }

        /// <summary>
        /// Show the moulding wizard.
        /// </summary>
        private void AddMoulding()
        {
            var part = this.Parent as Part;
            if (part == null)
            {
                throw new InvalidOperationException("Moulding module could not start because the process parent is not a Part.");
            }

            // The parent part needs to contain a raw material of type Polymer, in order to add moulding.
            bool polymersInPart = false;
            foreach (var materials in part.RawMaterials.Where(p => !p.IsDeleted))
            {
                if (materials.MaterialsClassificationL1 != null && materials.MaterialsClassificationL1.Guid == new Guid("f32882ee-c299-4991-9d6c-27449367aae6"))
                {
                    polymersInPart = true;
                }
            }

            if (!polymersInPart)
            {
                this.windowService.MessageDialogService.Show(LocalizedResources.Moulding_PolymersNeeded, MessageDialogType.Info);
            }
            else
            {
                using (this.UndoManager.StartBatch(navigateToBatchControls: true))
                {
                    // Initialize the moulding module view-model.
                    var mouldingViewModel = this.container.GetExportedValue<ViewModels.MouldingModuleViewModel>();
                    mouldingViewModel.ParentPart = part;
                    mouldingViewModel.PartDataSourceManager = this.DataSourceManager;
                    mouldingViewModel.NavigationService.StartWizard(mouldingViewModel);

                    this.InitializeSteps(mouldingViewModel.CreatedSteps, false);
                    this.AnalyzeDataCheckErrors();
                }
            }
        }

        /// <summary>
        /// Drops a process step 
        /// </summary>
        /// <param name="obj">The dropped object with it's source</param>
        /// <param name="isDroppedBeforeWidget">A value that indicates if the drop should be done before the target index in collection of after</param>
        private void DropProcessStep(Tuple<object, object> obj, bool isDroppedBeforeWidget)
        {
            var source = obj.Item1 as ProcessStepWidgetViewModel;
            var target = obj.Item2 as ProcessStepWidgetViewModel;
            if (source == null || target == null)
            {
                return;
            }

            var sourceIndex = this.ProcessStepWidgetItems.IndexOf(source);
            var targetIndex = this.ProcessStepWidgetItems.IndexOf(target);

            if (sourceIndex >= 0 && targetIndex >= 0)
            {
                if (isDroppedBeforeWidget)
                {
                    this.MoveProcessSteps(sourceIndex, targetIndex);
                }
                else
                {
                    if (targetIndex >= sourceIndex)
                    {
                        this.MoveProcessSteps(sourceIndex, targetIndex);
                    }
                    else
                    {
                        this.MoveProcessSteps(sourceIndex, targetIndex + 1);
                    }
                }
            }

            CommandManager.InvalidateRequerySuggested();
            this.IsChanged = true;
        }

        /// <summary>
        /// Moves the process step widget,view model,scrap and co2 items to a another position in they're collections
        /// </summary>
        /// <param name="sourceIndex">The process step index to be moved</param>
        /// <param name="targetIndex">The index representing the target of the process step items</param>
        private void MoveProcessSteps(int sourceIndex, int targetIndex)
        {
            var sourceProcessStep = ProcessStepWidgetItems.ElementAtOrDefault(sourceIndex);
            var targetProcessStep = ProcessStepWidgetItems.ElementAtOrDefault(targetIndex);

            // Move process step widgets
            this.ProcessStepWidgetItems.Move(sourceIndex, targetIndex);

            // Move the process step view models
            this.ProcessStepViewModelItems.Move(sourceIndex, targetIndex);

            // Refresh the widgets first and last flags
            this.RefreshWidgetsFirstLastFlags();

            // Refresh the scrap section
            this.RefreshScrapViewTab();

            // Refresh the CO2 emission section
            this.RefreshCO2EmissionsTab();
        }

        /// <summary>
        /// Drop an entity to a process step
        /// </summary>
        /// <param name="obj">The widget view model where the drop was perform and the entity dropped</param>
        private void DropEntityOnProcessStep(Tuple<object, object> obj)
        {
            var widget = obj.Item1 as ProcessStepWidgetViewModel;
            var entity = obj.Item2;
            if (widget == null || entity == null)
            {
                return;
            }

            this.SelectedProcessStepWidget = widget;
            var processStepViewModel = this.ProcessStepViewModelItems.FirstOrDefault((w) => w.Model.Guid == widget.Model.Guid);
            if (processStepViewModel != null)
            {
                processStepViewModel.DropCommand.Execute(entity);
            }
        }

        #endregion Command handling

        #region UndoManager handling

        /// <summary>
        /// Handle the undo performed on ProcessStepViewModelItems collection.
        /// </summary>
        /// <param name="undoItem">The undoable item executed.</param>
        private void HandleUndoOnProcess(UndoableItem undoItem)
        {
            if (undoItem == null)
            {
                return;
            }

            // Select the appropriate ProcessStepWidget for which the undo action is performed.
            var undoProcessStepVM = undoItem.ViewModelInstance as ProcessStepViewModel;
            if (undoProcessStepVM != null)
            {
                this.SelectProcessStepWidget(undoProcessStepVM);
            }

            // Select the appropriate ProcessStepWidget for which the MediaViewModel has changed.
            var undoMediaVM = undoItem.ViewModelInstance as MediaViewModel;
            if (undoMediaVM != null)
            {
                var undoMediaStepVM = this.ProcessStepViewModelItems.FirstOrDefault(v => v.MediaViewModel == undoMediaVM);
                if (undoMediaStepVM != null)
                {
                    this.SelectProcessStepWidget(undoMediaStepVM);
                }
            }

            // Select the appropriate ProcessStepWidget for which the PartsGridItem has changed.
            var partsGridItem = undoItem.Instance as PartsGridItem;
            if (partsGridItem != null)
            {
                var undoPartsStepVM = this.ProcessStepViewModelItems.FirstOrDefault(v => v.StepParts.Contains(partsGridItem));
                if (undoPartsStepVM != null)
                {
                    this.SelectProcessStepWidget(undoPartsStepVM);
                }
            }

            // Select the appropriate ProcessStepWidget for which the MachineDataGridItem has changed.
            var machineGridItem = undoItem.Instance as MachineDataGridItem;
            if (machineGridItem != null)
            {
                var undoMachinesStepVM = this.ProcessStepViewModelItems.FirstOrDefault(v => v.StepMachines.Contains(machineGridItem));
                if (undoMachinesStepVM != null)
                {
                    this.SelectProcessStepWidget(undoMachinesStepVM);
                }
            }

            // Select the appropriate ProcessStepWidget for which the TypeSelectorViewModel has changed.
            var undoClassificationVM = undoItem.ViewModelInstance as ClassificationSelectorViewModel;
            if (undoClassificationVM != null)
            {
                var undoStepVM = this.ProcessStepViewModelItems.FirstOrDefault(v => v.TypeSelectorViewModel == undoClassificationVM);
                if (undoStepVM != null)
                {
                    this.SelectProcessStepWidget(undoStepVM);
                }
            }
        }

        /// <summary>
        /// Select the corresponding ProcessStepWidget to the ProcessStepViewModel passed as parameter.
        /// </summary>
        /// <param name="processStepVM">The ProcessStep view-model.</param>
        private void SelectProcessStepWidget(ProcessStepViewModel processStepVM)
        {
            if (processStepVM == null || this.SelectedProcessStepViewModel == null)
            {
                return;
            }

            var stepModel = processStepVM.Model;
            var selectedStepModel = this.SelectedProcessStepViewModel.Model;
            if (stepModel.Guid != selectedStepModel.Guid)
            {
                var stepWidget = this.ProcessStepWidgetItems.FirstOrDefault(w => w.Model.Guid == stepModel.Guid);
                if (stepWidget != null)
                {
                    this.SelectedProcessStepWidget = stepWidget;
                }
            }
        }

        /// <summary>
        /// Handle the undo performed on ProcessStepViewModelItems collection.
        /// </summary>
        /// <param name="undoItem">The undoable item executed.</param>
        private void HandleUndoOnProcessStepsCollection(UndoableItem undoItem)
        {
            if (undoItem == null)
            {
                return;
            }

            using (this.UndoManager.Pause())
            {
                switch (undoItem.ActionType)
                {
                    case UndoActionType.Insert:
                        {
                            if (undoItem.Values != null)
                            {
                                var steps = new Collection<ProcessStep>();
                                var stepsClone = new Collection<ProcessStep>();
                                foreach (var item in undoItem.Values)
                                {
                                    var stepVMitem = item as ProcessStepViewModel;
                                    if (stepVMitem != null)
                                    {
                                        this.UndoManager.TrackObject(stepVMitem);
                                        var stepModel = stepVMitem.Model;
                                        steps.Add(stepModel);
                                        stepsClone.Add(stepVMitem.ModelClone);

                                        this.ProcessStepViewModelItems.Insert(undoItem.OldStartingIndex, stepVMitem);
                                        this.SaveNestedViewModels.RegisterCommand(stepVMitem.SaveToModelCommand);
                                        this.CancelNestedViewModels.RegisterCommand(stepVMitem.CancelCommand);

                                        // Create the widget.
                                        var newWidget = this.container.GetExportedValue<ProcessStepWidgetViewModel>();
                                        newWidget.DataSourceManager = this.DataSourceManager;
                                        newWidget.IsReadOnly = this.IsReadOnly;
                                        newWidget.IsInViewerMode = this.IsInViewerMode;
                                        newWidget.MessageToken = this.internalMessageToken;
                                        newWidget.Model = stepVMitem.Model;
                                        newWidget.AddCommand = stepVMitem.AddCommand;
                                        newWidget.DropCommand = this.DropEntityOnProcessStepCommand;

                                        // Update process step widget Name and Media.
                                        newWidget.Update(
                                            stepVMitem.Name.Value,
                                            stepVMitem.MediaViewModel.MediaHandler != null ? stepVMitem.MediaViewModel.MediaHandler.MediaSource.AllSources : new Collection<object>());

                                        if (this.Parent is Assembly)
                                        {
                                            newWidget.CanShowCommodity = true;
                                        }
                                        else if (this.Parent is Part)
                                        {
                                            newWidget.CanShowCommodity = false;
                                        }

                                        this.ProcessStepWidgetItems.Insert(undoItem.OldStartingIndex, newWidget);
                                        this.SelectedProcessStepWidget = newWidget;

                                        this.RefreshWidgetsFirstLastFlags();
                                    }
                                }

                                this.AddNewSteps(steps, stepsClone, undoItem.OldStartingIndex);
                                this.RefreshScrapViewTab();
                                this.RefreshCO2EmissionsTab();
                            }

                            break;
                        }

                    case UndoActionType.Delete:
                        {
                            if (undoItem.Values != null)
                            {
                                var steps = new Collection<ProcessStep>();
                                foreach (var item in undoItem.Values)
                                {
                                    var stepVMitem = item as ProcessStepViewModel;
                                    if (stepVMitem != null)
                                    {
                                        var stepModel = stepVMitem.Model;
                                        if (stepModel != null)
                                        {
                                            var stepWidget = this.ProcessStepWidgetItems.FirstOrDefault((w) => w.Model.Guid == stepModel.Guid);
                                            if (stepWidget != null)
                                            {
                                                int itemIndex = this.ProcessStepWidgetItems.IndexOf(stepWidget);
                                                if (itemIndex < 0 || itemIndex >= this.ProcessStepWidgetItems.Count)
                                                {
                                                    return;
                                                }

                                                if (this.ProcessSteps.Contains(stepModel))
                                                {
                                                    this.ProcessSteps.Remove(stepModel);
                                                }

                                                // Remove the step from the model clone.
                                                var stepToRemove = this.ModelClone.Steps.FirstOrDefault(s => s.Guid == stepModel.Guid);
                                                if (stepToRemove != null)
                                                {
                                                    this.ModelClone.Steps.Remove(stepToRemove);
                                                }

                                                ProcessStepWidgetViewModel nextWidgetToSelect = null;
                                                if (itemIndex == this.ProcessStepWidgetItems.Count - 1)
                                                {
                                                    nextWidgetToSelect = this.ProcessStepWidgetItems.ElementAtOrDefault(itemIndex - 1);
                                                }
                                                else
                                                {
                                                    nextWidgetToSelect = this.ProcessStepWidgetItems.ElementAtOrDefault(itemIndex + 1);
                                                }

                                                this.UndoManager.StopTrackingObject(stepVMitem);
                                                this.RemoveProcessStep(stepModel, stepWidget);
                                                this.SelectedProcessStepWidget = nextWidgetToSelect;
                                                this.RefreshWidgetsFirstLastFlags();
                                                this.RefreshScrapViewTab();
                                                this.RefreshCO2EmissionsTab();
                                            }

                                            this.IsChanged = true;
                                            this.AnalyzeDataCheckErrors();
                                        }
                                    }
                                }
                            }

                            break;
                        }

                    case UndoActionType.Move:
                        {
                            this.ProcessStepWidgetItems.Move(undoItem.NewStartingIndex, undoItem.OldStartingIndex);
                            this.ProcessStepViewModelItems.Move(undoItem.NewStartingIndex, undoItem.OldStartingIndex);
                            this.RefreshScrapViewTab();
                            this.RefreshCO2EmissionsTab();
                            this.RefreshWidgetsFirstLastFlags();
                            this.IsChanged = true;
                            break;
                        }

                    default:
                        break;
                }
            }
        }

        #endregion UndoManager handling

        #region Calculations

        /// <summary>
        /// Calculates the cost of the process' parent.
        /// </summary>
        /// <returns>The cost or null, if the parent is null.</returns>
        private CalculationResult CalculateParentCost()
        {
            if (this.Parent == null)
            {
                return null;
            }

            Part part = this.ModelParentClone as Part;
            if (part != null)
            {
                PartCostCalculationParameters calculationParams = null;
                if (this.ParentProject != null)
                {
                    calculationParams = CostCalculationHelper.CreatePartParamsFromProject(this.ParentProject);
                }
                else if (this.IsInViewerMode)
                {
                    calculationParams = this.modelBrowserHelperService.GetPartCostCalculationParameters();
                }

                if (calculationParams != null)
                {
                    var calculator = CostCalculatorFactory.GetCalculator(part.CalculationVariant);
                    var result = calculator.CalculatePartCost(part, calculationParams);
                    this.messenger.Send(new CurrentComponentCostChangedMessage(result), this.IsInViewerMode ? GlobalMessengerTokens.ModelBrowserTargetToken : GlobalMessengerTokens.MainViewTargetToken);
                    return result;
                }
            }
            else
            {
                Assembly assy = this.ModelParentClone as Assembly;
                if (assy != null)
                {
                    AssemblyCostCalculationParameters calculationParams = null;
                    if (this.ParentProject != null)
                    {
                        calculationParams = CostCalculationHelper.CreateAssemblyParamsFromProject(this.ParentProject);
                    }
                    else if (this.IsInViewerMode)
                    {
                        calculationParams = this.modelBrowserHelperService.GetAssemblyCostCalculationParameters();
                    }

                    if (calculationParams != null)
                    {
                        var calculator = CostCalculatorFactory.GetCalculator(assy.CalculationVariant);
                        var result = calculator.CalculateAssemblyCost(assy, calculationParams);
                        this.messenger.Send(new CurrentComponentCostChangedMessage(result), this.IsInViewerMode ? GlobalMessengerTokens.ModelBrowserTargetToken : GlobalMessengerTokens.MainViewTargetToken);
                        return result;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Calculates and caches the cost of all parts and assemblies found in the process's parent object,
        /// if the parent object is an Assembly.
        /// </summary>
        private void CalculateAndCacheSubpartsCost()
        {
            Assembly assembly = this.ModelParentClone as Assembly;
            if (assembly == null
                || (this.ParentProject == null && !this.IsInViewerMode)
                || (this.IsInViewerMode && this.internalCalculationResult == null))
            {
                return;
            }

            foreach (Part part in assembly.Parts.Where(p => !p.IsDeleted))
            {
                CalculationResult result = null;
                var partCost = this.internalCalculationResult.PartsCost.PartCosts.FirstOrDefault(ac => ac.PartId == part.Guid);
                if (partCost != null)
                {
                    result = partCost.FullCalculationResult;
                }
                else
                {
                    PartCostCalculationParameters calculationParams = null;
                    if (this.ParentProject != null)
                    {
                        calculationParams = CostCalculationHelper.CreatePartParamsFromProject(this.ParentProject);
                    }
                    else if (this.IsInViewerMode)
                    {
                        calculationParams = this.modelBrowserHelperService.GetPartCostCalculationParameters();
                    }

                    if (calculationParams != null)
                    {
                        ICostCalculator calculator = CostCalculatorFactory.GetCalculator(part.CalculationVariant);
                        result = calculator.CalculatePartCost(part, calculationParams);
                    }
                }

                this.subpartsCostCache.Add(part.Guid, result);
            }

            // Add all parent's assemblies to the list
            foreach (Assembly subassembly in assembly.Subassemblies.Where(a => !a.IsDeleted))
            {
                CalculationResult result = null;
                var assyCost = this.internalCalculationResult.AssembliesCost.AssemblyCosts.FirstOrDefault(ac => ac.AssemblyId == subassembly.Guid);
                if (assyCost != null)
                {
                    result = assyCost.FullCalculationResult;
                }
                else
                {
                    AssemblyCostCalculationParameters calculationParams = null;
                    if (this.ParentProject != null)
                    {
                        calculationParams = CostCalculationHelper.CreateAssemblyParamsFromProject(this.ParentProject);
                    }
                    else if (this.IsInViewerMode)
                    {
                        calculationParams = this.modelBrowserHelperService.GetAssemblyCostCalculationParameters();
                    }

                    if (calculationParams != null)
                    {
                        ICostCalculator calculator = CostCalculatorFactory.GetCalculator(subassembly.CalculationVariant);
                        result = calculator.CalculateAssemblyCost(subassembly, calculationParams);
                    }
                }

                this.subpartsCostCache.Add(subassembly.Guid, result);
            }
        }

        #endregion Calculations

        #region Navigation

        /// <summary>
        /// Tries to navigate in process screen to the object received
        /// </summary>
        /// <param name="obj">The entity to navigate to</param>
        private void NavigateToEntityMessage(object obj)
        {
            var objType = obj.GetType();

            var machine = obj as Machine;
            if (machine != null)
            {
                var stepViewModel = this.ProcessStepViewModelItems.FirstOrDefault(vm => vm.StepMachines.Any(machineItem => machineItem.Machine.Guid == machine.Guid));
                if (stepViewModel != null)
                {
                    var isSelectionChanged = this.TrySelectProcessStep(stepViewModel.Model);
                    if (isSelectionChanged)
                    {
                        this.SelectedProcessStepViewModel.SelectedTabIndex.Value = (int)SelectedProcessStepTab.MachinesTab;
                        var machineItem = SelectedProcessStepViewModel.StepMachines.FirstOrDefault((m) => m.Machine.Guid == machine.Guid);
                        this.SelectedProcessStepViewModel.SelectedMachineItem.Value = machineItem;
                    }
                }
            }
            else if (objType == typeof(Consumable))
            {
                var consumable = obj as Consumable;
                var stepViewModel = this.ProcessStepViewModelItems.FirstOrDefault(vm => vm.StepConsumables.Any(item => item.Consumable.Guid == consumable.Guid));
                if (stepViewModel != null)
                {
                    var isSelectionChanged = this.TrySelectProcessStep(stepViewModel.Model);
                    if (isSelectionChanged)
                    {
                        this.SelectedProcessStepViewModel.SelectedTabIndex.Value = (int)SelectedProcessStepTab.ConsumablesTab;
                        var consumableItem = SelectedProcessStepViewModel.StepConsumables.FirstOrDefault((m) => m.Consumable.Guid == consumable.Guid);
                        this.SelectedProcessStepViewModel.SelectedConsumable.Value = consumableItem;
                    }
                }
            }
            else if (objType == typeof(Die))
            {
                var die = obj as Die;
                var stepViewModel = this.ProcessStepViewModelItems.FirstOrDefault(vm => vm.StepDieItems.Any(item => item.Die.Guid == die.Guid));
                if (stepViewModel != null)
                {
                    var isSelectionChanged = this.TrySelectProcessStep(stepViewModel.Model);
                    if (isSelectionChanged)
                    {
                        this.SelectedProcessStepViewModel.SelectedTabIndex.Value = (int)SelectedProcessStepTab.DiesTab;
                        var dieItem = SelectedProcessStepViewModel.StepDieItems.FirstOrDefault((m) => m.Die.Guid == die.Guid);
                        this.SelectedProcessStepViewModel.SelectedDieItem.Value = dieItem;
                    }
                }
            }
            else if (objType == typeof(Commodity))
            {
                var commodity = obj as Commodity;
                var stepViewModel = this.ProcessStepViewModelItems.FirstOrDefault(vm => vm.StepCommodities.Any(item => item.Commodity.Guid == commodity.Guid));
                if (stepViewModel != null)
                {
                    var isSelectionChanged = this.TrySelectProcessStep(stepViewModel.Model);
                    if (isSelectionChanged)
                    {
                        this.SelectedProcessStepViewModel.SelectedTabIndex.Value = (int)SelectedProcessStepTab.CommoditiesTab;
                        var commodityItem = SelectedProcessStepViewModel.StepCommodities.FirstOrDefault((m) => m.Commodity.Guid == commodity.Guid);
                        this.SelectedProcessStepViewModel.SelectedCommodity.Value = commodityItem;
                    }
                }
            }
        }

        /// <summary>
        /// Tries to select a process step widget for a process step
        /// </summary>
        /// <param name="step">The process step</param>
        /// <returns>True if the selection succeeded and false otherwise</returns>
        private bool TrySelectProcessStep(ProcessStep step)
        {
            if (step != null)
            {
                var processStepWidget = this.ProcessStepWidgetItems.FirstOrDefault((p) => p.Model.Guid == step.Guid);
                if (processStepWidget != null)
                {
                    this.SelectedProcessStepWidget = processStepWidget;
                    return true;
                }
            }

            return false;
        }

        #endregion Navigation

        #region Others

        /// <summary>
        /// Refresh the first and last flags on widgets
        /// </summary>
        private void RefreshWidgetsFirstLastFlags()
        {
            var firstWidget = this.ProcessStepWidgetItems.FirstOrDefault();
            var lastWidget = this.ProcessStepWidgetItems.LastOrDefault();
            if (firstWidget != null && lastWidget != null)
            {
                if (firstWidget == lastWidget)
                {
                    firstWidget.IsFirstWidget = true;
                    firstWidget.IsLastWidget = true;
                }
                else
                {
                    firstWidget.IsFirstWidget = true;
                    firstWidget.IsLastWidget = false;
                    lastWidget.IsFirstWidget = false;
                    lastWidget.IsLastWidget = true;

                    // Reset all the flags for the rest of the widgets
                    foreach (var widget in this.ProcessStepWidgetItems)
                    {
                        if (widget != firstWidget && widget != lastWidget)
                        {
                            widget.IsLastWidget = false;
                            widget.IsFirstWidget = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Reconstructs all data in this instance.
        /// </summary>
        private void RefreshInternal()
        {
            this.SaveNestedViewModels = new CompositeCommand();
            this.CancelNestedViewModels = new CompositeCommand();

            this.ProcessStepWidgetItems.Clear();
            this.ProcessStepViewModelItems.Clear();

            this.ProcessSteps.Clear();
            this.ProcessSteps.AddRange(this.Model.Steps.OrderBy(s => s.Index));

            this.ComputeProcessViewData(this.ProcessSteps, false, true);

            this.ScrapViewItems.Clear();
            this.RefreshScrapViewData();

            this.CO2ChartFossilItems = new DispatchedObservableCollection<CO2ChartItem>();
            this.CO2ChartRenewableItems = new DispatchedObservableCollection<CO2ChartItem>();
            this.ProcessCo2EmissionItems.Clear();
            this.RefreshCO2EmissionsTab();
        }

        /// <summary>
        /// Creates the view-models and the process step widgets for the specified steps.
        /// </summary>
        /// <param name="steps">The steps.</param>
        /// <param name="initializeSteps">if set to true the steps will be initialized.</param>
        /// <param name="keepSelection">If it is set to true the widget selection will be kept if possible</param>
        private void CreateProcessStepViewModelAndWidget(IEnumerable<ProcessStep> steps, bool initializeSteps, bool keepSelection)
        {
            if (steps == null || steps.Count() == 0)
            {
                return;
            }

            var processStepsCount = steps.Count();
            var stepWidgetsCount = this.ProcessStepWidgetItems.Count;

            ProcessStep firstStep = null;
            if (stepWidgetsCount == 0
                && processStepsCount > 0)
            {
                firstStep = steps.FirstOrDefault();
            }

            ProcessStep lastStep = steps.LastOrDefault();

            // Reset the last widget flag.
            var currentLastWidget = this.ProcessStepWidgetItems.FirstOrDefault(w => w.IsLastWidget);
            if (currentLastWidget != null)
            {
                currentLastWidget.IsLastWidget = false;
            }

            foreach (var step in steps)
            {
                // Create the view-model.
                var processStepItem = this.container.GetExportedValue<ProcessStepViewModel>();

                using (this.UndoManager.Pause())
                {
                    processStepItem.DataSourceManager = this.DataSourceManager;
                    processStepItem.IsReadOnly = this.IsReadOnly;
                    processStepItem.IsInViewerMode = this.IsInViewerMode;
                    processStepItem.ShowSaveControls = false;
                    processStepItem.ParentProject = this.ParentProject;
                    processStepItem.ProcessParent = this.Parent;
                    processStepItem.ModelParentClone = this.ModelParentClone;
                    processStepItem.ShowCancelMessageFlag = false;
                    processStepItem.IsChild = true;
                    processStepItem.ParentCost = this.internalCalculationResult;
                    processStepItem.SubpartsCostCache = this.subpartsCostCache;
                    processStepItem.MessageToken = this.internalMessageToken;
                    processStepItem.ModelClone = this.ModelClone.Steps.FirstOrDefault(ps => ps.Guid == step.Guid);
                    processStepItem.UndoManager = this.UndoManager;
                    processStepItem.InitializeUndoManager();
                    this.UndoManager.RegisterInstanceAction(this.HandleUndoOnProcess, processStepItem);
                    processStepItem.InitializeCollections();
                    processStepItem.Model = step;
                    if (processStepItem.MediaViewModel != null)
                    {
                        this.UndoManager.RegisterInstanceAction(this.HandleUndoOnProcess, processStepItem.MediaViewModel);
                    }

                    foreach (var part in processStepItem.StepParts)
                    {
                        this.UndoManager.RegisterInstanceAction(this.HandleUndoOnProcess, part);
                    }

                    foreach (var machine in processStepItem.StepMachines)
                    {
                        this.UndoManager.RegisterInstanceAction(this.HandleUndoOnProcess, machine);
                    }

                    if (processStepItem.TypeSelectorViewModel != null)
                    {
                        this.UndoManager.RegisterInstanceAction(this.HandleUndoOnProcess, processStepItem.TypeSelectorViewModel);
                    }

                    if (initializeSteps)
                    {
                        processStepItem.InitializeForCreation();
                    }

                    this.SaveNestedViewModels.RegisterCommand(processStepItem.SaveToModelCommand);
                    this.CancelNestedViewModels.RegisterCommand(processStepItem.CancelCommand);

                    // Create the widget.
                    var newWidget = this.container.GetExportedValue<ProcessStepWidgetViewModel>();
                    newWidget.DataSourceManager = this.DataSourceManager;
                    newWidget.IsReadOnly = this.IsReadOnly;
                    newWidget.IsInViewerMode = this.IsInViewerMode;
                    newWidget.MessageToken = this.internalMessageToken;

                    Media stepMedia;
                    if (this.stepsMedia.TryGetValue(step.Guid, out stepMedia))
                    {
                        newWidget.StepMedia = stepMedia;
                    }

                    newWidget.Model = step;
                    newWidget.AddCommand = processStepItem.AddCommand;
                    newWidget.DropCommand = this.DropEntityOnProcessStepCommand;

                    if (this.Parent is Assembly)
                    {
                        newWidget.CanShowCommodity = true;
                    }
                    else if (this.Parent is Part)
                    {
                        newWidget.CanShowCommodity = false;
                    }

                    // Set if is necessary the first and the last widget flag.
                    if (step == firstStep)
                    {
                        newWidget.IsFirstWidget = true;
                    }

                    if (step == lastStep)
                    {
                        newWidget.IsLastWidget = true;
                    }

                    this.ProcessStepWidgetItems.Add(newWidget);
                }

                this.ProcessStepViewModelItems.Add(processStepItem);
            }

            var widgetToSelect = this.ProcessStepWidgetItems[stepWidgetsCount];
            if (keepSelection)
            {
                // If the step still exists try and select the widget corresponding to it
                if (this.SelectedProcessStepWidget != null)
                {
                    // Get the last selected process step widget
                    var lastSelectedProcessStepId = this.SelectedProcessStepWidget.Model.Guid;
                    var processStepWidget = this.ProcessStepWidgetItems.FirstOrDefault(w => w.Model.Guid == lastSelectedProcessStepId);
                    if (processStepWidget != null)
                    {
                        widgetToSelect = processStepWidget;
                    }
                }
            }

            this.SelectedProcessStepWidget = widgetToSelect;
        }

        /// <summary>
        /// Removes the view model and the process step widget for a step.
        /// </summary>
        /// <param name="processStep">The process step to remove view model and widget</param>
        /// <param name="processStepWidget">The process step widget to remove.</param>
        private void RemoveProcessStep(ProcessStep processStep, ProcessStepWidgetViewModel processStepWidget)
        {
            // Remove the widget.
            this.ProcessStepWidgetItems.Remove(processStepWidget);

            // Remove the view-model.
            var stepViewModelToRemove = this.ProcessStepViewModelItems.FirstOrDefault((v) => v.Model.Guid == processStep.Guid);
            if (stepViewModelToRemove != null)
            {
                this.SaveNestedViewModels.UnregisterCommand(stepViewModelToRemove.SaveToModelCommand);
                this.CancelNestedViewModels.UnregisterCommand(stepViewModelToRemove.CancelCommand);
                this.ProcessStepViewModelItems.Remove(stepViewModelToRemove);
                this.UndoManager.StopTrackingObject(stepViewModelToRemove);
            }

            // Refresh Calculations
            this.internalCalculationResult = CalculateParentCost();

            this.RefreshScrapViewTab();

            // Refresh CO2 Emissions Per Part Chart.
            this.RefreshCO2EmissionsTab();
        }

        #endregion Others

        #region ProcessView

        /// <summary>
        /// Computes the data need for the process view section
        /// </summary>
        /// <param name="steps">The steps.</param>
        /// <param name="initializeSteps">if set to true the steps will be initialized.</param>
        /// <param name="keepSelection">If it is set to true the widget selection will be kept if possible</param>
        private void ComputeProcessViewData(IEnumerable<ProcessStep> steps, bool initializeSteps, bool keepSelection)
        {
            if (this.Parent == null)
            {
                return;
            }

            // If the model parent is a part it can use the moulding and the casting wizards to add process steps, else those wizards are not available.
            var assy = this.Parent as Assembly;
            if (assy != null)
            {
                this.CanShowWizards = false;
            }
            else
            {
                var part = this.Parent as Part;
                var rawPart = this.Parent as RawPart;
                if (part != null || rawPart != null)
                {
                    this.CanShowWizards = true;
                }
            }

            // Compute process step widgets and process step view-models.
            this.CreateProcessStepViewModelAndWidget(steps, initializeSteps, keepSelection);
        }

        #endregion ProcessView

        #region ScrapView

        /// <summary>
        /// Computes the data needed to show scrap view section.
        /// </summary>
        public void RefreshScrapViewData()
        {
            if (this.Parent == null)
            {
                return;
            }

            string calculationVariant = null;
            Part part = this.Parent as Part;
            if (part != null)
            {
                calculationVariant = part.CalculationVariant;
            }
            else
            {
                Assembly assy = this.Parent as Assembly;
                if (assy != null)
                {
                    calculationVariant = assy.CalculationVariant;
                }
            }

            // For calculation variants before 1.1.1 the scrap view is not available
            if (CostCalculatorFactory.IsOlder(calculationVariant, "1.1.1"))
            {
                this.CanShowScrapRejectView = false;
            }
            else
            {
                this.CanShowScrapRejectView = true;
            }

            if (this.CanShowScrapRejectView)
            {
                if (this.Model.IsMasterData)
                {
                    this.CanShowPartsCost = true;
                    this.CanShowRawMaterialsCost = true;
                    this.CanShowRawPartsCost = true;
                }
                else if (this.Parent is Assembly)
                {
                    this.CanShowPartsCost = true;
                    this.CanShowRawMaterialsCost = false;
                    this.CanShowRawPartsCost = false;
                }
                else if (this.Parent is Part)
                {
                    this.CanShowPartsCost = false;
                    this.CanShowRawMaterialsCost = true;
                    this.CanShowRawPartsCost = true;

                    if (this.Parent is RawPart)
                    {
                        this.CanShowRawPartsCost = false;
                    }
                }

                this.RefreshScrapViewTab();
            }
        }

        /// <summary>
        /// Refreshes the scrap section and create,delete,updates or reorder items, 
        /// to synchronize the process steps with its corresponding scrap item 
        /// </summary>
        private void RefreshScrapViewTab()
        {
            // Refresh calculations if needed
            CalculationResult result = null;
            if (this.internalCalculationResult == null)
            {
                result = this.CalculateParentCost();
                if (result == null)
                {
                    return;
                }

                this.internalCalculationResult = result;
            }
            else
            {
                result = this.internalCalculationResult;
            }

            // Remove all scrap items of steps that don't exist
            var stepIds = this.ProcessStepViewModelItems.Select(it => it.Model.Guid);
            var scrapItemsToRemove = new Collection<ScrapViewItem>();
            scrapItemsToRemove.AddRange(this.ScrapViewItems.Where(it => !stepIds.Contains(it.StepId)));
            foreach (var item in scrapItemsToRemove)
            {
                this.ScrapViewItems.Remove(item);
            }

            for (int i = 0; i < this.ProcessStepViewModelItems.Count; i++)
            {
                var stepViewModel = this.ProcessStepViewModelItems[i];
                var stepScrapItem = this.ScrapViewItems.FirstOrDefault(it => it.StepId == stepViewModel.Model.Guid);
                if (stepScrapItem == null)
                {
                    // Step doesn't have an scrap item and will be created a new one
                    stepScrapItem = new ScrapViewItem()
                    {
                        RejectRatio = stepViewModel.Model.ScrapAmount ?? 0m,
                        StepName = stepViewModel.Model.Name,
                        StepId = stepViewModel.Model.Guid,
                        StepOrderNo = (i + 1).ToString(System.Globalization.CultureInfo.InvariantCulture)
                    };

                    ScrapViewItems.Insert(i, stepScrapItem);
                }
                else
                {
                    int crtIndex = ScrapViewItems.IndexOf(stepScrapItem);
                    if (crtIndex != i)
                    {
                        int newIndex = crtIndex < i ? i + 1 : i;
                        ScrapViewItems.Move(crtIndex, newIndex);
                    }

                    stepScrapItem.StepOrderNo = (i + 1).ToString(System.Globalization.CultureInfo.InvariantCulture);
                }

                // refresh step cost
                var stepCost = result.ProcessCost.StepCosts.FirstOrDefault((s) => s.StepId == stepViewModel.Model.Guid);
                if (stepCost != null)
                {
                    stepScrapItem.StepCost = stepCost;
                }
            }

            this.RefreshRejectOverview(result.ProcessCost.RejectOverview);
        }

        /// <summary>
        /// Refreshes the reject overview.
        /// </summary>
        /// <param name="reject">The reject.</param>
        private void RefreshRejectOverview(ProcessRejectOverview reject)
        {
            this.RejectOverviewCost = reject;

            if (this.TotalMaterialItems == null)
            {
                this.TotalMaterialItems = new DispatchedObservableCollection<ScrapViewTotalMaterialItem>();
            }

            var rejectCostInTotalCost = this.TotalMaterialItems.FirstOrDefault(it => it.Index == 0);
            if (rejectCostInTotalCost == null)
            {
                this.TotalMaterialItems.Add(new ScrapViewTotalMaterialItem
                    {
                        Name = LocalizedResources.Process_ScrapView_RatioRejectCostInTotalCost,
                        Index = 0,
                        PartsTotalCost = this.RejectOverviewCost.PartsTotal.RatioOfRejectCostInTotalCost,
                        RawMaterialsTotalCost = this.RejectOverviewCost.RawMaterialsTotal.RatioOfRejectCostInTotalCost,
                        CommoditiesTotalCost = this.RejectOverviewCost.CommoditiesTotal.RatioOfRejectCostInTotalCost,
                        RawPartsTotalCost = this.RejectOverviewCost.RawPartsTotal.RatioOfRejectCostInTotalCost,
                        UIUnit = this.MeasurementUnitsAdapter.UIPercentage
                    });
            }
            else
            {
                rejectCostInTotalCost.PartsTotalCost = this.RejectOverviewCost.PartsTotal.RatioOfRejectCostInTotalCost;
                rejectCostInTotalCost.RawMaterialsTotalCost = this.RejectOverviewCost.RawMaterialsTotal.RatioOfRejectCostInTotalCost;
                rejectCostInTotalCost.CommoditiesTotalCost = this.RejectOverviewCost.CommoditiesTotal.RatioOfRejectCostInTotalCost;
                rejectCostInTotalCost.RawPartsTotalCost = this.RejectOverviewCost.RawPartsTotal.RatioOfRejectCostInTotalCost;
                rejectCostInTotalCost.UIUnit = this.MeasurementUnitsAdapter.UIPercentage;
            }

            var materialCostPcs = this.TotalMaterialItems.FirstOrDefault(it => it.Index == 1);
            if (materialCostPcs == null)
            {
                this.TotalMaterialItems.Add(new ScrapViewTotalMaterialItem
                    {
                        Name = LocalizedResources.Process_ScrapView_MaterialCostPcs,
                        Index = 1,
                        PartsTotalCost = this.RejectOverviewCost.PartsTotal.MaterialCostPerPiece,
                        RawMaterialsTotalCost = this.RejectOverviewCost.RawMaterialsTotal.MaterialCostPerPiece,
                        CommoditiesTotalCost = this.RejectOverviewCost.CommoditiesTotal.MaterialCostPerPiece,
                        RawPartsTotalCost = this.RejectOverviewCost.RawPartsTotal.MaterialCostPerPiece,
                        BaseCurrency = this.MeasurementUnitsAdapter.BaseCurrency,
                        UIUnit = this.MeasurementUnitsAdapter.UICurrency
                    });
            }
            else
            {
                materialCostPcs.PartsTotalCost = this.RejectOverviewCost.PartsTotal.MaterialCostPerPiece;
                materialCostPcs.RawMaterialsTotalCost = this.RejectOverviewCost.RawMaterialsTotal.MaterialCostPerPiece;
                materialCostPcs.CommoditiesTotalCost = this.RejectOverviewCost.CommoditiesTotal.MaterialCostPerPiece;
                materialCostPcs.RawPartsTotalCost = this.RejectOverviewCost.RawPartsTotal.MaterialCostPerPiece;
                materialCostPcs.UIUnit = this.MeasurementUnitsAdapter.UICurrency;
            }

            var rejectMaterialCostPcs = this.TotalMaterialItems.FirstOrDefault(it => it.Index == 2);
            if (rejectMaterialCostPcs == null)
            {
                this.TotalMaterialItems.Add(new ScrapViewTotalMaterialItem
                    {
                        Name = LocalizedResources.Process_ScrapView_RejectMaterialCostPcs,
                        Index = 2,
                        PartsTotalCost = this.RejectOverviewCost.PartsTotal.RejectMaterialCostPerPiece,
                        RawMaterialsTotalCost = this.RejectOverviewCost.RawMaterialsTotal.RejectMaterialCostPerPiece,
                        CommoditiesTotalCost = this.RejectOverviewCost.CommoditiesTotal.RejectMaterialCostPerPiece,
                        RawPartsTotalCost = this.RejectOverviewCost.RawPartsTotal.RejectMaterialCostPerPiece,
                        BaseCurrency = this.MeasurementUnitsAdapter.BaseCurrency,
                        UIUnit = this.MeasurementUnitsAdapter.UICurrency
                    });
            }
            else
            {
                rejectMaterialCostPcs.PartsTotalCost = this.RejectOverviewCost.PartsTotal.RejectMaterialCostPerPiece;
                rejectMaterialCostPcs.RawMaterialsTotalCost = this.RejectOverviewCost.RawMaterialsTotal.RejectMaterialCostPerPiece;
                rejectMaterialCostPcs.CommoditiesTotalCost = this.RejectOverviewCost.CommoditiesTotal.RejectMaterialCostPerPiece;
                rejectMaterialCostPcs.RawPartsTotalCost = this.RejectOverviewCost.RawPartsTotal.RejectMaterialCostPerPiece;
                rejectMaterialCostPcs.UIUnit = this.MeasurementUnitsAdapter.UICurrency;
            }

            var totalRejectMaterialCost = this.TotalMaterialItems.FirstOrDefault(it => it.Index == 2);
            if (totalRejectMaterialCost == null)
            {
                this.TotalMaterialItems.Add(new ScrapViewTotalMaterialItem
                    {
                        Name = LocalizedResources.Process_ScrapView_TotalRejectMaterialCost,
                        Index = 3,
                        PartsTotalCost = this.RejectOverviewCost.PartsTotal.TotalRejectMaterialCost,
                        RawMaterialsTotalCost = this.RejectOverviewCost.RawMaterialsTotal.TotalRejectMaterialCost,
                        CommoditiesTotalCost = this.RejectOverviewCost.CommoditiesTotal.TotalRejectMaterialCost,
                        RawPartsTotalCost = this.RejectOverviewCost.RawPartsTotal.TotalRejectMaterialCost,
                        BaseCurrency = this.MeasurementUnitsAdapter.BaseCurrency,
                        UIUnit = this.MeasurementUnitsAdapter.UICurrency
                    });
            }
            else
            {
                totalRejectMaterialCost.PartsTotalCost = this.RejectOverviewCost.PartsTotal.TotalRejectMaterialCost;
                totalRejectMaterialCost.RawMaterialsTotalCost = this.RejectOverviewCost.RawMaterialsTotal.TotalRejectMaterialCost;
                totalRejectMaterialCost.CommoditiesTotalCost = this.RejectOverviewCost.CommoditiesTotal.TotalRejectMaterialCost;
                totalRejectMaterialCost.RawPartsTotalCost = this.RejectOverviewCost.RawPartsTotal.TotalRejectMaterialCost;
                totalRejectMaterialCost.UIUnit = this.MeasurementUnitsAdapter.UICurrency;
            }
        }

        #endregion ScrapView

        #region CO2Emissions

        /// <summary>
        /// Refreshes the CO2 emission item sources that are used in the CO2 emission tab.
        /// </summary>
        private void RefreshCO2EmissionsTab()
        {
            // coderev:florin: all 3 methods below recalculate the CO2 emissions instead of taking the calculation result as input
            // none of the methods is used elsewhere, to require the recalculation internally (actually they shouldn't as each is not an independent step in the CO2 Emissions refresh)
            this.RefreshCo2EmissionsPerPartChartData();
            this.RefreshCo2EmissionsByProcessChartData();
            this.RefreshProcessCo2EmissionItems();
        }

        /// <summary>
        /// Refreshes the co2 emission section and create,delete,updates or reorder items, 
        /// to synchronize the process steps with its corresponding co2 emission items 
        /// </summary>
        private void RefreshProcessCo2EmissionItems()
        {
            // Remove all co2 emission items of steps that don't exist
            var stepIds = this.ProcessStepViewModelItems.Select(it => it.Model.Guid);
            var co2ItemsToRemove = this.ProcessCo2EmissionItems.Where(it => !stepIds.Contains(it.ProcessStepId)).ToList();
            foreach (var item in co2ItemsToRemove)
            {
                this.ProcessCo2EmissionItems.Remove(item);
            }

            // Remove all co2 emission machines that don't exist anymore
            var allMachineIds = new List<Guid>();
            foreach (var item in this.ProcessStepViewModelItems)
            {
                allMachineIds.AddRange(item.StepMachines.Where(it => !it.Machine.IsDeleted).Select(it => it.Machine.Guid));
            }

            var co2EmissionMachineIds = this.ProcessCo2EmissionItems.Where(it => it.MachineId != Guid.Empty).Select(m => m.MachineId);
            var co2EmissionsMachineIdsToDelete = co2EmissionMachineIds.Where(m => !allMachineIds.Contains(m));
            co2ItemsToRemove = this.ProcessCo2EmissionItems.Where(it => co2EmissionsMachineIdsToDelete.Contains(it.MachineId)).ToList();
            foreach (var item in co2ItemsToRemove)
            {
                this.ProcessCo2EmissionItems.Remove(item);
            }

            var calculator = new CO2EmissionsCalculator();
            var processEmissions = calculator.CalculateEmissions(this.ModelClone);

            for (int i = 0; i < this.ProcessStepViewModelItems.Count; i++)
            {
                var stepViewModel = this.ProcessStepViewModelItems[i];
                var co2emissionItems = this.ProcessCo2EmissionItems.Where(it => it.ProcessStepId == stepViewModel.Model.Guid).ToList();
                var stepEmission = processEmissions.FirstOrDefault(s => s.StepId == stepViewModel.Model.Guid);
                var machines = stepViewModel.StepMachines.Where(it => !it.Machine.IsDeleted).Select(it => it.Machine);

                bool addMachines = machines.Select(m => m.Guid).Where(m => !co2EmissionMachineIds.Contains(m)).Count() > 0;
                if (co2emissionItems.Count == 0 || addMachines)
                {
                    int nextIndex = 0;
                    if (i > 0)
                    {
                        var previusOrderedItem = this.ProcessCo2EmissionItems.LastOrDefault((p) => p.ProcessStepId == this.ProcessStepViewModelItems[i - 1].Model.Guid);
                        if (previusOrderedItem != null)
                        {
                            nextIndex = this.ProcessCo2EmissionItems.IndexOf(previusOrderedItem) + 1;
                        }
                    }

                    if (co2emissionItems.Count == 0)
                    {
                        // Step doesn't have an co2 emission item and will be created a new one
                        if (stepEmission != null)
                        {
                            var co2emissionItem = new CO2EmissionDataGridItem()
                                    {
                                        ProcessStepId = stepViewModel.Model.Guid,
                                        ProcessStepNumber = i + 1,
                                        ProcessStepName = stepViewModel.ModelClone.Name,
                                        ProcessTime = stepViewModel.ModelClone.ProcessTime,
                                        ProcessPartsPerCycle = stepViewModel.ModelClone.PartsPerCycle,
                                        MachineName = string.Empty,
                                        MachineId = Guid.Empty,
                                        MachinePowerConsumption = null,
                                        MachineFossilEnergyConsumption = null,
                                        MachineRenewableEnergyConsumption = null,
                                        Emissions = stepEmission,
                                    };

                            this.ProcessCo2EmissionItems.Insert(nextIndex, co2emissionItem);
                        }
                    }

                    nextIndex++;

                    foreach (var machine in machines.OrderBy(m => m.Index))
                    {
                        var machineEmission = stepEmission.MachineEmissions.FirstOrDefault(m => m.MachineId == machine.Guid);
                        if (machineEmission != null)
                        {
                            if (!this.ProcessCo2EmissionItems.Select(p => p.MachineId).Contains(machine.Guid))
                            {
                                var co2EmissionMachineItem = new CO2EmissionDataGridItem
                                    {
                                        ProcessStepId = stepViewModel.Model.Guid,
                                        ProcessStepNumber = null,
                                        ProcessStepName = string.Empty,
                                        ProcessTime = null,
                                        ProcessPartsPerCycle = null,
                                        MachineName = machine.Name,
                                        MachineId = machine.Guid,
                                        MachinePowerConsumption = machine.PowerConsumption,
                                        MachineFossilEnergyConsumption = machine.FossilEnergyConsumptionRatio ?? 1m,
                                        MachineRenewableEnergyConsumption = machine.RenewableEnergyConsumptionRatio,
                                        Emissions = machineEmission,
                                    };

                                this.ProcessCo2EmissionItems.Insert(nextIndex, co2EmissionMachineItem);
                            }

                            nextIndex++;
                        }
                    }
                }
                else
                {
                    // Refresh process step and machine order
                    var processStepEmissions = co2emissionItems.FirstOrDefault(p => p.MachineId == Guid.Empty);
                    int crtIndex = this.ProcessCo2EmissionItems.IndexOf(processStepEmissions);
                    if (crtIndex >= 0)
                    {
                        int nextIndex = 0;
                        if (i > 0)
                        {
                            var previusOrderedItem = this.ProcessCo2EmissionItems.LastOrDefault(p => p.ProcessStepId == this.ProcessStepViewModelItems[i - 1].Model.Guid);
                            if (previusOrderedItem != null)
                            {
                                nextIndex = this.ProcessCo2EmissionItems.IndexOf(previusOrderedItem) + 1;
                            }
                        }

                        int targetIndex = nextIndex;
                        if (crtIndex != nextIndex)
                        {
                            // Move CO2 emission data grid items that contains the source process step and its machines
                            var itemIndex = this.ProcessCo2EmissionItems.IndexOf(processStepEmissions);
                            this.ProcessCo2EmissionItems.Move(itemIndex, targetIndex);
                        }

                        targetIndex++;
                        foreach (var machine in machines.OrderBy(m => m.Index))
                        {
                            var co2EmissionMachine = this.ProcessCo2EmissionItems.FirstOrDefault(it => it.MachineId == machine.Guid);
                            if (co2EmissionMachine != null)
                            {
                                var itemIndex = this.ProcessCo2EmissionItems.IndexOf(co2EmissionMachine);
                                if (itemIndex != targetIndex)
                                {
                                    this.ProcessCo2EmissionItems.Move(itemIndex, targetIndex);
                                }

                                targetIndex++;
                            }
                        }
                    }
                }

                // Refresh the co2 emission process step data
                var processStepEmission = co2emissionItems.FirstOrDefault(p => p.MachineId == Guid.Empty);
                if (processStepEmission != null)
                {
                    processStepEmission.ProcessStepNumber = i + 1;
                    processStepEmission.ProcessTime = stepViewModel.ModelClone.ProcessTime;
                    processStepEmission.ProcessPartsPerCycle = stepViewModel.ModelClone.PartsPerCycle;

                    if (stepEmission != null)
                    {
                        processStepEmission.Emissions = stepEmission;
                    }
                }

                // Refresh the process step machines data
                var co2MachineItems = co2emissionItems.Where(p => p.MachineId != Guid.Empty);
                foreach (var item in co2MachineItems)
                {
                    var machine = machines.FirstOrDefault((m) => m.Guid == item.MachineId);
                    if (machine != null)
                    {
                        item.MachineName = machine.Name;
                        item.MachinePowerConsumption = machine.PowerConsumption;
                        item.MachineFossilEnergyConsumption = machine.FossilEnergyConsumptionRatio ?? 1m;
                        item.MachineRenewableEnergyConsumption = machine.RenewableEnergyConsumptionRatio;
                    }

                    if (stepEmission != null)
                    {
                        var machineEmission = stepEmission.MachineEmissions.FirstOrDefault(m => m.MachineId == item.MachineId);
                        if (machineEmission != null)
                        {
                            item.Emissions = machineEmission;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Computes the CO2 emissions per part chart source items.
        /// </summary>
        private void RefreshCo2EmissionsPerPartChartData()
        {
            var calculator = new CO2EmissionsCalculator();
            var processEmissions = calculator.CalculateEmissions(this.ModelClone);

            var totalFossilEmissionsPerPart = processEmissions.Sum(em => em.FossilSourcesEmissionsPerPart);
            var totalRenewableEmissionsPerPart = processEmissions.Sum(em => em.RenewableSourcesEmissionsPerPart);
            decimal totalEmissions = totalFossilEmissionsPerPart + totalRenewableEmissionsPerPart;

            var chartItems = new Collection<LabeledPieChartItem>();
            chartItems.Add(new LabeledPieChartItem(LocalizedResources.Process_CO2Fossil, totalFossilEmissionsPerPart));
            chartItems.Add(new LabeledPieChartItem(LocalizedResources.Process_CO2Renewable, totalRenewableEmissionsPerPart));

            this.Co2EmissionsPerPartItems = chartItems;
            this.Co2EmissionsPerPartTotal = totalEmissions;
        }

        /// <summary>
        /// Computes the CO2 emissions of the process steps.
        /// </summary>
        private void RefreshCo2EmissionsByProcessChartData()
        {
            var fossilEmissions = new DispatchedObservableCollection<CO2ChartItem>();
            var renewableEmissions = new DispatchedObservableCollection<CO2ChartItem>();

            var calculator = new CO2EmissionsCalculator();
            var processEmissions = calculator.CalculateEmissions(this.ModelClone);

            for (int i = 0; i < this.ProcessStepViewModelItems.Count; i++)
            {
                var stepViewModel = this.ProcessStepViewModelItems[i];
                var processStep = stepViewModel.Model;

                var stepEmission = processEmissions.First(s => s.StepId == processStep.Guid);
                if (stepEmission != null)
                {
                    fossilEmissions.Add(new CO2ChartItem(processStep.Guid, i + 1, stepViewModel.Name.Value, stepEmission.FossilSourcesEmissionsPerPart));
                    renewableEmissions.Add(new CO2ChartItem(processStep.Guid, i + 1, stepViewModel.Name.Value, stepEmission.RenewableSourcesEmissionsPerPart));
                }
            }

            this.CO2ChartFossilItems = fossilEmissions;
            this.CO2ChartRenewableItems = renewableEmissions;
        }

        #endregion CO2Emissions

        #region Errors Check

        /// <summary>
        /// Checks the data of a specified process and display the errors warning image if necessary.
        /// </summary>
        private void AnalyzeDataCheckErrors()
        {
            var process = this.ModelClone;
            decimal parentYearlyProductionQuantity = 0;
            decimal parentBatchSize = 0;
            string parentCalculationVariant = string.Empty;

            var assemblyParent = this.Parent as Assembly;
            if (assemblyParent != null)
            {
                parentYearlyProductionQuantity = assemblyParent.YearlyProductionQuantity.GetValueOrDefault();
                parentBatchSize = assemblyParent.BatchSizePerYear.GetValueOrDefault();
                parentCalculationVariant = assemblyParent.CalculationVariant;
            }

            var partParent = this.Parent as Part;
            if (partParent != null)
            {
                parentYearlyProductionQuantity = partParent.YearlyProductionQuantity.GetValueOrDefault();
                parentBatchSize = partParent.BatchSizePerYear.GetValueOrDefault();
                parentCalculationVariant = partParent.CalculationVariant;
            }

            var checkResult = new EntityDataCheckResult(process);
            var dataCheckResults = EntityDataChecker.CheckProcessData(process, parentYearlyProductionQuantity, parentBatchSize, parentCalculationVariant);
            checkResult.Errors.AddRange(dataCheckResults);
            if (checkResult.HasErrors)
            {
                this.ExistsCalculationErrors = true;
            }
            else
            {
                this.ExistsCalculationErrors = false;
            }

            this.DataCheckResult = checkResult;
        }

        #endregion Errors Check

        #region Save/Cancel

        /// <summary>
        /// Saves all changed back into the model and resets the changed status. This method is executed by the SaveToModelCommand command.
        /// </summary>
        protected override void SaveToModel()
        {
            // Sync the internal steps collection with the Model's Steps collection.
            foreach (var step in this.Model.Steps.ToList())
            {
                if (!this.ProcessSteps.Contains(step))
                {
                    // The step was deleted -> remove it from the Model.
                    this.Model.Steps.Remove(step);
                    this.DataSourceManager.ProcessStepRepository.RemoveAll(step);
                }
            }

            foreach (var step in this.ProcessSteps)
            {
                if (!this.Model.Steps.Contains(step))
                {
                    // The step is new -> add it to the Model.
                    this.Model.Steps.Add(step);
                }
            }

            // Save the scalar properties back into the model.
            base.SaveToModel();

            // Adjust the step indexes to be consecutive.
            int i = 0;
            foreach (var widget in this.ProcessStepWidgetItems)
            {
                widget.Model.Index = i;
                i++;
            }
        }

        /// <summary>
        /// Determines whether this instance can perform the save operation. Executed by the SaveCommand.
        /// The default implementation allows the save to be performed if the input is valid.
        /// </summary>
        /// <returns>
        /// true if the save operation can be performed, false otherwise.
        /// </returns>
        protected override bool CanSave()
        {
            return base.CanSave() && this.SaveNestedViewModels.CanExecute(null);
        }

        /// <summary>
        /// Performs the save operation. Executed by the SaveCommand.
        /// </summary>
        protected override void Save()
        {
            using (this.UndoManager.Pause())
            {
                this.CheckModelAndDataSource();

                // Gets the process steps ids before saving to model
                var stepsIdsOrdered = this.ProcessSteps.OrderBy(s => s.Index).Select(i => i.Guid).ToList();

                // Save all changes back into the Model objects. The validity check of this operation is performed by the CanSave method.
                this.SaveNestedViewModels.Execute(null);
                this.SaveToModel();

                // Update process
                this.DataSourceManager.ProcessRepository.Save(this.Model);
                this.DataSourceManager.SaveChanges();

                // Notify the other components that the assembly was created/updated.
                EntityChangedMessage message = this.Model.IsMasterData ?
                    new EntityChangedMessage(Notification.MasterDataEntityChanged) :
                    new EntityChangedMessage(Notification.MyProjectsEntityChanged);

                message.ChangeType = this.EditMode == ViewModelEditMode.Create ? EntityChangeType.EntityCreated : EntityChangeType.EntityUpdated;
                message.Entity = this.Model;
                if (this.Parent != null)
                {
                    message.Parent = this.Parent;
                }

                this.messenger.Send(message);

                // Send a "Reorder" message so the projects tree is notified to refresh its items view to display the steps in the new order, if their order was changed.
                if (this.Model.Steps.Count > 0)
                {
                    if (this.CheckStepOrderIfChanged(stepsIdsOrdered))
                    {
                        EntityChangedMessage msg2 = new EntityChangedMessage(message.Notification)
                        {
                            Entity = this.Model.Steps.First(),
                            Parent = null,
                            ChangeType = EntityChangeType.EntityReordered
                        };
                        messenger.Send(msg2);
                    }

                    this.IsChanged = false;
                }

                this.UndoManager.Reset();
            }
        }

        /// <summary>
        /// Determines whether the Cancel operation can be performed. Executed by the CancelCommand.
        /// </summary>
        /// <returns>
        /// true if the changes can be canceled, false otherwise.
        /// </returns>
        protected override bool CanCancel()
        {
            return base.CanCancel() && this.CancelNestedViewModels.CanExecute(null);
        }

        /// <summary>
        /// Cancels all changes. Executed by the CancelCommand.
        /// </summary>
        protected override void Cancel()
        {
            if (!this.CanCancel())
            {
                return;
            }

            if (this.IsChanged)
            {
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_CancelChanges, MessageDialogType.YesNo);
                if (result != MessageDialogResult.Yes)
                {
                    // Don't cancel the changes and also don't close the view-model.
                    return;
                }
                else
                {
                    // Cancel all changes
                    base.Cancel();
                    this.CancelNestedViewModels.Execute(null);
                }

                // Save the current selected process step tab on SelectedProcessStepViewModel
                if (this.SelectedProcessStepViewModel != null)
                {
                    this.currentSelectedProcessStepTab = this.SelectedProcessStepViewModel.SelectedTabIndex.Value;
                }

                this.IsChanged = false;

                this.RefreshModelClone();

                this.SelectedProcessStepViewModel = null;
                this.internalCalculationResult = CalculateParentCost();
                this.RefreshInternal();

                this.AnalyzeDataCheckErrors();

                this.UndoManager.Reset();
            }
        }

        /// <summary>
        /// Called before unloading the view from its parent. Returning false will cancel the view's unloading.
        /// </summary>
        /// <returns>
        /// True if the unloading process should continue and false if it should be canceled.
        /// </returns>
        public override bool OnUnloading()
        {
            // Do nothing if the view-model is read-only, in viewer mode, it was not changed or its parent was deleted.
            var parent = this.Parent as ITrashable;
            if (this.IsReadOnly || this.IsInViewerMode || !this.IsChanged || (parent != null && parent.IsDeleted))
            {
                return true;
            }

            if (this.EditMode == ViewModelEditMode.Create)
            {
                // Ask the user to confirm quitting
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_Quit, MessageDialogType.YesNo);
                if (result != MessageDialogResult.Yes)
                {
                    // Don't quit.
                    return false;
                }

                this.IsChanged = false;
            }
            else if (this.EditMode == ViewModelEditMode.Edit)
            {
                // Ask the user if he wants to save
                var result = this.windowService.MessageDialogService.Show(LocalizedResources.Question_UnsavedDataOnQuit, MessageDialogType.YesNoCancel);
                if (result == MessageDialogResult.Yes)
                {
                    // The user whishes to save but it is not possible because the input is not valid. Return false to stop the view-model from unloading.
                    if (!this.CanSave())
                    {
                        return false;
                    }

                    this.Save();
                }
                else if (result == MessageDialogResult.No)
                {
                    // The user does not want to save.                    
                    this.IsChanged = false;
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Checks if the steps order is changed
        /// </summary>
        /// <param name="stepsIdsOrdered">The ids of the steps in the original order.</param>
        /// <returns>
        /// True if the steps order changed and false otherwise
        /// </returns>
        private bool CheckStepOrderIfChanged(List<Guid> stepsIdsOrdered)
        {
            var isOrderChanged = false;
            for (int i = 0; i < this.ProcessStepViewModelItems.Count; i++)
            {
                var processStepId = stepsIdsOrdered[i];
                if (processStepId != this.ProcessStepViewModelItems[i].Model.Guid)
                {
                    isOrderChanged = true;
                    break;
                }
            }

            return isOrderChanged;
        }

        #endregion Save/Cancel
    }
}
