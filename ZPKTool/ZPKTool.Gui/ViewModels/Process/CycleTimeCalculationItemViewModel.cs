﻿using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations;
using System.Windows;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the CycleTimeCalculationItem view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class CycleTimeCalculationItemViewModel : ViewModel<CycleTimeCalculation, IDataSourceManager>
    {
        #region Attributes

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The messenger.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The total time variable.
        /// </summary>
        private decimal totalTime;

        /// <summary>
        /// The visibility of the edit button.
        /// </summary>
        private Visibility editButtonVisibility;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="CycleTimeCalculationItemViewModel" /> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        /// <param name="messenger">The messenger.</param>
        [ImportingConstructor]
        public CycleTimeCalculationItemViewModel(IWindowService windowService, IMessenger messenger)
        {
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("messenger", messenger);

            this.windowService = windowService;
            this.messenger = messenger;

            this.EditButtonVisibility = Visibility.Visible;
        }

        #region Properties

        /// <summary>
        /// Gets the calculation item's description.
        /// </summary>
        [Required(ErrorMessageResourceName = "General_RequiredField", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("Description")]
        public DataProperty<string> Description { get; private set; }

        /// <summary>
        /// Gets the raw part's description.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("RawPartDescription")]
        public DataProperty<string> RawPartDescription { get; private set; }

        /// <summary>
        /// Gets the machining volume.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("MachiningVolume")]
        public DataProperty<decimal?> MachiningVolume { get; private set; }

        /// <summary>
        /// Gets the machining type.
        /// </summary>
        [ExposesModelProperty("MachiningType")]
        [UndoableProperty]
        public DataProperty<MachiningType?> MachiningType { get; private set; }

        /// <summary>
        /// Gets the tolerance type.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("ToleranceType")]
        public DataProperty<string> ToleranceType { get; private set; }

        /// <summary>
        /// Gets the surface type.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("SurfaceType")]
        public DataProperty<string> SurfaceType { get; private set; }

        /// <summary>
        /// Gets the transport time from buffer.
        /// </summary>
        [Required(ErrorMessageResourceName = "General_RequiredField", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("TransportTimeFromBuffer")]
        public DataProperty<decimal?> TransportTimeFromBuffer { get; private set; }

        /// <summary>
        /// Gets the tool change time.
        /// </summary>
        [Required(ErrorMessageResourceName = "General_RequiredField", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("ToolChangeTime")]
        [Range(0d, 100000000d, ErrorMessageResourceName = "General_RangeValidation", ErrorMessageResourceType = typeof(LocalizedResources))]
        public DataProperty<decimal?> ToolChangeTime { get; private set; }

        /// <summary>
        /// Gets the transport clamping time.
        /// </summary>
        [Required(ErrorMessageResourceName = "General_RequiredField", ErrorMessageResourceType = typeof(LocalizedResources))]
        [Range(0d, 100000000d, ErrorMessageResourceName = "General_RangeValidation", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("TransportClampingTime")]
        public DataProperty<decimal?> TransportClampingTime { get; private set; }

        /// <summary>
        /// Gets the machining time.
        /// </summary>
        [Required(ErrorMessageResourceName = "General_RequiredField", ErrorMessageResourceType = typeof(LocalizedResources))]
        [Range(0d, 100000000d, ErrorMessageResourceName = "General_RangeValidation", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("MachiningTime")]
        public DataProperty<decimal?> MachiningTime { get; private set; }

        /// <summary>
        /// Gets the take off time.
        /// </summary>
        [Required(ErrorMessageResourceName = "General_RequiredField", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        [ExposesModelProperty("TakeOffTime")]
        public DataProperty<decimal?> TakeOffTime { get; private set; }

        /// <summary>
        /// Gets the index.
        /// </summary>
        [ExposesModelProperty("Index")]
        public DataProperty<int?> Index { get; private set; }

        /// <summary>
        /// Gets the machining calculation data.
        /// </summary>
        [UndoableProperty(IsUnselectable = true, GroupConsecutiveChanges = true)]
        [ExposesModelProperty("MachiningCalculationData")]
        public DataProperty<byte[]> MachiningCalculationData { get; private set; }

        /// <summary>
        /// Gets or sets the edit button visibility.
        /// </summary>  
        public Visibility EditButtonVisibility
        {
            get { return this.editButtonVisibility; }
            set { this.SetProperty(ref this.editButtonVisibility, value, () => this.EditButtonVisibility); }
        }

        /// <summary>
        /// Gets the calculation item's total time.
        /// </summary>
        public decimal TotalTime
        {
            get { return this.totalTime; }
            private set { this.SetProperty(ref this.totalTime, value, () => this.TotalTime); }
        }

        #endregion

        /// <summary>
        /// Initializes the properties of this instance.
        /// </summary>
        private void RegisterValueChangedEvent()
        {
            this.MachiningType.ValueChanged += (s, e) => this.SetEditButtonVisibility();
            this.Description.ValueChanged += (s, e) => this.UpdateTotalTime();
            this.MachiningTime.ValueChanged += (s, e) => this.UpdateTotalTime();
            this.TakeOffTime.ValueChanged += (s, e) => this.UpdateTotalTime();
            this.ToolChangeTime.ValueChanged += (s, e) => this.UpdateTotalTime();
            this.TransportClampingTime.ValueChanged += (s, e) => this.UpdateTotalTime();
            this.TransportTimeFromBuffer.ValueChanged += (s, e) => this.UpdateTotalTime();
        }

        /// <summary>
        /// Called when the Model has changed.
        /// </summary>
        protected override void OnModelChanged()
        {
            base.OnModelChanged();

            this.RegisterValueChangedEvent();
            this.SetEditButtonVisibility();
            this.UpdateTotalTime();
        }

        /// <summary>
        /// Updates the total time value.
        /// </summary>
        private void UpdateTotalTime()
        {
            // Each property that is valid (has no errors) is added to the total time value.
            decimal tmpCalc = string.IsNullOrEmpty(this.TransportTimeFromBuffer.Error) ? (decimal)this.TransportTimeFromBuffer.Value : 0;
            tmpCalc += string.IsNullOrEmpty(this.ToolChangeTime.Error) ? (decimal)this.ToolChangeTime.Value : 0;
            tmpCalc += string.IsNullOrEmpty(this.TransportClampingTime.Error) ? (decimal)this.TransportClampingTime.Value : 0;
            tmpCalc += string.IsNullOrEmpty(this.MachiningTime.Error) ? (decimal)this.MachiningTime.Value : 0;
            tmpCalc += string.IsNullOrEmpty(this.TakeOffTime.Error) ? (decimal)this.TakeOffTime.Value : 0;

            if (this.TotalTime != tmpCalc)
            {
                // If the total time changed, send a notification message for the parent view model to update its total time.
                this.TotalTime = tmpCalc;
                this.messenger.Send<NotificationMessage>(new NotificationMessage(MachiningCalculatorViewModel.CalculatorNotificationMessages.TotalTimeChanged));
            }
        }

        /// <summary>
        /// Sets the visibility of the edit button.
        /// </summary>
        private void SetEditButtonVisibility()
        {
            if (MachiningType.Value == null)
            {
                // If the machining type is null, the edit button is not visible.
                EditButtonVisibility = Visibility.Collapsed;
            }
            else
            {
                if (MachiningType.Value.HasValue)
                {
                    if (CycleTimeCalculatorViewModel.IsCalculationSupported((MachiningType)MachiningType.Value))
                    {
                        // If the machining type calculation is supported, the edit button is visible.
                        EditButtonVisibility = Visibility.Visible;
                    }
                    else
                    {
                        EditButtonVisibility = Visibility.Collapsed;
                    }
                }
            }
        }

        /// <summary>
        /// Performs the save operation. Executed by the SaveCommand.
        /// </summary>
        protected override void Save()
        {
            this.CheckModelAndDataSource();

            // Save all changes back into the Model objects.
            this.SaveToModel();
        }
    }
}
