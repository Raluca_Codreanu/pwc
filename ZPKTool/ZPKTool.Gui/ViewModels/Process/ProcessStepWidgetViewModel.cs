﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows.Media;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Utils;
using ZPKTool.Gui.ViewModels.ProcessInfrastructure;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for the process step widget.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ProcessStepWidgetViewModel : ViewModel<ProcessStep, IDataSourceManager>
    {
        #region Attributes

        /// <summary>
        /// The pixel width to decode the process step images
        /// </summary>
        private const int ProcessStepImageDecodePixelWidth = 300;

        /// <summary>
        /// The pixel width to decode the process step machines images
        /// </summary>
        private const int MachineImageDecodePixelWidth = 50;

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The model browser helper service.
        /// </summary>
        private IModelBrowserHelperService modelBrowserHelperService;

        /// <summary>
        /// It indicates if the widget is the last one in the collection.
        /// </summary>
        private bool isLastWidget;

        /// <summary>
        /// It indicates if the widget is the first one in the collection.
        /// </summary>
        private bool isFirstWidget;

        /// <summary>
        /// It indicates if the widget is selected or not.
        /// </summary>
        private bool isSelected;

        /// <summary>
        /// The widget picture source representing the image of the model
        /// </summary>
        private ImageSource pictureSource;

        /// <summary>
        /// The number of machines of the model
        /// </summary>
        private int machinesCount;

        /// <summary>
        /// The list of the machines image and name of the model  
        /// </summary>
        private ObservableCollection<WidgetMachineItem> machineItems;

        /// <summary>
        /// It indicates if the model have commodities or not
        /// </summary>
        private bool canShowCommodity;

        /// <summary>
        /// The name of the process step.
        /// </summary>
        private string processStepName;

        /// <summary>
        /// The process step media
        /// </summary>
        private Media stepMedia;

        #endregion Attributes

        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessStepWidgetViewModel"/> class.
        /// </summary>
        /// <param name="messenger">The messenger.</param>
        /// <param name="modelBrowserHelperService">The model browser helper service.</param>
        [ImportingConstructor]
        public ProcessStepWidgetViewModel(IMessenger messenger, IModelBrowserHelperService modelBrowserHelperService)
        {
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("modelBrowserHelperService", modelBrowserHelperService);

            this.messenger = messenger;
            this.modelBrowserHelperService = modelBrowserHelperService;

            this.MachineItems = new ObservableCollection<WidgetMachineItem>();

            this.messenger.Register<MediaChangedMessage>(this.ProcessStepMediaChanged);
        }

        #region Commands

        /// <summary>
        /// Gets or sets the command for selecting a process step.
        /// </summary>
        public ICommand SelectProcessStepCommand { get; set; }

        /// <summary>
        /// Gets or sets the command for dropping an entity
        /// </summary>
        public ICommand DropCommand { get; set; }

        /// <summary>
        /// Gets or sets the command for adding an entity
        /// </summary>
        public ICommand AddCommand { get; set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets or sets the name of the process step.
        /// </summary>
        public string ProcessStepName
        {
            get { return this.processStepName; }
            set { this.SetProperty(ref this.processStepName, value, () => this.ProcessStepName); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the widget is the last one in the collection.
        /// </summary>
        public bool IsLastWidget
        {
            get { return this.isLastWidget; }
            set { this.SetProperty(ref this.isLastWidget, value, () => this.IsLastWidget); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the widget is the first one in the collection
        /// </summary>
        public bool IsFirstWidget
        {
            get { return this.isFirstWidget; }
            set { this.SetProperty(ref this.isFirstWidget, value, () => this.IsFirstWidget); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the widget is selected or not.
        /// </summary>
        public bool IsSelected
        {
            get { return this.isSelected; }
            set { this.SetProperty(ref this.isSelected, value, () => this.IsSelected); }
        }

        /// <summary>
        /// Gets or sets the widget picture source representing the image of the model
        /// </summary>
        public ImageSource PictureSource
        {
            get { return this.pictureSource; }
            set { this.SetProperty(ref this.pictureSource, value, () => this.PictureSource); }
        }

        /// <summary>
        /// Gets or sets the number of machines of the model.
        /// </summary>
        public int MachinesCount
        {
            get { return this.machinesCount; }
            set { this.SetProperty(ref this.machinesCount, value, () => this.MachinesCount); }
        }

        /// <summary>
        /// Gets or sets the list of the machines image and name of the model.
        /// </summary>
        public ObservableCollection<WidgetMachineItem> MachineItems
        {
            get { return this.machineItems; }
            set { this.SetProperty(ref this.machineItems, value, () => this.MachineItems); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the model can add commodities or not
        /// </summary>
        public bool CanShowCommodity
        {
            get { return this.canShowCommodity; }
            set { this.SetProperty(ref this.canShowCommodity, value, () => this.CanShowCommodity); }
        }

        /// <summary>
        /// Gets or sets the process step media
        /// </summary>
        public Media StepMedia
        {
            get { return this.stepMedia; }
            set { this.SetProperty(ref this.stepMedia, value, () => this.StepMedia); }
        }

        /// <summary>
        /// Gets or sets the message token to be used when sending messages with token 
        /// </summary>
        public string MessageToken { get; set; }

        #endregion Properties

        #region Message Handling

        /// <summary>
        /// Handle the MediaChanged message for process step model and it's machines
        /// </summary>
        /// <param name="msg">The message</param>
        private void ProcessStepMediaChanged(MediaChangedMessage msg)
        {
            var step = msg.Entity as ProcessStep;
            if (step != null)
            {
                if (this.Model.Guid == step.Guid)
                {
                    this.PictureSource = this.GetImageSource(msg.Media.FirstOrDefault(), ProcessStepImageDecodePixelWidth);
                }
            }
        }

        /// <summary>
        /// Handle the ProcessStepChanged Message 
        /// </summary>
        /// <param name="msg">The message</param>
        private void ProcessStepChanged(ProcessStepChangedMessage msg)
        {
            if (this.Model.Guid == msg.StepID)
            {
                if (msg.ChangeType == ProcessStepChangeType.NameUpdated)
                {
                    this.ProcessStepName = msg.Value as string;
                }
                else if (msg.ChangeType == ProcessStepChangeType.SubEntityAdded)
                {
                    var machine = msg.SubEntity as Machine;
                    if (machine != null)
                    {
                        var index = machine.Index ?? this.Model.Machines.Where(m => !m.IsDeleted).Count();
                        this.MachineItems.Insert(index - 1, new WidgetMachineItem { MachineName = machine.Name, MachineId = machine.Guid });
                        this.MachinesCount++;
                    }
                }
                else if (msg.ChangeType == ProcessStepChangeType.SubEntityNameUpdated)
                {
                    var machine = msg.SubEntity as Machine;
                    if (machine != null)
                    {
                        var machineItem = this.MachineItems.FirstOrDefault((m) => m.MachineId == machine.Guid);
                        if (machineItem != null)
                        {
                            machineItem.MachineName = machine.Name;
                        }
                    }
                }
                else if (msg.ChangeType == ProcessStepChangeType.SubEntityDeleted)
                {
                    var machine = msg.SubEntity as Machine;
                    if (machine != null)
                    {
                        var machineItem = this.MachineItems.FirstOrDefault((m) => m.MachineId == machine.Guid);
                        if (machineItem != null)
                        {
                            this.MachineItems.Remove(machineItem);
                            this.MachinesCount--;
                        }
                    }
                }
            }
        }

        #endregion Message Handling

        #region Property change handlers

        /// <summary>
        /// Loads the data from the model.
        /// Each view-model property decorated with the ExposedModelProperty attribute is loaded from the associated Model property.
        /// </summary>
        /// <param name="model">The model instance.</param>
        public override void LoadDataFromModel(ProcessStep model)
        {
            this.ProcessStepName = model.Name;

            var machines = new ObservableCollection<WidgetMachineItem>();

            // Get the picture from process step media
            if (this.IsInViewerMode)
            {
                var stepsMedia = this.modelBrowserHelperService.GetMediaForEntity(this.Model);
                ImageSource stepImageSource = null;

                var media = stepsMedia.FirstOrDefault();
                if (media != null)
                {
                    byte[] stepMedia = File.ReadAllBytes(media.CachedFilePath);
                    if (stepMedia != null)
                    {
                        stepImageSource = MediaUtils.CreateImageForDisplay(stepMedia, ProcessStepImageDecodePixelWidth);
                    }
                }

                this.PictureSource = stepImageSource;
            }
            else
            {
                if (this.StepMedia != null)
                {
                    this.PictureSource = MediaUtils.CreateImageForDisplay(stepMedia.Content, ProcessStepImageDecodePixelWidth);
                }
                else
                {
                    this.PictureSource = null;
                }
            }

            var modelMachines = this.Model.Machines.Where((m) => !m.IsDeleted);
            foreach (var machine in modelMachines.OrderBy((m) => m.Index))
            {
                machines.Add(new WidgetMachineItem { MachineName = machine.Name, MachineId = machine.Guid });
            }

            this.MachineItems = machines;
            this.MachinesCount = modelMachines.Count();

            // Register to the ProcessStepChangedMessage with the local message token if set or use the global one
            if (this.MessageToken != null)
            {
                this.messenger.Register<ProcessStepChangedMessage>(this.ProcessStepChanged, this.MessageToken);
            }
            else
            {
                this.messenger.Register<ProcessStepChangedMessage>(this.ProcessStepChanged, this.IsInViewerMode ? GlobalMessengerTokens.ModelBrowserTargetToken : GlobalMessengerTokens.MainViewTargetToken);
            }
        }

        #endregion Property change handlers

        #region Others

        /// <summary>
        /// Update process step widget.
        /// </summary>
        /// <param name="name">New Process step name value.</param>
        /// <param name="media">New Process step media.</param>
        public void Update(string name, Collection<object> media)
        {
            this.ProcessStepName = name;
            this.PictureSource = this.GetImageSource(media.FirstOrDefault(), ProcessStepImageDecodePixelWidth);
        }

        /// <summary>
        /// Get the ImageSource for media passed as parameter.
        /// </summary>
        /// <param name="media">The media object.</param>
        /// <param name="imageDecodePixelWidth">The pixel width to decode the image.</param>
        /// <returns>The created image source.</returns>
        private ImageSource GetImageSource(object media, int imageDecodePixelWidth)
        {
            ImageSource imageSource = null;
            var mediaPath = media as string;
            if (mediaPath != null)
            {
                byte[] bufferMedia = File.ReadAllBytes(mediaPath);
                if (bufferMedia != null)
                {
                    imageSource = MediaUtils.CreateImageForDisplay(bufferMedia, imageDecodePixelWidth);
                }
            }
            else
            {
                var mediaSource = media as Media;
                if (mediaSource != null)
                {
                    imageSource = MediaUtils.CreateImageForDisplay(mediaSource.Content, imageDecodePixelWidth);
                }
            }

            return imageSource;
        }

        #endregion Others
    }
}
