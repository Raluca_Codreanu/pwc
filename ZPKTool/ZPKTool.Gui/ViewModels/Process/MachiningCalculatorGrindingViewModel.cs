﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using ZPKTool.Business.MachiningCalculator;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the MachiningCalculatorGrinding view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class MachiningCalculatorGrindingViewModel : ViewModel<CycleTimeCalculationItemViewModel>
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The machining calculator.
        /// </summary>
        private MachiningCalculator machiningCalculator;

        /// <summary>
        /// The machine feed speed.
        /// </summary>
        private decimal? feedSpeed;

        /// <summary>
        /// The selected grinding material.
        /// </summary>
        private GrindingMaterial selectedMaterial;

        /// <summary>
        /// The process description.
        /// </summary>
        private string processDescription;

        /// <summary>
        /// The selected grinding operation type.
        /// </summary>
        private GrindingOperationType selectedGrindingOperationType;

        /// <summary>
        /// The selected grinding grain size.
        /// </summary>
        private decimal selectedGrindingGrainSize;

        /// <summary>
        /// The selected cutting depth per cycle.
        /// </summary>
        private decimal selectedCuttingDepth;

        /// <summary>
        /// The grinding tool diameter.
        /// </summary>
        private decimal? grindingToolDiameter;

        /// <summary>
        /// The grinding tool width.
        /// </summary>
        private decimal? grindingToolWidth;

        /// <summary>
        /// The start stop time for machine feed.
        /// </summary>
        private decimal? startStopTime;

        /// <summary>
        /// The machine feed length.
        /// </summary>
        private decimal? feedLength;

        /// <summary>
        /// The grinding part diameter.
        /// </summary>
        private decimal? grindingPartDiameter;

        /// <summary>
        /// The grinding length.
        /// </summary>
        private decimal? grindingLength;

        /// <summary>
        /// The grinding width.
        /// </summary>
        private decimal? grindingWidth;

        /// <summary>
        /// The grinding depth.
        /// </summary>
        private decimal? grindingDepth;

        /// <summary>
        /// The tool change time.
        /// </summary>
        private decimal? toolChangeTime;

        /// <summary>
        /// The calculated turning speed displayed in tooltip.
        /// </summary>
        private string calculatedTurningSpeed;

        /// <summary>
        /// The turning speed.
        /// </summary>
        private decimal? turningSpeed;

        /// <summary>
        /// The calculated feed rate displayed in tooltip.
        /// </summary>
        private string calculatedFeedRate;

        /// <summary>
        /// The feed rate.
        /// </summary>
        private decimal? feedRate;

        /// <summary>
        /// The resulting average grain distance.
        /// </summary>
        private decimal? resultingAvgGrainDistance;

        /// <summary>
        /// The resulting h(m).
        /// </summary>
        private decimal? resultingHM;

        /// <summary>
        /// The resulting grinding time.
        /// </summary>
        private decimal? resultingGrindingTime;

        /// <summary>
        /// The resulting gross process time.
        /// </summary>
        private decimal? resultingCycleTime;

        /// <summary>
        /// A value indicating whether the machining calculator is in expert mode or not.
        /// </summary>
        private bool isExpertMode;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="MachiningCalculatorGrindingViewModel" /> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        /// <param name="messenger">The messenger.</param>
        [ImportingConstructor]
        public MachiningCalculatorGrindingViewModel(IWindowService windowService, IMessenger messenger)
        {
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("messenger", messenger);

            this.windowService = windowService;
            this.messenger = messenger;

            InitializeProperties();
        }

        #region Properties

        /// <summary>
        /// Gets the list of grinding materials.
        /// </summary>
        public List<GrindingMaterial> GrindingMaterials { get; private set; }

        /// <summary>
        /// Gets the list of grinding grain sizes.
        /// </summary>
        public List<decimal> GrindingGrainSizes { get; private set; }

        /// <summary>
        /// Gets the list of grinding cutting depths.
        /// </summary>
        public List<decimal> GrindingCuttingDepths { get; private set; }

        /// <summary>
        /// Gets or sets the feed speed.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_FeedSpeed", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? FeedSpeed
        {
            get { return this.feedSpeed; }
            set { this.SetProperty(ref this.feedSpeed, value, () => this.FeedSpeed, this.UpdateGrindingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the selected grinding material.
        /// </summary>
        [UndoableProperty]
        public GrindingMaterial SelectedMaterial
        {
            get { return this.selectedMaterial; }
            set { this.SetProperty(ref this.selectedMaterial, value, () => this.SelectedMaterial, this.UpdateGrindingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the process description.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public string ProcessDescription
        {
            get { return this.processDescription; }
            set { this.SetProperty(ref this.processDescription, value, () => this.ProcessDescription); }
        }

        /// <summary>
        /// Gets or sets the selected grinding operation type.
        /// </summary>
        [UndoableProperty]
        public GrindingOperationType SelectedGrindingOperationType
        {
            get { return this.selectedGrindingOperationType; }
            set { this.SetProperty(ref this.selectedGrindingOperationType, value, () => this.SelectedGrindingOperationType, this.UpdateGrindingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the selected grinding grain size.
        /// </summary>
        [UndoableProperty]
        public decimal SelectedGrindingGrainSize
        {
            get { return this.selectedGrindingGrainSize; }
            set { this.SetProperty(ref this.selectedGrindingGrainSize, value, () => this.SelectedGrindingGrainSize, this.UpdateGrindingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the selected cutting depth per cycle.
        /// </summary>
        [UndoableProperty]
        public decimal SelectedCuttingDepth
        {
            get { return this.selectedCuttingDepth; }
            set { this.SetProperty(ref this.selectedCuttingDepth, value, () => this.SelectedCuttingDepth, this.UpdateGrindingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the grinding tool diameter.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_ToolDiameter", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? GrindingToolDiameter
        {
            get { return this.grindingToolDiameter; }
            set { this.SetProperty(ref this.grindingToolDiameter, value, () => this.GrindingToolDiameter, this.UpdateGrindingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the grinding tool width.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_ToolWidth", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? GrindingToolWidth
        {
            get { return this.grindingToolWidth; }
            set { this.SetProperty(ref this.grindingToolWidth, value, () => this.GrindingToolWidth, this.UpdateGrindingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the start stop time for machine feed.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_StartStopTime", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? StartStopTime
        {
            get { return this.startStopTime; }
            set { this.SetProperty(ref this.startStopTime, value, () => this.StartStopTime, this.UpdateGrindingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the machine feed length.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_FeedLength", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? FeedLength
        {
            get { return this.feedLength; }
            set { this.SetProperty(ref this.feedLength, value, () => this.FeedLength, this.UpdateGrindingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the grinding part diameter.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_FeedLength", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? GrindingPartDiameter
        {
            get { return this.grindingPartDiameter; }
            set { this.SetProperty(ref this.grindingPartDiameter, value, () => this.GrindingPartDiameter, this.UpdateGrindingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the grinding length.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_MillingLength", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? GrindingLength
        {
            get { return this.grindingLength; }
            set { this.SetProperty(ref this.grindingLength, value, () => this.GrindingLength, this.UpdateGrindingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the grinding width.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_MillingWidth", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? GrindingWidth
        {
            get { return this.grindingWidth; }
            set { this.SetProperty(ref this.grindingWidth, value, () => this.GrindingWidth, this.UpdateGrindingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the grinding depth.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_MillingDepth", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? GrindingDepth
        {
            get { return this.grindingDepth; }
            set { this.SetProperty(ref this.grindingDepth, value, () => this.GrindingDepth, this.UpdateGrindingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the tool change time.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_ToolChangeTime", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? ToolChangeTime
        {
            get { return this.toolChangeTime; }
            set { this.SetProperty(ref this.toolChangeTime, value, () => this.ToolChangeTime, this.UpdateGrindingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the calculated turning speed displayed in tooltip.
        /// </summary>
        public string CalculatedTurningSpeed
        {
            get { return this.calculatedTurningSpeed; }
            set { this.SetProperty(ref this.calculatedTurningSpeed, value, () => this.CalculatedTurningSpeed); }
        }

        /// <summary>
        /// Gets or sets the turning speed.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_TurningSpeed", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? TurningSpeed
        {
            get
            {
                return this.turningSpeed;
            }

            set
            {
                if (value != this.turningSpeed)
                {
                    var oldValue = this.turningSpeed;
                    this.turningSpeed = value.HasValue ? Math.Round(value.Value) : (decimal?)null;
                    this.OnPropertyChanged(() => this.TurningSpeed);

                    if (oldValue != this.turningSpeed)
                    {
                        this.UpdateGrindingCalculationResult();
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the calculated feed rate displayed in tooltip.
        /// </summary>
        public string CalculatedFeedRate
        {
            get { return this.calculatedFeedRate; }
            set { this.SetProperty(ref this.calculatedFeedRate, value, () => this.CalculatedFeedRate); }
        }

        /// <summary>
        /// Gets or sets the feed rate.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_FeedRate", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? FeedRate
        {
            get
            {
                return this.feedRate;
            }

            set
            {
                if (value != this.feedRate)
                {
                    var oldValue = this.feedRate;
                    this.feedRate = value.HasValue ? Math.Round(value.Value, 2) : (decimal?)null;
                    this.OnPropertyChanged(() => this.FeedRate);

                    if (oldValue != this.feedRate)
                    {
                        this.UpdateGrindingCalculationResult();
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the resulting average grain distance.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? ResultingAvgGrainDistance
        {
            get { return this.resultingAvgGrainDistance; }
            set { this.SetProperty(ref this.resultingAvgGrainDistance, value, () => this.ResultingAvgGrainDistance); }
        }

        /// <summary>
        /// Gets or sets the resulting h(m).
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? ResultingHM
        {
            get { return this.resultingHM; }
            set { this.SetProperty(ref this.resultingHM, value, () => this.ResultingHM); }
        }

        /// <summary>
        /// Gets or sets the resulting grinding time.
        /// </summary>
        public decimal? ResultingGrindingTime
        {
            get { return this.resultingGrindingTime; }
            set { this.SetProperty(ref this.resultingGrindingTime, value, () => this.ResultingGrindingTime); }
        }

        /// <summary>
        /// Gets or sets the resulting cycle time.
        /// </summary>
        public decimal? ResultingCycleTime
        {
            get { return this.resultingCycleTime; }
            set { this.SetProperty(ref this.resultingCycleTime, value, () => this.ResultingCycleTime); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the machining calculator is in expert mode or not.
        /// </summary>
        public bool IsExpertMode
        {
            get { return this.isExpertMode; }
            set { this.SetProperty(ref this.isExpertMode, value, () => this.IsExpertMode, this.UpdateGrindingCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the token used for sending messages to machining calculator.
        /// </summary>
        public Guid MessengerTokenForMachining { get; set; }

        /// <summary>
        /// Gets or sets the UndoManager from CycleTime parent window.
        /// </summary>
        public UndoManager ParentUndoManager { get; set; }

        #endregion

        /// <summary>
        /// Called when the Model has changed.
        /// </summary>
        protected override void OnModelChanged()
        {
            base.OnModelChanged();

            if ((this.Model != null) && this.Model.MachiningType.Value == MachiningType.Grinding && this.EditMode == ViewModelEditMode.Edit)
            {
                this.LoadGrindingResultDataFromCalculationToEdit();
            }
        }

        /// <summary>
        /// Initializes the properties of this instance.
        /// </summary>
        private void InitializeProperties()
        {
            this.machiningCalculator = new MachiningCalculator();
            this.GrindingMaterials = new List<GrindingMaterial>(this.machiningCalculator.Knowledgebase.GrindingData.MaterialsToMachine);
            this.GrindingGrainSizes = new List<decimal>(this.machiningCalculator.Knowledgebase.GrindingData.GrainSizeValues);
            this.GrindingCuttingDepths = new List<decimal>(this.machiningCalculator.Knowledgebase.GrindingData.GrindingDepthValues);

            this.SelectedMaterial = this.GrindingMaterials.FirstOrDefault();
            this.SelectedGrindingOperationType = GrindingOperationType.SurfaceGrindingColinear;
            this.FeedSpeed = this.machiningCalculator.Knowledgebase.GrindingData.DefaultMachineFeedSpeed;
            this.SelectedGrindingGrainSize = this.machiningCalculator.Knowledgebase.GrindingData.DefaultGrindingGrainSize;
            this.SelectedCuttingDepth = this.machiningCalculator.Knowledgebase.GrindingData.DefaultCuttingDepthPerCycle;
            this.GrindingToolDiameter = this.machiningCalculator.Knowledgebase.GrindingData.DefaultGrindingToolDiameter;
            this.GrindingToolWidth = this.machiningCalculator.Knowledgebase.GrindingData.DefaultGrindingToolWidth;
            this.StartStopTime = this.machiningCalculator.Knowledgebase.GrindingData.DefaultStartStopTimeForMachineFeed;
            this.FeedLength = this.machiningCalculator.Knowledgebase.GrindingData.DefaultMachineFeedLength;
            this.GrindingPartDiameter = this.machiningCalculator.Knowledgebase.GrindingData.DefaultGrindingPartDiameter;
            this.GrindingLength = this.machiningCalculator.Knowledgebase.GrindingData.DefaultGrindingLength;
            this.GrindingWidth = this.machiningCalculator.Knowledgebase.GrindingData.DefaultGrindingWidth;
            this.GrindingDepth = this.machiningCalculator.Knowledgebase.GrindingData.DefaultGrindingDepth;
            this.ToolChangeTime = this.machiningCalculator.Knowledgebase.GrindingData.DefaultToolChangeTime;
        }

        /// <summary>
        /// Loads the grinding result data from the object that the screen is editing.
        /// </summary>
        private void LoadGrindingResultDataFromCalculationToEdit()
        {
            if (this.Model.MachiningCalculationData == null ||
                this.Model.MachiningCalculationData.Value == null)
            {
                return;
            }

            UnicodeEncoding encoder = new UnicodeEncoding();
            string data = encoder.GetString(this.Model.MachiningCalculationData.Value);

            if (!string.IsNullOrEmpty(data))
            {
                this.ProcessDescription = this.Model.ToleranceType.Value;

                string[] parts = data.Split(';');
                if (parts.Length > 1)
                {
                    int grindingMaterial;
                    if (int.TryParse(parts[1], out grindingMaterial))
                    {
                        this.SelectedMaterial = this.GrindingMaterials[grindingMaterial];
                        if (this.SelectedMaterial == null)
                        {
                            this.windowService.MessageDialogService.Show(LocalizedResources.MachiningCalculator_MaterialUnavailable, MessageDialogType.Error);
                        }
                    }
                    else
                    {
                        this.SelectedMaterial = null;
                    }
                }

                if (parts.Length > 2)
                {
                    GrindingOperationType grindingOperationType;
                    if (Enum.TryParse(parts[2], out grindingOperationType))
                    {
                        this.SelectedGrindingOperationType = grindingOperationType;
                    }
                }

                if (parts.Length > 3)
                {
                    int grindingGrainSize;
                    if (int.TryParse(parts[3], out grindingGrainSize))
                    {
                        if (grindingGrainSize < this.GrindingGrainSizes.Count)
                        {
                            this.SelectedGrindingGrainSize = this.GrindingGrainSizes[grindingGrainSize];
                        }
                    }
                }

                if (parts.Length > 4)
                {
                    int grindingCuttingDepth;
                    if (int.TryParse(parts[4], out grindingCuttingDepth))
                    {
                        if (grindingCuttingDepth < this.GrindingCuttingDepths.Count)
                        {
                            this.SelectedCuttingDepth = this.GrindingCuttingDepths[grindingCuttingDepth];
                        }
                    }
                }

                if (parts.Length > 0)
                {
                    this.FeedSpeed = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[0]);
                }

                if (parts.Length > 5)
                {
                    this.GrindingToolDiameter = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[5]);
                }

                if (parts.Length > 6)
                {
                    this.GrindingToolWidth = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[6]);
                }

                if (parts.Length > 7)
                {
                    this.StartStopTime = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[7]);
                }

                if (parts.Length > 8)
                {
                    this.FeedLength = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[8]);
                }

                if (parts.Length > 9)
                {
                    this.GrindingPartDiameter = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[9]);
                }

                if (parts.Length > 10)
                {
                    this.GrindingLength = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[10]);
                }

                if (parts.Length > 11)
                {
                    this.GrindingWidth = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[11]);
                }

                if (parts.Length > 12)
                {
                    this.GrindingDepth = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[12]);
                }

                bool expertMode = false;
                if (parts.Length > 13)
                {
                    bool.TryParse(parts[13], out expertMode);
                    this.IsExpertMode = expertMode;

                    if (this.IsExpertMode)
                    {
                        var message = new NotificationMessage<CycleTimeCalculationItemViewModel>(MachiningCalculatorViewModel.CalculatorNotificationMessages.ExpertModeChanged, this.Model);
                        this.messenger.Send<NotificationMessage<CycleTimeCalculationItemViewModel>>(message, this.MessengerTokenForMachining);
                    }
                }

                if (expertMode && parts.Length > 14)
                {
                    this.FeedRate = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[14]);
                }

                if (parts.Length > 15)
                {
                    this.ToolChangeTime = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[15]);
                }

                if (expertMode && parts.Length > 16)
                {
                    this.TurningSpeed = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[16]);
                }
            }
        }

        /// <summary>
        /// Performs the grinding operation calculation and updates the result on the UI.
        /// </summary>
        private void UpdateGrindingCalculationResult()
        {
            using (this.UndoManager.Pause())
            {
                GrindingCalculationResult result = CalculateGrindingOperation();
                this.ResultingAvgGrainDistance = result.AverageGainDistance;
                this.ResultingHM = result.HM;
                this.ResultingGrindingTime = result.GrindingTime;
                this.ResultingCycleTime = result.GrossProcessTime;

                if (this.IsExpertMode)
                {
                    var resultedTurningSpeed = result.TurningSpeedOfGrindingTool.HasValue ? Math.Round(result.TurningSpeedOfGrindingTool.Value) : (decimal?)null;
                    this.CalculatedTurningSpeed = Formatter.FormatNumber(resultedTurningSpeed);

                    var resultedFeedRate = result.FeedRate.HasValue ? Math.Round(result.FeedRate.Value, 2) : (decimal?)null;
                    this.CalculatedFeedRate = Formatter.FormatNumber(resultedFeedRate);
                }
                else
                {
                    this.TurningSpeed = result.TurningSpeedOfGrindingTool;
                    this.FeedRate = result.FeedRate;
                }
            }
        }

        /// <summary>
        /// Performs the save operation. Executed by the SaveCommand.
        /// </summary>
        protected override void Save()
        {
            if (this.EditMode == ViewModelEditMode.Create)
            {
                // If the view model is not in edit mode, it does not have a model attached, so we have to create it.
                var calculation = new CycleTimeCalculation();
                var calculationItemVM = new CycleTimeCalculationItemViewModel(windowService, messenger);
                calculationItemVM.EditMode = ViewModelEditMode.Create;

                this.Model = calculationItemVM;
                calculationItemVM.Model = calculation;
            }

            this.SaveToModel();

            if (this.EditMode == ViewModelEditMode.Create)
            {
                var message = new NotificationMessage<CycleTimeCalculationItemViewModel>(MachiningCalculatorViewModel.CalculatorNotificationMessages.AddMachiningCalculator, this.Model);
                this.messenger.Send<NotificationMessage<CycleTimeCalculationItemViewModel>>(message, this.MessengerTokenForMachining);
            }
            else
            {
                var message = new NotificationMessage<CycleTimeCalculationItemViewModel>(MachiningCalculatorViewModel.CalculatorNotificationMessages.UpdateMachiningCalculator, this.Model);
                this.messenger.Send<NotificationMessage<CycleTimeCalculationItemViewModel>>(message, this.MessengerTokenForMachining);
            }
        }

        /// <summary>
        /// Executes the logic associated with the SaveToModelCommand command.
        /// </summary>
        protected override void SaveToModel()
        {
            if (this.ParentUndoManager != null)
            {
                using (this.ParentUndoManager.StartBatch(navigateToBatchControls: true))
                {
                    this.SaveGrindingData();
                }
            }
            else
            {
                this.SaveGrindingData();
            }
        }

        /// <summary>
        /// Saves all changed back into the model and resets the changed status.
        /// </summary>
        private void SaveGrindingData()
        {
            GrindingCalculationResult result = CalculateGrindingOperation();

            this.Model.Description.Value = LocalizedResources.MachiningCalculator_Grinding + " - " + this.SelectedGrindingOperationType;
            this.Model.MachiningVolume.Value = result.MachiningVolume.HasValue ? result.MachiningVolume.Value : 0;
            this.Model.MachiningType.Value = MachiningType.Grinding;

            decimal? toolChangeTime = this.ToolChangeTime;
            decimal? transportTime = result.GrossProcessTime - result.GrindingTime - toolChangeTime;
            this.Model.TransportClampingTime.Value = transportTime.GetValueOrDefault();
            this.Model.MachiningTime.Value = result.GrindingTime.GetValueOrDefault();
            this.Model.ToolChangeTime.Value = toolChangeTime.GetValueOrDefault();
            this.Model.ToleranceType.Value = this.ProcessDescription;

            this.SaveGrindingResultDataToBinary();
        }

        /// <summary>
        /// Calculates the values for the Grinding operation.
        /// </summary>
        /// <returns>The result.</returns>
        private GrindingCalculationResult CalculateGrindingOperation()
        {
            GrindingCalculationInputData input = new GrindingCalculationInputData();

            input.MachineFeedSpeed = this.FeedSpeed;
            input.Material = this.SelectedMaterial;
            input.OperationType = this.SelectedGrindingOperationType;
            input.GrindingGrainSize = this.SelectedGrindingGrainSize;
            input.CuttingDepthPerCycle = this.SelectedCuttingDepth;
            input.GrindingToolDiameter = this.GrindingToolDiameter;
            input.GrindingToolWidth = this.GrindingToolWidth;
            input.StartStopTimeForMachineFeed = this.StartStopTime;
            input.MachineFeedLength = this.FeedLength;
            input.GrindingPartDiameter = this.GrindingPartDiameter;
            input.GrindingLength = this.GrindingLength;
            input.GrindingWidth = this.GrindingWidth;
            input.GrindingDepth = this.GrindingDepth;
            input.ToolChangeTime = this.ToolChangeTime;
            if (this.isExpertMode)
            {
                input.ExpertModeCalculation = true;
                input.FeedRate = this.FeedRate;
                input.TurningSpeed = this.TurningSpeed;
            }
            else
            {
                input.ExpertModeCalculation = false;
            }

            return this.machiningCalculator.CalculateGrindingOperation(input);
        }

        /// <summary>
        /// Saves the current grinding result data into the machining calculation data.
        /// </summary>
        private void SaveGrindingResultDataToBinary()
        {
            // The data will be saved as a comma separated list.
            string data = string.Empty;

            string grindingMaterial = string.Empty;
            GrindingMaterial material = this.SelectedMaterial;
            if (material != null)
            {
                grindingMaterial = this.GrindingMaterials.IndexOf(material).ToString();
            }

            string grindingOperationType = string.Empty;
            grindingOperationType = ((int)this.SelectedGrindingOperationType).ToString();

            string grindingGrainSize = string.Empty;
            grindingGrainSize = this.GrindingGrainSizes.IndexOf(this.SelectedGrindingGrainSize).ToString();

            string grindingCuttingDepth = string.Empty;
            grindingCuttingDepth = this.GrindingCuttingDepths.IndexOf(this.SelectedCuttingDepth).ToString();

            data += (this.FeedSpeed.HasValue ? this.FeedSpeed.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += grindingMaterial + ";";
            data += grindingOperationType + ";";
            data += grindingGrainSize + ";";
            data += grindingCuttingDepth + ";";
            data += (this.GrindingToolDiameter.HasValue ? this.GrindingToolDiameter.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += (this.GrindingToolWidth.HasValue ? this.GrindingToolWidth.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += (this.StartStopTime.HasValue ? this.StartStopTime.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += (this.FeedLength.HasValue ? this.FeedLength.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += (this.GrindingPartDiameter.HasValue ? this.GrindingPartDiameter.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += (this.GrindingLength.HasValue ? this.GrindingLength.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += (this.GrindingWidth.HasValue ? this.GrindingWidth.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += (this.GrindingDepth.HasValue ? this.GrindingDepth.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += this.IsExpertMode + ";";
            data += (this.FeedRate.HasValue ? this.FeedRate.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += (this.ToolChangeTime.HasValue ? this.ToolChangeTime.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += this.TurningSpeed.HasValue ? this.TurningSpeed.Value.ToString(CultureInfo.InvariantCulture) : string.Empty;

            UnicodeEncoding encoding = new UnicodeEncoding();
            this.Model.MachiningCalculationData.Value = encoding.GetBytes(data);
        }
    }
}
