﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using ZPKTool.Data;
using ZPKTool.Gui.Managers;

namespace ZPKTool.Gui.ViewModels.ProcessInfrastructure
{
    /// <summary>
    /// The data displayed by an item in Total Material section, in Reject Cost Overview, in the Process screen
    /// </summary>
    public class ScrapViewTotalMaterialItem : INotifyPropertyChanged
    {
        /// <summary>
        /// The name of the material cost
        /// </summary>
        private string name;

        /// <summary>
        /// The index of the material cost
        /// </summary>
        private int index;

        /// <summary>
        /// The total cost of parts
        /// </summary>
        private decimal partsTotalCost;

        /// <summary>
        /// The total cost of raw materials
        /// </summary>
        private decimal rawMaterialsTotalCost;

        /// <summary>
        /// The total cost of commodities
        /// </summary>
        private decimal commoditiesTotalCost;

        /// <summary>
        /// The total cost of raw parts
        /// </summary>
        private decimal rawPartsTotalCost;

        /// <summary>
        /// The base unit.
        /// </summary>
        private Currency baseCurrency;

        /// <summary>
        /// The UI unit.
        /// </summary>
        private Unit uiUnit;

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets the name of the material cost
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                if (this.name != value)
                {
                    this.name = value;
                    this.RaisePropertyChangeEvent("Name");
                }
            }
        }

        /// <summary>
        /// Gets or sets the index of the material cost
        /// </summary>
        public int Index
        {
            get
            {
                return this.index;
            }

            set
            {
                if (this.index != value)
                {
                    this.index = value;
                    this.RaisePropertyChangeEvent("Index");
                }
            }
        }

        /// <summary>
        /// Gets or sets the total cost of parts
        /// </summary>
        public decimal PartsTotalCost
        {
            get
            {
                return this.partsTotalCost;
            }

            set
            {
                if (this.partsTotalCost != value)
                {
                    this.partsTotalCost = value;
                    this.RaisePropertyChangeEvent("PartsTotalCost");
                }
            }
        }

        /// <summary>
        /// Gets or sets the total cost of raw materials
        /// </summary>
        public decimal RawMaterialsTotalCost
        {
            get
            {
                return this.rawMaterialsTotalCost;
            }

            set
            {
                if (this.rawMaterialsTotalCost != value)
                {
                    this.rawMaterialsTotalCost = value;
                    this.RaisePropertyChangeEvent("RawMaterialsTotalCost");
                }
            }
        }

        /// <summary>
        /// Gets or sets the total cost of commodities
        /// </summary>
        public decimal CommoditiesTotalCost
        {
            get
            {
                return this.commoditiesTotalCost;
            }

            set
            {
                if (this.commoditiesTotalCost != value)
                {
                    this.commoditiesTotalCost = value;
                    this.RaisePropertyChangeEvent("CommoditiesTotalCost");
                }
            }
        }

        /// <summary>
        /// Gets or sets the total cost of raw parts
        /// </summary>
        public decimal RawPartsTotalCost
        {
            get
            {
                return this.rawPartsTotalCost;
            }

            set
            {
                if (this.rawPartsTotalCost != value)
                {
                    this.rawPartsTotalCost = value;
                    this.RaisePropertyChangeEvent("RawPartsTotalCost");
                }
            }
        }

        /// <summary>
        /// Gets or sets the base currency.
        /// </summary>
        public Currency BaseCurrency
        {
            get
            {
                return this.baseCurrency;
            }

            set
            {
                if (this.baseCurrency != value)
                {
                    this.baseCurrency = value;
                    this.RaisePropertyChangeEvent("BaseCurrency");
                }
            }
        }

        /// <summary>
        /// Gets or sets the UI unit.
        /// </summary>
        public Unit UIUnit
        {
            get
            {
                return this.uiUnit;
            }

            set
            {
                if (this.uiUnit != value)
                {
                    this.uiUnit = value;
                    this.RaisePropertyChangeEvent("UIUnit");
                }
            }
        }

        /// <summary>
        /// Raises the PropertyChange event.
        /// </summary>
        /// <param name="propertyName">The name of the property which has been changed</param>
        private void RaisePropertyChangeEvent(string propertyName)
        {
            if (null != PropertyChanged)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}