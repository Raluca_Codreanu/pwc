﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels.ProcessInfrastructure
{
    /// <summary>
    /// DataGrid machine item.
    /// </summary>
    public class MachineDataGridItem : INotifyPropertyChanged, INotifyPropertyChanging, IEditableObject
    {
        /// <summary>
        /// The UndoManager.
        /// </summary>
        private UndoManager undoManager;

        /// <summary>
        /// The order number of machines.
        /// </summary>
        private int orderNumber;

        /// <summary>
        /// The machine entity.
        /// </summary>
        private Machine machine;

        /// <summary>
        /// The cost of the machine.
        /// </summary>
        private MachineCost cost;

        /// <summary>
        /// The amount displayed in the machines list for this item.
        /// </summary>
        private int machineAmount;

        /// <summary>
        /// A value indicating whether the machine is specific to a project.
        /// </summary>
        private bool isProjectSpecific;

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Occurs when a property value is changing.
        /// </summary>
        public event PropertyChangingEventHandler PropertyChanging;

        /// <summary>
        /// Gets or sets the UndoManager.
        /// </summary>
        public UndoManager UndoManager
        {
            get
            {
                return this.undoManager;
            }

            set
            {
                if (this.undoManager != value)
                {
                    this.undoManager = value;
                    this.UndoManager.TrackObject(this);
                }
            }
        }

        /// <summary>
        /// Gets or sets the machine.
        /// </summary>
        public Machine Machine
        {
            get
            {
                return this.machine;
            }

            set
            {
                if (this.machine != value)
                {
                    this.machine = value;
                    this.MachineAmount = this.machine.Amount;
                    this.IsProjectSpecific = this.machine.IsProjectSpecific;
                }
            }
        }

        /// <summary>
        /// Gets or sets the order number.
        /// </summary>
        /// <value>
        /// The order number.
        /// </value>
        public int OrderNumber
        {
            get
            {
                return this.orderNumber;
            }

            set
            {
                this.orderNumber = value;
                RaisePropertyChangeEvent(ReflectionUtils.GetPropertyName(() => this.OrderNumber));
            }
        }

        /// <summary>
        /// Gets or sets the cost of the machine.
        /// </summary>
        public MachineCost Cost
        {
            get
            {
                return this.cost;
            }

            set
            {
                if (this.cost != value)
                {
                    this.cost = value;
                    this.RaisePropertyChangeEvent(ReflectionUtils.GetPropertyName(() => this.Cost));
                }
            }
        }

        /// <summary>
        /// Gets or sets the amount displayed in the machines list for this item.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public int MachineAmount
        {
            get
            {
                return this.machineAmount;
            }

            set
            {
                if (this.machineAmount != value)
                {
                    this.RaisePropertyChanging(ReflectionUtils.GetPropertyName(() => this.MachineAmount));
                    this.machineAmount = value;
                    this.RaisePropertyChangeEvent(ReflectionUtils.GetPropertyName(() => this.MachineAmount));
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the machine is specific to a project.
        /// </summary>
        [UndoableProperty]
        public bool IsProjectSpecific
        {
            get
            {
                return this.isProjectSpecific;
            }

            set
            {
                if (this.isProjectSpecific != value)
                {
                    this.RaisePropertyChanging(ReflectionUtils.GetPropertyName(() => this.IsProjectSpecific));
                    this.isProjectSpecific = value;
                    this.RaisePropertyChangeEvent(ReflectionUtils.GetPropertyName(() => this.IsProjectSpecific));
                }
            }
        }

        /// <summary>
        /// Saves all changes back into the model (the machine).
        /// </summary>
        public void SaveDataToModel()
        {
            this.Machine.Amount = this.MachineAmount;
            this.machine.IsProjectSpecific = this.IsProjectSpecific;
        }

        /// <summary>
        /// Raises the PropertyChange event.
        /// </summary>
        /// <param name="propertyName">The name of the property which has been changed</param>
        private void RaisePropertyChangeEvent(string propertyName)
        {
            if (null != PropertyChanged)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// This raises the INotifyPropertyChanging.PropertyChanging event to indicate a specific property value is changing.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        protected void RaisePropertyChanging(string propertyName)
        {
            var handler = this.PropertyChanging;
            if (handler != null)
            {
                handler(this, new PropertyChangingEventArgs(propertyName));
            }
        }

        /// <summary>
        /// Begins an edit on an object.
        /// </summary>
        public void BeginEdit()
        {
        }

        /// <summary>
        /// Discards changes since the last <see cref="M:System.ComponentModel.IEditableObject.BeginEdit"/> call.
        /// </summary>
        public void CancelEdit()
        {
        }

        /// <summary>
        /// Pushes changes since the last <see cref="M:System.ComponentModel.IEditableObject.BeginEdit"/> or <see cref="M:System.ComponentModel.IBindingList.AddNew"/> call into the underlying object.
        /// </summary>
        public void EndEdit()
        {
        }
    }
}
