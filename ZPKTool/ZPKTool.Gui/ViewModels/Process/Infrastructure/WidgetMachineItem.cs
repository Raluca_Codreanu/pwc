﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace ZPKTool.Gui.ViewModels.ProcessInfrastructure
{
    /// <summary>
    /// The data displayed for a machine entity in process step widget screen
    /// </summary>
    public class WidgetMachineItem : INotifyPropertyChanged
    {
        /// <summary>
        /// The name of the machine to which the scrap/reject cost belongs.
        /// </summary>
        private string machineName;

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets the id of the machine
        /// </summary>
        public Guid MachineId { get; set; }

        /// <summary>
        /// Gets or sets the name of the step to which the scrap/reject cost belongs.
        /// </summary>
        public string MachineName
        {
            get
            {
                return this.machineName;
            }

            set
            {
                if (this.machineName != value)
                {
                    this.machineName = value;
                    this.RaisePropertyChangeEvent("MachineName");
                }
            }
        }

        /// <summary>
        /// Raises the PropertyChange event.
        /// </summary>
        /// <param name="propertyName">The name of the property which has been changed</param>
        private void RaisePropertyChangeEvent(string propertyName)
        {
            if (null != PropertyChanged)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
