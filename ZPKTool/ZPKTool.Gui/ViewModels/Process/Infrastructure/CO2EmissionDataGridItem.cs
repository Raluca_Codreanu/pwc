﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Media;
using ZPKTool.Calculations.CO2Emissions;
using ZPKTool.Data;

namespace ZPKTool.Gui.ViewModels.ProcessInfrastructure
{
    /// <summary>
    /// This class is used for data binding in the CO2Emission data grid in the ResultDetails screen.
    /// </summary>
    public class CO2EmissionDataGridItem : INotifyPropertyChanged
    {
        /// <summary>
        /// The Process step name
        /// </summary>
        private string processStepName;

        /// <summary>
        /// The machine name.
        /// </summary>
        private string machineName;

        /// <summary>
        /// The machine id.
        /// </summary>
        private Guid machineId;

        /// <summary>
        /// The Process step number.
        /// </summary>
        private int? processStepNumber;

        /// <summary>
        /// The process and machines emissions
        /// </summary>
        private CO2Emissions emissions;

        /// <summary>
        /// The Process time.
        /// </summary>
        private decimal? processTime;

        /// <summary>
        /// The Process parts per cycle.
        /// </summary>
        private int? processPartsPerCycle;

        /// <summary>
        /// The Machine power consumption.
        /// </summary>
        private decimal? machinePowerConsumption;

        /// <summary>
        /// The Machine fossil energy consumption.
        /// </summary>
        private decimal? machineFossilEnergyConsumption;

        /// <summary>
        /// The Machine renewable energy consumption.
        /// </summary>
        private decimal? machineRenewableEnergyConsumption;

        /// <summary>
        /// The Process step id.
        /// </summary>
        private Guid processStepId;

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets the Process step id.
        /// </summary>
        public Guid ProcessStepId
        {
            get
            {
                return this.processStepId;
            }

            set
            {
                if (this.processStepId != value)
                {
                    this.processStepId = value;
                    this.RaisePropertyChangeEvent("ProcessStepId");
                }
            }
        }

        /// <summary>
        /// Gets or sets the Process step number.
        /// </summary>
        public int? ProcessStepNumber
        {
            get
            {
                return this.processStepNumber;
            }

            set
            {
                if (this.processStepNumber != value)
                {
                    this.processStepNumber = value;
                    this.RaisePropertyChangeEvent("ProcessStepNumber");
                }
            }
        }

        /// <summary>
        /// Gets or sets the Process step name.
        /// </summary>
        public string ProcessStepName
        {
            get
            {
                return this.processStepName;
            }

            set
            {
                if (this.processStepName != value)
                {
                    this.processStepName = value;
                    this.RaisePropertyChangeEvent("ProcessStepName");
                }
            }
        }

        /// <summary>
        /// Gets or sets the machine name.
        /// </summary>
        public string MachineName
        {
            get
            {
                return this.machineName;
            }

            set
            {
                if (this.machineName != value)
                {
                    this.machineName = value;
                    this.RaisePropertyChangeEvent("MachineName");
                }
            }
        }

        /// <summary>
        /// Gets or sets the machine id.
        /// </summary>
        public Guid MachineId
        {
            get
            {
                return this.machineId;
            }

            set
            {
                if (this.machineId != value)
                {
                    this.machineId = value;
                    this.RaisePropertyChangeEvent("MachineId");
                }
            }
        }

        /// <summary>
        /// Gets or sets the Process time.
        /// </summary>
        public decimal? ProcessTime
        {
            get
            {
                return this.processTime;
            }

            set
            {
                if (this.processTime != value)
                {
                    this.processTime = value;
                    this.RaisePropertyChangeEvent("ProcessTime");
                }
            }
        }

        /// <summary>
        /// Gets or sets the Process parts per cycle.
        /// </summary>
        public int? ProcessPartsPerCycle
        {
            get
            {
                return this.processPartsPerCycle;
            }

            set
            {
                if (this.processPartsPerCycle != value)
                {
                    this.processPartsPerCycle = value;
                    this.RaisePropertyChangeEvent("ProcessPartsPerCycle");
                }
            }
        }

        /// <summary>
        /// Gets or sets the Machine power consumption.
        /// </summary>
        public decimal? MachinePowerConsumption
        {
            get
            {
                return this.machinePowerConsumption;
            }

            set
            {
                if (this.machinePowerConsumption != value)
                {
                    this.machinePowerConsumption = value;
                    this.RaisePropertyChangeEvent("MachinePowerConsumption");
                }
            }
        }

        /// <summary>
        /// Gets or sets the Machine fossil energy consumption.
        /// </summary>
        public decimal? MachineFossilEnergyConsumption
        {
            get
            {
                return this.machineFossilEnergyConsumption;
            }

            set
            {
                if (this.machineFossilEnergyConsumption != value)
                {
                    this.machineFossilEnergyConsumption = value;
                    this.RaisePropertyChangeEvent("MachineFossilEnergyConsumption");
                }
            }
        }

        /// <summary>
        /// Gets or sets the Machine renewable energy consumption.
        /// </summary>
        public decimal? MachineRenewableEnergyConsumption
        {
            get
            {
                return this.machineRenewableEnergyConsumption;
            }

            set
            {
                if (this.machineRenewableEnergyConsumption != value)
                {
                    this.machineRenewableEnergyConsumption = value;
                    this.RaisePropertyChangeEvent("MachineRenewableEnergyConsumption");
                }
            }
        }

        /// <summary>
        /// Gets or sets the process and machines emissions
        /// </summary>
        public CO2Emissions Emissions
        {
            get
            {
                return this.emissions;
            }

            set
            {
                if (this.emissions != value)
                {
                    this.emissions = value;
                    this.RaisePropertyChangeEvent("Emissions");
                }
            }
        }

        /// <summary>
        /// Raises the PropertyChange event.
        /// </summary>
        /// <param name="propertyName">The name of the property which has been changed</param>
        private void RaisePropertyChangeEvent(string propertyName)
        {
            if (null != PropertyChanged)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}