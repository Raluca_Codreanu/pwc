﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using ZPKTool.Calculations.CostCalculation;

namespace ZPKTool.Gui.ViewModels.ProcessInfrastructure
{
    /// <summary>
    /// The data displayed by an item of the Scrap/Reject View, in the Process screen
    /// </summary>
    public class ScrapViewItem : INotifyPropertyChanged
    {
        /// <summary>
        /// The name of the step to which the scrap/reject cost belongs.
        /// </summary>
        private string stepName;

        /// <summary>
        /// The id of the step to which the scrap/reject cost belongs.
        /// </summary>
        private Guid stepId;

        /// <summary>
        /// The data structure containing the cost calculation result of the step.
        /// </summary>
        private ProcessStepCost stepCost;

        /// <summary>
        /// The order number of the step.
        /// </summary>
        private string stepOrderNo;

        /// <summary>
        /// The reject ratio for the step.
        /// </summary>
        private decimal rejectRatio;

        /// <summary>
        /// Initializes a new instance of the <see cref="ScrapViewItem"/> class.
        /// </summary>
        public ScrapViewItem()
        {
        }

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
                
        /// <summary>
        /// Gets or sets the name of the step to which the scrap/reject cost belongs.
        /// </summary>
        public string StepName
        {
            get
            {
                return this.stepName;
            }

            set
            {
                if (this.stepName != value)
                {
                    this.stepName = value;
                    this.RaisePropertyChangeEvent("StepName");
                }
            }
        }

        /// <summary>
        /// Gets or sets the id of the step to which the scrap/reject cost belongs.
        /// </summary>
        public Guid StepId
        {
            get
            {
                return this.stepId;
            }

            set
            {
                if (this.stepId != value)
                {
                    this.stepId = value;
                    this.RaisePropertyChangeEvent("StepId");
                }
            }
        }

        /// <summary>
        /// Gets or sets the order number of the step.
        /// </summary>
        public string StepOrderNo
        {
            get
            {
                return this.stepOrderNo;
            }

            set
            {
                if (this.stepOrderNo != value)
                {
                    this.stepOrderNo = value;
                    this.RaisePropertyChangeEvent("StepOrderNo");
                }
            }
        }

        /// <summary>
        /// Gets or sets the reject ratio for the step.
        /// </summary>
        public decimal RejectRatio
        {
            get
            {
                return this.rejectRatio;
            }

            set
            {
                if (this.rejectRatio != value)
                {
                    this.rejectRatio = value;
                    this.RaisePropertyChangeEvent("RejectRatio");
                }
            }
        }

        /// <summary>
        /// Gets or sets the data structure containing the cost calculation result of the step.
        /// </summary>
        public ProcessStepCost StepCost
        {
            get
            {
                return this.stepCost;
            }

            set
            {
                if (this.stepCost != value)
                {
                    this.stepCost = value;
                    this.RaisePropertyChangeEvent("StepCost");
                }
            }
        }

        /// <summary>
        /// Raises the PropertyChange event.
        /// </summary>
        /// <param name="propertyName">The name of the property which has been changed</param>
        private void RaisePropertyChangeEvent(string propertyName)
        {
            if (null != PropertyChanged)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
