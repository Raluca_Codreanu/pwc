﻿using System.ComponentModel;
using System.Linq;
using System.Windows.Media;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Common;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels.ProcessInfrastructure
{
    /// <summary>
    /// This class represents an item from the Parts List of the Process screen.
    /// </summary>
    public abstract class PartsGridItem : ObservableObject, IEditableObject
    {
        #region Fields

        /// <summary>
        /// The UndoManager.
        /// </summary>
        private UndoManager undoManager;

        /// <summary>
        /// The amount displayed in the parts list for this item.
        /// </summary>
        private int? amount;

        /// <summary>
        /// The amount sum for the rest of the part grid items that have the same entity behind.
        /// </summary>
        private int amountSum;

        /// <summary>
        /// The amount value before entering in edit mode.
        /// </summary>
        private int oldAmount;

        /// <summary>
        /// The initial value of the <see cref="amount"/> field.
        /// </summary>
        private int initialAmountValue;

        /// <summary>
        /// The price displayed in the parts list for this item.
        /// </summary>
        private decimal? price;

        /// <summary>
        /// The cost displayed in the parts list for this item.
        /// </summary>
        private decimal? cost;

        /// <summary>
        /// Indicating whether the part is external.
        /// </summary>
        private bool isExternal;

        /// <summary>
        /// The initial value of the <see cref="isExternal"/> field.
        /// </summary>
        private bool initialIsExternalValue;

        /// <summary>
        /// A value indicating whether the SG&amp;A overhead is applied to the part when it is external.
        /// </summary>
        private bool hasExternalSGA;

        /// <summary>
        /// The initial value of the <see cref="hasExternalSGA"/> field.
        /// </summary>
        private bool initialHasExternalSGAValue;

        #endregion Fields

        /// <summary>
        /// Initializes a new instance of the <see cref="PartsGridItem" /> class.
        /// </summary>
        /// <param name="amount">The amount of the part/assembly represented by this item (the initial value).</param>
        /// <param name="isExternal">A value indicating whether the part/assembly is external.</param>
        /// <param name="hasExternalSGA">A value indicating whether SGA overhead is applied to the part when is external.</param>
        /// <param name="commodityOverheadRatio">The commodity overhead ratio that applies to the part/assembly.</param>
        /// <param name="commodityMarginRatio">The commodity margin ratio that applies to the part/assembly.</param>
        /// <param name="sgaOverheadRatio">The ales and administration overhead overhead ratio that applies to the part/assembly.</param>
        public PartsGridItem(
            int amount,
            bool isExternal,
            bool hasExternalSGA,
            decimal commodityOverheadRatio,
            decimal commodityMarginRatio,
            decimal sgaOverheadRatio)
        {
            this.initialAmountValue = amount;
            this.Amount = amount;
            this.initialIsExternalValue = isExternal;
            this.IsExternal = isExternal;
            this.initialHasExternalSGAValue = hasExternalSGA;
            this.HasExternalSGA = hasExternalSGA;
            this.CommodityMarginRatio = commodityMarginRatio;
            this.CommodityOverheadRatio = commodityOverheadRatio;
            this.SGAOverheadRatio = sgaOverheadRatio;
            this.UndoManager = undoManager;
        }

        /// <summary>
        /// Gets or sets the UndoManager.
        /// </summary>
        public UndoManager UndoManager
        {
            get
            {
                return this.undoManager;
            }

            set
            {
                if (this.undoManager != value)
                {
                    this.undoManager = value;
                    this.UndoManager.RegisterPropertyAction(() => this.Amount, this.HandleUndoOnAmount, this);
                    this.UndoManager.TrackObject(this);
                }
            }
        }

        /// <summary>
        /// Gets or sets the icon displayed in the parts list for this item.
        /// </summary>
        public ImageSource Icon { get; set; }

        /// <summary>
        /// Gets or sets the name displayed in the parts list for this item.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the description displayed in the parts list for this item.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the part number displayed in the parts list for this item.
        /// </summary>
        public string PartNumber { get; set; }

        /// <summary>
        /// Gets the commodity overhead ratio used to calculate the External Overhead.
        /// </summary>        
        public decimal CommodityOverheadRatio { get; private set; }

        /// <summary>
        /// Gets the commodity margin ratio used to calculate the External Margin.
        /// </summary>        
        public decimal CommodityMarginRatio { get; private set; }

        /// <summary>
        /// Gets the Sales &amp; Administration overhead ratio used to calculate the SGA on external parts.
        /// </summary>
        public decimal SGAOverheadRatio { get; private set; }

        /// <summary>
        /// Gets or sets the calculation variant of the part/assembly.
        /// </summary>
        public string CalculationVariant { get; protected set; }

        /// <summary>
        /// Gets or sets the price displayed in the parts list for this item.
        /// </summary>
        public decimal? Price
        {
            get
            {
                return this.price;
            }

            protected set
            {
                if (this.price != value)
                {
                    this.price = value;

                    if (!this.IsRaisePropertyChangedSuspended)
                    {
                        this.OnPropertyChanged(() => this.Price);
                    }

                    this.ComputeCost();
                }
            }
        }

        /// <summary>
        /// Gets the cost displayed in the parts list for this item.
        /// </summary>      
        public decimal? Cost
        {
            get { return this.cost; }
            private set { this.SetProperty(ref this.cost, value, () => this.Cost); }
        }

        /// <summary>
        /// Gets or sets the amount displayed in the parts list for this item.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public int? Amount
        {
            get
            {
                return this.amount;
            }

            set
            {
                if (this.amount != value)
                {
                    if (this.Amount != null)
                    {
                        this.OnPropertyChanging(ReflectionUtils.GetPropertyName(() => this.Amount));
                        if (value == null)
                        {
                            this.AmountBeforeEdit = this.Amount.Value;
                        }
                    }

                    this.amount = value;

                    if (!this.IsRaisePropertyChangedSuspended)
                    {
                        this.OnPropertyChanged(() => this.Amount);
                    }

                    this.ComputeCost();
                }
            }
        }

        /// <summary>
        /// Gets or sets the amount sum for the rest of the part grid items that have the same entity behind.
        /// </summary>
        public int AmountSum
        {
            get
            {
                return this.amountSum;
            }

            set
            {
                if (this.amountSum != value)
                {
                    this.amountSum = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the amount before editing it.
        /// </summary>
        public int AmountBeforeEdit { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is external.
        /// For set operation we have to recalculate the cost, because the cost
        /// depends on ExternalOverhead and ExternalMargin properties and those properties 
        /// depends on isExternal property.
        /// </summary>     
        [UndoableProperty]
        public bool IsExternal
        {
            get
            {
                return this.isExternal;
            }

            set
            {
                if (this.isExternal != value)
                {
                    this.OnPropertyChanging(ReflectionUtils.GetPropertyName(() => this.IsExternal));
                    this.isExternal = value;

                    if (!this.IsRaisePropertyChangedSuspended)
                    {
                        this.OnPropertyChanged(() => this.IsExternal);
                        this.OnPropertyChanged(() => this.ExternalOverhead);
                        this.OnPropertyChanged(() => this.ExternalMargin);
                    }

                    // If the part in no longer external reset the has external SGA flag because it can be active only for external parts.
                    if (!this.isExternal)
                    {
                        this.HasExternalSGA = false;
                    }

                    this.ComputeCost();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the SG&amp;A overhead is applied to the part when it is external.
        /// </summary>
        [UndoableProperty]
        public bool HasExternalSGA
        {
            get
            {
                return this.hasExternalSGA;
            }

            set
            {
                if (this.hasExternalSGA != value)
                {
                    this.OnPropertyChanging(ReflectionUtils.GetPropertyName(() => this.HasExternalSGA));
                    this.hasExternalSGA = value;

                    if (!this.IsRaisePropertyChangedSuspended)
                    {
                        this.OnPropertyChanged(() => this.HasExternalSGA);
                        this.OnPropertyChanged(() => this.ExternalSGAOverhead);
                    }

                    this.ComputeCost();
                }
            }
        }

        /// <summary>
        /// Gets  the overhead applied to external parts/assemblies.
        /// </summary>
        public decimal ExternalOverhead
        {
            get
            {
                if (this.IsExternal)
                {
                    decimal? overhead = this.price * this.CommodityOverheadRatio;
                    if (overhead.HasValue)
                    {
                        return overhead.Value;
                    }
                }

                return 0;
            }
        }

        /// <summary>
        /// Gets the margin applied to external parts/assemblies.
        /// </summary>
        public decimal ExternalMargin
        {
            get
            {
                if (this.IsExternal)
                {
                    decimal? margin = this.price * this.CommodityMarginRatio;
                    if (margin.HasValue)
                    {
                        return margin.Value;
                    }
                }

                return 0;
            }
        }

        /// <summary>
        /// Gets the SG&amp;A overhead applied to external parts/assemblies.
        /// </summary>
        public decimal ExternalSGAOverhead
        {
            get
            {
                if (this.HasExternalSGA && !CostCalculatorFactory.IsOlder(this.CalculationVariant, "1.4"))
                {
                    return (this.Price.GetValueOrDefault(0) + this.ExternalOverhead + this.ExternalMargin) * this.SGAOverheadRatio;
                }
                else
                {
                    return 0m;
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether the editable data in this instance has changed (an should probably be saved).
        /// </summary>
        public virtual bool IsDirty
        {
            // Is dirty when Amount, IsExternal or HasExternalSGA are changed
            get
            {
                return this.amount != this.initialAmountValue
                    || this.isExternal != this.initialIsExternalValue
                    || this.hasExternalSGA != this.initialHasExternalSGAValue;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the property changed event raise is suspended or not.
        /// </summary>
        public bool IsRaisePropertyChangedSuspended { get; set; }

        /// <summary>
        /// Update initial values.
        /// </summary>
        public void RefreshInitialValues()
        {
            this.initialAmountValue = this.Amount.Value;
            this.initialIsExternalValue = this.IsExternal;
            this.initialHasExternalSGAValue = this.HasExternalSGA;
        }

        /// <summary>
        /// Handles the undo operation on the Amount property.
        /// </summary>
        /// <param name="restoredValue">The amount value restored by undo.</param>
        public abstract void HandleAmountUndo(int restoredValue);

        /// <summary>
        /// Handle the Undo performed on Amount property.
        /// </summary>
        /// <param name="undoItem">The undoable item.</param>
        private void HandleUndoOnAmount(UndoableItem undoItem)
        {
            if (undoItem.ActionName == ReflectionUtils.GetPropertyName(() => this.Amount)
                && undoItem.Values != null && undoItem.Values.Count == 1)
            {
                this.AmountBeforeEdit = this.Amount.Value;
                var amount = (int)undoItem.Values.ElementAt(0);

                var gridItem = undoItem.Instance as PartsGridItem;
                if (gridItem != null)
                {
                    gridItem.HandleAmountUndo(amount);
                }
            }
        }

        /// <summary>
        /// Computes the cost displayed in the parts list for this item.
        /// </summary>
        protected void ComputeCost()
        {
            this.Cost = this.amount * (this.price + this.ExternalOverhead + this.ExternalMargin + this.ExternalSGAOverhead);
        }

        /// <summary>
        /// Begins an edit on an object.
        /// </summary>
        public void BeginEdit()
        {
            oldAmount = this.Amount.Value;
        }

        /// <summary>
        /// Discards changes since the last <see cref="M:System.ComponentModel.IEditableObject.BeginEdit"/> call.
        /// </summary>
        public void CancelEdit()
        {
        }

        /// <summary>
        /// Pushes changes since the last <see cref="M:System.ComponentModel.IEditableObject.BeginEdit"/> or <see cref="M:System.ComponentModel.IBindingList.AddNew"/> call into the underlying object.
        /// </summary>
        public void EndEdit()
        {
            if (this.Amount == null)
            {
                this.Amount = oldAmount;
            }
        }
    }
}
