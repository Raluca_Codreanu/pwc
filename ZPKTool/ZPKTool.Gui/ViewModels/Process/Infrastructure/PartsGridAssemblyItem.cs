﻿using System;
using System.ComponentModel;
using System.Linq;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.Gui.Resources;

namespace ZPKTool.Gui.ViewModels.ProcessInfrastructure
{
    /// <summary>
    /// This class represents an Assembly item in the Parts List of the Process screen.
    /// </summary>
    public class PartsGridAssemblyItem : PartsGridItem
    {
        #region Fields

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PartsGridAssemblyItem" /> class.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <param name="price">The price (cost) of the assembly.</param>
        /// <param name="assemblyAmount">The amount object containing the information about the assembly's amount used in the process step.</param>
        /// <param name="processStep">The process step.</param>
        /// <param name="parentOHSettings">The overhead settings of the parent assembly.</param>
        public PartsGridAssemblyItem(
            Assembly assembly,
            decimal? price,
            ProcessStepAssemblyAmount assemblyAmount,
            ProcessStep processStep,
            OverheadSetting parentOHSettings)
            : base(
            assemblyAmount != null ? assemblyAmount.Amount : 0,
            assembly.IsExternal.GetValueOrDefault(),
            assembly.ExternalSGA,
            parentOHSettings.CommodityOverhead,
            parentOHSettings.CommodityMargin,
            parentOHSettings.SalesAndAdministrationOHValue)
        {
            this.Assembly = assembly;
            this.Price = price;
            this.AssemblyAmount = assemblyAmount;
            this.Step = processStep;

            // The amount must be initialized if it does not exist because otherwise it will remain out of sync with the process step model until its amount is edited.
            // Calling UpdateAmount() will create the AssemblyAmount instance.
            if (this.AssemblyAmount == null)
            {
                this.UpdateAmount();
            }

            this.Name = assembly.Name;
            this.PartNumber = assembly.Number;
            this.Description = assembly.Description;
            this.CalculationVariant = assembly.CalculationVariant;

            this.Icon = Images.AssemblyIcon;
            this.PropertyChanged += new PropertyChangedEventHandler(this.PartsGridAssemblyItem_PropertyChanged);

            this.ComputeCost();
        }

        #endregion Constructor

        #region Properties

        /// <summary>
        /// Gets the assembly for which the amount is set through this class.
        /// </summary>
        public Assembly Assembly { get; private set; }

        /// <summary>
        /// Gets the process step to which the amount of assembly is set.
        /// </summary>
        public ProcessStep Step { get; private set; }

        /// <summary>
        /// Gets the object that represents the amount of an assembly associated with a process step.
        /// </summary>
        public ProcessStepAssemblyAmount AssemblyAmount { get; private set; }

        #endregion Properties

        #region Event handlers

        /// <summary>
        /// Handles the PropertyChanged event for this.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void PartsGridAssemblyItem_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == ReflectionUtils.GetPropertyName(() => this.IsExternal))
            {
                if (this.Assembly.IsExternal != this.IsExternal)
                {
                    this.Assembly.IsExternal = this.IsExternal;
                }
            }
            else if (e.PropertyName == ReflectionUtils.GetPropertyName(() => this.HasExternalSGA))
            {
                if (this.Assembly.ExternalSGA != this.HasExternalSGA)
                {
                    this.Assembly.ExternalSGA = this.HasExternalSGA;
                }
            }
            else if (e.PropertyName == ReflectionUtils.GetPropertyName(() => this.Amount))
            {
                this.UpdateAmount();
            }
        }

        #endregion Event handlers

        #region Other methods

        /// <summary>
        /// Handles the undo operation on the Amount property.
        /// </summary>
        /// <param name="restoredValue">The amount value restored by undo.</param>
        public override void HandleAmountUndo(int restoredValue)
        {
            this.AssemblyAmount.Amount = restoredValue;
        }

        /// <summary>
        /// Update Assembly Amount.
        /// </summary>
        public void UpdateAmount()
        {
            if (this.Assembly == null || this.Step == null || this.Amount == null)
            {
                return;
            }

            if (this.AssemblyAmount == null)
            {
                this.AssemblyAmount = new ProcessStepAssemblyAmount();
                this.AssemblyAmount.Assembly = this.Assembly;
                this.Assembly.ProcessStepAssemblyAmounts.Add(this.AssemblyAmount);
                this.AssemblyAmount.ProcessStep = this.Step;
                this.Step.AssemblyAmounts.Add(this.AssemblyAmount);
            }

            if (this.AssemblyAmount.Amount != this.Amount)
            {
                this.AmountBeforeEdit = this.AssemblyAmount.Amount;
                this.AssemblyAmount.Amount = this.Amount.Value;
            }
        }

        #endregion Other methods
    }
}