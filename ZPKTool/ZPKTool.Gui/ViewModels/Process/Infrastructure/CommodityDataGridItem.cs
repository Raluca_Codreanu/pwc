﻿namespace ZPKTool.Gui.ViewModels.ProcessInfrastructure
{
    using ZPKTool.Common;
    using ZPKTool.Data;

    /// <summary>
    /// DataGrid commodity item.
    /// </summary>
    public class CommodityDataGridItem : ObservableObject
    {
        /// <summary>
        /// The cost of the commodity.
        /// </summary>
        private decimal cost;

        /// <summary>
        /// Gets or sets the commodity.
        /// </summary>
        public Commodity Commodity { get; set; }

        /// <summary>
        /// Gets or sets the cost of the commodity.
        /// </summary>
        public decimal Cost
        {
            get { return this.cost; }
            set { this.SetProperty(ref this.cost, value, () => this.Cost); }
        }
    }
}
