﻿using System;
using System.ComponentModel;
using System.Linq;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.Gui.Resources;

namespace ZPKTool.Gui.ViewModels.ProcessInfrastructure
{
    /// <summary>
    /// This class represents a Part item in the Parts List of the Process screen.
    /// </summary>
    public class PartsGridPartItem : PartsGridItem
    {
        #region Fields

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PartsGridPartItem" /> class.
        /// </summary>
        /// <param name="part">The part.</param>
        /// <param name="price">The price (cost) of the part.</param>        
        /// <param name="partAmount">The part amount object containing the information about the part's amount in the process step.</param>
        /// <param name="processStep">The process step.</param>
        /// <param name="parentOHSettings">The overhead settings of the parent assembly.</param>
        public PartsGridPartItem(
            Part part,
            decimal? price,
            ProcessStepPartAmount partAmount,
            ProcessStep processStep,
            OverheadSetting parentOHSettings)
            : base(
            partAmount != null ? partAmount.Amount : 0,
            part.IsExternal.GetValueOrDefault(),
            part.ExternalSGA,
            parentOHSettings.CommodityOverhead,
            parentOHSettings.CommodityMargin,
            parentOHSettings.SalesAndAdministrationOHValue)
        {
            this.Part = part;
            this.Price = price;
            this.PartAmount = partAmount;
            this.Step = processStep;

            // The part amount must be initialized if it does not exist because otherwise it will remain out of sync with the step model until its amount is edited.
            // Calling UpdateAmount() will create the PartAmount instance.
            if (this.PartAmount == null)
            {
                this.UpdateAmount();
            }

            this.Name = part.Name;
            this.Description = part.Description;
            this.PartNumber = part.Number;
            this.CalculationVariant = part.CalculationVariant;

            this.Icon = Images.PartIcon;
            this.PropertyChanged += new PropertyChangedEventHandler(PartsGridPartItem_PropertyChanged);

            this.ComputeCost();
        }

        #endregion Constructor

        #region Properties

        /// <summary>
        /// Gets the part for which the amount is set through this class.
        /// </summary>
        public Part Part { get; private set; }

        /// <summary>
        /// Gets the process step to which the amount of Part is set.
        /// </summary>
        public ProcessStep Step { get; private set; }

        /// <summary>
        /// Gets the object that represents the amount of a part associated with 
        /// a process step.
        /// </summary>
        public ProcessStepPartAmount PartAmount { get; private set; }

        #endregion Properties

        #region Event handlers

        /// <summary>
        /// Handles the PropertyChanged event for this.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void PartsGridPartItem_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == ReflectionUtils.GetPropertyName(() => this.IsExternal))
            {
                if (this.Part.IsExternal != this.IsExternal)
                {
                    this.Part.IsExternal = this.IsExternal;
                }
            }
            else if (e.PropertyName == ReflectionUtils.GetPropertyName(() => this.HasExternalSGA))
            {
                if (this.Part.ExternalSGA != this.HasExternalSGA)
                {
                    this.Part.ExternalSGA = this.HasExternalSGA;
                }
            }
            else if (e.PropertyName == ReflectionUtils.GetPropertyName(() => this.Amount))
            {
                this.UpdateAmount();
            }
        }

        #endregion Event handlers

        #region Other methods

        /// <summary>
        /// Handles the undo operation on the Amount property.
        /// </summary>
        /// <param name="restoredValue">The amount value restored by undo.</param>
        public override void HandleAmountUndo(int restoredValue)
        {
            this.PartAmount.Amount = restoredValue;
        }

        /// <summary>
        /// Update part Amount.
        /// </summary>
        public void UpdateAmount()
        {
            if (this.Part == null || this.Step == null || this.Amount == null)
            {
                return;
            }

            if (this.PartAmount == null)
            {
                this.PartAmount = new ProcessStepPartAmount();
                this.PartAmount.Part = this.Part;
                this.Part.ProcessStepPartAmounts.Add(this.PartAmount);
                this.PartAmount.ProcessStep = this.Step;
                this.Step.PartAmounts.Add(this.PartAmount);
            }

            if (this.PartAmount.Amount != this.Amount)
            {
                this.AmountBeforeEdit = this.PartAmount.Amount;
                this.PartAmount.Amount = this.Amount.Value;
            }
        }

        #endregion Other methods
    }
}