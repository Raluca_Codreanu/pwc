﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.ViewModels.ProcessInfrastructure
{
    /// <summary>
    /// The existing Process Step Tabs.
    /// </summary>
    public enum SelectedProcessStepTab
    {
        /// <summary>
        /// The Process Step Information Tab.
        /// </summary>
        InformationTab = 0,

        /// <summary>
        ///  The Process Step Labour Settings Tab.
        /// </summary>
        LabourSettingsTab = 1,

        /// <summary>
        ///  The Process Step Additional Settings Tab.
        /// </summary>
        AdditionalSettingsTab = 2,

        /// <summary>
        ///  The Process Step Machines Tab.
        /// </summary>
        MachinesTab = 3,

        /// <summary>
        ///  The Process Step Dies Tab.
        /// </summary>
        DiesTab = 4,

        /// <summary>
        ///  The Process Step Consumables Tab.
        /// </summary>
        ConsumablesTab = 5,

        /// <summary>
        ///  The Process Step Commodities Tab.
        /// </summary>
        CommoditiesTab = 6,

        /// <summary>
        ///  The Process Step Parts Tab.
        /// </summary>
        PartsTab = 7
    }
}
