﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.ViewModels.ProcessInfrastructure
{
    /// <summary>
    /// This class is used for data binding in the CO2 emission chart
    /// </summary>
    public class CO2ChartItem : INotifyPropertyChanged
    {
        /// <summary>
        /// The co2 chart item id
        /// </summary>
        private Guid id;

        /// <summary>
        /// The co2 chart item index
        /// </summary>
        private int index;

        /// <summary>
        /// The co2 chart item name
        /// </summary>
        private string name;

        /// <summary>
        /// The co2 chart item co2 emissions
        /// </summary>
        private decimal emission;

        /// <summary>
        /// Initializes a new instance of the <see cref="CO2ChartItem"/> class.
        /// </summary>
        /// <param name="id">The item id</param>
        /// <param name="index">The item index</param>
        /// <param name="name">The item name</param>
        /// <param name="emissions">The item co2 emissions</param>
        public CO2ChartItem(Guid id, int index, string name, decimal emissions)
        {
            this.Id = id;
            this.Index = index;
            this.Name = name;
            this.Emission = emissions;
        }

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets the co2 chart item id
        /// </summary>
        public Guid Id
        {
            get
            {
                return this.id;
            }

            set
            {
                if (this.id != value)
                {
                    this.id = value;
                    this.RaisePropertyChangeEvent("Id");
                }
            }
        }

        /// <summary>
        /// Gets or sets the co2 chart item index
        /// </summary>
        public int Index
        {
            get
            {
                return this.index;
            }

            set
            {
                if (this.index != value)
                {
                    this.index = value;
                    this.RaisePropertyChangeEvent("Index");
                }
            }
        }

        /// <summary>
        /// Gets or sets the co2 chart item name
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                if (this.name != value)
                {
                    this.name = value;
                    this.RaisePropertyChangeEvent("Name");
                }
            }
        }

        /// <summary>
        /// Gets or sets the co2 chart item co2 emissions
        /// </summary>
        public decimal Emission
        {
            get
            {
                return this.emission;
            }

            set
            {
                if (this.emission != value)
                {
                    this.emission = value;
                    this.RaisePropertyChangeEvent("Emission");
                }
            }
        }

        /// <summary>
        /// Raises the PropertyChange event.
        /// </summary>
        /// <param name="propertyName">The name of the property which has been changed</param>
        private void RaisePropertyChangeEvent(string propertyName)
        {
            if (null != PropertyChanged)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
