﻿namespace ZPKTool.Gui.ViewModels.ProcessInfrastructure
{
    using System;
    using System.ComponentModel;
    using ZPKTool.Common;
    using ZPKTool.Data;

    /// <summary>
    /// DataGrid consumable item.
    /// </summary>
    public class ConsumableDataGridItem : ObservableObject
    {
        /// <summary>
        /// The cost of the consumable.
        /// </summary>
        private decimal cost;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConsumableDataGridItem" /> class.
        /// </summary>
        /// <param name="consumable">The consumable wrapped by this instance.</param>
        public ConsumableDataGridItem(Consumable consumable)
        {
            if (consumable == null)
            {
                throw new ArgumentNullException("consumable", "The consumable was null.");
            }

            this.Consumable = consumable;
        }

        /// <summary>
        /// Gets the consumable.
        /// </summary>
        public Consumable Consumable { get; private set; }

        /// <summary>
        /// Gets or sets the cost of the consumable.
        /// </summary>
        public decimal Cost
        {
            get { return this.cost; }
            set { this.SetProperty(ref this.cost, value, () => this.Cost); }
        }
    }
}
