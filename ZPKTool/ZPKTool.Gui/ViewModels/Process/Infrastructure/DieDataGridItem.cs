﻿namespace ZPKTool.Gui.ViewModels.ProcessInfrastructure
{
    using ZPKTool.Calculations.CostCalculation;
    using ZPKTool.Common;
    using ZPKTool.Data;

    /// <summary>
    /// DataGrid die item.
    /// </summary>
    public class DieDataGridItem : ObservableObject
    {
        /// <summary>
        /// The cost of the a die.
        /// </summary>
        private DieCost cost;

        /// <summary>
        /// Gets or sets the machine.
        /// </summary>
        public Die Die { get; set; }

        /// <summary>
        /// Gets or sets the cost of a die.
        /// </summary>            
        public DieCost Cost
        {
            get { return this.cost; }
            set { this.SetProperty(ref this.cost, value, () => this.Cost); }
        }
    }
}
