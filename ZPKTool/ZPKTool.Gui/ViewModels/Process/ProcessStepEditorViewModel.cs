﻿namespace ZPKTool.Gui.ViewModels
{
    using System;
    using System.ComponentModel.Composition;
    using System.ComponentModel.Composition.Hosting;
    using System.Linq;
    using System.Windows.Input;
    using ZPKTool.Data;
    using ZPKTool.DataAccess;
    using ZPKTool.Gui.Notifications;
    using ZPKTool.Gui.Services;
    using ZPKTool.Gui.ViewModels.ProcessInfrastructure;
    using ZPKTool.MvvmCore;
    using ZPKTool.MvvmCore.Commands;
    using ZPKTool.MvvmCore.Services;

    /// <summary>
    /// The view-model for the Process step Editor.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ProcessStepEditorViewModel : ViewModel<ProcessStep, IDataSourceManager>
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The tab selected by the user which has to remain the same when a new page is instantiated.
        /// </summary>
        private static SelectedProcessStepTab selectedTab;

        /// <summary>
        /// The composition container reference which is used to get other view-models.
        /// </summary>
        private CompositionContainer compositionContainer;

        /// <summary>
        /// The messenger.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The units service.
        /// </summary>
        private IUnitsService unitsService;

        #endregion Attributes

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessStepEditorViewModel"/> class.
        /// </summary>
        /// <param name="costRecalculationCloneManager">The cost recalculation clone manager.</param>
        /// <param name="container">The composition container.</param>
        /// <param name="messenger">The messenger.</param>
        /// <param name="unitsService">The units service.</param>
        [ImportingConstructor]
        public ProcessStepEditorViewModel(
            ICostRecalculationCloneManager costRecalculationCloneManager,
            CompositionContainer container,
            IMessenger messenger,
            IUnitsService unitsService)
        {
            Argument.IsNotNull("container", container);
            Argument.IsNotNull("messenger", messenger);
            Argument.IsNotNull("unitsService", unitsService);
            Argument.IsNotNull("costRecalculationCloneManager", costRecalculationCloneManager);

            this.compositionContainer = container;
            this.messenger = messenger;
            this.unitsService = unitsService;
            this.CloneManager = costRecalculationCloneManager;

            this.PreviousStepCommand = new DelegateCommand(this.NavigateToPreviousStep, this.CanNavigateToPreviousStep);
            this.NextStepCommand = new DelegateCommand(this.NavigateToNextStep, this.CanNavigateToNextStep);

            this.messenger.Register<CurrentComponentCostChangedMessage>(this.HandleEditedStepCostChange, GlobalMessengerTokens.MainViewTargetToken);
        }

        #endregion Constructor

        #region Commands

        /// <summary>
        /// Gets the command for the previous step selection.
        /// </summary>
        public ICommand PreviousStepCommand { get; private set; }

        /// <summary>
        /// Gets the command for the next step selection.
        /// </summary>
        public ICommand NextStepCommand { get; private set; }

        #endregion Commands

        #region Properties

        /// <summary>
        /// Gets the current edited process step view-model.
        /// </summary>
        public VMProperty<ProcessStepViewModel> EditedStep { get; private set; }

        /// <summary>
        /// Gets the current process step cost.
        /// </summary>
        public VMProperty<decimal?> EditedStepCost { get; private set; }

        /// <summary>
        /// Gets or sets the project to which the Model belongs.
        /// This property is needed during cost calculations so it must be set when editing (it can be omitted when creating).
        /// </summary>
        public Project ParentProject { get; set; }

        /// <summary>
        /// Gets or sets the Process Parent. (assembly or part)
        /// </summary>
        public object ProcessParent { get; set; }

        /// <summary>
        /// Gets or sets the process step Process.
        /// </summary>
        public Process Process { get; set; }

        /// <summary>
        /// Gets or sets the child to select after the screen is loaded. Set it to null to be ignored.
        /// </summary>
        public object ChildToSelect { get; set; }

        /// <summary>
        /// Gets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter { get; private set; }

        #endregion Properties

        #region Commands handling

        /// <summary>
        /// Determines whether this instance can execute the PreviousStepCommand.
        /// </summary>
        /// <returns>
        /// true if the command can be executed; otherwise returns false.
        /// </returns>
        private bool CanNavigateToPreviousStep()
        {
            var stepModel = this.EditedStep.Value != null ? this.EditedStep.Value.Model : null;
            if (this.Process == null || stepModel == null
                || (this.Process.Steps.Count > 0 && stepModel.Index == this.Process.Steps.OrderBy(s => s.Index).First().Index))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Executes the logic associated with the PreviousStepCommand.
        /// </summary>
        private void NavigateToPreviousStep()
        {
            var selectPrevStepMsg = new NotificationMessage(Notification.SelectPreviousProcessStep);
            this.messenger.Send(selectPrevStepMsg);
        }

        /// <summary>
        /// Determines whether this instance can execute the NextStepCommand.
        /// </summary>
        /// <returns>
        /// true if the command can be executed; otherwise returns false.
        /// </returns>
        private bool CanNavigateToNextStep()
        {
            var stepModel = this.EditedStep.Value != null ? this.EditedStep.Value.Model : null;
            if (this.Process == null || stepModel == null
                || (this.Process.Steps.Count > 0 && stepModel.Index == this.Process.Steps.OrderByDescending(s => s.Index).First().Index))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Executes the logic associated with the NextStepCommand.
        /// </summary>
        private void NavigateToNextStep()
        {
            var selectNextStepMsg = new NotificationMessage(Notification.SelectNextProcessStep);
            this.messenger.Send(selectNextStepMsg);
        }

        #endregion Commands handling

        #region Load Step

        /// <summary>
        /// Loads the data from the model.
        /// Each view-model property decorated with the ExposedModelProperty attribute is loaded from the associated Model property.
        /// </summary>
        /// <param name="model">The model instance.</param>
        public override void LoadDataFromModel(ProcessStep model)
        {
            this.CloneManager.Clone(this);
            this.LoadProcessStepData();
            this.SelectChild(this.ChildToSelect);
        }

        /// <summary>
        /// Load a new process step view-model.
        /// </summary>
        private void LoadProcessStepData()
        {
            var newProcessStepVM = this.compositionContainer.GetExportedValue<ProcessStepViewModel>();
            this.EditedStep.Value = newProcessStepVM;
            newProcessStepVM.DataSourceManager = this.DataSourceManager;
            newProcessStepVM.IsInViewerMode = this.IsInViewerMode;
            newProcessStepVM.IsReadOnly = this.IsReadOnly;
            newProcessStepVM.ParentProject = this.ParentProject;
            newProcessStepVM.ProcessParent = this.ProcessParent;
            newProcessStepVM.ModelClone = this.ModelClone;
            newProcessStepVM.ModelParentClone = this.ModelParentClone;
            newProcessStepVM.Model = this.Model;

            this.SetProcessStepSelectedTab(newProcessStepVM, this.Model, selectedTab);
        }

        #endregion Load Step

        /// <summary>
        /// Called when the Model has changed.
        /// </summary>
        protected override void OnModelChanged()
        {
            base.OnModelChanged();

            if (this.IsInViewerMode)
            {
                this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(null);
            }
        }

        /// <summary>
        /// Called when the DataSourceManager changed.
        /// </summary>
        protected override void OnDataSourceManagerChanged()
        {
            base.OnDataSourceManagerChanged();

            this.MeasurementUnitsAdapter = this.unitsService.GetUnitsAdapter(this.DataSourceManager);
        }

        /// <summary>
        /// Selects the specified child object.
        /// </summary>
        /// <param name="childToSelect">The child to select.</param>
        public void SelectChild(object childToSelect)
        {
            if (childToSelect == null)
            {
                return;
            }

            Type childType = childToSelect.GetType();
            IIdentifiable identifiableEntity = childToSelect as IIdentifiable;

            if (identifiableEntity == null)
            {
                log.Warn("Can't select a child of a process step that is not IIdentifiable. Child type: {0}.", childType);
                return;
            }

            Guid childId = identifiableEntity.Guid;
            if (childType == typeof(Consumable))
            {
                ConsumableDataGridItem consumable = this.EditedStep.Value.StepConsumables.FirstOrDefault(c => c.Consumable.Guid == childId);
                if (consumable != null && this.EditedStep.Value.Accuracy.Value == ProcessCalculationAccuracy.Calculated)
                {
                    // Navigate to the machine only if the Process calculation accuracy is calculated.
                    this.EditedStep.Value.SelectedTabIndex.Value = (int)SelectedProcessStepTab.ConsumablesTab;
                    this.EditedStep.Value.SelectedConsumable.Value = consumable;
                }
            }
            else if (childType == typeof(Die))
            {
                DieDataGridItem die = this.EditedStep.Value.StepDieItems.FirstOrDefault(c => c.Die.Guid == childId);
                if (die != null && this.EditedStep.Value.Accuracy.Value == ProcessCalculationAccuracy.Calculated)
                {
                    // Navigate to the machine only if the Process calculation accuracy is calculated.
                    this.EditedStep.Value.SelectedTabIndex.Value = (int)SelectedProcessStepTab.DiesTab;
                    this.EditedStep.Value.SelectedDieItem.Value = die;
                }
            }
            else if (childType == typeof(Commodity))
            {
                CommodityDataGridItem commodity = this.EditedStep.Value.StepCommodities.FirstOrDefault(c => c.Commodity.Guid == childId);
                if (commodity != null && this.EditedStep.Value.Accuracy.Value == ProcessCalculationAccuracy.Calculated)
                {
                    // Navigate to the machine only if the Process calculation accuracy is calculated.
                    this.EditedStep.Value.SelectedTabIndex.Value = (int)SelectedProcessStepTab.CommoditiesTab;
                    this.EditedStep.Value.SelectedCommodity.Value = commodity;
                }
            }
            else if (childType == typeof(Machine))
            {
                MachineDataGridItem machine = this.EditedStep.Value.StepMachines.FirstOrDefault(c => c.Machine.Guid == childId);
                if (machine != null && this.EditedStep.Value.Accuracy.Value == ProcessCalculationAccuracy.Calculated)
                {
                    // Navigate to the machine only if the Process calculation accuracy is calculated.
                    this.EditedStep.Value.SelectedTabIndex.Value = (int)SelectedProcessStepTab.MachinesTab;
                    this.EditedStep.Value.SelectedMachineItem.Value = machine;
                }
            }
        }

        /// <summary>
        /// Called before unloading the view from its parent or closing it. Returning false will cancel the view's unloading or closing.
        /// </summary>
        /// <returns>
        /// True if the unloading process should continue and false if it should be canceled.
        /// </returns>
        public override bool OnUnloading()
        {
            if (this.EditedStep.Value == null)
            {
                log.Error("The ProcessStepEditor current StepViewModel is null.");
                return true;
            }

            selectedTab = (SelectedProcessStepTab)this.EditedStep.Value.SelectedTabIndex.Value;
            return this.EditedStep.Value.OnUnloading();
        }

        /// <summary>
        /// Handle the edited step cost change, by updating the displayed step cost.
        /// </summary>
        /// <param name="msg">The message notifying that the cost has changed.</param>
        private void HandleEditedStepCostChange(CurrentComponentCostChangedMessage msg)
        {
            var result = msg.Content;
            if (result != null && this.EditedStep.Value != null && this.EditedStep.Value.Model != null)
            {
                var cost = result.ProcessCost.StepCosts.FirstOrDefault(c => c.StepId == this.EditedStep.Value.Model.Guid);
                if (cost != null)
                {
                    this.EditedStepCost.Value = cost.TotalManufacturingCost;
                }
            }
        }

        /// <summary>
        /// Set the Selected tab for the ProcessStep view-model.
        /// </summary>
        /// <param name="processStepVM">The ProcessStep view-model.</param>
        /// <param name="step">The process step.</param>
        /// <param name="previousProcessStepVMSelectedTab">The previous ProcessStep view-model selected tab.</param>
        private void SetProcessStepSelectedTab(ProcessStepViewModel processStepVM, ProcessStep step, SelectedProcessStepTab previousProcessStepVMSelectedTab)
        {
            if (step is PartProcessStep)
            {
                switch (previousProcessStepVMSelectedTab)
                {
                    case SelectedProcessStepTab.CommoditiesTab:
                    case SelectedProcessStepTab.PartsTab:
                        {
                            previousProcessStepVMSelectedTab = SelectedProcessStepTab.InformationTab;
                            break;
                        }

                    default:
                        break;
                }
            }

            if (processStepVM.Accuracy.Value == ProcessCalculationAccuracy.Estimated
                && previousProcessStepVMSelectedTab != SelectedProcessStepTab.PartsTab)
            {
                previousProcessStepVMSelectedTab = SelectedProcessStepTab.InformationTab;
            }

            processStepVM.SelectedTabIndex.Value = (int)previousProcessStepVMSelectedTab;
        }
    }
}
