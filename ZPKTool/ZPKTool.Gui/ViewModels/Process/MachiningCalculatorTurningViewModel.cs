﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using ZPKTool.Business.MachiningCalculator;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model of the MachiningCalculatorTurning view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class MachiningCalculatorTurningViewModel : ViewModel<CycleTimeCalculationItemViewModel>
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The machining calculator.
        /// </summary>
        private MachiningCalculator machiningCalculator;

        /// <summary>
        /// The selected turning machining type.
        /// </summary>
        private TurningMachiningType selectedMachiningType;

        /// <summary>
        /// The selected turning material.
        /// </summary>
        private TurningMaterialToBeMachined selectedMaterial;

        /// <summary>
        /// The process description.
        /// </summary>
        private string processDescription;

        /// <summary>
        /// The selected turning type.
        /// </summary>
        private TurningType selectedTurningType;

        /// <summary>
        /// The machine feed speed.
        /// </summary>
        private decimal? feedSpeed;

        /// <summary>
        /// The selected turning situation.
        /// </summary>
        private TurningSituation selectedTurningSituation;

        /// <summary>
        /// The cut depth.
        /// </summary>
        private decimal? cutDepth;

        /// <summary>
        /// The start stop time for machine feed.
        /// </summary>
        private decimal? startStopTime;

        /// <summary>
        /// The machine feed length.
        /// </summary>
        private decimal? feedLength;

        /// <summary>
        /// The turning diameter.
        /// </summary>
        private decimal? turningDiameter;

        /// <summary>
        /// The turning length.
        /// </summary>
        private decimal? turningLength;

        /// <summary>
        /// The turning depth.
        /// </summary>
        private decimal? turningDepth;

        /// <summary>
        /// The tool change time.
        /// </summary>
        private decimal? toolChangeTime;

        /// <summary>
        /// The calculated turning speed displayed in tooltip.
        /// </summary>
        private string calculatedTurningSpeed;

        /// <summary>
        /// The turning speed.
        /// </summary>
        private decimal? turningSpeed;

        /// <summary>
        /// The calculated feed rate displayed in tooltip.
        /// </summary>
        private string calculatedFeedRate;

        /// <summary>
        /// The feed rate.
        /// </summary>
        private decimal? feedRate;

        /// <summary>
        /// The resulting turning time.
        /// </summary>
        private decimal? resultingTurningTime;

        /// <summary>
        /// The resulting gross process time.
        /// </summary>
        private decimal? resultingCycleTime;

        /// <summary>
        /// A value indicating whether the machining calculator is in expert mode or not.
        /// </summary>
        private bool isExpertMode;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="MachiningCalculatorTurningViewModel" /> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        /// <param name="messenger">The messenger.</param>
        [ImportingConstructor]
        public MachiningCalculatorTurningViewModel(IWindowService windowService, IMessenger messenger)
        {
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("messenger", messenger);

            this.windowService = windowService;
            this.messenger = messenger;

            InitializeProperties();
        }

        #region Properties

        /// <summary>
        /// Gets the list of turning materials.
        /// </summary>
        public List<TurningMaterialToBeMachined> TurningMaterials { get; private set; }

        /// <summary>
        /// Gets or sets the selected machining type.
        /// </summary>
        [UndoableProperty]
        public TurningMachiningType SelectedMachiningType
        {
            get { return this.selectedMachiningType; }
            set { this.SetProperty(ref this.selectedMachiningType, value, () => this.SelectedMachiningType, this.UpdateTurningCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the selected turning material.
        /// </summary>
        [UndoableProperty]
        public TurningMaterialToBeMachined SelectedMaterial
        {
            get { return this.selectedMaterial; }
            set { this.SetProperty(ref this.selectedMaterial, value, () => this.SelectedMaterial, this.UpdateTurningCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the process description.
        /// </summary>
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public string ProcessDescription
        {
            get { return this.processDescription; }
            set { this.SetProperty(ref this.processDescription, value, () => this.ProcessDescription); }
        }

        /// <summary>
        /// Gets or sets the selected turning type.
        /// </summary>
        [UndoableProperty]
        public TurningType SelectedTurningType
        {
            get { return this.selectedTurningType; }
            set { this.SetProperty(ref this.selectedTurningType, value, () => this.SelectedTurningType, this.UpdateTurningCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the feed speed.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_FeedSpeed", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? FeedSpeed
        {
            get { return this.feedSpeed; }
            set { this.SetProperty(ref this.feedSpeed, value, () => this.FeedSpeed, this.UpdateTurningCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the selected turning situation.
        /// </summary>
        [UndoableProperty]
        public TurningSituation SelectedTurningSituation
        {
            get { return this.selectedTurningSituation; }
            set { this.SetProperty(ref this.selectedTurningSituation, value, () => this.SelectedTurningSituation, this.UpdateTurningCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the cut depth.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_CutDepth", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? CutDepth
        {
            get { return this.cutDepth; }
            set { this.SetProperty(ref this.cutDepth, value, () => this.CutDepth, this.UpdateTurningCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the start stop time for machine feed.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_StartStopTime", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? StartStopTime
        {
            get { return this.startStopTime; }
            set { this.SetProperty(ref this.startStopTime, value, () => this.StartStopTime, this.UpdateTurningCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the machine feed length.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_FeedLength", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? FeedLength
        {
            get { return this.feedLength; }
            set { this.SetProperty(ref this.feedLength, value, () => this.FeedLength, this.UpdateTurningCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the turning diameter.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_Diameter", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? TurningDiameter
        {
            get { return this.turningDiameter; }
            set { this.SetProperty(ref this.turningDiameter, value, () => this.TurningDiameter, this.UpdateTurningCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the turning length.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_TurningLength", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? TurningLength
        {
            get { return this.turningLength; }
            set { this.SetProperty(ref this.turningLength, value, () => this.TurningLength, this.UpdateTurningCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the turning depth.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_TurningDepth", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? TurningDepth
        {
            get { return this.turningDepth; }
            set { this.SetProperty(ref this.turningDepth, value, () => this.TurningDepth, this.UpdateTurningCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the tool change time.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_ToolChangeTime", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? ToolChangeTime
        {
            get { return this.toolChangeTime; }
            set { this.SetProperty(ref this.toolChangeTime, value, () => this.ToolChangeTime, this.UpdateTurningCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the calculated turning speed displayed in tooltip.
        /// </summary>
        public string CalculatedTurningSpeed
        {
            get { return this.calculatedTurningSpeed; }
            set { this.SetProperty(ref this.calculatedTurningSpeed, value, () => this.CalculatedTurningSpeed); }
        }

        /// <summary>
        /// Gets or sets the turning speed.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_TurningSpeed", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? TurningSpeed
        {
            get
            {
                return this.turningSpeed;
            }

            set
            {
                if (value != this.turningSpeed)
                {
                    var oldValue = this.turningSpeed;
                    this.turningSpeed = value.HasValue ? Math.Round(value.Value) : (decimal?)null;
                    this.OnPropertyChanged(() => this.TurningSpeed);

                    if (oldValue != this.turningSpeed)
                    {
                        this.UpdateTurningCalculationResult();
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the calculated feed rate displayed in tooltip.
        /// </summary>
        public string CalculatedFeedRate
        {
            get { return this.calculatedFeedRate; }
            set { this.SetProperty(ref this.calculatedFeedRate, value, () => this.CalculatedFeedRate); }
        }

        /// <summary>
        /// Gets or sets the feed rate.
        /// </summary>
        [Required(ErrorMessageResourceName = "RequiredField_FeedRate", ErrorMessageResourceType = typeof(LocalizedResources))]
        [UndoableProperty(GroupConsecutiveChanges = true)]
        public decimal? FeedRate
        {
            get { return this.feedRate; }
            set { this.SetProperty(ref this.feedRate, value, () => this.FeedRate); }
        }

        /// <summary>
        /// Gets or sets the resulting turning time.
        /// </summary>
        public decimal? ResultingTurningTime
        {
            get { return this.resultingTurningTime; }
            set { this.SetProperty(ref this.resultingTurningTime, value, () => this.ResultingTurningTime); }
        }

        /// <summary>
        /// Gets or sets the resulting cycle time.
        /// </summary>
        public decimal? ResultingCycleTime
        {
            get { return this.resultingCycleTime; }
            set { this.SetProperty(ref this.resultingCycleTime, value, () => this.ResultingCycleTime); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the machining calculator is in expert mode or not.
        /// </summary>
        public bool IsExpertMode
        {
            get { return this.isExpertMode; }
            set { this.SetProperty(ref this.isExpertMode, value, () => this.IsExpertMode, this.UpdateTurningCalculationResult); }
        }

        /// <summary>
        /// Gets or sets the token used for sending messages to machining calculator.
        /// </summary>
        public Guid MessengerTokenForMachining { get; set; }

        /// <summary>
        /// Gets or sets the UndoManager from CycleTime parent window.
        /// </summary>
        public UndoManager ParentUndoManager { get; set; }

        #endregion

        /// <summary>
        /// Called when the Model has changed.
        /// </summary>
        protected override void OnModelChanged()
        {
            base.OnModelChanged();

            if ((this.Model != null) && this.Model.MachiningType.Value == MachiningType.Turning && this.EditMode == ViewModelEditMode.Edit)
            {
                this.LoadTurningResultDataFromCalculationToEdit();
            }
        }

        /// <summary>
        /// Initializes the properties of this instance.
        /// </summary>
        private void InitializeProperties()
        {
            this.machiningCalculator = new MachiningCalculator();
            this.TurningMaterials = new List<TurningMaterialToBeMachined>(this.machiningCalculator.Knowledgebase.TurningData.MaterialsToMachine);

            this.SelectedMachiningType = TurningMachiningType.FinestFeed;
            this.SelectedMaterial = this.TurningMaterials.FirstOrDefault();
            this.SelectedTurningType = TurningType.Axial;
            this.SelectedTurningSituation = TurningSituation.ContinuousCut;
            this.FeedSpeed = this.machiningCalculator.Knowledgebase.TurningData.DefaultMachineFeedSpeed;
            this.CutDepth = this.machiningCalculator.Knowledgebase.TurningData.DefaultCutDepth;
            this.StartStopTime = this.machiningCalculator.Knowledgebase.TurningData.DefaultStartStopTimeForMachineFeed;
            this.FeedLength = this.machiningCalculator.Knowledgebase.TurningData.DefaultMachineFeedLength;
            this.TurningDiameter = this.machiningCalculator.Knowledgebase.TurningData.DefaultTurningDiameter;
            this.TurningLength = this.machiningCalculator.Knowledgebase.TurningData.DefaultTurningLengh;
            this.TurningDepth = this.machiningCalculator.Knowledgebase.TurningData.DefaultTurningDepth;
            this.ToolChangeTime = this.machiningCalculator.Knowledgebase.TurningData.DefaultToolChangeTime;
        }

        /// <summary>
        /// Loads the turning result data from the object that the screen is editing.
        /// </summary>
        private void LoadTurningResultDataFromCalculationToEdit()
        {
            if (this.Model.MachiningCalculationData == null ||
                this.Model.MachiningCalculationData.Value == null)
            {
                return;
            }

            UnicodeEncoding encoder = new UnicodeEncoding();
            string data = encoder.GetString(this.Model.MachiningCalculationData.Value);

            if (!string.IsNullOrEmpty(data))
            {
                this.ProcessDescription = this.Model.ToleranceType.Value;

                string[] parts = data.Split(';');
                if (parts.Length > 0)
                {
                    int turningMaterialId;
                    if (int.TryParse(parts[0], out turningMaterialId))
                    {
                        this.SelectedMaterial = this.TurningMaterials.FirstOrDefault(material => material.MaterialID == turningMaterialId);
                        if (this.SelectedMaterial == null)
                        {
                            this.windowService.MessageDialogService.Show(LocalizedResources.MachiningCalculator_MaterialUnavailable, MessageDialogType.Error);
                        }
                    }
                    else
                    {
                        this.SelectedMaterial = null;
                    }
                }

                if (parts.Length > 1)
                {
                    TurningType turningType;
                    if (Enum.TryParse(parts[1], out turningType))
                    {
                        this.SelectedTurningType = turningType;
                    }
                }

                if (parts.Length > 2)
                {
                    this.FeedSpeed = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[2]);
                }

                if (parts.Length > 3)
                {
                    TurningSituation turningSituation;
                    if (Enum.TryParse(parts[3], out turningSituation))
                    {
                        this.SelectedTurningSituation = turningSituation;
                    }
                }

                if (parts.Length > 4)
                {
                    this.CutDepth = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[4]);
                }

                if (parts.Length > 5)
                {
                    this.StartStopTime = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[5]);
                }

                if (parts.Length > 6)
                {
                    this.FeedLength = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[6]);
                }

                if (parts.Length > 7)
                {
                    this.TurningDiameter = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[7]);
                }

                if (parts.Length > 8)
                {
                    this.TurningLength = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[8]);
                }

                if (parts.Length > 9)
                {
                    this.TurningDepth = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[9]);
                }

                bool expertMode = false;
                if (parts.Length > 10)
                {
                    bool.TryParse(parts[10], out expertMode);
                    this.IsExpertMode = expertMode;

                    if (this.IsExpertMode)
                    {
                        var message = new NotificationMessage<CycleTimeCalculationItemViewModel>(MachiningCalculatorViewModel.CalculatorNotificationMessages.ExpertModeChanged, this.Model);
                        this.messenger.Send<NotificationMessage<CycleTimeCalculationItemViewModel>>(message, this.MessengerTokenForMachining);
                    }
                }

                if (expertMode && parts.Length > 11)
                {
                    this.FeedRate = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[11]);
                }

                if (parts.Length > 12)
                {
                    this.ToolChangeTime = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[12]);
                }

                if (expertMode && parts.Length > 13)
                {
                    this.TurningSpeed = MachiningCalculatorHelper.ConvertFromInvariantNumberRepresentation(parts[13]);
                }

                if (parts.Length > 14)
                {
                    TurningMachiningType machiningType;
                    if (Enum.TryParse(parts[14], out machiningType))
                    {
                        this.SelectedMachiningType = machiningType;
                    }
                }
            }
        }

        /// <summary>
        /// Performs the turning operation calculation and updates the result on the UI.
        /// </summary>
        private void UpdateTurningCalculationResult()
        {
            using (this.UndoManager.Pause())
            {
                TurningCalculationResult result = CalculateTurningOperation();
                this.ResultingTurningTime = result.TurningTime;
                this.ResultingCycleTime = result.GrossProcessTime;

                if (this.IsExpertMode)
                {
                    var resultedTurningSpeed = result.TurningSpeed.HasValue ? Math.Round(result.TurningSpeed.Value) : (decimal?)null;
                    this.CalculatedTurningSpeed = Formatter.FormatNumber(resultedTurningSpeed);

                    var resultedFeedRate = result.CalculatedFeedRate.HasValue ? Math.Round(result.CalculatedFeedRate.Value, 2) : (decimal?)null;
                    this.CalculatedFeedRate = Formatter.FormatNumber(resultedFeedRate);
                }
                else
                {
                    this.TurningSpeed = result.TurningSpeed;
                    this.FeedRate = result.CalculatedFeedRate;
                }
            }
        }

        /// <summary>
        /// Performs the save operation. Executed by the SaveCommand.
        /// </summary>
        protected override void Save()
        {
            if (this.EditMode == ViewModelEditMode.Create)
            {
                // If the view model is not in edit mode, it does not have a model attached, so we have to create it.
                var calculation = new CycleTimeCalculation();
                var calculationItemVM = new CycleTimeCalculationItemViewModel(windowService, messenger);
                calculationItemVM.EditMode = ViewModelEditMode.Create;

                this.Model = calculationItemVM;
                calculationItemVM.Model = calculation;
            }

            this.SaveToModel();

            if (this.EditMode == ViewModelEditMode.Create)
            {
                var message = new NotificationMessage<CycleTimeCalculationItemViewModel>(MachiningCalculatorViewModel.CalculatorNotificationMessages.AddMachiningCalculator, this.Model);
                this.messenger.Send<NotificationMessage<CycleTimeCalculationItemViewModel>>(message, this.MessengerTokenForMachining);
            }
            else
            {
                var message = new NotificationMessage<CycleTimeCalculationItemViewModel>(MachiningCalculatorViewModel.CalculatorNotificationMessages.UpdateMachiningCalculator, this.Model);
                this.messenger.Send<NotificationMessage<CycleTimeCalculationItemViewModel>>(message, this.MessengerTokenForMachining);
            }
        }

        /// <summary>
        /// Executes the logic associated with the SaveToModelCommand command.
        /// </summary>
        protected override void SaveToModel()
        {
            if (this.ParentUndoManager != null)
            {
                using (this.ParentUndoManager.StartBatch(navigateToBatchControls: true))
                {
                    this.SaveTurningData();
                }
            }
            else
            {
                this.SaveTurningData();
            }
        }

        /// <summary>
        /// Saves all changed back into the model and resets the changed status.
        /// </summary>
        private void SaveTurningData()
        {
            TurningCalculationResult result = this.CalculateTurningOperation();

            this.Model.Description.Value = LocalizedResources.MachiningCalculator_Turning + " - " + this.SelectedTurningType;
            this.Model.MachiningVolume.Value = result.MachiningVolume.HasValue ? result.MachiningVolume.Value : 0;
            this.Model.MachiningType.Value = MachiningType.Turning;

            decimal? toolChangeTime = this.ToolChangeTime;
            decimal? transportTime = result.GrossProcessTime - result.TurningTime - toolChangeTime;
            this.Model.TransportClampingTime.Value = transportTime.GetValueOrDefault();
            this.Model.MachiningTime.Value = result.TurningTime.GetValueOrDefault();
            this.Model.ToolChangeTime.Value = toolChangeTime.GetValueOrDefault();
            this.Model.ToleranceType.Value = this.ProcessDescription;

            this.SaveTurningResultDataToBinary();
        }

        /// <summary>
        /// Calculates the turning operation.
        /// </summary>
        /// <returns>The result.</returns>
        private TurningCalculationResult CalculateTurningOperation()
        {
            TurningCalculationInputData input = new TurningCalculationInputData();

            input.MachiningType = this.SelectedMachiningType;
            input.Material = this.SelectedMaterial;
            input.TurningType = this.SelectedTurningType;
            input.TurningSituation = this.SelectedTurningSituation;
            input.CutDepth = this.CutDepth;
            input.MachineFeedLength = this.FeedLength;
            input.MachineFeedSpeed = this.FeedSpeed;
            input.StartStopTimeForMachineFeed = this.StartStopTime;
            input.TurningDiameter = this.TurningDiameter;
            input.TurningLengh = this.TurningLength;
            input.TurningDepth = this.TurningDepth;
            input.ToolChangeTime = this.ToolChangeTime;
            if (this.IsExpertMode)
            {
                input.ExpertModeCalculation = true;
                input.FeedRate = this.FeedRate;
                input.TurningSpeed = this.TurningSpeed;
            }
            else
            {
                input.ExpertModeCalculation = false;
            }

            return this.machiningCalculator.CalculateTurningOperation(input);
        }

        /// <summary>
        /// Saves the current turning result data into the machining calculation data.
        /// </summary>
        private void SaveTurningResultDataToBinary()
        {
            // The data will be saved as a comma separated list.
            string data = string.Empty;

            string turningMaterial = string.Empty;
            TurningMaterialToBeMachined material = this.SelectedMaterial;
            if (material != null)
            {
                turningMaterial = material.MaterialID.ToString();
            }

            string turningType = ((int)this.SelectedTurningType).ToString();
            string turningSituation = ((int)this.SelectedTurningSituation).ToString();
            string machiningType = ((int)this.SelectedMachiningType).ToString();

            data += turningMaterial + ";";
            data += turningType + ";";
            data += (this.FeedSpeed.HasValue ? this.FeedSpeed.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += turningSituation + ";";
            data += (this.CutDepth.HasValue ? this.CutDepth.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += (this.StartStopTime.HasValue ? this.StartStopTime.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += (this.FeedLength.HasValue ? this.FeedLength.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += (this.TurningDiameter.HasValue ? this.TurningDiameter.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += (this.TurningLength.HasValue ? this.TurningLength.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += (this.TurningDepth.HasValue ? this.TurningDepth.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += this.IsExpertMode + ";";
            data += (this.FeedRate.HasValue ? this.FeedRate.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += (this.ToolChangeTime.HasValue ? this.ToolChangeTime.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += (this.TurningSpeed.HasValue ? this.TurningSpeed.Value.ToString(CultureInfo.InvariantCulture) : string.Empty) + ";";
            data += machiningType;

            UnicodeEncoding encoding = new UnicodeEncoding();
            this.Model.MachiningCalculationData.Value = encoding.GetBytes(data);
        }
    }
}
