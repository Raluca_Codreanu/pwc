﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// Represents the database location.
    /// </summary>
    public enum DbLocation
    {
        /// <summary>
        /// Local database.
        /// </summary>
        Local = 0,

        /// <summary>
        /// Central database.
        /// </summary>
        Central = 1,  
    }
}
