﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Utils;
using ZPKTool.Gui.Views;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Commands;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.ViewModels
{
    /// <summary>
    /// The view-model for the RoleSelection view.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class RoleSelectionViewModel : ViewModel
    {
        /// <summary>
        /// The composition container.
        /// </summary>
        private CompositionContainer container;

        /// <summary>
        /// The message dialog service.
        /// </summary>
        private IMessageDialogService messageDialogService;

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The current user's roles.
        /// </summary>
        private EnumerationDescription<Role> roles;

        /// <summary>
        /// Initializes a new instance of the <see cref="RoleSelectionViewModel"/> class.
        /// </summary>
        /// <param name="container">The composition container.</param>
        /// <param name="messageDialogService">The message box service.</param>
        /// <param name="messenger">The messenger.</param>
        [ImportingConstructor]
        public RoleSelectionViewModel(CompositionContainer container, IMessageDialogService messageDialogService, IMessenger messenger)
        {
            this.container = container;
            this.messageDialogService = messageDialogService;
            this.messenger = messenger;

            if (SecurityManager.Instance.CurrentUser != null)
            {
                this.Roles = new EnumerationDescription<Role>(RoleRights.GetRolesSeparated(SecurityManager.Instance.CurrentUser.Roles));
            }

            this.OkCommand = new DelegateCommand(SaveSelectedRole, () => this.SelectedRole != Data.Role.None);
        }

        /// <summary>
        /// Gets the OK command.
        /// </summary>
        public DelegateCommand OkCommand { get; private set; }

        /// <summary>
        /// Gets the roles of the current user.
        /// </summary>
        /// <value>The roles.</value>
        public EnumerationDescription<Role> Roles
        {
            get
            {
                return this.roles;
            }

            private set
            {
                if (this.roles != value)
                {
                    this.roles = value;
                    OnPropertyChanged("Roles");
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected role.
        /// </summary>        
        public Role SelectedRole { get; set; }

        /// <summary>
        /// Saves the selected role and continues.
        /// </summary>
        private void SaveSelectedRole()
        {
            if (this.SelectedRole != Data.Role.None)
            {
                // Save the selected role and load the welcome screen.
                SecurityManager.Instance.ChangeCurrentUserRole(this.SelectedRole);
                ShellViewModel shell = this.container.GetExportedValue<ShellViewModel>();

                if (UserSettingsManager.Instance.ShowStartScreen)
                {
                    shell.Content = this.container.GetExportedValue<WelcomeViewModel>();
                }
                else
                {
                    shell.Content = this.container.GetExportedValue<MainViewModel>();
                }

                this.messenger.Send<NotificationMessage>(new NotificationMessage(Notification.LoggedIn));
            }
            else
            {
                this.messageDialogService.Show(LocalizedResources.Error_SelectRole, MessageDialogType.Error);
            }
        }
    }
}
