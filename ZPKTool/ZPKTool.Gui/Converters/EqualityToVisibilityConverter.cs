﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;

namespace ZPKTool.Gui.Converters
{
    /// <summary>
    /// Converts an equalty check to visible if the values are different or collapsed if not.
    /// </summary>
    public class EqualityToVisibilityConverter : IMultiValueConverter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EqualityToVisibilityConverter" /> class.
        /// </summary>
        public EqualityToVisibilityConverter()
        {
            this.EqualVisibility = Visibility.Collapsed;
            this.StringComparisonRule = StringComparison.InvariantCultureIgnoreCase;
        }

        /// <summary>
        /// Gets or sets the visibility when the compared values are equal. The default is <see cref="Visibility.Collapsed"/>.
        /// </summary>
        public Visibility EqualVisibility { get; set; }

        /// <summary>
        /// Gets or sets the string comparison rule. The default is <see cref="StringComparison.InvariantCultureIgnoreCase"/>.
        /// </summary>
        public StringComparison StringComparisonRule { get; set; }

        /// <summary>
        /// Converts source values to a value for the binding target. The data binding engine calls this method when it propagates the values from source bindings to the binding target.
        /// </summary>
        /// <param name="values">The array of values that the source bindings in the <see cref="T:System.Windows.Data.MultiBinding" /> produces. The value <see cref="F:System.Windows.DependencyProperty.UnsetValue" /> indicates that the source binding has no value to provide for conversion.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value.If the method returns null, the valid null value is used.A return value of <see cref="T:System.Windows.DependencyProperty" />.<see cref="F:System.Windows.DependencyProperty.UnsetValue" /> indicates that the converter did not produce a value, and that the binding will use the <see cref="P:System.Windows.Data.BindingBase.FallbackValue" /> if it is available, or else will use the default value.A return value of <see cref="T:System.Windows.Data.Binding" />.<see cref="F:System.Windows.Data.Binding.DoNothing" /> indicates that the binding does not transfer the value or use the <see cref="P:System.Windows.Data.BindingBase.FallbackValue" /> or the default value.
        /// </returns>
        /// <exception cref="System.ArgumentException">The list of values to convert was empty or not all values provided to the converter were Boolean.</exception>
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values.Count() != 2)
            {
                throw new ArgumentException("The list did not contain two values.");
            }

            bool areValuesEqual = false;
            if (values[0] == null && values[1] == null)
            {
                areValuesEqual = true;
            }
            else if ((values[0] == null && values[1] != null)
                || (values[0] != null && values[1] == null))
            {
                areValuesEqual = false;
            }
            else
            {
                if (values[0].GetType() != values[1].GetType())
                {
                    throw new InvalidOperationException("The converter values must have the same type.");
                }

                var firstString = values[0] as string;
                var secondString = values[1] as string;
                if (firstString != null
                    && secondString != null)
                {
                    if (string.Compare(firstString, secondString, StringComparisonRule) == 0)
                    {
                        areValuesEqual = true;
                    }
                }
                else
                {
                    var comparableFirstValue = values[0] as IComparable;
                    var comparableSecondValue = values[1] as IComparable;
                    if (comparableFirstValue != null
                        && comparableSecondValue != null)
                    {
                        areValuesEqual = comparableFirstValue.CompareTo(comparableSecondValue) == 0;
                    }
                }
            }

            if (areValuesEqual)
            {
                return EqualVisibility;
            }
            else
            {
                return EqualVisibility == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;
            }
        }

        /// <summary>
        /// Converts a binding target value to the source binding values.
        /// </summary>
        /// <param name="value">The value that the binding target produces.</param>
        /// <param name="targetTypes">The array of types to convert to. The array length indicates the number and types of values that are suggested for the method to return.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// An array of values that have been converted from the target value back to the source values.
        /// </returns>
        /// <exception cref="System.NotSupportedException">It is not possible to convert back from Visibility to multiple Boolean values.</exception>        
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException("It is not possible to convert back from Visibility to multiple Boolean values.");
        }
    }
}