﻿namespace ZPKTool.Gui.Converters
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Windows.Data;
    using System.Windows.Media;

    /// <summary>
    /// Compares the binding value to the parameter and returns a Green brush if the value is less than or equal to the parameter
    /// and Red brush is the value is greater than the parameter.
    /// </summary>
    public class ColorCodedComparisonConverter : IValueConverter
    {
        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value", "The value to convert was null");
            }

            if (parameter == null)
            {
                throw new ArgumentNullException("parameter", "The converter parameter was null");
            }

            if (value.GetType() != parameter.GetType())
            {
                throw new InvalidOperationException("The converter value and parameter must have the same type.");
            }

            IComparable comparableValue = value as IComparable;
            if (comparableValue == null)
            {
                throw new InvalidOperationException("The converter value and parameter must implement IComparable.");
            }

            int result = comparableValue.CompareTo(parameter);
            if (result <= 0)
            {
                return new SolidColorBrush(Color.FromArgb(255, 0, 215, 0));
            }
            else
            {
                return new SolidColorBrush(Colors.Red);
            }
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
