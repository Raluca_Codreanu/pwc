﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;

namespace ZPKTool.Gui.Converters
{
    /// <summary>
    /// Converts a value from bool to Visibility.
    /// </summary>
    [ValueConversion(typeof(bool), typeof(Visibility))]
    public class BoolToVisibilityConverter : IValueConverter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BoolToVisibilityConverter" /> class.
        /// </summary>
        public BoolToVisibilityConverter()
        {
            HideValue = Visibility.Collapsed;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the conversion logic is inverted, that is 'true' is converted to 'Collapsed' and 'false' to 'Visible'.
        /// Default value is false.
        /// </summary>
        public bool Inverted { get; set; }

        /// <summary>
        /// Gets or sets the value returned for hidden. The default is <see cref="Visibility.Collapsed"/>.
        /// </summary>
        public Visibility HideValue { get; set; }

        #region IValueConverter Members

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is bool)
            {
                bool val = (bool)value;
                if ((this.Inverted && !val)
                    || (!this.Inverted && val))
                {
                    return Visibility.Visible;
                }
            }

            return this.HideValue;
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is Visibility)
            {
                var visibility = (Visibility)value;
                if (visibility == Visibility.Visible)
                {
                    return !this.Inverted;
                }
                else
                {
                    return this.Inverted;
                }
            }

            return false;
        }

        #endregion
    }
}
