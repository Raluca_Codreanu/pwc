﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;

namespace ZPKTool.Gui.Converters
{
    /// <summary>
    /// Similar to <see cref="BoolToVisibilityConverter"/> but the Boolean value to convert is obtained by applying the "and" or "or"
    /// logical operator to multiple Boolean values.        
    /// </summary>
    public class BoolToVisibilityMultiConverter : IMultiValueConverter
    {
        /// <summary>
        /// Gets or sets a value indicating whether the conversion logic is inverted, that is 'true' is converted to 'Collapsed' and 'false' to 'Visible'.
        /// Default value is false.
        /// </summary>
        public bool Inverted { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to use the "OR" operator between the values to convert. If set to false, the "AND" operator is used.
        /// The default is false.
        /// </summary>
        public bool UseOrOperator { get; set; }

        /// <summary>
        /// Converts source values to a value for the binding target. The data binding engine calls this method when it propagates the values from source bindings to the binding target.
        /// </summary>
        /// <param name="values">The array of values that the source bindings in the <see cref="T:System.Windows.Data.MultiBinding" /> produces. The value <see cref="F:System.Windows.DependencyProperty.UnsetValue" /> indicates that the source binding has no value to provide for conversion.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value.If the method returns null, the valid null value is used.A return value of <see cref="T:System.Windows.DependencyProperty" />.<see cref="F:System.Windows.DependencyProperty.UnsetValue" /> indicates that the converter did not produce a value, and that the binding will use the <see cref="P:System.Windows.Data.BindingBase.FallbackValue" /> if it is available, or else will use the default value.A return value of <see cref="T:System.Windows.Data.Binding" />.<see cref="F:System.Windows.Data.Binding.DoNothing" /> indicates that the binding does not transfer the value or use the <see cref="P:System.Windows.Data.BindingBase.FallbackValue" /> or the default value.
        /// </returns>
        /// <exception cref="System.ArgumentException">The list of values to convert was empty or not all values provided to the converter were Boolean.</exception>
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values.Length == 0)
            {
                throw new ArgumentException("The list of values to convert was empty.");
            }

            var boolValues = values.OfType<bool>();
            if (boolValues.Count() != values.Length)
            {
                throw new ArgumentException("Not all values provided to the converter were Boolean.");
            }

            bool value;
            if (this.UseOrOperator)
            {
                value = false;
                foreach (var val in boolValues)
                {
                    value = value || val;
                }
            }
            else
            {
                value = true;
                foreach (var val in boolValues)
                {
                    value = value && val;
                }
            }

            if ((this.Inverted && !value)
                || (!this.Inverted && value))
            {
                return Visibility.Visible;
            }

            return Visibility.Collapsed;
        }

        /// <summary>
        /// Converts a binding target value to the source binding values.
        /// </summary>
        /// <param name="value">The value that the binding target produces.</param>
        /// <param name="targetTypes">The array of types to convert to. The array length indicates the number and types of values that are suggested for the method to return.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// An array of values that have been converted from the target value back to the source values.
        /// </returns>
        /// <exception cref="System.NotSupportedException">It is not possible to convert back from Visibility to multiple Boolean values.</exception>        
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException("It is not possible to convert back from Visibility to multiple Boolean values.");
        }
    }
}
