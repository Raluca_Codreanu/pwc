﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using ZPKTool.Calculations.CostCalculation;

namespace ZPKTool.Gui.Converters
{
    /// <summary>
    /// Determines a visibility value by comparing two calculation variants.
    /// </summary>
    public class VisibilityBasedOnCalculationVariantConverter : IValueConverter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VisibilityBasedOnCalculationVariantConverter" /> class.
        /// </summary>
        public VisibilityBasedOnCalculationVariantConverter()
        {
            this.CompareOperator = ValueConverterCompareOperator.GreaterThanEqual;
        }

        /// <summary>
        /// Gets or sets the compare operator used to compare the value with the parameter.
        /// The default value is <see cref="ValueConverterCompareOperator.GreaterThanEqual" />.
        /// </summary>
        public ValueConverterCompareOperator CompareOperator { get; set; }

        /// <summary>
        /// Compares the calculation variants provided in <paramref name="value"/> and <paramref name="parameter"/> using the
        /// operator specified by the <see cref="CompareOperator"/> property.
        /// </summary>
        /// <param name="value">The calculation variant to compare.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The calculation variant to compare to.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// Visibility.Visible if the comparison result was true; otherwise, Visibility.Collapsed.
        /// </returns>
        /// <exception cref="System.ArgumentException">The parameter provided to the converter was not a valid calculation variant.;parameter</exception>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string calcVariantToCompareTo = parameter as string;
            if (!CostCalculatorFactory.IsValidVersion(calcVariantToCompareTo))
            {
                throw new ArgumentException("The parameter provided to the converter was not a valid calculation variant.", "parameter");
            }

            string calculationVariant = value as string;
            if (!CostCalculatorFactory.IsValidVersion(calculationVariant))
            {
                calculationVariant = "1.0";
            }

            if (CompareOperator == ValueConverterCompareOperator.GreaterThanEqual)
            {
                if (CostCalculatorFactory.IsSame(calculationVariant, calcVariantToCompareTo)
                    || CostCalculatorFactory.IsNewer(calculationVariant, calcVariantToCompareTo))
                {
                    return Visibility.Visible;
                }
            }            
            else if (CompareOperator == ValueConverterCompareOperator.GreaterThan)
            {
                if (CostCalculatorFactory.IsNewer(calculationVariant, calcVariantToCompareTo))
                {
                    return Visibility.Visible;
                }
            }
            else if (CompareOperator == ValueConverterCompareOperator.LessThan)
            {
                if (CostCalculatorFactory.IsOlder(calculationVariant, calcVariantToCompareTo))
                {
                    return Visibility.Visible;
                }
            }
            else if (CompareOperator == ValueConverterCompareOperator.LessThanEqual)
            {
                if (CostCalculatorFactory.IsSame(calculationVariant, calcVariantToCompareTo)
                    || CostCalculatorFactory.IsOlder(calculationVariant, calcVariantToCompareTo))
                {
                    return Visibility.Visible;
                }
            }
            else if (CompareOperator == ValueConverterCompareOperator.Equal)
            {
                if (CostCalculatorFactory.IsSame(calculationVariant, calcVariantToCompareTo))
                {
                    return Visibility.Visible;
                }
            }
            else if (CompareOperator == ValueConverterCompareOperator.NotEqual)
            {
                if (!CostCalculatorFactory.IsSame(calculationVariant, calcVariantToCompareTo))
                {
                    return Visibility.Visible;
                }
            }

            return Visibility.Collapsed;
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        /// <exception cref="System.NotImplementedException">The method is not implemented.</exception>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
