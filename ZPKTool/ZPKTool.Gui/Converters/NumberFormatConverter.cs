﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;
using ZPKTool.Common;
using ZPKTool.Gui.Resources;

namespace ZPKTool.Gui.Converters
{
    /// <summary>
    /// A value converter that formats a number. The parameter can, optionally, contain the maximum number of digits the formatted number should contain.
    /// </summary>
    [ValueConversion(typeof(object), typeof(string))]
    public class NumberFormatConverter : IValueConverter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NumberFormatConverter" /> class.
        /// </summary>
        public NumberFormatConverter()
        {
            this.FallbackValue = string.Empty;
        }

        /// <summary>
        /// Gets or sets the fallback value, which is returned when the value to convert is null or could not be converted.
        /// </summary>        
        public string FallbackValue { get; set; }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                int decimalPlaces = 0;

                // If the parameter is null or not a string default to 0 decimal places.
                string strg = parameter as string;
                if (strg != null)
                {
                    int.TryParse(strg, out decimalPlaces);
                }

                string formattedValue = Formatter.FormatNumber(value, decimalPlaces);
                if (!string.IsNullOrEmpty(formattedValue))
                {
                    return formattedValue;
                }
            }

            return this.FallbackValue;
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
