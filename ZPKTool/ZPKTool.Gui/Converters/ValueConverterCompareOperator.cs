﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.Converters
{
    /// <summary>
    /// Specifies the comparison operators used by value converters.
    /// </summary>
    public enum ValueConverterCompareOperator
    {
        /// <summary>
        /// A comparison for equality.
        /// </summary>
        Equal,

        /// <summary>
        /// A comparison for inequality.
        /// </summary>
        NotEqual,

        /// <summary>
        /// A comparison for greater than.
        /// </summary>
        GreaterThan,

        /// <summary>
        /// A comparison for greater than or equal to.
        /// </summary>
        GreaterThanEqual,

        /// <summary>
        /// A comparison for less than.
        /// </summary>
        LessThan,

        /// <summary>
        /// A comparison for less than or equal to.
        /// </summary>
        LessThanEqual
    }
}
