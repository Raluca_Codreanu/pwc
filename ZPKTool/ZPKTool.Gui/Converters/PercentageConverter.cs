﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;
using ZPKTool.Common;
using ZPKTool.Gui.Resources;

namespace ZPKTool.Gui.Converters
{
    /// <summary>
    /// A value converter that formats a number.
    /// </summary>
    [ValueConversion(typeof(object), typeof(string))]
    public class PercentageConverter : IValueConverter
    {
        /// <summary>
        /// Converts a percentage value from db representation to UI representation (ex: 0.521 to 52.1%).
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// The converted value (multiplied with 100).
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return string.Empty;
            }

            if (value is decimal)
            {
                return Formatter.FormatNumber((decimal)value * 100, 4) + LocalizedResources.General_PercentSymbol;
            }

            if (value is string)
            {
                decimal decimalValue;
                if (decimal.TryParse((string)value, out decimalValue))
                {
                    return Formatter.FormatNumber(decimalValue * 100, 4) + LocalizedResources.General_PercentSymbol;
                }
                else
                {
                    return string.Empty;
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}