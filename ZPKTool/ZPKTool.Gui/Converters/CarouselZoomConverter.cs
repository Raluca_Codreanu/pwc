﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Media.Media3D;

namespace ZPKTool.Gui.Converters
{
    /// <summary>
    /// This class converts a double value representing the Application Zoom level to another double value that represent the specific carousel Zoom level.
    /// </summary>
    [ValueConversion(typeof(double), typeof(double))]
    public class CarouselZoomConverter : IValueConverter
    {
        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double cameraZCoord = 1;

            if (value is double)
            {
                double currentZoom = (double)value;

                if (currentZoom <= 1)
                {
                    cameraZCoord = 1 + ((1 - currentZoom) / 2);
                }
                else
                {
                    cameraZCoord = 1 - ((currentZoom - 1) / 4);
                }
            }

            Point3D temp = new Point3D(0, 0, cameraZCoord * 7);

            return new Point3D(0, 0, cameraZCoord * 7);
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
