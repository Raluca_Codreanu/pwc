﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;
using ZPKTool.Common;
using ZPKTool.Data;

namespace ZPKTool.Gui.Converters
{
    /// <summary>
    /// A value converter that formats a value representing a measurement unit.
    /// </summary>
    [ValueConversion(typeof(object), typeof(string))]
    public class MeasurementUnitConverter : IMultiValueConverter
    {
        /// <summary>
        /// Converts a value according to measurement unit type from db representation to UI representation (ex: 0.5m to 50cm).
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// The converted value.
        /// </returns>
        public object Convert(object[] value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || value.Length == 0)
            {
                return string.Empty;
            }

            decimal? valueToConvert = value[0] as decimal?;
            MeasurementUnit unit = null;
            if (value.Length > 1)
            {
                unit = value[1] as MeasurementUnit;
            }

            string displayValue = string.Empty;
            if (valueToConvert != null)
            {
                if (unit != null)
                {
                    if (unit.ConversionRate < 1)
                    {
                        displayValue = Formatter.FormatNumber(valueToConvert / unit.ConversionRate, 4);
                    }
                    else
                    {
                        displayValue = Formatter.FormatNumber(valueToConvert * unit.ConversionRate, 4);
                    }

                    if (!string.IsNullOrEmpty(unit.Symbol))
                    {
                        displayValue += " " + unit.Symbol;
                    }
                }
                else
                {
                    displayValue = Formatter.FormatNumber(valueToConvert, 4);
                }
            }

            return displayValue;
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object[] ConvertBack(object value, Type[] targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
