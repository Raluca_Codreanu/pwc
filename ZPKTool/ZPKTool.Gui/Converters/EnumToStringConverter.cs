﻿using System;
using System.Windows.Data;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Utils;

namespace ZPKTool.Gui.Converters
{
    /// <summary>
    /// This class converts a value representing an enumeration member to the string to be displayed on the UI in its place.
    /// The value representing the enumeration member can be the member's integral (short, int, long) value or the actual Enum member.
    /// </summary>
    [ValueConversion(typeof(short), typeof(object))]
    [ValueConversion(typeof(int), typeof(object))]
    [ValueConversion(typeof(Enum), typeof(object))]
    public class EnumToStringConverter : IValueConverter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EnumToStringConverter" /> class.
        /// </summary>
        public EnumToStringConverter()
        {
        }

        /// <summary>
        /// Gets or sets the fallback value, which is returned when the value to convert is null or could not be converted.
        /// </summary>        
        public object FallbackValue { get; set; }

        #region IValueConverter Members

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is Enum)
            {
                return UIUtils.GetEnumValueLabel(value as Enum);
            }
            else if (value != null)
            {
                var enumValue = System.Convert.ToInt64(value);
                var enumType = parameter as Type;
                if (enumType == null || !enumType.IsEnum)
                {
                    throw new ArgumentException("The converter parameter did not contain an enumeration type.", "targetType");
                }

                return UIUtils.GetEnumValueLabel(enumValue, enumType);
            }

            return this.FallbackValue;
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
