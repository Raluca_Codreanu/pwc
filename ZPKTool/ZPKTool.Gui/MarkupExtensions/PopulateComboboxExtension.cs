﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Xaml;
using ZPKTool.Common;
using ZPKTool.Controls;
using ZPKTool.Gui.Utils;

namespace ZPKTool.Gui.MarkupExtensions
{
    /// <summary>
    /// This markup extension based on an enumeration type returns the list of the members description.
    /// </summary>
    public class PopulateComboboxExtension : MarkupExtension
    {
        /// <summary>
        /// The enumeration type.
        /// </summary>
        private Type enumType;

        /// <summary>
        /// Initializes a new instance of the <see cref="PopulateComboboxExtension"/> class.
        /// </summary>
        /// <param name="type">The type of the enumeration.</param>s
        public PopulateComboboxExtension(Type type)
        {
            enumType = type;
        }

        /// <summary>
        /// When implemented in a derived class, returns an object that is set as the value of the target property for this markup extension.
        /// </summary>
        /// <param name="serviceProvider">Object that can provide services for the markup extension.</param>
        /// <returns>
        /// The object value to set on the property where the extension is applied.
        /// </returns>
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            var listType = typeof(EnumerationDescription<>);
            var constructedListType = listType.MakeGenericType(enumType);
            var instance = Activator.CreateInstance(constructedListType);

            IProvideValueTarget target = serviceProvider.GetService(typeof(IProvideValueTarget)) as IProvideValueTarget;
            if (target != null)
            {
                var combobox = target.TargetObject as ComboBox;
                if (combobox != null)
                {
                    var valueDescription = new ValueDescription<int>();
                    combobox.DisplayMemberPath = ReflectionUtils.GetPropertyName(() => valueDescription.Description);
                    combobox.SelectedValuePath = ReflectionUtils.GetPropertyName(() => valueDescription.Value);
                }
            }

            return instance;
        }
    }
}