﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;

namespace ZPKTool.Gui.MarkupExtensions
{
    /// <summary>
    /// This markup extension locates the first focusable child and returns it.
    /// It is intended to be used with FocusManager.FocusedElement:
    /// </summary>
    public class FocusedElementExtension : MarkupExtension
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FocusedElementExtension"/> class
        /// </summary>
        public FocusedElementExtension()
        {
            OneTime = false;
        }

        /// <summary>
        /// Gets or sets a value indicating whether to set focus to the element only the first time
        /// </summary>
        public bool OneTime { get; set; }

        /// <summary>
        /// This method locates the first focusable + visible element we can change focus to.
        /// </summary>
        /// <param name="serviceProvider">IServiceProvider from XAML</param>
        /// <returns>Focusable Element or null</returns>
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            // Ignore if is in design mode
            if ((bool)DesignerProperties.IsInDesignModeProperty.GetMetadata(typeof(DependencyObject)).DefaultValue)
            {
                return null;
            }

            // Get the IProvideValue interface which gives us access to the target property and object.  
            // MSDN calls this interface "internal" but it's necessary here because we need to know what property we are assigning to.
            var pvt = serviceProvider.GetService(typeof(IProvideValueTarget)) as IProvideValueTarget;
            if (pvt != null)
            {
                var element = pvt.TargetObject as FrameworkElement;
                object targetProperty = pvt.TargetProperty;
                if (element != null)
                {
                    // If the element isn't loaded yet, then wait for it.
                    if (!element.IsLoaded)
                    {
                        RoutedEventHandler deferredFocusHookup = null;
                        deferredFocusHookup = delegate
                        {
                            // Ignore if the element is now loaded but not visible - this happens for things like TabItem.
                            // Instead, we'll wait until the item becomes visible and then set focus.
                            if (element.IsVisible == false)
                            {
                                return;
                            }

                            // Look for a leaf child that is already selected
                            IInputElement child = GetFirstChildFocusable(element);

                            if (child != null)
                            {
                                if (targetProperty is DependencyProperty)
                                {
                                    // Specific case where we are setting focused element.
                                    // We really need to set this property onto the focus scope, so we'll use UIElement.Focus() which will do exactly that.
                                    if (targetProperty == FocusManager.FocusedElementProperty)
                                    {
                                        child.Focus();
                                    }
                                    else
                                    {
                                        // Being assigned to some other property - just assign it.
                                        element.SetValue((DependencyProperty)targetProperty, child);
                                    }
                                }
                                else if (targetProperty is PropertyInfo)
                                {
                                    // Simple property assignment through reflection.
                                    var propInfo = (PropertyInfo)targetProperty;
                                    propInfo.SetValue(element, child, null);
                                }
                            }

                            // Unhook the handler if we are supposed to.
                            if (OneTime)
                            {
                                element.Loaded -= deferredFocusHookup;
                            }
                        };

                        // Wait for the element to load 
                        element.Loaded += deferredFocusHookup;
                    }
                    else
                    {
                        // Look for the first focusable or focused child
                        return GetFirstChildFocusable(element);
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// This searches the Visual Tree looking for a valid child which can have focus.
        /// </summary>
        /// <param name="input">The root input element</param>
        /// <returns>The first focusable child or null</returns>
        private static IInputElement GetFirstChildFocusable(IInputElement input)
        {
            if (input == null)
            {
                return null;
            }

            var depObject = input as DependencyObject;
            DependencyObject child = null;
            if (depObject != null)
            {
                Func<DependencyObject, bool> predicate = (iic) =>
                    (iic is IInputElement) && ((IInputElement)iic).Focusable && ((IInputElement)iic).IsEnabled
                    && (!(iic is FrameworkElement) || ((FrameworkElement)iic).IsVisible);
                child = EnumerateVisualTree(depObject, c => !FocusManager.GetIsFocusScope(c)).FirstOrDefault(predicate);
            }

            return child as IInputElement;
        }

        /// <summary>
        /// A simple iterate method to expose the visual tree to LINQ
        /// </summary>
        /// <typeparam name="T">The element type</typeparam>
        /// <param name="start">The root element</param>
        /// <param name="eval">The predicate used evaluate</param>
        /// <returns>The found children collection</returns>
        private static IEnumerable<T> EnumerateVisualTree<T>(T start, Predicate<T> eval) where T : DependencyObject
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(start); i++)
            {
                var child = VisualTreeHelper.GetChild(start, i) as T;
                if (child != null && (eval != null && eval(child)))
                {
                    yield return child;
                    foreach (var childOfChild in EnumerateVisualTree(child, eval))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }
    }
}
