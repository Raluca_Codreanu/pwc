﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Threading;
using AvalonDock;
using ZPKTool.Common;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Utils;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Services
{
    /// <summary>
    /// Implementation of IPleaseWaitService that shows a please wait window.
    /// </summary>  
    [Export(typeof(IPleaseWaitService))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class PleaseWaitService : ObservableObject, IPleaseWaitService
    {
        #region Attributes

        /// <summary>
        /// Please Wait Window display minimum time
        /// </summary>
        private const int DisplayMinTime = 1000;

        /// <summary>
        /// The UI Dispatcher.
        /// </summary>
        private IDispatcherService dispatcher;

        /// <summary>
        /// The message displayed during work operations are performed.
        /// </summary>
        private string message;

        #endregion Attributes

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PleaseWaitService"/> class.
        /// </summary>
        /// <param name="dispatcherService">the dispatcher, used to execute code on the UI thread</param>
        [ImportingConstructor]
        public PleaseWaitService(IDispatcherService dispatcherService)
        {
            this.dispatcher = dispatcherService;
        }

        #endregion Constructors

        /// <summary>
        /// Gets or sets the message displayed during work operations are performed.
        /// </summary>
        public string Message
        {
            get { return this.message; }
            set { this.SetProperty(ref this.message, value, () => this.Message); }
        }

        #region Methods

        /// <summary>
        /// Displays the PleaseWaitWindow during work operations are performed.
        /// </summary>
        /// <param name="statusMessage">The message to display.</param>
        /// <param name="work">The action to be performed.</param>
        /// <param name="workCompleted">The action to be performed when the work is completed.</param>
        public void Show(string statusMessage, Action<PleaseWaitService.WorkParams> work, Action<PleaseWaitService.WorkParams> workCompleted)
        {
            this.Show(statusMessage, 0, work, workCompleted);
        }

        /// <summary>
        /// Displays the PleaseWaitWindow during work operations are performed.
        /// </summary>
        /// <param name="statusMessage">The message to display.</param>
        /// <param name="displayDelay">The delay (in milliseconds) before displaying the wait widget.</param>
        /// <param name="work">The action to be performed.</param>
        /// <param name="workCompleted">The action to be performed when the work is completed.</param>
        public void Show(string statusMessage, int displayDelay, Action<PleaseWaitService.WorkParams> work, Action<PleaseWaitService.WorkParams> workCompleted)
        {
            this.Message = statusMessage;

            BackgroundWorker backgroundWorker = new BackgroundWorker();
            BlockedUIData blockedUIInfo = null;

            // Initialize the timer
            DispatcherTimer displayTimer = new DispatcherTimer();
            displayTimer.Interval = new TimeSpan(0, 0, 0, 0, displayDelay);
            displayTimer.Tick += new EventHandler(delegate
            {
                displayTimer.Stop();
                blockedUIInfo = this.BlockUI(statusMessage);
            });

            // Display this control in its specified parent. If a parent panel was not specified, the main window's top-most panel is used.
            if (displayDelay == 0)
            {
                blockedUIInfo = this.BlockUI(statusMessage);
            }
            else
            {
                displayTimer.Start();
            }

            // Execute work
            backgroundWorker.DoWork += (sender, e) =>
            {
                DateTime workStartTime = DateTime.Now;

                WorkParams workParams = new WorkParams();
                work(workParams);
                e.Result = workParams.Result;

                DateTime workFinishTime = DateTime.Now;
                var workTime = (int)(workFinishTime - workStartTime).TotalMilliseconds;

                if (DisplayMinTime > workTime && blockedUIInfo != null)
                {
                    Thread.Sleep(DisplayMinTime - workTime);
                }
            };

            // Execute workCompleted  
            backgroundWorker.RunWorkerCompleted += (sender, e) =>
            {
                displayTimer.Stop();
                this.UnblockUI(blockedUIInfo);

                WorkParams workParams = new WorkParams();
                workParams.Error = e.Error;
                if (e.Error == null)
                {
                    workParams.Result = e.Result;
                }

                workCompleted(workParams);
            };

            // Starts the work
            backgroundWorker.RunWorkerAsync();
        }

        /// <summary>
        /// Blocks the application's UI by disabling it and displaying an overlay containing a wait message (usually while performing a blocking task).
        /// </summary>
        /// <param name="waitMessage">The message to display in the wait overlay.</param>
        /// <returns>Data about the blocked UI elements, which will be needed to unblock them.</returns>
        private BlockedUIData BlockUI(string waitMessage)
        {
            var blockedUIData = new BlockedUIData();

            // Remember the last focused visual item before disable all windows on screen
            // This is needed to permit restoring the focus after the please wait windows is unloaded
            blockedUIData.LastFocusedElement = System.Windows.Input.Keyboard.FocusedElement;

            // Disable the active window and display an overlay containing the wait message.
            // (The active window is disabled because, usually, this task is triggered by an action inside the active window.)
            var activeWindowData = this.GetActiveWindowTopMostPanel();
            var activeWindow = activeWindowData.Item2;
            var activeWindowRootPanel = activeWindowData.Item1;

            if (activeWindow.IsEnabled)
            {
                activeWindow.IsEnabled = false;
                blockedUIData.DisabledWindows.Add(activeWindow);
            }

            // Set the binding to the wait message to display.
            var pleaseWaitControl = new PleaseWaitControl();
            var messageBinding = new Binding("Message");
            messageBinding.Mode = BindingMode.OneWay;
            messageBinding.Source = this;
            pleaseWaitControl.SetBinding(PleaseWaitControl.MessageProperty, messageBinding);

            Grid.SetColumnSpan(pleaseWaitControl, 100);
            Grid.SetRowSpan(pleaseWaitControl, 100);
            activeWindowRootPanel.Children.Add(pleaseWaitControl);
            blockedUIData.BlockedPanels.Add(new Tuple<Panel, UIElement>(activeWindowRootPanel, pleaseWaitControl));

            // -- Disable the floating and fly-out windows created by AvalonDock --
            // Disable the floating windows
            var floatingWindows = Application.Current.Windows.OfType<FloatingWindow>();
            foreach (var floatingWnd in floatingWindows.Where(wnd => wnd.IsEnabled))
            {
                floatingWnd.IsEnabled = false;
                blockedUIData.DisabledWindows.Add(floatingWnd);

                var hostedPane = floatingWnd.HostedPane as DockablePane;
                if (hostedPane != null)
                {
                    // The hosted pane's only child in the visual tree should be a border and the next descendant should be a grid. That grid can be used to display the wait overlay.
                    // FloatingDockablePane -> Border -> "root" Grid
                    if (VisualTreeHelper.GetChildrenCount(hostedPane) > 0)
                    {
                        var border = VisualTreeHelper.GetChild(hostedPane, 0) as Border;
                        Grid rootGrid = null;
                        if (border != null && (rootGrid = border.Child as Grid) != null)
                        {
                            var overlay = CreateOverlayForAvalonDockWindow();
                            rootGrid.Children.Add(overlay);
                            blockedUIData.BlockedPanels.Add(new Tuple<Panel, UIElement>(rootGrid, overlay));
                        }
                    }
                }
            }

            // Disable the fly-out windows
            var flyoutWindows = Application.Current.Windows.OfType<FlyoutPaneWindow>();
            foreach (var flyoutWnd in flyoutWindows.Where(wnd => wnd.IsEnabled))
            {
                flyoutWnd.IsEnabled = false;
                blockedUIData.DisabledWindows.Add(flyoutWnd);

                if (VisualTreeHelper.GetChildrenCount(flyoutWnd) > 0)
                {
                    // The root element of a fly-out window is a Grid, so we can use it to display the wait overlay
                    var rootGrid = VisualTreeHelper.GetChild(flyoutWnd, 0) as Grid;
                    if (rootGrid != null)
                    {
                        var overlay = CreateOverlayForAvalonDockWindow();
                        rootGrid.Children.Add(overlay);
                        blockedUIData.BlockedPanels.Add(new Tuple<Panel, UIElement>(rootGrid, overlay));
                    }
                }
            }

            return blockedUIData;
        }

        /// <summary>
        /// Unblocks the UI by reversing the changes made by the <see cref="BlockUI" /> method.
        /// </summary>
        /// <param name="blockedUIData">The data describing the blocked UI elements.</param>
        private void UnblockUI(BlockedUIData blockedUIData)
        {
            if (blockedUIData == null)
            {
                return;
            }

            // Enable the disabled windows.
            foreach (var window in blockedUIData.DisabledWindows)
            {
                window.IsEnabled = true;
            }

            // Remove the wait overlays.
            foreach (var tuple in blockedUIData.BlockedPanels)
            {
                var panel = tuple.Item1;
                var overlay = tuple.Item2;
                panel.Children.Remove(overlay);
            }

            // Try to restore the focus
            if (blockedUIData.LastFocusedElement != null)
            {
                blockedUIData.LastFocusedElement.Focus();
            }
        }

        /// <summary>
        /// Gets the top-most panel in the visual tree of the application's main window.
        /// </summary>
        /// <returns>A tuple containing he top-most panel (Item1) and the active window (Item2).</returns>
        /// <exception cref="InvalidOperationException"> The main window is null or it does not contain a panel.</exception>
        private Tuple<Panel, Window> GetActiveWindowTopMostPanel()
        {
            // Get the active window, ignoring the widows created by AvalonDock.
            var appWindows = WindowHelper.GetApplicationWindows();
            Window activeWindow = appWindows.FirstOrDefault(wnd => wnd.IsActive);
            if (activeWindow == null)
            {
                // If there is no active window (the app does not have focus), use the last loaded window from the app windows list.
                activeWindow = appWindows.Last(wnd => wnd.IsLoaded);
            }

            if (activeWindow == null)
            {
                throw new InvalidOperationException("The active window was null.");
            }

            ContentControl content = activeWindow;
            while (content != null && content.Content as Panel == null)
            {
                content = content.Content as ContentControl;
            }

            if (content == null)
            {
                throw new InvalidOperationException("The active window did not contain a panel.");
            }

            Panel panel = content.Content as Panel;
            if (panel == null)
            {
                throw new InvalidOperationException("The active window did not contain a panel.");
            }

            content = content.Content as ContentControl;
            while (content != null && content.Content as Panel != null)
            {
                panel = content.Content as Panel;
                content = content.Content as ContentControl;
            }

            return new Tuple<Panel, Window>(panel, activeWindow);
        }

        /// <summary>
        /// Creates the overlay displayed in an avalon dock window while running the task.
        /// </summary>
        /// <returns>A Grid containing the overlay UI.</returns>
        private Grid CreateOverlayForAvalonDockWindow()
        {
            var overlay = new Grid();
            overlay.SetResourceReference(Grid.BackgroundProperty, "DarkTransparentBackground");
            Grid.SetRowSpan(overlay, 100);
            Grid.SetColumnSpan(overlay, 100);
            return overlay;
        }

        #endregion Methods

        #region Inner classes

        /// <summary>
        /// Class that contains the input and output data for the work performed by the <see cref="PleaseWaitService"/>
        /// </summary>
        public class WorkParams
        {
            /// <summary>
            /// Gets or sets a value that represents the result of Work process.
            /// </summary>
            public object Result { get; set; }

            /// <summary>
            /// Gets or sets a value indicating which error occurred during an Work process.
            /// </summary>
            public Exception Error { get; set; }
        }

        /// <summary>
        /// A class that stores data about the UI elements blocked (or locked) before running the task.
        /// This data is necessary to unblock the UI after the task has finished.
        /// </summary>
        private class BlockedUIData
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="BlockedUIData" /> class.
            /// </summary>
            public BlockedUIData()
            {
                this.BlockedPanels = new List<Tuple<Panel, UIElement>>();
                this.DisabledWindows = new List<Window>();
            }

            /// <summary>
            /// Gets the panels that were blocked.
            /// <para />
            /// The tuples in the list contain the blocked panel in Item1 , and the wait overlay (which was added into the panel) in Item2.
            /// </summary>
            public List<Tuple<Panel, UIElement>> BlockedPanels { get; private set; }

            /// <summary>
            /// Gets the windows that were disabled.
            /// </summary>
            public List<Window> DisabledWindows { get; private set; }

            /// <summary>
            /// Gets or sets the last visual item that had the keyboard focus before the PleaseWait window appeared
            /// </summary>
            public IInputElement LastFocusedElement { get; set; }
        }

        #endregion Inner classes
    }
}