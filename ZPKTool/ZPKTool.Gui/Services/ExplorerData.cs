﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.DataAccess;

namespace ZPKTool.Gui.Services
{
    /// <summary>
    /// Holds data and the data manager that was used to retrieve it.
    /// </summary>
    /// <typeparam name="T">The type of the explorer's data.</typeparam>
    public class ExplorerData<T>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExplorerData{T}"/> class.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="dataManager">The data manager.</param>
        public ExplorerData(IEnumerable<T> data, IDataSourceManager dataManager)
        {
            this.Data = data;
            this.DataManager = dataManager;
        }

        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        public IEnumerable<T> Data { get; set; }

        /// <summary>
        /// Gets or sets the data manager.
        /// </summary>
        public IDataSourceManager DataManager { get; set; }
    }
}