﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZPKTool.Business;
using ZPKTool.Gui.Notifications;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Services
{
    /// <summary>
    /// This is a service that periodically checks if the central database is online and notifies when its online state changes.
    /// It also checks asynchronously the online state of the central db at startup.
    /// </summary>
    [Export(typeof(IOnlineCheckService))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class OnlineCheckService : IOnlineCheckService
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The timer used to periodically check the central database connection state.
        /// </summary>
        private System.Timers.Timer checkTimer = new System.Timers.Timer();

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The task that checks the online state of the central db.
        /// </summary>
        private Task checkTask;

        /// <summary>
        /// The number of checkTasks that are running.
        /// </summary>
        private volatile int checkRunners = 0;

        /// <summary>
        /// Initializes a new instance of the <see cref="OnlineCheckService"/> class.
        /// </summary>
        /// <param name="messenger">The messenger service.</param>
        [ImportingConstructor]
        public OnlineCheckService(IMessenger messenger)
        {
            if (messenger == null)
            {
                throw new ArgumentNullException("messenger", "The messenger was null.");
            }

            this.messenger = messenger;

            // Create a new task that checks the database connection state.
            checkTask = new Task(() =>
            {
                try
                {
                    this.StopCheckTimer();
                    this.CheckDatabaseConnectionState();
                }
                finally
                {
                    this.StartCheckTimer();
                }
            });

            checkTask.ContinueWith(task =>
            {
                var errors = task.Exception;
            });
        }

        /// <summary>
        /// Gets a value indicating whether the central database is online.
        /// </summary>
        public bool IsOnline
        {
            get { return LastOnlineCheck != null ? LastOnlineCheck.IsOnline : false; }
        }

        /// <summary>
        /// Gets the result of the last online check.
        /// </summary>
        public DbOnlineCheckResult LastOnlineCheck { get; private set; }

        /// <summary>
        /// Starts the service.
        /// </summary>
        /// <param name="interval">The interval at which the online check is performed, in milliseconds.
        /// Should be greater than 0 and less then int.MaxValue; if is not it defaults to 60000 (1 minute).</param>
        public void Start(double interval)
        {
            if (interval <= 0 || interval > int.MaxValue)
            {
                this.checkTimer.Interval = 60000;
            }
            else
            {
                this.checkTimer.Interval = interval;
            }

            this.checkTimer.Elapsed += new System.Timers.ElapsedEventHandler(CheckTimer_Elapsed);
            this.checkTimer.Start();
        }

        /// <summary>
        /// Checks the online state of the central database on the calling thread.
        /// </summary>
        /// <returns>True if the central database is online, false if is offline.</returns>
        public bool CheckNow()
        {
            if (checkTask.Status == TaskStatus.Running)
            {
                checkTask.Wait();
                return IsOnline;
            }

            try
            {
                StopCheckTimer();
                CheckDatabaseConnectionState();
                return IsOnline;
            }
            finally
            {
                StartCheckTimer();
            }
        }

        /// <summary>
        /// Checks asynchronously the online state of the central db at startup.
        /// </summary>
        public void CheckAsync()
        {
            if (checkTask.Status != TaskStatus.Running)
            {
                checkTask.Start();
            }
        }

        /// <summary>
        /// Stops the checkTimer, if the checkTask is the first checkRunner;
        /// Else, just increases the number of checkRunners.
        /// </summary>
        private void StopCheckTimer()
        {
            if (checkRunners == 0)
            {
                this.checkTimer.Stop();
            }

            checkRunners++;
        }

        /// <summary>
        /// Decreases the number of checkRunners and
        /// Starts the checkTimer, if the checkTask is the last checkRunner.
        /// </summary>
        private void StartCheckTimer()
        {
            if (checkRunners > 0)
            {
                checkRunners--;
            }

            if (checkRunners == 0)
            {
                this.checkTimer.Start();
            }
        }

        /// <summary>
        /// Checks and updates the state of the central database connection.
        /// </summary>
        private void CheckDatabaseConnectionState()
        {
            var checkResult = DatabaseHelper.CentralDatabaseOnlineCheck();
            if (LastOnlineCheck == null || checkResult.IsOnline != LastOnlineCheck.IsOnline)
            {
                LastOnlineCheck = checkResult;
                messenger.Send(new OnlineStateChangedMessage(checkResult));
            }
        }

        /// <summary>
        /// Handles the Elapsed event of the checkTimer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Timers.ElapsedEventArgs"/> instance containing the event data.</param>
        private void CheckTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            lock (this)
            {
                this.CheckDatabaseConnectionState();
            }
        }
    }
}
