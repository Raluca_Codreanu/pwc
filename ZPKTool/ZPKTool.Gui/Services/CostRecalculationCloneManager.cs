﻿namespace ZPKTool.Gui.Services
{
    using System.ComponentModel.Composition;
    using System.Linq;
    using ZPKTool.Business;
    using ZPKTool.Data;
    using ZPKTool.Gui.ViewModels;
    using ZPKTool.MvvmCore;

    /// <summary>
    /// Handles the model clones creation, used for the real time cost recalculation feature.
    /// </summary>
    [Export(typeof(ICostRecalculationCloneManager))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class CostRecalculationCloneManager : ICostRecalculationCloneManager
    {
        #region Public Methods

        /// <summary>
        /// Clones the model for the specified view model.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        public void Clone(ViewModel viewModel)
        {
            var assemblyVm = viewModel as AssemblyViewModel;
            if (assemblyVm != null)
            {
                this.CloneAssembly(assemblyVm);
                return;
            }

            var partVm = viewModel as PartViewModel;
            if (partVm != null)
            {
                this.ClonePart(partVm);
                return;
            }

            var commodityVm = viewModel as CommodityViewModel;
            if (commodityVm != null)
            {
                this.CloneCommodity(commodityVm);
                return;
            }

            var consumableVm = viewModel as ConsumableViewModel;
            if (consumableVm != null)
            {
                this.CloneConsumable(consumableVm);
                return;
            }

            var dieVm = viewModel as DieViewModel;
            if (dieVm != null)
            {
                this.CloneDie(dieVm);
                return;
            }

            var machineVm = viewModel as MachineViewModel;
            if (machineVm != null)
            {
                this.CloneMachine(machineVm);
                return;
            }

            var rawMaterialVm = viewModel as RawMaterialViewModel;
            if (rawMaterialVm != null)
            {
                this.CloneRawMaterial(rawMaterialVm);
                return;
            }

            var processVm = viewModel as ProcessViewModel;
            if (processVm != null)
            {
                this.CloneProcess(processVm);
                return;
            }

            var processStepVm = viewModel as ProcessStepViewModel;
            if (processStepVm != null)
            {
                this.CloneProcessStep(processStepVm);
                return;
            }

            var processStepEditorVm = viewModel as ProcessStepEditorViewModel;
            if (processStepEditorVm != null)
            {
                this.CloneProcessStepFromEditor(processStepEditorVm);
            }
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Clones the assembly model.
        /// </summary>
        /// <param name="viewModel">The Assembly view-model.</param>
        private void CloneAssembly(AssemblyViewModel viewModel)
        {
            if (viewModel.EditMode != ViewModelEditMode.Edit)
            {
                return;
            }

            var cloneManager = new CloneManager();
            var topAssembly = EntityUtils.GetTopAssembly(viewModel.Model);
            if (topAssembly != null)
            {
                var modelTopParentClone = cloneManager.Clone(topAssembly, false, true);
                if (modelTopParentClone != null)
                {
                    viewModel.ModelClone = EntityUtils.GetSubAssembly(modelTopParentClone, viewModel.Model.Guid);
                }
            }
        }

        /// <summary>
        /// Clones the part model.
        /// </summary>
        /// <param name="viewModel">The Part view-model.</param>
        private void ClonePart(PartViewModel viewModel)
        {
            if (viewModel.EditMode != ViewModelEditMode.Edit)
            {
                return;
            }

            var cloneManager = new CloneManager();

            if (viewModel.Model is RawPart)
            {
                var parentPart = viewModel.Model.ParentOfRawPart.FirstOrDefault();
                if (parentPart != null)
                {
                    if (parentPart.Assembly == null)
                    {
                        viewModel.ModelClone = cloneManager.Clone(viewModel.Model, false, true);
                    }
                    else
                    {
                        var topAssembly = EntityUtils.GetTopAssembly(parentPart.Assembly);
                        if (topAssembly != null)
                        {
                            var modelTopParentClone = cloneManager.Clone(topAssembly, false, true);
                            if (modelTopParentClone != null)
                            {
                                var parentAssemblyClone = EntityUtils.GetSubAssembly(modelTopParentClone, parentPart.Assembly.Guid);
                                if (parentAssemblyClone != null)
                                {
                                    var parentPartClone = parentAssemblyClone.Parts.FirstOrDefault(p => p.Guid == parentPart.Guid);
                                    if (parentPartClone != null)
                                    {
                                        viewModel.ModelClone = parentPartClone.RawPart;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                if (viewModel.Model.Assembly == null)
                {
                    viewModel.ModelClone = cloneManager.Clone(viewModel.Model, false, true);
                }
                else
                {
                    var topAssembly = EntityUtils.GetTopAssembly(viewModel.Model.Assembly);
                    if (topAssembly != null)
                    {
                        var modelTopParentClone = cloneManager.Clone(topAssembly, false, true);
                        if (modelTopParentClone != null)
                        {
                            var parentAssemblyClone = EntityUtils.GetSubAssembly(modelTopParentClone, viewModel.Model.Assembly.Guid);
                            if (parentAssemblyClone != null)
                            {
                                viewModel.ModelClone = parentAssemblyClone.Parts.FirstOrDefault(p => p.Guid == viewModel.Model.Guid);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Clones the commodity model.
        /// </summary>
        /// <param name="viewModel">The Commodity view-model.</param>
        private void CloneCommodity(CommodityViewModel viewModel)
        {
            if (viewModel.EditMode != ViewModelEditMode.Edit)
            {
                return;
            }

            var cloneManager = new CloneManager();

            if (viewModel.CommodityParent != null)
            {
                var parentPart = viewModel.CommodityParent as Part;
                if (parentPart != null)
                {
                    if (parentPart.Assembly == null)
                    {
                        viewModel.ModelParentClone = cloneManager.Clone(parentPart, false, true);
                        if (viewModel.ModelParentClone != null)
                        {
                            viewModel.ModelClone = ((Part)viewModel.ModelParentClone).Commodities.FirstOrDefault(c => c.Guid == viewModel.Model.Guid);
                        }
                    }
                    else
                    {
                        var topAssembly = EntityUtils.GetTopAssembly(parentPart.Assembly);
                        if (topAssembly != null)
                        {
                            var modelTopParentClone = cloneManager.Clone(topAssembly, false, true);
                            if (modelTopParentClone != null)
                            {
                                var parentAssembly = EntityUtils.GetSubAssembly(modelTopParentClone, parentPart.Assembly.Guid);
                                if (parentAssembly != null)
                                {
                                    viewModel.ModelParentClone = parentAssembly.Parts.FirstOrDefault(p => p.Guid == parentPart.Guid);
                                    if (viewModel.ModelParentClone != null)
                                    {
                                        viewModel.ModelClone = ((Part)viewModel.ModelParentClone).Commodities.FirstOrDefault(c => c.Guid == viewModel.Model.Guid);
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    var parentAssembly = viewModel.CommodityParent as Assembly;
                    if (parentAssembly != null)
                    {
                        var topAssembly = EntityUtils.GetTopAssembly(parentAssembly);
                        if (topAssembly != null)
                        {
                            var modelTopParentClone = cloneManager.Clone(topAssembly, false, true);
                            if (modelTopParentClone != null)
                            {
                                viewModel.ModelParentClone = EntityUtils.GetSubAssembly(modelTopParentClone, parentAssembly.Guid);
                                if (viewModel.ModelParentClone != null)
                                {
                                    var parentStep = ((Assembly)viewModel.ModelParentClone).Process.Steps.FirstOrDefault(ps => ps.Guid == viewModel.Model.ProcessStep.Guid);
                                    if (parentStep != null)
                                    {
                                        viewModel.ModelClone = parentStep.Commodities.FirstOrDefault(c => c.Guid == viewModel.Model.Guid);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Clones the consumable model.
        /// </summary>
        /// <param name="viewModel">The Consumable view-model.</param>
        private void CloneConsumable(ConsumableViewModel viewModel)
        {
            if (viewModel.EditMode != ViewModelEditMode.Edit)
            {
                return;
            }

            var cloneManager = new CloneManager();

            if (viewModel.ConsumableParent != null)
            {
                var parentPart = viewModel.ConsumableParent as Part;
                if (parentPart != null)
                {
                    if (parentPart.Assembly == null)
                    {
                        viewModel.ModelParentClone = cloneManager.Clone(parentPart, false, true);
                        if (viewModel.ModelParentClone != null)
                        {
                            var parentStep = ((Part)viewModel.ModelParentClone).Process.Steps.FirstOrDefault(ps => ps.Guid == viewModel.Model.ProcessStep.Guid);
                            if (parentStep != null)
                            {
                                viewModel.ModelClone = parentStep.Consumables.FirstOrDefault(c => c.Guid == viewModel.Model.Guid);
                            }
                        }
                    }
                    else
                    {
                        var topAssembly = EntityUtils.GetTopAssembly(parentPart.Assembly);
                        if (topAssembly != null)
                        {
                            var modelTopParentClone = cloneManager.Clone(topAssembly, false, true);
                            if (modelTopParentClone != null)
                            {
                                var parentAssembly = EntityUtils.GetSubAssembly(modelTopParentClone, parentPart.Assembly.Guid);
                                if (parentAssembly != null)
                                {
                                    viewModel.ModelParentClone = parentAssembly.Parts.FirstOrDefault(p => p.Guid == parentPart.Guid);
                                    if (viewModel.ModelParentClone != null)
                                    {
                                        var parentStep = ((Part)viewModel.ModelParentClone).Process.Steps.FirstOrDefault(ps => ps.Guid == viewModel.Model.ProcessStep.Guid);
                                        if (parentStep != null)
                                        {
                                            viewModel.ModelClone = parentStep.Consumables.FirstOrDefault(c => c.Guid == viewModel.Model.Guid);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    var parentAssembly = viewModel.ConsumableParent as Assembly;
                    if (parentAssembly != null)
                    {
                        var topAssembly = EntityUtils.GetTopAssembly(parentAssembly);
                        if (topAssembly != null)
                        {
                            var modelTopParentClone = cloneManager.Clone(topAssembly, false, true);
                            if (modelTopParentClone != null)
                            {
                                viewModel.ModelParentClone = EntityUtils.GetSubAssembly(modelTopParentClone, parentAssembly.Guid);
                                if (viewModel.ModelParentClone != null)
                                {
                                    var parentStep = ((Assembly)viewModel.ModelParentClone).Process.Steps.FirstOrDefault(ps => ps.Guid == viewModel.Model.ProcessStep.Guid);
                                    if (parentStep != null)
                                    {
                                        viewModel.ModelClone = parentStep.Consumables.FirstOrDefault(c => c.Guid == viewModel.Model.Guid);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Clones the die model.
        /// </summary>
        /// <param name="viewModel">The Die view-model.</param>
        private void CloneDie(DieViewModel viewModel)
        {
            if (viewModel.EditMode != ViewModelEditMode.Edit)
            {
                return;
            }

            var cloneManager = new CloneManager();

            if (viewModel.DieParent != null)
            {
                var parentPart = viewModel.DieParent as Part;
                if (parentPart != null)
                {
                    if (parentPart.Assembly == null)
                    {
                        viewModel.ModelParentClone = cloneManager.Clone(parentPart, false, true);
                        if (viewModel.ModelParentClone != null)
                        {
                            var parentStep = ((Part)viewModel.ModelParentClone).Process.Steps.FirstOrDefault(ps => ps.Guid == viewModel.Model.ProcessStep.Guid);
                            if (parentStep != null)
                            {
                                viewModel.ModelClone = parentStep.Dies.FirstOrDefault(c => c.Guid == viewModel.Model.Guid);
                            }
                        }
                    }
                    else
                    {
                        var topAssembly = EntityUtils.GetTopAssembly(parentPart.Assembly);
                        if (topAssembly != null)
                        {
                            var modelTopParentClone = cloneManager.Clone(topAssembly, false, true);
                            if (modelTopParentClone != null)
                            {
                                var parentAssembly = EntityUtils.GetSubAssembly(modelTopParentClone, parentPart.Assembly.Guid);
                                if (parentAssembly != null)
                                {
                                    viewModel.ModelParentClone = parentAssembly.Parts.FirstOrDefault(p => p.Guid == parentPart.Guid);
                                    if (viewModel.ModelParentClone != null)
                                    {
                                        var parentStep = ((Part)viewModel.ModelParentClone).Process.Steps.FirstOrDefault(ps => ps.Guid == viewModel.Model.ProcessStep.Guid);
                                        if (parentStep != null)
                                        {
                                            viewModel.ModelClone = parentStep.Dies.FirstOrDefault(c => c.Guid == viewModel.Model.Guid);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    var parentAssembly = viewModel.DieParent as Assembly;
                    if (parentAssembly != null)
                    {
                        var topAssembly = EntityUtils.GetTopAssembly(parentAssembly);
                        if (topAssembly != null)
                        {
                            var modelTopParentClone = cloneManager.Clone(topAssembly, false, true);
                            if (modelTopParentClone != null)
                            {
                                viewModel.ModelParentClone = EntityUtils.GetSubAssembly(modelTopParentClone, parentAssembly.Guid);
                                if (viewModel.ModelParentClone != null)
                                {
                                    var parentStep = ((Assembly)viewModel.ModelParentClone).Process.Steps.FirstOrDefault(ps => ps.Guid == viewModel.Model.ProcessStep.Guid);
                                    if (parentStep != null)
                                    {
                                        viewModel.ModelClone = parentStep.Dies.FirstOrDefault(c => c.Guid == viewModel.Model.Guid);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Clones the machine model.
        /// </summary>
        /// <param name="viewModel">The Machine view-model.</param>
        private void CloneMachine(MachineViewModel viewModel)
        {
            if (viewModel.EditMode != ViewModelEditMode.Edit)
            {
                return;
            }

            var cloneManager = new CloneManager();

            if (viewModel.MachineParent != null)
            {
                var parentPart = viewModel.MachineParent as Part;
                if (parentPart != null)
                {
                    if (parentPart.Assembly == null)
                    {
                        viewModel.ModelParentClone = cloneManager.Clone(parentPart, false, true);
                        if (viewModel.ModelParentClone != null)
                        {
                            var parentStep = ((Part)viewModel.ModelParentClone).Process.Steps.FirstOrDefault(ps => ps.Guid == viewModel.Model.ProcessStep.Guid);
                            if (parentStep != null)
                            {
                                viewModel.ModelClone = parentStep.Machines.FirstOrDefault(c => c.Guid == viewModel.Model.Guid);
                            }
                        }
                    }
                    else
                    {
                        var topAssembly = EntityUtils.GetTopAssembly(parentPart.Assembly);
                        if (topAssembly != null)
                        {
                            var modelTopParentClone = cloneManager.Clone(topAssembly, false, true);
                            if (modelTopParentClone != null)
                            {
                                var parentAssembly = EntityUtils.GetSubAssembly(modelTopParentClone, parentPart.Assembly.Guid);
                                if (parentAssembly != null)
                                {
                                    viewModel.ModelParentClone = parentAssembly.Parts.FirstOrDefault(p => p.Guid == parentPart.Guid);
                                    if (viewModel.ModelParentClone != null)
                                    {
                                        var parentStep = ((Part)viewModel.ModelParentClone).Process.Steps.FirstOrDefault(ps => ps.Guid == viewModel.Model.ProcessStep.Guid);
                                        if (parentStep != null)
                                        {
                                            viewModel.ModelClone = parentStep.Machines.FirstOrDefault(c => c.Guid == viewModel.Model.Guid);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    var parentAssembly = viewModel.MachineParent as Assembly;
                    if (parentAssembly != null)
                    {
                        var topAssembly = EntityUtils.GetTopAssembly(parentAssembly);
                        if (topAssembly != null)
                        {
                            var modelTopParentClone = cloneManager.Clone(topAssembly, false, true);
                            if (modelTopParentClone != null)
                            {
                                viewModel.ModelParentClone = EntityUtils.GetSubAssembly(modelTopParentClone, parentAssembly.Guid);
                                if (viewModel.ModelParentClone != null)
                                {
                                    var parentStep = ((Assembly)viewModel.ModelParentClone).Process.Steps.FirstOrDefault(ps => ps.Guid == viewModel.Model.ProcessStep.Guid);
                                    if (parentStep != null)
                                    {
                                        viewModel.ModelClone = parentStep.Machines.FirstOrDefault(c => c.Guid == viewModel.Model.Guid);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Clones the raw-material model.
        /// </summary>
        /// <param name="viewModel">The RawMaterial view-model.</param>
        private void CloneRawMaterial(RawMaterialViewModel viewModel)
        {
            if (viewModel.EditMode != ViewModelEditMode.Edit || viewModel.Model.Part == null)
            {
                return;
            }

            var cloneManager = new CloneManager();

            if (viewModel.Model.Part.Assembly == null)
            {
                viewModel.ModelParentClone = cloneManager.Clone(viewModel.Model.Part, false, true);
                if (viewModel.ModelParentClone != null)
                {
                    viewModel.ModelClone = ((Part)viewModel.ModelParentClone).RawMaterials.FirstOrDefault(c => c.Guid == viewModel.Model.Guid);
                }
            }
            else if (viewModel.Model.Part != null)
            {
                var topAssembly = EntityUtils.GetTopAssembly(viewModel.Model.Part.Assembly);
                if (topAssembly != null)
                {
                    var modelTopParentClone = cloneManager.Clone(topAssembly, false, true);
                    if (modelTopParentClone != null)
                    {
                        var parentAssembly = EntityUtils.GetSubAssembly(modelTopParentClone, viewModel.Model.Part.Assembly.Guid);
                        if (parentAssembly != null)
                        {
                            viewModel.ModelParentClone = parentAssembly.Parts.FirstOrDefault(p => p.Guid == viewModel.Model.Part.Guid);
                            if (viewModel.ModelParentClone != null)
                            {
                                viewModel.ModelClone = ((Part)viewModel.ModelParentClone).RawMaterials.FirstOrDefault(p => p.Guid == viewModel.Model.Guid);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Clones the process.
        /// </summary>
        /// <param name="viewModel">The Process view-model.</param>
        private void CloneProcess(ProcessViewModel viewModel)
        {
            if (viewModel.EditMode != ViewModelEditMode.Edit)
            {
                return;
            }

            var cloneManager = new CloneManager();

            if (viewModel.Parent != null)
            {
                var parentPart = viewModel.Parent as Part;
                if (parentPart != null)
                {
                    if (parentPart.Assembly == null)
                    {
                        viewModel.ModelParentClone = cloneManager.Clone(parentPart, false, true);
                        if (viewModel.ModelParentClone != null)
                        {
                            var parentProcess = ((Part)viewModel.ModelParentClone).Process;
                            if (parentProcess != null)
                            {
                                viewModel.ModelClone = parentProcess;
                            }
                        }
                    }
                    else
                    {
                        var topAssembly = EntityUtils.GetTopAssembly(parentPart.Assembly);
                        if (topAssembly != null)
                        {
                            var modelTopParentClone = cloneManager.Clone(topAssembly, false, true);
                            if (modelTopParentClone != null)
                            {
                                var parentAssembly = EntityUtils.GetSubAssembly(modelTopParentClone, parentPart.Assembly.Guid);
                                if (parentAssembly != null)
                                {
                                    viewModel.ModelParentClone = parentAssembly.Parts.FirstOrDefault(p => p.Guid == parentPart.Guid);
                                    if (viewModel.ModelParentClone != null)
                                    {
                                        var parentProcess = ((Part)viewModel.ModelParentClone).Process;
                                        if (parentProcess != null)
                                        {
                                            viewModel.ModelClone = parentProcess;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    var parentAssembly = viewModel.Parent as Assembly;
                    if (parentAssembly != null)
                    {
                        var topAssembly = EntityUtils.GetTopAssembly(parentAssembly);
                        if (topAssembly != null)
                        {
                            var modelTopParentClone = cloneManager.Clone(topAssembly, false, true);
                            if (modelTopParentClone != null)
                            {
                                viewModel.ModelParentClone = EntityUtils.GetSubAssembly(modelTopParentClone, parentAssembly.Guid);
                                if (viewModel.ModelParentClone != null)
                                {
                                    var parentProcess = ((Assembly)viewModel.ModelParentClone).Process;
                                    if (parentProcess != null)
                                    {
                                        viewModel.ModelClone = parentProcess;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Clones the process step model.
        /// </summary>
        /// <param name="viewModel">The ProcessStep view-model.</param>
        private void CloneProcessStep(ProcessStepViewModel viewModel)
        {
            if (viewModel.EditMode != ViewModelEditMode.Edit)
            {
                return;
            }

            var cloneManager = new CloneManager();

            if (viewModel.ProcessParent != null)
            {
                var parentPart = viewModel.ProcessParent as Part;
                if (parentPart != null)
                {
                    if (parentPart.Assembly == null)
                    {
                        viewModel.ModelParentClone = cloneManager.Clone(parentPart, false, true);
                        if (viewModel.ModelParentClone != null)
                        {
                            var parentStep = ((Part)viewModel.ModelParentClone).Process.Steps.FirstOrDefault(ps => ps.Guid == viewModel.Model.Guid);
                            if (parentStep != null)
                            {
                                viewModel.ModelClone = parentStep;
                            }
                        }
                    }
                    else
                    {
                        var topAssembly = EntityUtils.GetTopAssembly(parentPart.Assembly);
                        if (topAssembly != null)
                        {
                            var modelTopParentClone = cloneManager.Clone(topAssembly, false, true);
                            if (modelTopParentClone != null)
                            {
                                var parentAssembly = EntityUtils.GetSubAssembly(modelTopParentClone, parentPart.Assembly.Guid);
                                if (parentAssembly != null)
                                {
                                    viewModel.ModelParentClone = parentAssembly.Parts.FirstOrDefault(p => p.Guid == parentPart.Guid);
                                    if (viewModel.ModelParentClone != null)
                                    {
                                        var parentStep = ((Part)viewModel.ModelParentClone).Process.Steps.FirstOrDefault(ps => ps.Guid == viewModel.Model.Guid);
                                        if (parentStep != null)
                                        {
                                            viewModel.ModelClone = parentStep;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    var parentAssembly = viewModel.ProcessParent as Assembly;
                    if (parentAssembly != null)
                    {
                        var topAssembly = EntityUtils.GetTopAssembly(parentAssembly);
                        if (topAssembly != null)
                        {
                            var modelTopParentClone = cloneManager.Clone(topAssembly, false, true);
                            if (modelTopParentClone != null)
                            {
                                viewModel.ModelParentClone = EntityUtils.GetSubAssembly(modelTopParentClone, parentAssembly.Guid);
                                if (viewModel.ModelParentClone != null)
                                {
                                    var parentStep = ((Assembly)viewModel.ModelParentClone).Process.Steps.FirstOrDefault(ps => ps.Guid == viewModel.Model.Guid);
                                    if (parentStep != null)
                                    {
                                        viewModel.ModelClone = parentStep;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Clones the process step model.
        /// </summary>
        /// <param name="viewModel">The ProcessStepEditor view-model.</param>
        private void CloneProcessStepFromEditor(ProcessStepEditorViewModel viewModel)
        {
            if (viewModel.EditMode != ViewModelEditMode.Edit)
            {
                return;
            }

            var cloneManager = new CloneManager();

            if (viewModel.ProcessParent != null)
            {
                var parentPart = viewModel.ProcessParent as Part;
                if (parentPart != null)
                {
                    if (parentPart.Assembly == null)
                    {
                        viewModel.ModelParentClone = cloneManager.Clone(parentPart, false, true);
                        if (viewModel.ModelParentClone != null)
                        {
                            var parentStep = ((Part)viewModel.ModelParentClone).Process.Steps.FirstOrDefault(ps => ps.Guid == viewModel.Model.Guid);
                            if (parentStep != null)
                            {
                                viewModel.ModelClone = parentStep;
                            }
                        }
                    }
                    else
                    {
                        var topAssembly = EntityUtils.GetTopAssembly(parentPart.Assembly);
                        if (topAssembly != null)
                        {
                            var modelTopParentClone = cloneManager.Clone(topAssembly, false, true);
                            if (modelTopParentClone != null)
                            {
                                var parentAssembly = EntityUtils.GetSubAssembly(modelTopParentClone, parentPart.Assembly.Guid);
                                if (parentAssembly != null)
                                {
                                    viewModel.ModelParentClone = parentAssembly.Parts.FirstOrDefault(p => p.Guid == parentPart.Guid);
                                    if (viewModel.ModelParentClone != null)
                                    {
                                        var parentStep = ((Part)viewModel.ModelParentClone).Process.Steps.FirstOrDefault(ps => ps.Guid == viewModel.Model.Guid);
                                        if (parentStep != null)
                                        {
                                            viewModel.ModelClone = parentStep;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    var parentAssembly = viewModel.ProcessParent as Assembly;
                    if (parentAssembly != null)
                    {
                        var topAssembly = EntityUtils.GetTopAssembly(parentAssembly);
                        if (topAssembly != null)
                        {
                            var modelTopParentClone = cloneManager.Clone(topAssembly, false, true);
                            if (modelTopParentClone != null)
                            {
                                viewModel.ModelParentClone = EntityUtils.GetSubAssembly(modelTopParentClone, parentAssembly.Guid);
                                if (viewModel.ModelParentClone != null)
                                {
                                    var parentStep = ((Assembly)viewModel.ModelParentClone).Process.Steps.FirstOrDefault(ps => ps.Guid == viewModel.Model.Guid);
                                    if (parentStep != null)
                                    {
                                        viewModel.ModelClone = parentStep;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        #endregion Private Methods
    }
}