﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Timers;
using ZPKTool.Business;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Services
{
    /// <summary>
    /// Implementation of <see cref="IProjectsExplorerDataService"/>.
    /// </summary>
    [Export(typeof(IProjectsExplorerDataService))]
    public class ProjectsExplorerDataService : IProjectsExplorerDataService
    {
        #region Fields

        /// <summary>
        /// The explorer data release timeout.
        /// The default is to 2 minutes.
        /// </summary>
        public const double ExplorerDataReleaseTimeout = 120000;

        /// <summary>
        /// The cached items.
        /// The key represents an entity id and the value the data manager where is loaded.
        /// </summary>
        private Dictionary<Guid, WeakReference> cachedDataManagers = new Dictionary<Guid, WeakReference>();

        /// <summary>
        /// The data release timer.
        /// </summary>
        private Timer dataReleaseTimer;

        /// <summary>
        /// The messenger service.
        /// </summary>
        private IMessenger messenger;

        /// <summary>
        /// The data release providers.
        /// </summary>
        private HashSet<IExplorerDataReleaseProvider> dataReleaseProviders = new HashSet<IExplorerDataReleaseProvider>();

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectsExplorerDataService"/> class.
        /// </summary>
        /// <param name="messenger">The messenger.</param>
        [ImportingConstructor]
        public ProjectsExplorerDataService(IMessenger messenger)
        {
            this.dataReleaseTimer = new Timer(ExplorerDataReleaseTimeout);
            this.dataReleaseTimer.Enabled = true;
            this.dataReleaseTimer.Elapsed += DataReleaseTimer_Elapsed;

            this.messenger = messenger;
            this.messenger.Register<EntityChangedMessage>(this.HandleEntityChangedMessage);
            this.messenger.Register<EntitiesChangedMessage>(this.HandleEntitiesChangedMessage);
        }

        #endregion Constructors

        #region Provider Registration

        /// <summary>
        /// Registers the provider.
        /// </summary>
        /// <param name="provider">The provider to register.</param>
        public void RegisterProvider(IExplorerDataReleaseProvider provider)
        {
            this.dataReleaseProviders.Add(provider);
        }

        /// <summary>
        /// Unregisters the provider.
        /// </summary>
        /// <param name="provider">The provider to unregister.</param>
        public void UnregisterProvider(IExplorerDataReleaseProvider provider)
        {
            this.dataReleaseProviders.Remove(provider);

            // If the last provider was removed clear the data managers cache.
            if (!this.dataReleaseProviders.Any())
            {
                this.cachedDataManagers.Clear();
            }
        }

        #endregion Provider Registration

        #region Data Release

        /// <summary>
        /// Releases the data that is no longer used.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="ElapsedEventArgs"/> instance containing the event data.</param>
        private void DataReleaseTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            var dataManagersForPotentialRelease = new List<IDataSourceManager>();
            var usedDataManagers = new List<IDataSourceManager>();

            // Analyze each data provider in order to determine the releasable data managers.
            foreach (var provider in this.dataReleaseProviders)
            {
                var releaseData = provider.AnalyzeDataForRelease();
                dataManagersForPotentialRelease.AddRange(releaseData.ReleasableDataManagers);
                usedDataManagers.AddRange(releaseData.UnreleasableDataManagers);
            }

            var releasableDataManagers = dataManagersForPotentialRelease.Where(m => !usedDataManagers.Contains(m));

            // Release the specified data managers.
            foreach (var provider in this.dataReleaseProviders)
            {
                provider.ReleaseData(releasableDataManagers);
            }

            // Remove the released data managers from the cache.
            var itemsFromCacheToRemove = this.cachedDataManagers.Where(m => releasableDataManagers.Contains(m.Value.Target)).ToList();
            foreach (var item in itemsFromCacheToRemove)
            {
                this.cachedDataManagers.Remove(item.Key);
            }

            this.CleanCache();
        }

        /// <summary>
        /// Releases data where the specified data manager is used.
        /// </summary>
        /// <param name="dataManager">The data manager.</param>
        public void ReleaseData(IDataSourceManager dataManager)
        {
            foreach (var provider in this.dataReleaseProviders)
            {
                provider.ReleaseData(new List<IDataSourceManager>() { dataManager });
            }

            // Remove the released data manager from the cache.
            var itemToRemove = this.cachedDataManagers.FirstOrDefault(m => m.Value == dataManager);
            this.cachedDataManagers.Remove(itemToRemove.Key);

            this.CleanCache();
        }

        #endregion Data Release

        #region Get Data

        /// <summary>
        /// Gets the released projects.
        /// </summary>
        /// <param name="databaseId">The database id.</param>
        /// <param name="folders">The released root folders.</param>
        /// <param name="projects">The released projects without parent.</param>
        /// <returns>The released folders, projects and the data manager that was used to retrieve them.</returns>
        public ExplorerData<object> GetReleasedProjects(DbIdentifier databaseId, out ICollection<ProjectFolder> folders, out ICollection<Project> projects)
        {
            var dataManager = DataAccessFactory.CreateDataSourceManager(databaseId);

            folders = dataManager.ProjectFolderRepository.GetReleasedRootFolders();
            projects = dataManager.ProjectRepository.GetReleasedProjectsForProjectsTree(Guid.Empty);

            return new ExplorerData<object>(null, dataManager);
        }

        /// <summary>
        /// Gets the projects and root folders.
        /// </summary>
        /// <param name="databaseId">The database id.</param>
        /// <param name="folders">The root folders.</param>
        /// <param name="projects">The projects without parent.</param>
        /// <returns>The projects, root folders and the data manager that was used to retrieve them.</returns>
        public ExplorerData<object> GetMyProjects(DbIdentifier databaseId, out ICollection<ProjectFolder> folders, out ICollection<Project> projects)
        {
            var dataManager = DataAccessFactory.CreateDataSourceManager(databaseId);

            folders = dataManager.ProjectFolderRepository.GetRootFolders(SecurityManager.Instance.CurrentUser.Guid);
            projects = dataManager.ProjectRepository.GetProjectsForProjectsTree(SecurityManager.Instance.CurrentUser.Guid, Guid.Empty);

            return new ExplorerData<object>(null, dataManager);
        }

        /// <summary>
        /// Gets the other users.
        /// </summary>
        /// <param name="databaseId">The database id.</param>
        /// <returns>The other users and the data manager that was used to retrieve them.</returns>
        public ExplorerData<User> GetOtherUsers(DbIdentifier databaseId)
        {
            var dataManager = DataAccessFactory.CreateDataSourceManager(databaseId);

            Collection<User> users;
            if (SecurityManager.Instance.CurrentUserHasRole(Role.Admin))
            {
                users = dataManager.UserRepository.GetAll(false, true);
            }
            else
            {
                users =
                    dataManager.UserRepository.GetUsersWithProjectsVisibleToOtherUser(
                        SecurityManager.Instance.CurrentUser.Guid);
            }

            // Remove the current user from the list.
            users.Remove(users.FirstOrDefault(u => u.Guid == SecurityManager.Instance.CurrentUser.Guid));
            return new ExplorerData<User>(users, dataManager);
        }

        /// <summary>
        /// Gets the folder from a cached data manager if exists or else from the database.
        /// </summary>
        /// <param name="folderId">The id of the folder to retrieve.</param>
        /// <param name="databaseId">The database id.</param>
        /// <returns>A folder and the data manager that was used to retrieve it.</returns>
        public ExplorerData<ProjectFolder> GetFolder(Guid folderId, DbIdentifier databaseId)
        {
            this.CleanCache();

            IDataSourceManager dataManager = null;
            WeakReference cachedManager = null;

            ProjectFolder folder = null;

            // Search the folder into the cache.
            // If is not cached load it from the database.
            this.cachedDataManagers.TryGetValue(folderId, out cachedManager);
            if (cachedManager != null
                && cachedManager.IsAlive
                && cachedManager.Target != null)
            {
                dataManager = (IDataSourceManager)cachedManager.Target;
            }
            else
            {
                // Get the underlying folder using the new data context so the folder instance and its sub-folders and projects retrieved below share the same data context.
                dataManager = DataAccessFactory.CreateDataSourceManager(databaseId);
                this.cachedDataManagers.Add(folderId, new WeakReference(dataManager));
                folder = dataManager.ProjectFolderRepository.GetFolderWithParent(folderId);
                if (folder != null)
                {
                    if (folder.IsReleased)
                    {
                        dataManager.ProjectFolderRepository.GetReleasedSubFolders(folderId);
                        dataManager.ProjectRepository.GetReleasedProjectsForProjectsTree(folderId);
                    }
                    else
                    {
                        dataManager.ProjectFolderRepository.GetSubFolders(folderId, folder.Owner.Guid);
                        dataManager.ProjectRepository.GetProjectsForProjectsTree(folder.Owner.Guid, folderId);
                    }
                }
            }

            folder = dataManager.ProjectFolderRepository.GetByKey(folderId);

            return new ExplorerData<ProjectFolder>(new List<ProjectFolder>() { folder }, dataManager);
        }

        /// <summary>
        /// Gets the project from a cached data manager if exists or else from the database.
        /// </summary>
        /// <param name="projectId">The id of the project to retrieve.</param>
        /// <param name="databaseId">The database id.</param>
        /// <returns>A project and the data manager used to retrieve it.</returns>
        public ExplorerData<Project> GetProject(Guid projectId, DbIdentifier databaseId)
        {
            this.CleanCache();

            IDataSourceManager dataManager = null;
            WeakReference cachedManager = null;

            // Search the project into the cache.
            // If is not cached load it from the database.
            this.cachedDataManagers.TryGetValue(projectId, out cachedManager);
            if (cachedManager != null
                && cachedManager.IsAlive
                && cachedManager.Target != null)
            {
                dataManager = (IDataSourceManager)cachedManager.Target;
            }
            else
            {
                dataManager = DataAccessFactory.CreateDataSourceManager(databaseId);
                this.cachedDataManagers.Add(projectId, new WeakReference(dataManager));
                dataManager.ProjectRepository.GetProjectIncludingTopLevelChildren(projectId);
            }

            var project = dataManager.ProjectRepository.GetByKey(projectId);

            return new ExplorerData<Project>(new List<Project>() { project }, dataManager);
        }

        /// <summary>
        /// Gets the assembly from a cached data manager if exists or else from the database.
        /// </summary>
        /// <param name="assemblyId">The id of the assembly to retrieve.</param>
        /// <param name="databaseId">The database id.</param>
        /// <returns>An assembly and the data manager used to retrieve it.</returns>
        public ExplorerData<Assembly> GetAssembly(Guid assemblyId, DbIdentifier databaseId)
        {
            this.CleanCache();

            IDataSourceManager dataManager = null;
            WeakReference cachedDataManager = null;

            // Search the assembly into the cache.
            // If is not cached get its top parent assembly and see if that one is cached.
            // If is not, fully load the top parent assembly from the db and cache it.
            this.cachedDataManagers.TryGetValue(assemblyId, out cachedDataManager);
            if (cachedDataManager != null
                && cachedDataManager.IsAlive
                && cachedDataManager.Target != null)
            {
                dataManager = (IDataSourceManager)cachedDataManager.Target;
            }
            else
            {
                dataManager = DataAccessFactory.CreateDataSourceManager(databaseId);
                var topParentId = dataManager.AssemblyRepository.GetTopParentAssemblyId(assemblyId);

                this.cachedDataManagers.TryGetValue(topParentId, out cachedDataManager);
                if (cachedDataManager != null
                    && cachedDataManager.IsAlive
                    && cachedDataManager.Target != null)
                {
                    dataManager = (IDataSourceManager)cachedDataManager.Target;
                }
                else
                {
                    this.cachedDataManagers.Add(topParentId, new WeakReference(dataManager));
                    dataManager.AssemblyRepository.GetAssemblyFull(topParentId);
                }
            }

            var assembly = dataManager.AssemblyRepository.GetByKey(assemblyId);

            return new ExplorerData<Assembly>(new List<Assembly>() { assembly }, dataManager);
        }

        /// <summary>
        /// Gets the part from a cached data manager if exists or else from the database.
        /// </summary>
        /// <param name="partId">The id of the part to retrieve.</param>
        /// <param name="databaseId">The database id.</param>
        /// <returns>A part and the data manager used to retrieve it.</returns>
        public ExplorerData<Part> GetPart(Guid partId, DbIdentifier databaseId)
        {
            this.CleanCache();

            IDataSourceManager dataManager = null;
            WeakReference cachedDataManager = null;

            // Search the part into the cache.
            // If is not cached get its top parent assembly or part and see if its cached.
            // If is not, fully load the top parent from the db and cache it.
            this.cachedDataManagers.TryGetValue(partId, out cachedDataManager);
            if (cachedDataManager != null
                && cachedDataManager.IsAlive
                && cachedDataManager.Target != null)
            {
                dataManager = (IDataSourceManager)cachedDataManager.Target;
            }
            else
            {
                dataManager = DataAccessFactory.CreateDataSourceManager(databaseId);

                var associatedId = Guid.Empty;
                var topParentAssembly = dataManager.AssemblyRepository.GetTopParentAssembly(new Part() { Guid = partId });
                if (topParentAssembly != null)
                {
                    associatedId = topParentAssembly.Guid;
                }
                else
                {
                    associatedId = dataManager.PartRepository.GetParentOfRawPartId(partId);
                    if (associatedId == Guid.Empty)
                    {
                        associatedId = partId;
                    }
                }

                WeakReference cachedManager = null;
                this.cachedDataManagers.TryGetValue(associatedId, out cachedManager);
                if (cachedManager != null
                    && cachedManager.IsAlive
                    && cachedManager.Target != null)
                {
                    dataManager = (IDataSourceManager)cachedManager.Target;
                }
                else
                {
                    this.cachedDataManagers.Add(associatedId, new WeakReference(dataManager));

                    if (topParentAssembly != null)
                    {
                        dataManager.AssemblyRepository.GetAssemblyFull(topParentAssembly.Guid);
                    }
                    else
                    {
                        dataManager.PartRepository.GetPartFull(associatedId);
                    }
                }
            }

            var part = dataManager.PartRepository.GetByKey(partId);

            return new ExplorerData<Part>(new List<Part>() { part }, dataManager);
        }

        /// <summary>
        /// Gets the bookmarked projects.
        /// </summary>
        /// <param name="databaseId">The database id.</param>
        /// <returns>The bookmarked projects and the data manager used to retrieve them.</returns>
        public ExplorerData<Project> GetBookmarkedProjects(DbIdentifier databaseId)
        {
            var dataManager = DataAccessFactory.CreateDataSourceManager(databaseId);
            var projects = dataManager.ProjectRepository.GetBookmarkedProjects(SecurityManager.Instance.CurrentUser.Guid);

            return new ExplorerData<Project>(projects, dataManager);
        }

        /// <summary>
        /// Gets the bookmarked assemblies.
        /// </summary>
        /// <param name="databaseId">The database id.</param>
        /// <returns>The bookmarked assemblies and the data manager used to retrieve them.</returns>
        public ExplorerData<Assembly> GetBookmarkedAssemblies(DbIdentifier databaseId)
        {
            var dataManager = DataAccessFactory.CreateDataSourceManager(databaseId);
            var assemblies = dataManager.AssemblyRepository.GetBookmarkedAssemblies(SecurityManager.Instance.CurrentUser.Guid);

            return new ExplorerData<Assembly>(assemblies, dataManager);
        }

        /// <summary>
        /// Gets the bookmarked parts.
        /// </summary>
        /// <param name="databaseId">The database id.</param>
        /// <returns>The bookmarked parts and the data manager used to retrieve them.</returns>
        public ExplorerData<Part> GetBookmarkedParts(DbIdentifier databaseId)
        {
            var dataManager = DataAccessFactory.CreateDataSourceManager(databaseId);
            var parts = dataManager.PartRepository.GetBookmarkedParts(SecurityManager.Instance.CurrentUser.Guid);

            return new ExplorerData<Part>(parts, dataManager);
        }

        #endregion Get Data

        /// <summary>
        /// Cleans the data managers cache.
        /// </summary>
        private void CleanCache()
        {
            var itemsToRemove = new List<Guid>();
            foreach (var item in this.cachedDataManagers)
            {
                if (!item.Value.IsAlive
                    || (item.Value.IsAlive && item.Value.Target == null))
                {
                    itemsToRemove.Add(item.Key);
                }
            }

            foreach (var item in itemsToRemove)
            {
                this.cachedDataManagers.Remove(item);
            }
        }

        #region EntityChangedMessage handling

        /// <summary>
        /// Handles the entity change messages.
        /// </summary>
        /// <param name="message">The message.</param>        
        private void HandleEntityChangedMessage(EntityChangedMessage message)
        {
            // If an entity was deleted we need remove from the cache the associated entry.
            if (message.ChangeType == EntityChangeType.EntityDeleted
                || message.ChangeType == EntityChangeType.EntityPermanentlyDeleted)
            {
                var identifiableEntity = message.Entity as IIdentifiable;
                if (identifiableEntity != null)
                {
                    if (this.cachedDataManagers.ContainsKey(identifiableEntity.Guid))
                    {
                        this.cachedDataManagers.Remove(identifiableEntity.Guid);
                    }
                }
            }
        }

        /// <summary>
        /// Handles the entities change messages.
        /// </summary>
        /// <param name="message">The message.</param>  
        private void HandleEntitiesChangedMessage(EntitiesChangedMessage message)
        {
            if (message == null)
            {
                return;
            }

            foreach (var msgChange in message.Changes)
            {
                this.HandleEntityChangedMessage(msgChange);
            }
        }

        #endregion EntityChangedMessage handling
    }
}