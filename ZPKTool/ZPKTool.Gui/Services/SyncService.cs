﻿namespace ZPKTool.Gui.Services
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.Composition;
    using System.Threading;
    using System.Threading.Tasks;
    using ZPKTool.Business;
    using ZPKTool.Common;
    using ZPKTool.Gui.Notifications;
    using ZPKTool.MvvmCore.Services;
    using ZPKTool.Synchronization;

    /// <summary>
    /// Implementation of ISyncService which uses Synchronization Manager to synchronize current user data.
    /// </summary>
    [Export(typeof(ISyncService))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class SyncService : ISyncService
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Weak event listener for the synchronization progress notification.
        /// The synchronization progress listener is used in order 
        /// to update the progress of the synchronization.
        /// </summary>
        private static WeakEventListener<SynchronizationProgressEventArgs> syncProgressListener;

        /// <summary>
        /// The messenger service.
        /// </summary>
        private readonly IMessenger messenger;

        /// <summary>
        /// The window service.
        /// </summary>
        private readonly IWindowService windowService;

        /// <summary>
        /// The timer used to handle synchronization postpone.
        /// </summary>
        private Timer syncDelayTimer;

        #endregion Attributes

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SyncService"/> class.
        /// </summary>
        /// <param name="messenger">The messenger.</param>
        /// <param name="windowService">The window service.</param>
        [ImportingConstructor]
        public SyncService(IMessenger messenger, IWindowService windowService)
        {
            this.messenger = messenger;
            this.windowService = windowService;

            syncProgressListener =
                new WeakEventListener<SynchronizationProgressEventArgs>(SynchronizationProgressHandler);
            SynchronizationProgressEventManager.AddListener(this.SyncManager, syncProgressListener);
        }

        #endregion Constructor

        #region Properties

        /// <summary>
        /// Gets the synchronization manager.
        /// </summary>
        public ISynchronizationManager SyncManager
        {
            get
            {
                return SynchronizationFactory.GetSyncManager();
            }
        }

        #endregion Properties

        #region Public methods

        /// <summary>
        /// Starts the synchronization.
        /// </summary>
        /// <param name="userGuid">The user unique identifier.</param>
        /// <param name="resolveConflicts">if set to true, this call will resolve
        /// any conflicts that appeared in previous sync runs.</param>
        public void StartSync(Guid userGuid, bool resolveConflicts = false)
        {
            /*
             * If the delay timer has been started, stop it.
             */
            if (this.syncDelayTimer != null)
            {
                this.syncDelayTimer.Change(Timeout.Infinite, Timeout.Infinite);
            }

            this.SynchronizeAsync(userGuid, resolveConflicts);
        }

        /// <summary>
        /// Postpones the synchronization start.
        /// </summary>
        /// <param name="userGuid">The user unique identifier.</param>
        /// <param name="minutesDelay">The minutes delay.</param>
        public void PostponeSync(Guid userGuid, int minutesDelay)
        {
            if (this.syncDelayTimer != null)
            {
                /*
                 * If the delay timer has been already started, update the due time.
                 */
                this.syncDelayTimer.Change(60000 * minutesDelay, Timeout.Infinite);
            }
            else
            {
                /*
                 * Start the synchronization timer.
                 */
                this.syncDelayTimer = new Timer(
                    obj => this.SynchronizeAsync(userGuid),
                    null,
                    TimeSpan.FromMinutes(minutesDelay),
                    TimeSpan.FromMilliseconds(-1));
            }
        }

        /// <summary>
        /// Cancel the execution of scheduled synchronization.
        /// </summary>
        public void CancelSyncPostponement()
        {
            if (this.syncDelayTimer != null)
            {
                this.syncDelayTimer.Change(Timeout.Infinite, Timeout.Infinite);
            }
        }

        #endregion Public methods

        #region Private methods

        /// <summary>
        /// Synchronizes the asynchronous.
        /// </summary>
        /// <param name="userGuid">The user unique identifier.</param>
        /// <param name="resolveConflicts">if set to true, this call will resolve any conflicts
        /// that appeared in previous sync runs.</param>
        private void SynchronizeAsync(Guid userGuid, bool resolveConflicts = false)
        {
            /*
             * Signal that the synchronization has started.
             */
            this.messenger.Send(new SynchronizationStartedMessage());

            /*
             * Run the synchronization process asynchronously.
             */
            if (this.SyncManager.CurrentStatus.State != SyncState.Running &&
                this.SyncManager.CurrentStatus.State != SyncState.Aborting)
            {
                Task.Factory.StartNew(() => this.SyncManager.Synchronize(
                    false, false, false, true, userGuid, resolveConflicts))
                    .ContinueWith((task) =>
                {
                    if (task.Exception != null)
                    {
                        var taskError = task.Exception.InnerException;
                        Log.ErrorException("An error occurred during synchronization.", taskError);
                    }
                });
            }
        }

        /// <summary>
        /// Handles the synchronization progress.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="eventArgs">The <see><cref>ZPKTool.Business.Managers.SynchronizationProgressEventArgs</cref></see>
        /// instance containing the event data.</param>
        private void SynchronizationProgressHandler(object sender, SynchronizationProgressEventArgs eventArgs)
        {
            if (eventArgs.CurrentTask == SynchronizationTask.CompletedAllTasks)
            {
                this.NotifySyncFinished();
            }
        }

        /// <summary>
        /// Signal that the synchronization has finished.
        /// </summary>
        private void NotifySyncFinished()
        {
            /*
             * TODO: As an optimization it would be nice to signal 
             * only the tasks for which content was actually changed
             */
            var completedTasks = new List<SynchronizationTask>();
            if (this.SyncManager.CurrentStatus.SyncReleasedProjects)
            {
                completedTasks.Add(SynchronizationTask.ReleasedProjects);
            }

            if (this.SyncManager.CurrentStatus.SyncMyProjects)
            {
                completedTasks.Add(SynchronizationTask.MyProjects);
                if (!this.SyncManager.CurrentStatus.SyncStaticDataAndSettings)
                {
                    completedTasks.Add(SynchronizationTask.StaticDataAndSettings);
                }
            }

            if (this.SyncManager.CurrentStatus.SyncStaticDataAndSettings)
            {
                LocalDataCache.Refresh();
                DbGlobalSettingsManager.ClearCache();
                completedTasks.Add(SynchronizationTask.StaticDataAndSettings);
            }

            if (completedTasks.Contains(SynchronizationTask.StaticDataAndSettings))
            {
                SecurityManager.Instance.RefreshCurrentUser();
            }

            this.messenger.Send(new SynchronizationFinishedMessage(completedTasks));
        }

        #endregion Private methods
    }
}
