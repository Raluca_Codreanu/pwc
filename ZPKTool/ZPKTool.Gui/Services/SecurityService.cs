﻿using System.ComponentModel.Composition;
using ZPKTool.Business;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Resources;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Services
{
    /// <summary>
    /// Implementation of ISecurityService that checks the user access rights.
    /// </summary>
    [Export(typeof(ISecurityService))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class SecurityService : ISecurityService
    {
        /// <summary>
        /// The window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The online checker.
        /// </summary>
        private IOnlineCheckService onlineChecker;

        /// <summary>
        /// The user role from the central database.
        /// </summary>
        private Role? userRoleOnCentral;

        /// <summary>
        /// Gets the user role from the central database.
        /// </summary>
        private Role UserRoleOnCentral
        {
            get
            {
                if (!this.userRoleOnCentral.HasValue
                    && this.onlineChecker.IsOnline)
                {
                    using (var dm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase))
                    {
                        var user = dm.UserRepository.GetByUsername(SecurityManager.Instance.CurrentUser.Username, false, true);
                        this.userRoleOnCentral = user != null && RoleRights.HasRole(user.Roles, SecurityManager.Instance.CurrentUserRole) ? SecurityManager.Instance.CurrentUserRole : Role.None;
                    }
                }

                return this.userRoleOnCentral.GetValueOrDefault();
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SecurityService"/> class.
        /// </summary>
        /// <param name="windowService">The window service.</param>
        /// <param name="onlineService">The online service.</param>
        [ImportingConstructor]
        public SecurityService(IWindowService windowService, IOnlineCheckService onlineService)
        {
            this.windowService = windowService;
            this.onlineChecker = onlineService;
        }

        /// <summary>
        /// Checks if the logged in user has the specified user right on the central database.
        /// </summary>
        /// <param name="userRight">The user right.</param>
        /// <returns>True if the current user has the specified right, false otherwise.</returns>
        public bool CheckUserForRights(Right userRight)
        {
            var hasRights = RoleRights.RoleHasRight(this.UserRoleOnCentral, userRight);
            if (!hasRights)
            {
                LoggingManager.Log(LogEventType.UnauthorizedAccess, string.Empty);
                this.windowService.MessageDialogService.Show(LocalizedResources.Error_InvalidAccessRight, MessageDialogType.Error);
            }

            return hasRights;
        }
    }
}