﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.DataAccess;

namespace ZPKTool.Gui.Services
{
    /// <summary>
    /// Holds the releasable explorer data.
    /// </summary>
    public class ReleasableExplorerData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReleasableExplorerData"/> class.
        /// </summary>
        public ReleasableExplorerData()
        {
            this.ReleasableDataManagers = new List<IDataSourceManager>();
            this.UnreleasableDataManagers = new List<IDataSourceManager>();
        }

        /// <summary>
        /// Gets the releasable data managers.
        /// </summary>
        public ICollection<IDataSourceManager> ReleasableDataManagers { get; private set; }

        /// <summary>
        /// Gets the unreleasable data managers.
        /// </summary>
        public ICollection<IDataSourceManager> UnreleasableDataManagers { get; private set; }
    }
}