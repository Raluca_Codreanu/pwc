﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Business.Export;
using ZPKTool.DataAccess;

namespace ZPKTool.Gui.Services
{
    /// <summary>
    /// The interface of a service that imports an entity from a file path
    /// </summary>
    public interface IImportService
    {
        /// <summary>
        /// Imports the entities from the specified file paths and shows a please wait message during importing process.
        /// </summary>
        /// <param name="filePaths">The paths to the files to import from.</param>
        /// <param name="parentEntity">The object that will be the parent of the imported objects.</param>
        /// <param name="dataSourceManager">The data source manager used for importing the entity.</param>
        /// <param name="saveChanges">If set to true, during import, data is saved in the database in batches in order to allow the memory used by the data to be reclaimed; otherwise, all data is imported into memory.</param>
        /// <param name="workCompleted">A delegate action to run after import is done.</param>
        void Import(
            IEnumerable<string> filePaths,
            object parentEntity,
            IDataSourceManager dataSourceManager,
            bool saveChanges,
            Action<PleaseWaitService.WorkParams, IEnumerable<ImportedData<object>>> workCompleted);

        /// <summary>
        /// Determines whether the calculation variant must be updated for the objects and receives the user feedback regarding the update action.
        /// </summary>
        /// <param name="target">The target.</param>
        /// <param name="objectsToUpdate">The objects for which the update action is checked.</param>
        /// <returns>True if the calculation version can be updated or false if not.</returns>  
        bool CanUpdateToRecipientCalculationVersion(object target, List<object> objectsToUpdate);
    }
}
