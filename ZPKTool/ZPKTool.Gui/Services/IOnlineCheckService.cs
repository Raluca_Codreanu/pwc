﻿namespace ZPKTool.Gui.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Business;

    /// <summary>
    /// The interface of a service that periodically checks if the central server is online.
    /// </summary>
    public interface IOnlineCheckService
    {
        /// <summary>
        /// Gets a value indicating whether the central server is online.
        /// </summary>
        bool IsOnline { get; }

        /// <summary>
        /// Gets the result of the last online check.
        /// </summary>
        DbOnlineCheckResult LastOnlineCheck { get; }

        /// <summary>
        /// Starts the service.
        /// </summary>
        /// <param name="interval">The interval at which the online check is performed, in milliseconds.
        /// Should be greater than 0 and less then int.MaxValue; if is not it defaults to 60000 (1 minute).</param>
        void Start(double interval);

        /// <summary>
        /// Checks the online state of the central server on the calling thread.
        /// </summary>
        /// <returns>True if the central server is online, false if is offline.</returns>
        bool CheckNow();

        /// <summary>
        /// Checks asynchronously the online state of the central server.
        /// </summary>
        void CheckAsync();
    }
}
