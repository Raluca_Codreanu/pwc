﻿using ZPKTool.Data;

namespace ZPKTool.Gui.Services
{
    /// <summary>
    /// The interface of a service that checks the user access rights.
    /// </summary>
    public interface ISecurityService
    {
        /// <summary>
        /// Checks if the logged in user has the specified user right on the central database.
        /// </summary>
        /// <param name="userRight">The user right.</param>
        /// <returns>True if the current user has the specified right, false otherwise.</returns>
        bool CheckUserForRights(Right userRight);
    }
}