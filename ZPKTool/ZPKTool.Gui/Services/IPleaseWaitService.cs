﻿using System;

namespace ZPKTool.Gui.Services
{
    /// <summary>
    /// The interface of a service that manage displaying a please wait window while certain operations are performed.
    /// </summary>
    public interface IPleaseWaitService
    {
        /// <summary>
        /// Gets or sets the message displayed during work operations are performed.
        /// </summary>
        string Message { get; set; }

        /// <summary>
        /// Displays the PleaseWaitWindow during work operations are performed.
        /// </summary>
        /// <param name="statusMessage">The message to display.</param>
        /// <param name="work">The action to be performed.</param>
        /// <param name="workCompleted">The action to be performed when the work is completed.</param>
        void Show(string statusMessage, Action<PleaseWaitService.WorkParams> work, Action<PleaseWaitService.WorkParams> workCompleted);

        /// <summary>
        /// Displays the PleaseWaitWindow during work operations are performed.
        /// </summary>
        /// <param name="statusMessage">The message to display.</param>
        /// <param name="displayDelay">The delay (in milliseconds) before displaying the wait widget.</param>
        /// <param name="work">The action to be performed.</param>
        /// <param name="workCompleted">The action to be performed when the work is completed.</param>
        void Show(string statusMessage, int displayDelay, Action<PleaseWaitService.WorkParams> work, Action<PleaseWaitService.WorkParams> workCompleted);
    }
}