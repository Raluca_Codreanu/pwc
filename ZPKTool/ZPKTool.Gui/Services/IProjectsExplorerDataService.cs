﻿using System;
using System.Collections.Generic;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;

namespace ZPKTool.Gui.Services
{
    /// <summary>
    /// An interface for a service that is used to retrieve and cache data.
    /// </summary>
    public interface IProjectsExplorerDataService
    {
        /// <summary>
        /// Registers the provider.
        /// </summary>
        /// <param name="provider">The provider to register.</param>
        void RegisterProvider(IExplorerDataReleaseProvider provider);

        /// <summary>
        /// Unregisters the provider.
        /// </summary>
        /// <param name="provider">The provider to unregister.</param>
        void UnregisterProvider(IExplorerDataReleaseProvider provider);

        /// <summary>
        /// Releases data where the specified data manager is used.
        /// </summary>
        /// <param name="dataManager">The data manager.</param>
        void ReleaseData(IDataSourceManager dataManager);

        /// <summary>
        /// Gets the released projects.
        /// </summary>
        /// <param name="databaseId">The database id.</param>
        /// <param name="folders">The released root folders.</param>
        /// <param name="projects">The released projects without parent.</param>
        /// <returns>The released folders, projects and the data manager that was used to retrieve them.</returns>
        ExplorerData<object> GetReleasedProjects(DbIdentifier databaseId, out ICollection<ProjectFolder> folders, out ICollection<Project> projects);

        /// <summary>
        /// Gets the projects and root folders.
        /// </summary>
        /// <param name="databaseId">The database id.</param>
        /// <param name="folders">The root folders.</param>
        /// <param name="projects">The projects without parent.</param>
        /// <returns>The projects, root folders and the data manager that was used to retrieve them.</returns>
        ExplorerData<object> GetMyProjects(DbIdentifier databaseId, out ICollection<ProjectFolder> folders, out ICollection<Project> projects);

        /// <summary>
        /// Gets the other users.
        /// </summary>
        /// <param name="databaseId">The database id.</param>
        /// <returns>The other users and the data manager that was used to retrieve them.</returns>
        ExplorerData<User> GetOtherUsers(DbIdentifier databaseId);

        /// <summary>
        /// Gets the folder from a cached data manager if exists or else from the database.
        /// </summary>
        /// <param name="folderId">The id of the folder to retrieve.</param>
        /// <param name="databaseId">The database id.</param>
        /// <returns>A folder and the data manager that was used to retrieve it.</returns>
        ExplorerData<ProjectFolder> GetFolder(Guid folderId, DbIdentifier databaseId);

        /// <summary>
        /// Gets the project from a cached data manager if exists or else from the database.
        /// </summary>
        /// <param name="projectId">The id of the project to retrieve.</param>
        /// <param name="databaseId">The database id.</param>
        /// <returns>A project and the data manager used to retrieve it.</returns>
        ExplorerData<Project> GetProject(Guid projectId, DbIdentifier databaseId);

        /// <summary>
        /// Gets the assembly from a cached data manager if exists or else from the database.
        /// </summary>
        /// <param name="assemblyId">The id of the assembly to retrieve.</param>
        /// <param name="databaseId">The database id.</param>
        /// <returns>An assembly and the data manager used to retrieve it.</returns>
        ExplorerData<Assembly> GetAssembly(Guid assemblyId, DbIdentifier databaseId);

        /// <summary>
        /// Gets the part from a cached data manager if exists or else from the database.
        /// </summary>
        /// <param name="partId">The id of the part to retrieve.</param>
        /// <param name="databaseId">The database id.</param>
        /// <returns>A part and the data manager used to retrieve it.</returns>
        ExplorerData<Part> GetPart(Guid partId, DbIdentifier databaseId);

        /// <summary>
        /// Gets the bookmarked projects.
        /// </summary>
        /// <param name="databaseId">The database id.</param>
        /// <returns>The bookmarked projects and the data manager used to retrieve them.</returns>
        ExplorerData<Project> GetBookmarkedProjects(DbIdentifier databaseId);

        /// <summary>
        /// Gets the bookmarked assemblies.
        /// </summary>
        /// <param name="databaseId">The database id.</param>
        /// <returns>The bookmarked assemblies and the data manager used to retrieve them.</returns>
        ExplorerData<Assembly> GetBookmarkedAssemblies(DbIdentifier databaseId);

        /// <summary>
        /// Gets the bookmarked parts.
        /// </summary>
        /// <param name="databaseId">The database id.</param>
        /// <returns>The bookmarked parts and the data manager used to retrieve them.</returns>
        ExplorerData<Part> GetBookmarkedParts(DbIdentifier databaseId);
    }
}