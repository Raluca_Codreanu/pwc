﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using ZPKTool.Business.Export;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Data;

namespace ZPKTool.Gui.Services
{
    /// <summary>
    /// The interface of a service which is used to provide various information (e.g. parent project, media) for the ModelBrowser.
    /// </summary>
    public interface IModelBrowserHelperService
    {
        /// <summary>
        /// Sets the data for the currently opened model into this instance.
        /// </summary>
        /// <param name="modelData">The model data.</param>
        /// <exception cref="System.ArgumentNullException">The model data was null.</exception>
        void SetCurrentModelData(ImportedData<object> modelData);

        /// <summary>
        /// Gets the media for entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// A list of imported media objects representing video or image which belong to the entity with the given ID, or an empty list if the given entity does not have any associated media.
        /// </returns>
        /// <exception cref="InvalidOperationException"> if the collection of media files is not initialized.</exception>
        IEnumerable<ImportedMedia> GetMediaForEntity(object entity);

        /// <summary>
        /// Gets the documents for entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// A list of imported media objects representing documents which belong to the entity with the given ID, or an empty list if the given entity does not have any associated media.
        /// </returns>
        /// <exception cref="InvalidOperationException"> if the collection of media files is not initialized.</exception>
        IEnumerable<ImportedMedia> GetDocumentsForEntity(object entity);

        /// <summary>
        /// Gets the cost calculation data stored by the service for the currently opened model as an instance of <see cref="PartCostCalculationParameters"/>,
        /// which is necessary for calculating the cost of a Part or Raw Part.
        /// </summary>
        /// <returns>The cost calculation parameters or null if the cost calculation data is not available (pre v1.3).</returns>
        PartCostCalculationParameters GetPartCostCalculationParameters();

        /// <summary>
        /// Gets the cost calculation data stored by the service for the currently opened model as an instance of <see cref="AssemblyCostCalculationParameters"/>,
        /// which is necessary for calculating the cost of an Assembly.
        /// </summary>
        /// <returns>The cost calculation parameters, or null if the cost calculation data is not available (pre v1.3).</returns>
        AssemblyCostCalculationParameters GetAssemblyCostCalculationParameters();
    }

    /// <summary>
    /// This service is used to provide various information (e.g. parent project, media) for the ModelBrowser.
    /// </summary>
    [Export(typeof(IModelBrowserHelperService))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class ModelBrowserHelperService : IModelBrowserHelperService
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The object that represents the model (the root object of the model graph).
        /// </summary>
        private object model;

        /// <summary>
        /// The data necessary for calculating the cost of the entity currently displayed in the model viewer.
        /// This data comes from the model file.
        /// </summary>
        private ExportedCalculationData calculationData;

        /// <summary>
        /// The imported media collection. The key is the id of the parent entity, the value is a list of ImportedMedia.
        /// </summary>
        private Dictionary<Guid, IEnumerable<ImportedMedia>> importedMediaDictionary;

        /// <summary>
        /// Initializes a new instance of the <see cref="ModelBrowserHelperService"/> class.
        /// </summary>
        [ImportingConstructor]
        public ModelBrowserHelperService()
        {
        }

        /// <summary>
        /// Sets the data for the currently opened model into this instance.
        /// </summary>
        /// <param name="modelData">The model data.</param>
        /// <exception cref="System.ArgumentNullException">The model data was null.</exception>
        public void SetCurrentModelData(ImportedData<object> modelData)
        {
            if (modelData == null)
            {
                throw new ArgumentNullException("modelData", "The model data was null.");
            }

            this.model = modelData.Entity;
            this.calculationData = modelData.CostCalculationData;
            this.importedMediaDictionary = modelData.MediaCollection;
        }

        /// <summary>
        /// Gets the media (video and images) for entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// A list of media (video and image) objects which belong to the entity with the given ID, or an empty list if the given entity does not have any associated media.
        /// </returns>
        /// <exception cref="InvalidOperationException"> if the collection of media files is not initialized.</exception>
        public IEnumerable<ImportedMedia> GetMediaForEntity(object entity)
        {
            if (this.importedMediaDictionary == null)
            {
                throw new InvalidOperationException("The collection of media file paths is not initialized.");
            }

            IEnumerable<ImportedMedia> result = new List<ImportedMedia>();
            var identifiable = entity as IIdentifiable;
            if (identifiable != null)
            {
                if (this.importedMediaDictionary.ContainsKey(identifiable.Guid))
                {
                    result = importedMediaDictionary[identifiable.Guid].Where(item => item.Type == MediaType.Image || item.Type == MediaType.Video);
                }
            }

            return result;
        }

        /// <summary>
        /// Gets the documents for entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// A list of imported media objects representing documents which belong to the entity with the given ID, or an empty list if the given entity does not have any associated media.
        /// </returns>
        /// <exception cref="InvalidOperationException"> if the collection of media files is not initialized.</exception>
        public IEnumerable<ImportedMedia> GetDocumentsForEntity(object entity)
        {
            if (this.importedMediaDictionary == null)
            {
                throw new InvalidOperationException("The collection of media file paths is not initialized.");
            }

            IEnumerable<ImportedMedia> result = new List<ImportedMedia>();
            var identifiable = entity as IIdentifiable;
            if (identifiable != null)
            {
                if (this.importedMediaDictionary.ContainsKey(identifiable.Guid))
                {
                    result = importedMediaDictionary[identifiable.Guid].Where(item => item.Type == MediaType.Document);
                }
            }

            return result;
        }

        /// <summary>
        /// Gets the cost calculation data stored by the service for the currently opened model as an instance of <see cref="PartCostCalculationParameters"/>,
        /// which is necessary for calculating the cost of a Part or Raw Part.
        /// </summary>
        /// <returns>The cost calculation parameters or null if the cost calculation data is not available (pre v1.3).</returns>
        public PartCostCalculationParameters GetPartCostCalculationParameters()
        {
            if (this.calculationData == null)
            {                
                return null;
            }

            return new PartCostCalculationParameters()
            {
                ProjectDepreciationPeriod = this.calculationData.DepreciationPeriod,
                ProjectDepreciationRate = this.calculationData.DepreciationRate,
                ProjectLogisticCostRatio = this.calculationData.LogisticCostRatio,
                ProjectCreateDate = this.calculationData.ProjectCreateDate
            };
        }

        /// <summary>
        /// Gets the cost calculation data stored by the service for the currently opened model as an instance of <see cref="AssemblyCostCalculationParameters"/>,
        /// which is necessary for calculating the cost of an Assembly.
        /// </summary>
        /// <returns>The cost calculation parameters, or null if the cost calculation data is not available (pre v1.3).</returns>
        public AssemblyCostCalculationParameters GetAssemblyCostCalculationParameters()
        {
            if (this.calculationData == null)
            {                
                return null;
            }

            return new AssemblyCostCalculationParameters()
            {
                ProjectDepreciationPeriod = this.calculationData.DepreciationPeriod,
                ProjectDepreciationRate = this.calculationData.DepreciationRate,
                ProjectLogisticCostRatio = this.calculationData.LogisticCostRatio,
                ProjectCreateDate = this.calculationData.ProjectCreateDate
            };
        }
    }
}
