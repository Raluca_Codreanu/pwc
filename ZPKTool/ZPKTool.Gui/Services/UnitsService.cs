﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;

namespace ZPKTool.Gui.Services
{
    /// <summary>
    /// Implementation of <see cref="IUnitsService"/>.
    /// </summary>  
    [Export(typeof(IUnitsService))]
    public class UnitsService : IUnitsService
    {
        #region Constants

        /// <summary>
        /// The adapter handlers list default size.
        /// </summary>
        private const int AdapterHandlersListDefaultSize = 10;

        #endregion Constants

        #region Fields

        /// <summary>
        /// The measurement units.
        /// </summary>
        private static Collection<MeasurementUnit> measurementUnits;

        /// <summary>
        /// An object used to sync access to the adapter.
        /// </summary>
        private readonly object adapterLock = new object();

        /// <summary>
        /// The cached units adapters.
        /// </summary>
        private List<UnitsAdapter> cachedUnitsAdapters = new List<UnitsAdapter>(AdapterHandlersListDefaultSize);

        #endregion Fields

        #region Initialization

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitsService"/> class.
        /// </summary>
        [ImportingConstructor]
        public UnitsService()
        {
            try
            {
                var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
                measurementUnits = new Collection<MeasurementUnit>(dsm.MeasurementUnitRepository.GetAll());
            }
            catch
            {
            }
        }

        #endregion Initialization

        #region Properties

        /// <summary>
        /// Gets or sets the currencies.
        /// </summary>
        public ICollection<Currency> Currencies { get; set; }

        /// <summary>
        /// Gets or sets the base currency.
        /// </summary>
        public Currency BaseCurrency { get; set; }

        #endregion Properties

        /// <summary>
        /// Gets the current unit symbol of the specified measurement unit type.
        /// </summary>
        /// <param name="unitType">The unit type.</param>
        /// <returns>The unit symbol.</returns>
        public static string GetCurrentUnitSymbol(MeasurementUnitType unitType)
        {
            var currenctUnitsSystem = UserSettingsManager.Instance.UnitsSystem;

            var symbol = string.Empty;
            if (currenctUnitsSystem == UnitsSystem.Metric)
            {
                symbol = measurementUnits.FirstOrDefault(u => u.Type == unitType && u.IsBaseUnit && (u.ScaleID == MeasurementUnitScale.MetricAreaScale
                                                                                                                        || u.ScaleID == MeasurementUnitScale.MetricLengthScale
                                                                                                                        || u.ScaleID == MeasurementUnitScale.MetricVolumeScale
                                                                                                                        || u.ScaleID == MeasurementUnitScale.MetricWeightScale)).Symbol;
            }
            else if (currenctUnitsSystem == UnitsSystem.Imperial)
            {
                symbol = measurementUnits.FirstOrDefault(u => u.Type == unitType && u.IsBaseUnit && (u.ScaleID == MeasurementUnitScale.ImperialAreaScale
                                                                                                                        || u.ScaleID == MeasurementUnitScale.ImperialLengthScale
                                                                                                                        || u.ScaleID == MeasurementUnitScale.ImperialVolumeScale
                                                                                                                        || u.ScaleID == MeasurementUnitScale.ImperialWeightScale)).Symbol;
            }

            return symbol;
        }

        /// <summary>
        /// Gets a units adapter with the specified data manager.
        /// </summary>
        /// <param name="dsm">The data manager.</param>
        /// <returns>A cached units adapter with the specified data source manager, or else a new units adapter.</returns>
        public UnitsAdapter GetUnitsAdapter(IDataSourceManager dsm)
        {
            lock (adapterLock)
            {
                // Remove the cached units adapters that are no longer alive.            
                this.cachedUnitsAdapters.RemoveAll(a => !a.IsDataManagerAlive);

                // Check if a cached units adapter with the specified data manager exists.
                var adapter = this.cachedUnitsAdapters.FirstOrDefault(a => a.DataManager == dsm);
                if (adapter != null)
                {
                    adapter.Currencies = this.Currencies;
                    adapter.BaseCurrency = this.BaseCurrency;
                }
                else
                {
                    // Create a units adapter.
                    adapter = new UnitsAdapter(dsm);
                    if (dsm != null
                        && this.BaseCurrency == null)
                    {
                        var baseCurrencies = dsm.CurrencyRepository.GetBaseCurrencies();

                        adapter.BaseCurrency = baseCurrencies.FirstOrDefault(c => c.IsoCode == Constants.DefaultCurrencyIsoCode);
                        adapter.Currencies = baseCurrencies.ToList();
                    }
                    else
                    {
                        adapter.BaseCurrency = this.BaseCurrency;
                        adapter.Currencies = this.Currencies;
                    }

                    // If the list that contains the cached units adapters if full, maintain it and cache the created units adapter.
                    if (this.cachedUnitsAdapters.Count == AdapterHandlersListDefaultSize)
                    {
                        this.cachedUnitsAdapters.RemoveAt(AdapterHandlersListDefaultSize - 1);
                    }

                    this.cachedUnitsAdapters.Insert(0, adapter);
                }

                return adapter;
            }
        }
    }
}