﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using ZPKTool.Business.Export;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Common;
using ZPKTool.Controls;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Managers;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Services
{
    /// <summary>
    /// Information about two compared objects.
    /// </summary>
    public enum CompareInfo
    {
        /// <summary>
        /// The first object is grater than the second one.
        /// </summary>
        IsGrater = 0,

        /// <summary>
        /// The first object is smaller than the second one.
        /// </summary>
        IsSmaller = 1,

        /// <summary>
        /// The first object is equal with the second one.
        /// </summary>
        IsEqual = 2
    }

    /// <summary>
    /// Implementation of IImportService that imports an entity from a file path
    /// </summary>
    [Export(typeof(IImportService))]
    public class ImportService : IImportService
    {
        /// <summary>
        /// The import confirmation view model
        /// </summary>
        private ImportConfirmationViewModel importConfirmationViewModel;

        /// <summary>
        /// the window service.
        /// </summary>
        private IWindowService windowService;

        /// <summary>
        /// The PleaseWaitService reference.
        /// </summary>
        private IPleaseWaitService pleaseWaitService;

        /// <summary>
        /// The dispatcher, which can be used to execute code on the UI thread.
        /// </summary>
        private IDispatcherService dispatcher;

        /// <summary>
        /// Initializes a new instance of the <see cref="ImportService"/> class
        /// </summary>
        /// <param name="windowService">The windows service</param>
        /// <param name="pleaseWaitService">The please wait service</param>
        /// <param name="dispatcherService">The dispatcher which is used to execute code on the UI thread.</param>
        /// <param name="importConfirmationViewModel">The import confirmation view model</param>
        [ImportingConstructor]
        public ImportService(
            IWindowService windowService,
            IPleaseWaitService pleaseWaitService,
            IDispatcherService dispatcherService,
            ImportConfirmationViewModel importConfirmationViewModel)
        {
            Argument.IsNotNull("windowService", windowService);
            Argument.IsNotNull("pleaseWaitService", pleaseWaitService);

            this.windowService = windowService;
            this.pleaseWaitService = pleaseWaitService;
            this.dispatcher = dispatcherService;
            this.importConfirmationViewModel = importConfirmationViewModel;
        }

        /// <summary>
        /// Imports the entities from the specified file paths and shows a please wait message during importing process.
        /// </summary>
        /// <param name="filePaths">The paths to the files to import from.</param>
        /// <param name="parentEntity">The object that will be the parent of the imported objects.</param>
        /// <param name="dataSourceManager">The data source manager used for importing the entity.</param>
        /// <param name="saveChanges">If set to true, during import, data is saved in the database in batches in order to allow the memory used by the data to be reclaimed; otherwise, all data is imported into memory.</param>
        /// <param name="workCompleted">A delegate action to run after import is done.</param>
        public void Import(
            IEnumerable<string> filePaths,
            object parentEntity,
            IDataSourceManager dataSourceManager,
            bool saveChanges,
            Action<PleaseWaitService.WorkParams, IEnumerable<ImportedData<object>>> workCompleted)
        {
            // Check if the selected files can be imported into the parent entity.
            var filesWithError = new List<string>();
            foreach (var file in filePaths)
            {
                var entityType = this.GetEntityTypeFromFilePath(file);
                if (entityType != null)
                {
                    if (parentEntity == null && (entityType != typeof(ProjectFolder) && entityType != typeof(Project)))
                    {
                        // Parent entity is null in case of "My Projects" node; this parent accepts only ProjectFolders and Projects.
                        filesWithError.Add(file);
                    }
                    else if (parentEntity != null &&
                        !ClipboardManager.Instance.CheckIfEntityAcceptsObject(parentEntity.GetType(), entityType))
                    {
                        filesWithError.Add(file);
                    }
                }
                else
                {
                    filesWithError.Add(file);
                }
            }

            if (filesWithError.Count > 0)
            {
                // Concatenate all files with errors into an error message.
                var errorMessage = string.Empty;
                foreach (var file in filesWithError)
                {
                    errorMessage += " \"" + Path.GetFileName(file) + "\",";
                }

                errorMessage = string.Format(LocalizedResources.General_ImportedFilesWithError, errorMessage.TrimEnd(','));
                this.windowService.MessageDialogService.Show(errorMessage, MessageDialogType.Error);
                return;
            }

            Func<object, bool> importConfirmation = new Func<object, bool>(entity =>
            {
                return HandleImportConfirmation(entity, parentEntity, dataSourceManager);
            });

            var importedEntitiesData = new List<ImportedData<object>>();
            var importedEntities = new List<object>();
            Action<PleaseWaitService.WorkParams> work = (workParams) =>
            {
                int fileIndex = 1;
                int filesNo = filePaths.Count();
                foreach (var filePath in filePaths)
                {
                    string workingMsg = null;
                    if (filesNo == 1)
                    {
                        workingMsg = this.GetWaitMessage(filePath);
                    }
                    else
                    {
                        workingMsg = string.Format(LocalizedResources.General_WaitImportObjects, fileIndex, filesNo);
                    }

                    // Set the wait message displayed during the import process.
                    this.pleaseWaitService.Message = workingMsg;
                    fileIndex++;

                    var importedData = this.ImportInternal(filePath, parentEntity, dataSourceManager, saveChanges, importConfirmation);
                    if (importedData != null)
                    {
                        importedEntitiesData.Add(importedData);
                        if (importedData.Entity != null)
                        {
                            // Add into imported entities list only the entities for which the import was successful.
                            importedEntities.Add(importedData.Entity);
                        }
                    }
                }

                if (importedEntities.Count > 0 && this.CanUpdateToRecipientCalculationVersion(parentEntity, importedEntities))
                {
                    foreach (var entity in importedEntities)
                    {
                        this.UpdateToRecipientCalculationVersion(parentEntity as Assembly, entity);
                    }

                    dataSourceManager.SaveChanges();
                }
            };

            Action<PleaseWaitService.WorkParams> internalWorkCompleted = (args) =>
            {
                if (args.Error != null)
                {
                    this.windowService.MessageDialogService.Show(args.Error);
                }
                else
                {
                    var count = importedEntitiesData.Count(imp => imp.Error != null);
                    if (count == 1)
                    {
                        this.windowService.MessageDialogService.Show(importedEntitiesData.FirstOrDefault(imp => imp.Error != null).Error);
                    }
                    else if (count > 1)
                    {
                        StringBuilder errorMessage = new StringBuilder();
                        errorMessage.AppendLine(LocalizedResources.General_ImportErrors);
                        foreach (var data in importedEntitiesData.Where(imp => imp.Error != null))
                        {
                            errorMessage.AppendLine("- \"" + Path.GetFileName(data.SourceFile) + "\": " + this.windowService.MessageDialogService.GetErrorMessage(data.Error));
                        }

                        // Return all errors into a single message.
                        this.windowService.MessageDialogService.Show(errorMessage.ToString(), MessageDialogType.Error);
                    }

                    if (workCompleted != null)
                    {
                        workCompleted(args, importedEntitiesData);
                    }
                }
            };

            this.pleaseWaitService.Show(string.Empty, work, internalWorkCompleted);
        }

        /// <summary>
        /// Executes an internal import for each entity during the work action.
        /// </summary>
        /// <param name="filePath">The file path to the entity to import.</param>
        /// <param name="parentEntity">The object that will be the parent of the imported entity.</param>
        /// <param name="dataSourceManager">The data source manager used for importing the entity.</param>
        /// <param name="saveChanges">If set to true, during import, data is saved in the database in batches; otherwise, all data is imported into memory.</param>
        /// <param name="importConfirmation">The import confirmation function.</param>
        /// <returns>The entity's imported data.</returns>
        /// <exception cref="System.InvalidOperationException">The file to import cannot be handled because it does not have a supported type.</exception>
        private ImportedData<object> ImportInternal(
            string filePath,
            object parentEntity,
            IDataSourceManager dataSourceManager,
            bool saveChanges,
            Func<object, bool> importConfirmation)
        {
            Type type = GetEntityTypeFromFilePath(filePath);
            if (type == typeof(ProjectFolder))
            {
                return ExportManager.Instance.ImportEntity<ProjectFolder>(filePath, parentEntity, dataSourceManager, saveChanges, importConfirmation);
            }
            else if (type == typeof(Project))
            {
                return ExportManager.Instance.ImportEntity<Project>(filePath, parentEntity, dataSourceManager, saveChanges, importConfirmation);
            }
            else if (type == typeof(Assembly))
            {
                return ExportManager.Instance.ImportEntity<Assembly>(filePath, parentEntity, dataSourceManager, saveChanges, importConfirmation);
            }
            else if (type == typeof(Part))
            {
                return ExportManager.Instance.ImportEntity<Part>(filePath, parentEntity, dataSourceManager, saveChanges, importConfirmation);
            }
            else if (type == typeof(RawPart))
            {
                Part parentPart = parentEntity as Part;
                if (parentPart != null && parentPart.RawPart != null)
                {
                    if (parentPart.RawPart.IsDeleted)
                    {
                        this.windowService.MessageDialogService.Show(LocalizedResources.Error_PartAlreadyContainsDeletedRawPart, MessageDialogType.Error);
                        return null;
                    }
                    else
                    {
                        this.windowService.MessageDialogService.Show(LocalizedResources.Error_PartAlreadyContainsRawPart, MessageDialogType.Error);
                        return null;
                    }
                }
                else
                {
                    return ExportManager.Instance.ImportEntity<RawPart>(filePath, parentEntity, dataSourceManager, saveChanges, importConfirmation);
                }
            }
            else if (type == typeof(RawMaterial))
            {
                return ExportManager.Instance.ImportEntity<RawMaterial>(filePath, parentEntity, dataSourceManager, saveChanges, importConfirmation);
            }
            else if (type == typeof(Commodity))
            {
                return ExportManager.Instance.ImportEntity<Commodity>(filePath, parentEntity, dataSourceManager, saveChanges, importConfirmation);
            }
            else if (type == typeof(Consumable))
            {
                return ExportManager.Instance.ImportEntity<Consumable>(filePath, parentEntity, dataSourceManager, saveChanges, importConfirmation);
            }
            else if (type == typeof(Machine))
            {
                return ExportManager.Instance.ImportEntity<Machine>(filePath, parentEntity, dataSourceManager, saveChanges, importConfirmation);
            }
            else if (type == typeof(Die))
            {
                return ExportManager.Instance.ImportEntity<Die>(filePath, parentEntity, dataSourceManager, saveChanges, importConfirmation);
            }

            throw new InvalidOperationException("The file to import cannot be handled because it does not have a supported type.");
        }

        /// <summary>
        /// Sets the import confirmation view model.
        /// </summary>
        /// <param name="entity">The imported entity.</param>
        /// <param name="parentEntity">The parent of imported entity.</param>
        /// <param name="dataSourceManager">The data source manager.</param>
        /// <returns>A value indicating whether the import can continue or not.</returns>
        private bool HandleImportConfirmation(object entity, object parentEntity, IDataSourceManager dataSourceManager)
        {
            if (!UserSettingsManager.Instance.DontShowImportConfirmation)
            {
                this.importConfirmationViewModel.IsReadOnly = true;
                this.importConfirmationViewModel.DataSourceManager = dataSourceManager;
                this.importConfirmationViewModel.ModelParent = parentEntity;
                this.importConfirmationViewModel.Model = entity;
                this.dispatcher.Invoke(() =>
                {
                    this.windowService.ShowViewInDialog(this.importConfirmationViewModel, "ImportConfirmationViewTemplate");
                });

                return this.importConfirmationViewModel.CanImportContinue;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Gets the entity type from it's file path.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <returns>The entity type.</returns>
        /// <exception cref="System.InvalidOperationException">The file extension is not supported.</exception>
        private Type GetEntityTypeFromFilePath(string filePath)
        {
            Type type = null;
            var extension = Path.GetExtension(filePath);
            if (!string.IsNullOrWhiteSpace(extension))
            {
                if (extension.Equals(".folder", StringComparison.InvariantCultureIgnoreCase))
                {
                    type = typeof(ProjectFolder);
                }
                else if (extension.Equals(".project", StringComparison.InvariantCultureIgnoreCase))
                {
                    type = typeof(Project);
                }
                else if (extension.Equals(".assembly", StringComparison.InvariantCultureIgnoreCase))
                {
                    type = typeof(Assembly);
                }
                else if (extension.Equals(".part", StringComparison.InvariantCultureIgnoreCase))
                {
                    type = typeof(Part);
                }
                else if (extension.Equals(".rawpart", StringComparison.InvariantCultureIgnoreCase))
                {
                    type = typeof(RawPart);
                }
                else if (extension.Equals(".rawmaterial", StringComparison.InvariantCultureIgnoreCase))
                {
                    type = typeof(RawMaterial);
                }
                else if (extension.Equals(".commodity", StringComparison.InvariantCultureIgnoreCase))
                {
                    type = typeof(Commodity);
                }
                else if (extension.Equals(".consumable", StringComparison.InvariantCultureIgnoreCase))
                {
                    type = typeof(Consumable);
                }
                else if (extension.Equals(".machine", StringComparison.InvariantCultureIgnoreCase))
                {
                    type = typeof(Machine);
                }
                else if (extension.Equals(".die", StringComparison.InvariantCultureIgnoreCase))
                {
                    type = typeof(Die);
                }
            }

            return type;
        }

        /// <summary>
        /// Gets the wait message displayed during the importing process, according to the entity type got from it's file path.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <returns>The string containing the wait message.</returns>
        private string GetWaitMessage(string filePath)
        {
            string workingMsg = string.Empty;
            Type type = this.GetEntityTypeFromFilePath(filePath);
            if (type == typeof(ProjectFolder))
            {
                workingMsg = LocalizedResources.General_WaitProjectFolderImport;
            }
            else if (type == typeof(Project))
            {
                workingMsg = LocalizedResources.General_WaitProjectImport;
            }
            else if (type == typeof(Assembly))
            {
                workingMsg = LocalizedResources.General_WaitAssemblyImport;
            }
            else if (type == typeof(Part))
            {
                workingMsg = LocalizedResources.General_WaitPartImport;
            }
            else if (type == typeof(RawPart))
            {
                workingMsg = LocalizedResources.General_WaitRawPartImport;
            }
            else if (type == typeof(RawMaterial))
            {
                workingMsg = LocalizedResources.General_WaitRawMaterialImport;
            }
            else if (type == typeof(Commodity))
            {
                workingMsg = LocalizedResources.General_WaitCommodityImport;
            }
            else if (type == typeof(Consumable))
            {
                workingMsg = LocalizedResources.General_WaitConsumableImport;
            }
            else if (type == typeof(Machine))
            {
                workingMsg = LocalizedResources.General_WaitMachineImport;
            }
            else if (type == typeof(Die))
            {
                workingMsg = LocalizedResources.General_WaitDieImport;
            }

            return workingMsg;
        }

        /// <summary>
        /// Determines whether the calculation variant must be updated for the objects and receives the user feedback regarding the update action.
        /// </summary>
        /// <param name="target">The target.</param>
        /// <param name="objectsToUpdate">The objects for which the update action is checked.</param>
        /// <returns>True if the calculation version can be updated or false if not.</returns>  
        public bool CanUpdateToRecipientCalculationVersion(object target, List<object> objectsToUpdate)
        {
            var textToDisplay = string.Empty;
            var updateToRecipientCalculationVersion = false;

            List<CompareInfo> compareInfo = new List<CompareInfo>();
            foreach (var obj in objectsToUpdate)
            {
                compareInfo.Add(this.CompareCalculationVersion(target as Assembly, obj));
            }

            if (compareInfo.Contains(CompareInfo.IsGrater) && compareInfo.Contains(CompareInfo.IsSmaller))
            {
                // The calculation variants for the objects are newer and older too; the general message must be displayed.
                textToDisplay = LocalizedResources.Paste_UpdateCalculationVariant;
            }
            else if (compareInfo.Contains(CompareInfo.IsGrater))
            {
                // The calculation variants for the objects are newer or equal to the calculation variant of target.
                textToDisplay = LocalizedResources.Paste_UpgradeCalculationVariant;
            }
            else if (compareInfo.Contains(CompareInfo.IsSmaller))
            {
                // The calculation variants for the objects are older or equal to the calculation variant of target.
                textToDisplay = LocalizedResources.Paste_DowngradeCalculationVariant;
            }

            if (!string.IsNullOrWhiteSpace(textToDisplay))
            {
                var answer = this.windowService.MessageDialogService.Show(
                    textToDisplay,
                    LocalizedResources.Paste_ChangeCalculationVariantNote,
                    MessageDialogType.YesNo);
                if (answer == MessageDialogResult.Yes)
                {
                    updateToRecipientCalculationVersion = true;
                }
            }

            return updateToRecipientCalculationVersion;
        }

        /// <summary>
        /// Compares the calculation version of target and source.
        /// </summary>
        /// <param name="targetAssembly">The target assembly.</param>
        /// <param name="source">The source.</param>
        /// <returns> Information about the objects' comparison. </returns>
        private CompareInfo CompareCalculationVersion(Assembly targetAssembly, object source)
        {
            if (targetAssembly == null)
            {
                return CompareInfo.IsEqual;
            }

            var targetCalculationVersion = targetAssembly.CalculationVariant;

            // If calculation version is empty or null than is set to the oldest variant.
            if (string.IsNullOrEmpty(targetCalculationVersion))
            {
                targetCalculationVersion = CostCalculatorFactory.OldestVersion;
            }

            var sourceCalculationVersion = string.Empty;
            var sourcePart = source as Part;
            if (sourcePart != null)
            {
                sourceCalculationVersion = sourcePart.CalculationVariant;
            }
            else
            {
                var sourceAssy = source as Assembly;
                if (sourceAssy != null)
                {
                    sourceCalculationVersion = sourceAssy.CalculationVariant;
                }
            }

            // If the source object has a calculation version and is empty or null than is set to the oldest variant.
            if (string.IsNullOrEmpty(sourceCalculationVersion))
            {
                sourceCalculationVersion = CostCalculatorFactory.OldestVersion;
            }

            var compareInfo = CompareInfo.IsEqual;
            if (CostCalculatorFactory.IsNewer(targetCalculationVersion, sourceCalculationVersion))
            {
                compareInfo = CompareInfo.IsGrater;
            }
            else if (CostCalculatorFactory.IsOlder(targetCalculationVersion, sourceCalculationVersion))
            {
                compareInfo = CompareInfo.IsSmaller;
            }

            return compareInfo;
        }

        /// <summary>
        /// Updates to recipient calculation version.
        /// </summary>
        /// <param name="recipientAssembly">The recipient assembly.</param>
        /// <param name="source">The source.</param>
        private void UpdateToRecipientCalculationVersion(Assembly recipientAssembly, object source)
        {
            if (recipientAssembly == null)
            {
                return;
            }

            var recipientCalculationVariant = recipientAssembly.CalculationVariant;
            if (string.IsNullOrEmpty(recipientCalculationVariant))
            {
                recipientCalculationVariant = CostCalculatorFactory.OldestVersion;
            }

            var importedAssy = source as Assembly;
            if (importedAssy != null)
            {
                importedAssy.SetCalculationVariant(recipientCalculationVariant);
            }
            else
            {
                var importedPart = source as Part;
                if (importedPart != null)
                {
                    importedPart.SetCalculationVariant(recipientCalculationVariant);
                }
            }
        }
    }
}