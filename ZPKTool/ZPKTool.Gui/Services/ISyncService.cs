﻿namespace ZPKTool.Gui.Services
{
    using System;
    using ZPKTool.Synchronization;

    /// <summary>
    /// The interface of a service which uses Synchronization Manager to synchronize current user data.
    /// </summary>
    public interface ISyncService
    {
        /// <summary>
        /// Gets the synchronization manager.
        /// </summary>
        ISynchronizationManager SyncManager { get; }

        /// <summary>
        /// Starts the synchronization.
        /// </summary>
        /// <param name="userGuid">The user unique identifier.</param>
        /// <param name="resolveConflicts">if set to true, this call will resolve any conflicts that appeared in previous sync runs.</param>
        void StartSync(Guid userGuid, bool resolveConflicts = false);

        /// <summary>
        /// Postpones the synchronization.
        /// </summary>
        /// <param name="userGuid">The user unique identifier.</param>
        /// <param name="minutesDelay">The minutes delay.</param>
        void PostponeSync(Guid userGuid, int minutesDelay);

        /// <summary>
        /// Cancel the execution of scheduled synchronization.
        /// </summary>
        void CancelSyncPostponement();
    }
}
