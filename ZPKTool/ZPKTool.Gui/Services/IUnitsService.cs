﻿using System.Collections.Generic;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Data;
using ZPKTool.DataAccess;

namespace ZPKTool.Gui.Services
{
    /// <summary>
    /// The interface of a service that handles the units adapters and holds the list of currencies and base currency for the selected object.
    /// </summary>
    public interface IUnitsService
    {
        /// <summary>
        /// Gets or sets the currencies.
        /// </summary>
        ICollection<Currency> Currencies { get; set; }

        /// <summary>
        /// Gets or sets the base currency.
        /// </summary>
        Currency BaseCurrency { get; set; }

        /// <summary>
        /// Gets a units adapter with the specified data manager.
        /// </summary>
        /// <param name="dataManager">The data manager.</param>
        /// <returns>A cached units adapter with the specified data manager, or else a new units adapter.</returns>
        UnitsAdapter GetUnitsAdapter(IDataSourceManager dataManager);
    }
}