﻿using System.Collections.Generic;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Controls;

namespace ZPKTool.Gui.Services
{
    /// <summary>
    /// Provides the way to release unused data.
    /// </summary>
    public interface IExplorerDataReleaseProvider
    {
        /// <summary>
        /// Analyzes the data to determine what is releasable.
        /// </summary>
        /// <returns>The releasable data.</returns>
        ReleasableExplorerData AnalyzeDataForRelease();

        /// <summary>
        /// Determine and release the data where the specified data managers are used.
        /// </summary>
        /// <param name="dataManagersToRelease">The data managers to release.</param>
        void ReleaseData(IEnumerable<IDataSourceManager> dataManagersToRelease);
    }
}