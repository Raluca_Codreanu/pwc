﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using ZPKTool.Controls;
using ZPKTool.Gui.Utils;

namespace ZPKTool.Gui.Controls
{
    /// <summary>
    /// Exposes attached behaviors that are applied to the TreeViewItem's MenuItems.
    /// </summary>
    public class TreeViewMenuItemBehavior
    {
        /// <summary>
        ///  Using a DependencyProperty as the backing store for SelectedItemsToCommandParameter.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty SelectedItemsToCommandParameterProperty =
            DependencyProperty.RegisterAttached("SelectedItemsToCommandParameter", typeof(bool), typeof(TreeViewMenuItemBehavior), new UIPropertyMetadata(false, SelectedItemsToCommandChanged));

        /// <summary>
        /// Gets a value indicating whether the attached MenuItem command parameter should contain the TreeViewEx parent Selected Items as a <see cref="EntityInfo"/> list.
        /// </summary>
        /// <param name="obj">The attached TreeViewItem.</param>
        /// <returns>True if the TreeViewItem is additionally selected; otherwise, false.</returns>
        public static bool GetSelectedItemsToCommandParameter(DependencyObject obj)
        {
            return (bool)obj.GetValue(SelectedItemsToCommandParameterProperty);
        }

        /// <summary>
        /// Sets a value indicating whether the attached MenuItem command parameter should contain the TreeViewEx parent Selected Items as a <see cref="EntityInfo"/> list.
        /// </summary>
        /// <param name="obj">The attached TreeViewItem.</param>
        /// <param name="value">If set to true the TreeViewItem should be highlighted.</param>
        public static void SetSelectedItemsToCommandParameter(DependencyObject obj, bool value)
        {
            obj.SetValue(SelectedItemsToCommandParameterProperty, value);
        }

        /// <summary>
        /// Invoked when the SelectedItemsToCommand property value has changed.
        /// </summary>
        /// <param name="d">The dependency object.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void SelectedItemsToCommandChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var menuItem = d as MenuItem;
            if (menuItem == null)
            {
                return;
            }

            menuItem.Loaded += MenuItem_Loaded;
        }

        /// <summary>
        /// Handles the Loaded event of menu item control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private static void MenuItem_Loaded(object sender, RoutedEventArgs e)
        {
            var item = sender as MenuItem;
            if (item != null)
            {
                ContextMenu contextMenu = null;
                var menuItem = item;

                while (contextMenu == null || menuItem != null)
                {
                    contextMenu = menuItem.Parent as ContextMenu;
                    menuItem = menuItem.Parent as MenuItem;
                }

                var treeView = UIHelper.FindParent<TreeViewEx>(contextMenu.PlacementTarget);
                if (treeView != null && treeView.SelectedItems != null)
                {
                    var selectedItems = new List<EntityInfo>();
                    foreach (var it in treeView.SelectedItems)
                    {
                        var treeItem = it as TreeViewDataItem;
                        if (treeItem != null)
                        {
                            selectedItems.Add(new EntityInfo(treeItem.DataObject, treeItem.DataObjectSource));
                        }
                    }

                    item.CommandParameter = selectedItems;
                }
            }
        }
    }
}
