﻿namespace ZPKTool.Gui.Controls
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Represents the options of lazy loading a TreeViewDataItem's children.
    /// </summary>
    public enum TreeViewItemLazyLoadMode
    {
        /// <summary>
        /// The children are not lazy loaded.
        /// </summary>
        None = 0,

        /// <summary>
        /// The children loading begins when the item is selected.
        /// </summary>
        OnSelection = 1,

        /// <summary>
        /// The children loading begins when the item is expanded.
        /// </summary>
        OnExpansion = 2,

        /// <summary>
        /// The children loading begins when the item is selected or expanded.
        /// </summary>        
        OnSelectionOrExpansion = 3
    }    
}
