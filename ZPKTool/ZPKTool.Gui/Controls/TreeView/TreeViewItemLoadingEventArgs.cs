﻿namespace ZPKTool.Gui.Controls
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Provides data for the handlers of the LoadingItem event (of TreeViewEx class). 
    /// </summary>
    public class TreeViewItemLoadingEventArgs : EventArgs
    {
        /// <summary>
        /// Gets or sets a value indicating whether the item is being loading.
        /// </summary>        
        public bool IsLoading { get; set; }
    }
}
