﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using ZPKTool.Common;
using ZPKTool.Controls;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Utils;

namespace ZPKTool.Gui.Controls
{
    /// <summary>
    /// This class extends System.Windows.Controls.TreeView to provide new functionality specific to the app UI.
    /// </summary>
    public class TreeViewEx : TreeView
    {
        #region Fields

        /// <summary>
        /// Using a DependencyProperty as the backing store for SelectedItems.  This enables animation, styling, binding, etc... 
        /// </summary>
        private static readonly DependencyPropertyKey SelectedItemsPropertyKey =
            DependencyProperty.RegisterReadOnly("SelectedItems", typeof(ObservableCollection<object>), typeof(TreeViewEx), new PropertyMetadata(null, SelectedItemsChangedCallback));

        /// <summary>
        /// The ScrollViewer of the TreeView.
        /// </summary>
        private ScrollViewer internalScrollViewer;

        /// <summary>
        /// The scroll viewer's horizontal offset.
        /// </summary>
        private double horizontalOffset;

        /// <summary>
        /// A value indicating whether a tree item's children are currently being loaded.
        /// The tree view is allowed to load only 1 of its items at a time -> this field is used to avoid starting the loading for multiple items.
        /// </summary>
        private volatile bool isLoadingItemChildren;

        #endregion Fields

        /// <summary>
        /// Initializes a new instance of the <see cref="TreeViewEx"/> class.
        /// </summary>
        public TreeViewEx()
            : base()
        {
            SetValue(SelectedItemsPropertyKey, new ObservableCollection<object>());
            this.AddHandler(TreeViewItem.ExpandedEvent, new RoutedEventHandler(TreeItemExpanded));
            this.PreviewMouseLeftButtonDown += TreeViewPreviewMouseLeftButtonDown;
        }

        #region Events

        /// <summary>
        /// Occurs when the loading of a tree item has started or finished.
        /// </summary>
        public event Action<object, TreeViewItemLoadingEventArgs> LoadingItem;

        /// <summary>Event fired when <see cref="TreeView.SelectedItem"/> changes. </summary>
        public static readonly RoutedEvent PreviewSelectedItemChangedEvent =
            EventManager.RegisterRoutedEvent("PreviewSelectedItemChanged", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(TreeViewEx));

        /// <summary>Event fired when <see cref="TreeView.SelectedItem"/> changes. </summary>
        [Category("Behavior")]
        public event RoutedEventHandler PreviewSelectedItemChanged
        {
            add
            {
                AddHandler(PreviewSelectedItemChangedEvent, value);
            }

            remove
            {
                RemoveHandler(PreviewSelectedItemChangedEvent, value);
            }
        }

        /// <summary>
        /// Occurs when the selected items collection is initialized.
        /// </summary>
        public event EventHandler<SelectedItemsChangedEventArgs> SelectedItemsChanged;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets the selected items.
        /// The selected items collection is read only and can not be set.
        /// </summary>
        public IEnumerable<object> SelectedItems
        {
            get { return (ObservableCollection<object>)GetValue(SelectedItemsPropertyKey.DependencyProperty); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to cancel the next execution of the SelectedItemChanged event.
        /// It is used to support the RaisePreviewSelectedItemChanged event and cancel the selection of an item.
        /// </summary>
        public bool ShouldCancelNextSelectedItemChangedEvent { get; set; }

        #endregion Properties

        /// <summary>
        /// Invoked when the SelectedItems property changed.
        /// </summary>
        /// <param name="d">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void SelectedItemsChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var treeView = d as TreeViewEx;
            if (treeView != null)
            {
                var selectedItems = (ObservableCollection<object>)treeView.GetValue(SelectedItemsPropertyKey.DependencyProperty);
                selectedItems.CollectionChanged += (s, args) =>
                {
                    var eventHandlers = treeView.SelectedItemsChanged;
                    if (eventHandlers != null)
                    {
                        eventHandlers(treeView, new SelectedItemsChangedEventArgs(selectedItems));
                    }
                };
            }
        }

        /// <summary>
        /// When overridden in a derived class, is invoked whenever application code or internal processes call <see cref="M:System.Windows.FrameworkElement.ApplyTemplate"/>.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            bool inDesignMode = DesignerProperties.GetIsInDesignMode(this);
            this.internalScrollViewer = this.GetTemplateChild("PART_ScrollViewer") as ScrollViewer;
            if (this.internalScrollViewer != null)
            {
                this.horizontalOffset = this.internalScrollViewer.HorizontalOffset;
                var isVirtualizationEnabled = VirtualizingStackPanel.GetIsVirtualizing(this);
                if (!isVirtualizationEnabled)
                {
                    this.internalScrollViewer.RequestBringIntoView += (s, e) => this.internalScrollViewer.ScrollToHorizontalOffset(this.horizontalOffset);
                }
            }
            else if (!inDesignMode)
            {
                throw new InvalidOperationException("The internal scroll viewer (PART_ScrollViewer) could not be found.");
            }
        }

        /// <summary>
        /// Invoked when an unhandled <see cref="E:System.Windows.Input.Keyboard.PreviewKeyDown"/> attached event reaches an element in its route that is derived from this class. Implement this method to add class handling for this event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.Windows.Input.KeyEventArgs"/> that contains the event data.</param>
        protected override void OnPreviewKeyDown(KeyEventArgs e)
        {
            // Disable the "Expand" shortcut ("*")
            if (e.Key == Key.Multiply)
            {
                e.Handled = true;
            }

            // If the command contains control modifier no navigation should be performed
            if ((e.KeyboardDevice.Modifiers & ModifierKeys.Control) != ModifierKeys.Control)
            {
                this.horizontalOffset = this.internalScrollViewer.HorizontalOffset;
                this.PerformKeyboardNavigation(e);
            }

            if (!e.Handled)
            {
                base.OnPreviewKeyDown(e);
            }
        }

        /// <summary>
        /// Invoked when an unhandled <see cref="E:System.Windows.Input.Keyboard.PreviewMouseDown"/> attached event reaches an element in its route that is derived from this class.
        /// </summary>
        /// <param name="e">The <see cref="T:System.Windows.Input.MouseButtonEventArgs"/> that contains the event data.</param>
        protected override void OnPreviewMouseDown(MouseButtonEventArgs e)
        {
            ToggleButton expanderButton = UIHelper.FindParent<ToggleButton>(e.OriginalSource as DependencyObject);

            if (expanderButton == null)
            {
                var clickedItem = UIHelper.FindParent<TreeViewItem>(e.OriginalSource as DependencyObject);
                if (clickedItem != null)
                {
                    var selectedItem = this.SelectedItem as TreeViewDataItem;
                    TreeViewDataItem dataItem = clickedItem.DataContext as TreeViewDataItem;
                    if (dataItem != null)
                    {
                        if (e.LeftButton == MouseButtonState.Pressed
                            && (Keyboard.Modifiers & ModifierKeys.Shift) == ModifierKeys.Shift
                            && selectedItem != null)
                        {
                            // Make the multiple selection using SHIFT key + Left mouse click
                            if (dataItem.Parent != null && dataItem.Parent == selectedItem.Parent && dataItem.Parent.AllowChildrenMultipleSelection)
                            {
                                // Select all items selectable between the last item selected and the item clicked
                                var itemParent = UIHelper.FindParent<TreeViewItem>(clickedItem);
                                var lastItem = itemParent.ItemContainerGenerator.ContainerFromItem(this.SelectedItems.LastOrDefault()) as TreeViewItem;
                                this.AddItemsRangeToMultiSelection(clickedItem, lastItem);
                            }

                            e.Handled = true;
                            return;
                        }
                        else if (e.LeftButton == MouseButtonState.Pressed && Keyboard.Modifiers == ModifierKeys.Control && selectedItem != null)
                        {
                            // Make the selection using CTRL key + Left mouse click
                            if (!dataItem.IsSelected && dataItem.Parent != null && dataItem.Parent.AllowChildrenMultipleSelection && dataItem.AllowMultipleSelection)
                            {
                                this.AddItemToMultiSelection(clickedItem, dataItem, true);
                            }

                            e.Handled = true;
                            return;
                        }
                        else
                        {
                            if (dataItem.IsSelected && e.LeftButton == MouseButtonState.Pressed)
                            {
                                this.SelectTreeItem(dataItem);
                            }
                            else
                            {
                                RoutedPropertyChangedEventArgs<object> args = new RoutedPropertyChangedEventArgs<object>(SelectedItem, dataItem, PreviewSelectedItemChangedEvent);
                                RaiseEvent(args);

                                if (args.Handled)
                                {
                                    e.Handled = args.Handled;
                                    return;
                                }
                                else
                                {
                                    // Set the flag to cancel the next selected item changed event to true. This will prevent the TreeItemIsSelectedConverter to raise the PreviewSelectedItemChangedEvent (again).
                                    this.ShouldCancelNextSelectedItemChangedEvent = true;
                                    dataItem.IsSelected = true;

                                    // At this point the TreeItemIsSelectedConverter did its part so reset the ShouldCancelNextSelectedItemChangedEvent flag.
                                    this.ShouldCancelNextSelectedItemChangedEvent = false;
                                }
                            }
                        }
                    }
                }
            }

            base.OnPreviewMouseDown(e);
        }

        /// <summary>
        /// Sets the additionally selected item.
        /// </summary>
        /// <param name="dataItem">The data item.</param>
        /// <param name="isSelected">if set to <c>true</c> [is selected].</param>
        private void SetAdditionallySelected(TreeViewDataItem dataItem, bool isSelected)
        {
            if (dataItem == null)
            {
                return;
            }

            var treeItem = this.FindItemContainer(dataItem);
            if (treeItem != null)
            {
                TreeViewItemBehavior.SetIsAdditionallySelected(treeItem, isSelected);
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Controls.TreeView.SelectedItemChanged"/> event when the <see cref="P:System.Windows.Controls.TreeView.SelectedItem"/> property value changes.
        /// </summary>
        /// <param name="e">Provides the item that was previously selected and the item that is currently selected for the <see cref="E:System.Windows.Controls.TreeView.SelectedItemChanged"/> event.</param>
        protected override void OnSelectedItemChanged(RoutedPropertyChangedEventArgs<object> e)
        {
            TreeViewDataItem dataItem = e.NewValue as TreeViewDataItem;
            if (dataItem != null && dataItem.ChildrenAreRefreshing)
            {
                return;
            }

            TreeViewDataItem firstDataItem = null;
            var selectedItem = this.SelectedItem as TreeViewDataItem;
            var selectedItems = (ObservableCollection<object>)GetValue(SelectedItemsPropertyKey.DependencyProperty);
            if (selectedItems.Count > 0)
            {
                firstDataItem = selectedItems[0] as TreeViewDataItem;
            }

            // The mouse button state needs to be evaluated because when you click an item that is not selected and there are selected items with the same parent the selected items list needs to be cleared.
            if (Mouse.RightButton == MouseButtonState.Released
                || (Mouse.RightButton == MouseButtonState.Pressed
                    && selectedItem != null
                    && !this.SelectedItems.Contains(selectedItem))
                || (firstDataItem != null
                    && selectedItem != null
                    && firstDataItem.Parent != selectedItem.Parent))
            {
                this.SelectTreeItem(selectedItem);
            }

            // Continue raising the SelectedItemChanged event.
            bool itemLoadStarted = false;

            // Load the selected item's children if not already loaded and trigger the SelectedItemChanged event.
            if (dataItem != null
                && (dataItem.LazyLoad == TreeViewItemLazyLoadMode.OnSelection || dataItem.LazyLoad == TreeViewItemLazyLoadMode.OnSelectionOrExpansion))
            {
                itemLoadStarted = this.LoadItemChildrenInBackground(
                    dataItem,
                    TreeViewItemLoadingAnimation.LoadingIcon,
                    () =>
                    {
                        base.OnSelectedItemChanged(e);
                    });
            }

            if (!itemLoadStarted)
            {
                base.OnSelectedItemChanged(e);
            }
        }

        #region TreeView event handlers

        /// <summary>
        /// Handles the Expanded event for the tree's items.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void TreeItemExpanded(object sender, RoutedEventArgs e)
        {
            TreeViewItem expandedItem = e.OriginalSource as TreeViewItem;
            if (expandedItem == null)
            {
                return;
            }

            TreeViewDataItem dataItem = expandedItem.DataContext as TreeViewDataItem;
            if (dataItem == null)
            {
                return;
            }

            if (dataItem.LazyLoad == TreeViewItemLazyLoadMode.OnExpansion
                || dataItem.LazyLoad == TreeViewItemLazyLoadMode.OnSelectionOrExpansion)
            {
                try
                {
                    this.LoadItemChildrenInBackground(dataItem, TreeViewItemLoadingAnimation.LoadingSubItem, null);
                }
                catch (Exception ex)
                {
                    // For some unknown reason if LoadItemChildrenInBackground throws an exception, the exception does not reach the dispatcher.
                    // As a workaround, we explicitly throw it on the dispatcher thread.
                    Action action = () => { throw ex; };
                    this.Dispatcher.Invoke(action);
                }
            }
        }

        /// <summary>
        /// Handles the PreviewMouseLeftButtonDown event of the TreeView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void TreeViewPreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.horizontalOffset = this.internalScrollViewer.HorizontalOffset;
        }

        #endregion TreeView event handlers

        #region Helpers

        /// <summary>
        /// Performs the special keyboard navigation.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.Input.KeyEventArgs"/> instance containing the keyboard event data.</param>
        /// <returns>True if it has navigated to an item; otherwise false.</returns>
        private bool PerformKeyboardNavigation(KeyEventArgs e)
        {
            if (this.SelectedItem == null)
            {
                return false;
            }

            TreeViewItem selectedViewItem = this.SelectedItem as TreeViewItem;
            TreeViewDataItem selectedDataItem = this.SelectedItem as TreeViewDataItem;
            if (selectedViewItem == null && selectedDataItem != null)
            {
                selectedViewItem = e.OriginalSource as TreeViewItem ?? UIHelper.FindParent<TreeViewItem>(e.OriginalSource as DependencyObject);
            }

            ItemsControl parentControl = UIHelper.FindParent<ItemsControl>(selectedViewItem);
            if (parentControl == null || selectedViewItem == null)
            {
                return false;
            }

            bool defaultKeyNav = false;
            TreeViewItem itemToNavigateTo = null;
            Key pressedKey = e.Key;

            if (pressedKey == Key.Up || pressedKey == Key.Down || pressedKey == Key.Home || pressedKey == Key.End || pressedKey == Key.PageDown || pressedKey == Key.PageUp)
            {
                defaultKeyNav = true;
            }
            else if (pressedKey == Key.Right)
            {
                if (selectedViewItem.HasItems && selectedViewItem.IsExpanded)
                {
                    defaultKeyNav = true;
                }
            }
            else if (pressedKey == Key.Left)
            {
                if (!selectedViewItem.IsExpanded && (selectedDataItem == null || selectedDataItem.Parent != null))
                {
                    defaultKeyNav = true;
                }
            }
            else
            {
                // The navigation by key logic is: you navigate to the next tree item whose header starts with the pressed key.
                if ((pressedKey >= Key.D0 && pressedKey <= Key.Z) || (pressedKey >= Key.NumPad0 && pressedKey <= Key.NumPad9))
                {
                    // Convert the key to its string representation
                    string keyChar = pressedKey.ToString().ToLower();
                    if (keyChar.StartsWith("d") && pressedKey != Key.D)
                    {
                        keyChar = keyChar.Replace("d", string.Empty);
                    }
                    else if (keyChar.StartsWith("numpad"))
                    {
                        keyChar = keyChar.Replace("numpad", string.Empty);
                    }

                    // Find the next item whose label starts with the pressed letter; begin the search at the item following the selected item.
                    int startIndex = parentControl.ItemContainerGenerator.IndexFromContainer(selectedViewItem) + 1;
                    int endIndex = parentControl.Items.Count - 1;
                    if (startIndex > endIndex)
                    {
                        endIndex = startIndex - 2;
                        startIndex = 0;
                    }

                    int loopPasses = 0;
                    for (int i = startIndex; i <= endIndex; i++)
                    {
                        TreeViewItem item = parentControl.Items[i] as TreeViewItem;
                        if (item != null)
                        {
                            string itemLabel = item.Header as string;
                            if (itemLabel != null && itemLabel.ToLower().StartsWith(keyChar))
                            {
                                itemToNavigateTo = item;
                                break;
                            }
                        }
                        else
                        {
                            TreeViewDataItem dataItem = parentControl.Items[i] as TreeViewDataItem;
                            if (dataItem != null && dataItem.Label.ToLower().StartsWith(keyChar))
                            {
                                itemToNavigateTo = parentControl.ItemContainerGenerator.ContainerFromIndex(i) as TreeViewItem;
                                break;
                            }
                        }

                        if (i == endIndex)
                        {
                            endIndex = startIndex - 2;
                            startIndex = 0;
                            i = -1;
                            loopPasses++;
                        }

                        // Guard against an endless loop by allowing to restart the loop only once.
                        if (loopPasses > 1)
                        {
                            break;
                        }
                    }
                }
            }

            if (!defaultKeyNav && itemToNavigateTo == null)
            {
                // Failed to determine the item to navigate to.
                return false;
            }

            // TODO: determine the item that will be selected by the key press and send it in the event arguments.
            RoutedPropertyChangedEventArgs<object> args = new RoutedPropertyChangedEventArgs<object>(selectedDataItem, null, PreviewSelectedItemChangedEvent);
            RaiseEvent(args);

            if (args.Handled)
            {
                e.Handled = args.Handled;
                return false;
            }
            else
            {
                // Select and show the item to navigate to.
                // Set the flag to cancel the next selected item changed event to true. This will prevent the TreeItemIsSelectedConverter to raise the PreviewSelectedItemChangedEvent (again).
                this.ShouldCancelNextSelectedItemChangedEvent = true;

                if (defaultKeyNav)
                {
                    return true;
                }
                else if (itemToNavigateTo != null)
                {
                    itemToNavigateTo.IsSelected = true;
                    itemToNavigateTo.BringIntoView();
                    itemToNavigateTo.Focus();

                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Loads the children of a specified tree view  data item using a background thread.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="animationType">Type of the animation.</param>
        /// <param name="continuation">The continuation.</param>
        /// <returns>True if the load was started; otherwise returns false.</returns>
        private bool LoadItemChildrenInBackground(
            TreeViewDataItem item,
            TreeViewItemLoadingAnimation animationType,
            Action continuation)
        {
            /*
             * IMPORTANT: any change in the order of the statements in this method must be done very carefully as it may have
             * unintended consequences.
             */

            if (this.isLoadingItemChildren
                || item == null
                || item.LazyLoad == TreeViewItemLazyLoadMode.None
                || item.AreChildrenLoaded)
            {
                return false;
            }

            try
            {
                item.AutomationItemStatus = TreeViewDataItem.AutomationItemStatuses.Loading;

                this.isLoadingItemChildren = true;

                // Notify that the item loading has started
                if (this.LoadingItem != null)
                {
                    this.LoadingItem(item, new TreeViewItemLoadingEventArgs() { IsLoading = true });
                }

                item.Children.Clear();

                // Show the "Loading ..." item or animation
                CircularProgressControl loadAnimation = new CircularProgressControl();
                loadAnimation.Color = new SolidColorBrush(Colors.Black);
                loadAnimation.HorizontalAlignment = HorizontalAlignment.Center;
                loadAnimation.VerticalAlignment = VerticalAlignment.Center;
                loadAnimation.Margin = new Thickness(2, 2, 2, 2);

                bool iconReplaced = false;
                object iconBackup = null;

                // This timer is used to display the loading item/icon with a bit of delay to eliminate the flickering that appears
                // when the children loading finishes fast, causing the UI to change very fast between loading icon and the normal icon.
                DispatcherTimer loadingIconTimer = new DispatcherTimer();
                loadingIconTimer.Interval = new TimeSpan(0, 0, 0, 0, 200);
                loadingIconTimer.Tick += delegate(object sender, EventArgs e)
                {
                    if (animationType == TreeViewItemLoadingAnimation.LoadingIcon)
                    {
                        iconBackup = item.IconResourceKey;
                        item.IconResourceKey = loadAnimation;
                    }
                    else
                    {
                        var loadingChildrenItem = new TreeViewDataItem() { IconResourceKey = loadAnimation, Label = LocalizedResources.General_Loading + "..." };
                        item.Children.Add(loadingChildrenItem);
                    }

                    // The timer must trigger only once so disable it.
                    iconReplaced = true;
                    loadingIconTimer.Stop();
                };
                loadingIconTimer.Start();

                ICollection<TreeViewDataItem> newChildren = null;
                BackgroundWorker worker = new BackgroundWorker();
                worker.DoWork += (s, e) =>
                {
                    newChildren = item.LoadChildren();
                };

                worker.RunWorkerCompleted += (s, e) =>
                {
                    try
                    {
                        // Remove the loading icon/item.
                        loadingIconTimer.Stop();
                        if (iconReplaced && animationType == TreeViewItemLoadingAnimation.LoadingIcon)
                        {
                            item.IconResourceKey = iconBackup;
                        }

                        item.Children.Clear();

                        // If an error occurs during children load add the dummy item back into the children collection in order to make it possible
                        // to expand the item again, to trigger the children loading.
                        // TODO: we need to better handle the case when there are no children; maybe show an empty node under it and when expanding it again retry the load.
                        bool wereChildrenLoaded = e.Error == null /*&& newChildren != null && newChildren.Count > 0*/;
                        item.AreChildrenLoaded = wereChildrenLoaded;

                        if (wereChildrenLoaded)
                        {
                            // Populate the children collection with the new children.
                            if (newChildren != null)
                            {
                                item.Children.AddRange(newChildren);
                            }
                        }
                        else
                        {
                            // Collapse the item as its load has failed.
                            item.IsExpanded = false;
                        }
                    }
                    finally
                    {
                        this.isLoadingItemChildren = false;
                        item.AutomationItemStatus = TreeViewDataItem.AutomationItemStatuses.Loaded;
                    }

                    // Notify that the item loading has finished.
                    if (this.LoadingItem != null)
                    {
                        this.LoadingItem(item, new TreeViewItemLoadingEventArgs() { IsLoading = false });
                    }

                    // Trigger the ChildrenLoaded event of the item.
                    item.RaiseChildrenLoadedEvent();

                    // Execute the custom action, chained after the item's loading.
                    if (continuation != null)
                    {
                        continuation();
                    }

                    if (e.Error != null)
                    {
                        throw e.Error;
                    }
                };

                worker.RunWorkerAsync();

                return true;
            }
            catch
            {
                this.isLoadingItemChildren = false;
                throw;
            }
        }

        /// <summary>
        /// Finds the container of the specified data item. The data item can be on any level of the hierarchy.
        /// </summary>
        /// <param name="item">The data item.</param>
        /// <returns>The container of null if it is not found.</returns>
        private TreeViewItem FindItemContainer(TreeViewDataItem item)
        {
            List<TreeViewDataItem> itemsQueue = new List<TreeViewDataItem>();

            TreeViewDataItem temp = item;
            while (temp != null)
            {
                itemsQueue.Add(temp);
                temp = temp.Parent;
            }

            itemsQueue.Reverse();

            TreeViewItem itemContainer = null;
            var currentContainerGenerator = this.ItemContainerGenerator;
            while (itemsQueue.Count > 0 && currentContainerGenerator != null)
            {
                var currentItem = itemsQueue[0];
                itemsQueue.Remove(currentItem);
                var currentItemContainer = currentContainerGenerator.ContainerFromItem(currentItem) as TreeViewItem;

                if (currentItemContainer == null)
                {
                    break;
                }
                else if (itemsQueue.Count == 0)
                {
                    itemContainer = currentItemContainer;
                }
                else
                {
                    currentContainerGenerator = currentItemContainer.ItemContainerGenerator;
                }
            }

            return itemContainer;
        }

        /// <summary>
        /// Selects the tree item provided and clears all the old selections.
        /// </summary>
        /// <param name="itemToSelect">The tree item to select.</param>
        private void SelectTreeItem(TreeViewDataItem itemToSelect)
        {
            var selectedItems = (ObservableCollection<object>)GetValue(SelectedItemsPropertyKey.DependencyProperty);

            // Unselect and clear the old selections
            foreach (TreeViewDataItem item in this.SelectedItems)
            {
                this.SetAdditionallySelected(item, false);
            }

            selectedItems.Clear();

            if (itemToSelect != null)
            {
                // Add the item to select to selected items collection
                selectedItems.Add(itemToSelect);
                this.SetAdditionallySelected(itemToSelect, true);
            }
        }

        /// <summary>
        /// Adds an item to the selected items collection.
        /// </summary>
        /// <param name="viewItem">The visual item of the tree item to add.</param>
        /// <param name="dataItem">The tree item to add.</param>
        /// <param name="toggleSelection">A value indicating whether to toggle the item selection.</param>
        private void AddItemToMultiSelection(TreeViewItem viewItem, TreeViewDataItem dataItem, bool toggleSelection)
        {
            TreeViewDataItem firstDataItem = null;
            var selectedItems = (ObservableCollection<object>)GetValue(SelectedItemsPropertyKey.DependencyProperty);
            if (selectedItems.Count > 0)
            {
                firstDataItem = selectedItems[0] as TreeViewDataItem;
            }

            if (firstDataItem == null || (dataItem.Parent == firstDataItem.Parent && firstDataItem.AllowMultipleSelection))
            {
                if (this.SelectedItems.Contains(dataItem))
                {
                    if (dataItem != this.SelectedItem && toggleSelection)
                    {
                        selectedItems.Remove(dataItem);
                        TreeViewItemBehavior.SetIsAdditionallySelected(viewItem, false);
                    }
                }
                else
                {
                    selectedItems.Add(dataItem);
                    TreeViewItemBehavior.SetIsAdditionallySelected(viewItem, true);
                }
            }
        }

        /// <summary>
        /// Adds the tree items from the range specified by the parameters.
        /// </summary>
        /// <param name="firstItem">The item from the start of the range.</param>
        /// <param name="lastItem">The item from the end of the range.</param>
        /// <remarks>The parameter items must have same parent in order to work.</remarks>
        private void AddItemsRangeToMultiSelection(TreeViewItem firstItem, TreeViewItem lastItem)
        {
            var firstItemParent = UIHelper.FindParent<TreeViewItem>(firstItem);
            var lastItemParent = UIHelper.FindParent<TreeViewItem>(firstItem);
            if (firstItemParent != lastItemParent)
            {
                throw new InvalidOperationException("The items specifying the range must have same parent");
            }

            var lastSelectedItemIndex = firstItemParent.ItemContainerGenerator.IndexFromContainer(lastItem);
            var clickedItemIndex = firstItemParent.ItemContainerGenerator.IndexFromContainer(firstItem);

            if (lastSelectedItemIndex < 0 || clickedItemIndex < 0)
            {
                return;
            }

            var startIndex = Math.Min(lastSelectedItemIndex, clickedItemIndex);
            var endIndex = Math.Max(lastSelectedItemIndex, clickedItemIndex);

            for (int i = startIndex; i <= endIndex; i++)
            {
                var itemToSelect = firstItemParent.ItemContainerGenerator.ContainerFromIndex(i) as TreeViewItem;
                if (itemToSelect != null)
                {
                    var dataItemToSelect = itemToSelect.DataContext as TreeViewDataItem;
                    if (dataItemToSelect != null && !dataItemToSelect.IsSelected && dataItemToSelect.AllowMultipleSelection)
                    {
                        this.AddItemToMultiSelection(itemToSelect, dataItemToSelect, false);
                    }
                }
            }
        }

        #endregion Helpers
    }
}