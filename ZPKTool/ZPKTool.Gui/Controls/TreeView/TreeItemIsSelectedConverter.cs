﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using ZPKTool.Gui.Controls;

namespace ZPKTool.Gui.Controls
{
    /// <summary>
    /// The tree item is selected converter.
    /// </summary>
    [ValueConversion(typeof(bool), typeof(bool))]
    [ValueConversion(typeof(TreeView), typeof(bool))]
    [ValueConversion(typeof(TreeViewDataItem), typeof(bool))]
    public class TreeItemIsSelectedConverter : IMultiValueConverter
    {
        /// <summary>
        /// Converts source values to a value for the binding target. The data binding engine calls this method when it propagates the values from source bindings to the binding target.
        /// </summary>
        /// <param name="values">The array of values that the source bindings in the <see cref="T:System.Windows.Data.MultiBinding"/> produces. The value <see cref="F:System.Windows.DependencyProperty.UnsetValue"/> indicates that the source binding has no value to provide for conversion.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <remarks>The input values must contain 3 items: The IsSelected property, the target TreeView and the source TreeViewDataItem.</remarks>
        /// <returns>
        /// A converted value.If the method returns null, the valid null value is used.A return value of <see cref="T:System.Windows.DependencyProperty"/>.<see cref="F:System.Windows.DependencyProperty.UnsetValue"/> indicates that the converter did not produce a value, and that the binding will use the <see cref="P:System.Windows.Data.BindingBase.FallbackValue"/> if it is available, or else will use the default value.A return value of <see cref="T:System.Windows.Data.Binding"/>.<see cref="F:System.Windows.Data.Binding.DoNothing"/> indicates that the binding does not transfer the value or use the <see cref="P:System.Windows.Data.BindingBase.FallbackValue"/> or the default value.
        /// </returns>
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var result = Binding.DoNothing;

            if (values == null || values.Length < 3)
            {
                throw new InvalidOperationException("The number of the input parameters in the TreeItemIsSelectedConverter is less then 3.");
            }

            if (values[0] != DependencyProperty.UnsetValue && values[1] != DependencyProperty.UnsetValue && values[2] != DependencyProperty.UnsetValue)
            {
                if (!(values[0] is bool))
                {
                    throw new InvalidOperationException("The first parameter in the TreeItemIsSelectedConverter is not a Bool value.");
                }

                TreeViewEx treeViewEx = values[1] as TreeViewEx;
                if (treeViewEx == null)
                {
                    throw new InvalidOperationException("The second parameter in the TreeItemIsSelectedConverter is not a TreeViewEx value.");
                }

                TreeViewDataItem newSelectedItem = values[2] as TreeViewDataItem;
                if (newSelectedItem == null)
                {
                    throw new InvalidOperationException("The third parameter in the TreeItemIsSelectedConverter is not a TreeViewDataItem value.");
                }

                bool selected = (bool)values[0];
                result = selected;

                if (selected && !treeViewEx.ShouldCancelNextSelectedItemChangedEvent)
                {
                    treeViewEx.ShouldCancelNextSelectedItemChangedEvent = true;

                    // Fire the PreviewSelectedItemChangedEvent to give the possibility to cancel the selection.
                    RoutedPropertyChangedEventArgs<object> args = new RoutedPropertyChangedEventArgs<object>(treeViewEx.SelectedItem, newSelectedItem, TreeViewEx.PreviewSelectedItemChangedEvent);
                    treeViewEx.RaiseEvent(args);

                    if (args.Handled)
                    {
                        // Revert the selection if the SelectedItemChanged event was cancelled because the TreeViewDataItem.IsSelected is already set to true,  
                        // this is what triggered this converter in the first place.
                        newSelectedItem.IsSelected = false;
                        result = false;
                    }
                }

                if (selected)
                {
                    treeViewEx.ShouldCancelNextSelectedItemChangedEvent = false;
                }
            }

            return result;
        }

        /// <summary>
        /// Converts a binding target value to the source binding values.
        /// </summary>
        /// <param name="value">The value that the binding target produces.</param>
        /// <param name="targetTypes">The array of types to convert to. The array length indicates the number and types of values that are suggested for the method to return.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// An array of values that have been converted from the target value back to the source values.
        /// </returns>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            return new[] { value, Binding.DoNothing, Binding.DoNothing };
        }
    }
}
