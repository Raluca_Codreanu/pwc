﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.Controls
{
    /// <summary>
    /// Contains event data for the <see cref="EventHandler"/>  event.
    /// </summary>
    public class SelectedItemsChangedEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SelectedItemsChangedEventArgs"/> class.
        /// </summary>
        /// <param name="selectedItems">The selected items collection.</param>
        public SelectedItemsChangedEventArgs(IEnumerable<object> selectedItems)
        {
            this.SelectedItems = selectedItems.ToList();
        }

        /// <summary>
        /// Gets the selected items collection.
        /// </summary>
        public IList<object> SelectedItems { get; private set; }
    }
}
