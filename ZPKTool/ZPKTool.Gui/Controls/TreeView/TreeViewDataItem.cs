﻿namespace ZPKTool.Gui.Controls
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.Timers;
    using System.Windows.Media;
    using ZPKTool.Common;
    using ZPKTool.Data;
    using ZPKTool.DataAccess;
    using ZPKTool.Gui.Services;

    /// <summary>
    /// The base class for the data source items of <see cref="ZPKTool.Gui.Controls.TreeViewEx"/>.
    /// </summary>
    public class TreeViewDataItem : ObservableObject
    {
        #region Fields

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The dummy item that is added into the Children collection of an item in order to get the item's expansion thumb to appear before the items actually has children (the thumb does not appear if the item has no children).
        /// Usually used by items whose content is lazy loaded.
        /// </summary>
        private static readonly TreeViewDataItem dummyItem = new DummyDataItem();

        /// <summary>
        /// A value indicating whether this instance is expanded in the Projects Tree.
        /// </summary>
        private bool isExpanded;

        /// <summary>
        /// A value indicating whether this instance is selected in the Projects Tree.
        /// </summary>
        private bool isSelected;

        /// <summary>
        /// The resource key for the image to be used as the menu item's icon.
        /// </summary>
        private object iconResourceKey;

        /// <summary>
        /// The icon displayed in the top right corner of the item
        /// </summary>
        private ImageSource topRightIcon;

        /// <summary>
        /// The icon displayed in the bottom right corner of the item.
        /// </summary>
        private ImageSource bottomRightIcon;

        /// <summary>
        /// The tooltip for the bottom right icon.
        /// </summary>
        private string bottomRightIconTooltip;

        /// <summary>
        /// The text displayed by the item.
        /// </summary>
        private string label;

        /// <summary>
        /// The tool tip displayed when hovering the mouse over the item.
        /// </summary>
        private object toolTip;

        /// <summary>
        /// A value indicating whether the item is read-only.
        /// </summary>
        private bool readOnly;

        /// <summary>
        /// The id of the group to which this item belongs. 
        /// </summary>
        private int groupId;

        /// <summary>
        /// A number that defines the order of items in a group.
        /// </summary>
        private int orderId;

        /// <summary>
        /// The data object backing this tree item.
        /// </summary>
        private object dataObject;

        /// <summary>
        /// A value indicating the database from which the data object was or should be obtained.
        /// </summary>
        private DbIdentifier dataObjectSource;

        /// <summary>
        ///  The data source manager used to load the data object of this item.
        /// </summary>
        private IDataSourceManager dataObjectContext;

        /// <summary>
        /// A value indicating whether to release this item's data object when the item is collapsed and no longer selected.
        /// </summary>
        private bool autoReleaseUnderlyingData;

        /// <summary>
        /// Indicated the way in which to lazy load the item's children.
        /// </summary>
        private TreeViewItemLazyLoadMode lazyLoad = TreeViewItemLazyLoadMode.None;

        /// <summary>
        /// A value indicating whether the children of this item are loaded.
        /// </summary>
        private bool areChildrenLoaded = true;

        /// <summary>
        /// A collection of System.ComponentModel.SortDescription objects that describe how this items's children are sorted in the view.
        /// </summary>
        private SortDescriptionCollection sortDescriptions;

        /// <summary>
        /// The item's parent.
        /// </summary>
        private TreeViewDataItem parent;

        /// <summary>
        /// the instance's status that is transmitted to automation frameworks.
        /// </summary>
        private string automationItemStatus;

        /// <summary>
        /// The item's automation id.
        /// </summary>
        private string automationId;

        #endregion Fields

        /// <summary>
        /// Initializes a new instance of the <see cref="TreeViewDataItem"/> class.
        /// </summary>
        public TreeViewDataItem()
        {
            this.AllowChildrenMultipleSelection = false;
            this.AutomationItemStatus = AutomationItemStatuses.Loaded;
            this.Children = new DispatchedObservableCollection<TreeViewDataItem>();
            this.Children.CollectionChanged += this.OnChildrenCollectionChanged;

            this.SortDescriptions = new SortDescriptionCollection();
            this.SortDescriptions.Add(new SortDescription("GroupId", ListSortDirection.Ascending));
            this.SortDescriptions.Add(new SortDescription("OrderId", ListSortDirection.Ascending));
            this.SortDescriptions.Add(new SortDescription("Label", ListSortDirection.Ascending));
            this.AutomationId = string.Empty;

            this.IsDataObjectReleased = false;
            this.AllowMultipleSelection = false;
            this.LastUnselectAndCollapseDateTime = DateTime.Now;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TreeViewDataItem" /> class.
        /// </summary>
        /// <param name="dataService">The data service.</param>
        public TreeViewDataItem(IProjectsExplorerDataService dataService)
            : this()
        {
            this.DataService = dataService;
        }

        #region Events

        /// <summary>
        /// Occurs when the children of this item have been loaded.
        /// This event is not triggered if lazy loading is disabled for the item.
        /// </summary>
        public event Action<TreeViewDataItem> ChildrenLoaded;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether this instance is expanded in the Projects Tree.
        /// </summary>        
        public bool IsExpanded
        {
            get
            {
                return this.isExpanded;
            }

            set
            {
                if (this.isExpanded != value)
                {
                    // All the parent items up to root need to be expanded when expanding an item.
                    // For example when selecting and not expanding a project it's not possible to navigate down its hierarchy until the project is expanded.
                    if (this.Parent != null
                        && value == true)
                    {
                        this.Parent.IsExpanded = true;
                    }

                    this.isExpanded = value;
                    this.OnPropertyChanged(() => this.IsExpanded);
                    this.OnIsExpandedChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is selected in the Projects Tree.
        /// </summary>
        public bool IsSelected
        {
            get
            {
                return this.isSelected;
            }

            set
            {
                if (this.isSelected != value)
                {
                    this.isSelected = value;
                    this.OnPropertyChanged(() => this.IsSelected);

                    if (!this.IsExpanded
                        && !this.IsSelected)
                    {
                        this.LastUnselectAndCollapseDateTime = DateTime.Now;
                    }

                    this.OnIsSelectedChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the resource key for the image to be used as the menu item's icon.
        /// This key should point to a resource containing a <see cref="System.Windows.Media.ImageSource"/> instance.
        /// </summary>        
        public object IconResourceKey
        {
            get { return this.iconResourceKey; }
            set { this.SetProperty(ref this.iconResourceKey, value, () => this.IconResourceKey); }
        }

        /// <summary>
        /// Gets or sets the icon displayed in the top right corner of the item.
        /// </summary>
        public ImageSource TopRightIcon
        {
            get { return this.topRightIcon; }
            set { this.SetProperty(ref this.topRightIcon, value, () => this.TopRightIcon); }
        }

        /// <summary>
        /// Gets or sets the icon displayed in the bottom right corner of the item.
        /// </summary>        
        public ImageSource BottomRightIcon
        {
            get { return this.bottomRightIcon; }
            set { this.SetProperty(ref this.bottomRightIcon, value, () => this.BottomRightIcon); }
        }

        /// <summary>
        /// Gets or sets the tooltip for the bottom right icon.
        /// </summary>        
        public string BottomRightIconTooltip
        {
            get { return this.bottomRightIconTooltip; }
            set { this.SetProperty(ref this.bottomRightIconTooltip, value, () => this.BottomRightIconTooltip); }
        }

        /// <summary>
        /// Gets or sets the text displayed by the item.
        /// </summary>        
        public string Label
        {
            get { return this.label; }
            set { this.SetProperty(ref this.label, value, () => this.Label); }
        }

        /// <summary>
        /// Gets or sets the tool tip displayed when hovering the mouse over the item.
        /// </summary>        
        public object ToolTip
        {
            get { return this.toolTip; }
            set { this.SetProperty(ref this.toolTip, value, () => this.ToolTip); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the item is read-only.
        /// </summary>        
        public bool ReadOnly
        {
            get
            {
                return this.readOnly;
            }

            set
            {
                if (this.readOnly != value)
                {
                    this.readOnly = value;
                    this.OnPropertyChanged(() => this.ReadOnly);

                    // Propagate the value down the hierarchy.
                    foreach (var child in this.Children)
                    {
                        child.ReadOnly = this.ReadOnly;
                    }
                }
            }
        }

        /// <summary>
        /// Gets the children items of this item.
        /// </summary>
        public DispatchedObservableCollection<TreeViewDataItem> Children { get; private set; }

        /// <summary>
        /// Gets or sets the way in which to lazy load the item's children.
        /// The default value is None which means that the children should be created at the same time as the item.
        /// </summary>                
        public TreeViewItemLazyLoadMode LazyLoad
        {
            get
            {
                return this.lazyLoad;
            }

            protected set
            {
                if (this.lazyLoad != value)
                {
                    this.lazyLoad = value;

                    // If turning off lazy loading, remove the dummy item. If turning it on while the children are not loaded, add the dummy item.
                    if (this.lazyLoad == TreeViewItemLazyLoadMode.None)
                    {
                        this.Children.Remove(TreeViewDataItem.dummyItem);
                    }
                    else if (!this.AreChildrenLoaded)
                    {
                        this.AddDummyItem();
                    }

                    this.OnPropertyChanged(() => this.LazyLoad);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the children of this item are loaded.
        /// </summary>                        
        public bool AreChildrenLoaded
        {
            get { return this.areChildrenLoaded; }
            set { this.SetProperty(ref this.areChildrenLoaded, value, () => this.AreChildrenLoaded); }
        }

        /// <summary>
        /// Gets or sets the data object backing this tree item.
        /// </summary>        
        public object DataObject
        {
            get { return this.dataObject; }
            set { this.SetProperty(ref this.dataObject, value, () => this.DataObject); }
        }

        /// <summary>
        /// Gets or sets a value indicating the database from which the data object was or should be obtained.
        /// </summary>                
        public DbIdentifier DataObjectSource
        {
            get { return this.dataObjectSource; }
            protected set { this.SetProperty(ref this.dataObjectSource, value, () => this.DataObjectSource); }
        }

        /// <summary>
        /// Gets or sets the data source manager used to load the data object of this item.        
        /// </summary>
        public IDataSourceManager DataObjectContext
        {
            get
            {
                if (this.dataObjectContext != null)
                {
                    return this.dataObjectContext;
                }
                else if (this.Parent != null)
                {
                    return this.Parent.DataObjectContext;
                }
                else
                {
                    return null;
                }
            }

            protected set
            {
                this.SetProperty(ref this.dataObjectContext, value, () => this.DataObjectContext);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to release this item's data object when the item is collapsed and no longer selected.
        /// </summary>                
        public bool AutoReleaseUnderlyingData
        {
            get { return this.autoReleaseUnderlyingData; }
            protected set { this.SetProperty(ref this.autoReleaseUnderlyingData, value, () => this.AutoReleaseUnderlyingData); }
        }

        /// <summary>
        /// Gets or sets the id of the group to which this item belongs. Usually used in grouping the children of an item.
        /// </summary>                
        public int GroupId
        {
            get { return this.groupId; }
            set { this.SetProperty(ref this.groupId, value, () => this.GroupId); }
        }

        /// <summary>
        /// Gets or sets a number that defines the order of items in a group. Used in sorting.
        /// </summary>        
        public int OrderId
        {
            get { return this.orderId; }
            protected set { this.SetProperty(ref this.orderId, value, () => this.OrderId); }
        }

        /// <summary>
        /// Gets a collection of System.ComponentModel.SortDescription objects that describe how this items's children are sorted in the view.
        /// </summary>        
        public SortDescriptionCollection SortDescriptions
        {
            get { return this.sortDescriptions; }
            private set { this.SetProperty(ref this.sortDescriptions, value, () => this.SortDescriptions); }
        }

        /// <summary>
        /// Gets the item's parent.
        /// </summary>                                
        public TreeViewDataItem Parent
        {
            get { return this.parent; }
            private set { this.SetProperty(ref this.parent, value, () => this.Parent); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this item allows multiple children to be selected at once.
        /// <para />
        /// The default value is false.
        /// </summary>        
        public bool AllowChildrenMultipleSelection { get; set; }

        /// <summary>
        /// Gets or sets the instance's status that is transmitted to automation frameworks.
        /// <para />
        /// The value "Loading" informs that this instance is currently loading its children and the value "Loaded" informs that its children have been loaded.
        /// </summary>
        public string AutomationItemStatus
        {
            get { return this.automationItemStatus; }
            set { this.SetProperty(ref this.automationItemStatus, value, () => this.AutomationItemStatus); }
        }

        /// <summary>
        /// Gets or sets the instance's automation id.        
        /// </summary>
        public string AutomationId
        {
            get { return this.automationId; }
            set { this.SetProperty(ref this.automationId, value, () => this.AutomationId); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the data object is released or not
        /// When data object is released it is replaced by a dummy data object and needs to be reloaded
        /// </summary>
        public bool IsDataObjectReleased { get; set; }

        /// <summary>
        /// Gets a value indicating whether the tree items children are currently refreshing
        /// If a child of the current tree item is selected and it's needed to refresh the current tree item children
        /// then the tree will recreate the children nodes and automatically select the node parent (for a short time
        /// the selected children does not exist during recreation) and when selected children is created it will be reselected
        /// Setting this flag to True the <see cref="TreeViewEx"/> tree will not automatically select the parent node while the selected child is recreated
        /// </summary>
        public bool ChildrenAreRefreshing { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether this item can be selected along other tree nodes.
        /// <para />
        /// The default value is false.
        /// </summary>
        public bool AllowMultipleSelection { get; set; }

        /// <summary>
        /// Gets the date time when this item was unselected and collapsed for the last time.
        /// </summary>
        public DateTime LastUnselectAndCollapseDateTime { get; private set; }

        /// <summary>
        /// Gets the data service.
        /// </summary>
        public IProjectsExplorerDataService DataService { get; private set; }

        #endregion Properties

        /// <summary>
        /// Determines whether this item has a child representing a given data object.
        /// The search is performed only in this item's Children collection, it does not search its entire sub-tree.
        /// </summary>
        /// <param name="dataObject">The data object for which to search a child.</param>
        /// <returns>
        /// True if this item contains a child for <paramref name="dataObject"/>, false otherwise.
        /// </returns>
        public bool HasChildForObject(IIdentifiable dataObject)
        {
            foreach (var child in this.Children)
            {
                if (child.DataObject == dataObject)
                {
                    // Compare by reference
                    return true;
                }
                else
                {
                    // Compare by id
                    IIdentifiable identifiableChild = child.DataObject as IIdentifiable;
                    if (identifiableChild != null && dataObject.Guid == identifiableChild.Guid)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Finds the item corresponding to a specified data object in the sub-tree of this item.
        /// The matching is done firstly by reference and secondly by id, if the data object is <see cref="IIdentifiable"/>.
        /// </summary>
        /// <param name="dataObject">The data object.</param>
        /// <returns>
        /// The child item or null if none was found.
        /// </returns>
        public TreeViewDataItem FindItemForDataObject(object dataObject)
        {
            if (dataObject == null)
            {
                return null;
            }

            // Search the item's sub-tree level by level by adding the items to search on each level to an inspection queue.
            TreeViewDataItem foundItem = null;
            Queue<TreeViewDataItem> inspectionQueue = new Queue<TreeViewDataItem>();
            inspectionQueue.Enqueue(this);
            while (inspectionQueue.Count > 0)
            {
                var crtItem = inspectionQueue.Dequeue();

                // 1st check by reference for non-identifiable objects.
                if (dataObject == crtItem.DataObject)
                {
                    foundItem = crtItem;
                    break;
                }
                else
                {
                    // 2nd, check by id if the data objects are IIdentifiable.                    
                    IIdentifiable identifiableDataObject = dataObject as IIdentifiable;
                    IIdentifiable underlyingObject = crtItem.DataObject as IIdentifiable;
                    if (identifiableDataObject != null
                        && underlyingObject != null
                        && identifiableDataObject.Guid == underlyingObject.Guid)
                    {
                        foundItem = crtItem;
                        break;
                    }
                }

                // The inspected item was not the searched one so we add its children to the inspection queue, to be searched.
                inspectionQueue.Enqueue(crtItem.Children);
            }

            return foundItem;
        }

        /// <summary>
        /// Finds the item corresponding to a specified data object in the sub-tree of this item.
        /// The search is performed by id.
        /// </summary>
        /// <param name="dataObjectId">The id of the data object.</param>
        /// <returns>
        /// The child item or null if none was found.
        /// </returns>
        public TreeViewDataItem FindItemForDataObject(Guid dataObjectId)
        {
            if (dataObjectId == Guid.Empty)
            {
                return null;
            }

            // Search the item's sub-tree level by level by adding the items to search on each level to an inspection queue.
            TreeViewDataItem foundItem = null;
            Queue<TreeViewDataItem> inspectionQueue = new Queue<TreeViewDataItem>();
            inspectionQueue.Enqueue(this);
            while (inspectionQueue.Count > 0)
            {
                var crtItem = inspectionQueue.Dequeue();

                IIdentifiable underlyingObject = crtItem.DataObject as IIdentifiable;
                if (underlyingObject != null && underlyingObject.Guid == dataObjectId)
                {
                    foundItem = crtItem;
                    break;
                }

                // The inspected item was not the searched one so we add its children to the inspection queue, to be searched.
                inspectionQueue.Enqueue(crtItem.Children);
            }

            return foundItem;
        }

        /// <summary>
        /// Visually selects this item by expanding all its parents and then actually selecting it.
        /// </summary>
        /// <remarks>
        /// Bringing it into view is automatically done on the view side.
        /// </remarks>        
        public void Select()
        {
            this.ExpandAllParents();
            this.IsSelected = true;
        }

        /// <summary>
        /// Raises the ChildrenLoaded event.
        /// </summary>
        public void RaiseChildrenLoadedEvent()
        {
            var handler = this.ChildrenLoaded;
            if (handler != null)
            {
                handler(this);
            }
        }

        /// <summary>
        /// Refreshes this item from the underlying data object, including its children collection.
        /// </summary>
        public void Refresh()
        {
            // Important: if additional base logic is needed in the "refresh", add it into the RefreshInternal method instead.
            // TODO: Why is RefreshInternal necessary? this method could be virtual and RefreshInternal deleted.
            RefreshInternal();
        }

        /// <summary>
        /// Recreates the view that displays the children of this item, also reapplying the sorting rules.
        /// The Refresh method of each child is called before recreating the view.
        /// <para />
        /// Recreating the view is expensive so use this method only when the children view must absolutely be recreated, for example when the child order
        /// must be refreshed (re-sort). Do not use it when a call to the Refresh method is enough.
        /// </summary>
        public void RefreshChildrenView()
        {
            foreach (var child in this.Children)
            {
                child.Refresh();
            }

            var action = new Action(() =>
                {
                    var view = System.Windows.Data.CollectionViewSource.GetDefaultView(this.Children);
                    if (view != null)
                    {
                        // Mark the current tree item before refreshing its children to announce the tree view 
                        // to not select the current tree item while recreating its children
                        try
                        {
                            this.ChildrenAreRefreshing = true;
                            view.Refresh();
                        }
                        finally
                        {
                            this.ChildrenAreRefreshing = false;
                        }
                    }
                });

            if (App.Current != null)
            {
                App.Current.Dispatcher.BeginInvoke(action);
            }
            else
            {
                action();
            }
        }

        /// <summary>
        /// Implements the children loading logic for items that support lazy loading.
        /// This method is executed asynchronously so it should not manipulate UI elements.
        /// </summary>        
        /// <returns>
        /// The loaded children.
        /// </returns>
        public virtual ICollection<TreeViewDataItem> LoadChildren()
        {
            return null;
        }

        /// <summary>
        /// Called after the underlying data object has been released.
        /// </summary>
        /// <param name="releasedData">A reference to the released data object.</param>
        protected virtual void OnUnderlyingDataReleased(object releasedData)
        {
        }

        /// <summary>
        /// Releases the data context by setting it to null.
        /// </summary>
        public void ReleaseDataContext()
        {
            this.DataObjectContext = null;
        }

        /// <summary>
        /// Refreshes this item. This method can be overridden in the derived classes if necessary.
        /// </summary>
        protected virtual void RefreshInternal()
        {
            this.Label = EntityUtils.GetEntityName(this.DataObject);
        }

        /// <summary>
        /// Called when the IsExpanded property has changed.
        /// </summary>
        private void OnIsExpandedChanged()
        {
            this.ReleaseUnderlyingDataIfNecessary();

            // If a lazy loaded item is collapsed without having its children loaded add the dummy item so it can be expanded again.
            if (!this.IsExpanded
                && this.LazyLoad != TreeViewItemLazyLoadMode.None
                && !this.AreChildrenLoaded)
            {
                this.AddDummyItem();
            }

            if (!this.IsExpanded
                && !this.IsSelected)
            {
                this.LastUnselectAndCollapseDateTime = DateTime.Now;
            }
        }

        /// <summary>
        /// Called when IsSelected property has changed.
        /// </summary>
        private void OnIsSelectedChanged()
        {
            this.ReleaseUnderlyingDataIfNecessary();
        }

        /// <summary>
        /// Called when the Children collection has changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Collections.Specialized.NotifyCollectionChangedEventArgs"/> instance containing the event data.</param>
        private void OnChildrenCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                // Set this item as the parent of new children.
                foreach (TreeViewDataItem newItem in e.NewItems)
                {
                    if (newItem != TreeViewDataItem.dummyItem)
                    {
                        newItem.Parent = this;
                    }
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                // Set to null the parent of removed children.
                foreach (TreeViewDataItem oldItem in e.OldItems)
                {
                    if (oldItem != TreeViewDataItem.dummyItem)
                    {
                        oldItem.Parent = null;
                    }
                }
            }
        }

        /// <summary>
        /// Releases the underlying data if it is necessary.
        /// </summary>
        private void ReleaseUnderlyingDataIfNecessary()
        {
            if (!this.AutoReleaseUnderlyingData)
            {
                return;
            }

            bool release = false;
            if (this.LazyLoad == TreeViewItemLazyLoadMode.OnExpansion && !this.IsExpanded)
            {
                release = true;
            }
            else if (!this.IsSelected && !this.IsExpanded)
            {
                release = true;
            }

            release = release && !this.IsChildSelected();
            if (!release)
            {
                return;
            }

            // Hold the released object's reference to pass it to the OnUnderlyingDataReleased call.
            object releasedData = this.DataObject;

            // Clear the children
            this.AreChildrenLoaded = false;
            this.Children.Clear();
            this.AddDummyItem();

            this.OnUnderlyingDataReleased(releasedData);
        }

        /// <summary>
        /// Releases the underlying data.
        /// </summary>
        public void ReleaseUnderlyingData()
        {
            this.IsDataObjectReleased = true;

            // Release the the tree item's data (the underlying object and its data context) by setting them to null.
            // Do not Dispose the data context because it may still be used for a short time after the item's data is released.            
            this.DataObjectContext = null;

            // If the DataObject is identifiable it should not be set to null because some navigation logic for the projects tree depends on its id.
            // Instead set it to a new instance of the same type and copy its id to the new instance. This way the "heavy" data object has its reference released
            // and the navigation logic will work.
            var identifiableDataObject = this.DataObject as IIdentifiable;
            if (identifiableDataObject != null)
            {
                var dummyDataObject = (IIdentifiable)Activator.CreateInstance(this.DataObject.GetType());
                dummyDataObject.Guid = identifiableDataObject.Guid;

                // Copy all value properties.
                var copiableDataObj = identifiableDataObject as ICopiable;
                var dummyCopiable = dummyDataObject as ICopiable;
                if (copiableDataObj != null
                    && dummyCopiable != null)
                {
                    copiableDataObj.CopyValuesTo(dummyCopiable);
                }

                this.DataObject = null;
                this.DataObject = dummyDataObject;
            }
        }

        #region Helpers

        /// <summary>
        /// Expands this item's parents, starting at the root of the tree.
        /// </summary>
        private void ExpandAllParents()
        {
            List<TreeViewDataItem> parents = new List<TreeViewDataItem>();
            var parent = this.Parent;
            while (parent != null)
            {
                parents.Insert(0, parent);
                parent = parent.Parent;
            }

            foreach (var item in parents)
            {
                item.IsExpanded = true;
            }
        }

        /// <summary>
        /// Determines whether any child in this item's sub-tree is selected.
        /// </summary>
        /// <returns>
        /// true if a child is selected; otherwise, false.
        /// </returns>
        public bool IsChildSelected()
        {
            Queue<TreeViewDataItem> childrenQueue = new Queue<TreeViewDataItem>(this.Children);
            while (childrenQueue.Count > 0)
            {
                var child = childrenQueue.Dequeue();
                if (child.IsSelected)
                {
                    return true;
                }
                else
                {
                    childrenQueue.Enqueue(child.Children);
                }
            }

            return false;
        }

        /// <summary>
        /// Adds the dummy item into this item's Children.
        /// </summary>
        private void AddDummyItem()
        {
            var children = this.Children;
            if (!children.Contains(TreeViewDataItem.dummyItem))
            {
                children.Add(TreeViewDataItem.dummyItem);
            }
        }

        #endregion Helpers

        #region Inner Classes

        /// <summary>
        /// The statuses transmitted to automation framework by instances of <see cref="TreeViewDataItem"/>
        /// </summary>
        public static class AutomationItemStatuses
        {
            /// <summary>
            /// A value indicating that the <see cref="TreeViewDataItem"/> is loading its children.
            /// </summary>
            public const string Loading = "Loading";

            /// <summary>
            /// A value indicating that the <see cref="TreeViewDataItem"/> has loaded its children.
            /// </summary>
            public const string Loaded = "Loaded";
        }

        /// <summary>
        /// A dummy item used internally to force an item's expansion thumb to appear when it does not have children.
        /// </summary>
        private class DummyDataItem : TreeViewDataItem
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="DummyDataItem"/> class.
            /// </summary>
            public DummyDataItem()
            {
                this.Label = null;
                this.IconResourceKey = null;
                this.TopRightIcon = null;
                this.BottomRightIcon = null;
            }
        }

        #endregion Inner Classes
    }
}
