﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.Controls
{
    /// <summary>
    /// The types of animations that can be used to show that the content of a tree item is loading.
    /// </summary>
    public enum TreeViewItemLoadingAnimation
    {
        /// <summary>
        /// The "loading" is indicated by adding a "loading sub-item" in the item.
        /// This animation is appropriate for items that can have sub-items.
        /// </summary>
        LoadingSubItem = 0,

        /// <summary>
        /// The "loading" is indicated by changing the item's icon into a "loading" animation.
        /// This animation is appropriate for items that can't have sub-items.
        /// </summary>
        LoadingIcon = 1
    }
}
