﻿namespace ZPKTool.Gui.Controls
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Windows;
    using System.Windows.Controls;
    using ZPKTool.Gui.Utils;

    /// <summary>
    /// Exposes attached behaviors that are applied to the TreeViewEx's TreeViewItems.
    /// </summary>
    public static class TreeViewItemBehavior
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        #region Attached dependency properties

        /// <summary>
        /// Using a DependencyProperty as the backing store for IsAdditionallySelected.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty IsAdditionallySelectedProperty =
            DependencyProperty.RegisterAttached("IsAdditionallySelected", typeof(bool), typeof(TreeViewItemBehavior), new UIPropertyMetadata(false));

        /// <summary>
        /// Using a DependencyProperty as the backing store for BringIntoViewWhenSelected.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty BringIntoViewWhenSelectedProperty = DependencyProperty.RegisterAttached(
            "BringIntoViewWhenSelected",
            typeof(bool),
            typeof(TreeViewItemBehavior),
            new UIPropertyMetadata(false, OnBringIntoViewWhenSelectedChanged));

        /// <summary>
        /// Identifies the ToolTipWhenNotFullyVisible dependency property.
        /// </summary>
        public static readonly DependencyProperty ToolTipWhenNotFullyVisibleProperty = DependencyProperty.RegisterAttached(
            "ToolTipWhenNotFullyVisible",
            typeof(object),
            typeof(TreeViewItemBehavior),
            new UIPropertyMetadata(null, OnToolTipWhenNotFullyVisibleChanged));

        /// <summary>
        /// Identifies the TrackMouseEnter attached property. 
        /// </summary>
        public static readonly DependencyProperty TrackMouseEnterProperty = DependencyProperty.RegisterAttached(
            "TrackMouseEnter",
            typeof(bool),
            typeof(TreeViewItemBehavior),
            new UIPropertyMetadata(false, OnTrackMouseEnterChanged));

        #endregion Attached dependency properties

        #region Getters and setters of the attached properties

        /// <summary>
        /// Gets a value indicating whether the attached TreeViewItem is additionally selected.
        /// </summary>
        /// <param name="obj">The attached TreeViewItem.</param>
        /// <returns>True if the TreeViewItem is additionally selected; otherwise, false.</returns>
        public static bool GetIsAdditionallySelected(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsAdditionallySelectedProperty);
        }

        /// <summary>
        /// Sets a value indicating whether the attached TreeViewItem is additionally selected.
        /// </summary>
        /// <param name="obj">The attached TreeViewItem.</param>
        /// <param name="value">If set to true the TreeViewItem should be highlighted.</param>
        public static void SetIsAdditionallySelected(DependencyObject obj, bool value)
        {
            obj.SetValue(IsAdditionallySelectedProperty, value);
        }

        /// <summary>
        /// Gets a value indicating whether to bring into view the attached TreeViewItem when it is selected.
        /// </summary>
        /// <param name="obj">The attached TreeViewItem.</param>
        /// <returns>True if the TreeViewItem should be brought into view when selected; otherwise, false.</returns>
        public static bool GetBringIntoViewWhenSelected(DependencyObject obj)
        {
            return (bool)obj.GetValue(BringIntoViewWhenSelectedProperty);
        }

        /// <summary>
        /// Sets a value indicating whether to bring into view the attached TreeViewItem when it is selected.
        /// </summary>
        /// <param name="obj">The attached TreeViewItem.</param>
        /// <param name="value">if set to true the TreeViewItem should be brought into view when selected</param>
        public static void SetBringIntoViewWhenSelected(DependencyObject obj, bool value)
        {
            obj.SetValue(BringIntoViewWhenSelectedProperty, value);
        }

        /// <summary>
        /// Gets the value to use as tool tip for the tree view item specified by the <paramref name="obj"/> parameter when the item
        /// is not fully visible in the tree view (when partially obscured when horizontally scrolling the tree).
        /// </summary>
        /// <param name="obj">The tree view item.</param>
        /// <returns>The tool tip value.</returns>
        public static object GetToolTipWhenNotFullyVisible(DependencyObject obj)
        {
            return (object)obj.GetValue(ToolTipWhenNotFullyVisibleProperty);
        }

        /// <summary>
        /// Sets the value to use as tool tip for the tree view item specified by the <paramref name="obj"/> parameter when the item
        /// is not fully visible in the tree view (when partially obscured when horizontally scrolling the tree).
        /// </summary>
        /// <param name="obj">The tree view item.</param>
        /// <param name="value">The tool tip's value.</param>
        public static void SetToolTipWhenNotFullyVisible(DependencyObject obj, object value)
        {
            obj.SetValue(ToolTipWhenNotFullyVisibleProperty, value);
        }

        /// <summary>
        /// Gets a value indicating whether to register to the MouseEnter event of the TreeViewItem specified by the <paramref name="obj"/> parameter.
        /// </summary>
        /// <param name="obj">The TreeViewItem instance.</param>
        /// <returns>True if MouseEnter event is tracked; otherwise, false.</returns>
        public static bool GetTrackMouseEnter(DependencyObject obj)
        {
            return (bool)obj.GetValue(TrackMouseEnterProperty);
        }

        /// <summary>
        /// Sets a value indicating whether to register to the MouseEnter event of the TreeViewItem specified by the <paramref name="obj"/> parameter,
        /// and also triggers the registration to the event.
        /// </summary>
        /// <param name="obj">The TreeViewItem.</param>
        /// <param name="value">if set to true, register to the event; otherwise unregister from the event.</param>
        public static void SetTrackMouseEnter(DependencyObject obj, bool value)
        {
            obj.SetValue(TrackMouseEnterProperty, value);
        }

        #endregion Getters and setters of the attached properties

        /// <summary>
        /// Called when BringIntoViewWhenSelected property changed.
        /// </summary>
        /// <param name="depObj">The dependency object for which the property changed.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnBringIntoViewWhenSelectedChanged(DependencyObject depObj, DependencyPropertyChangedEventArgs e)
        {
            TreeViewItem item = depObj as TreeViewItem;
            if (item == null)
            {
                return;
            }

            if (!(e.NewValue is bool))
            {
                return;
            }

            if ((bool)e.NewValue)
            {
                item.Selected += OnTreeViewItemSelected;
            }
            else
            {
                item.Selected -= OnTreeViewItemSelected;
            }
        }

        /// <summary>
        /// Called when the value of the ToolTipWhenNotFullyVisible property has changed.
        /// </summary>
        /// <param name="depObj">The dependency object.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnToolTipWhenNotFullyVisibleChanged(DependencyObject depObj, DependencyPropertyChangedEventArgs e)
        {
            TreeViewItem item = depObj as TreeViewItem;
            if (item == null)
            {
                return;
            }

            if (e.NewValue != null)
            {
                if (item.IsLoaded)
                {
                    AddDefaultToolTipIfNotFullyVisible(item);
                }
                else
                {
                    RoutedEventHandler loadedAction = null;
                    loadedAction = (s1, e1) =>
                         {
                             AddDefaultToolTipIfNotFullyVisible(item);
                             item.Loaded -= loadedAction;
                         };
                    item.Loaded += loadedAction;
                }
            }
        }

        /// <summary>
        /// Called when the TrackMouseEnter property value has changed.
        /// </summary>
        /// <param name="depObj">The dependency object.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnTrackMouseEnterChanged(DependencyObject depObj, DependencyPropertyChangedEventArgs e)
        {
            TreeViewItem item = depObj as TreeViewItem;
            if (item == null)
            {
                return;
            }

            if ((bool)e.NewValue)
            {
                item.MouseEnter += OnTreeViewItemMouseEnter;
            }
            else
            {
                item.MouseEnter -= OnTreeViewItemMouseEnter;
            }
        }

        /// <summary>
        /// Called when the IsSelected property of a TreeViewItem has changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private static void OnTreeViewItemSelected(object sender, RoutedEventArgs e)
        {
            // Only react to the Selected event raised by the TreeViewItem whose IsSelected property was modified.
            // Ignore all ancestors who are merely reporting that a descendant's Selected fired.
            if (sender != e.OriginalSource)
            {
                return;
            }

            TreeViewItem item = e.OriginalSource as TreeViewItem;
            if (item != null)
            {
                item.BringIntoView();
            }
        }

        /// <summary>
        /// Called when the MouseEnter event of a tree view item has been triggered.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseEventArgs"/> instance containing the event data.</param>
        private static void OnTreeViewItemMouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            var item = sender as TreeViewItem;
            if (item != null)
            {
                AddDefaultToolTipIfNotFullyVisible(item);
            }
        }

        /// <summary>
        /// Adds to the specified item the default tooltip specified by the ToolTipWhenNotFullyVisible property if the item is not fully visible in the tree.
        /// </summary>
        /// <param name="item">The item.</param>
        private static void AddDefaultToolTipIfNotFullyVisible(TreeViewItem item)
        {
            if (item == null || item.IsDisconnectedItem())
            {
                return;
            }

            object tooltip = GetToolTipWhenNotFullyVisible(item);
            if (tooltip == null)
            {
                return;
            }

            Action setTooltipIfNecessary = () =>
            {
                if (item.IsDisconnectedItem())
                {
                    return;
                }

                var itemHeader = item.Template.FindName("PART_Header", item) as ContentPresenter;
                if (itemHeader == null)
                {
                    return;
                }

                var parentTree = UIHelper.FindParent<TreeView>(itemHeader);
                if (parentTree == null)
                {
                    return;
                }

                var treeWidth = parentTree.ActualWidth - parentTree.Padding.Left - parentTree.Padding.Right;

                bool contentObscured = false;
                try
                {
                    // Find all text blocks in the the item's header and determine if any of them are obscured. Usually the
                    var textBlocks = UIHelper.FindChildren<TextBlock>(itemHeader);
                    foreach (var textBlock in textBlocks)
                    {
                        // Get the text block's rectangle relative to the tree to determine if it is fully visible or partially obscured due to horizontal scrolling.
                        var transform = textBlock.TransformToAncestor(parentTree);
                        Rect rect = new Rect(transform.Transform(new Point(0, 0)), transform.Transform(new Point(textBlock.ActualWidth, textBlock.ActualHeight)));
                        if (rect.Left < 0 || rect.Right > treeWidth)
                        {
                            contentObscured = true;
                            break;
                        }
                    }
                }
                catch (InvalidOperationException ex)
                {
                    // The transforms may throw exception but it should be ignored because this feature is not so important as to interrupt the user with an error message.
                    log.WarnException("Exception occurred while setting the default tree item tooltip.", ex);
                }

                if (contentObscured)
                {
                    // item.ToolTip = tooltip;
                    item.SetValue(TreeViewItem.ToolTipProperty, tooltip);
                }
                else
                {
                    // TODO: We need to somehow "reset" the tooltip. Setting it to null causes it to be inherited from the parent TreeViewItem.
                    item.ClearValue(TreeViewItem.ToolTipProperty);
                }
            };

            // Execute the logic above with lower priority than Render so the item's size is up to date at the moment the logic is executed.
            // This is necessary because this logic is also triggered by binding and at that moment the item's actual width is not updated to reflect the item's new content length.
            item.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Loaded, setTooltipIfNecessary);
        }
    }
}
