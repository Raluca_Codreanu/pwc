﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using ZPKTool.Controls;
using ZPKTool.Data;

namespace ZPKTool.Gui.Controls
{
    /// <summary>
    /// Interaction logic for PieChart.xaml
    /// </summary>
    public partial class PieChart : UserControl
    {
        /// <summary>
        /// Using a DependencyProperty as the backing store for ItemsSource.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register("ItemsSource", typeof(IEnumerable<LabeledPieChartItem>), typeof(PieChart), new UIPropertyMetadata(null, ItemsSourceChangedCallback));

        /// <summary>
        /// Using a DependencyProperty as the backing store for MeasurementUnitsAdapter.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty MeasurementUnitsAdapterProperty =
            DependencyProperty.Register("MeasurementUnitsAdapter", typeof(UnitsAdapter), typeof(PieChart), new UIPropertyMetadata(null));

        /// <summary>
        /// Using a DependencyProperty as the backing store for chart UiUnit.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty ChartUiUnitProperty =
            DependencyProperty.Register("ChartUiUnit", typeof(Unit), typeof(PieChart), new UIPropertyMetadata(null));

        /// <summary>
        /// Using a DependencyProperty as the backing store for chart UiSymbol.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty ChartUiSymbolProperty =
            DependencyProperty.Register("ChartUiSymbol", typeof(string), typeof(PieChart), new UIPropertyMetadata(string.Empty));

        /// <summary>
        /// The items that will be displayed in the chart.
        /// </summary>
        private IEnumerable<LabeledPieChartItem> internalItemsSource;

        /// <summary>
        /// Initializes a new instance of the <see cref="PieChart"/> class.
        /// </summary>
        public PieChart()
        {
            InitializeComponent();
        }

        /// <summary>
        /// An event handler for the selected item changed event
        /// </summary>
        public event SelectionChangedEventHandler SelectedItemChanged;

        /// <summary>
        /// Gets or sets the items used to compute the items displayed in the chart.
        /// </summary>
        /// <value>The items source.</value>
        public IEnumerable<LabeledPieChartItem> ItemsSource
        {
            get { return (IEnumerable<LabeledPieChartItem>)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        /// <summary>
        /// Gets or sets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter
        {
            get { return (UnitsAdapter)GetValue(MeasurementUnitsAdapterProperty); }
            set { SetValue(MeasurementUnitsAdapterProperty, value); }
        }

        /// <summary>
        /// Gets or sets the chart UI unit.
        /// </summary>
        public Unit ChartUiUnit
        {
            get { return (Unit)GetValue(ChartUiUnitProperty); }
            set { SetValue(ChartUiUnitProperty, value); }
        }

        /// <summary>
        /// Gets or sets the chart's UI symbol.
        /// </summary>
        public string ChartUiSymbol
        {
            get { return (string)GetValue(ChartUiSymbolProperty); }
            set { SetValue(ChartUiSymbolProperty, value); }
        }

        /// <summary>
        /// Invoked when the ItemsSource property value has changed.
        /// </summary>
        /// <param name="d">The dependency object.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void ItemsSourceChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            PieChart chart = d as PieChart;
            if (chart != null)
            {
                List<LabeledPieChartItem> pieChartDataContext = null;
                List<LabeledPieChartItem> pieChartItemSource = null;

                var newSource = e.NewValue as IEnumerable<LabeledPieChartItem>;
                if (newSource != null)
                {
                    pieChartDataContext = new List<LabeledPieChartItem>(newSource);
                    pieChartItemSource = new List<LabeledPieChartItem>();
                }
                else
                {
                    pieChartDataContext = new List<LabeledPieChartItem>();
                    pieChartItemSource = new List<LabeledPieChartItem>();
                }

                ChartPalette.ResetPalette();
                chart.LabeledPieChart.DataContext = pieChartDataContext;

                if (newSource != null)
                {
                    foreach (LabeledPieChartItem item in newSource.Where(p => p.DisplayOnChart))
                    {
                        // If the item doesn't affect the cost it is not displayed
                        if (!item.IsNotAffectingCost)
                        {
                            decimal value = item.ItemValue ?? 0m;

                            var chartItem = new ChartItem(item.ItemName, value, item.DisplayAdditionalInfo, item.ItemAdditionalInfo, item.GroupID);
                            chartItem.ItemColor = item.ItemColor;
                            chartItem.Id = item.Id;
                            chartItem.IsPrimaryInGroup = item.IsPrimaryInGroup;
                            chartItem.UiSymbol = chart.ChartUiSymbol;
                            chartItem.UiUnit = chart.ChartUiUnit;
                            chartItem.MeasurementUnitsAdapter = chart.MeasurementUnitsAdapter;

                            pieChartItemSource.Add(chartItem);
                        }
                    }
                }

                chart.internalItemsSource = pieChartItemSource;

                chart.LabeledPieSeries.ItemsSource = chart.internalItemsSource;
            }
        }

        /// <summary>
        /// Handles the SelectionChanged event from one of the charts.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Controls.SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void LabeledPieSeries_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.SelectedItemChanged != null)
            {
                this.SelectedItemChanged(sender, e);
            }
        }

        /// <summary>
        /// Resets the selection from the chart, non of the items will be selected
        /// </summary>
        public void ResetSelectedItem()
        {
            this.LabeledPieSeries.SelectedItem = null;
        }

        /// <summary>
        /// Selects an item from the chart
        /// </summary>
        /// <param name="item">The new item to select</param>
        public void SelectItem(LabeledPieChartItem item)
        {
            if (item != null && this.LabeledPieSeries.ItemsSource != null)
            {
                // due a bug in windows toolkit if the ItemSource on a SeriesDefinitions is null 
                // you can not select an item else it generates an error
                if (this.internalItemsSource != null)
                {
                    this.LabeledPieSeries.SelectedItem = this.internalItemsSource.FirstOrDefault(p => p.Id == item.Id);
                }
            }
        }

        /// <summary>
        /// Gets the selected item from chart
        /// </summary>
        /// <returns>The selected item</returns>
        public LabeledPieChartItem GetSelectedItem()
        {
            return this.LabeledPieSeries.SelectedItem as LabeledPieChartItem;
        }
    }
}