﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.Controls
{
    /// <summary>
    /// Represents the modes that can be visualized a chart
    /// </summary>
    public enum ChartModes
    {
        /// <summary>
        /// The pie chart only
        /// </summary>
        Pie = 0,

        /// <summary>
        /// The waterfall chart only
        /// </summary>
        Watefall = 1,

        /// <summary>
        /// Both pie and waterfall chart
        /// </summary>
        Both = 2
    }
}
