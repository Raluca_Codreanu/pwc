﻿using System;
using ZPKTool.Controls;

namespace ZPKTool.Gui.Controls
{
    /// <summary>
    /// This class serves as a data input for a waterfall chart
    /// </summary>
    public class ChartItem : LabeledPieChartItem
    {
        /// <summary>
        /// The item name wrapped by an object
        /// This way 2 names can not be equal
        /// </summary>
        private StringWrapper nameWrapped;

        /// <summary>
        /// Initializes a new instance of the <see cref="ChartItem"/> class.
        /// </summary>
        /// <param name="name">The name of the item.</param>
        /// <param name="value">The value of the item.</param>
        /// <param name="id">The unique identifier</param>
        public ChartItem(string name, decimal value, Guid id)
            : base(name, value, id)
        {
            this.nameWrapped = new StringWrapper(name);
            this.PropertyChanged += ChartItem_PropertyChanged;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChartItem"/> class.
        /// </summary>
        /// <param name="name">The name of the item.</param>
        /// <param name="value">The value of the item.</param>
        /// <param name="displayAdditionalInfo">if set to <c>true</c> display additional info item, otherwise don't.</param>
        /// <param name="additonalInfo">The additional info of the item.</param>
        /// <param name="groupID">The group ID.</param>
        public ChartItem(string name, decimal value, bool displayAdditionalInfo, decimal additonalInfo, int groupID)
            : base(name, value, displayAdditionalInfo, additonalInfo, groupID)
        {
            this.nameWrapped = new StringWrapper(name);
            this.PropertyChanged += ChartItem_PropertyChanged;
        }

        /// <summary>
        /// Gets or sets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter { get; set; }

        /// <summary>
        /// Gets or sets the UI Unit.
        /// </summary>
        public Unit UiUnit { get; set; }

        /// <summary>
        /// Gets or sets the UI symbol.
        /// </summary>
        public string UiSymbol { get; set; }

        /// <summary>
        /// Gets or sets the ratio between the item value and another value
        /// </summary>
        public decimal Ratio { get; set; }

        /// <summary>
        /// Gets or sets the item name wrapped
        /// </summary>
        public object NameWrapped
        {
            get
            {
                return this.nameWrapped;
            }

            set
            {
                this.nameWrapped.Name = value as string;
            }
        }

        /// <summary>
        /// Handles the PropertyChanged event of the ChartItem
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void ChartItem_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "ItemName")
            {
                this.NameWrapped = this.ItemName;
            }
        }

        /// <summary>
        /// Used as wrapper for string items
        /// This way 2 string values can not be equal
        /// </summary>
        private class StringWrapper
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="StringWrapper"/> class.
            /// </summary>
            /// <param name="value">The string value</param>
            public StringWrapper(string value)
            {
                this.Name = value;
            }

            /// <summary>
            /// Gets or sets the string object value
            /// </summary>
            public string Name { get; set; }

            /// <summary>
            /// Gets the string value of current class object
            /// </summary>
            /// <returns>The string value of object</returns>
            public override string ToString()
            {
                return this.Name;
            }
        }
    }
}