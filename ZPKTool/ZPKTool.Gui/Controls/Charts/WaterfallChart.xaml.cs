﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.DataVisualization.Charting;
using System.Windows.Documents;
using System.Windows.Media;
using ZPKTool.Common;
using ZPKTool.Controls;
using ZPKTool.Data;
using ZPKTool.Gui.Controls;
using ZPKTool.Gui.Resources;

namespace ZPKTool.Gui.Controls
{
    /// <summary>
    /// Interaction logic for WaterfallChart.xaml
    /// </summary>
    public partial class WaterfallChart : UserControl
    {
        /// <summary>
        /// The minimum font size for the chart column labels
        /// </summary>
        private const double MinimumDataPointLabelFontSize = 8;

        /// <summary>
        /// The maximum font size for the chart column labels
        /// </summary>
        private const double MaximumDataPointLabelFontSize = 11;

        /// <summary>
        /// Using a DependencyProperty as the backing store for TotalValue.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty TotalValueProperty =
            DependencyProperty.Register("TotalValue", typeof(decimal?), typeof(WaterfallChart), new UIPropertyMetadata(null));

        /// <summary>
        /// Using a DependencyProperty as the backing store for ItemsSource.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register("ItemsSource", typeof(IEnumerable<LabeledPieChartItem>), typeof(WaterfallChart), new UIPropertyMetadata(null, ItemsSourceChangedCallback));

        /// <summary>
        /// Using a DependencyProperty as the backing store for MeasurementUnitsAdapter.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty MeasurementUnitsAdapterProperty =
            DependencyProperty.Register("MeasurementUnitsAdapter", typeof(UnitsAdapter), typeof(WaterfallChart), new UIPropertyMetadata(null));

        /// <summary>
        /// Using a DependencyProperty as the backing store for MaxDigitsToDisplay.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty AxesMaxDigitsToDisplayProperty =
            DependencyProperty.Register("AxesMaxDigitsToDisplay", typeof(int), typeof(WaterfallChart), new UIPropertyMetadata(-1, AxesMaxDigitsToDisplayChangedCallback));

        /// <summary>
        /// Using a DependencyProperty as the backing store for AxesMaxDigits.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty AxesMaxDigitsProperty =
            DependencyProperty.RegisterAttached("AxesMaxDigits", typeof(int), typeof(WaterfallChart), new PropertyMetadata(-1));

        /// <summary>
        /// The source for the columns that are visible on chart
        /// </summary>
        private IEnumerable<LabeledPieChartItem> internalItemsSource;

        /// <summary>
        /// Weak event listener for the PropertyChanged notification
        /// </summary>
        private WeakEventListener<PropertyChangedEventArgs> propertyChangedListener;

        /// <summary>
        /// Contains information about the labels displayed on the Ox axis.
        /// </summary>
        private List<AxisOxLabelInfo> axisOxLabels = new List<AxisOxLabelInfo>();

        /// <summary>
        /// Initializes a new instance of the <see cref="WaterfallChart"/> class.
        /// </summary>
        public WaterfallChart()
        {
            InitializeComponent();

            this.propertyChangedListener = new WeakEventListener<PropertyChangedEventArgs>(Settings_PropertyChanged);
            PropertyChangedEventManager.AddListener(UserSettingsManager.Instance, propertyChangedListener, ReflectionUtils.GetPropertyName(() => UserSettingsManager.Instance.ShowPercentagesInWaterfallChart));
        }

        /// <summary>
        /// An event handler for the selected item changed event
        /// </summary>
        public event SelectionChangedEventHandler SelectedItemChanged;

        /// <summary>
        /// Gets or sets the total value.
        /// </summary>
        public decimal? TotalValue
        {
            get { return (decimal?)GetValue(TotalValueProperty); }
            set { SetValue(TotalValueProperty, value); }
        }

        /// <summary>
        /// Gets or sets the items that will be displayed in the chart.
        /// </summary>
        /// <value>The items source.</value>
        public IEnumerable<LabeledPieChartItem> ItemsSource
        {
            get { return (IEnumerable<LabeledPieChartItem>)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        /// <summary>
        /// Gets or sets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter
        {
            get { return (UnitsAdapter)GetValue(MeasurementUnitsAdapterProperty); }
            set { SetValue(MeasurementUnitsAdapterProperty, value); }
        }

        /// <summary>
        /// Gets or sets the maximum number of digits to display for the values on OX axis of the chart 
        /// </summary>
        public int AxesMaxDigitsToDisplay
        {
            get { return (int)GetValue(AxesMaxDigitsToDisplayProperty); }
            set { SetValue(AxesMaxDigitsToDisplayProperty, value); }
        }

        /// <summary>
        /// Gets the maximum digits for the waterfall chart's axis values 
        /// </summary>
        /// <param name="obj">The dependency object.</param>
        /// <returns>The maximum digit value</returns>
        public static int GetAxesMaxDigits(DependencyObject obj)
        {
            return (int)obj.GetValue(AxesMaxDigitsProperty);
        }

        /// <summary>
        /// Sets the maximum digits for the waterfall chart's axis values
        /// </summary>
        /// <param name="obj">The dependency object.</param>
        /// <param name="value">The new value</param>
        public static void SetAxesMaxDigits(DependencyObject obj, int value)
        {
            obj.SetValue(AxesMaxDigitsProperty, value);
        }

        /// <summary>
        /// Invoked when the ItemsSource property value has changed.
        /// </summary>
        /// <param name="d">The dependency object.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void ItemsSourceChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            WaterfallChart chart = d as WaterfallChart;
            if (chart != null)
            {
                List<LabeledPieChartItem> waterfallChartItemSource = null;

                var newSource = e.NewValue as IEnumerable<LabeledPieChartItem>;
                ChartPalette.ResetPalette();

                // Used to calculate the minimum and the maximum of the OY axis
                decimal sum = 0m;
                decimal min = 0m;
                decimal max = 0m;

                if (newSource != null)
                {
                    waterfallChartItemSource = new List<LabeledPieChartItem>();

                    var totalValue = chart.TotalValue ?? 0m;

                    foreach (LabeledPieChartItem item in newSource.Where(p => p.DisplayOnChart))
                    {
                        decimal value = item.ItemValue ?? 0m;
                        sum += value;
                        if (sum < min)
                        {
                            min = sum;
                        }
                        else if (max < sum)
                        {
                            max = sum;
                        }

                        var chartItem = new ChartItem(item.ItemName, value, item.DisplayAdditionalInfo, item.ItemAdditionalInfo, item.GroupID);
                        chartItem.ItemColor = item.ItemColor;
                        chartItem.Id = item.Id;
                        chartItem.IsPrimaryInGroup = item.IsPrimaryInGroup;
                        chartItem.MeasurementUnitsAdapter = chart.MeasurementUnitsAdapter;
                        chartItem.IsNotAffectingCost = item.IsNotAffectingCost;

                        if (totalValue == 0 || chartItem.IsNotAffectingCost)
                        {
                            // If the total value is 0 or if the item is not affecting cost then the cost is 0
                            chartItem.Ratio = 0m;
                        }
                        else
                        {
                            // Calculate the percentage represented by the item's value out of the total value.
                            chartItem.Ratio = value / totalValue;
                        }

                        if (value != 0m)
                        {
                            waterfallChartItemSource.Add(chartItem);
                        }
                    }

                    // If chart item source contains less then 2 items then don't add the Total column
                    if (waterfallChartItemSource.Count > 1)
                    {
                        // Add the total item and announce the waterfall series
                        var totalChartItem = new ChartItem(LocalizedResources.General_Total, totalValue, Guid.NewGuid());
                        totalChartItem.MeasurementUnitsAdapter = chart.MeasurementUnitsAdapter;
                        totalChartItem.Ratio = 1m;
                        waterfallChartItemSource.Add(totalChartItem);
                        chart.WaterfallSeriesChart.HasTotalValue = true;
                    }
                }

                chart.internalItemsSource = waterfallChartItemSource;

                chart.WaterfallSeriesChart.ItemsSource = chart.internalItemsSource;

                // Set the OY axis range
                if (waterfallChartItemSource.Count == 0)
                {
                    chart.OYAxis.Minimum = 0d;
                    chart.OYAxis.Maximum = 1d;
                }
                else
                {
                    min = min * 1.05m;
                    if (max == 0m)
                    {
                        max = Math.Abs(min) * 0.30m;
                    }
                    else
                    {
                        max = max * 1.30m;
                    }

                    chart.OYAxis.Minimum = Convert.ToDouble(min);
                    chart.OYAxis.Maximum = Convert.ToDouble(max);
                }

                // If charts item source is changed then recalculate the width of the Ox axis labels because now we have more or less labels than before.
                chart.RecalculateMaxWidthOfOxAxisLabels();
            }
        }

        /// <summary>
        /// Invoked when the AxesMaxDigitsToDisplay property value has changed.
        /// </summary>
        /// <param name="d">The dependency object.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void AxesMaxDigitsToDisplayChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            WaterfallChart chart = d as WaterfallChart;
            if (chart != null)
            {
                SetAxesMaxDigits(chart.OYAxis, chart.AxesMaxDigitsToDisplay);
            }
        }

        /// <summary>
        /// Handles the SelectionChanged event from one of the charts.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Controls.SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void StackedColumnSeries_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.SelectedItemChanged != null)
            {
                this.SelectedItemChanged(sender, e);
            }
        }

        /// <summary>
        /// Resets the selection from the chart, non of the items will be selected
        /// </summary>
        public void ResetSelectedItem()
        {
            this.WaterfallSeriesChart.SelectedItem = null;
        }

        /// <summary>
        /// Selects an item from the chart
        /// </summary>
        /// <param name="item">The new item to select</param>
        public void SelectItem(LabeledPieChartItem item)
        {
            if (item != null
                && this.WaterfallSeriesChart != null)
            {
                // due a bug in windows toolkit if the ItemSource on a SeriesDefinitions is null 
                // you can not select an item else it generates an error
                if (this.internalItemsSource != null)
                {
                    this.WaterfallSeriesChart.SelectedItem = this.internalItemsSource.FirstOrDefault(p => p.Id == item.Id);
                }
            }
        }

        /// <summary>
        /// Gets the selected item from chart
        /// </summary>
        /// <returns>The selected item</returns>
        public LabeledPieChartItem GetSelectedItem()
        {
            return this.WaterfallSeriesChart.SelectedItem as LabeledPieChartItem;
        }

        /// <summary>
        /// Handles the Loaded event of the root panel of the WaterfallDataPoint's control template.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SizeChangedEventArgs" /> instance containing the event data.</param>
        private void WaterfallDataPoint_Loaded(object sender, RoutedEventArgs e)
        {
            var dataPoint = sender as WaterfallDataPoint;
            if (dataPoint == null)
            {
                return;
            }

            this.RefreshWaterfallColumnLabel(dataPoint);
        }

        /// <summary>
        /// Handles the SizeChanged event of the root panel of the WaterfallDataPoint's control template.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SizeChangedEventArgs" /> instance containing the event data.</param>
        private void WaterfallDataPoint_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            var dataPoint = sender as WaterfallDataPoint;
            if (dataPoint == null)
            {
                return;
            }

            this.RefreshWaterfallColumnLabel(dataPoint);
        }

        /// <summary>
        /// Handles the SizeChanged event of the WaterfallSeriesChart control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SizeChangedEventArgs"/> instance containing the event data.</param>
        private void WaterfallSeriesChart_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            this.RecalculateMaxWidthOfOxAxisLabels();
        }

        /// <summary>
        /// Handles the PropertyChanged event of the Settings
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void Settings_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == ReflectionUtils.GetPropertyName(() => UserSettingsManager.Instance.ShowPercentagesInWaterfallChart))
            {
                var chartDataPoints = this.WaterfallSeriesChart.ChartDataPoints;
                foreach (var item in chartDataPoints)
                {
                    this.RefreshWaterfallColumnLabel(item);
                }
            }
        }

        /// <summary>
        /// Refreshes the waterfall column labels font size and visibility
        /// </summary>
        /// <param name="dataPoint">The data point representing the waterfall charts column</param>
        private void RefreshWaterfallColumnLabel(DataPoint dataPoint)
        {
            StackPanel valueLabelsContainer = dataPoint.Template.FindName("ValueLabelsContainer", dataPoint) as StackPanel;
            if (valueLabelsContainer == null)
            {
                return;
            }

            if (valueLabelsContainer.Children.Count < 2)
            {
                return;
            }

            var absoluteValueLabel = valueLabelsContainer.Children[0] as ExtendedLabelControl;
            var percentageValueLabel = valueLabelsContainer.Children[1] as ExtendedLabelControl;

            if (absoluteValueLabel == null || percentageValueLabel == null)
            {
                return;
            }

            // Refresh the percentage visibility according to the user setting
            if (UserSettingsManager.Instance.ShowPercentagesInWaterfallChart)
            {
                percentageValueLabel.Visibility = Visibility.Visible;
            }
            else
            {
                percentageValueLabel.Visibility = Visibility.Collapsed;
            }

            double absoluteValueWidth = 0;
            double percentageValueWidth = 0;

            // Calculate the labels maximum font size for the space available on the column chart
            // The font size must be between the minimum and maximum font size available
            for (double i = MaximumDataPointLabelFontSize; i >= MinimumDataPointLabelFontSize; i--)
            {
                absoluteValueWidth = this.CalculateWidthAndHeight(absoluteValueLabel, i).Width;
                percentageValueWidth = this.CalculateWidthAndHeight(percentageValueLabel, i).Width;

                double blockWidth = 0;
                if (UserSettingsManager.Instance.ShowPercentagesInWaterfallChart)
                {
                    blockWidth = Math.Max(absoluteValueWidth, percentageValueWidth);
                }
                else
                {
                    blockWidth = absoluteValueWidth;
                }

                if (blockWidth > 0 && blockWidth < dataPoint.Width)
                {
                    absoluteValueLabel.FontSize = i;
                    percentageValueLabel.FontSize = i;
                    break;
                }
            }

            double neededHeight = 0;
            double neededWidth = 0;

            // Refresh the position of the labels
            if (UserSettingsManager.Instance.ShowPercentagesInWaterfallChart)
            {
                neededHeight = Math.Round(
                                 CalculateWidthAndHeight(absoluteValueLabel, absoluteValueLabel.FontSize).Height +
                                 CalculateWidthAndHeight(percentageValueLabel, percentageValueLabel.FontSize).Height);
                neededWidth = Math.Max(absoluteValueWidth, percentageValueWidth);
            }
            else
            {
                neededHeight = CalculateWidthAndHeight(absoluteValueLabel, absoluteValueLabel.FontSize).Height;
                neededWidth = absoluteValueWidth;
            }

            valueLabelsContainer.Margin = new Thickness(0, 0 - neededHeight, 0, 0);

            // Refresh the labels container visibility
            if (neededWidth == 0 || neededWidth > dataPoint.Width)
            {
                valueLabelsContainer.Visibility = Visibility.Collapsed;
            }
            else
            {
                valueLabelsContainer.Visibility = Visibility.Visible;
            }
        }

        /// <summary>
        /// Calculates the width and height of an label with e specific font size 
        /// </summary>
        /// <param name="label">The label control</param>
        /// <param name="fontSize">The desired font size</param>
        /// <returns>The width and the height of the label</returns>
        private Size CalculateWidthAndHeight(ExtendedLabelControl label, double fontSize)
        {
            if (label != null)
            {
                FormattedText formattedText = new FormattedText(
                    label.DisplayedValue,
                    System.Threading.Thread.CurrentThread.CurrentUICulture,
                    FlowDirection.LeftToRight,
                    new Typeface(label.FontFamily, label.FontStyle, label.FontWeight, label.FontStretch),
                    fontSize,
                    label.Foreground);

                return new Size(Math.Round(formattedText.WidthIncludingTrailingWhitespace + 2, 2), Math.Round(formattedText.Height, 2));
            }

            return new Size(0, 0);
        }

        #region Ox axis labels width adjustment

        /// <summary>
        /// Handles the Loaded event of the text block of the OXAxis label template.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SizeChangedEventArgs" /> instance containing the event data.</param>
        private void OXAxisTextBlock_Loaded(object sender, RoutedEventArgs e)
        {
            var axis = this.OXAxis;
            var textBlock = sender as TextBlock;
            var axisLabel = VisualTreeHelper.GetParent(textBlock) as AxisLabel;
            var orientedPanel = axisLabel.Parent as Panel;

            if (axis == null || textBlock == null || axisLabel == null || orientedPanel == null)
            {
                return;
            }

            if (this.axisOxLabels.Any(info => info.Label == textBlock))
            {
                // The text block has been registered for refresh. This is necessary because the Loaded event of a text block can be 
                // triggered multiple times during its life time.
                return;
            }
            else
            {
                var labelInfo = new AxisOxLabelInfo(textBlock);
                this.axisOxLabels.Add(labelInfo);

                textBlock.MaxWidth = this.CalculateMaxWidthForOxAxisLabel(textBlock);
                textBlock.MaxHeight = this.CalculateMaxHeightForOxAxisLabel(textBlock);
            }
        }

        /// <summary>
        /// Calculates the maximum height for the labels displayed on the ox axis of the chart.
        /// </summary>
        /// <param name="label">The label.</param>
        /// <returns>The maximum height that was calculated.</returns>
        private double CalculateMaxHeightForOxAxisLabel(TextBlock label)
        {
            FormattedText minimumTextLabel = new FormattedText(
                             "MMM",
                             System.Threading.Thread.CurrentThread.CurrentUICulture,
                             FlowDirection.LeftToRight,
                             new Typeface(label.FontFamily, label.FontStyle, label.FontWeight, label.FontStretch),
                             label.FontSize,
                             label.Foreground);

            return Math.Round(minimumTextLabel.Height * 6, 2);
        }

        /// <summary>
        /// Calculates the maximum width for the labels displayed on the ox axis of the chart. All labels must have equal width.
        /// </summary>
        /// <param name="label">The label.</param>
        /// <returns>The maximum width that was calculated.</returns>
        private double CalculateMaxWidthForOxAxisLabel(TextBlock label)
        {
            var chart = this.OXAxis.DataContext as WaterfallChart;
            if (chart != null)
            {
                var itemNr = chart.WaterfallSeriesChart.ItemsSource.OfType<ChartItem>().Count();
                if (itemNr > 0)
                {
                    double categoryAxesWidth = this.OXAxis.ActualWidth;
                    var elementWidth = Math.Round(categoryAxesWidth * 0.9 / itemNr, 2);

                    FormattedText minimumTextLabel = new FormattedText(
                                  "MMM",
                                  System.Threading.Thread.CurrentThread.CurrentUICulture,
                                  FlowDirection.LeftToRight,
                                  new Typeface(label.FontFamily, label.FontStyle, label.FontWeight, label.FontStretch),
                                  label.FontSize,
                                  label.Foreground);

                    // If the available space is less than the space necessary for 3 "M" letters then the label will not be displayed
                    if (elementWidth > Math.Round(minimumTextLabel.WidthIncludingTrailingWhitespace, 2))
                    {
                        return elementWidth;
                    }
                }
            }

            return 0d;
        }

        /// <summary>
        /// Recalculates the max width of all labels on the Ox axis.
        /// </summary>
        private void RecalculateMaxWidthOfOxAxisLabels()
        {
            // Recalculate the max width of all OX axis labels.
            foreach (var labelInfo in this.axisOxLabels)
            {
                var label = labelInfo.Label;
                if (labelInfo.MaxWidthHistoryHasLoop())
                {
                    // The label's max width history contains a loop of applying the same two values over and over again.
                    // In order to break the loop, we do not apply recalculate the max width. We also reset the max width history
                    // to start fresh with the loop detection.
                    labelInfo.ResetMaxWidthHistory();
                }
                else
                {
                    double width = this.CalculateMaxWidthForOxAxisLabel(label);
                    if (width != 0d)
                    {
                        label.MaxWidth = width;
                        label.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        // If label MaxWidth is 0d then hide it to remove unnecessary space on the bottom of the chart
                        label.Visibility = Visibility.Collapsed;
                    }
                }
            }
        }

        #endregion Ox axis labels width adjustment

        #region Inner classes

        /// <summary>
        /// Contains information about a label displayed on the Ox axis.
        /// </summary>
        private class AxisOxLabelInfo
        {
            /// <summary>
            /// Helper used to receive notifications when the MaxWidth property of <see cref="Label"/> has changed.
            /// </summary>
            private readonly DependencyPropertyChangeNotifier labelMaxWidthChangedNotifier;

            /// <summary>
            /// Keeps the history of max width values applied to the label.
            /// </summary>
            private List<double> maxWidthHistory = new List<double>(5);
                        
            /// <summary>
            /// Initializes a new instance of the <see cref="AxisOxLabelInfo"/> class.
            /// </summary>
            /// <param name="label">The label for which to store information.</param>
            public AxisOxLabelInfo(TextBlock label)
            {
                this.Label = label;
                this.labelMaxWidthChangedNotifier = new DependencyPropertyChangeNotifier(this.Label, TextBlock.MaxWidthProperty);
                this.labelMaxWidthChangedNotifier.ValueChanged += this.OnLabelMaxWidthChanged;
            }

            /// <summary>
            /// Gets the label for which this instance stores information.
            /// </summary>
            public TextBlock Label { get; private set; }

            /// <summary>
            /// Resets the max width history.
            /// </summary>
            public void ResetMaxWidthHistory()
            {
                this.maxWidthHistory.Clear();
            }

            /// <summary>
            /// Checks if the max width history contains a loop of the same two values being present every time.
            /// </summary>
            /// <returns>True if the max with history contains the same two values in a loop; otherwise, false.</returns>
            public bool MaxWidthHistoryHasLoop()
            {
                if (this.maxWidthHistory.Count == 4
                    && this.maxWidthHistory[0] == this.maxWidthHistory[2]
                    && this.maxWidthHistory[1] == this.maxWidthHistory[3]
                    && this.maxWidthHistory[1] != this.maxWidthHistory[2])
                {
                    // We have 4 values in the history to be able to determine if we are in a loop of setting the same 2 values over and over.
                    return true;
                }

                return false;
            }

            /// <summary>
            /// Called when the MaxWidth property of the label has changed.
            /// </summary>
            /// <param name="sender">The sender.</param>
            /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
            private void OnLabelMaxWidthChanged(object sender, EventArgs e)
            {
                // When the label's max width changes, add the new value to the history.
                // It is enough to keep only the newest 4 values in history in order to detect infinite loops that set the same two values over and over.
                this.maxWidthHistory.Add(this.Label.MaxWidth);
                if (this.maxWidthHistory.Count > 4)
                {
                    this.maxWidthHistory.RemoveAt(0);
                }
            }
        }

        #endregion Inner classes
    }
}
