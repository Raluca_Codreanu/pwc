﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using Microsoft.Win32;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Common;
using ZPKTool.Controls;
using ZPKTool.Data;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Utils;
using ZPKTool.Gui.ViewModels;
using ZPKTool.Gui.ViewModels.ResultDetails;

namespace ZPKTool.Gui.Controls
{
    /// <summary>
    /// Interaction logic for Chart.xaml
    /// </summary>
    public partial class Chart : UserControl
    {
        /// <summary>
        /// The default value for the <see cref="AxesMaxDigits"/> property.
        /// </summary>
        private const int DefaultAxesMaxDigits = -1;

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The open file overlay window witch allows to open the chart's exported pdf file.
        /// </summary>
        private OpenFileOverlayWindow openFileOverlayWindow;

        /// <summary>
        /// The waterfall chart 
        /// </summary>
        private WaterfallChart waterfallChart;

        /// <summary>
        /// The pie chart
        /// </summary>
        private PieChart pieChart;

        /// <summary>
        /// Using a DependencyProperty as the backing store for MeasurementUnitsAdapter.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty MeasurementUnitsAdapterProperty =
            DependencyProperty.Register("MeasurementUnitsAdapter", typeof(UnitsAdapter), typeof(Chart), new UIPropertyMetadata(null, MeasurementUnitsAdapterChangedCallback));

        /// <summary>
        /// Using a DependencyProperty as the backing store for Chart UI Unit.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty ChartUiUnitProperty =
            DependencyProperty.Register("ChartUiUnit", typeof(Unit), typeof(Chart), new UIPropertyMetadata(null));

        /// <summary>
        /// Using a DependencyProperty as the backing store for Chart UI Symbol.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty ChartUiSymbolProperty =
            DependencyProperty.Register("ChartUiSymbol", typeof(string), typeof(Chart), new UIPropertyMetadata(string.Empty));

        /// <summary>
        /// Using a DependencyProperty as the backing store for ChartMode.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty ChartModeProperty =
            DependencyProperty.Register("ChartMode", typeof(ChartModes), typeof(Chart), new UIPropertyMetadata(ChartModes.Pie, ChartModeChangedCallBack));

        /// <summary>
        /// Using a DependencyProperty as the backing store for TotalValueLabel.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty TotalValueLabelProperty =
            DependencyProperty.Register("TotalValueLabel", typeof(string), typeof(Chart), new UIPropertyMetadata(null));

        /// <summary>
        /// Using a DependencyProperty as the backing store for TotalValue.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty TotalValueProperty =
            DependencyProperty.Register("TotalValue", typeof(decimal?), typeof(Chart), new UIPropertyMetadata(null, TotalValueChangedCallback));

        /// <summary>
        /// Using a DependencyProperty as the backing store for ItemsSource.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register("ItemsSource", typeof(IEnumerable<LabeledPieChartItem>), typeof(Chart), new UIPropertyMetadata(null, ItemsSourceChangedCallback));

        /// <summary>
        /// Using a DependencyProperty as the backing store for Entity.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty EntityProperty =
            DependencyProperty.Register("Entity", typeof(object), typeof(Chart), new UIPropertyMetadata(null));

        /// <summary>
        /// Using a DependencyProperty as the backing store for BreakdownName.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty BreakdownNameProperty =
            DependencyProperty.Register("BreakdownName", typeof(string), typeof(Chart), new UIPropertyMetadata(null));

        /// <summary>
        /// Using a DependencyProperty as the backing store for CurrentChartMode.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty CurrentChartModeProperty =
            DependencyProperty.Register("CurrentChartMode", typeof(ChartModes), typeof(Chart), new PropertyMetadata(ChartModes.Pie, CurrentChartModeChangedCallback));

        /// <summary>
        /// Using a DependencyProperty as the backing store for AxesMaxDigitsToDisplay.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty AxesMaxDigitsToDisplayProperty =
            DependencyProperty.Register("AxesMaxDigits", typeof(int), typeof(Chart), new UIPropertyMetadata(DefaultAxesMaxDigits, AxesMaxDigitsChangedCallback));

        /// <summary>
        /// Initializes a new instance of the <see cref="Chart"/> class.
        /// </summary>
        public Chart()
        {
            InitializeComponent();
            this.DataContext = this;

            // compute the default mode
            this.pieChart = new PieChart();
            this.PieChartContentPresenter.Content = this.pieChart;
            this.pieChart.Visibility = Visibility.Visible;
            this.SwitchChartButton.Visibility = Visibility.Collapsed;
            this.pieChart.SelectedItemChanged += new SelectionChangedEventHandler(this.Chart_SelectionChanged);
            this.pieChart.LabeledPieSeries.Loaded += new RoutedEventHandler(this.ChartSeries_Loaded);
            this.CurrentChartMode = ChartModes.Pie;
        }

        /// <summary>
        /// An event triggered when the view mode of this chart changes.
        /// </summary>
        public event Action<ChartModes> ChartViewModeChanged;

        /// <summary>
        /// Gets or sets the chart modes for the current screen
        /// The screen can contain only a waterfall chart,or a pie chart, or both
        /// </summary>
        public ChartModes ChartMode
        {
            get { return (ChartModes)GetValue(ChartModeProperty); }
            set { SetValue(ChartModeProperty, value); }
        }

        /// <summary>
        /// Gets or sets the label for the total value.
        /// </summary>
        public string TotalValueLabel
        {
            get { return (string)GetValue(TotalValueLabelProperty); }
            set { SetValue(TotalValueLabelProperty, value); }
        }

        /// <summary>
        /// Gets or sets the total value.
        /// </summary>
        public decimal? TotalValue
        {
            get { return (decimal?)GetValue(TotalValueProperty); }
            set { SetValue(TotalValueProperty, value); }
        }

        /// <summary>
        /// Gets or sets the items that will be displayed in the chart.
        /// </summary>
        /// <value>The items source.</value>
        public IEnumerable<LabeledPieChartItem> ItemsSource
        {
            get { return (IEnumerable<LabeledPieChartItem>)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        /// <summary>
        /// Gets or sets the entity for which the Result Details are displayed.
        /// </summary>        
        public object Entity
        {
            get { return (object)GetValue(EntityProperty); }
            set { SetValue(EntityProperty, value); }
        }

        /// <summary>
        /// Gets or sets the name of the breakdown on which this control is displayed. This information is used when exporting the chart.
        /// </summary>
        /// <value>The name of the breakdown.</value>
        public string BreakdownName
        {
            get { return (string)GetValue(BreakdownNameProperty); }
            set { SetValue(BreakdownNameProperty, value); }
        }

        /// <summary>
        /// Gets or sets the measurement units adapter.
        /// </summary>
        public UnitsAdapter MeasurementUnitsAdapter
        {
            get { return (UnitsAdapter)GetValue(MeasurementUnitsAdapterProperty); }
            set { SetValue(MeasurementUnitsAdapterProperty, value); }
        }

        /// <summary>
        /// Gets or sets the chart UI unit.
        /// </summary>
        public Unit ChartUiUnit
        {
            get { return (Unit)GetValue(ChartUiUnitProperty); }
            set { SetValue(ChartUiUnitProperty, value); }
        }

        /// <summary>
        /// Gets or sets the chart's UI symbol.
        /// </summary>
        public string ChartUiSymbol
        {
            get { return (string)GetValue(ChartUiSymbolProperty); }
            set { SetValue(ChartUiSymbolProperty, value); }
        }

        /// <summary>
        /// Gets or sets the current chart modes for the current screen
        /// The screen can contain at a time just a waterfall chart or a pie chart
        /// </summary>
        public ChartModes CurrentChartMode
        {
            get { return (ChartModes)GetValue(CurrentChartModeProperty); }
            set { SetValue(CurrentChartModeProperty, value); }
        }

        /// <summary>
        /// Gets or sets the maximum number of digits to display for the values on X axis of the waterfall chart 
        /// </summary>
        public int AxesMaxDigits
        {
            get { return (int)GetValue(AxesMaxDigitsToDisplayProperty); }
            set { SetValue(AxesMaxDigitsToDisplayProperty, value); }
        }

        /// <summary>
        /// Creates the open file overlay window.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="status">The status.</param>
        /// <param name="isOpenFileAvailable">if set to <c>true</c> [is open file available].</param>
        /// <returns>The overlayWindow</returns>
        private static OpenFileOverlayWindow CreateOpenFileOverlayWindow(string filePath, string status, bool isOpenFileAvailable)
        {
            OpenFileOverlayWindow openFileOverlayWindow = new OpenFileOverlayWindow();
            openFileOverlayWindow.Owner = WindowHelper.DetermineWindowOwner(openFileOverlayWindow);
            openFileOverlayWindow.MinHeight = 175d;
            openFileOverlayWindow.MinWidth = 275d;
            openFileOverlayWindow.LastCreatedFilePath = filePath;
            openFileOverlayWindow.IsOpenFileAvailable = isOpenFileAvailable;
            openFileOverlayWindow.CreatedFileStatus = status;

            return openFileOverlayWindow;
        }

        /// <summary>
        /// Invoked when the ItemsSource property value has changed.
        /// </summary>
        /// <param name="d">The dependency object.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void ItemsSourceChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Chart chart = d as Chart;
            if (chart != null)
            {
                chart.ComputeChartItems();
            }
        }

        /// <summary>
        /// Invoked when the total value property value has changed.
        /// </summary>
        /// <param name="d">The dependency object.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void TotalValueChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Chart chart = d as Chart;
            if (chart != null)
            {
                chart.ComputeChartItems();
            }
        }

        /// <summary>
        /// Invoked when the Chart Mode property value has changed.
        /// </summary>
        /// <param name="d">The dependency object.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void ChartModeChangedCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Chart resultDetails = d as Chart;
            if (resultDetails != null)
            {
                switch (resultDetails.ChartMode)
                {
                    case ChartModes.Watefall:
                        {
                            resultDetails.pieChart = null;

                            if (resultDetails.WaterfallChartContentPresenter.Content == null)
                            {
                                resultDetails.waterfallChart = new WaterfallChart();
                                resultDetails.waterfallChart.WaterfallSeriesChart.Loaded += new RoutedEventHandler(resultDetails.ChartSeries_Loaded);
                                resultDetails.WaterfallChartContentPresenter.Content = resultDetails.waterfallChart;
                                resultDetails.waterfallChart.Visibility = Visibility.Visible;
                                resultDetails.SwitchChartButton.Visibility = Visibility.Collapsed;
                                resultDetails.CurrentChartMode = ChartModes.Watefall;
                                resultDetails.waterfallChart.SelectedItemChanged += new SelectionChangedEventHandler(resultDetails.Chart_SelectionChanged);

                                if (resultDetails.AxesMaxDigits != DefaultAxesMaxDigits)
                                {
                                    resultDetails.waterfallChart.AxesMaxDigitsToDisplay = resultDetails.AxesMaxDigits;
                                }

                                resultDetails.ComputeChartItems();
                            }
                        }

                        break;

                    case ChartModes.Pie:
                        {
                            resultDetails.waterfallChart = null;

                            if (resultDetails.PieChartContentPresenter.Content == null)
                            {
                                resultDetails.pieChart = new PieChart();
                                resultDetails.pieChart.LabeledPieSeries.Loaded += new RoutedEventHandler(resultDetails.ChartSeries_Loaded);
                                resultDetails.PieChartContentPresenter.Content = resultDetails.pieChart;
                                resultDetails.pieChart.Visibility = Visibility.Visible;
                                resultDetails.SwitchChartButton.Visibility = Visibility.Collapsed;
                                resultDetails.CurrentChartMode = ChartModes.Pie;
                                resultDetails.pieChart.SelectedItemChanged += new SelectionChangedEventHandler(resultDetails.Chart_SelectionChanged);

                                resultDetails.ComputeChartItems();
                            }
                        }

                        break;

                    case ChartModes.Both:
                        {
                            if (resultDetails.WaterfallChartContentPresenter.Content == null)
                            {
                                resultDetails.waterfallChart = new WaterfallChart();
                                resultDetails.waterfallChart.WaterfallSeriesChart.Loaded += new RoutedEventHandler(resultDetails.ChartSeries_Loaded);
                                resultDetails.WaterfallChartContentPresenter.Content = resultDetails.waterfallChart;
                                resultDetails.waterfallChart.Visibility = Visibility.Collapsed;
                                resultDetails.waterfallChart.SelectedItemChanged += new SelectionChangedEventHandler(resultDetails.Chart_SelectionChanged);

                                if (resultDetails.AxesMaxDigits != DefaultAxesMaxDigits)
                                {
                                    resultDetails.waterfallChart.AxesMaxDigitsToDisplay = resultDetails.AxesMaxDigits;
                                }
                            }

                            if (resultDetails.PieChartContentPresenter.Content == null)
                            {
                                resultDetails.pieChart = new PieChart();
                                resultDetails.pieChart.LabeledPieSeries.Loaded += new RoutedEventHandler(resultDetails.ChartSeries_Loaded);
                                resultDetails.PieChartContentPresenter.Content = resultDetails.pieChart;
                                resultDetails.pieChart.SelectedItemChanged += new SelectionChangedEventHandler(resultDetails.Chart_SelectionChanged);
                            }

                            if ((ZPKTool.Gui.ViewModels.ChartViewMode)UserSettingsManager.Instance.DefaultChartViewMode == ZPKTool.Gui.ViewModels.ChartViewMode.PieView)
                            {
                                resultDetails.CurrentChartMode = ChartModes.Pie;
                            }
                            else if ((ZPKTool.Gui.ViewModels.ChartViewMode)UserSettingsManager.Instance.DefaultChartViewMode == ZPKTool.Gui.ViewModels.ChartViewMode.WatefallView)
                            {
                                resultDetails.CurrentChartMode = ChartModes.Watefall;
                            }

                            resultDetails.SwitchChartButton.Visibility = Visibility.Visible;
                            resultDetails.ComputeChartItems();
                        }

                        break;
                }
            }
        }

        /// <summary>
        /// Invoked when the current Chart Mode property value has changed.
        /// </summary>
        /// <param name="d">The dependency object.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void CurrentChartModeChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var chart = d as Chart;
            var newChartMode = (ChartModes)e.NewValue;
            if (chart != null)
            {
                if (newChartMode == ChartModes.Pie)
                {
                    chart.waterfallChart.Visibility = Visibility.Collapsed;
                    chart.pieChart.Visibility = Visibility.Visible;
                }
                else if (newChartMode == ChartModes.Watefall)
                {
                    chart.waterfallChart.Visibility = Visibility.Visible;
                    chart.pieChart.Visibility = Visibility.Collapsed;
                }
                else
                {
                    throw new InvalidOperationException("The current chart mode value can be only pie or waterfall");
                }
            }
        }

        /// <summary>
        /// Invoked when the AxesMaxDigits property value has changed.
        /// </summary>
        /// <param name="d">The dependency object.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void AxesMaxDigitsChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Chart resultDetails = d as Chart;
            if (resultDetails != null)
            {
                if (resultDetails.waterfallChart != null)
                {
                    resultDetails.waterfallChart.AxesMaxDigitsToDisplay = resultDetails.AxesMaxDigits;
                }
            }
        }

        /// <summary>
        /// Invoked when the MeasurementUnitsAdapter property value has changed.
        /// </summary>
        /// <param name="d">The dependency object.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void MeasurementUnitsAdapterChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Chart chart = d as Chart;
            if (chart != null)
            {
                chart.ComputeChartItems();
            }
        }

        /// <summary>
        /// Computes the chart item source used
        /// </summary>
        private void ComputeChartItems()
        {
            if (this.ItemsSource != null && this.MeasurementUnitsAdapter != null)
            {
                ObservableCollection<LabeledPieChartItem> legendItemSource = null;
                ObservableCollection<LabeledPieChartItem> chartItemSource = null;

                var newSource = this.ItemsSource;
                if (newSource != null)
                {
                    legendItemSource = new ObservableCollection<LabeledPieChartItem>(newSource);
                    chartItemSource = new ObservableCollection<LabeledPieChartItem>(newSource.Where(p => p.DisplayOnChart));
                }
                else
                {
                    legendItemSource = new ObservableCollection<LabeledPieChartItem>();
                    chartItemSource = new ObservableCollection<LabeledPieChartItem>();
                }

                ChartPalette.ResetPalette();

                this.LegendItemsList.ItemsSource = legendItemSource;

                if (this.waterfallChart != null && this.TotalValue != null)
                {
                    // use only this order when changing the items source in order to work as is should
                    this.waterfallChart.TotalValue = this.TotalValue;
                    this.waterfallChart.MeasurementUnitsAdapter = this.MeasurementUnitsAdapter;
                    this.waterfallChart.ItemsSource = chartItemSource;
                }

                if (this.pieChart != null)
                {
                    // use only this order when changing the items source in order to work as is should
                    this.pieChart.MeasurementUnitsAdapter = this.MeasurementUnitsAdapter;
                    this.pieChart.ChartUiUnit = this.ChartUiUnit;
                    this.pieChart.ChartUiSymbol = this.ChartUiSymbol;
                    this.pieChart.ItemsSource = chartItemSource;
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the ExportChartButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void ExportChartButton_Click(object sender, RoutedEventArgs e)
        {
            ExportChartToPdf();
        }

        /// <summary>
        /// Handles the SelectionChanged event of the ItemsList control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Controls.SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void LegendItemsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Bring the selected item into view
            if (this.LegendItemsList.SelectedItem != null)
            {
                this.LegendItemsList.ScrollIntoView(this.LegendItemsList.SelectedItem);

                var legendItem = this.LegendItemsList.SelectedItem as LabeledPieChartItem;

                if (this.pieChart != null)
                {
                    this.pieChart.SelectItem(legendItem);
                }

                if (this.waterfallChart != null)
                {
                    this.waterfallChart.SelectItem(legendItem);
                }
            }
        }

        /// <summary>
        /// Handles the click event on switch between charts - only 1 chart is shown at a time
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void SwitchChartButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.CurrentChartMode == ChartModes.Pie)
            {
                this.CurrentChartMode = ChartModes.Watefall;
            }
            else if (CurrentChartMode == ChartModes.Watefall)
            {
                this.CurrentChartMode = ChartModes.Pie;
            }

            if (ChartViewModeChanged != null)
            {
                ChartViewModeChanged(this.CurrentChartMode);
            }
        }

        /// <summary>
        /// Handles the Loaded event on chart series
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void ChartSeries_Loaded(object sender, RoutedEventArgs e)
        {
            var legendSelectedItem = this.LegendItemsList.SelectedItem as LabeledPieChartItem;
            if (legendSelectedItem != null)
            {
                var series = sender as System.Windows.Controls.DataVisualization.Charting.DataPointSeries;
                if (series != null && series.ItemsSource != null)
                {
                    series.SelectedItem = series.ItemsSource.OfType<LabeledPieChartItem>().FirstOrDefault(p => p.Id == legendSelectedItem.Id);
                }
            }
        }

        /// <summary>
        /// Handles the Chart_SelectionChanged event.
        /// It selects in legend list the item that has the same id as the item that has been selected in chart
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Controls.SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void Chart_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedItem = e.AddedItems.OfType<LabeledPieChartItem>().FirstOrDefault();
            if (this.LegendItemsList.ItemsSource != null && selectedItem != null)
            {
                var legendSource = this.LegendItemsList.ItemsSource as IEnumerable<LabeledPieChartItem>;
                this.LegendItemsList.SelectedItem = legendSource.FirstOrDefault(p => p.Id == selectedItem.Id);
            }
        }

        #region Export chart

        /// <summary>
        /// Exports the chart to a PDF document.
        /// </summary>
        private void ExportChartToPdf()
        {
            // Unselect the currently selected item on available chart slice so it does not appear selected in the exported image. The selection is restored in the end of the method.
            LabeledPieChartItem selectedPieChartItem = null;
            if (this.pieChart != null && this.pieChart.IsVisible)
            {
                selectedPieChartItem = this.pieChart.GetSelectedItem();
                this.pieChart.ResetSelectedItem();
            }
            else if (this.waterfallChart != null)
            {
                selectedPieChartItem = this.waterfallChart.GetSelectedItem();
                this.waterfallChart.ResetSelectedItem();
            }

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.CheckPathExists = true;
            saveFileDialog.Filter = LocalizedResources.DialogFilter_PdfDocs;
            saveFileDialog.OverwritePrompt = true;
            saveFileDialog.RestoreDirectory = true;

            // Generate a default name for the export file
            string fileName = GenerateChartExportFileName();
            if (!string.IsNullOrEmpty(fileName))
            {
                saveFileDialog.FileName = ZPKTool.Common.PathUtils.SanitizeFileName(fileName);
            }

            // Display the save file dialog
            bool? dialogresult = saveFileDialog.ShowDialog();
            if (dialogresult != true)
            {
                return;
            }

            // Create the pdf document
            var document = new iTextSharp.text.Document();

            try
            {
                string docFont = Constants.PdfReportsFont;
                var writer = iTextSharp.text.pdf.PdfWriter.GetInstance(document, new FileStream(saveFileDialog.FileName, FileMode.Create));
                document.Open();

                // Create the title of the pdf document                
                string title = GenerateChartExportTitle();

                // Add the title to the document
                var titlePara = new iTextSharp.text.Paragraph(title, iTextSharp.text.FontFactory.GetFont(docFont, 14));
                titlePara.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
                titlePara.Font.SetColor(109, 109, 109);
                this.HandlePhraseSubscriptDigits(title, titlePara, docFont, 14);
                document.Add(titlePara);

                document.Add(new iTextSharp.text.Chunk(Environment.NewLine));

                this.UpdateLayout();
                Stream chartImageSource = null;

                if (this.pieChart != null && this.pieChart.IsVisible)
                {
                    chartImageSource = this.pieChart.GenerateImagePng(3d);
                }
                else if (this.waterfallChart != null)
                {
                    chartImageSource = this.waterfallChart.GenerateImagePng(3d);
                }

                // Add the chart to the document
                using (chartImageSource)
                {
                    var img = iTextSharp.text.Image.GetInstance(chartImageSource);
                    img.ScaleToFit(450f, 450f);
                    img.Alignment = iTextSharp.text.Image.ALIGN_MIDDLE;
                    document.Add(img);
                }

                // Add the chart legend to the document
                var legendTable = new iTextSharp.text.pdf.PdfPTable(3);
                legendTable.SetWidths(new float[] { 12f, 100f, 50f });
                legendTable.SpacingBefore = 15f;

                // 1st add the legend title
                var legendTitle = new iTextSharp.text.Phrase(
                    LocalizedResources.General_LegendTitle,
                    iTextSharp.text.FontFactory.GetFont(docFont, 12, iTextSharp.text.Font.BOLD));
                var legendTitleCell = new iTextSharp.text.pdf.PdfPCell(legendTitle);
                legendTitleCell.Colspan = 3;
                legendTitleCell.HorizontalAlignment = 1;
                legendTitleCell.PaddingBottom = 5f;
                legendTable.AddCell(legendTitleCell);

                // Next, add to the table each item from the legend list
                foreach (object obj in this.LegendItemsList.Items)
                {
                    LabeledPieChartItem crtBindingItem = obj as LabeledPieChartItem;
                    ListBoxItem crtListItem = this.LegendItemsList.ItemContainerGenerator.ContainerFromItem(obj) as ListBoxItem;
                    if (crtListItem == null || crtBindingItem == null)
                    {
                        continue;
                    }

                    // Add the item's color key from the chart legend
                    iTextSharp.text.IElement colorKeyElement = null;
                    Rectangle colorItem = UIHelper.FindChildren<Rectangle>(crtListItem).Where(rect => rect.Name == "ColorKey").FirstOrDefault();
                    if (colorItem != null)
                    {
                        Color legendItemColor = Colors.Transparent;
                        if (colorItem.Fill is RadialGradientBrush)
                        {
                            // If the item's color key is a gradient use the "median" color of the gradient
                            RadialGradientBrush colorBrush = colorItem.Fill as RadialGradientBrush;
                            if (colorBrush != null && colorBrush.GradientStops != null)
                            {
                                int alfa = 0;
                                int blue = 0;
                                int green = 0;
                                int red = 0;

                                if (colorBrush.GradientStops.Count != 0)
                                {
                                    foreach (GradientStop gradientStop in colorBrush.GradientStops)
                                    {
                                        alfa += gradientStop.Color.A;
                                        blue += gradientStop.Color.B;
                                        green += gradientStop.Color.G;
                                        red += gradientStop.Color.R;
                                    }

                                    alfa /= colorBrush.GradientStops.Count;
                                    blue /= colorBrush.GradientStops.Count;
                                    green /= colorBrush.GradientStops.Count;
                                    red /= colorBrush.GradientStops.Count;
                                }

                                legendItemColor = Color.FromArgb((byte)alfa, (byte)red, (byte)green, (byte)blue);
                            }
                        }
                        else if (colorItem.Fill is SolidColorBrush)
                        {
                            SolidColorBrush colorBrush = colorItem.Fill as SolidColorBrush;
                            if (colorBrush != null)
                            {
                                legendItemColor = colorBrush.Color;
                            }
                        }

                        // Create an iTextSharp image from the color going through and intermediate System.Drawing.Bitmap image                         
                        System.Drawing.Color color = System.Drawing.Color.FromArgb(legendItemColor.A, legendItemColor.R, legendItemColor.G, legendItemColor.B);

                        System.Drawing.Bitmap bmpImage = new System.Drawing.Bitmap(13, 13, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
                        bmpImage.SetResolution(96, 96);
                        for (int i = 0; i < bmpImage.Size.Height; i++)
                        {
                            for (int j = 0; j < bmpImage.Size.Width; j++)
                            {
                                bmpImage.SetPixel(i, j, color);
                            }
                        }

                        var colorKey = iTextSharp.text.Image.GetInstance(bmpImage, System.Drawing.Imaging.ImageFormat.Bmp);
                        colorKey.ScaleToFit(13f, 13f);
                        colorKey.Alignment = iTextSharp.text.Image.ALIGN_MIDDLE | iTextSharp.text.Image.ALIGN_CENTER;
                        colorKeyElement = colorKey;
                    }

                    var colorKeyCell = new iTextSharp.text.pdf.PdfPCell();
                    colorKeyCell.AddElement(colorKeyElement);
                    legendTable.AddCell(colorKeyCell);

                    // Add the item's name
                    iTextSharp.text.Phrase namePhrase = null;
                    LabelControl nameLabel = UIHelper.FindChildren<LabelControl>(crtListItem).Where(lbl => lbl.Name == "NameLabel").FirstOrDefault();
                    if (nameLabel != null)
                    {
                        int fontStyle = MapFontWeightToiTextSharp(nameLabel.FontWeight);
                        namePhrase = new iTextSharp.text.Phrase(nameLabel.Text, iTextSharp.text.FontFactory.GetFont(docFont, 10, fontStyle));
                        this.HandlePhraseSubscriptDigits(nameLabel.Text, namePhrase, docFont, 10);
                    }

                    var nameCell = new iTextSharp.text.pdf.PdfPCell(namePhrase);
                    legendTable.AddCell(nameCell);

                    var extendedLabels = UIHelper.FindChildren<ExtendedLabelControl>(crtListItem);

                    // Add the item's value
                    iTextSharp.text.Phrase valuePhrase = null;
                    ExtendedLabelControl valueLabel = extendedLabels.Where(lbl => lbl.Name == "ValueLabel").FirstOrDefault();
                    if (valueLabel != null)
                    {
                        string value = valueLabel.DisplayedValue;
                        int fontStyle = MapFontWeightToiTextSharp(valueLabel.FontWeight);
                        valuePhrase = new iTextSharp.text.Phrase(value, iTextSharp.text.FontFactory.GetFont(docFont, 10, fontStyle));
                    }

                    // Add the item's extra info, if available, appended to the value phrase
                    if (crtBindingItem.DisplayAdditionalInfo)
                    {
                        ExtendedLabelControl additionalInfoLabel = extendedLabels.Where(lbl => lbl.Name == "AdditionalInfoLabel").FirstOrDefault();
                        if (additionalInfoLabel != null)
                        {
                            string additionalInfo = " " + additionalInfoLabel.DisplayedValue;
                            int fontStyle = MapFontWeightToiTextSharp(valueLabel.FontWeight);
                            if (valuePhrase != null)
                            {
                                var additionalInfoChunk = new iTextSharp.text.Chunk(additionalInfo, iTextSharp.text.FontFactory.GetFont(docFont, 10, fontStyle));
                                valuePhrase.Add(additionalInfoChunk);
                            }
                            else
                            {
                                valuePhrase = new iTextSharp.text.Phrase(additionalInfo, iTextSharp.text.FontFactory.GetFont(docFont, 10, fontStyle));
                            }
                        }
                    }

                    var valueCell = new iTextSharp.text.pdf.PdfPCell(valuePhrase);
                    legendTable.AddCell(valueCell);
                }

                // Last, add the total value in the table
                string totalValue = this.TotalValueLabel + ": " + this.TotalLabelControl.DisplayedValue;
                var totalValuePhrase = new iTextSharp.text.Phrase(totalValue, iTextSharp.text.FontFactory.GetFont(docFont, 11, iTextSharp.text.Font.BOLD));
                this.HandlePhraseSubscriptDigits(totalValue, totalValuePhrase, docFont, 11);
                var totalValueCell = new iTextSharp.text.pdf.PdfPCell(totalValuePhrase);
                totalValueCell.Colspan = 2;
                totalValueCell.PaddingBottom = 5f;
                legendTable.AddCell(new iTextSharp.text.pdf.PdfPCell());
                legendTable.AddCell(totalValueCell);
                document.Add(legendTable);
                document.Close();

                this.openFileOverlayWindow = CreateOpenFileOverlayWindow(saveFileDialog.FileName, LocalizedResources.ChartExport_Success, true);
                this.openFileOverlayWindow.ShowDialog();
            }
            catch (iTextSharp.text.DocumentException docEx)
            {
                log.ErrorException("iTextSharp error while exporting a chart to pdf.", docEx);
                this.openFileOverlayWindow = CreateOpenFileOverlayWindow(saveFileDialog.FileName, LocalizedResources.Error_Internal, false);
                this.openFileOverlayWindow.ShowDialog();
            }
            catch (IOException ex)
            {
                log.ErrorException("IO error while exporting a chat to pdf.", ex);
                this.openFileOverlayWindow = CreateOpenFileOverlayWindow(saveFileDialog.FileName, LocalizedResources.ChartExport_IOError, false);
                this.openFileOverlayWindow.ShowDialog();
            }
            finally
            {
                if (document.IsOpen())
                {
                    // Close document and writer  
                    document.Close();
                }

                if (this.pieChart != null && this.pieChart.IsVisible)
                {
                    this.pieChart.SelectItem(selectedPieChartItem);
                }
                else if (this.waterfallChart != null)
                {
                    this.waterfallChart.SelectItem(selectedPieChartItem);
                }
            }
        }

        /// <summary>
        /// Maps the specified font weight to the corresponding iTextSharp weight.
        /// </summary>
        /// <param name="weight">The weight.</param>
        /// <returns>The corresponding iTextSharp weight.</returns>
        private int MapFontWeightToiTextSharp(FontWeight weight)
        {
            int fontStyle = iTextSharp.text.Font.NORMAL;
            if (weight == FontWeights.Bold ||
                weight == FontWeights.DemiBold ||
                weight == FontWeights.ExtraBold ||
                weight == FontWeights.UltraBold)
            {
                fontStyle = iTextSharp.text.Font.BOLD;
            }

            return fontStyle;
        }

        /// <summary>
        /// Generates a default name for the chart export file.
        /// </summary>
        /// <returns>A string containing the generated file name.</returns>
        private string GenerateChartExportFileName()
        {
            string fileName = string.Empty;

            Assembly assy = this.Entity as Assembly;
            if (assy != null)
            {
                fileName = assy.Name + (!string.IsNullOrEmpty(assy.Number) ? "_" + assy.Number : null) + (assy.Version != null ? "_v" + assy.Version : null);
                fileName += " " + this.BreakdownName + " " + LocalizedResources.Chart;
            }
            else
            {
                Part part = this.Entity as Part;
                if (part != null)
                {
                    fileName = part.Name + (!string.IsNullOrEmpty(part.Number) ? "_" + part.Number : null) + (part.Version != null ? "_v" + part.Version : null);
                    fileName += " " + this.BreakdownName + " " + LocalizedResources.Chart;
                }
                else
                {
                    RawMaterialCost rawMaterialCost = this.Entity as RawMaterialCost;
                    if (rawMaterialCost != null)
                    {
                        fileName = rawMaterialCost.Name + " " + LocalizedResources.General_RawMaterial + " " + LocalizedResources.Chart;
                    }
                }
            }

            return fileName;
        }

        /// <summary>
        /// Generates the title of the exported chart.
        /// </summary>
        /// <returns>A string containing the generated title.</returns>
        private string GenerateChartExportTitle()
        {
            string entityName = string.Empty;
            string entityType = string.Empty;
            string breakdownName = this.BreakdownName;

            Part part = this.Entity as Part;
            if (part != null)
            {
                entityType = LocalizedResources.General_Part;
                entityName = part.Name;
                if (!string.IsNullOrEmpty(part.Number))
                {
                    entityName += " (" + part.Number + ")";
                }
            }
            else
            {
                Assembly assembly = this.Entity as Assembly;
                if (assembly != null)
                {
                    entityType = LocalizedResources.General_Assembly;
                    entityName = assembly.Name;
                    if (!string.IsNullOrEmpty(assembly.Number))
                    {
                        entityName += " (" + assembly.Number + ")";
                    }
                }
                else
                {
                    RawMaterialCost rawMaterialCost = this.Entity as RawMaterialCost;
                    if (rawMaterialCost != null)
                    {
                        entityType = LocalizedResources.General_RawMaterial;
                        entityName = rawMaterialCost.Name;
                        breakdownName = null;
                    }
                }
            }

            return string.Format(LocalizedResources.ChartExport_Title, breakdownName, entityName, entityType).Trim();
        }

        #endregion Export chart

        #region Subscripts handling

        /// <summary>
        /// Method used to handle a phrase with text that contains subscript digits, in order to save them properly to PDF.
        /// This method seeks sub-sequences of subscript digits and add them to a different chunk, 
        /// setting their font smaller and lower the text using Chunk.SetTextRise method.
        /// </summary>
        /// <param name="text">The phrase Text.</param>
        /// <param name="phrase">The phrase.</param>
        /// <param name="docFont">The document font.</param>
        /// <param name="fontSize">The font size value.</param>
        private void HandlePhraseSubscriptDigits(string text, iTextSharp.text.Phrase phrase, string docFont, int fontSize)
        {
            // If there are no subscript digits do nothing.
            var subscriptSequence = this.GetFirstSubscriptsSequence(text);
            if (subscriptSequence.Item1 == -1)
            {
                return;
            }

            phrase.Clear();
            while (subscriptSequence.Item1 > -1)
            {
                // Add the chunks with normal and subscript texts to the phrase setting the appropriate font.
                var normalTextChunk = new iTextSharp.text.Chunk(text.Substring(0, subscriptSequence.Item1), iTextSharp.text.FontFactory.GetFont(docFont, fontSize));
                var subscriptTextChunk = new iTextSharp.text.Chunk(subscriptSequence.Item3, iTextSharp.text.FontFactory.GetFont(docFont, fontSize > 5 ? fontSize - 5 : 2, iTextSharp.text.Font.BOLD)).SetTextRise(-2);
                phrase.Add(normalTextChunk);
                phrase.Add(subscriptTextChunk);

                var currentStringPos = subscriptSequence.Item1 + subscriptSequence.Item2;
                if (currentStringPos <= text.Length)
                {
                    // If not reached the end of the text, look for other possible substring sequences.
                    text = text.Substring(currentStringPos, text.Length - currentStringPos);
                    subscriptSequence = this.GetFirstSubscriptsSequence(text);
                }
                else
                {
                    text = string.Empty;
                    subscriptSequence = new Tuple<int, int, string>(-1, 0, string.Empty);
                }
            }

            // If not reached the end of the text, add the remaining text to the phrase.
            if (!string.IsNullOrEmpty(text))
            {
                var remainingTextChunk = new iTextSharp.text.Chunk(text, iTextSharp.text.FontFactory.GetFont(docFont, fontSize));
                phrase.Add(remainingTextChunk);
            }
        }

        /// <summary>
        /// Get the first subscript digits sub-sequence from a string.
        /// </summary>
        /// <param name="text">The input string.</param>
        /// <returns>A tuple containing: 
        /// 1. The sequence index in the input string. 
        /// 2. The sequence number of characters. 
        /// 3. The subscript digits sequence converted to normal digits.</returns>
        private Tuple<int, int, string> GetFirstSubscriptsSequence(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return new Tuple<int, int, string>(-1, 0, string.Empty);
            }

            int index = 0;
            int count = 0;
            int subscriptIndex = -1;
            var subscriptSequence = new StringBuilder();

            foreach (var c in text)
            {
                // 8320 is the asci code for '₀' and 8329 for '₉'
                if ((int)c >= 8320 && (int)c <= 8329)
                {
                    if (subscriptIndex == -1)
                    {
                        subscriptIndex = index;
                    }

                    // Since the asci code for '₀' is 8320 and '0' is 48 --> 8272 is the asci offset.
                    subscriptSequence.Append((char)((int)c - 8272));
                    count++;
                }
                else if (subscriptIndex > -1)
                {
                    return new Tuple<int, int, string>(subscriptIndex, count, subscriptSequence.ToString());
                }

                index++;
            }

            return new Tuple<int, int, string>(subscriptIndex, count, subscriptSequence.ToString());
        }

        #endregion Subscripts handling
    }
}