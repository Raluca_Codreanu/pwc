﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using ZPKTool.Business;
using ZPKTool.Controls;
using ZPKTool.Data;
using ZPKTool.DataAccess;
using ZPKTool.Gui.Notifications;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Utils;
using ZPKTool.Gui.ViewModels;

namespace ZPKTool.Gui.Controls
{
    /// <summary>
    /// Interaction logic for PleaseWaitControl.xaml
    /// </summary>
    public partial class PleaseWaitControl : UserControl
    {
        /// <summary>
        /// Using a DependencyProperty as the backing store for Message.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty MessageProperty =
            DependencyProperty.Register("Message", typeof(string), typeof(PleaseWaitControl), new UIPropertyMetadata(null));

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PleaseWaitControl"/> class from being created.
        /// </summary>
        public PleaseWaitControl()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Gets or sets the message to be displayed while working. This is a dependency property.
        /// </summary>
        public string Message
        {
            get { return (string)GetValue(MessageProperty); }
            set { SetValue(MessageProperty, value); }
        }
                
        #endregion Properties
    }
}