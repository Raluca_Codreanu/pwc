﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Controls;
using ZPKTool.Data;
using ZPKTool.Gui.Utils;

namespace ZPKTool.Gui.Controls
{
    /// <summary>
    /// Extends the textbox control by adding a symbol label to its right and automatically converting its content according to the UI unit and base currency.
    /// </summary>
    public class ExtendedTextBoxControl : TextBoxControl
    {
        #region Fields

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// A weak event listener for changes in the <see cref="DecimalPlacesNumber"/> application setting.
        /// </summary>
        private readonly WeakEventListener<PropertyChangedEventArgs> numberOfDigitsToDisplayChangedListener;

        /// <summary>
        /// The label that displays the symbol associated with the text box.
        /// Can be null so check before using it.
        /// </summary>
        private LabelControl symbolLabel;

        /// <summary>
        /// A flag indicating whether the BaseValue property was set by internal code (true - was set internally, false - it was set by client code that uses the text box)
        /// </summary>
        private bool baseValueSetInternally;

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtendedTextBoxControl"/> class.
        /// </summary>
        public ExtendedTextBoxControl()
        {
            this.numberOfDigitsToDisplayChangedListener = new WeakEventListener<PropertyChangedEventArgs>(this.HandleNumberOfDecimalPlacesChanged);
            PropertyChangedEventManager.AddListener(
                UserSettingsManager.Instance,
                numberOfDigitsToDisplayChangedListener,
                ReflectionUtils.GetPropertyName(() => UserSettingsManager.Instance.DecimalPlacesNumber));

            DependencyPropertyChangeNotifier notifier = new DependencyPropertyChangeNotifier(this, ExtendedTextBoxControl.WidthProperty);
            notifier.ValueChanged += this.WidthValueChanged;
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Gets or sets the base currency. This is the currency that will be used to calculate the base value (the value saved in the db).
        /// </summary>
        public Currency BaseCurrency
        {
            get { return (Currency)GetValue(BaseCurrencyProperty); }
            set { SetValue(BaseCurrencyProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for BaseCurrency.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty BaseCurrencyProperty =
            DependencyProperty.Register("BaseCurrency", typeof(Currency), typeof(ExtendedTextBoxControl), new UIPropertyMetadata(null, BaseCurrencyChangedCallback));

        /// <summary>
        /// Gets or sets the UI unit displayed next to the text box.
        /// </summary>
        public Unit UIUnit
        {
            get { return (Unit)GetValue(UIUnitProperty); }
            set { SetValue(UIUnitProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for UIUnit.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty UIUnitProperty =
            DependencyProperty.Register("UIUnit", typeof(Unit), typeof(ExtendedTextBoxControl), new UIPropertyMetadata(null, UIUnitChangedCallback));

        /// <summary>
        /// Gets or sets the symbol associated with the control (to the right of it).
        /// The symbol is set to the result of the ToString() method of the assigned value and can be set this way only if the UIUnit property is set to null.
        /// </summary>        
        public object Symbol
        {
            get { return GetValue(SymbolProperty); }
            set { SetValue(SymbolProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for Symbol.  This enables animation, styling, binding, etc...        
        /// </summary>
        public static readonly DependencyProperty SymbolProperty =
            DependencyProperty.Register("Symbol", typeof(object), typeof(ExtendedTextBoxControl), new UIPropertyMetadata(null, SymbolChangedCallback));

        /// <summary>
        /// Gets or sets the string format of the symbol.
        /// The format must contain the '{0}' placeholder which is reserved for the symbol.
        /// </summary>
        public string SymbolFormat
        {
            get { return (string)GetValue(SymbolFormatProperty); }
            set { SetValue(SymbolFormatProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for SymbolFormat.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty SymbolFormatProperty =
            DependencyProperty.Register("SymbolFormat", typeof(string), typeof(ExtendedTextBoxControl), new UIPropertyMetadata(null, SymbolFormatChangedCallback));

        /// <summary>
        /// Gets or sets the number based on which a value is displayed in the text box.
        /// <para/>
        /// If UIUnit is set to null, the value is converted to its string representation and displayed. If UIUnit is set to something else,
        /// the number is interpreted as a base value and it will be converted to the corresponding UI unit before displaying it; when retrieving it,
        /// the displayed value will be converted back the base value.        
        /// <para/>
        /// Use the Text property to get the value as it is displayed.
        /// </summary>
        public decimal? BaseValue
        {
            get { return (decimal?)GetValue(BaseValueProperty); }
            set { SetValue(BaseValueProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for BaseValue.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty BaseValueProperty =
            DependencyProperty.Register("BaseValue", typeof(decimal?), typeof(ExtendedTextBoxControl), new UIPropertyMetadata(null, BaseValueChangedCallback));

        /// <summary>
        /// Gets or sets the ExtendedTextBox control internal textbox Width according to ExtendedTextBox Width.
        /// </summary>
        public double TextBoxInternalWidth
        {
            get { return (double)GetValue(TextBoxInternalWidthProperty); }
            set { SetValue(TextBoxInternalWidthProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for TextBoxInternalWidth.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty TextBoxInternalWidthProperty =
            DependencyProperty.Register("TextBoxInternalWidth", typeof(double), typeof(ExtendedTextBoxControl), new UIPropertyMetadata(double.NaN));

        #endregion Properties

        #region Dependency Property Callbacks

        /// <summary>
        /// Callback executed when the value of the BaseValue property has changed.
        /// </summary>
        /// <param name="d">The dependency object that triggered the callback.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs" /> instance containing the event data.</param>
        private static void BaseValueChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var textBox = (ExtendedTextBoxControl)d;
            if (textBox.baseValueSetInternally)
            {
                return;
            }

            SetDisplayedValue(textBox);
        }

        /// <summary>
        /// Called when the value of the SymbolFormat dependency property has changed.
        /// </summary>
        /// <param name="d">The dependency object that triggered the callback.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void SymbolChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var textBox = (ExtendedTextBoxControl)d;
            textBox.ApplySymbol();
        }

        /// <summary>
        /// Called when the value of the SymbolFormat dependency property has changed.
        /// </summary>
        /// <param name="d">The dependency object that triggered the callback.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void SymbolFormatChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var textBox = (ExtendedTextBoxControl)d;

            // Refresh the text box's symbol to apply the new format.
            textBox.ApplySymbol();
        }

        /// <summary>
        /// Called when the value of the BaseCurrency dependency property has changed.
        /// </summary>
        /// <param name="d">The dependency object that triggered the callback.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void BaseCurrencyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var textBox = (ExtendedTextBoxControl)d;

            // Calculate the base value by using the rate of the old and new base currencies.
            var oldCurrency = e.OldValue as Currency;
            var newCurrency = e.NewValue as Currency;

            if (textBox.BaseValue.HasValue
                && oldCurrency != null
                && newCurrency != null)
            {
                textBox.BaseValue = CurrencyConversionManager.ConvertBaseValue(textBox.BaseValue, newCurrency.ExchangeRate, oldCurrency.ExchangeRate);
            }
        }

        /// <summary>
        /// Called when the value of the UIUnit dependency property has changed.
        /// </summary>
        /// <param name="d">The dependency object that triggered the callback.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void UIUnitChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var textBox = (ExtendedTextBoxControl)d;

            // Set the new units's symbol.
            var associatedSymbol = string.Empty;
            var uiUnit = e.NewValue as Unit;

            if (uiUnit != null)
            {
                associatedSymbol = uiUnit.Symbol;
            }

            textBox.Symbol = associatedSymbol;

            SetDisplayedValue(textBox);
        }

        #endregion Dependency Property Callbacks

        #region Methods

        /// <summary>
        /// Is called when a control template is applied.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            this.symbolLabel = this.Template.FindName("PART_UnitSymbol", this) as LabelControl;
            this.ApplySymbol();
        }

        /// <summary>
        /// Is called when content in this editing control changes.
        /// </summary>
        /// <param name="e">The arguments that are associated with the <see cref="E:System.Windows.Controls.Primitives.TextBoxBase.TextChanged"/> event.</param>
        protected override void OnTextChanged(TextChangedEventArgs e)
        {
            if (this.SuspendTextChangedEvent)
            {
                // The base implementation must be called, otherwise the TextBoxControl's internal state will not be correctly updated.
                base.OnTextChanged(e);
                return;
            }

            try
            {
                // Check if the new text is valid before recalculating the base value.
                bool isTextValid = true;
                var change = e.Changes.FirstOrDefault();
                if (change != null)
                {
                    isTextValid = this.ValidateCurrentTextOnTextChange(change);
                }

                // When the text changes, recalculate the base value.
                if (isTextValid)
                {
                    this.baseValueSetInternally = true;
                    this.BaseValue = this.CalculateBaseValue();
                }
            }
            finally
            {
                this.baseValueSetInternally = false;

                // Call the base implementation after the base value is updated so that clients subscribing to the TextChanged event will get the updated BaseValue.
                base.OnTextChanged(e);
            }
        }

        /// <summary>
        /// Sets the displayed value of the control by converting its base value.
        /// </summary>
        /// <param name="textBox">The text box.</param>
        private static void SetDisplayedValue(ExtendedTextBoxControl textBox)
        {
            if (textBox.BaseValue.HasValue)
            {
                decimal displayValue = 0m;
                if (textBox.BaseCurrency == null
                    && textBox.UIUnit != null)
                {
                    if (textBox.UIUnit.Type == UnitType.Percentage)
                    {
                        displayValue = textBox.BaseValue.Value * textBox.UIUnit.ConversionFactor;
                    }
                    else
                    {
                        displayValue = textBox.BaseValue.Value / textBox.UIUnit.ConversionFactor;
                    }
                }
                else if (textBox.BaseCurrency != null
                    && textBox.UIUnit != null)
                {
                    displayValue = CurrencyConversionManager.ConvertBaseValueToDisplayValue(textBox.BaseValue.Value, textBox.BaseCurrency.ExchangeRate, textBox.UIUnit.ConversionFactor);
                }
                else
                {
                    displayValue = textBox.BaseValue.Value;
                }

                // Convert the base value into the new unit of the type defined by the UIUnit property.
                if (displayValue > textBox.MaxContentLimit || displayValue < textBox.MinContentLimit)
                {
                    textBox.AllowOutOfLimitInitializer = true;
                }

                // Set the new text without triggering the TextChanged event.
                var suspendBak = textBox.SuspendTextChangedEvent;
                textBox.SuspendTextChangedEvent = true;
                try
                {
                    var text = TrimToNumberOfDigits(displayValue, textBox.MaxDecimalPlaces);
                    textBox.Text = text;
                }
                finally
                {
                    textBox.SuspendTextChangedEvent = suspendBak;
                }
            }
            else
            {
                textBox.Text = string.Empty;
            }
        }

        /// <summary>
        /// Calculates the base value of the current input.
        /// </summary>
        /// <returns>The base value or null if the base value is not set.</returns>
        private decimal? CalculateBaseValue()
        {
            var inputTextAsDecimal = Converter.StringToNullableDecimal(this.Text);
            if (!inputTextAsDecimal.HasValue)
            {
                return null;
            }

            decimal? baseValue = null;

            // If no UIUnit is set then the base value has the same value as the inputed text,
            // else the base value will be calculated using the rate of the UIUnit and/or BaseCurrency.
            if (this.BaseCurrency == null
                && this.UIUnit == null)
            {
                baseValue = inputTextAsDecimal;
            }
            else if (this.BaseCurrency == null
                    && this.UIUnit != null)
            {
                if (this.UIUnit.Type == UnitType.Percentage)
                {
                    baseValue = inputTextAsDecimal.Value / this.UIUnit.ConversionFactor;
                }
                else
                {
                    baseValue = inputTextAsDecimal.Value * this.UIUnit.ConversionFactor;
                }
            }
            else if (this.BaseCurrency != null
                    && this.UIUnit != null)
            {
                baseValue = CurrencyConversionManager.ConvertToBaseValue(inputTextAsDecimal.Value, this.UIUnit.ConversionFactor, this.BaseCurrency.ExchangeRate);
            }

            return baseValue;
        }

        /// <summary>
        /// Width value changed event handler.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void WidthValueChanged(object sender, EventArgs e)
        {
            if (!double.IsNaN(this.Width))
            {
                this.TextBoxInternalWidth = this.Width;
                this.Width = double.NaN;
            }
        }

        /// <summary>
        /// Called when the number of decimals property is changed, re-formats the textbox is necessary
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void HandleNumberOfDecimalPlacesChanged(object sender, PropertyChangedEventArgs e)
        {
            if (this.InputType == TextBoxControlInputType.Money
                || this.InputType == TextBoxControlInputType.Decimal)
            {
                this.MaxDecimalPlaces = UserSettingsManager.Instance.DecimalPlacesNumber;
                SetDisplayedValue(this);
            }
        }

        /// <summary>
        /// Trims the input value to the given number of digits. Note that this method does not do any rounding.
        /// </summary>
        /// <param name="inputValue">The input value.</param>
        /// <param name="numberOfDigits">The number of digits.</param>
        /// <returns>The input value having the decimal part trimmed to the given number of digits, or less if the input value contained less digits.</returns>
        public static string TrimToNumberOfDigits(decimal inputValue, int numberOfDigits)
        {
            // Format the input value using the needed number of digits + 1. This way "infinite" decimal values (e.g. 1.999999999999) will be rounded up, but in other cases the numbers which will be
            // displayed will not be modified.
            StringBuilder formatString = new StringBuilder("{0:#,0.");
            for (var digitsIndex = 0; digitsIndex <= numberOfDigits; digitsIndex++)
            {
                formatString.Append("#");
            }

            formatString.Append("}");

            var result = string.Format(CultureInfo.CurrentCulture, formatString.ToString(), inputValue);
            var decimalSeparator = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;

            var indexOfDecimalSeparator = result.IndexOf(decimalSeparator, StringComparison.OrdinalIgnoreCase);
            if (indexOfDecimalSeparator > -1)
            {
                // Get only the required number of decimal places (Substring). After that trim the ending zeros (first TrimEnd) and the decimal separator (second TrimEnd). 
                // Note that trimming the decimal separator is done only if all the decimal places were zeros and thus all were trimmed.
                result = result.Substring(0, Math.Min(result.Length, indexOfDecimalSeparator + numberOfDigits + 1)).TrimEnd(new[] { '0' }).TrimEnd(decimalSeparator.ToCharArray());
            }

            return result;
        }

        /// <summary>
        /// Sets the text inside the unit symbol label.
        /// You should use this method and not set the text directly using the unitSymbol member.
        /// </summary>        
        private void ApplySymbol()
        {
            // The symbol can be set only after the control template is applied (only then the symbol label becomes available).
            if (this.symbolLabel != null)
            {
                var symbol = this.Symbol != null ? this.Symbol.ToString() : string.Empty;
                if (!string.IsNullOrEmpty(symbol) && !string.IsNullOrEmpty(this.SymbolFormat))
                {
                    this.symbolLabel.Text = string.Format(this.SymbolFormat, symbol);
                }
                else if (!string.IsNullOrEmpty(symbol))
                {
                    this.symbolLabel.Text = symbol;
                }
                else
                {
                    this.symbolLabel.Text = string.Empty;
                }

                // If the symbol has at least a character show the symbol label, else collapse it.
                if (!string.IsNullOrEmpty(symbol) && this.symbolLabel.Visibility != Visibility.Visible)
                {
                    this.symbolLabel.Visibility = Visibility.Visible;
                }
                else if (string.IsNullOrEmpty(symbol) && this.symbolLabel.Visibility == Visibility.Visible)
                {
                    this.symbolLabel.Visibility = Visibility.Collapsed;
                }
            }
        }

        #endregion Methods
    }
}