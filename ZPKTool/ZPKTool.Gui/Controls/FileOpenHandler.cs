﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using ZPKTool.Gui.Resources;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Controls
{
    /// <summary>
    /// Handles the opening of the file, creating a new process
    /// </summary>
    public class FileOpenHandler
    {
        /// <summary>
        /// path to the opened file
        /// </summary>
        private string path;

        /// <summary>
        /// reference to the process in which the file is opened
        /// </summary>
        private System.Diagnostics.Process process;

        /// <summary>
        /// Initializes a new instance of the <see cref="FileOpenHandler"/> class.
        /// </summary>
        /// <param name="path">The path of the file</param>
        /// <param name="deleteAfterOpen">if set to <c>true</c> [delete after open].</param>
        public FileOpenHandler(string path, bool deleteAfterOpen)
        {
            try
            {
                this.path = path;
                this.process = new System.Diagnostics.Process();
                this.process.StartInfo = new ProcessStartInfo(path);
                this.process.EnableRaisingEvents = deleteAfterOpen;
                this.process.Exited += new EventHandler(ProcessExited);
                this.process.Start();
            }
            catch (System.ComponentModel.Win32Exception)
            {
                MessageWindow.Show(LocalizedResources.Error_FailedOpenFile, MessageDialogType.Error);
            }
        }

        /// <summary>
        /// Handles the Exited event of the process
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ProcessExited(object sender, EventArgs e)
        {
            if (File.Exists(this.path))
            {
                File.Delete(this.path);
            }
        }
    }
}
