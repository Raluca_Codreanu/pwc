namespace ZPKTool.Gui.Controls
{
    using System;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Threading;
    using ZPKTool.Gui.Resources;

    /// <summary>
    /// Define the possible status of the video element
    /// </summary>
    internal enum VideoPlaybackStatus
    {
        /// <summary>
        /// while the video is playing
        /// </summary>
        Playing,

        /// <summary>
        /// while the video is paused
        /// </summary>
        Paused,

        /// <summary>
        /// while the video is stopped
        /// </summary>
        Stopped
    }

    /// <summary>
    /// This class implements a video player control.
    /// </summary>
    public partial class VideoPlayer
    {
        #region Fields

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Indicates the current playback state.
        /// </summary>
        private VideoPlaybackStatus playbackStatus = VideoPlaybackStatus.Stopped;

        /// <summary>
        /// Timer used to update the seek slider.
        /// </summary>
        private DispatcherTimer timer = new DispatcherTimer();

        #endregion Fields

        /// <summary>
        /// Initializes a new instance of the <see cref="VideoPlayer"/> class.
        /// </summary>
        public VideoPlayer()
        {
            this.InitializeComponent();
            this.Mute();

            this.timer.Interval = new TimeSpan(0, 0, 0, 0, 500);
            this.timer.Tick += new EventHandler(this.Timer_Tick);
        }

        /// <summary>
        /// Occurs when playback is attempted without having a source (SourcePath is null or empty).
        /// <para/>
        /// The 1st parameter is the instance that triggered the event and the 2nd is the event arguments, which are always EventArgs.Empty
        /// </summary>
        public event Action<object, EventArgs> RequestSource;

        /// <summary>
        /// Occurs when the action area of the control is clicked.
        /// </summary>
        public event RoutedEventHandler Click;

        /// <summary>
        /// Gets or sets a value indicating whether the control is currently toggling between full screen and normal size.
        /// </summary>       
        public bool IsTogglingFullScreen
        {
            get { return (bool)GetValue(IsTogglingFullScreenProperty); }
            set { SetValue(IsTogglingFullScreenProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for IsTogglingFullScreen.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty IsTogglingFullScreenProperty =
            DependencyProperty.Register("IsTogglingFullScreen", typeof(bool), typeof(VideoPlayer), new UIPropertyMetadata(false));

        /// <summary>
        /// Gets or sets the path to the video file to be played by this instance.
        /// </summary>        
        public string SourcePath
        {
            get { return (string)GetValue(SourcePathProperty); }
            set { SetValue(SourcePathProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for SourcePath.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty SourcePathProperty =
            DependencyProperty.Register("SourcePath", typeof(string), typeof(VideoPlayer), new UIPropertyMetadata(null, OnSourcePathChanged));

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="VideoPlayer"/> is currently playing.
        /// <para />
        /// This property also controls the playback (true starts the playback, false stops it and null pauses it).
        /// </summary>
        public bool? IsPlaying
        {
            get { return (bool?)GetValue(IsPlayingProperty); }
            set { SetValue(IsPlayingProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for IsPlaying.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty IsPlayingProperty =
            DependencyProperty.Register("IsPlaying", typeof(bool?), typeof(VideoPlayer), new UIPropertyMetadata(false, OnIsPlayingChanged, OnIsPlayingCoerceValue));

        /// <summary>
        /// Called when SourcePath changed.
        /// </summary>
        /// <param name="depObj">The dependency object.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnSourcePathChanged(DependencyObject depObj, DependencyPropertyChangedEventArgs e)
        {
            VideoPlayer player = depObj as VideoPlayer;
            if (player == null)
            {
                return;
            }

            string path = e.NewValue as string;
            Uri source = null;
            if (!string.IsNullOrWhiteSpace(path))
            {
                source = new Uri(path);
            }

            player.MediaElement.Source = source;
        }

        /// <summary>
        /// Called when coercing the value of IsPlaying.
        /// </summary>
        /// <param name="depObj">The dependency object.</param>
        /// <param name="baseValue">The base value.</param>
        /// <returns>The value for the IsPlaying property.</returns>
        private static object OnIsPlayingCoerceValue(DependencyObject depObj, object baseValue)
        {
            VideoPlayer player = depObj as VideoPlayer;
            if (player == null)
            {
                return baseValue;
            }

            bool? play = baseValue as bool?;

            if (play.HasValue && play.Value
                && string.IsNullOrWhiteSpace(player.SourcePath))
            {
                // Request a playback source when attempting to play without having a playback source.
                var handler = player.RequestSource;
                if (handler != null)
                {
                    handler(player, EventArgs.Empty);
                }

                // todo: roland: check if this should be false as it was before.
                return baseValue;
            }

            return baseValue;
        }

        /// <summary>
        /// Called when IsPlaying changed.
        /// </summary>
        /// <param name="depObj">The dependency object.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnIsPlayingChanged(DependencyObject depObj, DependencyPropertyChangedEventArgs e)
        {
            VideoPlayer player = depObj as VideoPlayer;
            if (player == null)
            {
                return;
            }

            // Start or stop the playback
            bool? play = e.NewValue as bool?;
            if (!play.HasValue)
            {
                // Pause the playback.
                player.playbackStatus = VideoPlaybackStatus.Paused;
                player.PlayPauseButton.ImageAllStates = Images.PlayIcon;
                player.MediaElement.Pause();
            }
            else if (play.Value)
            {
                // Start the playback.
                player.MediaElement.Play();
                player.playbackStatus = VideoPlaybackStatus.Playing;
                player.PlayPauseButton.ImageAllStates = Images.PauseIcon;
                player.timer.Start();
                player.SeekSlider.IsEnabled = true;
            }
            else
            {
                // Stop the playback
                player.playbackStatus = VideoPlaybackStatus.Stopped;
                player.PlayPauseButton.ImageAllStates = Images.PlayIcon;
                player.MediaElement.Stop();
                player.timer.Stop();
                player.SeekSlider.Value = 0;
                player.SeekSlider.IsEnabled = false;
            }
        }

        #region Playback control

        /// <summary>
        /// Mute the video
        /// </summary>
        private void Mute()
        {
            this.MediaElement.IsMuted = true;
            this.MuteButton.ImageAllStates = Images.NoSpeakerIcon;
        }

        /// <summary>
        /// UnMute the video
        /// </summary>
        private void UnMute()
        {
            this.MediaElement.IsMuted = false;
            this.MuteButton.ImageAllStates = Images.SpeakerIcon;
        }

        #endregion Playback control

        #region Event handlers

        /// <summary>
        /// Event triggered on each unloading of the media element
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void VideoPlayer_Unloaded(object sender, RoutedEventArgs e)
        {
            if (!this.IsTogglingFullScreen)
            {
                this.IsPlaying = false;

                // Close the media to release all used resources.
                this.MediaElement.Close();
            }
        }

        /// <summary>
        /// Event triggered on each change of the visibility for the media element
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void VideoPlayer_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (!this.IsVisible && !this.IsTogglingFullScreen)
            {
                this.IsPlaying = false;
            }
        }

        /// <summary>
        /// Event raised when the video media is opened.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void MediaElement_MediaOpened(object sender, RoutedEventArgs e)
        {
            // Set the seek slider maximum value to the full duration of the video.
            // If the video duration couldn't be calculated then the seek slider maximum value can't be set
            // and trying to use the seek slider will not have any effect
            if (this.MediaElement.NaturalDuration.HasTimeSpan)
            {
                this.SeekSlider.Maximum = this.MediaElement.NaturalDuration.TimeSpan.TotalMilliseconds;
            }
        }

        /// <summary>
        /// Event raised when the media has ended playing
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void MediaElement_MediaEnded(object sender, RoutedEventArgs e)
        {
            this.IsPlaying = false;
        }

        /// <summary>
        /// Event raised when the video is clicked
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void MouseActionArea_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            // Trigger the Click event.
            var handler = this.Click;
            if (handler != null)
            {
                handler(sender, e);
            }
        }

        /// <summary>
        /// Event raised when the Play/Pause button is clicked
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void PlayPauseButton_Click(object sender, RoutedEventArgs e)
        {
            // Play or pause, based on the current playback status
            switch (playbackStatus)
            {
                // pause the video
                case VideoPlaybackStatus.Playing:
                    this.IsPlaying = null;
                    break;

                // play the video
                case VideoPlaybackStatus.Paused:
                case VideoPlaybackStatus.Stopped:
                    this.IsPlaying = true;
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// Event raised when the Stop button is clicked
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void StopButton_Click(object sender, RoutedEventArgs e)
        {
            // Stop if the video is playing or paused
            if (this.playbackStatus == VideoPlaybackStatus.Playing || this.playbackStatus == VideoPlaybackStatus.Paused)
            {
                this.IsPlaying = false;
            }
        }

        /// <summary>
        /// Event raised when the mute button is clicked
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void MuteButton_Click(object sender, RoutedEventArgs e)
        {
            // Toggle the mute state.
            if (this.MediaElement.IsMuted)
            {
                this.UnMute();
            }
            else
            {
                this.Mute();
            }
        }

        /// <summary>
        /// Event raised when the seek slider is clicked
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void SeekSlider_PreviewMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            // If the slider maximum value is the default value means that the maximum video length is not set and in this case seek will have no effects
            if (this.SeekSlider.Maximum == 1d)
            {
                return;
            }

            // stop the timer while dragging the slider
            this.timer.Stop();
        }

        /// <summary>
        /// Event raised when the seek slider is clicked.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void SeekSlider_PreviewMouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            // If the slider maximum value is the default value means that the maximum video length is not set and in this case seek will have no effects
            if (this.SeekSlider.Maximum == 1d)
            {
                return;
            }

            // Start the timer when the mouse button is released
            this.timer.Start();

            // Jump to the seek position.            
            this.MediaElement.Position = new TimeSpan(0, 0, 0, 0, (int)this.SeekSlider.Value);
        }

        /// <summary>
        /// Event raised when the VolumeSlider position has changed
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void VolumeSlider_ValueChanged(object sender, RoutedEventArgs e)
        {
            this.UnMute();
            this.MediaElement.Volume = this.VolumeSlider.Value;
        }

        /// <summary>
        /// Event raised when the timer ticks
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void Timer_Tick(object sender, EventArgs e)
        {
            // Update the seek slider position
            this.SeekSlider.Value = this.MediaElement.Position.TotalMilliseconds;
        }

        #endregion Event handlers
    }
}