﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Media;
using ZPKTool.Common;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Services;
using ZPKTool.Gui.Utils;
using ZPKTool.Gui.ViewModels;
using ZPKTool.MvvmCore.Services;

namespace ZPKTool.Gui.Controls
{
    /// <summary>
    /// This class represents the message windows in the application.
    /// </summary>
    public partial class MessageWindow : Window
    {
        #region Attributes

        /// <summary>
        /// The result returned by the Show methods.
        /// </summary>
        private static MessageDialogResult result = MessageDialogResult.None;

        /// <summary>
        /// The exception for which this dialog displays a message. It is set only when <see cref="Show(Exception e)"/> is called.
        /// </summary>
        private Exception exception;

        /// <summary>
        /// The type of the message dialog to display. It determines the icon and buttons displayed.
        /// </summary>
        private MessageDialogType windowType;

        #endregion

        #region Constructors

        /// <summary>
        /// Prevents a default instance of the <see cref="MessageWindow"/> class from being created.
        /// </summary>
        private MessageWindow()
        {
            this.InitializeComponent();
            this.Owner = WindowHelper.DetermineWindowOwner(this);
            if (this.Owner != null)
            {
                this.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            }
            else
            {
                this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageWindow" /> class.
        /// </summary>
        /// <param name="message">The message to display in the window.</param>
        /// <param name="additionalMessage">The additional message</param>
        /// <param name="type">The type of the window. Influences the icon and available buttons.</param>
        /// <param name="exception">The exception for which to show the message.</param>
        private MessageWindow(string message, string additionalMessage, MessageDialogType type, Exception exception)
            : this()
        {
            this.MessageText.Text = message;
            this.windowType = type;
            this.exception = exception;

            if (!string.IsNullOrWhiteSpace(additionalMessage))
            {
                this.MessageNoteText.Text = additionalMessage;
                this.MessageNoteText.Visibility = Visibility.Visible;
            }

            switch (type)
            {
                case MessageDialogType.Info:
                    AutomationProperties.SetAutomationId(this, "Info");
                    this.Title = LocalizedResources.General_Info;
                    this.MessageImage.Source = Images.InfoIcon;

                    this.OkButton.Visibility = Visibility.Visible;
                    this.OkButton.IsDefault = true;
                    break;

                case MessageDialogType.Warning:
                    AutomationProperties.SetAutomationId(this, "Warning");
                    this.Title = LocalizedResources.General_Warning;
                    this.MessageImage.Source = Images.WarningIcon;

                    this.OkButton.Visibility = Visibility.Visible;
                    this.OkButton.IsDefault = true;
                    break;

                case MessageDialogType.Error:
                    AutomationProperties.SetAutomationId(this, "Error");
                    this.Title = LocalizedResources.General_Error;
                    this.MessageImage.Source = Images.ErrorIcon;

                    this.OkButton.Visibility = Visibility.Visible;
                    this.OkButton.IsDefault = true;

                    if (message == LocalizedResources.Error_Internal)
                    {
                        this.SendErrorReportButton.Visibility = Visibility.Visible;
                    }

                    break;

                case MessageDialogType.YesNo:
                    AutomationProperties.SetAutomationId(this, "Question");
                    this.Title = LocalizedResources.General_Question;
                    this.MessageImage.Source = Images.QuestionImage;

                    this.YesButton.Visibility = Visibility.Visible;
                    this.NoButton.Visibility = Visibility.Visible;
                    this.YesButton.IsDefault = true;
                    break;

                case MessageDialogType.YesNoCancel:
                    AutomationProperties.SetAutomationId(this, "Question");
                    Title = LocalizedResources.General_Question;
                    MessageImage.Source = Images.QuestionImage;

                    this.YesButton.Visibility = Visibility.Visible;
                    this.NoButton.Visibility = Visibility.Visible;
                    this.CancelButton.Visibility = Visibility.Visible;
                    this.YesButton.IsDefault = true;
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageWindow"/> class.
        /// </summary>
        /// <param name="dialogInfo">The message dialog information used to set the window content</param>
        private MessageWindow(MessageDialogInfo dialogInfo)
            : this()
        {
            if (dialogInfo == null)
            {
                throw new ArgumentNullException("dialogInfo", "The custom dialog info was null.");
            }

            this.Title = dialogInfo.Title;
            this.MessageText.Text = dialogInfo.Message;
            if (dialogInfo.IconResourceKey != null)
            {
                this.MessageImage.Source = this.TryFindResource(dialogInfo.IconResourceKey) as ImageSource;
            }

            foreach (MessageDialogActionableItem item in dialogInfo.Actions)
            {
                switch (item.Type)
                {
                    case MessageDialogActionableItemType.Button:
                        // Add customized buttons for each action
                        var newButton = new ZPKTool.Controls.Button();
                        newButton.Content = item.Text;
                        newButton.Margin = new Thickness(5, 0, 5, 0);
                        newButton.MinWidth = 70;
                        AutomationProperties.SetAutomationId(newButton, item.AutomationId);
                        this.ButtonsStackPanel.Children.Add(newButton);

                        var itemAction = item.Action;
                        newButton.Click += new RoutedEventHandler((s, e) =>
                        {
                            this.Close();
                            if (itemAction != null)
                            {
                                itemAction.Invoke();
                            }
                        });

                        break;

                    default:
                        break;
                }
            }
        }

        #endregion Constructors

        #region Public methods

        /// <summary>
        /// Shows a message window.
        /// </summary>
        /// <param name="message">The message to displayed.</param>
        /// <param name="type">The type of the message.</param>
        /// <returns>
        /// A value signifying the action performed by the user on the message window.
        /// </returns>
        public static MessageDialogResult Show(string message, MessageDialogType type)
        {
            MessageWindow wnd = new MessageWindow(message, string.Empty, type, null);
            wnd.ShowDialog();

            // We have to reset the result because it is static and would have this value on the next call to Show.
            MessageDialogResult tmp = result;
            result = MessageDialogResult.None;
            return tmp;
        }

        /// <summary>
        /// Shows a message window with a message note
        /// </summary>
        /// <param name="message">The message to displayed.</param>
        /// <param name="additionalMessage">The additional message</param>
        /// <param name="type">The type of the message.</param>
        /// <returns>
        /// A value signifying the action performed by the user on the message window.
        /// </returns>
        public static MessageDialogResult Show(string message, string additionalMessage, MessageDialogType type)
        {
            MessageWindow wnd = new MessageWindow(message, additionalMessage, type, null);
            wnd.ShowDialog();

            // We have to reset the result because it is static and would have this value on the next call to Show.
            MessageDialogResult tmp = result;
            result = MessageDialogResult.None;
            return tmp;
        }

        /// <summary>
        /// Shows a message window.
        /// </summary>
        /// <param name="e">The exception for which it will display an error message.</param>
        /// <returns>
        /// A value signifying the action performed by the user on the message window.
        /// </returns>
        public static MessageDialogResult Show(Exception e)
        {
            string message = GetErrorMessage(e);

            MessageWindow wnd = new MessageWindow(message, string.Empty, MessageDialogType.Error, e);
            wnd.ShowDialog();

            // We have to reset the result because it is static and would have this value on the next call to Show.
            MessageDialogResult tmp = result;
            result = MessageDialogResult.None;
            return tmp;
        }

        /// <summary>
        /// Shows a custom message window
        /// </summary>
        /// <param name="dialogInfo">The message dialog information used to set the message window content</param>
        /// <returns>A value signifying the action performed by the user on the message window.</returns>
        public static MessageDialogResult Show(MessageDialogInfo dialogInfo)
        {
            MessageWindow wnd = new MessageWindow(dialogInfo);
            wnd.ShowDialog();

            // We have to reset the result because it is static and would have this value on the next call to Show.
            MessageDialogResult tmp = result;
            result = MessageDialogResult.None;
            return tmp;
        }

        /// <summary>
        /// Returns the error message to display for the specified exception.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <returns>
        /// The error message.
        /// </returns>
        public static string GetErrorMessage(Exception exception)
        {
            // Exception thrown by code invoked using reflection are wrapped in a TargetInvocationException.
            if (exception is System.Reflection.TargetInvocationException && exception.InnerException != null)
            {
                exception = exception.InnerException;
            }

            var compositionEx = exception as System.ComponentModel.Composition.CompositionException;
            if (compositionEx != null)
            {
                // A CompositionException can appear not only because of issues in composing MEF parts (which usually is due to bugs on our side),
                // but also if a part's constructor throws an exception. For example, a view-model is exported as a part and in the constructor makes a db call;
                // if the call fails, usually an exception that identifies the problem is thrown. The CompositionException hides the app specific exceptions so we
                // have to extract them.
                var compositionError1 = compositionEx.Errors[0];
                if (compositionError1.Exception != null)
                {
                    // We handle just the 1st error from CompositionException because if there are more they are not app related errors (they are real composition errors). 
                    // There is one composition error for each part, and that error usually contains another CompositionException, so we handle that recursively.
                    // The CompositionException from the composition error should contain a ComposablePartException, which will contain the application related exception.
                    return GetErrorMessage(compositionError1.Exception);
                }
            }

            var composablePartEx = exception as System.ComponentModel.Composition.Primitives.ComposablePartException;
            if (composablePartEx != null)
            {
                // The ComposablePartException should contain the actual application exception in the InnerException property.
                // If it doesn't, the recursive call will fall back to internal error.
                if (composablePartEx.InnerException != null)
                {
                    return GetErrorMessage(composablePartEx.InnerException);
                }
            }

            string message = null;
            ZPKException zpkException = exception as ZPKException;
            if (zpkException != null)
            {
                // Get the error message
                message = string.IsNullOrWhiteSpace(zpkException.ErrorCode) ? exception.Message : LocalizedResources.ResourceManager.GetString(zpkException.ErrorCode);

                // Format the error message using the error message format parameters
                if (!string.IsNullOrWhiteSpace(message)
                    && zpkException.ErrorMessageFormatArgs != null
                    && zpkException.ErrorMessageFormatArgs.Any())
                {
                    message = string.Format(message, zpkException.ErrorMessageFormatArgs.ToArray());
                }
            }
            else
            {
                message = LocalizedResources.ResourceManager.GetString(ErrorCodes.InternalError);
            }

            return message;
        }

        #endregion

        #region Events

        /// <summary>
        /// Handles the Click event of the CancelButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            result = MessageDialogResult.Cancel;
            Close();
        }

        /// <summary>
        /// Handles the Click event of the NoButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void NoButton_Click(object sender, RoutedEventArgs e)
        {
            result = MessageDialogResult.No;
            Close();
        }

        /// <summary>
        /// Handles the Click event of the YesButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void YesButton_Click(object sender, RoutedEventArgs e)
        {
            result = MessageDialogResult.Yes;
            Close();
        }

        /// <summary>
        /// Handles the Click event of the OKButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            result = MessageDialogResult.OK;
            Close();
        }

        /// <summary>
        /// Handles the Click event of the SendErrorReport control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void SendErrorReport_Click(object sender, RoutedEventArgs e)
        {
            result = MessageDialogResult.OK;
            Close();

            var windowService = new WindowService(new MessageDialogService(new DispatcherService()), new DispatcherService());
            var errorReportVM = new SendErrorReportViewModel(windowService, this.exception);
            windowService.ShowViewInDialog(errorReportVM);
        }

        /// <summary>
        /// Handles the Closing event of the Window control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (result == MessageDialogResult.None)
            {
                if (this.windowType == MessageDialogType.Error
                    || this.windowType == MessageDialogType.Info
                    || this.windowType == MessageDialogType.Warning)
                {
                    result = MessageDialogResult.OK;
                }
                else if (this.windowType == MessageDialogType.YesNo)
                {
                    result = MessageDialogResult.No;
                }
                else if (this.windowType == MessageDialogType.YesNoCancel)
                {
                    result = MessageDialogResult.Cancel;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        /// <summary>
        /// Handles the KeyDownEvent
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.KeyEventArgs"/> instance containing the event data.</param>
        private void Window_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Escape)
            {
                Close();
            }
        }

        #endregion
    }
}