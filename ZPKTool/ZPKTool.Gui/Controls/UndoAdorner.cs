﻿namespace ZPKTool.Gui.Controls
{
    using System.Windows;
    using System.Windows.Documents;
    using System.Windows.Media;

    /// <summary>
    /// The Undo Adorner class, used to highlights the UIElements, after an undo action was performed.
    /// </summary>
    public class UndoAdorner : Adorner
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="UndoAdorner"/> class. Represents the base class constructor.
        /// </summary>
        /// <param name="adornedElement">The adorner element.</param>
        public UndoAdorner(UIElement adornedElement)
            : base(adornedElement)
        {
        }

        #endregion Constructor

        #region On Render

        /// <summary>
        /// When overridden in a derived class, participates in rendering operations that are directed by the layout system. 
        /// The rendering instructions for this element are not used directly when this method is invoked, and are instead preserved for later asynchronous use by layout and drawing.
        /// </summary>
        /// <param name="drawingContext">The drawing instructions for a specific element. This context is provided to the layout system.</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            // Use the size of the adorner instead of the size of the element because sometimes the size of the element is incorrect.
            Rect adornedElementRect = new Rect(this.RenderSize);

            // Changed the size of the adorner to not cover some controls.
            var extendedTextBox = this.AdornedElement as ExtendedTextBoxControl;
            var textBoxWithUnit = this.AdornedElement as TextBoxWithUnitSelector;
            if ((extendedTextBox != null && extendedTextBox.Symbol != null)
                || textBoxWithUnit != null)
            {
                adornedElementRect.Width += 3;
            }

            // Style settings.
            SolidColorBrush renderBrush = new SolidColorBrush(Colors.Gray);
            renderBrush.Opacity = 0.2;
            Pen renderPen = new Pen(new SolidColorBrush(Colors.DarkGray), 1.5);
            double renderRadius = 5.0;

            // Draw a rounded rectangle over the adorner UIElement.
            drawingContext.DrawRoundedRectangle(renderBrush, renderPen, adornedElementRect, renderRadius, renderRadius);
        }

        #endregion On Render
    }
}
