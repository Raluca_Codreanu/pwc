﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using ZPKTool.Controls;
using ZPKTool.Data;
using ZPKTool.Gui.Managers;
using ZPKTool.Gui.Utils;

namespace ZPKTool.Gui.Controls
{
    /// <summary>
    /// A text box that has an associated units selector and automatically converts its value when the selector option is changed.
    /// </summary>
    public class TextBoxWithUnitSelector : TextBoxControl
    {
        #region Fields

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The units selection control.
        /// </summary>
        private ComboBoxControl unitsSelector;

        /// <summary>
        /// A flag indicating whether the BaseValue property was set by internal code (true - was set internally, false - it was set by client code that uses the text box).
        /// </summary>
        private bool baseValuePropertySetInternally = false;

        /// <summary>
        /// A flag indicating whether to trigger the SelectedUnitChanged event on the next change.
        /// </summary>
        /// <remarks>Setting it to false will ignore only the next event after it is set.</remarks>
        private bool triggerUnitSelectionChangedEvent = true;

        #endregion Fields

        #region Initialization

        /// <summary>
        /// Initializes static members of the <see cref="TextBoxWithUnitSelector"/> class.
        /// </summary>
        static TextBoxWithUnitSelector()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(TextBoxWithUnitSelector), new FrameworkPropertyMetadata(typeof(TextBoxWithUnitSelector)));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TextBoxWithUnitSelector"/> class.
        /// </summary>
        public TextBoxWithUnitSelector()
        {
        }

        #endregion Initialization

        #region DependencyProperties

        /// <summary>
        /// Using a DependencyProperty as the backing store for UnitType.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty UnitTypeProperty =
            DependencyProperty.Register("UnitType", typeof(MeasurementUnitType), typeof(TextBoxControl));

        /// <summary>
        /// Using a DependencyProperty as the backing store for BaseValue.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty BaseValueProperty =
            DependencyProperty.Register("BaseValue", typeof(decimal?), typeof(TextBoxWithUnitSelector), new UIPropertyMetadata(null, OnBaseValueChanged));

        /// <summary>
        /// Using a DependencyProperty as the backing store for SelectedUnit.  This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty SelectedUnitProperty =
            DependencyProperty.Register("SelectedUnit", typeof(MeasurementUnit), typeof(TextBoxWithUnitSelector), new UIPropertyMetadata(null, OnSelectedUnitChanged));

        /// <summary>
        /// Using a DependencyProperty as the backing store for SelectorUnits.  This enables animation, styling, binding, etc...
        /// </summary> 
        public static readonly DependencyProperty SelectorUnitsProperty =
            DependencyProperty.Register("SelectorUnits", typeof(ObservableCollection<MeasurementUnit>), typeof(TextBoxWithUnitSelector), new UIPropertyMetadata(null, OnSelectorUnitsChanged));

        /// <summary>
        /// Using a DependencyProperty as the backing store for MeasurementUnits.  This enables animation, styling, binding, etc...
        /// </summary> 
        public static readonly DependencyProperty MeasurementUnitsProperty =
            DependencyProperty.Register("MeasurementUnits", typeof(Collection<MeasurementUnit>), typeof(TextBoxWithUnitSelector), new UIPropertyMetadata(null, OnMeasurementUnitsChanged));

        #endregion DependencyProperties

        #region Properties

        /// <summary>
        /// Called when the selection is changed in the combo box
        /// </summary>
        public event SelectionChangedEventHandler SelectedUnitChanged;

        /// <summary>
        /// Gets or sets the type UI unit displayed next to the text box.
        /// </summary>
        public MeasurementUnitType UnitType
        {
            get { return (MeasurementUnitType)GetValue(UnitTypeProperty); }
            set { SetValue(UnitTypeProperty, value); }
        }

        /// <summary>
        /// Gets or sets the number based on which a value is displayed in the text box.
        /// <para/>
        /// If UIUnit is set to null, the value is converted to its string representation and displayed. If UIUnit is set to something else,
        /// the number is interpreted as a base value and it will be converted to the corresponding UI unit before displaying it; when retrieving it,
        /// the displayed value will be converted back the base value.
        /// <para/>
        /// Use the Text property to get the value as it is displayed.
        /// </summary>      
        public decimal? BaseValue
        {
            get { return (decimal?)GetValue(BaseValueProperty); }
            set { SetValue(BaseValueProperty, value); }
        }

        /// <summary>
        /// Gets or sets the selected unit.
        /// </summary>        
        public MeasurementUnit SelectedUnit
        {
            get { return (MeasurementUnit)GetValue(SelectedUnitProperty); }
            set { SetValue(SelectedUnitProperty, value); }
        }

        /// <summary>
        /// Gets or sets the units.
        /// </summary>
        public ObservableCollection<MeasurementUnit> SelectorUnits
        {
            get { return (ObservableCollection<MeasurementUnit>)GetValue(SelectorUnitsProperty); }
            set { SetValue(SelectorUnitsProperty, value); }
        }

        /// <summary>
        /// Gets or sets the measurement units.
        /// </summary>
        public Collection<MeasurementUnit> MeasurementUnits
        {
            get { return (Collection<MeasurementUnit>)GetValue(MeasurementUnitsProperty); }
            set { SetValue(MeasurementUnitsProperty, value); }
        }

        #endregion Properties

        #region DependencyPropertyCallbacks

        /// <summary>
        /// Called when the BaseValue property has changed.
        /// </summary>
        /// <param name="d">The instance whose property has changed.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnBaseValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var textBox = (TextBoxWithUnitSelector)d;
            if (textBox.baseValuePropertySetInternally)
            {
                return;
            }

            // When the base value is changed, set the new value into the textbox.
            var newValue = e.NewValue as decimal?;
            var text = string.Empty;

            if (newValue.HasValue)
            {
                // Compute the new value which is displayed in the textbox.
                var displayValue = newValue.Value;
                var baseScaleUnit = textBox.SelectorUnits != null ? textBox.SelectorUnits.FirstOrDefault(u => u.IsBaseUnit) : null;

                if (textBox.SelectedUnit != null
                    && baseScaleUnit != null
                    && baseScaleUnit.ConversionRate != 0m)
                {
                    var scaleFactor = textBox.SelectedUnit.ScaleFactor != 0m ? textBox.SelectedUnit.ScaleFactor : 1m;
                    displayValue = newValue.Value / baseScaleUnit.ConversionRate / scaleFactor;
                }

                text = ExtendedTextBoxControl.TrimToNumberOfDigits(displayValue, textBox.MaxDecimalPlaces);
            }

            textBox.SetTextWithoutTriggeringTextChangedEvent(text);
        }

        /// <summary>
        /// Called when the SelectedUnit property has changed.
        /// </summary>
        /// <param name="d">The instance whose property has changed.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnSelectedUnitChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var textBox = (TextBoxWithUnitSelector)d;
            if (textBox.SelectorUnits == null)
            {
                return;
            }

            // If the unit has changed re-compute the displayed value.
            var selectedUnit = e.NewValue as MeasurementUnit;
            var unselectedUnit = e.OldValue as MeasurementUnit;
            var baseUnit = textBox.SelectorUnits.FirstOrDefault(u => u.IsBaseUnit);

            // If the unit is not in the combobox (for example a released unit) it must be matched with the corresponding one from the combobox.
            if (selectedUnit != null
                && !textBox.SelectorUnits.Contains(selectedUnit))
            {
                var unitInComboBox = textBox.SelectorUnits.FirstOrDefault(u => string.Compare(u.Symbol, selectedUnit.Symbol, true) == 0 && u.Type == selectedUnit.Type);
                if (unitInComboBox != null)
                {
                    textBox.SelectedUnit = unitInComboBox;
                }
                else
                {
                    textBox.SelectedUnit = textBox.SelectorUnits.FirstOrDefault(u => u.IsBaseUnit);
                }
            }
            else if (selectedUnit != null
                     && baseUnit != null
                     && unselectedUnit != null
                     && selectedUnit != unselectedUnit
                     && unselectedUnit.Type == selectedUnit.Type)
            {
                // Convert the current displayed value from the old unit to the new one.
                if (textBox.BaseValue.HasValue)
                {
                    // Note: The BaseValue must be divided by the baseUnit.ConversionRate because, for example in the Consumables screen the Amount is stored in Metric value,
                    // but the selected unit can be imperial. In other cases the baseUnit.ConversionRate is 1, which does not affect the calculations.
                    var newValue = Unit.ConvertValue(textBox.BaseValue.Value / baseUnit.ConversionRate, baseUnit, selectedUnit);
                    textBox.SetTextWithoutTriggeringTextChangedEvent(ExtendedTextBoxControl.TrimToNumberOfDigits(newValue, textBox.MaxDecimalPlaces));
                }
            }
        }

        /// <summary>
        /// Called when the Units property has changed.
        /// </summary>
        /// <param name="d">The instance whose property has changed.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnSelectorUnitsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var textBox = (TextBoxWithUnitSelector)d;
            if (textBox.SelectorUnits != null && textBox.SelectorUnits.Count > 0)
            {
                textBox.SelectedUnit = textBox.SelectorUnits[0];
            }
        }

        /// <summary>
        /// Called when the MeasurementUnits property has changed.
        /// </summary>
        /// <param name="d">The instance whose property has changed.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnMeasurementUnitsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var textBox = (TextBoxWithUnitSelector)d;

            IEnumerable<MeasurementUnit> newUnits = null;
            if (e.NewValue != null)
            {
                newUnits = (IEnumerable<MeasurementUnit>)e.NewValue;
            }
            else if (e.OldValue != null)
            {
                newUnits = (IEnumerable<MeasurementUnit>)e.OldValue;
            }

            // Determine the new and the current base units.
            var newBaseUnit = newUnits.FirstOrDefault(u => u.IsBaseUnit && u.Type == textBox.UnitType);
            var currentBaseUnit = textBox.SelectorUnits != null ? textBox.SelectorUnits.FirstOrDefault(u => u.IsBaseUnit && u.Type == textBox.UnitType) : null;

            // If the base unit has changed converted the base value and load the new units.
            if (currentBaseUnit != null
                && currentBaseUnit.Guid != newBaseUnit.Guid)
            {
                try
                {
                    string newValue = null;
                    if (textBox.BaseValue.HasValue)
                    {
                        // Note: The BaseValue must be divided by the baseUnit.ConversionRate because for example in the Consumables screen the Amount is stored in Metric value,
                        // but the selected unit can be imperial. In other cases the baseUnit.ConversionRate is 1, which does not affect the calculations.
                        decimal valueInNewUnit = Unit.ConvertValue(textBox.BaseValue.Value / currentBaseUnit.ConversionRate, currentBaseUnit, newBaseUnit);
                        newValue = ExtendedTextBoxControl.TrimToNumberOfDigits(valueInNewUnit, textBox.MaxDecimalPlaces);
                    }

                    // This call also has the (good) side effect of triggering the UnitsSelectorSelectionChanged handler,
                    // but the selection changed event must not be triggered (save/cancel buttons remain inactive when changing the units).
                    textBox.triggerUnitSelectionChangedEvent = false;
                    textBox.SelectorUnits = new ObservableCollection<MeasurementUnit>(newUnits.Where(u => u.Type == textBox.UnitType).ToList());

                    // Set the value expressed in the new unit.
                    textBox.SetTextWithoutTriggeringTextChangedEvent(newValue);
                }
                catch (Exception ex)
                {
                    string msg = "TextBoxWithUnitSelector - Failed to update value to new unit (control name: " + textBox.Name + ")";
                    log.ErrorException(msg, ex);
                }
            }
            else
            {
                textBox.SelectorUnits = new ObservableCollection<MeasurementUnit>();
                textBox.SelectorUnits.AddRange(newUnits.Where(u => u.Type == textBox.UnitType).ToList());
                if (textBox.SelectedUnit == null
                    || textBox.SelectorUnits.FirstOrDefault(u => u.Guid == textBox.SelectedUnit.Guid) == null)
                {
                    textBox.SelectedUnit = textBox.SelectorUnits.FirstOrDefault(u => u.IsBaseUnit);
                }
            }
        }

        #endregion DependencyPropertyCallbacks

        #region Other Methods

        /// <summary>
        /// Sets the text without triggering the internal TextChanged event handler.
        /// </summary>
        /// <param name="text">The text to set.</param>
        private void SetTextWithoutTriggeringTextChangedEvent(string text)
        {
            bool suspendBak = this.SuspendTextChangedEvent;
            try
            {
                this.SuspendTextChangedEvent = true;
                this.Text = text;
            }
            finally
            {
                this.SuspendTextChangedEvent = suspendBak;
            }
        }

        /// <summary>
        /// Calculates the value for the BaseValue property from the current Text value.
        /// </summary>
        /// <returns>The new value for the BaseValue property.</returns>
        private decimal? CalculateValue()
        {
            var currentValue = Converter.StringToNullableDecimal(this.Text);
            if (!currentValue.HasValue)
            {
                return null;
            }

            // Compute the new value based on the input from the textbox and the unit.
            decimal? baseValue = null;
            var baseScaleUnit = this.SelectorUnits != null ? this.SelectorUnits.FirstOrDefault(u => u.IsBaseUnit) : null;
            if (baseScaleUnit != null &&
                this.SelectedUnit != null)
            {
                var scaleFactor = this.SelectedUnit.ScaleFactor != 0m ? this.SelectedUnit.ScaleFactor : 1m;
                baseValue = currentValue * scaleFactor * baseScaleUnit.ConversionRate;
            }
            else
            {
                baseValue = currentValue;
            }

            return baseValue;
        }

        /// <summary>
        /// Is called when a control template is applied.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            bool inDesignMode = System.ComponentModel.DesignerProperties.GetIsInDesignMode(this);

            this.unitsSelector = this.Template.FindName("PART_UnitSelector", this) as ComboBoxControl;
            if (this.unitsSelector != null)
            {
                this.unitsSelector.SelectionChanged += this.UnitsSelectorSelectionChanged;
                this.unitsSelector.Width = double.NaN;
                this.unitsSelector.MinWidth = 35d;
            }
            else if (!inDesignMode)
            {
                throw new InvalidOperationException("Could not locate the PART_UnitSelector element in the template of the TextBoxWithUnitSelector.");
            }
        }

        #endregion Other Methods

        #region Event Handlers

        /// <summary>
        /// Handles the SelectionChanged event of the UnitsSelector control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Controls.SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void UnitsSelectorSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                // Send the selected unit changed event only when the selection change was done manually by selecting a different value in the combobox.
                if (this.SelectedUnitChanged != null && this.triggerUnitSelectionChangedEvent)
                {
                    this.SelectedUnitChanged(sender, e);
                }
            }
            finally
            {
                // Set the trigger flag to true so that the manual selection works properly.
                this.triggerUnitSelectionChangedEvent = true;
            }
        }

        /// <summary>
        /// Is called when content in this editing control changes.
        /// </summary>
        /// <param name="e">The arguments that are associated with the <see cref="E:System.Windows.Controls.Primitives.TextBoxBase.TextChanged"/> event.</param>
        protected override void OnTextChanged(TextChangedEventArgs e)
        {
            if (this.SuspendTextChangedEvent)
            {
                // The base implementation must be called, otherwise the TextBoxControl's internal state will not be correctly updated.
                base.OnTextChanged(e);
                return;
            }

            try
            {
                // Check if the new text is valid before recalculating the base value.
                bool isTextValid = true;
                var change = e.Changes.FirstOrDefault();
                if (change != null)
                {
                    isTextValid = this.ValidateCurrentTextOnTextChange(change);
                }

                // When the text changes, recalculate the base value.
                if (isTextValid)
                {
                    this.baseValuePropertySetInternally = true;
                    this.BaseValue = this.CalculateValue();
                }
            }
            finally
            {
                this.baseValuePropertySetInternally = false;

                // Call the base implementation after the base value is updated so that clients subscribing to the TextChanged event will get the updated BaseValue.
                base.OnTextChanged(e);
            }
        }

        #endregion Event Handlers
    }
}