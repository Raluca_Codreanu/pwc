﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media.Animation;
using Microsoft.Win32;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Controls;
using ZPKTool.Data;
using ZPKTool.Gui.Reporting;
using ZPKTool.Gui.Resources;
using ZPKTool.Gui.Utils;

namespace ZPKTool.Gui.Controls
{
    /// <summary>
    /// This class extends the functionality of System.Windows.Controls.DataGrid.
    /// </summary>
    public class ExtendedDataGrid : DataGrid
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Reference to the visibility property changed notification of data grid column.
        /// </summary>
        private List<DependencyPropertyChangeNotifier> dataGridColumnVisibilityChangedNotifiers;

        /// <summary>
        /// The combo box which stores the names of ExtendedDataGrid columns to filter by.
        /// </summary>
        private ComboBoxControl filterByCombo;

        /// <summary>
        /// The text box which stores the text for filter.
        /// </summary>
        private TextBoxControl filterTextBox;

        /// <summary>
        /// The manager used to export the data grid data.
        /// </summary>
        private ReportsManager reportsManager = new ReportsManager();

        /// <summary>
        /// The open file overlay window witch allows to open the data grid's exported pdf or xml file.
        /// </summary>
        private OpenFileOverlayWindow openFileOverlayWindow;

        /// <summary>
        /// Contains the binding of each column in the data grid. It is populated only if filtering is enabled.
        /// The key is the column and the value is the binding info.
        /// </summary>
        private Dictionary<DataGridColumn, ColumnBindingInfo> columnsBindingForFiltering = new Dictionary<DataGridColumn, ColumnBindingInfo>();

        /// <summary>
        /// A flag used to indicate when the SelectedItems attached property is synchronized 
        /// with the SelectedItems from the base class, to avoid synchronization in revers.
        /// </summary>
        private bool isSelectedItemsInSync = false;

        #endregion Attributes

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtendedDataGrid"/> class.
        /// </summary>
        public ExtendedDataGrid()
            : base()
        {
            this.FilterMode = FilterMode.StartsWith;
            this.EnableExporting = true;

            this.SelectionChanged += this.OnExtendedDataGridSelectionChanged;
            this.dataGridColumnVisibilityChangedNotifiers = new List<DependencyPropertyChangeNotifier>();
        }

        #region Properties

        /// <summary>
        /// Gets or sets the data items corresponding to the selected rows.
        /// </summary>
        public new IList SelectedItems
        {
            get { return (IList)GetValue(SelectedItemsListProperty); }
            set { SetValue(SelectedItemsListProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for SelectedItemsList. This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty SelectedItemsListProperty =
                DependencyProperty.Register("SelectedItems", typeof(IList), typeof(ExtendedDataGrid), new PropertyMetadata(null, OnSelectedItemsChanged));

        /// <summary>
        /// Gets or sets a value indicating whether filtering is enabled. The default is true.
        /// </summary>
        public bool EnableFiltering
        {
            get { return (bool)GetValue(EnableFilteringProperty); }
            set { SetValue(EnableFilteringProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for EnableFiltering. This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty EnableFilteringProperty =
            DependencyProperty.Register("EnableFiltering", typeof(bool), typeof(ExtendedDataGrid), new UIPropertyMetadata(true));

        /// <summary>
        /// Gets or sets the filter mode. The Default value is StartsWith.
        /// </summary>        
        public FilterMode FilterMode
        {
            get { return (FilterMode)GetValue(FilterModeProperty); }
            set { SetValue(FilterModeProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for FilterMode. This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty FilterModeProperty =
            DependencyProperty.Register("FilterMode", typeof(FilterMode), typeof(ExtendedDataGrid), new UIPropertyMetadata(FilterMode.StartsWith));

        /// <summary>
        /// Gets or sets a value indicating whether exporting is enabled. The default is true.
        /// </summary>        
        public bool EnableExporting
        {
            get { return (bool)GetValue(EnableExportingProperty); }
            set { SetValue(EnableExportingProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for EnableExporting. This enables animation, styling, binding, etc... 
        /// </summary>
        public static readonly DependencyProperty EnableExportingProperty =
            DependencyProperty.Register("EnableExporting", typeof(bool), typeof(ExtendedDataGrid), new UIPropertyMetadata(true));

        /// <summary>
        /// Gets or sets the default file name to use when exporting the data grid content.
        /// </summary>
        /// <value>The name of the export default file.</value>
        public string ExportDefaultFileName
        {
            get { return (string)GetValue(ExportDefaultFileNameProperty); }
            set { SetValue(ExportDefaultFileNameProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for ExportDefaultFileName. This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty ExportDefaultFileNameProperty =
            DependencyProperty.Register("ExportDefaultFileName", typeof(string), typeof(ExtendedDataGrid), new UIPropertyMetadata(null));

        /// <summary>
        /// Gets or sets the author of the document containing the exported content.
        /// </summary>
        public string ExportAuthor
        {
            get { return (string)GetValue(ExportAuthorProperty); }
            set { SetValue(ExportAuthorProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for ExportAuthor. This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty ExportAuthorProperty =
            DependencyProperty.Register("ExportAuthor", typeof(string), typeof(ExtendedDataGrid), new UIPropertyMetadata(null));

        /// <summary>
        /// Gets or sets a list of values needed for the report.
        /// </summary>
        public ExtendedDataGridAdditionalReportInformation AdditionalReportInformation
        {
            get { return (ExtendedDataGridAdditionalReportInformation)GetValue(AdditionalReportInformationProperty); }
            set { SetValue(AdditionalReportInformationProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for ExportReportInformation.
        /// </summary>
        public static readonly DependencyProperty AdditionalReportInformationProperty =
            DependencyProperty.Register("AdditionalReportInformation", typeof(ExtendedDataGridAdditionalReportInformation), typeof(ExtendedDataGrid), new UIPropertyMetadata(null));

        #endregion Properties

        #region Static methods

        /// <summary>
        /// Occurs when the SelectedItems changes value.
        /// </summary>
        /// <param name="d">The dependency object that triggered the callback.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnSelectedItemsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var dataGrid = d as ExtendedDataGrid;
            if (dataGrid == null)
            {
                return;
            }

            var oldColl = e.OldValue as INotifyCollectionChanged;
            if (oldColl != null)
            {
                /*
                 * Unsubscribe from CollectionChanged on the old collection.
                 */
                oldColl.CollectionChanged -= dataGrid.OnSelectedItemsCollectionChanged;
            }

            var newColl = e.NewValue as ObservableCollection<object>;
            if (newColl != null)
            {
                /*
                 * Subscribe to CollectionChanged on the new collection.
                 */
                newColl.CollectionChanged += dataGrid.OnSelectedItemsCollectionChanged;
            }
        }

        #endregion Static methods

        /// <summary>
        /// Called after the template was applied to the control.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            foreach (var column in this.Columns)
            {
                var columnVisibilityChangeNotifier = new DependencyPropertyChangeNotifier(column, DataGridColumn.VisibilityProperty);
                columnVisibilityChangeNotifier.ValueChanged += new EventHandler(OnDataGridColumnVisibilityChanged);
                this.dataGridColumnVisibilityChangedNotifiers.Add(columnVisibilityChangeNotifier);
            }

            // Initialize the UI for filtering and exporting
            InitializeFilteringAndExportUIElements();
        }

        /// <summary>
        /// Initializes the filtering and export features.
        /// </summary>
        private void InitializeFilteringAndExportUIElements()
        {
            bool inDesignMode = DesignerProperties.GetIsInDesignMode(this);

            // Initialize the UI elements.            
            this.filterTextBox = this.Template.FindName("FilterTxt", this) as TextBoxControl;
            if (this.filterTextBox == null && !inDesignMode)
            {
                throw new InvalidOperationException("The filter text box (FilterTxt) could not be found in the template.");
            }

            this.filterByCombo = this.Template.FindName("FilterByCmb", this) as ComboBoxControl;
            if (this.filterByCombo == null && !inDesignMode)
            {
                throw new InvalidOperationException("The filter combo box (FilterByCmb) could not be found in the template.");
            }

            var exportToXlsButton = this.Template.FindName("ExportGridToXLSButton", this) as AnimatedImageButton;
            if (exportToXlsButton == null && !inDesignMode)
            {
                throw new InvalidOperationException("The ExportGridToXLSButton element could not be found in the template.");
            }

            var exportToPdfButton = this.Template.FindName("ExportGridToPDFButton", this) as AnimatedImageButton;
            if (this.filterByCombo == null && !inDesignMode)
            {
                throw new InvalidOperationException("The ExportGridToPDFButton element could not be found in the template.");
            }

            if (this.EnableFiltering
                && this.filterByCombo != null
                && this.filterTextBox != null)
            {
                this.filterByCombo.Loaded += new RoutedEventHandler(FilterByCombo_Loaded);
                this.filterByCombo.Unloaded += new RoutedEventHandler(FilterByCombo_Unloaded);
                this.filterByCombo.SelectionChanged += new SelectionChangedEventHandler(FilterByCombo_SelectionChanged);
                this.filterTextBox.TextChanged += new TextChangedEventHandler(FilterTxt_TextChanged);
            }

            if (this.EnableExporting
                && exportToXlsButton != null
                && exportToPdfButton != null)
            {
                exportToXlsButton.Click += new RoutedEventHandler(ExportToXlsButton_Click);
                exportToPdfButton.Click += new RoutedEventHandler(ExportToPdfButton_Click);
            }

            if (this.EnableExporting || this.EnableFiltering)
            {
                this.MouseEnter += new System.Windows.Input.MouseEventHandler(ExtendedDataGrid_MouseEnter);
                this.MouseLeave += new System.Windows.Input.MouseEventHandler(ExtendedDataGrid_MouseLeave);
            }
        }

        #region Filtering logic

        /// <summary>
        /// Reads the binding of all columns and stores it the columnsBindingForFiltering dictionary, to be used by the filtering logic.
        /// If the dictionary is already initialized it does nothing.
        /// </summary>
        private void InitializeColumnsBindingForFiltering()
        {
            if (this.columnsBindingForFiltering.Count > 0)
            {
                return;
            }

            foreach (var column in this.Columns)
            {
                string columnHeader = column.Header as string;
                if (string.IsNullOrEmpty(columnHeader))
                {
                    // Ignore columns without header text.
                    continue;
                }

                var boundColumn = column as DataGridBoundColumn;
                if (boundColumn != null && boundColumn.Binding != null)
                {
                    var info = new ColumnBindingInfo();
                    info.ValueBinding = boundColumn.Binding;
                    this.columnsBindingForFiltering.Add(column, info);
                }
                else
                {
                    var templateColumn = column as DataGridTemplateColumn;
                    if (templateColumn != null)
                    {
                        object content = templateColumn.CellTemplate.LoadContent();
                        var extendedLabel = content as ExtendedLabelControl;
                        if (extendedLabel != null)
                        {
                            var valueBinding = BindingOperations.GetBindingBase(extendedLabel, ExtendedLabelControl.BaseValueProperty);
                            if (valueBinding != null)
                            {
                                var info = new ColumnBindingInfo();
                                info.ValueBinding = valueBinding;

                                var uiUnit = BindingOperations.GetBindingBase(extendedLabel, ExtendedLabelControl.UIUnitProperty);
                                if (uiUnit != null)
                                {
                                    info.UIUnitBinding = uiUnit;
                                }
                                else
                                {
                                    info.UIUnit = extendedLabel.UIUnit;
                                }

                                var baseCurrency = BindingOperations.GetBindingBase(extendedLabel, ExtendedLabelControl.BaseCurrencyProperty);
                                if (baseCurrency != null)
                                {
                                    info.BaseCurrencyBinding = baseCurrency;
                                }
                                else
                                {
                                    info.BaseCurrency = extendedLabel.BaseCurrency;
                                }

                                if (extendedLabel.MaxDigitsToDisplay >= 0)
                                {
                                    info.MaxDecimalPlaces = extendedLabel.MaxDigitsToDisplay;
                                }
                                else
                                {
                                    info.MaxDecimalPlaces = UserSettingsManager.Instance.DecimalPlacesNumber;
                                }

                                info.VisibilityBinding = BindingOperations.GetBindingBase(extendedLabel, ExtendedLabelControl.VisibilityProperty);
                                this.columnsBindingForFiltering.Add(column, info);
                            }
                            else
                            {
                                var textBinding = BindingOperations.GetBindingBase(extendedLabel, ExtendedLabelControl.TextProperty);
                                if (textBinding != null)
                                {
                                    var info = new ColumnBindingInfo();
                                    info.ValueBinding = textBinding;
                                    info.VisibilityBinding = BindingOperations.GetBindingBase(extendedLabel, ExtendedLabelControl.VisibilityProperty);
                                    this.columnsBindingForFiltering.Add(column, info);
                                }
                            }
                        }
                        else
                        {
                            var textBlock = content as TextBlock;
                            if (textBlock != null)
                            {
                                var binding = BindingOperations.GetBindingBase(textBlock, TextBlock.TextProperty);
                                if (binding != null)
                                {
                                    var info = new ColumnBindingInfo();
                                    info.ValueBinding = binding;
                                    info.VisibilityBinding = BindingOperations.GetBindingBase(textBlock, TextBlock.VisibilityProperty);
                                    this.columnsBindingForFiltering.Add(column, info);
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Called when data grid column visibility changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void OnDataGridColumnVisibilityChanged(object sender, EventArgs args)
        {
            this.FillFilterComboBox();
        }

        /// <summary>
        /// Fills the filter combo box.
        /// </summary>
        private void FillFilterComboBox()
        {
            this.filterByCombo.Items.Clear();
            foreach (var col in this.Columns)
            {
                string headerText = col.Header as string;
                if (col.Header != null && !string.IsNullOrEmpty(headerText) && !(col is DataGridComboBoxColumn) && !(col is DataGridCheckBoxColumn))
                {
                    if (!(col.Visibility == Visibility.Hidden || col.Visibility == Visibility.Collapsed))
                    {
                        if (col is DataGridTemplateColumn)
                        {
                            var template = ((DataGridTemplateColumn)col).CellTemplate;
                            template.Seal();
                            var instance = template.LoadContent() as DependencyObject;
                            if (instance != null)
                            {
                                var extendedLabel = instance.FindChildren<ExtendedLabelControl>().FirstOrDefault();
                                var textBlock = instance.FindChildren<TextBlock>().FirstOrDefault();
                                var label = instance.FindChildren<Label>().FirstOrDefault();
                                if (extendedLabel != null || textBlock != null || label != null ||
                                    instance is Label || instance is ExtendedLabelControl || instance is TextBlock)
                                {
                                    this.filterByCombo.Items.Add(headerText);
                                }
                            }
                        }
                        else
                        {
                            this.filterByCombo.Items.Add(headerText);
                        }
                    }
                }
            }

            if (this.filterByCombo.SelectedItem == null)
            {
                this.filterByCombo.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// Determines whether an item is suitable for inclusion in the data grid view.
        /// </summary>
        /// <param name="obj">The item whose inclusion is to be determined.</param>
        /// <returns>
        /// True if the entity should be included, false if it shouldn't.
        /// </returns>
        private bool FilterPredicate(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            if (this.filterByCombo == null || this.filterTextBox == null)
            {
                return true;
            }

            if (string.IsNullOrEmpty(this.filterTextBox.Text) || this.filterByCombo.SelectedItem == null)
            {
                return true;
            }

            // Initialize the columns binding data (only the 1st call actually does the work, the rest just return).
            this.InitializeColumnsBindingForFiltering();

            // Decide if the item should appear in the grid or not.
            string filteringColumnName = this.filterByCombo.SelectedItem as string;
            var filteringColumn = this.Columns.FirstOrDefault(col => col.Header as string == filteringColumnName);
            if (filteringColumn != null)
            {
                ColumnBindingInfo bindingInfo;
                if (this.columnsBindingForFiltering.TryGetValue(filteringColumn, out bindingInfo))
                {
                    // Check whether the column's display value is visible for the object to filter.
                    if (bindingInfo.VisibilityBinding != null)
                    {
                        var visibility = (Visibility)this.EvaluateBinding(bindingInfo.VisibilityBinding, obj);
                        if (visibility != Visibility.Visible)
                        {
                            return false;
                        }
                    }

                    bool columnValueIsNumber;
                    string columnDisplayValue = GetColumnDisplayValue(bindingInfo, obj, out columnValueIsNumber);

                    if (!string.IsNullOrWhiteSpace(columnDisplayValue))
                    {
                        string searchedString = this.filterTextBox.Text.ToLower();
                        if (columnValueIsNumber)
                        {
                            // Check if the column display value is a number and if it is convert it back to string to remove any formatting.
                            columnDisplayValue = this.RemoveAnyNumberFormatting(columnDisplayValue);

                            // Check if the searched string is also a number and if it is convert it back to string to remove any formatting,
                            // so that it can match with the column display value.
                            searchedString = this.RemoveAnyNumberFormatting(searchedString);
                        }

                        columnDisplayValue = columnDisplayValue.ToLower();
                        if (this.FilterMode == FilterMode.StartsWith)
                        {
                            return columnDisplayValue.StartsWith(searchedString);
                        }
                        else if (FilterMode == FilterMode.Contains)
                        {
                            return columnDisplayValue.Contains(searchedString);
                        }
                    }
                }
            }

            return false;
        }

        #endregion Filtering logic

        #region Export logic

        /// <summary>
        /// Creates the open file overlay window.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="status">The status.</param>
        /// <param name="isOpenFileAvailable">if set to <c>true</c> [is open file available].</param>
        /// <returns>The overlayWindow</returns>
        private OpenFileOverlayWindow CreateOpenFileOverlayWindow(string filePath, string status, bool isOpenFileAvailable)
        {
            var openFileOverlayWindow = new OpenFileOverlayWindow();

            // The owner of the overlay window should be the window to which the data grid belongs.
            openFileOverlayWindow.Owner = UIHelper.FindParent<Window>(this);

            openFileOverlayWindow.MinHeight = 175d;
            openFileOverlayWindow.MinWidth = 275d;
            openFileOverlayWindow.LastCreatedFilePath = filePath;
            openFileOverlayWindow.IsOpenFileAvailable = isOpenFileAvailable;
            openFileOverlayWindow.CreatedFileStatus = status;

            return openFileOverlayWindow;
        }

        /// <summary>
        /// Exports the content of the data grid to excel.
        /// </summary>
        private void ExportContentToXls()
        {
            var saveFileDialog = new SaveFileDialog();
            saveFileDialog.CheckPathExists = true;
            saveFileDialog.Filter = LocalizedResources.DialogFilter_ExcelDocs;
            saveFileDialog.FileName = this.ExportDefaultFileName;
            saveFileDialog.OverwritePrompt = true;
            saveFileDialog.RestoreDirectory = true;

            bool? dialogresult = saveFileDialog.ShowDialog();
            if (dialogresult == true)
            {
                try
                {
                    StringCollection columnHeaders;
                    List<StringCollection> gridData;
                    this.CollectDataForExport(this, out columnHeaders, out gridData);
                    this.reportsManager.GenericExportToXls(columnHeaders, gridData, saveFileDialog.FileName, this.ExportAuthor, this.AdditionalReportInformation);

                    this.openFileOverlayWindow = this.CreateOpenFileOverlayWindow(saveFileDialog.FileName, LocalizedResources.General_ExportSuccesfully, true);
                    this.openFileOverlayWindow.ShowDialog();
                }
                catch (BusinessException ex)
                {
                    log.ErrorException("DataGrid Export content to xls.", ex);
                    this.openFileOverlayWindow = this.CreateOpenFileOverlayWindow(saveFileDialog.FileName, LocalizedResources.FileExport_Error, false);
                    this.openFileOverlayWindow.ShowDialog();
                }
            }
        }

        /// <summary>
        /// Exports the content to a pdf file.
        /// </summary>
        private void ExportContentToPdf()
        {
            var saveFileDialog = new SaveFileDialog();
            saveFileDialog.CheckPathExists = true;
            saveFileDialog.Filter = LocalizedResources.DialogFilter_PdfDocs;
            saveFileDialog.FileName = this.ExportDefaultFileName;
            saveFileDialog.OverwritePrompt = true;
            saveFileDialog.RestoreDirectory = true;

            bool? dialogresult = saveFileDialog.ShowDialog();
            if (dialogresult == true)
            {
                try
                {
                    StringCollection columnHeaders;
                    List<StringCollection> gridData;
                    this.CollectDataForExport(this, out columnHeaders, out gridData);
                    this.reportsManager.GenericExportToPdf(columnHeaders, gridData, saveFileDialog.FileName, this.ExportAuthor, this.AdditionalReportInformation);

                    this.openFileOverlayWindow = this.CreateOpenFileOverlayWindow(saveFileDialog.FileName, LocalizedResources.General_ExportSuccesfully, true);
                    this.openFileOverlayWindow.ShowDialog();
                }
                catch (BusinessException ex)
                {
                    log.ErrorException("DataGrid Export content to pdf.", ex);
                    this.openFileOverlayWindow = this.CreateOpenFileOverlayWindow(saveFileDialog.FileName, LocalizedResources.FileExport_Error, false);
                    this.openFileOverlayWindow.ShowDialog();
                }
            }
        }

        /// <summary>
        /// Collects the data to be exported from the data grid.
        /// </summary>
        /// <param name="dataGrid">The data grid.</param>
        /// <param name="columnHeaders">The column headers.</param>
        /// <param name="data">The data to export.</param>
        private void CollectDataForExport(DataGrid dataGrid, out StringCollection columnHeaders, out List<StringCollection> data)
        {
            columnHeaders = new StringCollection();
            data = new List<StringCollection>();
            var columnsBindingInfo = new List<ColumnBindingInfo>();

            // Collect the column headers                
            foreach (DataGridColumn column in dataGrid.Columns.Where(col => col.Visibility == Visibility.Visible))
            {
                if (column.Header != null && column.HeaderStringFormat != string.Empty)
                {
                    var columnBinding = GetColumnBindingInfoForExport(column);
                    columnsBindingInfo.Add(columnBinding);
                    columnHeaders.Add(column.Header.ToString());
                }
            }

            if (dataGrid.ItemsSource == null)
            {
                return;
            }

            // The currency used if it is present.
            string currencyUsed = string.Empty;

            // Collect the data grid data
            foreach (object item in dataGrid.ItemsSource)
            {
                var rowData = new StringCollection();
                foreach (var bindingInfo in columnsBindingInfo)
                {
                    if (bindingInfo == null || bindingInfo.ValueBinding == null)
                    {
                        rowData.Add(string.Empty);
                    }
                    else
                    {
                        bool columnValueIsNumber;
                        string cellValue = GetColumnDisplayValue(bindingInfo, item, out columnValueIsNumber);
                        rowData.Add(cellValue);
                    }

                    if (string.IsNullOrWhiteSpace(currencyUsed)
                        && !string.IsNullOrWhiteSpace(bindingInfo.Symbol))
                    {
                        currencyUsed = bindingInfo.Symbol;
                    }
                }

                data.Add(rowData);
            }
        }

        /// <summary>
        /// Gets the binding properties of data grid column.
        /// </summary>
        /// <param name="column">The data grid column.</param>
        /// <returns> A pair which contains the name of property which is bound
        /// to this column and a pair which is null if value of the cell doesn't need to be converted
        /// otherwise, contains information needed for conversion like UIUnitType and symbol.
        /// </returns>
        private ColumnBindingInfo GetColumnBindingInfoForExport(DataGridColumn column)
        {
            if (column == null)
            {
                return null;
            }

            ColumnBindingInfo result = null;

            var boundColumn = column as DataGridBoundColumn;
            if (boundColumn != null)
            {
                result = new ColumnBindingInfo() { ValueBinding = boundColumn.Binding };
            }
            else
            {
                var templateColumn = column as DataGridTemplateColumn;
                if (templateColumn != null && templateColumn.CellTemplate != null && templateColumn.CellTemplate.IsSealed)
                {
                    object content = templateColumn.CellTemplate.LoadContent();
                    if (content != null)
                    {
                        var label = content as ExtendedLabelControl;
                        if (label != null)
                        {
                            var binding = BindingOperations.GetBindingBase(label, ExtendedLabelControl.BaseValueProperty);
                            result = new ColumnBindingInfo();
                            result.ValueBinding = binding;

                            var uiUnit = BindingOperations.GetBindingBase(label, ExtendedLabelControl.UIUnitProperty);
                            if (uiUnit != null)
                            {
                                result.UIUnitBinding = uiUnit;
                            }
                            else
                            {
                                result.UIUnit = label.UIUnit;
                            }

                            var baseCurrency = BindingOperations.GetBindingBase(label, ExtendedLabelControl.BaseCurrencyProperty);
                            if (baseCurrency != null)
                            {
                                result.BaseCurrencyBinding = baseCurrency;
                            }
                            else
                            {
                                result.BaseCurrency = label.BaseCurrency;
                            }

                            var symbolBinding = BindingOperations.GetBindingBase(label, ExtendedLabelControl.SymbolProperty);
                            if (symbolBinding != null)
                            {
                                result.SymbolBinding = symbolBinding;
                            }
                            else
                            {
                                result.Symbol = label.Symbol;
                            }

                            if (label.MaxDigitsToDisplay >= 0)
                            {
                                result.MaxDecimalPlaces = label.MaxDigitsToDisplay;
                            }
                            else
                            {
                                result.MaxDecimalPlaces = UserSettingsManager.Instance.DecimalPlacesNumber;
                            }
                        }
                        else
                        {
                            var textBlock = content as TextBlock;
                            if (textBlock != null)
                            {
                                var binding = BindingOperations.GetBindingBase(textBlock, TextBlock.TextProperty);
                                result = new ColumnBindingInfo() { ValueBinding = binding };
                            }
                            else
                            {
                                var element = content as FrameworkElement;
                                if (element != null)
                                {
                                    // The Tag property of the root element stores an export friendly value for template columns.
                                    var binding = BindingOperations.GetBindingBase(element, FrameworkElement.TagProperty);
                                    if (binding == null)
                                    {
                                        // Get the tooltip binding (in case of columns with tooltip for the type name.)
                                        binding = BindingOperations.GetBindingBase(element, FrameworkElement.ToolTipProperty);
                                    }

                                    result = new ColumnBindingInfo() { ValueBinding = binding };
                                }
                            }
                        }
                    }
                }
            }

            return result;
        }

        #endregion Export logic

        #region Helpers

        /// <summary>
        /// Gets the value that will be displayed in a data grid column for a specified item.
        /// </summary>
        /// <param name="bindingInfo">The binding information for the column.</param>
        /// <param name="dataGridItem">The data grid item.</param>
        /// <param name="columnValueIsNumber">A value indicating whether the value displayed represents a number or not</param>
        /// <returns>
        /// The value that will be displayed in the column.
        /// </returns>
        private string GetColumnDisplayValue(ColumnBindingInfo bindingInfo, object dataGridItem, out bool columnValueIsNumber)
        {
            // TODO: the EvaluateBinding method called below identifies the binding source of a binding from the ColumnBindingInfo parameter each time it is called
            // but the source never changes. The source of the bindings in ColumnBindingInfo should be identified when created and cached.
            columnValueIsNumber = false;

            if (bindingInfo == null || dataGridItem == null)
            {
                return string.Empty;
            }

            string displayValue = null;

            // Get the value from the value binding.
            object value = this.EvaluateBinding(bindingInfo.ValueBinding, dataGridItem);

            // Get the UI Unit, if defined.
            Unit uiUnit = null;
            if (bindingInfo.UIUnitBinding != null)
            {
                uiUnit = (Unit)this.EvaluateBinding(bindingInfo.UIUnitBinding, dataGridItem);
            }
            else
            {
                uiUnit = bindingInfo.UIUnit;
            }

            // Get the base currency, if defined.
            Currency baseCurrency = null;
            if (bindingInfo.BaseCurrencyBinding != null)
            {
                baseCurrency = (Currency)this.EvaluateBinding(bindingInfo.BaseCurrencyBinding, dataGridItem);
            }
            else
            {
                baseCurrency = bindingInfo.BaseCurrency;
            }

            if (uiUnit != null)
            {
                decimal? baseValue = value as decimal?;
                if (baseValue.HasValue)
                {
                    columnValueIsNumber = true;
                    decimal convertedValue = 0m;

                    if (baseCurrency == null)
                    {
                        if (uiUnit.Type == UnitType.Percentage)
                        {
                            convertedValue = baseValue.Value * uiUnit.ConversionFactor;
                        }
                        else
                        {
                            convertedValue = baseValue.Value / uiUnit.ConversionFactor;
                        }
                    }
                    else
                    {
                        convertedValue = CurrencyConversionManager.ConvertBaseValueToDisplayValue(baseValue.Value, baseCurrency.ExchangeRate, uiUnit.ConversionFactor);
                    }

                    if (uiUnit.Type == UnitType.Currency)
                    {
                        displayValue = Formatter.FormatMoney(convertedValue);
                    }
                    else
                    {
                        // Get the number maximum decimal places if have one or the default value
                        var decimalPlaces = bindingInfo.MaxDecimalPlaces >= 0 ? bindingInfo.MaxDecimalPlaces : 4;
                        displayValue = Formatter.FormatNumber(convertedValue, decimalPlaces);
                    }
                }
                else
                {
                    displayValue = string.Empty;
                }
            }
            else
            {
                columnValueIsNumber = NumericUtils.IsNumber(value);

                // Get the symbol to display with the value.
                string symbolValue = null;
                if (bindingInfo.SymbolBinding != null)
                {
                    symbolValue = this.EvaluateBinding(bindingInfo.SymbolBinding, dataGridItem) as string;
                }
                else
                {
                    symbolValue = bindingInfo.Symbol;
                }

                if (symbolValue != null)
                {
                    displayValue = this.ConvertBindingValue(value) + " " + symbolValue;
                }
                else
                {
                    displayValue = this.ConvertBindingValue(value);
                }
            }

            return displayValue;
        }

        /// <summary>
        /// Evaluates the specified binding using the specified source and returns the evaluation result value.
        /// The binding can be a simple binding or a multi binding
        /// </summary>
        /// <param name="binding">The binding.</param>
        /// <param name="source">The source.</param>
        /// <returns>The binding evaluation result.</returns>
        private object EvaluateBinding(BindingBase binding, object source)
        {
            if (binding == null)
            {
                return null;
            }

            var simpleBinding = binding as Binding;
            if (simpleBinding != null)
            {
                return this.EvaluateBinding(simpleBinding, source);
            }
            else
            {
                var multiBinding = binding as MultiBinding;
                if (multiBinding != null)
                {
                    return this.EvaluateBinding(multiBinding, source);
                }
                else
                {
                    string message = string.Format("The binding type '{0}' is not handled.", binding.GetType());
                    throw new InvalidOperationException(message);
                }
            }
        }

        /// <summary>
        /// Evaluates the binding using a specified source and returns the value.
        /// </summary>
        /// <param name="binding">The binding.</param>
        /// <param name="source">The source.</param>
        /// <returns>The binding evaluation result.</returns>
        private object EvaluateBinding(Binding binding, object source)
        {
            if (binding == null)
            {
                return null;
            }

            object bindingValue = null;

            if (binding.Path.Path == ".")
            {
                bindingValue = source;
            }
            else
            {
                var bindingSource = this.FindBindingSource(binding);
                bindingValue = ReflectionUtils.GetComplexPropertyValue(bindingSource != null ? bindingSource : source, binding.Path.Path);
            }

            if (binding.Converter != null)
            {
                bindingValue = binding.Converter.Convert(
                    bindingValue,
                    typeof(object),
                    binding.ConverterParameter,
                    binding.ConverterCulture != null ? binding.ConverterCulture : System.Globalization.CultureInfo.CurrentUICulture);
            }

            // Apply the binding's string format, if defined.
            if (!string.IsNullOrEmpty(binding.StringFormat) && bindingValue != null)
            {
                return string.Format(binding.StringFormat, bindingValue);
            }
            else
            {
                return bindingValue;
            }
        }

        /// <summary>
        /// Evaluates a MultiBinding using a specified source.
        /// </summary>
        /// <param name="multiBinding">The multi binding.</param>
        /// <param name="source">The source.</param>
        /// <returns>The object resulted from the binding's evaluation.</returns>
        private object EvaluateBinding(MultiBinding multiBinding, object source)
        {
            if (multiBinding == null)
            {
                return null;
            }

            // Evaluate all binding in the multi binding.
            List<object> bindingValues = new List<object>();
            foreach (Binding binding in multiBinding.Bindings)
            {
                object value = this.EvaluateBinding(binding, source);
                bindingValues.Add(value);
            }

            // Apply the converter, if defined.
            object result = null;
            if (multiBinding.Converter != null)
            {
                result = multiBinding.Converter.Convert(
                    bindingValues.ToArray(),
                    typeof(object),
                    multiBinding.ConverterParameter,
                    multiBinding.ConverterCulture != null ? multiBinding.ConverterCulture : System.Globalization.CultureInfo.CurrentUICulture);

                // Apply the string format, if defined, to the converter's result.
                if (!string.IsNullOrEmpty(multiBinding.StringFormat))
                {
                    result = string.Format(multiBinding.StringFormat, result);
                }
            }
            else if (!string.IsNullOrEmpty(multiBinding.StringFormat))
            {
                // Apply the string format to the binding values.
                var values = bindingValues.ConvertAll<string>((val) => this.ConvertBindingValue(val));
                result = string.Format(multiBinding.StringFormat, bindingValues.ToArray());
            }

            return result;
        }

        /// <summary>
        /// Resolves and returns source of a specified binding.
        /// It resolves the sources specified by RelativeSource and ElementName.
        /// </summary>
        /// <param name="binding">The binding.</param>
        /// <returns>An object representing the source of the binding.</returns>
        public object FindBindingSource(Binding binding)
        {
            object source = null;

            if (binding.Source != null)
            {
                source = binding.Source;
            }
            else if (binding.RelativeSource != null)
            {
                // Resolve the relative source.
                // A relative source can be resolved only starting at this instance in the visual tree, because the UI Element from which the binding comes
                // can not be identified. Therefore, relative sources referring elements inside the data grid or Self will be resolved to null.
                var relativeSource = binding.RelativeSource;
                if (relativeSource.Mode == RelativeSourceMode.FindAncestor)
                {
                    source = UIHelper.FindParent(this, relativeSource.AncestorType, relativeSource.AncestorLevel);
                }
                else
                {
                    log.Warn("Relative sources other than FindAncestor can not be resolved.");
                }
            }
            else if (!string.IsNullOrWhiteSpace(binding.ElementName))
            {
                // Resolve the source indicated by the ElementName property of the binding.
                // From this data grid, walk up the parent tree until you find an element that has a value for the System.Windows.NameScope.NameScope
                // attached property; then use the INameScope instance to try to locate the element referred to by ElementName. If the element was not
                // located, go to the next parent and repeat the operations until the element is fount or the root element of the logical tree is reached.
                FrameworkElement walker = this;
                while (walker != null)
                {
                    var namescope = NameScope.GetNameScope(walker);
                    if (namescope != null)
                    {
                        source = namescope.FindName(binding.ElementName);
                        if (source != null)
                        {
                            break;
                        }
                    }

                    var parent = walker.Parent != null ? walker.Parent : walker.TemplatedParent;
                    walker = parent as FrameworkElement;
                }
            }

            return source;
        }

        /// <summary>
        /// Convert a binding value for display (to a string).        
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <returns>The string representation of the value.</returns>
        private string ConvertBindingValue(object value)
        {
            if (value == null)
            {
                return string.Empty;
            }

            // First it check if the value is string; if is not a string try to format it as a number and if that fails too, use its ToString() method.
            string result = null;
            if (value is string)
            {
                result = (string)value;
            }
            else if (value is bool)
            {
                bool boolVal = (bool)value;
                result = boolVal ? LocalizedResources.General_Yes : LocalizedResources.General_No;
            }
            else
            {
                // Format the value as number and if it fails format it as string.
                string formattedValue = Formatter.FormatNumber(value, 4);
                if (!string.IsNullOrEmpty(formattedValue))
                {
                    result = formattedValue;
                }
                else
                {
                    result = value.ToString();
                }
            }

            return result;
        }

        /// <summary>
        /// Removes any number formatting from parameter number provided as string
        /// </summary>
        /// <param name="stringNumber">The number in string format</param>
        /// <returns>
        /// A value representing the number without any formatting if the string provided is a number, 
        /// else the old value without any formatting removed
        /// </returns>
        private string RemoveAnyNumberFormatting(string stringNumber)
        {
            decimal columnValue;
            if (decimal.TryParse(stringNumber, out columnValue))
            {
                stringNumber = columnValue.ToString(CultureInfo.InvariantCulture);

                // Remove any trailing zeros because the decimal type store any trailing zeros between conversions (string->decimal->string)
                if (stringNumber.Contains('.'))
                {
                    return stringNumber = stringNumber.TrimEnd('0').TrimEnd('.');
                }
            }

            return stringNumber;
        }

        /// <summary>
        /// Synchronizes the new SelectedItems attached property with the SelectedItems from the base class,
        /// that changed when an item from the datagrid has been selected/unselected.
        /// </summary>
        private void SyncSelectedItems()
        {
            this.isSelectedItemsInSync = true;
            try
            {
                if (this.SelectedItems != null)
                {
                    foreach (var item in base.SelectedItems)
                    {
                        if (!this.SelectedItems.Contains(item))
                        {
                            this.SelectedItems.Add(item);
                        }
                    }

                    var i = 0;
                    while (i < this.SelectedItems.Count)
                    {
                        if (!base.SelectedItems.Contains(this.SelectedItems[i]))
                        {
                            this.SelectedItems.RemoveAt(i);
                        }
                        else
                        {
                            i++;
                        }
                    }
                }
            }
            finally
            {
                this.isSelectedItemsInSync = false;
            }
        }

        #endregion Helpers

        #region Events

        /// <summary>
        /// Handles the CollectionChanged event of SelectedItems ObservableCollection.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="NotifyCollectionChangedEventArgs"/> instance containing the event data.</param>
        private void OnSelectedItemsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            /*
             * If the collection is modified by the synchronization 
             * of SelectedItems collections, do nothing.
             */
            if (this.isSelectedItemsInSync)
            {
                return;
            }

            /*
             * Update the SelectedItems from the base class, 
             * so that the datagrid items will be selected properly.
             */
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        foreach (var newItem in e.NewItems)
                        {
                            if (!base.SelectedItems.Contains(newItem))
                            {
                                base.SelectedItems.Add(newItem);
                            }
                        }

                        break;
                    }

                case NotifyCollectionChangedAction.Remove:
                    {
                        foreach (var oldItem in e.OldItems)
                        {
                            if (base.SelectedItems.Contains(oldItem))
                            {
                                base.SelectedItems.Remove(oldItem);
                            }
                        }

                        break;
                    }

                case NotifyCollectionChangedAction.Reset:
                    {
                        base.SelectedItems.Clear();
                        break;
                    }
            }
        }

        /// <summary>
        /// Handles the SelectionChanged event of the ExtendedDataGrid control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void OnExtendedDataGridSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.SyncSelectedItems();
        }

        /// <summary>
        /// Handles the SelectionChanged event of the FilterByCmb control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Controls.SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void FilterByCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var view = CollectionViewSource.GetDefaultView(this.ItemsSource);
            if (view != null)
            {
                view.Refresh();
            }
        }

        /// <summary>
        /// Handles the Loaded event of the FilterByCmb control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void FilterByCombo_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.EnableFiltering)
            {
                // If the combo box filter is already initialized is no need to reinitiate its filter again
                // because the Loaded event can be triggered multiple times
                if (this.filterByCombo.Items.Count == 0)
                {
                    this.FillFilterComboBox();
                }

                this.Columns.CollectionChanged += new NotifyCollectionChangedEventHandler(ExtendedDataGridColumns_CollectionChanged);
            }
        }

        /// <summary>
        /// Handles the Unloaded event of the FilterByCmb control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void FilterByCombo_Unloaded(object sender, RoutedEventArgs e)
        {
            if (this.EnableFiltering)
            {
                this.Columns.CollectionChanged -= new NotifyCollectionChangedEventHandler(ExtendedDataGridColumns_CollectionChanged);
            }
        }

        /// <summary>
        /// Handles the CollectionChanged event of the Columns control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Collections.Specialized.NotifyCollectionChangedEventArgs"/> instance containing the event data.</param>
        private void ExtendedDataGridColumns_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            var dataGridColumns = this.Columns.ToList();

            // Register for visibility change notification for the added data grid columns.
            foreach (var column in dataGridColumns)
            {
                if (!this.dataGridColumnVisibilityChangedNotifiers.Exists(n => n.PropertySource == column))
                {
                    var columnVisibilityChangeNotifier = new DependencyPropertyChangeNotifier(column, DataGridColumn.VisibilityProperty);
                    columnVisibilityChangeNotifier.ValueChanged += new EventHandler(OnDataGridColumnVisibilityChanged);
                    this.dataGridColumnVisibilityChangedNotifiers.Add(columnVisibilityChangeNotifier);
                }
            }

            // Unregister visibility change notifications for the removed data grid columns.
            foreach (var changeNotifier in this.dataGridColumnVisibilityChangedNotifiers.ToList())
            {
                if (!dataGridColumns.Exists(column => column == changeNotifier.PropertySource))
                {
                    changeNotifier.ValueChanged -= OnDataGridColumnVisibilityChanged;
                    this.dataGridColumnVisibilityChangedNotifiers.Remove(changeNotifier);
                }
            }

            // Fill the filter combo box.
            this.FillFilterComboBox();
            this.columnsBindingForFiltering.Clear();
        }

        /// <summary>
        /// Handles the Click event of the ExportToPdfButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void ExportToPdfButton_Click(object sender, RoutedEventArgs e)
        {
            this.ExportContentToPdf();
        }

        /// <summary>
        /// Handles the Click event of the ExportToXLS control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void ExportToXlsButton_Click(object sender, RoutedEventArgs e)
        {
            this.ExportContentToXls();
        }

        /// <summary>
        /// Called when the source of an item in a selector changes.
        /// </summary>
        /// <param name="oldValue">The old source.</param>
        /// <param name="newValue">The new source.</param>
        protected override void OnItemsSourceChanged(IEnumerable oldValue, IEnumerable newValue)
        {
            base.OnItemsSourceChanged(oldValue, newValue);

            if (this.filterTextBox != null)
            {
                this.filterTextBox.Text = null;
            }

            if (this.EnableFiltering)
            {
                if (this.ItemsSource != null)
                {
                    // Setup the filtering for the data grid.
                    ICollectionView view = CollectionViewSource.GetDefaultView(this.ItemsSource);
                    if (view.CanFilter)
                    {
                        view.Filter = new Predicate<object>(FilterPredicate);
                    }
                    else
                    {
                        this.EnableFiltering = false;
                    }
                }

                // Clear the columns binding data because it may have changed.
                this.columnsBindingForFiltering.Clear();
            }
        }

        /// <summary>
        /// Handles the TextChanged event of the FilterTxt control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Controls.TextChangedEventArgs"/> instance containing the event data.</param>
        private void FilterTxt_TextChanged(object sender, TextChangedEventArgs e)
        {
            var view = CollectionViewSource.GetDefaultView(this.ItemsSource);
            if (view != null)
            {
                view.Refresh();
            }
        }

        /// <summary>
        /// Handler invoked when the export data grid  has finished.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.ComponentModel.RunWorkerCompletedEventArgs"/> instance containing the event data.</param>
        private void ExportReportFinished(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                MessageWindow.Show(e.Error);
            }
        }

        /// <summary>
        /// Handles the MouseEnter event of the ExtendedDataGrid control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseEventArgs"/> instance containing the event data.</param>
        private void ExtendedDataGrid_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            var mouseEnterStoryboard = this.FindResource("OnMouseEnterAnimation") as Storyboard;
            if (mouseEnterStoryboard != null &&
                this.filterTextBox.IsKeyboardFocused == false && this.filterByCombo.IsFocused == false)
            {
                mouseEnterStoryboard.Begin(this, this.Template);
            }
        }

        /// <summary>
        /// Handles the MouseLeave event of the ExtendedDataGrid control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseEventArgs"/> instance containing the event data.</param>
        private void ExtendedDataGrid_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            var mouseLeaveStoryboard = this.FindResource("OnMouseLeaveAnimation") as Storyboard;
            if (mouseLeaveStoryboard != null &&
                this.filterTextBox.IsKeyboardFocused == false && this.filterByCombo.IsFocused == false)
            {
                mouseLeaveStoryboard.Begin(this, this.Template);
            }
        }

        #endregion Events

        #region Inner Classes

        /// <summary>
        /// Stores the binding information of a data grid column.
        /// </summary>
        private class ColumnBindingInfo
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ColumnBindingInfo"/> class.
            /// </summary>
            public ColumnBindingInfo()
            {
                this.MaxDecimalPlaces = -1;
            }

            /// <summary>
            /// Gets or sets the binding for the column's values.
            /// </summary>
            public BindingBase ValueBinding { get; set; }

            /// <summary>
            /// Gets or sets the base currency, if the binding(s) is on an element that has a base currency.
            /// It's used if BaseCurrencyBinding does not have value.
            /// </summary>       
            public Currency BaseCurrency { get; set; }

            /// <summary>
            /// Gets or sets the binding for the base currency.
            /// It's used if BaseCurrency does not have value.
            /// </summary>
            public BindingBase BaseCurrencyBinding { get; set; }

            /// <summary>
            /// Gets or sets the UI unit, if the binding(s) is on an element that has a UI unit.
            /// It's used if UIUnitBinding does not have value.
            /// </summary>            
            public Unit UIUnit { get; set; }

            /// <summary>
            /// Gets or sets the binding for the UI unit.
            /// It's used if UIUnit does not have value.
            /// </summary>
            public BindingBase UIUnitBinding { get; set; }

            /// <summary>
            /// Gets or sets the symbol associated with the label that is used to display the column's values.
            /// It's used if SymbolBinding does not have value.
            /// </summary>            
            public string Symbol { get; set; }

            /// <summary>
            /// Gets or sets the binding for the symbol associated with the label that is used to display the column's values.
            /// It's used if Symbol does not have value.
            /// </summary>
            public BindingBase SymbolBinding { get; set; }

            /// <summary>
            /// Gets or sets the maximum decimal places for the column's value
            /// It's used only if the column's value represents a number
            /// </summary>
            public int MaxDecimalPlaces { get; set; }

            /// <summary>
            /// Gets or sets the binding for the Visibility property of the UIElement.
            /// </summary>            
            public BindingBase VisibilityBinding { get; set; }
        }

        #endregion Inner Classes
    }
}