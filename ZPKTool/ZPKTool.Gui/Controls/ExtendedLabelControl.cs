﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.Gui.Resources;

namespace ZPKTool.Gui.Controls
{
    /// <summary>
    /// This control is designed to display numeric values that have a unit symbol to their right.
    /// It converts numeric values from the base value (database storage value) to the corresponding UI unit.
    /// For currencies the base currency can also be set.
    /// It can also display a string followed by a symbol; the symbol can be formatted.
    /// </summary>
    public class ExtendedLabelControl : Control
    {
        /// <summary>
        /// The default value for the <see cref="MaxDigitsToDisplay"/> property.
        /// </summary>
        private const int DefaultMaxDigitsToDisplayValue = -1;

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// A weak event listener for changes in the <see cref="DecimalPlacesNumber"/> application setting.
        /// </summary>
        private readonly WeakEventListener<PropertyChangedEventArgs> numberOfDigitsToDisplayChangedListener;

        /// <summary>
        /// Initializes static members of the <see cref="ExtendedLabelControl"/> class.
        /// </summary>
        static ExtendedLabelControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ExtendedLabelControl), new FrameworkPropertyMetadata(typeof(ExtendedLabelControl)));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtendedLabelControl"/> class.
        /// </summary>
        public ExtendedLabelControl()
        {
            this.numberOfDigitsToDisplayChangedListener = new WeakEventListener<PropertyChangedEventArgs>(this.HandleNumberOfDecimalPlacesChanged);
            PropertyChangedEventManager.AddListener(
                UserSettingsManager.Instance,
                numberOfDigitsToDisplayChangedListener,
                ReflectionUtils.GetPropertyName(() => UserSettingsManager.Instance.DecimalPlacesNumber));
        }

        #region Dependency Properties

        /// <summary>
        /// Gets or sets the base currency. This is the unit that will be used to calculate the base value (the value saved in the db).
        /// </summary>
        public Currency BaseCurrency
        {
            get { return (Currency)GetValue(BaseCurrencyProperty); }
            set { SetValue(BaseCurrencyProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for BaseCurrency.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty BaseCurrencyProperty =
            DependencyProperty.Register("BaseCurrency", typeof(Currency), typeof(ExtendedLabelControl), new UIPropertyMetadata(null));

        /// <summary>
        /// Gets or sets the UI unit displayed next to the label.
        /// </summary>
        public Unit UIUnit
        {
            get { return (Unit)GetValue(UIUnitProperty); }
            set { SetValue(UIUnitProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for UIUnit.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty UIUnitProperty =
            DependencyProperty.Register("UIUnit", typeof(Unit), typeof(ExtendedLabelControl), new UIPropertyMetadata(null, UIUnitChangedCallback));

        /// <summary>
        /// Gets or sets the default text that is displayed if BaseValue is null. The default value is "n/a".
        /// </summary>
        public string DefaultText
        {
            get { return (string)GetValue(DefaultTextProperty); }
            set { SetValue(DefaultTextProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for DefaultText.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty DefaultTextProperty =
            DependencyProperty.Register("DefaultText", typeof(string), typeof(ExtendedLabelControl), new UIPropertyMetadata(string.Empty));

        /// <summary>
        /// Gets or sets the label's text.
        /// </summary>        
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for Text.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty TextProperty = DependencyProperty.Register(
            "Text",
            typeof(string),
            typeof(ExtendedLabelControl),
            new UIPropertyMetadata(string.Empty, TextChangedCallback, CoerceTextValueCallback));

        /// <summary>
        /// Gets or sets the string format applied to the entire label content. {0} is the label's text and {1} is the symbol.
        /// Default value is '{0} {1}'.
        /// </summary>
        public string StringFormat
        {
            get { return (string)GetValue(StringFormatProperty); }
            set { SetValue(StringFormatProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for StringFormat.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty StringFormatProperty =
            DependencyProperty.Register("StringFormat", typeof(string), typeof(ExtendedLabelControl), new UIPropertyMetadata("{0} {1}", StringFormatChangedCallback));

        /// <summary>
        /// Gets or sets the label's symbol.
        /// </summary>
        public string Symbol
        {
            get { return (string)GetValue(SymbolProperty); }
            set { SetValue(SymbolProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for Symbol.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty SymbolProperty =
            DependencyProperty.Register("Symbol", typeof(string), typeof(ExtendedLabelControl), new UIPropertyMetadata(SymbolChangedCallback));

        /// <summary>
        /// Gets or sets the number based on which a value is displayed in the label (displays n/a if the value is null).
        /// The value is interpreted as a base value and it will be converted to the corresponding UI unit before displaying it.
        /// Works only if UIUnit is set to something different than None.
        /// Use the Text property to get the value as it is displayed.
        /// </summary>
        public decimal? BaseValue
        {
            get { return (decimal?)GetValue(BaseValueProperty); }
            set { SetValue(BaseValueProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for BaseValue. This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty BaseValueProperty =
            DependencyProperty.Register("BaseValue", typeof(decimal?), typeof(ExtendedLabelControl), new PropertyMetadata(null, BaseValueChangedCallback));

        /// <summary>
        /// Gets or sets the maximum number of digits to display when the text is set by the BaseValue property.
        /// Default is 4.
        /// </summary>
        public int MaxDigitsToDisplay
        {
            get { return (int)GetValue(MaxDigitsToDisplayProperty); }
            set { SetValue(MaxDigitsToDisplayProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for MaxDigitsToDisplay.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty MaxDigitsToDisplayProperty =
            DependencyProperty.Register("MaxDigitsToDisplay", typeof(int), typeof(ExtendedLabelControl), new UIPropertyMetadata(DefaultMaxDigitsToDisplayValue, MaxDigitsToDisplayPropertyChanged));

        /// <summary>
        /// Gets the value as displayed by the control. This dependency property is used internally.        
        /// </summary>        
        public string DisplayedValue
        {
            get { return (string)GetValue(DisplayedValueProperty); }
            private set { SetValue(DisplayedValueProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for DisplayedValue.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty DisplayedValueProperty =
            DependencyProperty.Register("DisplayedValue", typeof(string), typeof(ExtendedLabelControl), new UIPropertyMetadata(string.Empty));

        /// <summary>
        /// Gets or sets a value indicating whether the symbol is shown or not on label
        /// By default the symbol is shown
        /// </summary> 
        public bool ShowSymbol
        {
            get { return (bool)GetValue(ShowSymbolProperty); }
            set { SetValue(ShowSymbolProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for ShowSymbol.  This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty ShowSymbolProperty =
           DependencyProperty.Register("ShowSymbol", typeof(bool), typeof(ExtendedLabelControl), new PropertyMetadata(true, ShowSymbolPropertyChanged));

        #endregion Dependency Properties

        #region Dependency properties change callbacks

        /// <summary>
        /// Handles the PropertyChanged event of the MaxDigitsToDisplay property.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void MaxDigitsToDisplayPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var label = (ExtendedLabelControl)sender;
            SetDisplayedValue(label);
        }

        /// <summary>
        /// Called when the value of the Text property is re-evaluated or coercion is requested.
        /// </summary>
        /// <param name="d">The dependency object.</param>
        /// <param name="baseValue">The base value.</param>
        /// <returns>The coerced/re-evaluated value</returns>
        private static object CoerceTextValueCallback(DependencyObject d, object baseValue)
        {
            var label = (ExtendedLabelControl)d;

            // Empty text is replaced with the default text.
            return string.IsNullOrWhiteSpace((string)baseValue) ? label.DefaultText : baseValue;
        }

        /// <summary>
        /// Called when the Text property has changed.
        /// </summary>
        /// <param name="depObj">The dependency object.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void TextChangedCallback(DependencyObject depObj, DependencyPropertyChangedEventArgs e)
        {
            var label = (ExtendedLabelControl)depObj;
            label.RefreshDisplayedValue();
        }

        /// <summary>
        /// Called when the StringFormat property has changed.
        /// </summary>
        /// <param name="depObj">The dependency object.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void StringFormatChangedCallback(DependencyObject depObj, DependencyPropertyChangedEventArgs e)
        {
            var label = (ExtendedLabelControl)depObj;
            label.RefreshDisplayedValue();
        }

        /// <summary>
        /// Called when the Symbol property has changed.
        /// </summary>
        /// <param name="depObj">The dependency object.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void SymbolChangedCallback(DependencyObject depObj, DependencyPropertyChangedEventArgs e)
        {
            var label = (ExtendedLabelControl)depObj;
            label.RefreshDisplayedValue();
        }

        /// <summary>
        /// Triggered when the BaseValueProperty value is changed.
        /// </summary>
        /// <param name="d">The source.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void BaseValueChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var label = (ExtendedLabelControl)d;
            SetDisplayedValue(label);
        }

        /// <summary>
        /// Triggered when the ShowSymbolProperty value is changed.
        /// </summary>
        /// <param name="d">The source.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void ShowSymbolPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var label = (ExtendedLabelControl)d;
            label.RefreshDisplayedValue();
        }

        /// <summary>
        /// Called when the value of the UIUnit dependency property has changed.
        /// </summary>
        /// <param name="d">The dependency object that triggered the callback.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void UIUnitChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var label = (ExtendedLabelControl)d;

            // Set the new units's symbol.
            var associatedSymbol = string.Empty;
            var uiUnit = e.NewValue as Unit;

            if (uiUnit != null)
            {
                associatedSymbol = uiUnit.Symbol;
            }

            label.Symbol = associatedSymbol;

            SetDisplayedValue(label);
        }

        #endregion Dependency properties change callbacks

        /// <summary>
        /// Sets the displayed value of the control by converting its base value.
        /// </summary>
        /// <param name="label">The label.</param>
        private static void SetDisplayedValue(ExtendedLabelControl label)
        {
            if (label.BaseValue.HasValue)
            {
                decimal displayValue = 0m;
                if (label.BaseCurrency == null
                    && label.UIUnit != null)
                {
                    if (label.UIUnit.Type == UnitType.Percentage)
                    {
                        displayValue = label.BaseValue.Value * label.UIUnit.ConversionFactor;
                    }
                    else
                    {
                        displayValue = label.BaseValue.Value / label.UIUnit.ConversionFactor;
                    }
                }
                else if (label.BaseCurrency != null
                        && label.UIUnit != null)
                {
                    displayValue = CurrencyConversionManager.ConvertBaseValueToDisplayValue(label.BaseValue.Value, label.BaseCurrency.ExchangeRate, label.UIUnit.ConversionFactor);
                }
                else
                {
                    displayValue = label.BaseValue.Value;
                }

                label.Text = Formatter.FormatNumber(displayValue, label.MaxDigitsToDisplay != DefaultMaxDigitsToDisplayValue ? label.MaxDigitsToDisplay : UserSettingsManager.Instance.DecimalPlacesNumber);
            }
            else
            {
                label.Text = null;
            }
        }

        /// <summary>
        /// Refreshes the displayed value by reading the Text and Symbol properties and applying the format specified by StringFormat.
        /// </summary>
        private void RefreshDisplayedValue()
        {
            try
            {
                var text = this.Text ?? string.Empty;
                var symbol = this.Symbol ?? string.Empty;

                if (string.IsNullOrWhiteSpace(text))
                {
                    this.DisplayedValue = null;
                }
                else if (!string.IsNullOrWhiteSpace(symbol) && this.ShowSymbol && this.Text != this.DefaultText)
                {
                    this.DisplayedValue = string.Format(this.StringFormat, text, symbol);
                }
                else
                {
                    // Don't use string.Format if the symbol is empty because it adds a space after the text.
                    this.DisplayedValue = text;
                }
            }
            catch (FormatException ex)
            {
                log.WarnException("Failed to format the extended label display value.", ex);
            }
        }

        /// <summary>
        /// Called when the number of decimals property is changed, re-formats the textbox is necessary.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void HandleNumberOfDecimalPlacesChanged(object sender, PropertyChangedEventArgs e)
        {
            SetDisplayedValue(this);
        }
    }
}