﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace ZPKTool.Gui.Controls
{
    /// <summary>
    /// This is a control that displays an animation is a Grid for usage in views to display a wait overlay while executing log operations in the background.
    /// Using it on a control other than Grid will throw an InvalidOperationException.
    /// </summary>
    /// <example>
    /// &lt;Grid WaitAnimationMVVM.Trigger="{Binding Path=IsLoading,Mode=OneWay}" 
    ///          WaitAnimationMVVM.Message="The message"/&gt;
    /// </example>
    //// TODO: rename to WaitAnimationBehavior, add the option to set a display delay, maybe move it to the Controls lib.
    public class WaitAnimationMVVM : ContentControl
    {        
        /// <summary>
        /// A value that indicates whether to start the wait animation (a value of true) or stop it (a value of false).
        /// The default value is false, corresponding to a stopped animation.
        /// This is an Attached DependencyProperty.
        /// </summary>
        public static readonly DependencyProperty TriggerProperty =
            DependencyProperty.RegisterAttached("Trigger", typeof(bool), typeof(WaitAnimationMVVM), new UIPropertyMetadata(false, OnTriggerChanged));

        /// <summary>
        /// The message displayed during the animation.
        /// This is an Attached DependencyProperty.
        /// </summary>
        public static readonly DependencyProperty MessageProperty =
            DependencyProperty.RegisterAttached("Message", typeof(string), typeof(WaitAnimationMVVM), new UIPropertyMetadata(null));

        /// <summary>
        /// A value indicating whether the animation should ignore its parent grid's margin and overflow over it, covering the parent completely.
        /// Coupled with the IgnoreParentMarginLevel property, you can ignore the margin of any number of parents and cover them with the animation.
        /// The default value is true and coupled with IgnoreParentMarginLevel's default property of 1 it will ignore the margins of the Grid on which is defined.
        /// This is an Attached DependencyProperty.
        /// </summary>
        public static readonly DependencyProperty IgnoreParentMarginProperty =
            DependencyProperty.RegisterAttached("IgnoreParentMargin", typeof(bool), typeof(WaitAnimationMVVM), new UIPropertyMetadata(true));

        /// <summary>
        /// A value indicating for how may parents the margins should be ignored. Has no effect if IgnoreParentMargin is false.
        /// The default value is 1.
        /// This is an Attached DependencyProperty.
        /// </summary>
        public static readonly DependencyProperty IgnoreParentMarginLevelProperty =
            DependencyProperty.RegisterAttached("IgnoreParentMarginLevel", typeof(int), typeof(WaitAnimationMVVM), new UIPropertyMetadata(1));

        /// <summary>
        /// Gets the value of the Trigger DependencyProperty.
        /// </summary>
        /// <param name="obj">The dependency object for which to get the value.</param>
        /// <returns>The value.</returns>
        public static bool GetTrigger(DependencyObject obj)
        {
            return (bool)obj.GetValue(TriggerProperty);
        }

        /// <summary>
        /// Sets the value of the Trigger DependencyProperty.
        /// </summary>
        /// <param name="obj">The dependency object for which to get the value.</param>
        /// <param name="value">The new value.</param>
        public static void SetTrigger(DependencyObject obj, bool value)
        {
            obj.SetValue(TriggerProperty, value);
        }

        /// <summary>
        /// Gets the value of the Message DependencyProperty.
        /// </summary>
        /// <param name="obj">The dependency object for which to get the value.</param>
        /// <returns>The value.</returns>
        public static string GetMessage(DependencyObject obj)
        {
            return (string)obj.GetValue(MessageProperty);
        }

        /// <summary>
        /// Sets the value of the Message DependencyProperty.
        /// </summary>
        /// <param name="obj">The dependency object for which to get the value.</param>
        /// <param name="value">The new value.</param>
        public static void SetMessage(DependencyObject obj, string value)
        {
            obj.SetValue(MessageProperty, value);
        }

        /// <summary>
        /// Gets the value of the IgnoreParentMargin DependencyProperty.
        /// </summary>
        /// <param name="obj">The dependency object for which to get the value.</param>
        /// <returns>The value.</returns>
        public static bool GetIgnoreParentMargin(DependencyObject obj)
        {
            return (bool)obj.GetValue(IgnoreParentMarginProperty);
        }

        /// <summary>
        /// Sets the value of the IgnoreParentMargin DependencyProperty.
        /// </summary>
        /// <param name="obj">The dependency object for which to get the value.</param>
        /// <param name="value">The new value.</param>
        public static void SetIgnoreParentMargin(DependencyObject obj, bool value)
        {
            obj.SetValue(IgnoreParentMarginProperty, value);
        }

        /// <summary>
        /// Gets the value of the IgnoreParentMarginLevel DependencyProperty.
        /// </summary>
        /// <param name="obj">The dependency object for which to get the value.</param>
        /// <returns>The value.</returns>
        public static int GetIgnoreParentMarginLevel(DependencyObject obj)
        {
            return (int)obj.GetValue(IgnoreParentMarginLevelProperty);
        }

        /// <summary>
        /// Sets the value of the IgnoreParentMarginLevel DependencyProperty.
        /// </summary>
        /// <param name="obj">The dependency object for which to get the value.</param>
        /// <param name="value">The new value.</param>
        public static void SetIgnoreParentMarginLevel(DependencyObject obj, int value)
        {
            obj.SetValue(IgnoreParentMarginLevelProperty, value);
        }
                
        /// <summary>
        /// Called when the Trigger property has changed.
        /// </summary>
        /// <param name="d">The dependency property for which the property has changed.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnTriggerChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Grid parentGrid = d as Grid;
            if (parentGrid == null)
            {
                throw new InvalidOperationException("This control can't be attached to an UIElement other than Grid.");
            }

            bool trigger = GetTrigger(parentGrid);
            if (trigger)
            {
                // Create and add the animation in the target grid.
                WaitAnimationMVVM animation = new WaitAnimationMVVM();
                animation.Name = "WaitAnimation";
                SetMessage(animation, GetMessage(parentGrid));

                // Disable the keyboard input while the performing the animation (the mouse input is disabled because the animation covers the parent grid with an overlay).
                // TODO: find a better way to disable the key input because it is possible for other components to register a handler to this event before
                // the execution gets here, allowing them to process key events.
                parentGrid.PreviewKeyDown += StopPreviewKeyDownEvent;

                // Ignore the margin of as many parents as it is specified.
                if (GetIgnoreParentMargin(parentGrid))
                {
                    int ignoreLevel = GetIgnoreParentMarginLevel(parentGrid);
                    FrameworkElement parent = parentGrid;
                    while (ignoreLevel > 0 && parent != null)
                    {
                        // Subtract the parent grid's margin
                        animation.Margin = SubtractThickness(animation.Margin, parent.Margin);
                        parent = parent.Parent as FrameworkElement;
                        ignoreLevel--;
                    }                    
                }

                // Set the animation control to cover the whole area of its parent grid.
                if (parentGrid.ColumnDefinitions.Count > 0)
                {
                    Grid.SetColumnSpan(animation, parentGrid.ColumnDefinitions.Count);
                }

                if (parentGrid.RowDefinitions.Count > 0)
                {
                    Grid.SetRowSpan(animation, parentGrid.RowDefinitions.Count);
                }

                parentGrid.Children.Add(animation);
            }
            else
            {
                // Enable the keyboard input.                
                parentGrid.PreviewKeyDown -= StopPreviewKeyDownEvent;

                // Remove the animation from the target grid.
                parentGrid.Children.Remove(null);
                parentGrid.Children.Remove(parentGrid.Children.OfType<WaitAnimationMVVM>().FirstOrDefault());
            }
        }

        /// <summary>
        /// A handler for the PreviewKeyDown event that stops the event from being sent further.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.KeyEventArgs"/> instance containing the event data.</param>
        private static void StopPreviewKeyDownEvent(object sender, System.Windows.Input.KeyEventArgs e)
        {
            e.Handled = true;
        }

        /// <summary>
        /// Subtracts the values of a Thickness instance from the values of another, equivalent to a = b - c.
        /// </summary>
        /// <param name="a">The thickness from which to subtract.</param>
        /// <param name="b">The thickness to subtract.</param>
        /// <returns>The resulting thickness.</returns>
        private static Thickness SubtractThickness(Thickness a, Thickness b)
        {
            Thickness result = new Thickness();
            result.Left = a.Left - b.Left;
            result.Top = a.Top - b.Top;
            result.Right = a.Right - b.Right;
            result.Bottom = a.Bottom - b.Bottom;

            return result;
        }
    }
}
