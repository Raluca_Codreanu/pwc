﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Gui.Resources;

namespace ZPKTool.Gui.Controls
{
    /// <summary>
    /// The properties of the extra report information
    /// </summary>
    public class ExtendedDataGridAdditionalReportInformation
    {
        #region Attributes

        /// <summary>
        /// The project name.
        /// </summary>
        private string projectName = LocalizedResources.Report_ProjectName;

        /// <summary>
        /// The parent name.
        /// </summary>
        private string parentName = LocalizedResources.Report_ParentName;

        /// <summary>
        /// The name.
        /// </summary>
        private string name = LocalizedResources.Report_Name;

        /// <summary>
        /// The version.
        /// </summary>
        private string version = LocalizedResources.Report_Version;

        #endregion Attributes

        #region Properties

        /// <summary>
        /// Gets or sets the project name.
        /// </summary>
        public string ProjectName
        {
            get
            {
                return this.projectName;
            }

            set
            {
                this.projectName = value;
            }
        }

        /// <summary>
        /// Gets or sets the parent name.
        /// </summary>
        public string ParentName
        {
            get
            {
                return this.parentName;
            }

            set
            {
                this.parentName = value;
            }
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                this.name = value;
            }
        }

        /// <summary>
        /// Gets or sets the author.
        /// </summary>
        public string Author { get; set; }

        /// <summary>
        /// Gets or sets the date.
        /// </summary>
        public string Date { get; set; }

        /// <summary>
        /// Gets or sets the curency.
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        public string Version
        {
            get
            {
                return this.version;
            }

            set
            {
                this.version = value;
            }
        }

        #endregion Properties
    }
}
