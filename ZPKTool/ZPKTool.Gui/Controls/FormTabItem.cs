﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace ZPKTool.Gui.Controls
{
    /// <summary>
    /// A TabItem to be used when inputting data in form-like screens.
    /// </summary>
    public class FormTabItem : TabItem
    {
        /// <summary>
        /// Using a DependencyProperty as the backing store for InvalidInputMessage.  This enables animation, styling, binding, etc...                
        /// </summary>
        public static readonly DependencyProperty InvalidInputMessageProperty =
            DependencyProperty.Register("InvalidInputMessage", typeof(object), typeof(FormTabItem), new UIPropertyMetadata(null));

        /// <summary>
        /// Initializes static members of the <see cref="FormTabItem"/> class.
        /// </summary>
        static FormTabItem()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(FormTabItem), new FrameworkPropertyMetadata(typeof(FormTabItem)));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FormTabItem"/> class.
        /// </summary>
        public FormTabItem()
        {
        }

        /// <summary>
        /// Gets or sets the message to display when hovering the mouse over the invalid input icon.
        /// </summary>        
        public object InvalidInputMessage
        {
            get { return (object)GetValue(InvalidInputMessageProperty); }
            set { SetValue(InvalidInputMessageProperty, value); }
        }
    }
}
