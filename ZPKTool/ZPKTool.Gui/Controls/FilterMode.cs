﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Gui.Controls
{
    /// <summary>
    /// The ways by which to filter the content of an items control.
    /// </summary>
    public enum FilterMode
    {
        /// <summary>
        /// The elements that start with the specified character(s) are displayed.
        /// </summary>
        StartsWith = 0,

        /// <summary>
        /// The elements that contain the specified character(s) are displayed.
        /// </summary>
        Contains = 1
    }
}
