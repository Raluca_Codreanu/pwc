﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace ZPKTool.Gui.Utils
{
    /// <summary>
    /// Contains helper methods for the UI classes.
    /// </summary>
    public static class UIHelper
    {
        /// <summary>
        /// Finds a parent of a given item on the visual tree.
        /// </summary>
        /// <typeparam name="T">The type of the queried item.</typeparam>
        /// <param name="child">A direct or indirect child of the queried item.</param>
        /// <returns>The first parent item that matches the submitted type parameter. If not matching item can be found, a null reference is being returned.</returns>
        public static T FindParent<T>(DependencyObject child) where T : DependencyObject
        {
            // get parent item
            DependencyObject parentObject = GetParentObject(child);

            // we've reached the end of the tree
            if (parentObject == null)
            {
                return null;
            }

            // check if the parent matches the type we're looking for
            T parent = parentObject as T;
            if (parent != null)
            {
                return parent;
            }
            else
            {
                // use recursion to proceed with next level
                return FindParent<T>(parentObject);
            }
        }

        /// <summary>
        /// Finds in the visual tree the 1st parent of a given element that has the specified type.
        /// </summary>
        /// <param name="child">The element whose parent to find.</param>
        /// <param name="parentType">Type of the searched parent.</param>
        /// <returns>
        /// The first parent that matches the specified type. If no parent is found, returns null.
        /// </returns>        
        public static object FindParent(DependencyObject child, Type parentType)
        {
            return FindParent(child, parentType, 1);
        }

        /// <summary>
        /// Finds in the visual tree the n-th parent of a given element. The parent must have the specified type.        
        /// </summary>
        /// <param name="child">The element whose parent to find.</param>
        /// <param name="parentType">Type of the searched parent.</param>
        /// <param name="parentLevel">The ordinal position of the parent among all parents of the given type. Must be greater than or equal to 1.</param>
        /// <returns>
        /// The first parent that matches the specified type. If no parent is found, returns null.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">The parent type was null.</exception>
        /// <exception cref="System.ArgumentException">The parent level was less than 1.</exception>
        public static object FindParent(DependencyObject child, Type parentType, int parentLevel)
        {
            if (parentType == null)
            {
                throw new ArgumentNullException("parentType", "The parent type was null.");
            }

            if (parentLevel <= 0)
            {
                throw new ArgumentException("The parent level can not be less than 1.", "parentLevel");
            }

            // Get the child's direct parent.
            DependencyObject parentObject = GetParentObject(child);
            if (parentObject == null)
            {
                return null;
            }

            var parentObjectType = parentObject.GetType();
            if (parentObjectType == parentType || parentObjectType.IsSubclassOf(parentType))
            {
                parentLevel--;
                if (parentLevel == 0)
                {
                    return parentObject;
                }
            }

            return FindParent(parentObject, parentType, parentLevel);
        }

        /// <summary>
        /// This method is an alternative to WPF's <see cref="VisualTreeHelper.GetParent"/> method, which also supports content elements.
        /// Do note, that for content element, this method falls back to the logical tree of the element!
        /// </summary>
        /// <param name="child">The item to be processed.</param>
        /// <returns>The submitted item's parent, if available. Otherwise null.</returns>
        public static DependencyObject GetParentObject(DependencyObject child)
        {
            if (child == null)
            {
                return null;
            }

            ContentElement contentElement = child as ContentElement;
            if (contentElement != null)
            {
                DependencyObject parent = ContentOperations.GetParent(contentElement);
                if (parent != null)
                {
                    return parent;
                }

                FrameworkContentElement fce = contentElement as FrameworkContentElement;
                return fce != null ? fce.Parent : null;
            }

            // if it's not a ContentElement, rely on VisualTreeHelper
            return VisualTreeHelper.GetParent(child);
        }

        /// <summary>
        /// Analyzes both visual and logical tree in order to find all elements of a given type that are
        /// descendants of the <paramref name="source"/> item.
        /// </summary>
        /// <typeparam name="T">The type of the items to find.</typeparam>
        /// <param name="source">The root element that marks the source of the search.
        /// If the source is already of the requested type, it will not be included in the result.</param>
        /// <returns>All descendants of <paramref name="source"/> that match the requested type.</returns>
        public static IEnumerable<T> FindChildren<T>(this DependencyObject source) where T : DependencyObject
        {
            if (source != null)
            {
                var children = GetChildren(source);
                foreach (DependencyObject child in children)
                {
                    // analyze if children match the requested type
                    if (child != null && child is T)
                    {
                        yield return (T)child;
                    }

                    // recurse tree
                    foreach (T descendant in FindChildren<T>(child))
                    {
                        yield return descendant;
                    }
                }
            }
        }

        /// <summary>
        /// Gets the visual child.
        /// </summary>
        /// <typeparam name="T">The type of the queried item.</typeparam>
        /// <param name="parent">The parent.</param>
        /// <returns>The first child of type T.</returns>
        public static T GetVisualChild<T>(Visual parent) where T : Visual
        {
            // TODO: it should test all children of parent if they are T before searching into children (now it tests a child and if it is not T it tests the child's children and so on).
            T child = default(T);
            int numVisuals = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < numVisuals; i++)
            {
                Visual v = (Visual)VisualTreeHelper.GetChild(parent, i);
                child = v as T;
                if (child == null)
                {
                    child = GetVisualChild<T>(v);
                }

                if (child != null)
                {
                    break;
                }
            }

            return child;
        }

        /// <summary>
        /// This method is an alternative to WPF's <see cref="VisualTreeHelper.GetChild"/> method, which also supports content elements.
        /// Keep in mind that for content elements, this method falls back to the logical tree of the element.
        /// </summary>
        /// <param name="parent">The item to be processed.</param>
        /// <returns>The submitted item's child elements, if available.</returns>
        public static IEnumerable<DependencyObject> GetChildren(this DependencyObject parent)
        {
            if (parent == null)
            {
                yield break;
            }

            ContentElement contentElement = parent as ContentElement;
            if (contentElement != null)
            {
                // use the logical tree for content elements
                foreach (object obj in LogicalTreeHelper.GetChildren(contentElement))
                {
                    var depObj = obj as DependencyObject;
                    if (depObj != null)
                    {
                        yield return (DependencyObject)obj;
                    }
                }
            }
            else
            {
                // use the visual tree per default
                int count = VisualTreeHelper.GetChildrenCount(parent);
                for (int i = 0; i < count; i++)
                {
                    yield return VisualTreeHelper.GetChild(parent, i);
                }
            }
        }
    }
}
