﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using ZPKTool.Common;

namespace ZPKTool.Gui.Utils
{
    /// <summary>
    /// This class contains methods from converting between different types of objects/values.
    /// </summary>
    public static class Converter
    {
        /// <summary>
        /// Converts an empty string to null. A non-empty string is not changed in any way.
        /// </summary>
        /// <param name="src">The input string.</param>
        /// <returns>The input string if it is not empty, null otherwise.</returns>
        public static string EmptyStringAsNull(string src)
        {
            if (src == string.Empty)
            {
                return null;
            }
            else
            {
                return src;
            }
        }

        /// <summary>
        /// Convert a string to an integer number.
        /// </summary>
        /// <param name="src">The string to convert.</param>
        /// <returns>The converted integer or null if the conversion failed of the input was empty/null.</returns>
        public static int? StringToNullableInt(string src)
        {
            if (string.IsNullOrEmpty(src))
            {
                return null;
            }

            int result;
            if (int.TryParse(src, NumberStyles.AllowThousands, CultureInfo.CurrentCulture, out result))
            {
                return result;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Convert a string to a decimal number.
        /// </summary>
        /// <param name="src">The string to convert.</param>
        /// <returns>If the conversion succeeded returns the converted decimal;
        /// if it failed or the input empty/null returns null.</returns>
        public static decimal? StringToNullableDecimal(string src)
        {
            if (string.IsNullOrEmpty(src))
            {
                return null;
            }

            decimal result;
            if (decimal.TryParse(src, out result))
            {
                return result;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Casts to specified type.
        /// </summary>
        /// <param name="type">The type of the object to cast.</param>
        /// <param name="obj">The object source.</param>
        /// <param name="result">The result object.</param>
        /// <returns>
        /// True if the cast was successful, false otherwise.
        /// </returns>
        public static bool Cast(Type type, object obj, ref object result)
        {
            if (obj == null)
            {
                return false;
            }

            if (string.IsNullOrWhiteSpace(obj.ToString()))
            {
                return false;
            }

            if (type == typeof(byte) || (type == typeof(byte?)))
            {
                result = Convert.ToByte(obj);
            }
            else if (type == typeof(short) || (type == typeof(short?)))
            {
                result = Convert.ToInt16(obj);
            }
            else if (type == typeof(int) || (type == typeof(int?)))
            {
                result = Convert.ToInt32(obj);
            }
            else if (type == typeof(double) || (type == typeof(double?)))
            {
                result = Convert.ToDouble(obj);
            }
            else if (type == typeof(decimal) || (type == typeof(decimal?)))
            {
                result = Convert.ToDecimal(obj);
            }
            else if (type == typeof(string))
            {
                result = Convert.ToString(obj);
            }
            else if (type == typeof(bool) || (type == typeof(bool?)))
            {
                result = Convert.ToBoolean(obj);
            }
            else if (type == typeof(Guid) || (type == typeof(Guid?)))
            {
                if (string.IsNullOrWhiteSpace(obj.ToString()))
                {
                    result = Guid.NewGuid();
                }
                else
                {
                    result = new Guid(obj.ToString());
                }
            }

            return true;
        }
    }
}
