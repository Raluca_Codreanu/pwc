﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Common;
using ZPKTool.DataAccess;

namespace ZPKTool.Gui.Utils
{
    /// <summary>
    /// Weak event manager for the <see cref="DataSourceManagerChangesSavedEventManager.ChangesSaved" /> event.
    /// </summary>
    public sealed class DataSourceManagerChangesSavedEventManager : WeakEventManagerBase<DataSourceManagerChangesSavedEventManager, IDataSourceManager>
    {
        /// <summary>
        /// Attaches the event handler.
        /// </summary>
        /// <param name="source">The source to which to attach.</param>
        protected override void StartListening(IDataSourceManager source)
        {
            source.ChangesSaved += DeliverEvent;
        }

        /// <summary>
        /// Detaches the event handler.
        /// </summary>
        /// <param name="source">The source from which to detach.</param>
        protected override void StopListening(IDataSourceManager source)
        {
            source.ChangesSaved -= DeliverEvent;
        }
    }
}
