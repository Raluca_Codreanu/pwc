﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using ZPKTool.Business.Export;
using ZPKTool.Common;
using ZPKTool.Controls;
using ZPKTool.Data;
using ZPKTool.Gui.Resources;

namespace ZPKTool.Gui.Utils
{
    /// <summary>
    /// Helper class containing useful methods used all over the application
    /// </summary>
    public static class UIUtils
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Determines whether the specified key causes data grid selection change or not.
        /// </summary>
        /// <param name="key">The specified key.</param>
        /// <returns><c>true</c> if it causes; otherwise, <c>false</c>.</returns>
        public static bool IsCausingDataGridSelectionChange(System.Windows.Input.Key key)
        {
            if (key == System.Windows.Input.Key.Up ||
                key == System.Windows.Input.Key.Down ||
                key == System.Windows.Input.Key.PageUp ||
                key == System.Windows.Input.Key.PageDown ||
                key == System.Windows.Input.Key.Enter ||
                key == System.Windows.Input.Key.Return)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Focus an UI element. If calling the Focus() method of the element doe not work then use this method.
        /// </summary>
        /// <param name="element">The element.</param>
        public static void FocusElement(UIElement element)
        {
            if (element != null)
            {
                bool focused = element.Focus();
                if (!focused)
                {
                    Action action = () => { element.Focus(); };
                    element.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Send, action);
                }
            }
        }

        /// <summary>
        /// Gets the text that should be displayed on the UI in place of an enumeration value.
        /// </summary>
        /// <param name="enumValue">The enum value for which to get the label.</param>
        /// <returns>A string containing the text associated with the enum value or an empty string
        /// if the dictionary does not contain any text associated with the enum value.</returns>
        public static string GetEnumValueLabel(Enum enumValue)
        {
            string label = string.Empty;
            Type enumType = enumValue.GetType();
            string enumMemberName = Enum.GetName(enumType, enumValue);
            if (enumMemberName != null)
            {
                label = LocalizedResources.ResourceManager.GetString(enumType.Name + "_" + enumMemberName);
            }

            return label;
        }

        /// <summary>
        /// Gets the text that should be displayed on the UI in place of an enumeration underlying value and enum type.
        /// </summary>
        /// <param name="underlyingValue">The index.</param>
        /// <param name="valueType">Type of the value.</param>
        /// <returns>A string containing the text associated with the enum underlying value and enum type or an empty string if the dictionary does not contain any text associated.</returns>
        public static string GetEnumValueLabel(long underlyingValue, Type valueType)
        {
            string label = string.Empty;
            if (valueType != null)
            {
                string[] names = Enum.GetNames(valueType);
                if (underlyingValue >= 0
                    && underlyingValue < names.Length)
                {
                    label = LocalizedResources.ResourceManager.GetString(valueType.Name + "_" + names[underlyingValue]);
                }
            }

            return label;
        }

        /// <summary>
        /// Finds the first text box and adds focus on it
        /// </summary>
        /// <param name="content">Content in which the search is done</param>
        public static void FocusFirstTextbox(object content)
        {
            UIElement contentElem = content as UIElement;
            if (contentElem != null)
            {
                TextBoxControl firstTextbox = UIHelper.FindChildren<TextBoxControl>(contentElem).FirstOrDefault();
                if (firstTextbox != null && !firstTextbox.IsReadOnly)
                {
                    UIUtils.FocusElement(firstTextbox);
                }
            }
        }
        
        /// <summary>
        /// Returns the file path of the log file in use
        /// </summary>
        /// <returns>A string containing the path.</returns>
        public static string GetLogFilePath()
        {
            string path = null;

            var fileTarget = NLog.LogManager.Configuration.FindTargetByName("file") as NLog.Targets.FileTarget;
            if (fileTarget != null)
            {
                path = NLog.Layouts.SimpleLayout.Evaluate(fileTarget.FileName.ToString());
                path = path.Trim('\'');
            }

            return path;
        }

        /// <summary>
        /// Gets the dialog filter to be used when importing multiple object types.
        /// </summary>
        /// <param name="typesToImport">The types that can be imported.</param>
        /// <returns>A string containing the dialog filter.</returns>
        public static string GetDialogFilterForMultipleImportedTypes(IEnumerable<Type> typesToImport)
        {
            string filter = null;

            // Get the dialog filters for each entity type and join them.
            var typesFilters = typesToImport.Select(t => GetDialogFilterForExportImport(t));
            filter = string.Join("|", typesFilters);

            if (typesFilters.Count() > 1)
            {
                // Get the extensions from dialog filters and create the filter for all the supported types.
                var typesExtensionsFilter = typesFilters.Select(f =>
                {
                    var parts = f.Split('|');
                    switch (parts.Length)
                    {
                        case 1:
                            return parts[0];

                        case 2:
                            return parts[1];

                        default:
                            return f;
                    }
                });
                filter = LocalizedResources.DialogFilter_AllSuportedFiles + "|" + string.Join(";", typesExtensionsFilter) + "|" + filter;
            }

            return filter;
        }

        /// <summary>
        /// Gets the dialog filter to be used when exporting or importing an object of a specified type.
        /// </summary>
        /// <param name="objectType">Type of the exported/imported object.</param>
        /// <returns>A string containing the dialog filter.</returns>
        public static string GetDialogFilterForExportImport(Type objectType)
        {
            string filter = null;
            if (objectType == typeof(Commodity))
            {
                filter = LocalizedResources.ExportImportDialogFilter_Commodity;
            }
            else if (objectType == typeof(Consumable))
            {
                filter = LocalizedResources.ExportImportDialogFilter_Consumable;
            }
            else if (objectType == typeof(Die))
            {
                filter = LocalizedResources.ExportImportDialogFilter_Die;
            }
            else if (objectType == typeof(Machine))
            {
                filter = LocalizedResources.ExportImportDialogFilter_Machine;
            }
            else if (objectType == typeof(RawMaterial))
            {
                filter = LocalizedResources.ExportImportDialogFilter_RawMaterial;
            }
            else if (objectType == typeof(Part))
            {
                filter = LocalizedResources.ExportImportDialogFilter_Part;
            }
            else if (objectType == typeof(RawPart))
            {
                filter = LocalizedResources.ExportImportDialogFilter_RawPart;
            }
            else if (objectType == typeof(Project))
            {
                filter = LocalizedResources.ExportImportDialogFilter_Project;
            }
            else if (objectType == typeof(ProjectFolder))
            {
                filter = LocalizedResources.ExportImportDialogFilter_ProjectFolder;
            }
            else if (objectType == typeof(Assembly))
            {
                filter = LocalizedResources.ExportImportDialogFilter_Assembly;
            }
            else
            {
                filter = LocalizedResources.ExportImportDialogFilter_Default;
            }

            return filter;
        }

        /// <summary>
        /// Gets the file name with the extension for the object to export.
        /// With this name the exported object will be saved to disc. 
        /// </summary>
        /// <param name="entity">The object to export.</param>
        /// <returns> The file name with the extension for the object to export. </returns>
        public static string GetExportObjectFileName(object entity)
        {
            var defaultFileName = string.Empty;
            var defaultFileNameVersion = GetVersion(entity);

            var namedObject = entity as INameable;
            if (namedObject != null)
            {
                defaultFileName = namedObject.Name;
                var fileExtension = ExportedTypesInfo.GetExtensionFromType(entity.GetType());

                if (defaultFileNameVersion != null)
                {
                    defaultFileName += "_v" + defaultFileNameVersion.Value;
                }

                defaultFileName = PathUtils.SanitizeFileName(defaultFileName + fileExtension);
            }

            return defaultFileName;
        }

        /// <summary>
        /// Gets the message that is shown in the 
        /// please wait service during the export of an object.
        /// </summary>
        /// <param name="entity">The object that is going to be exported.</param>
        /// <returns>The message to be shown into the please wait service during export.</returns>
        public static string GetExportWaitMessage(object entity)
        {
            string message;
            var type = entity.GetType();
            if (type == typeof(ProjectFolder))
            {
                message = LocalizedResources.General_WaitProjectFolderExport;
            }
            else if (type == typeof(Project))
            {
                message = LocalizedResources.General_WaitProjectExport;
            }
            else if (type == typeof(Assembly))
            {
                message = LocalizedResources.General_WaitAssemblyExport;
            }
            else if (type == typeof(Part))
            {
                message = LocalizedResources.General_WaitPartExport;
            }
            else if (type == typeof(RawPart))
            {
                message = LocalizedResources.General_WaitRawPartExport;
            }
            else if (type == typeof(RawMaterial))
            {
                message = LocalizedResources.General_WaitRawMaterialExport;
            }
            else if (type == typeof(Commodity))
            {
                message = LocalizedResources.General_WaitCommodityExport;
            }
            else if (type == typeof(Consumable))
            {
                message = LocalizedResources.General_WaitConsumableExport;
            }
            else if (type == typeof(Machine))
            {
                message = LocalizedResources.General_WaitMachineExport;
            }
            else if (type == typeof(Die))
            {
                message = LocalizedResources.General_WaitDieExport;
            }
            else
            {
                message = LocalizedResources.General_WaitExport;
            }

            return message;
        }

        /// <summary>
        /// Get the value of the Version field of a specified object, if the object has that field.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns>The version or null.</returns>
        public static decimal? GetVersion(object obj)
        {
            decimal? version = null;
            Project proj = obj as Project;
            if (proj != null)
            {
                version = proj.Version;
            }
            else
            {
                Assembly assy = obj as Assembly;
                if (assy != null)
                {
                    version = assy.Version;
                }
                else
                {
                    Part part = obj as Part;
                    if (part != null)
                    {
                        version = part.Version;
                    }
                }
            }

            return version;
        }

        /// <summary>
        /// Gets the localized name of property.
        /// </summary>
        /// <param name="propertyInfo">The property info.</param>
        /// <returns>The localized name of the given property or the property name as it is if it was not found in the resource file.</returns>
        public static string GetLocalizedNameOfProperty(System.Reflection.PropertyInfo propertyInfo)
        {
            string result = string.Empty;

            if (propertyInfo != null)
            {
                string resourceKey = propertyInfo.DeclaringType.Name + "_" + propertyInfo.Name;
                result = LocalizedResources.ResourceManager.GetString(resourceKey);

                if (string.IsNullOrWhiteSpace(result))
                {
                    Log.Warn("Could not find localized name for key: " + resourceKey);
                    result = propertyInfo.Name;
                }
            }

            return result;
        }
    }
}