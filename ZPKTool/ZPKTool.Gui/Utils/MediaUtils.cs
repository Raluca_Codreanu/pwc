﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using ZPKTool.Common;
using ZPKTool.Data;

namespace ZPKTool.Gui.Utils
{
    /// <summary>
    /// The image encodings supported by EncodeImage.
    /// </summary>
    public enum ImageEncodings
    {
        /// <summary>
        /// PNG format.
        /// </summary>
        PNG,

        /// <summary>
        /// BMP format.
        /// </summary>
        BMP,

        /// <summary>
        /// Tiff format.
        /// </summary>
        TIFF,

        /// <summary>
        /// Gif format.
        /// </summary>
        GIF,

        /// <summary>
        /// WMP format.
        /// </summary>
        WMP
    }

    /// <summary>
    /// Contains methods used to manipulate images and videos.
    /// </summary>
    public static class MediaUtils
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The maximum width an image can have. If an image has a larger width it is, usually, proportionally resized to this value.
        /// </summary>
        public const int MaxImageWidth = 800;

        /// <summary>
        /// Creates a bitmap image from its binary representation with the purpose of displaying it in the application.
        /// The created image is decoded with the width MediaUtils.MaxImageWidth
        /// </summary>
        /// <param name="imageData">The binary representation of the image.</param>
        /// <returns>A BitmapImage instance or null, if the binary input was null.</returns>
        public static BitmapImage CreateImageForDisplay(byte[] imageData)
        {
            return CreateImageInternal(imageData);
        }

        /// <summary>
        /// Creates a bitmap image from its binary representation with the purpose of displaying it in the application.
        /// The created image is decoded with the width MediaUtils.MaxImageWidth
        /// </summary>
        /// <param name="imageData">The binary representation of the image.</param>
        /// <param name="decodePixelWidth">The width, in pixels, that the image is decoded to.</param>
        /// <returns>A BitmapImage instance or null, if the binary input was null.</returns>
        public static BitmapImage CreateImageForDisplay(byte[] imageData, int decodePixelWidth)
        {
            return CreateImageInternal(imageData, decodePixelWidth);
        }

        /// <summary>
        /// Creates a bitmap image from a file with the purpose of displaying it in the application.
        /// The created image is decoded with the width MediaUtils.MaxImageWidth
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <returns>An instance of BitmapImage.</returns>        
        /// <exception cref="ZPKException">The BitmapImage creation failed.</exception>
        public static BitmapImage CreateImageForDisplay(string filePath)
        {
            try
            {
                using (var imageStream = new FileStream(filePath, FileMode.Open))
                {
                    var tempImage = new BitmapImage();
                    tempImage.BeginInit();
                    tempImage.CreateOptions = BitmapCreateOptions.None;

                    // Delay the image creation in order to first check its resolution
                    tempImage.CacheOption = BitmapCacheOption.OnDemand;

                    tempImage.StreamSource = imageStream;
                    tempImage.EndInit();

                    var image = new BitmapImage();
                    image.BeginInit();

                    // If the image's width is larger then the maximum allowed width, decode it with the max width. This saves memory when working with large resolution images.
                    if (tempImage.PixelWidth > MaxImageWidth)
                    {
                        // compute the multiplication factor in case the dpi of the image is different than the default dpi value 96
                        double factor = tempImage.DpiX / 96;
                        image.DecodePixelWidth = Convert.ToInt32(MaxImageWidth * factor);
                    }

                    imageStream.Position = 0;
                    image.CreateOptions = BitmapCreateOptions.None;
                    image.CacheOption = BitmapCacheOption.OnLoad;
                    image.StreamSource = imageStream;
                    image.EndInit();

                    image.Freeze();
                    return image;
                }
            }
            catch (Exception ex)
            {
                throw new UIException(ErrorCodes.ErrorCreatingImage, ex);
            }
        }

        /// <summary>
        /// Converts the image specified by <paramref name="imageData"/> into a JPEG image with the maximum width of MediaUtils.MaxImageWidth
        /// </summary>
        /// <param name="imageData">The input image.</param>
        /// <returns>JPEG image bytes</returns>
        public static byte[] GetMediaJPEGImageBytes(byte[] imageData)
        {
            BitmapImage image = MediaUtils.CreateImageInternal(imageData);
            return MediaUtils.EncodeImageAsJPEG(image, 0);
        }

        /// <summary>
        /// Encodes an image using the JPEG encoding.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <param name="qualityLevel">The quality level (1 - 100, 1 being worst quality).</param>
        /// <returns>A byte array containing the encoded image or null if the image was null.</returns>
        public static byte[] EncodeImageAsJPEG(BitmapSource image, int qualityLevel)
        {
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            if (qualityLevel > 0)
            {
                encoder.QualityLevel = qualityLevel;
            }

            return PerformEncoding(image, encoder);
        }

        /// <summary>
        /// Encodes an image using a chosen encoding.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <param name="encoding">The encoding.</param>
        /// <returns>A byte array containing the encoded image or null if the image was null.</returns>
        public static byte[] EncodeImage(BitmapSource image, ImageEncodings encoding)
        {
            BitmapEncoder encoder = null;
            switch (encoding)
            {
                case ImageEncodings.BMP:
                    encoder = new BmpBitmapEncoder();
                    break;

                case ImageEncodings.PNG:
                    encoder = new PngBitmapEncoder();
                    break;

                case ImageEncodings.TIFF:
                    encoder = new TiffBitmapEncoder();
                    break;

                case ImageEncodings.GIF:
                    encoder = new GifBitmapEncoder();
                    break;

                case ImageEncodings.WMP:
                    encoder = new WmpBitmapEncoder();
                    break;
            }

            return PerformEncoding(image, encoder);
        }

        /// <summary>
        /// Convert a System.Drawing.Icon object to a System.Windows.Media.Imaging.Bitmap image.
        /// </summary>
        /// <param name="icon">The icon to be converted.</param>
        /// <returns>The resulting bitmap image or null.</returns>
        /// <exception cref="System.ArgumentException">Can be thrown by internal call.</exception>
        /// <exception cref="System.Exception">Can be thrown by internal call.</exception>
        public static BitmapSource IconToBitmap(System.Drawing.Icon icon)
        {
            BitmapSource iconBitmap = null;

            if (icon != null)
            {
                System.Drawing.Bitmap bitmap = icon.ToBitmap();
                IntPtr bitmapHandle = bitmap.GetHbitmap();

                iconBitmap = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                    bitmapHandle, IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
            }

            if (iconBitmap != null)
            {
                iconBitmap.Freeze();
            }

            return iconBitmap;
        }

        /// <summary>
        /// Returns the content of the file on the disk
        /// </summary>
        /// <param name="sourcePath">Path to the source file</param>
        /// <returns>A byte array</returns>
        public static byte[] GetFileBytes(string sourcePath)
        {
            byte[] bytes = null;
            try
            {
                using (FileStream fs = new FileStream(sourcePath, FileMode.Open, FileAccess.Read))
                {
                    int fileLen = (int)fs.Length;
                    bytes = new byte[fileLen];
                    fs.Read(bytes, 0, fileLen);
                }
            }
            catch (Exception ex)
            {
                log.ErrorException("Failed to read media file.", ex);
                throw new ZPKException(ErrorCodes.MediaFileReadFailed);
            }

            return bytes;
        }

        /// <summary>
        /// Creates a bitmap image from its binary representation.
        /// The created image is decoded with the maximum width of MediaUtils.MaxImageWidth
        /// </summary>
        /// <param name="imageData">The binary representation of the image.</param>
        /// <param name="decodePixelWidth">The width, in pixels, that the image is decoded to.</param>
        /// <returns>
        /// A BitmapImage instance or null, if the creation failed.
        /// </returns>
        private static BitmapImage CreateImageInternal(byte[] imageData, int decodePixelWidth = MediaUtils.MaxImageWidth)
        {
            if (imageData == null || imageData.Length == 0)
            {
                return null;
            }

            BitmapImage image = null;
            try
            {
                image = new BitmapImage();
                image.BeginInit();

                image.CreateOptions = BitmapCreateOptions.None;

                // Delay the image creation in order to first check its resolution
                image.CacheOption = BitmapCacheOption.OnDemand;

                image.StreamSource = new MemoryStream(imageData);
                image.EndInit();

                // If the image's width is larger then the maximum allowed width, decode it with the max width. This saves memory when working with large resolution images.
                if (image.PixelWidth > decodePixelWidth)
                {
                    // compute the multiplication factor in case the dpi of the image is different than the default dpi value 96
                    double factor = image.DpiX / 96;
                    image = null;
                    image = new BitmapImage();
                    image.BeginInit();
                    image.DecodePixelWidth = Convert.ToInt32(decodePixelWidth * factor);
                    image.CreateOptions = BitmapCreateOptions.None;
                    image.CacheOption = BitmapCacheOption.OnDemand;
                    image.StreamSource = new MemoryStream(imageData);
                    image.EndInit();
                }

                image.Freeze();
            }
            catch (Exception ex)
            {
                log.ErrorException("Failed to convert binary data to image.", ex);
                image = null;
            }

            return image;
        }

        /// <summary>
        /// Encode an image using a given encoder.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <param name="encoder">The encoder.</param>
        /// <returns>A byte array containing the encoded image, or null if the encoding failed.</returns>
        private static byte[] PerformEncoding(BitmapSource image, BitmapEncoder encoder)
        {
            if (image == null || encoder == null)
            {
                return null;
            }

            byte[] result = null;

            try
            {
                MemoryStream stream = new MemoryStream();
                encoder.Frames.Add(BitmapFrame.Create(image));
                encoder.Save(stream);
                stream.Seek(0, SeekOrigin.Begin);
                result = new byte[stream.Length];
                BinaryReader br = new BinaryReader(stream);
                br.Read(result, 0, (int)stream.Length);
                br.Close();
                stream.Close();
            }
            catch (Exception ex)
            {
                log.ErrorException("Failed to encode image.", ex);
                result = null;
            }

            return result;
        }
    }
}