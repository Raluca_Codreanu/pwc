﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using ZPKTool.Gui.Controls;

namespace ZPKTool.Gui.Utils
{
    /// <summary>
    /// Helper methods for window-related functionality.
    /// </summary>
    public static class WindowHelper
    {
        /// <summary>
        /// Finds a suitable owner for the specified window.
        /// </summary>
        /// <param name="window">The window for which to determine the owner.</param>
        /// <returns>
        /// A Window or null.
        /// </returns>
        public static Window DetermineWindowOwner(Window window)
        {
            // Set the active window as the owner, if there is one. Ignore windows opened by AvalonDock.
            var appWindows = WindowHelper.GetApplicationWindows();
            Window owner = appWindows.FirstOrDefault(wnd => wnd != window && wnd.IsActive);
            if (owner == null)
            {
                // If there is no active window (the app does not have focus), use the last loaded window from the app windows list as the owner.
                owner = appWindows.Reverse().FirstOrDefault(wnd => wnd != window && wnd.IsLoaded);
            }

            if (owner is MessageWindow)
            {
                return DetermineWindowOwner(owner);
            }
            else
            {
                return owner;
            }
        }

        /// <summary>
        /// Get all distinct windows created by the application, excluding any inner or overlay windows created by AvalonDock or other frameworks.
        /// </summary>
        /// <returns>The list of application windows.</returns>
        public static IEnumerable<Window> GetApplicationWindows()
        {
            if (Application.Current == null)
            {
                return new List<Window>();
            }

            return Application.Current.Windows.OfType<Window>().Where(wnd =>
            {
                if (wnd == null)
                {
                    return false;
                };

                // Exclude from the list the avalon dock windows and the windows created by the Visual Studio UI debug tools.
                var type = wnd.GetType();
                if (type == typeof(AvalonDock.AvalonDockWindow)
                || type.FullName == "Microsoft.XamlDiagnostics.WpfTap.WpfVisualTreeService.Adorners.AdornerLayerWindow")
                {
                    return false;
                }

                return true;
            });
        }
    }
}
