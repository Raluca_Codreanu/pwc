﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using ZPKTool.Gui.Resources;

namespace ZPKTool.Gui.Utils
{
    /// <summary>
    /// Provides localized descriptions for the members of an enumeration.
    /// The localization of each enumeration member must follow the convention of having a key in the localized resources
    /// composed of the enumeration type name followed by underscore followed by the enumeration member name (ex: ProjectStatus_Working).
    /// <para/>
    /// Derives from list so it can be used as ItemsSource for ItemsControl, like combo boxes.
    /// The display member is "Description" and the value member is "Value".
    /// </summary>
    /// <typeparam name="T">The type for which to provide the descriptions. Must be an enum.</typeparam>
    public class EnumerationDescription<T> : ObservableCollection<ValueDescription<T>>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EnumerationDescription&lt;T&gt;"/> class.
        /// </summary>
        /// <exception cref="ArgumentException">T is not derived from System.Enum</exception>
        public EnumerationDescription()
            : this(null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EnumerationDescription{T}"/> class.
        /// </summary>
        /// <param name="elements">The elements.</param>
        /// <exception cref="System.ArgumentException">The provided type was not an enumeration;T</exception>
        public EnumerationDescription(IEnumerable<T> elements)
        {
            Type enumType = typeof(T);
            if (!enumType.IsSubclassOf(typeof(System.Enum)))
            {
                throw new ArgumentException("The provided type was not an enumeration", "T");
            }

            // If no element was specified add all of them.
            if (elements == null)
            {
                elements = Enum.GetValues(enumType).Cast<T>();
            }

            foreach (T value in elements)
            {
                string enumValueName = Enum.GetName(enumType, value);
                string enumValueDescription = LocalizedResources.ResourceManager.GetString(enumType.Name + "_" + enumValueName);
                ValueDescription<T> item = new ValueDescription<T>(value, enumValueDescription);
                this.Add(item);
            }
        }
    }
}
