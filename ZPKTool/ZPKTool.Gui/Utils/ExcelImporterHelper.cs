﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using NPOI.SS.UserModel;
using OfficeOpenXml;
using ZPKTool.Gui.Resources;

namespace ZPKTool.Gui.Utils
{
    /// <summary>
    /// Contains helper methods for imports excel file.
    /// </summary>
    public static class ExcelImporterHelper
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Imports the data from XLS.
        /// </summary>
        /// <param name="selectedSheet">The selected sheet.</param>
        /// <param name="tableData">The table data.</param>
        /// <param name="firstCellTableAddress">The first cell table address</param>
        /// <returns>The proper error message or empty string if there were no issues.</returns>
        public static string TryPopulateTableFromExcel(object selectedSheet, List<List<object>> tableData, string firstCellTableAddress)
        {
            var errorMessage = string.Empty;

            try
            {
                if (selectedSheet != null && !string.IsNullOrWhiteSpace(firstCellTableAddress))
                {
                    int rowIndex;
                    int columnIndex;
                    if (selectedSheet is ExcelWorksheet)
                    {
                        GetRowAndColumnIndexEPPlus(firstCellTableAddress, out rowIndex, out columnIndex);
                    }
                    else
                    {
                        GetRowAndColumnIndexNPOI(firstCellTableAddress, out rowIndex, out columnIndex);
                    }

                    errorMessage = PopulateTableDataFromSheet(selectedSheet, rowIndex, columnIndex, tableData);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error importing table data from xls file.", ex);
                errorMessage = LocalizedResources.Import_ErrorPopulateTable;
            }

            return errorMessage;
        }

        /// <summary>
        /// Populates the table data from sheet.
        /// </summary>
        /// <param name="workSheet">The work sheet.</param>
        /// <param name="indexRow">The index row.</param>
        /// <param name="indexColumn">The index column.</param>
        /// <param name="tableData">The table data.</param>
        /// <returns>The error string or string.empty if there were no issues.</returns>
        public static string PopulateTableDataFromSheet(object workSheet, int indexRow, int indexColumn, List<List<object>> tableData)
        {
            var errorString = string.Empty;

            ExcelWorksheet workSheet2007 = workSheet as ExcelWorksheet;
            if (workSheet2007 != null)
            {
                tableData.Clear();

                int indexBeginColumn = indexColumn;
                int headersCount = 0;

                // Get table headers.
                var headersData = GetHeadersList(workSheet2007, indexRow, ref indexColumn);

                if (indexColumn - indexBeginColumn < 3)
                {
                    return LocalizedResources.Import_ErrorLessThanThreeColumnsInCvs;
                }

                tableData.Add(headersData);
                headersCount = indexColumn;
                indexColumn = indexBeginColumn;
                indexRow++;

                // Add table data.
                while (workSheet2007.Cells[indexRow, indexColumn].Value != null)
                {
                    List<object> rowData = new List<object>();

                    for (int index = indexColumn; index < indexColumn + headersCount - 1; index++)
                    {
                        rowData.Add(workSheet2007.Cells[indexRow, index].Value);
                    }

                    tableData.Add(rowData);
                    indexRow++;
                }
            }
            else
            {
                ISheet sheet = workSheet as ISheet;
                if (sheet != null)
                {
                    tableData.Clear();

                    int indexBeginColumn = indexColumn;
                    int headersCount = 0;

                    // Get table headers.
                    var headersData = GetHeadersList(sheet, indexRow, ref indexColumn);

                    if (indexColumn - indexBeginColumn < 3)
                    {
                        return LocalizedResources.Import_ErrorLessThanThreeColumnsInXls;
                    }

                    tableData.Add(headersData);
                    headersCount = indexColumn;
                    indexColumn = indexBeginColumn;
                    indexRow++;

                    bool continnue = true;

                    // Add table data.
                    while (continnue)
                    {
                        if (sheet.GetRow(indexRow) == null)
                        {
                            continnue = false;
                            break;
                        }
                        else if (sheet.GetRow(indexRow).GetCell(indexColumn) == null)
                        {
                            if (sheet.GetRow(indexRow).GetCell(indexColumn + 1) == null)
                            {
                                continnue = false;
                                break;
                            }
                            else
                            {
                                sheet.GetRow(indexRow).CreateCell(indexColumn).SetCellValue(string.Empty);
                            }
                        }
                        else if (sheet.GetRow(indexRow).GetCell(indexColumn).CellType == CellType.BLANK)
                        {
                            continnue = false;
                            break;
                        }

                        var y = sheet.GetRow(indexRow).GetCell(indexColumn).CellType;
                        List<object> rowData = new List<object>();

                        for (int index = indexColumn; index < indexColumn + headersCount; index++)
                        {
                            if (sheet.GetRow(indexRow).GetCell(index) == null)
                            {
                                sheet.GetRow(indexRow).CreateCell(index).SetCellValue(string.Empty);
                                rowData.Add(string.Empty);
                            }
                            else if (sheet.GetRow(indexRow).GetCell(index).CellType == CellType.BOOLEAN)
                            {
                                rowData.Add(sheet.GetRow(indexRow).GetCell(index).BooleanCellValue);
                            }
                            else if (sheet.GetRow(indexRow).GetCell(index).CellType == CellType.NUMERIC)
                            {
                                rowData.Add(sheet.GetRow(indexRow).GetCell(index).NumericCellValue);
                            }
                            else if (sheet.GetRow(indexRow).GetCell(index).CellType == CellType.STRING)
                            {
                                rowData.Add(sheet.GetRow(indexRow).GetCell(index).StringCellValue);
                            }
                            else if (sheet.GetRow(indexRow).GetCell(index).CellType == CellType.BLANK)
                            {
                                rowData.Add(string.Empty);
                            }
                            else
                            {
                                rowData.Add(string.Empty);
                            }
                        }

                        if (!rowData.All(item => item == null || string.IsNullOrWhiteSpace(item.ToString())))
                        {
                            tableData.Add(rowData);
                        }

                        indexRow++;
                    }
                }
            }

            return errorString;
        }

        /// <summary>
        /// Gets the headers list from the excel file.
        /// </summary>
        /// <param name="workSheet">The work sheet.</param>
        /// <param name="indexRow">The index row.</param>
        /// <param name="indexColumn">The index column.</param>
        /// <returns>The list of headers.</returns>
        public static List<object> GetHeadersList(object workSheet, int indexRow, ref int indexColumn)
        {
            List<object> headersData = new List<object>();

            ExcelWorksheet workSheet2007 = workSheet as ExcelWorksheet;
            if (workSheet2007 != null)
            {
                // Add table headers.
                while (workSheet2007.Cells[indexRow, indexColumn].Value != null)
                {
                    headersData.Add(workSheet2007.Cells[indexRow, indexColumn].Value);
                    indexColumn++;
                }
            }
            else
            {
                ISheet sheet = workSheet as ISheet;
                if (sheet != null)
                {
                    // Add table headers.
                    while (sheet.GetRow(indexRow) != null
                        && sheet.GetRow(indexRow).GetCell(indexColumn) != null
                        && !string.IsNullOrWhiteSpace(sheet.GetRow(indexRow).GetCell(indexColumn).StringCellValue))
                    {
                        headersData.Add(sheet.GetRow(indexRow).GetCell(indexColumn).StringCellValue);
                        indexColumn++;
                    }
                }
            }

            return headersData;
        }

        /// <summary>
        /// Gets the row and the column index for a cell address for <see cref="EPPlus"/> library.
        /// It gets the coordinates as 1-based
        /// </summary>
        /// <param name="cellAddress">The cell address</param>
        /// <param name="rowIndex">The index of the row of the cell address</param>
        /// <param name="columnIndex">The index of the column of the cell address</param>
        public static void GetRowAndColumnIndexEPPlus(string cellAddress, out int rowIndex, out int columnIndex)
        {
            rowIndex = 0;
            columnIndex = 0;
            string columnPattern = @"[a-zA-Z]+";
            string rowPattern = @"[0-9]+";
            Match columnMatch = Regex.Match(cellAddress, columnPattern);
            if (columnMatch.Success)
            {
                columnIndex = ConvertColumnAddressToColumnIndex(columnMatch.Value);
            }

            Match rowMatch = Regex.Match(cellAddress, rowPattern);
            if (rowMatch.Success)
            {
                rowIndex = ConvertTo(rowMatch.Value, 0);
            }
        }

        /// <summary>
        /// Gets the row and the column index for a cell address for <see cref="NPOI"/> library.
        /// It gets the coordinates as 0-based
        /// </summary>
        /// <param name="cellAddress">The cell address</param>
        /// <param name="rowIndex">The index of the row of the cell address</param>
        /// <param name="columnIndex">The index of the column of the cell address</param>
        public static void GetRowAndColumnIndexNPOI(string cellAddress, out int rowIndex, out int columnIndex)
        {
            int row;
            int column;
            GetRowAndColumnIndexEPPlus(cellAddress, out row, out column);

            rowIndex = row - 1;
            columnIndex = column - 1;
        }

        /// <summary>
        /// Converts the index of the column address to  column index.
        /// </summary>
        /// <param name="columnAddress">Name of the column.</param>
        /// <returns>The column index.</returns>
        /// Convert given excel column name to column Index, ex 'A=1', 'AA=27'
        /// @parameter columnName
        /// @return 0 based index of the column
        public static int ConvertColumnAddressToColumnIndex(string columnAddress)
        {
            columnAddress = columnAddress.ToUpper();
            int value = 0;
            for (int i = 0, k = columnAddress.Length - 1; i < columnAddress.Length; i++, k--)
            {
                int alpabetIndex = ((short)columnAddress[i]) - 64;
                int delta = 0;

                // last column simply add it
                if (k == 0)
                {
                    delta = alpabetIndex;
                }
                else
                {
                    // aggregate
                    if (alpabetIndex == 0)
                    {
                        delta = 26 * k;
                    }
                    else
                    {
                        delta = alpabetIndex * 26 * k;
                    }
                }

                value += delta;
            }

            return value;
        }

        /// <summary>
        /// Converts an object to a given type without throwing conversion error.
        /// If the conversion fails it returns the value specified by the <paramref name="defaultValue"/> parameter.
        /// </summary>
        /// <typeparam name="T">The type to convert to.</typeparam>
        /// <param name="obj">The object to convert.</param>
        /// <param name="defaultValue">The value returned in case of a conversion error.</param>
        /// <returns>An instance of T.</returns>
        public static T ConvertTo<T>(object obj, T defaultValue)
        {
            try
            {
                return (T)Convert.ChangeType(obj, typeof(T));
            }
            catch
            {
            }

            return defaultValue;
        }
    }
}
