﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media.Imaging;
using Microsoft.Win32;
using ZPKTool.Common;

namespace ZPKTool.Gui.Utils
{
    /// <summary>
    /// This class implements methods for extracting the icon of the program associated in the system 
    /// with a given file extension.
    /// </summary>
    public class RegisteredFileTypeIconExtractor
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// This dictionary contains all file types registered in the system and the info needed to obtain
        /// their associated icons.
        /// </summary>
        private static Dictionary<string, string> fileTypesInfo = null;

        /// <summary>
        /// Object needed to synchronize the creation of the fileTypesInfo member.
        /// </summary>
        private static object fileTypesInfoLock = new object();

        /// <summary>
        /// Obtain the iconBitmap associated with a specific file type in the system.
        /// </summary>
        /// <param name="fileType">The type of file (the file extension, including the beginning dot).</param>
        /// <returns>
        /// A bitmap image if successful, null if the file type does not have an iconBitmap associated with it
        /// or an error occurred.
        /// </returns>
        public BitmapSource GetIconForFileType(string fileType)
        {
            BitmapSource iconBitmap = null;

            // Initialize the list of system registered file types. Only need to do it once, then reuse it.
            if (fileTypesInfo == null)
            {
                lock (fileTypesInfoLock)
                {
                    if (fileTypesInfo == null)
                    {
                        try
                        {
                            fileTypesInfo = GetSystemFileTypesAndIcons();
                        }
                        catch (Exception e)
                        {
                            // We ignore all errors because the registry may contain invalid data or the 
                            // user may not have the needed permissions to access the registry.
                            log.ErrorException("Failed to get system file types info: ", e);
                        }
                    }
                }
            }

            if (!string.IsNullOrEmpty(fileType))
            {
                // Add the starting dot to the file extension if it is missing
                if (!fileType.StartsWith("."))
                {
                    fileType = "." + fileType;
                }

                // We ignore any extraction and conversion errors because the icon source info is not reliable
                System.Drawing.Icon icon = null;
                try
                {
                    icon = ExtractIcon(fileTypesInfo, fileType);
                }
                catch (Exception e)
                {
                    log.ErrorException("Failed to extract a file type icon.", e);
                }

                try
                {
                    iconBitmap = MediaUtils.IconToBitmap(icon);
                }
                catch (Exception e)
                {
                    log.ErrorException("Failed to convert a file type icon to bitmap.", e);
                }
            }

            return iconBitmap;
        }

        /// <summary>
        /// Gets registered file types and their associated iconBitmap in the system (from the registry).
        /// </summary>
        /// <returns>Returns a dictionary which contains the file extension as key and the the iconBitmap file info
        /// as value.</returns>
        private Dictionary<string, string> GetSystemFileTypesAndIcons()
        {
            // Open the HKEY_CLASSES_ROOT registry section
            RegistryKey rootKey = Registry.ClassesRoot;

            // Gets all sub keys' names. Many of them represent registered file extensions.
            string[] subKeyNames = rootKey.GetSubKeyNames();
            Dictionary<string, string> iconsInfo = new Dictionary<string, string>();

            // Find the file icon.
            foreach (string keyName in subKeyNames)
            {
                if (string.IsNullOrEmpty(keyName))
                {
                    continue;
                }

                // If this key is not a file extension (does not start with .), skip it.
                int indexOfPoint = keyName.IndexOf(".");
                if (indexOfPoint != 0)
                {
                    continue;
                }

                RegistryKey fileTypeKey = rootKey.OpenSubKey(keyName);
                if (fileTypeKey == null)
                {
                    continue;
                }

                // Get the default value of this key that contains the information of file type.                
                object defaultValue = fileTypeKey.GetValue(string.Empty);
                if (defaultValue == null)
                {
                    continue;
                }

                try
                {
                    // Go to the key that specifies the default iconBitmap associated with this file type.
                    string defaultIcon = defaultValue.ToString() + "\\DefaultIcon";
                    RegistryKey fileIconKey = rootKey.OpenSubKey(defaultIcon);
                    if (fileIconKey != null)
                    {
                        // Get the path to the file that contains the iconBitmap and the index of the iconBitmap in that file.                        
                        object value = fileIconKey.GetValue(string.Empty);
                        if (value != null)
                        {
                            // Clear all unnecessary " sign in the string to avoid error.                            
                            string filePathAndIndex = value.ToString().Replace("\"", string.Empty);
                            iconsInfo.Add(keyName.ToLower(), filePathAndIndex);
                        }

                        fileIconKey.Close();
                    }
                }
                catch (Exception)
                {
                    // Ignore any errors that appear when extracting the icon path from the registry as that info
                    // is not guaranteed to be correct.                    
                }

                fileTypeKey.Close();
            }

            rootKey.Close();

            return iconsInfo;
        }

        /// <summary>
        /// Obtain the iconBitmap associated with a specific file type in the system.
        /// </summary>
        /// <param name="fileTypesInfo">All system registered file types and their associated iconBitmap info.</param>
        /// <param name="fileType">The type of file (the file extension, including the beginning dot).</param>
        /// <returns>
        /// The iconBitmap or null if it could not be extracted.
        /// </returns>
        private System.Drawing.Icon ExtractIcon(Dictionary<string, string> fileTypesInfo, string fileType)
        {
            // Validate the input parameters
            if (fileTypesInfo == null)
            {
                return null;
            }

            string fileAndIconIndex = null;
            try
            {
                fileAndIconIndex = fileTypesInfo[fileType.ToLower()];
            }
            catch (KeyNotFoundException)
            {
                return null;
            }

            if (string.IsNullOrEmpty(fileAndIconIndex))
            {
                return null;
            }

            System.Drawing.Icon icon = null;

            // The name of the file that contains the iconBitmap.
            string fileName = string.Empty;

            // The index of the iconBitmap in the file.
            string iconIndexString = string.Empty;

            // Separate the file name and iconBitmap index. fileAndIconIndex contains a string 
            // like c:\program\run.exe,10
            int index = fileAndIconIndex.IndexOf(",");
            if (index > 0)
            {
                fileName = fileAndIconIndex.Substring(0, index);
                iconIndexString = fileAndIconIndex.Substring(index + 1);
            }
            else
            {
                fileName = fileAndIconIndex;
            }

            // Convert the iconBitmap index to an integer and if any error occurs default it to 0.
            int iconIndex = 0;
            if (!string.IsNullOrEmpty(iconIndexString))
            {
                int.TryParse(iconIndexString, out iconIndex);
                if (iconIndex < 0)
                {
                    iconIndex = 0;
                }
            }

            // Get the handle of the icon bitmap.            
            IntPtr iconHandle = NativeMethods.ExtractIcon(
                System.Diagnostics.Process.GetCurrentProcess().Handle,
                fileName,
                iconIndex);

            // Create an iconBitmap from the handle
            if (iconHandle != IntPtr.Zero)
            {
                icon = System.Drawing.Icon.FromHandle(iconHandle);
            }

            return icon;
        }
    }
}
