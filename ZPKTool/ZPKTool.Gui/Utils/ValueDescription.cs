﻿namespace ZPKTool.Gui.Utils
{
    /// <summary>
    /// Holds the description of a specified value.
    /// </summary>
    /// <typeparam name="T1">The type of the value.</typeparam>
    public class ValueDescription<T1>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ValueDescription&lt;T1&gt;"/> class.
        /// </summary>
        public ValueDescription()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ValueDescription&lt;T1&gt;"/> class.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="description">The description.</param>
        public ValueDescription(T1 value, string description)
        {
            this.Value = value;
            this.Description = description;
        }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>            
        public T1 Value { get; set; }

        /// <summary>
        /// Gets or sets the description of the value.
        /// </summary>
        public string Description { get; set; }
    }
}