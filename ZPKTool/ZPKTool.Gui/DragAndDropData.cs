﻿namespace ZPKTool.Gui
{
    /// <summary>
    /// This class contains the data transferred during a drag-and-drop operation.
    /// </summary>
    public class DragAndDropData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DragAndDropData"/> class.
        /// </summary>
        public DragAndDropData()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DragAndDropData"/> class.
        /// </summary>
        /// <param name="dataObject">The data to be transferred.</param>
        /// <param name="domainObjectContext">The data context of the data to be transferred.</param>
        public DragAndDropData(object dataObject, ZPKTool.DataAccess.DbIdentifier domainObjectContext)
        {
            this.DataObject = dataObject;
            this.DataObjectContext = domainObjectContext;
        }

        /// <summary>
        /// Gets or sets the transferred data object.
        /// </summary>
        public object DataObject { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the data context of the DataObject.
        /// </summary>
        public ZPKTool.DataAccess.DbIdentifier DataObjectContext { get; set; }
    }
}
