﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

namespace ZPKTool.Data.Tests
{
    [TestClass]
    public class MediaTest
    {
        /// <summary>
        /// Tests that the Media.Content property is copied by value not by reference.
        /// </summary>
        [TestMethod]
        public void CopyMediaContentTest()
        {            
            Media media = new Media();
            media.Content = new byte[5] { 1, 0, 0, 1, 1 };

            var copy = media.Copy();
            
            Assert.AreNotEqual(media.Content, copy.Content, "The Media's Content was copied by reference, not by value.");
        }

        /// <summary>
        /// Tests that the Media.Content property assignment is by reference and not by value.
        /// </summary>
        [TestMethod]
        public void AssignMediaContentTest()
        {
            var data = new byte[5] { 1, 0, 0, 1, 1 };

            var media = new Media();
            media.Content = data;

            Assert.AreEqual(data, media.Content, "The Media's Content was assigned by value, not by reference.");
        }
    }
}
