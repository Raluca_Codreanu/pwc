﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ZPKTool.Data.Tests
{
    [TestClass]
    public class MasterDataScramblerTest
    {
        [TestMethod]
        public void ScramblePositiveDecimal()
        {
            decimal scrambledNumber = MasterDataScrambler.ScrambleNumber(75600.00099948m);
            Assert.AreEqual(28400.00031777m, scrambledNumber, "Decimal number scrambling has failed.");
        }

        [TestMethod]
        public void ScramblePositiveDecimalWithMaximumDecimalPlaces()
        {
            var number = 0.8376521984678234509638126535M;
            decimal scrambledNumber = MasterDataScrambler.ScrambleNumber(number);

            Assert.AreEqual(0.8582963527081593421376982453M, scrambledNumber, "The scrambling of a decimal number yielded an unexpected result.");
        }

        /// <summary>
        /// Scrambles the max decimal number that can be scrambled (not decimal.Max).
        /// </summary>
        [TestMethod]
        public void ScrambleMaxDecimal()
        {
            decimal scrambledNumber = MasterDataScrambler.ScrambleNumber(decimal.MaxValue / 10m);
            Assert.AreEqual(1508751857845512916892639974M, scrambledNumber, "The scrambling of the max allowed decimal number yielded an unexpected result.");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ScrambleDecimalLargerThanSupported()
        {
            decimal scrambledNumber = MasterDataScrambler.ScrambleNumber(decimal.MaxValue);
        }

        [TestMethod]
        public void ScrambleNegativeDecimal()
        {
            decimal scrambledNumber = MasterDataScrambler.ScrambleNumber(-888474.008074731m);
            Assert.AreEqual(-141333.006541403M, scrambledNumber);
        }

        [TestMethod]
        public void ScrambleMinDecimal()
        {
            decimal scrambledNumber = MasterDataScrambler.ScrambleNumber(decimal.MinValue / 10m);
            Assert.AreEqual(-1508751857845512916892639974M, scrambledNumber, "The scrambling of the min allowed decimal number has failed.");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ScrambleDecimalSmallerThanSupported()
        {
            decimal scrambledNumber = MasterDataScrambler.ScrambleNumber(decimal.MinValue);
        }

        [TestMethod]
        public void ScrambleNullableDecimal()
        {
            decimal number = 1234.5678m;
            decimal? nullableNumber = number;

            decimal scrambledNumber = MasterDataScrambler.ScrambleNumber(number);
            decimal? scrambledNullableNumber = MasterDataScrambler.ScrambleNumber(nullableNumber);

            Assert.AreEqual(scrambledNumber, scrambledNullableNumber, "Scrambling nullable decimal yielded a different value than scrambling the same number as non-nullable.");
        }

        [TestMethod]
        public void ScrambleZeroAsDecimal()
        {
            decimal scrambledNumber = MasterDataScrambler.ScrambleNumber(0m);
            Assert.AreEqual(0m, scrambledNumber, "Scrambling decimal zero yielded non-zero number.");
        }

        [TestMethod]
        public void ScrambleNullDecimal()
        {
            decimal? number = null;
            decimal? scrambledNumber = MasterDataScrambler.ScrambleNumber(number);
            Assert.IsNull(scrambledNumber, "Scrambling null decimal yielded non-null.");
        }

        [TestMethod]
        public void ScramblePositiveInteger()
        {
            int scrambledNumber = MasterDataScrambler.ScrambleNumber(125083600);
            Assert.AreEqual(253089600, scrambledNumber, "Integer number scrambling has failed.");
        }

        [TestMethod]
        public void ScrambleMaxSupportedInteger()
        {
            int scrambledNumber = MasterDataScrambler.ScrambleNumber(MasterDataScrambler.MaxSupportedInt);
            Assert.AreEqual(1000000006, scrambledNumber, "Integer number scrambling has failed.");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ScrambleIntegerLargerThanMaxSupported()
        {
            int scrambledNumber = MasterDataScrambler.ScrambleNumber(int.MaxValue);
        }

        [TestMethod]
        public void ScrambleZeroAsInteger()
        {
            int scrambledNumber = MasterDataScrambler.ScrambleNumber(0);
            Assert.AreEqual(0, scrambledNumber, "Scrambling zero as integer has failed.");
        }

        [TestMethod]
        public void ScrambleNegativeInteger()
        {
            int scrambledNumber = MasterDataScrambler.ScrambleNumber(-17568);
            Assert.AreEqual(-32846, scrambledNumber, "Negative integer number scrambling has failed.");
        }

        [TestMethod]
        public void ScrambleMinSupportedInteger()
        {
            int scrambledNumber = MasterDataScrambler.ScrambleNumber(MasterDataScrambler.MinSupportedInt);
            Assert.AreEqual(-777777777, scrambledNumber, "Integer number scrambling has failed.");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ScrambleIntegerLessThanTheMinSupportedValue()
        {
            int scrambledNumber = MasterDataScrambler.ScrambleNumber(MasterDataScrambler.MinSupportedInt - 1);
        }

        [TestMethod]
        public void ScrambleText()
        {
            string text = "abcîddgh";

            string scrambledText = MasterDataScrambler.ScrambleText(text);

            byte[] textBytes = System.Text.Encoding.Unicode.GetBytes(text);
            byte[] scrambledTextBytes = System.Text.Encoding.Unicode.GetBytes(scrambledText);
            for (int i = 0; i < scrambledTextBytes.Length; i++)
            {
                var crtByte = scrambledTextBytes[i];
                var actualValue = crtByte >= 128 ? crtByte - 128 : crtByte + 128;
                Assert.AreEqual(textBytes[i], actualValue, "The scrambled text was different from the original text.");
            }
        }

        [TestMethod]
        public void ScrambleNullText()
        {
            string text = null;
            string scrambledText = MasterDataScrambler.ScrambleText(text);

            Assert.IsNull(scrambledText, "The scrambling of null text resulted in non-null value.");
        }

        [TestMethod]
        public void ScrambleEmptyText()
        {
            string text = string.Empty;
            string scrambledText = MasterDataScrambler.ScrambleText(text);

            Assert.AreEqual(string.Empty, scrambledText, "The scrambling of empty text resulted in non-empty value.");
        }

        [TestMethod]
        public void UnscramblePositiveDecimal()
        {
            decimal unscrambledNumber = MasterDataScrambler.UnscrambleNumber(28400.00031777m);
            Assert.AreEqual(75600.00099948m, unscrambledNumber, "Decimal number unscrambling has failed.");
        }

        [TestMethod]
        public void UnscramblePositiveDecimalWithMaximumDecimalPlaces()
        {
            decimal unscrambledNumber = MasterDataScrambler.UnscrambleNumber(0.8582963527081593421376982453M);
            Assert.AreEqual(0.8376521984678234509638126535M, unscrambledNumber, "The unscrambling of a decimal number yielded an unexpected result.");
        }

        [TestMethod]
        public void UnscrambleNegativeDecimal()
        {
            decimal unscrambledNumber = MasterDataScrambler.UnscrambleNumber(-141333.006541403M);
            Assert.AreEqual(-888474.008074731M, unscrambledNumber, "Unscrambling a negative decimal failed.");
        }

        [TestMethod]
        public void UnscrambleNullableDecimal()
        {
            decimal scrambledNumber = 1596.3428M;
            decimal? scrambledNullableNumber = scrambledNumber;

            decimal unscrambledNumber = MasterDataScrambler.UnscrambleNumber(scrambledNumber);
            decimal? unscrambledNullableNumber = MasterDataScrambler.UnscrambleNumber(scrambledNullableNumber);

            Assert.AreEqual(unscrambledNumber, unscrambledNullableNumber, "Unscrambling a nullable decimal yielded a different value than unscrambling the same number as non-nullable.");
        }

        [TestMethod]
        public void UnscrambleNullDecimal()
        {
            decimal? number = null;
            decimal? unscrambledNumber = MasterDataScrambler.UnscrambleNumber(number);
            Assert.IsNull(unscrambledNumber, "Unscrambling null decimal yielded non-null.");
        }

        [TestMethod]
        public void UnscramblePositiveInteger()
        {
            int unscrambledNumber = MasterDataScrambler.UnscrambleNumber(253089600);
            Assert.AreEqual(125083600, unscrambledNumber, "Integer number unscrambling has failed.");
        }

        [TestMethod]
        public void UnscrambleNegativeInteger()
        {
            int unscrambledNumber = MasterDataScrambler.UnscrambleNumber(-32846);
            Assert.AreEqual(-17568, unscrambledNumber, "Negative integer number unscrambling has failed.");
        }

        [TestMethod]
        public void UnscrambleText()
        {
            string text = "abî;cdşdgh";

            string scrambledText = MasterDataScrambler.ScrambleText(text);
            string unscrambledText = MasterDataScrambler.UnscrambleText(scrambledText);

            Assert.AreEqual(text, unscrambledText, "Text unscrambling failed.");
        }

        [TestMethod]
        public void UnscrambleNullText()
        {
            string text = null;
            string unscrambledText = MasterDataScrambler.UnscrambleText(text);

            Assert.IsNull(unscrambledText, "The unscrambling of null text resulted in non-null value.");
        }

        [TestMethod]
        public void UnscrambleEmptyText()
        {
            string text = string.Empty;
            string unscrambledText = MasterDataScrambler.UnscrambleText(text);

            Assert.AreEqual(string.Empty, unscrambledText, "The unscrambling of empty text resulted in non-empty value.");
        }

        /// <summary>
        /// Tests that scrambling the negative value of a number yields the negative value of the scrambling of the positive number.
        /// </summary>
        [TestMethod]
        public void CompareScramblingOfPositiveAndNegativeValueOfADecimal()
        {
            decimal number = -888474.008074731m;
            decimal scrambledNumber = MasterDataScrambler.ScrambleNumber(number);
            decimal scrambledPositiveNumber = MasterDataScrambler.ScrambleNumber(number * -1m);

            Assert.AreEqual(scrambledNumber, scrambledPositiveNumber * -1m, "The scrambling of positive and negative decimal number gave different results.");
        }

        /// <summary>
        /// Compares the scrambling same integer as positive and negative.
        /// The scrambling of the positive should be the scrambling of the negative times minus one.
        /// </summary>
        [TestMethod]
        public void CompareScramblingSameIntegerAsPositiveAndNegative()
        {
            int negativeNumber = -17568;
            int scrambledNegative = MasterDataScrambler.ScrambleNumber(negativeNumber);
            int scrambledPositive = MasterDataScrambler.ScrambleNumber(negativeNumber * -1);

            Assert.AreEqual(scrambledNegative, scrambledPositive * -1, "Negative integer number scrambling has failed.");
        }
    }
}
