﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ZPKTool.Data.Tests
{
    [TestClass]
    public class EntityUtilsTest
    {
        [TestMethod]
        public void GetParentProjectForAssemblyWithoutProject()
        {
            var assy = new Assembly();

            var project = EntityUtils.GetParentProject(assy);
            Assert.IsNull(project, "The assembly should not have a parent project.");

            var parentAssy = new Assembly();
            assy.ParentAssembly = parentAssy;

            var project2 = EntityUtils.GetParentProject(assy);
            Assert.IsNull(project2, "The assembly should not have a parent project.");
        }

        [TestMethod]
        public void GetParentProjectForAssemblyWithProject()
        {
            var parentProject = new Project();
            var assy = new Assembly();
            assy.Project = parentProject;

            var project = EntityUtils.GetParentProject(assy);
            Assert.AreEqual(parentProject, project, "The assembly's parent project was not the correct one.");

            assy = new Assembly();
            var parentAssy = new Assembly();
            assy.ParentAssembly = parentAssy;
            parentAssy.ParentAssembly = new Assembly();
            parentAssy.ParentAssembly.Project = parentProject;

            var project2 = EntityUtils.GetParentProject(assy);
            Assert.AreEqual(parentProject, project2, "The assembly' parent project was not the correct one.");
        }

        [TestMethod]
        public void GetParentProjectForNullAssembly()
        {
            Assembly assy = null;

            var project = EntityUtils.GetParentProject(assy);
            Assert.IsNull(project, "A null assembly should not have a parent project.");
        }

        [TestMethod]
        public void GetParentProjectForPartWithoutProject()
        {
            var part = new Part();

            var project = EntityUtils.GetParentProject(part);
            Assert.IsNull(project, "The part should not have a parent project.");

            var parentAssy = new Assembly();
            part.Assembly = parentAssy;

            var project2 = EntityUtils.GetParentProject(part);
            Assert.IsNull(project2, "The part should not have a parent project.");
        }

        [TestMethod]
        public void GetParentProjectForPartWithProject()
        {
            var parentProject = new Project();
            var part = new Part();
            part.Project = parentProject;

            var project = EntityUtils.GetParentProject(part);
            Assert.AreEqual(parentProject, project, "The part's parent project was not the correct one.");

            var parentAssy = new Assembly();
            parentAssy.Project = parentProject;
            part = new Part();
            part.Assembly = parentAssy;

            var project2 = EntityUtils.GetParentProject(part);
            Assert.AreEqual(parentProject, project2, "The part's parent project was not the correct one.");
        }

        [TestMethod]
        public void GetParentProjectForNullPart()
        {
            Part part = null;
            var project = EntityUtils.GetParentProject(part);
            Assert.IsNull(project, "A null part should not have a parent project.");
        }

        [TestMethod]
        public void GetTopAssemblyForNullAssembly()
        {
            Assembly assy = null;
            var parent = EntityUtils.GetTopAssembly(assy);
            Assert.IsNull(parent, "A null assembly should not have a top assembly.");
        }

        [TestMethod]
        public void GetTopAssemblyForAssemblyInProject()
        {
            Assembly assy = new Assembly();
            assy.Project = new Project();

            var parent = EntityUtils.GetTopAssembly(assy);
            Assert.AreEqual(assy, parent, "A project-level assembly should be its own top assembly.");
        }

        [TestMethod]
        public void GetTopAssemblyForSubAssembly()
        {
            var assy = new Assembly();
            var topAssy = new Assembly();
            assy.ParentAssembly = new Assembly();
            assy.ParentAssembly.ParentAssembly = new Assembly();
            assy.ParentAssembly.ParentAssembly.ParentAssembly = new Assembly();
            assy.ParentAssembly.ParentAssembly.ParentAssembly.ParentAssembly = topAssy;

            var parent = EntityUtils.GetTopAssembly(assy);
            Assert.AreEqual(topAssy, parent, "The top level assembly was wrong.");
        }

        [TestMethod]
        public void GetSubAssemblyTest()
        {
            var assy = new Assembly();
            var parentAssy = new Assembly();

            var subAssy1 = new Assembly();
            var subAssy2 = new Assembly();
            var subAssy3 = new Assembly();
            subAssy1.Subassemblies.Add(subAssy2);
            subAssy2.Subassemblies.Add(subAssy3);
            subAssy3.Subassemblies.Add(assy);
            parentAssy.Subassemblies.Add(subAssy1);

            var result = EntityUtils.GetSubAssembly(parentAssy, assy.Guid);
            Assert.AreEqual(assy, result, "The sub-assembly was not found.");
        }

        [TestMethod]
        public void GetInexistentSubAssemblyTest()
        {
            var assy = new Assembly();
            var parentAssy = new Assembly();

            var subAssy1 = new Assembly();
            var subAssy2 = new Assembly();
            var subAssy3 = new Assembly();
            subAssy1.Subassemblies.Add(subAssy2);
            subAssy2.Subassemblies.Add(subAssy3);
            subAssy3.Subassemblies.Add(assy);
            parentAssy.Subassemblies.Add(subAssy1);

            var result = EntityUtils.GetSubAssembly(parentAssy, Guid.NewGuid());
            Assert.IsNull(result, "The inexistent sub-assembly was found.");
        }

        [TestMethod]
        public void IsMasterDataTest()
        {
            int obj1 = 1;
            Assert.IsFalse(EntityUtils.IsMasterData(obj1));

            string obj2 = "aaa";
            Assert.IsFalse(EntityUtils.IsMasterData(obj2));

            Assembly assy = new Assembly();
            assy.IsMasterData = false;
            Assert.IsFalse(EntityUtils.IsMasterData(assy));

            var project = new Project();            
            Assert.IsFalse(EntityUtils.IsMasterData(project));

            var part = new Part();
            part.IsMasterData = true;
            Assert.IsTrue(EntityUtils.IsMasterData(part));
        }
    }
}
