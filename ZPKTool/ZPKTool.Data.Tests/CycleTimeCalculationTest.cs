﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ZPKTool.Data.Tests
{
    [TestClass]
    public class CycleTimeCalculationTest
    {
        /// <summary>
        /// Tests that the CycleTimeCalculation.MachiningCalculationData property is copied by value not by reference.
        /// </summary>
        [TestMethod]
        public void MachiningCalculationDataCopyTest()
        {
            CycleTimeCalculation calculation = new CycleTimeCalculation();            
            calculation.MachiningCalculationData = new byte[5] { 1, 0, 0, 1, 1 };

            var copy = new CycleTimeCalculation();
            calculation.CopyValuesTo(copy);

            Assert.AreNotEqual(calculation.MachiningCalculationData, copy.MachiningCalculationData, "The CycleTimeCalculation's MachiningCalculationData was copied by reference, not by value.");
        }

        /// <summary>
        /// Tests that the CycleTimeCalculation.MachiningCalculationData property assignment is by reference and not by value.
        /// </summary>
        [TestMethod]
        public void MachiningCalculationDataAssignmentTest()
        {            
            var data = new byte[5] { 1, 0, 0, 1, 1 };

            CycleTimeCalculation calculation = new CycleTimeCalculation();
            calculation.MachiningCalculationData = data;

            Assert.AreEqual(data, calculation.MachiningCalculationData, "The CycleTimeCalculation's MachiningCalculationData was assigned by value, not by reference.");
        }
    }
}
