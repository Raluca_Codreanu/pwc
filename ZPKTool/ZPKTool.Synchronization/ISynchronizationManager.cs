﻿namespace ZPKTool.Synchronization
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The interface of a service that handles the user data synchronization.
    /// </summary>
    public interface ISynchronizationManager
    {
        #region Properties

        /// <summary>
        /// Gets the synchronization current status.
        /// </summary>
        SyncStatus CurrentStatus { get; }

        /// <summary>
        /// Gets the synchronization conflicts.
        /// </summary>        
        ICollection<SynchronizationConflict> Conflicts { get; }

        #endregion Properties

        #region Events

        /// <summary>
        /// Occurs durring the synchronization process progress.
        /// </summary>
        event SynchronizationProgressHandler Progress;

        #endregion Events

        #region Methods

        /// <summary>
        /// Initializes the scopes.
        /// </summary>
        /// <param name="userGuid">The user GUID.</param>
        /// <param name="keepTrackOfSuccessfullyMaintainedUser">if set to <c>true</c> keeps track of successfully maintained user.</param>
        void InitializeScopesAndConvert(Guid userGuid, bool keepTrackOfSuccessfullyMaintainedUser);

        /// <summary>
        /// Determines whether a synchronization scope(s) update is necessary.
        /// </summary>
        /// <param name="userGuid">The user ID for which to perform the check.</param>
        /// <returns>
        /// true if a scope update is necessary; otherwise, false.
        /// </returns>
        bool IsSyncScopeUpdateNecessary(Guid userGuid);

        /// <summary>
        /// Starts a synchronization session.
        /// </summary>
        /// <param name="syncStaticDataAndSettings">if set to true the static data and settings should be synced.</param>
        /// <param name="syncReleasedProjects">if set to true the released projects should be synced.</param>
        /// <param name="syncMasterData">if set to true the master data should be synced.</param>
        /// <param name="syncMyProjects">if set to true my projects should be synced.</param>
        /// <param name="userGuid">The ID of the user whose projects (my projects) should be synced.</param>
        /// <param name="resolveConflicts">if set to true, this call will resolve any conflicts that appeared in previous runs and stored in the Conflicts property.</param>
        /// <exception cref="ZPKTool.Synchronization.SynchronizationException">An error occurred during synchronization.</exception>
        void Synchronize(
            bool syncStaticDataAndSettings,
            bool syncReleasedProjects,
            bool syncMasterData,
            bool syncMyProjects,
            Guid userGuid,
            bool resolveConflicts);

        /// <summary>
        /// Starts the synchronization process with the last settings for data to be sync and resolves the conflicts; the method actually restarts the last sync, to resolve it's conflicts.
        /// </summary>
        /// <param name="userGuid">The ID of the user whose projects (my projects) should be synced.</param>
        void ResynchronizeAndResolveConflicts(Guid userGuid);

        /// <summary>
        /// Starts  the Synchronization process.
        /// </summary>
        /// <param name="syncType">Type of the synchronization.</param>
        /// <param name="userGuid">The user GUID.</param>
        /// <param name="direction">The synchronization direction.</param>
        /// <param name="notifyProgress">A value indicating whether to notify the progress when sync is finished, or not.</param>
        /// <exception cref="SynchronizationException">Synchronization Exception</exception>        
        void Synchronize(SyncType syncType, Guid userGuid, SyncDirection direction, bool notifyProgress);

        /// <summary>
        /// Aborts the synchronization.
        /// </summary>
        void AbortSynchronization();

        /// <summary>
        /// Resets the current status.
        /// </summary>
        void ResetCurrentStatus();

        #endregion Methods
    }
}
