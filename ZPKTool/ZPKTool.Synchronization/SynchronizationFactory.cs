﻿namespace ZPKTool.Synchronization
{
    using System.IO;

    /// <summary>
    /// Creates synchronization managers.
    /// </summary>
    public static class SynchronizationFactory
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The only instance of this synchronization manager.
        /// </summary>
        private static ISynchronizationManager syncManager;

        #endregion Attributes

        #region Public Methods

        /// <summary>
        /// Initializes the static members of the <see cref="SynchronizationFactory"/> class.
        /// </summary>
        /// <returns>Synchronization Manager.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "This method will violate the rule, but should not be a property. The method throws an exception if the SynchronizationManager can not be created")]
        public static ISynchronizationManager GetSyncManager()
        {
            if (syncManager == null)
            {
                try
                {
                    syncManager = new SynchronizationManager();
                }
                catch (FileNotFoundException ex)
                {
                    Log.ErrorException("Sync framework initialization error. The sync framework redistributable package may not be installed correctly.", ex);
                    throw new SynchronizationException(SyncErrorCodes.SyncFrameworkNotInstalled, ex);
                }
            }

            return syncManager;
        }

        /// <summary>
        /// Resets the synchronize manager.
        /// </summary>
        public static void ResetSyncManager()
        {
            syncManager.ResetCurrentStatus();
        }

        #endregion Public Methods
    }
}
