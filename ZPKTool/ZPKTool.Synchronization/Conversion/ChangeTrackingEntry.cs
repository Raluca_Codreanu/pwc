﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;

namespace ZPKTool.Synchronization
{
    /// <summary>
    /// Specifies the entities for which synchronization is implemented.
    /// Each member of this enumeration must have the same name as the class  of the entity it represents.
    /// DO NOT change the current order of the elements.
    /// </summary>
    [SuppressMessage("Microsoft.Design", "CA1027:MarkEnumsWithFlags", Justification = "Values can not be combined.")]
    public enum SynchOldChangeTrackingEntityType
    {
        /// <summary>
        /// Change tracking is not necessary.
        /// </summary>
        NotTracked = 0,

        /// <summary>
        /// User type synchronization
        /// </summary>
        Users = 1,

        /// <summary>
        /// Role type synchronization
        /// </summary>
        Roles = 2,

        /// <summary>
        /// AccessRight type synchronization
        /// </summary>
        AccessRights = 3,

        /// <summary>
        /// Country type synchronization
        /// </summary>
        Countries = 4,

        /// <summary>
        /// CountryState type synchronization
        /// </summary>
        CountryStates = 5,

        /// <summary>
        /// CountrySetting type synchronization
        /// </summary>
        CountrySettings = 6,

        /// <summary>
        /// CountrySettingsValueHistory type synchronization
        /// </summary>
        CountrySettingsValueHistory = 7,

        /// <summary>
        /// Currency type synchronization
        /// </summary>
        Currencies = 8,

        /// <summary>
        /// MeasurementUnit type synchronization
        /// </summary>
        MeasurementUnits = 9,

        /// <summary>
        /// OverheadSetting type synchronization
        /// </summary>
        OverheadSettings = 10,

        /// <summary>
        /// Media type synchronization
        /// </summary>
        Media = 11,

        /// <summary>
        /// Project type synchronization
        /// </summary>
        Projects = 12,

        /// <summary>
        /// Supplier type synchronization
        /// </summary>
        Customers = 13,

        /// <summary>
        /// Assembly type synchronization
        /// </summary>
        Assemblies = 14,

        /// <summary>
        /// RawMaterialDeliveryType type synchronization
        /// </summary>
        RawMaterialDeliveryTypes = 15,

        /// <summary>
        /// MaterialsClassification type synchronization
        /// </summary>
        MaterialsClassification = 16,

        /// <summary>
        /// Part type synchronization
        /// </summary>
        Parts = 17,

        /// <summary>
        /// Manufacturer type synchronization
        /// </summary>
        Manufacturers = 18,

        /// <summary>
        /// RawMaterial type synchronization
        /// </summary>
        RawMaterials = 19,

        /// <summary>
        /// RawMaterialsPriceHistory type synchronization
        /// </summary>
        RawMaterialsPriceHistory = 20,

        /// <summary>
        /// Consumable type synchronization
        /// </summary>
        Consumables = 21,

        /// <summary>
        /// ConsumablesPriceHistory type synchronization
        /// </summary>
        ConsumablesPriceHistory = 22,

        /// <summary>
        /// Commodity type synchronization
        /// </summary>
        Commodities = 23,

        /// <summary>
        /// CommoditiesPriceHistory type synchronization
        /// </summary>
        CommoditiesPriceHistory = 24,

        /// <summary>
        /// Die type synchronization
        /// </summary>
        Dies = 25,

        /// <summary>
        /// Machine type synchronization
        /// </summary>
        Machines = 26,

        /// <summary>
        /// MachinesClassification type synchronization
        /// </summary>
        MachinesClassification = 27,

        /// <summary>
        /// Process type synchronization
        /// </summary>
        Processes = 28,

        /// <summary>
        /// PartProcessStep type synchronization
        /// PartProcess Step = 29 , AssemblyProcessStep = 30
        /// </summary>
        ProcessSteps = 29,

        /// <summary>
        /// ProcessStepsClassification type synchronization
        /// </summary>
        ProcessStepsClassification = 31,

        /// <summary>
        /// ProcessStepPartAmount type synchronization
        /// </summary>
        ProcessStepPartAmounts = 32,

        /// <summary>
        /// ProcessStepAssemblyAmount type synchronization
        /// </summary>
        ProcessStepAssemblyAmounts = 33,

        /// <summary>
        /// CycleTimeCalculation type synchronization
        /// </summary>
        CycleTimeCalculations = 34,

        /// <summary>
        /// TrashBinItem type synchronization
        /// </summary>
        TrashBinItems = 35,

        /// <summary>
        /// BasicSetting type synchronization
        /// </summary>
        BasicSettings = 36,

        /// <summary>
        /// ProjectFolder type synchronization
        /// </summary>
        ProjectFolders = 37,

        /// <summary>
        /// Bookmarks type synchronization
        /// </summary>
        Bookmarks = 28,

        /// <summary>
        /// Transport cost calculations synchronization
        /// </summary>
        TransportCostCalculations = 38,

        /// <summary>
        /// Transport cost calculation settings synchronization
        /// </summary>
        TransportCostCalculationSettings = 39
    }

    /// <summary>
    /// Change Tracking Entry class.
    /// </summary>
    public class ChangeTrackingEntry
    {
        /// <summary>
        /// Gets or sets the GUID.
        /// </summary>
        /// <value>
        /// The GUID.
        /// </value>
        public Guid Guid { get; set; }

        /// <summary>
        /// Gets or sets the type of the change.
        /// </summary>
        /// <value>
        /// The type of the change.
        /// </value>
        public int ChangeType { get; set; }

        /// <summary>
        /// Gets or sets the timestamp.
        /// </summary>
        /// <value>
        /// The timestamp.
        /// </value>
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the original timestamp.
        /// </summary>
        /// <value>
        /// The original timestamp.
        /// </value>
        public DateTime OriginalTimestamp { get; set; }

        /// <summary>
        /// Gets or sets the entity GUID.
        /// </summary>
        /// <value>
        /// The entity GUID.
        /// </value>
        public Guid EntityGuid { get; set; }

        /// <summary>
        /// Gets or sets the type of the entity.
        /// </summary>
        /// <value>
        /// The type of the entity.
        /// </value>
        public int EntityType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [entity is master data].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [entity is master data]; otherwise, <c>false</c>.
        /// </value>
        public bool EntityIsMasterData { get; set; }

        /// <summary>
        /// Gets or sets the entity parent GUID.
        /// </summary>
        /// <value>
        /// The entity parent GUID.
        /// </value>
        public Guid EntityParentGuid { get; set; }

        /// <summary>
        /// Gets or sets the type of the entity parent.
        /// </summary>
        /// <value>
        /// The type of the entity parent.
        /// </value>
        public int EntityParentType { get; set; }

        /// <summary>
        /// Gets or sets the entity owner GUID.
        /// </summary>
        /// <value>
        /// The entity owner GUID.
        /// </value>
        public Guid EntityOwnerGuid { get; set; }
    }
}
