﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Microsoft.Synchronization;
using ZPKTool.Common;

namespace ZPKTool.Synchronization
{
    /// <summary>
    /// The class is responsible with the conversion of old change tracking data to the new sync framework change tracking data.
    /// </summary>
    public static class ConvertChangeTrackings
    {
        #region Membrers

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        #endregion

        #region Convert

        /// <summary>
        /// Converts the specified XML configuration.
        /// </summary>
        /// <param name="scopeNameLocal">The scope name local.</param>
        /// <param name="syncType">Type of the sync.</param>
        /// <param name="userGuid">The user GUID.</param>
        /// <param name="scopeNameCentral">The scope name central.</param>
        /// <param name="sqlConnStringLocal">The SQL connection string local.</param>
        /// <param name="sqlConnStringCentral">The SQL connection string central.</param>
        [SuppressMessage("Microsoft.Performance", "CA1809:AvoidExcessiveLocals", Justification = "Legacy code that is too complicated to be refactored and will be removed in later versions.")]
        [SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Justification = "Legacy code that is too complicated to be refactored and will be removed in later versions.")]
        public static void Convert(string scopeNameLocal, SyncType syncType, Guid userGuid, string scopeNameCentral, string sqlConnStringLocal, string sqlConnStringCentral)
        {
            try
            {
                if (syncType == SyncType.MasterData ||
                    syncType == SyncType.MyProjects ||
                    syncType == SyncType.StaticDataAndSettings ||
                    syncType == SyncType.ReleasedProjects)
                {
                    // Convert old change tracking to new one's.
                    string oldConfigLocal = SynchronizationManager.GetSqlProviderConfiguration(sqlConnStringLocal, scopeNameLocal);
                    string oldConfigCentral = SynchronizationManager.GetSqlProviderConfiguration(sqlConnStringCentral, scopeNameCentral);

                    if (string.IsNullOrEmpty(oldConfigLocal) || string.IsNullOrEmpty(oldConfigCentral))
                    {
                        return;
                    }

                    XDocument configXmlLocal = XDocument.Parse(oldConfigLocal);
                    XDocument configXmlCentral = XDocument.Parse(oldConfigCentral);

                    int scopeLocalId = GetScopeLocalId(scopeNameLocal, sqlConnStringLocal);
                    int scopeCentralId = GetScopeLocalId(scopeNameCentral, sqlConnStringCentral);

                    if (scopeLocalId == -1 || scopeCentralId == -1)
                    {
                        log.Error("Could not get the scope Id from the configuration file.");
                        return;
                    }

                    Task localTask = null;
                    Task centralTask = null;

                    try
                    {
                        using (SqlConnection connectionLocal = new SqlConnection(sqlConnStringLocal))
                        {
                            connectionLocal.Open();

                            using (SqlConnection connectionCentral = new SqlConnection(sqlConnStringCentral))
                            {
                                connectionCentral.Open();

                                List<XElement> localElements = configXmlLocal.Descendants("Adapter").ToList();
                                List<XElement> centralElements = configXmlCentral.Descendants("Adapter").ToList();

                                if (localElements.Count != centralElements.Count)
                                {
                                    log.Error("The local scope configuration and central scope configuration xml doesn't match their number of items.");
                                    return;
                                }
                                else
                                {
                                    for (int index = 0; index < localElements.Count; index++)
                                    {
                                        string tableNameLocal = localElements[index].Attribute("Name").Value;
                                        string tableNameCentral = centralElements[index].Attribute("Name").Value;

                                        tableNameLocal = tableNameLocal.Substring(1, tableNameLocal.Length - 2);
                                        tableNameCentral = tableNameCentral.Substring(1, tableNameCentral.Length - 2);

                                        string insertMetaProcLocal = localElements[index].Attribute("InsMetaProc").Value;
                                        string insertMetaProcCentral = centralElements[index].Attribute("InsMetaProc").Value;

                                        insertMetaProcLocal = insertMetaProcLocal.Substring(1, insertMetaProcLocal.Length - 2);
                                        insertMetaProcCentral = insertMetaProcCentral.Substring(1, insertMetaProcCentral.Length - 2);

                                        string updateMetaProcLocal = localElements[index].Attribute("UpdMetaProc").Value;
                                        string updateMetaProcCentral = centralElements[index].Attribute("UpdMetaProc").Value;

                                        updateMetaProcLocal = updateMetaProcLocal.Substring(1, updateMetaProcLocal.Length - 2);
                                        updateMetaProcCentral = updateMetaProcCentral.Substring(1, updateMetaProcCentral.Length - 2);

                                        string changeTrackingTableNameLocal = localElements[index].Attribute("TrackingTable").Value;
                                        string changeTrackingTableNameCentral = centralElements[index].Attribute("TrackingTable").Value;

                                        changeTrackingTableNameLocal = changeTrackingTableNameLocal.Substring(1, changeTrackingTableNameLocal.Length - 2);
                                        changeTrackingTableNameCentral = changeTrackingTableNameCentral.Substring(1, changeTrackingTableNameCentral.Length - 2);

                                        SynchOldChangeTrackingEntityType type;
                                        if (Enum.TryParse<SynchOldChangeTrackingEntityType>(tableNameLocal, out type) == false)
                                        {
                                            if (!(tableNameLocal.Equals("ProjectMedia") ||
                                                tableNameLocal.Equals("AssemblyMedia") ||
                                                tableNameLocal.Equals("PartMedia") ||
                                                tableNameLocal.Equals("Roles") ||
                                                tableNameLocal.Equals("RoleAccessRights") ||
                                                tableNameLocal.Equals("UserRoles")))
                                            {
                                                log.Error("Could not get the old change tracking type while converting the change tracking: {0}.", tableNameLocal);
                                            }

                                            continue;
                                        }

                                        string whereSqlCondition = "WHERE EntityType=" + (int)type;

                                        if (type == SynchOldChangeTrackingEntityType.ProcessSteps)
                                        {
                                            whereSqlCondition = "WHERE (EntityType=29 or EntityType=30)";
                                        }

                                        if (syncType == SyncType.MasterData)
                                        {
                                            whereSqlCondition += " and EntityIsMasterData='True' and EntityOwnerGuid is NULL";
                                        }
                                        else if (syncType == SyncType.MyProjects)
                                        {
                                            whereSqlCondition += " and EntityOwnerGuid='" + userGuid + "'";
                                        }
                                        else if (syncType == SyncType.StaticDataAndSettings)
                                        {
                                            whereSqlCondition += " and EntityIsMasterData='False' and EntityOwnerGuid is NULL";
                                        }
                                        else if (syncType == SyncType.ReleasedProjects)
                                        {
                                            whereSqlCondition += " and EntityIsMasterData='False' and EntityOwnerGuid is NULL";
                                        }

                                        List<ChangeTrackingEntry> changeTrackingsLocal = new List<ChangeTrackingEntry>();
                                        List<ChangeTrackingEntry> changeTrackingsCentral = new List<ChangeTrackingEntry>();
                                        string inGuidsLocal = string.Empty;
                                        string inGuidsCentral = string.Empty;

                                        inGuidsLocal = GetOldChanges(whereSqlCondition, ref changeTrackingsLocal, inGuidsLocal, connectionLocal);
                                        inGuidsCentral = GetOldChanges(whereSqlCondition, ref changeTrackingsCentral, inGuidsCentral, connectionCentral);

                                        List<Pair<string, string>> primaryKeyParameterNameLocal = (from elem in localElements[index].Descendants("Col")
                                                                                                   where elem.Attribute("pk") != null && elem.Attribute("pk").Value.Equals("true")
                                                                                                   select new Pair<string, string> { First = elem.Attribute("param").Value, Second = elem.Attribute("name").Value }).ToList();

                                        List<Pair<string, string>> primaryKeyParameterNameCentral = (from elem in centralElements[index].Descendants("Col")
                                                                                                     where elem.Attribute("pk") != null && elem.Attribute("pk").Value.Equals("true")
                                                                                                     select new Pair<string, string> { First = elem.Attribute("param").Value, Second = elem.Attribute("name").Value }).ToList();

                                        List<Guid> changesThatExistOnLocal = new List<Guid>();
                                        List<Guid> changesThatExistOnCentral = new List<Guid>();

                                        if (!(tableNameLocal.Equals("ProcessStepPartAmounts") || tableNameLocal.Equals("ProcessStepAssemblyAmounts")))
                                        {
                                            changesThatExistOnLocal = CheckIfChangeTrackingRowExist(inGuidsLocal, connectionLocal, changeTrackingTableNameLocal);
                                            changesThatExistOnCentral = CheckIfChangeTrackingRowExist(inGuidsCentral, connectionCentral, changeTrackingTableNameCentral);
                                        }

                                        var itemsUnion = changeTrackingsLocal.Select(l => l.EntityGuid)
                                                         .Union(changeTrackingsCentral.Select(s => s.EntityGuid))
                                                         .Distinct();

                                        List<Pair<ChangeTrackingEntry, ChangeTrackingEntry>> groupedChanges = (from item in itemsUnion
                                                                                                               join localc in changeTrackingsLocal on item equals localc.EntityGuid into jsc
                                                                                                               from loc in jsc.DefaultIfEmpty()
                                                                                                               join centralc in changeTrackingsCentral on item equals centralc.EntityGuid into jst
                                                                                                               from cen in jst.DefaultIfEmpty()
                                                                                                               select new Pair<ChangeTrackingEntry, ChangeTrackingEntry>(loc, cen)).ToList();

                                        using (SqlTransaction transactionLocal = connectionLocal.BeginTransaction())
                                        {
                                            using (SqlTransaction transactionCentral = connectionCentral.BeginTransaction())
                                            {
                                                if (syncType != SyncType.ReleasedProjects)
                                                {
                                                    foreach (Pair<ChangeTrackingEntry, ChangeTrackingEntry> pair in groupedChanges)
                                                    {
                                                        ChangeTrackingEntry localChange = pair.First;
                                                        ChangeTrackingEntry centralChange = pair.Second;

                                                        if (localChange != null && centralChange != null)
                                                        {
                                                            bool existsLocal = true;
                                                            bool existsCentral = true;

                                                            if (!(tableNameLocal.Equals("ProcessStepPartAmounts") || tableNameLocal.Equals("ProcessStepAssemblyAmounts")))
                                                            {
                                                                existsLocal = changesThatExistOnLocal.Contains(localChange.EntityGuid);
                                                                existsCentral = changesThatExistOnCentral.Contains(centralChange.EntityGuid);
                                                            }
                                                            else
                                                            {
                                                                if (localChange.ChangeType == 2)
                                                                {
                                                                    existsLocal = false;
                                                                }

                                                                if (centralChange.ChangeType == 2)
                                                                {
                                                                    existsCentral = false;
                                                                }
                                                            }

                                                            if (existsLocal == true && existsCentral == true)
                                                            {
                                                                if (!(syncType == SyncType.MasterData || syncType == SyncType.StaticDataAndSettings))
                                                                {
                                                                    ExecuteInsertUpdateDependingOnTimeStamps(
                                                                        existsLocal,
                                                                        existsCentral,
                                                                        updateMetaProcLocal,
                                                                        updateMetaProcCentral,
                                                                        syncType,
                                                                        sqlConnStringLocal,
                                                                        sqlConnStringCentral,
                                                                        configXmlLocal,
                                                                        configXmlCentral,
                                                                        scopeLocalId,
                                                                        scopeCentralId,
                                                                        connectionLocal,
                                                                        connectionCentral,
                                                                        tableNameLocal,
                                                                        tableNameCentral,
                                                                        primaryKeyParameterNameLocal,
                                                                        primaryKeyParameterNameCentral,
                                                                        transactionLocal,
                                                                        transactionCentral,
                                                                        localChange,
                                                                        centralChange);
                                                                }
                                                            }
                                                            else if (existsLocal == true && existsCentral == false)
                                                            {
                                                                ExecuteInsertUpdateDependingOnTimeStamps(
                                                                    existsLocal,
                                                                    existsCentral,
                                                                    updateMetaProcLocal,
                                                                    insertMetaProcCentral,
                                                                    syncType,
                                                                    sqlConnStringLocal,
                                                                    sqlConnStringCentral,
                                                                    configXmlLocal,
                                                                    configXmlCentral,
                                                                    scopeLocalId,
                                                                    scopeCentralId,
                                                                    connectionLocal,
                                                                    connectionCentral,
                                                                    tableNameLocal,
                                                                    tableNameCentral,
                                                                    primaryKeyParameterNameLocal,
                                                                    primaryKeyParameterNameCentral,
                                                                    transactionLocal,
                                                                    transactionCentral,
                                                                    localChange,
                                                                    centralChange);
                                                            }
                                                            else if (existsLocal == false && existsCentral == true)
                                                            {
                                                                if (!(syncType == SyncType.MasterData || syncType == SyncType.StaticDataAndSettings))
                                                                {
                                                                    ExecuteInsertUpdateDependingOnTimeStamps(
                                                                        existsLocal,
                                                                        existsCentral,
                                                                        insertMetaProcLocal,
                                                                        updateMetaProcCentral,
                                                                        syncType,
                                                                        sqlConnStringLocal,
                                                                        sqlConnStringCentral,
                                                                        configXmlLocal,
                                                                        configXmlCentral,
                                                                        scopeLocalId,
                                                                        scopeCentralId,
                                                                        connectionLocal,
                                                                        connectionCentral,
                                                                        tableNameLocal,
                                                                        tableNameCentral,
                                                                        primaryKeyParameterNameLocal,
                                                                        primaryKeyParameterNameCentral,
                                                                        transactionLocal,
                                                                        transactionCentral,
                                                                        localChange,
                                                                        centralChange);
                                                                }
                                                            }
                                                            else if (existsCentral == false && existsLocal == false)
                                                            {
                                                                if (!(syncType == SyncType.MasterData || syncType == SyncType.StaticDataAndSettings))
                                                                {
                                                                    ExecuteInsertUpdateDependingOnTimeStamps(
                                                                       existsLocal,
                                                                       existsCentral,
                                                                       insertMetaProcLocal,
                                                                       insertMetaProcCentral,
                                                                       syncType,
                                                                       sqlConnStringLocal,
                                                                       sqlConnStringCentral,
                                                                       configXmlLocal,
                                                                       configXmlCentral,
                                                                       scopeLocalId,
                                                                       scopeCentralId,
                                                                       connectionLocal,
                                                                       connectionCentral,
                                                                       tableNameLocal,
                                                                       tableNameCentral,
                                                                       primaryKeyParameterNameLocal,
                                                                       primaryKeyParameterNameCentral,
                                                                       transactionLocal,
                                                                       transactionCentral,
                                                                       localChange,
                                                                       centralChange);
                                                                }
                                                            }
                                                        }
                                                        else if (localChange == null)
                                                        {
                                                            bool existsCentral = changesThatExistOnCentral.Contains(centralChange.EntityGuid);

                                                            if (existsCentral == false)
                                                            {
                                                                ExecuteInsertUpdateMetadataStoredProcedure(
                                                                                                            true,
                                                                                                            null,
                                                                                                            null,
                                                                                                            insertMetaProcCentral,
                                                                                                            tableNameCentral,
                                                                                                            null,
                                                                                                            transactionCentral,
                                                                                                            primaryKeyParameterNameCentral,
                                                                                                            centralChange,
                                                                                                            connectionCentral,
                                                                                                            sqlConnStringCentral,
                                                                                                            sqlConnStringLocal);
                                                            }
                                                        }
                                                        else if (centralChange == null)
                                                        {
                                                            bool existsLocal = changesThatExistOnLocal.Contains(localChange.EntityGuid);

                                                            if (existsLocal == false)
                                                            {
                                                                ExecuteInsertUpdateMetadataStoredProcedure(
                                                                                                           true,
                                                                                                           null,
                                                                                                           null,
                                                                                                           insertMetaProcLocal,
                                                                                                           tableNameLocal,
                                                                                                           null,
                                                                                                           transactionLocal,
                                                                                                           primaryKeyParameterNameLocal,
                                                                                                           localChange,
                                                                                                           connectionLocal,
                                                                                                           sqlConnStringLocal,
                                                                                                           sqlConnStringCentral);
                                                            }
                                                        }
                                                    }
                                                }

                                                DeleteChangeTrackings(connectionLocal, inGuidsLocal, transactionLocal);
                                                DeleteChangeTrackings(connectionCentral, inGuidsCentral, transactionCentral);

                                                centralTask = Task.Factory.StartNew((Action)delegate
                                                {
                                                    transactionCentral.Commit();
                                                });

                                                localTask = Task.Factory.StartNew((Action)delegate
                                                {
                                                    transactionLocal.Commit();
                                                });

                                                Task.WaitAll(localTask, centralTask);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (AggregateException ex)
                    {
                        foreach (var e in ex.InnerExceptions)
                        {
                            log.ErrorException("Could not convert change tracking", e);
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                log.WarnException("Sync conversion error", ex);
                throw new SynchronizationException(SynchronizationException.ParseSqlException(ex), ex);
            }
            catch (SyncException ex)
            {
                log.ErrorException("Sync conversion error", ex);
                throw new SynchronizationException(SyncErrorCodes.InternalError, ex);
            }
        }

        #endregion

        #region Delete ChangeTrackings

        /// <summary>
        /// Deletes the change trackings.
        /// </summary>
        /// <param name="connectionLocal">The connection local.</param>
        /// <param name="inGuids">The in guids local.</param>
        /// <param name="transactionLocal">The transaction local.</param>
        [SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "Legacy code that is too complicated to be refactored and will be removed in later versions.")]
        private static void DeleteChangeTrackings(SqlConnection connectionLocal, string inGuids, SqlTransaction transactionLocal)
        {
            if (!string.IsNullOrEmpty(inGuids))
            {
                using (SqlCommand sqlCommand = new SqlCommand())
                {
                    inGuids = inGuids.Substring(1, inGuids.Length - 1);
                    inGuids = "( " + inGuids + " )";
                    sqlCommand.CommandText = "DELETE FROM ChangeTrackingData WHERE EntityGuid IN " + inGuids;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.Connection = connectionLocal;
                    sqlCommand.Transaction = transactionLocal;
                    sqlCommand.CommandTimeout = 90;
                    sqlCommand.ExecuteNonQuery();
                }
            }
        }

        #endregion

        #region Get scope id, old change tracking

        /// <summary>
        /// Gets the old changes.
        /// </summary>
        /// <param name="whereSqlCondition">The where SQL condition.</param>
        /// <param name="changeTrackings">The change trackings local.</param>
        /// <param name="inGuids">The in Guids local.</param>
        /// <param name="connection">The connection local.</param>
        /// <returns>A string that has a concatenation of Guids which are selected.</returns>
        [SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "Legacy code that is too complicated to be refactored and will be removed in later versions.")]
        private static string GetOldChanges(string whereSqlCondition, ref List<ChangeTrackingEntry> changeTrackings, string inGuids, SqlConnection connection)
        {
            try
            {
                using (SqlCommand sqlCommand = new SqlCommand())
                {
                    sqlCommand.CommandText = " IF OBJECT_ID('ChangeTrackingData', 'U') IS NOT NULL  SELECT Guid, EntityGuid, ChangeType, TimeStamp, OriginalTimestamp, EntityIsMasterData, EntityOwnerGuid, EntityParentType FROM ChangeTrackingData " + whereSqlCondition;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.Connection = connection;
                    sqlCommand.CommandTimeout = 90;

                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ChangeTrackingEntry entry = new ChangeTrackingEntry();

                            if (!reader.IsDBNull(0))
                            {
                                entry.Guid = reader.GetGuid(0);
                            }

                            if (!reader.IsDBNull(1))
                            {
                                entry.EntityGuid = reader.GetGuid(1);
                                inGuids += ",'" + entry.EntityGuid + "'";
                            }

                            if (!reader.IsDBNull(2))
                            {
                                entry.ChangeType = reader.GetInt16(2);
                            }

                            if (!reader.IsDBNull(3))
                            {
                                entry.Timestamp = reader.GetDateTime(3);
                            }

                            if (!reader.IsDBNull(4))
                            {
                                entry.OriginalTimestamp = reader.GetDateTime(4);
                            }

                            if (!reader.IsDBNull(5))
                            {
                                entry.EntityIsMasterData = reader.GetBoolean(5);
                            }

                            if (!reader.IsDBNull(6))
                            {
                                entry.EntityOwnerGuid = reader.GetGuid(6);
                            }

                            if (!reader.IsDBNull(7))
                            {
                                entry.EntityParentType = reader.GetInt16(7);
                            }

                            changeTrackings.Add(entry);
                        }
                    }
                }

                return inGuids;
            }
            catch (SqlException ex)
            {
                throw new SynchronizationException(SynchronizationException.ParseSqlException(ex), ex);
            }
        }

        /// <summary>
        /// Gets the scope local id.
        /// </summary>
        /// <param name="scopeName">Name of the scope.</param>
        /// <param name="sqlConnectionString">The SQL connection string.</param>
        /// <returns>The scope local id if succeeded, else -1.</returns>
        [SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "Legacy code that is too complicated to be refactored and will be removed in later versions.")]
        private static int GetScopeLocalId(string scopeName, string sqlConnectionString)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(sqlConnectionString))
                {
                    connection.Open();

                    using (SqlCommand sqlCommand = new SqlCommand())
                    {
                        string scopeLocalIdSqlQuery = "SELECT scope_info.scope_local_id FROM scope_info WHERE  scope_info.sync_scope_name = '" + scopeName + "'";
                        sqlCommand.CommandText = scopeLocalIdSqlQuery;
                        sqlCommand.CommandType = CommandType.Text;
                        sqlCommand.Connection = connection;
                        sqlCommand.CommandTimeout = 90;

                        using (SqlDataReader reader = sqlCommand.ExecuteReader())
                        {
                            reader.Read();
                            if (reader.HasRows)
                            {
                                return reader.GetInt32(0);
                            }
                        }
                    }
                }

                return -1;
            }
            catch (SqlException ex)
            {
                log.ErrorException("Could not get the local id for scope: " + scopeName, ex);
                return -1;
            }
        }

        #endregion

        #region Execute Insert/ update metadata

        /// <summary>
        /// Executes the insert update metadata store procedure.
        /// </summary>
        /// <param name="isInsert">if set to <c>true</c> [is insert].</param>
        /// <param name="createTimestamp">The create timestamp.</param>
        /// <param name="updateTimestamp">The update timestamp.</param>
        /// <param name="storedProcedureName">Name of the stored procedure.</param>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="scopeLocalId">The scope local id.</param>
        /// <param name="transaction">The transaction.</param>
        /// <param name="primarykeyParameterNamePair">The pk parameter name pair.</param>
        /// <param name="change">The change.</param>
        /// <param name="sqlConnection">The connection.</param>
        /// <param name="sqlConnectionString">The SQL connection string.</param>
        /// <param name="othersideSqlConnectionString">The otherside SQL connection string.</param>
        [SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Justification = "Legacy code that is too complicated to be refactored and will be removed in later versions.")]
        [SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "Legacy code that is too complicated to be refactored and will be removed in later versions.")]
        private static void ExecuteInsertUpdateMetadataStoredProcedure(bool isInsert, int? createTimestamp, int? updateTimestamp, string storedProcedureName, string tableName, int? scopeLocalId, SqlTransaction transaction, List<Pair<string, string>> primarykeyParameterNamePair, ChangeTrackingEntry change, SqlConnection sqlConnection, string sqlConnectionString, string othersideSqlConnectionString)
        {
            try
            {
                // my project's type change tracking with is masterdata flag set to true
                if (change.EntityOwnerGuid != Guid.Empty && change.EntityIsMasterData == true)
                {
                    change.EntityIsMasterData = false;
                }

                if (change.EntityGuid != Guid.Empty)
                {
                    List<Guid> paramGuids = new List<Guid>();

                    using (SqlCommand sqlCommand = new SqlCommand(storedProcedureName))
                    {
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.Transaction = transaction;
                        sqlCommand.Connection = sqlConnection;
                        sqlCommand.CommandTimeout = 90;

                        if (tableName.Equals("ProcessStepPartAmounts") || tableName.Equals("ProcessStepAssemblyAmounts"))
                        {
                            string query = "SELECT ";

                            foreach (Pair<string, string> pk in primarykeyParameterNamePair)
                            {
                                query += pk.Second + ", ";
                            }

                            query = query.Substring(0, query.Count() - 2);

                            query += " FROM " + tableName + " WHERE Guid='" + change.EntityGuid + "'";

                            paramGuids = new List<Guid>();

                            using (SqlConnection queryConnection = new SqlConnection(sqlConnectionString))
                            {
                                queryConnection.Open();

                                using (SqlCommand command = new SqlCommand())
                                {
                                    command.CommandText = query;
                                    command.CommandType = CommandType.Text;
                                    command.Connection = queryConnection;
                                    command.CommandTimeout = 90;

                                    using (SqlDataReader reader = command.ExecuteReader())
                                    {
                                        while (reader.Read())
                                        {
                                            paramGuids.Add(reader.GetGuid(0));
                                            paramGuids.Add(reader.GetGuid(1));
                                        }
                                    }
                                }
                            }

                            if (paramGuids.Count == primarykeyParameterNamePair.Count)
                            {
                                for (int index = 0; index < primarykeyParameterNamePair.Count(); index++)
                                {
                                    sqlCommand.Parameters.AddWithValue(primarykeyParameterNamePair[index].First, System.Data.SqlTypes.SqlGuid.Parse(paramGuids[index].ToString()));
                                }
                            }
                            else
                            {
                                using (SqlConnection otherSideQueryConnection = new SqlConnection(othersideSqlConnectionString))
                                {
                                    otherSideQueryConnection.Open();

                                    using (SqlCommand readGuidcommand = new SqlCommand())
                                    {
                                        readGuidcommand.CommandText = query;
                                        readGuidcommand.CommandType = CommandType.Text;
                                        readGuidcommand.Connection = otherSideQueryConnection;
                                        readGuidcommand.CommandTimeout = 90;

                                        using (SqlDataReader reader = readGuidcommand.ExecuteReader())
                                        {
                                            while (reader.Read())
                                            {
                                                paramGuids.Add(reader.GetGuid(0));
                                                paramGuids.Add(reader.GetGuid(1));
                                            }
                                        }
                                    }
                                }

                                if (paramGuids.Count == primarykeyParameterNamePair.Count)
                                {
                                    for (int index = 0; index < primarykeyParameterNamePair.Count(); index++)
                                    {
                                        System.Data.SqlTypes.SqlGuid sqlGuid = new System.Data.SqlTypes.SqlGuid(paramGuids[index]);
                                        sqlCommand.Parameters.AddWithValue(primarykeyParameterNamePair[index].First, sqlGuid);
                                    }
                                }
                                else
                                {
                                    return;
                                }
                            }
                        }
                        else
                        {
                            sqlCommand.Parameters.AddWithValue(primarykeyParameterNamePair.FirstOrDefault().First, System.Data.SqlTypes.SqlGuid.Parse(change.EntityGuid.ToString()));
                        }

                        if (scopeLocalId == null)
                        {
                            sqlCommand.Parameters.AddWithValue("@sync_scope_local_id", DBNull.Value);
                        }
                        else
                        {
                            sqlCommand.Parameters.AddWithValue("@sync_scope_local_id", scopeLocalId);
                        }

                        int isTombstone = 0;
                        if (change.ChangeType == 2)
                        {
                            isTombstone = 1;
                        }

                        sqlCommand.Parameters.AddWithValue("@sync_row_is_tombstone", isTombstone);

                        if (createTimestamp == null)
                        {
                            sqlCommand.Parameters.AddWithValue("@sync_create_peer_key", DBNull.Value);
                            sqlCommand.Parameters.AddWithValue("@sync_create_peer_timestamp", DBNull.Value);
                        }
                        else
                        {
                            sqlCommand.Parameters.AddWithValue("@sync_create_peer_key", createTimestamp);
                            sqlCommand.Parameters.AddWithValue("@sync_create_peer_timestamp", createTimestamp);
                        }

                        if (updateTimestamp == null)
                        {
                            sqlCommand.Parameters.AddWithValue("@sync_update_peer_key", DBNull.Value);
                            sqlCommand.Parameters.AddWithValue("@sync_update_peer_timestamp", DBNull.Value);
                        }
                        else
                        {
                            sqlCommand.Parameters.AddWithValue("@sync_update_peer_key", updateTimestamp);
                            sqlCommand.Parameters.AddWithValue("@sync_update_peer_timestamp", updateTimestamp);
                        }

                        int checkConcurency = 0;
                        sqlCommand.Parameters.AddWithValue("@sync_check_concurrency", checkConcurency);
                        sqlCommand.Parameters.AddWithValue("@sync_row_timestamp", DBNull.Value);
                        sqlCommand.Parameters.AddWithValue("@sync_row_count", DBNull.Value);

                        var returnParameter = sqlCommand.Parameters.Add("@return_value", SqlDbType.Int);
                        returnParameter.Direction = ParameterDirection.ReturnValue;

                        sqlCommand.ExecuteNonQuery();
                    }

                    if (isInsert)
                    {
                        using (SqlCommand sqlCommand = new SqlCommand())
                        {
                            string query = string.Empty;

                            if (tableName.Equals("BasicSettings") || tableName.Equals("PartMedia") || tableName.Equals("AssemblyMedia") || tableName.Equals("ProjectMedia") || tableName.Equals("CountryStates"))
                            {
                                return;
                            }
                            else if (tableName.Equals("Projects"))
                            {
                                if (change.EntityOwnerGuid == Guid.Empty)
                                {
                                    query = "UPDATE " + tableName + "_tracking" + " SET [IsReleased]='True' WHERE [Guid]='" + change.EntityGuid + "'";
                                }
                                else
                                {
                                    query = "UPDATE " + tableName + "_tracking" + " SET [OwnerGuid] ='" + change.EntityOwnerGuid + "',[IsReleased]='False' WHERE [Guid]='" + change.EntityGuid + "'";
                                }
                            }
                            else if (tableName.Equals("ProcessStepAssemblyAmounts") || tableName.Equals("ProcessStepPartAmounts"))
                            {
                                string whereClause = " WHERE ";

                                if (paramGuids != null)
                                {
                                    if (paramGuids.Count == primarykeyParameterNamePair.Count)
                                    {
                                        if (tableName.Equals("ProcessStepAssemblyAmounts"))
                                        {
                                            whereClause += "ProcessStepGuid='" + paramGuids[0].ToString() + "' and AssemblyGuid='" + paramGuids[1].ToString() + "'";
                                        }
                                        else if (tableName.Equals("ProcessStepPartAmounts"))
                                        {
                                            whereClause += "ProcessStepGuid='" + paramGuids[0].ToString() + "' and PartGuid='" + paramGuids[1].ToString() + "'";
                                        }
                                        else
                                        {
                                            return;
                                        }

                                        if (change.EntityOwnerGuid == Guid.Empty)
                                        {
                                            query = "UPDATE " + tableName + "_tracking" + " SET [IsMasterData] ='" + change.EntityIsMasterData + "', [Guid]='" + change.EntityGuid + "'";
                                            query += whereClause;
                                        }
                                        else
                                        {
                                            query = "UPDATE " + tableName + "_tracking" + " SET [OwnerGuid] ='" + change.EntityOwnerGuid + "', [IsMasterData] ='" + change.EntityIsMasterData + "', [Guid]='" + change.EntityGuid + "'";
                                            query += whereClause;
                                        }
                                    }
                                    else
                                    {
                                        return;
                                    }
                                }
                                else
                                {
                                    return;
                                }
                            }
                            else if (tableName.Equals("MachinesClassification") || tableName.Equals("MaterialsClassification") || tableName.Equals("ProcessStepsClassification"))
                            {
                                query = "UPDATE " + tableName + "_tracking" + " SET IsReleased='False' WHERE [Guid]='" + change.EntityGuid + "'";
                            }
                            else if (tableName.Equals("Currencies") || tableName.Equals("MeasurementUnits"))
                            {
                                query = "UPDATE " + tableName + "_tracking" + " SET IsReleased='False' WHERE [Guid]='" + change.EntityGuid + "'";
                            }
                            else if (tableName.Equals("Countries"))
                            {
                                query = "UPDATE " + tableName + "_tracking" + " SET IsReleased='False' WHERE [Guid]='" + change.EntityGuid + "'";
                            }
                            else if (tableName.Equals("CountrySettings") || tableName.Equals("ProjectFolders"))
                            {
                                if (change.EntityOwnerGuid == Guid.Empty)
                                {
                                    return;
                                }
                                else
                                {
                                    query = "UPDATE " + tableName + "_tracking" + " SET [OwnerGuid] ='" + change.EntityOwnerGuid + "' WHERE [Guid]='" + change.EntityGuid + "'";
                                }
                            }
                            else if (tableName.Equals("TrashBinItems"))
                            {
                                if (change.EntityOwnerGuid == Guid.Empty)
                                {
                                    return;
                                }
                                else
                                {
                                    query = "UPDATE " + tableName + "_tracking" + " SET [OwnerGuid] ='" + change.EntityOwnerGuid + "' WHERE [Guid]='" + change.EntityGuid + "'";
                                }
                            }
                            else
                            {
                                if (change.EntityOwnerGuid == Guid.Empty)
                                {
                                    query = "UPDATE " + tableName + "_tracking" + " SET [IsMasterData] ='" + change.EntityIsMasterData + "'" + " WHERE [Guid]='" + change.EntityGuid + "'";
                                }
                                else
                                {
                                    query = "UPDATE " + tableName + "_tracking" + " SET [OwnerGuid] ='" + change.EntityOwnerGuid + "', [IsMasterData] ='" + change.EntityIsMasterData + "'" + " WHERE [Guid]='" + change.EntityGuid + "'";
                                }
                            }

                            sqlCommand.CommandText = query;
                            sqlCommand.CommandType = CommandType.Text;
                            sqlCommand.Transaction = transaction;
                            sqlCommand.Connection = sqlConnection;
                            sqlCommand.CommandTimeout = 90;

                            sqlCommand.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                log.ErrorException("Could not execute insert update metadata for sync framework change tracking.", ex);
            }
        }

        /// <summary>
        /// Checks if change tracking row exists.
        /// </summary>
        /// <param name="inGuids">The where Sql clause Guids to search for.</param>
        /// <param name="connection">The connection.</param>
        /// <param name="changeTrackingTableName">Name of the change tracking table.</param>
        /// <returns>List of Guids for which there exists a row in the change tracking table.</returns>
        [SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "Legacy code that is too complicated to be refactored and will be removed in later versions.")]
        private static List<Guid> CheckIfChangeTrackingRowExist(string inGuids, SqlConnection connection, string changeTrackingTableName)
        {
            try
            {
                List<Guid> rowsExists = new List<Guid>();

                if (!string.IsNullOrEmpty(inGuids))
                {
                    using (SqlCommand sqlCommand = new SqlCommand())
                    {
                        inGuids = inGuids.Substring(1, inGuids.Length - 1);
                        inGuids = "( " + inGuids + " )";

                        sqlCommand.CommandText = "SELECT Guid FROM " + changeTrackingTableName + " WHERE Guid IN " + inGuids;

                        sqlCommand.CommandType = CommandType.Text;
                        sqlCommand.Connection = connection;
                        sqlCommand.CommandTimeout = 90;

                        using (SqlDataReader reader = sqlCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                rowsExists.Add(reader.GetGuid(0));
                            }
                        }
                    }
                }

                return rowsExists;
            }
            catch (SqlException ex)
            {
                log.WarnException("Could not execute insert update metadata for sync framework change tracking.", ex);
                throw new SynchronizationException(SynchronizationException.ParseSqlException(ex), ex);
            }
        }

        #endregion

        #region Insert update depending on timestamp

        /// <summary>
        /// Executes the insert update depending on time stamps.
        /// </summary>
        /// <param name="existsLocal">if set to <c>true</c> [exists local].</param>
        /// <param name="existsCentral">if set to <c>true</c> [exists central].</param>
        /// <param name="storedProcedureLocal">The stored procedure local.</param>
        /// <param name="storedProcedureCentral">The stored procedure central.</param>
        /// <param name="syncType">Type of the sync.</param>
        /// <param name="sqlConnStringLocal">The SQL conn string local.</param>
        /// <param name="sqlConnStringCentral">The SQL conn string central.</param>
        /// <param name="configXmlLocal">The config XML local.</param>
        /// <param name="configXmlCentral">The config XML central.</param>
        /// <param name="scopeLocalId">The scope local id.</param>
        /// <param name="scopeCentralId">The scope central id.</param>
        /// <param name="connectionLocal">The connection local.</param>
        /// <param name="connectionCentral">The connection central.</param>
        /// <param name="tableNameLocal">The table name local.</param>
        /// <param name="tableNameCentral">The table name central.</param>
        /// <param name="primaryKeyParameterNameLocal">The primary key parameter name local.</param>
        /// <param name="primaryKeyParameterNameCentral">The primary key parameter name central.</param>
        /// <param name="transactionLocal">The transaction local.</param>
        /// <param name="transactionCentral">The transaction central.</param>
        /// <param name="localChange">The local change.</param>
        /// <param name="centralChange">The central change.</param>
        [SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Justification = "Legacy code that is too complicated to be refactored and will be removed in later versions.")]
        private static void ExecuteInsertUpdateDependingOnTimeStamps(
            bool existsLocal,
            bool existsCentral,
            string storedProcedureLocal,
            string storedProcedureCentral,
            SyncType syncType,
            string sqlConnStringLocal,
            string sqlConnStringCentral,
            XDocument configXmlLocal,
            XDocument configXmlCentral,
            int scopeLocalId,
            int scopeCentralId,
            SqlConnection connectionLocal,
            SqlConnection connectionCentral,
            string tableNameLocal,
            string tableNameCentral,
            List<Pair<string, string>> primaryKeyParameterNameLocal,
            List<Pair<string, string>> primaryKeyParameterNameCentral,
            SqlTransaction transactionLocal,
            SqlTransaction transactionCentral,
            ChangeTrackingEntry localChange,
            ChangeTrackingEntry centralChange)
        {
            if (centralChange.Timestamp == localChange.OriginalTimestamp
                && localChange.Timestamp == localChange.OriginalTimestamp)
            {
                if (syncType == SyncType.MyProjects)
                {
                    ExecuteInsertUpdateMetadataStoredProcedure(!existsCentral, 1, 1, storedProcedureCentral, tableNameCentral, scopeCentralId, transactionCentral, primaryKeyParameterNameCentral, centralChange, connectionCentral, sqlConnStringCentral, sqlConnStringLocal);
                    ExecuteInsertUpdateMetadataStoredProcedure(!existsLocal, 1, 1, storedProcedureLocal, tableNameLocal, scopeLocalId, transactionLocal, primaryKeyParameterNameLocal, localChange, connectionLocal, sqlConnStringLocal, sqlConnStringCentral);
                }
                else if (syncType == SyncType.MasterData || syncType == SyncType.StaticDataAndSettings)
                {
                    // delete from local if it doesn't exist on central.
                    if (existsLocal == true && existsCentral == false)
                    {
                        centralChange.ChangeType = 2;
                    }

                    ExecuteInsertUpdateMetadataStoredProcedure(!existsCentral, null, null, storedProcedureCentral, tableNameCentral, null, transactionCentral, primaryKeyParameterNameCentral, centralChange, connectionCentral, sqlConnStringCentral, sqlConnStringLocal);
                    ExecuteInsertUpdateMetadataStoredProcedure(!existsLocal, null, null, storedProcedureLocal, tableNameLocal, null, transactionLocal, primaryKeyParameterNameLocal, localChange, connectionLocal, sqlConnStringLocal, sqlConnStringCentral);
                }

                if (tableNameLocal.Equals("Media") || tableNameCentral.Equals("Media"))
                {
                    string storedProcedureName = string.Empty;
                    string tableName = string.Empty;

                    if (localChange.EntityParentType == 12)
                    {
                        storedProcedureName = "ProjectMedia_updatemetadata";
                        tableName = "ProjectMedia";
                    }
                    else if (localChange.EntityParentType == 17)
                    {
                        storedProcedureName = "PartMedia_updatemetadata";
                        tableName = "PartMedia";
                    }
                    else if (localChange.EntityParentType == 14)
                    {
                        storedProcedureName = "AssemblyMedia_updatemetadata";
                        tableName = "AssemblyMedia";
                    }

                    if (!(string.IsNullOrEmpty(storedProcedureName) || string.IsNullOrEmpty(tableName)))
                    {
                        XElement localElementMedia = (from elem in configXmlLocal.Descendants("Adapter")
                                                      where elem.Attribute("Name") != null && elem.Attribute("Name").Value.Equals("[" + tableName + "]")
                                                      select elem).FirstOrDefault();

                        XElement centralElementsMedia = (from elem in configXmlCentral.Descendants("Adapter")
                                                         where elem.Attribute("Name") != null && elem.Attribute("Name").Value.Equals("[" + tableName + "]")
                                                         select elem).FirstOrDefault();

                        List<Pair<string, string>> primaryKeyParameterNameCentralMedia = (from elem in centralElementsMedia.Descendants("Col")
                                                                                          where elem.Attribute("pk") != null && elem.Attribute("pk").Value.Equals("true")
                                                                                          select new Pair<string, string> { First = elem.Attribute("param").Value, Second = elem.Attribute("name").Value }).ToList();

                        List<Pair<string, string>> primaryKeyParameterNameLocalMedia = (from elem in localElementMedia.Descendants("Col")
                                                                                        where elem.Attribute("pk") != null && elem.Attribute("pk").Value.Equals("true")
                                                                                        select new Pair<string, string> { First = elem.Attribute("param").Value, Second = elem.Attribute("name").Value }).ToList();

                        if (!string.IsNullOrEmpty(storedProcedureName))
                        {
                            if (syncType == SyncType.MyProjects)
                            {
                                ExecuteInsertUpdateMetadataStoredProcedure(false, 1, 1, storedProcedureName, tableName, scopeCentralId, transactionCentral, primaryKeyParameterNameCentralMedia, centralChange, connectionCentral, sqlConnStringCentral, sqlConnStringLocal);
                                ExecuteInsertUpdateMetadataStoredProcedure(false, 1, 1, storedProcedureName, tableName, scopeLocalId, transactionLocal, primaryKeyParameterNameLocalMedia, localChange, connectionLocal, sqlConnStringLocal, sqlConnStringCentral);
                            }
                            else if (syncType == SyncType.MasterData || syncType == SyncType.StaticDataAndSettings)
                            {
                                ExecuteInsertUpdateMetadataStoredProcedure(false, null, null, storedProcedureName, tableName, null, transactionCentral, primaryKeyParameterNameCentralMedia, centralChange, connectionCentral, sqlConnStringCentral, sqlConnStringLocal);
                                ExecuteInsertUpdateMetadataStoredProcedure(false, null, null, storedProcedureName, tableName, null, transactionLocal, primaryKeyParameterNameLocalMedia, localChange, connectionLocal, sqlConnStringLocal, sqlConnStringCentral);
                            }
                        }
                    }
                }
            }
            else if (centralChange.Timestamp == localChange.OriginalTimestamp &&   // The entry is changed in the local db but not changed in the central db.
                localChange.Timestamp != localChange.OriginalTimestamp)
            {
                if (syncType == SyncType.MyProjects)
                {
                    ExecuteInsertUpdateMetadataStoredProcedure(!existsCentral, 1, 1, storedProcedureCentral, tableNameCentral, scopeCentralId, transactionCentral, primaryKeyParameterNameCentral, centralChange, connectionCentral, sqlConnStringCentral, sqlConnStringLocal);
                    ExecuteInsertUpdateMetadataStoredProcedure(!existsLocal, null, null, storedProcedureLocal, tableNameLocal, null, transactionLocal, primaryKeyParameterNameLocal, localChange, connectionLocal, sqlConnStringLocal, sqlConnStringCentral);
                }
                else if (syncType == SyncType.MasterData || syncType == SyncType.StaticDataAndSettings)
                {
                    // delete from local if it doesn't exist on central.
                    if (existsLocal == true && existsCentral == false)
                    {
                        centralChange.ChangeType = 2;
                    }

                    ExecuteInsertUpdateMetadataStoredProcedure(!existsCentral, null, null, storedProcedureCentral, tableNameCentral, null, transactionCentral, primaryKeyParameterNameCentral, centralChange, connectionCentral, sqlConnStringCentral, sqlConnStringLocal);
                    ExecuteInsertUpdateMetadataStoredProcedure(!existsLocal, null, null, storedProcedureLocal, tableNameLocal, null, transactionLocal, primaryKeyParameterNameLocal, localChange, connectionLocal, sqlConnStringLocal, sqlConnStringCentral);
                }

                if (tableNameLocal.Equals("Media") || tableNameCentral.Equals("Media"))
                {
                    string storedProcedureName = string.Empty;
                    string tableName = string.Empty;

                    if (localChange.EntityParentType == 12)
                    {
                        storedProcedureName = "ProjectMedia_updatemetadata";
                        tableName = "ProjectMedia";
                    }
                    else if (localChange.EntityParentType == 17)
                    {
                        storedProcedureName = "PartMedia_updatemetadata";
                        tableName = "PartMedia";
                    }
                    else if (localChange.EntityParentType == 14)
                    {
                        storedProcedureName = "AssemblyMedia_updatemetadata";
                        tableName = "AssemblyMedia";
                    }

                    if (!(string.IsNullOrEmpty(storedProcedureName) || string.IsNullOrEmpty(tableName)))
                    {
                        XElement localElementMedia = (from elem in configXmlLocal.Descendants("Adapter")
                                                      where elem.Attribute("Name") != null && elem.Attribute("Name").Value.Equals("[" + tableName + "]")
                                                      select elem).FirstOrDefault();

                        XElement centralElementsMedia = (from elem in configXmlCentral.Descendants("Adapter")
                                                         where elem.Attribute("Name") != null && elem.Attribute("Name").Value.Equals("[" + tableName + "]")
                                                         select elem).FirstOrDefault();

                        List<Pair<string, string>> primaryKeyParameterNameCentralMedia = (from elem in centralElementsMedia.Descendants("Col")
                                                                                          where elem.Attribute("pk") != null && elem.Attribute("pk").Value.Equals("true")
                                                                                          select new Pair<string, string> { First = elem.Attribute("param").Value, Second = elem.Attribute("name").Value }).ToList();

                        List<Pair<string, string>> primaryKeyParameterNameLocalMedia = (from elem in localElementMedia.Descendants("Col")
                                                                                        where elem.Attribute("pk") != null && elem.Attribute("pk").Value.Equals("true")
                                                                                        select new Pair<string, string> { First = elem.Attribute("param").Value, Second = elem.Attribute("name").Value }).ToList();

                        if (!string.IsNullOrEmpty(storedProcedureName))
                        {
                            if (syncType == SyncType.MyProjects)
                            {
                                ExecuteInsertUpdateMetadataStoredProcedure(false, 1, 1, storedProcedureName, tableName, scopeCentralId, transactionCentral, primaryKeyParameterNameCentralMedia, centralChange, connectionCentral, sqlConnStringCentral, sqlConnStringLocal);
                                ExecuteInsertUpdateMetadataStoredProcedure(false, null, null, storedProcedureName, tableName, null, transactionLocal, primaryKeyParameterNameLocalMedia, localChange, connectionLocal, sqlConnStringLocal, sqlConnStringCentral);
                            }
                            else if (syncType == SyncType.MasterData || syncType == SyncType.StaticDataAndSettings)
                            {
                                ExecuteInsertUpdateMetadataStoredProcedure(false, null, null, storedProcedureName, tableName, null, transactionCentral, primaryKeyParameterNameCentralMedia, centralChange, connectionCentral, sqlConnStringCentral, sqlConnStringLocal);
                                ExecuteInsertUpdateMetadataStoredProcedure(false, null, null, storedProcedureName, tableName, null, transactionLocal, primaryKeyParameterNameLocalMedia, localChange, connectionLocal, sqlConnStringLocal, sqlConnStringCentral);
                            }
                        }
                    }
                }
            }
            else if (centralChange.Timestamp != localChange.OriginalTimestamp &&   // The entry is changed in the central db but not in the local db
                localChange.Timestamp == localChange.OriginalTimestamp)
            {
                if (syncType == SyncType.MyProjects)
                {
                    ExecuteInsertUpdateMetadataStoredProcedure(!existsCentral, 0, 0, storedProcedureCentral, tableNameCentral, null, transactionCentral, primaryKeyParameterNameCentral, centralChange, connectionCentral, sqlConnStringCentral, sqlConnStringLocal);
                    ExecuteInsertUpdateMetadataStoredProcedure(!existsLocal, 1, 1, storedProcedureLocal, tableNameLocal, scopeLocalId, transactionLocal, primaryKeyParameterNameLocal, localChange, connectionLocal, sqlConnStringLocal, sqlConnStringCentral);
                }
                else if (syncType == SyncType.MasterData || syncType == SyncType.StaticDataAndSettings)
                {
                    // delete from local if it doesn't exist on central.
                    if (existsLocal == true && existsCentral == false)
                    {
                        centralChange.ChangeType = 2;
                    }

                    ExecuteInsertUpdateMetadataStoredProcedure(!existsCentral, null, null, storedProcedureCentral, tableNameCentral, null, transactionCentral, primaryKeyParameterNameCentral, centralChange, connectionCentral, sqlConnStringCentral, sqlConnStringLocal);
                    ExecuteInsertUpdateMetadataStoredProcedure(!existsLocal, null, null, storedProcedureLocal, tableNameLocal, null, transactionLocal, primaryKeyParameterNameLocal, localChange, connectionLocal, sqlConnStringLocal, sqlConnStringCentral);
                }

                if (tableNameLocal.Equals("Media") || tableNameCentral.Equals("Media"))
                {
                    string storedProcedureName = string.Empty;
                    string tableName = string.Empty;

                    if (localChange.EntityParentType == 12)
                    {
                        storedProcedureName = "ProjectMedia_updatemetadata";
                        tableName = "ProjectMedia";
                    }
                    else if (localChange.EntityParentType == 17)
                    {
                        storedProcedureName = "PartMedia_updatemetadata";
                        tableName = "PartMedia";
                    }
                    else if (localChange.EntityParentType == 14)
                    {
                        storedProcedureName = "AssemblyMedia_updatemetadata";
                        tableName = "AssemblyMedia";
                    }

                    if (!(string.IsNullOrEmpty(storedProcedureName) || string.IsNullOrEmpty(tableName)))
                    {
                        XElement localElementMedia = (from elem in configXmlLocal.Descendants("Adapter")
                                                      where elem.Attribute("Name") != null && elem.Attribute("Name").Value.Equals("[" + tableName + "]")
                                                      select elem).FirstOrDefault();

                        XElement centralElementsMedia = (from elem in configXmlCentral.Descendants("Adapter")
                                                         where elem.Attribute("Name") != null && elem.Attribute("Name").Value.Equals("[" + tableName + "]")
                                                         select elem).FirstOrDefault();

                        List<Pair<string, string>> primaryKeyParameterNameCentralMedia = (from elem in centralElementsMedia.Descendants("Col")
                                                                                          where elem.Attribute("pk") != null && elem.Attribute("pk").Value.Equals("true")
                                                                                          select new Pair<string, string> { First = elem.Attribute("param").Value, Second = elem.Attribute("name").Value }).ToList();

                        List<Pair<string, string>> primaryKeyParameterNameLocalMedia = (from elem in localElementMedia.Descendants("Col")
                                                                                        where elem.Attribute("pk") != null && elem.Attribute("pk").Value.Equals("true")
                                                                                        select new Pair<string, string> { First = elem.Attribute("param").Value, Second = elem.Attribute("name").Value }).ToList();

                        if (!string.IsNullOrEmpty(storedProcedureName))
                        {
                            if (syncType == SyncType.MyProjects)
                            {
                                ExecuteInsertUpdateMetadataStoredProcedure(false, 0, 0, storedProcedureName, tableName, null, transactionCentral, primaryKeyParameterNameCentralMedia, centralChange, connectionCentral, sqlConnStringCentral, sqlConnStringLocal);
                                ExecuteInsertUpdateMetadataStoredProcedure(false, 1, 1, storedProcedureName, tableName, scopeLocalId, transactionLocal, primaryKeyParameterNameLocalMedia, localChange, connectionLocal, sqlConnStringLocal, sqlConnStringCentral);
                            }
                            else if (syncType == SyncType.MasterData || syncType == SyncType.StaticDataAndSettings)
                            {
                                ExecuteInsertUpdateMetadataStoredProcedure(false, null, null, storedProcedureName, tableName, null, transactionCentral, primaryKeyParameterNameCentralMedia, centralChange, connectionCentral, sqlConnStringCentral, sqlConnStringLocal);
                                ExecuteInsertUpdateMetadataStoredProcedure(false, null, null, storedProcedureName, tableName, null, transactionLocal, primaryKeyParameterNameLocalMedia, localChange, connectionLocal, sqlConnStringLocal, sqlConnStringCentral);
                            }
                        }
                    }
                }
            }
        }

        #endregion
    }
}
