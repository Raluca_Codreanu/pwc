﻿namespace ZPKTool.Synchronization
{
    /// <summary>
    /// The status of a synchronization task.
    /// </summary>
    public enum SynchronizationTaskStatus
    {
        /// <summary>
        /// Task has started.
        /// </summary>
        Initializing,

        /// <summary>
        /// Task is selecting changes.
        /// </summary>
        SelectingChanges,

        /// <summary>
        /// Task is applying changes.
        /// </summary>
        ApplyingChanges,
        
        /// <summary>
        /// Task has finished.
        /// </summary>
        Finished,

        /// <summary>
        /// The task was aborted.
        /// </summary>
        Aborted
    }
}
