﻿namespace ZPKTool.Synchronization
{
    /// <summary>
    /// The synchronization tasks.
    /// </summary>
    public enum SynchronizationTask
    {
        /// <summary>
        /// Synchronizing the released projects.
        /// </summary>
        ReleasedProjects = 0,

        /// <summary>
        /// Synchronizing My Projects (current user's projects).
        /// </summary>
        MyProjects = 1,

        /// <summary>
        /// Synchronizing master data.
        /// </summary>
        MasterData = 2,

        /// <summary>
        /// Synchronizing static data &amp; settings (users, classifications, measurement units, countries, etc.)
        /// </summary>
        StaticDataAndSettings = 3,

        /// <summary>
        /// Processing change tracking for conflicts and generating conflict data.
        /// </summary>
        ProcessConflicts = 4,

        /// <summary>
        /// Completed all tasks.
        /// </summary>
        CompletedAllTasks = 5,

        /// <summary>
        /// Initializes the synchronization scopes.
        /// </summary>
        Initialising = 6
    }
}
