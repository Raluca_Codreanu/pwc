﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using ZPKTool.Common;
using ZPKTool.DataAccess;

namespace ZPKTool.Synchronization
{
    /// <summary>
    /// Exception thrown by the synchronization.
    /// </summary>
    [Serializable]
    public class SynchronizationException : ZPKException
    {
        #region Initialization

        /// <summary>
        /// Initializes a new instance of the <see cref="SynchronizationException"/> class.
        /// </summary>
        public SynchronizationException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SynchronizationException"/> class.
        /// </summary>
        /// <param name="errorCode">The code of the error</param>
        public SynchronizationException(string errorCode)
            : base(string.Empty)
        {
            ErrorCode = errorCode;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SynchronizationException"/> class.
        /// </summary>
        /// <param name="errorCode">The error code.</param>
        /// <param name="ex">The exception wrapped by this instance.</param>
        public SynchronizationException(string errorCode, Exception ex)
            : base(string.Empty, ex)
        {
            this.ErrorCode = errorCode;

            if (ex is COMException || ex.InnerException is COMException)
            {
                this.ErrorCode = SyncErrorCodes.SyncFrameworkNotInstalled;
            }
            else
            {
                Exception exception = ex;
                while (exception != null)
                {
                    if (exception is SqlException)
                    {
                        this.ErrorCode = SynchronizationException.ParseSqlException(exception as SqlException);
                    }

                    exception = exception.InnerException;
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SynchronizationException"/> class.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the exception being thrown.</param>
        /// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the source or destination.</param>
        protected SynchronizationException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        #endregion Initialization

        /// <summary>
        /// Parses the SQL exception exception.
        /// </summary>
        /// <param name="ex">The SQL exception to parse.</param>
        /// <returns>
        /// The specific error code.
        /// </returns>
        public static string ParseSqlException(SqlException ex)
        {
            string errorCode = SyncErrorCodes.InternalError;

            if (SqlServerErrorCodes.NoDbConnectionCodes.Contains(ex.Number))
            {
                errorCode = SyncErrorCodes.NoConnection;
            }
            else if (ex.Number == 195 && ex.Message.Contains("min_active_rowversion"))
            {
                errorCode = SyncErrorCodes.MinSqlServerVersionError;
            }

            return errorCode;
        }
    }
}
