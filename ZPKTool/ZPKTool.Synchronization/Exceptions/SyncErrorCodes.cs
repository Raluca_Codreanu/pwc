﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Synchronization
{
    /// <summary>
    /// This class contains the error codes used in the synchronization.
    /// An error code should be created only if the error must be reported to the user (and makes sense to report it).
    /// </summary>
    public static class SyncErrorCodes
    {
        /// <summary>
        /// The error was not due to happen at anytime and is probably the result of a bug.
        /// </summary>
        public const string InternalError = "Synchronization_Error_Internal";

        /// <summary>
        /// The connection to the db server is not possible (server name is incorrect).
        /// The error is identified by the sql exception number.
        /// </summary>
        public const string NoConnection = "Database_NoConnection";

        /// <summary>
        /// The synchronization cannot run because one of the databases (local or central) is hosted in an unsupported version of Sql Server. The minimum supported version is Sql Server 2005 SP2 (9.00.3042).
        /// </summary>
        public const string MinSqlServerVersionError = "Database_OtherVersionRequired";
           
        /// <summary>
        /// COM Exception Sync Framework is not installed.
        /// </summary>
        public const string SyncFrameworkNotInstalled = "Sync_SynFrameworkNotInstalled";
            
        /// <summary>
        /// Database version mismatch (local version &lt; central version).
        /// </summary>
        public const string DataBaseVersionErrorLocalLowerThanRemote = "Synchronization_LocalDbVersionLowerThanCentral";

        /// <summary>
        /// Database version mismatch (central version > local version),
        /// </summary>
        public const string DataBaseVersionErrorLocalHigherThanLocal = "Synchronization_LocalDbVersionHigherThanCentral";
    }
}
