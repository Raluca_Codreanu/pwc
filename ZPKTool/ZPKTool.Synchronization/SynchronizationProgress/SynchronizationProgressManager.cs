﻿namespace ZPKTool.Synchronization
{
    /// <summary>
    /// Manages the progress of a synchronization task.
    /// </summary>
    public class SynchronizationProgressManager
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SynchronizationProgressManager"/> class.
        /// </summary>
        public SynchronizationProgressManager()
        {
        }

        /// <summary>
        /// Gets or sets the total number of items to be processed by the task.
        /// </summary>
        /// <value>
        /// The total items no.
        /// </value>
        public int TotalItemsCount { get; set; }

        /// <summary>
        /// Gets or sets the number of processed items.
        /// </summary>
        /// <value>
        /// The number of processed items.
        /// </value>
        private int TotalProcessedItemsCount { get; set; }

        /// <summary>
        /// Gets or sets the number of conflicts.
        /// </summary>
        /// <value>
        /// The number of conflicts.
        /// </value>
        private int TotalConflictsItemsCount { get; set; }

        /// <summary>
        /// Gets the progress as a percentage.
        /// </summary>
        private double TotalPercentageCompleted
        {
            get
            {
                if (this.TotalItemsCount == 0)
                {
                    return 0;
                }
                else
                {
                    return (((double)TotalProcessedItemsCount + (double)PartialProcessedItemsCount) * 100) / (double)TotalItemsCount;
                }
            }
        }

        /// <summary>
        /// Gets or sets the partial processed items count.
        /// </summary>
        /// <value>
        /// The partial processed items count.
        /// </value>
        public int PartialProcessedItemsCount { get; set; }

        /// <summary>
        /// Gets or sets the partial conflicts items count.
        /// </summary>
        /// <value>
        /// The partial conflicts items count.
        /// </value>
        public int PartialConflictsItemsCount { get; set; }

        /// <summary>
        /// Gets the current progress.
        /// </summary>        
        public SynchronizationProgressData Progress
        {
            get
            {
                return new SynchronizationProgressData(
                    TotalItemsCount,
                    TotalProcessedItemsCount + PartialProcessedItemsCount,
                    TotalConflictsItemsCount + PartialConflictsItemsCount,
                    TotalPercentageCompleted);
            }
        }

        /// <summary>
        /// Adds the partial item count to total items count.
        /// </summary>
        public void AddPartialItemCountToTotalItemsCount()
        {
            this.TotalProcessedItemsCount += this.PartialProcessedItemsCount;
            this.PartialProcessedItemsCount = 0;

            this.TotalConflictsItemsCount += this.PartialConflictsItemsCount;
            this.PartialConflictsItemsCount = 0;
        }
    }
}
