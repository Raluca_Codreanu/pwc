﻿namespace ZPKTool.Synchronization
{
    using ZPKTool.Common;

    /// <summary>
    /// WeakEventManager implementation for the Synchronization Progress event of the SynchronizationProgressManager.
    /// </summary>
    public class SynchronizationProgressEventManager : WeakEventManagerBase<SynchronizationProgressEventManager, ISynchronizationManager>
    {
        /// <summary>
        /// Attaches the event handler.
        /// </summary>
        /// <param name="source">The source to which to attach.</param>
        protected override void StartListening(ISynchronizationManager source)
        {
            source.Progress += DeliverEvent;
        }

        /// <summary>
        /// Detaches the event handler.
        /// </summary>
        /// <param name="source">The source from which to detach.</param>
        protected override void StopListening(ISynchronizationManager source)
        {
            source.Progress -= DeliverEvent;
        }
    }
}
