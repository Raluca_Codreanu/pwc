﻿namespace ZPKTool.Synchronization
{
    /// <summary>
    /// Progress information for a synchronization task.
    /// </summary>    
    public class SynchronizationProgressData
    {
        /// <summary>
        /// The total number of items to be processed by the task.
        /// </summary>
        private int totalItems = 0;

        /// <summary>
        /// The number of items processed.
        /// </summary>
        private int processedItems = 0;

        /// <summary>
        /// The number of conflicts.
        /// </summary>
        private int conflicts = 0;

        /// <summary>
        /// The progress of the task expressed as a percentage.
        /// </summary>
        private double percentageCompleted = 0;

        /// <summary>
        /// Initializes a new instance of the <see cref="SynchronizationProgressData" /> class.
        /// </summary>
        /// <param name="totalItems">The total items number.</param>
        /// <param name="processedItems">The number of processed items.</param>
        /// <param name="conflicts">The number of conflicts.</param>
        /// <param name="percentageCompleted">The progress as a percentage.</param>
        public SynchronizationProgressData(int totalItems, int processedItems, int conflicts, double percentageCompleted)
        {
            this.totalItems = totalItems;
            this.processedItems = processedItems;
            this.conflicts = conflicts;
            this.percentageCompleted = percentageCompleted;
        }

        /// <summary>
        /// Gets the total number of items.
        /// </summary>
        /// <value>The total items.</value>
        public int TotalItemsNumber
        {
            get { return totalItems; }
        }

        /// <summary>
        /// Gets the number of processed items.
        /// </summary>
        public int ProcessedItems
        {
            get { return processedItems; }
        }    

        /// <summary>
        /// Gets the number of conflicts.
        /// </summary>
        public int Conflicts
        {
            get { return conflicts; }
        }

        /// <summary>
        /// Gets the progress of the task expressed as a percentage.
        /// </summary>
        public double PercentageCompleted
        {
            get { return percentageCompleted; }
        }
    }
}
