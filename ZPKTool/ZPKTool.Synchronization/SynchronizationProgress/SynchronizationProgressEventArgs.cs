﻿using System;

namespace ZPKTool.Synchronization
{
    /// <summary>
    /// Contains event data sent during synchronization progress change.
    /// </summary>
    public class SynchronizationProgressEventArgs : EventArgs
    {
        /// <summary>
        /// The currently running synchronization task.
        /// </summary>
        private SynchronizationTask crtTask;

        /// <summary>
        /// The status of the currently running task.
        /// </summary>
        private SynchronizationTaskStatus crtTaskStatus;

        /// <summary>
        /// Progress data for the current task.
        /// </summary>
        private SynchronizationProgressData data;

        /// <summary>
        /// Initializes a new instance of the <see cref="SynchronizationProgressEventArgs"/> class.
        /// </summary>
        /// <param name="task">The current task.</param>
        /// <param name="taskStatus">The task status.</param>
        /// <param name="data">The task progress data.</param>
        public SynchronizationProgressEventArgs(SynchronizationTask task, SynchronizationTaskStatus taskStatus, SynchronizationProgressData data)
        {
            this.crtTask = task;
            this.crtTaskStatus = taskStatus;
            this.data = data;
        }

        /// <summary>
        /// Gets the current task.
        /// </summary>
        /// <value>The current task.</value>
        public SynchronizationTask CurrentTask
        {
            get { return crtTask; }
        }

        /// <summary>
        /// Gets the current task's status.
        /// </summary>
        /// <value>The current task status.</value>
        public SynchronizationTaskStatus CurrentTaskStatus
        {
            get { return crtTaskStatus; }
        }

        /// <summary>
        /// Gets the progress data for the current task.
        /// </summary>
        /// <value>The progress data.</value>
        public SynchronizationProgressData Data
        {
            get { return data; }
        }
    }
}
