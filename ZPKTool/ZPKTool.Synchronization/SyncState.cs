﻿namespace ZPKTool.Synchronization
{
    /// <summary>
    /// The synchronization state.
    /// </summary>
    public enum SyncState
    {
        /// <summary>
        /// The synchronization is not running.
        /// </summary>
        NotStarted = 0,

        /// <summary>
        /// The synchronization is running.
        /// </summary>
        Running = 1,

        /// <summary>
        /// The synchronization is aborted.
        /// </summary>
        Aborting = 2,

        /// <summary>
        /// The synchronization was aborted.
        /// </summary>
        Aborted = 3
    }
}
