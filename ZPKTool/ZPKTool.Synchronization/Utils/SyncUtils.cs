﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Text;
using ZPKTool.Business;
using ZPKTool.Common;
using ZPKTool.DataAccess;

namespace ZPKTool.Synchronization
{
    /// <summary>
    /// DataBase Helper class.
    /// </summary>
    public static class SyncUtils
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Determines if the database table exists.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="connectionString">The connection string.</param>
        /// <returns>
        /// True if the table exists and false otherwise.
        /// </returns>
        /// <exception cref="SynchronizationException">A SQL Server error occurred.</exception>
        public static bool DbTableExists(string tableName, string connectionString)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand())
                    {
                        command.CommandText = "IF OBJECT_ID(@TableName, 'U') IS NOT NULL SELECT 'true' ELSE SELECT 'false'";
                        command.Parameters.Add(new SqlParameter("@TableName", tableName));
                        command.CommandType = CommandType.Text;
                        command.CommandTimeout = 90;
                        command.Connection = connection;

                        return Convert.ToBoolean(command.ExecuteScalar());
                    }
                }
            }
            catch (SqlException ex)
            {
                log.WarnException("Could not determine if the change tracking table exists.", ex);
                throw new SynchronizationException(SynchronizationException.ParseSqlException(ex), ex);
            }
        }

        /// <summary>
        /// Deletes the table if has no records.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="connectionString">The connection string.</param>
        public static void DeleteTableIfHasNoRecords(string tableName, string connectionString)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    string strCheckTable =
                        @"declare @count int
                        set @count = (SELECT rows FROM sysindexes WHERE id = OBJECT_ID('{0}') AND indid < 2)
                        if(@count = 0)
                        DROP TABLE @TableName";

                    using (SqlCommand command = new SqlCommand(strCheckTable, connection))
                    {
                        command.Parameters.Add(new SqlParameter("@TableName", tableName));
                        command.CommandType = CommandType.Text;
                        command.CommandTimeout = 90;
                        connection.Open();

                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (SqlException ex)
            {
                log.ErrorException("Could not delete the change tracking data table.", ex);
                throw new SynchronizationException(SyncErrorCodes.InternalError, ex);
            }
        }

        /// <summary>
        /// Creates the data base function for sync if it doesn't exists or updates the function
        /// if it's function body saved in file is different than the one in the database.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="functionName">Name of the function.</param>
        /// <param name="scriptDirectoryPath">The directory path to where the file containing the function resides..</param>
        /// <exception cref="SynchronizationException">A sql exception occurred while executing the creation or update of the function.</exception>
        [SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "The command text does not contain any user input.")]
        public static void CreateOrUpdateDataBaseFunctionForSync(string connectionString, string functionName, string scriptDirectoryPath)
        {
            try
            {
                bool executeScript = false;
                string dbFunctionBody;
                string scriptFunctionBody = SyncUtils.ReadEmbeddedResource(scriptDirectoryPath + "." + functionName + ".sql");
                if (SyncUtils.TryGetSqlFunctionBody(connectionString, functionName, out dbFunctionBody))
                {
                    if (!dbFunctionBody.Equals(scriptFunctionBody))
                    {
                        // Replace create function with alter function. The CREATE AND ALTER are keywords.
                        scriptFunctionBody = scriptFunctionBody.Replace("CREATE", "ALTER");
                        executeScript = true;
                    }
                }
                else
                {
                    executeScript = true;
                }

                if (executeScript)
                {
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        using (SqlCommand command = new SqlCommand(scriptFunctionBody, connection))
                        {
                            command.CommandType = CommandType.Text;
                            connection.Open();
                            command.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                log.ErrorException("Could not create or alter the database function " + functionName + ".", ex);
                throw new SynchronizationException(SynchronizationException.ParseSqlException(ex), ex);
            }
        }

        /// <summary>
        /// Gets the SQL function body.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="functionName">Name of the function.</param>
        /// <param name="functionBody">The function body.</param>
        /// <returns>True if the function body was retrieved; otherwise, false.</returns>
        /// <exception cref="SynchronizationException">A sql server error occurred.</exception>
        public static bool TryGetSqlFunctionBody(string connectionString, string functionName, out string functionBody)
        {
            try
            {
                functionBody = string.Empty;

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.CommandText = "IF OBJECT_ID (@FunctionName, N'IF') IS NOT NULL EXEC sp_helptext @FunctionName";
                        command.Parameters.Add(new SqlParameter("@FunctionName", functionName));
                        command.CommandType = CommandType.Text;
                        command.Connection = connection;
                        connection.Open();

                        using (SqlDataReader rdr = command.ExecuteReader())
                        {
                            while (rdr.Read())
                            {
                                functionBody += rdr.GetString(0);
                            }

                            if (string.IsNullOrWhiteSpace(functionBody))
                            {
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                log.WarnException("Could not get the " + functionName + " body.", ex);
                throw new SynchronizationException(SynchronizationException.ParseSqlException(ex), ex);
            }
        }

        /// <summary>
        /// Checks if the local db version and central db version are the same.
        /// </summary>
        public static void ValidateDatabaseVersion()
        {
            decimal localVersion = 0;
            decimal centralVersion = 0;

            try
            {
                // The remote(central) and local database must have the same version.
                var globalSettingsManager = new DbGlobalSettingsManager(DbIdentifier.LocalDatabase);
                var globalSettings = globalSettingsManager.Get();
                localVersion = globalSettings.Version;
            }
            catch (DataAccessException ex)
            {
                if (ex.ErrorCode != DatabaseErrorCode.NoConnection)
                {
                    log.WarnException("Could not get the local database version due to a data access exception.", ex);
                }
            }

            try
            {
                var globalSettingsManager = new DbGlobalSettingsManager(DbIdentifier.CentralDatabase);
                var globalSettings = globalSettingsManager.Get();
                centralVersion = globalSettings.Version;
            }
            catch (DataAccessException ex)
            {
                if (ex.ErrorCode != DatabaseErrorCode.NoConnection)
                {
                    log.WarnException("Could not get the central database version due to a data access exception.", ex);
                }
            }

            if (localVersion < centralVersion)
            {
                log.Warn("Local database version ({0}) < central database version({1}).", localVersion, centralVersion);
                throw new SynchronizationException(SyncErrorCodes.DataBaseVersionErrorLocalLowerThanRemote);
            }
            else if (localVersion > centralVersion)
            {
                log.Warn("Local database version ({0}) > central database version({1}).", localVersion, centralVersion);
                throw new SynchronizationException(SyncErrorCodes.DataBaseVersionErrorLocalHigherThanLocal);
            }
        }

        /// <summary>
        /// Reads the embedded resource with the specified name.
        /// </summary>
        /// <param name="resourceName">Name of the file.</param>
        /// <returns>The embedded resource as a string.</returns>
        public static string ReadEmbeddedResource(string resourceName)
        {
            try
            {
                string resourceEmbedded = typeof(SyncUtils).Namespace + "." + resourceName;

                var assembly = System.Reflection.Assembly.GetExecutingAssembly();
                using (Stream embeddedStream = assembly.GetManifestResourceStream(resourceEmbedded))
                {
                    using (StreamReader reader = new StreamReader(embeddedStream, Encoding.Default))
                    {
                        string script = reader.ReadToEnd();
                        return script;
                    }
                }
            }
            catch (Exception ex)
            {
                log.ErrorException("Could not read the embedded resource from the " + resourceName + " script.", ex);
                throw;
            }
        }
    }
}
