﻿namespace ZPKTool.Synchronization
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using System.Xml.Linq;
    using Business;
    using DataAccess;
    using Microsoft.Synchronization;
    using Microsoft.Synchronization.Data;
    using Microsoft.Synchronization.Data.SqlServer;

    /// <summary>
    /// Encapsulates a method that handles progress reports during the synchronization process. 
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The event information.</param>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1003:UseGenericEventHandlerInstances", Justification = "Considering the use of the sync progress handler, a generic event handler is not necessary.")]
    public delegate void SynchronizationProgressHandler(object sender, SynchronizationProgressEventArgs e);

    /// <summary>
    /// Synchronization Manager uses Sync Framework 2.1 to synchronize databases.
    /// Sync Framework is a comprehensive synchronization platform that enables collaboration and
    /// offline access for applications, services, and devices.
    /// Sync Framework features technologies and tools that enable roaming, data sharing,
    /// and taking data offline. By using Sync Framework, developers can build synchronization
    /// ecosystems that integrate any application with data from any store, by using any protocol
    /// over any network.
    /// </summary>
    /// <exception cref="FileNotFoundException">File Not Found Exception: The sync framework redistributable package is not installed correctly.</exception>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.OrderingRules", "SA1204:StaticElementsMustAppearBeforeInstanceElements", Justification = "This is a large class so the methods are organized in regions by their purpose.")]
    internal class SynchronizationManager : ISynchronizationManager
    {
        #region Members

        /// <summary>
        /// The message of the exception that is thrown when the synchronization session is aborted.
        /// </summary>
        private const string AbortExceptionMessage = "Sync aborted by user action";

        /// <summary>
        /// The SQL command timeout.
        /// </summary>
        private const int SqlCommandTimeout = 120;

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger Log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The local DB ADO connection string.
        /// </summary>
        private readonly string localConnectionString = Configuration.ConnectionStringLocalADO;

        /// <summary>
        /// The central DB ADO connection string.
        /// </summary>
        private readonly string centralConnectionString = Configuration.ConnectionStringCentralADO;

        /// <summary>
        /// Manages the progress of a synchronization task.
        /// </summary>
        private SynchronizationProgressManager progressManager;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SynchronizationManager"/> class.
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "version", Justification = "This instance is created to trigger the loading of its assembly in order to check if the Sync Framework is installed.")]
        public SynchronizationManager()
        {
            // Do not remove this line. It is here to trigger an exception if the Microsoft.Synchronization assembly is not found in the system. 
            SyncVersion version = new SyncVersion(0, 0);

            this.Conflicts = new List<SynchronizationConflict>();
            this.CurrentStatus = new SyncStatus();
        }

        #endregion

        #region Properties

        /// <summary>
        /// An event handler for the synchronization progress event.
        /// </summary>
        public event SynchronizationProgressHandler Progress;

        /// <summary>
        /// Gets the synchronization conflicts.
        /// </summary>        
        public ICollection<SynchronizationConflict> Conflicts { get; private set; }

        /// <summary>
        /// Gets the synchronization current status.
        /// </summary>
        public SyncStatus CurrentStatus { get; private set; }

        /// <summary>
        /// Gets or sets the Sync Orchestrator that contains the two SyncProvider objects that will participate in synchronization.
        /// It starts and controls the synchronization session, and dispatches progress events to the application.
        /// </summary>
        /// <value>
        /// The orchestrator.
        /// </value>
        private SyncOrchestrator Orchestrator { get; set; }

        /// <summary>
        /// Gets or sets the type of the synchronization.
        /// </summary>
        /// <value>
        /// The type of the sync.
        /// </value>
        private SyncType SynchronizationType { get; set; }

        /// <summary>
        /// Gets or sets the sync retry guids.
        /// </summary>
        /// <value>
        /// The sync retry guids.
        /// </value>
        private List<Guid> SyncRetryGuids { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the synchronization needs sync retry.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [needs sync retry]; otherwise, <c>false</c>.
        /// </value>
        private bool DoSyncRetry { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [stop sync retry].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [stop sync retry]; otherwise, <c>false</c>.
        /// </value>
        private bool StopSyncRetry { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is sync retry session.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is sync retry session; otherwise, <c>false</c>.
        /// </value>
        private bool IsSyncRetrySession { get; set; }

        #endregion

        #region Initialize Scopes

        /// <summary>
        /// Initializes the scopes.
        /// </summary>
        /// <param name="userGuid">The user GUID.</param>
        /// <param name="keepTrackOfSuccesfullyMaintainedUser">if set to <c>true</c> keeps track of successfully maintained user.</param>
        public void InitializeScopesAndConvert(Guid userGuid, bool keepTrackOfSuccesfullyMaintainedUser)
        {
            try
            {
                SyncUtils.ValidateDatabaseVersion();

                // Check if change tracking table exists.
                if (SyncUtils.DbTableExists("ChangeTrackingData", this.localConnectionString)
                    || SyncUtils.DbTableExists("ChangeTrackingData", this.centralConnectionString))
                {
                    // Configure user sync scope.
                    var userLocalSyncProvider = ConfigureSqlSyncProvider(SyncType.User, userGuid, this.localConnectionString);
                    var userCentralSyncProvider = ConfigureSqlSyncProvider(SyncType.User, userGuid, this.centralConnectionString);

                    userLocalSyncProvider.Dispose();
                    userCentralSyncProvider.Dispose();

                    // Configure static data and settings sync scope.
                    var settingsLocalSyncProvider = ConfigureSqlSyncProvider(SyncType.StaticDataAndSettings, userGuid, this.localConnectionString);
                    var settingsCentralSyncProvider = ConfigureSqlSyncProvider(SyncType.StaticDataAndSettings, userGuid, this.centralConnectionString);

                    string settingsScopeNameLocal = settingsLocalSyncProvider.ScopeName;
                    string settingsScopeNameCentral = settingsCentralSyncProvider.ScopeName;

                    // Convert old change tracking.
                    ConvertChangeTrackings.Convert(settingsScopeNameLocal, SyncType.StaticDataAndSettings, userGuid, settingsScopeNameCentral, this.localConnectionString, this.centralConnectionString);

                    settingsLocalSyncProvider.Dispose();
                    settingsCentralSyncProvider.Dispose();

                    // Configure logs sync scope.
                    var logsLocalSyncProvider = ConfigureSqlSyncProvider(SyncType.Logs, userGuid, this.localConnectionString);
                    var logsCentralSyncProvider = ConfigureSqlSyncProvider(SyncType.Logs, userGuid, this.centralConnectionString);

                    logsLocalSyncProvider.Dispose();
                    logsCentralSyncProvider.Dispose();

                    // Configure master data sync scope.
                    var masterDataLocalSyncProvider = ConfigureSqlSyncProvider(SyncType.MasterData, userGuid, this.localConnectionString);
                    var masterDataCentralSyncProvider = ConfigureSqlSyncProvider(SyncType.MasterData, userGuid, this.centralConnectionString);

                    if (SyncUtils.DbTableExists("ChangeTrackingData", this.localConnectionString) &&
                        SyncUtils.DbTableExists("ChangeTrackingData", this.centralConnectionString))
                    {
                        var masterDataScopeNameLocal = masterDataLocalSyncProvider.ScopeName;
                        var masterDataScopeNameCentral = masterDataCentralSyncProvider.ScopeName;

                        // Convert old change tracking.
                        ConvertChangeTrackings.Convert(masterDataScopeNameLocal, SyncType.MasterData, userGuid, masterDataScopeNameCentral, this.localConnectionString, this.centralConnectionString);
                    }

                    masterDataLocalSyncProvider.Dispose();
                    masterDataCentralSyncProvider.Dispose();

                    // Configure my projects sync scope.
                    var myProjectsLocalSyncProvider = ConfigureSqlSyncProvider(SyncType.MyProjects, userGuid, this.localConnectionString);
                    var myProjectCentralSyncProvider = ConfigureSqlSyncProvider(SyncType.MyProjects, userGuid, this.centralConnectionString);

                    if (SyncUtils.DbTableExists("ChangeTrackingData", this.localConnectionString) &&
                        SyncUtils.DbTableExists("ChangeTrackingData", this.centralConnectionString))
                    {
                        var myProjectsScopeNameLocal = myProjectsLocalSyncProvider.ScopeName;
                        var myProjectsScopeNameCentral = myProjectCentralSyncProvider.ScopeName;

                        // Convert old change tracking.
                        ConvertChangeTrackings.Convert(myProjectsScopeNameLocal, SyncType.MyProjects, userGuid, myProjectsScopeNameCentral, this.localConnectionString, this.centralConnectionString);

                        SyncUtils.DeleteTableIfHasNoRecords("ChangeTrackingData", this.localConnectionString);
                        SyncUtils.DeleteTableIfHasNoRecords("ChangeTrackingData", this.centralConnectionString);
                    }

                    myProjectsLocalSyncProvider.Dispose();
                    myProjectCentralSyncProvider.Dispose();

                    // Configure released projects sync scope.
                    var releasedLocalSyncProvider = ConfigureSqlSyncProvider(SyncType.ReleasedProjects, userGuid, this.localConnectionString);
                    var releasedCentralSyncProvider = ConfigureSqlSyncProvider(SyncType.ReleasedProjects, userGuid, this.centralConnectionString);

                    // Delete released projects old change trackings.
                    if (SyncUtils.DbTableExists("ChangeTrackingData", this.localConnectionString) &&
                        SyncUtils.DbTableExists("ChangeTrackingData", this.centralConnectionString))
                    {
                        string releasedScopeNameLocal = releasedLocalSyncProvider.ScopeName;
                        string releasedScopeNameCentral = releasedCentralSyncProvider.ScopeName;

                        // Convert old change tracking.
                        ConvertChangeTrackings.Convert(releasedScopeNameLocal, SyncType.ReleasedProjects, userGuid, releasedScopeNameCentral, this.localConnectionString, this.centralConnectionString);

                        SyncUtils.DeleteTableIfHasNoRecords("ChangeTrackingData", this.localConnectionString);
                        SyncUtils.DeleteTableIfHasNoRecords("ChangeTrackingData", this.centralConnectionString);
                    }

                    releasedLocalSyncProvider.Dispose();
                    releasedCentralSyncProvider.Dispose();

                    if (keepTrackOfSuccesfullyMaintainedUser)
                    {
                        UpdateSuccesfullyScopeUpdate(userGuid);
                    }
                }
                else
                {
                    if (keepTrackOfSuccesfullyMaintainedUser)
                    {
                        // Configure user sync scope.
                        var userLocalSyncProvider = ConfigureSqlSyncProvider(SyncType.User, userGuid, this.localConnectionString);
                        var userCentralSyncProvider = ConfigureSqlSyncProvider(SyncType.User, userGuid, this.centralConnectionString);

                        userLocalSyncProvider.Dispose();
                        userCentralSyncProvider.Dispose();

                        // Configure static data and settings sync scope.
                        var settingsLocalSyncProvider = ConfigureSqlSyncProvider(SyncType.StaticDataAndSettings, userGuid, this.localConnectionString);
                        var settingsCentralSyncProvider = ConfigureSqlSyncProvider(SyncType.StaticDataAndSettings, userGuid, this.centralConnectionString);

                        settingsLocalSyncProvider.Dispose();
                        settingsCentralSyncProvider.Dispose();

                        // Configure logs sync scope.
                        var logsLocalSyncProvider = ConfigureSqlSyncProvider(SyncType.Logs, userGuid, this.localConnectionString);
                        var logsCentralSyncProvider = ConfigureSqlSyncProvider(SyncType.Logs, userGuid, this.centralConnectionString);

                        logsLocalSyncProvider.Dispose();
                        logsCentralSyncProvider.Dispose();

                        // Configure master data sync scope.
                        var masterDataLocalSyncProvider = ConfigureSqlSyncProvider(SyncType.MasterData, userGuid, this.localConnectionString);
                        var masterDataCentralSyncProvider = ConfigureSqlSyncProvider(SyncType.MasterData, userGuid, this.centralConnectionString);

                        masterDataLocalSyncProvider.Dispose();
                        masterDataCentralSyncProvider.Dispose();

                        // Configure my projects sync scope.
                        var myProjectsLocalSyncProvider = ConfigureSqlSyncProvider(SyncType.MyProjects, userGuid, this.localConnectionString);
                        var myProjectCentralSyncProvider = ConfigureSqlSyncProvider(SyncType.MyProjects, userGuid, this.centralConnectionString);

                        myProjectsLocalSyncProvider.Dispose();
                        myProjectCentralSyncProvider.Dispose();

                        // Configure released projects sync scope.
                        var releasedLocalSyncProvider = ConfigureSqlSyncProvider(SyncType.ReleasedProjects, userGuid, this.localConnectionString);
                        var releasedCentralSyncProvider = ConfigureSqlSyncProvider(SyncType.ReleasedProjects, userGuid, this.centralConnectionString);

                        releasedLocalSyncProvider.Dispose();
                        releasedCentralSyncProvider.Dispose();

                        UpdateSuccesfullyScopeUpdate(userGuid);
                    }
                }
            }
            catch (SyncException ex)
            {
                Log.ErrorException("Error encountered during initializing scopes for synchronization", ex);
                throw new SynchronizationException(SyncErrorCodes.InternalError, ex);
            }
            catch (SqlException ex)
            {
                Log.ErrorException("SQL Exception encountered during synchronization initialization and conversion.", ex);
                throw new SynchronizationException(SynchronizationException.ParseSqlException(ex), ex);
            }
        }

        #endregion

        #region Synchronize

        /// <summary>
        /// Starts a synchronization session.
        /// </summary>
        /// <param name="syncStaticDataAndSettings">if set to true the static data and settings should be synced.</param>
        /// <param name="syncReleasedProjects">if set to true the released projects should be synced.</param>
        /// <param name="syncMasterData">if set to true the master data should be synced.</param>
        /// <param name="syncMyProjects">if set to true my projects should be synced.</param>
        /// <param name="userGuid">The ID of the user whose projects (my projects) should be synced.</param>
        /// <param name="resolveConflicts">if set to true, this call will resolve any conflicts that appeared in previous runs and stored in the Conflicts property.</param>
        /// <exception cref="ZPKTool.Synchronization.SynchronizationException">An error occurred during synchronization.</exception>
        public void Synchronize(
            bool syncStaticDataAndSettings,
            bool syncReleasedProjects,
            bool syncMasterData,
            bool syncMyProjects,
            Guid userGuid,
            bool resolveConflicts)
        {
            if (this.CurrentStatus.State == SyncState.Running
                || this.CurrentStatus.State == SyncState.Aborting)
            {
                return;
            }

            if (!resolveConflicts)
            {
                this.Conflicts = new List<SynchronizationConflict>();
            }

            this.InitializeCurrentStatus(syncStaticDataAndSettings, syncReleasedProjects, syncMasterData, syncMyProjects);

            try
            {
                this.ProgressChanged(SynchronizationTask.Initialising, SynchronizationTaskStatus.Initializing, null);

                this.InitializeScopesAndConvert(userGuid, false);

                Action syncSettings = () =>
                {
                    Synchronize(SyncType.StaticDataAndSettings, userGuid, SyncDirection.Download);

                    // Also synchronize the logs when synchronizing static data.
                    Synchronize(SyncType.Logs, userGuid, SyncDirection.Upload);

                    // Sync the global settings too
                    Synchronize(SyncType.GlobalSettings, userGuid, SyncDirection.Download);
                };

                if (syncStaticDataAndSettings)
                {
                    syncSettings();
                }

                if (syncReleasedProjects && this.CurrentStatus.State != SyncState.Aborting)
                {
                    // Check if scope was initialized. It deletes the local released projects made with the old synchronization.
                    Synchronize(SyncType.ReleasedProjects, Guid.Empty, SyncDirection.Upload, false);

                    // Synchronizes the released projects. From central to local database.
                    Synchronize(SyncType.ReleasedProjects, userGuid, SyncDirection.Download);
                }

                if (syncMasterData && this.CurrentStatus.State != SyncState.Aborting)
                {
                    Synchronize(SyncType.MasterData, userGuid, SyncDirection.Download);
                }

                if (syncMyProjects && this.CurrentStatus.State != SyncState.Aborting)
                {
                    if (!syncStaticDataAndSettings)
                    {
                        syncSettings();
                    }

                    Synchronize(SyncType.MyProjects, userGuid, SyncDirection.UploadAndDownload);
                }
            }
            catch (SyncException ex)
            {
                Log.ErrorException("Error encountered during synchronization", ex);
                this.CurrentStatus.Error = new SynchronizationException(SyncErrorCodes.InternalError, ex);
                throw new SynchronizationException(SyncErrorCodes.InternalError, ex);
            }
            catch (SqlException ex)
            {
                Log.WarnException("Sql Exception encountered.", ex);
                this.CurrentStatus.Error = new SynchronizationException(SynchronizationException.ParseSqlException(ex), ex);
                throw new SynchronizationException(SynchronizationException.ParseSqlException(ex), ex);
            }
            finally
            {
                var status = this.CurrentStatus.State == SyncState.Aborted ? SynchronizationTaskStatus.Aborted : SynchronizationTaskStatus.Finished;
                this.CurrentStatus.State = this.CurrentStatus.State == SyncState.Aborting
                    ? SyncState.Aborted : SyncState.NotStarted;

                foreach (var conflict in this.Conflicts.ToList())
                {
                    if (conflict.IsResolutionSetByUser)
                    {
                        this.Conflicts.Remove(conflict);
                    }
                }

                if (this.Progress != null)
                {
                    this.ProgressChanged(SynchronizationTask.CompletedAllTasks, status, null);
                }
            }
        }

        /// <summary>
        /// Starts the synchronization process with the last settings for data to be sync and resolves the conflicts; the method actually restarts the last sync, to resolve it's conflicts.
        /// </summary>
        /// <param name="userGuid">The ID of the user whose projects (my projects) should be synced.</param>
        public void ResynchronizeAndResolveConflicts(Guid userGuid)
        {
            this.Synchronize(
                this.CurrentStatus.SyncStaticDataAndSettings,
                this.CurrentStatus.SyncReleasedProjects,
                this.CurrentStatus.SyncMasterData,
                this.CurrentStatus.SyncMyProjects,
                userGuid,
                true);
        }

        /// <summary>
        /// Starts  the Synchronization process.
        /// </summary>
        /// <param name="syncType">Type of the synchronization.</param>
        /// <param name="userGuid">The user GUID.</param>
        /// <param name="direction">The synchronization direction.</param>
        /// <param name="notifyProgress">A value indicating whether to notify the progress when sync is finished, or not.</param>
        /// <exception cref="SynchronizationException">Synchronization Exception</exception>        
        public void Synchronize(SyncType syncType, Guid userGuid, SyncDirection direction, bool notifyProgress = true)
        {
            try
            {
                // The remote(central) and local database must have the same version.
                SyncUtils.ValidateDatabaseVersion();

                if (syncType == SyncType.GlobalSettings)
                {
                    // The Global settings are not synced with SyncFramework because not all rows in the tables must be synced (Version and UpdateSyncScopes for example)
                    // This is a quick workaround - the sync direction is always download and there is no progress notification.
                    SyncGlobalSettings();
                    return;
                }

                // Initiates and controls synchronization sessions.
                this.SynchronizationType = syncType;
                this.progressManager = new SynchronizationProgressManager();
                this.ReportProgressChanged(this.SynchronizationType, SynchronizationTaskStatus.Initializing, this.progressManager.Progress);

                this.Orchestrator = new SyncOrchestrator();
                this.Orchestrator.LocalProvider = ConfigureSqlSyncProvider(syncType, userGuid, this.localConnectionString);
                this.Orchestrator.RemoteProvider = ConfigureSqlSyncProvider(syncType, userGuid, this.centralConnectionString);

                // The direction is combined with the relative positions of the synchronization providers
                // to determine the flow of changes during synchronization. 
                // Upload means changes originate from the local provider and are applied to the
                // remote provider. Download means changes originate from the remote provider and are 
                // applied to the local provider.
                this.Orchestrator.Direction = GetSyncDirectionOrder(direction);

                ((SqlSyncProvider)Orchestrator.LocalProvider).ApplyChangeFailed += new EventHandler<DbApplyChangeFailedEventArgs>(SynchronizationManagerApplyChangeFailed);
                ((SqlSyncProvider)Orchestrator.RemoteProvider).ApplyChangeFailed += new EventHandler<DbApplyChangeFailedEventArgs>(SynchronizationManagerApplyChangeFailed);
                ((SqlSyncProvider)Orchestrator.LocalProvider).SyncProgress += new EventHandler<DbSyncProgressEventArgs>(SynchronizationProgressChanged);
                ((SqlSyncProvider)Orchestrator.RemoteProvider).SyncProgress += new EventHandler<DbSyncProgressEventArgs>(SynchronizationProgressChanged);
                ((SqlSyncProvider)Orchestrator.LocalProvider).ChangesSelected += new EventHandler<DbChangesSelectedEventArgs>(SynchronizationManager_ChangesSelected);
                ((SqlSyncProvider)Orchestrator.RemoteProvider).ChangesSelected += new EventHandler<DbChangesSelectedEventArgs>(SynchronizationManager_ChangesSelected);

                this.Orchestrator.StateChanged += new EventHandler<SyncOrchestratorStateChangedEventArgs>(OrchestratorStateChanged);

                this.SyncRetryGuids = new List<Guid>();
                this.DoSyncRetry = false;
                this.StopSyncRetry = false;
                this.IsSyncRetrySession = false;

                this.SynchronizeWork(0);

                if (syncType == SyncType.MyProjects)
                {
                    SynchronizeOfflineProjects(userGuid);
                }

                if (notifyProgress && this.progressManager != null)
                {
                    this.ReportProgressChanged(syncType, SynchronizationTaskStatus.Finished, this.progressManager.Progress);
                }
            }
            catch (SyncAbortedException)
            {
                // Sync Aborted by User  
                if (this.progressManager != null)
                {
                    this.ReportProgressChanged(syncType, SynchronizationTaskStatus.Aborted, this.progressManager.Progress);
                }
            }
            catch (SyncException ex)
            {
                /*
                 * Check if the exception thrown relates somehow to the abort action, and if so, 
                 * do not consider the exception as an internal synchronization error.
                 */
                var isAbortException = false;
                Exception innerException = ex;

                do
                {
                    isAbortException = string.Equals(innerException.Message, AbortExceptionMessage);
                    innerException = innerException.InnerException;
                }
                while (innerException != null && !isAbortException);

                if (!isAbortException)
                {
                    Log.ErrorException("Sync Exception occurred.", ex);
                    this.CurrentStatus.Error = new SynchronizationException(SyncErrorCodes.InternalError, ex);
                    throw new SynchronizationException(SyncErrorCodes.InternalError, ex);
                }
            }
        }

        /// <summary>
        /// Gets the <see cref="Microsoft.Synchronization.SyncDirectionOrder"/> corresponding to the specified <see cref="ZPKTool.Synchronization.SyncDirection"/>
        /// </summary>
        /// <param name="direction">The direction.</param>
        /// <returns>Sync direction.</returns>
        private SyncDirectionOrder GetSyncDirectionOrder(SyncDirection direction)
        {
            var syncDirection = SyncDirectionOrder.Download;

            switch (direction)
            {
                case SyncDirection.Download:
                    syncDirection = SyncDirectionOrder.Download;
                    break;
                case SyncDirection.DownloadAndUpload:
                    syncDirection = SyncDirectionOrder.DownloadAndUpload;
                    break;
                case SyncDirection.Upload:
                    syncDirection = SyncDirectionOrder.Upload;
                    break;
                case SyncDirection.UploadAndDownload:
                    syncDirection = SyncDirectionOrder.UploadAndDownload;
                    break;
            }

            return syncDirection;
        }

        /// <summary>
        /// Synchronizes the left work.
        /// </summary>
        /// <param name="retryConflictCount">The retry conflict count.</param>
        private void SynchronizeWork(int retryConflictCount)
        {
            // Synchronize data between the two providers.
            this.Orchestrator.Synchronize();

            if (this.DoSyncRetry)
            {
                this.IsSyncRetrySession = true;

                if (this.SyncRetryGuids.Count == retryConflictCount)
                {
                    this.StopSyncRetry = true;
                }

                int retryCount = this.SyncRetryGuids.Count;
                this.SyncRetryGuids = new List<Guid>();
                this.DoSyncRetry = false;

                if (this.progressManager != null)
                {
                    this.progressManager.AddPartialItemCountToTotalItemsCount();
                }

                this.SynchronizeWork(retryCount);
            }
        }

        /// <summary>
        /// Resets the current status.
        /// </summary>
        public void ResetCurrentStatus()
        {
            this.CurrentStatus = new SyncStatus();
        }

        /// <summary>
        /// Initializes the current status when the synchronization is started.
        /// </summary>
        /// <param name="syncStaticDataAndSettings">specifies that static data and settings will be synchronized.</param>
        /// <param name="syncReleasedProjects">specifies released projects will be synchronized.</param>
        /// <param name="syncMasterData">specifies that masterdata will be synchronized.</param>
        /// <param name="syncMyProjects">specifies that my projects be synchronized.</param>
        private void InitializeCurrentStatus(
            bool syncStaticDataAndSettings,
            bool syncReleasedProjects,
            bool syncMasterData,
            bool syncMyProjects)
        {
            this.CurrentStatus.StartTime = DateTime.Now;
            this.CurrentStatus.State = SyncState.Running;

            this.CurrentStatus.TasksCount = 0;
            this.CurrentStatus.TasksCompletedCount = 0;
            this.CurrentStatus.TotalItemsCount = 0;
            this.CurrentStatus.ProcessedItemsCount = 0;
            this.CurrentStatus.TotalConflictsCount = 0;

            this.CurrentStatus.SyncStaticDataAndSettings = syncStaticDataAndSettings;
            this.CurrentStatus.SyncReleasedProjects = syncReleasedProjects;
            this.CurrentStatus.SyncMasterData = syncMasterData;
            this.CurrentStatus.SyncMyProjects = syncMyProjects;

            if (this.CurrentStatus.SyncReleasedProjects)
            {
                this.CurrentStatus.TasksCount++;
            }

            if (this.CurrentStatus.SyncMyProjects)
            {
                if (this.CurrentStatus.SyncStaticDataAndSettings)
                {
                    this.CurrentStatus.TasksCount++;
                }
                else
                {
                    this.CurrentStatus.TasksCount += 2;
                }
            }

            if (this.CurrentStatus.SyncMasterData)
            {
                this.CurrentStatus.TasksCount++;
            }

            if (this.CurrentStatus.SyncStaticDataAndSettings)
            {
                this.CurrentStatus.TasksCount++;
            }
        }

        #endregion

        #region Conflict Handling

        /// <summary>
        /// Handles the ApplyChangeFailed event of the SyncManager control.
        /// If a row cannot be applied during synchronization, the ApplyChangeFailed event is raised. 
        /// The ApplyChangeFailedEventArgs object provides information about the error or conflict that caused the failure.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Microsoft.Synchronization.Data.DbApplyChangeFailedEventArgs"/> instance containing the event data.</param>
        [SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Justification = "Basically it contains a big switch statement.")]
        private void SynchronizationManagerApplyChangeFailed(object sender, DbApplyChangeFailedEventArgs e)
        {
            var syncConflict = e.Conflict;
            if (syncConflict == null)
            {
                return;
            }

            DataTable localChangeAbsoluteVersion = null;
            DataTable centralChangeAbsoluteVersion = null;

            this.GetAbsoluteVersionFromRelativePosition(syncConflict, out localChangeAbsoluteVersion, out centralChangeAbsoluteVersion);
            var itemGuid = GetConflictItemGuid(e, syncConflict);

            if (syncConflict.Type == DbConflictType.ErrorsOccurred)
            {
                if (!this.StopSyncRetry)
                {
                    this.DoSyncRetry = true;

                    if (!this.SyncRetryGuids.Contains(itemGuid))
                    {
                        this.SyncRetryGuids.Add(itemGuid);
                    }

                    e.Action = ApplyAction.RetryNextSync;
                }
                else
                {
                    this.DoSyncRetry = false;

                    string errorMessage = "Could not sync: " + Enum.GetName(typeof(SyncType), this.SynchronizationType) + ". Owner or MasterData flag of the parent entity is not set correctly. " + e.Conflict.ErrorMessage;

                    if (localChangeAbsoluteVersion != null && localChangeAbsoluteVersion.Rows[0] != null && localChangeAbsoluteVersion.Rows[0].ItemArray.FirstOrDefault(i => i.GetType() == typeof(Guid)) != null)
                    {
                        string localInf = localChangeAbsoluteVersion.Rows[0].ItemArray.FirstOrDefault(i => i.GetType() == typeof(Guid)).ToString();
                        errorMessage += " LocalEntityGuid: " + localInf;
                    }

                    if (centralChangeAbsoluteVersion != null && centralChangeAbsoluteVersion.Rows[0] != null && centralChangeAbsoluteVersion.Rows[0].ItemArray.FirstOrDefault(i => i.GetType() == typeof(Guid)) != null)
                    {
                        string remoteInf = centralChangeAbsoluteVersion.Rows[0].ItemArray.FirstOrDefault(i => i.GetType() == typeof(Guid)).ToString();
                        errorMessage += " CentralEntityGuid: " + remoteInf;
                    }

                    Log.Error(errorMessage);
                }
            }
            else if (syncConflict.Type == DbConflictType.LocalCleanedupDeleteRemoteUpdate)
            {
                e.Action = ApplyAction.RetryWithForceWrite;
            }
            else if (syncConflict.Type == DbConflictType.LocalDeleteRemoteDelete)
            {
                if (this.SynchronizationType == SyncType.MyProjects)
                {
                    e.Action = ApplyAction.RetryWithForceWrite;
                }
                else
                {
                    e.Action = ApplyAction.Continue;
                }
            }
            else if (syncConflict.Type == DbConflictType.LocalDeleteRemoteUpdate
                || e.Conflict.Type == DbConflictType.LocalUpdateRemoteDelete)
            {
                e.Action = ApplyAction.RetryWithForceWrite;
            }
            else if (syncConflict.Type == DbConflictType.LocalInsertRemoteInsert
                || e.Conflict.Type == DbConflictType.LocalUpdateRemoteUpdate)
            {
                this.CheckIfIdenticalAndResolveConflict(e, localChangeAbsoluteVersion, centralChangeAbsoluteVersion, itemGuid);
            }
        }

        /// <summary>
        /// Sets the resolution.
        /// </summary>
        /// <param name="e">The <see cref="Microsoft.Synchronization.Data.DbApplyChangeFailedEventArgs"/> instance containing the event data.</param>
        /// <param name="conflict">The conflict.</param>
        private void SetResolutionForConflict(DbApplyChangeFailedEventArgs e, SynchronizationConflict conflict)
        {
            if (e != null && conflict != null)
            {
                if (conflict.IsResolutionSetByUser)
                {
                    if (conflict.Resolution == SynchronizationConflictResolution.Ignore)
                    {
                        e.Action = ApplyAction.RetryNextSync;
                    }
                    else if (conflict.Resolution == SynchronizationConflictResolution.LocalVersionWins)
                    {
                        if (this.Orchestrator.State == SyncOrchestratorState.Uploading)
                        {
                            e.Action = ApplyAction.RetryWithForceWrite;
                        }
                        else if (this.Orchestrator.State == SyncOrchestratorState.Downloading)
                        {
                            e.Action = ApplyAction.Continue;
                        }
                    }
                    else if (conflict.Resolution == SynchronizationConflictResolution.CentralVersionWins)
                    {
                        if (this.Orchestrator.State == SyncOrchestratorState.Uploading)
                        {
                            e.Action = ApplyAction.Continue;
                        }
                        else if (this.Orchestrator.State == SyncOrchestratorState.Downloading)
                        {
                            e.Action = ApplyAction.RetryWithForceWrite;
                        }
                    }
                }
                else
                {
                    e.Action = ApplyAction.RetryNextSync;
                }
            }
        }

        /// <summary>
        /// Adds the conflict to the conflicts list if it wasn't added before.
        /// </summary>
        /// <param name="localChangeAbsoluteVersion">The local change absolute version.</param>
        /// <param name="centralChangeAbsoluteVersion">The central change absolute version.</param>
        /// <param name="itemGuid">The item GUID.</param>
        /// <returns>
        /// A synchronization conflict.
        /// </returns>
        private SynchronizationConflict AddConflictIfNeeded(DataTable localChangeAbsoluteVersion, DataTable centralChangeAbsoluteVersion, Guid itemGuid)
        {
            var conflict = new SynchronizationConflict();
            conflict.IsResolutionSetByUser = false;

            // set the conflict synchronization task.
            conflict.SyncTask = this.SynchronizationType;

            if (localChangeAbsoluteVersion != null)
            {
                if (!string.IsNullOrEmpty(localChangeAbsoluteVersion.TableName))
                {
                    conflict.EntityType = localChangeAbsoluteVersion.TableName;
                }

                if (localChangeAbsoluteVersion.Rows != null && localChangeAbsoluteVersion.Rows.Count > 0)
                {
                    conflict.LocalEntityVersion = localChangeAbsoluteVersion.Rows[0];
                }
            }

            if (centralChangeAbsoluteVersion != null)
            {
                if (!string.IsNullOrEmpty(conflict.EntityType))
                {
                    if (!string.IsNullOrEmpty(localChangeAbsoluteVersion.TableName))
                    {
                        conflict.EntityType = localChangeAbsoluteVersion.TableName;
                    }
                }

                if (centralChangeAbsoluteVersion.Rows != null && centralChangeAbsoluteVersion.Rows.Count > 0)
                {
                    conflict.CentralEntityVersion = centralChangeAbsoluteVersion.Rows[0];
                }
            }

            // set the conflict item guid.
            conflict.EntityGuid = itemGuid;

            if (this.Conflicts != null)
            {
                if (this.Conflicts.FirstOrDefault(p => p.EntityGuid == conflict.EntityGuid) == null)
                {
                    this.progressManager.PartialConflictsItemsCount = this.progressManager.PartialConflictsItemsCount + 1;
                    ReportProgressChanged(this.SynchronizationType, SynchronizationTaskStatus.ApplyingChanges, this.progressManager.Progress);
                    this.Conflicts.Add(conflict);
                }
                else
                {
                    return this.Conflicts.FirstOrDefault(p => p.EntityGuid == conflict.EntityGuid);
                }
            }

            return conflict;
        }

        /// <summary>
        /// Checks if identical and resolve conflict.
        /// </summary>
        /// <param name="e">The <see cref="Microsoft.Synchronization.Data.DbApplyChangeFailedEventArgs"/> instance containing the event data.</param>
        /// <param name="localChangeAbsoluteVersion">The local change absolute version.</param>
        /// <param name="centralChangeAbsoluteVersion">The central change absolute version.</param>
        /// <param name="itemGuid">The item GUID.</param>
        private void CheckIfIdenticalAndResolveConflict(DbApplyChangeFailedEventArgs e, DataTable localChangeAbsoluteVersion, DataTable centralChangeAbsoluteVersion, Guid itemGuid)
        {
            if (localChangeAbsoluteVersion != null &&
                localChangeAbsoluteVersion.Rows[0] != null &&
                centralChangeAbsoluteVersion != null &&
                centralChangeAbsoluteVersion.Rows[0] != null)
            {
                var localChange = localChangeAbsoluteVersion.Rows[0];
                var centralChange = centralChangeAbsoluteVersion.Rows[0];

                var identical = true;
                for (int index = 0; index < localChange.Table.Columns.Count - 4; index++)
                {
                    var local = localChange[index].ToString();
                    var remote = centralChange[index].ToString();

                    if (!local.Equals(remote))
                    {
                        identical = false;
                        break;
                    }
                }

                if (identical)
                {
                    e.Action = ApplyAction.Continue;
                }
                else
                {
                    if (this.SynchronizationType == SyncType.StaticDataAndSettings
                        || this.SynchronizationType == SyncType.Logs
                        || this.SynchronizationType == SyncType.MasterData
                        || this.SynchronizationType == SyncType.User
                        || this.SynchronizationType == SyncType.ReleasedProjects)
                    {
                        e.Action = ApplyAction.RetryWithForceWrite;
                    }
                    else
                    {
                        var conflict = AddConflictIfNeeded(localChangeAbsoluteVersion, centralChangeAbsoluteVersion, itemGuid);
                        SetResolutionForConflict(e, conflict);
                    }
                }
            }
        }

        /// <summary>
        /// Gets the conflict item GUID.
        /// </summary>
        /// <param name="e">The <see cref="Microsoft.Synchronization.Data.DbApplyChangeFailedEventArgs"/> instance containing the event data.</param>
        /// <param name="syncConflict">The sync conflict.</param>
        /// <returns>The conflict item guid.</returns>
        private Guid GetConflictItemGuid(DbApplyChangeFailedEventArgs e, DbSyncConflict syncConflict)
        {
            var itemGuid = Guid.Empty;

            if (syncConflict.LocalChange != null &&
                syncConflict.LocalChange.Rows[0] != null &&
                syncConflict.LocalChange.Rows[0].ItemArray.FirstOrDefault(i => i.GetType() == typeof(Guid)) != null)
            {
                itemGuid = (Guid)e.Conflict.LocalChange.Rows[0].ItemArray.FirstOrDefault(i => i.GetType() == typeof(Guid));
            }

            if (syncConflict.RemoteChange != null &&
              syncConflict.RemoteChange.Rows[0] != null &&
              syncConflict.RemoteChange.Rows[0].ItemArray.FirstOrDefault(i => i.GetType() == typeof(Guid)) != null)
            {
                itemGuid = (Guid)e.Conflict.RemoteChange.Rows[0].ItemArray.FirstOrDefault(i => i.GetType() == typeof(Guid));
            }

            return itemGuid;
        }

        /// <summary>
        /// Gets the absolute version from relative position.
        /// </summary>
        /// <param name="syncConflict">The sync conflict.</param>
        /// <param name="localChangeAbsoluteVersion">The local change absolute version.</param>
        /// <param name="centralChangeAbsoluteVersion">The central change absolute version.</param>
        private void GetAbsoluteVersionFromRelativePosition(DbSyncConflict syncConflict, out DataTable localChangeAbsoluteVersion, out DataTable centralChangeAbsoluteVersion)
        {
            localChangeAbsoluteVersion = null;
            centralChangeAbsoluteVersion = null;

            if (this.Orchestrator.State == SyncOrchestratorState.Downloading)
            {
                localChangeAbsoluteVersion = syncConflict.LocalChange;
                centralChangeAbsoluteVersion = syncConflict.RemoteChange;
            }
            else if (this.Orchestrator.State == SyncOrchestratorState.Uploading)
            {
                localChangeAbsoluteVersion = syncConflict.RemoteChange;
                centralChangeAbsoluteVersion = syncConflict.LocalChange;
            }
        }

        #endregion

        #region Configure Sql Provider

        /// <summary>
        /// Configures the SQL synchronization provider.
        /// </summary>
        /// <param name="syncType">Type of the synchronization.</param>
        /// <param name="userGuid">The current user GUID, For Master Data and Static Data and Settings the User Guid can be null.</param>
        /// <param name="sqlConnectionString">The SQL connection string.</param>
        /// <returns>
        /// The configured sql sync provider.
        /// </returns>
        private SqlSyncProvider ConfigureSqlSyncProvider(SyncType syncType, Guid userGuid, string sqlConnectionString)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(sqlConnectionString))
                {
                    // Represents a synchronization provider that communicates with a SQL Server database and 
                    // shields other Sync Framework components from the specific implementation of the database. 
                    var provider = new SqlSyncProvider();

                    // For peer-to-peer database synchronization, a scope is typically a set of tables.
                    var scopeName = string.Empty;

                    switch (syncType)
                    {
                        case SyncType.StaticDataAndSettings:
                        case SyncType.MasterData:
                        case SyncType.ReleasedProjects:
                        case SyncType.Logs:
                            scopeName = Enum.GetName(typeof(SyncType), syncType);
                            break;
                        case SyncType.User:
                        case SyncType.MyProjects:
                            scopeName = Enum.GetName(typeof(SyncType), syncType) + "-" + userGuid;
                            break;
                    }

                    // If there is missing metadata continue the synchronization process with a partial synch instead of aborting it.
                    provider.SyncPeerOutdated += (s, e) => { e.Action = DbOutdatedSyncAction.PartialSync; };

                    // The maximum amount of memory, in KB, that Sync Framework uses to cache changes before spooling those changes to disk.
                    provider.MemoryDataCacheSize = 100000;

                    // The maximum transaction size used during change application, in kilobytes. 
                    provider.ApplicationTransactionSize = 50000;

                    // By describing a sync scope, you define what you want to synchronize. 
                    // A sync scope is a set of tables that must be synchronized as a single unit.
                    // The tables can already exist in the database or they can be described by 
                    // using the Sync Framework object model and then be generated at runtime 
                    // when the underlying store is provisioned. 
                    provider.ScopeName = scopeName;
                    provider.Connection = sqlConnection;
                    provider.CommandTimeout = SqlCommandTimeout;

                    // class to be used to provision the scope defined above
                    // The provisioning of a database involves adding sync scope related artifacts such as tracking tables, 
                    // triggers, and stored procedures to the database. These artifacts are used by the synchronization process at runtime. 
                    var serverConfig = new SqlSyncScopeProvisioning((SqlConnection)provider.Connection);
                    serverConfig.CommandTimeout = SqlCommandTimeout;

                    if (syncType == SyncType.StaticDataAndSettings)
                    {
                        ProvisionTemplateForStaticDataAndSettings(sqlConnection, scopeName, sqlConnectionString);
                        PopulateStaticDataAndSettingsScopeFromTemplate(provider, scopeName, serverConfig, false, sqlConnectionString);
                    }
                    else if (syncType == SyncType.Logs)
                    {
                        ProvisionTemplateForLogs(sqlConnection, scopeName, sqlConnectionString);
                        PopulateLogsScopeFromTemplate(provider, scopeName, serverConfig, sqlConnectionString);
                    }
                    else if (syncType == SyncType.MyProjects)
                    {
                        ProvisionTemplateForMyProjects(sqlConnection, scopeName, sqlConnectionString);
                        PopulateMyProjectsScopeFromTemplate(provider, scopeName, serverConfig, false, false, userGuid, sqlConnectionString);
                    }
                    else if (syncType == SyncType.MasterData)
                    {
                        ProvisionTemplateForMasterData(sqlConnection, scopeName, sqlConnectionString);
                        PopulateMasterDataScopeFromTemplate(provider, scopeName, serverConfig, true, sqlConnectionString);
                    }
                    else if (syncType == SyncType.ReleasedProjects)
                    {
                        ProvisionTemplateForReleasedProjects(sqlConnection, scopeName, sqlConnectionString);
                        PopulateReleasedProjectsScopeFromTemplate(provider, scopeName, serverConfig, false, true, sqlConnectionString);
                    }
                    else if (syncType == SyncType.User)
                    {
                        ProvisionTemplateForUser(sqlConnection, scopeName, sqlConnectionString);
                        PopulateUserFromTemplate(provider, scopeName, serverConfig, userGuid, sqlConnectionString);
                    }

                    return provider;
                }
            }
            catch (SyncException ex)
            {
                Log.ErrorException("Could not configure Sql sync provider.", ex);
                throw new SynchronizationException(SyncErrorCodes.InternalError, ex);
            }
            catch (SqlException ex)
            {
                Log.ErrorException("SQL Exception encountered.", ex);
                throw new SynchronizationException(SynchronizationException.ParseSqlException(ex), ex);
            }
        }

        #endregion

        #region User Synchronization

        /// <summary>
        /// Sets the template-based scope description from which the database should be provisioned.
        /// Templates are used to describe a parameter-based filtered scope.
        /// A filtered scope is then created by using PopulateFromTemplate to retrieve the template
        /// description, and by defining specific values for the parameters described in the template.
        /// </summary>
        /// <param name="provider">The synchronization provider.</param>
        /// <param name="scopeName">Name of the scope.</param>
        /// <param name="serverConfig">The server scope provisioning.</param>
        /// <param name="userGuid">The user GUID.</param>
        /// <param name="sqlConnectionString">The SQL connection string.</param>
        private void PopulateUserFromTemplate(SqlSyncProvider provider, string scopeName, SqlSyncScopeProvisioning serverConfig, Guid userGuid, string sqlConnectionString)
        {
            try
            {
                // Sets the template-based scope description from which the database should be provisioned.
                serverConfig.PopulateFromTemplate(provider.ScopeName, Enum.GetName(typeof(SyncTemplates), SyncTemplates.User_Template));
                serverConfig.Tables["Users"].FilterParameters["@ownerGuid"].Value = userGuid;

                if (!serverConfig.ScopeExists(scopeName))
                {
                    serverConfig.Apply();
                }
                else
                {
                    HandleScopeParametersChange(scopeName, serverConfig, sqlConnectionString);
                }
            }
            catch (DbProvisioningException ex)
            {
                Log.WarnException("You tried to synchronize a scope that is currently being provisioned to a SQL Server.", ex);
                throw new SynchronizationException(SyncErrorCodes.InternalError, ex);
            }
            catch (SyncException ex)
            {
                Log.ErrorException("Could not populate scope from template", ex);
                throw new SynchronizationException(SyncErrorCodes.InternalError, ex);
            }
            catch (SqlException ex)
            {
                Log.WarnException("SQL Exception encountered.", ex);
                throw new SynchronizationException(SynchronizationException.ParseSqlException(ex), ex);
            }
        }

        /// <summary>
        /// Provisions the template for user.
        /// After you define the synchronization scope, you provision the database to create a change-tracking and
        /// metadata management infrastructure that consists of metadata tables, triggers, and stored procedures.
        /// </summary>
        /// <param name="sqlConnection">The SQL connection.</param>
        /// <param name="scopeName">Name of the scope.</param>
        /// <param name="sqlConnectionString">The SQL connection string.</param>
        private void ProvisionTemplateForUser(SqlConnection sqlConnection, string scopeName, string sqlConnectionString)
        {
            try
            {
                var templateName = Enum.GetName(typeof(SyncTemplates), SyncTemplates.User_Template);

                // create a new scope description and add the appropriate tables to this scope
                var scopeDesc = new DbSyncScopeDescription(templateName);

                // class to be used to provision the scope defined above
                var serverConfig = new SqlSyncScopeProvisioning(sqlConnection);

                // For each table that is synchronized, changes are selected from the server database in the order of inserts, updates, 
                // and then deletes. Changes are applied to the client database in the order of deletes, inserts, and then updates. 
                // When several tables are synchronized, the order in which each table is processed depends on
                // the order in which its SyncTable object was added to the collection of tables for the synchronization agent.
                var usersTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Users", sqlConnection);
                scopeDesc.Tables.Add(usersTableDesc);

                // Represents the provisioning of a SQL Server database for a particular scope that is represented by a DbSyncScopeDescription object. 
                var serverTemplate = new SqlSyncScopeProvisioning(sqlConnection, scopeDesc, SqlSyncScopeProvisioningType.Template);

                var ownerParam = new SqlParameter("@ownerGuid", SqlDbType.UniqueIdentifier);

                // Add a filter column to a SqlSyncTableProvisioning object in the 
                // synchronization scope by using AddFilterColumn. 
                // This adds the filter column to the tracking table that tracks changes
                // for the base table.

                // Define one or more filter parameters by adding SqlParameter objects to the
                // FilterParameters collection of the SqlSyncTableProvisioning object. 
                // This adds the specified parameters to the argument list of the stored
                // procedure that enumerates changes during synchronization.

                // Add a filter clause that defines the relationship between parameter values
                // and column values by setting the FilterClause property of the
                // SqlSyncTableProvisioning object. The filter clause is a WHERE clause 
                // without the WHERE keyword. The [side] alias is an alias for the tracking 
                // table. The parameters match the parameters specified in the FilterParameters
                // collection. At this point you are only defining the relationship between 
                // the filter parameters and columns. The actual values for the parameters 
                // will be specified later, when the filtered scope is created.
                serverTemplate.Tables["Users"].AddFilterColumn("Guid");
                serverTemplate.Tables["Users"].FilterClause = "[side].[Guid] = @ownerGuid";
                serverTemplate.Tables["Users"].FilterParameters.Add(ownerParam);

                // Sets whether to create a set of stored procedures that insert, update, and delete data and synchronization metadata.
                serverTemplate.SetCreateTrackingTableDefault(DbSyncCreationOption.CreateOrUseExisting);

                // Sets whether to create on base tables triggers that update tracking tables.
                serverTemplate.SetCreateTriggersDefault(DbSyncCreationOption.CreateOrUseExisting);

                // Sets whether to create a set of stored procedures that insert, update, and delete data and synchronization metadata.
                serverTemplate.SetCreateProceduresDefault(DbSyncCreationOption.CreateOrUseExisting);

                // Sets whether to create base tables when a scope is configured.
                serverTemplate.SetCreateTableDefault(DbSyncCreationOption.CreateOrUseExisting);

                // Sets whether to insert metadata into change-tracking tables for rows that already exist in base tables.
                serverTemplate.SetPopulateTrackingTableDefault(DbSyncCreationOption.CreateOrUseExisting);

                // create a new select changes stored proc for this scope
                // Sets whether to create for any additional scopes sets of stored 
                // procedures that insert, update, and delete data and synchronization metadata.
                serverTemplate.SetCreateProceduresForAdditionalScopeDefault(DbSyncCreationOption.Create);

                if (!serverConfig.TemplateExists(templateName))
                {
                    // Create the template in the database.
                    serverTemplate.Apply();
                }
                else
                {
                    HandleSchemaChanges(sqlConnectionString, scopeName, scopeDesc, serverTemplate);
                }
            }
            catch (SyncException ex)
            {
                Log.ErrorException("Could not configure sql sync provider.", ex);
                throw new SynchronizationException(SyncErrorCodes.InternalError, ex);
            }
            catch (SqlException ex)
            {
                Log.ErrorException("SQL Exception encountered.", ex);
                throw new SynchronizationException(SynchronizationException.ParseSqlException(ex), ex);
            }
        }

        #endregion

        #region Released Projects Synchronization

        /// <summary>
        /// Provisions the template for released projects.
        /// After you define the synchronization scope, you provision the database to create a change-tracking and
        /// metadata management infrastructure that consists of metadata tables, triggers, and stored procedures.
        /// </summary>
        /// <param name="sqlConnection">The SQL connection.</param>
        /// <param name="scopeName">Name of the scope.</param>
        /// <param name="sqlConnectionString">The SQL connection string.</param>
        private void ProvisionTemplateForReleasedProjects(SqlConnection sqlConnection, string scopeName, string sqlConnectionString)
        {
            try
            {
                string templateName = Enum.GetName(typeof(SyncTemplates), SyncTemplates.ReleasedProjects_Template);

                // create a new scope description and add the appropriate tables to this scope
                var scopeDesc = new DbSyncScopeDescription(templateName);

                // class to be used to provision the scope defined above
                var serverConfig = new SqlSyncScopeProvisioning(sqlConnection);

                // Represents the schema of a table that is included in the Tables list of a DbSyncScopeDescription object. This is used during database provisioning. 
                // For each table that is synchronized, changes are selected from the server database in the order of inserts, updates, 
                // and then deletes. Changes are applied to the client database in the order of deletes, inserts, and then updates. 
                // When several tables are synchronized, the order in which each table is processed depends on
                // the order in which its SyncTable object was added to the collection of tables for the synchronization agent.                                      
                DbSyncTableDescription usersTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Users", sqlConnection);
                scopeDesc.Tables.Add(usersTableDesc);
                DbSyncTableDescription currenciesTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Currencies", sqlConnection);
                scopeDesc.Tables.Add(currenciesTableDesc);
                DbSyncTableDescription countriesTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Countries", sqlConnection);
                scopeDesc.Tables.Add(countriesTableDesc);
                DbSyncTableDescription processStepsClassificationTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("ProcessStepsClassification", sqlConnection);
                scopeDesc.Tables.Add(processStepsClassificationTableDesc);
                DbSyncTableDescription machinesClassificationTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("MachinesClassification", sqlConnection);
                scopeDesc.Tables.Add(machinesClassificationTableDesc);
                DbSyncTableDescription materialsClassificationTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("MaterialsClassification", sqlConnection);
                scopeDesc.Tables.Add(materialsClassificationTableDesc);
                DbSyncTableDescription measurementUnitsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("MeasurementUnits", sqlConnection);
                scopeDesc.Tables.Add(measurementUnitsTableDesc);
                DbSyncTableDescription countrySettingsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("CountrySettings", sqlConnection);
                scopeDesc.Tables.Add(countrySettingsTableDesc);
                DbSyncTableDescription customersTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Customers", sqlConnection);
                scopeDesc.Tables.Add(customersTableDesc);
                DbSyncTableDescription overheadSettingsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("OverheadSettings", sqlConnection);
                scopeDesc.Tables.Add(overheadSettingsTableDesc);
                DbSyncTableDescription manufacturersTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Manufacturers", sqlConnection);
                scopeDesc.Tables.Add(manufacturersTableDesc);
                DbSyncTableDescription mediaTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Media", sqlConnection);
                scopeDesc.Tables.Add(mediaTableDesc);
                DbSyncTableDescription processesTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Processes", sqlConnection);
                scopeDesc.Tables.Add(processesTableDesc);
                DbSyncTableDescription projectFoldersTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("ProjectFolders", sqlConnection);
                scopeDesc.Tables.Add(projectFoldersTableDesc);
                DbSyncTableDescription projectsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Projects", sqlConnection);
                scopeDesc.Tables.Add(projectsTableDesc);
                DbSyncTableDescription assembliesTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Assemblies", sqlConnection);
                scopeDesc.Tables.Add(assembliesTableDesc);
                DbSyncTableDescription partsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Parts", sqlConnection);
                scopeDesc.Tables.Add(partsTableDesc);
                DbSyncTableDescription assemblyMediaTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("AssemblyMedia", sqlConnection);
                scopeDesc.Tables.Add(assemblyMediaTableDesc);
                DbSyncTableDescription partMediaTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("PartMedia", sqlConnection);
                scopeDesc.Tables.Add(partMediaTableDesc);
                DbSyncTableDescription projectMediaTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("ProjectMedia", sqlConnection);
                scopeDesc.Tables.Add(projectMediaTableDesc);
                DbSyncTableDescription processStepsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("ProcessSteps", sqlConnection);
                scopeDesc.Tables.Add(processStepsTableDesc);
                DbSyncTableDescription cycleTimeCalculationsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("CycleTimeCalculations", sqlConnection);
                scopeDesc.Tables.Add(cycleTimeCalculationsTableDesc);
                DbSyncTableDescription consumablesTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Consumables", sqlConnection);
                scopeDesc.Tables.Add(consumablesTableDesc);
                DbSyncTableDescription diesTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Dies", sqlConnection);
                scopeDesc.Tables.Add(diesTableDesc);
                DbSyncTableDescription rawMaterialsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("RawMaterials", sqlConnection);
                scopeDesc.Tables.Add(rawMaterialsTableDesc);
                DbSyncTableDescription machinesTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Machines", sqlConnection);
                scopeDesc.Tables.Add(machinesTableDesc);
                DbSyncTableDescription commoditiesTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Commodities", sqlConnection);
                scopeDesc.Tables.Add(commoditiesTableDesc);
                DbSyncTableDescription processStepPartAmountsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("ProcessStepPartAmounts", sqlConnection);
                scopeDesc.Tables.Add(processStepPartAmountsTableDesc);
                DbSyncTableDescription processStepAssemblyAmountsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("ProcessStepAssemblyAmounts", sqlConnection);
                scopeDesc.Tables.Add(processStepAssemblyAmountsTableDesc);
                DbSyncTableDescription projectCurrenciesTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("ProjectCurrencies", sqlConnection);
                scopeDesc.Tables.Add(projectCurrenciesTableDesc);
                DbSyncTableDescription transportCostCalculationsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("TransportCostCalculations", sqlConnection);
                scopeDesc.Tables.Add(transportCostCalculationsTableDesc);
                DbSyncTableDescription transportCostCalculationSettingsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("TransportCostCalculationSettings", sqlConnection);
                scopeDesc.Tables.Add(transportCostCalculationSettingsTableDesc);

                // Represents the provisioning of a SQL Server database for a particular scope that is represented by a DbSyncScopeDescription object. 
                var serverTemplate = new SqlSyncScopeProvisioning(sqlConnection, scopeDesc, SqlSyncScopeProvisioningType.Template);

                // Add a filter column to a SqlSyncTableProvisioning object in the 
                // synchronization scope by using AddFilterColumn. 
                // This adds the filter column to the tracking table that tracks changes
                // for the base table.

                // Define one or more filter parameters by adding SqlParameter objects to the
                // FilterParameters collection of the SqlSyncTableProvisioning object. 
                // This adds the specified parameters to the argument list of the stored
                // procedure that enumerates changes during synchronization.

                // Add a filter clause that defines the relationship between parameter values
                // and column values by setting the FilterClause property of the
                // SqlSyncTableProvisioning object. The filter clause is a WHERE clause 
                // without the WHERE keyword. The [side] alias is an alias for the tracking 
                // table. The parameters match the parameters specified in the FilterParameters
                // collection. At this point you are only defining the relationship between 
                // the filter parameters and columns. The actual values for the parameters 
                // will be specified later, when the filtered scope is created.                
                var isMasterDataParam = new SqlParameter("@isMasterData", SqlDbType.Bit);
                var isReleasedParam = new SqlParameter("@isReleased", SqlDbType.Bit);

                serverTemplate.Tables["Users"].AddFilterColumn("IsReleased");
                serverTemplate.Tables["Users"].FilterClause = "[side].[IsReleased] = @isReleased";
                serverTemplate.Tables["Users"].FilterParameters.Add(isReleasedParam);

                serverTemplate.Tables["ProcessStepsClassification"].AddFilterColumn("IsReleased");
                serverTemplate.Tables["ProcessStepsClassification"].AddFilterColumn("Guid");
                serverTemplate.Tables["ProcessStepsClassification"].FilterClause = "[side].[IsReleased] = @isReleased";
                serverTemplate.Tables["ProcessStepsClassification"].FilterParameters.Add(isReleasedParam);

                serverTemplate.Tables["MachinesClassification"].AddFilterColumn("IsReleased");
                serverTemplate.Tables["MachinesClassification"].AddFilterColumn("Guid");
                serverTemplate.Tables["MachinesClassification"].FilterClause = "[side].[IsReleased] = @isReleased";
                serverTemplate.Tables["MachinesClassification"].FilterParameters.Add(isReleasedParam);

                serverTemplate.Tables["MaterialsClassification"].AddFilterColumn("IsReleased");
                serverTemplate.Tables["MaterialsClassification"].AddFilterColumn("Guid");
                serverTemplate.Tables["MaterialsClassification"].FilterClause = "[side].[IsReleased] = @isReleased";
                serverTemplate.Tables["MaterialsClassification"].FilterParameters.Add(isReleasedParam);

                serverTemplate.Tables["MeasurementUnits"].AddFilterColumn("IsReleased");
                serverTemplate.Tables["MeasurementUnits"].AddFilterColumn("Guid");
                serverTemplate.Tables["MeasurementUnits"].FilterClause = "[side].[IsReleased] = @isReleased";
                serverTemplate.Tables["MeasurementUnits"].FilterParameters.Add(isReleasedParam);

                serverTemplate.Tables["Currencies"].AddFilterColumn("IsReleased");
                serverTemplate.Tables["Currencies"].AddFilterColumn("Guid");
                serverTemplate.Tables["Currencies"].FilterClause = "[side].[IsReleased] = @isReleased";
                serverTemplate.Tables["Currencies"].FilterClause = "[side].[Guid] IN (SELECT [Guid] FROM GetCurrencyHierarchyForReleasedProjectsSync())";
                serverTemplate.Tables["Currencies"].FilterParameters.Add(isReleasedParam);

                serverTemplate.Tables["Countries"].AddFilterColumn("IsReleased");
                serverTemplate.Tables["Countries"].AddFilterColumn("Guid");
                serverTemplate.Tables["Countries"].AddFilterColumn("SettingsGuid");
                serverTemplate.Tables["Countries"].FilterClause = "[side].[IsReleased] = @isReleased";
                serverTemplate.Tables["Countries"].FilterParameters.Add(isReleasedParam);

                serverTemplate.Tables["CountrySettings"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["CountrySettings"].AddFilterColumn("Guid");
                serverTemplate.Tables["CountrySettings"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetCountrySettingsHierarchyForReleasedProjectsSync()))";
                serverTemplate.Tables["CountrySettings"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["AssemblyMedia"].AddFilterColumn("AssemblyGuid");
                serverTemplate.Tables["AssemblyMedia"].AddFilterColumn("MediaGuid");
                serverTemplate.Tables["AssemblyMedia"].FilterClause = "([side].[MediaGuid] IN (SELECT [MediaGuid] FROM GetAssemblyMediaHierarchyForReleasedProjectsSync()))";
                serverTemplate.Tables["AssemblyMedia"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["PartMedia"].AddFilterColumn("PartGuid");
                serverTemplate.Tables["PartMedia"].AddFilterColumn("MediaGuid");
                serverTemplate.Tables["PartMedia"].FilterClause = "([side].[MediaGuid] IN (SELECT [MediaGuid] FROM GetPartMediaHierarchyForReleasedProjectsSync()))";
                serverTemplate.Tables["PartMedia"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["ProjectMedia"].AddFilterColumn("ProjectGuid");
                serverTemplate.Tables["ProjectMedia"].AddFilterColumn("MediaGuid");
                serverTemplate.Tables["ProjectMedia"].FilterClause = "([side].[MediaGuid] IN (SELECT [MediaGuid] FROM GetProjectMediaHierarchyForReleasedProjectsSync()))";
                serverTemplate.Tables["ProjectMedia"].FilterParameters.Add(isReleasedParam);

                serverTemplate.Tables["Customers"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["Customers"].AddFilterColumn("Guid");
                serverTemplate.Tables["Customers"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["Customers"].FilterClause = "([side].[Guid] IN (SELECT [CustomerGuid] FROM GetProjectsHierarchyForReleasedProjectsSync()))";
                serverTemplate.Tables["Customers"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["OverheadSettings"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["OverheadSettings"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["OverheadSettings"].AddFilterColumn("Guid");
                serverTemplate.Tables["OverheadSettings"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetOverheadSettingsHierarchyForReleasedProjectsSync()))";
                serverTemplate.Tables["OverheadSettings"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["Manufacturers"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["Manufacturers"].AddFilterColumn("Guid");
                serverTemplate.Tables["Manufacturers"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["Manufacturers"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetManufacturersHierarchyForReleasedProjectsSync()))";
                serverTemplate.Tables["Manufacturers"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["Media"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["Media"].AddFilterColumn("Guid");
                serverTemplate.Tables["Media"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["Media"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetMediaHierarchyForReleasedProjectsSync()))";
                serverTemplate.Tables["Media"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["Processes"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["Processes"].AddFilterColumn("Guid");
                serverTemplate.Tables["Processes"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["Processes"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetProcessesHierarchyForReleasedProjectsSync()))";
                serverTemplate.Tables["Processes"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["ProjectFolders"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["ProjectFolders"].AddFilterColumn("Guid");
                serverTemplate.Tables["ProjectFolders"].AddFilterColumn("ParentGuid");
                serverTemplate.Tables["ProjectFolders"].AddFilterColumn("IsOffline");
                serverTemplate.Tables["ProjectFolders"].AddFilterColumn("IsReleased");
                serverTemplate.Tables["ProjectFolders"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetProjectFoldersHierarchyForReleasedProjectsSync()))";
                serverTemplate.Tables["ProjectFolders"].FilterParameters.Add(isReleasedParam);

                serverTemplate.Tables["Projects"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["Projects"].AddFilterColumn("Guid");
                serverTemplate.Tables["Projects"].AddFilterColumn("IsReleased");
                serverTemplate.Tables["Projects"].AddFilterColumn("ProjectLeaderGuid");
                serverTemplate.Tables["Projects"].AddFilterColumn("ResponsableCalculatorGuid");
                serverTemplate.Tables["Projects"].AddFilterColumn("CustomerGuid");
                serverTemplate.Tables["Projects"].AddFilterColumn("OverheadSettingsGuid");
                serverTemplate.Tables["Projects"].AddFilterColumn("FolderGuid");
                serverTemplate.Tables["Projects"].AddFilterColumn("BaseCurrencyId");
                serverTemplate.Tables["Projects"].AddFilterColumn("IsOffline");
                serverTemplate.Tables["Projects"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetProjectsHierarchyForReleasedProjectsSync()))";
                serverTemplate.Tables["Projects"].FilterParameters.Add(isReleasedParam);

                serverTemplate.Tables["Assemblies"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["Assemblies"].AddFilterColumn("Guid");
                serverTemplate.Tables["Assemblies"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["Assemblies"].AddFilterColumn("CountrySettingsGuid");
                serverTemplate.Tables["Assemblies"].AddFilterColumn("OverheadSettingsGuid");
                serverTemplate.Tables["Assemblies"].AddFilterColumn("ProcessGuid");
                serverTemplate.Tables["Assemblies"].AddFilterColumn("ManufacturerGuid");
                serverTemplate.Tables["Assemblies"].AddFilterColumn("ParentAssemblyGuid");
                serverTemplate.Tables["Assemblies"].AddFilterColumn("ProjectGuid");
                serverTemplate.Tables["Assemblies"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetAssemblyHierarchyForReleasedProjectsSync()))";
                serverTemplate.Tables["Assemblies"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["Parts"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["Parts"].AddFilterColumn("Guid");
                serverTemplate.Tables["Parts"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["Parts"].AddFilterColumn("CountrySettingsGuid");
                serverTemplate.Tables["Parts"].AddFilterColumn("ManufacturerGuid");
                serverTemplate.Tables["Parts"].AddFilterColumn("OverheadSettingsGuid");
                serverTemplate.Tables["Parts"].AddFilterColumn("ProcessGuid");
                serverTemplate.Tables["Parts"].AddFilterColumn("CalculatorGuid");
                serverTemplate.Tables["Parts"].AddFilterColumn("ParentAssemblyGuid");
                serverTemplate.Tables["Parts"].AddFilterColumn("ParentProjectGuid");
                serverTemplate.Tables["Parts"].AddFilterColumn("IsRawPart");
                serverTemplate.Tables["Parts"].AddFilterColumn("RawPartId");
                serverTemplate.Tables["Parts"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetPartsHierarchyForReleasedProjectsSync()))";
                serverTemplate.Tables["Parts"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["ProcessSteps"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["ProcessSteps"].AddFilterColumn("Guid");
                serverTemplate.Tables["ProcessSteps"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["ProcessSteps"].AddFilterColumn("TypeGuid");
                serverTemplate.Tables["ProcessSteps"].AddFilterColumn("SubTypeGuid");
                serverTemplate.Tables["ProcessSteps"].AddFilterColumn("MediaGuid");
                serverTemplate.Tables["ProcessSteps"].AddFilterColumn("ProcessGuid");
                serverTemplate.Tables["ProcessSteps"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetProcessStepsHierarchyForReleasedProjectsSync()))";
                serverTemplate.Tables["ProcessSteps"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["CycleTimeCalculations"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["CycleTimeCalculations"].AddFilterColumn("Guid");
                serverTemplate.Tables["CycleTimeCalculations"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["CycleTimeCalculations"].AddFilterColumn("ProcessStepGuid");
                serverTemplate.Tables["CycleTimeCalculations"].FilterClause = "([side].[ProcessStepGuid] IN (SELECT [Guid] FROM GetProcessStepsHierarchyForReleasedProjectsSync()))";
                serverTemplate.Tables["CycleTimeCalculations"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["Consumables"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["Consumables"].AddFilterColumn("Guid");
                serverTemplate.Tables["Consumables"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["Consumables"].AddFilterColumn("ManufacturerGuid");
                serverTemplate.Tables["Consumables"].AddFilterColumn("ProcessStepGuid");
                serverTemplate.Tables["Consumables"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetConsumablesHierarchyForReleasedProjectsSync()))";
                serverTemplate.Tables["Consumables"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["Dies"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["Dies"].AddFilterColumn("Guid");
                serverTemplate.Tables["Dies"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["Dies"].AddFilterColumn("MediaGuid");
                serverTemplate.Tables["Dies"].AddFilterColumn("ManufacturerGuid");
                serverTemplate.Tables["Dies"].AddFilterColumn("ProcessStepGuid");
                serverTemplate.Tables["Dies"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetDiesHierarchyForReleasedProjectsSync()))";
                serverTemplate.Tables["Dies"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["RawMaterials"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["RawMaterials"].AddFilterColumn("Guid");
                serverTemplate.Tables["RawMaterials"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["RawMaterials"].AddFilterColumn("MediaGuid");
                serverTemplate.Tables["RawMaterials"].AddFilterColumn("ManufacturerGuid");
                serverTemplate.Tables["RawMaterials"].AddFilterColumn("PartGuid");
                serverTemplate.Tables["RawMaterials"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetRawMaterialsHierarchyForReleasedProjectsSync()))";
                serverTemplate.Tables["RawMaterials"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["Machines"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["Machines"].AddFilterColumn("Guid");
                serverTemplate.Tables["Machines"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["Machines"].AddFilterColumn("MediaGuid");
                serverTemplate.Tables["Machines"].AddFilterColumn("ManufacturerGuid");
                serverTemplate.Tables["Machines"].AddFilterColumn("ProcessStepGuid");
                serverTemplate.Tables["Machines"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetMachinesHierarchyForReleasedProjectsSync()))";
                serverTemplate.Tables["Machines"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["Commodities"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["Commodities"].AddFilterColumn("Guid");
                serverTemplate.Tables["Commodities"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["Commodities"].AddFilterColumn("MediaGuid");
                serverTemplate.Tables["Commodities"].AddFilterColumn("ManufacturerGuid");
                serverTemplate.Tables["Commodities"].AddFilterColumn("ProcessStepGuid");
                serverTemplate.Tables["Commodities"].AddFilterColumn("PartGuid");
                serverTemplate.Tables["Commodities"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetCommoditiesHierarchyForReleasedProjectsSync()))";
                serverTemplate.Tables["Commodities"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["ProcessStepPartAmounts"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["ProcessStepPartAmounts"].AddFilterColumn("Guid");
                serverTemplate.Tables["ProcessStepPartAmounts"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["ProcessStepPartAmounts"].AddFilterColumn("PartGuid");
                serverTemplate.Tables["ProcessStepPartAmounts"].AddFilterColumn("ProcessStepGuid");
                serverTemplate.Tables["ProcessStepPartAmounts"].FilterClause = "([side].[PartGuid] IN (SELECT [PartGuid] FROM GetProcessStepPartAmountsHierarchyForReleasedProjectsSync()))";
                serverTemplate.Tables["ProcessStepPartAmounts"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["ProcessStepAssemblyAmounts"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["ProcessStepAssemblyAmounts"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["ProcessStepAssemblyAmounts"].AddFilterColumn("Guid");
                serverTemplate.Tables["ProcessStepAssemblyAmounts"].AddFilterColumn("AssemblyGuid");
                serverTemplate.Tables["ProcessStepAssemblyAmounts"].AddFilterColumn("ProcessStepGuid");
                serverTemplate.Tables["ProcessStepAssemblyAmounts"].FilterClause = "([side].[AssemblyGuid] IN (SELECT [AssemblyGuid] FROM GetProcessStepAssemblyAmountsHierarchyForReleasedProjectsSync()))";
                serverTemplate.Tables["ProcessStepAssemblyAmounts"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["ProjectCurrencies"].AddFilterColumn("ProjectId");
                serverTemplate.Tables["ProjectCurrencies"].AddFilterColumn("CurrencyId");
                serverTemplate.Tables["ProjectCurrencies"].FilterClause = "(side.CurrencyId IN (SELECT CurrencyId FROM GetProjectCurrenciesHierarchyForReleasedProjectsSync()))";

                serverTemplate.Tables["TransportCostCalculations"].AddFilterColumn("OwnerId");
                serverTemplate.Tables["TransportCostCalculations"].AddFilterColumn("Guid");
                serverTemplate.Tables["TransportCostCalculations"].AddFilterColumn("AssemblyId");
                serverTemplate.Tables["TransportCostCalculations"].AddFilterColumn("PartId");
                serverTemplate.Tables["TransportCostCalculations"].FilterClause = "(side.AssemblyId IN (SELECT [Guid] FROM GetAssemblyHierarchyForReleasedProjectsSync()) OR side.PartId IN (SELECT [Guid] FROM GetPartsHierarchyForReleasedProjectsSync()))";

                serverTemplate.Tables["TransportCostCalculationSettings"].AddFilterColumn("OwnerId");
                serverTemplate.Tables["TransportCostCalculationSettings"].AddFilterColumn("Guid");
                serverTemplate.Tables["TransportCostCalculationSettings"].AddFilterColumn("TransportCostCalculationId");
                serverTemplate.Tables["TransportCostCalculationSettings"].FilterClause = "(side.TransportCostCalculationId IN (SELECT [Guid] FROM GetTransportCostCalculationsHierarchyForReleasedProjectsSync()))";

                //----------------------------------------------------------------------------------------
                // Sets whether to create a set of stored procedures that insert, update, and delete data and synchronization metadata.
                serverTemplate.SetCreateTrackingTableDefault(DbSyncCreationOption.CreateOrUseExisting);

                // Sets whether to create on base tables triggers that update tracking tables.
                serverTemplate.SetCreateTriggersDefault(DbSyncCreationOption.CreateOrUseExisting);

                // Sets whether to create a set of stored procedures that insert, update, and delete data and synchronization metadata.
                serverTemplate.SetCreateProceduresDefault(DbSyncCreationOption.CreateOrUseExisting);

                // Sets whether to create base tables when a scope is configured.
                serverTemplate.SetCreateTableDefault(DbSyncCreationOption.CreateOrUseExisting);

                // Sets whether to insert metadata into change-tracking tables for rows that already exist in base tables.
                serverTemplate.SetPopulateTrackingTableDefault(DbSyncCreationOption.CreateOrUseExisting);

                // create a new select changes stored proc for this scope
                // Sets whether to create for any additional scopes sets of stored 
                // procedures that insert, update, and delete data and synchronization metadata.
                serverTemplate.SetCreateProceduresForAdditionalScopeDefault(DbSyncCreationOption.Create);

                if (!serverConfig.TemplateExists(templateName))
                {
                    // Create the template in the database.
                    serverTemplate.Apply();
                }
                else
                {
                    HandleSchemaChanges(sqlConnectionString, scopeName, scopeDesc, serverTemplate);
                }
            }
            catch (SyncException ex)
            {
                Log.ErrorException("Could not configure sql sync provider.", ex);
                throw new SynchronizationException(SyncErrorCodes.InternalError, ex);
            }
            catch (SqlException ex)
            {
                Log.ErrorException("SQL Exception encountered.", ex);
                throw new SynchronizationException(SynchronizationException.ParseSqlException(ex), ex);
            }
        }

        /// <summary>
        /// Populates the released projects scope from template.
        /// Sets the template-based scope description from which the database should be provisioned.
        /// Templates are used to describe a parameter-based filtered scope.
        /// A filtered scope is then created by using PopulateFromTemplate to retrieve the template
        /// description, and by defining specific values for the parameters described in the template.
        /// </summary>
        /// <param name="provider">The provider.</param>
        /// <param name="scopeName">Name of the scope.</param>
        /// <param name="serverConfig">The server config.</param>
        /// <param name="isMasterData">if set to <c>true</c> [is master data].</param>
        /// <param name="isReleased">if set to <c>true</c> [is released].</param>
        /// <param name="sqlConnectionString">The SQL connection string.</param>
        private void PopulateReleasedProjectsScopeFromTemplate(SqlSyncProvider provider, string scopeName, SqlSyncScopeProvisioning serverConfig, bool isMasterData, bool isReleased, string sqlConnectionString)
        {
            try
            {
                // Sets the template-based scope description from which the database should be provisioned.
                serverConfig.PopulateFromTemplate(provider.ScopeName, Enum.GetName(typeof(SyncTemplates), SyncTemplates.ReleasedProjects_Template));

                serverConfig.Tables["Customers"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["OverheadSettings"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["Manufacturers"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["Media"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["Processes"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["Assemblies"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["Parts"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["ProcessSteps"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["CycleTimeCalculations"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["Consumables"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["Dies"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["CountrySettings"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["RawMaterials"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["Machines"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["Commodities"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["ProcessStepPartAmounts"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["ProcessStepAssemblyAmounts"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["AssemblyMedia"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["PartMedia"].FilterParameters["@isMasterData"].Value = isMasterData;

                serverConfig.Tables["ProjectFolders"].FilterParameters["@isReleased"].Value = isReleased;
                serverConfig.Tables["Projects"].FilterParameters["@isReleased"].Value = isReleased;
                serverConfig.Tables["Users"].FilterParameters["@isReleased"].Value = isReleased;
                serverConfig.Tables["ProcessStepsClassification"].FilterParameters["@isReleased"].Value = isReleased;
                serverConfig.Tables["MachinesClassification"].FilterParameters["@isReleased"].Value = isReleased;
                serverConfig.Tables["MaterialsClassification"].FilterParameters["@isReleased"].Value = isReleased;
                serverConfig.Tables["MeasurementUnits"].FilterParameters["@isReleased"].Value = isReleased;
                serverConfig.Tables["Currencies"].FilterParameters["@isReleased"].Value = isReleased;
                serverConfig.Tables["Countries"].FilterParameters["@isReleased"].Value = isReleased;
                serverConfig.Tables["ProjectMedia"].FilterParameters["@isReleased"].Value = isReleased;

                // Create or update database functions used in the scope provisioning in the filter clause.
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetProjectFoldersHierarchyForReleasedProjectsSync", "SqlFunctions.ReleasedProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetProjectsHierarchyForReleasedProjectsSync", "SqlFunctions.ReleasedProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetAssemblyHierarchyForReleasedProjectsSync", "SqlFunctions.ReleasedProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetPartsHierarchyForReleasedProjectsSync", "SqlFunctions.ReleasedProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetCountrySettingsHierarchyForReleasedProjectsSync", "SqlFunctions.ReleasedProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetOverheadSettingsHierarchyForReleasedProjectsSync", "SqlFunctions.ReleasedProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetProcessesHierarchyForReleasedProjectsSync", "SqlFunctions.ReleasedProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetProcessStepsHierarchyForReleasedProjectsSync", "SqlFunctions.ReleasedProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetDiesHierarchyForReleasedProjectsSync", "SqlFunctions.ReleasedProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetMachinesHierarchyForReleasedProjectsSync", "SqlFunctions.ReleasedProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetRawMaterialsHierarchyForReleasedProjectsSync", "SqlFunctions.ReleasedProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetConsumablesHierarchyForReleasedProjectsSync", "SqlFunctions.ReleasedProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetCommoditiesHierarchyForReleasedProjectsSync", "SqlFunctions.ReleasedProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetProjectMediaHierarchyForReleasedProjectsSync", "SqlFunctions.ReleasedProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetAssemblyMediaHierarchyForReleasedProjectsSync", "SqlFunctions.ReleasedProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetPartMediaHierarchyForReleasedProjectsSync", "SqlFunctions.ReleasedProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetMediaHierarchyForReleasedProjectsSync", "SqlFunctions.ReleasedProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetManufacturersHierarchyForReleasedProjectsSync", "SqlFunctions.ReleasedProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetProcessStepAssemblyAmountsHierarchyForReleasedProjectsSync", "SqlFunctions.ReleasedProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetProcessStepPartAmountsHierarchyForReleasedProjectsSync", "SqlFunctions.ReleasedProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetProjectCurrenciesHierarchyForReleasedProjectsSync", "SqlFunctions.ReleasedProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetCurrencyHierarchyForReleasedProjectsSync", "SqlFunctions.ReleasedProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetTransportCostCalculationsHierarchyForReleasedProjectsSync", "SqlFunctions.ReleasedProjects");

                if (!serverConfig.ScopeExists(scopeName))
                {
                    serverConfig.Apply();
                }
                else
                {
                    HandleScopeParametersChange(scopeName, serverConfig, sqlConnectionString);
                }
            }
            catch (DbProvisioningException ex)
            {
                Log.WarnException("You tried to synchronize a scope that is currently being provisioned to a SQL Server, DbProvisioningException is thrown", ex);
                throw new SynchronizationException(SyncErrorCodes.InternalError, ex);
            }
            catch (SyncException ex)
            {
                Log.ErrorException("Could not populate scope from template", ex);
                throw new SynchronizationException(SyncErrorCodes.InternalError, ex);
            }
            catch (SqlException ex)
            {
                Log.ErrorException("SQL Exception encountered.", ex);
                throw new SynchronizationException(SynchronizationException.ParseSqlException(ex), ex);
            }
        }

        #endregion

        #region Master Data Synchronization
        /// <summary>
        /// Provisions the template for master data.
        /// After you define the synchronization scope, you provision the database to create a change-tracking and
        /// metadata management infrastructure that consists of metadata tables, triggers, and stored procedures.
        /// </summary>
        /// <param name="sqlConnection">The SQL connection.</param>
        /// <param name="scopeName">Name of the scope.</param>
        /// <param name="sqlConnectionString">The SQL connection string.</param>
        private void ProvisionTemplateForMasterData(SqlConnection sqlConnection, string scopeName, string sqlConnectionString)
        {
            try
            {
                string templateName = Enum.GetName(typeof(SyncTemplates), SyncTemplates.MasterData_Template);

                // create a new scope description and add the appropriate tables to this scope
                DbSyncScopeDescription scopeDesc = new DbSyncScopeDescription(templateName);

                // class to be used to provision the scope defined above
                SqlSyncScopeProvisioning serverConfig = new SqlSyncScopeProvisioning(sqlConnection);

                // Represents the schema of a table that is included in the Tables list of a DbSyncScopeDescription object. This is used during database provisioning.
                // For each table that is synchronized, changes are selected from the server database in the order of inserts, updates, 
                // and then deletes. Changes are applied to the client database in the order of deletes, inserts, and then updates. 
                // When several tables are synchronized, the order in which each table is processed depends on
                // the order in which its SyncTable object was added to the collection of tables for the synchronization agent.
                DbSyncTableDescription processStepsClassificationTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("ProcessStepsClassification", sqlConnection);
                scopeDesc.Tables.Add(processStepsClassificationTableDesc);
                DbSyncTableDescription machinesClassificationTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("MachinesClassification", sqlConnection);
                scopeDesc.Tables.Add(machinesClassificationTableDesc);
                DbSyncTableDescription materialsClassificationTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("MaterialsClassification", sqlConnection);
                scopeDesc.Tables.Add(materialsClassificationTableDesc);
                DbSyncTableDescription countrySettingsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("CountrySettings", sqlConnection);
                scopeDesc.Tables.Add(countrySettingsTableDesc);
                DbSyncTableDescription customersTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Customers", sqlConnection);
                scopeDesc.Tables.Add(customersTableDesc);
                DbSyncTableDescription overheadSettingsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("OverheadSettings", sqlConnection);
                scopeDesc.Tables.Add(overheadSettingsTableDesc);
                DbSyncTableDescription manufacturersTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Manufacturers", sqlConnection);
                scopeDesc.Tables.Add(manufacturersTableDesc);
                DbSyncTableDescription mediaTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Media", sqlConnection);
                scopeDesc.Tables.Add(mediaTableDesc);
                DbSyncTableDescription processesTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Processes", sqlConnection);
                scopeDesc.Tables.Add(processesTableDesc);
                DbSyncTableDescription assembliesTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Assemblies", sqlConnection);
                scopeDesc.Tables.Add(assembliesTableDesc);
                DbSyncTableDescription partsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Parts", sqlConnection);
                scopeDesc.Tables.Add(partsTableDesc);
                DbSyncTableDescription assemblyMediaTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("AssemblyMedia", sqlConnection);
                scopeDesc.Tables.Add(assemblyMediaTableDesc);
                DbSyncTableDescription partMediaTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("PartMedia", sqlConnection);
                scopeDesc.Tables.Add(partMediaTableDesc);
                DbSyncTableDescription processStepsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("ProcessSteps", sqlConnection);
                scopeDesc.Tables.Add(processStepsTableDesc);
                DbSyncTableDescription cycleTimeCalculationsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("CycleTimeCalculations", sqlConnection);
                scopeDesc.Tables.Add(cycleTimeCalculationsTableDesc);
                DbSyncTableDescription consumablesTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Consumables", sqlConnection);
                scopeDesc.Tables.Add(consumablesTableDesc);
                DbSyncTableDescription consumablesPriceHistoryTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("ConsumablesPriceHistory", sqlConnection);
                scopeDesc.Tables.Add(consumablesPriceHistoryTableDesc);
                DbSyncTableDescription diesTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Dies", sqlConnection);
                scopeDesc.Tables.Add(diesTableDesc);
                DbSyncTableDescription rawMaterialsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("RawMaterials", sqlConnection);
                scopeDesc.Tables.Add(rawMaterialsTableDesc);
                DbSyncTableDescription rawMaterialsPriceHistoryTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("RawMaterialsPriceHistory", sqlConnection);
                scopeDesc.Tables.Add(rawMaterialsPriceHistoryTableDesc);
                DbSyncTableDescription machinesTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Machines", sqlConnection);
                scopeDesc.Tables.Add(machinesTableDesc);
                DbSyncTableDescription commoditiesTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Commodities", sqlConnection);
                scopeDesc.Tables.Add(commoditiesTableDesc);
                DbSyncTableDescription commoditiesPriceHistoryTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("CommoditiesPriceHistory", sqlConnection);
                scopeDesc.Tables.Add(commoditiesPriceHistoryTableDesc);
                DbSyncTableDescription processStepPartAmountsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("ProcessStepPartAmounts", sqlConnection);
                scopeDesc.Tables.Add(processStepPartAmountsTableDesc);
                DbSyncTableDescription processStepAssemblyAmountsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("ProcessStepAssemblyAmounts", sqlConnection);
                scopeDesc.Tables.Add(processStepAssemblyAmountsTableDesc);

                // Add a filter column to a SqlSyncTableProvisioning object in the 
                // synchronization scope by using AddFilterColumn. 
                // This adds the filter column to the tracking table that tracks changes
                // for the base table.

                // Define one or more filter parameters by adding SqlParameter objects to the
                // FilterParameters collection of the SqlSyncTableProvisioning object. 
                // This adds the specified parameters to the argument list of the stored
                // procedure that enumerates changes during synchronization.

                // Add a filter clause that defines the relationship between parameter values
                // and column values by setting the FilterClause property of the
                // SqlSyncTableProvisioning object. The filter clause is a WHERE clause 
                // without the WHERE keyword. The [side] alias is an alias for the tracking 
                // table. The parameters match the parameters specified in the FilterParameters
                // collection. At this point you are only defining the relationship between 
                // the filter parameters and columns. The actual values for the parameters 
                // will be specified later, when the filtered scope is created.

                // Represents the provisioning of a SQL Server database for a particular scope that is represented by a DbSyncScopeDescription object. 
                SqlSyncScopeProvisioning serverTemplate = new SqlSyncScopeProvisioning(sqlConnection, scopeDesc, SqlSyncScopeProvisioningType.Template);

                SqlParameter isMasterDataParam = new SqlParameter("@isMasterData", SqlDbType.Bit);

                serverTemplate.Tables["ProcessStepsClassification"].AddFilterColumn("Guid");
                serverTemplate.Tables["ProcessStepsClassification"].FilterClause = "([side].[Guid] IN ( SELECT [ProcessSteps].[TypeGuid] FROM [ProcessSteps] WHERE [ProcessSteps].[OwnerGuid] is NULL and [ProcessSteps].[IsMasterData] = @isMasterData and [ProcessSteps].[TypeGuid] is not NULL )" +
                                                                                    " or [side].[Guid] IN ( SELECT [ProcessSteps].[SubTypeGuid] FROM [ProcessSteps] WHERE [ProcessSteps].[OwnerGuid] is NULL and [ProcessSteps].[IsMasterData] = @isMasterData and  [ProcessSteps].[SubTypeGuid] is not NULL ))";
                serverTemplate.Tables["ProcessStepsClassification"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["MachinesClassification"].AddFilterColumn("Guid");
                serverTemplate.Tables["MachinesClassification"].FilterClause = "([side].[Guid] IN ( SELECT [Machines].[MainClassificationGuid] FROM [Machines] WHERE [Machines].[OwnerGuid] is NULL and [Machines].[IsMasterData] = @isMasterData and [Machines].[MainClassificationGuid] is not NULL )" +
                                                                                    " or [side].[Guid] IN ( SELECT [Machines].[TypeClassificationGuid] FROM [Machines] WHERE [Machines].[OwnerGuid] is NULL and [Machines].[IsMasterData] = @isMasterData and  [Machines].[TypeClassificationGuid] is not NULL )" +
                                                                                     " or [side].[Guid] IN ( SELECT [Machines].[SubClassificationGuid] FROM [Machines] WHERE [Machines].[OwnerGuid] is NULL and [Machines].[IsMasterData] = @isMasterData and [Machines].[SubClassificationGuid] is not NULL )" +
                                                                                      " or [side].[Guid] IN ( SELECT [Machines].[ClassificationLevel4Guid] FROM [Machines] WHERE [Machines].[OwnerGuid] is NULL and [Machines].[IsMasterData] = @isMasterData and [Machines].[ClassificationLevel4Guid] is not NULL ))";
                serverTemplate.Tables["MachinesClassification"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["MaterialsClassification"].AddFilterColumn("Guid");
                serverTemplate.Tables["MaterialsClassification"].FilterClause = "([side].[Guid] IN ( SELECT [RawMaterials].[ClassificationLevel4Guid] FROM [RawMaterials] WHERE [RawMaterials].[OwnerGuid] is NULL and [RawMaterials].[IsMasterData] = @isMasterData and  [RawMaterials].[ClassificationLevel4Guid] is not NULL )" +
                                                                                    " or [side].[Guid] IN ( SELECT [RawMaterials].[ClassificationLevel3Guid] FROM [RawMaterials] WHERE [RawMaterials].[OwnerGuid] is NULL and [RawMaterials].[IsMasterData] = @isMasterData and [RawMaterials].[ClassificationLevel3Guid] is not NULL )" +
                                                                                     " or [side].[Guid] IN ( SELECT [RawMaterials].[ClassificationLevel2Guid] FROM [RawMaterials] WHERE [RawMaterials].[OwnerGuid] is NULL and [RawMaterials].[IsMasterData] = @isMasterData and [RawMaterials].[ClassificationLevel2Guid] is not NULL )" +
                                                                                      " or [side].[Guid] IN ( SELECT [RawMaterials].[ClassificationLevel1Guid] FROM [RawMaterials] WHERE [RawMaterials].[OwnerGuid] is NULL and [RawMaterials].[IsMasterData] = @isMasterData and [RawMaterials].[ClassificationLevel1Guid] is not NULL ))";
                serverTemplate.Tables["MaterialsClassification"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["AssemblyMedia"].AddFilterColumn("AssemblyGuid");
                serverTemplate.Tables["AssemblyMedia"].AddFilterColumn("MediaGuid");
                serverTemplate.Tables["AssemblyMedia"].FilterClause = "([side].[MediaGuid] IN (SELECT [MediaGuid] FROM GetAssemblyMediaHierarchyForMasterDataSync()))";
                serverTemplate.Tables["AssemblyMedia"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["PartMedia"].AddFilterColumn("PartGuid");
                serverTemplate.Tables["PartMedia"].AddFilterColumn("MediaGuid");
                serverTemplate.Tables["PartMedia"].FilterClause = "([side].[MediaGuid] IN (SELECT [MediaGuid] FROM GetPartMediaHierarchyForMasterDataSync()))";
                serverTemplate.Tables["PartMedia"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["CountrySettings"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["CountrySettings"].AddFilterColumn("Guid");
                serverTemplate.Tables["CountrySettings"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetCountrySettingsHierarchyForMasterDataSync()))";
                serverTemplate.Tables["CountrySettings"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["Customers"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["Customers"].AddFilterColumn("Guid");
                serverTemplate.Tables["Customers"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["Customers"].FilterClause = "([side].[OwnerGuid] is NULL and [side].[IsMasterData] = @isMasterData)";
                serverTemplate.Tables["Customers"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["OverheadSettings"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["OverheadSettings"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["OverheadSettings"].AddFilterColumn("Guid");
                serverTemplate.Tables["OverheadSettings"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetOverheadSettingsHierarchyForMasterDataSync()))";
                serverTemplate.Tables["OverheadSettings"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["Manufacturers"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["Manufacturers"].AddFilterColumn("Guid");
                serverTemplate.Tables["Manufacturers"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["Manufacturers"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetManufacturersHierarchyForMasterDataSync()))";
                serverTemplate.Tables["Manufacturers"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["Media"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["Media"].AddFilterColumn("Guid");
                serverTemplate.Tables["Media"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["Media"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetMediaHierarchyForMasterDataSync()))";
                serverTemplate.Tables["Media"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["Processes"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["Processes"].AddFilterColumn("Guid");
                serverTemplate.Tables["Processes"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["Processes"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetProcessesHierarchyForMasterDataSync()))";
                serverTemplate.Tables["Processes"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["Assemblies"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["Assemblies"].AddFilterColumn("Guid");
                serverTemplate.Tables["Assemblies"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["Assemblies"].AddFilterColumn("CountrySettingsGuid");
                serverTemplate.Tables["Assemblies"].AddFilterColumn("OverheadSettingsGuid");
                serverTemplate.Tables["Assemblies"].AddFilterColumn("ProcessGuid");
                serverTemplate.Tables["Assemblies"].AddFilterColumn("ManufacturerGuid");
                serverTemplate.Tables["Assemblies"].AddFilterColumn("ParentAssemblyGuid");
                serverTemplate.Tables["Assemblies"].AddFilterColumn("ProjectGuid");
                serverTemplate.Tables["Assemblies"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetAssemblyHierarchyForMasterDataSync()))";
                serverTemplate.Tables["Assemblies"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["Parts"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["Parts"].AddFilterColumn("Guid");
                serverTemplate.Tables["Parts"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["Parts"].AddFilterColumn("CountrySettingsGuid");
                serverTemplate.Tables["Parts"].AddFilterColumn("ManufacturerGuid");
                serverTemplate.Tables["Parts"].AddFilterColumn("OverheadSettingsGuid");
                serverTemplate.Tables["Parts"].AddFilterColumn("ProcessGuid");
                serverTemplate.Tables["Parts"].AddFilterColumn("CalculatorGuid");
                serverTemplate.Tables["Parts"].AddFilterColumn("ParentAssemblyGuid");
                serverTemplate.Tables["Parts"].AddFilterColumn("ParentProjectGuid");
                serverTemplate.Tables["Parts"].AddFilterColumn("IsRawPart");
                serverTemplate.Tables["Parts"].AddFilterColumn("RawPartId");
                serverTemplate.Tables["Parts"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetPartsHierarchyForMasterDataSync()))";
                serverTemplate.Tables["Parts"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["ProcessSteps"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["ProcessSteps"].AddFilterColumn("Guid");
                serverTemplate.Tables["ProcessSteps"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["ProcessSteps"].AddFilterColumn("TypeGuid");
                serverTemplate.Tables["ProcessSteps"].AddFilterColumn("SubTypeGuid");
                serverTemplate.Tables["ProcessSteps"].AddFilterColumn("MediaGuid");
                serverTemplate.Tables["ProcessSteps"].AddFilterColumn("ProcessGuid");
                serverTemplate.Tables["ProcessSteps"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetProcessStepsHierarchyForMasterDataSync()))";
                serverTemplate.Tables["ProcessSteps"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["CycleTimeCalculations"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["CycleTimeCalculations"].AddFilterColumn("Guid");
                serverTemplate.Tables["CycleTimeCalculations"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["CycleTimeCalculations"].AddFilterColumn("ProcessStepGuid");
                serverTemplate.Tables["CycleTimeCalculations"].FilterClause = "([side].[ProcessStepGuid] IN (SELECT [Guid] FROM GetProcessStepsHierarchyForMasterDataSync()))";
                serverTemplate.Tables["CycleTimeCalculations"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["Consumables"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["Consumables"].AddFilterColumn("Guid");
                serverTemplate.Tables["Consumables"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["Consumables"].AddFilterColumn("ManufacturerGuid");
                serverTemplate.Tables["Consumables"].AddFilterColumn("ProcessStepGuid");
                serverTemplate.Tables["Consumables"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetConsumablesHierarchyForMasterDataSync()))";
                serverTemplate.Tables["Consumables"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["ConsumablesPriceHistory"].AddFilterColumn("ConsumableGuid");
                serverTemplate.Tables["ConsumablesPriceHistory"].AddFilterColumn("Guid");
                serverTemplate.Tables["ConsumablesPriceHistory"].FilterClause = "([side].[ConsumableGuid] IN (SELECT [Guid] FROM GetConsumablesHierarchyForMasterDataSync()))";
                serverTemplate.Tables["ConsumablesPriceHistory"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["Dies"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["Dies"].AddFilterColumn("Guid");
                serverTemplate.Tables["Dies"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["Dies"].AddFilterColumn("MediaGuid");
                serverTemplate.Tables["Dies"].AddFilterColumn("ManufacturerGuid");
                serverTemplate.Tables["Dies"].AddFilterColumn("ProcessStepGuid");
                serverTemplate.Tables["Dies"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetDiesHierarchyForMasterDataSync()))";
                serverTemplate.Tables["Dies"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["RawMaterials"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["RawMaterials"].AddFilterColumn("Guid");
                serverTemplate.Tables["RawMaterials"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["RawMaterials"].AddFilterColumn("MediaGuid");
                serverTemplate.Tables["RawMaterials"].AddFilterColumn("ManufacturerGuid");
                serverTemplate.Tables["RawMaterials"].AddFilterColumn("PartGuid");
                serverTemplate.Tables["RawMaterials"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetRawMaterialsHierarchyForMasterDataSync()))";
                serverTemplate.Tables["RawMaterials"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["RawMaterialsPriceHistory"].AddFilterColumn("RawMaterialGuid");
                serverTemplate.Tables["RawMaterialsPriceHistory"].AddFilterColumn("Guid");
                serverTemplate.Tables["RawMaterialsPriceHistory"].FilterClause = "([side].[RawMaterialGuid] IN (SELECT [Guid] FROM GetRawMaterialsHierarchyForMasterDataSync()))";
                serverTemplate.Tables["RawMaterialsPriceHistory"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["Machines"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["Machines"].AddFilterColumn("Guid");
                serverTemplate.Tables["Machines"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["Machines"].AddFilterColumn("MediaGuid");
                serverTemplate.Tables["Machines"].AddFilterColumn("ManufacturerGuid");
                serverTemplate.Tables["Machines"].AddFilterColumn("ProcessStepGuid");
                serverTemplate.Tables["Machines"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetMachinesHierarchyForMasterDataSync()))";
                serverTemplate.Tables["Machines"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["Commodities"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["Commodities"].AddFilterColumn("Guid");
                serverTemplate.Tables["Commodities"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["Commodities"].AddFilterColumn("MediaGuid");
                serverTemplate.Tables["Commodities"].AddFilterColumn("ManufacturerGuid");
                serverTemplate.Tables["Commodities"].AddFilterColumn("ProcessStepGuid");
                serverTemplate.Tables["Commodities"].AddFilterColumn("PartGuid");
                serverTemplate.Tables["Commodities"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetCommoditiesHierarchyForMasterDataSync()))";
                serverTemplate.Tables["Commodities"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["CommoditiesPriceHistory"].AddFilterColumn("CommodityGuid");
                serverTemplate.Tables["CommoditiesPriceHistory"].AddFilterColumn("Guid");
                serverTemplate.Tables["CommoditiesPriceHistory"].FilterClause = "([side].[CommodityGuid] IN (SELECT [Guid] FROM GetCommoditiesHierarchyForMasterDataSync()))";
                serverTemplate.Tables["CommoditiesPriceHistory"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["ProcessStepPartAmounts"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["ProcessStepPartAmounts"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["ProcessStepPartAmounts"].AddFilterColumn("Guid");
                serverTemplate.Tables["ProcessStepPartAmounts"].AddFilterColumn("PartGuid");
                serverTemplate.Tables["ProcessStepPartAmounts"].AddFilterColumn("ProcessStepGuid");
                serverTemplate.Tables["ProcessStepPartAmounts"].FilterClause = "([side].[PartGuid] IN (SELECT [PartGuid] FROM GetProcessStepPartAmountsHierarchyForMasterDataSync()))";
                serverTemplate.Tables["ProcessStepPartAmounts"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["ProcessStepAssemblyAmounts"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["ProcessStepAssemblyAmounts"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["ProcessStepAssemblyAmounts"].AddFilterColumn("Guid");
                serverTemplate.Tables["ProcessStepAssemblyAmounts"].AddFilterColumn("AssemblyGuid");
                serverTemplate.Tables["ProcessStepAssemblyAmounts"].AddFilterColumn("ProcessStepGuid");
                serverTemplate.Tables["ProcessStepAssemblyAmounts"].FilterClause = "([side].[AssemblyGuid] IN (SELECT [AssemblyGuid] FROM GetProcessStepAssemblyAmountsHierarchyForMasterDataSync()))";
                serverTemplate.Tables["ProcessStepAssemblyAmounts"].FilterParameters.Add(isMasterDataParam);

                //----------------------------------------------------------------------------------------
                // Sets whether to create a set of stored procedures that insert, update, and delete data and synchronization metadata.
                serverTemplate.SetCreateTrackingTableDefault(DbSyncCreationOption.CreateOrUseExisting);

                // Sets whether to create on base tables triggers that update tracking tables.
                serverTemplate.SetCreateTriggersDefault(DbSyncCreationOption.CreateOrUseExisting);

                // Sets whether to create a set of stored procedures that insert, update, and delete data and synchronization metadata.
                serverTemplate.SetCreateProceduresDefault(DbSyncCreationOption.CreateOrUseExisting);

                // Sets whether to create base tables when a scope is configured.
                serverTemplate.SetCreateTableDefault(DbSyncCreationOption.CreateOrUseExisting);

                // Sets whether to insert metadata into change-tracking tables for rows that already exist in base tables.
                serverTemplate.SetPopulateTrackingTableDefault(DbSyncCreationOption.CreateOrUseExisting);

                // create a new select changes stored proc for this scope
                // Sets whether to create for any additional scopes sets of stored 
                // procedures that insert, update, and delete data and synchronization metadata.
                serverTemplate.SetCreateProceduresForAdditionalScopeDefault(DbSyncCreationOption.Create);

                if (!serverConfig.TemplateExists(templateName))
                {
                    // Create the template in the database.
                    serverTemplate.Apply();
                }
                else
                {
                    HandleSchemaChanges(sqlConnectionString, scopeName, scopeDesc, serverTemplate);
                }
            }
            catch (SyncException ex)
            {
                Log.ErrorException("Could not configure sql sync provider.", ex);
                throw new SynchronizationException(SyncErrorCodes.InternalError, ex);
            }
            catch (SqlException ex)
            {
                Log.ErrorException("SQL Exception encountered.", ex);
                throw new SynchronizationException(SynchronizationException.ParseSqlException(ex), ex);
            }
        }

        /// <summary>
        /// Populates the master data scope from template.
        /// Sets the template-based scope description from which the database should be provisioned.
        /// Templates are used to describe a parameter-based filtered scope.
        /// A filtered scope is then created by using PopulateFromTemplate to retrieve the template
        /// description, and by defining specific values for the parameters described in the template.
        /// </summary>
        /// <param name="provider">The provider.</param>
        /// <param name="scopeName">Name of the scope.</param>
        /// <param name="serverConfig">The server scope provisioning.</param>
        /// <param name="isMasterData">if set to <c>true</c> [is master data].</param>
        /// <param name="sqlConnectionString">The SQL connection string.</param>
        private void PopulateMasterDataScopeFromTemplate(SqlSyncProvider provider, string scopeName, SqlSyncScopeProvisioning serverConfig, bool isMasterData, string sqlConnectionString)
        {
            try
            {
                // Sets the template-based scope description from which the database should be provisioned.
                serverConfig.PopulateFromTemplate(provider.ScopeName, Enum.GetName(typeof(SyncTemplates), SyncTemplates.MasterData_Template));

                serverConfig.Tables["Customers"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["OverheadSettings"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["Manufacturers"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["Media"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["Processes"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["Assemblies"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["Parts"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["ProcessSteps"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["CycleTimeCalculations"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["Consumables"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["ConsumablesPriceHistory"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["Dies"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["RawMaterials"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["RawMaterialsPriceHistory"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["Machines"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["Commodities"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["CommoditiesPriceHistory"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["ProcessStepPartAmounts"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["ProcessStepAssemblyAmounts"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["ProcessStepsClassification"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["MachinesClassification"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["MaterialsClassification"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["CountrySettings"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["AssemblyMedia"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["PartMedia"].FilterParameters["@isMasterData"].Value = isMasterData;

                // Create or alter database functions used in the scope provisioning in the filter clause.  
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetAssemblyHierarchyForMasterDataSync", "SqlFunctions.MasterData");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetPartsHierarchyForMasterDataSync", "SqlFunctions.MasterData");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetCountrySettingsHierarchyForMasterDataSync", "SqlFunctions.MasterData");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetOverheadSettingsHierarchyForMasterDataSync", "SqlFunctions.MasterData");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetProcessesHierarchyForMasterDataSync", "SqlFunctions.MasterData");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetProcessStepsHierarchyForMasterDataSync", "SqlFunctions.MasterData");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetDiesHierarchyForMasterDataSync", "SqlFunctions.MasterData");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetMachinesHierarchyForMasterDataSync", "SqlFunctions.MasterData");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetRawMaterialsHierarchyForMasterDataSync", "SqlFunctions.MasterData");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetConsumablesHierarchyForMasterDataSync", "SqlFunctions.MasterData");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetCommoditiesHierarchyForMasterDataSync", "SqlFunctions.MasterData");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetAssemblyMediaHierarchyForMasterDataSync", "SqlFunctions.MasterData");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetPartMediaHierarchyForMasterDataSync", "SqlFunctions.MasterData");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetMediaHierarchyForMasterDataSync", "SqlFunctions.MasterData");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetManufacturersHierarchyForMasterDataSync", "SqlFunctions.MasterData");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetProcessStepAssemblyAmountsHierarchyForMasterDataSync", "SqlFunctions.MasterData");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetProcessStepPartAmountsHierarchyForMasterDataSync", "SqlFunctions.MasterData");

                if (!serverConfig.ScopeExists(scopeName))
                {
                    serverConfig.Apply();
                }
                else
                {
                    HandleScopeParametersChange(scopeName, serverConfig, sqlConnectionString);
                }
            }
            catch (DbProvisioningException ex)
            {
                Log.WarnException("You tried to synchronize a scope that is currently being provisioned to a SQL Server, DbProvisioningException is thrown", ex);
                throw new SynchronizationException(SyncErrorCodes.InternalError, ex);
            }
            catch (SyncException ex)
            {
                Log.ErrorException("Could not populate scope from template", ex);
                throw new SynchronizationException(SyncErrorCodes.InternalError, ex);
            }
            catch (SqlException ex)
            {
                Log.ErrorException("SQL Exception encountered.", ex);
                throw new SynchronizationException(SynchronizationException.ParseSqlException(ex), ex);
            }
        }
        #endregion

        #region My Projects Synchronization

        /// <summary>
        /// Provisions the template for my projects.
        /// After you define the synchronization scope, you provision the database to create a change-tracking and
        /// metadata management infrastructure that consists of metadata tables, triggers, and stored procedures.
        /// </summary>
        /// <param name="sqlConnection">The SQL connection.</param>        
        /// <param name="scopeName">Name of the scope.</param>
        /// <param name="sqlConnectionString">The SQL connection string.</param>
        private void ProvisionTemplateForMyProjects(SqlConnection sqlConnection, string scopeName, string sqlConnectionString)
        {
            try
            {
                string templateName = Enum.GetName(typeof(SyncTemplates), SyncTemplates.MyProjects_Template);

                // create a new scope description and add the appropriate tables to this scope
                DbSyncScopeDescription scopeDesc = new DbSyncScopeDescription(templateName);

                // class to be used to provision the scope defined above
                SqlSyncScopeProvisioning serverConfig = new SqlSyncScopeProvisioning(sqlConnection);

                // For each table that is synchronized, changes are selected from the server database in the order of inserts, updates, 
                // and then deletes. Changes are applied to the client database in the order of deletes, inserts, and then updates. 
                // When several tables are synchronized, the order in which each table is processed depends on
                // the order in which its SyncTable object was added to the collection of tables for the synchronization agent.

                // Represents the schema of a table that is included in the Tables list of a DbSyncScopeDescription object. This is used during database provisioning. 
                DbSyncTableDescription countrySettingsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("CountrySettings", sqlConnection);
                scopeDesc.Tables.Add(countrySettingsTableDesc);
                DbSyncTableDescription countrySettingsValueHistoryTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("CountrySettingsValueHistory", sqlConnection);
                scopeDesc.Tables.Add(countrySettingsValueHistoryTableDesc);
                DbSyncTableDescription customersTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Customers", sqlConnection);
                scopeDesc.Tables.Add(customersTableDesc);
                DbSyncTableDescription overheadSettingsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("OverheadSettings", sqlConnection);
                scopeDesc.Tables.Add(overheadSettingsTableDesc);
                DbSyncTableDescription manufacturersTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Manufacturers", sqlConnection);
                scopeDesc.Tables.Add(manufacturersTableDesc);
                DbSyncTableDescription mediaTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Media", sqlConnection);
                scopeDesc.Tables.Add(mediaTableDesc);
                DbSyncTableDescription projectFoldersTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("ProjectFolders", sqlConnection);
                scopeDesc.Tables.Add(projectFoldersTableDesc);
                DbSyncTableDescription projectsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Projects", sqlConnection);
                scopeDesc.Tables.Add(projectsTableDesc);
                DbSyncTableDescription processesTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Processes", sqlConnection);
                scopeDesc.Tables.Add(processesTableDesc);
                DbSyncTableDescription assembliesTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Assemblies", sqlConnection);
                scopeDesc.Tables.Add(assembliesTableDesc);
                DbSyncTableDescription partsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Parts", sqlConnection);
                scopeDesc.Tables.Add(partsTableDesc);
                DbSyncTableDescription assemblyMediaTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("AssemblyMedia", sqlConnection);
                scopeDesc.Tables.Add(assemblyMediaTableDesc);
                DbSyncTableDescription partMediaTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("PartMedia", sqlConnection);
                scopeDesc.Tables.Add(partMediaTableDesc);
                DbSyncTableDescription projectMediaTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("ProjectMedia", sqlConnection);
                scopeDesc.Tables.Add(projectMediaTableDesc);
                DbSyncTableDescription processStepsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("ProcessSteps", sqlConnection);
                scopeDesc.Tables.Add(processStepsTableDesc);
                DbSyncTableDescription cycleTimeCalculationsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("CycleTimeCalculations", sqlConnection);
                scopeDesc.Tables.Add(cycleTimeCalculationsTableDesc);
                DbSyncTableDescription consumablesTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Consumables", sqlConnection);
                scopeDesc.Tables.Add(consumablesTableDesc);
                DbSyncTableDescription consumablesPriceHistoryTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("ConsumablesPriceHistory", sqlConnection);
                scopeDesc.Tables.Add(consumablesPriceHistoryTableDesc);
                DbSyncTableDescription diesTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Dies", sqlConnection);
                scopeDesc.Tables.Add(diesTableDesc);
                DbSyncTableDescription rawMaterialsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("RawMaterials", sqlConnection);
                scopeDesc.Tables.Add(rawMaterialsTableDesc);
                DbSyncTableDescription rawMaterialsPriceHistoryTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("RawMaterialsPriceHistory", sqlConnection);
                scopeDesc.Tables.Add(rawMaterialsPriceHistoryTableDesc);
                DbSyncTableDescription machinesTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Machines", sqlConnection);
                scopeDesc.Tables.Add(machinesTableDesc);
                DbSyncTableDescription commoditiesTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Commodities", sqlConnection);
                scopeDesc.Tables.Add(commoditiesTableDesc);
                DbSyncTableDescription commoditiesPriceHistoryTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("CommoditiesPriceHistory", sqlConnection);
                scopeDesc.Tables.Add(commoditiesPriceHistoryTableDesc);
                DbSyncTableDescription processStepPartAmountsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("ProcessStepPartAmounts", sqlConnection);
                scopeDesc.Tables.Add(processStepPartAmountsTableDesc);
                DbSyncTableDescription processStepAssemblyAmountsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("ProcessStepAssemblyAmounts", sqlConnection);
                scopeDesc.Tables.Add(processStepAssemblyAmountsTableDesc);
                DbSyncTableDescription trashBinItemsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("TrashBinItems", sqlConnection);
                scopeDesc.Tables.Add(trashBinItemsTableDesc);
                DbSyncTableDescription bookmarksTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Bookmarks", sqlConnection);
                scopeDesc.Tables.Add(bookmarksTableDesc);
                DbSyncTableDescription currenciesTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Currencies", sqlConnection);
                scopeDesc.Tables.Add(currenciesTableDesc);
                DbSyncTableDescription projectCurrenciesTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("ProjectCurrencies", sqlConnection);
                scopeDesc.Tables.Add(projectCurrenciesTableDesc);
                DbSyncTableDescription transportCostCalculationsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("TransportCostCalculations", sqlConnection);
                scopeDesc.Tables.Add(transportCostCalculationsTableDesc);
                DbSyncTableDescription transportCostCalculationSettingsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("TransportCostCalculationSettings", sqlConnection);
                scopeDesc.Tables.Add(transportCostCalculationSettingsTableDesc);

                // Add a filter column to a SqlSyncTableProvisioning object in the 
                // synchronization scope by using AddFilterColumn. 
                // This adds the filter column to the tracking table that tracks changes
                // for the base table.

                // Define one or more filter parameters by adding SqlParameter objects to the
                // FilterParameters collection of the SqlSyncTableProvisioning object. 
                // This adds the specified parameters to the argument list of the stored
                // procedure that enumerates changes during synchronization.

                // Add a filter clause that defines the relationship between parameter values
                // and column values by setting the FilterClause property of the
                // SqlSyncTableProvisioning object. The filter clause is a WHERE clause 
                // without the WHERE keyword. The [side] alias is an alias for the tracking 
                // table. The parameters match the parameters specified in the FilterParameters
                // collection. At this point you are only defining the relationship between 
                // the filter parameters and columns. The actual values for the parameters 
                // will be specified later, when the filtered scope is created.

                // Represents the provisioning of a SQL Server database for a particular scope that is represented by a DbSyncScopeDescription object. 
                SqlSyncScopeProvisioning serverTemplate = new SqlSyncScopeProvisioning(sqlConnection, scopeDesc, SqlSyncScopeProvisioningType.Template);
                SqlParameter ownerParam = new SqlParameter("@ownerGuid", SqlDbType.UniqueIdentifier);
                SqlParameter isMasterDataParam = new SqlParameter("@isMasterData", SqlDbType.Bit);
                SqlParameter isReleasedParam = new SqlParameter("@isReleased", SqlDbType.Bit);
                SqlParameter offline = new SqlParameter("@offline", SqlDbType.Bit);

                serverTemplate.Tables["CountrySettings"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["CountrySettings"].AddFilterColumn("Guid");
                serverTemplate.Tables["CountrySettings"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetCountrySettingsHierarchyForMyProjectsSync(@ownerGuid,@offline)))";
                serverTemplate.Tables["CountrySettings"].FilterParameters.Add(ownerParam);
                serverTemplate.Tables["CountrySettings"].FilterParameters.Add(offline);

                serverTemplate.Tables["CountrySettingsValueHistory"].AddFilterColumn("CountrySettingsGuid");
                serverTemplate.Tables["CountrySettingsValueHistory"].AddFilterColumn("Guid");
                serverTemplate.Tables["CountrySettingsValueHistory"].FilterClause = "([side].[CountrySettingsGuid] IN (SELECT [Guid] FROM GetCountrySettingsHierarchyForMyProjectsSync(@ownerGuid,@offline)))";
                serverTemplate.Tables["CountrySettingsValueHistory"].FilterParameters.Add(ownerParam);
                serverTemplate.Tables["CountrySettingsValueHistory"].FilterParameters.Add(offline);

                serverTemplate.Tables["Customers"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["Customers"].AddFilterColumn("Guid");
                serverTemplate.Tables["Customers"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["Customers"].FilterClause = "([side].[Guid] IN (SELECT [CustomerGuid] FROM GetProjectsHierarchyForMyProjectsSync(@ownerGuid,@offline)))";
                serverTemplate.Tables["Customers"].FilterParameters.Add(ownerParam);
                serverTemplate.Tables["Customers"].FilterParameters.Add(isMasterDataParam);
                serverTemplate.Tables["Customers"].FilterParameters.Add(offline);

                serverTemplate.Tables["OverheadSettings"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["OverheadSettings"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["OverheadSettings"].AddFilterColumn("Guid");
                serverTemplate.Tables["OverheadSettings"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetOverheadSettingsHierarchyForMyProjectsSync(@ownerGuid,@offline)))";
                serverTemplate.Tables["OverheadSettings"].FilterParameters.Add(ownerParam);
                serverTemplate.Tables["OverheadSettings"].FilterParameters.Add(isMasterDataParam);
                serverTemplate.Tables["OverheadSettings"].FilterParameters.Add(offline);

                serverTemplate.Tables["Manufacturers"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["Manufacturers"].AddFilterColumn("Guid");
                serverTemplate.Tables["Manufacturers"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["Manufacturers"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetManufacturersHierarchyForMyProjectsSync(@ownerGuid,@offline)))";
                serverTemplate.Tables["Manufacturers"].FilterParameters.Add(ownerParam);
                serverTemplate.Tables["Manufacturers"].FilterParameters.Add(isMasterDataParam);
                serverTemplate.Tables["Manufacturers"].FilterParameters.Add(offline);

                serverTemplate.Tables["Media"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["Media"].AddFilterColumn("Guid");
                serverTemplate.Tables["Media"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["Media"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetMediaHierarchyForMyProjectsSync(@ownerGuid,@offline)))";
                serverTemplate.Tables["Media"].FilterParameters.Add(ownerParam);
                serverTemplate.Tables["Media"].FilterParameters.Add(isMasterDataParam);
                serverTemplate.Tables["Media"].FilterParameters.Add(offline);

                serverTemplate.Tables["Processes"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["Processes"].AddFilterColumn("Guid");
                serverTemplate.Tables["Processes"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["Processes"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetProcessesHierarchyForMyProjectsSync(@ownerGuid,@offline)))";
                serverTemplate.Tables["Processes"].FilterParameters.Add(ownerParam);
                serverTemplate.Tables["Processes"].FilterParameters.Add(isMasterDataParam);
                serverTemplate.Tables["Processes"].FilterParameters.Add(offline);

                serverTemplate.Tables["ProjectFolders"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["ProjectFolders"].AddFilterColumn("Guid");
                serverTemplate.Tables["ProjectFolders"].AddFilterColumn("ParentGuid");
                serverTemplate.Tables["ProjectFolders"].AddFilterColumn("IsOffline");
                serverTemplate.Tables["ProjectFolders"].AddFilterColumn("IsReleased");
                serverTemplate.Tables["ProjectFolders"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetProjectFoldersHierarchyForMyProjectsSync(@ownerGuid,@offline)))";
                serverTemplate.Tables["ProjectFolders"].FilterParameters.Add(ownerParam);
                serverTemplate.Tables["ProjectFolders"].FilterParameters.Add(offline);

                serverTemplate.Tables["Projects"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["Projects"].AddFilterColumn("Guid");
                serverTemplate.Tables["Projects"].AddFilterColumn("IsReleased");
                serverTemplate.Tables["Projects"].AddFilterColumn("ProjectLeaderGuid");
                serverTemplate.Tables["Projects"].AddFilterColumn("ResponsableCalculatorGuid");
                serverTemplate.Tables["Projects"].AddFilterColumn("CustomerGuid");
                serverTemplate.Tables["Projects"].AddFilterColumn("OverheadSettingsGuid");
                serverTemplate.Tables["Projects"].AddFilterColumn("FolderGuid");
                serverTemplate.Tables["Projects"].AddFilterColumn("BaseCurrencyId");
                serverTemplate.Tables["Projects"].AddFilterColumn("IsOffline");
                serverTemplate.Tables["Projects"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetProjectsHierarchyForMyProjectsSync(@ownerGuid,@offline)))";
                serverTemplate.Tables["Projects"].FilterParameters.Add(ownerParam);
                serverTemplate.Tables["Projects"].FilterParameters.Add(isReleasedParam);
                serverTemplate.Tables["Projects"].FilterParameters.Add(offline);

                serverTemplate.Tables["Assemblies"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["Assemblies"].AddFilterColumn("Guid");
                serverTemplate.Tables["Assemblies"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["Assemblies"].AddFilterColumn("CountrySettingsGuid");
                serverTemplate.Tables["Assemblies"].AddFilterColumn("OverheadSettingsGuid");
                serverTemplate.Tables["Assemblies"].AddFilterColumn("ProcessGuid");
                serverTemplate.Tables["Assemblies"].AddFilterColumn("ManufacturerGuid");
                serverTemplate.Tables["Assemblies"].AddFilterColumn("ParentAssemblyGuid");
                serverTemplate.Tables["Assemblies"].AddFilterColumn("ProjectGuid");
                serverTemplate.Tables["Assemblies"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetAssemblyHierarchyForMyProjectsSync(@ownerGuid,@offline)))";
                serverTemplate.Tables["Assemblies"].FilterParameters.Add(ownerParam);
                serverTemplate.Tables["Assemblies"].FilterParameters.Add(isMasterDataParam);
                serverTemplate.Tables["Assemblies"].FilterParameters.Add(offline);

                serverTemplate.Tables["Parts"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["Parts"].AddFilterColumn("Guid");
                serverTemplate.Tables["Parts"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["Parts"].AddFilterColumn("CountrySettingsGuid");
                serverTemplate.Tables["Parts"].AddFilterColumn("ManufacturerGuid");
                serverTemplate.Tables["Parts"].AddFilterColumn("OverheadSettingsGuid");
                serverTemplate.Tables["Parts"].AddFilterColumn("ProcessGuid");
                serverTemplate.Tables["Parts"].AddFilterColumn("CalculatorGuid");
                serverTemplate.Tables["Parts"].AddFilterColumn("ParentAssemblyGuid");
                serverTemplate.Tables["Parts"].AddFilterColumn("ParentProjectGuid");
                serverTemplate.Tables["Parts"].AddFilterColumn("IsRawPart");
                serverTemplate.Tables["Parts"].AddFilterColumn("RawPartId");
                serverTemplate.Tables["Parts"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetPartsHierarchyForMyProjectsSync(@ownerGuid,@offline)))";
                serverTemplate.Tables["Parts"].FilterParameters.Add(ownerParam);
                serverTemplate.Tables["Parts"].FilterParameters.Add(isMasterDataParam);
                serverTemplate.Tables["Parts"].FilterParameters.Add(offline);

                serverTemplate.Tables["ProjectMedia"].AddFilterColumn("ProjectGuid");
                serverTemplate.Tables["ProjectMedia"].AddFilterColumn("MediaGuid");
                serverTemplate.Tables["ProjectMedia"].FilterClause = "([side].[MediaGuid] IN (SELECT [MediaGuid] FROM GetProjectMediaHierarchyForMyProjectsSync(@ownerGuid,@offline)))";
                serverTemplate.Tables["ProjectMedia"].FilterParameters.Add(ownerParam);
                serverTemplate.Tables["ProjectMedia"].FilterParameters.Add(isReleasedParam);
                serverTemplate.Tables["ProjectMedia"].FilterParameters.Add(offline);

                serverTemplate.Tables["AssemblyMedia"].AddFilterColumn("AssemblyGuid");
                serverTemplate.Tables["AssemblyMedia"].AddFilterColumn("MediaGuid");
                serverTemplate.Tables["AssemblyMedia"].FilterClause = "([side].[MediaGuid] IN (SELECT [MediaGuid] FROM GetAssemblyMediaHierarchyForMyProjectsSync(@ownerGuid,@offline)))";
                serverTemplate.Tables["AssemblyMedia"].FilterParameters.Add(ownerParam);
                serverTemplate.Tables["AssemblyMedia"].FilterParameters.Add(isMasterDataParam);
                serverTemplate.Tables["AssemblyMedia"].FilterParameters.Add(offline);

                serverTemplate.Tables["PartMedia"].AddFilterColumn("PartGuid");
                serverTemplate.Tables["PartMedia"].AddFilterColumn("MediaGuid");
                serverTemplate.Tables["PartMedia"].FilterClause = "([side].[MediaGuid] IN (SELECT [MediaGuid] FROM GetPartMediaHierarchyForMyProjectsSync(@ownerGuid,@offline)))";
                serverTemplate.Tables["PartMedia"].FilterParameters.Add(ownerParam);
                serverTemplate.Tables["PartMedia"].FilterParameters.Add(isMasterDataParam);
                serverTemplate.Tables["PartMedia"].FilterParameters.Add(offline);

                serverTemplate.Tables["ProcessSteps"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["ProcessSteps"].AddFilterColumn("Guid");
                serverTemplate.Tables["ProcessSteps"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["ProcessSteps"].AddFilterColumn("TypeGuid");
                serverTemplate.Tables["ProcessSteps"].AddFilterColumn("SubTypeGuid");
                serverTemplate.Tables["ProcessSteps"].AddFilterColumn("MediaGuid");
                serverTemplate.Tables["ProcessSteps"].AddFilterColumn("ProcessGuid");
                serverTemplate.Tables["ProcessSteps"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetProcessStepsHierarchyForMyProjectsSync(@ownerGuid,@offline)))";
                serverTemplate.Tables["ProcessSteps"].FilterParameters.Add(ownerParam);
                serverTemplate.Tables["ProcessSteps"].FilterParameters.Add(isMasterDataParam);
                serverTemplate.Tables["ProcessSteps"].FilterParameters.Add(offline);

                serverTemplate.Tables["CycleTimeCalculations"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["CycleTimeCalculations"].AddFilterColumn("Guid");
                serverTemplate.Tables["CycleTimeCalculations"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["CycleTimeCalculations"].AddFilterColumn("ProcessStepGuid");
                serverTemplate.Tables["CycleTimeCalculations"].FilterClause = "([side].[ProcessStepGuid] IN (SELECT [Guid] FROM GetProcessStepsHierarchyForMyProjectsSync(@ownerGuid,@offline)))";
                serverTemplate.Tables["CycleTimeCalculations"].FilterParameters.Add(ownerParam);
                serverTemplate.Tables["CycleTimeCalculations"].FilterParameters.Add(isMasterDataParam);
                serverTemplate.Tables["CycleTimeCalculations"].FilterParameters.Add(offline);

                serverTemplate.Tables["Consumables"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["Consumables"].AddFilterColumn("Guid");
                serverTemplate.Tables["Consumables"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["Consumables"].AddFilterColumn("ManufacturerGuid");
                serverTemplate.Tables["Consumables"].AddFilterColumn("ProcessStepGuid");
                serverTemplate.Tables["Consumables"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetConsumablesHierarchyForMyProjectsSync(@ownerGuid,@offline)))";
                serverTemplate.Tables["Consumables"].FilterParameters.Add(ownerParam);
                serverTemplate.Tables["Consumables"].FilterParameters.Add(isMasterDataParam);
                serverTemplate.Tables["Consumables"].FilterParameters.Add(offline);

                serverTemplate.Tables["ConsumablesPriceHistory"].AddFilterColumn("ConsumableGuid");
                serverTemplate.Tables["ConsumablesPriceHistory"].AddFilterColumn("Guid");
                serverTemplate.Tables["ConsumablesPriceHistory"].FilterClause = "([side].[ConsumableGuid] IN (SELECT [Guid] FROM GetConsumablesHierarchyForMyProjectsSync(@ownerGuid,@offline)))";
                serverTemplate.Tables["ConsumablesPriceHistory"].FilterParameters.Add(ownerParam);
                serverTemplate.Tables["ConsumablesPriceHistory"].FilterParameters.Add(isMasterDataParam);
                serverTemplate.Tables["ConsumablesPriceHistory"].FilterParameters.Add(offline);

                serverTemplate.Tables["Dies"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["Dies"].AddFilterColumn("Guid");
                serverTemplate.Tables["Dies"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["Dies"].AddFilterColumn("MediaGuid");
                serverTemplate.Tables["Dies"].AddFilterColumn("ManufacturerGuid");
                serverTemplate.Tables["Dies"].AddFilterColumn("ProcessStepGuid");
                serverTemplate.Tables["Dies"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetDiesHierarchyForMyProjectsSync(@ownerGuid,@offline)))";
                serverTemplate.Tables["Dies"].FilterParameters.Add(ownerParam);
                serverTemplate.Tables["Dies"].FilterParameters.Add(isMasterDataParam);
                serverTemplate.Tables["Dies"].FilterParameters.Add(offline);

                serverTemplate.Tables["RawMaterials"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["RawMaterials"].AddFilterColumn("Guid");
                serverTemplate.Tables["RawMaterials"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["RawMaterials"].AddFilterColumn("MediaGuid");
                serverTemplate.Tables["RawMaterials"].AddFilterColumn("ManufacturerGuid");
                serverTemplate.Tables["RawMaterials"].AddFilterColumn("PartGuid");
                serverTemplate.Tables["RawMaterials"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetRawMaterialsHierarchyForMyProjectsSync(@ownerGuid,@offline)))";
                serverTemplate.Tables["RawMaterials"].FilterParameters.Add(ownerParam);
                serverTemplate.Tables["RawMaterials"].FilterParameters.Add(isMasterDataParam);
                serverTemplate.Tables["RawMaterials"].FilterParameters.Add(offline);

                serverTemplate.Tables["RawMaterialsPriceHistory"].AddFilterColumn("RawMaterialGuid");
                serverTemplate.Tables["RawMaterialsPriceHistory"].AddFilterColumn("Guid");
                serverTemplate.Tables["RawMaterialsPriceHistory"].FilterClause = "([side].[RawMaterialGuid] IN (SELECT [Guid] FROM GetRawMaterialsHierarchyForMyProjectsSync(@ownerGuid,@offline)))";
                serverTemplate.Tables["RawMaterialsPriceHistory"].FilterParameters.Add(ownerParam);
                serverTemplate.Tables["RawMaterialsPriceHistory"].FilterParameters.Add(isMasterDataParam);
                serverTemplate.Tables["RawMaterialsPriceHistory"].FilterParameters.Add(offline);

                serverTemplate.Tables["Machines"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["Machines"].AddFilterColumn("Guid");
                serverTemplate.Tables["Machines"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["Machines"].AddFilterColumn("MediaGuid");
                serverTemplate.Tables["Machines"].AddFilterColumn("ManufacturerGuid");
                serverTemplate.Tables["Machines"].AddFilterColumn("ProcessStepGuid");
                serverTemplate.Tables["Machines"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetMachinesHierarchyForMyProjectsSync(@ownerGuid,@offline)))";
                serverTemplate.Tables["Machines"].FilterParameters.Add(ownerParam);
                serverTemplate.Tables["Machines"].FilterParameters.Add(isMasterDataParam);
                serverTemplate.Tables["Machines"].FilterParameters.Add(offline);

                serverTemplate.Tables["Commodities"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["Commodities"].AddFilterColumn("Guid");
                serverTemplate.Tables["Commodities"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["Commodities"].AddFilterColumn("MediaGuid");
                serverTemplate.Tables["Commodities"].AddFilterColumn("ManufacturerGuid");
                serverTemplate.Tables["Commodities"].AddFilterColumn("ProcessStepGuid");
                serverTemplate.Tables["Commodities"].AddFilterColumn("PartGuid");
                serverTemplate.Tables["Commodities"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetCommoditiesHierarchyForMyProjectsSync(@ownerGuid,@offline)))";
                serverTemplate.Tables["Commodities"].FilterParameters.Add(ownerParam);
                serverTemplate.Tables["Commodities"].FilterParameters.Add(isMasterDataParam);
                serverTemplate.Tables["Commodities"].FilterParameters.Add(offline);

                serverTemplate.Tables["CommoditiesPriceHistory"].AddFilterColumn("CommodityGuid");
                serverTemplate.Tables["CommoditiesPriceHistory"].AddFilterColumn("Guid");
                serverTemplate.Tables["CommoditiesPriceHistory"].FilterClause = "([side].[CommodityGuid] IN (SELECT [Guid] FROM GetCommoditiesHierarchyForMyProjectsSync(@ownerGuid,@offline)))";
                serverTemplate.Tables["CommoditiesPriceHistory"].FilterParameters.Add(ownerParam);
                serverTemplate.Tables["CommoditiesPriceHistory"].FilterParameters.Add(isMasterDataParam);
                serverTemplate.Tables["CommoditiesPriceHistory"].FilterParameters.Add(offline);

                serverTemplate.Tables["ProcessStepPartAmounts"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["ProcessStepPartAmounts"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["ProcessStepPartAmounts"].AddFilterColumn("Guid");
                serverTemplate.Tables["ProcessStepPartAmounts"].AddFilterColumn("PartGuid");
                serverTemplate.Tables["ProcessStepPartAmounts"].AddFilterColumn("ProcessStepGuid");
                serverTemplate.Tables["ProcessStepPartAmounts"].FilterClause = "([side].[PartGuid] IN (SELECT [PartGuid] FROM GetProcessStepPartAmountsHierarchyForMyProjectsSync(@ownerGuid,@offline)))";
                serverTemplate.Tables["ProcessStepPartAmounts"].FilterParameters.Add(ownerParam);
                serverTemplate.Tables["ProcessStepPartAmounts"].FilterParameters.Add(isMasterDataParam);
                serverTemplate.Tables["ProcessStepPartAmounts"].FilterParameters.Add(offline);

                serverTemplate.Tables["ProcessStepAssemblyAmounts"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["ProcessStepAssemblyAmounts"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["ProcessStepAssemblyAmounts"].AddFilterColumn("Guid");
                serverTemplate.Tables["ProcessStepAssemblyAmounts"].AddFilterColumn("AssemblyGuid");
                serverTemplate.Tables["ProcessStepAssemblyAmounts"].AddFilterColumn("ProcessStepGuid");
                serverTemplate.Tables["ProcessStepAssemblyAmounts"].FilterClause = "([side].[AssemblyGuid] IN (SELECT [AssemblyGuid] FROM GetProcessStepAssemblyAmountsHierarchyForMyProjectsSync(@ownerGuid,@offline)))";
                serverTemplate.Tables["ProcessStepAssemblyAmounts"].FilterParameters.Add(ownerParam);
                serverTemplate.Tables["ProcessStepAssemblyAmounts"].FilterParameters.Add(isMasterDataParam);
                serverTemplate.Tables["ProcessStepAssemblyAmounts"].FilterParameters.Add(offline);

                serverTemplate.Tables["TrashBinItems"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["TrashBinItems"].AddFilterColumn("Guid");
                serverTemplate.Tables["TrashBinItems"].FilterClause = "[side].[OwnerGuid] = @ownerGuid";
                serverTemplate.Tables["TrashBinItems"].FilterParameters.Add(ownerParam);

                serverTemplate.Tables["Bookmarks"].AddFilterColumn("OwnerId");
                serverTemplate.Tables["Bookmarks"].AddFilterColumn("Guid");
                serverTemplate.Tables["Bookmarks"].FilterClause = "[side].[OwnerId] = @ownerGuid";
                serverTemplate.Tables["Bookmarks"].FilterParameters.Add(ownerParam);

                serverTemplate.Tables["Currencies"].AddFilterColumn("Guid");
                serverTemplate.Tables["Currencies"].FilterClause = "([side].[Guid] IN (SELECT [Guid] FROM GetCurrencyHierarchyForMyProjectsSync(@ownerGuid,@offline)))";
                serverTemplate.Tables["Currencies"].FilterParameters.Add(ownerParam);
                serverTemplate.Tables["Currencies"].FilterParameters.Add(offline);

                serverTemplate.Tables["ProjectCurrencies"].AddFilterColumn("ProjectId");
                serverTemplate.Tables["ProjectCurrencies"].AddFilterColumn("CurrencyId");
                serverTemplate.Tables["ProjectCurrencies"].FilterClause = "([side].CurrencyId IN (SELECT CurrencyId FROM GetProjectCurrenciesHierarchyForMyProjectsSync(@ownerGuid,@offline)))";
                serverTemplate.Tables["ProjectCurrencies"].FilterParameters.Add(ownerParam);
                serverTemplate.Tables["ProjectCurrencies"].FilterParameters.Add(offline);

                serverTemplate.Tables["TransportCostCalculations"].AddFilterColumn("OwnerId");
                serverTemplate.Tables["TransportCostCalculations"].AddFilterColumn("Guid");
                serverTemplate.Tables["TransportCostCalculations"].AddFilterColumn("AssemblyId");
                serverTemplate.Tables["TransportCostCalculations"].AddFilterColumn("PartId");
                serverTemplate.Tables["TransportCostCalculations"].FilterClause = "(side.AssemblyId IN (SELECT [Guid] FROM GetAssemblyHierarchyForMyProjectsSync(@ownerGuid,@offline)) OR side.PartId IN (SELECT [Guid] FROM GetPartsHierarchyForMyProjectsSync(@ownerGuid,@offline)))";
                serverTemplate.Tables["TransportCostCalculations"].FilterParameters.Add(ownerParam);
                serverTemplate.Tables["TransportCostCalculations"].FilterParameters.Add(offline);

                serverTemplate.Tables["TransportCostCalculationSettings"].AddFilterColumn("OwnerId");
                serverTemplate.Tables["TransportCostCalculationSettings"].AddFilterColumn("Guid");
                serverTemplate.Tables["TransportCostCalculationSettings"].AddFilterColumn("TransportCostCalculationId");
                serverTemplate.Tables["TransportCostCalculationSettings"].FilterClause = "(side.TransportCostCalculationId IN (SELECT [Guid] FROM GetTransportCostCalculationsHierarchyForMyProjectsSync(@ownerGuid,@offline)))";
                serverTemplate.Tables["TransportCostCalculationSettings"].FilterParameters.Add(ownerParam);
                serverTemplate.Tables["TransportCostCalculationSettings"].FilterParameters.Add(offline);

                //----------------------------------------------------------------------------------------
                // Sets whether to create a set of stored procedures that insert, update, and delete data and synchronization metadata.
                serverTemplate.SetCreateTrackingTableDefault(DbSyncCreationOption.CreateOrUseExisting);

                // Sets whether to create on base tables triggers that update tracking tables.
                serverTemplate.SetCreateTriggersDefault(DbSyncCreationOption.CreateOrUseExisting);

                // Sets whether to create a set of stored procedures that insert, update, and delete data and synchronization metadata.
                serverTemplate.SetCreateProceduresDefault(DbSyncCreationOption.CreateOrUseExisting);

                // Sets whether to create base tables when a scope is configured.
                serverTemplate.SetCreateTableDefault(DbSyncCreationOption.CreateOrUseExisting);

                // Sets whether to insert metadata into change-tracking tables for rows that already exist in base tables.
                serverTemplate.SetPopulateTrackingTableDefault(DbSyncCreationOption.CreateOrUseExisting);

                // create a new select changes stored proc for this scope
                // Sets whether to create for any additional scopes sets of stored 
                // procedures that insert, update, and delete data and synchronization metadata.
                serverTemplate.SetCreateProceduresForAdditionalScopeDefault(DbSyncCreationOption.Create);

                if (!serverConfig.TemplateExists(templateName))
                {
                    // Create the template in the database.
                    serverTemplate.Apply();
                }
                else
                {
                    HandleSchemaChanges(sqlConnectionString, scopeName, scopeDesc, serverTemplate);
                }
            }
            catch (SyncException ex)
            {
                Log.ErrorException("Could not configure sql sync provider.", ex);
                throw new SynchronizationException(SyncErrorCodes.InternalError, ex);
            }
            catch (SqlException ex)
            {
                Log.ErrorException("SQL Exception encountered.", ex);
                throw new SynchronizationException(SynchronizationException.ParseSqlException(ex), ex);
            }
        }

        /// <summary>
        /// Populates the scope from template.
        /// Sets the template-based scope description from which the database should be provisioned.
        /// Templates are used to describe a parameter-based filtered scope.
        /// A filtered scope is then created by using PopulateFromTemplate to retrieve the template
        /// description, and by defining specific values for the parameters described in the template.
        /// </summary>
        /// <param name="provider">The provider.</param>
        /// <param name="scopeName">Name of the scope.</param>
        /// <param name="serverConfig">The server scope provisioning.</param>
        /// <param name="isMasterData">if set to <c>true</c> [is master data].</param>
        /// <param name="isReleased">if set to <c>true</c> [is released].</param>
        /// <param name="userGuid">The user GUID.</param>
        /// <param name="sqlConnectionString">The SQL connection string.</param>
        private void PopulateMyProjectsScopeFromTemplate(SqlSyncProvider provider, string scopeName, SqlSyncScopeProvisioning serverConfig, bool isMasterData, bool isReleased, Guid userGuid, string sqlConnectionString)
        {
            try
            {
                // Sets the template-based scope description from which the database should be provisioned.
                serverConfig.PopulateFromTemplate(provider.ScopeName, Enum.GetName(typeof(SyncTemplates), SyncTemplates.MyProjects_Template));

                serverConfig.Tables["CountrySettings"].FilterParameters["@ownerGuid"].Value = userGuid;
                serverConfig.Tables["CountrySettingsValueHistory"].FilterParameters["@ownerGuid"].Value = userGuid;
                serverConfig.Tables["AssemblyMedia"].FilterParameters["@ownerGuid"].Value = userGuid;
                serverConfig.Tables["PartMedia"].FilterParameters["@ownerGuid"].Value = userGuid;
                serverConfig.Tables["ProjectMedia"].FilterParameters["@ownerGuid"].Value = userGuid;
                serverConfig.Tables["Customers"].FilterParameters["@ownerGuid"].Value = userGuid;
                serverConfig.Tables["OverheadSettings"].FilterParameters["@ownerGuid"].Value = userGuid;
                serverConfig.Tables["Manufacturers"].FilterParameters["@ownerGuid"].Value = userGuid;
                serverConfig.Tables["Media"].FilterParameters["@ownerGuid"].Value = userGuid;
                serverConfig.Tables["Processes"].FilterParameters["@ownerGuid"].Value = userGuid;
                serverConfig.Tables["ProjectFolders"].FilterParameters["@ownerGuid"].Value = userGuid;
                serverConfig.Tables["Projects"].FilterParameters["@ownerGuid"].Value = userGuid;
                serverConfig.Tables["Assemblies"].FilterParameters["@ownerGuid"].Value = userGuid;
                serverConfig.Tables["Parts"].FilterParameters["@ownerGuid"].Value = userGuid;
                serverConfig.Tables["ProcessSteps"].FilterParameters["@ownerGuid"].Value = userGuid;
                serverConfig.Tables["CycleTimeCalculations"].FilterParameters["@ownerGuid"].Value = userGuid;
                serverConfig.Tables["Consumables"].FilterParameters["@ownerGuid"].Value = userGuid;
                serverConfig.Tables["ConsumablesPriceHistory"].FilterParameters["@ownerGuid"].Value = userGuid;
                serverConfig.Tables["Dies"].FilterParameters["@ownerGuid"].Value = userGuid;
                serverConfig.Tables["RawMaterials"].FilterParameters["@ownerGuid"].Value = userGuid;
                serverConfig.Tables["RawMaterialsPriceHistory"].FilterParameters["@ownerGuid"].Value = userGuid;
                serverConfig.Tables["Machines"].FilterParameters["@ownerGuid"].Value = userGuid;
                serverConfig.Tables["Commodities"].FilterParameters["@ownerGuid"].Value = userGuid;
                serverConfig.Tables["CommoditiesPriceHistory"].FilterParameters["@ownerGuid"].Value = userGuid;
                serverConfig.Tables["ProcessStepPartAmounts"].FilterParameters["@ownerGuid"].Value = userGuid;
                serverConfig.Tables["ProcessStepAssemblyAmounts"].FilterParameters["@ownerGuid"].Value = userGuid;
                serverConfig.Tables["TrashBinItems"].FilterParameters["@ownerGuid"].Value = userGuid;
                serverConfig.Tables["Bookmarks"].FilterParameters["@ownerGuid"].Value = userGuid;
                serverConfig.Tables["Currencies"].FilterParameters["@ownerGuid"].Value = userGuid;
                serverConfig.Tables["ProjectCurrencies"].FilterParameters["@ownerGuid"].Value = userGuid;
                serverConfig.Tables["TransportCostCalculations"].FilterParameters["@ownerGuid"].Value = userGuid;
                serverConfig.Tables["TransportCostCalculationSettings"].FilterParameters["@ownerGuid"].Value = userGuid;

                serverConfig.Tables["CountrySettings"].FilterParameters["@offline"].Value = false;
                serverConfig.Tables["CountrySettingsValueHistory"].FilterParameters["@offline"].Value = false;
                serverConfig.Tables["AssemblyMedia"].FilterParameters["@offline"].Value = false;
                serverConfig.Tables["PartMedia"].FilterParameters["@offline"].Value = false;
                serverConfig.Tables["ProjectMedia"].FilterParameters["@offline"].Value = false;
                serverConfig.Tables["Customers"].FilterParameters["@offline"].Value = false;
                serverConfig.Tables["OverheadSettings"].FilterParameters["@offline"].Value = false;
                serverConfig.Tables["Manufacturers"].FilterParameters["@offline"].Value = false;
                serverConfig.Tables["Media"].FilterParameters["@offline"].Value = false;
                serverConfig.Tables["Processes"].FilterParameters["@offline"].Value = false;
                serverConfig.Tables["ProjectFolders"].FilterParameters["@offline"].Value = false;
                serverConfig.Tables["Projects"].FilterParameters["@offline"].Value = false;
                serverConfig.Tables["Assemblies"].FilterParameters["@offline"].Value = false;
                serverConfig.Tables["Parts"].FilterParameters["@offline"].Value = false;
                serverConfig.Tables["ProcessSteps"].FilterParameters["@offline"].Value = false;
                serverConfig.Tables["CycleTimeCalculations"].FilterParameters["@offline"].Value = false;
                serverConfig.Tables["Consumables"].FilterParameters["@offline"].Value = false;
                serverConfig.Tables["ConsumablesPriceHistory"].FilterParameters["@offline"].Value = false;
                serverConfig.Tables["Dies"].FilterParameters["@offline"].Value = false;
                serverConfig.Tables["RawMaterials"].FilterParameters["@offline"].Value = false;
                serverConfig.Tables["RawMaterialsPriceHistory"].FilterParameters["@offline"].Value = false;
                serverConfig.Tables["Machines"].FilterParameters["@offline"].Value = false;
                serverConfig.Tables["Commodities"].FilterParameters["@offline"].Value = false;
                serverConfig.Tables["CommoditiesPriceHistory"].FilterParameters["@offline"].Value = false;
                serverConfig.Tables["ProcessStepPartAmounts"].FilterParameters["@offline"].Value = false;
                serverConfig.Tables["ProcessStepAssemblyAmounts"].FilterParameters["@offline"].Value = false;
                serverConfig.Tables["Currencies"].FilterParameters["@offline"].Value = false;
                serverConfig.Tables["ProjectCurrencies"].FilterParameters["@offline"].Value = false;
                serverConfig.Tables["TransportCostCalculations"].FilterParameters["@offline"].Value = false;
                serverConfig.Tables["TransportCostCalculationSettings"].FilterParameters["@offline"].Value = false;

                serverConfig.Tables["Customers"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["OverheadSettings"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["Manufacturers"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["Media"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["Processes"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["Assemblies"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["Parts"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["ProcessSteps"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["CycleTimeCalculations"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["Consumables"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["ConsumablesPriceHistory"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["Dies"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["RawMaterials"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["RawMaterialsPriceHistory"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["Machines"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["Commodities"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["CommoditiesPriceHistory"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["ProcessStepPartAmounts"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["ProcessStepAssemblyAmounts"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["AssemblyMedia"].FilterParameters["@isMasterData"].Value = isMasterData;
                serverConfig.Tables["PartMedia"].FilterParameters["@isMasterData"].Value = isMasterData;

                serverConfig.Tables["Projects"].FilterParameters["@isReleased"].Value = isReleased;
                serverConfig.Tables["ProjectMedia"].FilterParameters["@isReleased"].Value = isReleased;

                // Create or alter database functions used in the scope provisioning in the filter clause.
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetProjectFoldersHierarchyForMyProjectsSync", "SqlFunctions.MyProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetProjectsHierarchyForMyProjectsSync", "SqlFunctions.MyProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetAssemblyHierarchyForMyProjectsSync", "SqlFunctions.MyProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetPartsHierarchyForMyProjectsSync", "SqlFunctions.MyProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetCountrySettingsHierarchyForMyProjectsSync", "SqlFunctions.MyProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetOverheadSettingsHierarchyForMyProjectsSync", "SqlFunctions.MyProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetProcessesHierarchyForMyProjectsSync", "SqlFunctions.MyProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetProcessStepsHierarchyForMyProjectsSync", "SqlFunctions.MyProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetDiesHierarchyForMyProjectsSync", "SqlFunctions.MyProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetMachinesHierarchyForMyProjectsSync", "SqlFunctions.MyProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetRawMaterialsHierarchyForMyProjectsSync", "SqlFunctions.MyProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetConsumablesHierarchyForMyProjectsSync", "SqlFunctions.MyProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetCommoditiesHierarchyForMyProjectsSync", "SqlFunctions.MyProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetProjectMediaHierarchyForMyProjectsSync", "SqlFunctions.MyProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetAssemblyMediaHierarchyForMyProjectsSync", "SqlFunctions.MyProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetPartMediaHierarchyForMyProjectsSync", "SqlFunctions.MyProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetMediaHierarchyForMyProjectsSync", "SqlFunctions.MyProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetManufacturersHierarchyForMyProjectsSync", "SqlFunctions.MyProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetProcessStepAssemblyAmountsHierarchyForMyProjectsSync", "SqlFunctions.MyProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetProcessStepPartAmountsHierarchyForMyProjectsSync", "SqlFunctions.MyProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetProjectCurrenciesHierarchyForMyProjectsSync", "SqlFunctions.MyProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetCurrencyHierarchyForMyProjectsSync", "SqlFunctions.MyProjects");
                SyncUtils.CreateOrUpdateDataBaseFunctionForSync(sqlConnectionString, "GetTransportCostCalculationsHierarchyForMyProjectsSync", "SqlFunctions.MyProjects");

                if (!serverConfig.ScopeExists(scopeName))
                {
                    serverConfig.Apply();
                }
                else
                {
                    HandleScopeParametersChange(scopeName, serverConfig, sqlConnectionString);
                }
            }
            catch (DbProvisioningException ex)
            {
                Log.WarnException("You try to synchronize a scope that is currently being provisioned to a SQL Server", ex);
                throw new SynchronizationException(SyncErrorCodes.InternalError, ex);
            }
            catch (SyncException ex)
            {
                Log.ErrorException("Could not populate scope from template", ex);
                throw new SynchronizationException(SyncErrorCodes.InternalError, ex);
            }
            catch (SqlException ex)
            {
                Log.ErrorException("SQL Exception encountered.", ex);
                throw new SynchronizationException(SynchronizationException.ParseSqlException(ex), ex);
            }
        }

        #endregion

        #region Offline Projects Synchronization

        /// <summary>
        /// Synchronizes the offline projects and folders.
        /// </summary>
        /// <param name="ownerGuid">The owner GUID.</param>        
        [SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "The query string does not contain user input.")]
        private void SynchronizeOfflineProjects(Guid ownerGuid)
        {
            try
            {
                // Get Offline Projects and Folders From Local database.
                IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
                var offlineProjectsGuids = dataManager.ProjectRepository.GetOfflineProjects(ownerGuid);
                var offlineFoldersGuids = dataManager.ProjectFolderRepository.GetOfflineProjectFolders(ownerGuid);

                // Build the SQLsul IN Clause for the Update of offline projects/folders from the central database.
                if (offlineProjectsGuids.Count > 0)
                {
                    // Offline Projects SQL IN Clause builder
                    string offlineProjectsInSqlClause = "(";
                    foreach (Guid guid in offlineProjectsGuids)
                    {
                        offlineProjectsInSqlClause += "'" + guid + "'" + ",";
                    }

                    if (offlineProjectsInSqlClause.Count() > 1)
                    {
                        offlineProjectsInSqlClause = offlineProjectsInSqlClause.Substring(0, offlineProjectsInSqlClause.Length - 1);
                        offlineProjectsInSqlClause += ")";
                    }
                    else
                    {
                        offlineProjectsInSqlClause += ")";
                    }

                    // Update Offline Project flag on Central Database.
                    using (SqlConnection sqlConnection = new SqlConnection(this.centralConnectionString))
                    {
                        using (SqlCommand sqlCommand = new SqlCommand())
                        {
                            sqlCommand.Connection = sqlConnection;
                            sqlCommand.CommandType = CommandType.Text;
                            sqlCommand.CommandText = "UPDATE Projects Set [IsOffline] ='True' WHERE [OwnerGuid] = '" + ownerGuid + "' and [Guid] IN " + offlineProjectsInSqlClause;
                            sqlCommand.CommandTimeout = SqlCommandTimeout;
                            sqlConnection.Open();
                            sqlCommand.ExecuteNonQuery();
                        }
                    }
                }

                if (offlineFoldersGuids.Count > 0)
                {
                    // Offline Folders SQL IN Clause builder
                    string offlineProjectFoldersInSqlClause = "(";
                    foreach (Guid guid in offlineFoldersGuids)
                    {
                        offlineProjectFoldersInSqlClause += "'" + guid + "'" + ",";
                    }

                    if (offlineProjectFoldersInSqlClause.Count() > 1)
                    {
                        offlineProjectFoldersInSqlClause = offlineProjectFoldersInSqlClause.Substring(0, offlineProjectFoldersInSqlClause.Length - 1);
                        offlineProjectFoldersInSqlClause += ")";
                    }
                    else
                    {
                        offlineProjectFoldersInSqlClause += ")";
                    }

                    // Update Offline Project on Central
                    using (SqlConnection sqlConnection = new SqlConnection(this.centralConnectionString))
                    {
                        using (SqlCommand sqlCommand = new SqlCommand())
                        {
                            sqlCommand.Connection = sqlConnection;
                            sqlCommand.CommandType = CommandType.Text;
                            sqlCommand.CommandText = "UPDATE ProjectFolders Set [IsOffline] ='True' WHERE [OwnerGuid] = '" + ownerGuid + "' and [Guid] IN " + offlineProjectFoldersInSqlClause;
                            sqlCommand.CommandTimeout = SqlCommandTimeout;
                            sqlConnection.Open();
                            sqlCommand.ExecuteNonQuery();
                        }
                    }
                }

                DeleteOfflineProjectsAndFoldersFromCentral(ownerGuid);
                UpdateOfflineChangeTrackings(ownerGuid, this.localConnectionString);
            }
            catch (SqlException ex)
            {
                Log.ErrorException("Offline Projects Exception", ex);
                throw new SynchronizationException(SynchronizationException.ParseSqlException(ex), ex);
            }
            catch (DataException ex)
            {
                Log.ErrorException("Could not get the offline projects from database.", ex);
                throw new SynchronizationException(SyncErrorCodes.InternalError, ex);
            }
        }

        /// <summary>
        /// Deletes the offline projects and folders from central database.
        /// </summary>
        /// <param name="ownerGuid">The owner GUID.</param>        
        [SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "The delete query strings are safe.")]
        private void DeleteOfflineProjectsAndFoldersFromCentral(Guid ownerGuid)
        {
            try
            {
                string[] deleteCommands =
                {
                    " DELETE FROM ProcessStepAssemblyAmounts WHERE [AssemblyGuid] IN (SELECT [AssemblyGuid] FROM GetProcessStepAssemblyAmountsHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    " DELETE FROM ProcessStepPartAmounts WHERE [PartGuid] IN (SELECT [PartGuid] FROM GetProcessStepPartAmountsHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    " DELETE FROM CommoditiesPriceHistory WHERE [CommodityGuid] IN (SELECT [CommodityGuid] FROM GetCommoditiesHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    " DELETE FROM Commodities WHERE [Guid] IN (SELECT [Guid] FROM GetCommoditiesHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    " DELETE FROM Machines WHERE [Guid] IN (SELECT [Guid] FROM GetMachinesHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    " DELETE FROM RawMaterialsPriceHistory WHERE [RawMaterialGuid] IN (SELECT [RawMaterialGuid] FROM GetRawMaterialsHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    " DELETE FROM RawMaterials WHERE [Guid] IN (SELECT [Guid] FROM GetRawMaterialsHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    " DELETE FROM Dies WHERE [Guid] IN (SELECT [Guid] FROM GetDiesHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    " DELETE FROM ConsumablesPriceHistory WHERE [ConsumableGuid] IN (SELECT [ConsumableGuid] FROM GetConsumablesHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    " DELETE FROM Consumables WHERE [Guid] IN (SELECT [Guid] FROM GetConsumablesHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    " DELETE FROM CycleTimeCalculations WHERE [ProcessStepGuid] IN (SELECT [ProcessStepGuid] FROM GetProcessStepsHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    " DELETE FROM ProcessSteps WHERE [Guid] IN (SELECT [Guid] FROM GetProcessStepsHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    " DELETE FROM ProjectMedia WHERE [MediaGuid] IN (SELECT [MediaGuid] FROM GetProjectMediaHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    " DELETE FROM PartMedia WHERE [MediaGuid] IN (SELECT [MediaGuid] FROM GetPartMediaHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    " DELETE FROM AssemblyMedia WHERE [MediaGuid] IN (SELECT [MediaGuid] FROM GetAssemblyMediaHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    " DELETE FROM Parts WHERE [Guid] IN (SELECT [Guid] FROM GetPartsHierarchyForMyProjectsSync(@OwnerId,'True'))                        ",
                    " DELETE FROM Assemblies WHERE [Guid] IN (SELECT [Guid] FROM GetAssemblyHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    " DELETE FROM Processes WHERE [Guid] IN (SELECT [Guid] FROM GetProcessesHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    " DELETE FROM Projects WHERE [Guid] IN (SELECT [Guid] FROM GetProjectsHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    " DELETE FROM ProjectFolders WHERE [Guid] IN (SELECT [Guid] FROM GetProjectFoldersHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    " DELETE FROM Media WHERE [Guid] IN (SELECT [Guid] FROM GetMediaHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    " DELETE FROM Manufacturers WHERE [Guid] IN (SELECT [Guid] FROM GetManufacturersHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    " DELETE FROM OverheadSettings WHERE [Guid] IN (SELECT [Guid] FROM GetOverheadSettingsHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    " DELETE FROM Customers WHERE [Guid] IN (SELECT [CustomerGuid] FROM GetProjectsHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    " DELETE FROM CountrySettings WHERE [Guid] IN (SELECT [Guid] FROM GetCountrySettingsHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    " DELETE FROM Currencies WHERE Guid IN (SELECT Guid FROM GetCurrencyHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    
                    ////" DELETE FROM ProcessStepAssemblyAmounts_tracking WHERE [AssemblyGuid] IN (SELECT [AssemblyGuid] FROM GetProcessStepAssemblyAmountsHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    ////" DELETE FROM ProcessStepPartAmounts_tracking WHERE [PartGuid] IN (SELECT [PartGuid] FROM GetProcessStepPartAmountsHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    ////" DELETE FROM Media_tracking WHERE [Guid] IN (SELECT [Guid] FROM GetMediaHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    ////" DELETE FROM Manufacturers_tracking WHERE [Guid] IN (SELECT [Guid] FROM GetManufacturersHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    ////" DELETE FROM OverheadSettings_tracking WHERE [Guid] IN (SELECT [Guid] FROM GetOverheadSettingsHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    ////" DELETE FROM Customers_tracking WHERE [Guid] IN (SELECT [CustomerGuid] FROM GetProjectsHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    ////" DELETE FROM CountrySettings_tracking WHERE [Guid] IN (SELECT [Guid] FROM GetCountrySettingsHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    ////" DELETE FROM CommoditiesPriceHistory_tracking WHERE [CommodityGuid] IN (SELECT [CommodityGuid] FROM GetCommoditiesHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    ////" DELETE FROM Commodities_tracking WHERE [Guid] IN (SELECT [Guid] FROM GetCommoditiesHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    ////" DELETE FROM Machines_tracking WHERE [Guid] IN (SELECT [Guid] FROM GetMachinesHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    ////" DELETE FROM RawMaterialsPriceHistory_tracking WHERE [RawMaterialGuid] IN (SELECT [RawMaterialGuid] FROM GetRawMaterialsHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    ////" DELETE FROM RawMaterials_tracking WHERE [Guid] IN (SELECT [Guid] FROM GetRawMaterialsHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    ////" DELETE FROM Dies_tracking WHERE [Guid] IN (SELECT [Guid] FROM GetDiesHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    ////" DELETE FROM ConsumablesPriceHistory_tracking WHERE [ConsumableGuid] IN (SELECT [ConsumableGuid] FROM GetConsumablesHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    ////" DELETE FROM Consumables_tracking WHERE [Guid] IN (SELECT [Guid] FROM GetConsumablesHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    ////" DELETE FROM CycleTimeCalculations_tracking WHERE [ProcessStepGuid] IN (SELECT [ProcessStepGuid] FROM GetProcessStepsHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    ////" DELETE FROM ProcessSteps_tracking WHERE [Guid] IN (SELECT [Guid] FROM GetProcessStepsHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    ////" DELETE FROM ProjectMedia_tracking WHERE [MediaGuid] IN (SELECT [MediaGuid] FROM GetProjectMediaHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    ////" DELETE FROM PartMedia_tracking WHERE [MediaGuid] IN (SELECT [MediaGuid] FROM GetPartMediaHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    ////" DELETE FROM AssemblyMedia_tracking WHERE [MediaGuid] IN (SELECT [MediaGuid] FROM GetAssemblyMediaHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    ////" DELETE FROM Processes_tracking WHERE [Guid] IN (SELECT [Guid] FROM GetProcessesHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    ////" DELETE FROM Parts_tracking WHERE [Guid] IN (SELECT [Guid] FROM GetPartsHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    ////" DELETE FROM Assemblies_tracking WHERE [Guid] IN (SELECT [Guid] FROM GetAssemblyHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    ////" DELETE FROM Projects_tracking WHERE [Guid] IN (SELECT [Guid] FROM GetProjectsHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    ////" DELETE FROM ProjectFolders_tracking WHERE [Guid] IN (SELECT [Guid] FROM GetProjectFoldersHierarchyForMyProjectsSync(@OwnerId,'True'))",
                    
                    //// Delete media without reference to Project, Assembly, Part, RawMaterials,Commodities, Machines, Dies, ProcessSteps
                    @"DELETE FROM Media WHERE 
                                             [Guid]  IN (
                                                             SELECT m.[Guid] FROM Media m
                                                             LEFT JOIN RawMaterials r on m.[Guid] = r.[MediaGuid]
                                                             LEFT JOIN Commodities c on m.[Guid] = c.[MediaGuid]
                                                             LEFT JOIN Machines ma on m.[Guid] = ma.[MediaGuid]
                                                             LEFT JOIN Dies d on m.[Guid] = d.[MediaGuid] 
                                                             LEFT JOIN ProcessSteps p on m.[Guid] = p.[MediaGuid]
                                                             LEFT JOIN ProjectMedia projm on m.[Guid] = projm.[MediaGuid]
                                                             LEFT JOIN AssemblyMedia am on m.[Guid] = am.[MediaGuid]
                                                             LEFT JOIN PartMedia pm on m.[Guid] = pm.[MediaGuid]
                                                             WHERE r.MediaGuid is NULL AND
                                                                   c.MediaGuid is NULL AND
                                                                   ma.MediaGuid is NULL AND
                                                                   d.MediaGuid is NULL AND
                                                                   p.MediaGuid is NULL AND
                                                                   projm.MediaGuid is NULL AND
                                                                   am.MediaGuid is NULL AND
                                                                   pm.MediaGuid is NULL
                                             )"
                };

                using (SqlConnection sqlConnection = new SqlConnection(this.centralConnectionString))
                {
                    sqlConnection.Open();

                    // Delete Offline Projects From Central.
                    // Delete table data and then change tracking data.
                    // The deletion order is important; first the sub-entities must be deleted and and only after that can the parent entity be deleted.
                    foreach (string deleteCommand in deleteCommands)
                    {
                        using (SqlCommand sqlCommand = new SqlCommand())
                        {
                            sqlCommand.Connection = sqlConnection;
                            sqlCommand.CommandType = CommandType.Text;
                            sqlCommand.CommandText = deleteCommand;
                            sqlCommand.Parameters.Add(new SqlParameter("@OwnerId", ownerGuid));
                            sqlCommand.CommandTimeout = SqlCommandTimeout;

                            sqlCommand.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Log.ErrorException("Offline Projects Delete exception.", ex);
            }
        }

        /// <summary>
        /// Updates the change tracking information for offline projects.
        /// </summary>
        /// <param name="ownerGuid">The owner GUID.</param>
        /// <param name="connectionString">The connection string.</param>
        private void UpdateOfflineChangeTrackings(Guid ownerGuid, string connectionString)
        {
            try
            {
                // Update Offline Projects Change Time on Local so the My Projects Sync Scope can detect them as changed.
                string updateOfflineProjectsCommandText = @"BEGIN TRANSACTION UpdateOfflineProjects
                                                                UPDATE  ProcessStepAssemblyAmounts_tracking 
                                                                        SET [last_change_datetime] = GETDATE(),
                                                                            [local_update_peer_key] = 0,
                                                                            [restore_timestamp] = NULL, 
                                                                            [update_scope_local_id] = NULL 
                                                                        WHERE [sync_row_is_tombstone] = 0 AND 
                                                                               [AssemblyGuid] IN (SELECT [AssemblyGuid] FROM GetProcessStepAssemblyAmountsHierarchyForMyProjectsSync(@OwnerId,'True'))
                                                                UPDATE  ProcessStepPartAmounts_tracking 
                                                                        SET [last_change_datetime] = GETDATE(),
                                                                            [local_update_peer_key] = 0,
                                                                            [restore_timestamp] = NULL, 
                                                                            [update_scope_local_id] = NULL  
                                                                        WHERE [sync_row_is_tombstone] = 0 AND  [PartGuid] IN (SELECT [PartGuid] FROM GetProcessStepPartAmountsHierarchyForMyProjectsSync(@OwnerId,'True'))
                                                                UPDATE  CommoditiesPriceHistory_tracking 
                                                                        SET [last_change_datetime] = GETDATE(),
                                                                            [local_update_peer_key] = 0,
                                                                            [restore_timestamp] = NULL, 
                                                                            [update_scope_local_id] = NULL  
                                                                        WHERE [sync_row_is_tombstone] = 0 AND  [CommodityGuid] IN (SELECT [CommodityGuid] FROM GetCommoditiesHierarchyForMyProjectsSync(@OwnerId,'True'))
                                                                UPDATE  Commodities_tracking 
                                                                        SET [last_change_datetime] = GETDATE(),
                                                                            [local_update_peer_key] = 0,
                                                                            [restore_timestamp] = NULL, 
                                                                            [update_scope_local_id] = NULL  
                                                                        WHERE [sync_row_is_tombstone] = 0 AND  [Guid] IN (SELECT [Guid] FROM GetCommoditiesHierarchyForMyProjectsSync(@OwnerId,'True'))
                                                                UPDATE  Machines_tracking 
                                                                        SET [last_change_datetime] = GETDATE(),
                                                                            [local_update_peer_key] = 0,
                                                                            [restore_timestamp] = NULL, 
                                                                            [update_scope_local_id] = NULL 
                                                                        WHERE [sync_row_is_tombstone] = 0 AND  [Guid] IN (SELECT [Guid] FROM GetMachinesHierarchyForMyProjectsSync(@OwnerId,'True'))
                                                                UPDATE  RawMaterialsPriceHistory_tracking 
                                                                        SET [last_change_datetime] = GETDATE(),
                                                                            [local_update_peer_key] = 0,
                                                                            [restore_timestamp] = NULL, 
                                                                            [update_scope_local_id] = NULL 
                                                                        WHERE [sync_row_is_tombstone] = 0 AND  [RawMaterialGuid] IN (SELECT [RawMaterialGuid] FROM GetRawMaterialsHierarchyForMyProjectsSync(@OwnerId,'True'))
                                                                UPDATE  RawMaterials_tracking 
                                                                        SET [last_change_datetime] = GETDATE() ,
                                                                            [local_update_peer_key] = 0,
                                                                            [restore_timestamp] = NULL, 
                                                                            [update_scope_local_id] = NULL 
                                                                        WHERE [sync_row_is_tombstone] = 0 AND  [Guid] IN (SELECT [Guid] FROM GetRawMaterialsHierarchyForMyProjectsSync(@OwnerId,'True'))
                                                                UPDATE  Dies_tracking 
                                                                        SET [last_change_datetime] = GETDATE(),
                                                                            [local_update_peer_key] = 0,
                                                                            [restore_timestamp] = NULL, 
                                                                            [update_scope_local_id] = NULL 
                                                                        WHERE [sync_row_is_tombstone] = 0 AND  [Guid] IN (SELECT [Guid] FROM GetDiesHierarchyForMyProjectsSync(@OwnerId,'True'))
                                                                UPDATE  ConsumablesPriceHistory_tracking 
                                                                        SET [last_change_datetime] = GETDATE(),
                                                                            [local_update_peer_key] = 0,
                                                                            [restore_timestamp] = NULL, 
                                                                            [update_scope_local_id] = NULL 
                                                                        WHERE  [sync_row_is_tombstone] = 0 AND  [ConsumableGuid] IN (SELECT [ConsumableGuid] FROM GetConsumablesHierarchyForMyProjectsSync(@OwnerId,'True'))
                                                                UPDATE  Consumables_tracking 
                                                                        SET [last_change_datetime] = GETDATE(),
                                                                            [local_update_peer_key] = 0,
                                                                            [restore_timestamp] = NULL, 
                                                                            [update_scope_local_id] = NULL 
                                                                        WHERE [sync_row_is_tombstone] = 0 AND  [Guid] IN (SELECT [Guid] FROM GetConsumablesHierarchyForMyProjectsSync(@OwnerId,'True'))
                                                                UPDATE  CycleTimeCalculations_tracking 
                                                                        SET [last_change_datetime] = GETDATE(),
                                                                            [local_update_peer_key] = 0,
                                                                            [restore_timestamp] = NULL, 
                                                                            [update_scope_local_id] = NULL  
                                                                        WHERE [sync_row_is_tombstone] = 0 AND  [ProcessStepGuid] IN (SELECT [ProcessStepGuid] FROM GetProcessStepsHierarchyForMyProjectsSync(@OwnerId,'True'))
                                                                UPDATE  ProcessSteps_tracking 
                                                                        SET [last_change_datetime] = GETDATE(),
                                                                            [local_update_peer_key] = 0,
                                                                            [restore_timestamp] = NULL, 
                                                                            [update_scope_local_id] = NULL  
                                                                        WHERE [sync_row_is_tombstone] = 0 AND  [Guid] IN (SELECT [Guid] FROM GetProcessStepsHierarchyForMyProjectsSync(@OwnerId,'True'))
                                                                UPDATE  ProjectMedia_tracking 
                                                                        SET [last_change_datetime] = GETDATE(),
                                                                            [local_update_peer_key] = 0,
                                                                            [restore_timestamp] = NULL, 
                                                                            [update_scope_local_id] = NULL 
                                                                        WHERE [sync_row_is_tombstone] = 0 AND  [MediaGuid] IN (SELECT [MediaGuid] FROM GetProjectMediaHierarchyForMyProjectsSync(@OwnerId,'True'))
                                                                UPDATE  PartMedia_tracking
                                                                        SET [last_change_datetime] = GETDATE(),
                                                                            [local_update_peer_key] = 0,
                                                                            [restore_timestamp] = NULL, 
                                                                            [update_scope_local_id] = NULL 
                                                                        WHERE [sync_row_is_tombstone] = 0 AND  [MediaGuid] IN (SELECT [MediaGuid] FROM GetPartMediaHierarchyForMyProjectsSync(@OwnerId,'True'))
                                                                UPDATE  AssemblyMedia_tracking 
                                                                        SET [last_change_datetime] = GETDATE(),
                                                                            [local_update_peer_key] = 0,
                                                                            [restore_timestamp] = NULL, 
                                                                            [update_scope_local_id] = NULL 
                                                                        WHERE [sync_row_is_tombstone] = 0 AND  [MediaGuid] IN (SELECT [MediaGuid] FROM GetAssemblyMediaHierarchyForMyProjectsSync(@OwnerId,'True'))
                                                                UPDATE  Parts_tracking
                                                                        SET [last_change_datetime] = GETDATE(),
                                                                            [local_update_peer_key] = 0,
                                                                            [restore_timestamp] = NULL, 
                                                                            [update_scope_local_id] = NULL  
                                                                        WHERE [sync_row_is_tombstone] = 0 AND  [Guid] IN (SELECT [Guid] FROM GetPartsHierarchyForMyProjectsSync(@OwnerId,'True'))
                                                                UPDATE  Assemblies_tracking
                                                                        SET [last_change_datetime] = GETDATE(),
                                                                            [local_update_peer_key] = 0,
                                                                            [restore_timestamp] = NULL, 
                                                                            [update_scope_local_id] = NULL 
                                                                        WHERE [sync_row_is_tombstone] = 0 AND  [Guid] IN (SELECT [Guid] FROM GetAssemblyHierarchyForMyProjectsSync(@OwnerId,'True'))
                                                                UPDATE  Processes_tracking 
                                                                        SET [last_change_datetime] = GETDATE(),
                                                                            [local_update_peer_key] = 0,
                                                                            [restore_timestamp] = NULL, 
                                                                            [update_scope_local_id] = NULL 
                                                                        WHERE [sync_row_is_tombstone] = 0 AND  [Guid] IN (SELECT [Guid] FROM GetProcessesHierarchyForMyProjectsSync(@OwnerId,'True'))
                                                                UPDATE  Projects_tracking
                                                                        SET [last_change_datetime] = GETDATE(),
                                                                            [local_update_peer_key] = 0,
                                                                            [restore_timestamp] = NULL, 
                                                                            [update_scope_local_id] = NULL  
                                                                        WHERE [sync_row_is_tombstone] = 0 AND  [Guid] IN (SELECT [Guid] FROM GetProjectsHierarchyForMyProjectsSync(@OwnerId,'True'))
                                                                UPDATE  ProjectFolders_tracking
                                                                        SET [last_change_datetime] = GETDATE(),
                                                                            [local_update_peer_key] = 0,
                                                                            [restore_timestamp] = NULL, 
                                                                            [update_scope_local_id] = NULL 
                                                                        WHERE [sync_row_is_tombstone] = 0 AND  [Guid] IN (SELECT [Guid] FROM GetProjectFoldersHierarchyForMyProjectsSync(@OwnerId,'True'))
                                                                UPDATE  Media_tracking 
                                                                        SET [last_change_datetime] = GETDATE(),
                                                                            [local_update_peer_key] = 0,
                                                                            [restore_timestamp] = NULL, 
                                                                            [update_scope_local_id] = NULL  
                                                                        WHERE [sync_row_is_tombstone] = 0 AND  [Guid] IN (SELECT [Guid] FROM GetMediaHierarchyForMyProjectsSync(@OwnerId,'True'))
                                                                UPDATE  Manufacturers_tracking 
                                                                        SET [last_change_datetime] = GETDATE(),
                                                                            [local_update_peer_key] = 0,
                                                                            [restore_timestamp] = NULL, 
                                                                            [update_scope_local_id] = NULL  
                                                                        WHERE [sync_row_is_tombstone] = 0 AND  [Guid] IN (SELECT [Guid] FROM GetManufacturersHierarchyForMyProjectsSync(@OwnerId,'True'))
                                                                UPDATE  OverheadSettings_tracking 
                                                                        SET [last_change_datetime] = GETDATE(),
                                                                            [local_update_peer_key] = 0,
                                                                            [restore_timestamp] = NULL, 
                                                                            [update_scope_local_id] = NULL 
                                                                        WHERE [sync_row_is_tombstone] = 0 AND  [Guid] IN (SELECT [Guid] FROM GetOverheadSettingsHierarchyForMyProjectsSync(@OwnerId,'True'))
                                                                UPDATE  Customers_tracking 
                                                                        SET [last_change_datetime] = GETDATE(),
                                                                            [local_update_peer_key] = 0,
                                                                            [restore_timestamp] = NULL, 
                                                                            [update_scope_local_id] = NULL 
                                                                        WHERE [sync_row_is_tombstone] = 0 AND  [Guid] IN (SELECT [CustomerGuid] FROM GetProjectsHierarchyForMyProjectsSync(@OwnerId,'True'))
                                                                UPDATE  CountrySettings_tracking 
                                                                        SET [last_change_datetime] = GETDATE(),
                                                                            [local_update_peer_key] = 0,
                                                                            [restore_timestamp] = NULL, 
                                                                            [update_scope_local_id] = NULL 
                                                                        WHERE [sync_row_is_tombstone] = 0 AND  [Guid] IN (SELECT [Guid] FROM GetCountrySettingsHierarchyForMyProjectsSync(@OwnerId,'True'))
                                                                COMMIT TRANSACTION UpdateOfflineProjects";

                using (SqlConnection sqlConnection = new SqlConnection(connectionString))
                {
                    sqlConnection.Open();

                    using (SqlCommand sqlCommand = new SqlCommand())
                    {
                        sqlCommand.Connection = sqlConnection;
                        sqlCommand.CommandType = CommandType.Text;
                        sqlCommand.CommandText = updateOfflineProjectsCommandText;
                        sqlCommand.Parameters.Add(new SqlParameter("@OwnerId", ownerGuid));
                        sqlCommand.CommandTimeout = SqlCommandTimeout;
                        sqlCommand.ExecuteNonQuery();
                    }
                }
            }
            catch (SqlException ex)
            {
                Log.ErrorException("An error occurred while updating change tracking data for offline projects.", ex);
            }
        }

        #endregion

        #region StaticData & Settings

        /// <summary>
        /// Provisions the template for static data and settings.
        /// After you define the synchronization scope, you provision the database to create a change-tracking and
        /// metadata management infrastructure that consists of metadata tables, triggers, and stored procedures.
        /// </summary>
        /// <param name="sqlConnection">The SQL connection.</param>
        /// <param name="scopeName">Name of the scope.</param>
        /// <param name="sqlConnectionString">The SQL connection string.</param>
        private void ProvisionTemplateForStaticDataAndSettings(SqlConnection sqlConnection, string scopeName, string sqlConnectionString)
        {
            try
            {
                string templateName = Enum.GetName(typeof(SyncTemplates), SyncTemplates.StaticDataAndSettings_Template);

                // create a new scope description and add the appropriate tables to this scope
                DbSyncScopeDescription scopeDesc = new DbSyncScopeDescription(templateName);

                // class to be used to provision the scope defined above
                SqlSyncScopeProvisioning serverConfig = new SqlSyncScopeProvisioning(sqlConnection);

                // For each table that is synchronized, changes are selected from the server database in the order of inserts, updates, 
                // and then deletes. Changes are applied to the client database in the order of deletes, inserts, and then updates. 
                // When several tables are synchronized, the order in which each table is processed depends on
                // the order in which its SyncTable object was added to the collection of tables for the synchronization agent.
                DbSyncTableDescription basicSettingsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("BasicSettings", sqlConnection);
                scopeDesc.Tables.Add(basicSettingsTableDesc);
                DbSyncTableDescription overheadSettingsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("OverheadSettings", sqlConnection);
                scopeDesc.Tables.Add(overheadSettingsTableDesc);
                DbSyncTableDescription measurementUnitsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("MeasurementUnits", sqlConnection);
                scopeDesc.Tables.Add(measurementUnitsTableDesc);
                DbSyncTableDescription currenciesTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Currencies", sqlConnection);
                scopeDesc.Tables.Add(currenciesTableDesc);
                DbSyncTableDescription currenciesExchangeRateHistoryTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("CurrenciesExchangeRateHistory", sqlConnection);
                scopeDesc.Tables.Add(currenciesExchangeRateHistoryTableDesc);
                DbSyncTableDescription countrySettingsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("CountrySettings", sqlConnection);
                scopeDesc.Tables.Add(countrySettingsTableDesc);
                DbSyncTableDescription countrySettingsValueHistoryTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("CountrySettingsValueHistory", sqlConnection);
                scopeDesc.Tables.Add(countrySettingsValueHistoryTableDesc);
                DbSyncTableDescription countriesTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Countries", sqlConnection);
                scopeDesc.Tables.Add(countriesTableDesc);
                DbSyncTableDescription usersTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Users", sqlConnection);
                scopeDesc.Tables.Add(usersTableDesc);
                DbSyncTableDescription countryStatesTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("CountryStates", sqlConnection);
                scopeDesc.Tables.Add(countryStatesTableDesc);
                DbSyncTableDescription processStepsClassificationTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("ProcessStepsClassification", sqlConnection);
                scopeDesc.Tables.Add(processStepsClassificationTableDesc);
                DbSyncTableDescription machinesClassificationTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("MachinesClassification", sqlConnection);
                scopeDesc.Tables.Add(machinesClassificationTableDesc);
                DbSyncTableDescription materialsClassificationTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("MaterialsClassification", sqlConnection);
                scopeDesc.Tables.Add(materialsClassificationTableDesc);

                // Add a filter column to a SqlSyncTableProvisioning object in the 
                // synchronization scope by using AddFilterColumn. 
                // This adds the filter column to the tracking table that tracks changes
                // for the base table.

                // Define one or more filter parameters by adding SqlParameter objects to the
                // FilterParameters collection of the SqlSyncTableProvisioning object. 
                // This adds the specified parameters to the argument list of the stored
                // procedure that enumerates changes during synchronization.

                // Add a filter clause that defines the relationship between parameter values
                // and column values by setting the FilterClause property of the
                // SqlSyncTableProvisioning object. The filter clause is a WHERE clause 
                // without the WHERE keyword. The [side] alias is an alias for the tracking 
                // table. The parameters match the parameters specified in the FilterParameters
                // collection. At this point you are only defining the relationship between 
                // the filter parameters and columns. The actual values for the parameters 
                // will be specified later, when the filtered scope is created.

                // Represents the provisioning of a SQL Server database for a particular scope that is represented by a DbSyncScopeDescription object. 
                SqlSyncScopeProvisioning serverTemplate = new SqlSyncScopeProvisioning(sqlConnection, scopeDesc, SqlSyncScopeProvisioningType.Template);
                SqlParameter isReleasedParam = new SqlParameter("@isReleased", SqlDbType.Bit);
                SqlParameter isMasterDataParam = new SqlParameter("@isMasterData", SqlDbType.Bit);

                serverTemplate.Tables["CountryStates"].AddFilterColumn("SettingsGuid");

                serverTemplate.Tables["CountrySettingsValueHistory"].AddFilterColumn("CountrySettingsGuid");
                serverTemplate.Tables["CountrySettingsValueHistory"].AddFilterColumn("Guid");
                serverTemplate.Tables["CountrySettingsValueHistory"].FilterClause = "(side.CountrySettingsGuid IN (SELECT CountrySettings_tracking.Guid FROM CountrySettings_tracking WHERE CountrySettings_tracking.OwnerGuid is NULL))";

                serverTemplate.Tables["CountrySettings"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["CountrySettings"].AddFilterColumn("Guid");
                serverTemplate.Tables["CountrySettings"].FilterClause = "(([side].[OwnerGuid] is NULL and [side].[Guid] IN (SELECT [Countries_tracking].[SettingsGuid] FROM [Countries_tracking] WHERE [Countries_tracking].[IsReleased]=@isReleased)) or ([side].[OwnerGuid] is NULL and [side].[Guid] IN (SELECT [CountryStates_tracking].[SettingsGuid] FROM [CountryStates_tracking])))";
                serverTemplate.Tables["CountrySettings"].FilterParameters.Add(isReleasedParam);

                serverTemplate.Tables["OverheadSettings"].AddFilterColumn("OwnerGuid");
                serverTemplate.Tables["OverheadSettings"].AddFilterColumn("Guid");
                serverTemplate.Tables["OverheadSettings"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["OverheadSettings"].FilterClause = "([side].[OwnerGuid] is NULL and [side].[IsMasterData] = @isMasterData)";
                serverTemplate.Tables["OverheadSettings"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["MeasurementUnits"].AddFilterColumn("IsReleased");
                serverTemplate.Tables["MeasurementUnits"].AddFilterColumn("Guid");
                serverTemplate.Tables["MeasurementUnits"].FilterClause = "([side].[IsReleased] = @isReleased)";
                serverTemplate.Tables["MeasurementUnits"].FilterParameters.Add(isReleasedParam);

                serverTemplate.Tables["Currencies"].AddFilterColumn("IsReleased");
                serverTemplate.Tables["Currencies"].AddFilterColumn("IsMasterData");
                serverTemplate.Tables["Currencies"].AddFilterColumn("Guid");
                serverTemplate.Tables["Currencies"].FilterClause = "(side.IsReleased = @isReleased AND side.IsMasterData = @isMasterData)";
                serverTemplate.Tables["Currencies"].FilterParameters.Add(isReleasedParam);
                serverTemplate.Tables["Currencies"].FilterParameters.Add(isMasterDataParam);

                serverTemplate.Tables["CurrenciesExchangeRateHistory"].AddFilterColumn("Guid");
                serverTemplate.Tables["CurrenciesExchangeRateHistory"].AddFilterColumn("CurrencyGuid");
                serverTemplate.Tables["CurrenciesExchangeRateHistory"].FilterClause = "(side.Guid IN (SELECT Currencies_tracking.Guid FROM Currencies_tracking WHERE Currencies_tracking.IsMasterData = 'True'))";

                serverTemplate.Tables["Countries"].AddFilterColumn("IsReleased");
                serverTemplate.Tables["Countries"].AddFilterColumn("Guid");
                serverTemplate.Tables["Countries"].AddFilterColumn("SettingsGuid");
                serverTemplate.Tables["Countries"].FilterClause = "([side].[IsReleased] = @isReleased and [side].[SettingsGuid] IN ( SELECT [CountrySettings_tracking].[Guid] FROM [CountrySettings_tracking] WHERE [CountrySettings_tracking].OwnerGuid is NULL))";
                serverTemplate.Tables["Countries"].FilterParameters.Add(isReleasedParam);

                serverTemplate.Tables["Users"].AddFilterColumn("IsReleased");
                serverTemplate.Tables["Users"].AddFilterColumn("Guid");
                serverTemplate.Tables["Users"].FilterClause = "[side].[IsReleased] = @isReleased";
                serverTemplate.Tables["Users"].FilterParameters.Add(isReleasedParam);

                serverTemplate.Tables["ProcessStepsClassification"].AddFilterColumn("IsReleased");
                serverTemplate.Tables["ProcessStepsClassification"].AddFilterColumn("Guid");
                serverTemplate.Tables["ProcessStepsClassification"].FilterClause = "[side].[IsReleased] = @isReleased";
                serverTemplate.Tables["ProcessStepsClassification"].FilterParameters.Add(isReleasedParam);

                serverTemplate.Tables["MachinesClassification"].AddFilterColumn("IsReleased");
                serverTemplate.Tables["MachinesClassification"].AddFilterColumn("Guid");
                serverTemplate.Tables["MachinesClassification"].FilterClause = "[side].[IsReleased] = @isReleased";
                serverTemplate.Tables["MachinesClassification"].FilterParameters.Add(isReleasedParam);

                serverTemplate.Tables["MaterialsClassification"].AddFilterColumn("IsReleased");
                serverTemplate.Tables["MaterialsClassification"].AddFilterColumn("Guid");
                serverTemplate.Tables["MaterialsClassification"].FilterClause = "[side].[IsReleased] = @isReleased";
                serverTemplate.Tables["MaterialsClassification"].FilterParameters.Add(isReleasedParam);

                //----------------------------------------------------------------------------------------
                // Sets whether to create a set of stored procedures that insert, update, and delete data and synchronization metadata.
                serverTemplate.SetCreateTrackingTableDefault(DbSyncCreationOption.CreateOrUseExisting);

                // Sets whether to create on base tables triggers that update tracking tables.
                serverTemplate.SetCreateTriggersDefault(DbSyncCreationOption.CreateOrUseExisting);

                // Sets whether to create a set of stored procedures that insert, update, and delete data and synchronization metadata.
                serverTemplate.SetCreateProceduresDefault(DbSyncCreationOption.CreateOrUseExisting);

                // Sets whether to create base tables when a scope is configured.
                serverTemplate.SetCreateTableDefault(DbSyncCreationOption.CreateOrUseExisting);

                // Sets whether to insert metadata into change-tracking tables for rows that already exist in base tables.
                serverTemplate.SetPopulateTrackingTableDefault(DbSyncCreationOption.CreateOrUseExisting);

                // create a new select changes stored proc for this scope
                // Sets whether to create for any additional scopes sets of stored 
                // procedures that insert, update, and delete data and synchronization metadata.
                serverTemplate.SetCreateProceduresForAdditionalScopeDefault(DbSyncCreationOption.Create);

                if (!serverConfig.TemplateExists(templateName))
                {
                    // Create the template in the database.
                    serverTemplate.Apply();
                }
                else
                {
                    HandleSchemaChanges(sqlConnectionString, scopeName, scopeDesc, serverTemplate);
                }
            }
            catch (SyncException ex)
            {
                Log.ErrorException("Could not configure sql sync provider.", ex);
                throw new SynchronizationException(SyncErrorCodes.InternalError, ex);
            }
            catch (SqlException ex)
            {
                Log.ErrorException("SQL Exception encountered.", ex);
                throw new SynchronizationException(SynchronizationException.ParseSqlException(ex), ex);
            }
        }

        /// <summary>
        /// Populates the static data and settings scope from template.
        /// Sets the template-based scope description from which the database should be provisioned.
        /// Templates are used to describe a parameter-based filtered scope.
        /// A filtered scope is then created by using PopulateFromTemplate to retrieve the template
        /// description, and by defining specific values for the parameters described in the template.
        /// </summary>
        /// <param name="provider">The provider.</param>
        /// <param name="scopeName">Name of the scope.</param>
        /// <param name="serverConfig">The server scope provisioning.</param>
        /// <param name="isReleased">if set to <c>true</c> [is released].</param>
        /// <param name="sqlConnectionString">The SQL connection string.</param>
        private void PopulateStaticDataAndSettingsScopeFromTemplate(SqlSyncProvider provider, string scopeName, SqlSyncScopeProvisioning serverConfig, bool isReleased, string sqlConnectionString)
        {
            try
            {
                // Sets the template-based scope description from which the database should be provisioned.
                serverConfig.PopulateFromTemplate(provider.ScopeName, Enum.GetName(typeof(SyncTemplates), SyncTemplates.StaticDataAndSettings_Template));

                serverConfig.Tables["OverheadSettings"].FilterParameters["@isMasterData"].Value = true;
                serverConfig.Tables["CountrySettings"].FilterParameters["@isReleased"].Value = isReleased;
                serverConfig.Tables["MeasurementUnits"].FilterParameters["@isReleased"].Value = isReleased;
                serverConfig.Tables["Currencies"].FilterParameters["@isReleased"].Value = isReleased;
                serverConfig.Tables["Countries"].FilterParameters["@isReleased"].Value = isReleased;
                serverConfig.Tables["Users"].FilterParameters["@isReleased"].Value = isReleased;
                serverConfig.Tables["ProcessStepsClassification"].FilterParameters["@isReleased"].Value = isReleased;
                serverConfig.Tables["MachinesClassification"].FilterParameters["@isReleased"].Value = isReleased;
                serverConfig.Tables["MaterialsClassification"].FilterParameters["@isReleased"].Value = isReleased;
                serverConfig.Tables["Currencies"].FilterParameters["@isMasterData"].Value = true;

                if (!serverConfig.ScopeExists(scopeName))
                {
                    serverConfig.Apply();
                }
                else
                {
                    HandleScopeParametersChange(scopeName, serverConfig, sqlConnectionString);
                }
            }
            catch (DbProvisioningException ex)
            {
                Log.WarnException("You try to synchronize a scope that is currently being provisioned to a SQL Server.", ex);
                throw new SynchronizationException(SyncErrorCodes.InternalError, ex);
            }
            catch (SyncException ex)
            {
                Log.ErrorException("Could not populate scope from template", ex);
                throw new SynchronizationException(SyncErrorCodes.InternalError, ex);
            }
            catch (SqlException ex)
            {
                Log.WarnException("SQL Exception encountered.", ex);
                throw new SynchronizationException(SynchronizationException.ParseSqlException(ex), ex);
            }
        }

        #endregion

        #region Logs

        /// <summary>
        /// Provisions the template for the logs.
        /// After you define the synchronization scope, you provision the database to create a change-tracking and
        /// metadata management infrastructure that consists of metadata tables, triggers, and stored procedures.
        /// </summary>
        /// <param name="sqlConnection">The SQL connection.</param>
        /// <param name="scopeName">Name of the scope.</param>
        /// <param name="sqlConnectionString">The SQL connection string.</param>
        private void ProvisionTemplateForLogs(SqlConnection sqlConnection, string scopeName, string sqlConnectionString)
        {
            try
            {
                string templateName = Enum.GetName(typeof(SyncTemplates), SyncTemplates.Logs_Template);

                // create a new scope description and add the appropriate tables to this scope
                DbSyncScopeDescription scopeDesc = new DbSyncScopeDescription(templateName);

                // class to be used to provision the scope defined above
                SqlSyncScopeProvisioning serverConfig = new SqlSyncScopeProvisioning(sqlConnection);

                // For each table that is synchronized, changes are selected from the server database in the order of inserts, updates, 
                // and then deletes. Changes are applied to the client database in the order of deletes, inserts, and then updates. 
                // When several tables are synchronized, the order in which each table is processed depends on
                // the order in which its SyncTable object was added to the collection of tables for the synchronization agent.
                DbSyncTableDescription logsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Logs", sqlConnection);
                scopeDesc.Tables.Add(logsTableDesc);

                // Add a filter column to a SqlSyncTableProvisioning object in the 
                // synchronization scope by using AddFilterColumn. 
                // This adds the filter column to the tracking table that tracks changes
                // for the base table.

                // Define one or more filter parameters by adding SqlParameter objects to the
                // FilterParameters collection of the SqlSyncTableProvisioning object. 
                // This adds the specified parameters to the argument list of the stored
                // procedure that enumerates changes during synchronization.

                // Add a filter clause that defines the relationship between parameter values
                // and column values by setting the FilterClause property of the
                // SqlSyncTableProvisioning object. The filter clause is a WHERE clause 
                // without the WHERE keyword. The [side] alias is an alias for the tracking 
                // table. The parameters match the parameters specified in the FilterParameters
                // collection. At this point you are only defining the relationship between 
                // the filter parameters and columns. The actual values for the parameters 
                // will be specified later, when the filtered scope is created.

                // Represents the provisioning of a SQL Server database for a particular scope that is represented by a DbSyncScopeDescription object. 
                SqlSyncScopeProvisioning serverTemplate = new SqlSyncScopeProvisioning(sqlConnection, scopeDesc, SqlSyncScopeProvisioningType.Template);

                //----------------------------------------------------------------------------------------
                // Sets whether to create a set of stored procedures that insert, update, and delete data and synchronization metadata.
                serverTemplate.SetCreateTrackingTableDefault(DbSyncCreationOption.CreateOrUseExisting);

                // Sets whether to create on base tables triggers that update tracking tables.
                serverTemplate.SetCreateTriggersDefault(DbSyncCreationOption.CreateOrUseExisting);

                // Sets whether to create a set of stored procedures that insert, update, and delete data and synchronization metadata.
                serverTemplate.SetCreateProceduresDefault(DbSyncCreationOption.CreateOrUseExisting);

                // Sets whether to create base tables when a scope is configured.
                serverTemplate.SetCreateTableDefault(DbSyncCreationOption.CreateOrUseExisting);

                // Sets whether to insert metadata into change-tracking tables for rows that already exist in base tables.
                serverTemplate.SetPopulateTrackingTableDefault(DbSyncCreationOption.CreateOrUseExisting);

                // create a new select changes stored proc for this scope
                // Sets whether to create for any additional scopes sets of stored 
                // procedures that insert, update, and delete data and synchronization metadata.
                serverTemplate.SetCreateProceduresForAdditionalScopeDefault(DbSyncCreationOption.Create);

                if (!serverConfig.TemplateExists(templateName))
                {
                    // Create the template in the database.
                    serverTemplate.Apply();
                }
                else
                {
                    HandleSchemaChanges(sqlConnectionString, scopeName, scopeDesc, serverTemplate);
                }
            }
            catch (SyncException ex)
            {
                Log.ErrorException("Could not configure sql sync provider.", ex);
                throw new SynchronizationException(SyncErrorCodes.InternalError, ex);
            }
            catch (SqlException ex)
            {
                Log.ErrorException("SQL Exception encountered.", ex);
                throw new SynchronizationException(SynchronizationException.ParseSqlException(ex), ex);
            }
        }

        /// <summary>
        /// Populates the logs scope from template.
        /// Sets the template-based scope description from which the database should be provisioned.
        /// Templates are used to describe a parameter-based filtered scope.
        /// A filtered scope is then created by using PopulateFromTemplate to retrieve the template
        /// description, and by defining specific values for the parameters described in the template.
        /// </summary>
        /// <param name="provider">The provider.</param>
        /// <param name="scopeName">Name of the scope.</param>
        /// <param name="serverConfig">The server scope provisioning.</param>
        /// <param name="sqlConnectionString">The SQL connection string.</param>
        private void PopulateLogsScopeFromTemplate(SqlSyncProvider provider, string scopeName, SqlSyncScopeProvisioning serverConfig, string sqlConnectionString)
        {
            try
            {
                // Sets the template-based scope description from which the database should be provisioned.
                serverConfig.PopulateFromTemplate(provider.ScopeName, Enum.GetName(typeof(SyncTemplates), SyncTemplates.Logs_Template));
                if (!serverConfig.ScopeExists(scopeName))
                {
                    serverConfig.Apply();
                }
                else
                {
                    HandleScopeParametersChange(scopeName, serverConfig, sqlConnectionString);
                }
            }
            catch (DbProvisioningException ex)
            {
                Log.WarnException("You try to synchronize a scope that is currently being provisioned to a SQL Server.", ex);
                throw new SynchronizationException(SyncErrorCodes.InternalError, ex);
            }
            catch (SyncException ex)
            {
                Log.ErrorException("Could not populate scope from template", ex);
                throw new SynchronizationException(SyncErrorCodes.InternalError, ex);
            }
            catch (SqlException ex)
            {
                Log.WarnException("SQL Exception encountered.", ex);
                throw new SynchronizationException(SynchronizationException.ParseSqlException(ex), ex);
            }
        }

        #endregion Logs

        #region Global Settings

        /// <summary>
        /// Synchronizes the global settings from central to local.
        /// </summary>
        private void SyncGlobalSettings()
        {
            // Get the settings from both central and local databases
            var centralSettingsManager = new DbGlobalSettingsManager(DbIdentifier.CentralDatabase);
            var localSettingsManager = new DbGlobalSettingsManager(DbIdentifier.LocalDatabase);

            var centralSettings = centralSettingsManager.Get();
            var localSettings = localSettingsManager.Get();

            // Copy the necessary settings from central to local and save
            localSettings.NumberOfPreviousPasswordsToCheck = centralSettings.NumberOfPreviousPasswordsToCheck;
            localSettings.ContactEmail = centralSettings.ContactEmail;

            localSettingsManager.Save(localSettings);
        }

        #endregion Global Settings

        #region Synchronization Progress

        /// <summary>
        /// Handles the StateChanged event of the orchestrator control.
        /// Occurs when there is a change in the synchronization session state.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Microsoft.Synchronization.SyncOrchestratorStateChangedEventArgs"/> instance containing the event data.</param>
        private void OrchestratorStateChanged(object sender, SyncOrchestratorStateChangedEventArgs e)
        {
            if ((e.NewState == SyncOrchestratorState.Downloading ||
                e.NewState == SyncOrchestratorState.Uploading ||
                e.NewState == SyncOrchestratorState.UploadingAndDownloading)
                && this.CurrentStatus.State == SyncState.Aborting)
            {
                throw new SyncAbortedException(AbortExceptionMessage);
            }
        }

        /// <summary>
        /// Handles the SyncProgress event of the SyncManager control.
        /// Occurs during the selection and application of changes for a synchronization group at
        /// the server.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Microsoft.Synchronization.Data.DbSyncProgressEventArgs"/> instance containing the event data.</param>
        private void SynchronizationProgressChanged(object sender, DbSyncProgressEventArgs e)
        {
            if (e.Stage == DbSyncStage.SelectingChanges)
            {
                if (this.progressManager != null)
                {
                    // The total changes can't be 0.
                    if (e.ScopeProgress.TotalChanges != 0)
                    {
                        if (this.IsSyncRetrySession == false)
                        {
                            this.progressManager.TotalItemsCount = e.ScopeProgress.TotalChanges;
                            this.ReportProgressChanged(this.SynchronizationType, SynchronizationTaskStatus.SelectingChanges, this.progressManager.Progress);
                        }
                    }
                }
            }
            else if (e.Stage == DbSyncStage.ApplyingDeletes || e.Stage == DbSyncStage.ApplyingInserts || e.Stage == DbSyncStage.ApplyingUpdates)
            {
                if (this.progressManager != null)
                {
                    this.progressManager.PartialProcessedItemsCount = e.ScopeProgress.TotalChangesApplied;
                    this.ReportProgressChanged(this.SynchronizationType, SynchronizationTaskStatus.ApplyingChanges, this.progressManager.Progress);
                }
            }

            // Abort sync if abort operation was triggered.
            if ((this.Orchestrator.State == SyncOrchestratorState.Downloading ||
                this.Orchestrator.State == SyncOrchestratorState.Uploading ||
                this.Orchestrator.State == SyncOrchestratorState.UploadingAndDownloading)
                && this.CurrentStatus.State == SyncState.Aborting)
            {
                throw new SyncAbortedException(AbortExceptionMessage);
            }
        }

        /// <summary>
        /// Reports the progress changed.
        /// </summary>
        /// <param name="syncType">Type of the sync.</param>
        /// <param name="taskStatus">The task status.</param>
        /// <param name="progressData">The progress data.</param>
        private void ReportProgressChanged(SyncType syncType, SynchronizationTaskStatus taskStatus, SynchronizationProgressData progressData)
        {
            if (this.Progress != null)
            {
                switch (syncType)
                {
                    case SyncType.StaticDataAndSettings:
                    case SyncType.Logs:
                        this.ProgressChanged(SynchronizationTask.StaticDataAndSettings, taskStatus, progressData);
                        break;
                    case SyncType.ReleasedProjects:
                        this.ProgressChanged(SynchronizationTask.ReleasedProjects, taskStatus, progressData);
                        break;
                    case SyncType.MyProjects:
                        this.ProgressChanged(SynchronizationTask.MyProjects, taskStatus, progressData);
                        break;
                    case SyncType.MasterData:
                        this.ProgressChanged(SynchronizationTask.MasterData, taskStatus, progressData);
                        break;
                }
            }
        }

        /// <summary>
        /// Sends a synchronization progress changed event.
        /// </summary>
        /// <param name="task">The synch task.</param>
        /// <param name="taskStatus">The synch task status.</param>
        /// <param name="data">Some event data.</param>
        private void ProgressChanged(
            SynchronizationTask task,
            SynchronizationTaskStatus taskStatus,
            SynchronizationProgressData data)
        {
            this.CurrentStatus.CurrentTask = task;
            this.CurrentStatus.CurrentTaskStatus = taskStatus;
            this.CurrentStatus.ProgressData = data;

            if ((this.CurrentStatus.CurrentTaskStatus == SynchronizationTaskStatus.Finished
                || this.CurrentStatus.CurrentTaskStatus == SynchronizationTaskStatus.Aborted)
                && this.CurrentStatus.ProgressData != null)
            {
                this.CurrentStatus.TotalItemsCount += data.TotalItemsNumber;
                this.CurrentStatus.ProcessedItemsCount += data.ProcessedItems;
                this.CurrentStatus.TotalConflictsCount += data.Conflicts;

                if (this.CurrentStatus.CurrentTaskStatus == SynchronizationTaskStatus.Finished)
                {
                    this.CurrentStatus.TasksCompletedCount++;
                }
            }

            var handler = this.Progress;
            if (handler != null)
            {
                var evt = new SynchronizationProgressEventArgs(task, taskStatus, data);
                handler(this, evt);
            }
        }

        #endregion

        #region Abort synchronization

        /// <summary>
        /// Aborts the synchronization.
        /// </summary>
        public void AbortSynchronization()
        {
            this.CurrentStatus.State = SyncState.Aborting;
        }

        #endregion

        #region  Database Server Sync Provider Events

        /// <summary>
        /// Handles the ChangesSelected event of the SynchronizationManager control.
        /// Occurs after all changes to be applied to the client for a synchronization group are selected from the server.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Microsoft.Synchronization.Data.DbChangesSelectedEventArgs"/> instance containing the event data.</param>
        [SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Justification = "Left it like this due to complexity and sensitivity to refactoring. May be reviewed and refactored later.")]
        [SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "The query string is not composed using data from user input.")]
        private void SynchronizationManager_ChangesSelected(object sender, DbChangesSelectedEventArgs e)
        {
            var syncProvider = sender as SqlSyncProvider;

            if (syncProvider.SyncProviderPosition != SyncProviderPosition.Local
                || e.Context != null)
            {
                return;
            }

            // Delete released project from local database after the conversion occurs. Sync released project with sync direction = upload.
            // Occurs only after the scope is initialized.
            if (this.SynchronizationType == SyncType.ReleasedProjects && this.Orchestrator.Direction == SyncDirectionOrder.Upload)
            {
                var syncContext = e.Context;

                if (syncContext.DataSet != null)
                {
                    DataSet syncDataSet = syncContext.DataSet;

                    if (syncDataSet.Tables != null)
                    {
                        using (var sqlConnection = new SqlConnection(this.localConnectionString))
                        {
                            sqlConnection.Open();

                            for (int tableIndex = syncDataSet.Tables.Count - 1; tableIndex >= 0; tableIndex--)
                            {
                                var table = syncDataSet.Tables[tableIndex];
                                var rows = table.Rows.Cast<DataRow>().ToList();
                                foreach (var deletedRow in rows.Where(p => p.RowState == DataRowState.Deleted).ToList())
                                {
                                    rows.Remove(deletedRow);
                                }

                                var orderedRows = new List<DataRow>();

                                if (table.TableName.Equals("Assemblies") ||
                                    table.TableName.Equals("ProcessStepsClassification") ||
                                    table.TableName.Equals("MachinesClassification") ||
                                    table.TableName.Equals("MaterialsClassification"))
                                {
                                    var parentColumnName = string.Empty;

                                    if (table.TableName.Equals("Assemblies"))
                                    {
                                        parentColumnName = "ParentAssemblyGuid";
                                    }
                                    else if (table.TableName.Equals("ProcessStepsClassification") ||
                                              table.TableName.Equals("MachinesClassification") ||
                                              table.TableName.Equals("MaterialsClassification"))
                                    {
                                        parentColumnName = "ParentGuid";
                                    }

                                    OrderListParentChild(parentColumnName, ref rows, ref orderedRows);
                                }
                                else
                                {
                                    orderedRows = rows;
                                }

                                for (var index = orderedRows.Count - 1; index >= 0; index--)
                                {
                                    if (orderedRows[index].RowState != DataRowState.Deleted)
                                    {
                                        var deleteQuery = "DELETE FROM " + table.TableName + " WHERE ";

                                        switch (table.TableName)
                                        {
                                            case "ProcessStepPartAmounts":
                                                deleteQuery += " ProcessStepGuid='" + orderedRows[index]["ProcessStepGuid"].ToString() + "'  and PartGuid='" + orderedRows[index]["PartGuid"].ToString() + "'";
                                                break;
                                            case "ProcessStepAssemblyAmounts":
                                                deleteQuery += " ProcessStepGuid='" + orderedRows[index]["ProcessStepGuid"].ToString() + "'  and AssemblyGuid='" + orderedRows[index]["AssemblyGuid"].ToString() + "'";
                                                break;
                                            case "ProjectMedia":
                                                deleteQuery += " MediaGuid='" + orderedRows[index]["MediaGuid"].ToString() + "'";
                                                break;
                                            case "AssemblyMedia":
                                                deleteQuery += " MediaGuid='" + orderedRows[index]["MediaGuid"].ToString() + "'";
                                                break;
                                            case "PartMedia":
                                                deleteQuery += " MediaGuid='" + orderedRows[index]["MediaGuid"].ToString() + "'";
                                                break;
                                            default:
                                                deleteQuery += " Guid='" + orderedRows[index]["Guid"].ToString() + "'";
                                                break;
                                        }

                                        using (var sqlCommand = new SqlCommand())
                                        {
                                            sqlCommand.Connection = sqlConnection;
                                            sqlCommand.CommandText = deleteQuery;
                                            sqlCommand.CommandType = CommandType.Text;

                                            try
                                            {
                                                sqlCommand.ExecuteNonQuery();
                                            }
                                            catch (SqlException ex)
                                            {
                                                Log.ErrorException("Could not delete from released projects.", ex);
                                            }
                                        }

                                        orderedRows[index].AcceptChanges();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Orders the list in parent child relationships.
        /// </summary>
        /// <param name="parentColumnName">Name of the parent column.</param>
        /// <param name="rows">The list of data rows.</param>
        /// <param name="orderedRows">The ordered rows.</param>
        private void OrderListParentChild(string parentColumnName, ref List<DataRow> rows, ref List<DataRow> orderedRows)
        {
            var added = rows.Where(p => string.IsNullOrEmpty(p[parentColumnName].ToString())).ToList();
            orderedRows.AddRange(added);

            foreach (var deleteRow in added.ToList())
            {
                rows.Remove(deleteRow);
            }

            var childs = (from row in rows
                          join orderedRow in orderedRows
                          on row[parentColumnName].ToString() equals orderedRow["Guid"].ToString()
                          select row).ToList<DataRow>();

            if (childs.Count() + added.Count() == 0)
            {
                if (rows.Any())
                {
                    orderedRows.AddRange(rows);
                }

                return;
            }

            orderedRows.AddRange(childs);

            foreach (var deleteRow in childs.ToList())
            {
                rows.Remove(deleteRow);
            }

            if (rows.Count > 0)
            {
                OrderListParentChild(parentColumnName, ref rows, ref orderedRows);
            }
        }

        #endregion

        #region Schema Changed

        /// <summary>
        /// Handles the schema change.
        /// to add or remove columns from a scope definition, the following are the affected objects:
        /// // 1. Add/Remove column
        /// (Assuming the column is not a PK or a FilterColumn)
        /// modify select_changes SP
        /// modify select_row SP
        /// modify Insert/Update SP
        /// drop Bulk Insert/Update /Delete SP (they reference the bulk type and we can’t re-create the bulk type without dropping them)
        /// drop and recreate BulkType (there is no ALTER TYPE in SQL Server)
        /// recreate Bulk Insert/Update /Delete SP
        /// modify config_data column in the scope_config table
        /// 2. Add/Change  FilterColumn/FilterClause
        /// (assuming the FilterColumn is not a PK and was added using #1 above or is a column that is already part of the scope)
        /// modify tracking_table (filter columns are added in the tracking table)
        /// modify insert/update triggers
        /// modify select_changes SP
        /// modify config_data column in the scope_config table
        /// if the scope is based on a filter template, modify the parameter_data column of the scope_parameters table
        /// 3. Add Table
        /// add tracking table
        /// populate tracking table
        /// add insert/update/delete SP
        /// add bulk type
        /// add bulk insert/update/delete SP
        /// add select_changes SP
        /// add insert/update/delete metadata SP
        /// add select_row SP
        /// modify the config_data column in the scope_config table
        /// 4. Remove Table
        /// </summary>
        /// <param name="sqlConnectionString">The SQL connection string.</param>
        /// <param name="scopeName">Name of the scope.</param>
        /// <param name="scopeDesc">The scope description.</param>
        /// <param name="serverTemplate">The server template.</param>
        [SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "The query string is not composed using data from user input.")]
        private void HandleSchemaChanges(string sqlConnectionString, string scopeName, DbSyncScopeDescription scopeDesc, SqlSyncScopeProvisioning serverTemplate)
        {
            if (string.IsNullOrEmpty(scopeName))
            {
                throw new ArgumentException("The scope name was empty.", "scopeName");
            }

            if (scopeDesc == null)
            {
                throw new ArgumentNullException("scopeDesc", "The sql scope description was null");
            }

            if (serverTemplate == null)
            {
                throw new ArgumentNullException("serverTemplate", "The sync scope provisioning was null");
            }

            try
            {
                string oldConfig = GetSqlProviderConfiguration(sqlConnectionString, scopeName);

                if (!string.IsNullOrEmpty(oldConfig))
                {
                    // set SetCreateProceduresDefault so Sync Fx thinks this is a new scope 
                    // and create almost all the objects except tracking tables
                    serverTemplate.SetCreateProceduresDefault(DbSyncCreationOption.Create);
                    serverTemplate.SetCreateTrackingTableDefault(DbSyncCreationOption.CreateOrUseExisting);
                    serverTemplate.SetCreateProceduresForAdditionalScopeDefault(DbSyncCreationOption.Create);
                    serverTemplate.SetCreateTriggersDefault(DbSyncCreationOption.Create);
                    string provScript = serverTemplate.Script();

                    // extract the config_data entry from the script
                    int x = provScript.IndexOf("<SqlSyncProviderScopeConfiguration");
                    int y = provScript.IndexOf("</SqlSyncProviderScopeConfiguration>");
                    string newConfig = provScript.Substring(x, y - x) + "</SqlSyncProviderScopeConfiguration>";

                    if (oldConfig != newConfig)
                    {
                        XDocument oldConfigXml = XDocument.Parse(oldConfig);
                        XDocument newConfigXml = XDocument.Parse(newConfig);

                        // drop statements for the affected objects
                        var stringBuilder = new StringBuilder();

                        foreach (XElement oldElement in oldConfigXml.Descendants("Adapter"))
                        {
                            XElement newElement = (from element in newConfigXml.Descendants("Adapter")
                                                   where element.Attribute("Name").Value == oldElement.Attribute("Name").Value
                                                   select element).FirstOrDefault();
                            if (newElement != null)
                            {
                                string oldValue = oldElement.Attribute("SelChngProc").Value;
                                string newValue = newElement.Attribute("SelChngProc").Value;
                                provScript = provScript.Replace(newValue, oldValue);

                                newElement.Attribute("SelChngProc").Value = oldElement.Attribute("SelChngProc").Value;
                                stringBuilder.AppendLine(" IF OBJECT_ID('" + oldElement.Attribute("SelChngProc").Value + "', 'P') is not null DROP PROCEDURE " + oldElement.Attribute("SelChngProc").Value);
                                stringBuilder.AppendLine(" IF OBJECT_ID('" + oldElement.Attribute("SelRowProc").Value + "', 'P') is not null DROP PROCEDURE " + oldElement.Attribute("SelRowProc").Value);
                                stringBuilder.AppendLine(" IF OBJECT_ID('" + oldElement.Attribute("InsProc").Value + "', 'P') is not null DROP PROCEDURE " + oldElement.Attribute("InsProc").Value);
                                stringBuilder.AppendLine(" IF OBJECT_ID('" + oldElement.Attribute("UpdProc").Value + "', 'P') is not null DROP PROCEDURE " + oldElement.Attribute("UpdProc").Value);
                                stringBuilder.AppendLine(" IF OBJECT_ID('" + oldElement.Attribute("DelProc").Value + "', 'P') is not null DROP PROCEDURE " + oldElement.Attribute("DelProc").Value);
                                stringBuilder.AppendLine(" IF OBJECT_ID('" + oldElement.Attribute("InsMetaProc").Value + "', 'P') is not null  DROP PROCEDURE " + oldElement.Attribute("InsMetaProc").Value);
                                stringBuilder.AppendLine(" IF OBJECT_ID('" + oldElement.Attribute("UpdMetaProc").Value + "', 'P') is not null DROP PROCEDURE " + oldElement.Attribute("UpdMetaProc").Value);
                                stringBuilder.AppendLine(" IF OBJECT_ID('" + oldElement.Attribute("DelMetaProc").Value + "', 'P') is not null DROP PROCEDURE " + oldElement.Attribute("DelMetaProc").Value);

                                var adapterName = oldElement.Attribute("Name").Value.Replace("]", string.Empty);
                                stringBuilder.AppendLine(" IF OBJECT_ID('" + adapterName + "_bulkinsert]', 'P') is not null  DROP PROCEDURE " + adapterName + "_bulkinsert]");
                                stringBuilder.AppendLine(" IF OBJECT_ID('" + adapterName + "_bulkupdate]', 'P') is not null  DROP PROCEDURE " + adapterName + "_bulkupdate]");
                                stringBuilder.AppendLine(" IF OBJECT_ID('" + adapterName + "_bulkdelete]', 'P') is not null  DROP PROCEDURE " + adapterName + "_bulkdelete]");
                                stringBuilder.AppendLine(" IF TYPE_ID('" + adapterName + "_bulktype]') is not null DROP TYPE " + adapterName + "_bulktype] ");
                            }
                        }

                        List<XElement> newElements = new List<XElement>();

                        // new scope table added
                        foreach (XElement element in newConfigXml.Descendants("Adapter"))
                        {
                            if (oldConfigXml.Descendants("Adapter").FirstOrDefault(p => p.Attribute("Name").Value == element.Attribute("Name").Value) == null)
                            {
                                newElements.Add(element);
                            }
                        }

                        foreach (XElement newElement in newElements)
                        {
                            string oldValue = newElement.Attribute("SelChngProc").Value;
                            string oldValue2 = oldValue.Remove(oldValue.Count() - 1, 1);
                            string newValue = oldValue2 + "_" + Guid.NewGuid() + "]";

                            newElement.Attribute("SelChngProc").Value = newValue;
                            provScript = provScript.Replace(oldValue, newValue);

                            stringBuilder.AppendLine(" IF OBJECT_ID('" + newElement.Attribute("SelRowProc").Value + "', 'P') is not null DROP PROCEDURE " + newElement.Attribute("SelRowProc").Value);
                            stringBuilder.AppendLine(" IF OBJECT_ID('" + newElement.Attribute("InsProc").Value + "', 'P') is not null DROP PROCEDURE " + newElement.Attribute("InsProc").Value);
                            stringBuilder.AppendLine(" IF OBJECT_ID('" + newElement.Attribute("UpdProc").Value + "', 'P') is not null DROP PROCEDURE " + newElement.Attribute("UpdProc").Value);
                            stringBuilder.AppendLine(" IF OBJECT_ID('" + newElement.Attribute("DelProc").Value + "', 'P') is not null DROP PROCEDURE " + newElement.Attribute("DelProc").Value);
                            stringBuilder.AppendLine(" IF OBJECT_ID('" + newElement.Attribute("InsMetaProc").Value + "', 'P') is not null  DROP PROCEDURE " + newElement.Attribute("InsMetaProc").Value);
                            stringBuilder.AppendLine(" IF OBJECT_ID('" + newElement.Attribute("UpdMetaProc").Value + "', 'P') is not null DROP PROCEDURE " + newElement.Attribute("UpdMetaProc").Value);
                            stringBuilder.AppendLine(" IF OBJECT_ID('" + newElement.Attribute("DelMetaProc").Value + "', 'P') is not null DROP PROCEDURE " + newElement.Attribute("DelMetaProc").Value);

                            var adapterName = newElement.Attribute("Name").Value.Replace("]", string.Empty);
                            stringBuilder.AppendLine(" IF OBJECT_ID('" + adapterName + "_bulkinsert]', 'P') is not null  DROP PROCEDURE " + adapterName + "_bulkinsert]");
                            stringBuilder.AppendLine(" IF OBJECT_ID('" + adapterName + "_bulkupdate]', 'P') is not null  DROP PROCEDURE " + adapterName + "_bulkupdate]");
                            stringBuilder.AppendLine(" IF OBJECT_ID('" + adapterName + "_bulkdelete]', 'P') is not null  DROP PROCEDURE " + adapterName + "_bulkdelete]");
                            stringBuilder.AppendLine(" IF TYPE_ID('" + adapterName + "_bulktype]') is not null DROP TYPE " + adapterName + "_bulktype] ");
                        }

                        newConfig = newConfigXml.ToString(SaveOptions.DisableFormatting);

                        CompareOptions compareOptions = CompareOptions.IgnoreCase
                            | CompareOptions.IgnoreWidth
                            | CompareOptions.IgnoreNonSpace;

                        // returns 0 if the configurations are identical.
                        int identical = string.Compare(oldConfig, newConfig, CultureInfo.InvariantCulture, compareOptions);

                        if (identical != 0)
                        {
                            // append the sync provisioning script after the drop statements
                            var alterScopeSql = stringBuilder.Append(provScript).ToString();

                            // remove the inserts for the scope_info and scope_config
                            x = alterScopeSql.IndexOf("-- BEGIN Add scope");
                            y = alterScopeSql.IndexOf("-- END Add Scope");
                            alterScopeSql = alterScopeSql.Remove(x, y - x);

                            // replace the update scope_config to update the config_data column with the revised scope definition
                            alterScopeSql = alterScopeSql.Replace("scope_status = 'C'", "config_data=" + "N'" + newConfig + "'");

                            // remove and replace the where clause to update the current scope with the revised config_data          
                            x = alterScopeSql.IndexOf("WHERE [config_id] =");
                            alterScopeSql = alterScopeSql.Remove(x, alterScopeSql.Length - x);
                            alterScopeSql = alterScopeSql
                                + " WHERE [config_id] = (SELECT scope_config_id FROM scope_info WHERE sync_scope_name='"
                                + scopeName + "')";

                            List<string> commands = alterScopeSql.Split(new string[] { "\nGO\n", Environment.NewLine + "GO" + Environment.NewLine, "\n" + "GO ", Environment.NewLine + "GO " }, StringSplitOptions.RemoveEmptyEntries).ToList();

                            // sort commands : PLACE CREATE TRIGGER BEFORE ALTER TRIGGER, AND INCLUDE ONLY CREATE TRIGGER 
                            this.FixCreateTriggerStatements(commands);

                            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
                            {
                                connection.Open();

                                using (SqlTransaction transaction = connection.BeginTransaction())
                                {
                                    try
                                    {
                                        foreach (var command in commands)
                                        {
                                            using (SqlCommand sqlCommand = new SqlCommand())
                                            {
                                                sqlCommand.Connection = connection;
                                                sqlCommand.CommandType = CommandType.Text;
                                                sqlCommand.CommandText = command;
                                                sqlCommand.Transaction = transaction;
                                                sqlCommand.CommandTimeout = SqlCommandTimeout;
                                                sqlCommand.ExecuteNonQuery();
                                            }
                                        }

                                        transaction.Commit();
                                    }
                                    catch (SqlException ex)
                                    {
                                        transaction.Rollback();
                                        Log.WarnException("Handle database schema changes.", ex);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (XmlException ex)
            {
                Log.ErrorException("Error occurred while handling the xml scope configuration.", ex);
                throw new SynchronizationException(SyncErrorCodes.InternalError, ex);
            }
            catch (SqlException ex)
            {
                Log.ErrorException("Error occurred while Scripting the provisioning scope. ", ex);
                throw new SynchronizationException(SynchronizationException.ParseSqlException(ex), ex);
            }
        }

        /// <summary>
        /// Replaces all SQL statements that create triggers with safer versions that check if a trigger exists before creating it in order to avoid errors
        /// when trying to create an existing trigger; if the trigger exists the statement is changed to alter trigger.
        /// This method is used when handling schema changes during provisioning.
        /// </summary>
        /// <remarks>
        /// Each statement starting with CREATE TRIGGER is replaced with an equivalent statement 
        /// of the form "IF TRIGGER X IS NULL CREATE TRIGGER X ... ELSE ALTER TRIGGER X ...".
        /// </remarks>
        /// <param name="sqlStatements">The SQL statements.</param>
        private void FixCreateTriggerStatements(List<string> sqlStatements)
        {
            string createTriggerStr = "CREATE TRIGGER";
            string alterTriggerStr = "ALTER TRIGGER";

            // The template for the "fixed" statement, which first checks if the trigger exists; if it exists it executes an alter trigger statement, otherwise
            // it executes the original create trigger statement.
            string createOrAlterStmtFormat =
@"IF OBJECT_ID('{0}', 'TR') IS NULL
BEGIN
    EXEC('{1}')
END
ELSE
BEGIN
    EXEC('{2}')
END";

            // Remove the alter trigger statements because will cause problems if the trigger does not exist.
            // The fixed create statements contain both create and alter triggers.
            sqlStatements.RemoveAll(s => s.IndexOf(alterTriggerStr) > 0);

            foreach (string statement in sqlStatements.ToList())
            {
                int createStmtIndx = statement.IndexOf(createTriggerStr);
                if (createStmtIndx > 0)
                {
                    // Extract the exact CREATE TRIGGER statement (usually it has some comments and whitespace before it).
                    string createStatement = statement.Substring(createStmtIndx);

                    // Extract the trigger name from the CREATE TRIGGER statement.
                    int triggerNameStartIndx = createTriggerStr.Length + 1;
                    int triggerNameEndIndex = createStatement.IndexOf(" ", triggerNameStartIndx);
                    string triggerName = null;

                    if (triggerNameStartIndx < createStatement.Length
                        && triggerNameEndIndex < createStatement.Length
                        && triggerNameStartIndx <= triggerNameEndIndex)
                    {
                        triggerName = createStatement.Substring(triggerNameStartIndx, triggerNameEndIndex - triggerNameStartIndx).Trim(' ', '[', ']');
                    }

                    if (!string.IsNullOrWhiteSpace(triggerName) && triggerName.IndexOf(" ") < 0)
                    {
                        // Make a copy of the create statement which will be transformed into an alter statement.
                        string alterStatement = statement.Substring(createStmtIndx).Replace(createTriggerStr, alterTriggerStr);

                        // Create the fixed statement (if trigger does not exist create it else alter it).
                        // The CREATE and ALTER TRIGGER statement are invoke with EXEC so single quote characters are replaced with two single quote characters.
                        string fixedStatement = string.Format(
                            createOrAlterStmtFormat,
                            triggerName,
                            createStatement.Replace("'", "''"),
                            alterStatement.Replace("'", "''"));

                        // Remove the original statement and add the fixed one to the statement lists.
                        sqlStatements.Remove(statement);
                        sqlStatements.Add(fixedStatement);
                    }
                }
            }
        }

        /// <summary>
        /// Handles the scope parameters change.
        /// </summary>
        /// <param name="scopeName">Name of the scope.</param>
        /// <param name="serverConfig">The server configuration.</param>
        /// <param name="sqlConnectionString">The SQL connection string.</param>
        private void HandleScopeParametersChange(string scopeName, SqlSyncScopeProvisioning serverConfig, string sqlConnectionString)
        {
            if (string.IsNullOrWhiteSpace(scopeName))
            {
                throw new ArgumentNullException("scopeName", "The scope name was null");
            }

            if (serverConfig == null)
            {
                throw new ArgumentNullException("serverConfig", "The sync scope provisioning was null");
            }

            try
            {
                string alterScopeSql = serverConfig.Script();
                string oldScopeParameters = null;

                using (SqlConnection connection = new SqlConnection(sqlConnectionString))
                {
                    connection.Open();

                    using (SqlCommand sqlCommand = new SqlCommand())
                    {
                        string selectScopeParametersQuery = "SELECT parameter_data FROM scope_parameters WHERE sync_scope_name = @ScopeName";
                        sqlCommand.CommandText = selectScopeParametersQuery;
                        sqlCommand.Parameters.Add(new SqlParameter("@ScopeName", scopeName));
                        sqlCommand.CommandType = CommandType.Text;
                        sqlCommand.Connection = connection;
                        sqlCommand.CommandTimeout = SqlCommandTimeout;

                        using (SqlDataReader reader = sqlCommand.ExecuteReader())
                        {
                            reader.Read();
                            if (reader.HasRows)
                            {
                                oldScopeParameters = reader.GetString(0);
                            }
                        }
                    }
                }

                if (!string.IsNullOrEmpty(oldScopeParameters))
                {
                    // extract the config_data entry from the script
                    int x = alterScopeSql.IndexOf("<SqlSyncProviderScopeParameters");
                    int y = alterScopeSql.IndexOf("</SqlSyncProviderScopeParameters>");
                    if (x > 0 && y > x)
                    {
                        var newScopeParameters = alterScopeSql.Substring(x, y - x) + "</SqlSyncProviderScopeParameters>";
                        if (oldScopeParameters != newScopeParameters)
                        {
                            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
                            {
                                connection.Open();

                                using (SqlTransaction transaction = connection.BeginTransaction())
                                {
                                    using (SqlCommand sqlCommand = new SqlCommand())
                                    {
                                        sqlCommand.CommandType = CommandType.Text;
                                        sqlCommand.CommandText =
                                            "UPDATE scope_parameters SET parameter_data = @NewScopeParameters WHERE sync_scope_name = @ScopeName";
                                        sqlCommand.Parameters.Add(new SqlParameter("@NewScopeParameters", newScopeParameters));
                                        sqlCommand.Parameters.Add(new SqlParameter("@ScopeName", scopeName));

                                        sqlCommand.Connection = connection;
                                        sqlCommand.Transaction = transaction;
                                        sqlCommand.CommandTimeout = SqlCommandTimeout;
                                        sqlCommand.ExecuteNonQuery();
                                    }

                                    transaction.Commit();
                                }
                            }
                        }
                    }
                }
            }
            catch (SyncException ex)
            {
                Log.ErrorException("Scope parameters change: Error occurred while Scripting the sql scope provisioning.", ex);
                throw new SynchronizationException(SyncErrorCodes.InternalError, ex);
            }
            catch (SqlException ex)
            {
                Log.ErrorException("Scope parameters change: Error occurred while trying to get the existing scope parameters.", ex);
                throw new SynchronizationException(SynchronizationException.ParseSqlException(ex), ex);
            }
        }

        /// <summary>
        /// Gets the SQL provider configuration.
        /// </summary>
        /// <param name="sqlConnectionString">The SQL connection string.</param>
        /// <param name="scopeName">Name of the scope.</param>
        /// <returns>The scope configuration xml.</returns>
        public static string GetSqlProviderConfiguration(string sqlConnectionString, string scopeName)
        {
            try
            {
                string oldConfig = string.Empty;

                using (SqlConnection connection = new SqlConnection(sqlConnectionString))
                {
                    connection.Open();

                    using (SqlCommand sqlCommand = new SqlCommand())
                    {
                        sqlCommand.CommandText =
                            "SELECT scope_config.config_data FROM scope_config, scope_info WHERE  scope_info.sync_scope_name = @ScopeName and scope_info.scope_config_id = scope_config.config_id";
                        sqlCommand.Parameters.Add(new SqlParameter("@ScopeName", scopeName));
                        sqlCommand.CommandType = CommandType.Text;
                        sqlCommand.CommandTimeout = SqlCommandTimeout;
                        sqlCommand.Connection = connection;

                        using (SqlDataReader reader = sqlCommand.ExecuteReader())
                        {
                            reader.Read();
                            if (reader.HasRows)
                            {
                                oldConfig = reader.GetString(0);
                            }
                        }
                    }
                }

                return oldConfig;
            }
            catch (SqlException ex)
            {
                Log.ErrorException("Could not get the configuration xml for scope:" + scopeName, ex);
                throw new SynchronizationException(SynchronizationException.ParseSqlException(ex), ex);
            }
        }

        #endregion

        #region Update Scopes

        /// <summary>
        /// Determines whether a synchronization scope(s) update is necessary.
        /// </summary>
        /// <param name="userGuid">The user ID for which to perform the check.</param>
        /// <returns>
        /// true if a scope update is necessary; otherwise, false.
        /// </returns>
        public bool IsSyncScopeUpdateNecessary(Guid userGuid)
        {
            try
            {
                var isSyncUpdateNecessary = false;

                var globalSettingsManager = new DbGlobalSettingsManager(DbIdentifier.LocalDatabase);
                var globalSettings = globalSettingsManager.Get();
                decimal version = globalSettings.Version;

                var syncUpdateScopes = new List<Tuple<Guid, decimal>>();
                syncUpdateScopes.AddRange(SplitUpdateSyncScopesValue(globalSettings.UpdateSyncScopes));

                var isFound = false;
                foreach (Tuple<Guid, decimal> tuple in syncUpdateScopes)
                {
                    if (tuple.Item1 == userGuid)
                    {
                        isFound = true;

                        if (tuple.Item2 != version)
                        {
                            isSyncUpdateNecessary = true;
                        }

                        break;
                    }
                }

                if (isFound == false)
                {
                    isSyncUpdateNecessary = true;
                }

                return isSyncUpdateNecessary;
            }
            catch (DataAccessException ex)
            {
                Log.ErrorException("Could not determine if the scope update is necessary.", ex);
                throw;
            }
        }

        /// <summary>
        /// Concatenates the successfully user guid to the list of successfully conversion maintenance user guids in table DataBaseInfo with Key = ConversionMaintainedUsers.
        /// </summary>
        /// <param name="userGuid">The user GUID.</param>
        private void UpdateSuccesfullyScopeUpdate(Guid userGuid)
        {
            try
            {
                string updateValue = string.Empty;

                var globalSettingsManager = new DbGlobalSettingsManager(DbIdentifier.LocalDatabase);
                var globalSettings = globalSettingsManager.Get();
                var version = globalSettings.Version;

                var syncUpdateScopes = new List<Tuple<Guid, decimal>>();
                syncUpdateScopes.AddRange(SplitUpdateSyncScopesValue(globalSettings.UpdateSyncScopes));

                bool isFound = false;
                foreach (var tuple in syncUpdateScopes)
                {
                    updateValue += tuple.Item1;
                    updateValue += "|";

                    if (tuple.Item1 == userGuid)
                    {
                        isFound = true;
                        updateValue += version.ToString(CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        updateValue += tuple.Item2.ToString(CultureInfo.InvariantCulture);
                    }

                    updateValue += "\\";
                }

                if (isFound == false)
                {
                    updateValue += userGuid;
                    updateValue += "|";
                    updateValue += version.ToString(CultureInfo.InvariantCulture);
                    updateValue += "\\";
                }

                globalSettings.UpdateSyncScopes = updateValue;
                globalSettingsManager.Save(globalSettings);
            }
            catch (SqlException ex)
            {
                Log.ErrorException("Could not concatenate the user's guid to the successfully converted user guids.", ex);
                throw new SynchronizationException(SynchronizationException.ParseSqlException(ex));
            }
        }

        /// <summary>
        /// Splits the update sync scopes value.
        /// </summary>
        /// <param name="concatenatedTuples">The concatenated data that must be split.</param>
        /// <returns>
        /// Pairs of user id - version.
        /// </returns>
        private static List<Tuple<Guid, decimal>> SplitUpdateSyncScopesValue(string concatenatedTuples)
        {
            var usersGuidWithVersion = new List<Tuple<Guid, decimal>>();

            var tuplesStrings = concatenatedTuples.Split('\\').ToList();

            foreach (string tupleStr in tuplesStrings)
            {
                var pair = tupleStr.Split('|').ToList();

                if (pair.Count == 2)
                {
                    Guid userGuid;
                    if (Guid.TryParse(pair[0], out userGuid))
                    {
                        decimal version;
                        if (decimal.TryParse(pair[1], NumberStyles.Number, CultureInfo.InvariantCulture, out version))
                        {
                            var tuple = new Tuple<Guid, decimal>(userGuid, version);
                            usersGuidWithVersion.Add(tuple);
                        }
                    }
                }
            }

            return usersGuidWithVersion;
        }

        #endregion
    }
}