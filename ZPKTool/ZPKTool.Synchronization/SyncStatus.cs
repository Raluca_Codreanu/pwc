﻿namespace ZPKTool.Synchronization
{
    using System;

    /// <summary>
    /// THe synchronization status.
    /// </summary>
    public class SyncStatus
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SyncStatus"/> class.
        /// </summary>
        public SyncStatus()
        {
            this.State = SyncState.NotStarted;
        }

        #endregion Constructor

        #region Properties

        /// <summary>
        /// Gets the state of the synchronization process.
        /// </summary>
        public SyncState State
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets the total number of selected tasks for synchronization. It is used to represent the total progress.
        /// </summary>
        public int TasksCount
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets the number of synchronization tasks processed out of the total number of sync tasks. It is used to represent the total progress.
        /// </summary>
        public int TasksCompletedCount
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets the current task.
        /// </summary>
        public SynchronizationTask CurrentTask
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets the current task's status.
        /// </summary>
        public SynchronizationTaskStatus CurrentTaskStatus
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets the progress data for the current task.
        /// </summary>
        public SynchronizationProgressData ProgressData
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets a value indicating whether to synch static data and settings or not.
        /// </summary>
        public bool SyncStaticDataAndSettings
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets a value indicating whether to synch released projects or not.
        /// </summary>
        public bool SyncReleasedProjects
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets a value indicating whether to synch master data or not.
        /// </summary>
        public bool SyncMasterData
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets a value indicating whether to synch my projects or not.
        /// </summary>
        public bool SyncMyProjects
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets the total items count.
        /// </summary>
        public int TotalItemsCount
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets the processed items count.
        /// </summary>
        public int ProcessedItemsCount
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets the total conflicts count.
        /// </summary>
        public int TotalConflictsCount
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets the duration of the synchronization.
        /// </summary>
        public DateTime StartTime
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets an Exception encountered during synchronization.
        /// </summary>
        public Exception Error
        {
            get;
            internal set;
        }

        #endregion Properties
    }
}
