CREATE FUNCTION GetProjectCurrenciesHierarchyForReleasedProjectsSync()
RETURNS TABLE
AS
RETURN
	(
		SELECT CurrencyId
			FROM ProjectCurrencies_tracking
			WHERE ProjectId IN (SELECT [Guid] FROM GetProjectsHierarchyForReleasedProjectsSync())					   
	)