CREATE FUNCTION GetManufacturersHierarchyForReleasedProjectsSync()
RETURNS TABLE
AS
 RETURN
   (
    SELECT [Guid] 
	   FROM Manufacturers_tracking 
	   WHERE Manufacturers_tracking.[Guid] IN (SELECT [ManufacturerGuid] FROM GetAssemblyHierarchyForReleasedProjectsSync()) OR
	                Manufacturers_tracking.[Guid] IN (SELECT [ManufacturerGuid] FROM GetCommoditiesHierarchyForReleasedProjectsSync()) OR
					Manufacturers_tracking.[Guid] IN (SELECT [ManufacturerGuid] FROM GetConsumablesHierarchyForReleasedProjectsSync()) OR
					Manufacturers_tracking.[Guid] IN (SELECT [ManufacturerGuid] FROM GetDiesHierarchyForReleasedProjectsSync()) OR
				    Manufacturers_tracking.[Guid] IN (SELECT [ManufacturerGuid] FROM GetMachinesHierarchyForReleasedProjectsSync()) OR
					Manufacturers_tracking.[Guid] IN (SELECT [ManufacturerGuid] FROM GetPartsHierarchyForReleasedProjectsSync()) OR
					Manufacturers_tracking.[Guid] IN (SELECT [ManufacturerGuid] FROM GetRawMaterialsHierarchyForReleasedProjectsSync()) 
   )
