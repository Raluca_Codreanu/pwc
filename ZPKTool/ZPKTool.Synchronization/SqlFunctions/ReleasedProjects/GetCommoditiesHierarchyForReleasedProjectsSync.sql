CREATE FUNCTION GetCommoditiesHierarchyForReleasedProjectsSync()
RETURNS TABLE
AS
 RETURN
   (
   SELECT Commodities_tracking.[Guid],
   Commodities_tracking.[MediaGuid],
   Commodities_tracking.[ManufacturerGuid]
    FROM Commodities_tracking
	 WHERE Commodities_tracking.[ProcessStepGuid] IN (SELECT [Guid] FROM GetProcessStepsHierarchyForReleasedProjectsSync()) OR 
	 Commodities_tracking.[PartGuid] IN (SELECT [Guid] FROM GetPartsHierarchyForReleasedProjectsSync())
   )
