CREATE FUNCTION GetTransportCostCalculationsHierarchyForReleasedProjectsSync()
RETURNS TABLE
AS
 RETURN
   (
       SELECT [Guid]
	      FROM TransportCostCalculations_tracking
		  WHERE AssemblyId IN (SELECT [Guid] FROM GetAssemblyHierarchyForReleasedProjectsSync()) OR
				PartId IN (SELECT [Guid] FROM GetPartsHierarchyForReleasedProjectsSync())
   )