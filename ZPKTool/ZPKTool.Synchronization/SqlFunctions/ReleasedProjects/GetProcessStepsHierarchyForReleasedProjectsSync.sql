CREATE FUNCTION GetProcessStepsHierarchyForReleasedProjectsSync()
RETURNS TABLE
AS
 RETURN
   (
     SELECT ProcessSteps_tracking.[Guid],
	              ProcessSteps_tracking.[TypeGuid],
				  ProcessSteps_tracking.[SubTypeGuid],
				  ProcessSteps_tracking.[MediaGuid],
				  ProcessSteps_tracking.[ProcessGuid]	
	     FROM ProcessSteps_tracking
		 WHERE 
		             ProcessSteps_tracking.[ProcessGuid]  IN (SELECT  [Guid] FROM GetProcessesHierarchyForReleasedProjectsSync())
   )
