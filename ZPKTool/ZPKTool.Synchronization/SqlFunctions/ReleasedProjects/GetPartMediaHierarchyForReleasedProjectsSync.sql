CREATE FUNCTION GetPartMediaHierarchyForReleasedProjectsSync()
RETURNS TABLE
AS
 RETURN
   (
      SELECT 
	              [MediaGuid] 
				  FROM PartMedia_tracking
				  WHERE PartMedia_tracking.[PartGuid] IN (SELECT [Guid] FROM GetPartsHierarchyForReleasedProjectsSync())					   
   )
