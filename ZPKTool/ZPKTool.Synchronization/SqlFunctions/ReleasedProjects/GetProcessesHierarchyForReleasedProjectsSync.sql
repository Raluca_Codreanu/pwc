CREATE FUNCTION GetProcessesHierarchyForReleasedProjectsSync()
RETURNS TABLE
AS
 RETURN
   (
       SELECT Processes_tracking.[Guid]
	      FROM Processes_tracking
		  WHERE 
		              Processes_tracking.[Guid] IN (SELECT [ProcessGuid] FROM GetAssemblyHierarchyForReleasedProjectsSync()) OR
					  Processes_tracking.[Guid] IN (SELECT [ProcessGuid] FROM GetPartsHierarchyForReleasedProjectsSync())
   )
