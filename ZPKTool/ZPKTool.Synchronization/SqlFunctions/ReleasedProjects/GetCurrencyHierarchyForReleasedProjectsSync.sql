CREATE FUNCTION GetCurrencyHierarchyForReleasedProjectsSync()
RETURNS TABLE
AS
RETURN
	(
		SELECT [Guid]
			FROM Currencies_tracking
			WHERE [Guid] IN (SELECT CurrencyId FROM GetProjectCurrenciesHierarchyForReleasedProjectsSync())
	)