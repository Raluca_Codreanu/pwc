CREATE FUNCTION GetProjectsHierarchyForReleasedProjectsSync()
RETURNS TABLE
AS
 RETURN
   (
	SELECT Projects_tracking.[Guid],
               Projects_tracking.[ProjectLeaderGuid], 
               Projects_tracking.[ResponsableCalculatorGuid],
               Projects_tracking.[CustomerGuid],
               Projects_tracking.[OverheadSettingsGuid]
	  FROM Projects_tracking
		WHERE  Projects_tracking.OwnerGuid IS NULL AND 
					  Projects_tracking.IsReleased='True'					                                                                                                          
)
