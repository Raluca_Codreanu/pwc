CREATE FUNCTION GetDiesHierarchyForReleasedProjectsSync()
RETURNS TABLE
AS
 RETURN
   (
       SELECT Dies_tracking.[Guid],
	                 Dies_tracking.[ManufacturerGuid],
					 Dies_tracking.[MediaGuid]			
					 FROM Dies_tracking
					     WHERE Dies_tracking.[ProcessStepGuid] IN (SELECT [Guid] FROM GetProcessStepsHierarchyForReleasedProjectsSync())   
   )
