CREATE FUNCTION GetOverheadSettingsHierarchyForReleasedProjectsSync()
RETURNS TABLE
AS
 RETURN
   (
	 SELECT 
		OverheadSettings_tracking.[Guid]
	 FROM OverheadSettings_tracking
	 WHERE OverheadSettings_tracking.[Guid] IN ( SELECT [OverheadSettingsGuid] FROM GetAssemblyHierarchyForReleasedProjectsSync()) OR
		   OverheadSettings_tracking.[Guid] IN ( SELECT [OverheadSettingsGuid] FROM GetPartsHierarchyForReleasedProjectsSync())  OR
		   OverheadSettings_tracking.[Guid] IN ( SELECT [OverheadSettingsGuid] FROM  GetProjectsHierarchyForReleasedProjectsSync())

   )
