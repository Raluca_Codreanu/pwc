CREATE FUNCTION GetMediaHierarchyForReleasedProjectsSync()
RETURNS TABLE
AS
 RETURN
   (
      Select Media_tracking.[Guid]
	  FROM Media_tracking
	  WHERE Media_tracking.[Guid] IN (SELECT [MediaGuid]  FROM GetMachinesHierarchyForReleasedProjectsSync()) OR
	               Media_tracking.[Guid] IN (SELECT [MediaGuid]  FROM GetCommoditiesHierarchyForReleasedProjectsSync()) OR
				   Media_tracking.[Guid] IN (SELECT [MediaGuid]  FROM GetRawMaterialsHierarchyForReleasedProjectsSync()) OR
				   Media_tracking.[Guid] IN (SELECT [MediaGuid]  FROM GetDiesHierarchyForReleasedProjectsSync()) OR
				   Media_tracking.[Guid] IN (SELECT [MediaGuid]  FROM GetProjectMediaHierarchyForReleasedProjectsSync()) OR
				   Media_tracking.[Guid] IN (SELECT [MediaGuid]  FROM GetAssemblyMediaHierarchyForReleasedProjectsSync()) OR
				   Media_tracking.[Guid] IN (SELECT [MediaGuid]  FROM GetPartMediaHierarchyForReleasedProjectsSync())  OR
				   Media_tracking.[Guid] IN (SELECT [MediaGuid]  FROM GetProcessStepsHierarchyForReleasedProjectsSync())  
   )
