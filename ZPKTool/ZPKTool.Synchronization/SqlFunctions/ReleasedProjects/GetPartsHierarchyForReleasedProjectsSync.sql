CREATE FUNCTION GetPartsHierarchyForReleasedProjectsSync()
RETURNS TABLE
AS
RETURN
	(SELECT Parts_tracking.[Guid],
			Parts_tracking.ManufacturerGuid,
			Parts_tracking.OverheadSettingsGuid,
			Parts_tracking.ProcessGuid,
			Parts_tracking.CountrySettingsGuid,
			Parts_tracking.CalculatorGuid,
			Parts_tracking.OwnerGuid
		FROM Parts_tracking
		WHERE Parts_tracking.ParentAssemblyGuid IN (SELECT [Guid] FROM GetAssemblyHierarchyForReleasedProjectsSync()) OR
			Parts_tracking.ParentProjectGuid IN (SELECT [Guid] FROM GetProjectsHierarchyForReleasedProjectsSync())
	UNION
	SELECT Parts_tracking.[Guid],
			Parts_tracking.ManufacturerGuid,
			Parts_tracking.OverheadSettingsGuid,
			Parts_tracking.ProcessGuid,
			Parts_tracking.CountrySettingsGuid,
			Parts_tracking.CalculatorGuid,
			Parts_tracking.OwnerGuid
		FROM Parts_tracking
		WHERE Parts_tracking.[Guid] IN (
			SELECT Parts_tracking.RawPartId
				FROM Parts_tracking
				WHERE Parts_tracking.ParentAssemblyGuid IN (SELECT [Guid] FROM GetAssemblyHierarchyForReleasedProjectsSync()) OR
					Parts_tracking.ParentProjectGuid IN (SELECT [Guid] FROM GetProjectsHierarchyForReleasedProjectsSync()))
	)