CREATE FUNCTION GetProcessStepPartAmountsHierarchyForReleasedProjectsSync()
RETURNS TABLE
AS
 RETURN
   (
      SELECT [ProcessStepGuid],
	                [PartGuid]
					FROM ProcessStepPartAmounts_tracking
					WHERE  [PartGuid] IN (SELECT [Guid] FROM GetPartsHierarchyForReleasedProjectsSync()) 					
   )
