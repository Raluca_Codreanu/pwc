CREATE FUNCTION GetCountrySettingsHierarchyForReleasedProjectsSync()
RETURNS TABLE
AS
 RETURN
   (
	 SELECT 
		CountrySettings_tracking.[Guid]
	 FROM CountrySettings_tracking
	 WHERE CountrySettings_tracking.[Guid] IN ( SELECT [CountrySettingsGuid] FROM GetAssemblyHierarchyForReleasedProjectsSync()) OR
		   CountrySettings_tracking.[Guid] IN ( SELECT [CountrySettingsGuid] FROM GetPartsHierarchyForReleasedProjectsSync())

   )

