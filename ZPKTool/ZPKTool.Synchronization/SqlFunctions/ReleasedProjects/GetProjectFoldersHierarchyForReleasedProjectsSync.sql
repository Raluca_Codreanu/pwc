﻿CREATE FUNCTION GetProjectFoldersHierarchyForReleasedProjectsSync()
RETURNS TABLE
AS
RETURN
(
WITH ProjectFoldersHierarchy ([Guid],[IsReleased]) AS
(
-- Base case : Anchor member definition
SELECT
ProjectFolders_tracking.[Guid],
ProjectFolders_tracking.[IsReleased]
FROM [ProjectFolders_tracking]
WHERE ProjectFolders_tracking.[ParentGuid] IS NULL AND
ProjectFolders_tracking.[OwnerGuid] IS NULL AND
ProjectFolders_tracking.[IsReleased]='True'

UNION ALL

-- Recursive member definition, recursive step
SELECT p.[Guid],p.[IsReleased]
FROM ProjectFolders_tracking p
INNER JOIN ProjectFoldersHierarchy ph ON
p.[ParentGuid] = ph.[Guid]
)

SELECT [Guid] FROM ProjectFoldersHierarchy
)

