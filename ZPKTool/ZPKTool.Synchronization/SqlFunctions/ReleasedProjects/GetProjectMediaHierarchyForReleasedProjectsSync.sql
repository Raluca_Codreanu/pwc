CREATE FUNCTION GetProjectMediaHierarchyForReleasedProjectsSync()
RETURNS TABLE
AS
 RETURN
   (
      SELECT 
	              [MediaGuid] 
				  FROM ProjectMedia_tracking
				  WHERE ProjectMedia_tracking.[ProjectGuid] IN (SELECT [Guid] FROM GetProjectsHierarchyForReleasedProjectsSync())					   
   )
