CREATE FUNCTION GetMachinesHierarchyForReleasedProjectsSync()
RETURNS TABLE
AS
 RETURN
   (
     Select Machines_tracking.[Guid],
	            Machines_tracking.[MediaGuid],
				Machines_tracking.[ManufacturerGuid]
				FROM 
				    Machines_tracking
					WHERE Machines_tracking.[ProcessStepGuid] IN (SELECT [Guid] FROM  GetProcessStepsHierarchyForReleasedProjectsSync())   
				
   )
