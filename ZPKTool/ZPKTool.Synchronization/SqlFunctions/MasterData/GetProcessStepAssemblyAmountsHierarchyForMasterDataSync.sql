CREATE FUNCTION GetProcessStepAssemblyAmountsHierarchyForMasterDataSync()
RETURNS TABLE
AS
 RETURN
   (
      SELECT [ProcessStepGuid],
	                [AssemblyGuid]
					FROM ProcessStepAssemblyAmounts_tracking
					WHERE  [AssemblyGuid] IN (SELECT [Guid] FROM GetAssemblyHierarchyForMasterDataSync()) 					
   )
