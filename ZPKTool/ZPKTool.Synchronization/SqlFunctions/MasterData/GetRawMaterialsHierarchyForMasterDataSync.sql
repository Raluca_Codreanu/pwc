CREATE FUNCTION GetRawMaterialsHierarchyForMasterDataSync()
	RETURNS TABLE
	AS
	 RETURN
	   (
		 SELECT RawMaterials_tracking.[Guid],
					   RawMaterials_tracking.[MediaGuid],
					   RawMaterials_tracking.[ManufacturerGuid]
					   FROM RawMaterials_tracking
					   WHERE RawMaterials_tracking.[PartGuid] IN (SELECT [Guid] FROM GetPartsHierarchyForMasterDataSync())	 OR
									 ( RawMaterials_tracking.[PartGuid] IS NULL AND
									   RawMaterials_tracking.[OwnerGuid] IS NULL AND
									   RawMaterials_tracking.[IsMasterData] = 'True'
									   )
	   )
