CREATE FUNCTION GetMachinesHierarchyForMasterDataSync()
	RETURNS TABLE
	AS
	 RETURN
	   (
		 Select Machines_tracking.[Guid],
					Machines_tracking.[MediaGuid],
					Machines_tracking.[ManufacturerGuid]
					FROM 
						Machines_tracking
						WHERE Machines_tracking.[ProcessStepGuid] IN (SELECT [Guid] FROM  GetProcessStepsHierarchyForMasterDataSync())  OR
									  (Machines_tracking.[ProcessStepGuid]  IS NULL AND
									  Machines_tracking.[OwnerGuid]  IS NULL AND
									  Machines_tracking.[IsMasterData] = 'True' 
									  )
					
	   )
