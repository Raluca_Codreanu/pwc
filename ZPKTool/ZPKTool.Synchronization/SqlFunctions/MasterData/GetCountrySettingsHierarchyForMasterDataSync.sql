CREATE FUNCTION GetCountrySettingsHierarchyForMasterDataSync()
RETURNS TABLE
AS
 RETURN
   (
	 SELECT 
		CountrySettings_tracking.[Guid]
	 FROM CountrySettings_tracking
	 WHERE CountrySettings_tracking.[Guid] IN ( SELECT [CountrySettingsGuid] FROM GetAssemblyHierarchyForMasterDataSync()) OR
		          CountrySettings_tracking.[Guid] IN ( SELECT [CountrySettingsGuid] FROM GetPartsHierarchyForMasterDataSync()) 

   )

