CREATE FUNCTION GetDiesHierarchyForMasterDataSync()
RETURNS TABLE
AS
RETURN
(
SELECT Dies_tracking.[Guid],
Dies_tracking.[ManufacturerGuid],
Dies_tracking.[MediaGuid]
FROM Dies_tracking
WHERE Dies_tracking.[ProcessStepGuid] IN (SELECT [Guid] FROM GetProcessStepsHierarchyForMasterDataSync()) OR
(Dies_tracking.[ProcessStepGuid]  IS NULL AND
Dies_tracking.[OwnerGuid]  IS NULL AND
Dies_tracking.[IsMasterData] = 'True'
)

)
