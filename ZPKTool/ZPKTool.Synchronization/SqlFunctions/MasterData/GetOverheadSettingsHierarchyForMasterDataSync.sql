CREATE FUNCTION GetOverheadSettingsHierarchyForMasterDataSync()
	RETURNS TABLE
	AS
	 RETURN
	   (
		 SELECT 
			OverheadSettings_tracking.[Guid]
		 FROM OverheadSettings_tracking
		 WHERE OverheadSettings_tracking.[Guid] IN ( SELECT [OverheadSettingsGuid] FROM GetAssemblyHierarchyForMasterDataSync()) OR
			   OverheadSettings_tracking.[Guid] IN ( SELECT [OverheadSettingsGuid] FROM GetPartsHierarchyForMasterDataSync())  	 

	   )
