CREATE FUNCTION GetCommoditiesHierarchyForMasterDataSync()
	RETURNS TABLE
	AS
	 RETURN
	   (
	   SELECT Commodities_tracking.[Guid],
	   Commodities_tracking.[MediaGuid],
	   Commodities_tracking.[ManufacturerGuid]
		FROM Commodities_tracking
		 WHERE Commodities_tracking.[ProcessStepGuid] IN (SELECT [Guid] FROM GetProcessStepsHierarchyForMasterDataSync()) OR 
					  Commodities_tracking.[PartGuid] IN (SELECT [Guid] FROM GetPartsHierarchyForMasterDataSync()) OR
					  (Commodities_tracking.[PartGuid] IS NULL  AND
					  Commodities_tracking.[ProcessStepGuid] IS NULL  AND 
					  Commodities_tracking.[IsMasterData] = 'True'  AND
					  Commodities_tracking.[OwnerGuid] IS NULL)
	   )
