CREATE FUNCTION GetConsumablesHierarchyForMasterDataSync()
	RETURNS TABLE
	AS
	 RETURN
	   (
		   SELECT Consumables_tracking.[Guid],
						 Consumables_tracking.[ManufacturerGuid]				
						 FROM Consumables_tracking
							 WHERE Consumables_tracking.[ProcessStepGuid] IN (SELECT [Guid] FROM GetProcessStepsHierarchyForMasterDataSync()) OR
										  (Consumables_tracking.[ProcessStepGuid] IS NULL AND 
											Consumables_tracking.[OwnerGuid] IS NULL AND
											Consumables_tracking.[IsMasterData] = 'True')						 
	   )
