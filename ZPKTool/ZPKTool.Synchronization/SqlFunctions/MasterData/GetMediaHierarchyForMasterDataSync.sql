CREATE FUNCTION GetMediaHierarchyForMasterDataSync()
	RETURNS TABLE
	AS
	 RETURN
	   (
		  Select Media_tracking.[Guid]
		  FROM Media_tracking
		  WHERE Media_tracking.[OwnerGuid] is NULL AND Media_tracking.[IsMasterData]='True'  
	   )
