CREATE FUNCTION GetManufacturersHierarchyForMasterDataSync()
	RETURNS TABLE
	AS
	 RETURN
	   (
		SELECT [Guid] 
		   FROM Manufacturers_tracking 
		   WHERE Manufacturers_tracking.[Guid] IN (SELECT [ManufacturerGuid] FROM GetAssemblyHierarchyForMasterDataSync()) OR
						Manufacturers_tracking.[Guid] IN (SELECT [ManufacturerGuid] FROM GetCommoditiesHierarchyForMasterDataSync()) OR
						Manufacturers_tracking.[Guid] IN (SELECT [ManufacturerGuid] FROM GetConsumablesHierarchyForMasterDataSync()) OR
						Manufacturers_tracking.[Guid] IN (SELECT [ManufacturerGuid] FROM GetDiesHierarchyForMasterDataSync()) OR
						Manufacturers_tracking.[Guid] IN (SELECT [ManufacturerGuid] FROM GetMachinesHierarchyForMasterDataSync()) OR
						Manufacturers_tracking.[Guid] IN (SELECT [ManufacturerGuid] FROM GetPartsHierarchyForMasterDataSync()) OR
						Manufacturers_tracking.[Guid] IN (SELECT [ManufacturerGuid] FROM GetRawMaterialsHierarchyForMasterDataSync()) OR
						(Manufacturers_tracking.[OwnerGuid] IS NULL AND
						Manufacturers_tracking.[IsMasterData] = 'True'
						)
	   )
