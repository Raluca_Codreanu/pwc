CREATE FUNCTION GetCountrySettingsHierarchyForMyProjectsSync(@OwnerGuid UNIQUEIDENTIFIER, @Offline BIT)
	RETURNS TABLE
	AS
	 RETURN
	   (
		 SELECT 
			CountrySettings_tracking.[Guid]
		 FROM CountrySettings_tracking
		 WHERE CountrySettings_tracking.[Guid] IN ( SELECT [CountrySettingsGuid] FROM GetAssemblyHierarchyForMyProjectsSync(@OwnerGuid, @Offline)) OR
			          CountrySettings_tracking.[Guid] IN ( SELECT [CountrySettingsGuid] FROM GetPartsHierarchyForMyProjectsSync(@OwnerGuid, @Offline))

	   )
