CREATE FUNCTION GetPartsHierarchyForMyProjectsSync(@OwnerGuid UNIQUEIDENTIFIER, @Offline BIT)
RETURNS TABLE
AS
RETURN
	(SELECT Parts_tracking.[Guid],
			Parts_tracking.ManufacturerGuid,
			Parts_tracking.OverheadSettingsGuid,
			Parts_tracking.ProcessGuid,
			Parts_tracking.CountrySettingsGuid,
			Parts_tracking.CalculatorGuid,
			Parts_tracking.OwnerGuid
		FROM Parts_tracking
		WHERE Parts_tracking.ParentAssemblyGuid IN (SELECT [Guid] FROM GetAssemblyHierarchyForMyProjectsSync(@OwnerGuid, @Offline)) OR
			Parts_tracking.ParentProjectGuid IN (SELECT [Guid] FROM GetProjectsHierarchyForMyProjectsSync(@OwnerGuid, @Offline))
	UNION
	SELECT Parts_tracking.[Guid],
			Parts_tracking.ManufacturerGuid,
			Parts_tracking.OverheadSettingsGuid,
			Parts_tracking.ProcessGuid,
			Parts_tracking.CountrySettingsGuid,
			Parts_tracking.CalculatorGuid,
			Parts_tracking.OwnerGuid
		FROM Parts_tracking
		WHERE Parts_tracking.[Guid] IN (
			SELECT Parts_tracking.RawPartId
				FROM Parts_tracking
				WHERE Parts_tracking.ParentAssemblyGuid IN (SELECT [Guid] FROM GetAssemblyHierarchyForMyProjectsSync(@OwnerGuid, @Offline)) OR
					Parts_tracking.ParentProjectGuid IN (SELECT [Guid] FROM GetProjectsHierarchyForMyProjectsSync(@OwnerGuid, @Offline)))
	UNION
	SELECT Parts_tracking.[Guid],
			Parts_tracking.ManufacturerGuid,
			Parts_tracking.OverheadSettingsGuid,
			Parts_tracking.ProcessGuid,
			Parts_tracking.CountrySettingsGuid,
			Parts_tracking.CalculatorGuid,
			Parts_tracking.OwnerGuid
		FROM Parts_tracking
		WHERE Parts_tracking.IsRawPart = 'True' AND
			Parts_tracking.OwnerGuid = @OwnerGuid AND
			Parts_tracking.[Guid] NOT IN (
				SELECT Parts_tracking.RawPartId
					FROM Parts_tracking
					WHERE Parts_tracking.RawPartId IS NOT NULL AND
						Parts_tracking.OwnerGuid = @OwnerGuid)
	)