CREATE FUNCTION GetProjectCurrenciesHierarchyForMyProjectsSync(@OwnerGuid UNIQUEIDENTIFIER, @Offline BIT)
RETURNS TABLE
AS
RETURN
	(
		SELECT CurrencyId
			FROM ProjectCurrencies_tracking
			WHERE ProjectId IN (SELECT [Guid] FROM GetProjectsHierarchyForMyProjectsSync(@OwnerGuid, @Offline))					   
	)