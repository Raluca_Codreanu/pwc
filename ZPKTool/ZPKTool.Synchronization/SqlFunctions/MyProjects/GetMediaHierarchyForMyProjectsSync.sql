CREATE FUNCTION GetMediaHierarchyForMyProjectsSync(@OwnerGuid UNIQUEIDENTIFIER, @Offline BIT)
RETURNS TABLE
AS
RETURN
(
Select Media_tracking.[Guid]
FROM Media_tracking
WHERE Media_tracking.[Guid] IN (SELECT [MediaGuid]  FROM GetMachinesHierarchyForMyProjectsSync(@OwnerGuid, @Offline)) OR
Media_tracking.[Guid] IN (SELECT [MediaGuid]  FROM GetCommoditiesHierarchyForMyProjectsSync(@OwnerGuid, @Offline)) OR
Media_tracking.[Guid] IN (SELECT [MediaGuid]  FROM GetRawMaterialsHierarchyForMyProjectsSync(@OwnerGuid, @Offline)) OR
Media_tracking.[Guid] IN (SELECT [MediaGuid]  FROM GetDiesHierarchyForMyProjectsSync(@OwnerGuid, @Offline)) OR
Media_tracking.[Guid] IN (SELECT [MediaGuid]  FROM GetProjectMediaHierarchyForMyProjectsSync(@OwnerGuid, @Offline)) OR
Media_tracking.[Guid] IN (SELECT [MediaGuid]  FROM GetAssemblyMediaHierarchyForMyProjectsSync(@OwnerGuid, @Offline)) OR
Media_tracking.[Guid] IN (SELECT [MediaGuid]  FROM GetPartMediaHierarchyForMyProjectsSync(@OwnerGuid, @Offline))  OR
Media_tracking.[Guid] IN (SELECT [MediaGuid]  FROM GetProcessStepsHierarchyForMyProjectsSync(@OwnerGuid, @Offline))  
)
