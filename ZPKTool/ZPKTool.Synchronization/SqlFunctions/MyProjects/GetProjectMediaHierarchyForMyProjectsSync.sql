CREATE FUNCTION GetProjectMediaHierarchyForMyProjectsSync(@OwnerGuid UNIQUEIDENTIFIER, @Offline BIT)
RETURNS TABLE
AS
 RETURN
   (
      SELECT 
	              [MediaGuid] 
				  FROM ProjectMedia_tracking
				  WHERE ProjectMedia_tracking.[ProjectGuid] IN (SELECT [Guid] FROM GetProjectsHierarchyForMyProjectsSync(@OwnerGuid, @Offline))					   
   )
