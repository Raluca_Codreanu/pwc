CREATE FUNCTION GetProcessesHierarchyForMyProjectsSync(@OwnerGuid UNIQUEIDENTIFIER, @Offline BIT)
	RETURNS TABLE
	AS
	 RETURN
	   (
		   SELECT Processes_tracking.[Guid]
			  FROM Processes_tracking
			  WHERE 
						  Processes_tracking.[Guid] IN (SELECT [ProcessGuid] FROM GetAssemblyHierarchyForMyProjectsSync(@OwnerGuid, @Offline)) OR
						  Processes_tracking.[Guid] IN (SELECT [ProcessGuid] FROM GetPartsHierarchyForMyProjectsSync(@OwnerGuid, @Offline))
	   )
