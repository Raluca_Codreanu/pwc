CREATE FUNCTION GetRawMaterialsHierarchyForMyProjectsSync(@OwnerGuid UNIQUEIDENTIFIER, @Offline BIT)
RETURNS TABLE
AS
 RETURN
   (
     SELECT RawMaterials_tracking.[Guid],
	              RawMaterials_tracking.[MediaGuid],
	              RawMaterials_tracking.[ManufacturerGuid]
				  FROM RawMaterials_tracking
				  WHERE RawMaterials_tracking.[PartGuid] IN (SELECT [Guid] FROM GetPartsHierarchyForMyProjectsSync(@OwnerGuid, @Offline))
	 
   )
