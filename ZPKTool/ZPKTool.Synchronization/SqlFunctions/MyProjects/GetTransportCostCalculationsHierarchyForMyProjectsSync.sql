CREATE FUNCTION GetTransportCostCalculationsHierarchyForMyProjectsSync(@OwnerGuid UNIQUEIDENTIFIER, @Offline BIT)
RETURNS TABLE
AS
 RETURN
   (
       SELECT [Guid]
	      FROM TransportCostCalculations_tracking
		  WHERE AssemblyId IN (SELECT [Guid] FROM GetAssemblyHierarchyForMyProjectsSync(@OwnerGuid, @Offline)) OR
				PartId IN (SELECT [Guid] FROM GetPartsHierarchyForMyProjectsSync(@OwnerGuid, @Offline))
   )