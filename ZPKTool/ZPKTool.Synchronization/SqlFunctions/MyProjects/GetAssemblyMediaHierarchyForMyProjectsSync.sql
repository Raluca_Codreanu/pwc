CREATE FUNCTION GetAssemblyMediaHierarchyForMyProjectsSync(@OwnerGuid UNIQUEIDENTIFIER,@Offline BIT)
	RETURNS TABLE
	AS
	 RETURN
	   (
			 SELECT 
					  [MediaGuid] 
					  FROM AssemblyMedia_tracking
					  WHERE AssemblyMedia_tracking.[AssemblyGuid] IN (SELECT [Guid] FROM GetAssemblyHierarchyForMyProjectsSync(@OwnerGuid,@Offline))		
	   )
