CREATE FUNCTION GetManufacturersHierarchyForMyProjectsSync(@OwnerGuid UNIQUEIDENTIFIER, @Offline BIT)
	RETURNS TABLE
	AS
	 RETURN
	   (
		SELECT [Guid] 
		   FROM Manufacturers_tracking 
		   WHERE Manufacturers_tracking.[Guid] IN (SELECT [ManufacturerGuid] FROM GetAssemblyHierarchyForMyProjectsSync(@OwnerGuid, @Offline)) OR
						Manufacturers_tracking.[Guid] IN (SELECT [ManufacturerGuid] FROM GetCommoditiesHierarchyForMyProjectsSync(@OwnerGuid, @Offline)) OR
						Manufacturers_tracking.[Guid] IN (SELECT [ManufacturerGuid] FROM GetConsumablesHierarchyForMyProjectsSync(@OwnerGuid, @Offline)) OR
						Manufacturers_tracking.[Guid] IN (SELECT [ManufacturerGuid] FROM GetDiesHierarchyForMyProjectsSync(@OwnerGuid, @Offline)) OR
						Manufacturers_tracking.[Guid] IN (SELECT [ManufacturerGuid] FROM GetMachinesHierarchyForMyProjectsSync(@OwnerGuid, @Offline)) OR
						Manufacturers_tracking.[Guid] IN (SELECT [ManufacturerGuid] FROM GetPartsHierarchyForMyProjectsSync(@OwnerGuid, @Offline)) OR
						Manufacturers_tracking.[Guid] IN (SELECT [ManufacturerGuid] FROM GetRawMaterialsHierarchyForMyProjectsSync(@OwnerGuid, @Offline)) 
	   )
