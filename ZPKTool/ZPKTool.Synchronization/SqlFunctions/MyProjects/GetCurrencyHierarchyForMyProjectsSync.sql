CREATE FUNCTION GetCurrencyHierarchyForMyProjectsSync(@OwnerGuid UNIQUEIDENTIFIER, @Offline BIT)
RETURNS TABLE
AS
RETURN
	(
		SELECT [Guid]
			FROM Currencies_tracking
			WHERE [Guid] IN (SELECT CurrencyId FROM GetProjectCurrenciesHierarchyForMyProjectsSync(@OwnerGuid, @Offline))
	)