CREATE FUNCTION GetProcessStepAssemblyAmountsHierarchyForMyProjectsSync(@OwnerGuid UNIQUEIDENTIFIER, @Offline BIT)
RETURNS TABLE
AS
 RETURN
   (
      SELECT [ProcessStepGuid],
	                [AssemblyGuid]
					FROM ProcessStepAssemblyAmounts_tracking
					WHERE  [AssemblyGuid] IN (SELECT [Guid] FROM GetAssemblyHierarchyForMyProjectsSync(@OwnerGuid, @Offline)) 					
   )
