CREATE FUNCTION GetOverheadSettingsHierarchyForMyProjectsSync(@OwnerGuid UNIQUEIDENTIFIER, @Offline BIT)
	RETURNS TABLE
	AS
	 RETURN
	   (
		 SELECT 
			OverheadSettings_tracking.[Guid]
		 FROM OverheadSettings_tracking
		 WHERE OverheadSettings_tracking.[Guid] IN ( SELECT [OverheadSettingsGuid] FROM GetAssemblyHierarchyForMyProjectsSync(@OwnerGuid, @Offline)) OR
			   OverheadSettings_tracking.[Guid] IN ( SELECT [OverheadSettingsGuid] FROM GetPartsHierarchyForMyProjectsSync(@OwnerGuid, @Offline))  OR
			   OverheadSettings_tracking.[Guid] IN ( SELECT [OverheadSettingsGuid] FROM  GetProjectsHierarchyForMyProjectsSync(@OwnerGuid, @Offline))

	   )
