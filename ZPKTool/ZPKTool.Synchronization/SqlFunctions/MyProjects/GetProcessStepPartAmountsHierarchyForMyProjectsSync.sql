CREATE FUNCTION GetProcessStepPartAmountsHierarchyForMyProjectsSync(@OwnerGuid UNIQUEIDENTIFIER, @Offline BIT)
RETURNS TABLE
AS
 RETURN
   (
      SELECT [ProcessStepGuid],
	                [PartGuid]
					FROM ProcessStepPartAmounts_tracking
					WHERE  [PartGuid] IN (SELECT [Guid] FROM GetPartsHierarchyForMyProjectsSync(@OwnerGuid, @Offline)) 					
   )
