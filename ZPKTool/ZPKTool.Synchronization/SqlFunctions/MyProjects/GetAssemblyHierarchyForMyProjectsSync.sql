CREATE FUNCTION GetAssemblyHierarchyForMyProjectsSync(@OwnerGuid UNIQUEIDENTIFIER, @Offline BIT)
	RETURNS TABLE
	AS
	 RETURN
	   (
		WITH AssembliesHierarchy([Guid],[ProcessGuid],[OverheadSettingsGuid],[CountrySettingsGuid],[ManufacturerGuid]) AS
		(
		 -- Base case : Anchor member definition
		 SELECT 
			Assemblies_tracking.[Guid],
			Assemblies_tracking.[ProcessGuid],
			Assemblies_tracking.[OverheadSettingsGuid],
			Assemblies_tracking.[CountrySettingsGuid],
			Assemblies_tracking.[ManufacturerGuid]	
		 FROM Assemblies_tracking
		 WHERE Assemblies_tracking.ParentAssemblyGuid IS NULL AND		 
					  Assemblies_tracking.ProjectGuid IN (SELECT [Guid] FROM GetProjectsHierarchyForMyProjectsSync(@ownerGuid,@Offline))

		 UNION ALL

		 -- Recursive member definition, recursive step
		 SELECT a.[Guid],      
					  a.[ProcessGuid],
					  a.[OverheadSettingsGuid],
					  a.[CountrySettingsGuid],
					  a.[ManufacturerGuid]		 
		  FROM Assemblies_tracking a
		  INNER JOIN AssembliesHierarchy ah ON
			a.ParentAssemblyGuid = ah.[Guid]
		)

		SELECT [Guid], 
					  [ProcessGuid],
					  [OverheadSettingsGuid],
					  [CountrySettingsGuid],
					  [ManufacturerGuid] 
					  FROM AssembliesHierarchy
	)

