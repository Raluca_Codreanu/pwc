CREATE FUNCTION GetProjectsHierarchyForMyProjectsSync(@OwnerGuid UNIQUEIDENTIFIER, @Offline BIT)
RETURNS TABLE
AS
 RETURN
   (
	SELECT Projects_tracking.[Guid],
               Projects_tracking.[ProjectLeaderGuid], 
               Projects_tracking.[ResponsableCalculatorGuid],
               Projects_tracking.[CustomerGuid],
               Projects_tracking.[OverheadSettingsGuid]
	  FROM Projects_tracking
		WHERE Projects_tracking.FolderGuid IN (SELECT [Guid] FROM GetProjectFoldersHierarchyForMyProjectsSync(@OwnerGuid, @Offline)) OR
					(Projects_tracking.[OwnerGuid] = @OwnerGuid AND Projects_tracking.[IsReleased]='False' AND Projects_tracking.[IsOffline] = @Offline)					                                                                                                          
)

