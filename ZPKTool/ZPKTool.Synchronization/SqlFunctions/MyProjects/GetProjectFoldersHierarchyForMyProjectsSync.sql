CREATE FUNCTION GetProjectFoldersHierarchyForMyProjectsSync(@OwnerGuid UNIQUEIDENTIFIER, @Offline BIT)
RETURNS TABLE
AS
RETURN
(
WITH ProjectFoldersHierarchy ([Guid],[IsOffline]) AS
(
-- Base case : Anchor member definition
SELECT
ProjectFolders_tracking.[Guid],
ProjectFolders_tracking.[IsOffline]
FROM [ProjectFolders_tracking]
WHERE ProjectFolders_tracking.[ParentGuid] IS NULL AND
ProjectFolders_tracking.[OwnerGuid] = @OwnerGuid

UNION ALL

-- Recursive member definition, recursive step
SELECT p.[Guid],p.[IsOffline]
FROM ProjectFolders_tracking p
INNER JOIN ProjectFoldersHierarchy ph ON
p.[ParentGuid] = ph.[Guid]
)

SELECT [Guid] FROM ProjectFoldersHierarchy WHERE [IsOffline] = @Offline
)


