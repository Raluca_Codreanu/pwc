CREATE FUNCTION GetConsumablesHierarchyForMyProjectsSync(@OwnerGuid UNIQUEIDENTIFIER, @Offline BIT)
	RETURNS TABLE
	AS
	 RETURN
	   (
		   SELECT Consumables_tracking.[Guid],
						 Consumables_tracking.[ManufacturerGuid]				
						 FROM Consumables_tracking
							 WHERE Consumables_tracking.[ProcessStepGuid] IN (SELECT [Guid] FROM GetProcessStepsHierarchyForMyProjectsSync(@OwnerGuid, @Offline))   
	   )
