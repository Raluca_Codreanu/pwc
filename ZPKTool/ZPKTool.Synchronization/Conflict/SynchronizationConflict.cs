﻿using System;

namespace ZPKTool.Synchronization
{
    /// <summary>
    /// The possible resolutions for a conflict.
    /// </summary>
    public enum SynchronizationConflictResolution
    {
        /// <summary>
        /// Ignore the conflict.
        /// </summary>
        Ignore = 0,

        /// <summary>
        /// The local db copy wins (will overwrite the central db copy).
        /// </summary>
        LocalVersionWins = 1,

        /// <summary>
        /// The central db copy wins (will overwrite the local db copy).
        /// </summary>
        CentralVersionWins = 2
    }

    /// <summary>
    /// Encapsulates information about a conflict detected during synchronization.
    /// </summary>
    public class SynchronizationConflict
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SynchronizationConflict"/> class.
        /// </summary>
        public SynchronizationConflict()
        {
            Resolution = SynchronizationConflictResolution.Ignore;
        }

        /// <summary>
        /// Gets or sets the local db copy of the entity for which the conflict appeared.
        /// </summary>
        public object LocalEntityVersion { get; set; }

        /// <summary>
        /// Gets or sets the central db copy of the entity for which the conflict appeared.
        /// </summary>
        public object CentralEntityVersion { get; set; }

        /// <summary>
        /// Gets or sets the entity GUID.
        /// </summary>
        /// <value>
        /// The entity GUID.
        /// </value>
        public Guid EntityGuid { get; set; }

        /// <summary>
        /// Gets or sets the resolution for this conflict. The default is Ignore.
        /// </summary>
        public SynchronizationConflictResolution Resolution { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is resolution set by user.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is resolution set by user; otherwise, <c>false</c>.
        /// </value>
        public bool IsResolutionSetByUser { get; set; }

        /// <summary>
        /// Gets or sets the type of the entity.
        /// </summary>
        public string EntityType { get; set; }

        /// <summary>
        /// Gets or sets the sync task.
        /// </summary>
        /// <value>
        /// The sync task.
        /// </value>
        public SyncType SyncTask { get; set; }
    }
}
