﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace ZPKTool.Synchronization
{
    /// <summary>
    /// The Synchronization types.
    /// Don't rename!
    /// </summary>
    public enum SyncType
    {
        /// <summary>
        /// Static Data and Settings synchronization.
        /// </summary>
        StaticDataAndSettings = 0,

        /// <summary>
        /// My Projects synchronization.
        /// </summary>
        MyProjects = 1,

        /// <summary>
        /// Master Data synchronization.
        /// </summary>
        MasterData = 2,

        /// <summary>
        /// Released Projects synchronization.
        /// </summary>
        ReleasedProjects = 3,

        /// <summary>
        /// User synchronization.
        /// </summary>
        User = 4,

        /// <summary>
        /// The logs synchronization.
        /// </summary>
        Logs = 5,

        /// <summary>
        /// The global settings synchronization.
        /// </summary>
        GlobalSettings = 6
    }

    /// <summary>
    /// The synchronization scope provisioning templates.
    /// Don't rename!
    /// </summary>
    public enum SyncTemplates
    {
        /// <summary>
        /// Static Data and Settings Synchronization Template.
        /// </summary>
        StaticDataAndSettings_Template = 0,

        /// <summary>
        /// My Projects Synchronization Template.
        /// </summary>
        MyProjects_Template = 1,

        /// <summary>
        /// Master Data Synchronization Template.
        /// </summary>
        MasterData_Template = 2,

        /// <summary>
        /// Released Projects Synchronization Template.
        /// </summary>
        ReleasedProjects_Template = 3,

        /// <summary>
        /// User Synchronization Template.
        /// </summary>
        User_Template = 4,

        /// <summary>
        /// Logs Template.
        /// </summary>
        Logs_Template = 5
    }
}
