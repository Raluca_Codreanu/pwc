﻿namespace ZPKTool.Synchronization
{
    using System.Data.SqlClient;
    using ZPKTool.Common;

    /// <summary>
    /// Contains the static data used for configuring the database access.
    /// </summary>
    public static class Configuration
    {
        /// <summary>
        /// Gets the connection central ADO connection string. To be used with ADO connection objects (SqlConnection).
        /// </summary>
        public static string ConnectionStringCentralADO { get; private set; }

        /// <summary>
        /// Gets the connection local ADO connection string. To be used with ADO connection objects (SqlConnection).
        /// </summary>
        public static string ConnectionStringLocalADO { get; private set; }

        /// <summary>
        /// Initializes the DB access. This initialization should be done at the startup of the application.
        /// It is used by the synchronization orchestrator to connect to the local and remote databases.
        /// </summary>
        /// <param name="localConnectionParams">The local connection parameters.</param>
        /// <param name="centralConnectionParams">The central connection parameters.</param>
        public static void InitializeDBAccess(DbConnectionInfo localConnectionParams, DbConnectionInfo centralConnectionParams)
        {
            localConnectionParams.ConnectionTimeout = 5;
            localConnectionParams.Pooling = true;
            centralConnectionParams.ConnectionTimeout = 5;
            centralConnectionParams.Pooling = true;

            ConnectionStringLocalADO = CreateSqlConnectionString(localConnectionParams);
            ConnectionStringCentralADO = CreateSqlConnectionString(centralConnectionParams);
        }

        /// <summary>
        /// Creates a connection string to be used by the <see cref="System.Data.SqlClient.SqlConnection"/> class.
        /// </summary>
        /// <param name="connectionInfo">The connection information.</param>
        /// <returns>The connection string</returns>
        public static string CreateSqlConnectionString(DbConnectionInfo connectionInfo)
        {
            string decryptedPassword = connectionInfo.Password;
            if (!string.IsNullOrEmpty(connectionInfo.PasswordEncryptionKey))
            {
                decryptedPassword = EncryptionManager.Instance.DecryptBlowfish(connectionInfo.Password, connectionInfo.PasswordEncryptionKey);
            }

            var connStrgBuilder = new SqlConnectionStringBuilder();
            connStrgBuilder.DataSource = connectionInfo.DataSource;
            connStrgBuilder.InitialCatalog = connectionInfo.InitialCatalog;
            connStrgBuilder.WorkstationID = "SyncWorker";

            if (connectionInfo.UseWindowsAuthentication)
            {
                connStrgBuilder.IntegratedSecurity = true;
            }
            else
            {
                connStrgBuilder.IntegratedSecurity = false;
                connStrgBuilder.UserID = connectionInfo.UserId;
                connStrgBuilder.Password = decryptedPassword;
            }

            connStrgBuilder.Pooling = connectionInfo.Pooling;
            if (connectionInfo.ConnectionTimeout.HasValue)
            {
                connStrgBuilder.ConnectTimeout = connectionInfo.ConnectionTimeout.Value;
            }

            return connStrgBuilder.ConnectionString;
        }
    }
}
