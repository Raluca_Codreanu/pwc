﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Synchronization
{
    /// <summary>
    /// Indicates the direction of synchronization. For two-way synchronizations this also includes 
    /// the order in which the synchronizations are performed.
    /// The direction is combined with the relative positions of the synchronization providers to
    /// determine the flow of changes during synchronization. Upload means changes originate from the 
    /// local provider and are applied to the remote provider. Download means changes originate from 
    /// the remote provider and are applied to the local provider.
    /// </summary>
    public enum SyncDirection
    {
        /// <summary>
        /// Upload followed by Download. 
        /// </summary>
        UploadAndDownload = 0,

        /// <summary>
        /// Download followed by upload. 
        /// </summary>
        DownloadAndUpload = 1,

        /// <summary>
        /// Upload only. 
        /// </summary>
        Upload = 2,

        /// <summary>
        /// Download only. 
        /// </summary>
        Download = 3,
    }
}
