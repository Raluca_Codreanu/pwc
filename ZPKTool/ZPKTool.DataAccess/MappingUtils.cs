﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Mapping;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Contains methods to work with the entity framework mappings.
    /// </summary>
    public static class MappingUtils
    {
        /// <summary>
        /// The data source manager.
        /// </summary>
        private static IDataSourceManager dataSourceManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);

        /// <summary>
        /// Gets the model type mappings.
        /// </summary>
        /// <param name="entityName">The name of the entity.</param>
        /// <returns>A collection of model type mappings.</returns>
        public static ICollection<MappingData> GetModelTypeMappings(string entityName)
        {
            var metadata = ((IObjectContextAdapter)dataSourceManager.EntityContext).ObjectContext.MetadataWorkspace;

            // Get the part of the model that contains info about the actual CLR types.
            var objectItemCollection = (ObjectItemCollection)metadata.GetItemCollection(DataSpace.OSpace);

            // Get the entity type from the model that maps to the CLR type.
            var entityType = metadata
                    .GetItems<EntityType>(DataSpace.OSpace)
                          .Single(e => objectItemCollection.GetClrType(e).Name == entityName);

            // Get the entity set that uses this entity type.
            var entitySet = metadata
                .GetItems<EntityContainer>(DataSpace.CSpace)
                      .Single()
                      .EntitySets
                      .Single(s => s.ElementType.Name == entityType.Name);

            // Find the mapping between conceptual and storage model for this entity set.
            var mapping = metadata.GetItems<EntityContainerMapping>(DataSpace.CSSpace)
                          .Single()
                          .EntitySetMappings
                          .Single(s => s.EntitySet == entitySet);

            var propertiesMapping = mapping
                .EntityTypeMappings.Single()
                .Fragments.Single()
                .PropertyMappings
                .OfType<ScalarPropertyMapping>()
                .Select(a => new MappingData { ColumnName = a.Column.Name, PropertyName = a.Property.Name, EntityType = a.Property.IsEnumType ? objectItemCollection.GetClrType(metadata.GetObjectSpaceType(a.Property.EnumType)) : a.Property.PrimitiveType.ClrEquivalentType })
                 .ToList();

            return propertiesMapping;
        }
    }
}
