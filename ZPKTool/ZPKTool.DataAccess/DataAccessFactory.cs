﻿namespace ZPKTool.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using ZPKTool.Data;

    /// <summary>
    /// A factory class for creating instances of data access related classes.
    /// </summary>
    public static class DataAccessFactory
    {        
        /// <summary>
        /// Gets or sets the delegate that is invoked to produce the instance returned by the <see cref="DataAccessFactory.CreateDataSourceManager"/> method.
        /// </summary>        
        public static Func<DbIdentifier, IDataSourceManager> DataSourceManagerFactory { get; set; }

        /// <summary>
        /// Creates a new <see cref="IDataSourceManager"/> instance.
        /// </summary>
        /// <param name="databaseId">A value indicating the database to which the <see cref="IDataSourceManager"/> instance will connect.</param>
        /// <returns>A new <see cref="IDataSourceManager"/> instance.</returns>
        public static IDataSourceManager CreateDataSourceManager(DbIdentifier databaseId)
        {
            var factory = DataSourceManagerFactory;
            if (factory != null)
            {
                return factory(databaseId);
            }
            else
            {
                return new DataSourceManager(databaseId);
            }
        }
    }
}
