﻿using System;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Contains the mapping between the column and the property of an entity.
    /// </summary>
    public class MappingData
    {
        /// <summary>
        /// Gets or sets the column name of the property.
        /// </summary>
        public string ColumnName { get; set; }

        /// <summary>
        /// Gets or sets the name of the property.
        /// </summary>
        public string PropertyName { get; set; }

        /// <summary>
        /// Gets or sets the entity type of the property.
        /// </summary>
        public Type EntityType { get; set; }
    }
}
