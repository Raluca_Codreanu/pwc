﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using ZPKTool.Common;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Implements the Unit of Work pattern for data operations.
    /// <para/>
    /// All objects obtained using the same instance of this class share the same Object Context (so the change tracking is working) and they will all be
    /// saved when the SaveChanges() method is called.
    /// <para/>
    /// Use an instance for each form/screen or for each set of related forms/screens when the save operation is performed only once for all of them.
    /// If possible, dispose of the instance when is no longer needed (not necessary, but a good practice in order to reclaim some memory).
    /// </summary>
    internal class DataSourceManager : IDataSourceManager
    {
        #region Fields

        /// <summary>
        /// specifies the amount of time in seconds for the timeout of the commit, when media files are involved
        /// </summary>
        private const int CommandTimeoutForMedia = 120;

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The cache of non-generic repository instances. It is gradually populated by the <see cref="ZPKTool.DataAccess.DataSourceManager.Repository"/> method
        /// by adding new instances to it whenever a requested repository does not exist.
        /// </summary>
        private Dictionary<Type, IRepository> repositoryCache = new Dictionary<Type, IRepository>();

        #endregion

        #region Constructors

        /// <summary>
        /// Prevents a default instance of the <see cref="DataSourceManager"/> class from being created.
        /// </summary>
        private DataSourceManager()
        {
            this.Uid = Guid.NewGuid();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataSourceManager"/> class.
        /// </summary>
        /// <param name="databaseId">The database on which this instance will perform operations.</param>
        public DataSourceManager(DbIdentifier databaseId)
            : this()
        {
            this.EntityContext = this.CreateEntityContext(databaseId);
            this.DatabaseId = databaseId;

            ((IObjectContextAdapter)this.EntityContext).ObjectContext.SavingChanges += new EventHandler(EntityContext_SavingChanges);

            this.AssemblyRepository = new AssemblyRepository(this.EntityContext, this.DatabaseId);
            this.BasicSettingsRepository = new BasicSettingsRepository(this.EntityContext, this.DatabaseId);
            this.CountrySettingsValueHistoryRepository = new CountrySettingsValueHistoryRepository(this.EntityContext, this.DatabaseId);
            this.CommodityRepository = new CommodityRepository(this.EntityContext, this.DatabaseId);
            this.CommodityPriceHistoryRepository = new CommodityPriceHistoryRepository(this.EntityContext, this.DatabaseId);
            this.ConsumableRepository = new ConsumableRepository(this.EntityContext, this.DatabaseId);
            this.ConsumablePriceHistoryRepository = new ConsumablePriceHistoryRepository(this.EntityContext, this.DatabaseId);
            this.CountryRepository = new CountryRepository(this.EntityContext, this.DatabaseId);
            this.CurrencyRepository = new CurrencyRepository(this.EntityContext, this.DatabaseId);
            this.CycleTimeCalculationRepository = new CycleTimeCalculationRepository(this.EntityContext, this.DatabaseId);
            this.GlobalSettingsRepository = new GlobalSettingsRepository(this.EntityContext, this.DatabaseId);
            this.DieRepository = new DieRepository(this.EntityContext, this.DatabaseId);
            this.MachineRepository = new MachineRepository(this.EntityContext, this.DatabaseId);
            this.MachinesClassificationRepository = new MachinesClassificationRepository(this.EntityContext, this.DatabaseId);
            this.ManufacturerRepository = new ManufacturerRepository(this.EntityContext, this.DatabaseId);
            this.MaterialsClassificationRepository = new MaterialsClassificationRepository(this.EntityContext, this.DatabaseId);
            this.MeasurementUnitRepository = new MeasurementUnitRepository(this.EntityContext, this.DatabaseId);
            this.MediaRepository = new MediaRepository(this.EntityContext, this.DatabaseId);
            this.OverheadSettingsRepository = new OverheadSettingsRepository(this.EntityContext, this.DatabaseId);
            this.PartRepository = new PartRepository(this.EntityContext, this.DatabaseId);
            this.PasswordsHistoryRepository = new PasswordsHistoryRepository(this.EntityContext, this.DatabaseId);
            this.ProcessRepository = new ProcessRepository(this.EntityContext, this.DatabaseId);
            this.ProcessStepRepository = new ProcessStepRepository(this.EntityContext, this.DatabaseId);
            this.ProcessStepsClassificationRepository = new ProcessStepsClassificationRepository(this.EntityContext, this.DatabaseId);
            this.ProjectFolderRepository = new ProjectFolderRepository(this.EntityContext, this.DatabaseId);
            this.ProjectRepository = new ProjectRepository(this.EntityContext, this.DatabaseId);
            this.RawMaterialRepository = new RawMaterialRepository(this.EntityContext, this.DatabaseId);
            this.RawMaterialDeliveryTypeRepository = new RawMaterialDeliveryTypeRepository(this.EntityContext, this.DatabaseId);
            this.RawMaterialPriceHistoryRepository = new RawMaterialPriceHistoryRepository(this.EntityContext, this.DatabaseId);
            this.SupplierRepository = new SupplierRepository(this.EntityContext, this.DatabaseId);
            this.TransportCostCalculationRepository = new TransportCostCalculationRepository(this.EntityContext, this.DatabaseId);
            this.TransportCostCalculationSettingRepository = new TransportCostCalculationSettingRepository(this.EntityContext, this.DatabaseId);
            this.TrashBinRepository = new TrashBinRepository(this.EntityContext, this.DatabaseId);
            this.UserRepository = new UserRepository(this.EntityContext, this.DatabaseId);
            this.BookmarkRepository = new BookmarkRepository(this.EntityContext, this.DatabaseId);
            this.CountrySupplierRepository = new CountrySupplierRepository(this.EntityContext, this.DatabaseId);
            this.CountrySettingRepository = new CountrySettingRepository(this.EntityContext, this.DatabaseId);
            this.LogRepository = new LogRepository(this.EntityContext, this.DatabaseId);
        }

        #endregion Constructors

        #region Events

        /// <summary>
        /// Occurs after the changes in this instance have been saved.
        /// </summary>
        public event EventHandler ChangesSaved;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets a value indicating the database on which this instance operates.
        /// </summary>        
        public DbIdentifier DatabaseId { get; private set; }

        /// <summary>
        /// Gets the Entity Framework ObjectContext used to execute the database operations.
        /// </summary>      
        //// TODO: this property should not be public (internal at best).
        public DataEntities EntityContext { get; private set; }

        /// <summary>
        /// Gets the unique identifier of this instance.
        /// </summary>
        public Guid Uid { get; private set; }

        #endregion Properties

        #region Repositories

        /// <summary>
        /// Gets the assembly repository.
        /// </summary>
        public IAssemblyRepository AssemblyRepository { get; private set; }

        /// <summary>
        /// Gets the basic settings repository.
        /// </summary>
        public IBasicSettingsRepository BasicSettingsRepository { get; private set; }

        /// <summary>
        /// Gets the country settings value history repository.
        /// </summary>
        public ICountrySettingsValueHistoryRepository CountrySettingsValueHistoryRepository { get; private set; }

        /// <summary>
        /// Gets the commodity repository.
        /// </summary>
        public ICommodityRepository CommodityRepository { get; private set; }

        /// <summary>
        /// Gets the commodity price history repository.
        /// </summary>
        public ICommodityPriceHistoryRepository CommodityPriceHistoryRepository { get; private set; }

        /// <summary>
        /// Gets the consumable repository.
        /// </summary>
        public IConsumableRepository ConsumableRepository { get; private set; }

        /// <summary>
        /// Gets the consumable price history repository.
        /// </summary>
        public IConsumablePriceHistoryRepository ConsumablePriceHistoryRepository { get; private set; }

        /// <summary>
        /// Gets the country repository.
        /// </summary>
        public ICountryRepository CountryRepository { get; private set; }

        /// <summary>
        /// Gets the currency repository.
        /// </summary>
        public ICurrencyRepository CurrencyRepository { get; private set; }

        /// <summary>
        /// Gets the cycle time calculation repository.
        /// </summary>
        public ICycleTimeCalculationRepository CycleTimeCalculationRepository { get; private set; }

        /// <summary>
        /// Gets the data base info repository.
        /// </summary>
        public IGlobalSettingsRepository GlobalSettingsRepository { get; private set; }

        /// <summary>
        /// Gets the die repository.
        /// </summary>
        public IDieRepository DieRepository { get; private set; }

        /// <summary>
        /// Gets the machine repository.
        /// </summary>
        public IMachineRepository MachineRepository { get; private set; }

        /// <summary>
        /// Gets the machines classification repository.
        /// </summary>
        public IMachinesClassificationRepository MachinesClassificationRepository { get; private set; }

        /// <summary>
        /// Gets the manufacturer repository.
        /// </summary>
        public IManufacturerRepository ManufacturerRepository { get; private set; }

        /// <summary>
        /// Gets the materials classification repository.
        /// </summary>
        public IMaterialsClassificationRepository MaterialsClassificationRepository { get; private set; }

        /// <summary>
        /// Gets the measurement unit repository.
        /// </summary>
        public IMeasurementUnitRepository MeasurementUnitRepository { get; private set; }

        /// <summary>
        /// Gets the media repository.
        /// </summary>
        public IMediaRepository MediaRepository { get; private set; }

        /// <summary>
        /// Gets the overhead settings repository.
        /// </summary>
        public IOverheadSettingsRepository OverheadSettingsRepository { get; private set; }

        /// <summary>
        /// Gets the part repository.
        /// </summary>
        public IPartRepository PartRepository { get; private set; }

        /// <summary>
        /// Gets the passwords history repository.
        /// </summary>
        public IPasswordsHistoryRepository PasswordsHistoryRepository { get; private set; }

        /// <summary>
        /// Gets the process repository.
        /// </summary>
        public IProcessRepository ProcessRepository { get; private set; }

        /// <summary>
        /// Gets the process step repository.
        /// </summary>
        public IProcessStepRepository ProcessStepRepository { get; private set; }

        /// <summary>
        /// Gets the process steps classification repository.
        /// </summary>
        public IProcessStepsClassificationRepository ProcessStepsClassificationRepository { get; private set; }

        /// <summary>
        /// Gets the project folder repository.
        /// </summary>
        public IProjectFolderRepository ProjectFolderRepository { get; private set; }

        /// <summary>
        /// Gets the project repository.
        /// </summary>
        public IProjectRepository ProjectRepository { get; private set; }

        /// <summary>
        /// Gets the raw material repository.
        /// </summary>
        public IRawMaterialRepository RawMaterialRepository { get; private set; }

        /// <summary>
        /// Gets the raw material price history repository.
        /// </summary>
        public IRawMaterialPriceHistoryRepository RawMaterialPriceHistoryRepository { get; private set; }

        /// <summary>
        /// Gets the raw material delivery type repository.
        /// </summary>
        public IRawMaterialDeliveryTypeRepository RawMaterialDeliveryTypeRepository { get; private set; }

        /// <summary>
        /// Gets the supplier repository.
        /// </summary>
        public ISupplierRepository SupplierRepository { get; private set; }

        /// <summary>
        /// Gets the transport cost calculation repository.
        /// </summary>
        public ITransportCostCalculationRepository TransportCostCalculationRepository { get; private set; }

        /// <summary>
        /// Gets the transport cost calculation setting repository.
        /// </summary>
        public ITransportCostCalculationSettingRepository TransportCostCalculationSettingRepository { get; private set; }

        /// <summary>
        /// Gets the trash bin repository.
        /// </summary>
        public ITrashBinRepository TrashBinRepository { get; private set; }

        /// <summary>
        /// Gets the user repository.
        /// </summary>
        public IUserRepository UserRepository { get; private set; }

        /// <summary>
        /// Gets the bookmark repository.
        /// </summary>
        public IBookmarkRepository BookmarkRepository { get; private set; }

        /// <summary>
        /// Gets the country supplier repository.
        /// </summary>        
        public ICountrySupplierRepository CountrySupplierRepository { get; private set; }

        /// <summary>
        /// Gets the country setting repository.
        /// </summary>        
        public ICountrySettingRepository CountrySettingRepository { get; private set; }

        /// <summary>
        /// Gets the log repository.
        /// </summary>
        public ILogRepository LogRepository { get; private set; }

        #endregion Repositories

        #region Save changes

        /// <summary>
        /// Saves all changes to the database.
        /// </summary>
        /// <exception cref="ConcurrencyException">Thrown when a concurrency issue occurs.</exception>
        /// <exception cref="DataAccessException">Thrown when a database related error occurs.</exception>
        public void SaveChanges()
        {
            SaveChanges(false);
        }

        /// <summary>
        /// Saves all changes to the database.
        /// </summary>
        /// <param name="autoHandleConcurrency">if set to true, any concurrency error is silently handled; otherwise, a <see cref="ConcurrencyException" /> is thrown.</param>
        /// /// <exception cref="ConcurrencyException">
        /// Thrown when a concurrency issue occurs and the <paramref name="autoHandleConcurrency"/> parameter is set to false.
        /// </exception>
        /// <exception cref="DataAccessException">Thrown when a database related error occurs.</exception>
        public void SaveChanges(bool autoHandleConcurrency)
        {
            // Automatic change detection is disabled so we need to trigger it before doing any operation involving the context's entities.
            this.EntityContext.ChangeTracker.DetectChanges();

            // Check if large media entities are about to be saved in order to increase the command timeout (saving tens of Mb of media takes more than the default timeout).
            bool mediaCommit = this.CheckLargeMediaForSave();
            int? oldTimeout = this.EntityContext.CommandTimeout;

            try
            {
                if (mediaCommit)
                {
                    this.EntityContext.CommandTimeout = CommandTimeoutForMedia;
                }

                this.EntityContext.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                log.WarnException("A concurrency exception has occurred while saving the context changes.", ex);
                throw this.CreateConcurrencyException(autoHandleConcurrency, ex);
            }
            catch (DataException ex)
            {
                log.ErrorException("DataException occurred while committing context changes.", ex);

                // The exception must be parsed before the Rollback call, before the entity information contained in the exception is rolled back.
                var newEx = Utils.ParseDataException(ex, this.DatabaseId);
                this.Rollback();

                throw newEx;
            }
            catch
            {
                this.Rollback();
                throw;
            }
            finally
            {
                if (mediaCommit)
                {
                    this.EntityContext.CommandTimeout = oldTimeout;
                }

                // Detach all media objects from the context to save memory.
                foreach (var localMedia in this.EntityContext.MediaSet.Local.ToList())
                {
                    this.EntityContext.Detach(localMedia);
                }

                var handler = this.ChangesSaved;
                if (handler != null)
                {
                    handler(this, EventArgs.Empty);
                }
            }
        }

        /// <summary>
        /// In case of errors, remove all changes performed in the context.
        /// </summary>
        internal void Rollback()
        {
            // Revert all changes of the context in order to eliminate the cause of the data error(s).
            this.EntityContext.RevertChanges();
        }

        /// <summary>
        /// Checks if the there is any media file (video or document) which will be added/modified in the database.
        /// </summary>
        /// <returns>True if a large media is added or modified in the object context.</returns>
        private bool CheckLargeMediaForSave()
        {
            var mediaObjects = from media in this.EntityContext.MediaSet.Local
                               let state = this.EntityContext.Entry(media).State
                               where state == EntityState.Added || state == EntityState.Modified
                               select media;

            foreach (Media media in mediaObjects)
            {
                MediaType type = (MediaType)media.Type;
                if (type == MediaType.Document || type == MediaType.Video)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Creates a <see cref="ConcurrencyException"/> instance based on the specified <see cref="DbUpdateConcurrencyException"/>
        /// and sets the appropriate error code.
        /// </summary>
        /// <param name="autoHandleConcurrency">if set to true the concurrency issue is automatically fixed; otherwise it specifies that it will be handled by the client code, later..</param>
        /// <param name="ex">The concurrency exception.</param>
        /// <returns>
        /// A new instance of <see cref="ConcurrencyException"/>.
        /// </returns>
        private ConcurrencyException CreateConcurrencyException(bool autoHandleConcurrency, DbUpdateConcurrencyException ex)
        {
            ConcurrencyException concurrencyException = new ConcurrencyException(ex, autoHandleConcurrency, this);
            concurrencyException.ErrorCode = DatabaseErrorCode.OptimisticConcurrency;
            return concurrencyException;
        }

        #endregion Save changes

        #region Public Methods

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            DataEntities context = this.EntityContext;
            if (context != null)
            {
                context.Dispose();
            }
        }

        /// <summary>
        /// Reverts all entity related changes tracked by the context.
        /// All added entities are deleted and all modified or deleted entities are restored to their original state.        
        /// </summary>
        public void RevertChanges()
        {
            this.EntityContext.RevertChanges();
        }

        /// <summary>
        /// Counts the number of entities that are not synchronized.
        /// </summary>
        /// <param name="userId">The Id of the user whose un-synched items to count.</param>
        /// <returns>
        /// The number of entities are not synchronized.
        /// </returns>
        public int CountUnSynchedItems(Guid userId)
        {
            try
            {
                ObjectParameter count = new ObjectParameter("unSynchedCount", typeof(int));
                this.EntityContext.CountUnSynchedItems(userId, count);

                return (int)count.Value;
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.DatabaseId);
            }
        }

        /// <summary>
        /// Gets the ids of the entities having a specified type that exist in a project in which the specified user is Responsible Calculator or Project Leader.
        /// </summary>
        /// <param name="userId">The id of the user.</param>
        /// <param name="tableName">The name of the database table containing the entities.</param>
        /// <returns>
        /// A collection containing the ids of entities that have the project associated with the specified user.
        /// </returns>
        /// TODO: tableName should be replaced by entity type
        public Collection<Guid> GetOtherUsersEntitiesId(Guid userId, string tableName)
        {
            try
            {
                var result = new Collection<Guid>();

                var user = this.UserRepository.GetById(userId, true);
                if (user != null)
                {
                    var isAdmin = RoleRights.HasRole(user.Roles, Role.Admin);
                    result.AddRange(this.EntityContext.OtherUsersEntitiesId(userId, isAdmin, tableName).Where(id => id.HasValue).Select(id => id.Value).ToList());
                }

                return result;
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.DatabaseId);
            }
        }

        /// <summary>
        /// Returns a non-generic repository that offers access to entities of the specified type.
        /// The returned repository has the same underlying data context as the generic repositories of this instance.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <returns>
        /// A repository for the specified entity type.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">The entity type was null.</exception>
        /// <exception cref="InvalidOperationException">The entity type is not part of the data model.</exception>
        public IRepository Repository(Type entityType)
        {
            if (entityType == null)
            {
                throw new ArgumentNullException("entityType", "The entity type was null.");
            }

            // The repositories are cached so we don't create a repository instance for every call to this method.
            IRepository repository;
            if (!this.repositoryCache.TryGetValue(entityType, out repository))
            {
                repository = new Repository(entityType, this.EntityContext, this.DatabaseId);
                this.repositoryCache[entityType] = repository;
            }

            return repository;
        }

        #endregion

        #region Events

        /// <summary>
        /// Handles the SavingChanges event of the EntityContext. Before saving the changes, we have to update 
        /// the LastChangeTimestamp property of Project/Assembly/Part.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void EntityContext_SavingChanges(object sender, EventArgs e)
        {
            var addedEntries = this.EntityContext.ChangeTracker.Entries().Where(entry => entry.State == EntityState.Added);
            var changedEntries = this.EntityContext.ChangeTracker.Entries().Where(entry => entry.State == EntityState.Modified);
            var removedEntries = this.EntityContext.ChangeTracker.Entries().Where(entry => entry.State == EntityState.Deleted);

            foreach (var entry in addedEntries)
            {
                this.HandleLastModifiedDateUpdate(entry.Entity, EntityState.Added);
            }

            foreach (var entry in changedEntries)
            {
                this.HandleLastModifiedDateUpdate(entry.Entity, EntityState.Modified);
            }

            foreach (var entry in removedEntries)
            {
                this.HandleLastModifiedDateUpdate(entry.Entity, EntityState.Deleted);
            }

            // Detect the changes made above, otherwise they will not be saved.
            this.EntityContext.ChangeTracker.DetectChanges();
        }

        #endregion Events

        #region Update Last Modified Date

        /// <summary>
        /// Handles the update of last modified date, according to the entity type.
        /// </summary>
        /// <param name="entity">The entity to update.</param>
        /// <param name="state">The entity state.</param>
        private void HandleLastModifiedDateUpdate(object entity, EntityState state)
        {
            if (entity is Project || entity is Assembly || entity is Part)
            {
                // The projects, assemblies and parts are directly updated.
                this.UpdateLastModifiedDateOfEntity(entity);
            }
            else
            {
                // The other entities are checked to see if their parents must be updated.
                this.CheckEntityParentToUpdateLastModifiedDate(entity, state);
            }
        }

        /// <summary>
        /// Updates the last modified date (LastChangeTimestamp) for a project, assembly or part. 
        /// </summary>
        /// <param name="entity">The entity to update.</param>
        private void UpdateLastModifiedDateOfEntity(object entity)
        {
            Project project = entity as Project;
            if (project != null)
            {
                // The entity to update is a project.
                var entityState = this.EntityContext.Entry(project).State;
                if (entityState != EntityState.Deleted &&
                    (!(entityState == EntityState.Added && project.LastChangeTimestamp != null)))
                {
                    // If entity is created (not with copy/paste or import) or edited or unchanged, set timestamp.
                    project.LastChangeTimestamp = DateTime.Now;
                }
            }

            Assembly assembly = entity as Assembly;
            if (assembly != null)
            {
                // The entity to update is an assembly.
                var entityState = this.EntityContext.Entry(assembly).State;
                if (entityState != EntityState.Deleted &&
                    (!(entityState == EntityState.Added && assembly.LastChangeTimestamp != null)))
                {
                    // If entity is created (not with copy/paste or import) or edited or unchanged, set timestamp.
                    assembly.LastChangeTimestamp = DateTime.Now;
                }

                if (assembly.Project != null)
                {
                    UpdateLastModifiedDateOfEntity(assembly.Project);
                }
                else if (assembly.ParentAssembly != null)
                {
                    UpdateLastModifiedDateOfEntity(assembly.ParentAssembly);
                }
                else
                {
                    // Workaround to update timestamp for parent project when editing the assembly (project reference is not loaded by default).
                    Project parentProject = this.ProjectRepository.GetParentProject(assembly);
                    if (parentProject != null)
                    {
                        UpdateLastModifiedDateOfEntity(parentProject);
                    }
                }
            }

            Part part = entity as Part;
            if (part != null)
            {
                // The entity to update is a part.
                var entityState = this.EntityContext.Entry(part).State;
                if (entityState != EntityState.Deleted &&
                    (!(entityState == EntityState.Added && part.LastChangeTimestamp != null)))
                {
                    // If entity is created (not with copy/paste or import) or edited or unchanged, set timestamp.
                    part.LastChangeTimestamp = DateTime.Now;
                }

                if (part.Project != null)
                {
                    UpdateLastModifiedDateOfEntity(part.Project);
                }
                else if (part.Assembly != null)
                {
                    UpdateLastModifiedDateOfEntity(part.Assembly);
                }
                else if (part.ParentOfRawPart.Count > 0)
                {
                    this.UpdateLastModifiedDateOfEntity(part.ParentOfRawPart.FirstOrDefault());
                }
                else
                {
                    // Workaround to update timestamp for parent project when editing the part (project reference is not loaded by default).
                    Project parentProject = this.ProjectRepository.GetParentProject(part);
                    if (parentProject != null)
                    {
                        UpdateLastModifiedDateOfEntity(parentProject);
                    }
                }
            }
        }

        /// <summary>
        /// Checks if the entity's parent must be updated with a new value for the last modified date.
        /// </summary>
        /// <param name="entity">The entity to check.</param>
        /// <param name="state">The entity state.</param>
        private void CheckEntityParentToUpdateLastModifiedDate(object entity, EntityState state)
        {
            RawMaterial rawMaterial = entity as RawMaterial;
            if (rawMaterial != null &&
                (state == EntityState.Added || state == EntityState.Deleted || rawMaterial.IsDeleted == true) &&
                rawMaterial.Part != null)
            {
                // Update the timestamp of parent part if a raw material is added or deleted.
                UpdateLastModifiedDateOfEntity(rawMaterial.Part);
            }

            Commodity commodity = entity as Commodity;
            if (commodity != null &&
                (state == EntityState.Added || state == EntityState.Deleted || commodity.IsDeleted == true) &&
                commodity.Part != null)
            {
                // Update the timestamp of parent part if a commodity is added or deleted.
                UpdateLastModifiedDateOfEntity(commodity.Part);
            }

            ProcessStep processStep = entity as ProcessStep;
            if (processStep != null &&
                (state == EntityState.Added || state == EntityState.Deleted))
            {
                // The entity to check is an added or deleted process step.
                Process parentProcess = processStep.Process;
                if (parentProcess != null)
                {
                    if (parentProcess.Assemblies != null && parentProcess.Assemblies.Count > 0)
                    {
                        // Update the timestamp of the process's parent assembly.
                        Assembly parentAssembly = parentProcess.Assemblies.FirstOrDefault();
                        UpdateLastModifiedDateOfEntity(parentAssembly);
                    }
                    else if (parentProcess.Parts != null && parentProcess.Parts.Count > 0)
                    {
                        // Update the timestamp of the process's parent part.
                        Part parentPart = parentProcess.Parts.FirstOrDefault();
                        UpdateLastModifiedDateOfEntity(parentPart);
                    }
                }
            }

            Customer customer = entity as Customer;
            if (customer != null && state == EntityState.Modified)
            {
                // The entity to check is a modified customer.
                Project parentProject = customer.Projects.FirstOrDefault();
                if (parentProject != null)
                {
                    // Update the timestamp of the customer's parent project.
                    UpdateLastModifiedDateOfEntity(parentProject);
                }
            }

            OverheadSetting setting = entity as OverheadSetting;
            if (setting != null && state == EntityState.Modified)
            {
                // The entity to check is a modified overhead setting.
                Project parentProject = setting.Projects.FirstOrDefault();
                if (parentProject != null)
                {
                    // Update the timestamp of the overhead setting's parent project.
                    UpdateLastModifiedDateOfEntity(parentProject);
                }
            }
        }

        #endregion Update Last Modified Date

        /// <summary>
        /// Creates a new <see cref="DataEntities" /> instance based on a specified database identifier.
        /// </summary>
        /// <param name="databaseId">The database identifier.</param>
        /// <returns>
        /// A new instance of <see cref="DataEntities" />
        /// </returns>
        /// <exception cref="System.ArgumentException">The provided database id is not supported.</exception>
        private DataEntities CreateEntityContext(DbIdentifier databaseId)
        {
            DataEntities dbContext = null;
            if (databaseId == DbIdentifier.CentralDatabase)
            {
                dbContext = new DataEntities(ConnectionConfiguration.CentralDbEntityConnectionString);
            }
            else if (databaseId == DbIdentifier.LocalDatabase)
            {
                dbContext = new DataEntities(ConnectionConfiguration.LocalDbEntityConnectionString);
            }
            else
            {
                throw new ArgumentException("The provided database id is not supported.", "databaseId");
            }

            // Automatic change detection is disabled because of poor performance when the DbContext has many objects (thousands).
            // The poor performance is the cost of snapshot change tracking and there is nothing to do to improve it.
            dbContext.Configuration.AutoDetectChangesEnabled = false;

            // Disable proxies because neither lazy loading not proxy-based change tracking are enabled.
            dbContext.Configuration.ProxyCreationEnabled = false;

            return dbContext;
        }
    }
}
