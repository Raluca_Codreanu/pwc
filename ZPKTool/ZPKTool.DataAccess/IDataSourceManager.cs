﻿namespace ZPKTool.DataAccess
{
    using System;
    using System.Collections.ObjectModel;
    using System.Data;
    using ZPKTool.Data;

    /// <summary>
    /// Defines the interface for a class that implements the Unit of Work pattern for database operations.
    /// <para/>
    /// All objects obtained using the same instance of this class share the same Object Context (so the change tracking is working) and they will all be
    /// saved when the SaveChanges() method is called.
    /// <para/>
    /// Use an instance for each form/screen or for each set of related forms/screens when the save operation is performed only once for all of them.
    /// If possible, dispose of the instance when is no longer needed (not necessary, but a good practice in order to reclaim some memory).
    /// </summary>
    public interface IDataSourceManager : IDisposable
    {
        /// <summary>
        /// Occurs after the changes in this instance have been saved.
        /// </summary>
        event EventHandler ChangesSaved;

        /// <summary>
        /// Gets the unique identifier of this instance.
        /// </summary>
        Guid Uid { get; }

        /// <summary>
        /// Gets a value indicating the database on which this instance operates.
        /// </summary>
        DbIdentifier DatabaseId { get; }

        /// <summary>
        /// Gets the Entity Framework DbContext used to execute the database operations.
        /// </summary>        
        DataEntities EntityContext { get; }

        #region Repository properties

        /// <summary>
        /// Gets the assembly repository.
        /// </summary>
        IAssemblyRepository AssemblyRepository { get; }

        /// <summary>
        /// Gets the basic settings repository.
        /// </summary>
        IBasicSettingsRepository BasicSettingsRepository { get; }

        /// <summary>
        /// Gets the country settings value history repository.
        /// </summary>
        ICountrySettingsValueHistoryRepository CountrySettingsValueHistoryRepository { get; }

        /// <summary>
        /// Gets the commodity repository.
        /// </summary>
        ICommodityRepository CommodityRepository { get; }

        /// <summary>
        /// Gets the commodity price history repository.
        /// </summary>
        ICommodityPriceHistoryRepository CommodityPriceHistoryRepository { get; }

        /// <summary>
        /// Gets the consumable repository.
        /// </summary>
        IConsumableRepository ConsumableRepository { get; }

        /// <summary>
        /// Gets the consumable price history repository.
        /// </summary>
        IConsumablePriceHistoryRepository ConsumablePriceHistoryRepository { get; }

        /// <summary>
        /// Gets the country repository.
        /// </summary>
        ICountryRepository CountryRepository { get; }

        /// <summary>
        /// Gets the currency repository.
        /// </summary>
        ICurrencyRepository CurrencyRepository { get; }

        /// <summary>
        /// Gets the cycle time calculation repository.
        /// </summary>
        ICycleTimeCalculationRepository CycleTimeCalculationRepository { get; }

        /// <summary>
        /// Gets the data base info repository.
        /// </summary>
        IGlobalSettingsRepository GlobalSettingsRepository { get; }

        /// <summary>
        /// Gets the die repository.
        /// </summary>
        IDieRepository DieRepository { get; }

        /// <summary>
        /// Gets the machine repository.
        /// </summary>
        IMachineRepository MachineRepository { get; }

        /// <summary>
        /// Gets the machines classification repository.
        /// </summary>
        IMachinesClassificationRepository MachinesClassificationRepository { get; }

        /// <summary>
        /// Gets the manufacturer repository.
        /// </summary>
        IManufacturerRepository ManufacturerRepository { get; }

        /// <summary>
        /// Gets the materials classification repository.
        /// </summary>
        IMaterialsClassificationRepository MaterialsClassificationRepository { get; }

        /// <summary>
        /// Gets the measurement unit repository.
        /// </summary>
        IMeasurementUnitRepository MeasurementUnitRepository { get; }

        /// <summary>
        /// Gets the media repository.
        /// </summary>
        IMediaRepository MediaRepository { get; }

        /// <summary>
        /// Gets the overhead settings repository.
        /// </summary>
        IOverheadSettingsRepository OverheadSettingsRepository { get; }

        /// <summary>
        /// Gets the part repository.
        /// </summary>
        IPartRepository PartRepository { get; }

        /// <summary>
        /// Gets the passwords history repository.
        /// </summary>
        IPasswordsHistoryRepository PasswordsHistoryRepository { get; }

        /// <summary>
        /// Gets the process repository.
        /// </summary>
        IProcessRepository ProcessRepository { get; }

        /// <summary>
        /// Gets the process step repository.
        /// </summary>
        IProcessStepRepository ProcessStepRepository { get; }

        /// <summary>
        /// Gets the process steps classification repository.
        /// </summary>
        IProcessStepsClassificationRepository ProcessStepsClassificationRepository { get; }

        /// <summary>
        /// Gets the project folder repository.
        /// </summary>
        IProjectFolderRepository ProjectFolderRepository { get; }

        /// <summary>
        /// Gets the project repository.
        /// </summary>
        IProjectRepository ProjectRepository { get; }

        /// <summary>
        /// Gets the raw material repository.
        /// </summary>
        IRawMaterialRepository RawMaterialRepository { get; }

        /// <summary>
        /// Gets the raw material price history repository.
        /// </summary>
        IRawMaterialPriceHistoryRepository RawMaterialPriceHistoryRepository { get; }

        /// <summary>
        /// Gets the raw material delivery type repository.
        /// </summary>
        IRawMaterialDeliveryTypeRepository RawMaterialDeliveryTypeRepository { get; }

        /// <summary>
        /// Gets the supplier repository.
        /// </summary>
        ISupplierRepository SupplierRepository { get; }

        /// <summary>
        /// Gets the transport cost calculation repository.
        /// </summary>
        ITransportCostCalculationRepository TransportCostCalculationRepository { get; }

        /// <summary>
        /// Gets the transport cost calculation setting repository.
        /// </summary>
        ITransportCostCalculationSettingRepository TransportCostCalculationSettingRepository { get; }

        /// <summary>
        /// Gets the trash bin repository.
        /// </summary>
        ITrashBinRepository TrashBinRepository { get; }

        /// <summary>
        /// Gets the user repository.
        /// </summary>
        IUserRepository UserRepository { get; }

        /// <summary>
        /// Gets the bookmark repository.
        /// </summary>
        IBookmarkRepository BookmarkRepository { get; }

        /// <summary>
        /// Gets the country supplier repository.
        /// </summary>        
        ICountrySupplierRepository CountrySupplierRepository { get; }

        /// <summary>
        /// Gets the country setting repository.
        /// </summary>
        ICountrySettingRepository CountrySettingRepository { get; }

        /// <summary>
        /// Gets the log repository.
        /// </summary>
        ILogRepository LogRepository { get; }

        #endregion Repository properties

        /// <summary>
        /// Saves all changes to the database.
        /// </summary>
        /// <exception cref="ConcurrencyException">Thrown when a concurrency issue occurs.</exception>
        /// <exception cref="DataAccessException">Thrown when a database related error occurs.</exception>
        void SaveChanges();

        /// <summary>
        /// Saves all changes to the database.
        /// </summary>
        /// <param name="autoHandleConcurrency">if set to true, any concurrency error is silently handled; otherwise, a <see cref="ConcurrencyException"/> is thrown.</param>
        /// <exception cref="ConcurrencyException">
        /// Thrown when a concurrency issue occurs and the <paramref name="autoHandleConcurrency"/> parameter is set to false.
        /// </exception>
        /// <exception cref="DataAccessException">Thrown when a database related error occurs.</exception>
        void SaveChanges(bool autoHandleConcurrency);

        /// <summary>
        /// Returns a non-generic repository that offers access to entities of the specified type.
        /// The returned repository has the same underlying data context as the generic repositories of this instance.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <returns>A repository for the specified entity type.</returns>
        /// <exception cref="System.ArgumentNullException">The entity type was null.</exception>
        /// <exception cref="InvalidOperationException">The entity type is not part of the data model.</exception>
        IRepository Repository(Type entityType);

        /// <summary>
        /// Counts the number of entities that are not synchronized.
        /// </summary>
        /// <param name="userId">The Id of the user whose un-synched items to count.</param>
        /// <returns>
        /// The number of entities are not synchronized.
        /// </returns>
        int CountUnSynchedItems(Guid userId);

        /// <summary>
        /// Gets the ids of the entities having a specified type that exist in a project in which the specified user is Responsible Calculator or Project Leader.
        /// </summary>
        /// <param name="userId">The id of the user.</param>
        /// <param name="tableName">The name of the database table containing the entities.</param>
        /// <returns>
        /// A collection containing the ids of entities that have the project associated with the specified user.
        /// </returns>        
        Collection<Guid> GetOtherUsersEntitiesId(Guid userId, string tableName);

        /// <summary>
        /// Reverts all changes made to entities that were obtained using this instance (and tracked by it).
        /// All added entities are deleted and all modified entities are restored to their original state.
        /// The deleted entities are restored to their state prior to their deletion (their original values are lost when deleted). 
        /// </summary>
        void RevertChanges();
    }
}
