﻿namespace ZPKTool.DataAccess
{
    /// <summary>
    /// provides a way to specify one of the databases used by the application.
    /// </summary>
    public enum DbIdentifier
    {
        /// <summary>
        /// The entity's source or target database is not set. This is the default value.
        /// </summary>
        NotSet = 0,

        /// <summary>
        /// The entity's source or target database is the Local Database.
        /// </summary>
        LocalDatabase = 1,

        /// <summary>
        /// The entity's source or target database is the Central Database.
        /// </summary>
        CentralDatabase = 2
    }
}
