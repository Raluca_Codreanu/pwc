﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ZPKTool.Common;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Represents the error that occurs when the local and central databases do not have the same version.
    /// </summary>
    [Serializable]
    public class DatabaseVersionMismatchException : ZPKException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseVersionMismatchException"/> class.
        /// </summary>
        public DatabaseVersionMismatchException()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseVersionMismatchException"/> class.
        /// </summary>
        /// <param name="errorCode">The error code.</param>
        public DatabaseVersionMismatchException(string errorCode)
            : base(errorCode)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseVersionMismatchException"/> class.
        /// </summary>
        /// <param name="errorCode">The error code.</param>
        /// <param name="ex">The ex.</param>
        public DatabaseVersionMismatchException(string errorCode, Exception ex)
            : base(errorCode, ex)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseVersionMismatchException"/> class.
        /// </summary>
        /// <param name="errorCode">The error code.</param>
        /// <param name="errorMessage">The error message.</param>
        /// <param name="ex">The ex.</param>
        public DatabaseVersionMismatchException(string errorCode, string errorMessage, Exception ex)
            : base(errorCode, errorMessage, ex)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseVersionMismatchException"/> class.
        /// </summary>
        /// <param name="errorCode">The error code.</param>
        /// <param name="errorMessage">The error message.</param>
        public DatabaseVersionMismatchException(string errorCode, string errorMessage)
            : base(errorCode, errorMessage)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseVersionMismatchException"/> class.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the exception being thrown.</param>
        /// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the source or destination.</param>
        protected DatabaseVersionMismatchException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
