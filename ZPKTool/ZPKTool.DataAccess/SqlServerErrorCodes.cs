﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Contains the SQL server error codes handled by the application.
    /// </summary>
    public static class SqlServerErrorCodes
    {
        /// <summary>
        /// The code for "Timeout expired violation" error.
        /// </summary>
        public const int TimeoutExpired = -2;

        /// <summary>
        /// The code for "constraint violation" error.
        /// </summary>
        public const int ConstraintViolation = 2627;

        /// <summary>
        /// The code for "foreign key violation" error.
        /// </summary>
        public const int ForeignKeyViolation = 547;

        /// <summary>
        /// The error code for invalid username or password.
        /// </summary>
        public const int InvalidUserOrPassword = 18456;

        /// <summary>
        /// The error code for login failure due to a disabled account.
        /// </summary>
        public const int AccountDisabled = 18470;

        /// <summary>
        /// The error code for deadlock.
        /// </summary>
        public const int Deadlock = 1205;

        /// <summary>
        /// A duplicate ISO code was detected when inserting or updating a master data currency.
        /// The ISO code must be unique among the master data currencies.
        /// </summary>
        public const int DuplicateCurrencyIsoCode = 493710000;

        /// <summary>
        /// Invalid database schema.
        /// </summary>
        [SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes", Justification = "The field is immutable because it does not allow to change its values.")]
        public static readonly ReadOnlyCollection<int> InvalidSchema = new ReadOnlyCollection<int>(new List<int>()
        {
            // Invalid column name.
            207,

            // Invalid object name.
            208,

            // Invalid cast exception when pulling data type schema from db.
            241
        });

        /// <summary>
        /// Error codes representing that a connection to the database is not available (is down).
        /// </summary>
        [SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes", Justification = "The field is immutable because it does not allow to change its values.")]
        public static readonly ReadOnlyCollection<int> NoDbConnectionCodes = new ReadOnlyCollection<int>(new List<int>()
        {            
            // This codes appears when the SQL server uses named pipes to connect to it.        
            2,
            
            // Appears when a database can't be opened for various reasons.
            4060,
                
            // This codes appears when connection to the SQL server through shared memory; this is the default way of connecting to local servers.        
            233,
        
            // One of the error codes for failure to connect to the database. 
            10061,
        
            // One of the error codes for failure to connect to the database.         
            1326,
        
            // Login failed. The login is from an un-trusted domain and cannot be used with Windows authentication.
            18452,

            // SQL Server service has been paused. No new connections will be allowed. To resume the service,
            // use SQL Computer Manager or the Services application in Control Panel. 
            17142,

            // A network-related or instance-specific error occurred while establishing a connection to SQL Server. 
            // The server was not found or was not accessible. Verify that the instance name is correct and that SQL Server is configured to allow remote connections.
            -1
        });
    }
}