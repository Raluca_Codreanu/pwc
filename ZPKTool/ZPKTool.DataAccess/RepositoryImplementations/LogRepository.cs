﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// The repository implementation for the <see cref="Logs"/> entity.
    /// </summary>
    internal class LogRepository : Repository<Log>, ILogRepository
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Initializes a new instance of the <see cref="LogRepository"/> class.
        /// </summary>
        /// <param name="entityContext">The object context to be used for data operations.</param>
        /// <param name="sourceDatabase">A value indicating the database on which this repository operates.</param>
        public LogRepository(DataEntities entityContext, DbIdentifier sourceDatabase)
            : base(entityContext, sourceDatabase)
        {
        }

        /// <summary>
        /// Gets all logs.
        /// </summary>
        /// <returns>A list of logs.</returns>
        public IEnumerable<Log> GetAllLogs()
        {
            var query = from logs in this.EntityContext.LogSet
                            .Include(l => l.User)
                        select logs;

            try
            {
                return query.ToList();
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the logs based on an interval.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns>A list of logs.</returns>
        public IEnumerable<Log> GetLogs(DateTime startDate, DateTime endDate)
        {
            var query = from logs in this.EntityContext.LogSet
                        .Include(l => l.User)
                        where (startDate.Date <= System.Data.Entity.DbFunctions.TruncateTime(logs.Date) && System.Data.Entity.DbFunctions.TruncateTime(logs.Date) <= endDate.Date)
                        select logs;
            try
            {
                return query.ToList();
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }
    }
}
