﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using ZPKTool.Data;
using ZPKTool.DataAccess;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Implements operations for PasswordsHistory.
    /// </summary>
    internal class PasswordsHistoryRepository : Repository<PasswordsHistory>, IPasswordsHistoryRepository
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Initializes a new instance of the <see cref="PasswordsHistoryRepository"/> class.
        /// </summary>
        /// <param name="entityContext">The object context to be used for data operations.</param>
        /// <param name="sourceDatabase">A value indicating the database on wich this repository operates.</param>
        public PasswordsHistoryRepository(DataEntities entityContext, DbIdentifier sourceDatabase)
            : base(entityContext, sourceDatabase)
        {
        }

        /// <summary>
        /// Gets the passwords history and the last saved setting coresponding to a specified user.
        /// </summary>
        /// <param name="userID">The user id.</param>
        /// <returns>A list of passwords.</returns>
        public IEnumerable<PasswordsHistory> GetPasswordsHistory(Guid userID)
        {
            try
            {
                var query = from item in this.EntityContext.PasswordsHistoriesSet
                            where item.User.Guid == userID
                            select item;

                return query.OrderBy(p => p.Date).ToList();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }
    }
}
