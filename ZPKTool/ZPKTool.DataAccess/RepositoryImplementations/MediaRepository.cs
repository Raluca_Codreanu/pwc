﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Entity;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Implements operations on Media.
    /// </summary>
    internal class MediaRepository : Repository<Media>, IMediaRepository
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Initializes a new instance of the <see cref="MediaRepository"/> class.
        /// </summary>
        /// <param name="entityContext">The object context to be used for data operations.</param>
        /// <param name="sourceDatabase">A value indicating the database on which this repository operates.</param>
        public MediaRepository(DataEntities entityContext, DbIdentifier sourceDatabase)
            : base(entityContext, sourceDatabase)
        {
        }

        /// <summary>
        /// Gets the meta data for the media having one of the specified types and belonging to the specified entity.
        /// </summary>
        /// <param name="entity">The entity whose media meta data to search.</param>
        /// <param name="types">The types of media to be searched.</param>
        /// <returns>
        /// The meta data of the media belonging to the entity and having one of the specified types.
        /// </returns>
        public Collection<MediaMetaData> GetMediaMetaData(object entity, IEnumerable<MediaType> types)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "The entity was null.");
            }

            if (types == null)
            {
                throw new ArgumentNullException("types", "The types list was null.");
            }

            try
            {
                // Filter by media type
                var mediaTypeQuery = this.EntityContext.MediaSet.Where(Utils.BuildContainsExpression<Media, MediaType>(m => m.Type, types));

                // Filter by entity
                var mediaQuery = mediaTypeQuery.Where(BuildWherePredicate(entity));

                var metaDataQuery = from media in mediaQuery
                                    select new MediaMetaData()
                                    {
                                        Guid = media.Guid,
                                        IsMasterData = media.IsMasterData,
                                        OriginalFileName = media.OriginalFileName,
                                        Size = media.Size,
                                        Type = media.Type
                                    };

                List<MediaMetaData> metaDataList = metaDataQuery.AsNoTracking().ToList();

                // Remove from the results the metadata for media that has been deleted but not saved into the data source yet.
                var deletedMedia = this.GetLocalEntities(EntityState.Deleted);
                foreach (Media media in deletedMedia)
                {
                    MediaMetaData meta = metaDataList.FirstOrDefault(m => m.Guid == media.Guid);
                    if (meta != null)
                    {
                        metaDataList.Remove(meta);
                    }
                }

                return new Collection<MediaMetaData>(metaDataList);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the meta data for all media of the specified entities.
        /// </summary>
        /// <param name="entities">The entities.</param>
        /// <returns>
        /// The media meta data.
        /// </returns>
        [SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Justification = "Complexity can not be reduced. Understanding the code will become more difficult if the method is split.")]
        public Collection<MediaMetaData> GetMediaMetaData(IEnumerable<object> entities)
        {
            if (entities == null)
            {
                throw new ArgumentNullException("entities", "The entities list was null.");
            }

            if (entities.Count() == 0)
            {
                return new Collection<MediaMetaData>();
            }

            DataEntities context = this.EntityContext;
            IQueryable<MediaMetaData> mainQuery = null;

            // Split the entities in groups, by type, and create a media selection query for each group.
            // The main query will be the union of all these "group" queries.
            var entitiesByType = entities.GroupBy(o => o.GetType());
            foreach (var group in entitiesByType)
            {
                Type entityType = group.Key;
                IQueryable<MediaMetaData> groupQuery = null;

                if (entityType == typeof(Project))
                {
                    var ids = group.Cast<Project>().Select(p => p.Guid);
                    groupQuery = from media in context.MediaSet
                                 where ids.Contains(media.Project.Guid)
                                 select new MediaMetaData()
                                 {
                                     Guid = media.Guid,
                                     IsMasterData = media.IsMasterData,
                                     OriginalFileName = media.OriginalFileName,
                                     Size = media.Size,
                                     Type = media.Type,
                                     ParentId = media.Project.Guid
                                 };
                }
                else if (entityType == typeof(Assembly))
                {
                    var ids = group.Cast<Assembly>().Select(a => a.Guid);
                    groupQuery = from media in context.MediaSet
                                 where ids.Contains(media.Assembly.Guid)
                                 select new MediaMetaData()
                                 {
                                     Guid = media.Guid,
                                     IsMasterData = media.IsMasterData,
                                     OriginalFileName = media.OriginalFileName,
                                     Size = media.Size,
                                     Type = media.Type,
                                     ParentId = media.Assembly.Guid
                                 };
                }
                else if (entityType == typeof(Part) || entityType.IsSubclassOf(typeof(Part)))
                {
                    var ids = group.Cast<Part>().Select(a => a.Guid);
                    groupQuery = from media in context.MediaSet
                                 where ids.Contains(media.Part.Guid)
                                 select new MediaMetaData()
                                 {
                                     Guid = media.Guid,
                                     IsMasterData = media.IsMasterData,
                                     OriginalFileName = media.OriginalFileName,
                                     Size = media.Size,
                                     Type = media.Type,
                                     ParentId = media.Part.Guid
                                 };
                }
                else if (entityType == typeof(Commodity))
                {
                    var ids = group.Cast<Commodity>().Select(e => e.Guid);
                    groupQuery = from media in context.MediaSet
                                 join commodity in context.CommoditySet on media equals commodity.Media
                                 where ids.Contains(commodity.Guid)
                                 select new MediaMetaData()
                                 {
                                     Guid = media.Guid,
                                     IsMasterData = media.IsMasterData,
                                     OriginalFileName = media.OriginalFileName,
                                     Size = media.Size,
                                     Type = media.Type,
                                     ParentId = commodity.Guid
                                 };
                }
                else if (entityType == typeof(Machine))
                {
                    var ids = group.Cast<Machine>().Select(e => e.Guid);
                    groupQuery = from media in context.MediaSet
                                 join machine in context.MachineSet on media equals machine.Media
                                 where ids.Contains(machine.Guid)
                                 select new MediaMetaData()
                                 {
                                     Guid = media.Guid,
                                     IsMasterData = media.IsMasterData,
                                     OriginalFileName = media.OriginalFileName,
                                     Size = media.Size,
                                     Type = media.Type,
                                     ParentId = machine.Guid
                                 };
                }
                else if (entityType == typeof(ProcessStep) || entityType.IsSubclassOf(typeof(ProcessStep)))
                {
                    var ids = group.Cast<ProcessStep>().Select(e => e.Guid);
                    groupQuery = from media in context.MediaSet
                                 join step in context.ProcessStepSet on media equals step.Picture
                                 where ids.Contains(step.Guid)
                                 select new MediaMetaData()
                                 {
                                     Guid = media.Guid,
                                     IsMasterData = media.IsMasterData,
                                     OriginalFileName = media.OriginalFileName,
                                     Size = media.Size,
                                     Type = media.Type,
                                     ParentId = step.Guid
                                 };
                }
                else if (entityType == typeof(RawMaterial))
                {
                    var ids = group.Cast<RawMaterial>().Select(e => e.Guid);
                    groupQuery = from media in context.MediaSet
                                 join material in context.RawMaterialSet on media equals material.Media
                                 where ids.Contains(material.Guid)
                                 select new MediaMetaData()
                                 {
                                     Guid = media.Guid,
                                     IsMasterData = media.IsMasterData,
                                     OriginalFileName = media.OriginalFileName,
                                     Size = media.Size,
                                     Type = media.Type,
                                     ParentId = material.Guid
                                 };
                }
                else if (entityType == typeof(Die))
                {
                    var ids = group.Cast<Die>().Select(e => e.Guid);
                    groupQuery = from media in context.MediaSet
                                 join die in context.DieSet on media equals die.Media
                                 where ids.Contains(die.Guid)
                                 select new MediaMetaData()
                                 {
                                     Guid = media.Guid,
                                     IsMasterData = media.IsMasterData,
                                     OriginalFileName = media.OriginalFileName,
                                     Size = media.Size,
                                     Type = media.Type,
                                     ParentId = die.Guid
                                 };
                }
                else
                {
                    throw new InvalidOperationException(string.Format("The entity type {0} doesn't support Media.", entityType.FullName));
                }

                // Make a union between the current media query and the query generated for the current group.
                mainQuery = mainQuery == null ? groupQuery : mainQuery.Union(groupQuery);
            }

            try
            {
                List<MediaMetaData> metaDataList = mainQuery.AsNoTracking().ToList();

                // Remove from the results the metadata for media that has been deleted but not saved into the data source yet.
                var deletedMedia = this.GetLocalEntities(EntityState.Deleted);
                metaDataList.RemoveAll(meta => deletedMedia.Any(media => meta.Guid == media.Guid));

                return new Collection<MediaMetaData>(metaDataList);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data access error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the media with the specified id. The result is attached to the data context.
        /// </summary>
        /// <param name="mediaId">The media id.</param>
        /// <returns>
        /// The media or null if not found.
        /// </returns>
        public Media GetById(Guid mediaId)
        {
            var query = from media in this.EntityContext.MediaSet
                                .Include(m => m.Owner)
                        where media.Guid == mediaId
                        select media;

            try
            {
                return query.FirstOrDefault();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Returns the media object specified by the meta data info.
        /// The media is detached from the context.
        /// </summary>
        /// <param name="metaData">Media meta data information</param>
        /// <param name="noTracking">Indicates whether change tracking is enabled for the returned object.</param>
        /// <returns>
        /// One media object.
        /// </returns>
        public Media GetMedia(MediaMetaData metaData, bool noTracking = false)
        {
            if (metaData == null)
            {
                throw new ArgumentNullException("metaData", "The metadata was null.");
            }

            var query = from media in this.EntityContext.MediaSet
                                .Include(m => m.Owner)
                        where media.Guid == metaData.Guid
                        select media;

            if (noTracking)
            {
                query = query.AsNoTracking();
            }

            try
            {
                return query.FirstOrDefault();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the media objects identified by the metadata list.
        /// The returned media objects are detached from the data context.
        /// </summary>
        /// <param name="metadata">The metadata of the media to get.</param>
        /// <returns>
        /// A collection containing the specified media.
        /// </returns>
        public Collection<Media> GetMedia(IEnumerable<MediaMetaData> metadata)
        {
            if (metadata == null)
            {
                throw new ArgumentNullException("metadata", "The metadata was null.");
            }

            if (!metadata.Any())
            {
                return new Collection<Media>();
            }

            var mediaIds = metadata.Select(m => m.Guid);
            var query = from media in this.EntityContext.MediaSet
                        where mediaIds.Contains(media.Guid)
                        select media;

            try
            {
                var media = query.AsNoTracking().ToList();
                return new Collection<Media>(media);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets all media belonging to the specified entity.
        /// </summary>
        /// <param name="entity">The entity whose media to get.</param>
        /// <returns>
        /// The media objects belonging to the specified entity.
        /// </returns>
        public Collection<Media> GetMedia(object entity)
        {
            var allMediaTypes = Enum.GetValues(typeof(MediaType)).Cast<MediaType>().ToList();
            return this.GetMedia(entity, allMediaTypes);
        }

        /// <summary>
        /// Gets all media having one of the specified types and belonging to the specified entity.
        /// </summary>
        /// <param name="entity">The entity whose media to get.</param>
        /// <param name="types">The types of media to get.</param>
        /// <returns>
        /// The media objects belonging to the specified entity.
        /// </returns>
        public Collection<Media> GetMedia(object entity, IEnumerable<MediaType> types)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "The entity was null.");
            }

            if (types == null)
            {
                throw new ArgumentNullException("types", "The types list was null.");
            }

            try
            {
                var query = from media in this.EntityContext.MediaSet
                                .Include(m => m.Owner)
                            select media;

                // Filter by media type
                var typeQuery = query.Where(Utils.BuildContainsExpression<Media, MediaType>(m => m.Type, types));

                // Filter by entity
                var mediaQuery = typeQuery.Where(BuildWherePredicate(entity));

                return new Collection<Media>(mediaQuery.AsNoTracking().ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the media for the specified process steps.
        /// </summary>
        /// <param name="steps">The process steps.</param>
        /// <returns>
        /// A dictionary with the key containing the step id and the value being its associated media.
        /// If a step does not have media its id is not present in the dictionary with a null value associated to it.
        /// </returns>
        public Dictionary<Guid, Media> GetMediaForProcessSteps(IEnumerable<ProcessStep> steps)
        {
            if (steps == null)
            {
                throw new ArgumentNullException("steps", "The list of steps was null.");
            }

            if (steps.Count() == 0)
            {
                return new Dictionary<Guid, Media>();
            }

            var sourceSteps = this.EntityContext.ProcessStepSet;
            var sourceMedia = this.EntityContext.MediaSet;

            var stepIds = steps.Select(p => p.Guid).ToList();

            var query = from media in sourceMedia
                        join step in sourceSteps on media equals step.Picture
                        where stepIds.Contains(step.Guid)
                        select new
                        {
                            StepId = step.Guid,
                            Media = media
                        };

            try
            {
                var queryResult = query.AsNoTracking().ToList();

                var resultDictionary = new Dictionary<Guid, Media>();
                foreach (var value in queryResult)
                {
                    resultDictionary[value.StepId] = value.Media;
                }

                return resultDictionary;
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Delete the media specified by its meta data info.
        /// </summary>
        /// <param name="metaData">Media meta data information referring the object to delete</param>
        public void DeleteMedia(MediaMetaData metaData)
        {
            if (metaData == null)
            {
                throw new ArgumentNullException("metaData", "The meta data was null.");
            }

            try
            {
                var query = from m in this.EntityContext.MediaSet
                               .Include(m => m.Owner)
                            where m.Guid == metaData.Guid
                            select m;

                Media media = query.FirstOrDefault();
                if (media != null)
                {
                    this.RemoveAll(media);
                }
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Deletes all media objects of an entity (image/video and documents).
        /// </summary>
        /// <param name="entity">The entity.</param>
        public void DeleteAllMedia(object entity)
        {
            this.DeleteAllMedia(entity, new List<MediaType>() { MediaType.Image, MediaType.Video, MediaType.Document });
        }

        /// <summary>
        /// Deletes all the media of the specified types of an entity.
        /// </summary>
        /// <param name="entity">The entity from which media objects are deleted</param>
        /// <param name="types">The list of media types that to delete.</param>
        public void DeleteAllMedia(object entity, IEnumerable<MediaType> types)
        {
            // If the entity contains media objects in the Added state, those objects must be explicitly deleted because
            // the GetMediaMetaData call will return nothing for them as they are not yet saved in the database.
            List<Media> addedMedia = new List<Media>();
            IEntityWithMediaCollection entityWithMultipleMedia = entity as IEntityWithMediaCollection;
            if (entityWithMultipleMedia != null)
            {
                addedMedia = entityWithMultipleMedia.Media.Where(media => this.EntityContext.Entry(media).State == EntityState.Added).ToList();
            }
            else
            {
                IEntityWithMedia entityWithMedia = entity as IEntityWithMedia;
                if (entityWithMedia != null
                    && entityWithMedia.Media != null
                    && this.EntityContext.Entry(entityWithMedia.Media).State == EntityState.Added)
                {
                    addedMedia.Add(entityWithMedia.Media);
                }
            }

            foreach (Media media in addedMedia)
            {
                this.RemoveAll(media);
            }

            var mediaMetadata = this.GetMediaMetaData(entity, types);
            foreach (MediaMetaData meta in mediaMetadata)
            {
                this.DeleteMedia(meta);
            }
        }

        /// <summary>
        /// Deletes the specified media from the repository using a stored procedure. All sub-entities owned by the media are also deleted.
        /// The actual deletion from the database is performed during this call (when the stored procedure is called), not when SaveChanges() is called.
        /// </summary>
        /// <param name="media">The media to delete.</param>
        public void DeleteByStoredProcedure(Media media)
        {
            if (media == null)
            {
                throw new ArgumentNullException("media", "The media was null.");
            }

            var originalCommandTimeout = this.EntityContext.CommandTimeout;

            try
            {
                this.EntityContext.CommandTimeout = 600;
                this.EntityContext.DeleteMedia(media.Guid);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, SourceDatabase);
            }
            finally
            {
                this.EntityContext.CommandTimeout = originalCommandTimeout;
            }
        }

        #region Helpers

        /// <summary>
        /// Builds a predicate to be used in a 'where' filter for getting all media objects belonging to a specified entity.
        /// </summary>
        /// <param name="entity">The entity whose media to search.</param>
        /// <returns>A predicate.</returns>
        private Expression<Func<Media, bool>> BuildWherePredicate(object entity)
        {
            Expression<Func<Media, bool>> wherePredicate = null;

            if (entity is Project)
            {
                wherePredicate = media => media.Project.Guid == ((Project)entity).Guid;
            }
            else if (entity is ZPKTool.Data.Assembly)
            {
                wherePredicate = media => media.Assembly.Guid == ((ZPKTool.Data.Assembly)entity).Guid;
            }
            else if (entity is Part)
            {
                wherePredicate = media => media.Part.Guid == ((Part)entity).Guid;
            }
            else if (entity is Commodity)
            {
                wherePredicate = media => media.Commodities.Where(c => c.Guid == ((Commodity)entity).Guid).FirstOrDefault() != null;
            }
            else if (entity is Machine)
            {
                wherePredicate = media => media.Machines.Where(m => m.Guid == ((Machine)entity).Guid).FirstOrDefault() != null;
            }
            else if (entity is ProcessStep)
            {
                wherePredicate = media => media.ProcessSteps.Where(s => s.Guid == ((ProcessStep)entity).Guid).FirstOrDefault() != null;
            }
            else if (entity is RawMaterial)
            {
                wherePredicate = media => media.RawMaterials.Where(mat => mat.Guid == ((RawMaterial)entity).Guid).FirstOrDefault() != null;
            }
            else if (entity is Die)
            {
                wherePredicate = media => media.Dies.Where(die => die.Guid == ((Die)entity).Guid).FirstOrDefault() != null;
            }
            else
            {
                throw new InvalidOperationException(string.Format("The entity type {0} doesn't support Media.", entity.GetType().FullName));
            }

            return wherePredicate;
        }

        #endregion Helpers
    }
}
