﻿using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Implements operations for CountrySettings Value history repository.
    /// </summary>
    internal class CountrySettingsValueHistoryRepository : Repository<CountrySettingsValueHistory>, ICountrySettingsValueHistoryRepository
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Initializes a new instance of the <see cref="CountrySettingsValueHistoryRepository" /> class.
        /// </summary>
        /// <param name="entityContext">The object context to be used for data operations.</param>
        /// <param name="sourceDatabase">A value indicating the database on which this repository operates.</param>
        public CountrySettingsValueHistoryRepository(DataEntities entityContext, DbIdentifier sourceDatabase)
            : base(entityContext, sourceDatabase)
        {
        }

        /// <summary>
        /// Retrieves the last modified country setting from value history, based on the country setting guid.
        /// </summary>
        /// <param name="countrySettingGuid">The country setting's guid.</param>
        /// <returns>The last modified country setting from value history.</returns>
        public CountrySettingsValueHistory GetLastModifiedByCountrySettingGuid(Guid countrySettingGuid)
        {
            try
            {
                var query = from setting in this.EntityContext.CountrySettingsValueHistory
                            orderby setting.Timestamp descending
                            where setting.CountrySetting.Guid == countrySettingGuid
                            select setting;

                return query.AsNoTracking().FirstOrDefault();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }
    }
}
