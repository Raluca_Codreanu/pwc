﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Implements operations for ProjectFolder.
    /// </summary>
    internal class ProjectFolderRepository : Repository<ProjectFolder>, IProjectFolderRepository
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectFolderRepository"/> class.
        /// </summary>
        /// <param name="entityContext">The object context to be used for data operations.</param>
        /// <param name="sourceDatabase">A value indicating the database on which this repository operates.</param>
        public ProjectFolderRepository(DataEntities entityContext, DbIdentifier sourceDatabase)
            : base(entityContext, sourceDatabase)
        {
        }

        /// <summary>
        /// Gets the maximum number of root folders to display on the welcome view for a specified user.
        /// A root folder is a folder that does not have a parent folder.
        /// </summary>
        /// <param name="ownerId">The owner's id.</param>
        /// <returns>
        /// A collection of project folders.
        /// </returns>
        public Collection<ProjectFolder> GetRootFolders(Guid ownerId)
        {
            try
            {
                return new Collection<ProjectFolder>(GetRootFolders(ownerId, int.MaxValue).ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the root folders of a specified user. A root folder is a folder that does not have a parent folder.
        /// </summary>
        /// <param name="ownerId">The owner's id.</param>
        /// /// <param name="maxNumberOfFolders">The maximum number of root folders to display.</param>
        /// <returns>
        /// A collection of project folders.
        /// </returns>
        public Collection<ProjectFolder> GetRootFolders(Guid ownerId, int maxNumberOfFolders)
        {
            try
            {
                var query = (from folder in this.EntityContext.ProjectFolderSet
                                 .Include(p => p.Owner)
                             where (folder.Owner.Guid == ownerId && !folder.IsDeleted && folder.ParentProjectFolder == null)
                             select folder).OrderBy(f => f.Name);

                query.SetMergeOption(MergeOption.PreserveChanges);
                return new Collection<ProjectFolder>(query.Take(maxNumberOfFolders).ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the root released folders. A root folder is a folder that does not have a parent folder.
        /// </summary>
        /// <returns>A collection of project folders.</returns>
        public Collection<ProjectFolder> GetReleasedRootFolders()
        {
            try
            {
                var query = (from folder in this.EntityContext.ProjectFolderSet
                                 .Include(p => p.Owner)
                             where (folder.IsReleased == true && !folder.IsDeleted && folder.ParentProjectFolder == null)
                             select folder).OrderBy(f => f.Name);

                return new Collection<ProjectFolder>(query.ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the released sub folders of a specified folder.
        /// </summary>
        /// <param name="parentFolderId">The parent folder id.</param>
        /// <returns>A collection of sub-folders.</returns>
        public Collection<ProjectFolder> GetReleasedSubFolders(Guid parentFolderId)
        {
            try
            {
                var query = (from folder in this.EntityContext.ProjectFolderSet
                             where (folder.IsReleased == true && folder.ParentProjectFolder.Guid == parentFolderId && !folder.IsDeleted)
                             select folder).OrderBy(f => f.Name);

                query.SetMergeOption(MergeOption.PreserveChanges);
                return new Collection<ProjectFolder>(query.ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Error occurred while getting some sub-folders.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the sub-folders of a specified folder.
        /// </summary>
        /// <param name="parentFolderId">The id of the folder whose sub-folders are requested.</param>
        /// <param name="ownerId">The id of the sub-folders' owner.</param>
        /// <returns>
        /// A collection of sub-folders.
        /// </returns>
        public Collection<ProjectFolder> GetSubFolders(Guid parentFolderId, Guid ownerId)
        {
            try
            {
                var query = (from folder in this.EntityContext.ProjectFolderSet
                                 .Include(f => f.Owner)
                             where (folder.Owner.Guid == ownerId && folder.ParentProjectFolder.Guid == parentFolderId && !folder.IsDeleted)
                             select folder).OrderBy(f => f.Name);

                query.SetMergeOption(MergeOption.PreserveChanges);
                return new Collection<ProjectFolder>(query.ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Error occurred while getting some sub-folders.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets a specified project folder with the purpose of deleting it.
        /// All objects owned by it (that must also be deleted) are loaded.
        /// </summary>
        /// <param name="folderId">The folder id.</param>
        /// <returns>The folder or null if not found.</returns>
        public ProjectFolder GetProjectFolderFull(Guid folderId)
        {
            try
            {
                // TODO: the folder tree loading below is inefficient because it executes one query for each folder level; rewrite it like the part/assy loading.
                var query = from folder in this.EntityContext.ProjectFolderSet
                                .Include(f => f.Owner)
                                .Include(f => f.ChildrenProjectFolders)
                                .Include(f => f.Projects)
                            where folder.Guid == folderId
                            select folder;

                ProjectFolder result = query.FirstOrDefault();
                if (result != null)
                {
                    var projects = result.Projects.ToList();
                    if (projects.Count > 0)
                    {
                        var projectRepository = new ProjectRepository(this.EntityContext, this.SourceDatabase);
                        foreach (Project project in projects)
                        {
                            projectRepository.GetProjectFull(project.Guid);
                        }
                    }

                    var subFolders = result.ChildrenProjectFolders.ToList();
                    if (subFolders.Count > 0)
                    {
                        var projectFolderRepository = new ProjectFolderRepository(this.EntityContext, this.SourceDatabase);
                        foreach (ProjectFolder subFolder in subFolders)
                        {
                            projectFolderRepository.GetProjectFolderFull(subFolder.Guid);
                        }
                    }
                }

                return result;
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the folder with the specified id, including its parent folder. The loaded references are ParentProjectFolder and Owner.
        /// </summary>
        /// <param name="folderId">The folder id.</param>
        /// <returns>
        /// A project folder, or null if it is not found.
        /// </returns>
        public ProjectFolder GetFolderWithParent(Guid folderId)
        {
            try
            {
                var query = from folder in this.EntityContext.ProjectFolderSet
                                .Include(f => f.Owner)
                                .Include(f => f.ParentProjectFolder)
                            where folder.Guid == folderId
                            select folder;

                return query.FirstOrDefault();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the offline folders.
        /// </summary>
        /// <param name="ownerGuid">The owner GUID.</param>
        /// <returns>List of offline folders.</returns>
        public ICollection<Guid> GetOfflineProjectFolders(Guid ownerGuid)
        {
            try
            {
                var offlineProjectFoldersQuery = from folder in this.EntityContext.ProjectFolderSet
                                                     .Include(p => p.Owner)
                                                 where folder.Owner.Guid == ownerGuid && folder.IsOffline
                                                 select folder.Guid;

                return offlineProjectFoldersQuery.ToList();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the ids for all project folders of a specified user.
        /// </summary>
        /// <param name="userID">The specified user id.</param>
        /// <returns>A collection containing the ids of all project folders for a specified user.</returns>
        public Collection<Guid> GetProjectFoldersId(Guid userID)
        {
            var query = from project in this.EntityContext.ProjectFolderSet
                        where project.Owner.Guid == userID
                        select project.Guid;

            try
            {
                return new Collection<Guid>(query.ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets all non-deleted released project folders.
        /// </summary>
        /// <returns>
        /// Collection of released project folders.
        /// </returns>
        public Collection<ProjectFolder> GetReleasedProjectFolders()
        {
            try
            {
                var query = (from folder in this.EntityContext.ProjectFolderSet
                             where (!folder.IsDeleted && folder.IsReleased)
                             select folder).OrderBy(f => f.Name);

                return new Collection<ProjectFolder>(query.ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the owner of the specified folder.
        /// </summary>
        /// <param name="folder">The folder.</param>
        /// <returns>
        /// The folder's owner, or null if the folder does not have an owner.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">folder;The folder was null.</exception>
        public User GetOwner(ProjectFolder folder)
        {
            if (folder == null)
            {
                throw new ArgumentNullException("folder", "The folder was null.");
            }

            try
            {
                var query = from f in this.EntityContext.ProjectFolderSet
                            join user in this.EntityContext.UserSet on f.Owner equals user
                            where f.Guid == folder.Guid
                            select user;

                return query.FirstOrDefault();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Checks if a specified project folder is the parent of another folder, directly or indirectly (parent of parent ...).
        /// </summary>
        /// <param name="parent">The folder to check if it is the parent.</param>
        /// <param name="folder">The folder that is the child.</param>
        /// <returns>true if <paramref name="parent"/> is the parent folder of <paramref name="folder"/>; otherwise false.</returns>
        public bool CheckIfParentOf(ProjectFolder parent, ProjectFolder folder)
        {
            if (folder == null || parent == null)
            {
                return false;
            }

            try
            {
                // TODO: this method executes a query for each cycle of the while loop. Write this method as a Table Valued Function in SQL.
                bool isParent = false;
                ProjectFolder freshFolder = GetFolderWithParent(folder.Guid);
                while (freshFolder != null)
                {
                    if (freshFolder.ParentProjectFolder != null && freshFolder.ParentProjectFolder.Guid == parent.Guid)
                    {
                        isParent = true;
                        break;
                    }

                    freshFolder = freshFolder.ParentProjectFolder != null ? this.GetFolderWithParent(freshFolder.ParentProjectFolder.Guid) : null;
                }

                return isParent;
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Deletes the specified folder from the repository using a stored procedure. All sub-entities owned by the folder are also deleted.
        /// The actual deletion from the database is performed during this call (when the stored procedure is called), not when SaveChanges() is called.
        /// </summary>
        /// <param name="folder">The folder to delete.</param>
        public void DeleteByStoredProcedure(ProjectFolder folder)
        {
            if (folder == null)
            {
                throw new ArgumentNullException("folder", "The folder was null.");
            }

            var originalCommandTimeout = this.EntityContext.CommandTimeout;

            try
            {
                this.EntityContext.CommandTimeout = 600;
                this.EntityContext.DeleteProjectFolder(folder.Guid);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, SourceDatabase);
            }
            finally
            {
                this.EntityContext.CommandTimeout = originalCommandTimeout;
            }
        }
    }
}
