﻿using System;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Implements operations for Country.
    /// </summary>
    internal class CountryRepository : Repository<Country>, ICountryRepository
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// A query that fully loads a country.
        /// </summary>
        private IQueryable<Country> queryFull;

        /// <summary>
        /// Initializes a new instance of the <see cref="CountryRepository" /> class.
        /// </summary>
        /// <param name="entityContext">The object context to be used for data operations.</param>
        /// <param name="sourceDatabase">A value indicating the database on which this repository operates.</param>
        public CountryRepository(DataEntities entityContext, DbIdentifier sourceDatabase)
            : base(entityContext, sourceDatabase)
        {
            this.queryFull = from country in this.EntityContext.CountrySet
                                 .Include(c => c.CountrySetting)
                                 .Include(c => c.Currency)
                                 .Include(c => c.WeightMeasurementUnit)
                                 .Include(c => c.LengthMeasurementUnit)
                                 .Include(c => c.VolumeMeasurementUnit)
                                 .Include(c => c.TimeMeasurementUnit)
                                 .Include(c => c.FloorMeasurementUnit)
                                 .Include("States.CountrySettings")
                             select country;
        }

        /// <summary>
        /// Gets all countries.
        /// </summary>
        /// <returns>
        /// A collection containing all countries.
        /// </returns>
        public Collection<Country> GetAll()
        {
            var query = from country in this.queryFull
                        where !country.IsReleased
                        select country;

            try
            {
                return new Collection<Country>(query.ToList());
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the country with the specified id.
        /// </summary>
        /// <param name="id">The country's id.</param>
        /// <returns>
        /// The country or null if it was not found.
        /// </returns>
        public Country GetById(Guid id)
        {
            var query = from country in this.queryFull
                        where country.Guid == id
                        select country;

            try
            {
                return query.FirstOrDefault();
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the country with the specified name.
        /// </summary>
        /// <param name="name">The country's name.</param>
        /// <returns>
        /// The country or null if it was not found.
        /// </returns>
        public Country GetByName(string name)
        {
            return this.GetByName(name, false);
        }

        /// <summary>
        /// Gets the country with the specified name.
        /// </summary>
        /// <param name="name">The country's name.</param>
        /// <param name="noTracking">if set to true the returned objects are not tracked by the data context.</param>
        /// <returns>
        /// The country or null if it was not found.
        /// </returns>
        public Country GetByName(string name, bool noTracking)
        {
            var query = from country in this.queryFull
                        where country.Name.Trim() == name.Trim() && !country.IsReleased
                        select country;

            if (noTracking)
            {
                query = query.AsNoTracking();
            }

            try
            {
                return query.FirstOrDefault();
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Checks if a country with the specified name exists.
        /// </summary>
        /// <param name="countryName">Name of the country to check if exists.</param>
        /// <returns>True if a country with the specified name exists; otherwise, false.</returns>
        public bool CheckIfExists(string countryName)
        {
            var query = from country in this.EntityContext.CountrySet
                        where string.Compare(country.Name, countryName, true) == 0
                        select country;

            try
            {
                return query.Count() > 0;
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the country with a specific id, or with a specific name, or a name like "country-name Country" (ex: "China Country")
        /// This method is used when its a possibility that the country name may have been modified in master data
        /// </summary>
        /// <param name="countryId">The country id</param>
        /// <param name="countryName">The country name</param>
        /// <returns>The country or null if it was not found.</returns>
        public Country ResolveCountry(Guid countryId, string countryName)
        {
            // try to get the country by the ID; if this is null
            // try to get it by name (case-insensitive); if this is null
            // try to get by name combined with "Country" (case-insensitive); the search string will be "<country-name> Country" (ex: "China Country") 
            var query = from country in this.queryFull
                        where (country.Guid == countryId || country.Name.Trim() == countryName.Trim() || country.Name.Trim() == countryName.Trim() + " country") && !country.IsReleased
                        select country;

            try
            {
                return query.FirstOrDefault();
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }
    }
}
