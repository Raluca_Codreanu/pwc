﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Implements the operations that are common to all repositories.
    /// </summary>
    /// <typeparam name="T">The type of entities in the repository</typeparam>
    internal abstract class Repository<T> : IRepository<T> where T : class
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// A non generic repository.
        /// </summary>
        private Repository nonGenericRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository&lt;T&gt;" /> class.
        /// </summary>
        /// <param name="entityContext">The object context to be used for data operations.</param>
        /// <exception cref="System.ArgumentNullException">The entity context was null.</exception>
        public Repository(DataEntities entityContext)
            : this(entityContext, DbIdentifier.NotSet)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository&lt;T&gt;" /> class.
        /// </summary>
        /// <param name="entityContext">The object context to be used for data operations.</param>
        /// <param name="sourceDatabase">A value indicating the database on which this repository operates.</param>
        /// <exception cref="System.ArgumentNullException">The entity context was null.</exception>
        public Repository(DataEntities entityContext, DbIdentifier sourceDatabase)
        {
            if (entityContext == null)
            {
                throw new ArgumentNullException("entityContext", "The entity context was null.");
            }

            this.EntityContext = entityContext;
            this.SourceDatabase = sourceDatabase;
            this.nonGenericRepository = new Repository(typeof(T), this.EntityContext, this.SourceDatabase);
        }

        /// <summary>
        /// Gets the object context used to access the data source.
        /// </summary>
        public DataEntities EntityContext { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating the database on which this repository operates.
        /// </summary>        
        public DbIdentifier SourceDatabase { get; protected set; }

        #region IRepository<T> Members

        /// <summary>
        /// Checks if an entity with the specified id exists in the database.
        /// </summary>
        /// <param name="id">The id of the entity.</param>
        /// <returns>
        /// True if a entity with the specified id exists; otherwise, false.
        /// </returns>        
        public bool CheckIfExists(Guid id)
        {
            try
            {
                var objectContext = ((System.Data.Entity.Infrastructure.IObjectContextAdapter)this.EntityContext).ObjectContext;
                var objSet = objectContext.CreateObjectSet<T>();
                var query = objectContext.CreateQuery<T>("[" + objSet.EntitySet.Name + "]").Where("it.Guid=@id");
                query.Parameters.Add(new ObjectParameter("id", id));

                return query.Count() > 0;
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, SourceDatabase);
            }
        }

        /// <summary>
        /// Adds the specified entity to the repository.
        /// </summary>
        /// <param name="entity">The entity to add.</param>
        public virtual void Add(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "The entity to add was null.");
            }

            try
            {
                var entitySet = this.EntityContext.Set<T>();
                entitySet.Add(entity);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, SourceDatabase);
            }
        }

        /// <summary>
        /// Saves the entity into the database. If it does not exist in the database then it is added.        
        /// </summary>
        /// <param name="entity">The entity to save.</param> 
        //// TODO: this method should be a variant of the Add method.
        public virtual void Save(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "The entity to save was null.");
            }

            var entityEntry = this.EntityContext.Entry(entity);
            if (entityEntry.State == EntityState.Detached)
            {
                // The Detached state means that the entity is not associated with this context => add it into the context.
                // If it is in any other state it should not be added into the context because the add operation will change its state to Added.
                this.Add(entity);
            }

            // otherwise the entity is already tracked by the data context so there is nothing left to do            
        }

        /// <summary>
        /// Refreshes an entity with the values from database. The entities in its graph are not refreshed.
        /// </summary>
        /// <param name="entity">The entity to refresh.</param>
        public void Refresh(T entity)
        {
            try
            {
                this.EntityContext.Entry(entity).Reload();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, SourceDatabase);
            }
        }

        /// <summary>
        /// Refreshes multiple entities with the values from database. The entities in their graph are not refreshed.
        /// </summary>
        /// <param name="entities">The entities to refresh.</param>
        public void Refresh(IEnumerable<T> entities)
        {
            foreach (var item in entities)
            {
                this.Refresh(item);
            }
        }

        /// <summary>
        /// Gets the entity having the specified primary key.
        /// <para />
        /// If an entity with the specified primary key exists in the context, then it is returned immediately without making a request to the database.
        /// Otherwise, a query is performed on the database to get the entity, and if found, the entity is attached to the context and returned.
        /// If no entity is found in the context or the database, then null is returned.
        /// </summary>
        /// <param name="key">The entity's key.</param>
        /// <returns>
        /// The entity having the specified key, or null if it was not found
        /// </returns>
        /// <exception cref="System.ArgumentNullException">The key was null.</exception>
        public T GetByKey(object key)
        {
            if (key == null)
            {
                throw new ArgumentNullException("key", "The key was null.");
            }

            try
            {
                return this.EntityContext.Set<T>().Find(key);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the entities that are currently loaded into the context and have the specified state.        
        /// </summary>
        /// <param name="state">The state of the entities that are returned.</param>
        /// <returns>A list of entities having the specified state.</returns>
        public IEnumerable<T> GetLocalEntities(EntityState state)
        {
            this.EntityContext.ChangeTracker.DetectChanges();
            return this.EntityContext.ChangeTracker.Entries<T>().Where(e => e.State == state).Select(e => e.Entity);
        }

        /// <summary>
        /// Determines whether the specified entity is marked as Added in the repository's data context. Entities in the Added state will be inserted
        /// into the data store when the repository changes are saved.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// true if the specified entity is in the Added state; otherwise, false.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">The entity was null.</exception>        
        public bool IsAdded(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "The entity was null.");
            }

            // Calling DetectChanges here is not necessary because the Added state is set when the object is attached to the data context even if AutoDetectChanges is disabled.
            return this.EntityContext.Entry(entity).State == EntityState.Added;
        }

        /// <summary>
        /// Determines whether the specified entity is marked as Deleted in the repository's data context. Entities in the Deleted state will be deleted
        /// from the data store when the repository changes are saved.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// true if the specified entity is in the Deleted state; otherwise, false.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">The entity was null.</exception>
        public bool IsDeleted(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "The entity was null.");
            }

            // Calling DetectChanges here is not necessary because the Deleted state is set when the object is removed from the data context even if AutoDetectChanges is disabled.
            return this.EntityContext.Entry(entity).State == EntityState.Deleted;
        }

        #endregion

        #region Delete

        /// <summary>
        /// Marks the specified entity, including any children entities owned by the entity, for deletion.
        /// </summary>
        /// <param name="entity">The entity to remove.</param>
        public void RemoveAll(T entity)
        {
            this.nonGenericRepository.RemoveAll(entity);
        }

        /// <summary>
        /// Detaches the specified entity from the context.
        /// </summary>
        /// <param name="entity">The entity to detach.</param>
        public void Detach(T entity)
        {
            this.nonGenericRepository.Detach(entity);
        }

        /// <summary>
        /// Detaches the specified entity, including any children entities owned by the entity, from the context.
        /// </summary>
        /// <param name="entity">The entity to detach.</param>
        public void DetachAll(T entity)
        {
            this.nonGenericRepository.DetachAll(entity);
        }

        #endregion Delete

        #region Undo

        /// <summary>
        /// Reverses the changes made to an entity and all the entities in its graph.
        /// </summary>
        /// <param name="entity">The entity whose changes to undo.</param>
        /*public void UndoChanges(T entity)
        {
            // TODO: the algorithm works but it is painfully slow (probably because of how DbReferenceEntry and DbCollectionProperty are obtained for each object).
            // Anyway, this undo algorithm is too generic; it knows not which objects are owned by the root entity and which not - it does undo on all.
            // Undo should be implemented explicitly in each repository, undoing only the objects owned by an entity type (recursively called for sub-objects but not going through Owner and units references, for example).
            var sw = new System.Diagnostics.Stopwatch();
            sw.Start();

            if (entity == null)
            {
                return;
            }

            // Can't undo a detached entity because its changes are not tracked.
            var entityEntry = this.EntityContext.Entry(entity);
            if (entityEntry.State == EntityState.Detached)
            {
                throw new InvalidOperationException("Undo can not be performed on a detached entity because its changes were not tracked.");
            }

            if (entityEntry.State == EntityState.Added)
            {
                throw new InvalidOperationException("Undo should not be performed on an Added entity because it would consist of deleting the entity and that should be done by the Delete operation.");
            }

            try
            {
                var dbContext = this.EntityContext;
                ObjectContext objectContext = ((IObjectContextAdapter)this.EntityContext).ObjectContext;

                // The list of all delete relationships in the context is necessary for undoing relationship changes.
                dbContext.ChangeTracker.DetectChanges();
                var deletedRelationships = objectContext.ObjectStateManager.GetObjectStateEntries(EntityState.Deleted).Where(e => e.IsRelationship).ToList();

                // Set up and start walking the entity's object graph to undo changes.
                List<object> parsedObjects = new List<object>();
                Queue<object> graphParseQueue = new Queue<object>();
                graphParseQueue.Enqueue(entity);

                // Will contain all entities found in the Added state in the entity graph. They will be deleted after the graph parsing is done.
                List<object> addedEntities = new List<object>();

                while (graphParseQueue.Count > 0)
                {
                    var currentEntity = graphParseQueue.Dequeue();

                    // Add the object to a "parsed" objects list to avoid infinite loops caused by circular paths in the object graph.
                    if (parsedObjects.Contains(currentEntity))
                    {
                        continue;
                    }
                    else
                    {
                        parsedObjects.Add(currentEntity);
                    }

                    var currentEntityEntry = dbContext.Entry(currentEntity);
                    if (currentEntityEntry.State == EntityState.Detached)
                    {
                        // Should not be the case, but is better to be safe.
                        continue;
                    }

                    if (currentEntityEntry.State == EntityState.Added)
                    {
                        // Entities in Added state are handled after the object graph parsing ends.
                        addedEntities.Add(currentEntity);
                    }
                    else if (currentEntityEntry.State == EntityState.Deleted)
                    {
                        // Deleted entities must be move to the Modified state first, otherwise their original values are lost.
                        currentEntityEntry.State = EntityState.Modified;
                        currentEntityEntry.State = EntityState.Unchanged;
                    }
                    else if (currentEntityEntry.State != EntityState.Unchanged)
                    {
                        // Marks the entity as not changed and restores its original values for the scalar properties.
                        currentEntityEntry.State = EntityState.Unchanged;
                    }

                    // Restore the deleted sub-objects of the entity and the relationships that links them to the entity. This is done in order for the restored
                    // sub-objects to be found in the parent's navigation properties by the code that adds them to the graph parsing queue.
                    var currentEntityObjectStateEntry = objectContext.ObjectStateManager.GetObjectStateEntry(currentEntity);
                    var relationshipsDeletedFromEntity =
                        deletedRelationships.Where(e => e.State == EntityState.Deleted && e.IsRelationshipForKey(currentEntityObjectStateEntry.EntityKey)).ToList();
                    foreach (var relationship in relationshipsDeletedFromEntity)
                    {
                        // Get the entity at the other end of the deleted relationship: 1st get its key, then its object state entry and finally the entity.
                        EntityKey otherEndKey = relationship.OtherEndKey(currentEntityObjectStateEntry.EntityKey);
                        if (otherEndKey != null)
                        {
                            ObjectStateEntry otherEndStateEntry;
                            if (objectContext.ObjectStateManager.TryGetObjectStateEntry(otherEndKey, out otherEndStateEntry)
                                && otherEndStateEntry.Entity != null)
                            {
                                var otherEndEntry = dbContext.Entry(otherEndStateEntry.Entity);
                                if (otherEndEntry.State == EntityState.Deleted)
                                {
                                    // Deleted entities must be move to the Modified state first, otherwise their original values are lost.
                                    otherEndEntry.State = EntityState.Modified;
                                    otherEndEntry.State = EntityState.Unchanged;
                                }

                                relationship.ChangeState(EntityState.Unchanged);
                            }
                        }
                    }

                    // Add all sub-objects of the entity to the graph parsing queue.
                    foreach (var entityMember in currentEntity.GetType().GetProperties().Select(p => currentEntityEntry.Member(p.Name)))
                    {
                        var referenceMember = entityMember as DbReferenceEntry;
                        if (referenceMember != null && referenceMember.CurrentValue != null)
                        {
                            var value = referenceMember.CurrentValue;
                            if (!graphParseQueue.Contains(value))
                            {
                                graphParseQueue.Enqueue(value);
                            }
                        }
                        else
                        {
                            var collectionMember = entityMember as DbCollectionEntry;
                            if (collectionMember != null && collectionMember.CurrentValue != null)
                            {
                                var collection = collectionMember.CurrentValue as IEnumerable;
                                if (collection == null)
                                {
                                    var message = string.Format(
                                        CultureInfo.InvariantCulture,
                                        "The collection property '{0}' of entity type '{1}' is not enumerable.",
                                        collectionMember.Name,
                                        currentEntity.GetType().Name);
                                    throw new InvalidOperationException(message);
                                }

                                foreach (var value in collection)
                                {
                                    if (!graphParseQueue.Contains(value))
                                    {
                                        graphParseQueue.Enqueue(value);
                                    }
                                }
                            }
                        }
                    }
                }

                // Delete all entities that were found in the Added state in the entity graph. They were not deleted during the graph parsing because that
                // would have disconnected them from the graph and their sub-objects. Deleting them will also delete any relationship they have with any other entity,
                // added or unchanged, so no "added" relationships cleanup is necessary.
                foreach (var obj in addedEntities)
                {
                    dbContext.Set(obj.GetType()).Remove(obj);
                }
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, SourceDatabase);
            }

            sw.Stop();
            log.Debug("Undo of entity '{0}' took {1}ms.", EntityUtils.GetEntityType(entity).Name, sw.ElapsedMilliseconds);
        }*/

        #endregion Undo
    }
}
