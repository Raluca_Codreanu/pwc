﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Implements operations for CountrySupplier.
    /// </summary>
    internal class CountrySupplierRepository : Repository<CountryState>, ICountrySupplierRepository
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Initializes a new instance of the <see cref="CountrySupplierRepository" /> class.
        /// </summary>
        /// <param name="entityContext">The object context to be used for data operations.</param>
        /// <param name="sourceDatabase">A value indicating the database on which this repository operates.</param>
        public CountrySupplierRepository(DataEntities entityContext, DbIdentifier sourceDatabase)
            : base(entityContext, sourceDatabase)
        {
        }

        /// <summary>
        /// Checks if a supplier with the specified name already exists.
        /// </summary>
        /// <param name="supplierName">Name of the supplier to check if exists.</param>
        /// <returns>True if a supplier with the specified name exists; otherwise, false.</returns>
        public bool CheckIfExists(string supplierName)
        {
            var query = from countryStates in this.EntityContext.CountryStateSet
                        where string.Compare(countryStates.Name, supplierName, true) == 0
                        select countryStates;
            try
            {
                return query.Count() > 0;
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the supplier with the specified id.
        /// </summary>
        /// <param name="id">The supplier's id.</param>
        /// <returns>
        /// The supplier or null if it was not found.
        /// </returns>
        public CountryState FindById(Guid id)
        {
            var query = from supplier in this.EntityContext.CountryStateSet
                        where supplier.Guid == id
                        select supplier;

            try
            {
                return query.FirstOrDefault();
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Retrieves the country suppliers.
        /// </summary>
        /// <returns>The collection of country suppliers</returns>
        public Collection<CountryState> FindAll()
        {
            var query = from supplier in this.EntityContext.CountryStateSet
                        select supplier;

            try
            {
                return new Collection<CountryState>(query.ToList());
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }
    }
}
