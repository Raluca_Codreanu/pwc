﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Implements the MachinesClassification Repository.
    /// </summary>
    internal class MachinesClassificationRepository : Repository<MachinesClassification>, IMachinesClassificationRepository
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Initializes a new instance of the <see cref="MachinesClassificationRepository"/> class.
        /// </summary>
        /// <param name="entityContext">The object context to be used for data operations.</param>
        /// <param name="sourceDatabase">A value indicating the database on which this repository operates.</param>
        public MachinesClassificationRepository(DataEntities entityContext, DbIdentifier sourceDatabase)
            : base(entityContext, sourceDatabase)
        {
        }

        #region IMachinesClassificationRepository Members

        /// <summary>
        /// Load the entire machines classification tree from the database, flattened. The hierarchy can be reconstructed using the Parent property
        /// of the MachinesClassification instances.
        /// </summary>
        /// <returns>
        /// A collection containing the flattened MachinesClassification hierarchy.
        /// </returns>
        public Collection<MachinesClassification> LoadClassificationTree()
        {
            IQueryable<MachinesClassification> query = from mc in this.EntityContext.MachinesClassificationSet
                                                       where !mc.IsReleased
                                                       select mc;

            try
            {
                List<MachinesClassification> results = query.ToList();
                return new Collection<MachinesClassification>(results);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets a machine classification item by name
        /// </summary>
        /// <param name="classificationName">Name of the classification</param>
        /// <returns>
        /// The classification.
        /// </returns>
        public MachinesClassification GetByName(string classificationName)
        {
            var query = from cls in this.EntityContext.MachinesClassificationSet
                        where cls.Name == classificationName && !cls.IsReleased
                        select cls;

            try
            {
                return query.FirstOrDefault();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        #endregion
    }
}
