﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Implements operations for Assembly.
    /// </summary>
    internal class AssemblyRepository : Repository<Assembly>, IAssemblyRepository
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Initializes a new instance of the <see cref="AssemblyRepository" /> class.
        /// </summary>
        /// <param name="entityContext">The object context to be used for data operations.</param>
        /// <param name="sourceDatabase">A value indicating the database on which this repository operates.</param>
        public AssemblyRepository(DataEntities entityContext, DbIdentifier sourceDatabase)
            : base(entityContext, sourceDatabase)
        {
        }

        #region Get

        /// <summary>
        /// Gets the assembly with the specified id fully loaded.
        /// </summary>
        /// <param name="assemblyId">The assembly id.</param>
        /// <returns>
        /// The assembly or null if it was not found.
        /// </returns>
        public Assembly GetAssemblyFull(Guid assemblyId)
        {
            return this.GetAssemblyFull(assemblyId, true);
        }

        /// <summary>
        /// Gets all the assemblies with the specified ids fully loaded.
        /// </summary>
        /// <param name="assembliesIds">The list with assemblies ids.</param>
        /// <returns>The assemblies list or null if none assembly was found.</returns>
        public IEnumerable<Assembly> GetAssembliesFull(IEnumerable<Guid> assembliesIds)
        {
            return this.GetAssembliesFull(assembliesIds, true);
        }

        /// <summary>
        /// Gets the assembly with the specified id. No references are loaded.
        /// </summary>
        /// <param name="assemblyId">The assembly id.</param>
        /// <param name="noTracking">if set to true disable change tracking for the retuned object(s).</param>
        /// <returns>
        /// The assembly or null if it is not found.
        /// </returns>
        public Assembly GetAssembly(Guid assemblyId, bool noTracking = true)
        {
            try
            {
                var query = from assy in this.EntityContext.AssemblySet
                            where assy.Guid == assemblyId
                            select assy;

                if (noTracking)
                {
                    query = query.AsNoTracking();
                }

                return query.FirstOrDefault();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the assembly specified by its id with only one reference, that to the owner.
        /// </summary>
        /// <param name="assemblyGuid">The id of the assembly</param>
        /// <returns>The assembly with the owner reference</returns>
        public Assembly GetAssemblyWithOwner(Guid assemblyGuid)
        {
            try
            {
                var query = from assy in this.EntityContext.AssemblySet
                                .Include(p => p.Owner)
                            where assy.Guid == assemblyGuid
                            select assy;

                return query.FirstOrDefault();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the assembly with top children subassemblies.
        /// </summary>
        /// <param name="assemblyGuid">The assembly GUID.</param>
        /// <returns>Assembly with top level subassemblies</returns>
        public Assembly GetAssemblyWithTopSubassemblies(Guid assemblyGuid)
        {
            try
            {
                var query = from assy in this.EntityContext.AssemblySet
                                .Include(p => p.Subassemblies)
                            where assy.Guid == assemblyGuid
                            select assy;

                return query.AsNoTracking().FirstOrDefault();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the assembly with top parts.
        /// </summary>
        /// <param name="assemblyGuid">The assembly GUID.</param>
        /// <returns>Assembly with loaded top level parts </returns>
        public Assembly GetAssemblyWithTopParts(Guid assemblyGuid)
        {
            try
            {
                var query = from assy in this.EntityContext.AssemblySet
                                .Include(p => p.Parts)
                            where assy.Guid == assemblyGuid
                            select assy;

                return query.AsNoTracking().FirstOrDefault();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets an Assembly to view its details in trash bin.
        /// </summary>
        /// <param name="assemblyId">The assembly id.</param>
        /// <returns>An Assembly.</returns>
        public Assembly GetAssemblyForView(Guid assemblyId)
        {
            try
            {
                var query = from assy in this.EntityContext.AssemblySet
                                .Include(p => p.Manufacturer)
                                .Include(p => p.OverheadSettings)
                                .Include(p => p.CountrySettings)
                                .Include(p => p.MeasurementUnit)
                            where assy.Guid == assemblyId
                            select assy;

                return query.AsNoTracking().FirstOrDefault();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the direct parent assembly of the specified entity
        /// </summary>
        /// <param name="entity">The entity to get it's parent.</param>
        /// <param name="noTracking">if set to true disable change tracking for the retuned object(s).</param>
        /// <returns>The parent assembly of the given entity.</returns>
        public Assembly GetParentAssembly(object entity, bool noTracking = true)
        {
            if (entity == null)
            {
                return null;
            }

            DataEntities context = this.EntityContext;
            IQueryable<Assembly> query = null;

            var process = entity as Process;
            if (process != null)
            {
                query = from assy in context.AssemblySet
                        join proc in context.ProcessSet on assy.Process equals proc
                        where proc.Guid == process.Guid
                        select assy;
            }
            else
            {
                var partEntity = entity as Part;
                if (partEntity != null)
                {
                    query = from part in context.PartSet
                            join assy in context.AssemblySet on part.Assembly equals assy
                            where part.Guid == partEntity.Guid
                            select assy;
                }
                else
                {
                    var assembly = entity as Assembly;
                    if (assembly != null)
                    {
                        query = from assy in context.AssemblySet
                                join assy2 in context.AssemblySet on assy.ParentAssembly equals assy2
                                where assy.Guid == assembly.Guid
                                select assy2;
                    }
                    else
                    {
                        throw new InvalidOperationException("The entity provided is not handled");
                    }
                }
            }

            if (noTracking)
            {
                query = query.AsNoTracking();
            }

            try
            {
                return query.FirstOrDefault();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the top assembly to which a specified entity belongs.
        /// </summary>
        /// <param name="entity">The entity to get it's parent.</param>
        /// <returns>
        /// The parent assembly of the given entity.
        /// </returns>
        public Assembly GetTopParentAssembly(object entity)
        {
            Guid assemblyGuid = Guid.Empty;

            if (entity is Assembly)
            {
                assemblyGuid = GetTopParentAssemblyId((entity as IIdentifiable).Guid);
            }
            else if (entity is Part)
            {
                var partRepository = new PartRepository(this.EntityContext, this.SourceDatabase);
                assemblyGuid = partRepository.GetTopParentAssemblyId((Part)entity);
            }
            else if (entity is Machine)
            {
                var machineRepository = new MachineRepository(this.EntityContext, this.SourceDatabase);
                assemblyGuid = machineRepository.GetParentAssemblyId((Machine)entity);
            }
            else if (entity is RawMaterial)
            {
                var rawMaterialRepository = new RawMaterialRepository(this.EntityContext, this.SourceDatabase);
                assemblyGuid = rawMaterialRepository.GetParentAssemblyId((RawMaterial)entity);
            }
            else if (entity is Consumable)
            {
                var consumableRepository = new ConsumableRepository(this.EntityContext, this.SourceDatabase);
                assemblyGuid = consumableRepository.GetParentAssemblyId((Consumable)entity);
            }
            else if (entity is Commodity)
            {
                var commodityRepository = new CommodityRepository(this.EntityContext, this.SourceDatabase);
                assemblyGuid = commodityRepository.GetParentAssemblyId((Commodity)entity);
            }
            else if (entity is Die)
            {
                var dieRepository = new DieRepository(this.EntityContext, this.SourceDatabase);
                assemblyGuid = dieRepository.GetParentAssemblyId((Die)entity);
            }
            else if (entity is ProcessStep)
            {
                var processStepRepository = new ProcessStepRepository(this.EntityContext, this.SourceDatabase);
                assemblyGuid = processStepRepository.GetParentAssemblyId((ProcessStep)entity);
            }
            else if (entity is Process)
            {
                var processRepository = new ProcessRepository(this.EntityContext, this.SourceDatabase);
                assemblyGuid = processRepository.GetParentAssemblyId((Process)entity);
            }

            if (assemblyGuid != Guid.Empty)
            {
                return GetAssembly(assemblyGuid);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the assemblies of a project. The assemblies are barebones.
        /// </summary>
        /// <param name="projectGuid">The guid of the parent project.</param>
        /// <returns>The collection of child assemblies</returns>
        public Collection<Assembly> GetProjectAssemblies(Guid projectGuid)
        {
            try
            {
                var query = from assy in this.EntityContext.AssemblySet
                            where assy.Project.Guid == projectGuid
                            select assy;

                return new Collection<Assembly>(query.ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets all master data assemblies.
        /// </summary>
        /// <returns>A collection containing the assemblies.</returns>
        public Collection<Assembly> GetAllMasterData()
        {
            var query = from assy in this.EntityContext.AssemblySet
                            .Include(a => a.Manufacturer)
                            .Include(a => a.CountrySettings)
                            .Include(a => a.OverheadSettings)
                            .Include(a => a.MeasurementUnit)
                        where assy.IsMasterData
                        select assy;

            try
            {
                return new Collection<Assembly>(query.AsNoTracking().ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the assemblies of a specified user, that are bookmarked, to be displayed in bookmarks tree.
        /// </summary>
        /// <param name="ownerId">The owner id.</param>
        /// <returns>A collection of assemblies.</returns>
        public Collection<Assembly> GetBookmarkedAssemblies(Guid ownerId)
        {
            try
            {
                var query = from assembly in this.EntityContext.AssemblySet
                            join bookmark in this.EntityContext.BookmarkSet on assembly.Guid equals bookmark.EntityId
                            where assembly.Owner.Guid == ownerId && !assembly.IsDeleted && bookmark.EntityTypeId == BookmarkedItemType.Assembly
                            select assembly;

                return new Collection<Assembly>(query.ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Retrieves the assemblies based on the ids list.
        /// </summary>
        /// <param name="ids">The ids of the assemblies to get.</param>
        /// <returns>A collection of assemblies.</returns>
        public Collection<Assembly> GetByIds(IEnumerable<Guid> ids)
        {
            var query = from assy in this.EntityContext.AssemblySet
                            .Include(a => a.Manufacturer)
                        where ids.Contains(assy.Guid)
                        select assy;

            try
            {
                return new Collection<Assembly>(query.AsNoTracking().ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the id of the project in which a specified assembly exists at any level in the assembly hierarchy.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <returns>
        /// The parent project's id or Guid.Empty if the assembly is not part of a project.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">The assembly was null.</exception>
        public Guid GetParentProjectId(Assembly assembly)
        {
            return this.GetParentProjectId(assembly, false);
        }

        /// <summary>
        /// Gets the id of the project in which a specified assembly exists, optionally specifying whether the assembly can exist anywhere
        /// in assembly hierarchy or only on the 1st level.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <param name="assyIsOnFirstLevel">If set to true, it specifies that the assembly is on the first level of the assemblies hierarchy;
        /// otherwise it means that the assembly can be anywhere in the hierarchy. Basically, if is true it instructs the method to return the project id only
        /// if the assembly is directly in a project (and is not a sub-assembly of another assembly).</param>
        /// <returns>
        /// The parent project's id
        /// - or - Guid.Empty if <paramref name="assyIsOnFirstLevel" /> is false and the assembly is not part of a project (at any level of the assemblies hierarchy)
        /// - or - Guid.Empty if if <paramref name="assyIsOnFirstLevel" /> is true and the assembly is not a direct child of a project.
        /// </returns>
        public Guid GetParentProjectId(Assembly assembly, bool assyIsOnFirstLevel)
        {
            if (assembly == null)
            {
                throw new ArgumentNullException("assembly", "The assembly was null.");
            }

            try
            {
                if (assyIsOnFirstLevel)
                {
                    var query = from assy in this.EntityContext.AssemblySet
                                    .Include(a => a.Project)
                                where assy.Guid == assembly.Guid
                                select (Guid?)assy.Project.Guid;

                    var projId = query.FirstOrDefault();
                    return projId.HasValue ? projId.Value : Guid.Empty;
                }
                else
                {
                    return this.EntityContext.FindTopParentId(assembly.Guid, "Assemblies", 1);
                }
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the top most parent assembly Id.
        /// </summary>
        /// <param name="assemblyId">The assembly Id.</param>
        /// <returns>The top most assembly to which the specified assembly belong to.</returns>
        public Guid GetTopParentAssemblyId(Guid assemblyId)
        {
            if (assemblyId == Guid.Empty)
            {
                throw new ArgumentNullException("assemblyId", "Assembly was null.");
            }

            try
            {
                return this.EntityContext.FindTopParentId(assemblyId, "Assemblies", 2);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the parent assembly Id.
        /// </summary>
        /// <param name="assemblyId">The assembly with the specified Id.</param>
        /// <returns>The parent assembly Id.</returns>
        public Guid GetParentAssemblyId(Guid assemblyId)
        {
            if (assemblyId == Guid.Empty)
            {
                throw new ArgumentNullException("assemblyId", "The specified Id of the assembly was empty.");
            }

            var query = from assy in this.EntityContext.AssemblySet
                        join assy2 in this.EntityContext.AssemblySet on assy.ParentAssembly.Guid equals assy2.Guid
                        where assy.Guid == assemblyId
                        select assy2.Guid;

            try
            {
                return query.FirstOrDefault();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the master data top level assemblies (assemblies that have no parent).
        /// </summary>
        /// <returns>A collection containing the assemblies.</returns>
        public Collection<Assembly> GetTopLevelMasterData()
        {
            var query = from assy in this.EntityContext.AssemblySet
                           .Include(a => a.Manufacturer)
                           .Include(a => a.CountrySettings)
                           .Include(a => a.OverheadSettings)
                           .Include(a => a.MeasurementUnit)
                        where assy.IsMasterData && assy.ParentAssembly == null
                        select assy;

            try
            {
                return new Collection<Assembly>(query.AsNoTracking().ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        #endregion

        #region Logic for fully loading an assembly

        /// <summary>
        /// Gets the assembly with the specified id fully loaded.
        /// </summary>
        /// <param name="assemblyId">The assembly id.</param>
        /// <param name="loadSubAssembliesAndParts">
        /// if set to true it fully loads the assemblies sub-assemblies and sub-parts (recursively, down the hierarchy); otherwise it fully loads just the specified assembly.
        /// </param>
        /// <returns>
        /// The assembly or null if it was not found.
        /// </returns>        
        internal Assembly GetAssemblyFull(Guid assemblyId, bool loadSubAssembliesAndParts)
        {
            /* Many of the queries below have a Contains clause in the condition, which is translated into a SQL IN clause.
             * The number of elements in the IN clause is limited (the limit is not low, is thousands of elements) and the more elements it has
             * the worse the query performs but on all client provided models there were less than 200 elements and the performance was good.
             * In the future it may be necessary to partition the elements and execute the query for each partition.
             */

            try
            {
                var context = this.EntityContext;
                var merge = MergeOption.PreserveChanges;

                Collection<Guid> allAssembliesIds = null;
                if (loadSubAssembliesAndParts)
                {
                    // Get the id of all sub-assemblies in the searched assembly's graph. The call to GetSubAssembliesIds should also return the id passed into it as parameter.
                    allAssembliesIds = context.GetSubAssembliesIds(assemblyId);
                    if (!allAssembliesIds.Contains(assemblyId))
                    {
                        allAssembliesIds.Add(assemblyId);
                    }
                }
                else
                {
                    // Load just the specified assembly.
                    allAssembliesIds = new Collection<Guid>() { assemblyId };
                }

                if (allAssembliesIds.Count == 0)
                {
                    return null;
                }

                // Load the assembly and all its sub-assemblies
                var assyQuery = from assembly in context.AssemblySet
                                    .Include(a => a.Owner)
                                    .Include(a => a.Manufacturer)
                                    .Include(a => a.CountrySettings)
                                    .Include(a => a.OverheadSettings)
                                    .Include(a => a.Subassemblies)
                                    .Include(a => a.MeasurementUnit)
                                    .Include(a => a.ProcessStepAssemblyAmounts)
                                where allAssembliesIds.Contains(assembly.Guid)
                                select assembly;
                assyQuery.SetMergeOption(merge);
                var assemblies = assyQuery.ToList();

                if (assemblies.Count > 0)
                {
                    // Load the assembly's process and steps list here. It is faster than putting the "Process.Steps" include in the main assy query.                    
                    var processQuery = from process in context.ProcessSet
                                       join a in context.AssemblySet on process.Guid equals a.Process.Guid
                                       where allAssembliesIds.Contains(a.Guid)
                                       select process;

                    // When using a join the ObjectSet<T>.Include does not work so we have to set the include just before the query is executed.
                    processQuery.SetMergeOption(merge);
                    processQuery.Include(p => p.Steps).ToList();

                    var processStepsIds = assemblies.Where(a => a.Process != null).SelectMany(a => a.Process.Steps.Select(s => s.Guid)).ToList();
                    if (processStepsIds.Count > 0)
                    {
                        // Loading the steps here is faster than as part of the compiled query.
                        var stepsQuery = from step in context.ProcessStepSet
                                             .Include(s => s.CycleTimeCalculations)
                                             .Include(s => s.CycleTimeUnit)
                                             .Include(s => s.MaxDownTimeUnit)
                                             .Include(s => s.ProcessTimeUnit)
                                             .Include(s => s.SetupTimeUnit)
                                             .Include(s => s.SubType)
                                             .Include(s => s.Type)
                                             .Include(s => s.AssemblyAmounts)
                                             .Include(s => s.PartAmounts)
                                         where processStepsIds.Contains(step.Guid)
                                         select step;
                        stepsQuery.SetMergeOption(merge);
                        stepsQuery.ToList();

                        // Load the process' commodities.
                        var commoditiesQuery = from commodity in context.CommoditySet
                                                   .Include(c => c.Manufacturer)
                                                   .Include(c => c.WeightUnitBase)
                                               where processStepsIds.Contains(commodity.ProcessStep.Guid)
                                               select commodity;
                        commoditiesQuery.SetMergeOption(merge);
                        commoditiesQuery.ToList();

                        // Load the process' consumables
                        var consumablesQuery = from consumable in context.ConsumableSet
                                                   .Include(c => c.Manufacturer)
                                                   .Include(c => c.AmountUnitBase)
                                                   .Include(c => c.PriceUnitBase)
                                               where processStepsIds.Contains(consumable.ProcessStep.Guid)
                                               select consumable;
                        consumablesQuery.SetMergeOption(merge);
                        consumablesQuery.ToList();

                        // Load the process' dies
                        var diesQuery = from die in context.DieSet
                                            .Include(d => d.Manufacturer)
                                        where processStepsIds.Contains(die.ProcessStep.Guid)
                                        select die;
                        diesQuery.SetMergeOption(merge);
                        diesQuery.ToList();

                        // Load the process' machines
                        var machinesQuery = from machine in context.MachineSet
                                                .Include(m => m.Manufacturer)
                                                .Include(m => m.ClassificationLevel4)
                                                .Include(m => m.MainClassification)
                                                .Include(m => m.SubClassification)
                                                .Include(m => m.TypeClassification)
                                            where processStepsIds.Contains(machine.ProcessStep.Guid)
                                            select machine;
                        machinesQuery.SetMergeOption(merge);
                        machinesQuery.ToList();
                    }

                    if (loadSubAssembliesAndParts)
                    {
                        // Load all parts of all assemblies.
                        var partRepository = new PartRepository(this.EntityContext, this.SourceDatabase);
                        partRepository.LoadAssemblyPartsFull(allAssembliesIds);
                    }

                    // Load the transport cost calculations and their settings from the assembly and its subassemblies.
                    var calculationQuery = from calculation in context.TransportCostCalculationSet
                                               .Include(c => c.TransportCostCalculationSetting)
                                           where allAssembliesIds.Contains(calculation.Assembly.Guid)
                                           select calculation;
                    calculationQuery.SetMergeOption(merge);
                    calculationQuery.ToList();
                }

                var assy = assemblies.FirstOrDefault(a => a.Guid == assemblyId);

                return assy;
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the assemblies with the specified ids fully loaded.
        /// </summary>
        /// <param name="assembliesIds">The assemblies ids.</param>
        /// <param name="loadSubAssembliesAndParts">
        /// if set to true it fully loads the assemblies sub-assemblies and sub-parts (recursively, down the hierarchy); otherwise it fully loads just the specified assembly.
        /// </param>
        /// <returns>
        /// The assemblies loaded or null if none assembly was found.
        /// </returns>        
        public IEnumerable<Assembly> GetAssembliesFull(IEnumerable<Guid> assembliesIds, bool loadSubAssembliesAndParts)
        {
            /* Many of the queries below have a Contains clause in the condition, which is translated into a SQL IN clause.
             * The number of elements in the IN clause is limited (the limit is not low, is thousands of elements) and the more elements it has
             * the worse the query performs but on all client provided models there were less than 200 elements and the performance was good.
             * In the future it may be necessary to partition the elements and execute the query for each partition.
             */

            try
            {
                var context = this.EntityContext;
                var merge = MergeOption.PreserveChanges;

                var allAssembliesIds = new Collection<Guid>();
                if (loadSubAssembliesAndParts)
                {
                    // Get the id of all sub-assemblies in the searched assembly's graph. The call to GetSubAssembliesIds should also return the id passed into it as parameter.
                    foreach (var assemblyId in assembliesIds)
                    {
                        allAssembliesIds.AddRange(context.GetSubAssembliesIds(assemblyId));
                        if (!allAssembliesIds.Contains(assemblyId))
                        {
                            allAssembliesIds.Add(assemblyId);
                        }
                    }
                }
                else
                {
                    // Load just the specified assembly.
                    allAssembliesIds = new Collection<Guid>();
                    allAssembliesIds.AddRange(assembliesIds);
                }

                if (allAssembliesIds.Count == 0)
                {
                    return null;
                }

                // Load the assembly and all its sub-assemblies
                var assyQuery = from assembly in context.AssemblySet
                                    .Include(a => a.Owner)
                                    .Include(a => a.Manufacturer)
                                    .Include(a => a.CountrySettings)
                                    .Include(a => a.OverheadSettings)
                                    .Include(a => a.Subassemblies)
                                    .Include(a => a.MeasurementUnit)
                                    .Include(a => a.ProcessStepAssemblyAmounts)
                                where allAssembliesIds.Contains(assembly.Guid)
                                select assembly;
                assyQuery.SetMergeOption(merge);
                var assemblies = assyQuery.ToList();

                if (assemblies.Count > 0)
                {
                    // Load the assembly's process and steps list here. It is faster than putting the "Process.Steps" include in the main assy query.                    
                    var processQuery = from process in context.ProcessSet
                                       join a in context.AssemblySet on process.Guid equals a.Process.Guid
                                       where allAssembliesIds.Contains(a.Guid)
                                       select process;

                    // When using a join the ObjectSet<T>.Include does not work so we have to set the include just before the query is executed.
                    processQuery.SetMergeOption(merge);
                    processQuery.Include(p => p.Steps).ToList();

                    var processStepsIds = assemblies.Where(a => a.Process != null).SelectMany(a => a.Process.Steps.Select(s => s.Guid)).ToList();
                    if (processStepsIds.Count > 0)
                    {
                        // Loading the steps here is faster than as part of the compiled query.
                        var stepsQuery = from step in context.ProcessStepSet
                                             .Include(s => s.CycleTimeCalculations)
                                             .Include(s => s.CycleTimeUnit)
                                             .Include(s => s.MaxDownTimeUnit)
                                             .Include(s => s.ProcessTimeUnit)
                                             .Include(s => s.SetupTimeUnit)
                                             .Include(s => s.SubType)
                                             .Include(s => s.Type)
                                             .Include(s => s.AssemblyAmounts)
                                             .Include(s => s.PartAmounts)
                                         where processStepsIds.Contains(step.Guid)
                                         select step;
                        stepsQuery.SetMergeOption(merge);
                        stepsQuery.ToList();

                        // Load the process' commodities.
                        var commoditiesQuery = from commodity in context.CommoditySet
                                                   .Include(c => c.Manufacturer)
                                                   .Include(c => c.WeightUnitBase)
                                               where processStepsIds.Contains(commodity.ProcessStep.Guid)
                                               select commodity;
                        commoditiesQuery.SetMergeOption(merge);
                        commoditiesQuery.ToList();

                        // Load the process' consumables
                        var consumablesQuery = from consumable in context.ConsumableSet
                                                   .Include(c => c.Manufacturer)
                                                   .Include(c => c.AmountUnitBase)
                                                   .Include(c => c.PriceUnitBase)
                                               where processStepsIds.Contains(consumable.ProcessStep.Guid)
                                               select consumable;
                        consumablesQuery.SetMergeOption(merge);
                        consumablesQuery.ToList();

                        // Load the process' dies
                        var diesQuery = from die in context.DieSet
                                            .Include(d => d.Manufacturer)
                                        where processStepsIds.Contains(die.ProcessStep.Guid)
                                        select die;
                        diesQuery.SetMergeOption(merge);
                        diesQuery.ToList();

                        // Load the process' machines
                        var machinesQuery = from machine in context.MachineSet
                                                .Include(m => m.Manufacturer)
                                                .Include(m => m.ClassificationLevel4)
                                                .Include(m => m.MainClassification)
                                                .Include(m => m.SubClassification)
                                                .Include(m => m.TypeClassification)
                                            where processStepsIds.Contains(machine.ProcessStep.Guid)
                                            select machine;
                        machinesQuery.SetMergeOption(merge);
                        machinesQuery.ToList();
                    }

                    if (loadSubAssembliesAndParts)
                    {
                        // Load all parts of all assemblies.
                        var partRepository = new PartRepository(this.EntityContext, this.SourceDatabase);
                        partRepository.LoadAssemblyPartsFull(allAssembliesIds);
                    }

                    // Load the transport cost calculations and their settings from the assembly and its subassemblies.
                    var calculationQuery = from calculation in context.TransportCostCalculationSet
                                               .Include(c => c.TransportCostCalculationSetting)
                                           where allAssembliesIds.Contains(calculation.Assembly.Guid)
                                           select calculation;
                    calculationQuery.SetMergeOption(merge);
                    calculationQuery.ToList();
                }

                return assemblies.Where(a => assembliesIds.Contains(a.Guid));
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        #endregion Logic for fully loading an assembly

        #region Save/Update/Refresh

        /// <summary>
        /// Updates the overhead settings in the assembly retrieved from the db, calls the methods for all its parts and subassemblies and commits the changes
        /// </summary>
        /// <param name="assemblyId">The guid of the updated assembly</param>
        /// <param name="overheadSettings">The updated overhead settings</param>
        public void UpdateOverheadSettings(Guid assemblyId, OverheadSetting overheadSettings)
        {
            try
            {
                var query = from assy in this.EntityContext.AssemblySet
                                .Include(p => p.OverheadSettings)
                                .Include(p => p.Subassemblies)
                                .Include(p => p.Parts)
                            where assy.Guid == assemblyId
                            select assy;

                query.SetMergeOption(MergeOption.PreserveChanges);

                Assembly assembly = query.FirstOrDefault();
                if (assembly != null)
                {
                    overheadSettings.CopyValuesTo(assembly.OverheadSettings);
                }

                if (assembly.Parts.Count > 0)
                {
                    var partRepository = new PartRepository(this.EntityContext, this.SourceDatabase);
                    foreach (Part part in assembly.Parts)
                    {
                        partRepository.UpdateOverheadSettings(part.Guid, overheadSettings);
                    }
                }

                foreach (Assembly subassy in assembly.Subassemblies)
                {
                    UpdateOverheadSettings(subassy.Guid, overheadSettings);
                }
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Refreshes the assembly and its process by overriding the changes with data from the db.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        public void RefreshWithProcess(Assembly assembly)
        {
            try
            {
                this.EntityContext.RefreshNavigationProperty(assembly, a => a.ProcessStepAssemblyAmounts);

                if (assembly.Process != null)
                {
                    this.EntityContext.RefreshNavigationProperty(assembly.Process, p => p.Steps);
                }

                // Refresh the assembly last because it will be detached if it was deleted from the database.                
                this.Refresh(assembly);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Refreshes the assembly, including its children.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        public void RefreshWithChildren(Assembly assembly)
        {
            try
            {
                this.GetAssemblyFull(assembly.Guid, false);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes the specified assembly from the repository using a stored procedure.
        /// The actual deletion from the database is performed during this call (when the stored procedure is called), not when SaveChanges() is called.
        /// </summary>
        /// <param name="assembly">The assembly to delete.</param>
        public void DeleteByStoredProcedure(Assembly assembly)
        {
            if (assembly == null)
            {
                throw new ArgumentNullException("assembly", "The assembly was null.");
            }

            var originalCommandTimeout = this.EntityContext.CommandTimeout;

            try
            {
                this.EntityContext.CommandTimeout = 600;
                this.EntityContext.DeleteAssembly(assembly.Guid);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, SourceDatabase);
            }
            finally
            {
                this.EntityContext.CommandTimeout = originalCommandTimeout;
            }
        }

        #endregion
    }
}