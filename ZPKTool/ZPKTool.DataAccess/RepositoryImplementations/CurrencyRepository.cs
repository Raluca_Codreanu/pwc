﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using ZPKTool.Common;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Implements operations for Currency.
    /// </summary>
    internal class CurrencyRepository : Repository<Currency>, ICurrencyRepository
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Initializes a new instance of the <see cref="CurrencyRepository"/> class.
        /// </summary>
        /// <param name="entityContext">The object context to be used for data operations.</param>
        /// <param name="sourceDatabase">A value indicating the database on which this repository operates.</param>
        public CurrencyRepository(DataEntities entityContext, DbIdentifier sourceDatabase)
            : base(entityContext, sourceDatabase)
        {
        }

        /// <summary>
        /// Check if a currency is assigned to any country
        /// </summary>
        /// <param name="currencyId">The Currency Id</param>
        /// <returns>True if the currency is used, False if not </returns>
        public bool CheckCurrencyHasCountry(Guid currencyId)
        {
            var query = from country in this.EntityContext.CountrySet
                        where country.Currency.Guid == currencyId
                        select country;

            try
            {
                return query.Count() == 0 ? false : true;
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the currency with the specified id.
        /// </summary>
        /// <param name="id">The currency's id.</param>
        /// <returns>The currency or null if it was not found.</returns>
        public Currency GetById(Guid id)
        {
            var query = from currency in this.EntityContext.CurrencySet
                        where currency.Guid == id
                        select currency;

            try
            {
                return query.FirstOrDefault();
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the currency with the specified ISO code.
        /// </summary>
        /// <param name="isoCode">The currency's ISO code.</param>
        /// <returns>
        /// The currency or null if it was not found.
        /// </returns>
        public Currency GetByIsoCode(string isoCode)
        {
            var query = from currency in this.EntityContext.CurrencySet
                        where currency.IsMasterData && currency.IsoCode == isoCode
                        select currency;

            try
            {
                return query.FirstOrDefault();
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the base currency of the project with the specified Id.
        /// </summary>
        /// <param name="projectId">The project Id.</param>
        /// <returns>
        /// The base currency or a dummy default currency, that represent Euro, if the project has not base currency set.
        /// </returns>
        /// <remarks>If the project with the specified id is not found, the default currency, euro, is returned.</remarks>
        public Currency GetProjectBaseCurrency(Guid projectId)
        {
            var query = from project in this.EntityContext.ProjectSet
                        where project.Guid == projectId
                        select project.BaseCurrency;

            try
            {
                var baseCurrency = query.FirstOrDefault();
                if (baseCurrency == null)
                {
                    baseCurrency = new Currency()
                    {
                        Name = "Euro",
                        IsoCode = "EUR",
                        Symbol = "€",
                        ExchangeRate = 1m
                    };
                }

                return baseCurrency;
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the base, non released, currencies.
        /// </summary>
        /// <returns>
        /// A collection of currencies.
        /// </returns>
        public Collection<Currency> GetBaseCurrencies()
        {
            var query = from currency in this.EntityContext.CurrencySet
                        where !currency.IsReleased && currency.IsMasterData
                        select currency;

            try
            {
                return new Collection<Currency>(query.ToList());
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the currencies of the project with the specified Id.
        /// </summary>
        /// <param name="projectId">The project Id.</param>
        /// <returns>
        /// A collection of currencies.
        /// </returns>
        public Collection<Currency> GetProjectCurrencies(Guid projectId)
        {
            var query = from currency in this.EntityContext.CurrencySet
                        where currency.Project.Guid == projectId
                        select currency;

            try
            {
                return new Collection<Currency>(query.ToList());
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the currencies with the specified Id.
        /// </summary>
        /// <param name="currenciesId">The list of currencies Id.</param>
        /// <returns>
        /// A collection of currencies.
        /// </returns>
        public Collection<Currency> GetCurrencies(IEnumerable<Guid> currenciesId)
        {
            var query = from currency in this.EntityContext.CurrencySet
                        where currenciesId.Contains(currency.Guid)
                        select currency;

            try
            {
                return new Collection<Currency>(query.ToList());
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Deletes the entity currencies.
        /// </summary>
        /// <param name="entity">The entity from which currency objects are deleted</param>
        public void DeleteCurrencies(object entity)
        {
            var currenciesToDelete = new List<Currency>();
            var project = entity as Project;
            if (project != null)
            {
                currenciesToDelete.AddRange(project.Currencies);
            }

            foreach (var currency in currenciesToDelete)
            {
                this.RemoveAll(currency);
            }
        }
    }
}