﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// The repository implementation for the <see cref="ConsumablePriceHistory"/> entity.
    /// </summary>
    internal class ConsumablePriceHistoryRepository : Repository<ConsumablesPriceHistory>, IConsumablePriceHistoryRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConsumablePriceHistoryRepository" /> class.
        /// </summary>
        /// <param name="entityContext">The object context to be used for data operations.</param>
        /// <param name="sourceDatabase">A value indicating the database on which this repository operates.</param>
        public ConsumablePriceHistoryRepository(DataEntities entityContext, DbIdentifier sourceDatabase)
            : base(entityContext, sourceDatabase)
        {
        }
    }
}
