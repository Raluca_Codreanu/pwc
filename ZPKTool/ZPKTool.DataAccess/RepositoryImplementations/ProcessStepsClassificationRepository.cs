﻿using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Implements the MachinesClassification Repository.
    /// </summary>
    internal class ProcessStepsClassificationRepository : Repository<ProcessStepsClassification>, IProcessStepsClassificationRepository
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessStepsClassificationRepository"/> class.
        /// </summary>
        /// <param name="entityContext">The object context to be used for data operations.</param>
        /// <param name="sourceDatabase">A value indicating the database on which this repository operates.</param>
        public ProcessStepsClassificationRepository(DataEntities entityContext, DbIdentifier sourceDatabase)
            : base(entityContext, sourceDatabase)
        {
        }

        #region IProcessStepsClassificationRepository Members

        /// <summary>
        /// Load the entire process steps classification tree from the database, flattened. The hierarchy can be reconstructed using the Parent property
        /// of the ProcessStepsClassification instances.
        /// </summary>
        /// <returns>
        /// A collection containing the flattened ProcessStepsClassification hierarchy.
        /// </returns>
        public Collection<ProcessStepsClassification> LoadClassificationTree()
        {
            var query = from cls in this.EntityContext.ProcessStepsClassificationSet
                        where !cls.IsReleased
                        select cls;

            try
            {
                return new Collection<ProcessStepsClassification>(query.ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets a process step classification item by name
        /// </summary>
        /// <param name="classificationName">Name of the classification</param>
        /// <returns>
        /// The classification, or null if not found.
        /// </returns>
        public ProcessStepsClassification GetByName(string classificationName)
        {
            var query = from cls in this.EntityContext.ProcessStepsClassificationSet
                        where cls.Name == classificationName && !cls.IsReleased
                        select cls;

            try
            {
                return query.FirstOrDefault();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        #endregion
    }
}
