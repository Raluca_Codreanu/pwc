﻿using System;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Implements operations for OverheadSettings.
    /// </summary>
    internal class OverheadSettingsRepository : Repository<OverheadSetting>, IOverheadSettingsRepository
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Initializes a new instance of the <see cref="OverheadSettingsRepository"/> class.
        /// </summary>
        /// <param name="entityContext">The object context to be used for data operations.</param>
        /// <param name="sourceDatabase">A value indicating the database on which this repository operates.</param>
        public OverheadSettingsRepository(DataEntities entityContext, DbIdentifier sourceDatabase)
            : base(entityContext, sourceDatabase)
        {
        }

        /// <summary>
        /// Retrieves the master data overhead settings.
        /// </summary>
        /// <returns>
        /// The master data overhead settings.
        /// </returns>
        public OverheadSetting GetMasterData()
        {
            var query = from ohsetting in this.EntityContext.OverheadSettingSet
                        where ohsetting.IsMasterData
                        select ohsetting;

            query.SetMergeOption(MergeOption.PreserveChanges);

            try
            {
                return query.FirstOrDefault();
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Deletes the specified overhead settings from the repository using a stored procedure. All sub-entities owned by <paramref name="overheadSettings"/> are also deleted.
        /// The actual deletion from the database is performed during this call (when the stored procedure is called), not when SaveChanges() is called.
        /// </summary>
        /// <param name="overheadSettings">The overhead settings to delete.</param>
        public void DeleteByStoredProcedure(OverheadSetting overheadSettings)
        {
            if (overheadSettings == null)
            {
                throw new ArgumentNullException("overheadSettings", "The overhead setting was null.");
            }

            var originalCommandTimeout = this.EntityContext.CommandTimeout;

            try
            {
                this.EntityContext.CommandTimeout = 600;
                this.EntityContext.DeleteOverheadSetting(overheadSettings.Guid);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, SourceDatabase);
            }
            finally
            {
                this.EntityContext.CommandTimeout = originalCommandTimeout;
            }
        }
    }
}
