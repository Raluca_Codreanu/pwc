﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Implements operations for CountrySetting.
    /// </summary>
    internal class CountrySettingRepository : Repository<CountrySetting>, ICountrySettingRepository
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Initializes a new instance of the <see cref="CountrySettingRepository" /> class.
        /// </summary>
        /// <param name="entityContext">The object context to be used for data operations.</param>
        /// <param name="sourceDatabase">A value indicating the database on which this repository operates.</param>
        public CountrySettingRepository(DataEntities entityContext, DbIdentifier sourceDatabase)
            : base(entityContext, sourceDatabase)
        {
        }

        /// <summary>
        /// Gets the country settings with the default last change timestamp.
        /// </summary>
        /// <returns>A collection of country settings.</returns>
        public ICollection<CountrySetting> GetCountrySettingsWithDefaultTimestamp()
        {
            try
            {
                var query = from setting in this.EntityContext.CountrySettingSet
                                .Include(s => s.Assemblies)
                                .Include(s => s.Parts)
                            where setting.LastChangeTimestamp == new DateTime(2000, 1, 1)
                            select setting;

                return query.ToList();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }
    }
}