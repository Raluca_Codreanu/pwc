﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Implements operations for Machine.
    /// </summary>
    internal class MachineRepository : Repository<Machine>, IMachineRepository
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The query that fully loads a machine.
        /// </summary>
        private IQueryable<Machine> queryFull;

        /// <summary>
        /// Initializes a new instance of the <see cref="MachineRepository"/> class.
        /// </summary>
        /// <param name="entityContext">The object context to be used for data operations.</param>
        /// <param name="sourceDatabase">A value indicating the database on which this repository operates.</param>
        public MachineRepository(DataEntities entityContext, DbIdentifier sourceDatabase)
            : base(entityContext, sourceDatabase)
        {
            queryFull = from machine in EntityContext.MachineSet
                            .Include(m => m.MainClassification)
                            .Include(m => m.TypeClassification)
                            .Include(m => m.SubClassification)
                            .Include(m => m.ClassificationLevel4)
                            .Include(m => m.Manufacturer.Owner)
                            .Include(m => m.Owner)
                            .Include(m => m.ProcessStep)
                        select machine;
        }

        /// <summary>
        /// Retrieves the machines based on the ids list.
        /// </summary>
        /// <param name="id">The id of the machines to get.</param>
        /// <param name="noTracking">Option specifying if the MergeOption should be NoTracking.</param>
        /// <returns>A collection of machines.</returns>
        public Machine GetById(Guid id, bool noTracking = false)
        {
            return this.GetByIds(new List<Guid> { id }, noTracking).FirstOrDefault();
        }

        /// <summary>
        /// Retrieves the machines based on the ids list.
        /// </summary>
        /// <param name="ids">AList of machine ids.</param>
        /// <param name="noTracking">Option specifying if the MergeOption should be NoTracking.</param>
        /// <returns>A collection of machines.</returns>
        public Collection<Machine> GetByIds(IEnumerable<Guid> ids, bool noTracking = false)
        {
            var query = from machine in queryFull
                        where ids.Contains(machine.Guid)
                        select machine;

            if (noTracking)
            {
                query = query.AsNoTracking();
            }
            else
            {
                query.SetMergeOption(MergeOption.PreserveChanges);
            }

            try
            {
                return new Collection<Machine>(query.ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Retrieves all the master data machines
        /// </summary>
        /// <returns>A collection containing all the master data machines</returns>
        public Collection<Machine> GetAllMasterData()
        {
            var query = from machine in queryFull
                        where machine.IsMasterData
                        select machine;

            try
            {
                return new Collection<Machine>(query.ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the master data machines with the specified GUIDs.
        /// </summary>
        /// <param name="guids">The GUIDs of the machines to get.</param>
        /// <returns>
        /// A collection of machines.
        /// </returns>
        public Collection<Machine> GetMasterDataMachines(IEnumerable<Guid> guids)
        {
            if (guids == null)
            {
                return new Collection<Machine>();
            }

            try
            {
                var query = from machine in queryFull
                            where guids.Contains(machine.Guid) && machine.IsMasterData
                            select machine;

                return new Collection<Machine>(query.ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the id of the parent project of a specified machine.
        /// </summary>
        /// <param name="machine">The machine.</param>
        /// <returns>The guid or Guid.Empty if an error occurs.</returns>
        public Guid GetParentProjectId(Machine machine)
        {
            if (machine == null)
            {
                throw new ArgumentNullException("machine", "Machine was null.");
            }

            try
            {
                return this.EntityContext.FindTopParentId(machine.Guid, "Machines", 1);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the id of the parent assembly of a specified machine.
        /// </summary>
        /// <param name="machine">The machine.</param>
        /// <returns>The guid or Guid.Empty if an error occurs.</returns>
        public Guid GetParentAssemblyId(Machine machine)
        {
            if (machine == null)
            {
                throw new ArgumentNullException("machine", "Machine was null.");
            }

            try
            {
                return this.EntityContext.FindTopParentId(machine.Guid, "Machines", 2);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the id of the machines's parent process step.
        /// </summary>
        /// <param name="machine">The machine.</param>
        /// <returns>The guid or Guid.Empty if no parent process step is found.</returns>
        public Guid GetParentProcessStepId(Machine machine)
        {
            var query = from mac in this.EntityContext.MachineSet
                            .Include(p => p.ProcessStep)
                        where mac.Guid == machine.Guid
                        select mac.ProcessStep;

            try
            {
                var processStep = query.FirstOrDefault();
                return processStep != null ? processStep.Guid : Guid.Empty;
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Deletes the specified machine from the repository using a stored procedure.
        /// The actual deletion from the database is performed during this call (when the stored procedure is called), not when SaveChanges() is called.
        /// </summary>
        /// <param name="machine">The machine to delete.</param>
        public void DeleteByStoredProcedure(Machine machine)
        {
            if (machine == null)
            {
                throw new ArgumentNullException("machine", "The machine was null.");
            }

            var originalCommandTimeout = this.EntityContext.CommandTimeout;

            try
            {
                this.EntityContext.CommandTimeout = 600;
                this.EntityContext.DeleteMachine(machine.Guid);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, SourceDatabase);
            }
            finally
            {
                this.EntityContext.CommandTimeout = originalCommandTimeout;
            }
        }
    }
}
