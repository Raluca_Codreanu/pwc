﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Implements operations on RawMaterials.
    /// </summary>
    internal class RawMaterialRepository : Repository<RawMaterial>, IRawMaterialRepository
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// A query that fully loads raw materials.
        /// </summary>
        private IQueryable<RawMaterial> queryFull;

        /// <summary>
        /// Initializes a new instance of the <see cref="RawMaterialRepository"/> class.
        /// </summary>
        /// <param name="entityContext">The object context to be used for data operations.</param>
        /// <param name="sourceDatabase">A value indicating the database on which this repository operates.</param>
        public RawMaterialRepository(DataEntities entityContext, DbIdentifier sourceDatabase)
            : base(entityContext, sourceDatabase)
        {
            this.queryFull = from material in this.EntityContext.RawMaterialSet
                                 .Include(m => m.Part)
                                 .Include(m => m.MaterialsClassificationL1)
                                 .Include(m => m.MaterialsClassificationL2)
                                 .Include(m => m.MaterialsClassificationL3)
                                 .Include(m => m.MaterialsClassificationL4)
                                 .Include(m => m.PriceUnitBase)
                                 .Include(m => m.DeliveryType)
                                 .Include(m => m.ParentWeightUnitBase)
                                 .Include(m => m.QuantityUnitBase)
                                 .Include(m => m.Manufacturer.Owner)
                                 .Include(m => m.Owner)
                             select material;
        }

        /// <summary>
        /// Retrieves the raw material based on the id.
        /// </summary>
        /// <param name="id">The raw material id.</param>
        /// <param name="noTracking">Option specifying if the MergeOption should be NoTracking.</param>
        /// <returns>The raw material.</returns>
        public RawMaterial GetById(Guid id, bool noTracking = false)
        {
            return this.GetByIds(new List<Guid> { id }, noTracking).FirstOrDefault();
        }

        /// <summary>
        /// Retrieves the raw material based on the ids list.
        /// </summary>
        /// <param name="ids">The ids of the raw materials to get.</param>
        /// <param name="noTracking">Option specifying if the MergeOption should be NoTracking.</param>
        /// <returns>A collection of raw materials.</returns>
        public Collection<RawMaterial> GetByIds(IEnumerable<Guid> ids, bool noTracking = false)
        {
            var query = from material in this.queryFull
                        where ids.Contains(material.Guid)
                        select material;

            if (noTracking)
            {
                query = query.AsNoTracking();
            }
            else
            {
                query.SetMergeOption(MergeOption.PreserveChanges);
            }

            try
            {
                return new Collection<RawMaterial>(query.ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Retrieves the master data raw materials.
        /// </summary>
        /// <returns>
        /// The collection of master data raw materials
        /// </returns>
        public Collection<RawMaterial> GetAllMasterData()
        {
            var query = from material in this.queryFull
                        where material.IsMasterData
                        select material;

            try
            {
                return new Collection<RawMaterial>(query.ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the id of the project to which a specified material belongs to.
        /// </summary>
        /// <param name="rawMaterial">The raw material.</param>
        /// <returns>
        /// The id of the project or Guid.Empty if the material does not have a parent project.
        /// </returns>
        public Guid GetParentProjectId(RawMaterial rawMaterial)
        {
            if (rawMaterial == null)
            {
                throw new ArgumentNullException("rawMaterial", "The raw material was null.");
            }

            try
            {
                return this.EntityContext.FindTopParentId(rawMaterial.Guid, "RawMaterials", 1);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the id of the assembly to which a specified material belongs to.
        /// </summary>
        /// <param name="rawMaterial">The raw material.</param>
        /// <returns>
        /// The id of the assembly or Guid.Empty if the material does not have a parent assembly.
        /// </returns>
        public Guid GetParentAssemblyId(RawMaterial rawMaterial)
        {
            if (rawMaterial == null)
            {
                throw new ArgumentNullException("rawMaterial", "The raw material was null.");
            }

            try
            {
                return this.EntityContext.FindTopParentId(rawMaterial.Guid, "RawMaterials", 2);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the id of the raw material's parent part.
        /// </summary>
        /// <param name="rawMaterial">The raw material.</param>
        /// <returns>The guid or Guid.Empty if no parent part is found.</returns>
        public Guid GetParentPartId(RawMaterial rawMaterial)
        {
            var query = from rawM in this.EntityContext.RawMaterialSet
                            .Include(p => p.Part)
                        where rawM.Guid == rawMaterial.Guid
                        select rawM.Part;

            try
            {
                var part = query.FirstOrDefault();
                return part != null ? part.Guid : Guid.Empty;
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Deletes the specified material from the repository using a stored procedure. All sub-entities owned by the material are also deleted.
        /// The actual deletion from the database is performed during this call (when the stored procedure is called), not when SaveChanges() is called.
        /// </summary>
        /// <param name="material">The material to delete.</param>
        public void DeleteByStoredProcedure(RawMaterial material)
        {
            if (material == null)
            {
                throw new ArgumentNullException("material", "The material was null.");
            }

            var originalCommandTimeout = this.EntityContext.CommandTimeout;

            try
            {
                this.EntityContext.CommandTimeout = 600;
                this.EntityContext.DeleteRawMaterial(material.Guid);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, SourceDatabase);
            }
            finally
            {
                this.EntityContext.CommandTimeout = originalCommandTimeout;
            }
        }
    }
}
