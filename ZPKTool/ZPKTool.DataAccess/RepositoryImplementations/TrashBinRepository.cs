﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Entity;
using System.Linq;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Implements the Trash Bin operations.
    /// </summary>
    internal class TrashBinRepository : Repository<TrashBinItem>, ITrashBinRepository
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Initializes a new instance of the <see cref="TrashBinRepository"/> class.
        /// </summary>
        /// <param name="entityContext">The object context to be used for data operations.</param>
        /// <param name="sourceDatabase">A value indicating the database on which this repository operates.</param>
        public TrashBinRepository(DataEntities entityContext, DbIdentifier sourceDatabase)
            : base(entityContext, sourceDatabase)
        {
        }

        #region Get

        /// <summary>
        /// Gets the number of trash bin items belonging to a specified user.
        /// </summary>
        /// <param name="userId">The id of the user whose trash bin items to count.</param>
        /// <returns>
        /// The number of trash bin items belonging to the specified user.
        /// </returns>
        public int GetItemCount(Guid userId)
        {
            try
            {
                var query = from item in this.EntityContext.TrashBinItemSet
                            where item.Owner.Guid == userId
                            select item;

                return query.Count();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets all trash bin items.
        /// </summary>
        /// <param name="ownerId">The id of the user whose items to get.</param>
        /// <param name="noTracking">if set to true change tracking is disabled for the returned object(s)..</param>
        /// <returns>
        /// A collection of trash bin items.
        /// </returns>
        public Collection<TrashBinItem> GetAll(Guid ownerId, bool noTracking = false)
        {
            var query = from item in this.EntityContext.TrashBinItemSet
                        where item.Owner.Guid == ownerId
                        select item;

            if (noTracking)
            {
                query = query.AsNoTracking();
            }

            try
            {
                return new Collection<TrashBinItem>(query.ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the trash bin item corresponding to a specified entity.
        /// </summary>
        /// <param name="entityId">The entity id.</param>
        /// <returns>
        /// The trash bin item corresponding to the entity or null if the entity is not deleted.
        /// </returns>
        public TrashBinItem GetItemOfObject(Guid entityId)
        {
            var query = from item in this.EntityContext.TrashBinItemSet
                        where item.EntityGuid == entityId
                        select item;

            try
            {
                return query.FirstOrDefault();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        #endregion

        #region Delete & Restore

        /// <summary>
        /// Restores the specified item from the trash bin.
        /// </summary>
        /// <param name="obj">The object to undelete.</param>
        public void RestoreFromTrashBin(ITrashable obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("obj", "The object to restore was null.");
            }

            try
            {
                // Un-set the IsDeleted flag on the item and all relevant sub-objects
                obj.SetIsDeleted(false);

                // Remove the trash bin entry
                DeleteItemOfObjectAndChildren(obj);
            }
            catch (DataException ex)
            {
                log.ErrorException("Error while restoring an object from the trash bin.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Deletes the specified item to the trash bin.
        /// </summary>
        /// <param name="obj">The object to delete.</param>
        public void DeleteToTrashBin(ITrashable obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("obj", "The object to delete to trash bin was null.");
            }

            try
            {
                // Set the IsDeleted flag on the item and all relevant sub-objects
                obj.SetIsDeleted(true);

                // Add the trash bin entry
                AddItemForObject(obj);
            }
            catch (DataException ex)
            {
                log.ErrorException("Error while deleting an object to trash bin.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Adds a trash bin item for the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        public void AddItemForObject(ITrashable obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("obj", "The object was null.");
            }

            TrashBinItem tbi = new TrashBinItem();
            tbi.EntityType = obj.TrashBinItemType;
            tbi.EntityGuid = obj.Guid;
            tbi.Owner = obj.Owner;
            tbi.EntityName = obj.Name;

            Add(tbi);
        }

        /// <summary>
        /// Deletes the trash bin item of the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        public void DeleteItemOfObject(ITrashable obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("obj", "The object was null.");
            }

            TrashBinItem tbi = GetItemOfObject(obj.Guid);
            if (tbi != null)
            {
                this.RemoveAll(tbi);
            }
        }

        #endregion

        #region Cascade Delete Trashbin Items

        /// <summary>
        /// Deletes the tbi of an assembly and its children if there are any
        /// </summary>
        /// <param name="assembly">The assembly for which we delete the tbi</param>
        private void DeleteItemOfAssembly(Assembly assembly)
        {
            DeleteItemOfObject(assembly);

            foreach (Assembly subassy in assembly.Subassemblies)
            {
                DeleteItemOfAssembly(subassy);
            }

            foreach (Part part in assembly.Parts)
            {
                DeleteItemOfPart(part);
            }

            foreach (ProcessStep step in assembly.Process.Steps)
            {
                foreach (Machine machine in step.Machines)
                {
                    DeleteItemOfObject(machine);
                }

                foreach (Die die in step.Dies)
                {
                    DeleteItemOfObject(die);
                }

                foreach (Commodity comm in step.Commodities)
                {
                    DeleteItemOfObject(comm);
                }

                foreach (Consumable cons in step.Consumables)
                {
                    DeleteItemOfObject(cons);
                }
            }
        }

        /// <summary>
        /// Deletes the tbi of a part and its children if there are any
        /// </summary>
        /// <param name="part">The part for which we delete the tbi</param>
        private void DeleteItemOfPart(Part part)
        {
            DeleteItemOfObject(part);

            foreach (RawMaterial material in part.RawMaterials)
            {
                DeleteItemOfObject(material);
            }

            foreach (Commodity commodity in part.Commodities)
            {
                DeleteItemOfObject(commodity);
            }

            foreach (ProcessStep step in part.Process.Steps)
            {
                foreach (Machine machine in step.Machines)
                {
                    DeleteItemOfObject(machine);
                }

                foreach (Die die in step.Dies)
                {
                    DeleteItemOfObject(die);
                }

                foreach (Consumable cons in step.Consumables)
                {
                    DeleteItemOfObject(cons);
                }
            }

            if (part.RawPart != null)
            {
                DeleteItemOfPart(part.RawPart);
            }
        }

        /// <summary>
        /// Deletes the tbi of a project and its children if there are any
        /// </summary>
        /// <param name="project">The project for which we delete the tbi</param>
        private void DeleteItemOfProject(Project project)
        {
            DeleteItemOfObject(project);

            foreach (Assembly assy in project.Assemblies)
            {
                DeleteItemOfAssembly(assy);
            }

            foreach (Part part in project.Parts)
            {
                DeleteItemOfPart(part);
            }
        }

        /// <summary>
        /// Deletes the tbi of a folder and its children if there are any
        /// </summary>
        /// <param name="folder">The folder for which we delete the tbi</param>
        private void DeleteItemOfFolder(ProjectFolder folder)
        {
            DeleteItemOfObject(folder);

            foreach (Project project in folder.Projects)
            {
                DeleteItemOfProject(project);
            }

            foreach (ProjectFolder subfolder in folder.ChildrenProjectFolders)
            {
                DeleteItemOfFolder(subfolder);
            }
        }

        /// <summary>
        /// Deletes all the tbi associated with the input object, including the tbi's of the children of the input object.
        /// </summary>
        /// <param name="obj">The obj.</param>
        private void DeleteItemOfObjectAndChildren(ITrashable obj)
        {
            ProjectFolder folder = obj as ProjectFolder;
            if (folder != null)
            {
                DeleteItemOfFolder(folder);
                return;
            }

            Project project = obj as Project;
            if (project != null)
            {
                DeleteItemOfProject(project);
                return;
            }

            Assembly assembly = obj as Assembly;
            if (assembly != null)
            {
                DeleteItemOfAssembly(assembly);
                return;
            }

            Part part = obj as Part;
            if (part != null)
            {
                DeleteItemOfPart(part);
                return;
            }

            // if the obj has no possible children in the trash bin, delete only its tbi.
            DeleteItemOfObject(obj);
        }

        #endregion

        /// <summary>
        /// Deletes the specified trash bin item from the repository using a stored procedure. All sub-entities owned by the item are also deleted.
        /// The actual deletion from the database is performed during this call (when the stored procedure is called), not when SaveChanges() is called.
        /// </summary>
        /// <param name="trashBinItem">The trash bin item to delete.</param>
        public void DeleteByStoredProcedure(TrashBinItem trashBinItem)
        {
            if (trashBinItem == null)
            {
                throw new ArgumentNullException("trashBinItem", "The trash bin item was null.");
            }

            var originalCommandTimeout = this.EntityContext.CommandTimeout;

            try
            {
                this.EntityContext.CommandTimeout = 600;
                this.EntityContext.DeleteTrashBinItem(trashBinItem.Guid);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, SourceDatabase);
            }
            finally
            {
                this.EntityContext.CommandTimeout = originalCommandTimeout;
            }
        }
    }
}
