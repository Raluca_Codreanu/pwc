﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Implements operations for Project.
    /// </summary>
    internal class ProjectRepository : Repository<Project>, IProjectRepository
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectRepository"/> class.
        /// </summary>
        /// <param name="entityContext">The object context to be used for data operations.</param>
        /// <param name="sourceDatabase">A value indicating the database on which this repository operates.</param>
        public ProjectRepository(DataEntities entityContext, DbIdentifier sourceDatabase)
            : base(entityContext, sourceDatabase)
        {
        }

        #region Get

        /// <summary>
        /// Gets a specified project with the purpose of being edited.
        /// The following references are loaded: Customer, OverheadSettings, ProjectLeader, ResponsibleCalculator, Owner.
        /// </summary>
        /// <param name="projectId">The id of the project to retrieve</param>
        /// <returns>
        /// The project or null if not found.
        /// </returns>
        public Project GetProjectForEdit(Guid projectId)
        {
            try
            {
                var query = from proj in this.EntityContext.ProjectSet
                                .Include((p) => p.Customer)
                                .Include((p) => p.OverheadSettings)
                                .Include((p) => p.ProjectLeader)
                                .Include((p) => p.ResponsibleCalculator)
                                .Include((p) => p.Owner)
                                .Include(p => p.BaseCurrency.CurrenciesExchangeRateHistory)
                                .Include(p => p.Currencies)
                            where proj.Guid == projectId
                            select proj;

                return query.FirstOrDefault();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets a project to view its details in trash bin.
        /// </summary>
        /// <param name="projectId">The project id.</param>
        /// <returns>The project.</returns>
        public Project GetProjectForView(Guid projectId)
        {
            try
            {
                var query = from project in this.EntityContext.ProjectSet
                                .Include(p => p.Customer)
                                .Include(p => p.ProjectLeader)
                                .Include(p => p.ResponsibleCalculator)
                                .Include(p => p.OverheadSettings)
                                .Include(p => p.BaseCurrency.CurrenciesExchangeRateHistory)
                                .Include(p => p.Currencies)
                            where project.Guid == projectId
                            select project;

                return query.AsNoTracking().FirstOrDefault();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets a specified project to be displayed projects tree. The following references are loaded: Customer, OverheadSettings, ProjectFolder.
        /// </summary>
        /// <param name="projectId">The project id.</param>
        /// <returns>A project, or null if it is not found.</returns>
        public Project GetProjectForProjectsTree(Guid projectId)
        {
            try
            {
                var query = from project in this.EntityContext.ProjectSet
                                .Include(p => p.Customer)
                                .Include(p => p.OverheadSettings)
                                .Include(p => p.ProjectFolder)
                                .Include(p => p.Owner)
                            where project.Guid == projectId && !project.IsDeleted
                            select project;

                query.SetMergeOption(MergeOption.PreserveChanges);
                return query.FirstOrDefault();
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets a specified project with all sub-objects and collections fully loaded
        /// </summary>
        /// <param name="projectId">The project id.</param>
        /// <returns>The project or null if not found.</returns>
        public Project GetProjectFull(Guid projectId)
        {
            try
            {
                var query = from proj in this.EntityContext.ProjectSet
                                .Include(p => p.Customer)
                                .Include(p => p.OverheadSettings)
                                .Include(p => p.Owner)
                                .Include(p => p.ProjectLeader)
                                .Include(p => p.ResponsibleCalculator)
                                .Include(p => p.Assemblies)
                                .Include(p => p.Parts)
                                .Include(p => p.BaseCurrency.CurrenciesExchangeRateHistory)
                                .Include(p => p.Currencies)
                            where proj.Guid == projectId
                            select proj;

                var project = query.FirstOrDefault();
                if (project != null)
                {
                    if (project.Assemblies.Count > 0)
                    {
                        var assyIds = project.Assemblies.Select(assy => assy.Guid);

                        var assemblyRepository = new AssemblyRepository(this.EntityContext, this.SourceDatabase);
                        assemblyRepository.GetAssembliesFull(assyIds);
                    }

                    if (project.Parts.Count > 0)
                    {
                        var partsIds = project.Parts.Select(part => part.Guid);

                        var partRepository = new PartRepository(this.EntityContext, this.SourceDatabase);
                        partRepository.GetPartsFull(partsIds);
                    }
                }

                return project;
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets a specified project with first level of sub-objects and collections.
        /// </summary>
        /// <param name="projectId">The project id.</param>
        /// <param name="noTracking">if set to true change tracking is disabled for the returned value.</param>
        /// <returns>
        /// The project with first level of assemblies and parts.
        /// </returns>
        public Project GetProjectIncludingTopLevelChildren(Guid projectId, bool noTracking = false)
        {
            try
            {
                var query = from proj in this.EntityContext.ProjectSet
                                .Include(p => p.Customer)
                                .Include(p => p.OverheadSettings)
                                .Include(p => p.Owner)
                                .Include(p => p.ProjectLeader)
                                .Include(p => p.ResponsibleCalculator)
                                .Include(p => p.Assemblies)
                                .Include(p => p.Parts)
                                .Include(p => p.BaseCurrency.CurrenciesExchangeRateHistory)
                                .Include(p => p.Currencies)
                            where proj.Guid == projectId
                            select proj;

                if (noTracking)
                {
                    query = query.AsNoTracking();
                }
                else
                {
                    query.SetMergeOption(MergeOption.PreserveChanges);
                }

                Project project = query.FirstOrDefault();
                return project;
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the maximum number of projects to display on the welcome view for a specified user. Only the 1st image of each project is loaded.
        /// </summary>        
        /// <param name="userGuid">The guid of the user whose projects to get.</param>
        /// <param name="parentFolderId">The id of the parent folder</param>
        /// <param name="maxNumberOfProjects">The maximum number of projects to display.</param>
        /// <returns>
        /// A collection containing the user's projects.
        /// </returns>
        public Collection<Project> GetProjectsForWelcomeView(Guid userGuid, Guid parentFolderId, int maxNumberOfProjects)
        {
            try
            {
                var projectsQuery = from proj in this.EntityContext.ProjectSet
                                        .Include(p => p.Owner)
                                    where !proj.IsReleased && !proj.IsDeleted && proj.Owner.Guid == userGuid
                                        && ((proj.ProjectFolder.Guid == parentFolderId) || (parentFolderId == Guid.Empty && proj.ProjectFolder == null))
                                    orderby proj.Name
                                    select proj;

                List<Project> projects = projectsQuery.Take(maxNumberOfProjects).ToList();
                List<Guid> projectIds = projects.Select(p => p.Guid).ToList();

                // Get the 1st image of each project
                var mediaQuery = from media in this.EntityContext.MediaSet
                                 where media.Type == (short)MediaType.Image && projectIds.Contains(media.Project.Guid)
                                 group media by media.Project.Guid into projGroups
                                 select projGroups.FirstOrDefault();

                // Materialize the media so they are linked to their projects                
                mediaQuery.ToList();

                return new Collection<Project>(projects);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets projects of a specified user to be displayed projects tree. The following references are loaded: Customer, OverheadSettings.
        /// </summary>
        /// <param name="ownerId">The owner id.</param>
        /// <param name="folderId">The id of the folder whose projects to return.
        /// Setting it to empty will return the projects without a parent folder (root folders).</param>
        /// <returns>
        /// A collection of projects.
        /// </returns>
        public Collection<Project> GetProjectsForProjectsTree(Guid ownerId, Guid folderId)
        {
            try
            {
                // Select all projects with the specified owner that are not deleted
                var baseQuery = from project in this.EntityContext.ProjectSet
                                     .Include(p => p.Customer)
                                     .Include(p => p.OverheadSettings)
                                     .Include(p => p.Owner)
                                where project.Owner.Guid == ownerId && !project.IsDeleted
                                select project;

                // Select either the projects without a parent folder or the projects with the specified parent folder.
                IQueryable<Project> query = null;
                if (folderId != Guid.Empty)
                {
                    query = from project in baseQuery
                            where project.ProjectFolder.Guid == folderId
                            select project;
                }
                else
                {
                    query = from project in baseQuery
                            where project.ProjectFolder == null
                            select project;
                }

                query.SetMergeOption(MergeOption.PreserveChanges);
                return new Collection<Project>(query.ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the ids for all projects of a specified user.
        /// </summary>
        /// <param name="userID">The specified user id.</param>
        /// <returns>A list containing the ids of all projects for a specified user.</returns>
        public IEnumerable<Guid> GetProjectsId(Guid userID)
        {
            var query = from project in this.EntityContext.ProjectSet
                        where project.Owner.Guid == userID
                        select project.Guid;

            try
            {
                return query.ToList();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the projects of a specified user, that are bookmarked, to be displayed in bookmarks tree.
        /// </summary>
        /// <param name="ownerId">The owner id.</param>
        /// <returns>A collection of projects.</returns>
        public Collection<Project> GetBookmarkedProjects(Guid ownerId)
        {
            try
            {
                var query = from project in this.EntityContext.ProjectSet
                            join bookmark in this.EntityContext.BookmarkSet on project.Guid equals bookmark.EntityId
                            where project.Owner.Guid == ownerId && !project.IsDeleted && bookmark.EntityTypeId == BookmarkedItemType.Project
                            select project;

                return new Collection<Project>(query.ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the released projects to be displayed projects tree. The following references are loaded: Customer, OverheadSettings.
        /// </summary>
        /// <param name="folderId">The folder id.</param>
        /// <returns>
        /// A collection containing the released projects.
        /// </returns>
        public Collection<Project> GetReleasedProjectsForProjectsTree(Guid folderId)
        {
            try
            {
                var baseQuery = from project in this.EntityContext.ProjectSet
                                .Include(p => p.Customer)
                                .Include(p => p.OverheadSettings)
                                where project.IsReleased && !project.IsDeleted
                                select project;

                // Select either the projects without a parent folder or the projects with the specified parent folder.
                IQueryable<Project> query = null;
                if (folderId != Guid.Empty)
                {
                    query = from project in baseQuery
                            where project.ProjectFolder.Guid == folderId
                            select project;
                }
                else
                {
                    query = from project in baseQuery
                            where project.ProjectFolder == null
                            select project;
                }

                query.SetMergeOption(MergeOption.PreserveChanges);
                return new Collection<Project>(query.ToList());
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the projects of another user.
        /// </summary>
        /// <param name="userId">The id of the user whose projects to get.</param>
        /// <param name="leaderOrCalculatorId">
        /// The id of the project leader or responsible calculator that the projects must have. If this is set to Guid.Empty it will return all projects of the user,
        /// otherwise it will return all projects of the user that have the project leader or responsible calculator with the id <paramref name="leaderOrCalculatorId"/>.
        /// </param>
        /// <returns>A collection of projects.</returns>
        public Collection<Project> GetOtherUserProjects(Guid userId, Guid leaderOrCalculatorId)
        {
            var baseQuery = from project in this.EntityContext.ProjectSet
                                .Include(p => p.Customer)
                                .Include(p => p.OverheadSettings)
                                .Include(p => p.ProjectFolder)
                                .Include(p => p.Owner)
                            where project.Owner.Guid == userId && !project.IsDeleted
                            select project;

            IQueryable<Project> query = null;
            if (leaderOrCalculatorId != Guid.Empty)
            {
                query = from project in baseQuery
                        where project.ProjectLeader.Guid == leaderOrCalculatorId || project.ResponsibleCalculator.Guid == leaderOrCalculatorId
                        select project;
            }
            else
            {
                query = baseQuery;
            }

            try
            {
                query.SetMergeOption(MergeOption.PreserveChanges);
                var projects = query.ToList();

                // Load each project's folder tree                                
                // TODO: load all folders of all projects in 1 query - TVF (or at least all folders of each project in 1 query).
                foreach (Project project in projects)
                {
                    ProjectFolder folder = project.ProjectFolder;
                    while (folder != null)
                    {
                        var getParentFolderQuery = from f1 in this.EntityContext.ProjectFolderSet
                                                   join f2 in this.EntityContext.ProjectFolderSet on f1.ParentProjectFolder equals f2
                                                   where f1.Guid == folder.Guid
                                                   select f2;
                        var parentFolder = getParentFolderQuery.FirstOrDefault();
                        folder = parentFolder;
                    }
                }

                return new Collection<Project>(projects);
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the parent project from the db (if it is not loaded already).
        /// </summary>
        /// <param name="entity">The entity to get its parent.</param>
        /// <param name="noTracking">if set to true disable change tracking for the retuned object(s).</param>
        /// <returns>The parent entity of the given entity</returns>
        public Project GetParentProject(object entity, bool noTracking = false)
        {
            Guid projectGuid = Guid.Empty;

            try
            {
                if (entity is Project)
                {
                    projectGuid = ((Project)entity).Guid;
                }
                else if (entity is Assembly)
                {
                    var assemblyRepository = new AssemblyRepository(this.EntityContext, this.SourceDatabase);
                    projectGuid = assemblyRepository.GetParentProjectId((Assembly)entity);
                }
                else if (entity is Part)
                {
                    var partRepository = new PartRepository(this.EntityContext, this.SourceDatabase);
                    projectGuid = partRepository.GetParentProjectId((Part)entity);
                }
                else if (entity is Machine)
                {
                    var machineRepository = new MachineRepository(this.EntityContext, this.SourceDatabase);
                    projectGuid = machineRepository.GetParentProjectId((Machine)entity);
                }
                else if (entity is RawMaterial)
                {
                    var rawMaterialRepository = new RawMaterialRepository(this.EntityContext, this.SourceDatabase);
                    projectGuid = rawMaterialRepository.GetParentProjectId((RawMaterial)entity);
                }
                else if (entity is Consumable)
                {
                    var consumableRepository = new ConsumableRepository(this.EntityContext, this.SourceDatabase);
                    projectGuid = consumableRepository.GetParentProjectId((Consumable)entity);
                }
                else if (entity is Commodity)
                {
                    var commodityRepository = new CommodityRepository(this.EntityContext, this.SourceDatabase);
                    projectGuid = commodityRepository.GetParentProjectId((Commodity)entity);
                }
                else if (entity is Die)
                {
                    var dieRepository = new DieRepository(this.EntityContext, this.SourceDatabase);
                    projectGuid = dieRepository.GetParentProjectId((Die)entity);
                }
                else if (entity is ProcessStep)
                {
                    var processStepRepository = new ProcessStepRepository(this.EntityContext, this.SourceDatabase);
                    projectGuid = processStepRepository.GetParentProjectId((ProcessStep)entity);
                }
                else if (entity is Process)
                {
                    var processRepository = new ProcessRepository(this.EntityContext, this.SourceDatabase);
                    projectGuid = processRepository.GetParentProjectId((Process)entity);
                }
                else if (entity is TransportCostCalculation)
                {
                    var transportCostCalculationRepository = new TransportCostCalculationRepository(this.EntityContext, this.SourceDatabase);
                    projectGuid = transportCostCalculationRepository.GetParentProjectId((TransportCostCalculation)entity);
                }
                else if (entity is TransportCostCalculationSetting)
                {
                    var transportCostCalculationSettingRepository = new TransportCostCalculationSettingRepository(this.EntityContext, this.SourceDatabase);
                    projectGuid = transportCostCalculationSettingRepository.GetParentProjectId((TransportCostCalculationSetting)entity);
                }

                // Load the parent project.
                // Its parent folder and owner are needed for the projects tree navigation logic.
                if (projectGuid != Guid.Empty)
                {
                    var query = from project in this.EntityContext.ProjectSet
                                    .Include(p => p.ProjectFolder)
                                    .Include(p => p.Owner)
                                    .Include(p => p.BaseCurrency.CurrenciesExchangeRateHistory)
                                where project.Guid == projectGuid
                                select project;

                    if (noTracking)
                    {
                        query = query.AsNoTracking();
                    }

                    return query.FirstOrDefault();
                }
                else
                {
                    return null;
                }
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the hierarchy of folders to which a project belongs.        
        /// </summary>
        /// <param name="projectId">The project id.</param>
        /// <returns>
        /// A collection containing the IDs of the folders in the hierarchy. The 1st item in the collection is the direct parent of the project
        /// while the last one is the root of the hierarchy.
        /// </returns>
        public Collection<Guid> GetProjectParentFolderHierarchy(Guid projectId)
        {
            try
            {
                return this.EntityContext.GetProjectParentFolderHierarchy(projectId);
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the offline projects.
        /// </summary>
        /// <param name="ownerGuid">The owner GUID.</param>
        /// <returns>
        /// List of Guids representing the Guid of the offline projects
        /// </returns>
        public ICollection<Guid> GetOfflineProjects(Guid ownerGuid)
        {
            try
            {
                var offlineProjectsQuery = from proj in this.EntityContext.ProjectSet
                                        .Include(p => p.Owner)
                                           where proj.Owner.Guid == ownerGuid &&
                                                 proj.IsOffline == true
                                           select proj.Guid;

                return offlineProjectsQuery.ToList();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets all non-deleted released projects.
        /// </summary>
        /// <returns>Collection of released projects.</returns>
        public Collection<Project> GetReleasedProjects()
        {
            try
            {
                var query = (from project in this.EntityContext.ProjectSet
                             where !project.IsDeleted && project.IsReleased
                             select project).OrderBy(f => f.Name);

                return new Collection<Project>(query.ToList());
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets all projects of a folder.
        /// </summary>
        /// <param name="folderId">The folder id.</param>
        /// <returns>
        /// Collection of projects.
        /// </returns>
        public Collection<Project> GetProjectsOfFolder(Guid folderId)
        {
            try
            {
                var query = from project in this.EntityContext.ProjectSet
                            where project.ProjectFolder.Guid == folderId
                            select project;

                return new Collection<Project>(query.ToList());
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Retrieves the projects based on the ids list.
        /// </summary>
        /// <param name="ids">The ids of the projects to get.</param>
        /// <returns>A collection of projects.</returns>
        public Collection<Project> GetByIds(IEnumerable<Guid> ids)
        {
            var query = from project in this.EntityContext.ProjectSet
                            .Include(p => p.Customer)
                            .Include(l => l.ProjectLeader)
                            .Include(r => r.ResponsibleCalculator)
                        where ids.Contains(project.Guid)
                        select project;

            try
            {
                return new Collection<Project>(query.AsNoTracking().ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the id of the project's parent folder.
        /// </summary>
        /// <param name="project">The project.</param>
        /// <returns>The guid or Guid.Empty if no parent folder is found.</returns>
        public Guid GetParentFolderId(Project project)
        {
            var query = from proj in this.EntityContext.ProjectSet
                            .Include(p => p.ProjectFolder)
                        where proj.Guid == project.Guid
                        select proj.ProjectFolder;

            try
            {
                var projFolder = query.FirstOrDefault();
                return projFolder != null ? projFolder.Guid : Guid.Empty;
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes the specified project from the repository using a stored procedure. All sub-entities owned by the project are also deleted.
        /// The actual deletion from the database is performed during this call (when the stored procedure is called), not when SaveChanges() is called.
        /// </summary>
        /// <param name="project">The project to delete.</param>
        public void DeleteByStoredProcedure(Project project)
        {
            if (project == null)
            {
                throw new ArgumentNullException("project", "The project was null.");
            }

            var originalCommandTimeout = this.EntityContext.CommandTimeout;

            try
            {
                this.EntityContext.CommandTimeout = 600;
                this.EntityContext.DeleteProject(project.Guid);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, SourceDatabase);
            }
            finally
            {
                this.EntityContext.CommandTimeout = originalCommandTimeout;
            }
        }

        #endregion
    }
}
