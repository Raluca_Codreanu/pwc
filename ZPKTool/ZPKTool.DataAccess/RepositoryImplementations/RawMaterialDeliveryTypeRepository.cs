﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Implements the data operations of <see cref="RawMaterialDeliveryType"/>.
    /// </summary>
    internal class RawMaterialDeliveryTypeRepository : Repository<RawMaterialDeliveryType>, IRawMaterialDeliveryTypeRepository
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Initializes a new instance of the <see cref="RawMaterialDeliveryTypeRepository"/> class.
        /// </summary>
        /// <param name="entityContext">The object context to be used for data operations.</param>
        /// <param name="sourceDatabase">A value indicating the database on which this repository operates.</param>
        public RawMaterialDeliveryTypeRepository(DataEntities entityContext, DbIdentifier sourceDatabase)
            : base(entityContext, sourceDatabase)
        {
        }

        /// <summary>
        /// Gets a raw material delivery type based on its id.
        /// </summary>
        /// <param name="deliveryTypeId">The delivery type id.</param>
        /// <returns>
        /// The delivery type or null if is not found.
        /// </returns>
        public RawMaterialDeliveryType FindById(Guid deliveryTypeId)
        {            
            try
            {
                return this.EntityContext.RawMaterialDeliveryTypeSet.Find(deliveryTypeId);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets a raw material delivery type based on its name.
        /// </summary>
        /// <param name="deliveryTypeName">Name of the delivery type.</param>
        /// <returns>
        /// The delivery type or null if is not found.
        /// </returns>
        public RawMaterialDeliveryType FindByName(string deliveryTypeName)
        {
            var query = from dt in this.EntityContext.RawMaterialDeliveryTypeSet
                        where dt.Name == deliveryTypeName && !dt.IsReleased
                        select dt;

            try
            {
                return query.FirstOrDefault();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets all raw material delivery types.
        /// </summary>
        /// <returns>
        /// A collection of RawMaterialDeliveryType objects.
        /// </returns>
        public Collection<RawMaterialDeliveryType> FindAll()
        {
            var query = (from dt in this.EntityContext.RawMaterialDeliveryTypeSet
                         where !dt.IsReleased
                         select dt).OrderBy(u => u.Name);
            
            try
            {
                return new Collection<RawMaterialDeliveryType>(query.ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }
    }
}
