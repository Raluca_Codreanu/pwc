﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Implements operations for Commodity Price History Repository.
    /// </summary>
    internal class CommodityPriceHistoryRepository : Repository<CommoditiesPriceHistory>, ICommodityPriceHistoryRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommodityPriceHistoryRepository" /> class.
        /// </summary>
        /// <param name="entityContext">The object context to be used for data operations.</param>
        /// <param name="sourceDatabase">A value indicating the database on which this repository operates.</param>
        public CommodityPriceHistoryRepository(DataEntities entityContext, DbIdentifier sourceDatabase)
            : base(entityContext, sourceDatabase)
        {
        }
    }
}
