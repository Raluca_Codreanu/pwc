﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Implements operations for Part.
    /// </summary>
    internal class PartRepository : Repository<Part>, IPartRepository
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Initializes a new instance of the <see cref="PartRepository"/> class.
        /// </summary>
        /// <param name="entityContext">The object context to be used for data operations.</param>
        /// <param name="sourceDatabase">A value indicating the database on which this repository operates.</param>
        public PartRepository(DataEntities entityContext, DbIdentifier sourceDatabase)
            : base(entityContext, sourceDatabase)
        {
        }

        #region Get

        /// <summary>
        /// Gets the part with the specified id fully loaded.
        /// </summary>
        /// <param name="partId">The part id.</param>
        /// <returns>The part or null if it was not found.</returns>
        public Part GetPartFull(Guid partId)
        {
            return this.LoadPartsFull(new List<Guid>() { partId }, true).FirstOrDefault();
        }

        /// <summary>
        /// Gets the parts with the specified ids fully loaded.
        /// </summary>
        /// <param name="partsIds">The list of parts ids.</param>
        /// <returns>The parts fully loaded.</returns>
        public IEnumerable<Part> GetPartsFull(IEnumerable<Guid> partsIds)
        {
            return this.LoadPartsFull(partsIds, true);
        }

        /// <summary>
        /// Gets the part with the specified id. No references are loaded.
        /// </summary>
        /// <param name="partId">The part id.</param>
        /// <param name="noTracking">if set to true disable change tracking for the retuned object(s).</param>
        /// <returns>The part or null if it is not found.</returns>
        public Part GetPart(Guid partId, bool noTracking = true)
        {
            try
            {
                var query = from part in this.EntityContext.PartSet
                            where part.Guid == partId
                            select part;

                if (noTracking)
                {
                    query = query.AsNoTracking();
                }

                return query.FirstOrDefault();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the part with its owner
        /// </summary>
        /// <param name="partGuid">The guid of the part</param>
        /// <returns>The part with the owner reference</returns>
        public Part GetPartWithOwner(Guid partGuid)
        {
            try
            {
                var query = from part in this.EntityContext.PartSet
                                .Include(p => p.Owner)
                            where part.Guid == partGuid
                            select part;

                return query.FirstOrDefault();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, SourceDatabase);
            }
        }

        /// <summary>
        /// Gets a Part to view its details in trash bin.
        /// </summary>
        /// <param name="partId">The part id.</param>
        /// <returns>The Part with the specified id.</returns>
        public Part GetPartForView(Guid partId)
        {
            try
            {
                var query = from part in this.EntityContext.PartSet
                                .Include(p => p.CalculatorUser)
                                .Include(p => p.Manufacturer)
                                .Include(p => p.OverheadSettings)
                                .Include(p => p.CountrySettings)
                                .Include(p => p.MeasurementUnit)
                            where part.Guid == partId
                            select part;

                return query.AsNoTracking().FirstOrDefault();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the Part to which a specified entity belongs.
        /// </summary>
        /// <param name="entity">The entity to get it's parent.</param>
        /// <returns>The parent Part of the given entity.</returns>
        public Part GetParentPart(object entity)
        {
            DataEntities context = this.EntityContext;
            IQueryable<Part> query = null;
            Guid entityId = Guid.Empty;

            IIdentifiable identifiableObj = entity as IIdentifiable;
            if (identifiableObj != null)
            {
                entityId = identifiableObj.Guid;
            }

            if (entity is Machine)
            {
                query = from part in context.PartSet
                        join process in context.ProcessSet on part.Process equals process
                        join processStep in context.ProcessStepSet on process equals processStep.Process
                        join machine in context.MachineSet on processStep equals machine.ProcessStep
                        where machine.Guid == entityId
                        select part;
            }
            else if (entity is RawMaterial)
            {
                query = from part in context.PartSet
                        join material in context.RawMaterialSet on part equals material.Part
                        where material.Guid == entityId
                        select part;
            }
            else if (entity is Consumable)
            {
                query = from part in context.PartSet
                        join process in context.ProcessSet on part.Process equals process
                        join processStep in context.ProcessStepSet on process equals processStep.Process
                        join consumable in context.ConsumableSet on processStep equals consumable.ProcessStep
                        where consumable.Guid == entityId
                        select part;
            }
            else if (entity is Commodity)
            {
                query = from part in context.PartSet
                        join commodity in context.CommoditySet on part equals commodity.Part
                        where commodity.Guid == entityId
                        select part;
            }
            else if (entity is Die)
            {
                query = from part in context.PartSet
                        join process in context.ProcessSet on part.Process equals process
                        join processStep in context.ProcessStepSet on process equals processStep.Process
                        join die in context.DieSet on processStep equals die.ProcessStep
                        where die.Guid == entityId
                        select part;
            }
            else if (entity is ProcessStep)
            {
                query = from part in context.PartSet
                        join process in context.ProcessSet on part.Process equals process
                        join processStep in context.ProcessStepSet on process equals processStep.Process
                        where processStep.Guid == entityId
                        select part;
            }
            else if (entity is Process)
            {
                query = from part in context.PartSet
                        join process in context.ProcessSet on part.Process equals process
                        where process.Guid == entityId
                        select part;
            }
            else if (entity is RawPart)
            {
                query = from part in context.PartSet
                        where part.RawPart.Guid == entityId
                        select part;
            }

            if (entityId != Guid.Empty && query != null)
            {
                try
                {
                    return query.AsNoTracking().FirstOrDefault();
                }
                catch (DataException ex)
                {
                    log.ErrorException("Data error.", ex);
                    throw Utils.ParseDataException(ex, SourceDatabase);
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the parts of a project. The parts are barebones.
        /// </summary>
        /// <param name="projectGuid">The guid of the parent project.</param>
        /// <returns>The collection of child parts</returns>
        public Collection<Part> GetProjectParts(Guid projectGuid)
        {
            try
            {
                var query = from part in this.EntityContext.PartSet
                            where part.Project.Guid == projectGuid
                            select part;

                return new Collection<Part>(query.ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, SourceDatabase);
            }
        }

        /// <summary>
        /// Gets all master data parts.
        /// </summary>
        /// <returns>A collection containing Parts.</returns>
        public Collection<Part> GetAllMasterDataPart()
        {
            var query = from part in this.EntityContext.PartSet
                            .Include(p => p.Manufacturer)
                            .Include(p => p.CountrySettings)
                            .Include(p => p.OverheadSettings)
                            .Include(p => p.CalculatorUser)
                            .Include(p => p.MeasurementUnit)
                        where part.IsMasterData && !(part is RawPart)
                        select part;

            try
            {
                return new Collection<Part>(query.AsNoTracking().ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, SourceDatabase);
            }
        }

        /// <summary>
        /// Gets all master data raw parts.
        /// </summary>
        /// <returns>
        /// A collection containing RawParts
        /// </returns>
        public Collection<Part> GetAllMasterDataRawPart()
        {
            var query = from part in this.EntityContext.PartSet
                            .Include(p => p.Manufacturer)
                            .Include(p => p.CountrySettings)
                            .Include(p => p.OverheadSettings)
                            .Include(p => p.CalculatorUser)
                            .Include(p => p.MeasurementUnit)
                        where part.IsMasterData && part is RawPart
                        select part;

            try
            {
                return new Collection<Part>(query.AsNoTracking().ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, SourceDatabase);
            }
        }

        /// <summary>
        /// Retrieves the parts based on the ids list.
        /// </summary>
        /// <param name="ids">The ids of the parts to get.</param>
        /// <returns>A collection of parts.</returns>
        public Collection<Part> GetByIds(IEnumerable<Guid> ids)
        {
            var query = from part in this.EntityContext.PartSet
                            .Include(p => p.Manufacturer)
                        where ids.Contains(part.Guid)
                        select part;

            try
            {
                return new Collection<Part>(query.AsNoTracking().ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the id of the project in which a specified part exists at any level in the assembly hierarchy.
        /// </summary>
        /// <param name="part">The part.</param>
        /// <returns>
        /// The parent project's id or Guid.Empty if the part is not part of a project.
        /// </returns>
        public Guid GetParentProjectId(Part part)
        {
            return this.GetParentProjectId(part, false);
        }

        /// <summary>
        /// Gets the id of the project in which a specified part exists, optionally specifying whether the part can exist anywhere
        /// in assembly hierarchy or only directly in the project.
        /// </summary>
        /// <param name="part">The part.</param>
        /// <param name="partIsOnFirstLevel">If set to true, it specifies that the part exists directly in the project;
        /// otherwise it means that the part can be anywhere in the assembly hierarchy. Basically, if is true it instructs the method to return the project id only
        /// if the part is directly in a project (and is not a sub-part of an assembly).</param>
        /// <returns>
        /// The parent project's id
        /// - or - Guid.Empty if <paramref name="partIsOnFirstLevel" /> is false and the part is not part of a project (at any level of the assemblies hierarchy)
        /// - or - Guid.Empty if if <paramref name="partIsOnFirstLevel" /> is true and the part is not a direct child of a project.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">part;Part was null.</exception>
        public Guid GetParentProjectId(Part part, bool partIsOnFirstLevel)
        {
            if (part == null)
            {
                throw new ArgumentNullException("part", "Part was null.");
            }

            try
            {
                if (partIsOnFirstLevel)
                {
                    var query = from p in this.EntityContext.PartSet
                                    .Include(p => p.Project)
                                where p.Guid == part.Guid
                                select (Guid?)p.Project.Guid;

                    var projId = query.FirstOrDefault();
                    return projId.HasValue ? projId.Value : Guid.Empty;
                }
                else
                {
                    return this.EntityContext.FindTopParentId(part.Guid, "Parts", 1);
                }
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the id of the top-most parent assembly of a specified part.
        /// </summary>
        /// <param name="part">The part whose assembly to find.</param>
        /// <returns>
        /// The top-most assembly's id or Guid.Empty if the part is not part of an assembly.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">part;Part was null.</exception>
        public Guid GetTopParentAssemblyId(Part part)
        {
            if (part == null)
            {
                throw new ArgumentNullException("part", "Part was null.");
            }

            try
            {
                return this.EntityContext.FindTopParentId(part.Guid, "Parts", 2);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the id of the (direct) parent assembly of a specified part.
        /// </summary>
        /// <param name="part">The part whose assembly to find.</param>
        /// <returns>
        /// The assembly's id or Guid.Empty if the part is not part of an assembly.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">part;Part was null.</exception>
        public Guid GetParentAssemblyId(Part part)
        {
            if (part == null)
            {
                throw new ArgumentNullException("part", "The part was null.");
            }

            try
            {
                var query = from subPart in this.EntityContext.PartSet
                            join parent in this.EntityContext.AssemblySet on subPart.Assembly equals parent
                            where subPart.Guid == part.Guid
                            select parent.Guid;

                return query.FirstOrDefault();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the parts of a specified user, that are bookmarked, to be displayed in bookmarks tree.
        /// </summary>
        /// <param name="ownerId">The owner id.</param>
        /// <returns>A collection of parts.</returns>
        public Collection<Part> GetBookmarkedParts(Guid ownerId)
        {
            try
            {
                var query = from part in this.EntityContext.PartSet
                            join bookmark in this.EntityContext.BookmarkSet on part.Guid equals bookmark.EntityId
                            where part.Owner.Guid == ownerId && !part.IsDeleted && bookmark.EntityTypeId == BookmarkedItemType.Part
                            select part;

                return new Collection<Part>(query.ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the id of the raw part's parent part.
        /// </summary>
        /// <param name="rawPartId">The raw part id.</param>
        /// <returns>The guid or Guid.Empty if no parent part is found.</returns>
        public Guid GetParentOfRawPartId(Guid rawPartId)
        {
            var query = from part in this.EntityContext.PartSet
                        where part.RawPart.Guid == rawPartId
                        select part;

            try
            {
                var part = query.FirstOrDefault();
                return part != null ? part.Guid : Guid.Empty;
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        #endregion

        #region Logic for fully loading a part

        /// <summary>
        /// Fully loads the data of the specified parts into the data context.
        /// </summary>
        /// <param name="partIds">The ids of the parts to load.</param>
        /// <param name="loadRawParts">if set to true the raw parts of all parts are also loaded; otherwise they are not.</param>
        /// <returns>The loaded parts.</returns>
        internal Collection<Part> LoadPartsFull(IEnumerable<Guid> partIds, bool loadRawParts)
        {
            var parts = this.LoadPartsFull(null, partIds, loadRawParts);
            return new Collection<Part>(parts);
        }

        /// <summary>
        /// Fully loads the data of all parts of the specified assemblies into the data context.
        /// </summary>
        /// <param name="parentAssemblyIds">The ids of the parent assemblies of the parts to load.</param>
        internal void LoadAssemblyPartsFull(IEnumerable<Guid> parentAssemblyIds)
        {
            this.LoadPartsFull(parentAssemblyIds, null, true);
        }

        /// <summary>
        /// Fully loads the data of the specified parts into the data context. The parts to be loaded can be specified
        /// by the id of their parent assemblies (<paramref name="parentAssemblyIds" />) or by their own id (<paramref name="partIds" />) but not by both.
        /// </summary>
        /// <param name="parentAssemblyIds">The ids of the parent assemblies of the parts to load.</param>
        /// <param name="partIds">The ids of the parts to load.</param>
        /// <param name="loadRawParts">if set to true the raw parts of all parts are also loaded; otherwise they are not.</param>
        /// <returns>
        /// A list containing the loaded parts for the case when loading parts by their ID.
        /// When loading parts by their parent assembly IDs, it returns an empty list.
        /// </returns>
        /// <exception cref="System.ArgumentException">all parameters were null at the same time or all were set at the same time</exception>
        [SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Justification = "Its easier to understand if all the code is in the method.")]
        private List<Part> LoadPartsFull(IEnumerable<Guid> parentAssemblyIds, IEnumerable<Guid> partIds, bool loadRawParts)
        {
            if (parentAssemblyIds == null && partIds == null)
            {
                throw new ArgumentException("Both lists of ids can not be null.");
            }

            if (parentAssemblyIds != null && partIds != null)
            {
                throw new ArgumentException("Both lists of ids can not be set. You have to provide just one and the other must be null.");
            }

            if ((parentAssemblyIds != null && !parentAssemblyIds.Any())
                || (partIds != null && !partIds.Any()))
            {
                return new List<Part>();
            }

            /* Many of the queries below have a Contains clause in the condition, which is translated into a SQL IN clause.
             * The number of elements in the IN clause is limited (the limit is not low, is thousands of elements) and the more elements it has
             * the worse the query performs but on all client provided models there were less than 200 elements and the performance was good.
             * In the future it may be necessary to partition the elements and execute the query for each partition.
             */

            try
            {
                var context = this.EntityContext;
                var merge = MergeOption.PreserveChanges;

                // The base part query defines the references that must be loaded with each part.
                var basePartQuery = from part in context.PartSet
                                        .Include(p => p.Owner)
                                        .Include(p => p.CalculatorUser)
                                        .Include(p => p.CountrySettings)
                                        .Include(p => p.Manufacturer)
                                        .Include(p => p.OverheadSettings)
                                        .Include(p => p.MeasurementUnit)
                                        .Include(p => p.ProcessStepPartAmounts)
                                    select part;

                // Create the part selection query based on parent assembly ids or part ids and execute it.
                IQueryable<Part> partQuery = null;
                if (parentAssemblyIds != null)
                {
                    partQuery = from part in basePartQuery
                                where parentAssemblyIds.Contains(part.Assembly.Guid)
                                select part;
                }
                else
                {
                    partQuery = from part in basePartQuery
                                where partIds.Contains(part.Guid)
                                select part;
                }

                partQuery.SetMergeOption(merge);
                var parts = partQuery.ToList();

                if (parts.Count > 0)
                {
                    var loadedPartsIds = parts.Select(p => p.Guid).ToList();

                    // Load the raw parts of the loaded parts
                    if (loadRawParts)
                    {
                        // Get the id of the raw parts belonging to the loaded parts
                        var rawPartIdsQuery = from part in context.PartSet
                                              join rawPart in context.PartSet on part.RawPart equals rawPart
                                              where loadedPartsIds.Contains(part.Guid)
                                              select rawPart.Guid;
                        var rawPartIds = rawPartIdsQuery.ToList();

                        // Load the raw parts based on the ids obtained above, using the base part query in order not to re-define the Part includes.
                        if (rawPartIds.Count > 0)
                        {
                            var rawPartsQuery = from part in basePartQuery
                                                where rawPartIds.Contains(part.Guid)
                                                select part;
                            rawPartsQuery.SetMergeOption(merge);
                            var rawParts = rawPartsQuery.ToList();

                            // Add the loaded raw parts and their IDs in the parts and partIds lists so their sub-objects are loaded with the rest of the parts sub-objects.
                            loadedPartsIds.AddRange(rawPartIds);
                            parts.AddRange(rawParts);
                        }
                    }

                    // Load the part's commodities.
                    var commoditiesQuery = from commodity in context.CommoditySet
                                               .Include(c => c.Manufacturer)
                                               .Include(c => c.WeightUnitBase)
                                           where loadedPartsIds.Contains(commodity.Part.Guid)
                                           select commodity;
                    commoditiesQuery.SetMergeOption(merge);
                    commoditiesQuery.ToList();

                    // Load the part's raw materials
                    var materialsQuery = from material in context.RawMaterialSet
                                             .Include(m => m.DeliveryType)
                                             .Include(m => m.Manufacturer)
                                             .Include(m => m.MaterialsClassificationL1)
                                             .Include(m => m.MaterialsClassificationL2)
                                             .Include(m => m.MaterialsClassificationL3)
                                             .Include(m => m.MaterialsClassificationL4)
                                             .Include(m => m.ParentWeightUnitBase)
                                             .Include(m => m.QuantityUnitBase)
                                         where loadedPartsIds.Contains(material.Part.Guid)
                                         select material;
                    materialsQuery.SetMergeOption(merge);
                    materialsQuery.ToList();

                    // Load the part's process and steps list here. It is faster than putting the "Process.Steps" include in the main part query.                    
                    var processQuery = from process in context.ProcessSet
                                       join p in context.PartSet on process.Guid equals p.Process.Guid
                                       where loadedPartsIds.Contains(p.Guid)
                                       select process;

                    // When using a join the ObjectSet<T>.Include does not work so we have to set the include just before the query is executed.
                    processQuery.SetMergeOption(merge);
                    processQuery.Include(p => p.Steps).ToList();

                    var processStepsIds = parts.Where(p => p.Process != null).SelectMany(p => p.Process.Steps.Select(s => s.Guid)).ToList();
                    if (processStepsIds.Count > 0)
                    {
                        // Load all process steps of all parts and raw parts.
                        var stepQuery = from step in context.ProcessStepSet
                                            .Include(s => s.CycleTimeCalculations)
                                            .Include(s => s.CycleTimeUnit)
                                            .Include(s => s.MaxDownTimeUnit)
                                            .Include(s => s.ProcessTimeUnit)
                                            .Include(s => s.SetupTimeUnit)
                                            .Include(s => s.SubType)
                                            .Include(s => s.Type)
                                        where processStepsIds.Contains(step.Guid)
                                        select step;
                        stepQuery.SetMergeOption(merge);
                        stepQuery.ToList();

                        // Load the process' consumables
                        var consumablesQuery = from consumable in context.ConsumableSet
                                                   .Include(c => c.Manufacturer)
                                                   .Include(c => c.AmountUnitBase)
                                                   .Include(c => c.PriceUnitBase)
                                               where processStepsIds.Contains(consumable.ProcessStep.Guid)
                                               select consumable;
                        consumablesQuery.SetMergeOption(merge);
                        consumablesQuery.ToList();

                        // Load the process' dies
                        var diesQuery = from die in context.DieSet
                                            .Include(d => d.Manufacturer)
                                        where processStepsIds.Contains(die.ProcessStep.Guid)
                                        select die;
                        diesQuery.SetMergeOption(merge);
                        diesQuery.ToList();

                        // Load the process' machines
                        var machinesQuery = from machine in context.MachineSet
                                                .Include(m => m.Manufacturer)
                                                .Include(m => m.ClassificationLevel4)
                                                .Include(m => m.MainClassification)
                                                .Include(m => m.SubClassification)
                                                .Include(m => m.TypeClassification)
                                            where processStepsIds.Contains(machine.ProcessStep.Guid)
                                            select machine;
                        machinesQuery.SetMergeOption(merge);
                        machinesQuery.ToList();
                    }

                    // Load the transport cost calculations and their settings for the parts and raw parts.
                    var calculationQuery = from calculation in context.TransportCostCalculationSet
                                               .Include(c => c.TransportCostCalculationSetting)
                                           where loadedPartsIds.Contains(calculation.Part.Guid)
                                           select calculation;

                    calculationQuery.SetMergeOption(merge);
                    calculationQuery.ToList();
                }

                if (partIds != null)
                {
                    return parts.Where(p => partIds.Contains(p.Guid)).ToList();
                }
                else
                {
                    return new List<Part>();
                }
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, SourceDatabase);
            }
        }

        #endregion Logic for fully loading a part

        #region Save/Update/Refresh

        /// <summary>
        /// Updates the overhead settings in the part retrieved from the db and commits the changes
        /// </summary>
        /// <param name="partId">Guid of the updated part</param>
        /// <param name="overheadSettings">The updated overhead settings</param>
        public void UpdateOverheadSettings(Guid partId, OverheadSetting overheadSettings)
        {
            try
            {
                var query = from part in this.EntityContext.PartSet
                                .Include(p => p.OverheadSettings)
                            where part.Guid == partId
                            select part;

                query.SetMergeOption(MergeOption.PreserveChanges);

                Part partEntity = query.FirstOrDefault();
                if (partEntity != null)
                {
                    overheadSettings.CopyValuesTo(partEntity.OverheadSettings);
                }
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, SourceDatabase);
            }
        }

        /// <summary>
        /// Refreshes the part and its process by overriding the changes with data from the db.
        /// </summary>
        /// <param name="part">The part.</param>
        public void RefreshWithProcess(Part part)
        {
            try
            {
                this.EntityContext.RefreshNavigationProperty(part, p => p.ProcessStepPartAmounts);

                if (part.Process != null)
                {
                    this.EntityContext.RefreshNavigationProperty(part.Process, p => p.Steps);
                }

                // Refresh the part last because it will be detached if it was deleted from the database.
                this.Refresh(part);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, SourceDatabase);
            }
        }

        /// <summary>
        /// Refreshes the part, including its children.
        /// </summary>
        /// <param name="part">The part.</param>
        public void RefreshWithChildren(Part part)
        {
            try
            {
                this.LoadPartsFull(new List<Guid>() { part.Guid }, false);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, SourceDatabase);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes the specified part from the repository using a stored procedure. All sub-entities owned by the part are also deleted.
        /// The actual deletion from the database is performed during this call (when the stored procedure is called), not when SaveChanges() is called.
        /// </summary>
        /// <param name="part">The part to delete.</param>
        public void DeleteByStoredProcedure(Part part)
        {
            if (part == null)
            {
                throw new ArgumentNullException("part", "The part was null.");
            }

            var originalCommandTimeout = this.EntityContext.CommandTimeout;

            try
            {
                this.EntityContext.CommandTimeout = 600;
                this.EntityContext.DeletePart(part.Guid);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, SourceDatabase);
            }
            finally
            {
                this.EntityContext.CommandTimeout = originalCommandTimeout;
            }
        }

        #endregion
    }
}
