﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Entity;
using System.Linq;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// The Raw Material Price History Repository.
    /// </summary>
    internal class RawMaterialPriceHistoryRepository : Repository<RawMaterialsPriceHistory>, IRawMaterialPriceHistoryRepository
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Initializes a new instance of the <see cref="RawMaterialPriceHistoryRepository"/> class.
        /// </summary>
        /// <param name="entityContext">The object context to be used for data operations.</param>
        /// <param name="sourceDatabase">A value indicating the database on which this repository operates.</param>
        public RawMaterialPriceHistoryRepository(DataEntities entityContext, DbIdentifier sourceDatabase)
            : base(entityContext, sourceDatabase)
        {
        }

        /// <summary>
        /// Gets the price history.
        /// </summary>
        /// <param name="rawMaterialId">The raw material id.</param>
        /// <returns>
        /// The result as a collection of Raw Material Price History.
        /// </returns>
        public Collection<RawMaterialsPriceHistory> GetPriceHistory(Guid rawMaterialId)
        {
            try
            {
                var query = from priceHistory in this.EntityContext.RawMaterialsPriceHistorySet
                            where priceHistory.RawMaterial.Guid == rawMaterialId
                            select priceHistory;

                return new Collection<RawMaterialsPriceHistory>(query.AsNoTracking().ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }
    }
}
