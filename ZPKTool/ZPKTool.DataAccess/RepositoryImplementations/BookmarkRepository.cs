﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Implements operations for Bookmark.
    /// </summary>
    internal class BookmarkRepository : Repository<Bookmark>, IBookmarkRepository
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Initializes a new instance of the <see cref="BookmarkRepository"/> class.
        /// </summary>
        /// <param name="entityContext">The object context to be used for data operations.</param>
        /// <param name="sourceDatabase">A value indicating the database on which this repository operates.</param>
        public BookmarkRepository(DataEntities entityContext, DbIdentifier sourceDatabase)
            : base(entityContext, sourceDatabase)
        {
        }

        /// <summary>
        /// Checks if an item has a bookmark.
        /// </summary>
        /// <param name="itemId">The id of the item.</param>
        /// <returns>
        /// True if a bookmark exists for the specified item, otherwise false.
        /// </returns>
        public bool HasBookmark(Guid itemId)
        {
            var query = from bookmarks in this.EntityContext.BookmarkSet                            
                        where bookmarks.EntityId == itemId
                        select bookmarks;

            try
            {
                return query.AsNoTracking().Count() > 0;
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the entity type and total count for each type that are bookmarked and not deleted. 
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <returns>A dictionary containing the entity type as key and the total count of entities that are not deleted and have a bookmark as a value.</returns>
        public Dictionary<BookmarkedItemType, int> GetBookmarkedItemTypesCount(Guid userId)
        {
            var query = from bookmark in this.EntityContext.BookmarkSet
                        join project in this.EntityContext.ProjectSet on bookmark.EntityId equals project.Guid into bookmarkedProjects
                        join assembly in this.EntityContext.AssemblySet on bookmark.EntityId equals assembly.Guid into bookmarkedAssemblies
                        join part in this.EntityContext.PartSet on bookmark.EntityId equals part.Guid into bookmarkedParts
                        where bookmark.Owner.Guid == userId
                            && (bookmarkedProjects.Count() == 0 || !bookmarkedProjects.FirstOrDefault().IsDeleted)
                            && (bookmarkedAssemblies.Count() == 0 || !bookmarkedAssemblies.FirstOrDefault().IsDeleted)
                            && (bookmarkedParts.Count() == 0 || !bookmarkedParts.FirstOrDefault().IsDeleted)
                        select bookmark;

            try
            {
                // If the group by statement is put in the query above entity framework fails to generate a query (throws an exception).
                // Creating the query below just for grouping seems to work.
                var groupQuery = from bookmark in query
                                 group bookmark by bookmark.EntityTypeId into c
                                 select new { c.Key, Count = c.Count() };

                return groupQuery.AsNoTracking().ToDictionary(b => (BookmarkedItemType)b.Key, b => b.Count);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the bookmark item corresponding to a specified entity.
        /// </summary>
        /// <param name="entityId">The entity id.</param>
        /// <returns>The bookmark item corresponding to the entity or null if the entity doesn't have a bookmark.</returns>
        public Bookmark GetBookmark(Guid entityId)
        {
            var query = from item in this.EntityContext.BookmarkSet
                            .Include(b => b.Owner)
                        where item.EntityId == entityId
                        select item;

            try
            {
                return query.FirstOrDefault();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Deletes the bookmark item of the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        public void DeleteBookmark(object obj)
        {
            try
            {
                IIdentifiable identifiableItem = obj as IIdentifiable;
                if (obj != null)
                {
                    Bookmark item = this.GetBookmark(identifiableItem.Guid);
                    if (item != null)
                    {
                        this.RemoveAll(item);
                    }
                }
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }
    }
}