﻿using System.Data;
using System.Data.Entity;
using System.Linq;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Implements operations on BasicSetting.
    /// </summary>
    internal class BasicSettingsRepository : Repository<BasicSetting>, IBasicSettingsRepository
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Initializes a new instance of the <see cref="BasicSettingsRepository"/> class.
        /// </summary>        
        /// <param name="entityContext">The object context to be used for data operations.</param>
        /// <param name="sourceDatabase">A value indicating the database on which this repository operates.</param>
        public BasicSettingsRepository(DataEntities entityContext, DbIdentifier sourceDatabase)
            : base(entityContext, sourceDatabase)
        {
        }

        /// <summary>
        /// Retrieves the basic settings of the application; there is only one basic settings record.
        /// </summary>
        /// <param name="noTracking">if set to true change tracking is disabled for the returned object(s).</param>
        /// <returns>
        /// The basic settings
        /// </returns>
        public BasicSetting GetBasicSettings(bool noTracking = false)
        {
            var query = (from settings in this.EntityContext.BasicSettingSet
                         select settings).Take(1);

            if (noTracking)
            {
                query = query.AsNoTracking();
            }

            try
            {
                return query.FirstOrDefault();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }
    }
}
