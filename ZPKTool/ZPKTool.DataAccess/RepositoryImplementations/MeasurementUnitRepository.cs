﻿using System.Collections.ObjectModel;
using System.Linq;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Implements operations for MeasurementUnit.
    /// </summary>
    internal class MeasurementUnitRepository : Repository<MeasurementUnit>, IMeasurementUnitRepository
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Initializes a new instance of the <see cref="MeasurementUnitRepository"/> class.
        /// </summary>
        /// <param name="entityContext">The object context to be used for data operations.</param>
        /// <param name="sourceDatabase">A value indicating the database on which this repository operates.</param>
        public MeasurementUnitRepository(DataEntities entityContext, DbIdentifier sourceDatabase)
            : base(entityContext, sourceDatabase)
        {
        }

        /// <summary>
        /// Gets all measurement units.
        /// </summary>
        /// <returns>
        /// A collection of MeasurementUnits.
        /// </returns>
        public Collection<MeasurementUnit> GetAll()
        {
            var query = from measurementUnit in this.EntityContext.MeasurementUnitSet
                        where !measurementUnit.IsReleased
                        orderby measurementUnit.ScaleFactor descending
                        select measurementUnit;

            try
            {
                return new Collection<MeasurementUnit>(query.ToList());
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets all measurement units of a given type.
        /// </summary>
        /// <param name="unitsType">Type of the units to get.</param>
        /// <returns>
        /// A list of MeasurementUnits with the type <paramref name="unitsType"/>
        /// </returns>
        public Collection<MeasurementUnit> GetAll(MeasurementUnitType unitsType)
        {
            var query = from measurementUnit in this.EntityContext.MeasurementUnitSet
                        where measurementUnit.Type == unitsType && !measurementUnit.IsReleased
                        orderby measurementUnit.ScaleFactor descending
                        select measurementUnit;

            try
            {
                return new Collection<MeasurementUnit>(query.ToList());
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets all measurement units in a given scale.
        /// </summary>
        /// <param name="unitsScale">The scale to whom units to get belongs.</param>
        /// <returns>
        /// A collection of MeasurementUnits with the type <paramref name="unitsType"/>
        /// </returns>
        public Collection<MeasurementUnit> GetAll(MeasurementUnitScale unitsScale)
        {
            var query = from measurementUnit in this.EntityContext.MeasurementUnitSet
                        where measurementUnit.ScaleID == unitsScale && !measurementUnit.IsReleased
                        orderby measurementUnit.ScaleFactor descending
                        select measurementUnit;

            try
            {
                return new Collection<MeasurementUnit>(query.ToList());
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the base measurement units of all scales (the units which have scale factor equal to 1).
        /// </summary>
        /// <returns>
        /// A collection of MeasurementUnits.
        /// </returns>
        public Collection<MeasurementUnit> GetBaseMeasurementUnits()
        {
            var query = from measurementUnit in this.EntityContext.MeasurementUnitSet
                        where measurementUnit.ScaleFactor == 1 && !measurementUnit.IsReleased
                        orderby measurementUnit.ScaleFactor descending
                        select measurementUnit;

            try
            {
                return new Collection<MeasurementUnit>(query.ToList());
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the base measurement unit of a scale.
        /// </summary>
        /// <param name="scale">The scale.</param>
        /// <returns>
        /// A MeasurementUnit instance.
        /// </returns>
        public MeasurementUnit GetBaseMeasurementUnit(MeasurementUnitScale scale)
        {
            var query = from measurementUnit in this.EntityContext.MeasurementUnitSet
                        where measurementUnit.ScaleID == scale
                            && measurementUnit.ScaleFactor == 1
                            && !measurementUnit.IsReleased
                        orderby measurementUnit.ScaleFactor descending
                        select measurementUnit;

            try
            {
                return query.FirstOrDefault();
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets a measurement unit item by name
        /// </summary>
        /// <param name="unitName">Name of the unit</param>
        /// <returns>
        /// The unit or null if is not found.
        /// </returns>
        public MeasurementUnit GetByName(string unitName)
        {
            var query = from mu in this.EntityContext.MeasurementUnitSet
                        where mu.Name == unitName && !mu.IsReleased
                        select mu;

            try
            {
                return query.FirstOrDefault();
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }
    }
}
