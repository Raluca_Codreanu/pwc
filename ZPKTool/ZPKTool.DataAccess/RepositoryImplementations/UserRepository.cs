﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Entity;
using System.Linq;
using ZPKTool.Common;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Implements operations on Users.
    /// </summary>
    internal class UserRepository : Repository<User>, IUserRepository
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Initializes a new instance of the <see cref="UserRepository"/> class.
        /// </summary>
        /// <param name="entityContext">The object context to be used for data operations.</param>
        /// <param name="sourceDatabase">A value indicating the database on which this repository operates.</param>
        public UserRepository(DataEntities entityContext, DbIdentifier sourceDatabase)
            : base(entityContext, sourceDatabase)
        {
        }

        /// <summary>
        /// Gets all users from the database.
        /// </summary>
        /// <param name="includeDisabled">if set to true include disabled users in the result.</param>
        /// <param name="noTracking">if set to true change tracking is disabled for the objects in the result.</param>
        /// <returns>
        /// A collection with all the users.
        /// </returns>
        public Collection<User> GetAll(bool includeDisabled, bool noTracking = false)
        {
            var query = from user in this.EntityContext.UserSet
                            .Include(u => u.UICurrency)
                        where (includeDisabled || !user.Disabled) && !user.IsReleased
                        select user;

            if (noTracking)
            {
                query = (IOrderedQueryable<User>)query.AsNoTracking();
            }

            try
            {
                return new Collection<User>(query.ToList().OrderBy(u => u.Username).ThenBy(u => u.Name).ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the users from the database, that have at least one project for which the user passed as parameter is responsible or Project Leader.
        /// </summary>
        /// <param name="userGuid">User Guid for seeking projects to other users.</param>
        /// <returns>
        /// A collection of all users found that meet the search requirements.
        /// </returns>
        public Collection<User> GetUsersWithProjectsVisibleToOtherUser(Guid userGuid)
        {
            var query = from project in this.EntityContext.ProjectSet
                        where
                            !project.IsDeleted
                            && (project.ProjectLeader.Guid == userGuid
                                || project.ResponsibleCalculator.Guid == userGuid)
                        select project.Owner;

            try
            {
                return new Collection<User>(query.ToList().Distinct().OrderBy(u => u.Username).ThenBy(u => u.Name).ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the user with a given id.        
        /// </summary>
        /// <param name="id">The id of the searched user.</param>
        /// <param name="includeDisabled">if set to true include disabled users in the result.</param>
        /// <returns>A User instance or null if not found.</returns>
        public User GetById(Guid id, bool includeDisabled)
        {
            var query = from user in this.EntityContext.UserSet
                            .Include(u => u.UICurrency)
                        where user.Guid == id &&
                              (includeDisabled || !user.Disabled) &&
                              !user.IsReleased
                        select user;

            try
            {
                return query.FirstOrDefault();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets a user by its username.
        /// </summary>
        /// <param name="username">The username to search by.</param>
        /// <param name="includeDisabled">if set to true, include disabled users in the result.</param>
        /// <param name="noTracking">if set to true the returned object is not tracked for changes.</param>
        /// <returns>
        /// An User instance having the requested user name.
        /// </returns>
        public User GetByUsername(string username, bool includeDisabled, bool noTracking = false)
        {
            var encryptedUsername = Encryption.Encrypt(username, Encryption.EncryptionPassword);
            var query = from user in this.EntityContext.UserSet
                            .Include(u => u.UICurrency)
                        where user.EncryptedUsername == encryptedUsername
                            && (includeDisabled || !user.Disabled)
                            && !user.IsReleased
                        select user;

            if (noTracking)
            {
                query = query.AsNoTracking();
            }

            try
            {
                return query.FirstOrDefault();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Adds or updates the specified user.
        /// </summary>
        /// <param name="user">The user to save</param>
        public override void Save(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user", "The user was null.");
            }

            // Check if a user with the same name exists
            User existingUser = GetByUsername(user.Username, true, true);

            if (existingUser != null && existingUser.Guid != user.Guid)
            {
                // We must undo the changes of the user if it is not saved. This is needed because any subsequent db commit will try to save it again (and fail).
                // TODO: implement Undo in this repository and apply it here.
                this.EntityContext.ChangeTracker.DetectChanges();
                var entry = this.EntityContext.Entry(user);
                if (entry.State == EntityState.Modified)
                {
                    entry.State = EntityState.Unchanged;
                }

                log.Warn("Can's save user because a user with the same name already exists.");
                throw new DataAccessException(ErrorCodes.UsernameAlreadyExists);
            }
            else
            {
                base.Save(user);
            }
        }
    }
}
