﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// A non-generic version of <see cref="ZPKTool.DataAccess.Repository&lt;T&gt;"/> that can be used when the type of the entity in the repository is not known.
    /// </summary>
    internal class Repository : IRepository
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The context used to access the entity data store.
        /// </summary>
        private DataEntities context;

        /// <summary>
        /// The entity type for which this instance was created and on which it works.
        /// </summary>
        private Type entityType;

        /// <summary>
        /// The set object used to access the entities of the type on which this instance works, and perform operations on them.
        /// </summary>
        private DbSet entitySet;

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository" /> class.
        /// </summary>
        /// <param name="entityType">The type of the entity for which this instance will be created.</param>
        /// <param name="context">The context used to access the data store.</param>
        /// <param name="sourceDatabase">A value indicating the database on which this repository operates.</param>
        /// <exception cref="System.ArgumentNullException">The data context was null.</exception>
        /// <exception cref="InvalidOperationException">The entity type is not part of the data model.</exception>
        public Repository(Type entityType, DataEntities context, DbIdentifier sourceDatabase)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context", "The data context was null.");
            }

            this.entityType = ObjectContext.GetObjectType(entityType);
            this.context = context;
            this.SourceDatabase = sourceDatabase;
            this.entitySet = this.context.Set(this.entityType);
        }

        /// <summary>
        /// Gets or sets a value indicating the database on which this repository operates.
        /// </summary>        
        public DbIdentifier SourceDatabase { get; protected set; }

        /// <summary>
        /// Adds the specified entity to the repository (and sets its state to Added). If the entity is already in the repository in another state, its state
        /// will still be set to Added.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <exception cref="System.ArgumentNullException">The entity was null.</exception>
        /// <exception cref="ArgumentException">The type of the entity does not match the entity type for which the repository was created.</exception>
        public void Add(object entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "The entity was null.");
            }

            try
            {
                this.entitySet.Add(entity);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets an entity with the given primary key values. If an entity with the given primary key values exists in the repository, then it is returned
        /// without making a request to the store. Otherwise, the entity is queried from the store and returned if found (is also attached to the repository's data context).
        /// If no entity is found in the context or the store, then null is returned.
        /// </summary>
        /// <param name="keys">The values of the primary key for the entity to be found.</param>
        /// <returns>The entity with the specified primary key values or null if it was not found.</returns>
        /// <exception cref="InvalidOperationException">Multiple entities exist in the context with the primary key values given, or
        /// the type of entity is not part of the data model for this context, or
        /// the types, order or number of the key values do not match the types, order or number of the key values of the entity type to be found as defined in the data model.
        /// </exception>
        public object GetByKey(params object[] keys)
        {
            try
            {
                return this.entitySet.Find(keys);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the entity instance that corresponds to the specified entity in this repository. If the searched entity exists in the repository
        /// it is returned immediately, otherwise it queries it from the store. If the searched entity is not found in the repository or the store, null is returned.
        /// </summary>
        /// <param name="entity">The entity to get from the repository.</param>
        /// <returns>The found entity or null if it is not found.</returns>
        /// <exception cref="System.ArgumentNullException">The entity was null.</exception>
        /// <exception cref="ArgumentException">The type of the entity does not match the entity type for which the repository was created.</exception>
        public object GetByEntity(object entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "The entity was null.");
            }

            if (ObjectContext.GetObjectType(entity.GetType()) != this.entityType)
            {
                throw new ArgumentException("The type of the entity does not match the repository's entity type.", "entity");
            }

            try
            {
                // Obtain the key information for the entity type from the data model metadata store.
                var objectContext = ((IObjectContextAdapter)this.context).ObjectContext;
                var edmEntityType = objectContext.MetadataWorkspace.GetItems<EntityType>(DataSpace.CSpace).FirstOrDefault(item => item.Name == this.entityType.Name);
                if (edmEntityType == null)
                {
                    string message = string.Format(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "The type metadata of the entity type '{0}' could not be found in the object context's metadata store. Usually it this means that the type is not part of the data model.",
                        this.entityType.Name);
                    throw new InvalidOperationException(message);
                }

                // Get the key values from the entity.
                var keyValues = edmEntityType.KeyMembers.Select(k => this.entityType.GetProperty(k.Name).GetValue(entity, null)).ToArray();

                // Get the corresponding entity using the key values.
                return this.entitySet.Find(keyValues);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Marks the specified entity as Deleted such that it will be deleted from the database when the repository changes are saved.
        /// Note that the entity must exist in the repository's data context in some other state before this method is called.
        /// </summary>
        /// <param name="entity">The entity to mark for deletion.</param>
        /// <exception cref="System.ArgumentNullException">The entity was null.</exception>
        /// <exception cref="ArgumentException">The type of the entity does not match the entity type for which the repository was created.</exception>
        /// <exception cref="InvalidOperationException">The entity cannot be deleted because it was not found in the repository's data context.</exception>
        public void Remove(object entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "The entity was null.");
            }

            try
            {
                this.entitySet.Remove(entity);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Determines whether the specified entity is marked as Added in the repository's data context. Entities in the Added state will be inserted
        /// into the data store when the repository changes are saved.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// true if the specified entity is in the Added state; otherwise, false.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">The entity was null.</exception>
        /// <exception cref="ArgumentException">The type of the entity does not match the entity type for which the repository was created.</exception>
        public bool IsAdded(object entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "The entity was null.");
            }

            if (ObjectContext.GetObjectType(entity.GetType()) != this.entityType)
            {
                throw new ArgumentException("The type of the entity does not match the repository's entity type.", "entity");
            }

            // Calling DetectChanges here is not necessary because the Added state is set when the object is attached to the data context even if AutoDetectChanges is disabled.
            return this.context.Entry(entity).State == EntityState.Added;
        }

        /// <summary>
        /// Determines whether the specified entity is marked as Deleted in the repository's data context. Entities in the Deleted state will be deleted
        /// from the data store when the repository changes are saved.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// true if the specified entity is in the Deleted state; otherwise, false.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">The entity was null.</exception>
        /// <exception cref="ArgumentException">The type of the entity does not match the entity type for which the repository was created.</exception>
        public bool IsDeleted(object entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "The entity was null.");
            }

            if (ObjectContext.GetObjectType(entity.GetType()) != this.entityType)
            {
                throw new ArgumentException("The type of the entity does not match the repository's entity type.", "entity");
            }

            // Calling DetectChanges here is not necessary because the Deleted state is set when the object is removed from the data context even if AutoDetectChanges is disabled.
            return this.context.Entry(entity).State == EntityState.Deleted;
        }

        /// <summary>
        /// Refreshes an entity with the values from database. The entities in its graph are not refreshed.
        /// </summary>
        /// <param name="entity">The entity to refresh.</param>
        public void Refresh(object entity)
        {
            try
            {
                this.context.Entry(entity).Reload();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Detaches the specified entity from the context.
        /// </summary>
        /// <param name="entity">The entity to detach.</param>
        public void Detach(object entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "The entity was null.");
            }

            try
            {
                var entityEntry = this.context.Entry(entity);
                if (entityEntry.State == EntityState.Detached)
                {
                    throw new InvalidOperationException("The entity is already detached.");
                }

                this.context.Detach(entity);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Detaches the specified entity, including any children entities owned by the entity, from the context.
        /// </summary>
        /// <param name="entity">The entity to detach.</param>
        public void DetachAll(object entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "The entity was null.");
            }

            var entitiesToDetach = RelationshipsGraph.CollectOwnedObjects(entity);

            // When detaching an entity sometimes some relationships to its parents still remains and need to be manually removed
            // Gather all parent entities of entity using the relationships information from the RelationshipsGraph and
            // explicitly detach the EF relationship between entity and each parent.
            var objectContext = ((IObjectContextAdapter)this.context).ObjectContext;
            var entityRelationships = RelationshipsGraph.EntitiesRelationshipsGraph[entity.GetType()];
            foreach (var parentRelationship in entityRelationships.HasAsParent)
            {
                var parents = new List<object>();
                var parentValue = parentRelationship.Property.GetValue(entity, null);
                var parentCollection = parentValue as IEnumerable;
                if (parentCollection != null)
                {
                    // The parent property value is enumerable. This means that the parent property is a collection of parents (for one-to-many or many-to-many relationships).
                    parents = parentCollection.Cast<object>().ToList();
                }
                else if (parentValue != null)
                {
                    // The parent value is the parent itself.
                    parents.Add(parentValue);
                }

                // Detach the relationships between entity and each parent entity identified above.
                foreach (var parent in parents)
                {
                    if (parent != null)
                    {
                        objectContext.ObjectStateManager.ChangeRelationshipState(entity, parent, parentRelationship.Property.Name, EntityState.Detached);
                    }
                }
            }

            // Detach the entity and all its children
            foreach (var entityToDetach in entitiesToDetach)
            {
                var entityEntry = this.context.Entry(entityToDetach);
                if (entityEntry.State != EntityState.Detached)
                {
                    this.Detach(entityToDetach);
                }
            }
        }

        /// <summary>
        /// Marks the specified entity, including any children entities owned by the entity, for deletion.
        /// </summary>
        /// <param name="entity">The entity to remove.</param>
        /// <exception cref="System.ArgumentNullException">entity;The entity was null.</exception>
        public void RemoveAll(object entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "The entity was null.");
            }

            // Can't remove objects that are not attached to a context.
            var entityEntry = this.context.Entry(entity);
            if (entityEntry.State == EntityState.Detached)
            {
                return;
            }

            var entitiesToDelete = RelationshipsGraph.CollectOwnedObjects(entity);
            foreach (var entityToDelete in entitiesToDelete)
            {
                // Some master data machines share the same manufacturer, so we check if this manufacturer is shared or not.
                var manufacturerToDelete = entityToDelete as Manufacturer;
                if (manufacturerToDelete != null
                    && manufacturerToDelete.Machines.Count > 0
                    && this.CheckIfMachineManufacturerIsShared(manufacturerToDelete.Guid))
                {
                    continue;
                }

                // Delete the entity's media.
                if (entityToDelete is IEntityWithMedia
                    || entityToDelete is IEntityWithMediaCollection)
                {
                    var mediaRepository = new MediaRepository(this.context, this.SourceDatabase);
                    mediaRepository.DeleteAllMedia(entityToDelete);
                }

                try
                {
                    var entityTypeSet = this.context.Set(entityToDelete.GetType());
                    entityTypeSet.Remove(entityToDelete);
                }
                catch (DataException ex)
                {
                    log.ErrorException("Data error.", ex);
                    throw Utils.ParseDataException(ex, this.SourceDatabase);
                }
            }
        }

        #region Helper

        /// <summary>
        /// Checks if the machine's manufacturer is shared.
        /// </summary>
        /// <param name="manufacturerId">The manufacturer id.</param>
        /// <returns>True if the manufacturer is shared, false otherwise.</returns>
        private bool CheckIfMachineManufacturerIsShared(Guid manufacturerId)
        {
            var query = from machine in this.context.MachineSet
                        where machine.Manufacturer.Guid == manufacturerId
                        select machine;

            try
            {
                return query.AsNoTracking().Count() > 1;
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        #endregion Helper

        /// <summary>
        /// Refreshes the relations for the specified entity to its parents.
        /// </summary>
        /// <param name="entity">The entity.</param>
        public void RefreshParentRelations(object entity)
        {
            try
            {
                this.Refresh(entity);

                // Refresh any collection relations to the entity parents, because
                // the collection relations are not refreshed when the entity is refreshed.
                var parentRelationships = RelationshipsGraph.EntitiesRelationshipsGraph[entity.GetType()].HasAsParent;
                foreach (var parentRel in parentRelationships)
                {
                    var navigationProperty = parentRel.Property.Name;
                    if (this.context.IsCollectionNavigationProperty(entity, navigationProperty))
                    {
                        this.context.RefreshCollectionNavigationProperty(entity, navigationProperty);
                    }
                }
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }
    }
}