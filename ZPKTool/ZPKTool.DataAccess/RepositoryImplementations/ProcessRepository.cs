﻿using System;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Implements operations for Process.
    /// </summary>
    internal class ProcessRepository : Repository<Process>, IProcessRepository
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessRepository"/> class.
        /// </summary>
        /// <param name="entityContext">The object context to be used for data operations.</param>
        /// <param name="sourceDatabase">A value indicating the database on which this repository operates.</param>
        public ProcessRepository(DataEntities entityContext, DbIdentifier sourceDatabase)
            : base(entityContext, sourceDatabase)
        {
        }

        /// <summary>
        /// Gets the process with steps.
        /// </summary>
        /// <param name="processId">The process Id.</param>
        /// <returns>The process or null if not found.</returns>
        public Process GetProcessWithSteps(Guid processId)
        {
            try
            {
                var query = from proc in this.EntityContext.ProcessSet
                                .Include(p => p.Steps)
                            where proc.Guid == processId
                            select proc;

                query.SetMergeOption(MergeOption.PreserveChanges);
                Process process = query.ToList().FirstOrDefault();

                var steps = process.Steps.ToList();
                if (steps.Count > 0)
                {
                    var processStepRepository = new ProcessStepRepository(this.EntityContext, this.SourceDatabase);
                    foreach (ProcessStep processStep in steps)
                    {
                        ProcessStep freshprocessStep = processStepRepository.GetProcessStepFull(processStep.Guid);
                        if (freshprocessStep == null)
                        {
                            processStepRepository.Detach(processStep);
                        }
                    }
                }

                return process;
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the guid of the parent project of a specified process.
        /// </summary>
        /// <param name="process">The process.</param>
        /// <returns>The guid or Guid.Empty if an error occurs.</returns>
        public Guid GetParentProjectId(Process process)
        {
            if (process == null)
            {
                throw new ArgumentNullException("process", "Process was null.");
            }

            try
            {
                return this.EntityContext.FindTopParentId(process.Guid, "Processes", 1);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the guid of the parent assembly of a specified process.
        /// </summary>
        /// <param name="process">The process.</param>
        /// <returns>The guid or Guid.Empty if an error occurs.</returns>
        public Guid GetParentAssemblyId(Process process)
        {
            if (process == null)
            {
                throw new ArgumentNullException("process", "Process was null.");
            }

            try
            {
                return this.EntityContext.FindTopParentId(process.Guid, "Processes", 2);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Deletes the specified process from the repository using a stored procedure. All sub-entities owned by the process are also deleted.
        /// The actual deletion from the database is performed during this call (when the stored procedure is called), not when SaveChanges() is called.
        /// </summary>
        /// <param name="process">The process to delete.</param>
        public void DeleteByStoredProcedure(Process process)
        {
        }
    }
}
