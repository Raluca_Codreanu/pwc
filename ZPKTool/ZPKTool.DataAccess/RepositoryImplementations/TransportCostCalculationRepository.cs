﻿using System;
using System.Data;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Implements operations for TransportCostCalculation.
    /// </summary>
    internal class TransportCostCalculationRepository : Repository<TransportCostCalculation>, ITransportCostCalculationRepository
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Initializes a new instance of the <see cref="TransportCostCalculationRepository" /> class.
        /// </summary>
        /// <param name="entityContext">The entity context.</param>
        /// <param name="sourceDatabase">The source database.</param>
        public TransportCostCalculationRepository(DataEntities entityContext, DbIdentifier sourceDatabase)
            : base(entityContext, sourceDatabase)
        {
        }

        /// <summary>
        /// Gets the guid of the parent project of a specified transport cost calculation.
        /// </summary>
        /// <param name="calculation">The calculation.</param>
        /// <returns>The guid or Guid.Empty if an error occurs.</returns>
        public Guid GetParentProjectId(TransportCostCalculation calculation)
        {
            if (calculation == null)
            {
                throw new ArgumentNullException("calculation", "Calculation was null.");
            }

            try
            {
                return this.EntityContext.FindTopParentId(calculation.Guid, "TransportCostCalculations", 1);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }
    }
}
