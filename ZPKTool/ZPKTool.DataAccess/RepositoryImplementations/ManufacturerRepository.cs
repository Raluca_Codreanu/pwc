﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Entity;
using System.Linq;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Implements operations on Manufacturer.
    /// </summary>
    internal class ManufacturerRepository : Repository<Manufacturer>, IManufacturerRepository
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// A query that fully loads manufacturer
        /// </summary>
        private IQueryable<Manufacturer> queryFull;

        /// <summary>
        /// Initializes a new instance of the <see cref="ManufacturerRepository"/> class.
        /// </summary>
        /// <param name="entityContext">The object context to be used for data operations.</param>
        /// <param name="sourceDatabase">A value indicating the database on which this repository operates.</param>
        public ManufacturerRepository(DataEntities entityContext, DbIdentifier sourceDatabase)
            : base(entityContext, sourceDatabase)
        {
            this.queryFull = from manufacturer in this.EntityContext.ManufacturerSet
                                 .Include(m => m.Owner)
                             select manufacturer;
        }

        /// <summary>
        /// Retrieves the manufacturer with a given id.
        /// </summary>
        /// <param name="id">The id of the manufacturer to retrieve.</param>
        /// <param name="noTracking">if set to true the change tracking for the resulted object is disabled.</param>
        /// <returns>
        /// The manufacturer or null if it was not found.
        /// </returns>
        public Manufacturer GetById(Guid id, bool noTracking = false)
        {
            var query = from manufacturer in this.queryFull
                        where manufacturer.Guid == id
                        select manufacturer;

            if (noTracking)
            {
                query = query.AsNoTracking();
            }

            try
            {
                return query.FirstOrDefault();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Retrieves the parent of the manufacturer with the given id.
        /// </summary>
        /// <param name="id">The id of the manufacturer</param>
        /// <returns>The parent of the manufacturer, or null if no parent is found</returns>
        public object GetParent(Guid id)
        {
            var context = this.EntityContext;
            var query = from man in context.ManufacturerSet
                        join assy in context.AssemblySet on man equals assy.Manufacturer into assyGroup
                        join part in context.PartSet on man equals part.Manufacturer into partGroup
                        join machine in context.MachineSet on man equals machine.Manufacturer into machineGroup
                        join material in context.RawMaterialSet on man equals material.Manufacturer into materialGroup
                        join consumable in context.ConsumableSet on man equals consumable.Manufacturer into consumableGroup
                        join commodity in context.CommoditySet on man equals commodity.Manufacturer into commodityGroup
                        join die in context.DieSet on man equals die.Manufacturer into dieGroup
                        where man.Guid == id
                        select new
                            {
                                assembly = assyGroup.FirstOrDefault(),
                                part = partGroup.FirstOrDefault(),
                                machine = machineGroup.FirstOrDefault(),
                                material = materialGroup.FirstOrDefault(),
                                commodity = commodityGroup.FirstOrDefault(),
                                consumable = consumableGroup.FirstOrDefault(),
                                die = dieGroup.FirstOrDefault()
                            };

            try
            {
                var result = query.AsNoTracking().FirstOrDefault();
                if (result.assembly != null)
                {
                    return result.assembly;
                }

                if (result.part != null)
                {
                    return result.part;
                }

                if (result.machine != null)
                {
                    return result.machine;
                }

                if (result.material != null)
                {
                    return result.material;
                }

                if (result.consumable != null)
                {
                    return result.consumable;
                }

                if (result.commodity != null)
                {
                    return result.commodity;
                }

                if (result.die != null)
                {
                    return result.die;
                }

                return null;
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Retrieves the master data manufacturers.
        /// </summary>
        /// <returns>
        /// The collection of master data manufacturers
        /// </returns>
        public Collection<Manufacturer> GetAllMasterData()
        {
            var query = from manufacturer in this.queryFull
                        where manufacturer.IsMasterData
                        select manufacturer;

            try
            {
                return new Collection<Manufacturer>(query.ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Deletes the specified manufacturer from the repository using a stored procedure.
        /// The actual deletion from the database is performed during this call (when the stored procedure is called), not when SaveChanges() is called.
        /// </summary>
        /// <param name="manufacturer">The manufacturer to delete.</param>
        public void DeleteByStoredProcedure(Manufacturer manufacturer)
        {
            if (manufacturer == null)
            {
                throw new ArgumentNullException("manufacturer", "The manufacturer was null.");
            }

            var originalCommandTimeout = this.EntityContext.CommandTimeout;

            try
            {
                this.EntityContext.CommandTimeout = 600;
                this.EntityContext.DeleteManufacturer(manufacturer.Guid);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, SourceDatabase);
            }
            finally
            {
                this.EntityContext.CommandTimeout = originalCommandTimeout;
            }
        }
    }
}
