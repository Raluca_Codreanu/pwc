﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Implements operations for Consumable.
    /// </summary>
    internal class ConsumableRepository : Repository<Consumable>, IConsumableRepository
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// A query that fully loads consumables.
        /// </summary>
        private IQueryable<Consumable> queryFull;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConsumableRepository" /> class.
        /// </summary>
        /// <param name="entityContext">The object context to be used for data operations.</param>
        /// <param name="sourceDatabase">A value indicating the database on which this repository operates.</param>
        public ConsumableRepository(DataEntities entityContext, DbIdentifier sourceDatabase)
            : base(entityContext, sourceDatabase)
        {
            queryFull = from consumable in this.EntityContext.ConsumableSet
                            .Include(c => c.AmountUnitBase)
                            .Include(c => c.Manufacturer.Owner)
                            .Include(c => c.PriceUnitBase)
                            .Include(c => c.ProcessStep)
                            .Include(c => c.Owner)
                        select consumable;
        }

        /// <summary>
        /// Retrieves the consumable based on the id.
        /// </summary>
        /// <param name="id">The consumable id.</param>
        /// <param name="noTracking">Option specifying if the MergeOption should be NoTracking.</param>
        /// <returns>The consumable.</returns>
        public Consumable GetById(Guid id, bool noTracking = false)
        {
            return this.GetByIds(new List<Guid> { id }, noTracking).FirstOrDefault();
        }

        /// <summary>
        /// Retrieves the consumables based on the ids list.
        /// </summary>
        /// <param name="ids">The ids of the consumables to get.</param>
        /// <param name="noTracking">Option specifying if the MergeOption should be NoTracking.</param>
        /// <returns>A collection of consumables.</returns>
        public Collection<Consumable> GetByIds(IEnumerable<Guid> ids, bool noTracking = false)
        {
            var query = from consumable in queryFull
                        where ids.Contains(consumable.Guid)
                        select consumable;

            if (noTracking)
            {
                query = query.AsNoTracking();
            }
            else
            {
                query.SetMergeOption(MergeOption.PreserveChanges);
            }

            try
            {
                return new Collection<Consumable>(query.ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Retrieves all the master data consumables
        /// </summary>
        /// <returns>A collection containing all the master data consumables</returns>
        public Collection<Consumable> GetAllMasterData()
        {
            var query = from consumable in queryFull
                        where consumable.IsMasterData
                        select consumable;

            try
            {
                return new Collection<Consumable>(query.ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the guid of the parent project of a specified consumable.
        /// </summary>
        /// <param name="consumable">The consumable.</param>
        /// <returns>The guid or Guid.Empty if an error occurs.</returns>
        public Guid GetParentProjectId(Consumable consumable)
        {
            if (consumable == null)
            {
                throw new ArgumentNullException("consumable", "Consumable was null.");
            }

            try
            {
                return this.EntityContext.FindTopParentId(consumable.Guid, "Consumables", 1);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the guid of the parent assembly of a specified consumable.
        /// </summary>
        /// <param name="consumable">The consumable.</param>
        /// <returns>The guid or Guid.Empty if an error occurs.</returns>
        public Guid GetParentAssemblyId(Consumable consumable)
        {
            if (consumable == null)
            {
                throw new ArgumentNullException("consumable", "Consumable was null.");
            }

            try
            {
                return this.EntityContext.FindTopParentId(consumable.Guid, "Consumables", 2);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the id of the consumable's parent process step.
        /// </summary>
        /// <param name="consumable">The consumable.</param>
        /// <returns>The guid or Guid.Empty if no parent process step is found.</returns>
        public Guid GetParentProcessStepId(Consumable consumable)
        {
            var query = from cons in this.EntityContext.ConsumableSet
                            .Include(p => p.ProcessStep)
                        where cons.Guid == consumable.Guid
                        select cons.ProcessStep;

            try
            {
                var processStep = query.FirstOrDefault();
                return processStep != null ? processStep.Guid : Guid.Empty;
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Deletes the specified consumable from the repository using a stored procedure.
        /// The actual deletion from the database is performed during this call (when the stored procedure is called), not when SaveChanges() is called.
        /// </summary>
        /// <param name="consumable">The consumable to delete.</param>
        public void DeleteByStoredProcedure(Consumable consumable)
        {
            if (consumable == null)
            {
                throw new ArgumentNullException("consumable", "The consumable was null.");
            }

            var originalCommandTimeout = this.EntityContext.CommandTimeout;

            try
            {
                this.EntityContext.CommandTimeout = 600;

                this.EntityContext.DeleteConsumable(consumable.Guid);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, SourceDatabase);
            }
            finally
            {
                this.EntityContext.CommandTimeout = originalCommandTimeout;
            }
        }
    }
}
