﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Implements operations for Die.
    /// </summary>
    internal class DieRepository : Repository<Die>, IDieRepository
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// A query that fully loads consumables.
        /// </summary>
        private IQueryable<Die> queryFull;

        /// <summary>
        /// Initializes a new instance of the <see cref="DieRepository"/> class.
        /// </summary>
        /// <param name="entityContext">The object context to be used for data operations.</param>
        /// <param name="sourceDatabase">A value indicating the database on which this repository operates.</param>
        public DieRepository(DataEntities entityContext, DbIdentifier sourceDatabase)
            : base(entityContext, sourceDatabase)
        {
            queryFull = from die in EntityContext.DieSet
                            .Include(d => d.Manufacturer.Owner)
                            .Include(d => d.Owner)
                            .Include(d => d.ProcessStep)
                        select die;
        }

        /// <summary>
        /// Retrieves the die based on the id.
        /// </summary>
        /// <param name="id">The die id.</param>
        /// <param name="noTracking">Option specifying if the MergeOption should be NoTracking.</param>
        /// <returns>The die.</returns>
        public Die GetById(Guid id, bool noTracking = false)
        {
            return this.GetByIds(new List<Guid> { id }, noTracking).FirstOrDefault();
        }

        /// <summary>
        /// Retrieves the dies based on the ids list.
        /// </summary>
        /// <param name="ids">The ids of the dies to get.</param>
        /// <param name="noTracking">Option specifying if the MergeOption should be NoTracking.</param>
        /// <returns>A collection of dies.</returns>
        public Collection<Die> GetByIds(IEnumerable<Guid> ids, bool noTracking = false)
        {
            var query = from die in queryFull
                        where ids.Contains(die.Guid)
                        select die;

            if (noTracking)
            {
                query = query.AsNoTracking();
            }
            else
            {
                query.SetMergeOption(MergeOption.PreserveChanges);
            }

            try
            {
                return new Collection<Die>(query.ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Retrieves all the master data Dies
        /// </summary>
        /// <returns>A collection containing all the master data Dies</returns>
        public Collection<Die> GetAllMasterData()
        {
            var query = from die in queryFull
                        where die.IsMasterData
                        select die;

            try
            {
                return new Collection<Die>(query.ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the guid of the parent project of a specified die.
        /// </summary>
        /// <param name="die">The die.</param>
        /// <returns>The guid or Guid.Empty if an error occurs.</returns>
        public Guid GetParentProjectId(Die die)
        {
            if (die == null)
            {
                throw new ArgumentNullException("die", "Die was null.");
            }

            try
            {
                return this.EntityContext.FindTopParentId(die.Guid, "Dies", 1);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the guid of the parent assembly of a specified die.
        /// </summary>
        /// <param name="die">The die.</param>
        /// <returns>The guid or Guid.Empty if an error occurs.</returns>
        public Guid GetParentAssemblyId(Die die)
        {
            if (die == null)
            {
                throw new ArgumentNullException("die", "Die was null.");
            }

            try
            {
                return this.EntityContext.FindTopParentId(die.Guid, "Dies", 2);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the id of the die's parent process step.
        /// </summary>
        /// <param name="die">The die.</param>
        /// <returns>The guid or Guid.Empty if no parent process step is found.</returns>
        public Guid GetParentProcessStepId(Die die)
        {
            var query = from d in this.EntityContext.DieSet
                            .Include(p => p.ProcessStep)
                        where d.Guid == die.Guid
                        select d.ProcessStep;

            try
            {
                var processStep = query.FirstOrDefault();
                return processStep != null ? processStep.Guid : Guid.Empty;
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Deletes the specified die from the repository using a stored procedure.
        /// The actual deletion from the database is performed during this call (when the stored procedure is called), not when SaveChanges() is called.
        /// </summary>
        /// <param name="die">The die to delete.</param>
        public void DeleteByStoredProcedure(Die die)
        {
            if (die == null)
            {
                throw new ArgumentNullException("die", "The die was null.");
            }

            var originalCommandTimeout = this.EntityContext.CommandTimeout;

            try
            {
                this.EntityContext.CommandTimeout = 600;
                this.EntityContext.DeleteDie(die.Guid);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, SourceDatabase);
            }
            finally
            {
                this.EntityContext.CommandTimeout = originalCommandTimeout;
            }
        }
    }
}
