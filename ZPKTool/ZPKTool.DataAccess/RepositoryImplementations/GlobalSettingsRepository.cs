﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Implements operations for GlobalSettings.
    /// </summary>
    internal class GlobalSettingsRepository : Repository<GlobalSetting>, IGlobalSettingsRepository
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Initializes a new instance of the <see cref="GlobalSettingsRepository"/> class.
        /// </summary>
        /// <param name="entityContext">The object context to be used for data operations.</param>
        /// <param name="sourceDatabase">A value indicating the database on which this repository operates.</param>
        public GlobalSettingsRepository(DataEntities entityContext, DbIdentifier sourceDatabase)
            : base(entityContext, sourceDatabase)
        {
        }

        /// <summary>
        /// Gets all global settings.
        /// </summary>
        /// <returns>A list of global settings.</returns>
        public IEnumerable<GlobalSetting> GetAllGlobalSettings()
        {
            var query = from item in this.EntityContext.GlobalSettingsSet
                        select item;
            try
            {
                return query.ToList();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the setting containing the database version.
        /// </summary>
        /// <returns>
        /// A <see cref="GlobalSetting" /> instance
        /// </returns>
        public GlobalSetting GetVersion()
        {
            var key = Encryption.Encrypt("Version", Encryption.EncryptionPassword);

            var query = from item in EntityContext.GlobalSettingsSet
                        where item.EncryptedKey == key
                        select item;
            try
            {
                return query.FirstOrDefault();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }
    }
}
