﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Implements the MaterialsClassification Repository.
    /// </summary>
    internal class MaterialsClassificationRepository : Repository<MaterialsClassification>, IMaterialsClassificationRepository
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Initializes a new instance of the <see cref="MaterialsClassificationRepository"/> class.
        /// </summary>
        /// <param name="entityContext">The object context to be used for data operations.</param>
        /// <param name="sourceDatabase">A value indicating the database on which this repository operates.</param>
        public MaterialsClassificationRepository(DataEntities entityContext, DbIdentifier sourceDatabase)
            : base(entityContext, sourceDatabase)
        {
        }

        #region IMaterialsClassificationRepository Members

        /// <summary>
        /// Load the entire machines classification tree from the database, flattened. The hierarchy can be reconstructed using the Parent property
        /// of the MachinesClassification instances.
        /// </summary>
        /// <returns>
        /// A collection containing the flattened MachinesClassification hierarchy.
        /// </returns>
        public Collection<MaterialsClassification> LoadClassificationTree()
        {
            var query = from cls in this.EntityContext.MaterialsClassificationSet
                        where !cls.IsReleased
                        select cls;

            try
            {
                return new Collection<MaterialsClassification>(query.ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets a material classification item by name
        /// </summary>
        /// <param name="classificationName">Name of the classification</param>
        /// <returns>
        /// The classification or null if is not found.
        /// </returns>
        public MaterialsClassification GetByName(string classificationName)
        {
            var query = from cls in this.EntityContext.MaterialsClassificationSet
                        where cls.Name == classificationName && !cls.IsReleased
                        select cls;

            try
            {
                return query.FirstOrDefault();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        #endregion
    }
}
