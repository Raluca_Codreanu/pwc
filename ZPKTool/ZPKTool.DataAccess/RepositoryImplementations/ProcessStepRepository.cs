﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Implements operations for ProcessStep.
    /// </summary>
    internal class ProcessStepRepository : Repository<ProcessStep>, IProcessStepRepository
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessStepRepository"/> class.
        /// </summary>
        /// <param name="entityContext">The object context to be used for data operations.</param>
        /// <param name="sourceDatabase">A value indicating the database on which this repository operates.</param>
        public ProcessStepRepository(DataEntities entityContext, DbIdentifier sourceDatabase)
            : base(entityContext, sourceDatabase)
        {
        }

        /// <summary>
        /// Gets the id of the parent project of a specified processStep.
        /// </summary>
        /// <param name="processStep">The processStep.</param>
        /// <returns>The guid or Guid.Empty if an error occurs.</returns>
        public Guid GetParentProjectId(ProcessStep processStep)
        {
            if (processStep == null)
            {
                throw new ArgumentNullException("processStep", "ProcessStep was null.");
            }

            try
            {
                return this.EntityContext.FindTopParentId(processStep.Guid, "ProcessSteps", 1);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the guid of the parent assembly of a specified processStep.
        /// </summary>
        /// <param name="processStep">The process step.</param>
        /// <returns>The guid or Guid.Empty if an error occurs.</returns>
        public Guid GetParentAssemblyId(ProcessStep processStep)
        {
            if (processStep == null)
            {
                throw new ArgumentNullException("processStep", "ProcessStep was null.");
            }

            try
            {
                return this.EntityContext.FindTopParentId(processStep.Guid, "ProcessSteps", 2);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the process step with the specified id fully loaded.
        /// </summary>
        /// <param name="stepId">The step id.</param>
        /// <returns>The step or null if not found.</returns>
        public ProcessStep GetProcessStepFull(Guid stepId)
        {
            try
            {
                var context = this.EntityContext;
                var mergeOption = MergeOption.PreserveChanges;

                // Load the step data
                var stepQuery = from step in context.ProcessStepSet
                                    .Include(s => s.Owner)
                                    .Include(s => s.CycleTimeCalculations)
                                    .Include(s => s.CycleTimeUnit)
                                    .Include(s => s.MaxDownTimeUnit)
                                    .Include(s => s.ProcessTimeUnit)
                                    .Include(s => s.SetupTimeUnit)
                                    .Include(s => s.SubType)
                                    .Include(s => s.Type)
                                    .Include(s => s.AssemblyAmounts)
                                    .Include(s => s.PartAmounts)
                                where step.Guid == stepId
                                select step;

                stepQuery.SetMergeOption(mergeOption);
                var loadedStep = stepQuery.FirstOrDefault();

                if (loadedStep != null)
                {
                    // Load the step's commodities only for assembling process steps because they exist only in them.
                    if (loadedStep is AssemblyProcessStep)
                    {
                        var commoditiesQuery = from commodity in context.CommoditySet
                                                   .Include(c => c.Manufacturer)
                                                   .Include(c => c.WeightUnitBase)
                                               where commodity.ProcessStep.Guid == loadedStep.Guid
                                               select commodity;
                        commoditiesQuery.SetMergeOption(mergeOption);
                        commoditiesQuery.ToList();
                    }

                    // Load the step's consumables
                    var consumablesQuery = from consumable in context.ConsumableSet
                                               .Include(c => c.Manufacturer)
                                               .Include(c => c.AmountUnitBase)
                                               .Include(c => c.PriceUnitBase)
                                           where consumable.ProcessStep.Guid == loadedStep.Guid
                                           select consumable;
                    consumablesQuery.SetMergeOption(mergeOption);
                    consumablesQuery.ToList();

                    // Load the step's dies
                    var diesQuery = from die in context.DieSet
                                        .Include(d => d.Manufacturer)
                                    where die.ProcessStep.Guid == loadedStep.Guid
                                    select die;
                    diesQuery.SetMergeOption(mergeOption);
                    diesQuery.ToList();

                    // Load the step's machines
                    var machinesQuery = from machine in context.MachineSet
                                            .Include(m => m.Manufacturer)
                                            .Include(m => m.ClassificationLevel4)
                                            .Include(m => m.MainClassification)
                                            .Include(m => m.SubClassification)
                                            .Include(m => m.TypeClassification)
                                        where machine.ProcessStep.Guid == loadedStep.Guid
                                        select machine;
                    machinesQuery.SetMergeOption(mergeOption);
                    machinesQuery.ToList();
                }

                return loadedStep;
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the ProcessStep to which a specified entity belongs.
        /// </summary>
        /// <param name="entity">The entity to get it's parent.</param>
        /// <returns>
        /// The parent ProcessStep of the given entity.
        /// </returns>
        public ProcessStep GetParentProcessStep(object entity)
        {
            IQueryable<ProcessStep> query = null;
            Guid entityId = Guid.Empty;

            IIdentifiable identifiableObj = entity as IIdentifiable;
            if (identifiableObj != null)
            {
                entityId = identifiableObj.Guid;
            }

            if (entity is Machine)
            {
                query = from processStep in EntityContext.ProcessStepSet
                        join machine in EntityContext.MachineSet on processStep equals machine.ProcessStep
                        where machine.Guid == entityId
                        select processStep;
            }
            else if (entity is Consumable)
            {
                query = from processStep in EntityContext.ProcessStepSet
                        join consumable in EntityContext.ConsumableSet on processStep equals consumable.ProcessStep
                        where consumable.Guid == entityId
                        select processStep;
            }
            else if (entity is Commodity)
            {
                query = from processStep in EntityContext.ProcessStepSet
                        join commodity in EntityContext.CommoditySet on processStep equals commodity.ProcessStep
                        where commodity.Guid == entityId
                        select processStep;
            }
            else if (entity is Die)
            {
                query = from processStep in EntityContext.ProcessStepSet
                        join die in EntityContext.DieSet on processStep equals die.ProcessStep
                        where die.Guid == entityId
                        select processStep;
            }

            if (entityId != Guid.Empty && query != null)
            {
                try
                {
                    return query.AsNoTracking().FirstOrDefault();
                }
                catch (DataException ex)
                {
                    log.ErrorException("Data error.", ex);
                    throw Utils.ParseDataException(ex, this.SourceDatabase);
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Retrieves the process steps based on the ids list.
        /// </summary>
        /// <param name="ids">The ids of the process steps to get.</param>
        /// <returns>A collection of process steps.</returns>
        public Collection<ProcessStep> GetByIds(IEnumerable<Guid> ids)
        {
            var query = from processStep in this.EntityContext.ProcessStepSet
                        where ids.Contains(processStep.Guid)
                        select processStep;

            try
            {
                return new Collection<ProcessStep>(query.AsNoTracking().ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Deletes the specified process step from the repository using a stored procedure. All sub-entities owned by the process step are also deleted.
        /// The actual deletion from the database is performed during this call (when the stored procedure is called), not when SaveChanges() is called.
        /// </summary>
        /// <param name="processStep">The process step to delete.</param>
        public void DeleteByStoredProcedure(ProcessStep processStep)
        {
            if (processStep == null)
            {
                throw new ArgumentNullException("processStep", "The process step was null.");
            }

            var originalCommandTimeout = this.EntityContext.CommandTimeout;

            try
            {
                this.EntityContext.CommandTimeout = 600;
                this.EntityContext.DeleteProcessStep(processStep.Guid);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, SourceDatabase);
            }
            finally
            {
                this.EntityContext.CommandTimeout = originalCommandTimeout;
            }
        }

        /// <summary>
        /// Gets the id of the process step's parent process.
        /// </summary>
        /// <param name="processStep">The process step.</param>
        /// <returns>The guid or Guid.Empty if no parent process is found.</returns>
        public Guid GetParentProcessId(ProcessStep processStep)
        {
            var query = from procStep in this.EntityContext.ProcessStepSet
                            .Include(p => p.Process)
                        where procStep.Guid == processStep.Guid
                        select procStep.Process;

            try
            {
                var process = query.FirstOrDefault();
                return process != null ? process.Guid : Guid.Empty;
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }
    }
}
