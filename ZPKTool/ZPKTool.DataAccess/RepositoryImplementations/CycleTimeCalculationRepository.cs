﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Implements operations for CycleTimeCalculation.
    /// </summary>
    internal class CycleTimeCalculationRepository : Repository<CycleTimeCalculation>, ICycleTimeCalculationRepository
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Initializes a new instance of the <see cref="CycleTimeCalculationRepository"/> class.
        /// </summary>
        /// <param name="entityContext">The object context to be used for data operations.</param>
        /// <param name="sourceDatabase">A value indicating the database on which this repository operates.</param>
        public CycleTimeCalculationRepository(DataEntities entityContext, DbIdentifier sourceDatabase)
            : base(entityContext, sourceDatabase)
        {
        }

        /// <summary>
        /// Deletes the specified cycle time calculation from the repository using a stored procedure.
        /// The actual deletion from the database is performed during this call (when the stored procedure is called), not when SaveChanges() is called.
        /// </summary>
        /// <param name="cycleTimeCalculation">The cycle time calculation to delete.</param>
        public void DeleteByStoredProcedure(CycleTimeCalculation cycleTimeCalculation)
        {
            if (cycleTimeCalculation == null)
            {
                throw new ArgumentNullException("cycleTimeCalculation", "The cycle time calculation was null.");
            }

            var originalCommandTimeout = this.EntityContext.CommandTimeout;

            try
            {
                this.EntityContext.CommandTimeout = 600;
                this.EntityContext.DeleteCycleTimeCalculation(cycleTimeCalculation.Guid);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, SourceDatabase);
            }
            finally
            {
                this.EntityContext.CommandTimeout = originalCommandTimeout;
            }
        }
    }
}
