﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Entity;
using System.Linq;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Implements operations for Supplier
    /// </summary>
    internal class SupplierRepository : Repository<Customer>, ISupplierRepository
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// A query that fully loads the supplier
        /// </summary>
        private IQueryable<Customer> queryFull;

        /// <summary>
        /// Initializes a new instance of the <see cref="SupplierRepository"/> class.
        /// </summary>
        /// <param name="entityContext">The object context to be used for data operations.</param>
        /// <param name="sourceDatabase">A value indicating the database on which this repository operates.</param>
        public SupplierRepository(DataEntities entityContext, DbIdentifier sourceDatabase)
            : base(entityContext, sourceDatabase)
        {
            queryFull = from supplier in this.EntityContext.CustomerSet
                            .Include(s => s.Owner)
                        select supplier;
        }

        /// <summary>
        /// Retrieves the master data suppliers.
        /// </summary>
        /// <returns>The collection of master data suppliers</returns>
        public Collection<Customer> FindAllMasterData()
        {
            var query = from supplier in queryFull
                        where supplier.IsMasterData
                        select supplier;

            try
            {
                return new Collection<Customer>(query.ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Retrieves the supplier with a given guid.
        /// </summary>
        /// <param name="supplierGuid">The guid of the supplier to retrieve</param>
        /// <param name="noTracking">Specifies if the supplier will be retrieved using the NoTracking option</param>
        /// <returns>The supplier.</returns>
        public Customer FindById(Guid supplierGuid, bool noTracking = false)
        {
            var query = from supplier in queryFull
                        where supplier.Guid == supplierGuid
                        select supplier;

            if (noTracking)
            {
                query = query.AsNoTracking();
            }

            try
            {
                return query.FirstOrDefault();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Deletes the specified supplier from the repository using a stored procedure.
        /// The actual deletion from the database is performed during this call (when the stored procedure is called), not when SaveChanges() is called.
        /// </summary>
        /// <param name="supplier">The supplier to delete.</param>
        public void DeleteByStoredProcedure(Customer supplier)
        {
            if (supplier == null)
            {
                throw new ArgumentNullException("supplier", "The supplier was null.");
            }

            var originalCommandTimeout = this.EntityContext.CommandTimeout;

            try
            {
                this.EntityContext.CommandTimeout = 600;
                this.EntityContext.DeleteCustomer(supplier.Guid);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, SourceDatabase);
            }
            finally
            {
                this.EntityContext.CommandTimeout = originalCommandTimeout;
            }
        }
    }
}
