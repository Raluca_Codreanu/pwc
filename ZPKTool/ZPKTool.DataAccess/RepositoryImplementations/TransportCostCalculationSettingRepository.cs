﻿using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Implements operations for TransportCostCalculationSetting.
    /// </summary>
    internal class TransportCostCalculationSettingRepository : Repository<TransportCostCalculationSetting>, ITransportCostCalculationSettingRepository
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Initializes a new instance of the <see cref="TransportCostCalculationSettingRepository" /> class.
        /// </summary>
        /// <param name="entityContext">The entity context.</param>
        /// <param name="sourceDatabase">The source database.</param>
        public TransportCostCalculationSettingRepository(DataEntities entityContext, DbIdentifier sourceDatabase)
            : base(entityContext, sourceDatabase)
        {
        }

        /// <summary>
        /// Gets the last saved setting of an owner.
        /// </summary>
        /// <param name="ownerId">The owner id.</param>
        /// <returns>The latest owner's setting.</returns>
        public TransportCostCalculationSetting GetLatestSetting(Guid ownerId)
        {
            try
            {
                var query = from setting in this.EntityContext.TransportCostCalculationSettingSet
                                .Include(p => p.Owner)
                            where setting.Owner.Guid == ownerId
                            orderby setting.Timestamp descending
                            select setting;

                return query.AsNoTracking().FirstOrDefault();
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the guid of the parent project of a specified transport cost calculation setting.
        /// </summary>
        /// <param name="setting">The setting.</param>
        /// <returns>The guid or Guid.Empty if an error occurs.</returns>
        public Guid GetParentProjectId(TransportCostCalculationSetting setting)
        {
            if (setting == null)
            {
                throw new ArgumentNullException("setting", "Setting was null.");
            }

            try
            {
                return this.EntityContext.FindTopParentId(setting.Guid, "TransportCostCalculationSettings", 1);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }
    }
}
