﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Implements operations for Commodity.
    /// </summary>
    internal class CommodityRepository : Repository<Commodity>, ICommodityRepository
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// A query that fully loads commodities.
        /// </summary>
        private IQueryable<Commodity> queryFull;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommodityRepository"/> class.
        /// </summary>
        /// <param name="entityContext">The object context to be used for data operations.</param>
        /// <param name="sourceDatabase">A value indicating the database on which this repository operates.</param>
        public CommodityRepository(DataEntities entityContext, DbIdentifier sourceDatabase)
            : base(entityContext, sourceDatabase)
        {
            this.queryFull = from commodity in this.EntityContext.CommoditySet
                                .Include(c => c.Part)
                                .Include(c => c.ProcessStep)
                                .Include(c => c.Owner)
                                .Include(c => c.Manufacturer.Owner)
                                .Include(c => c.WeightUnitBase)
                             select commodity;
        }

        /// <summary>
        /// Retrieves the commodity based on the id.
        /// </summary>
        /// <param name="id">The commodity id.</param>
        /// <param name="noTracking">Option specifying if the MergeOption should be NoTracking.</param>
        /// <returns>The commodity.</returns>
        public Commodity GetById(Guid id, bool noTracking = false)
        {
            return this.GetByIds(new List<Guid> { id }, noTracking).FirstOrDefault();
        }

        /// <summary>
        /// Retrieves the commodities based on the ids list.
        /// </summary>
        /// <param name="ids">The ids of the commodities to get.</param>
        /// <param name="noTracking">Option specifying if the MergeOption should be NoTracking.</param>
        /// <returns>A collection of commodities.</returns>
        public Collection<Commodity> GetByIds(IEnumerable<Guid> ids, bool noTracking = false)
        {
            var query = from commodity in queryFull
                        where ids.Contains(commodity.Guid)
                        select commodity;

            if (noTracking)
            {
                query = query.AsNoTracking();
            }
            else
            {
                query.SetMergeOption(MergeOption.PreserveChanges);
            }

            try
            {
                return new Collection<Commodity>(query.ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Retrieves all the master data commodities
        /// </summary>
        /// <returns>A collection containing all the master data commodities</returns>
        public Collection<Commodity> GetAllMasterData()
        {
            var query = from commodity in queryFull
                        where commodity.IsMasterData
                        select commodity;

            try
            {
                return new Collection<Commodity>(query.ToList());
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the guid of the parent project of a specified commodity.
        /// </summary>
        /// <param name="commodity">The commodity.</param>
        /// <returns>The guid or Guid.Empty if an error occurs.</returns>
        public Guid GetParentProjectId(Commodity commodity)
        {
            if (commodity == null)
            {
                throw new ArgumentNullException("commodity", "Commodity was null.");
            }

            try
            {
                return this.EntityContext.FindTopParentId(commodity.Guid, "Commodities", 1);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the guid of the parent assembly of a specified commodity.
        /// </summary>
        /// <param name="commodity">The commodity.</param>
        /// <returns>The guid or Guid.Empty if an error occurs.</returns>
        public Guid GetParentAssemblyId(Commodity commodity)
        {
            if (commodity == null)
            {
                throw new ArgumentNullException("commodity", "Commodity was null.");
            }

            try
            {
                return this.EntityContext.FindTopParentId(commodity.Guid, "Commodities", 2);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the id of the commodity's parent process step.
        /// </summary>
        /// <param name="commodity">The commodity.</param>
        /// <returns>The guid or Guid.Empty if no parent process step is found.</returns>
        public Guid GetParentProcessStepId(Commodity commodity)
        {
            var query = from com in this.EntityContext.CommoditySet
                            .Include(p => p.ProcessStep)
                        where com.Guid == commodity.Guid
                        select com.ProcessStep;

            try
            {
                var processStep = query.FirstOrDefault();
                return processStep != null ? processStep.Guid : Guid.Empty;
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Gets the id of the commodity's parent part.
        /// </summary>
        /// <param name="commodity">The commodity.</param>
        /// <returns>The guid or Guid.Empty if no parent part is found.</returns>
        public Guid GetParentPartId(Commodity commodity)
        {
            var query = from com in this.EntityContext.CommoditySet
                            .Include(p => p.Part)
                        where com.Guid == com.Guid
                        select com.Part;

            try
            {
                var part = query.FirstOrDefault();
                return part != null ? part.Guid : Guid.Empty;
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, this.SourceDatabase);
            }
        }

        /// <summary>
        /// Deletes the specified commodity from the repository using a stored procedure.
        /// The actual deletion from the database is performed during this call (when the stored procedure is called), not when SaveChanges() is called.
        /// </summary>
        /// <param name="commodity">The commodity to delete.</param>
        public void DeleteByStoredProcedure(Commodity commodity)
        {
            if (commodity == null)
            {
                throw new ArgumentNullException("commodity", "The commodity was null.");
            }

            var originalCommandTimeout = this.EntityContext.CommandTimeout;

            try
            {
                this.EntityContext.CommandTimeout = 600;

                this.EntityContext.DeleteCommodity(commodity.Guid);
            }
            catch (DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw Utils.ParseDataException(ex, SourceDatabase);
            }
            finally
            {
                this.EntityContext.CommandTimeout = originalCommandTimeout;
            }
        }
    }
}
