﻿using System;
using System.Runtime.Serialization;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Represents the Concurrency Exception.
    /// </summary>
    [Serializable]
    public class ConcurrencyException : DataAccessException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConcurrencyException"/> class.
        /// </summary>
        public ConcurrencyException()
            : this(null, false, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConcurrencyException" /> class.
        /// </summary>
        /// <param name="errorCode">The error code.</param>
        public ConcurrencyException(string errorCode)
            : this(null, false, null)
        {
            this.ErrorCode = errorCode;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConcurrencyException"/> class.
        /// </summary>
        /// <param name="errorCode">The error code.</param>
        /// <param name="ex">The exception object.</param>
        public ConcurrencyException(string errorCode, Exception ex)
            : this(ex, false, null)
        {
            this.ErrorCode = errorCode;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConcurrencyException" /> class.
        /// </summary>
        /// <param name="ex">The inner Exception.</param>
        /// <param name="autoHandle">if set to true the concurrency issue is handled automatically, without asking the user for input.</param>
        /// <param name="dataSourceManager">The data source manager for which the exception occurred.</param>
        public ConcurrencyException(Exception ex, bool autoHandle, IDataSourceManager dataSourceManager)
            : base(string.Empty, ex)
        {
            this.AutoHandle = autoHandle;
            this.DataSourceManager = dataSourceManager;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConcurrencyException"/> class.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the exception being thrown.</param>
        /// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the source or destination.</param>
        protected ConcurrencyException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        /// <summary>
        /// Gets or sets a value indicating whether to auto handle the exception or not.
        /// </summary>
        public bool AutoHandle { get; set; }

        /// <summary>
        /// Gets or sets the Context this exception is referring to.
        /// </summary>
        public IDataSourceManager DataSourceManager { get; set; }

        /// <summary>
        /// When overridden in a derived class, sets the <see cref="T:System.Runtime.Serialization.SerializationInfo" /> with information about the exception.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the exception being thrown.</param>
        /// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the source or destination.</param>
        /// <PermissionSet>
        ///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Read="*AllFiles*" PathDiscovery="*AllFiles*" />
        ///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="SerializationFormatter" />
        /// </PermissionSet>
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);

            info.AddValue("AutoHandle", this.AutoHandle);
            info.AddValue("DataSourceManager", this.DataSourceManager);
        }
    }
}