﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines operations available for commodity price history repository.
    /// </summary>
    public interface ICommodityPriceHistoryRepository : IRepository<CommoditiesPriceHistory>
    {
    }
}
