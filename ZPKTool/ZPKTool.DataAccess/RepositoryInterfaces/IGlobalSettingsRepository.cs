﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines operations available for GlobalSettings.
    /// </summary>
    public interface IGlobalSettingsRepository : IRepository<GlobalSetting>
    {
        /// <summary>
        /// Gets all global settings.
        /// </summary>
        /// <returns>A list of global settings.</returns>
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "The method performs database access.")]
        IEnumerable<GlobalSetting> GetAllGlobalSettings();

        /// <summary>
        /// Gets the setting containing the database version.
        /// </summary>
        /// <returns>A <see cref="GlobalSetting"/> instance</returns>
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "Needed to get the value of just the Version setting.")]
        GlobalSetting GetVersion();
    }
}