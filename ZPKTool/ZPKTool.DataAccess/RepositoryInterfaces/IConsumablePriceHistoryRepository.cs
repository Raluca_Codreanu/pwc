﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines operations available for consumable price history repository.
    /// </summary>
    public interface IConsumablePriceHistoryRepository : IRepository<ConsumablesPriceHistory>
    {
    }
}
