﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines operations available for Commodity.
    /// </summary>
    public interface ICommodityRepository : IRepository<Commodity>
    {
        /// <summary>
        /// Retrieves the commodity based on the id.
        /// </summary>
        /// <param name="id">The commodity id.</param>
        /// <param name="noTracking">Option specifying if the MergeOption should be NoTracking.</param>
        /// <returns>The commodity.</returns>
        Commodity GetById(Guid id, bool noTracking = false);

        /// <summary>
        /// Retrieves the commodities based on the ids list.
        /// </summary>
        /// <param name="ids">The ids of the commodities to get.</param>
        /// <param name="noTracking">Option specifying if the MergeOption should be NoTracking.</param>
        /// <returns>A collection of commodities.</returns>
        Collection<Commodity> GetByIds(IEnumerable<Guid> ids, bool noTracking = false);

        /// <summary>
        /// Retrieves all the master data commodities
        /// </summary>
        /// <returns>A collection containing all the master data commodities</returns>
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "The method can not be converted to a property because it accesses a database.")]
        Collection<Commodity> GetAllMasterData();

        /// <summary>
        /// Gets the id of the parent project of a specified commodity.
        /// </summary>
        /// <param name="commodity">The commodity.</param>
        /// <returns>The id or Guid.Empty if an error occurs.</returns>
        Guid GetParentProjectId(Commodity commodity);

        /// <summary>
        /// Gets the id of the parent assembly of a specified commodity.
        /// </summary>
        /// <param name="commodity">The commodity.</param>
        /// <returns>The id or Guid.Empty if an error occurs.</returns>
        Guid GetParentAssemblyId(Commodity commodity);

        /// <summary>
        /// Gets the id of the commodity's parent process step.
        /// </summary>
        /// <param name="commodity">The commodity.</param>
        /// <returns>The id or Guid.Empty if no parent process step is found.</returns>
        Guid GetParentProcessStepId(Commodity commodity);

        /// <summary>
        /// Gets the id of the commodity's parent part.
        /// </summary>
        /// <param name="commodity">The commodity.</param>
        /// <returns>The id or Guid.Empty if no parent part is found.</returns>
        Guid GetParentPartId(Commodity commodity);

        /// <summary>
        /// Deletes the specified commodity from the repository using a stored procedure.
        /// The actual deletion from the database is performed during this call (when the stored procedure is called), not when SaveChanges() is called.
        /// </summary>
        /// <param name="commodity">The commodity to delete.</param>
        void DeleteByStoredProcedure(Commodity commodity);
    }
}
