﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines operations available for TransportCostCalculationSetting.
    /// </summary>
    public interface ITransportCostCalculationSettingRepository : IRepository<TransportCostCalculationSetting>
    {
        /// <summary>
        /// Gets the last saved setting of the owner.
        /// </summary>
        /// <param name="ownerId">The owner id.</param>
        /// <returns>The latest owner's setting.</returns>
        TransportCostCalculationSetting GetLatestSetting(Guid ownerId);

        /// <summary>
        /// Gets the guid of the parent project of a specified transport cost calculation setting.
        /// </summary>
        /// <param name="setting">The setting.</param>
        /// <returns>The guid or Guid.Empty if an error occurs.</returns>
        Guid GetParentProjectId(TransportCostCalculationSetting setting);
    }
}
