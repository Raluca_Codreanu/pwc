﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines operations available for the trash bin.
    /// </summary>
    public interface ITrashBinRepository : IRepository<TrashBinItem>
    {
        /// <summary>
        /// Gets the number of trash bin items belonging to a specified user.
        /// </summary>
        /// <param name="userId">The id of the user whose trash bin items to count.</param>
        /// <returns>The number of trash bin items belonging to the specified user.</returns>
        int GetItemCount(Guid userId);

        /// <summary>
        /// Gets all trash bin items.
        /// </summary>
        /// <param name="ownerId">The id of the user whose items to get.</param>
        /// <param name="noTracking">if set to true change tracking is disabled for the returned object(s)..</param>
        /// <returns>A collection of trash bin items.</returns>
        Collection<TrashBinItem> GetAll(Guid ownerId, bool noTracking = false);

        /// <summary>
        /// Gets the trash bin item corresponding to a specified object.
        /// </summary>
        /// <param name="objectId">The object's id.</param>
        /// <returns>The trash bin item corresponding to the object or null if the object is not deleted.</returns>
        TrashBinItem GetItemOfObject(Guid objectId);

        /// <summary>
        /// Adds a trash bin item for the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        void AddItemForObject(ITrashable obj);

        /// <summary>
        /// Deletes the trash bin item of the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        void DeleteItemOfObject(ITrashable obj);

        /// <summary>
        /// Deletes the specified item to the trash bin.
        /// </summary>
        /// <param name="obj">The object to delete.</param>
        void DeleteToTrashBin(ITrashable obj);

        /// <summary>
        /// Restores the specified item from the trash bin.
        /// </summary>
        /// <param name="obj">The object to undelete.</param>
        void RestoreFromTrashBin(ITrashable obj);

        /// <summary>
        /// Deletes the specified trash bin item from the repository using a stored procedure. All sub-entities owned by the item are also deleted.
        /// The actual deletion from the database is performed during this call (when the stored procedure is called), not when SaveChanges() is called.
        /// </summary>
        /// <param name="trashBinItem">The trash bin item to delete.</param>
        void DeleteByStoredProcedure(TrashBinItem trashBinItem);
    }
}
