﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines operations available for Log.
    /// </summary>
    public interface ILogRepository : IRepository<Log>
    {
        /// <summary>
        /// Gets all logs.
        /// </summary>
        /// <returns>A list of logs.</returns>
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "The method performs database access.")]
        IEnumerable<Log> GetAllLogs();

        /// <summary>
        /// Gets the logs based on an interval.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns>A list of logs.</returns>
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "The method performs database access.")]
        IEnumerable<Log> GetLogs(DateTime startDate, DateTime endDate);
    }
}
