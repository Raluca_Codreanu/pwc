﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines operations on Manufacturer.
    /// </summary>
    public interface IManufacturerRepository : IRepository<Manufacturer>
    {
        /// <summary>
        /// Retrieves the manufacturer with a given id.
        /// </summary>
        /// <param name="id">The id of the manufacturer to retrieve.</param>
        /// <param name="noTracking">if set to true the change tracking for the resulted object is disabled.</param>
        /// <returns>
        /// The manufacturer or null if it was not found.
        /// </returns>
        Manufacturer GetById(Guid id, bool noTracking = false);

        /// <summary>
        /// Retrieves the parent of the manufacturer with the given id.
        /// </summary>
        /// <param name="id">The id of the manufacturer</param>
        /// <returns>The parent of the manufacturer, or null if no parent is found</returns>
        object GetParent(Guid id);

        /// <summary>
        /// Retrieves the master data manufacturers.
        /// </summary>
        /// <returns>The collection of master data manufacturers</returns>
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "The method performs database access.")]
        Collection<Manufacturer> GetAllMasterData();

        /// <summary>
        /// Deletes the specified manufacturer from the repository using a stored procedure.
        /// The actual deletion from the database is performed during this call (when the stored procedure is called), not when SaveChanges() is called.
        /// </summary>
        /// <param name="manufacturer">The manufacturer to delete.</param>
        void DeleteByStoredProcedure(Manufacturer manufacturer);
    }
}
