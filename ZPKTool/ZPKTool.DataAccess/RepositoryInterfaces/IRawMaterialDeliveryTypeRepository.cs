﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines the data operations of <see cref="RawMaterialDeliveryType"/>.
    /// </summary>
    public interface IRawMaterialDeliveryTypeRepository : IRepository<RawMaterialDeliveryType>
    {
        /// <summary>
        /// Gets a raw material delivery type based on its id.
        /// </summary>
        /// <param name="deliveryTypeId">The delivery type id.</param>
        /// <returns>
        /// The delivery type or null if is not found.
        /// </returns>
        RawMaterialDeliveryType FindById(Guid deliveryTypeId);

        /// <summary>
        /// Gets a raw material delivery type based on its name.
        /// </summary>
        /// <param name="deliveryTypeName">Name of the delivery type.</param>
        /// <returns>
        /// The delivery type or null if is not found.
        /// </returns>
        RawMaterialDeliveryType FindByName(string deliveryTypeName);

        /// <summary>
        /// Gets all raw material delivery types.
        /// </summary>
        /// <returns>
        /// A collection of RawMaterialDeliveryType objects.
        /// </returns>
        Collection<RawMaterialDeliveryType> FindAll();
    }
}
