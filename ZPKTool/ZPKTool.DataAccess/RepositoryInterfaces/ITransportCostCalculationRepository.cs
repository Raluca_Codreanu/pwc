﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines operations available for TransportCostCalculation.
    /// </summary>
    public interface ITransportCostCalculationRepository : IRepository<TransportCostCalculation>
    {
        /// <summary>
        /// Gets the guid of the parent project of a specified transport cost calculation.
        /// </summary>
        /// <param name="calculation">The calculation.</param>
        /// <returns>The guid or Guid.Empty if an error occurs.</returns>
        Guid GetParentProjectId(TransportCostCalculation calculation);
    }
}
