﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines the operations available for raw material price history repository.
    /// </summary>
    public interface IRawMaterialPriceHistoryRepository : IRepository<RawMaterialsPriceHistory>
    {
        /// <summary>
        /// Gets the price history.
        /// </summary>
        /// <param name="rawMaterialId">The raw material id.</param>
        /// <returns>A collection containing the history elements.</returns>
        Collection<RawMaterialsPriceHistory> GetPriceHistory(Guid rawMaterialId);
    }
}
