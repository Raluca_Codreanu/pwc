﻿using System;
using System.Collections.Generic;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines operations available for Bookmark.
    /// </summary>
    public interface IBookmarkRepository : IRepository<Bookmark>
    {
        /// <summary>
        /// Checks if an item has a bookmark.
        /// </summary>
        /// <param name="itemId">The id of the item.</param>
        /// <returns>True if a bookmark exists for the specified item, otherwise false.</returns>
        bool HasBookmark(Guid itemId);

        /// <summary>
        /// Gets the entity type and total count for each type that are bookmarked and not deleted. 
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <returns>A dictionary containing the entity type as key and the total count of entities that are not deleted and have a bookmark as a value.</returns>
        Dictionary<BookmarkedItemType, int> GetBookmarkedItemTypesCount(Guid userId);

        /// <summary>
        /// Gets the bookmark item corresponding to a specified entity.
        /// </summary>
        /// <param name="entityId">The entity id.</param>
        /// <returns>The bookmark item corresponding to the entity or null if the entity doesn't have a bookmark.</returns>
        Bookmark GetBookmark(Guid entityId);

        /// <summary>
        /// Deletes the bookmark item of the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        void DeleteBookmark(object obj);
    }
}