﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines operations on Media.
    /// </summary>
    public interface IMediaRepository : IRepository<Media>
    {
        /// <summary>
        /// Gets the meta data for the media having one of the specified types and belonging to the specified entity.
        /// </summary>
        /// <param name="entity">The entity whose media meta data to search.</param>
        /// <param name="types">The types of media to be searched.</param>
        /// <returns>The meta data of the media belonging to the entity and having one of the specified types.</returns>
        Collection<MediaMetaData> GetMediaMetaData(object entity, IEnumerable<MediaType> types);

        /// <summary>
        /// Gets the meta data for all media of the specified entities.
        /// </summary>
        /// <param name="entities">The entities.</param>
        /// <returns>The media meta data.</returns>
        Collection<MediaMetaData> GetMediaMetaData(IEnumerable<object> entities);

        /// <summary>
        /// Returns the media object specified by the meta data info.
        /// The media is detached from the context.
        /// </summary>
        /// <param name="metaData">Media meta data information</param>
        /// <param name="noTracking">Indicates whether change tracking is enabled for the returned object.</param>
        /// <returns>One media object.</returns>
        Media GetMedia(MediaMetaData metaData, bool noTracking = false);

        /// <summary>
        /// Gets the media objects identified by the metadata list.
        /// The returned media objects are detached from the data context.
        /// </summary>
        /// <param name="metadata">The metadata of the media to get.</param>
        /// <returns>A collection containing the specified media.</returns>
        Collection<Media> GetMedia(IEnumerable<MediaMetaData> metadata);

        /// <summary>
        /// Gets all media belonging to the specified entity.
        /// </summary>
        /// <param name="entity">The entity whose media to get.</param>
        /// <returns>
        /// The media objects belonging to the specified entity.
        /// </returns>
        Collection<Media> GetMedia(object entity);

        /// <summary>
        /// Gets all media having one of the specified types and belonging to the specified entity.
        /// </summary>
        /// <param name="entity">The entity whose media to get.</param>
        /// <param name="types">The types of media to get.</param>
        /// <returns>
        /// The media objects belonging to the specified entity.
        /// </returns>
        Collection<Media> GetMedia(object entity, IEnumerable<MediaType> types);

        /// <summary>
        /// Gets the media with the specified id. The result is attached to the data context.
        /// </summary>
        /// <param name="mediaId">The media id.</param>
        /// <returns>The media or null if not found</returns>
        Media GetById(Guid mediaId);

        /// <summary>
        /// Gets the media for the specified process steps.
        /// </summary>
        /// <param name="steps">The process steps.</param>
        /// <returns>
        /// A dictionary with the key containing the step id and the value being its associated media.
        /// If a step does not have media its id is not present in the dictionary with a null value associated to it.
        /// </returns>
        Dictionary<Guid, Media> GetMediaForProcessSteps(IEnumerable<ProcessStep> steps);

        /// <summary>
        /// Call stored procedure to delete the given media.
        /// </summary>
        /// <param name="mediaMetaData">Media meta data information referring the object to delete</param>
        void DeleteMedia(MediaMetaData mediaMetaData);

        /// <summary>
        /// Deletes all media objects of an entity (image/video and documents).
        /// </summary>
        /// <param name="entity">The entity.</param>
        void DeleteAllMedia(object entity);

        /// <summary>
        /// Deletes all the media (images,video,documents) of a specific entity
        /// </summary>
        /// <param name="entity">The entity from which media objects are deleted</param>
        /// <param name="types">The types which are deleted</param>
        void DeleteAllMedia(object entity, IEnumerable<MediaType> types);

        /// <summary>
        /// Deletes the specified media from the repository using a stored procedure. All sub-entities owned by the media are also deleted.
        /// The actual deletion from the database is performed during this call (when the stored procedure is called), not when SaveChanges() is called.
        /// </summary>
        /// <param name="media">The media to delete.</param>
        void DeleteByStoredProcedure(Media media);
    }
}
