﻿using System;
using System.Collections.ObjectModel;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines operations available for CountrySupplier.
    /// </summary>
    public interface ICountrySupplierRepository : IRepository<CountryState>
    {
        /// <summary>
        /// Checks if a supplier with the specified name already exists.
        /// </summary>
        /// <param name="supplierName">Name of the supplier to check if exists.</param>
        /// <returns>True if a supplier with the specified name exists; otherwise, false.</returns>
        bool CheckIfExists(string supplierName);

        /// <summary>
        /// Gets the supplier with the specified id.
        /// </summary>
        /// <param name="id">The supplier's id.</param>
        /// <returns>The supplier or null if it was not found.</returns>
        CountryState FindById(Guid id);

        /// <summary>
        /// Retrieves the country suppliers.
        /// </summary>
        /// <returns>The collection of country suppliers</returns>
        Collection<CountryState> FindAll();
    }
}
