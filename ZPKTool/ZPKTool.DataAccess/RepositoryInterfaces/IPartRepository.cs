﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines operations available for Part.
    /// </summary>
    public interface IPartRepository : IRepository<Part>
    {
        /// <summary>
        /// Gets the id of the project in which a specified part exists at any level in the assembly hierarchy.
        /// </summary>
        /// <param name="part">The part.</param>
        /// <returns>The parent project's id or Guid.Empty if the part is not part of a project.</returns>
        Guid GetParentProjectId(Part part);

        /// <summary>
        /// Gets the id of the project in which a specified part exists, optionally specifying whether the part can exist anywhere
        /// in assembly hierarchy or only directly in the project.
        /// </summary>
        /// <param name="part">The part.</param>
        /// <param name="partIsOnFirstLevel"> If set to true, it specifies that the part exists directly in the project;
        /// otherwise it means that the part can be anywhere in the assembly hierarchy. Basically, if is true it instructs the method to return the project id only
        /// if the part is directly in a project (and is not a sub-part of an assembly).</param>
        /// <returns>
        /// The parent project's id
        /// - or - Guid.Empty if <paramref name="partIsOnFirstLevel"/> is false and the part is not part of a project (at any level of the assemblies hierarchy)
        /// - or - Guid.Empty if if <paramref name="partIsOnFirstLevel"/> is true and the part is not a direct child of a project.
        /// </returns>
        Guid GetParentProjectId(Part part, bool partIsOnFirstLevel);

        /// <summary>
        /// Gets the id of the top-most parent assembly of a specified part.
        /// </summary>
        /// <param name="part">The part whose assembly to find.</param>
        /// <returns>The top-most assembly's id or Guid.Empty if the part is not part of an assembly.</returns>
        Guid GetTopParentAssemblyId(Part part);

        /// <summary>
        /// Gets the id of the (direct) parent assembly of a specified part.
        /// </summary>
        /// <param name="part">The part whose assembly to find.</param>
        /// <returns>The assembly's id or Guid.Empty if the part is not part of an assembly.</returns>
        Guid GetParentAssemblyId(Part part);

        /// <summary>
        /// Gets the part with the specified id fully loaded.
        /// </summary>
        /// <param name="partId">The part id.</param>
        /// <returns>
        /// The part or null if it was not found.
        /// </returns>
        Part GetPartFull(Guid partId);

        /// <summary>
        /// Gets the part with the specified id. No references are loaded.
        /// </summary>
        /// <param name="partId">The part id.</param>
        /// <param name="noTracking">if set to true disable change tracking for the retuned object(s).</param>
        /// <returns>The part or null if it is not found.</returns>
        Part GetPart(Guid partId, bool noTracking = true);

        /// <summary>
        /// Gets the part with its owner
        /// </summary>
        /// <param name="partGuid">The guid of the part</param>
        /// <returns>The part with the owner reference</returns>
        Part GetPartWithOwner(Guid partGuid);

        /// <summary>
        /// Gets a Part to view its details in trash bin.
        /// </summary>
        /// <param name="partId">The part id.</param>
        /// <returns>The Part with the specified id.</returns>
        Part GetPartForView(Guid partId);

        /// <summary>
        /// Gets the Part to which a specified entity belongs.
        /// </summary>
        /// <param name="entity">The entity to get it's parent.</param>
        /// <returns>The parent Part of the given entity.</returns>
        Part GetParentPart(object entity);

        /// <summary>
        /// Gets the parts of a project. The parts are barebones.
        /// </summary>
        /// <param name="projectGuid">The guid of the parent project.</param>
        /// <returns>The collection of child parts</returns>
        Collection<Part> GetProjectParts(Guid projectGuid);

        /// <summary>
        /// Gets all master data parts (excluding raw parts).
        /// </summary>
        /// <returns>A collection containing Parts (excluding raw parts).</returns>
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "The method performs database access.")]
        Collection<Part> GetAllMasterDataPart();

        /// <summary>
        /// Gets all master data raw parts.
        /// </summary>
        /// <returns>A collection containing RawParts</returns>
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "The method performs database access.")]
        Collection<Part> GetAllMasterDataRawPart();

        /// <summary>
        /// Gets the parts of a specified user, that are bookmarked, to be displayed in bookmarks tree.
        /// </summary>
        /// <param name="ownerId">The owner id.</param>
        /// <returns>A collection of parts.</returns>
        Collection<Part> GetBookmarkedParts(Guid ownerId);

        /// <summary>
        /// Retrieves the parts based on the ids list.
        /// </summary>
        /// <param name="ids">The ids of the parts to get.</param>
        /// <returns>A collection of parts.</returns>
        Collection<Part> GetByIds(IEnumerable<Guid> ids);

        /// <summary>
        /// Gets the id of the raw part's parent part.
        /// </summary>
        /// <param name="rawPartId">The raw part id.</param>
        /// <returns>The id or Guid.Empty if no parent part is found.</returns>
        Guid GetParentOfRawPartId(Guid rawPartId);

        /// <summary>
        /// Updates the overhead settings in the part retrieved from the db and commits the changes
        /// </summary>
        /// <param name="partId">Id of the updated part</param>
        /// <param name="overheadSettings">The updated overhead settings</param>
        void UpdateOverheadSettings(Guid partId, OverheadSetting overheadSettings);

        /// <summary>
        /// Refreshes the part and its process by overriding the changes with data from the db.
        /// </summary>
        /// <param name="part">The part.</param>
        void RefreshWithProcess(Part part);

        /// <summary>
        /// Refreshes the part including, its children.
        /// </summary>
        /// <param name="part">The part.</param>
        void RefreshWithChildren(Part part);

        /// <summary>
        /// Deletes the specified part from the repository using a stored procedure. All sub-entities owned by the part are also deleted.
        /// The actual deletion from the database is performed during this call (when the stored procedure is called), not when SaveChanges() is called.
        /// </summary>
        /// <param name="part">The part to delete.</param>
        void DeleteByStoredProcedure(Part part);
    }
}