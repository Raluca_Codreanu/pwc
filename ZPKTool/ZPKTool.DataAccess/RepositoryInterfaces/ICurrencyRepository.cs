﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines operations available for Currency.
    /// </summary>
    public interface ICurrencyRepository : IRepository<Currency>
    {
        /// <summary>
        /// Gets the currency with the specified id.
        /// </summary>
        /// <param name="id">The currency's id.</param>
        /// <returns>The currency or null if it was not found.</returns>
        Currency GetById(Guid id);

        /// <summary>
        /// Gets the currency with the specified ISO code.
        /// </summary>
        /// <param name="isoCode">The currency's ISO code.</param>
        /// <returns>The currency or null if it was not found.</returns>
        Currency GetByIsoCode(string isoCode);

        /// <summary>
        /// Gets the base currency of the project with the specified Id.
        /// </summary>
        /// <param name="projectId">The project Id.</param>
        /// <returns>
        /// The base currency or a dummy default currency, that represent Euro, if the project has not base currency set.
        /// </returns>
        Currency GetProjectBaseCurrency(Guid projectId);

        /// <summary>
        /// Gets the base, non released, currencies.
        /// </summary>
        /// <returns>
        /// A collection of currencies.
        /// </returns>
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "The method can not be converted to a property because it accesses a database.")]
        Collection<Currency> GetBaseCurrencies();

        /// <summary>
        /// Gets the currencies of the project with the specified Id.
        /// </summary>
        /// <param name="projectId">The project Id.</param>
        /// <returns>
        /// A collection of currencies.
        /// </returns>
        Collection<Currency> GetProjectCurrencies(Guid projectId);

        /// <summary>
        /// Gets the currencies with the specified Id.
        /// </summary>
        /// <param name="currenciesId">The list of currencies Id.</param>
        /// <returns>
        /// A collection of currencies.
        /// </returns>
        Collection<Currency> GetCurrencies(IEnumerable<Guid> currenciesId);

        /// <summary>
        /// Deletes the entity currencies.
        /// </summary>
        /// <param name="entity">The entity from which currency objects are deleted</param>
        void DeleteCurrencies(object entity);

        /// <summary>
        /// Check if a currency is assigned to any country.
        /// </summary>
        /// <param name="currencyId">The currency ID</param>
        /// <returns>True if is assignee to a country, False if not</returns>
        bool CheckCurrencyHasCountry(Guid currencyId);
    }
}
