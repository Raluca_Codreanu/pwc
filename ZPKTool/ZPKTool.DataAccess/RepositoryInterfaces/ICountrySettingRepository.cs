﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines operations available for CountrySetting.
    /// </summary>
    public interface ICountrySettingRepository : IRepository<CountrySetting>
    {
        /// <summary>
        /// Gets the country settings with the default last change timestamp.
        /// </summary>
        /// <returns>A collection of country settings.</returns>
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "The method can not be converted to a property because it accesses a database.")]
        ICollection<CountrySetting> GetCountrySettingsWithDefaultTimestamp();
    }
}