﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines operations available for MeasurementUnit.
    /// </summary>
    public interface IMeasurementUnitRepository : IRepository<MeasurementUnit>
    {
        /// <summary>
        /// Gets all measurement units.
        /// </summary>
        /// <returns>
        /// A collection of MeasurementUnits.
        /// </returns>
        Collection<MeasurementUnit> GetAll();

        /// <summary>
        /// Gets all measurement units of a given type.
        /// </summary>
        /// <param name="unitsType">Type of the units to get.</param>
        /// <returns>
        /// A collection of MeasurementUnits with the type <paramref name="unitsType"/>
        /// </returns>
        Collection<MeasurementUnit> GetAll(MeasurementUnitType unitsType);

        /// <summary>
        /// Gets all measurement units in a given scale.
        /// </summary>
        /// <param name="unitsScale">The scale to whom units to get belongs.</param>
        /// <returns>
        /// A collection of MeasurementUnits with the type <paramref name="unitsType"/>
        /// </returns>
        Collection<MeasurementUnit> GetAll(MeasurementUnitScale unitsScale);

        /// <summary>
        /// Gets the base measurement units of all scales (the units which have scale factor equal to 1).
        /// </summary>
        /// <returns>
        /// A collection of MeasurementUnits.
        /// </returns>
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "The method performs database access.")]
        Collection<MeasurementUnit> GetBaseMeasurementUnits();

        /// <summary>
        /// Gets the base measurement unit of a scale.
        /// </summary>
        /// <param name="scale">The scale.</param>
        /// <returns>
        /// A MeasurementUnit instance.
        /// </returns>
        MeasurementUnit GetBaseMeasurementUnit(MeasurementUnitScale scale);

        /// <summary>
        /// Gets a measurement unit item by name
        /// </summary>
        /// <param name="unitName">Name of the unit</param>
        /// <returns>The unit or null if is not found.</returns>
        MeasurementUnit GetByName(string unitName);
    }
}
