﻿using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines operations that are common to all repositories.
    /// </summary>
    /// <typeparam name="T">The type of entities in repository.</typeparam>
    public interface IRepository<T> where T : class
    {
        /// <summary>
        /// Checks if an entity with the specified id exists in the database.
        /// </summary>
        /// <param name="id">The id of the entity.</param>
        /// <returns>True if a entity with the specified id exists; otherwise, false.</returns>
        bool CheckIfExists(Guid id);

        /// <summary>
        /// Adds the specified entity to the repository.
        /// </summary>
        /// <param name="entity">The entity to add.</param>
        void Add(T entity);

        /// <summary>
        /// Saves the entity into the database. If it does not exist in the database then it is added.        
        /// </summary>
        /// <param name="entity">The entity to save.</param>        
        void Save(T entity);

        /// <summary>
        /// Marks the specified entity, including any children entities owned by the entity, for deletion.
        /// </summary>
        /// <param name="entity">The entity to remove.</param>
        void RemoveAll(T entity);

        /// <summary>
        /// Detaches the specified entity from the context.
        /// </summary>
        /// <param name="entity">The entity to detach.</param>
        void Detach(T entity);

        /// <summary>
        /// Detaches the specified entity, including any children entities owned by the entity, from the context.
        /// </summary>
        /// <param name="entity">The entity to detach.</param>
        void DetachAll(T entity);

        /// <summary>
        /// Refreshes an entity with the values from database. The entities in its graph are not refreshed.
        /// </summary>
        /// <param name="entity">The entity to refresh.</param>
        void Refresh(T entity);

        /// <summary>
        /// Refreshes multiple entities with the values from database. The entities in their graph are not refreshed.
        /// </summary>
        /// <param name="entities">The entities to refresh.</param>
        void Refresh(IEnumerable<T> entities);

        /// <summary>
        /// Gets the entity having the specified primary key.
        /// <para />
        /// If an entity with the specified primary key exists in the context, then it is returned immediately without making a request to the database.
        /// Otherwise, a query is performed on the database to get the entity, and if found, the entity is attached to the context and returned.
        /// If no entity is found in the context or the database, then null is returned.
        /// </summary>
        /// <param name="key">The entity's key.</param>
        /// <returns>The entity having the specified key, or null if it was not found</returns>
        T GetByKey(object key);

        /// <summary>
        /// Gets the entities that are currently loaded into the context and have the specified state.        
        /// </summary>
        /// <param name="state">The state of the entities that are returned.</param>
        /// <returns>A list of entities having the specified state.</returns>
        IEnumerable<T> GetLocalEntities(EntityState state);

        /// <summary>
        /// Determines whether the specified entity is marked as Added in the repository's data context. Entities in the Added state will be inserted
        /// into the data store when the repository changes are saved.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// true if the specified entity is in the Added state; otherwise, false.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">The entity was null.</exception>        
        bool IsAdded(T entity);

        /// <summary>
        /// Determines whether the specified entity is marked as Deleted in the repository's data context. Entities in the Deleted state will be deleted
        /// from the data store when the repository changes are saved.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// true if the specified entity is in the Deleted state; otherwise, false.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">The entity was null.</exception>        
        bool IsDeleted(T entity);
    }
}
