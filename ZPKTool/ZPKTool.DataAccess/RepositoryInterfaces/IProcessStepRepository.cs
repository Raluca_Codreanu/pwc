﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using ZPKTool.Common;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines operations available for ProcessStep.
    /// </summary>
    public interface IProcessStepRepository : IRepository<ProcessStep>
    {
        /// <summary>
        /// Gets the id of the parent project of a specified processStep.
        /// </summary>
        /// <param name="processStep">The processStep.</param>
        /// <returns>The id or Guid.Empty if an error occurs.</returns>
        Guid GetParentProjectId(ProcessStep processStep);

        /// <summary>
        /// Gets the id of the parent assembly of a specified processStep.
        /// </summary>
        /// <param name="processStep">The process step.</param>
        /// <returns>The id or Guid.Empty if an error occurs.</returns>
        Guid GetParentAssemblyId(ProcessStep processStep);
                
        /// <summary>
        /// Gets the process step with the specified id fully loaded.
        /// </summary>
        /// <param name="stepId">The step id.</param>
        /// <returns>The step or null if not found.</returns>
        ProcessStep GetProcessStepFull(Guid stepId);

        /// <summary>
        /// Gets the ProcessStep to which a specified entity belongs using the NoTracking option
        /// </summary>
        /// <param name="entity">The entity to get it's parent.</param>
        /// <returns>The parent ProcessStep of the given entity</returns>
        ProcessStep GetParentProcessStep(object entity);

        /// <summary>
        /// Retrieves the process steps based on the ids list.
        /// </summary>
        /// <param name="ids">The ids of the process steps to get.</param>
        /// <returns>A collection of process steps.</returns>
        Collection<ProcessStep> GetByIds(IEnumerable<Guid> ids);

        /// <summary>
        /// Gets the id of the process step's parent process.
        /// </summary>
        /// <param name="processStep">The process step.</param>
        /// <returns>The id or Guid.Empty if no parent process is found.</returns>
        Guid GetParentProcessId(ProcessStep processStep);

        /// <summary>
        /// Deletes the specified process step from the repository using a stored procedure. All sub-entities owned by the process step are also deleted.
        /// The actual deletion from the database is performed during this call (when the stored procedure is called), not when SaveChanges() is called.
        /// </summary>
        /// <param name="processStep">The process step to delete.</param>
        void DeleteByStoredProcedure(ProcessStep processStep);
    }
}
