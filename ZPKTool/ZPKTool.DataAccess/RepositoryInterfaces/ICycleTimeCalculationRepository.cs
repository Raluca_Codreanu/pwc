﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines operations available for CycleTimeCalculation.
    /// </summary>
    public interface ICycleTimeCalculationRepository : IRepository<CycleTimeCalculation>
    {
        /// <summary>
        /// Deletes the specified cycle time calculation from the repository using a stored procedure.
        /// The actual deletion from the database is performed during this call (when the stored procedure is called), not when SaveChanges() is called.
        /// </summary>
        /// <param name="cycleTimeCalculation">The cycle time calculation to delete.</param>
        void DeleteByStoredProcedure(CycleTimeCalculation cycleTimeCalculation);
    }
}
