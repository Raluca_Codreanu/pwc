﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines operations available for Consumable.
    /// </summary>
    public interface IConsumableRepository : IRepository<Consumable>
    {
        /// <summary>
        /// Retrieves the consumable based on the id.
        /// </summary>
        /// <param name="id">The consumable id.</param>
        /// <param name="noTracking">Option specifying if the MergeOption should be NoTracking.</param>
        /// <returns>The consumable.</returns>
        Consumable GetById(Guid id, bool noTracking = false);

        /// <summary>
        /// Retrieves the consumables based on the ids list.
        /// </summary>
        /// <param name="ids">The ids of the consumables to get.</param>
        /// <param name="noTracking">Option specifying if the MergeOption should be NoTracking.</param>
        /// <returns>A collection of consumables.</returns>
        Collection<Consumable> GetByIds(IEnumerable<Guid> ids, bool noTracking = false);

        /// <summary>
        /// Retrieves all the master data consumables
        /// </summary>
        /// <returns>A collection containing all the master data consumables</returns>
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "The method can not be converted to a property because it accesses a database.")]
        Collection<Consumable> GetAllMasterData();

        /// <summary>
        /// Gets the id of the parent project of a specified consumable.
        /// </summary>
        /// <param name="consumable">The consumable.</param>
        /// <returns>The id or Guid.Empty if an error occurs.</returns>
        Guid GetParentProjectId(Consumable consumable);

        /// <summary>
        /// Gets the id of the parent assembly of a specified consumable.
        /// </summary>
        /// <param name="consumable">The consumable.</param>
        /// <returns>The id or Guid.Empty if an error occurs.</returns>
        Guid GetParentAssemblyId(Consumable consumable);

        /// <summary>
        /// Gets the id of the consumable's parent process step.
        /// </summary>
        /// <param name="consumable">The consumable.</param>
        /// <returns>The id or Guid.Empty if no parent process step is found.</returns>
        Guid GetParentProcessStepId(Consumable consumable);

        /// <summary>
        /// Deletes the specified consumable from the repository using a stored procedure.
        /// The actual deletion from the database is performed during this call (when the stored procedure is called), not when SaveChanges() is called.
        /// </summary>
        /// <param name="consumable">The consumable to delete.</param>
        void DeleteByStoredProcedure(Consumable consumable);
    }
}
