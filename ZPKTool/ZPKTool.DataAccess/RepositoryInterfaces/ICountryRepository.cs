﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines operations available for Country.
    /// </summary>
    public interface ICountryRepository : IRepository<Country>
    {
        /// <summary>
        /// Gets all countries.
        /// </summary>
        /// <returns>A collection containing all countries.</returns>
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "The method can not be converted to a property because it accesses a database.")]
        Collection<Country> GetAll();

        /// <summary>
        /// Gets the country with the specified id.
        /// </summary>
        /// <param name="id">The country's id.</param>
        /// <returns>The country or null if it was not found.</returns>
        Country GetById(Guid id);

        /// <summary>
        /// Gets the country with the specified name.
        /// </summary>
        /// <param name="name">The country's name.</param>
        /// <returns>The country or null if it was not found.</returns>
        Country GetByName(string name);

        /// <summary>
        /// Gets the country with the specified name.
        /// </summary>
        /// <param name="name">The country's name.</param>
        /// <param name="noTracking">if set to true the returned objects are not tracked by the data context.</param>
        /// <returns>
        /// The country or null if it was not found.
        /// </returns>
        Country GetByName(string name, bool noTracking);

        /// <summary>
        /// Checks if a country with the specified name exists.
        /// </summary>
        /// <param name="countryName">Name of the country to check if exists.</param>
        /// <returns>True if a country with the specified name exists; otherwise, false.</returns>
        bool CheckIfExists(string countryName);

        /// <summary>
        /// Gets the country with a specific id, or with a specific name, or a name like "country-name Country" (ex: "China Country")
        /// This method is used when its a possibility that the country name may have been modified in master data
        /// </summary>
        /// <param name="countryId">The country id</param>
        /// <param name="countryName">The country name</param>
        /// <returns>The country or null if it was not found.</returns>
        Country ResolveCountry(Guid countryId, string countryName);
    }
}
