﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines the operations on MachinesClassification entities.
    /// </summary>
    public interface IProcessStepsClassificationRepository : IRepository<ProcessStepsClassification>
    {
        /// <summary>
        /// Load the entire process steps classification tree from the database, flattened. The hierarchy can be reconstructed using the Parent property
        /// of the ProcessStepsClassification instances.
        /// </summary>
        /// <returns>
        /// A collection containing the flattened ProcessStepsClassification hierarchy. 
        /// </returns>
        Collection<ProcessStepsClassification> LoadClassificationTree();

        /// <summary>
        /// Gets a process step classification item by name
        /// </summary>
        /// <param name="classificationName">Name of the classification</param>
        /// <returns>The classification, or null if not found.</returns>
        ProcessStepsClassification GetByName(string classificationName);
    }
}
