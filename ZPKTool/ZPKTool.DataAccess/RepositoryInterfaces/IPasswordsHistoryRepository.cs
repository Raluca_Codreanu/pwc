﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines operations available for PasswordsHistory.
    /// </summary>
    public interface IPasswordsHistoryRepository : IRepository<PasswordsHistory>
    {
        /// <summary>
        /// Gets the passwords history and the last saved setting coresponding to a specified user.
        /// </summary>
        /// <param name="passwordID">The specified user id.</param>
        /// <returns>A list of passwords.</returns>
        IEnumerable<PasswordsHistory> GetPasswordsHistory(Guid passwordID);
    }
}
