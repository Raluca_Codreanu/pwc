﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines operations available for Supplier
    /// </summary>
    public interface ISupplierRepository : IRepository<Customer>
    {
        /// <summary>
        /// Retrieves the supplier with a given guid.
        /// </summary>
        /// <param name="supplierGuid">The guid of the supplier to retrieve</param>
        /// <param name="noTracking">Specifies if the supplier will be retrieved using the NoTracking option</param>
        /// <returns>The supplier.</returns>
        Customer FindById(Guid supplierGuid, bool noTracking = false);

        /// <summary>
        /// Retrieves the master data suppliers.
        /// </summary>
        /// <returns>The collection of master data suppliers</returns>
        Collection<Customer> FindAllMasterData();

        /// <summary>
        /// Deletes the specified supplier from the repository using a stored procedure.
        /// The actual deletion from the database is performed during this call (when the stored procedure is called), not when SaveChanges() is called.
        /// </summary>
        /// <param name="supplier">The supplier to delete.</param>
        void DeleteByStoredProcedure(Customer supplier);
    }
}
