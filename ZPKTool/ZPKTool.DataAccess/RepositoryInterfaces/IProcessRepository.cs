﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines operations available for Process.
    /// </summary>
    public interface IProcessRepository : IRepository<Process>
    {
        /// <summary>
        /// Gets the process with steps.
        /// </summary>
        /// <param name="processId">The process Id.</param>
        /// <returns>The process or null if not found.</returns>
        Process GetProcessWithSteps(Guid processId);

        /// <summary>
        /// Gets the id of the parent project of a specified process.
        /// </summary>
        /// <param name="process">The process.</param>
        /// <returns>The id or Guid.Empty if an error occurs.</returns>
        Guid GetParentProjectId(Process process);

        /// <summary>
        /// Gets the id of the parent assembly of a specified process.
        /// </summary>
        /// <param name="process">The process.</param>
        /// <returns>The id or Guid.Empty if an error occurs.</returns>
        Guid GetParentAssemblyId(Process process);

        /// <summary>
        /// Deletes the specified process from the repository using a stored procedure. All sub-entities owned by the process are also deleted.
        /// The actual deletion from the database is performed during this call (when the stored procedure is called), not when SaveChanges() is called.
        /// </summary>
        /// <param name="process">The process to delete.</param>
        void DeleteByStoredProcedure(Process process);
    }
}
