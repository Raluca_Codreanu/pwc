﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines operations available for Die.
    /// </summary>
    public interface IDieRepository : IRepository<Die>
    {
        /// <summary>
        /// Retrieves the die based on the id.
        /// </summary>
        /// <param name="id">The die id.</param>
        /// <param name="noTracking">Option specifying if the MergeOption should be NoTracking.</param>
        /// <returns>The die.</returns>
        Die GetById(Guid id, bool noTracking = false);

        /// <summary>
        /// Retrieves the dies based on the ids list.
        /// </summary>
        /// <param name="ids">The ids of the dies to get.</param>
        /// <param name="noTracking">Option specifying if the MergeOption should be NoTracking.</param>
        /// <returns>A collection of dies.</returns>
        Collection<Die> GetByIds(IEnumerable<Guid> ids, bool noTracking = false);

        /// <summary>
        /// Retrieves all the master data Dies
        /// </summary>
        /// <returns>A collection containing all the master data Dies</returns>
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "The method performs database access.")]
        Collection<Die> GetAllMasterData();

        /// <summary>
        /// Gets the id of the parent project of a specified die.
        /// </summary>
        /// <param name="die">The die.</param>
        /// <returns>The id or Guid.Empty if an error occurs.</returns>
        Guid GetParentProjectId(Die die);

        /// <summary>
        /// Gets the id of the parent assembly of a specified die.
        /// </summary>
        /// <param name="die">The die.</param>
        /// <returns>The id or Guid.Empty if an error occurs.</returns>
        Guid GetParentAssemblyId(Die die);

        /// <summary>
        /// Gets the id of the die's parent process step.
        /// </summary>
        /// <param name="die">The die.</param>
        /// <returns>The id or Guid.Empty if no parent process step is found.</returns>
        Guid GetParentProcessStepId(Die die);

        /// <summary>
        /// Deletes the specified die from the repository using a stored procedure.
        /// The actual deletion from the database is performed during this call (when the stored procedure is called), not when SaveChanges() is called.
        /// </summary>
        /// <param name="die">The die to delete.</param>
        void DeleteByStoredProcedure(Die die);
    }
}
