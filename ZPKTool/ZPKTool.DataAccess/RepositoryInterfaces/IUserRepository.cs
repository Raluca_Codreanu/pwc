﻿using System;
using System.Collections.ObjectModel;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines operations available for the User object.
    /// </summary>
    public interface IUserRepository : IRepository<User>
    {
        /// <summary>
        /// Gets all users from the database.
        /// </summary>
        /// <param name="includeDisabled">if set to true include disabled users in the result.</param>
        /// <param name="noTracking">if set to true change tracking is disabled for the objects in the result.</param>
        /// <returns>
        /// A collection with all the users.
        /// </returns>
        Collection<User> GetAll(bool includeDisabled, bool noTracking = false);

        /// <summary>
        /// Gets the users from the database, that have at least one project for which the user passed as parameter is responsible or Project Leader.
        /// </summary>
        /// <param name="userGuid">User Guid for seeking projects to other users.</param>
        /// <returns>
        /// A collection of all users found that meet the search requirements.
        /// </returns>
        Collection<User> GetUsersWithProjectsVisibleToOtherUser(Guid userGuid);

        /// <summary>
        /// Gets the user with a given id.        
        /// </summary>
        /// <param name="id">The id of the searched user.</param>
        /// <param name="includeDisabled">if set to true include disabled users in the result.</param>
        /// <returns>A User instance or null if not found.</returns>
        User GetById(Guid id, bool includeDisabled);

        /// <summary>
        /// Gets a user by its username.
        /// </summary>
        /// <param name="username">The username to search by.</param>
        /// <param name="includeDisabled">if set to true, include disabled users in the result.</param>
        /// <param name="noTracking">if set to true the returned object is not tracked for changes.</param>
        /// <returns>
        /// An User instance having the requested user name.
        /// </returns>
        User GetByUsername(string username, bool includeDisabled, bool noTracking = false);
    }
}
