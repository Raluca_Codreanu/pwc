﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines the operations on MaterialsClassification objects.
    /// </summary>
    public interface IMaterialsClassificationRepository : IRepository<MaterialsClassification>
    {
        /// <summary>
        /// Load the entire materials classification tree from the database, flattened. The hierarchy can be reconstructed using the Parent property
        /// of the instances.
        /// </summary>
        /// <returns>
        /// A collection containing the flattened MaterialsClassification hierarchy. 
        /// </returns>
        Collection<MaterialsClassification> LoadClassificationTree();

        /// <summary>
        /// Gets a material classification item by name
        /// </summary>
        /// <param name="classificationName">Name of the classification</param>
        /// <returns>The classification or null if is not found.</returns>
        MaterialsClassification GetByName(string classificationName);
    }
}
