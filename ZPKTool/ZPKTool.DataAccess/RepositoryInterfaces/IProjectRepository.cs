﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines operations available for Project.
    /// </summary>
    public interface IProjectRepository : IRepository<Project>
    {
        /// <summary>
        /// Gets a specified project with the purpose of being edited.
        /// The following references are loaded: Customer, OverheadSettings, ProjectLeader, ResponsibleCalculator, Owner.
        /// </summary>
        /// <param name="projectId">The id of the project to retrieve</param>
        /// <returns>The project or null if not found.</returns>
        Project GetProjectForEdit(Guid projectId);

        /// <summary>
        /// Gets a project to view its details in trash bin.
        /// </summary>
        /// <param name="projectId">The project id.</param>
        /// <returns>The project.</returns>
        Project GetProjectForView(Guid projectId);

        /// <summary>
        /// Gets a specified project to be displayed projects tree. The following references are loaded: Customer, OverheadSettings, ProjectFolder, Owner.
        /// </summary>
        /// <param name="projectId">The project id.</param>
        /// <returns>A project, or null if it is not found.</returns>
        Project GetProjectForProjectsTree(Guid projectId);

        /// <summary>
        /// Gets a specified project with all sub-objects and collections fully loaded
        /// </summary>
        /// <param name="projectId">The project id.</param>
        /// <returns>The project or null if not found.</returns>
        Project GetProjectFull(Guid projectId);

        /// <summary>
        /// Gets the maximum number of projects to display on the welcome view for a specified user. Only the 1st image of each project is loaded.
        /// </summary>
        /// <param name="userGuid">The guid of the user whose projects to get.</param>
        /// <param name="parentFolderId">The guid for parent project folder</param>
        /// <param name="maxNumberOfProjects">The maximum number of projects to display.</param>
        /// <returns>A collection containing the user's projects.</returns>
        Collection<Project> GetProjectsForWelcomeView(Guid userGuid, Guid parentFolderId, int maxNumberOfProjects);

        /// <summary>
        /// Gets projects of a specified user to be displayed projects tree. The following references are loaded: Customer, OverheadSettings.
        /// </summary>
        /// <param name="ownerId">The owner id.</param>
        /// <param name="folderId">The id of the folder whose projects to return.
        /// Setting it to empty will return the projects without a parent folder (root folders).</param>
        /// <returns>A collection of projects.</returns>
        Collection<Project> GetProjectsForProjectsTree(Guid ownerId, Guid folderId);

        /// <summary>
        /// Gets the projects of a specified user, that are bookmarked, to be displayed in bookmarks tree.
        /// </summary>
        /// <param name="ownerId">The owner id.</param>
        /// <returns>A collection of projects.</returns>
        Collection<Project> GetBookmarkedProjects(Guid ownerId);

        /// <summary>
        /// Gets the projects of another user.
        /// </summary>
        /// <param name="userId">The id of the user whose projects to get.</param>
        /// <param name="leaderOrCalculatorId">
        /// The id of the project leader or responsible calculator that the projects must have. If this is set to Guid.Empty it will return all projects of the user,
        /// otherwise it will return all projects of the user that have the project leader or responsible calculator with the id <paramref name="leaderOrCalculatorId"/>.
        /// </param>
        /// <returns>A collection of projects.</returns>
        Collection<Project> GetOtherUserProjects(Guid userId, Guid leaderOrCalculatorId);

        /// <summary>
        /// Gets the parent project from the db (if it is not loaded already).
        /// </summary>
        /// <param name="entity">The entity to get its parent.</param>
        /// <param name="noTracking">if set to true disable change tracking for the retuned object(s).</param>
        /// <returns>The parent entity of the given entity</returns>
        Project GetParentProject(object entity, bool noTracking = false);

        /// <summary>
        /// Gets a specified project with first level of sub-objects and collections.
        /// </summary>
        /// <param name="projectId">The project id.</param>
        /// <param name="noTracking">if set to true change tracking is disabled for the returned value.</param>
        /// <returns>
        /// The project with first level of assemblies and parts.
        /// </returns>
        Project GetProjectIncludingTopLevelChildren(Guid projectId, bool noTracking = false);

        /// <summary>
        /// Gets the hierarchy of folders to which a project belongs.        
        /// </summary>
        /// <param name="projectId">The project id.</param>
        /// <returns>
        /// A collection containing the IDs of the folders in the hierarchy. The 1st item in the collection is the direct parent of the project
        /// while the last one is the root of the hierarchy.
        /// </returns>
        Collection<Guid> GetProjectParentFolderHierarchy(Guid projectId);

        /// <summary>
        /// Gets the offline projects.
        /// </summary>
        /// <param name="ownerGuid">The owner GUID.</param>
        /// <returns>List of Guids representing the Guid of the offline projects</returns>
        ICollection<Guid> GetOfflineProjects(Guid ownerGuid);

        /// <summary>
        /// Gets the released projects.
        /// </summary>
        /// <returns>
        /// List of released project.
        /// </returns>
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "The method performs database access.")]
        Collection<Project> GetReleasedProjects();

        /// <summary>
        /// Gets all projects of a folder.
        /// </summary>
        /// <param name="folderId">The folder id.</param>
        /// <returns>
        /// Collection of projects.
        /// </returns>
        Collection<Project> GetProjectsOfFolder(Guid folderId);

        /// <summary>
        /// Gets the released projects for projects tree.
        /// </summary>
        /// <param name="folderId">The parent folder id.</param>
        /// <returns>List of released project.</returns>
        Collection<Project> GetReleasedProjectsForProjectsTree(Guid folderId);

        /// <summary>
        /// Retrieves the projects based on the ids list.
        /// </summary>
        /// <param name="ids">The ids of the projects to get.</param>
        /// <returns>A collection of projects.</returns>
        Collection<Project> GetByIds(IEnumerable<Guid> ids);

        /// <summary>
        /// Gets the id of the project's parent folder.
        /// </summary>
        /// <param name="project">The project.</param>
        /// <returns>The guid or Guid.Empty if no parent folder is found.</returns>
        Guid GetParentFolderId(Project project);

        /// <summary>
        /// Deletes the specified project from the repository using a stored procedure. All sub-entities owned by the project are also deleted.
        /// The actual deletion from the database is performed during this call (when the stored procedure is called), not when SaveChanges() is called.
        /// </summary>
        /// <param name="project">The project to delete.</param>
        void DeleteByStoredProcedure(Project project);

        /// <summary>
        /// Gets the ids for all projects of a specified user.
        /// </summary>
        /// <param name="userID">The specified user id.</param>
        /// <returns>A list containing the ids of all projects for a specified user.</returns>
        IEnumerable<Guid> GetProjectsId(Guid userID);
    }
}
