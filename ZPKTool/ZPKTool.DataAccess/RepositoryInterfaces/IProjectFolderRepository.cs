﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines operations available for ProjectFolder.
    /// </summary>
    public interface IProjectFolderRepository : IRepository<ProjectFolder>
    {
        /// <summary>
        /// Gets the root folders of a specified user. A root folder is a folder that does not have a parent folder.
        /// </summary>
        /// <param name="ownerId">The owner's id.</param>
        /// <returns>A collection of project folders.</returns>
        Collection<ProjectFolder> GetRootFolders(Guid ownerId);

        /// <summary>
        /// Gets the maximum number of root folders to display on the welcome view for a specified user.
        /// A root folder is a folder that does not have a parent folder.
        /// </summary>
        /// <param name="ownerId">The owner's id.</param>
        /// <param name="maxNumberOfFolders">The maximum number of root folders to display.</param>
        /// <returns>A collection of project folders.</returns>
        Collection<ProjectFolder> GetRootFolders(Guid ownerId, int maxNumberOfFolders);

        /// <summary>
        /// Gets the sub-folders of a specified folder.
        /// </summary>
        /// <param name="parentFolderId">The id of the folder whose sub-folders are requested.</param>
        /// <param name="ownerId">The id of the sub-folders' owner.</param>
        /// <returns>A collection of sub-folders.</returns>
        Collection<ProjectFolder> GetSubFolders(Guid parentFolderId, Guid ownerId);

        /// <summary>
        /// Gets a specified project folder with all its sub-objects and collections fully loaded
        /// </summary>
        /// <param name="folderId">The folder id.</param>
        /// <returns>
        /// The folder or null if not found.
        /// </returns>
        ProjectFolder GetProjectFolderFull(Guid folderId);

        /// <summary>
        /// Gets the folder with the specified id, including its parent folder. The loaded references are ParentProjectFolder and Owner.
        /// </summary>
        /// <param name="folderId">The folder id.</param>
        /// <returns>
        /// A project folder, or null if it is not found.
        /// </returns>
        ProjectFolder GetFolderWithParent(Guid folderId);

        /// <summary>
        /// Gets the offline project folders ids.
        /// </summary>
        /// <param name="ownerGuid">The owner GUID.</param>
        /// <returns>List containing the id of all offline project folders</returns>
        ICollection<Guid> GetOfflineProjectFolders(Guid ownerGuid);

        /// <summary>
        /// Gets the ids for all project folders of a specified user.
        /// </summary>
        /// <param name="userID">The specified user id.</param>
        /// <returns>A collection containing the ids of all project folders for a specified user.</returns>
        Collection<Guid> GetProjectFoldersId(Guid userID);

        /// <summary>
        /// Gets the released project folders.
        /// </summary>
        /// <returns>
        /// List of released project folders
        /// </returns>
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "The method performs database access.")]
        Collection<ProjectFolder> GetReleasedProjectFolders();

        /// <summary>
        /// Gets the root released folders. A root folder is a folder that does not have a parent folder.
        /// </summary>
        /// <returns>A collection of project folders.</returns>
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "The method performs database access.")]
        Collection<ProjectFolder> GetReleasedRootFolders();

        /// <summary>
        /// Gets the released sub folders.
        /// </summary>
        /// <param name="parentFolderId">The parent folder id.</param>
        /// <returns>A collection containing the released sub folders.</returns>
        Collection<ProjectFolder> GetReleasedSubFolders(Guid parentFolderId);

        /// <summary>
        /// Gets the owner of the specified folder.
        /// </summary>
        /// <param name="folder">The folder.</param>
        /// <returns>
        /// The folder's owner, or null if the folder does not have an owner.
        /// </returns>
        User GetOwner(ProjectFolder folder);

        /// <summary>
        /// Checks if a specified project folder is the parent of another folder, directly or indirectly (parent of parent ...).
        /// </summary>
        /// <param name="parent">The folder to check if it is the parent.</param>
        /// <param name="folder">The folder that is the child.</param>
        /// <returns>true if <paramref name="parent"/> is the parent folder of <paramref name="folder"/>; otherwise false.</returns>
        bool CheckIfParentOf(ProjectFolder parent, ProjectFolder folder);

        /// <summary>
        /// Deletes the specified folder from the repository using a stored procedure. All sub-entities owned by the folder are also deleted.
        /// The actual deletion from the database is performed during this call (when the stored procedure is called), not when SaveChanges() is called.
        /// </summary>
        /// <param name="folder">The folder to delete.</param>
        void DeleteByStoredProcedure(ProjectFolder folder);
    }
}
