﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines operations on RawMaterial.
    /// </summary>
    public interface IRawMaterialRepository : IRepository<RawMaterial>
    {
        /// <summary>
        /// Retrieves the raw material based on the id.
        /// </summary>
        /// <param name="id">The raw material id.</param>
        /// <param name="noTracking">Option specifying if the MergeOption should be NoTracking.</param>
        /// <returns>The raw material.</returns>
        RawMaterial GetById(Guid id, bool noTracking = false);

        /// <summary>
        /// Retrieves the raw material based on the ids list.
        /// </summary>
        /// <param name="ids">The ids of the raw materials to get.</param>
        /// <param name="noTracking">Option specifying if the MergeOption should be NoTracking.</param>
        /// <returns>A collection of raw materials.</returns>
        Collection<RawMaterial> GetByIds(IEnumerable<Guid> ids, bool noTracking = false);

        /// <summary>
        /// Retrieves the master data raw materials.
        /// </summary>
        /// <returns>The collection of master data raw materials</returns>
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "The method performs database access.")]
        Collection<RawMaterial> GetAllMasterData();

        /// <summary>
        /// Gets the id of the project to which a specified material belongs to.
        /// </summary>
        /// <param name="rawMaterial">The raw material.</param>
        /// <returns>The id of the project or Guid.Empty if the material does not have a parent project.</returns>
        Guid GetParentProjectId(RawMaterial rawMaterial);

        /// <summary>
        /// Gets the id of the assembly to which a specified material belongs to.
        /// </summary>
        /// <param name="rawMaterial">The raw material.</param>
        /// <returns>The id of the assembly or Guid.Empty if the material does not have a parent assembly.</returns>
        Guid GetParentAssemblyId(RawMaterial rawMaterial);               
                
        /// <summary>
        /// Gets the id of the raw material's parent part.
        /// </summary>
        /// <param name="rawMaterial">The raw material.</param>
        /// <returns>The id or Guid.Empty if no parent part is found.</returns>
        Guid GetParentPartId(RawMaterial rawMaterial);

        /// <summary>
        /// Deletes the specified material from the repository using a stored procedure. All sub-entities owned by the material are also deleted.
        /// The actual deletion from the database is performed during this call (when the stored procedure is called), not when SaveChanges() is called.
        /// </summary>
        /// <param name="material">The material to delete.</param>
        void DeleteByStoredProcedure(RawMaterial material);
    }
}
