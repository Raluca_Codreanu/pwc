﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines operations available for BasicSetting.
    /// </summary>
    public interface IBasicSettingsRepository : IRepository<BasicSetting>
    {
        /// <summary>
        /// Retrieves the basic settings of the application; there is only one basic settings record.
        /// </summary>
        /// <param name="noTracking">if set to true change tracking is disabled for the returned object(s).</param>
        /// <returns>
        /// The basic settings
        /// </returns>
        BasicSetting GetBasicSettings(bool noTracking = false);
    }
}
