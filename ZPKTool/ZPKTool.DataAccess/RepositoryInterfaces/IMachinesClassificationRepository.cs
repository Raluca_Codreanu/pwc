﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines the operations on MachinesClassification entities.
    /// </summary>
    public interface IMachinesClassificationRepository : IRepository<MachinesClassification>
    {
        /// <summary>
        /// Load the entire machines classification tree from the database, flattened. The hierarchy can be reconstructed using the Parent property
        /// of the MachinesClassification instances.
        /// </summary>
        /// <returns>
        /// A collection containing the flattened MachinesClassification hierarchy. 
        /// </returns>
        Collection<MachinesClassification> LoadClassificationTree();

        /// <summary>
        /// Gets a machine classification item by name
        /// </summary>
        /// <param name="classificationName">Name of the classification</param>
        /// <returns>The classification.</returns>
        MachinesClassification GetByName(string classificationName);
    }
}
