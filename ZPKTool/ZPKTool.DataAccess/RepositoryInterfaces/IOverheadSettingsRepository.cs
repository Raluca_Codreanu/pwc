﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines operations available for OverheadSettings.
    /// </summary>
    public interface IOverheadSettingsRepository : IRepository<OverheadSetting>
    {
        /// <summary>
        /// Retrieves the master data overhead settings.
        /// </summary>
        /// <returns>
        /// The master data overhead settings.
        /// </returns>
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "The method performs database access.")]
        OverheadSetting GetMasterData();

        /// <summary>
        /// Deletes the specified overhead settings from the repository using a stored procedure. All sub-entities owned by <paramref name="overheadSettings"/> are also deleted.
        /// The actual deletion from the database is performed during this call (when the stored procedure is called), not when SaveChanges() is called.
        /// </summary>
        /// <param name="overheadSettings">The overhead settings to delete.</param>
        void DeleteByStoredProcedure(OverheadSetting overheadSettings);
    }
}
