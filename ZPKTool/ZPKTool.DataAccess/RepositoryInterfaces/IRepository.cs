﻿using System;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// A non-generic version of <see cref="ZPKTool.DataAccess.IRepository&lt;T&gt;"/> that can be used when the type of the entity in the repository is not known.
    /// </summary>
    public interface IRepository
    {
        /// <summary>
        /// Adds the specified entity to the repository (and sets its state to Added). If the entity is already in the repository in another state, its state
        /// will still be set to Added.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <exception cref="System.ArgumentNullException">The entity was null.</exception>
        /// <exception cref="ArgumentException">The type of the entity does not match the entity type for which the repository was created.</exception>
        void Add(object entity);

        /// <summary>
        /// Gets an entity with the given primary key values. If an entity with the given primary key values exists in the context, then it is returned
        /// without making a request to the store. Otherwise, the entity is queried from the store and returned if found (is also attached to the repository's data context).
        /// If no entity is found in the context or the store, then null is returned.
        /// </summary>
        /// <param name="keys">The values of the primary key for the entity to be found.</param>
        /// <returns>The entity with the specified primary key values or null if it was not found.</returns>
        /// <exception cref="InvalidOperationException">Multiple entities exist in the context with the primary key values given, or
        /// the type of entity is not part of the data model for this context, or
        /// the types, order or number of the key values do not match the types, order or number of the key values of the entity type to be found as defined in the data model.
        /// </exception>
        object GetByKey(params object[] keys);

        /// <summary>
        /// Gets the entity instance that corresponds to the specified entity in this repository. If the searched entity exists in the repository
        /// it is returned immediately, otherwise it queries it from the store. If the searched entity is not found in the repository or the store, null is returned.
        /// </summary>
        /// <param name="entity">The entity to get from the repository.</param>
        /// <returns>The found entity or null if it is not found.</returns>
        /// <exception cref="System.ArgumentNullException">The entity was null.</exception>
        /// <exception cref="ArgumentException">The type of the entity does not match the entity type for which the repository was created.</exception>
        object GetByEntity(object entity);

        /// <summary>
        /// Marks the specified entity as Deleted such that it will be deleted from the database when the repository changes are saved.
        /// Note that the entity must exist in the repository's data context in some other state before this method is called.
        /// </summary>
        /// <param name="entity">The entity to mark for deletion.</param>
        /// <exception cref="System.ArgumentNullException">The entity was null.</exception>
        /// <exception cref="ArgumentException">The type of the entity does not match the entity type for which the repository was created.</exception>
        /// <exception cref="InvalidOperationException">The entity cannot be deleted because it was not found in the repository's data context.</exception>
        void Remove(object entity);

        /// <summary>
        /// Determines whether the specified entity is marked as Added in the repository's data context. Entities in the Added state will be inserted
        /// into the data store when the repository changes are saved.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// true if the specified entity is in the Added state; otherwise, false.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">The entity was null.</exception>
        /// <exception cref="ArgumentException">The type of the entity does not match the entity type for which the repository was created.</exception>
        bool IsAdded(object entity);

        /// <summary>
        /// Determines whether the specified entity is marked as Deleted in the repository's data context. Entities in the Deleted state will be deleted
        /// from the data store when the repository changes are saved.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// true if the specified entity is in the Deleted state; otherwise, false.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">The entity was null.</exception>
        /// <exception cref="ArgumentException">The type of the entity does not match the entity type for which the repository was created.</exception>
        bool IsDeleted(object entity);

        /// <summary>
        /// Refreshes an entity with the values from database. The entities in its graph are not refreshed.
        /// </summary>
        /// <param name="entity">The entity to refresh.</param>
        void Refresh(object entity);

        /// <summary>
        /// Detaches the specified entity from the context.
        /// </summary>
        /// <param name="entity">The entity to detach.</param>
        void Detach(object entity);

        /// <summary>
        /// Detaches the specified entity, including any children entities owned by the entity, from the context.
        /// </summary>
        /// <param name="entity">The entity to detach.</param>
        void DetachAll(object entity);

        /// <summary>
        /// Marks the specified entity, including any children entities owned by the entity, for deletion.
        /// </summary>
        /// <param name="entity">The entity to remove.</param>
        void RemoveAll(object entity);

        /// <summary>
        /// Refreshes the relations for the specified entity to its parents.
        /// </summary>
        /// <param name="entity">The entity.</param>
        void RefreshParentRelations(object entity);
    }
}
