﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines operations available for Assembly.
    /// </summary>
    public interface IAssemblyRepository : IRepository<Assembly>
    {
        /// <summary>
        /// Gets the id of the project in which a specified assembly exists at any level in the assembly hierarchy.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <returns>The parent project's id or Guid.Empty if the assembly is not part of a project.</returns>
        Guid GetParentProjectId(Assembly assembly);

        /// <summary>
        /// Gets the id of the project in which a specified assembly exists, optionally specifying whether the assembly can exist anywhere
        /// in assembly hierarchy or only on the 1st level.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <param name="assyIsOnFirstLevel"> If set to true, it specifies that the assembly is on the first level of the assemblies hierarchy;
        /// otherwise it means that the assembly can be anywhere in the hierarchy. Basically, if is true it instructs the method to return the project id only
        /// if the assembly is directly in a project (and is not a sub-assembly of another assembly).</param>
        /// <returns>
        /// The parent project's id
        /// - or - Guid.Empty if <paramref name="assyIsOnFirstLevel"/> is false and the assembly is not part of a project (at any level of the assemblies hierarchy)
        /// - or - Guid.Empty if if <paramref name="assyIsOnFirstLevel"/> is true and the assembly is not a direct child of a project.
        /// </returns>
        Guid GetParentProjectId(Assembly assembly, bool assyIsOnFirstLevel);

        /// <summary>
        /// Gets the top most parent assembly Id.
        /// </summary>
        /// <param name="assemblyId">The assembly Id.</param>
        /// <returns>The top most assembly to which the specified assembly belong to.</returns>
        Guid GetTopParentAssemblyId(Guid assemblyId);

        /// <summary>
        /// Gets the parent assembly Id.
        /// </summary>
        /// <param name="assemblyId">The assembly with the specified Id.</param>
        /// <returns>The parent assembly Id.</returns>
        Guid GetParentAssemblyId(Guid assemblyId);

        /// <summary>
        /// Gets the assembly with the specified id fully loaded.
        /// </summary>
        /// <param name="assemblyId">The assembly id.</param>
        /// <returns>The assembly or null if it was not found.</returns>
        Assembly GetAssemblyFull(Guid assemblyId);

        /// <summary>
        /// Gets the assembly with the specified id. No references are loaded.
        /// </summary>
        /// <param name="assemblyId">The assembly id.</param>
        /// <param name="noTracking">if set to true disable change tracking for the retuned object(s).</param>
        /// <returns>The assembly or null if it is not found.</returns>
        Assembly GetAssembly(Guid assemblyId, bool noTracking = true);

        /// <summary>
        /// Gets the assembly specified by its guid with only one reference, that to the owner.
        /// </summary>
        /// <param name="assemblyGuid">The guid of the assembly</param>
        /// <returns>The assembly with the owner reference</returns>
        Assembly GetAssemblyWithOwner(Guid assemblyGuid);

        /// <summary>
        /// Gets an Assembly to view its details in trash bin.
        /// </summary>
        /// <param name="assemblyId">The assembly id.</param>
        /// <returns>An Assembly.</returns>
        Assembly GetAssemblyForView(Guid assemblyId);

        /// <summary>
        /// Gets the direct parent assembly of the specified entity
        /// </summary>
        /// <param name="entity">The entity to get it's parent.</param>
        /// <param name="noTracking">If set to true disable change tracking for the retuned object(s).</param>
        /// <returns>The parent assembly of the given entity.</returns>
        Assembly GetParentAssembly(object entity, bool noTracking = true);

        /// <summary>
        /// Gets the top assembly to which a specified entity belongs.
        /// </summary>
        /// <param name="entity">The entity to get it's parent.</param>
        /// <returns>The parent assembly of the given entity.</returns>
        Assembly GetTopParentAssembly(object entity);

        /// <summary>
        /// Gets the assembly with top subassemblies.
        /// </summary>
        /// <param name="assemblyGuid">The assembly GUID.</param>
        /// <returns>Assembly with loaded top level assemblies.</returns>
        Assembly GetAssemblyWithTopSubassemblies(Guid assemblyGuid);

        /// <summary>
        /// Gets the assembly with top parts.
        /// </summary>
        /// <param name="assemblyGuid">The assembly GUID.</param>
        /// <returns>Assembly with loaded top level parts.</returns>
        Assembly GetAssemblyWithTopParts(Guid assemblyGuid);

        /// <summary>
        /// Gets the assemblies of a project. The assemblies are barebones.
        /// </summary>
        /// <param name="projectGuid">The guid of the parent project.</param>
        /// <returns>The collection of child assemblies</returns>
        Collection<Assembly> GetProjectAssemblies(Guid projectGuid);

        /// <summary>
        /// Gets all master data assemblies.
        /// </summary>
        /// <returns>A collection containing the assemblies.</returns>
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "The method can not be converted to a property because it accesses a database.")]
        Collection<Assembly> GetAllMasterData();

        /// <summary>
        /// Gets the assemblies of a specified user, that are bookmarked, to be displayed in bookmarks tree.
        /// </summary>
        /// <param name="ownerId">The owner id.</param>
        /// <returns>A collection of assemblies.</returns>
        Collection<Assembly> GetBookmarkedAssemblies(Guid ownerId);

        /// <summary>
        /// Retrieves the assemblies based on the ids list.
        /// </summary>
        /// <param name="ids">The ids of the assemblies to get.</param>
        /// <returns>A collection of assemblies.</returns>
        Collection<Assembly> GetByIds(IEnumerable<Guid> ids);

        /// <summary>
        /// Gets the master data are top level assemblies (assemblies that have no parent).
        /// </summary>
        /// <returns>A collection containing the assemblies.</returns>
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "The method can not be converted to a property because it accesses a database.")]
        Collection<Assembly> GetTopLevelMasterData();

        /// <summary>
        /// Updates the overhead settings in the assembly retrieved from the db, calls the methods for all its parts and subassemblies and commits the changes
        /// </summary>
        /// <param name="assemblyId">The id of the updated assembly</param>
        /// <param name="overheadSettings">The updated overhead settings</param>
        void UpdateOverheadSettings(Guid assemblyId, OverheadSetting overheadSettings);

        /// <summary>
        /// Refreshes the assembly and its process by overriding the changes with data from the db.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        void RefreshWithProcess(Assembly assembly);

        /// <summary>
        /// Refreshes the assembly, including its children.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        void RefreshWithChildren(Assembly assembly);

        /// <summary>
        /// Deletes the specified assembly from the repository using a stored procedure.
        /// The actual deletion from the database is performed during this call (when the stored procedure is called), not when SaveChanges() is called.
        /// </summary>
        /// <param name="assembly">The assembly to delete.</param>
        void DeleteByStoredProcedure(Assembly assembly);
    }
}