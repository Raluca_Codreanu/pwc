﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines the operations available for country settings value history repository.
    /// </summary>
    public interface ICountrySettingsValueHistoryRepository : IRepository<CountrySettingsValueHistory>
    {
        /// <summary>
        /// Retrieves the last modified country setting from value history, based on the country setting guid.
        /// </summary>
        /// <param name="countrySettingGuid">The country setting's guid.</param>
        /// <returns>The last modified country setting from value history.</returns>
        CountrySettingsValueHistory GetLastModifiedByCountrySettingGuid(Guid countrySettingGuid);
    }
}
