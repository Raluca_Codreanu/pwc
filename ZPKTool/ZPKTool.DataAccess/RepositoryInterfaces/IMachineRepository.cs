﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Defines operations available for Machine.
    /// </summary>
    public interface IMachineRepository : IRepository<Machine>
    {
        /// <summary>
        /// Retrieves the machine based on the id.
        /// </summary>
        /// <param name="id">The machine id.</param>
        /// <param name="noTracking">Option specifying if the MergeOption should be NoTracking.</param>
        /// <returns>The machine.</returns>
        Machine GetById(Guid id, bool noTracking = false);

        /// <summary>
        /// Retrieves the machines based on the ids list.
        /// </summary>
        /// <param name="ids">The ids of the machines to get.</param>
        /// <param name="noTracking">Option specifying if the MergeOption should be NoTracking.</param>
        /// <returns>A collection of machines.</returns>
        Collection<Machine> GetByIds(IEnumerable<Guid> ids, bool noTracking = false);

        /// <summary>
        /// Retrieves all the master data machines
        /// </summary>
        /// <returns>A collection containing all the master data machines</returns>
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "The method performs database access.")]
        Collection<Machine> GetAllMasterData();

        /// <summary>
        /// Gets the master data machines with the specified GUIDs.
        /// </summary>
        /// <param name="guids">The GUIDs of the machines to get.</param>
        /// <returns>
        /// A collection of machines.
        /// </returns>
        Collection<Machine> GetMasterDataMachines(IEnumerable<Guid> guids);

        /// <summary>
        /// Gets the id of the parent project of a specified machine.
        /// </summary>
        /// <param name="machine">The machine.</param>
        /// <returns>The id or Guid.Empty if an error occurs.</returns>
        Guid GetParentProjectId(Machine machine);

        /// <summary>
        /// Gets the id of the parent assembly of a specified machine.
        /// </summary>
        /// <param name="machine">The machine.</param>
        /// <returns>The id or Guid.Empty if an error occurs.</returns>
        Guid GetParentAssemblyId(Machine machine);

        /// <summary>
        /// Gets the id of the machines's parent process step.
        /// </summary>
        /// <param name="machine">The machine.</param>
        /// <returns>The id or Guid.Empty if no parent process step is found.</returns>
        Guid GetParentProcessStepId(Machine machine);

        /// <summary>
        /// Deletes the specified machine from the repository using a stored procedure.
        /// The actual deletion from the database is performed during this call (when the stored procedure is called), not when SaveChanges() is called.
        /// </summary>
        /// <param name="machine">The machine to delete.</param>
        void DeleteByStoredProcedure(Machine machine);
    }
}
