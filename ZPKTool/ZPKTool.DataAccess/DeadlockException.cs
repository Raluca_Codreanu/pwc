﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Represents a deadlock that has occurred at database level.
    /// Deadlocking occurs when two user processes have locks on separate objects and each process is trying 
    /// to acquire a lock on the object that the other process has. When this happens,
    /// SQL Server identifies the problem and ends the deadlock by automatically choosing
    /// one process and aborting the other process, allowing the other process to continue. 
    /// The aborted transaction is rolled back and an error message is sent to the user of the
    /// aborted process. Generally, the transaction that requires the least amount of 
    /// overhead to rollback is the transaction that is aborted.
    /// </summary>
    [Serializable]
    public class DeadlockException : DataAccessException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DeadlockException"/> class.
        /// </summary>
        public DeadlockException()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DeadlockException"/> class.
        /// </summary>
        /// <param name="errorCode">The error code.</param>
        public DeadlockException(string errorCode)
            : base(errorCode)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DeadlockException"/> class.
        /// </summary>
        /// <param name="errorCode">The code for the error.</param>
        /// <param name="ex">The inner exception.</param>
        public DeadlockException(string errorCode, Exception ex)
            : base(errorCode, ex)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DeadlockException"/> class.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the exception being thrown.</param>
        /// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the source or destination.</param>
        protected DeadlockException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
