﻿namespace ZPKTool.DataAccess
{
    /*
     * This code is not necessary now but it will be usefull if hacks for the Entity Framework's DbContext will become necessary.
     * 
    /// <summary>
    /// This class adds a few extension methods to some of the objects from Entity Framework.
    /// </summary>
    public static class EFExtensions
    {
        /// <summary>
        /// Determines whether an object has the EntityReference generic type.
        /// </summary>
        /// <param name="relatedEnd">The object.</param>
        /// <returns>
        /// True if <paramref name="relatedEnd"/> is an EntityReference, otherwise false.
        /// </returns>
        public static bool IsEntityReference(this IRelatedEnd relatedEnd)
        {
            try
            {
                // The call below will throw an exception if relatedEnd does not have a generic type.
                // We return false in this case because obviously it is not an entity reference.
                Type relationshipType = relatedEnd.GetType().GetGenericTypeDefinition();
                return relationshipType == typeof(EntityReference<>);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Determines whether an object has the EntityCollection generic type.
        /// </summary>
        /// <param name="relatedEnd">The object.</param>
        /// <returns>
        /// True if <paramref name="relatedEnd"/> is an EntityCollection, otherwise false.
        /// </returns>
        public static bool IsEntityCollection(this IRelatedEnd relatedEnd)
        {
            try
            {
                // The call below will throw an exception if relatedEnd does not have a generic type.
                // We return false in this case because obviously it is not an entity collection.
                Type relationshipType = relatedEnd.GetType().GetGenericTypeDefinition();
                return relationshipType == typeof(EntityCollection<>);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Determines whether a relationship has at one of its ends an object with a given key.
        /// </summary>
        /// <param name="entry">The relationship.</param>
        /// <param name="key">The key of an entity.</param>
        /// <returns>
        /// True if the relationship has an object with the key at one end, false otherwise.
        /// </returns>
        public static bool IsRelationshipForKey(this ObjectStateEntry entry, EntityKey key)
        {
            if (entry.IsRelationship == false)
            {
                return false;
            }

            return ((EntityKey)entry.UsableValues()[0] == key) || ((EntityKey)entry.UsableValues()[1] == key);
        }

        /// <summary>
        /// Given a relationship and the key from one of its ends, obtain the key at the other end of the relationship.
        /// </summary>
        /// <param name="relationshipEntry">The relationship.</param>
        /// <param name="thisEndKey">The key from one of <paramref name="relationshipEntry"/>'s ends.</param>
        /// <returns>The key at the other end of <paramref name="relationshipEntry"/>' or null if there is no
        /// key at the other end.</returns>
        public static EntityKey OtherEndKey(this ObjectStateEntry relationshipEntry, EntityKey thisEndKey)
        {
            if ((EntityKey)relationshipEntry.UsableValues()[0] == thisEndKey)
            {
                return (EntityKey)relationshipEntry.UsableValues()[1];
            }
            else
                if ((EntityKey)relationshipEntry.UsableValues()[1] == thisEndKey)
                {
                    return (EntityKey)relationshipEntry.UsableValues()[0];
                }

            return null;
        }

        /// <summary>
        /// The ObjectStateEntry instance corresponding to an entity (ObjectEntity) contains two sets of 
        /// properties, one for current values and one for original values, and depending on the "state" of the 
        /// entity you may be able to access only one of them or both. This method returns the set of 
        /// properties that is available, giving priority to the current values (if both sets are avialable
        /// the current values are returned).
        /// </summary>
        /// <param name="entry">The entry.</param>
        /// <returns>The available set of properties.</returns>
        /// <exception cref="InvalidOperationException">
        /// Thrown if <paramref name="entry"/> is in an invalid state.
        /// </exception>
        public static IExtendedDataRecord UsableValues(this ObjectStateEntry entry)
        {
            switch (entry.State)
            {
                case EntityState.Added:
                case EntityState.Detached:
                case EntityState.Unchanged:
                case EntityState.Modified:
                    return (IExtendedDataRecord)entry.CurrentValues;
                case EntityState.Deleted:
                    return (IExtendedDataRecord)entry.OriginalValues;
                default:
                    throw new InvalidOperationException("This entity state (" + entry.State.ToString() + ") should not exist.");
            }
        }

        /// <summary>
        /// Gets the value of an EntityReference&lt;T&gt;.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>The value.</returns>
        public static IEntityWithRelationships GetValue(this EntityReference entity)
        {
            PropertyInfo pi = entity.GetType().GetProperty("Value");
            if (pi != null && pi.CanRead)
            {
                return pi.GetValue(entity, null) as IEntityWithRelationships;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Sets the Value property of an EntityReference&lt;T&gt;.
        /// </summary>
        /// <param name="entityRef">The entity reference.</param>
        /// <param name="value">The value.</param>
        public static void SetValue(this EntityReference entityRef, object value)
        {
            PropertyInfo prop = entityRef.GetType().GetProperty("Value");
            if (prop != null && prop.CanWrite)
            {
                prop.SetValue(entityRef, value, null);
            }
        }
    }*/
}
