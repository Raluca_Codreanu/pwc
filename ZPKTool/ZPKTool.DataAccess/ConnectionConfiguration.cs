﻿using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using ZPKTool.Common;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Stores the information necessary to connect to the local and central databases and provides ways to obtain connection strings.
    /// </summary>
    public static class ConnectionConfiguration
    {
        /// <summary>
        /// The connection information for the local database.
        /// </summary>
        private static DbConnectionInfo localDbConnInfo;

        /// <summary>
        /// The connection information for the central database.
        /// </summary>
        private static DbConnectionInfo centralDbConnInfo;

        /// <summary>
        /// Gets the local database connection string to be used by the <see cref="System.Data.SqlClient.SqlConnection"/> class.
        /// </summary>
        public static string LocalDbSQLConnectionString
        {
            get { return CreateSQLConnectionString(localDbConnInfo, DbIdentifier.LocalDatabase); }
        }

        /// <summary>
        /// Gets the local database connection string to be used by the <see cref="System.Data.Entity.Core.Objects.ObjectContext"/> class.
        /// </summary>
        public static string LocalDbEntityConnectionString
        {
            get { return CreateEntityConnectionString(localDbConnInfo, DbIdentifier.LocalDatabase); }
        }

        /// <summary>
        /// Gets the central database connection string to be used by the <see cref="System.Data.SqlClient.SqlConnection"/> class.
        /// </summary>
        public static string CentralDbSQLConnectionString
        {
            get { return CreateSQLConnectionString(centralDbConnInfo, DbIdentifier.CentralDatabase); }
        }

        /// <summary>
        /// Gets the central database connection string to be used by the <see cref="System.Data.Entity.Core.Objects.ObjectContext"/> class.
        /// </summary>
        public static string CentralDbEntityConnectionString
        {
            get { return CreateEntityConnectionString(centralDbConnInfo, DbIdentifier.CentralDatabase); }
        }

        /// <summary>
        /// Stores the connection information for the local and central databases to be used later for creating connection strings.        
        /// </summary>
        /// <param name="localDbConnectionInfo">The connection information for the local database.</param>
        /// <param name="centralDbConnectionInfo">The connection information for the central database.</param>
        public static void Initialize(DbConnectionInfo localDbConnectionInfo, DbConnectionInfo centralDbConnectionInfo)
        {
            // Set some default connection values imposed by the DataAccess layer.
            localDbConnectionInfo.ConnectionTimeout = 5;
            localDbConnectionInfo.Pooling = false;
            centralDbConnectionInfo.ConnectionTimeout = 5;
            centralDbConnectionInfo.Pooling = false;

            localDbConnInfo = localDbConnectionInfo;
            centralDbConnInfo = centralDbConnectionInfo;
        }

        /// <summary>
        /// Creates a connection string to be used by the <see cref="System.Data.SqlClient.SqlConnection"/> class.
        /// </summary>
        /// <param name="connectionInfo">The connection information.</param>
        /// <param name="dbLocation">The database used.</param>
        /// <returns>The connection string.</returns>
        public static string CreateSQLConnectionString(DbConnectionInfo connectionInfo, DbIdentifier dbLocation)
        {
            string decryptedPassword = connectionInfo.Password;
            if (!string.IsNullOrEmpty(connectionInfo.PasswordEncryptionKey))
            {
                decryptedPassword = EncryptionManager.Instance.DecryptBlowfish(connectionInfo.Password, connectionInfo.PasswordEncryptionKey);
            }

            var connStrgBuilder = new SqlConnectionStringBuilder();
            connStrgBuilder.DataSource = connectionInfo.DataSource;
            connStrgBuilder.InitialCatalog = connectionInfo.InitialCatalog;

            if (connectionInfo.UseWindowsAuthentication)
            {
                connStrgBuilder.IntegratedSecurity = true;
            }
            else
            {
                connStrgBuilder.IntegratedSecurity = false;
                connStrgBuilder.UserID = connectionInfo.UserId;
                connStrgBuilder.Password = decryptedPassword;
            }

            if (connectionInfo.UseSSLSupport && dbLocation == DbIdentifier.CentralDatabase)
            {
                connStrgBuilder.Encrypt = true;
            }

            connStrgBuilder.Pooling = connectionInfo.Pooling;
            if (connectionInfo.ConnectionTimeout.HasValue)
            {
                connStrgBuilder.ConnectTimeout = connectionInfo.ConnectionTimeout.Value;
            }

            return connStrgBuilder.ConnectionString;
        }

        /// <summary>
        /// Creates a connection string to be used by the <see cref="System.Data.Objects.ObjectContext"/> class.
        /// </summary>
        /// <param name="connectionInfo">The connection information.</param>
        /// <param name="dbLocation">The database used.</param>
        /// <returns>The created entity connection string.</returns>
        public static string CreateEntityConnectionString(DbConnectionInfo connectionInfo, DbIdentifier dbLocation)
        {
            var entityConnBuilder = new EntityConnectionStringBuilder();
            entityConnBuilder.Metadata = "res://*";
            entityConnBuilder.Provider = "System.Data.SqlClient";

            entityConnBuilder.ProviderConnectionString = CreateSQLConnectionString(connectionInfo, dbLocation);
            return entityConnBuilder.ConnectionString;
        }
    }
}
