﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using ZPKTool.Common;
using ZPKTool.Data;

namespace ZPKTool.DataAccess
{
    /// <summary>
    /// Helper methods.
    /// </summary>
    public static class Utils
    {
        /// <summary>
        /// Parse the given data exception to give it the appropriate error code and message.
        /// </summary>
        /// <param name="ex">The exception.</param>
        /// <param name="databaseId">A value indicating the database for which the exception occurred.</param>
        /// <returns>
        /// A persistence exception containing appropriate error code and message.
        /// </returns>
        public static DataAccessException ParseDataException(DataException ex, DbIdentifier databaseId)
        {
            DataAccessException dataAccessException = ParseDataException(ex);
            if (dataAccessException != null)
            {
                dataAccessException.Database = databaseId;

                // If the error is due to invalid schema, modify the error code to one tailored for the database on which it occurred.
                if (dataAccessException.ErrorCode == DatabaseErrorCode.InvalidSchema)
                {
                    if (databaseId == DbIdentifier.LocalDatabase)
                    {
                        dataAccessException.ErrorCode = DatabaseErrorCode.LocalDatabaseVersionMismatch;
                    }
                    else if (databaseId == DbIdentifier.CentralDatabase)
                    {
                        dataAccessException.ErrorCode = DatabaseErrorCode.CentralDatabaseVersionMismatch;
                    }
                }
            }

            return dataAccessException;
        }

        /// <summary>
        /// Parse the given data exception to give it the appropriate error code and message.
        /// </summary>
        /// <param name="ex">The exception.</param>
        /// <returns>
        /// A persistence exception containing appropriate error code and message.
        /// </returns>
        public static DataAccessException ParseDataException(DataException ex)
        {
            DataAccessException dataAcessException = null;

            var validationException = ex as System.Data.Entity.Validation.DbEntityValidationException;
            if (validationException != null)
            {
                string exceptionMessage = "Validation failed for one or more entities.";
                foreach (var entityError in validationException.EntityValidationErrors)
                {
                    string entityErrorMessage = Environment.NewLine;
                    if (entityError.Entry != null && entityError.Entry.Entity != null)
                    {
                        entityErrorMessage += entityError.Entry.Entity.GetType().Name + ": ";
                    }

                    var validationErrorMessages = entityError.ValidationErrors.Select(e => "'" + e.ErrorMessage + "'");
                    entityErrorMessage += string.Join(", ", validationErrorMessages);

                    exceptionMessage += entityErrorMessage;
                }

                exceptionMessage += Environment.NewLine;
                dataAcessException = new DataAccessException(DatabaseErrorCode.Unknown, exceptionMessage, validationException);
            }
            else
            {
                // Determine if the exception hierarchy contains an SQL exception and select an error code based on the
                // SQL exception's error number.
                SqlException sqlException = null;
                Exception parser = ex.InnerException;
                while (parser != null && sqlException == null)
                {
                    sqlException = parser as SqlException;
                    parser = parser.InnerException;
                }

                if (sqlException != null)
                {
                    int sqlExceptionNumber = sqlException.Number;

                    // no connection to the DB
                    if (SqlServerErrorCodes.NoDbConnectionCodes.Contains(sqlExceptionNumber))
                    {
                        dataAcessException = new DataAccessException(DatabaseErrorCode.NoConnection, ex.InnerException);
                    }
                    else if (SqlServerErrorCodes.InvalidSchema.Contains(sqlExceptionNumber))
                    {
                        dataAcessException = new DataAccessException(DatabaseErrorCode.InvalidSchema, ex.InnerException);
                    }
                    else if (sqlExceptionNumber == SqlServerErrorCodes.InvalidUserOrPassword)
                    {
                        dataAcessException = new DataAccessException(DatabaseErrorCode.LoginFailed, ex.InnerException);
                    }
                    else if (sqlExceptionNumber == SqlServerErrorCodes.ForeignKeyViolation)
                    {
                        dataAcessException = new DataAccessException(DatabaseErrorCode.ForeignKeyViolation, ex.InnerException);
                    }
                    else if (sqlExceptionNumber == SqlServerErrorCodes.AccountDisabled)
                    {
                        dataAcessException = new DataAccessException(DatabaseErrorCode.UserAccountDisabled, ex.InnerException);
                    }
                    else if (sqlExceptionNumber == SqlServerErrorCodes.Deadlock)
                    {
                        dataAcessException = new DeadlockException(DatabaseErrorCode.Deadlock, ex.InnerException);
                    }
                    else if (sqlExceptionNumber == SqlServerErrorCodes.TimeoutExpired)
                    {
                        dataAcessException = new DataAccessException(DatabaseErrorCode.TimeoutExpired, ex.InnerException);
                    }
                    else if (sqlExceptionNumber == SqlServerErrorCodes.DuplicateCurrencyIsoCode)
                    {
                        dataAcessException = new DataAccessException(DatabaseErrorCode.DuplicateCurrencyIsoCode, ex.InnerException);
                    }
                }

                // Determine if the exception hierarchy contains an Overflow exception and set the proper error code.
                OverflowException overflowException = null;
                parser = ex.InnerException;
                while (parser != null && overflowException == null)
                {
                    overflowException = parser as OverflowException;
                    parser = parser.InnerException;
                }

                if (overflowException != null)
                {
                    dataAcessException = new DataAccessException(DatabaseErrorCode.ArithmeticOverflow, ex.InnerException);
                }
            }

            // Failsafe in case no error code could be found appropriate for the DataException provided
            if (dataAcessException == null)
            {
                dataAcessException = new DataAccessException(DatabaseErrorCode.Unknown, ex);
            }

            return dataAcessException;
        }

        /// <summary>
        /// Builds a contains expression for a LINQ to Entities query. This is a workaround to the lack of support in
        /// LINQ to Entities for queries of type "List ids; from x in db.XTable where ids.Contains(x.ID) select x".
        /// </summary>
        /// <typeparam name="TElement">The type of the element.</typeparam>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="valueSelector">The value selector.</param>
        /// <param name="values">The values.</param>
        /// <returns>The Contains expression.</returns>
        /// <exception cref="ArgumentNullException">Any of the parameters is null.</exception>
        [SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "The syntax allows lambda expressions, which are simple.")]
        public static Expression<Func<TElement, bool>> BuildContainsExpression<TElement, TValue>(
            Expression<Func<TElement, TValue>> valueSelector,
            IEnumerable<TValue> values)
        {
            if (valueSelector == null)
            {
                throw new ArgumentNullException("valueSelector", "The value selector expression was null");
            }

            if (values == null)
            {
                throw new ArgumentNullException("values", "The values list was null");
            }

            ParameterExpression p = valueSelector.Parameters.Single();
            if (!values.Any())
            {
                return e => false;
            }

            var equals = values.Select(value =>
                (Expression)Expression.Equal(valueSelector.Body, Expression.Constant(value, typeof(TValue))));
            var body = equals.Aggregate<Expression>((accumulate, equal) =>
                Expression.Or(accumulate, equal));

            return Expression.Lambda<Func<TElement, bool>>(body, p);
        }

        /// <summary>
        /// Gets the first parent entity stored in db. The entity is obtained by walking up the data reference chain.
        /// </summary>
        /// <param name="entity">The entity to get its db parent.</param>
        /// <param name="dataSourceManager">the data context associated with the entity..</param>
        /// <returns>The parent entity of the given entity.</returns>
        //// TODO: this method should be removed because it is un-maintainable.
        [SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Justification = "The method will be removed/reworked soon.")]
        public static object GetDBEntity(object entity, IDataSourceManager dataSourceManager)
        {
            try
            {
                if (entity == null)
                {
                    return null;
                }

                var project = entity as Project;
                if (project != null)
                {
                    if (dataSourceManager.ProjectRepository.CheckIfExists(project.Guid))
                    {
                        return project;
                    }
                }

                var assembly = entity as Assembly;
                if (assembly != null)
                {
                    if (dataSourceManager.AssemblyRepository.CheckIfExists(assembly.Guid))
                    {
                        return assembly;
                    }
                }

                var part = entity as Part;
                if (part != null)
                {
                    if (dataSourceManager.PartRepository.CheckIfExists(part.Guid))
                    {
                        return part;
                    }
                }

                var machine = entity as Machine;
                if (machine != null)
                {
                    if (dataSourceManager.MachineRepository.CheckIfExists(machine.Guid))
                    {
                        return machine;
                    }
                    else
                    {
                        return GetDBEntity(machine.ProcessStep, dataSourceManager);
                    }
                }

                var rawMaterial = entity as RawMaterial;
                if (rawMaterial != null)
                {
                    if (dataSourceManager.RawMaterialRepository.CheckIfExists(rawMaterial.Guid))
                    {
                        return rawMaterial;
                    }
                    else
                    {
                        var parentPart = rawMaterial.Part as Part;
                        if (parentPart != null)
                        {
                            return GetDBEntity(parentPart, dataSourceManager);
                        }
                    }
                }

                var consumable = entity as Consumable;
                if (consumable != null)
                {
                    if (dataSourceManager.ConsumableRepository.CheckIfExists(consumable.Guid))
                    {
                        return consumable;
                    }
                    else
                    {
                        return GetDBEntity(consumable.ProcessStep, dataSourceManager);
                    }
                }

                var commodity = entity as Commodity;
                if (commodity != null)
                {
                    if (dataSourceManager.CommodityRepository.CheckIfExists(commodity.Guid))
                    {
                        return commodity;
                    }
                    else
                    {
                        var parentPart = commodity.Part as Part;
                        if (parentPart != null)
                        {
                            return GetDBEntity(parentPart, dataSourceManager);
                        }
                        else
                        {
                            var parentStep = commodity.ProcessStep as AssemblyProcessStep;
                            if (parentStep != null)
                            {
                                return GetDBEntity(parentStep, dataSourceManager);
                            }
                        }
                    }
                }

                var die = entity as Die;
                if (die != null)
                {
                    if (dataSourceManager.DieRepository.CheckIfExists(die.Guid))
                    {
                        return die;
                    }
                    else
                    {
                        return GetDBEntity(die.ProcessStep, dataSourceManager);
                    }
                }

                var step = entity as ProcessStep;
                if (entity is ProcessStep)
                {
                    if (dataSourceManager.ProcessStepRepository.CheckIfExists(step.Guid))
                    {
                        return step;
                    }
                    else
                    {
                        return GetDBEntity(step.Process, dataSourceManager);
                    }
                }

                var process = entity as Process;
                if (entity is Process)
                {
                    if (dataSourceManager.ProcessRepository.CheckIfExists(process.Guid))
                    {
                        return process;
                    }
                    else
                    {
                        var parentAssy = process.Assemblies.FirstOrDefault();
                        if (parentAssy != null)
                        {
                            return GetDBEntity(parentAssy, dataSourceManager);
                        }
                        else
                        {
                            var parentPart = process.Parts.FirstOrDefault();
                            if (parentPart != null)
                            {
                                return GetDBEntity(parentPart, dataSourceManager);
                            }
                        }
                    }
                }

                return null;
            }
            catch (DataException ex)
            {
                throw Utils.ParseDataException(ex, dataSourceManager.DatabaseId);
            }
        }
    }
}