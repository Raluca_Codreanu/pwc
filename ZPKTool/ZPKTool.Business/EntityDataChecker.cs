﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using ZPKTool.Business.Resources;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Common;
using ZPKTool.Data;

namespace ZPKTool.Business
{
    /// <summary>
    /// Provides methods for checking if the data contained in an entity is complete and correct.
    /// </summary>
    public static class EntityDataChecker
    {
        /// <summary>
        /// Checks the specified assembly's data against a set of business rules specific to Assemblies.
        /// </summary>
        /// <param name="assembly">The assembly to check.</param>
        /// <returns>
        /// The check result.
        /// </returns>
        public static EntityDataCheckResult CheckAssemblyData(Assembly assembly)
        {
            EntityDataCheckResult checkResult = new EntityDataCheckResult(assembly);

            PartCalculationAccuracy accuracy =
                assembly.CalculationAccuracy.HasValue ? (PartCalculationAccuracy)assembly.CalculationAccuracy.Value : PartCalculationAccuracy.FineCalculation;
            if (accuracy != PartCalculationAccuracy.FineCalculation)
            {
                if (assembly.EstimatedCost == null)
                {
                    checkResult.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => assembly.EstimatedCost));
                }

                return checkResult;
            }

            if (assembly.LifeTime == null)
            {
                checkResult.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => assembly.LifeTime));
            }

            if (assembly.OtherCost == null)
            {
                checkResult.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => assembly.OtherCost));
            }

            if (assembly.PackagingCost == null)
            {
                checkResult.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => assembly.PackagingCost));
            }

            if (assembly.CalculateLogisticCost != true)
            {
                if (assembly.LogisticCost == null)
                {
                    checkResult.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => assembly.LogisticCost));
                }
            }

            if (assembly.ProjectInvest == null)
            {
                checkResult.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => assembly.ProjectInvest));
            }

            if (assembly.DevelopmentCost == null)
            {
                checkResult.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => assembly.DevelopmentCost));
            }

            var unusedSubEntities = GetUnusedSubPartsAndSubAssemblies(assembly);
            checkResult.UnusedEntities.AddRange(unusedSubEntities);

            // Check the process
            var processCheckResults = CheckProcessData(
                assembly.Process,
                assembly.YearlyProductionQuantity.GetValueOrDefault(),
                assembly.BatchSizePerYear.GetValueOrDefault(),
                assembly.CalculationVariant);

            checkResult.Errors.AddRange(processCheckResults);

            // Check the yearly production qty.
            checkResult.YearlyProductionQtyErrors.AddRange(CheckAssemblyYearlyProductionQty(assembly));

            foreach (Part part in assembly.Parts.Where(p => !p.IsDeleted))
            {
                var partCheckResults = CheckPartData(part);
                if (partCheckResults.HasErrors)
                {
                    checkResult.Errors.Add(partCheckResults);
                }
            }

            foreach (Assembly subassembly in assembly.Subassemblies.Where(a => !a.IsDeleted))
            {
                var subAssemblyCheckResults = CheckAssemblyData(subassembly);
                if (subAssemblyCheckResults.HasErrors)
                {
                    checkResult.Errors.Add(subAssemblyCheckResults);
                }
            }

            return checkResult;
        }

        /// <summary>
        /// Checks the specified part's data against a set of business rules specific to Parts.
        /// </summary>
        /// <param name="part">The part to check.</param>
        /// <returns>
        /// The check result.
        /// </returns>
        public static EntityDataCheckResult CheckPartData(Part part)
        {
            EntityDataCheckResult checkResult = new EntityDataCheckResult(part);

            PartCalculationAccuracy accuracy =
                part.CalculationAccuracy.HasValue ? (PartCalculationAccuracy)part.CalculationAccuracy.Value : PartCalculationAccuracy.FineCalculation;
            if (accuracy != PartCalculationAccuracy.FineCalculation)
            {
                if (part.EstimatedCost == null)
                {
                    checkResult.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => part.EstimatedCost));
                }

                return checkResult;
            }

            if (part.LifeTime == null)
            {
                checkResult.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => part.LifeTime));
            }

            if (part.OtherCost == null)
            {
                checkResult.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => part.OtherCost));
            }

            if (part.PackagingCost == null)
            {
                checkResult.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => part.PackagingCost));
            }

            if (part.CalculateLogisticCost != true)
            {
                if (part.LogisticCost == null)
                {
                    checkResult.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => part.LogisticCost));
                }
            }

            if (part.ProjectInvest == null)
            {
                checkResult.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => part.ProjectInvest));
            }

            if (part.DevelopmentCost == null)
            {
                checkResult.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => part.DevelopmentCost));
            }

            foreach (RawMaterial material in part.RawMaterials.Where(m => !m.IsDeleted))
            {
                var materialCheckResult = GetInvalidRawMaterialData(material);
                if (materialCheckResult.HasErrors)
                {
                    checkResult.Errors.Add(materialCheckResult);
                }
            }

            foreach (Commodity commodity in part.Commodities.Where(c => !c.IsDeleted))
            {
                var commodityCheckResult = GetMissingCommodityData(commodity);
                if (commodityCheckResult.HasErrors)
                {
                    checkResult.Errors.Add(commodityCheckResult);
                }
            }

            // Check the process
            var processCheckResults = CheckProcessData(
                part.Process,
                part.YearlyProductionQuantity.GetValueOrDefault(),
                part.BatchSizePerYear.GetValueOrDefault(),
                part.CalculationVariant);

            checkResult.Errors.AddRange(processCheckResults);

            if (part.RawPart != null && !part.RawPart.IsDeleted)
            {
                var rawPartCheckResult = CheckPartData(part.RawPart);
                if (rawPartCheckResult.HasErrors)
                {
                    checkResult.Errors.Add(rawPartCheckResult);
                }
            }

            return checkResult;
        }

        /// <summary>
        /// Checks the data of a specified process.
        /// </summary>
        /// <param name="process">The process.</param>
        /// <param name="parentYearlyProductionQuantity">The process parent's yearly production quantity.</param>
        /// <param name="parentBatchSize">The process parent's batch size.</param>
        /// <param name="parentCalculationVariant">The process parent's calculation variant.</param>
        /// <returns>The list containing the check results for the process steps that had errors.</returns>
        public static ICollection<EntityDataCheckResult> CheckProcessData(
            Process process,
            decimal parentYearlyProductionQuantity,
            decimal parentBatchSize,
            string parentCalculationVariant)
        {
            if (process == null)
            {
                return new List<EntityDataCheckResult>();
            }

            List<EntityDataCheckResult> checkResults = new List<EntityDataCheckResult>();

            // Calculate the batch size target
            decimal partsPerYear = parentYearlyProductionQuantity;
            decimal batchesPerYear = parentBatchSize;
            decimal batchSizeTarget = 0m;
            if (batchesPerYear != 0m)
            {
                batchSizeTarget = Math.Ceiling(partsPerYear / batchesPerYear);
            }

            decimal totalNecessaryBatchSize = batchSizeTarget;

            // The steps must be processed in reverse order to calculate the gross batch size
            foreach (ProcessStep step in process.Steps.OrderByDescending(s => s.Index))
            {
                // Check the step data                
                EntityDataCheckResult stepCheckResult = CheckProcessStepData(step);

                ProcessCalculationAccuracy stepAccuracy =
                    (ProcessCalculationAccuracy)step.Accuracy.GetValueOrDefault(ProcessCalculationAccuracy.Calculated);

                /*** Machine overload check ****/

                // The Machine overload check is performed starting from calculation version 1.2 and on. For older versions it is not performed.
                if (!CostCalculatorFactory.IsOlder(parentCalculationVariant, "1.2")
                    && stepAccuracy == ProcessCalculationAccuracy.Calculated)
                {
                    // Calculate the gross batch size for the step (see enhanced scrap calculation algorithm for what gross batch size means)
                    decimal grossBatchSize = Math.Ceiling(((1m + step.ScrapAmount) ?? 0) * totalNecessaryBatchSize);
                    totalNecessaryBatchSize = grossBatchSize;

                    // Compute the time needed for producing the step's parts
                    decimal grossPartsPerYear = grossBatchSize * batchesPerYear;
                    decimal processTimePerPart = 0m;
                    if (step.PartsPerCycle.HasValue && step.PartsPerCycle.Value != 0m)
                    {
                        processTimePerPart = step.ProcessTime.GetValueOrDefault() / step.PartsPerCycle.Value;
                    }

                    decimal processTimePerYear = grossPartsPerYear * processTimePerPart;

                    decimal setupTimePerBatch = step.MaxDownTime.GetValueOrDefault() * step.SetupsPerBatch.GetValueOrDefault();
                    decimal stepBatchesPerYear = 0m;
                    if (step.BatchSize.HasValue && step.BatchSize.Value != 0m)
                    {
                        stepBatchesPerYear = Math.Ceiling(partsPerYear / step.BatchSize.Value);
                    }

                    decimal setupTimePerYear = stepBatchesPerYear * setupTimePerBatch;

                    // Time needed to produce Parts per Year
                    decimal totalTimeNeeded = processTimePerYear + setupTimePerYear;

                    // The total available production time in 1 year (in seconds)
                    decimal productionTimePerYear = (step.ShiftsPerWeek * step.HoursPerShift * step.ProductionWeeksPerYear).GetValueOrDefault() * 3600m;

                    foreach (Machine machine in step.Machines.Where(m => !m.IsDeleted).OrderBy(m => m.Index))
                    {
                        decimal machiningTimeAvailable = productionTimePerYear * machine.Availability.GetValueOrDefault();
                        if (totalTimeNeeded > machiningTimeAvailable * machine.Amount)
                        {
                            // the machine is overloaded
                            stepCheckResult.OverloadedMachines.Add(machine);
                        }
                    }
                }

                /*********************************/

                // Add the check result to the list if it contains errors.
                if (stepCheckResult.HasErrors)
                {
                    checkResults.Insert(0, stepCheckResult);
                }
            }

            return checkResults;
        }

        /// <summary>
        /// Checks a process step's data against some business rules.
        /// </summary>
        /// <param name="step">The step to check.</param>        
        /// <returns>
        /// The check result for the step.
        /// </returns>
        [SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Justification = "The method is simple enough to be easily understood.")]
        private static EntityDataCheckResult CheckProcessStepData(ProcessStep step)
        {
            EntityDataCheckResult checkResult = new EntityDataCheckResult(step);

            ProcessCalculationAccuracy calcAccuracy =
                step.Accuracy.HasValue ? (ProcessCalculationAccuracy)step.Accuracy.Value : ProcessCalculationAccuracy.Calculated;
            if (calcAccuracy == ProcessCalculationAccuracy.Estimated)
            {
                if (step.Price == null)
                {
                    checkResult.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step.Price));
                }

                return checkResult;
            }

            if (step.CycleTime == null)
            {
                checkResult.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step.CycleTime));
            }

            if (step.SetupsPerBatch == null)
            {
                checkResult.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step.SetupsPerBatch));
            }

            if (step.SetupTime == null)
            {
                checkResult.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step.SetupTime));
            }

            if (step.MaxDownTime == null)
            {
                checkResult.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step.MaxDownTime));
            }

            if (step.SetupUnskilledLabour == null)
            {
                checkResult.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step.SetupUnskilledLabour));
            }

            if (step.SetupSkilledLabour == null)
            {
                checkResult.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step.SetupSkilledLabour));
            }

            if (step.SetupForeman == null)
            {
                checkResult.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step.SetupForeman));
            }

            if (step.SetupTechnicians == null)
            {
                checkResult.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step.SetupTechnicians));
            }

            if (step.SetupEngineers == null)
            {
                checkResult.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step.SetupEngineers));
            }

            if (step.BatchSize == null)
            {
                checkResult.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step.BatchSize));
            }

            if (step.ProcessTime == null)
            {
                checkResult.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step.ProcessTime));
            }

            if (step.ProductionUnskilledLabour == null)
            {
                checkResult.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step.ProductionUnskilledLabour));
            }

            if (step.ProductionSkilledLabour == null)
            {
                checkResult.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step.ProductionSkilledLabour));
            }

            if (step.ProductionForeman == null)
            {
                checkResult.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step.ProductionForeman));
            }

            if (step.ProductionTechnicians == null)
            {
                checkResult.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step.ProductionTechnicians));
            }

            if (step.ProductionEngineers == null)
            {
                checkResult.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step.ProductionEngineers));
            }

            if (step.ScrapAmount == null)
            {
                checkResult.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step.ScrapAmount));
            }

            if (step.PartsPerCycle == null)
            {
                checkResult.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step.PartsPerCycle));
            }

            // check for division by zero
            if (step.CycleTime != null && step.CycleTime == 0)
            {
                checkResult.InvalidValueProperties.Add(ReflectionUtils.GetPropertyName(() => step.CycleTime));
            }

            if (step.BatchSize != null && step.BatchSize == 0)
            {
                checkResult.InvalidValueProperties.Add(ReflectionUtils.GetPropertyName(() => step.BatchSize));
            }

            if (step.ExceedShiftCost)
            {
                if (!step.ExtraShiftsNumber.HasValue)
                {
                    checkResult.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step.ExtraShiftsNumber));
                }

                if (!step.ShiftCostExceedRatio.HasValue)
                {
                    checkResult.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => step.ShiftCostExceedRatio));
                }
            }

            foreach (Machine machine in step.Machines.Where(m => !m.IsDeleted).OrderBy(m => m.Index))
            {
                EntityDataCheckResult machineCheckResult = GetMissingMachineData(machine);
                if (machineCheckResult.HasErrors)
                {
                    checkResult.Errors.Add(machineCheckResult);
                }
            }

            foreach (Consumable consumable in step.Consumables.Where(c => !c.IsDeleted))
            {
                EntityDataCheckResult consumableCheckResult = GetMissingConsumableData(consumable);
                if (consumableCheckResult.HasErrors)
                {
                    checkResult.Errors.Add(consumableCheckResult);
                }
            }

            foreach (Commodity commodity in step.Commodities.Where(c => !c.IsDeleted))
            {
                EntityDataCheckResult commodityCheckResult = GetMissingCommodityData(commodity);
                if (commodityCheckResult.HasErrors)
                {
                    checkResult.Errors.Add(commodityCheckResult);
                }
            }

            foreach (Die die in step.Dies.Where(d => !d.IsDeleted))
            {
                EntityDataCheckResult dieCheckResult = GetMissingDieData(die);
                if (dieCheckResult.HasErrors)
                {
                    checkResult.Errors.Add(dieCheckResult);
                }
            }

            return checkResult;
        }

        /// <summary>
        /// Checks the yearly production quantity by comparing, for each sub item, the needed quantity for an assembly with the sub item's quantity.
        /// </summary>
        /// <param name="assembly">The assembly for which the yearly production quantity is checked.</param>
        /// <returns>The list of yearly production qty errors.</returns>
        private static List<YearlyProductionQtyError> CheckAssemblyYearlyProductionQty(Assembly assembly)
        {
            var yearlyProductionQtyErrors = new List<YearlyProductionQtyError>();

            if (assembly.YearlyProductionQuantity == null)
            {
                return yearlyProductionQtyErrors;
            }

            foreach (var subassembly in assembly.Subassemblies.Where(a => !a.IsDeleted && a.YearlyProductionQuantity != null))
            {
                // Compare the parent's needed qty with the subassembly's qty.
                var neededQty = CalculateNeededQuantity(subassembly);
                if (subassembly.YearlyProductionQuantity.Value < neededQty)
                {
                    yearlyProductionQtyErrors.Add(new YearlyProductionQtyError() { Entity = subassembly, EntityName = subassembly.Name, IsValueSmaller = true });
                }
                else if (subassembly.YearlyProductionQuantity.Value > neededQty)
                {
                    yearlyProductionQtyErrors.Add(new YearlyProductionQtyError() { Entity = subassembly, EntityName = subassembly.Name, IsValueSmaller = false });
                }
            }

            foreach (var subpart in assembly.Parts.Where(p => !p.IsDeleted && p.YearlyProductionQuantity != null))
            {
                // Compare the parent's needed qty with the subpart's qty.
                var neededQty = CalculateNeededQuantity(subpart);
                if (subpart.YearlyProductionQuantity.Value < neededQty)
                {
                    yearlyProductionQtyErrors.Add(new YearlyProductionQtyError() { Entity = subpart, EntityName = subpart.Name, IsValueSmaller = true });
                }
                else if (subpart.YearlyProductionQuantity.Value > neededQty)
                {
                    yearlyProductionQtyErrors.Add(new YearlyProductionQtyError() { Entity = subpart, EntityName = subpart.Name, IsValueSmaller = false });
                }
            }

            return yearlyProductionQtyErrors;
        }

        /// <summary>
        /// Calculates the needed quantity of an assembly.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <returns>The needed quantity.</returns>
        private static int CalculateNeededQuantity(Assembly assembly)
        {
            // assembly needed quantity = assembly amount *
            // -parent yearly production quantity, if the parent is the top parent assembly
            // -parent needed quantity, else
            var quantity = 0;

            if (assembly.ParentAssembly == null)
            {
                return quantity;
            }

            int noOfPartsUsed = 0;
            if (assembly.ParentAssembly.Process != null)
            {
                noOfPartsUsed = assembly.ParentAssembly.Process.Steps.Sum(step =>
                {
                    var amount = step.AssemblyAmounts.FirstOrDefault(assemblyAmount => assemblyAmount.FindAssemblyId() == assembly.Guid);
                    return amount != null ? amount.Amount : 0;
                });
            }

            if (assembly.ParentAssembly.ParentAssembly == null)
            {
                quantity = assembly.ParentAssembly.YearlyProductionQuantity.HasValue
                    ? assembly.ParentAssembly.YearlyProductionQuantity.Value
                    : 0;
            }
            else
            {
                quantity = CalculateNeededQuantity(assembly.ParentAssembly);
            }

            return noOfPartsUsed * quantity;
        }

        /// <summary>
        /// Calculates the needed quantity of a part.
        /// </summary>
        /// <param name="part">The part.</param>
        /// <returns>The needed quantity.</returns>
        private static int CalculateNeededQuantity(Part part)
        {
            // part needed quantity = assembly amount *
            // -parent yearly production quantity, if the parent is the top parent assembly
            // -parent needed quantity, else
            var quantity = 0;

            if (part.Assembly == null)
            {
                return quantity;
            }

            int noOfPartsUsed = 1;
            if (part.Assembly.Process != null)
            {
                noOfPartsUsed = part.Assembly.Process.Steps.Sum(step =>
                {
                    var amount = step.PartAmounts.FirstOrDefault(partAmount => partAmount.FindPartId() == part.Guid);
                    return amount != null ? amount.Amount : 0;
                });
            }

            if (part.Assembly.ParentAssembly == null)
            {
                quantity = part.Assembly.YearlyProductionQuantity.Value;
            }
            else
            {
                quantity = CalculateNeededQuantity(part.Assembly);
            }

            return noOfPartsUsed * quantity;
        }

        #region Missing entity data checks

        /// <summary>
        /// Gets the parts and sub-assemblies which are not used in the process of the specified assembly.
        /// </summary>
        /// <param name="assembly">The assembly for which to get the unused parts/subassemblies.</param>
        /// <returns>A list containing all unused sub-parts and sub-assemblies.</returns>
        [SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Justification = "The method is easy to understand.")]
        private static List<object> GetUnusedSubPartsAndSubAssemblies(Assembly assembly)
        {
            List<object> unusedEntitiesList = new List<object>();

            List<Part> unusedParts = new List<Part>();
            List<Assembly> unusedAssemblies = new List<Assembly>();

            // At first, all sub-parts and sub-assemblies are considered unused
            unusedParts.AddRange(assembly.Parts.Where(p => !p.IsDeleted));
            unusedAssemblies.AddRange(assembly.Subassemblies.Where(a => !a.IsDeleted));

            if (assembly.Process != null && assembly.Process.Steps != null && assembly.Process.Steps.Count > 0)
            {
                foreach (ProcessStep step in assembly.Process.Steps)
                {
                    foreach (ProcessStepPartAmount partAmount in step.PartAmounts)
                    {
                        Part part = assembly.Parts.FirstOrDefault(p => !p.IsDeleted && p.Guid == partAmount.FindPartId());
                        if (part != null && partAmount.Amount > 0)
                        {
                            unusedParts.Remove(part);
                        }
                    }

                    foreach (ProcessStepAssemblyAmount assemblyAmount in step.AssemblyAmounts.Where(a => a.Assembly != null && !a.Assembly.IsDeleted))
                    {
                        Assembly subAssy = assembly.Subassemblies.FirstOrDefault(a => !a.IsDeleted && a.Guid == assemblyAmount.FindAssemblyId());
                        if (subAssy != null && assemblyAmount.Amount > 0)
                        {
                            unusedAssemblies.Remove(subAssy);
                        }
                    }
                }
            }

            unusedEntitiesList.AddRange(unusedParts);
            unusedEntitiesList.AddRange(unusedAssemblies);

            return unusedEntitiesList;
        }

        /// <summary>
        /// Checks if the specified raw material contains all the required data for calculating its cost.
        /// </summary>
        /// <param name="material">The material to check.</param>
        /// <returns>An structure containing the check result.</returns>
        private static EntityDataCheckResult GetInvalidRawMaterialData(RawMaterial material)
        {
            EntityDataCheckResult result = new EntityDataCheckResult(material);

            if (material.Price == null)
            {
                result.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => material.Price));
            }

            if (material.Quantity == null)
            {
                result.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => material.Quantity));
            }

            if (material.ParentWeight == null)
            {
                result.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => material.ParentWeight));
            }

            if (!material.Loss.HasValue)
            {
                result.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => material.Loss));
            }

            if (material.ScrapRefundRatio == null)
            {
                result.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => material.ScrapRefundRatio));
            }

            if (material.RejectRatio == null)
            {
                result.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => material.RejectRatio));
            }

            if (material.Sprue != null && material.Sprue.Value < 0)
            {
                result.InvalidValueProperties.Add(ReflectionUtils.GetPropertyName(() => material.Sprue));
            }

            if (material.StockKeeping == null)
            {
                result.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => material.StockKeeping));
            }

            if (material.RecyclingRatio == null)
            {
                result.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => material.RecyclingRatio));
            }

            return result;
        }

        /// <summary>
        /// Checks if the specified commodity contains all the required data for calculating its cost.
        /// </summary>
        /// <param name="commodity">The commodity to check.</param>
        /// <returns>An structure containing the check result.</returns>
        private static EntityDataCheckResult GetMissingCommodityData(Commodity commodity)
        {
            EntityDataCheckResult result = new EntityDataCheckResult(commodity);

            if (commodity.Price == null)
            {
                result.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => commodity.Price));
            }

            if (!commodity.Amount.HasValue)
            {
                result.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => commodity.Amount));
            }

            return result;
        }

        /// <summary>
        /// Checks if the specified consumable contains all the required data for calculating its cost.
        /// </summary>
        /// <param name="consumable">The consumable to check.</param>
        /// <returns>An structure containing the check result.</returns>
        private static EntityDataCheckResult GetMissingConsumableData(Consumable consumable)
        {
            EntityDataCheckResult result = new EntityDataCheckResult(consumable);

            if (consumable.PriceUnitBase == null)
            {
                result.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => consumable.PriceUnitBase));
            }

            if (!consumable.Amount.HasValue)
            {
                result.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => consumable.Amount));
            }

            if (!consumable.Price.HasValue)
            {
                result.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => consumable.Price));
            }

            return result;
        }

        /// <summary>
        /// Checks if the specified machine contains all the required data for calculating its cost.
        /// </summary>
        /// <param name="machine">The machine to check.</param>
        /// <returns>An structure containing the check result.</returns>
        private static EntityDataCheckResult GetMissingMachineData(Machine machine)
        {
            EntityDataCheckResult result = new EntityDataCheckResult(machine);

            if (machine.CalculateWithKValue == true)
            {
                if (machine.KValue == null)
                {
                    result.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => machine.KValue));
                }

                if (machine.MachineInvestment == null && machine.MachineLeaseCosts == null)
                {
                    result.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => machine.MachineInvestment));
                }
            }
            else
            {
                if (machine.ConsumablesCost == null)
                {
                    result.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => machine.ConsumablesCost));
                }

                if (machine.ExternalWorkCost == null)
                {
                    result.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => machine.ExternalWorkCost));
                }

                if (machine.MaterialCost == null)
                {
                    result.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => machine.MaterialCost));
                }

                if (machine.RepairsCost == null)
                {
                    result.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => machine.RepairsCost));
                }
            }

            if (machine.AirConsumption == null)
            {
                result.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => machine.AirConsumption));
            }

            if (machine.WaterConsumption == null)
            {
                result.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => machine.WaterConsumption));
            }

            if (machine.FullLoadRate == null)
            {
                result.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => machine.FullLoadRate));
            }

            if (machine.Availability == null)
            {
                result.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => machine.Availability));
            }

            return result;
        }

        /// <summary>
        /// Checks if the specified die contains all the required data for calculating its cost.
        /// </summary>
        /// <param name="die">The die to check.</param>
        /// <returns>A structure containing the check result.</returns>
        private static EntityDataCheckResult GetMissingDieData(Die die)
        {
            EntityDataCheckResult result = new EntityDataCheckResult(die);

            if (!die.LifeTime.HasValue)
            {
                result.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => die.LifeTime));
            }

            if (!die.Investment.HasValue)
            {
                result.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => die.Investment));
            }

            if (!die.AllocationRatio.HasValue)
            {
                result.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => die.AllocationRatio));
            }

            if (!die.Wear.HasValue)
            {
                result.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => die.Wear));
            }

            if (!die.Maintenance.HasValue)
            {
                result.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => die.Maintenance));
            }

            if (die.CostAllocationBasedOnPartsPerLifeTime != true && !die.CostAllocationNumberOfParts.HasValue)
            {
                result.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => die.CostAllocationNumberOfParts));
            }

            if (die.AreAllPaidByCustomer != true && !die.DiesetsNumberPaidByCustomer.HasValue)
            {
                result.NullValueProperties.Add(ReflectionUtils.GetPropertyName(() => die.DiesetsNumberPaidByCustomer));
            }

            return result;
        }

        #endregion Missing entity data checks
    }
}