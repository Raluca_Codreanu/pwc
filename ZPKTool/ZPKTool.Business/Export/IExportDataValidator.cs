﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.Export
{
    /// <summary>
    /// Defines methods to validate a data object (entity) before exporting it and to validate an <see cref="ExportedObject"/> before importing it.
    /// <para />
    /// This functionality allows to heal models that were corrupted by bugs in the export or import algorithm because 
    /// objects that are found "invalid" are skipped during the export/import process and the process is not stopped nor does it fail.
    /// </summary>
    public interface IExportDataValidator
    {
        /// <summary>
        /// Validates the specified <see cref="ExportedObject" /> during import.        
        /// </summary>
        /// <param name="exportedObject">The exported object to validate.</param>
        /// <param name="dataObjectType">The type of the data object represented by <paramref name="exportedObject"/>.</param>
        /// <returns>
        /// True if the exported object is valid; otherwise false.
        /// </returns>
        bool ValidateExportedObject(ExportedObject exportedObject, Type dataObjectType);

        /// <summary>
        /// Validates the specified data object during export.
        /// </summary>
        /// <param name="dataObject">The data object.</param>
        /// <returns>True if the data object is valid, false otherwise.</returns>
        bool ValidateDataObject(object dataObject);
    }
}
