﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.Export
{
    /// <summary>
    /// Provides a way to convert the value of a field/property from older versions of the field/property to the current version during
    /// the import of an object.
    /// Example: if you have a property name "StartDate" of type int, then you change it to string and then finally to DateTime, its implementation
    /// will convert the int and string values to DateTime.
    /// </summary>
    public interface IExportedValueConverter
    {
        /// <summary>
        /// Converts the specified value to the specified type.
        /// </summary>
        /// <param name="fieldName">Name of the field.</param>
        /// <param name="value">The value to convert.</param>
        /// <param name="targetType">The type to which to convert.</param>
        /// <returns>The converted value.</returns>
        object Convert(string fieldName, object value, Type targetType);
    }
}
