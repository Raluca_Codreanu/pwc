﻿using System;
using System.Collections.ObjectModel;

namespace ZPKTool.Business.Export
{
    /// <summary>
    /// An object containing data to be serialized in binary format.
    /// </summary>
    [Serializable]
    public class ExportedCurrenciesData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExportedCurrenciesData" /> class.
        /// </summary>
        public ExportedCurrenciesData()
        {
            this.Currencies = new Collection<ExportedObject>();
        }

        /// <summary>
        /// Gets the currencies.
        /// </summary>
        public Collection<ExportedObject> Currencies { get; private set; }

        /// <summary>
        /// Gets or sets the base currency.
        /// </summary>
        public ExportedObject BaseCurrency { get; set; }
    }
}