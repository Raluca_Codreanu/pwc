﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.Export
{    
    /// <summary>
    /// Represents a piece of exported data.
    /// </summary>
    [Serializable]
    public class DataPiece
    {
        /// <summary>
        /// Gets or sets the type of the data piece.
        /// </summary>
        public DataType Type { get; set; }

        /// <summary>
        /// Gets or sets the key that identifies this data piece; usually is the name of the field/property represented by this instance.
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public object Value { get; set; }
    }
}