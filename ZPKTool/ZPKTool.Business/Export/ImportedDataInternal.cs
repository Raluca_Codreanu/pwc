using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ZPKTool.Data;

namespace ZPKTool.Business.Export
{
    /// <summary>
    /// Contains the data imported from a file during an import operation, along with error information.
    /// </summary>
    /// <typeparam name="TEntity">The type of the imported entity.</typeparam>
    internal class ImportedDataInternal<TEntity> : ImportedData<TEntity>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ImportedDataInternal{TEntity}"/> class.
        /// </summary>
        public ImportedDataInternal()
        {
            this.MediaCollection = new Dictionary<Guid, IEnumerable<ImportedMedia>>();
            this.Currencies = new List<Currency>();
        }

        /// <summary>
        /// Gets or sets the entity imported.
        /// </summary>
        public TEntity Entity { get; set; }

        /// <summary>
        /// Gets or sets the exception that occurred during import.
        /// </summary>            
        public Exception Error { get; set; }

        /// <summary>
        /// Gets or sets the cost calculation data that was imported with the entity. It can be null because it is not exported for all types of entities.
        /// This is normally used to calculate the entity's cost when the entity is not attached to a parent.
        /// </summary>        
        public ExportedCalculationData CostCalculationData { get; set; }

        /// <summary>
        /// Gets the currencies data that was imported with the entity. It can be null because it is not exported for all types of entities.
        /// </summary>
        public ICollection<Currency> Currencies { get; private set; }

        /// <summary>
        /// Gets or sets the base currency that was imported with the entity. It can be null because it is not exported for all types of entities.
        /// </summary>
        public Currency BaseCurrency { get; set; }

        /// <summary>
        /// Gets the media collection. An item of this dictionary contains the Id of the parent entity (the Key) and the list of ImportedMedia objects which
        /// which represent the media of the given entity (the Value).
        /// </summary>
        public Dictionary<Guid, IEnumerable<ImportedMedia>> MediaCollection { get; private set; }

        /// <summary>
        /// Gets or sets the path to the imported entity file.
        /// </summary>
        public string SourceFile { get; set; }
    }
}