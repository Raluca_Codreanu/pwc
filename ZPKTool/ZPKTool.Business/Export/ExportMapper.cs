﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using ZPKTool.Common;

namespace ZPKTool.Business.Export
{
    /// <summary>
    /// Maps data types to and from export object format.
    /// </summary>    
    internal class ExportMapper
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The type mappings used for export and import.
        /// </summary>
        private List<ExportedType> typeMappings = new List<ExportedType>();

        /// <summary>
        /// The data validator to be used during export and import.
        /// </summary>
        private IExportDataValidator dataValidator;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExportMapper"/> class.
        /// </summary>
        /// <exception cref="BusinessException">Error occurred while loading mapping data.</exception>
        public ExportMapper()
        {
            this.InitializeMappings();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExportMapper" /> class.
        /// </summary>
        /// <param name="dataValidator">The data validator.</param>
        /// <exception cref="System.ArgumentNullException">The export data validator was null.</exception>
        /// <exception cref="BusinessException">Error occurred while loading mapping data.</exception>
        public ExportMapper(IExportDataValidator dataValidator)
            : this()
        {
            if (dataValidator == null)
            {
                throw new ArgumentNullException("dataValidator", "The export data validator was null.");
            }

            this.dataValidator = dataValidator;
        }

        /// <summary>
        /// Maps a specified data object to an ExportedObject that can be serialized in binary format.
        /// </summary>
        /// <param name="obj">The object to map.</param>
        /// <returns>An object that can be serialized or null if an error occurred.</returns>
        /// <exception cref="BusinessException">An error occurred during the mapping process.</exception>
        public ExportedObject MapDataObject(object obj)
        {
            return MapDataObject(obj, new Dictionary<object, ExportedObject>());
        }

        /// <summary>
        /// Maps a specified data object to an ExportedObject that can be serialized in binary format.
        /// </summary>
        /// <param name="dataObject">The object to map.</param>
        /// <param name="exportMap">
        /// A dictionary containing each ExportedObject instance created for each of the objects in <paramref name="dataObject"/>'s graphs.
        /// </param>
        /// <returns>
        /// An object that can be serialized or null if an error occurred.
        /// </returns>
        /// <exception cref="BusinessException">An error occurred during the mapping process.</exception>
        public ExportedObject MapDataObject(object dataObject, out Dictionary<object, ExportedObject> exportMap)
        {
            exportMap = new Dictionary<object, ExportedObject>();
            return MapDataObject(dataObject, exportMap);
        }

        /// <summary>
        /// Maps an ExportedObject to the data object it represents.
        /// </summary>
        /// <param name="obj">The exported object.</param>
        /// <returns>The data object contained in t he exported object.</returns>
        public object MapExportedObject(ExportedObject obj)
        {
            return MapExportedObject(obj, new Dictionary<ExportedObject, object>());
        }

        /// <summary>
        /// Maps an ExportedObject to the data object it represents.
        /// </summary>
        /// <param name="obj">The exported object.</param>
        /// <param name="importedObjectsMap">
        /// Contains each object in the imported data object's graph along with the unique identifier of the ExportedObject instance from which it was created.
        /// It is used to associate the exported media to the imported data objects.
        /// </param>
        /// <returns>
        /// The data object contained in the exported object.
        /// </returns>
        public object MapExportedObject(ExportedObject obj, out Dictionary<Guid, object> importedObjectsMap)
        {
            // For each ExportedObject instance in the input object's graph, it contains the data object mapped to it by the mapping algorithm.
            Dictionary<ExportedObject, object> importMap = new Dictionary<ExportedObject, object>();
            object dataObject = MapExportedObject(obj, importMap);

            // Replaces the ExportedObject from the importMap with its Uid.
            importedObjectsMap = new Dictionary<Guid, object>();
            foreach (var kvp in importMap)
            {
                importedObjectsMap.Add(kvp.Key.Uid, kvp.Value);
            }

            return dataObject;
        }

        /// <summary>
        /// Maps a specified data object to an ExportedObject that can be serialized in binary format.        
        /// </summary>
        /// <param name="obj">The object to map.</param>
        /// <param name="exportedObjects">The objects from the graph of obj that have already been exported. It is used to resolve
        /// circular dependencies so that an object is not exported more than once because is being referenced by multiple objects.</param>
        /// <returns>
        /// An object that can be serialized or null if an error occurred.
        /// </returns>
        /// <exception cref="BusinessException">An error occurred during the mapping process.</exception>
        private ExportedObject MapDataObject(object obj, Dictionary<object, ExportedObject> exportedObjects)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("obj", "The object to map was null.");
            }

            Type type = obj.GetType();
            ExportedType typeMapping = GetMappingForType(type.AssemblyQualifiedName, false);
            if (typeMapping == null)
            {
                log.Error("Export mapping was not found for type {0}.", type.FullName);
                throw new BusinessException(ErrorCodes.InternalError, "MapDataObject - Data object mapping failed.");
            }

            // Search for an export of obj in the already exported objects. If a corresponding export is found return it in order to preserve
            // circular references and not duplicate the object.
            ExportedObject foundExport;
            if (exportedObjects.TryGetValue(obj, out foundExport) && foundExport != null)
            {
                return foundExport;
            }

            // Validate the data object, if the data validator is defined.
            if (this.dataValidator != null
                && !this.dataValidator.ValidateDataObject(obj))
            {
                return null;
            }

            // Export each mapped value of the object.
            ExportedObject export = new ExportedObject();
            export.ExportedTypeName = typeMapping.Name;

            foreach (Value valueMapping in typeMapping.Values)
            {
                object valueObj = null;
                DataType dataType;

                // Get the value of the current mapped member of the object based on how it is publicly exposed.
                if (valueMapping.ExposedAs == MemberExposureType.Field)
                {
                    FieldInfo fieldInfo = type.GetField(valueMapping.Name, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                    if (fieldInfo != null)
                    {
                        valueObj = fieldInfo.GetValue(obj);
                        dataType = DataType.Field;
                    }
                    else
                    {
                        log.Warn("The field '{0}' of the type mapping '{1}' was not found in the source object '{2}'.", valueMapping.Name, typeMapping.Name, type.FullName);
                        continue;
                    }
                }
                else if (valueMapping.ExposedAs == MemberExposureType.Property)
                {
                    PropertyInfo propInfo = type.GetProperty(valueMapping.Name, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                    if (propInfo != null && propInfo.CanRead)
                    {
                        valueObj = propInfo.GetValue(obj, null);
                        dataType = DataType.Property;
                    }
                    else
                    {
                        log.Warn("The property '{0}' of the type mapping '{1}' was not found in the source object '{2}' or it can't be read.", valueMapping.Name, typeMapping.Name, type.FullName);
                        continue;
                    }
                }
                else
                {
                    log.Warn("The member exposure type '{0}' is not implemented for Value. The member '{1}' of the maping '{2}' was ignored.", Enum.GetName(typeof(MemberExposureType), valueMapping.ExposedAs), valueMapping.Name, typeMapping.Name);
                    continue;
                }

                // Export the value obtained above. If is null then the member is not exported at all (null is the default value for references so when importing it will already be assigned to each reference).
                if (valueObj != null)
                {
                    object mappedValue;
                    if (this.MapValueForExport(valueObj, exportedObjects, out mappedValue))
                    {
                        if (mappedValue != null)
                        {
                            DataPiece data = new DataPiece();
                            data.Type = dataType;
                            data.Key = valueMapping.Name;
                            data.Value = mappedValue;
                            export.Data.Add(data);
                        }
                    }
                    else
                    {
                        log.Warn("Failed to map the value of the '{0}' field/property belonging to the '{1}' mapped type.", valueMapping.Name, typeMapping.Name);
                    }
                }
            }

            // Export each mapped collection.
            foreach (Collection collectionMapping in typeMapping.Collections)
            {
                object collectionValue = null;

                // Get a reference to the mapped collection based on how it is publicly exposed.
                if (collectionMapping.ExposedAs == MemberExposureType.Field)
                {
                    FieldInfo fieldInfo = type.GetField(collectionMapping.Name, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                    if (fieldInfo != null)
                    {
                        collectionValue = fieldInfo.GetValue(obj);
                    }
                    else
                    {
                        log.Warn(
                            "The collection '{0}' (exposed by a field) of the type mapping '{1}' was not found in the source object '{2}'. The collection was ignored.",
                            collectionMapping.Name,
                            typeMapping.Name,
                            type.FullName);
                        continue;
                    }
                }
                else if (collectionMapping.ExposedAs == MemberExposureType.Property)
                {
                    PropertyInfo propInfo = type.GetProperty(collectionMapping.Name, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                    if (propInfo != null && propInfo.CanRead)
                    {
                        collectionValue = propInfo.GetValue(obj, null);
                    }
                    else
                    {
                        log.Warn("The collection '{0}' (exposed by a property) of the type mapping '{1}' was not found in the source object '{2}' or it can't be read. The property was ignored.", collectionMapping.Name, typeMapping.Name, type.FullName);
                        continue;
                    }
                }
                else
                {
                    log.Warn("The member exposure type '{0}' is not implemented for Collection. Member '{1}' of the mapping '{2}' was ignored.", Enum.GetName(typeof(MemberExposureType), collectionMapping.ExposedAs), collectionMapping.Name, typeMapping.Name);
                    continue;
                }

                // Map and export all objects in the collection.
                var collection = collectionValue as IEnumerable;
                if (collection != null)
                {
                    List<object> mappedCollection = new List<object>();
                    bool abort = false;

                    if (collection is IEnumerable<ZPKTool.Data.Media>)
                    {
                        // Media objects are no longer exported with the entity graph; they are exported separately.
                        abort = true;
                    }
                    else
                    {
                        foreach (object elem in collection)
                        {
                            // Map each object in the collection
                            if (elem != null)
                            {
                                object mappedElem;
                                if (MapValueForExport(elem, exportedObjects, out mappedElem))
                                {
                                    if (mappedElem != null)
                                    {
                                        mappedCollection.Add(mappedElem);
                                    }
                                }
                                else
                                {
                                    log.Warn("Failed to map an element of the collection '{0}'.'{1}'.", typeMapping.Name, collectionMapping.Name);
                                    abort = true;
                                    break;
                                }
                            }
                        }
                    }

                    if (!abort)
                    {
                        // Export the list of mapped objects.
                        DataPiece data = new DataPiece();
                        data.Key = collectionMapping.Name;
                        data.Type = DataType.Collection;
                        data.Value = mappedCollection;
                        export.Data.Add(data);
                    }
                }
                else
                {
                    log.Warn("The value of the '{0}'.'{1}' collection was IEnumerable and it was ignored.", typeMapping.Name, collectionMapping.Name);
                }
            }

            exportedObjects.Add(obj, export);
            return export;
        }

        /// <summary>
        /// Maps an ExportedObject to the data object it represents.
        /// </summary>
        /// <param name="obj">The exported object.</param>
        /// <param name="importedObjects">The objects already imported.  It is used to resolve circular dependencies so that an object is not
        /// imported more than once because is being referenced by multiple objects.</param>
        /// <returns>The data object.</returns>
        private object MapExportedObject(ExportedObject obj, Dictionary<ExportedObject, object> importedObjects)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("obj", "The exported object was null.");
            }

            ExportedType typeMapping = GetMappingForType(obj.ExportedTypeName, true);
            if (typeMapping == null)
            {
                log.Warn("Export mapping was not found for type {0}.", obj.ExportedTypeName);
                throw new BusinessException(ErrorCodes.InternalError);
            }

            Type type = Type.GetType(typeMapping.Name, false, true);
            if (type == null)
            {
                log.Warn("Failed to get the type {0}.", typeMapping.Name);
                throw new BusinessException(ErrorCodes.InternalError);
            }

            // Check if the object represented by the exported object has already been imported.
            // If a corresponding import is found return it in order to preserve circular references and not duplicate the already exported object.
            object foundImportedObject;
            if (importedObjects.TryGetValue(obj, out foundImportedObject) && foundImportedObject != null)
            {
                return foundImportedObject;
            }

            // Validate the exported object, if the data validator is defined.
            if (this.dataValidator != null
                && !this.dataValidator.ValidateExportedObject(obj, type))
            {
                return null;
            }

            try
            {
                object dataObject = Activator.CreateInstance(type);

                IExportedValueConverter converter = null;
                if (typeMapping.Converter != null)
                {
                    Type t = Type.GetType(typeMapping.Converter);
                    converter = (IExportedValueConverter)Activator.CreateInstance(t);
                }

                // Import each piece of exported data
                foreach (DataPiece data in obj.Data)
                {
                    if (data.Type == DataType.Field || data.Type == DataType.Property)
                    {
                        // Retrieve the value mapping corresponding to the member name stored in the data piece's key.                        
                        Value valueMember = ResolveValueMappingByName(data.Key, typeMapping);
                        if (valueMember == null)
                        {
                            // The field/property corresponding to this piece of data is no longer mapped for export so we just ignore it.
                            continue;
                        }

                        // Map the value from the data piece, if it is an exported object; otherwise it represents a type that was exported
                        // without needing to be mapped.
                        object value = null;
                        if (data.Value is ExportedObject)
                        {
                            value = MapExportedObject((ExportedObject)data.Value, importedObjects);
                        }
                        else
                        {
                            value = data.Value;
                        }

                        // If the value mapping has a converter attached to it then use it to convert the data value to an object currently
                        // accepted by the imported object's member referred by the value mapping.
                        if (converter != null)
                        {
                            // Get the type of the member from the value mapping
                            Type targetType = null;
                            try
                            {
                                targetType = GetType(valueMember.Type);
                            }
                            catch
                            {
                                log.Error(
                                    "Failed to get the type specified by the value mapping '{0}' belonging to the type mapping '{1}'.",
                                    valueMember.Name,
                                    typeMapping.Name);
                            }

                            if (targetType != null)
                            {
                                try
                                {
                                    value = converter.Convert(valueMember.Name, value, targetType);
                                }
                                catch (Exception e)
                                {
                                    string msg = string.Format(
                                        "Failed to convert the value '{0} ({1})' for the mapping '{2} - {3}'.",
                                        value,
                                        value.GetType().FullName,
                                        valueMember.Name,
                                        typeMapping.Name);
                                    log.ErrorException(msg, e);
                                }
                            }
                        }

                        // Set the imported value in the appropriate field or property of the output object
                        if (data.Type == DataType.Field)
                        {
                            FieldInfo fieldInfo = type.GetField(valueMember.Name, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                            if (fieldInfo != null)
                            {
                                fieldInfo.SetValue(dataObject, value);
                            }
                            else
                            {
                                log.Warn("The field '{0}' was not found in the data type '{1}'. The field was ignored.", valueMember.Name, type.FullName);
                                continue;
                            }
                        }
                        else if (data.Type == DataType.Property)
                        {
                            PropertyInfo propInfo = type.GetProperty(valueMember.Name, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                            if (propInfo != null && propInfo.CanWrite)
                            {
                                // If the property is a nullable enum we can't dirrectly set its value so we need to get the value as enum before setting it.
                                if (propInfo.PropertyType.IsGenericType
                                    && Nullable.GetUnderlyingType(propInfo.PropertyType) != null
                                    && Nullable.GetUnderlyingType(propInfo.PropertyType).IsEnum)
                                {
                                    var enumType = Nullable.GetUnderlyingType(propInfo.PropertyType);
                                    var enumValue = Enum.ToObject(enumType, value);
                                    propInfo.SetValue(dataObject, value == null ? null : enumValue, null);
                                }
                                else
                                {
                                    propInfo.SetValue(dataObject, value, null);
                                }
                            }
                            else
                            {
                                log.Warn("The property '{0}' was not found in the data type '{1}' or it can't be written; it was ignored.", valueMember.Name, type.FullName);
                                continue;
                            }
                        }
                    }
                    else if (data.Type == DataType.Collection)
                    {
                        Collection collectionField = ResolveCollectionMappingByName(data.Key, typeMapping);
                        if (collectionField == null)
                        {
                            // The collection corresponding to this piece of data is no longer mapped for export so we just ignore it.                            
                            continue;
                        }

                        // Get a reference to the field/property exposing the collection
                        object collection = null;
                        Type collectionMemberType = null;
                        if (collectionField.ExposedAs == MemberExposureType.Field)
                        {
                            FieldInfo fieldInfo = type.GetField(collectionField.Name, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                            if (fieldInfo != null)
                            {
                                collection = fieldInfo.GetValue(dataObject);
                                collectionMemberType = fieldInfo.FieldType;
                            }
                            else
                            {
                                log.Warn("The property or field '{0}' was not found in the data type '{1}'.", collectionField.Name, type.FullName);
                                continue;
                            }
                        }
                        else if (collectionField.ExposedAs == MemberExposureType.Property)
                        {
                            PropertyInfo propInfo = type.GetProperty(collectionField.Name, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                            if (propInfo != null && propInfo.CanRead)
                            {
                                collection = propInfo.GetValue(dataObject, null);
                                collectionMemberType = propInfo.PropertyType;
                            }
                            else
                            {
                                log.Warn("The property '{0}' was not found in the data type '{1}' or it can't be read.", collectionField.Name, type.FullName);
                                continue;
                            }
                        }
                        else
                        {
                            log.Error("The MemberExposureType '{0}' is not handled for Collections.", collectionField.ExposedAs);
                            continue;
                        }

                        if (collection == null)
                        {
                            log.Warn("The collection '{0}' of the data type '{1}' was not initialized.", collectionField.Name, type.FullName);
                            continue;
                        }
                        else if (collection.GetType().GetInterfaces().Any(x => x.IsGenericType && x.GetGenericTypeDefinition() == typeof(ICollection<>)))
                        {
                            // Map each object contained in the imported data and add it to the collection.
                            foreach (object dataVal in (IEnumerable)data.Value)
                            {
                                object val = null;
                                if (dataVal is ExportedObject)
                                {
                                    val = MapExportedObject((ExportedObject)dataVal, importedObjects);
                                }
                                else
                                {
                                    val = dataVal;
                                }

                                if (val != null)
                                {
                                    collectionMemberType.InvokeMember(
                                        "Add",
                                        BindingFlags.InvokeMethod | BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public,
                                        null,
                                        collection,
                                        new object[] { val });
                                }
                            }
                        }
                        else
                        {
                            log.Warn("The collection '{0}' of the data type '{1}' was not ICollection.", collectionField.Name, type.FullName);
                            continue;
                        }
                    }
                    else
                    {
                        log.Warn("MapExportedObject - DataType '{0}' not handled.", Enum.GetName(typeof(DataType), data.Type));
                    }
                }

                importedObjects.Add(obj, dataObject);

                return dataObject;
            }
            catch (BusinessException)
            {
                throw;
            }
            catch (Exception e)
            {
                log.ErrorException("Unknown error occurred while mapping an exported object.", e);
                throw new BusinessException(ErrorCodes.InternalError, e);
            }
        }

        /// <summary>
        /// Gets the mapping for a specified data type.
        /// Optionally, you can specify if the typeName should also be searched in the aliases of the types.
        /// </summary>
        /// <param name="assemblyQualifiedTypeName">The assembly qualified type name.</param>
        /// <param name="checkAliases">if set to true also check the specified typeName as an alias.</param>
        /// <returns>
        /// A type mapping or null if none was found.
        /// </returns>
        private ExportedType GetMappingForType(string assemblyQualifiedTypeName, bool checkAliases)
        {
            string[] parts = assemblyQualifiedTypeName.Split(',');
            string typeName = parts[0].Trim();
            string assemblyName = string.Empty;
            if (parts.Length > 1)
            {
                assemblyName = parts[1].Trim();
            }

            foreach (ExportedType typeMapping in this.typeMappings)
            {
                string[] pcs = typeMapping.Name.Split(',');
                string mappingTypeName = pcs[0].Trim();
                string mappingTypeAssembly = string.Empty;
                if (pcs.Length > 1)
                {
                    mappingTypeAssembly = pcs[1].Trim();
                }

                if (typeName == mappingTypeName && assemblyName == mappingTypeAssembly)
                {
                    return typeMapping;
                }
                else if (checkAliases)
                {
                    Alias alias = typeMapping.Aliases.Where(a => a.Type == typeName).FirstOrDefault();
                    if (alias != null)
                    {
                        return typeMapping;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Maps a value for export.
        /// </summary>
        /// <param name="value">The value to map.</param>
        /// <param name="exportedObjects">The exported objects. used by the call to MapDataObject().</param>
        /// <param name="mappedValue">The mapped value.</param>
        /// <returns>
        /// True if the mapping was successful, false otherwise.
        /// </returns>
        /// <exception cref="BusinessException">value was null</exception>
        private bool MapValueForExport(object value, Dictionary<object, ExportedObject> exportedObjects, out object mappedValue)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value", "value was null");
            }

            bool success = false;
            Type type = value.GetType();
            if (type.IsArray)
            {
                type = type.GetElementType();
            }
            else if (type.IsGenericType && type.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                type = Nullable.GetUnderlyingType(type);
            }

            TypeCode code = Type.GetTypeCode(type);
            if (code != TypeCode.Object || type == typeof(Guid))
            {
                // The value is a type that does not need mapping (usually is the case of System types)
                mappedValue = value;
                success = true;
            }
            else
            {
                // Try to map the value as a data object.
                try
                {
                    mappedValue = MapDataObject(value, exportedObjects);
                    success = true;
                }
                catch
                {
                    mappedValue = null;
                    success = false;
                }
            }

            return success;
        }

        /// <summary>
        /// Gets the Value with the specified name belonging to a specified type mapping.
        /// If the mapping does not contain a Value object with the specified name, the name is also checked in the aliases of all Values from the mapping..
        /// </summary>
        /// <param name="name">The name of the value.</param>
        /// <param name="typeMapping">The type mapping.</param>
        /// <returns>
        /// The Value with the specified name or containing an alias with the specified name.
        /// </returns>
        private Value ResolveValueMappingByName(string name, ExportedType typeMapping)
        {
            Value value = typeMapping.Values.Where(v => v.Name.Equals(name, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
            if (value == null)
            {
                foreach (Value val in typeMapping.Values)
                {
                    Alias alias = val.Aliases.Where(a => a.Name.Equals(name, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                    if (alias != null)
                    {
                        value = val;
                        break;
                    }
                }
            }

            return value;
        }

        /// <summary>
        /// Gets the Collection with a specified name belonging to a specified type mapping.
        /// If the mapping does not contain a Collection mapping with the specified name, the name is also checked as an alias.
        /// </summary>
        /// <param name="name">The name of the collection.</param>
        /// <param name="typeMapping">The type mapping.</param>
        /// <returns>The Collection with the specified name or alias.</returns>
        private Collection ResolveCollectionMappingByName(string name, ExportedType typeMapping)
        {
            Collection val = typeMapping.Collections.Where(c => c.Name.Equals(name, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
            if (val == null)
            {
                val = typeMapping.Collections.Where(c => c.Aliases.Where(a => a.Name.Equals(name, StringComparison.OrdinalIgnoreCase)).FirstOrDefault() != null).FirstOrDefault();
            }

            return val;
        }

        /// <summary>
        /// Gets the type with the specified name. For the primitive types (int, decimal, string, etc.) it does not need them to be qualified.
        /// For other types it wraps Type.GetType() so the type names must be fully qualified for system types and assembly qualified for custom types.
        /// Throws exception if a type with the specified name was not found.
        /// </summary>
        /// <param name="typeName">Name of the type.</param>
        /// <returns>The type with the specified name.</returns>
        private Type GetType(string typeName)
        {
            if (typeName.ToLower() == "bool")
            {
                return typeof(bool);
            }
            else if (typeName.ToLower() == "char")
            {
                return typeof(char);
            }
            else if (typeName.ToLower() == "sbyte")
            {
                return typeof(sbyte);
            }
            else if (typeName.ToLower() == "byte")
            {
                return typeof(byte);
            }
            else if (typeName.ToLower() == "short")
            {
                return typeof(short);
            }
            else if (typeName.ToLower() == "ushort")
            {
                return typeof(ushort);
            }
            else if (typeName.ToLower() == "int")
            {
                return typeof(int);
            }
            else if (typeName.ToLower() == "uint")
            {
                return typeof(uint);
            }
            else if (typeName.ToLower() == "long")
            {
                return typeof(long);
            }
            else if (typeName.ToLower() == "ulong")
            {
                return typeof(ulong);
            }
            else if (typeName.ToLower() == "float")
            {
                return typeof(float);
            }
            else if (typeName.ToLower() == "double")
            {
                return typeof(double);
            }
            else if (typeName.ToLower() == "decimal")
            {
                return typeof(decimal);
            }
            else if (typeName.ToLower() == "string")
            {
                return typeof(string);
            }
            else if (typeName.ToLower() == "datetime")
            {
                return typeof(DateTime);
            }
            else if (typeName.ToLower() == "guid")
            {
                return typeof(Guid);
            }
            else
            {
                return Type.GetType(typeName, true);
            }
        }

        #region Mapping Data Load

        /// <summary>
        /// Reads the mappings from the xml file and stores them in memory.
        /// </summary>
        /// <exception cref="BusinessException">An error occurred while loading the mappings.</exception>
        private void InitializeMappings()
        {
            typeMappings.Clear();
            try
            {
                string xmlResource = "ZPKTool.Business.Export.ExportMappings.xml";
                using (Stream stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(xmlResource))
                {
                    XmlDocument mappingsDoc = new XmlDocument();
                    mappingsDoc.Load(stream);

                    foreach (XmlNode exportedTypeNode in mappingsDoc.DocumentElement.SelectNodes("ExportedTypes/ExportedType"))
                    {
                        ExportedType typeMapping = ReadExportedType(exportedTypeNode);
                        this.typeMappings.Add(typeMapping);
                    }
                }
            }
            catch (Exception e)
            {
                log.ErrorException("An error occurred while reading the mappings from the xml file.", e);
                throw new BusinessException(ErrorCodes.InternalError, e);
            }
        }

        /// <summary>
        /// Reads an ExportedType from an xml node.
        /// </summary>
        /// <param name="exportedTypeNode">The exported type node.</param>
        /// <returns>An instance of ExportedType.</returns>
        private ExportedType ReadExportedType(XmlNode exportedTypeNode)
        {
            ExportedType exportedType = new ExportedType();
            exportedType.Name = exportedTypeNode.Attributes["Name"].Value;

            if (exportedTypeNode.Attributes["Converter"] != null)
            {
                exportedType.Converter = exportedTypeNode.Attributes["Converter"].Value;
            }

            exportedType.Aliases.AddRange(ReadAliases(exportedTypeNode));

            foreach (XmlNode valueNode in exportedTypeNode.SelectNodes("Values/Value"))
            {
                Value value = ReadValue(valueNode);
                exportedType.Values.Add(value);
            }

            foreach (XmlNode collectionNode in exportedTypeNode.SelectNodes("Collections/Collection"))
            {
                Collection collection = ReadCollection(collectionNode);
                exportedType.Collections.Add(collection);
            }

            return exportedType;
        }

        /// <summary>
        /// Reads all Aliases from an xml node's Aliases sub-node.
        /// </summary>
        /// <param name="node">The node containing aliases.</param>
        /// <returns>A list of aliases.</returns>
        private List<Alias> ReadAliases(XmlNode node)
        {
            List<Alias> aliases = new List<Alias>();
            foreach (XmlNode aliasNode in node.SelectNodes("Aliases/Alias"))
            {
                Alias alias = new Alias();
                alias.Name = aliasNode.Attributes["Name"].Value;
                alias.Type = aliasNode.Attributes["Type"].Value;
                aliases.Add(alias);
            }

            return aliases;
        }

        /// <summary>
        /// Reads a field from an xml node.
        /// </summary>
        /// <param name="valueNode">The xml node defining a value.</param>
        /// <returns>A Value instance.</returns>
        private Value ReadValue(XmlNode valueNode)
        {
            Value value = new Value();
            value.Name = valueNode.Attributes["Name"].Value;
            value.Type = valueNode.Attributes["Type"].Value;
            value.ExposedAs = (MemberExposureType)Enum.Parse(typeof(MemberExposureType), valueNode.Attributes["ExposedAs"].Value, true);

            value.Aliases.AddRange(ReadAliases(valueNode));
            return value;
        }

        /// <summary>
        /// Reads a Collection from an xml node.
        /// </summary>
        /// <param name="collectionNode">The xml node containing the collection.</param>
        /// <returns>A Collection instance.</returns>
        private Collection ReadCollection(XmlNode collectionNode)
        {
            Collection collection = new Collection();
            collection.Name = collectionNode.Attributes["Name"].Value;
            collection.Type = collectionNode.Attributes["ItemsType"].Value;
            collection.ExposedAs = (MemberExposureType)Enum.Parse(typeof(MemberExposureType), collectionNode.Attributes["ExposedAs"].Value, true);
            collection.Aliases.AddRange(ReadAliases(collectionNode));
            return collection;
        }

        #endregion Mapping Data Load
    }
}
