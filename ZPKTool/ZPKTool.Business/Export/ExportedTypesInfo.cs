﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.Business.Export
{
    /// <summary>
    /// Contains the types of entity that are supported for export and some helper methods
    /// </summary>
    public static class ExportedTypesInfo
    {
        /// <summary>
        /// The collection containing the entities data for export (like entity type and the file extension to save on disc).
        /// </summary>
        private static readonly IEnumerable<ExportedData> exportData = new List<ExportedData>()
            {
                new ExportedData(typeof(Commodity), ".commodity"),
                new ExportedData(typeof(Consumable), ".consumable"),
                new ExportedData(typeof(Die), ".die"),
                new ExportedData(typeof(Machine), ".machine"),
                new ExportedData(typeof(RawMaterial), ".rawMaterial"),
                new ExportedData(typeof(Part), ".part"),
                new ExportedData(typeof(ProjectFolder), ".folder"),
                new ExportedData(typeof(Project), ".project"),
                new ExportedData(typeof(Assembly), ".assembly"),
                new ExportedData(typeof(RawPart), ".rawPart")
            };

        /// <summary>
        /// Checks if the provided entity type is supported to export.
        /// </summary>
        /// <param name="type">The type to check.</param>
        /// <returns>True if the entity type is supported to export, false otherwise.</returns>
        public static bool IsEntityTypeSupported(Type type)
        {
            return exportData.Any(item => item.EntityType == type);
        }

        /// <summary>
        /// Gets the entity type for the provided file extension from the export data collection.
        /// </summary>
        /// <param name="fileExtension">The file extension</param>
        /// <returns>The type for the file extension provided if existing any or null otherwise.</returns>
        public static Type GetTypeFromExtension(string fileExtension)
        {
            var exportedData = exportData.FirstOrDefault(item => item.FileExtension.Equals(fileExtension, StringComparison.InvariantCultureIgnoreCase));
            return exportedData != null ? exportedData.EntityType : null;
        }

        /// <summary>
        /// Gets the entity file extension for the provided entity type from the export data collection.
        /// </summary>
        /// <param name="type">The entity type</param>
        /// <returns>The file extension for the entity type provided if existing any or null otherwise.</returns>
        public static string GetExtensionFromType(Type type)
        {
            var exportedData = exportData.FirstOrDefault(item => item.EntityType == type);
            return exportedData != null ? exportedData.FileExtension : string.Empty;
        }

        /// <summary>
        /// Represents the export information for a type that can be exported (like entity type and the file extension to save on disc).
        /// </summary>
        private class ExportedData
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ExportedData"/> class.
            /// </summary>
            /// <param name="type">The type of the entity to export.</param>
            /// <param name="fileExtension">The file extension of the entity to export.</param>
            public ExportedData(Type type, string fileExtension)
            {
                this.EntityType = type;
                this.FileExtension = fileExtension;
            }

            /// <summary>
            /// Gets the type of the entity to export.
            /// </summary>
            public Type EntityType { get; private set; }

            /// <summary>
            /// Gets a value representing a file extension used for export data.
            /// </summary>
            public string FileExtension { get; private set; }
        }
    }
}
