﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.Business.Export
{
    /// <summary>
    /// Performs conversions during the import of a Supplier.
    /// </summary>
    public class SupplierConverter : IExportedValueConverter
    {
        /// <summary>
        /// Converts the specified value to the specified type.
        /// </summary>
        /// <param name="fieldName">Name of the field.</param>
        /// <param name="value">The value to convert.</param>
        /// <param name="targetType">The type to which to convert.</param>
        /// <returns>The converted value.</returns>
        public object Convert(string fieldName, object value, Type targetType)
        {
            if (fieldName == "Country" && value is Country)
            {
                Country country = (Country)value;
                return country.Name;
            }
            else if (fieldName == "State" && value is CountryState)
            {
                CountryState state = (CountryState)value;
                return state.Name;
            }
            else
            {
                return value;
            }
        }
    }
}
