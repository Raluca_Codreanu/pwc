﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.Export
{
    /// <summary>
    /// Represents the mapping for export of a data type's collection of sub-objects.
    /// </summary>
    public class Collection
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Collection"/> class.
        /// </summary>
        public Collection()
        {
            this.Aliases = new List<Alias>();
        }

        /// <summary>
        /// Gets or sets the name of the collection.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the type of the collection.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the method used to expose the collection.
        /// </summary>        
        public MemberExposureType ExposedAs { get; set; }

        /// <summary>
        /// Gets the aliases of the collection.
        /// </summary>        
        public ICollection<Alias> Aliases { get; private set; }
    }
}
