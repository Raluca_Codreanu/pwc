﻿namespace ZPKTool.Business.Export
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Contains an exported media object and some information about it necessary for the import operation.
    /// </summary>
    [Serializable]
    public class ExportedMedia
    {
        /// <summary>
        /// Gets or sets the exported media.
        /// </summary>
        public ExportedObject Media { get; set; }

        /// <summary>
        /// Gets or sets the unique id of the exported object to which this media belongs to.
        /// </summary>
        public Guid ParentId { get; set; }
    }
}
