﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.Export
{
    /// <summary>
    /// An object containing data to be serialized in binary format.
    /// </summary>
    [Serializable]
    public class ExportedObject
    {
        /// <summary>
        /// The data to be exported.
        /// </summary>
        private List<DataPiece> dataPieces = new List<DataPiece>();

        /// <summary>
        /// Initializes a new instance of the <see cref="ExportedObject"/> class.
        /// </summary>
        public ExportedObject()
        {
            this.Uid = Guid.NewGuid();
        }

        /// <summary>
        /// Gets or sets the type ob the object whose data is exported in this instance.
        /// </summary>        
        public string ExportedTypeName { get; set; }

        /// <summary>
        /// Gets the data to be exported.
        /// </summary>        
        public List<DataPiece> Data
        {
            get { return dataPieces; }
        }

        /// <summary>
        /// Gets or sets the unique identifier assigned to this instance during the export operation.                
        /// </summary>
        /// <remarks>
        /// Its purpose for now is to identify to which ExportedObject an ExportedMedia belongs to.
        /// </remarks>
        public Guid Uid { get; set; }
    }
}
