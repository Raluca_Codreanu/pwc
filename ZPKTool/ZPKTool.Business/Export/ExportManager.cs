﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Ionic.Zip;
using Ionic.Zlib;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;

namespace ZPKTool.Business.Export
{
    /// <summary>
    /// The export manager.
    /// </summary>
    public sealed class ExportManager
    {
        /// <summary>
        /// The current version of the format used to export data.
        /// <para/>
        /// This value should be increased only when the changes made to the export algorithm or serialization cause older versions of
        /// the app to be unable to import data in the current format.
        /// </summary>
        private const decimal FormatVersion = 1.3m;

        /// <summary>
        /// The maximum batch size used when exporting media, in bytes.
        /// </summary>
        private const int MaxMediaBatchSizeForExport = 5242880; // 5Mb

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The only instance of this class.
        /// </summary>
        private static readonly ExportManager instance = new ExportManager();

        /// <summary>
        /// The instance used for mapping of objects to and from the export object format.
        /// </summary>
        private ExportMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExportManager"/> class.
        /// </summary>
        public ExportManager()
        {
            this.mapper = new ExportMapper(new ExportDataValidator());
        }

        /// <summary>
        /// Gets the only instance of this class.
        /// </summary>
        public static ExportManager Instance
        {
            get { return instance; }
        }

        #region Export

        /// <summary>
        /// Exports the specified entity in a file specified by a path.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="dataSourceManager">The data manager associated with the entity.</param>
        /// <param name="filePath">The path to the file to export to (including the file name).</param>
        /// <param name="currencies">The currencies of the parent.</param>
        /// <param name="baseCurrency">The base currency of the parent.</param>
        public void Export(object entity, IDataSourceManager dataSourceManager, string filePath, IEnumerable<Currency> currencies, Currency baseCurrency)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "The entity was null.");
            }

            if (dataSourceManager == null)
            {
                throw new ArgumentNullException("dataSourceManager", "The data source manager was null.");
            }

            if (string.IsNullOrWhiteSpace(filePath))
            {
                throw new ArgumentException("The export file path was null or whitespace.", "filePath");
            }

            var identifableEntity = entity as INameable;
            if (identifableEntity != null)
            {
                LoggingManager.Log(LogEventType.ExportedData, identifableEntity.Name);
            }

            bool exportFileCreated = false;
            bool exportSucceeded = false;

            // The temporary folder where all data is kept before being added to the export zip file.
            string tempFolder = null;
            try
            {
                Dictionary<object, ExportedObject> exportMap;
                CompressionLevel zipCompression = CompressionLevel.Default;

                // Get the calculation related data that must be exported with the model.
                var costCalculationData = this.GetCostCalculationDataForExport(entity, dataSourceManager);

                // Create the export metadata.
                ExportMetadata metadata = new ExportMetadata();
                metadata.ExportVersion = ExportManager.FormatVersion;
                metadata.AppVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

                // Create the exported currencies data.
                ExportedCurrenciesData currenciesData = new ExportedCurrenciesData();
                ExportedObject exportedBaseCurrency = mapper.MapDataObject(baseCurrency);
                currenciesData.BaseCurrency = exportedBaseCurrency;

                foreach (var currency in currencies)
                {
                    ExportedObject exportedCurrency = mapper.MapDataObject(currency);
                    currenciesData.Currencies.Add(exportedCurrency);
                }

                // Export the entity without any media. The entity's graph without media is small so it can be all serialized in memory.                        
                ExportedObject exportedEntity = mapper.MapDataObject(entity, out exportMap);

                // Serialize and add to the export file the metadata, currencies data, cost calculation data and the object to export.
                Serializer serializer = new Serializer();

                tempFolder = PathUtils.CreateTemporaryFolder();
                var metadataFileName = Path.Combine(tempFolder, "Metadata.bin");
                using (var metadataStream = new FileStream(metadataFileName, FileMode.Create, FileAccess.ReadWrite))
                {
                    serializer.Serialize(metadata, metadataStream);

                    var modelDataFileName = Path.Combine(tempFolder, "ModelData.bin");
                    using (var exportedDataStream = new FileStream(modelDataFileName, FileMode.Create, FileAccess.ReadWrite))
                    {
                        serializer.Serialize(exportedEntity, exportedDataStream);

                        var currenciesDataFileName = Path.Combine(tempFolder, "CurrenciesData.bin");
                        using (var currenciesDataStream = new FileStream(currenciesDataFileName, FileMode.Create, FileAccess.ReadWrite))
                        {
                            serializer.Serialize(currenciesData, currenciesDataStream);

                            var calculationDataFileName = Path.Combine(tempFolder, "CalculationData.bin");
                            using (var calculationDataStream = new FileStream(calculationDataFileName, FileMode.Create, FileAccess.ReadWrite))
                            {
                                if (costCalculationData != null)
                                {
                                    serializer.Serialize(costCalculationData, calculationDataStream);
                                }

                                metadataStream.Position = 0L;
                                exportedDataStream.Position = 0L;
                                currenciesDataStream.Position = 0L;
                                calculationDataStream.Position = 0L;

                                using (ZipFile zip = new ZipFile())
                                {
                                    zip.CompressionLevel = zipCompression;
                                    zip.AlternateEncoding = Encoding.UTF8;
                                    zip.AlternateEncodingUsage = ZipOption.AsNecessary;

                                    zip.AddEntry("ModelData.bin", exportedDataStream);
                                    zip.AddEntry("CurrenciesData.bin", currenciesDataStream);
                                    zip.AddEntry("Metadata.bin", metadataStream);
                                    if (calculationDataStream.Length > 0)
                                    {
                                        zip.AddEntry("CalculationData.bin", calculationDataStream);
                                    }

                                    zip.Save(filePath);
                                    exportFileCreated = true;
                                }
                            }
                        }
                    }
                }

                // Export the media of the entity and all objects in its graph.
                // The loading strategy for media is not to load the media of each object separately, but to get the media metadata for
                // all objects and based on the media size from metadata to load a media batch of X Mb.
                using (IDataSourceManager mediaDataManager = DataAccessFactory.CreateDataSourceManager(dataSourceManager.DatabaseId))
                {
                    // Extract the objects with media from the exported objects map.
                    var exportedObjectsWithMedia = exportMap.Where(kv => kv.Key is IEntityWithMedia || kv.Key is IEntityWithMediaCollection).Select(kv => kv.Key);

                    const int ExportedObjectsMaxLimit = 25000;
                    var exportedObjectsWithMediaBatchesCount =
                        (int)Math.Ceiling((double)exportedObjectsWithMedia.Count() / ExportedObjectsMaxLimit);

                    var index = 0;
                    var exportedObjectsWithMediaBatches =
                        exportedObjectsWithMedia.GroupBy(obj => index++ % exportedObjectsWithMediaBatchesCount)
                            .Select(batch => batch.AsEnumerable());

                    var allMediaMetadata = new Collection<MediaMetaData>();
                    foreach (var exportedObjectsWithMediaBatch in exportedObjectsWithMediaBatches)
                    {
                        // Get the meta data of the media of all exported objects.
                        allMediaMetadata.AddRange(mediaDataManager.MediaRepository.GetMediaMetaData(exportedObjectsWithMediaBatch));
                    }

                    // Create media batches to be exported based on each media's byte size information from the metadata
                    // and export all media one batch at a time, to allow the memory consumed by one batch to be reclaimed.
                    var metadataBatches = this.CreateOrderedMediaBatches(allMediaMetadata);
                    if (metadataBatches.Any())
                    {
                        int mediaBatchesCount = 1;

                        // Create a temp folder where to export each media batch.
                        var tempMediaFolder = Path.Combine(tempFolder, "Media");
                        Directory.CreateDirectory(tempMediaFolder);

                        // Convert all exported objects to IIdentifiable to be able to search through them.
                        var identifiableExportedObjects = exportedObjectsWithMedia.Cast<IIdentifiable>();

                        // Export the media batches.
                        foreach (var batch in metadataBatches)
                        {
                            // Get all media in the current batch
                            var mediaBatch = mediaDataManager.MediaRepository.GetMedia(batch);
                            ExportedMediaBatch exportedBatch = new ExportedMediaBatch();

                            // Map for export and serialize each media in the batch.
                            foreach (var media in mediaBatch)
                            {
                                var mediaMetadata = batch.FirstOrDefault(m => m.Guid == media.Guid);
                                if (mediaMetadata != null)
                                {
                                    // Determine the id of the media's ExportedObject parent; the ExportedObject parent is the mapping of the media's entity parent.
                                    object parentObj = identifiableExportedObjects.FirstOrDefault(obj => obj.Guid == mediaMetadata.ParentId);
                                    if (parentObj == null)
                                    {
                                        log.Error("Failed to find the parent entity of a media in the list of exported objects.");
                                        continue;
                                    }

                                    ExportedObject exportedParentObject;
                                    if (exportMap.TryGetValue(parentObj, out exportedParentObject))
                                    {
                                        ExportedMedia mediaExport = new ExportedMedia();
                                        mediaExport.Media = mapper.MapDataObject(media);
                                        mediaExport.ParentId = exportedParentObject.Uid;

                                        exportedBatch.Media.Add(mediaExport);
                                    }
                                    else
                                    {
                                        log.Error("Failed to find the parent ExportedObject of a media in the list of exported objects.");
                                    }
                                }
                            }

                            // Serialize the media batch in one file, in the temporary media folder.
                            if (exportedBatch.Media.Count > 0)
                            {
                                string mediaBatchFilename = string.Format("Batch{0}.bin", mediaBatchesCount.ToString(CultureInfo.InvariantCulture));
                                using (FileStream mediaStream = new FileStream(Path.Combine(tempMediaFolder, mediaBatchFilename), FileMode.Create, FileAccess.Write))
                                {
                                    serializer.Serialize(exportedBatch, mediaStream);
                                }

                                mediaBatchesCount++;
                            }
                        }

                        // Add the exported media batch files in to the export zip file.
                        if (Directory.Exists(tempMediaFolder) && mediaBatchesCount > 1)
                        {
                            using (ZipFile zip = new ZipFile(filePath))
                            {
                                zip.CompressionLevel = zipCompression;
                                zip.AlternateEncoding = Encoding.UTF8;
                                zip.AlternateEncodingUsage = ZipOption.AsNecessary;

                                zip.AddDirectory(tempMediaFolder, "Media");
                                zip.Save();
                            }
                        }
                    }
                }

                exportSucceeded = true;
            }
            catch (BusinessException)
            {
                // Was probably thrown by an internal call.
                throw;
            }
            catch (DataAccessException)
            {
                // Was probably thrown by an internal call to load data from the database.
                throw;
            }
            catch (ZipException ex)
            {
                log.ErrorException("A zip related error occurred while creating the export file.", ex);
                throw new BusinessException(ErrorCodes.ExportFileError, ex);
            }
            catch (Exception ex)
            {
                var wrappedEx = PathUtils.ParseFileRelatedException(ex);
                if (wrappedEx is FileRelatedException)
                {
                    log.ErrorException("A file/folder related error occurred during export.", ex);
                    throw wrappedEx;
                }
                else
                {
                    log.ErrorException("An unknown error occurred during export.", ex);
                    throw new BusinessException(ErrorCodes.ExportFileError, ex);
                }
            }
            finally
            {
                // Delete the temporary folder where exported data was kept
                if (!string.IsNullOrEmpty(tempFolder) && Directory.Exists(tempFolder))
                {
                    try
                    {
                        Directory.Delete(tempFolder, true);
                    }
                    catch
                    {
                    }
                }

                // If the export failed but the export file was created, try to delete it.
                if (!exportSucceeded && exportFileCreated && File.Exists(filePath))
                {
                    try
                    {
                        File.Delete(filePath);
                    }
                    catch
                    {
                    }
                }
            }
        }

        /// <summary>
        /// Creates media batches for export.
        /// It maintains the media order in the batches (if we flatten the batches list the media should be in the same order
        /// as in <paramref name="mediaMetadata"/>). The media order is important so the pictures and documents do not appear in a different order after import.
        /// </summary>
        /// <param name="mediaMetadata">The media metadata to split into batches.</param>
        /// <returns>A List of media batches (each batch is also a list).</returns>
        private List<List<MediaMetaData>> CreateOrderedMediaBatches(IEnumerable<MediaMetaData> mediaMetadata)
        {
            var metaDataList = mediaMetadata.ToList();
            if (metaDataList.Count == 0)
            {
                return new List<List<MediaMetaData>>();
            }

            List<List<MediaMetaData>> batches = new List<List<MediaMetaData>>();
            List<MediaMetaData> currentBatch = new List<MediaMetaData>();
            batches.Add(currentBatch);
            int currentBatchSize = 0;

            // The media must be split into batches but it must maintain the order from the input list
            // (if we flatten the batches list the media should be in the same order as in the input list).
            do
            {
                var metadata = metaDataList[0];

                // All media whose size is equal to or larger than the batch size, or whose size information is missing, are each put in its own batch.
                if (!metadata.Size.HasValue || metadata.Size.Value >= ExportManager.MaxMediaBatchSizeForExport)
                {
                    batches.Add(new List<MediaMetaData>() { metadata });
                    metaDataList.Remove(metadata);
                    currentBatch = new List<MediaMetaData>();
                    batches.Add(currentBatch);
                    currentBatchSize = 0;
                }
                else if (metadata.Size.Value + currentBatchSize <= ExportManager.MaxMediaBatchSizeForExport)
                {
                    // Add the media to the current batch, if the batch size is less than the max batch size
                    currentBatch.Add(metadata);
                    currentBatchSize += metadata.Size.Value;
                    metaDataList.Remove(metadata);
                }
                else
                {
                    // Start a new batch because the current media does not fit in the current batch.
                    currentBatch = new List<MediaMetaData>();
                    batches.Add(currentBatch);
                    currentBatchSize = 0;
                }
            }
            while (metaDataList.Count > 0);

            batches.RemoveAll(batch => batch.Count == 0);

            return batches;
        }

        /// <summary>
        /// Gets the cost calculation data that must be exported with the entity in order to be able to calculate its cost in "viewer" mode.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="dataManager">The data manager used for data operations related to <paramref name="entity"/>.</param>
        /// <returns>The data or null if no data needs to be exported.</returns>
        private ExportedCalculationData GetCostCalculationDataForExport(object entity, IDataSourceManager dataManager)
        {
            ExportedCalculationData calculationData = null;

            // For Assembly and Part we have to export the Depreciation Period, Depreciation Rate(Interest Rate) and Logistic Cost Ratio from
            // their parent Project. If the Project's Logistic Cost Ratio is not set we take its value from the basic settings.
            var assembly = entity as Assembly;
            var part = entity as Part;
            if (assembly != null || part != null)
            {
                Project parentProject = dataManager.ProjectRepository.GetParentProject(entity, true);
                if (parentProject != null)
                {
                    decimal logisticCostRatio = 0m;
                    if (parentProject.LogisticCostRatio.HasValue)
                    {
                        logisticCostRatio = parentProject.LogisticCostRatio.Value;
                    }
                    else
                    {
                        BasicSetting basicSettings = LocalDataCache.GetBasicSettings();
                        if (basicSettings != null)
                        {
                            logisticCostRatio = basicSettings.LogisticCostRatio;
                        }
                    }

                    calculationData = new ExportedCalculationData();
                    calculationData.DepreciationPeriod = parentProject.DepreciationPeriod.GetValueOrDefault();
                    calculationData.DepreciationRate = parentProject.DepreciationRate.GetValueOrDefault();
                    calculationData.LogisticCostRatio = logisticCostRatio;
                    calculationData.ProjectCreateDate = CostCalculationHelper.GetProjectCreateDate(parentProject);

                    // Calculate the Yearly Production Quantity.                                        
                    decimal? yearlyProductionQuantity = null;
                    if (assembly != null)
                    {
                        var calculator = ZPKTool.Calculations.CostCalculation.CostCalculatorFactory.GetCalculator(assembly.CalculationVariant);
                        yearlyProductionQuantity = calculator.CalculateYearlyProductionQuantity(assembly);
                    }
                    else
                    {
                        var calculator = ZPKTool.Calculations.CostCalculation.CostCalculatorFactory.GetCalculator(part.CalculationVariant);
                        yearlyProductionQuantity = calculator.CalculateYearlyProductionQuantity(part);
                    }

                    if (yearlyProductionQuantity != -1m)
                    {
                        calculationData.YearlyProductionQuantity = yearlyProductionQuantity;
                    }
                }
            }

            return calculationData;
        }

        #endregion Export

        #region Import

        /// <summary>
        /// Imports an entity from a file into a parent entity using the parent's data context.
        /// The imported entity is associated with <paramref name="dataManager" /> after the import and the <paramref name="saveChanges" /> parameters
        /// determines whether the DataContext.SaveChanges() method is called.
        /// <para />
        /// This method does not throw any exception; if an exception was thrown internally it will be placed in the returned object's Error property.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity in the file.</typeparam>
        /// <param name="filePath">The path to the file to import from.</param>
        /// <param name="parentEntity">The object that will be the parent of the imported object. Can be null only for Project and ProjectFolder.</param>
        /// <param name="dataManager">The data manager to use for importing the entity will be imported. I must also be the same data manager with which <paramref name="objToImportInto" /> is associated.</param>
        /// <param name="saveChanges">If set to true, during import, data is saved in the database in batches in order to allow the memory used by the data to be reclaimed; otherwise all data is imported into memory.
        /// <para />
        /// Usually the value of this parameter should be true except for the case when the import is part of a larger data transaction/unit of work that is committed later.</param>
        /// <returns>
        /// An object containing the imported data.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">filePath;The file path to import from was null or empty.</exception>
        /// <exception cref="System.InvalidOperationException">Import is not supported for the type </exception>
        /// <exception cref="BusinessException"></exception>
        public ImportedData<TEntity> ImportEntity<TEntity>(string filePath, object parentEntity, IDataSourceManager dataManager, bool saveChanges)
        {
            return this.ImportEntity<TEntity>(filePath, parentEntity, dataManager, saveChanges, null);
        }

        /// <summary>
        /// Imports an entity from a file into a parent entity using the parent's data context.
        /// The imported entity is associated with <paramref name="dataManager" /> after the import and the <paramref name="saveChanges" /> parameters
        /// determines whether the DataContext.SaveChanges() method is called.
        /// <para />
        /// This method does not throw any exception; if an exception was thrown internally it will be placed in the returned object's Error property.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity in the file.</typeparam>
        /// <param name="filePath">The path to the file to import from.</param>
        /// <param name="parentEntity">The object that will be the parent of the imported object. Can be null only for Project and ProjectFolder.</param>
        /// <param name="dataManager">The data manager to use for importing the entity will be imported. I must also be the same data manager with which <paramref name="objToImportInto" /> is associated.</param>
        /// <param name="saveChanges">If set to true, during import, data is saved in the database in batches in order to allow the memory used by the data to be reclaimed; otherwise all data is imported into memory.</param>
        /// <param name="importConfirmAction">If has value this will show user a window that will contain the object that wants to import</param>
        /// <para />
        /// Usually the value of this parameter should be true except for the case when the import is part of a larger data transaction/unit of work that is committed later.
        /// <returns>
        /// An object containing the imported data.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">filePath;The file path to import from was null or empty.</exception>
        /// <exception cref="System.InvalidOperationException">Import is not supported for the type </exception>
        /// <exception cref="BusinessException"></exception>
        public ImportedData<TEntity> ImportEntity<TEntity>(string filePath, object parentEntity, IDataSourceManager dataManager, bool saveChanges, Func<object, bool> importConfirmAction)
        {
            try
            {
                if (string.IsNullOrEmpty(filePath))
                {
                    throw new ArgumentNullException("filePath", "The file path to import from was null or empty.");
                }

                if (dataManager == null)
                {
                    throw new ArgumentNullException("dataManager", "The data manager was null.");
                }

                // Check if the type TEntity is supported for import.
                Type entityType = typeof(TEntity);
                if (!ExportedTypesInfo.IsEntityTypeSupported(entityType))
                {
                    throw new InvalidOperationException("Import is not supported for the type " + entityType.FullName);
                }

                if (parentEntity == null && !this.CanImportWithoutParent(entityType))
                {
                    throw new InvalidOperationException("The entity type " + entityType.FullName + " can't be imported without specifying a parent entity.");
                }

                if (parentEntity != null && parentEntity.GetType() == typeof(Project) && entityType == typeof(RawPart))
                {
                    throw new InvalidOperationException("Cannot import a Raw Part into a Project.");
                }

                LoggingManager.Log(LogEventType.ImportedData, entityType.Name);

                // Determine the format version of the file to import from and invoke the appropriate import algorithm.                
                decimal formatVersion = 1.0m;

                if (ZipFile.IsZipFile(filePath))
                {
                    using (ZipFile zip = ZipFile.Read(filePath))
                    {
                        zip.AlternateEncoding = Encoding.UTF8;
                        zip.AlternateEncodingUsage = ZipOption.AsNecessary;

                        var metadataEntry = zip.FirstOrDefault(e => string.Compare(e.FileName, "metadata.bin", StringComparison.OrdinalIgnoreCase) == 0);
                        if (metadataEntry != null)
                        {
                            using (MemoryStream metadataStream = new MemoryStream())
                            {
                                metadataEntry.Extract(metadataStream);
                                metadataStream.Position = 0L;

                                Serializer serializer = new Serializer();
                                ExportMetadata metadata = (ExportMetadata)serializer.Deserialize(metadataStream);
                                formatVersion = metadata.ExportVersion;
                            }
                        }
                    }
                }

                if (formatVersion < 1.3m)
                {
                    var importedEntity = this.ImportEntity_v1_0<TEntity>(filePath, parentEntity, dataManager, saveChanges, importConfirmAction);
                    return new ImportedDataInternal<TEntity>() { Entity = importedEntity, SourceFile = filePath };
                }
                else if (formatVersion == 1.3m)
                {
                    return this.ImportEntity_v1_3<TEntity>(filePath, parentEntity, dataManager, saveChanges, importConfirmAction);
                }
                else
                {
                    log.Error("The imported file was created by a newer app version so its format is not supported.");
                    throw new BusinessException(ErrorCodes.ImportFileCreatedByNewerVersion);
                }
            }
            catch (Exception ex)
            {
                var error = this.LogAndWrapImportException(ex);
                return new ImportedDataInternal<TEntity>() { Error = error, SourceFile = filePath };
            }
        }

        /// <summary>
        /// Imports the model in memory and caches its media on disk.
        /// <para />
        /// This method does not throw any exception; if an exception was thrown internally it will be placed in the returned object's Error property.
        /// </summary>
        /// <remarks>
        /// This method was implemented for the Model Viewer feature but it my be reused provided its meaning and implementation is not changed.
        /// </remarks>
        /// <param name="modelFilePath">The path to the model file to import from.</param>
        /// <returns>An object containing the imported data.</returns>
        public ImportedData<object> ImportModelInMemory(string modelFilePath)
        {
            try
            {
                if (string.IsNullOrEmpty(modelFilePath))
                {
                    throw new ArgumentNullException("modelFilePath", "The file path to import from was null or empty.");
                }

                // Determine the format version of the file to import from and invoke the appropriate import algorithm.                
                decimal formatVersion = 1.0m;

                if (ZipFile.IsZipFile(modelFilePath))
                {
                    using (ZipFile zip = ZipFile.Read(modelFilePath))
                    {
                        zip.AlternateEncoding = Encoding.UTF8;
                        zip.AlternateEncodingUsage = ZipOption.AsNecessary;

                        var metadataEntry = zip.FirstOrDefault(e => string.Compare(e.FileName, "metadata.bin", StringComparison.OrdinalIgnoreCase) == 0);
                        if (metadataEntry != null)
                        {
                            using (MemoryStream metadataStream = new MemoryStream())
                            {
                                metadataEntry.Extract(metadataStream);
                                metadataStream.Position = 0L;

                                Serializer serializer = new Serializer();
                                ExportMetadata metadata = (ExportMetadata)serializer.Deserialize(metadataStream);
                                formatVersion = metadata.ExportVersion;
                            }
                        }
                    }
                }

                if (formatVersion < 1.3m)
                {
                    return this.ImportModelInMemory_v1_0(modelFilePath);
                }
                else if (formatVersion == 1.3m)
                {
                    return this.ImportModelInMemory_v1_3(modelFilePath);
                }
                else
                {
                    log.Error("The imported file was created by a newer app version so its format is not supported.");
                    throw new BusinessException(ErrorCodes.ImportFileCreatedByNewerVersion);
                }
            }
            catch (Exception ex)
            {
                var error = this.LogAndWrapImportException(ex);
                return new ImportedDataInternal<object>() { Error = error, SourceFile = modelFilePath };
            }
        }

        /// <summary>
        /// Checks if an entity type can be imported without being associated with a parent.
        /// </summary>
        /// <param name="entityType">Type of the entity to check.</param>
        /// <returns>True if it can, false if not.</returns>
        private bool CanImportWithoutParent(Type entityType)
        {
            if (entityType == typeof(ProjectFolder) || entityType == typeof(Project))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the error code that means the file did not contain the right entity.
        /// </summary>
        /// <param name="entityType">Type of the entity for which to get the error code.</param>
        /// <returns>A string containing an error code.</returns>
        private string GetErrorCodeForInvalidEntityInFile(Type entityType)
        {
            if (entityType == typeof(Commodity))
            {
                return ErrorCodes.ImportCommodityInvalidFileContent;
            }

            if (entityType == typeof(Consumable))
            {
                return ErrorCodes.ImportConsumableInvalidFileContent;
            }

            if (entityType == typeof(Die))
            {
                return ErrorCodes.ImportDieInvalidFileContent;
            }

            if (entityType == typeof(Machine))
            {
                return ErrorCodes.ImportMachineInvalidFileContent;
            }

            if (entityType == typeof(RawMaterial))
            {
                return ErrorCodes.ImportRawMaterialInvalidFileContent;
            }

            if (entityType == typeof(Part))
            {
                return ErrorCodes.ImportPartInvalidFileContent;
            }

            if (entityType == typeof(Project))
            {
                return ErrorCodes.ImportProjectInvalidFileContent;
            }

            if (entityType == typeof(ProjectFolder))
            {
                return ErrorCodes.ImportProjectFolderInvalidFileContent;
            }

            if (entityType == typeof(Assembly))
            {
                return ErrorCodes.ImportAssemblyInvalidFileContent;
            }

            return ErrorCodes.InternalError;
        }

        /// <summary>
        /// Logs and wraps in an Export/Import related exception the specified exception that occurred during the import operation.
        /// </summary>
        /// <param name="ex">The ex.</param>
        /// <returns>The wrapped exception or the original, if wrapping was not appropriate.</returns>
        private Exception LogAndWrapImportException(Exception ex)
        {
            Exception outputEx = null;

            if (ex is BusinessException)
            {
                // Was probably thrown by an internal call so don't wrap it in another business exception.
                outputEx = ex;
            }
            else if (ex is ZipException)
            {
                log.ErrorException("Failed to unzip the import file.", ex);
                outputEx = new BusinessException(ErrorCodes.ImportFileInvalidContent, ex);
            }
            else if (ex is SerializationException)
            {
                log.ErrorException("Import serialization error.", ex);
                outputEx = new BusinessException(ErrorCodes.ImportFileInvalidContent, ex);
            }
            else
            {
                var newEx = PathUtils.ParseFileRelatedException(ex);
                if (newEx is FileRelatedException)
                {
                    log.ErrorException("Import file related error.", ex);
                    outputEx = newEx;
                }
                else if (ex is DataAccessException)
                {
                    // Don't wrap data errors in internal error as they usually contain appropriate error codes.
                    log.Error("Import file database access error.");
                    outputEx = ex;
                }
                else
                {
                    log.ErrorException("Import unknown failure.", ex);
                    outputEx = new BusinessException(ErrorCodes.InternalError, ex);
                }
            }

            return outputEx;
        }

        /// <summary>
        /// Fixes the entity's country settings by setting their last change timestamp. This is done by comparing them with the master data country settings.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="databaseId">The database id.</param>
        private void FixCountrySettingsTimestamp(object entity, DbIdentifier databaseId)
        {
            var dataManager = DataAccessFactory.CreateDataSourceManager(databaseId);
            var countries = dataManager.CountryRepository.GetAll();
            this.FixCountrySettingsTimestamp(entity, countries);
        }

        /// <summary>
        /// Fixes the entity's name by setting a default value if the name is null. This is done by checking the type of entity and give it as a default name. 
        /// </summary>
        /// <param name="entity">The entity.</param>
        public void FixEntityName(object entity)
        {
            if (entity == null)
            {
                return;
            }

            var projectFolder = entity as ProjectFolder;
            if (projectFolder != null)
            {
                if (projectFolder.Name == null)
                {
                    projectFolder.Name = typeof(ProjectFolder).Name;
                }

                foreach (var subFolder in projectFolder.ChildrenProjectFolders)
                {
                    this.FixEntityName(subFolder);
                }

                foreach (var projects in projectFolder.Projects)
                {
                    this.FixEntityName(projects);
                }
            }

            var project = entity as Project;
            if (project != null)
            {
                if (project.Name == null)
                {
                    project.Name = typeof(Project).Name;
                }

                foreach (var projectAssembly in project.Assemblies)
                {
                    this.FixEntityName(projectAssembly);
                }

                foreach (var projectPart in project.Parts)
                {
                    this.FixEntityName(projectPart);
                }
            }

            var assembly = entity as Assembly;
            if (assembly != null)
            {
                if (assembly.Name == null)
                {
                    assembly.Name = typeof(Assembly).Name;
                }

                foreach (var subAssembly in assembly.Subassemblies)
                {
                    this.FixEntityName(subAssembly);
                }

                foreach (var assemblyPart in assembly.Parts)
                {
                    this.FixEntityName(assemblyPart);
                }

                if (assembly.Process != null)
                {
                    foreach (var step in assembly.Process.Steps)
                    {
                        this.FixEntityName(step);
                    }
                }
            }

            var part = entity as Part;
            if (part != null)
            {
                if (part.Name == null)
                {
                    part.Name = typeof(Part).Name;
                }

                foreach (var partCommodity in part.Commodities)
                {
                    this.FixEntityName(partCommodity);
                }

                foreach (var partRawMaterial in part.RawMaterials)
                {
                    this.FixEntityName(partRawMaterial);
                }

                this.FixEntityName(part.RawPart);

                if (part.Process != null)
                {
                    foreach (var step in part.Process.Steps)
                    {
                        this.FixEntityName(step);
                    }
                }
            }

            var processStep = entity as ProcessStep;
            if (processStep != null)
            {
                if (processStep.Name == null)
                {
                    processStep.Name = typeof(ProcessStep).Name;
                }

                foreach (var stepCommodity in processStep.Commodities)
                {
                    this.FixEntityName(stepCommodity);
                }

                foreach (var stepConsumables in processStep.Consumables)
                {
                    this.FixEntityName(stepConsumables);
                }

                foreach (var stepDie in processStep.Dies)
                {
                    this.FixEntityName(stepDie);
                }

                foreach (var stepMachine in processStep.Machines)
                {
                    this.FixEntityName(stepMachine);
                }
            }

            var rawMaterial = entity as RawMaterial;
            if (rawMaterial != null)
            {
                if (rawMaterial.Name == null)
                {
                    rawMaterial.Name = typeof(RawMaterial).Name;
                }
            }

            var commodity = entity as Commodity;
            if (commodity != null)
            {
                if (commodity.Name == null)
                {
                    commodity.Name = typeof(Commodity).Name;
                }
            }

            var consumable = entity as Consumable;
            if (consumable != null)
            {
                if (consumable.Name == null)
                {
                    consumable.Name = typeof(Consumable).Name;
                }
            }

            var machine = entity as Machine;
            if (machine != null)
            {
                if (machine.Name == null)
                {
                    machine.Name = typeof(Machine).Name;
                }
            }

            var die = entity as Die;
            if (die != null)
            {
                if (die.Name == null)
                {
                    die.Name = typeof(Die).Name;
                }
            }
        }

        /// <summary>
        /// Fixes the entity's country settings by setting their last change timestamp. This is done by comparing them with the master data country settings.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="countries">The countries.</param>
        /// <exception cref="System.ArgumentNullException">dataManager;The data manager was null.</exception>
        private void FixCountrySettingsTimestamp(object entity, Collection<Country> countries)
        {
            if (entity == null)
            {
                return;
            }

            var folder = entity as ProjectFolder;
            if (folder != null)
            {
                foreach (var subFolder in folder.ChildrenProjectFolders)
                {
                    this.FixCountrySettingsTimestamp(subFolder, countries);
                }

                foreach (var proj in folder.Projects)
                {
                    this.FixCountrySettingsTimestamp(proj, countries);
                }
            }

            var project = entity as Project;
            if (project != null)
            {
                foreach (var projectAssembly in project.Assemblies)
                {
                    this.FixCountrySettingsTimestamp(projectAssembly, countries);
                }

                foreach (var projectPart in project.Parts)
                {
                    this.FixCountrySettingsTimestamp(projectPart, countries);
                }
            }

            var assembly = entity as Assembly;
            if (assembly != null)
            {
                this.SetCountrySettingTimestamp(assembly.CountrySettings, assembly.AssemblingCountryId, assembly.AssemblingCountry, assembly.AssemblingSupplier, countries);

                foreach (var subAssy in assembly.Subassemblies)
                {
                    this.FixCountrySettingsTimestamp(subAssy, countries);
                }

                foreach (var subPart in assembly.Parts)
                {
                    this.FixCountrySettingsTimestamp(subPart, countries);
                }
            }

            var part = entity as Part;
            if (part != null)
            {
                this.SetCountrySettingTimestamp(part.CountrySettings, part.ManufacturingCountryId, part.ManufacturingCountry, part.ManufacturingSupplier, countries);

                this.FixCountrySettingsTimestamp(part.RawPart, countries);
            }
        }

        /// <summary>
        /// Sets the country setting timestamp.
        /// </summary>
        /// <param name="setting">The country setting.</param>
        /// <param name="countryId">The country setting parent country Id.</param>
        /// <param name="countryName">The country setting parent country name.</param>
        /// <param name="supplierName">The country setting parent supplier name.</param>
        /// <param name="countries">The countries.</param>
        /// <exception cref="System.ArgumentNullException">dataManager;The data manager was null.</exception>
        /// <remarks>
        /// The last change timestamp is determined by comparing the values of the current and master data country settings.
        /// If they are equal the timestamp of the master data country is used, else and older timestamp is set.
        /// If the country setting has no master data country setting associated the current timestamp is used.
        /// </remarks>
        private void SetCountrySettingTimestamp(CountrySetting setting, Guid? countryId, string countryName, string supplierName, Collection<Country> countries)
        {
            if (setting == null
                || (!countryId.HasValue && string.IsNullOrWhiteSpace(countryName)))
            {
                return;
            }

            // Try to get the country by Id; if this is null
            // try to get it by name (case-insensitive); if this is null
            // try to get it by name combined with "Country" (case-insensitive); the search string will be "<country-name> Country" (ex: "China Country").
            CountrySetting masterDataCountrySetting = null;
            var parentCountryId = countryId ?? Guid.Empty;
            var country = countries.FirstOrDefault(c => c.Guid == parentCountryId || c.Name.Trim() == countryName.Trim() || c.Name.Trim() == countryName.Trim() + " country");
            if (country != null)
            {
                masterDataCountrySetting = country.CountrySetting;
                if (!string.IsNullOrWhiteSpace(supplierName))
                {
                    var supplier = country.States.FirstOrDefault(s => s.Name.Trim() == supplierName.Trim());
                    if (supplier != null
                        && supplier.CountrySettings != null)
                    {
                        masterDataCountrySetting = supplier.CountrySettings;
                    }
                }
            }

            if (masterDataCountrySetting != null)
            {
                // If the compared settings have the same values set the timestamp from the master data country setting.
                // Else set an older timestamp.
                if (setting.EqualsValues(masterDataCountrySetting))
                {
                    setting.LastChangeTimestamp = masterDataCountrySetting.LastChangeTimestamp;
                }
                else
                {
                    setting.LastChangeTimestamp = masterDataCountrySetting.LastChangeTimestamp.AddMonths(-1);
                }
            }
        }

        #region Import algorithm v1.3

        /// <summary>
        /// This is the import algorithm implemented for app version 1.3.
        /// It supports the importing of the export format version 1.3, which exports media in batches saved in separate files.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity in the file.</typeparam>
        /// <param name="filePath">The path to the file to import from.</param>
        /// <param name="parentEntity">The object that will be the parent of the imported object. Can be null only for Project and ProjectFolder.</param>
        /// <param name="dataManager">The data manager to use for importing the entity. I must also be the data manager with which <paramref name="objToImportInto" /> is associated.</param>
        /// <param name="saveChanges">If set to true, during import, data is saved into the database in batches in order to allow the memory used by the data to be reclaimed;
        /// otherwise all data is imported into memory and no save is performed (it is assumed the save is controlled externally).</param>
        /// <param name="importConfirmation">If has value this will show user a window that will contain the object that wants to import</param>
        /// <returns>
        /// A reference to the imported object.
        /// </returns>
        /// <exception cref="BusinessException">Import failed.</exception>
        private ImportedData<TEntity> ImportEntity_v1_3<TEntity>(
            string filePath,
            object parentEntity,
            IDataSourceManager dataManager,
            bool saveChanges,
            Func<object, bool> importConfirmation)
        {
            /*
             * 1. Deserialize the entity (model) object in the file
             * 2. Deserialize the metadata, if necessary
             * 3. Attach the entity to the parent and save the DataContext changes.
             * 4. Obtain a flat list of all objects in the entity's graph in order to add their media in the next step.
             * 5. Take each media batch and add each media to its parent. This operation should use a new DataContext for each batch and saves the changes
             *    in order to keep the memory consumption to minimum, unless we import in memory (saveChanges == true).
             */

            var importedData = new ImportedDataInternal<TEntity>();
            importedData.SourceFile = filePath;

            try
            {
                Type entityType = typeof(TEntity);
                using (ZipFile zip = ZipFile.Read(filePath))
                {
                    zip.AlternateEncoding = Encoding.UTF8;
                    zip.AlternateEncodingUsage = ZipOption.AsNecessary;

                    // Read the metadata and model data files
                    var metadataEntry = zip.FirstOrDefault(e => string.Compare(e.FileName, "metadata.bin", StringComparison.OrdinalIgnoreCase) == 0);
                    if (metadataEntry == null)
                    {
                        log.Error("The metadata file could not be found in the file to import.");
                        throw new BusinessException(ErrorCodes.ImportFileInvalidContent);
                    }

                    var modelDataEntry = zip.FirstOrDefault(e => string.Compare(e.FileName, "modeldata.bin", StringComparison.OrdinalIgnoreCase) == 0);
                    if (modelDataEntry == null)
                    {
                        log.Error("The model data file could not be found in the file to import.");
                        throw new BusinessException(ErrorCodes.ImportFileInvalidContent);
                    }

                    Serializer serializer = new Serializer();

                    // Deserialize the entity (model) object
                    ExportedObject exportedObj = null;
                    using (MemoryStream modelDataStream = new MemoryStream())
                    {
                        modelDataEntry.Extract(modelDataStream);
                        modelDataStream.Position = 0L;
                        exportedObj = (ExportedObject)serializer.Deserialize(modelDataStream);
                    }

                    Dictionary<Guid, object> importedObjectsMap;
                    object model = mapper.MapExportedObject(exportedObj, out importedObjectsMap);
                    exportedObj = null;

                    if (!(model is TEntity))
                    {
                        // The file did not contain the expected model type.
                        log.Error("The import file did not contain an entity of type {0}.", entityType.FullName);
                        throw new BusinessException(this.GetErrorCodeForInvalidEntityInFile(entityType));
                    }

                    Collection<Currency> currencies = new Collection<Currency>();
                    Currency baseCurrency = null;

                    // Read the currencies data file and if it exists deserialize the exported currencies data.
                    ExportedCurrenciesData currenciesData = null;
                    var currenciesDataEntry = zip.FirstOrDefault(e => string.Compare(e.FileName, "CurrenciesData.bin", StringComparison.OrdinalIgnoreCase) == 0);
                    if (currenciesDataEntry != null)
                    {
                        using (MemoryStream currenciesDataStream = new MemoryStream())
                        {
                            currenciesDataEntry.Extract(currenciesDataStream);
                            currenciesDataStream.Position = 0L;

                            currenciesData = (ExportedCurrenciesData)serializer.Deserialize(currenciesDataStream);

                            // Map the exported currencies and base currency.
                            baseCurrency = (Currency)mapper.MapExportedObject(currenciesData.BaseCurrency);
                            foreach (var currency in currenciesData.Currencies)
                            {
                                currencies.Add((Currency)mapper.MapExportedObject(currency));
                            }
                        }
                    }

                    CurrencyConversionManager.ConvertCurrency(parentEntity, model, dataManager.DatabaseId, currencies, baseCurrency);

                    // Fix the entity's country settings last change timestamp.
                    if (currenciesDataEntry == null)
                    {
                        this.FixCountrySettingsTimestamp(model, dataManager.DatabaseId);
                    }

                    // Fix the entity's name if is null by setting a default value
                    this.FixEntityName(model);
                    bool confirmationImport;

                    // confirm the import if necessary
                    if (importConfirmation != null)
                    {
                        confirmationImport = importConfirmation(model);
                    }
                    else
                    {
                        confirmationImport = true;
                    }

                    if (confirmationImport)
                    {
                        // Update the references to shared objects in the model object to avoid duplicating shared objects like measurement units, etc.
                        CloneManager cloneMngr = new CloneManager(dataManager, true);
                        cloneMngr.UpdateSharedReferences(model, true);

                        // Attach the imported object to the parent entity.
                        this.AttachObjectToParent(model, parentEntity, dataManager);

                        if (saveChanges)
                        {
                            // Add the entity to the data context before saving.
                            var repository = dataManager.Repository(model.GetType());
                            repository.Add(model);

                            // Save changes at this point so the imported entity is inserted into the database.
                            // This is necessary so we are able to independently add the media to the objects in the imported entity's graph.                            
                            dataManager.SaveChanges();
                        }

                        importedData.Entity = (TEntity)model;

                        // Extract the media batches from the import file, attach the media inside to their parent objects
                        // and save the changes after each batch is processed (to keep memory consumption low).
                        string tempFolder = null;
                        try
                        {
                            tempFolder = PathUtils.CreateTemporaryFolder();
                            zip.ExtractSelectedEntries("type = F", "Media", tempFolder);

                            // Check if the Media folder exists, for the case when the imported object has no media.
                            var batchFilesFolder = Path.Combine(tempFolder, "Media");
                            IEnumerable<string> batchFiles = null;
                            if (Directory.Exists(batchFilesFolder))
                            {
                                batchFiles = Directory.EnumerateFiles(batchFilesFolder);
                            }
                            else
                            {
                                batchFiles = new List<string>();
                            }

                            foreach (var batchFilePath in batchFiles)
                            {
                                // If saving changes is enabled we create a new data context for importing the media of each batch.
                                // The data context is disposed of before processing the next batch.
                                IDataSourceManager mediaDataManager = null;
                                if (saveChanges)
                                {
                                    mediaDataManager = DataAccessFactory.CreateDataSourceManager(dataManager.DatabaseId);
                                }

                                using (FileStream fs = new FileStream(batchFilePath, FileMode.Open, FileAccess.Read))
                                {
                                    ExportedMediaBatch batch = (ExportedMediaBatch)serializer.Deserialize(fs);
                                    foreach (var exportedMedia in batch.Media)
                                    {
                                        object importedParent;
                                        if (importedObjectsMap.TryGetValue(exportedMedia.ParentId, out importedParent))
                                        {
                                            // If save changes is enabled, the media parent must be obtained from the data manager instance used to save each media batch,
                                            // otherwise the parent is the instance from the model graph (importedParent)
                                            object mediaParent = importedParent;
                                            if (saveChanges)
                                            {
                                                var repository = mediaDataManager.Repository(importedParent.GetType());
                                                mediaParent = repository.GetByEntity(importedParent);
                                            }

                                            if (mediaParent != null)
                                            {
                                                Media media = (Media)mapper.MapExportedObject(exportedMedia.Media);

                                                var entityWithMediaCollection = mediaParent as IEntityWithMediaCollection;
                                                if (entityWithMediaCollection != null)
                                                {
                                                    entityWithMediaCollection.Media.Add(media);
                                                }
                                                else
                                                {
                                                    var entityWithMedia = mediaParent as IEntityWithMedia;
                                                    if (entityWithMedia != null)
                                                    {
                                                        entityWithMedia.Media = media;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            log.Error("Failed to identify the parent of a media during import. Media skipped.");
                                        }
                                    }
                                }

                                if (saveChanges)
                                {
                                    mediaDataManager.SaveChanges();
                                    mediaDataManager.Dispose();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            // If an error occurs during the media import use a relevant error code.
                            throw new BusinessException(ErrorCodes.ImportMediaError, ex);
                        }
                        finally
                        {
                            try
                            {
                                if (!string.IsNullOrEmpty(tempFolder))
                                {
                                    Directory.Delete(tempFolder, true);
                                }
                            }
                            catch
                            {
                                // There is no point reporting this error to the user or breaking the import flow by throwing it.
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                importedData.Error = this.LogAndWrapImportException(ex);
            }

            return importedData;
        }

        /// <summary>
        /// Imports the model in memory and caches its media on disk.
        /// This method imports from the exported file format v1.3
        /// </summary>
        /// <param name="modelFilePath">The model file path.</param>
        /// <returns>An object containing the imported data.</returns>
        private ImportedData<object> ImportModelInMemory_v1_3(string modelFilePath)
        {
            var importedData = new ImportedDataInternal<object>();
            importedData.SourceFile = modelFilePath;
            string tempMediaFolder = null;

            try
            {
                using (ZipFile zip = ZipFile.Read(modelFilePath))
                {
                    zip.AlternateEncoding = Encoding.UTF8;
                    zip.AlternateEncodingUsage = ZipOption.AsNecessary;

                    // Read the model data from the file, deserialize and convert it to a domain object.
                    var modelDataEntry = zip.FirstOrDefault(e => string.Compare(e.FileName, "modeldata.bin", StringComparison.OrdinalIgnoreCase) == 0);
                    if (modelDataEntry == null)
                    {
                        log.Error("Model data file could not be found in the file to import");
                        throw new BusinessException(ErrorCodes.ImportFileInvalidContent);
                    }

                    Serializer serializer = new Serializer();

                    // Deserialize the entity (model) object                    
                    ExportedObject exportedObj = null;
                    using (MemoryStream modelDataStream = new MemoryStream())
                    {
                        modelDataEntry.Extract(modelDataStream);
                        modelDataStream.Position = 0L;
                        exportedObj = (ExportedObject)serializer.Deserialize(modelDataStream);
                    }

                    Dictionary<Guid, object> importedObjectsMap;
                    object model = mapper.MapExportedObject(exportedObj, out importedObjectsMap);
                    importedData.Entity = model;
                    exportedObj = null;

                    RelationshipsGraph.FixupEntityRelationships(model);

                    // Read the cost calculation data from the file (if exists), and deserialize it.
                    var calcDataEntry = zip.FirstOrDefault(e => string.Compare(e.FileName, "CalculationData.bin", StringComparison.OrdinalIgnoreCase) == 0);
                    if (calcDataEntry != null)
                    {
                        using (MemoryStream calculationDataStream = new MemoryStream())
                        {
                            calcDataEntry.Extract(calculationDataStream);
                            calculationDataStream.Position = 0L;
                            importedData.CostCalculationData = (ExportedCalculationData)serializer.Deserialize(calculationDataStream);
                        }
                    }

                    // Read the currencies data file and if it exists deserialize the exported currencies data.                    
                    ExportedCurrenciesData currenciesData = null;
                    var currenciesDataEntry = zip.FirstOrDefault(e => string.Compare(e.FileName, "CurrenciesData.bin", StringComparison.OrdinalIgnoreCase) == 0);
                    if (currenciesDataEntry != null)
                    {
                        using (MemoryStream currenciesDataStream = new MemoryStream())
                        {
                            currenciesDataEntry.Extract(currenciesDataStream);
                            currenciesDataStream.Position = 0L;

                            currenciesData = (ExportedCurrenciesData)serializer.Deserialize(currenciesDataStream);

                            // Map the exported currencies and base currency.
                            importedData.BaseCurrency = (Currency)mapper.MapExportedObject(currenciesData.BaseCurrency);
                            foreach (var currency in currenciesData.Currencies)
                            {
                                importedData.Currencies.Add((Currency)mapper.MapExportedObject(currency));
                            }
                        }
                    }

                    tempMediaFolder = PathUtils.CreateTemporaryFolder();
                    zip.ExtractSelectedEntries("type = F", "Media", tempMediaFolder);

                    // Check if the Media folder exists, for the case when the imported object has no media.
                    var batchFilesFolder = Path.Combine(tempMediaFolder, "Media");
                    var batchFiles = Directory.Exists(batchFilesFolder) ? Directory.EnumerateFiles(batchFilesFolder) : new List<string>();

                    foreach (var batchFilePath in batchFiles)
                    {
                        using (FileStream fs = new FileStream(batchFilePath, FileMode.Open, FileAccess.Read))
                        {
                            ExportedMediaBatch batch = (ExportedMediaBatch)serializer.Deserialize(fs);
                            foreach (var exportedMedia in batch.Media)
                            {
                                object importedParent;
                                if (importedObjectsMap.TryGetValue(exportedMedia.ParentId, out importedParent))
                                {
                                    IIdentifiable identifiableImportedParent = importedParent as IIdentifiable;
                                    if (identifiableImportedParent != null)
                                    {
                                        Media media = (Media)mapper.MapExportedObject(exportedMedia.Media);
                                        AddMediaToImportedData(importedData, identifiableImportedParent.Guid, media);
                                    }
                                    else
                                    {
                                        log.Error("Failed to identify the parent of a media during import. The parent is not an IIdentifiable object. Media skipped.");
                                    }
                                }
                                else
                                {
                                    log.Error("Failed to identify the parent of a media during import. Media skipped.");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                importedData.Error = this.LogAndWrapImportException(ex);
            }
            finally
            {
                // Delete the temporary folder where exported media was kept
                if (!string.IsNullOrEmpty(tempMediaFolder) && Directory.Exists(tempMediaFolder))
                {
                    try
                    {
                        Directory.Delete(tempMediaFolder, true);
                    }
                    catch
                    {
                    }
                }
            }

            return importedData;
        }

        /// <summary>
        /// Adds the media to imported data.
        /// </summary>
        /// <param name="importedData">The imported data to add the media into.</param>
        /// <param name="parentEntityId">The parent entity id.</param>
        /// <param name="media">The media.</param>
        private void AddMediaToImportedData(ImportedDataInternal<object> importedData, Guid parentEntityId, Media media)
        {
            if (media == null)
            {
                return;
            }

            List<ImportedMedia> mediaPathList;
            if (importedData.MediaCollection.ContainsKey(parentEntityId))
            {
                mediaPathList = (List<ImportedMedia>)importedData.MediaCollection[parentEntityId];
            }
            else
            {
                mediaPathList = new List<ImportedMedia>();
                importedData.MediaCollection.Add(parentEntityId, mediaPathList);
            }

            if (!Directory.Exists(Constants.ViewerModeCacheFolderPath))
            {
                Directory.CreateDirectory(Constants.ViewerModeCacheFolderPath);
            }

            var mediaFilePath = Path.Combine(Constants.ViewerModeCacheFolderPath, Guid.NewGuid() + Path.GetExtension(media.OriginalFileName));
            ImportedMedia importedMedia = new ImportedMedia(media, mediaFilePath);

            try
            {
                File.WriteAllBytes(mediaFilePath, media.Content);
                mediaPathList.Add(importedMedia);
            }
            catch (Exception ex)
            {
                log.Error("Could not write imported media to disk.", ex);
            }
        }

        #endregion Import algorithm v1.3

        #region Import algorithm v1.0

        /// <summary>
        /// This is the 1st (initial) version of the import algorithm. It remains for backwards compatibility.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity in the file.</typeparam>
        /// <param name="filePath">The path to the file to import from.</param>
        /// <param name="parentEntity">The object that will be the parent of the imported object. Can be null only for Project and ProjectFolder.</param>
        /// <param name="dataManager">The data manager to use for importing the entity. I must also be the data manager with which <paramref name="objToImportInto"/> is associated.</param>
        /// <param name="saveChanges"> if set to true the imported entity is saved into the database; otherwise the save is handled externally.</param>
        /// <param name="importConfirmation">If has value this will show user a window that will contain the object that wants to import</param>
        /// <returns>
        /// A reference to the imported object.
        /// </returns>
        /// <exception cref="BusinessException">Import failed.</exception>
        private TEntity ImportEntity_v1_0<TEntity>(string filePath, object parentEntity, IDataSourceManager dataManager, bool saveChanges, Func<object, bool> importConfirmation)
        {
            Type entityType = typeof(TEntity);
            object importedEntity = null;

            // De-serialize the object from the file and check if is TEntity
            object deserializedEntity = this.DeserializeEntity_v1_0(filePath);
            if (!(deserializedEntity is TEntity))
            {
                log.Error("Import file did not contain an entity of type {0}.", entityType.FullName);
                string code = this.GetErrorCodeForInvalidEntityInFile(entityType);
                throw new BusinessException(code);
            }

            // Convert the model hierarchy currencies data.
            CurrencyConversionManager.ConvertCurrency(parentEntity, deserializedEntity, dataManager.DatabaseId, null, null);

            // Fix the entity's name if is null by setting a default value
            this.FixEntityName(deserializedEntity);

            this.FixCountrySettingsTimestamp(deserializedEntity, dataManager.DatabaseId);

            bool confirmationImport;

            // confirm the import if necessary
            if (importConfirmation != null)
            {
                confirmationImport = importConfirmation(deserializedEntity);
            }
            else
            {
                confirmationImport = true;
            }

            if (confirmationImport)
            {
                // Clone the imported object so that the shared references are set and it is added into the data context.
                importedEntity = this.CloneDeserializedObject(deserializedEntity, dataManager);
                deserializedEntity = null;

                // Attach the imported object to the parent entity.
                if (importedEntity != null)
                {
                    this.AttachObjectToParent(importedEntity, parentEntity, dataManager);
                    if (saveChanges)
                    {
                        // Add the entity to the data context before saving.
                        var repository = dataManager.Repository(importedEntity.GetType());
                        repository.Add(importedEntity);

                        dataManager.SaveChanges();
                    }
                }
                else
                {
                    log.Error("Import - cloning entity of type {0} resulted in null.", entityType.FullName);
                    throw new BusinessException(ErrorCodes.InternalError);
                }
            }

            return (TEntity)importedEntity;
        }

        /// <summary>
        /// Imports the model in memory and caches its media on disk.
        /// This method imports from the exported file format v1.0
        /// </summary>
        /// <param name="modelFilePath">The model file path.</param>
        /// <returns>An object containing the imported data.</returns>
        private ImportedData<object> ImportModelInMemory_v1_0(string modelFilePath)
        {
            var importedData = new ImportedDataInternal<object>();
            importedData.SourceFile = modelFilePath;
            try
            {
                var model = this.DeserializeEntity_v1_0(modelFilePath);

                RelationshipsGraph.FixupEntityRelationships(model);

                SaveAndDetachMedia(model, importedData);
                importedData.Entity = model;
            }
            catch (Exception ex)
            {
                importedData.Error = this.LogAndWrapImportException(ex);
            }

            return importedData;
        }

        /// <summary>
        /// Saves the and detach media.
        /// </summary>
        /// <param name="importedObject">The imported object.</param>
        /// <param name="importedData">The imported data.</param>
        private void SaveAndDetachMedia(object importedObject, ImportedDataInternal<object> importedData)
        {
            if (importedObject == null)
            {
                return;
            }

            var commodity = importedObject as Commodity;
            if (commodity != null)
            {
                var media = commodity.Media;
                commodity.Media = null;
                this.AddMediaToImportedData(importedData, commodity.Guid, media);

                return;
            }

            var consumable = importedObject as Consumable;
            if (consumable != null)
            {
                // Note: the consumable does not have media.
                return;
            }

            var die = importedObject as Die;
            if (die != null)
            {
                var media = die.Media;
                die.Media = null;
                this.AddMediaToImportedData(importedData, die.Guid, media);

                return;
            }

            var machine = importedObject as Machine;
            if (machine != null)
            {
                var media = machine.Media;
                machine.Media = null;
                this.AddMediaToImportedData(importedData, machine.Guid, media);

                return;
            }

            var material = importedObject as RawMaterial;
            if (material != null)
            {
                var media = material.Media;
                material.Media = null;
                this.AddMediaToImportedData(importedData, material.Guid, media);

                return;
            }

            var part = importedObject as Part;
            if (part != null)
            {
                var medias = part.Media.ToList();

                foreach (var media in medias)
                {
                    part.Media.Remove(media);
                    this.AddMediaToImportedData(importedData, part.Guid, media);
                }

                foreach (var partCommodity in part.Commodities)
                {
                    this.SaveAndDetachMedia(partCommodity, importedData);
                }

                foreach (var partRawMaterial in part.RawMaterials)
                {
                    this.SaveAndDetachMedia(partRawMaterial, importedData);
                }

                this.SaveAndDetachMedia(part.RawPart, importedData);

                if (part.Process.Steps != null)
                {
                    foreach (var step in part.Process.Steps)
                    {
                        this.SaveAndDetachMedia(step, importedData);
                    }
                }

                return;
            }

            var folder = importedObject as ProjectFolder;
            if (folder != null)
            {
                foreach (var childrenProjectFolder in folder.ChildrenProjectFolders)
                {
                    this.SaveAndDetachMedia(childrenProjectFolder, importedData);
                }

                foreach (var folderProject in folder.Projects)
                {
                    this.SaveAndDetachMedia(folderProject, importedData);
                }

                return;
            }

            var project = importedObject as Project;
            if (project != null)
            {
                var medias = project.Media.ToList();
                foreach (var media in medias)
                {
                    project.Media.Remove(media);
                    this.AddMediaToImportedData(importedData, project.Guid, media);
                }

                foreach (var projectAssembly in project.Assemblies)
                {
                    this.SaveAndDetachMedia(projectAssembly, importedData);
                }

                foreach (var projectPart in project.Parts)
                {
                    this.SaveAndDetachMedia(projectPart, importedData);
                }

                return;
            }

            var assy = importedObject as Assembly;
            if (assy != null)
            {
                var medias = assy.Media.ToList();
                foreach (var media in medias)
                {
                    assy.Media.Remove(media);
                    this.AddMediaToImportedData(importedData, assy.Guid, media);
                }

                foreach (var subassembly in assy.Subassemblies)
                {
                    this.SaveAndDetachMedia(subassembly, importedData);
                }

                foreach (var assemblyPart in assy.Parts)
                {
                    this.SaveAndDetachMedia(assemblyPart, importedData);
                }

                if (assy.Process.Steps != null)
                {
                    foreach (var step in assy.Process.Steps)
                    {
                        this.SaveAndDetachMedia(step, importedData);
                    }
                }

                return;
            }

            var processStep = importedObject as ProcessStep;
            if (processStep != null)
            {
                var media = processStep.Media;
                processStep.Media = null;
                this.AddMediaToImportedData(importedData, processStep.Guid, media);

                foreach (var stepCommodity in processStep.Commodities)
                {
                    this.SaveAndDetachMedia(stepCommodity, importedData);
                }

                foreach (var stepDie in processStep.Dies)
                {
                    this.SaveAndDetachMedia(stepDie, importedData);
                }

                foreach (var stepMachine in processStep.Machines)
                {
                    this.SaveAndDetachMedia(stepMachine, importedData);
                }

                return;
            }

            log.Error("The imported object ({0}) is not handled.", importedObject.GetType().FullName);
            throw new BusinessException(ErrorCodes.InternalError);
        }

        /// <summary>
        /// Imports an entity from a specified file (de-serializes the data from the file and maps it using an ExportMapper instance).
        /// </summary>
        /// <param name="path">The file path.</param>
        /// <returns>The object unpacked from the file.</returns>
        /// <exception cref="BusinessException">De-serialization or mapping errors have occurred.</exception>
        private object DeserializeEntity_v1_0(string path)
        {
            // This variable is set to true when the import path contained a zipped file which was unzipped in TEMP. This file must be deleted at the end.
            bool pathIsTempFile = false;

            try
            {
                if (ZipFile.IsZipFile(path))
                {
                    using (ZipFile zip = ZipFile.Read(path))
                    {
                        zip.AlternateEncoding = Encoding.UTF8;
                        zip.AlternateEncodingUsage = ZipOption.AsNecessary;

                        ZipEntry entry = zip.FirstOrDefault();
                        if (entry != null)
                        {
                            // set a random file name to zip entry because in case of a crash the temporary file used for extraction 
                            // will not be deleted and it will cause conflict if is trying to extract an object with the same name
                            entry.FileName = Path.GetRandomFileName();

                            // Unzip the content in the TEMP dir
                            entry.Extract(Path.GetTempPath(), ExtractExistingFileAction.OverwriteSilently);
                            path = Path.Combine(Path.GetTempPath(), entry.FileName);
                            pathIsTempFile = true;
                        }
                        else
                        {
                            throw new ZipException();
                        }
                    }
                }

                using (FileStream importStream = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    Serializer serializer = new Serializer();
                    ExportedObject obj = (ExportedObject)serializer.Deserialize(importStream);
                    return mapper.MapExportedObject(obj);
                }
            }
            catch (Exception ex)
            {
                throw this.LogAndWrapImportException(ex);
            }
            finally
            {
                if (pathIsTempFile)
                {
                    try
                    {
                        File.Delete(path);
                    }
                    catch (Exception ex)
                    {
                        log.WarnException("Failed to delete temporary file extracted during import.", ex);
                    }
                }
            }
        }

        /// <summary>
        /// Clones the de-serialized object so that its references that are shared with other objects are retrieved from the database and set into the clone object.
        /// The purpose of this operation is to avoid adding to the database new instances of objects that are shared, like measurement units,
        /// countries, classifications, etc.
        /// </summary>
        /// <param name="deserializedObject">The de-serialized object.</param>
        /// <param name="dataSourceManager">The data source manager to use for cloning.</param>
        /// <returns>The cloned object.</returns>
        private object CloneDeserializedObject(object deserializedObject, IDataSourceManager dataSourceManager)
        {
            if (deserializedObject == null)
            {
                throw new InvalidOperationException("Can't clone a null de-serialized object.");
            }

            CloneManager cloneManager = new CloneManager(dataSourceManager, true);

            ProjectFolder projectFolder = deserializedObject as ProjectFolder;
            if (projectFolder != null)
            {
                return cloneManager.Clone(projectFolder);
            }

            Project project = deserializedObject as Project;
            if (project != null)
            {
                return cloneManager.Clone(project);
            }

            Assembly assy = deserializedObject as Assembly;
            if (assy != null)
            {
                return cloneManager.Clone(assy);
            }

            RawPart rawPart = deserializedObject as RawPart;
            if (rawPart != null)
            {
                return cloneManager.Clone(rawPart, true);
            }

            Part part = deserializedObject as Part;
            if (part != null)
            {
                return cloneManager.Clone(part);
            }

            RawMaterial material = deserializedObject as RawMaterial;
            if (material != null)
            {
                return cloneManager.Clone(material);
            }

            Commodity commodity = deserializedObject as Commodity;
            if (commodity != null)
            {
                return cloneManager.Clone(commodity);
            }

            Consumable consumable = deserializedObject as Consumable;
            if (consumable != null)
            {
                return cloneManager.Clone(consumable);
            }

            Machine machine = deserializedObject as Machine;
            if (machine != null)
            {
                return cloneManager.Clone(machine);
            }

            Die die = deserializedObject as Die;
            if (die != null)
            {
                return cloneManager.Clone(die);
            }

            string msg = string.Format("The cloning of de-serialized objects is not implemented for type {0}.", deserializedObject.GetType().Name);
            throw new InvalidOperationException(msg);
        }

        #region Logic for attaching imported objects to a parent

        /// <summary>
        /// Associates the specified object with the specified parent.
        /// </summary>
        /// <param name="importedObject">The imported object.</param>
        /// <param name="parentObject">The parent object.</param>
        /// <param name="dataSourceManager">The data manager associated with the parent and with which the child object will be associated.</param>
        private void AttachObjectToParent(object importedObject, object parentObject, IDataSourceManager dataSourceManager)
        {
            var commodity = importedObject as Commodity;
            if (commodity != null)
            {
                this.AddImportedObjectToParent(commodity, parentObject);
                return;
            }

            var consumable = importedObject as Consumable;
            if (consumable != null)
            {
                this.AddImportedObjectToParent(consumable, parentObject);
                return;
            }

            var die = importedObject as Die;
            if (die != null)
            {
                this.AddImportedObjectToParent(die, parentObject);
                return;
            }

            var machine = importedObject as Machine;
            if (machine != null)
            {
                this.AddImportedObjectToParent(machine, parentObject);
                return;
            }

            var material = importedObject as RawMaterial;
            if (material != null)
            {
                this.AddImportedObjectToParent(material, parentObject);
                return;
            }

            var part = importedObject as Part;
            if (part != null)
            {
                this.AddImportedObjectToParent(part, parentObject);
                return;
            }

            var folder = importedObject as ProjectFolder;
            if (folder != null)
            {
                this.AddImportedObjectToParent(folder, parentObject, dataSourceManager);
                return;
            }

            var project = importedObject as Project;
            if (project != null)
            {
                this.AddImportedObjectToParent(project, parentObject, dataSourceManager);
                return;
            }

            var assy = importedObject as Assembly;
            if (assy != null)
            {
                this.AddImportedObjectToParent(assy, parentObject);
                return;
            }

            log.Error("The imported object ({0}) is not handled.", importedObject.GetType().FullName);
            throw new BusinessException(ErrorCodes.InternalError);
        }

        /// <summary>
        /// Associates the imported commodity with a parent.
        /// </summary>
        /// <param name="importedObject">The imported commodity.</param>
        /// <param name="parentObject">The parent object.</param>
        private void AddImportedObjectToParent(Commodity importedObject, object parentObject)
        {
            var parentPart = parentObject as Part;
            if (parentPart != null)
            {
                parentPart.Commodities.Add(importedObject);
            }
            else
            {
                var parentStep = parentObject as ProcessStep;
                if (parentStep != null)
                {
                    importedObject.ProcessStep = parentStep;
                }
                else
                {
                    log.Error("Import commodity - parent object not supported: {0}.", parentObject.GetType().FullName);
                    throw new BusinessException(ErrorCodes.InternalError);
                }
            }

            this.CopyDataFromParent(importedObject, parentObject);
        }

        /// <summary>
        /// Associates the imported Consumable with a parent.
        /// </summary>
        /// <param name="importedObject">The imported consumable.</param>
        /// <param name="parentObject">The parent object.</param>
        private void AddImportedObjectToParent(Consumable importedObject, object parentObject)
        {
            var parentStep = parentObject as ProcessStep;
            if (parentStep != null)
            {
                importedObject.ProcessStep = parentStep;
            }
            else
            {
                log.Error("Import consumable - parent object not supported: {0}.", parentObject.GetType().FullName);
                throw new BusinessException(ErrorCodes.InternalError);
            }

            this.CopyDataFromParent(importedObject, parentObject);
        }

        /// <summary>
        /// Associates the imported Die with a parent.
        /// </summary>
        /// <param name="importedObject">The imported die.</param>
        /// <param name="parentObject">The parent object.</param>
        private void AddImportedObjectToParent(Die importedObject, object parentObject)
        {
            var parentStep = parentObject as ProcessStep;
            if (parentStep != null)
            {
                importedObject.ProcessStep = parentStep;
            }
            else
            {
                log.Error("Import die - parent object not supported: {0}.", parentObject.GetType().FullName);
                throw new BusinessException(ErrorCodes.InternalError);
            }

            this.CopyDataFromParent(importedObject, parentObject);
        }

        /// <summary>
        /// Associates the imported Machine with a parent.
        /// </summary>
        /// <param name="importedObject">The imported machine.</param>
        /// <param name="parentObject">The parent object.</param>
        private void AddImportedObjectToParent(Machine importedObject, object parentObject)
        {
            var parentStep = parentObject as ProcessStep;
            if (parentStep != null)
            {
                importedObject.ProcessStep = parentStep;
            }
            else
            {
                log.Error("Import machine - parent object not supported: {0}.", parentObject.GetType().FullName);
                throw new BusinessException(ErrorCodes.InternalError);
            }

            this.CopyDataFromParent(importedObject, parentObject);
        }

        /// <summary>
        /// Associates the imported RawMaterial with a parent.
        /// </summary>
        /// <param name="importedObject">The imported raw material.</param>
        /// <param name="parentObject">The parent object.</param>
        private void AddImportedObjectToParent(RawMaterial importedObject, object parentObject)
        {
            var parentPart = parentObject as Part;
            if (parentPart != null)
            {
                parentPart.RawMaterials.Add(importedObject);
            }
            else
            {
                log.Error("Import raw material - parent object not supported: {0}.", parentObject.GetType().FullName);
                throw new BusinessException(ErrorCodes.InternalError);
            }

            this.CopyDataFromParent(importedObject, parentObject);
        }

        /// <summary>
        /// Associates the imported Part with a parent.
        /// </summary>
        /// <param name="importedObject">The imported part.</param>
        /// <param name="parentObject">The parent object.</param>
        private void AddImportedObjectToParent(Part importedObject, object parentObject)
        {
            Project parentProject = parentObject as Project;
            if (parentProject != null)
            {
                importedObject.Index = parentProject.Parts.Max(p => p.Index) + 1 ?? 0;
                parentProject.Parts.Add(importedObject);
            }
            else
            {
                Assembly parentAssembly = parentObject as Assembly;
                if (parentAssembly != null)
                {
                    importedObject.Index = parentAssembly.Parts.Max(p => p.Index) + 1 ?? 0;
                    parentAssembly.Parts.Add(importedObject);
                }
                else
                {
                    Part parentPart = parentObject as Part;
                    if (parentPart != null)
                    {
                        parentPart.RawPart = importedObject;
                    }
                    else
                    {
                        log.Error("Import Part - parent object not supported: {0}.", parentObject.GetType().FullName);
                        throw new BusinessException(ErrorCodes.InternalError);
                    }
                }
            }

            this.CopyDataFromParent(importedObject, parentObject);
        }

        /// <summary>
        /// Adds the imported Project Folders to the database.
        /// </summary>
        /// <param name="importedObject">The imported project folder.</param>
        /// <param name="parentObject">The parent object.</param>
        /// <param name="dataManager">The data source manager associated with the project folder and the parent.</param>
        private void AddImportedObjectToParent(ProjectFolder importedObject, object parentObject, IDataSourceManager dataManager)
        {
            if (parentObject == null)
            {
                importedObject.ParentProjectFolder = null;

                // The owner of projects without parent is the current logged in user.
                User owner = dataManager.UserRepository.GetByKey(SecurityManager.Instance.CurrentUser.Guid);
                importedObject.SetOwner(owner);
            }
            else
            {
                ProjectFolder folder = parentObject as ProjectFolder;
                if (folder != null)
                {
                    ProjectFolder freshFolder = dataManager.ProjectFolderRepository.GetByKey(folder.Guid);
                    if (freshFolder == null)
                    {
                        throw new BusinessException(ErrorCodes.ItemNotFound);
                    }

                    freshFolder.ChildrenProjectFolders.Add(importedObject);

                    User owner = freshFolder.Owner;
                    if (owner == null)
                    {
                        owner = dataManager.ProjectFolderRepository.GetOwner(freshFolder);
                    }

                    importedObject.SetOwner(owner);
                }
                else
                {
                    log.Error("Import Project Folder - parent object not supported: {0}.", parentObject.GetType().FullName);
                    throw new BusinessException(ErrorCodes.InternalError);
                }
            }
        }

        /// <summary>
        /// Adds the imported Projects to the database.
        /// </summary>
        /// <param name="importedObject">The imported project.</param>
        /// <param name="parentObject">The parent object.</param>
        /// <param name="dataManager">The data manager associated with the project and the parent.</param>
        private void AddImportedObjectToParent(Project importedObject, object parentObject, IDataSourceManager dataManager)
        {
            if (parentObject == null)
            {
                importedObject.ProjectFolder = null;

                // The owner of projects without parent is the current logged in user.
                User owner = dataManager.UserRepository.GetByKey(SecurityManager.Instance.CurrentUser.Guid);
                importedObject.SetOwner(owner);
            }
            else
            {
                ProjectFolder folder = parentObject as ProjectFolder;
                if (folder != null)
                {
                    ProjectFolder freshFolder = dataManager.ProjectFolderRepository.GetByKey(folder.Guid);
                    if (freshFolder == null)
                    {
                        throw new BusinessException(ErrorCodes.ItemNotFound);
                    }

                    freshFolder.Projects.Add(importedObject);

                    User owner = freshFolder.Owner;
                    if (owner == null)
                    {
                        owner = dataManager.ProjectFolderRepository.GetOwner(freshFolder);
                    }

                    importedObject.SetOwner(owner);
                }
                else
                {
                    log.Error("Import Project - parent object not supported: {0}.", parentObject.GetType().FullName);
                    throw new BusinessException(ErrorCodes.InternalError);
                }
            }

            importedObject.SetIsMasterData(false);
        }

        /// <summary>
        /// Associates the imported Assembly with a parent.
        /// </summary>
        /// <param name="importedObject">The imported assembly.</param>
        /// <param name="parentObject">The parent object.</param>
        private void AddImportedObjectToParent(Assembly importedObject, object parentObject)
        {
            Project parentProject = parentObject as Project;
            if (parentProject != null)
            {
                importedObject.Index = parentProject.Assemblies.Max(p => p.Index) + 1 ?? 0;
                parentProject.Assemblies.Add(importedObject);
            }
            else
            {
                Assembly parentAssembly = parentObject as Assembly;
                if (parentAssembly != null)
                {
                    importedObject.Index = parentAssembly.Subassemblies.Max(p => p.Index) + 1 ?? 0;
                    parentAssembly.Subassemblies.Add(importedObject);
                }
                else
                {
                    log.Error("Import Assembly - parent object not supported: {0}.", parentObject.GetType().FullName);
                    throw new BusinessException(ErrorCodes.InternalError);
                }
            }

            this.CopyDataFromParent(importedObject, parentObject);
        }

        /// <summary>
        /// Copies some data from the parent into the imported object (like owner, IsMasterData flag, etc).
        /// </summary>
        /// <typeparam name="T">The type of the imported object.</typeparam>
        /// <param name="importedObject">The imported object.</param>
        /// <param name="parent">The imported object's parent.</param>
        private void CopyDataFromParent<T>(T importedObject, object parent) where T : IOwnedObject, IMasterDataObject
        {
            if (importedObject == null)
            {
                throw new ArgumentNullException("importedObject", "The imported object can not be null.");
            }

            bool isMasterdata = EntityUtils.IsMasterData(parent);
            importedObject.SetIsMasterData(isMasterdata);

            IOwnedObject ownedObj = parent as IOwnedObject;
            if (ownedObj != null)
            {
                importedObject.SetOwner(ownedObj.Owner);
            }
        }

        #endregion Logic for attaching imported objects to a parent

        #endregion Import algorithm v1.0

        #endregion Import
    }
}