﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.Export
{
    /// <summary>
    /// Represents the ways in which a data type value is exposed
    /// </summary>
    public enum MemberExposureType
    {
        /// <summary>
        /// The value is exposed through a field.
        /// </summary>
        Field = 0,

        /// <summary>
        /// The value is exposed through a property.
        /// </summary>
        Property = 1
    }
}
