﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.Export
{
    /// <summary>
    /// Represents an old version of a data type/field/property/collection that must be supported for import.
    /// </summary>
    [SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", MessageId = "Alias", Justification = "The name is appropriate in this context.")]
    public class Alias
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Alias"/> class.
        /// </summary>
        public Alias()
        {
        }

        /// <summary>
        /// Gets or sets the alias name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the alias type.
        /// </summary>
        public string Type { get; set; }
    }
}
