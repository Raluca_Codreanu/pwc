﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace ZPKTool.Business.Export
{
    /// <summary>
    /// Implements the serialization and de-serialization processes for the Import and Export operations.
    /// </summary>
    public class Serializer
    {
        /// <summary>
        /// The serialization provider used to serialize data during export.
        /// </summary>
        private BinaryFormatter serializationFormatter = new BinaryFormatter();

        /// <summary>
        /// Initializes a new instance of the <see cref="Serializer"/> class.
        /// </summary>
        public Serializer()
        {
            this.serializationFormatter.Binder = new BinaryFormatterBinder();
            this.serializationFormatter.AssemblyFormat = FormatterAssemblyStyle.Simple;
        }

        /// <summary>
        /// Serializes the specified object or graph of objects to the specified stream.
        /// </summary>
        /// <param name="graph">The object graph.</param>
        /// <param name="serializationStream">The serialization stream.</param>
        /// <exception cref="System.ArgumentNullException">The object graph was null or the stream was null</exception>
        /// <exception cref="System.Runtime.Serialization.SerializationException"> A serialization error occurred.</exception>
        public void Serialize(object graph, Stream serializationStream)
        {
            this.serializationFormatter.Serialize(serializationStream, graph);
        }

        /// <summary>
        /// Deserializes the specified stream into an object graph.
        /// </summary>
        /// <param name="serializationStream">The stream from which to deserialize.</param>
        /// <returns>The object at the top of the deserialized objects graph.</returns>
        /// <exception cref="System.ArgumentNullException">The stream was null</exception>
        /// <exception cref="System.Runtime.Serialization.SerializationException"> A deserialization error occurred.</exception>
        public object Deserialize(Stream serializationStream)
        {
            return this.serializationFormatter.Deserialize(serializationStream);
        }

        #region Inner classes

        /// <summary>
        /// Handles type substitution and other tasks for the internal binary formatter.
        /// </summary>                
        private sealed class BinaryFormatterBinder : SerializationBinder
        {
            /// <summary>
            /// The name of the assembly that contains the Export code.
            /// </summary>
            private static readonly AssemblyName currentAssemblyName = Assembly.GetExecutingAssembly().GetName();

            /// <summary>
            /// When overridden in a derived class, controls the binding of a serialized object to a type.
            /// </summary>
            /// <param name="assemblyName">Specifies the <see cref="T:System.Reflection.Assembly" /> name of the serialized object.</param>
            /// <param name="typeName">Specifies the <see cref="T:System.Type" /> name of the serialized object.</param>
            /// <returns>
            /// The type of the object the formatter creates a new instance of.
            /// </returns>
            public override Type BindToType(string assemblyName, string typeName)
            {
                // "Fix" the assembly and type names then get the Type represented by typeName qualified with assemblyName.
                if (!string.IsNullOrEmpty(assemblyName))
                {
                    assemblyName = this.FixAssemblyNameForDeserialization(assemblyName);
                }

                typeName = this.FixTypeDefinitionForDeserialization(typeName);
                var qualifiedTypeName = Assembly.CreateQualifiedName(assemblyName, typeName);
                return Type.GetType(qualifiedTypeName);
            }

            /// <summary>
            /// "Fixes" the specified assembly name (usually fully qualified) during the de-serialization process.
            /// <para />
            /// This means:
            /// <para />
            ///   - Remove the Version and PublicTokenKey information in order to use whatever version of the assembly is available.
            /// <para />
            ///   - Replace the old name of assemblies with the new one (for the renamed assemblies).
            /// </summary>
            /// <param name="assemblyName">The name of the assembly.</param>
            /// <returns>The fixed assembly name.</returns>
            private string FixAssemblyNameForDeserialization(string assemblyName)
            {
                var name = new AssemblyName(assemblyName);

                // Remove the Version from the assemblyName because if it is set, Type.GetType() will find a type only if its assembly matches the version.
                // For the purpose of exporting and importing, the Version and PublicKeyToken attached to an exported object's type do not matter.
                // We always have to use the available version of an assembly, not the one saved by the BinaryFormatter during export.
                name.Version = null;
                name.SetPublicKeyToken(null);

                // Replaced old assembly names with the current ones.                
                if (name.Name == "Business")
                {
                    // Business.dll was renamed to ZPKTool.Business.dll
                    name.Name = BinaryFormatterBinder.currentAssemblyName.Name;
                }

                return name.FullName;
            }

            /// <summary>
            /// Performs the following "fixes" on a string representing a type definition in order to successfully de-serialize. The type definition can
            /// contain any amount of levels of generic definition nesting.
            /// <para />
            ///   - Removes the Version and PublicTokenKey from all assembly names (including core .NET framework assemblies). This is necessary to successfully
            ///     match Types when the assembly versions change.
            /// <para />
            ///   - Replaces old assembly names, for assemblies that were renamed.
            /// </summary>
            /// <remarks>
            /// This method is necessary to perform the fixed on complex type definition involving many levels of generics nesting. Ex:
            /// <para />
            /// System.Collections.Generic.Dictionary`2[[ZPKTool.Business.Export.ExportedObject, ZPKTool.Business, Version=1.4.6.0, Culture=neutral, PublicKeyToken=0b1303a1646951cd],[System.Collections.Generic.List`1[[ZPKTool.Business.Export.ExportedMedia, ZPKTool.Business, Version=1.4.6.0, Culture=neutral, PublicKeyToken=0b1303a1646951cd]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]
            /// </remarks>
            /// <param name="typeDefinition">The type definition.</param>
            /// <returns>The "fixed" type definition.</returns>
            private string FixTypeDefinitionForDeserialization(string typeDefinition)
            {
                string fixedTypeDefinition = null;

                // Splitting into square bracket tokens is necessary for fixing type names inside generic definitions.
                var tokens = this.SplitInSquareBracketSurroundedTokens(typeDefinition);
                if (tokens.Count > 0)
                {
                    // The string part preceding the 1st token must be preserved.
                    fixedTypeDefinition = typeDefinition.Substring(0, tokens[0].StartIndex);

                    for (int i = 0; i < tokens.Count; i++)
                    {
                        var token = tokens[i];
                        var fixedContent = this.FixTypeDefinitionForDeserialization(token.Content);
                        fixedTypeDefinition += "[" + fixedContent + "]";

                        if (i < tokens.Count - 1)
                        {
                            // Preserver the text exists between tokens.
                            fixedTypeDefinition += typeDefinition.Substring(token.EndIndex + 1, tokens[i + 1].StartIndex - token.EndIndex - 1);
                        }
                        else if (token.EndIndex < typeDefinition.Length - 1)
                        {
                            // Text at the end of the token that is not part of any sub-token; usually is the assembly information associated with
                            // a nested generic definition (ex: the part including and after "mscorlib" [System.Collections.Generic.List`1[[ExportedMedia, ZPKTool.Business, Version=1.4.6.0, Culture=neutral, PublicKeyToken=0b1303a1646951cd]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089])
                            string endText = typeDefinition.Substring(token.EndIndex + 1, typeDefinition.Length - token.EndIndex - 1);
                            try
                            {
                                // Try to interpret the endText as assembly name. If that fails append the endText to the fixedTypeDefinition as it is.
                                var assemblyName = endText.Trim(',', ' ');
                                var fixedAssemblyName = this.FixAssemblyNameForDeserialization(assemblyName);
                                fixedTypeDefinition += ", " + fixedAssemblyName;
                            }
                            catch
                            {
                                // The text at the end of the token was not assembly information, so add it back as it was.
                                fixedTypeDefinition += endText;
                            }
                        }
                    }
                }
                else
                {
                    // Simple type definition, without generics. ex: "MyClass, ZPKTool.Business, Version=1.0.0.0, PublicKeyToken=jsadg278436"
                    // Up to the 1st comma is the type name and from there on is the assembly name. The type definition may omit the assembly name.
                    int firstCommaIndex = typeDefinition.IndexOf(',');
                    if (firstCommaIndex > 0)
                    {
                        var assemblyName = typeDefinition.Substring(firstCommaIndex + 1);
                        var fixedAssemblyName = this.FixAssemblyNameForDeserialization(assemblyName);

                        string typeName = typeDefinition.Substring(0, firstCommaIndex + 1);
                        fixedTypeDefinition = typeName + fixedAssemblyName;
                    }
                    else
                    {
                        // The type definition does not contain the assembly information. Nothing to do.
                        fixedTypeDefinition = typeDefinition;
                    }
                }

                return fixedTypeDefinition;
            }

            /// <summary>
            /// Splits the specified string into tokens representing pairs of opening and closing square brackets.
            /// Does not identify nested tokens, that is, pairs of square brackets inside the found tokens.
            /// </summary>
            /// <param name="value">The string to split.</param>
            /// <returns>A list of <see cref="SquareBracketsToken"/> that contain information about the tokens found.</returns>
            private List<SquareBracketsToken> SplitInSquareBracketSurroundedTokens(string value)
            {
                List<SquareBracketsToken> tokens = new List<SquareBracketsToken>();

                int firstOpenBracketIndex = -1;
                int openedBrackets = 0;
                int closedBrackets = 0;
                for (int i = 0; i < value.Length; i++)
                {
                    if (value[i] == '[')
                    {
                        if (openedBrackets == 0)
                        {
                            firstOpenBracketIndex = i;
                        }

                        openedBrackets++;
                    }
                    else if (value[i] == ']')
                    {
                        closedBrackets++;
                    }

                    if (openedBrackets != 0 && openedBrackets == closedBrackets)
                    {
                        int tokenStartIndex = firstOpenBracketIndex;
                        int tokenEndIndex = i;
                        string tokenContent = value.Substring(tokenStartIndex + 1, tokenEndIndex - tokenStartIndex - 1);
                        tokens.Add(new SquareBracketsToken(tokenStartIndex, tokenEndIndex, tokenContent));

                        firstOpenBracketIndex = -1;
                        openedBrackets = 0;
                        closedBrackets = 0;
                    }
                }

                return tokens;
            }

            /// <summary>
            /// Contains information about the text enclosed between an opening and a closing square bracket.
            /// </summary>
            private class SquareBracketsToken
            {
                /// <summary>
                /// Initializes a new instance of the <see cref="SquareBracketsToken"/> class.
                /// </summary>
                /// <param name="startIndex">The index where the token starts in the parent string (the index of the opening bracket).</param>
                /// <param name="endIndex">The index where the token ends in the parent string (the index of the closing bracket).</param>
                /// <param name="content">The string content of the token, excluding the enclosing square brackets.</param>
                public SquareBracketsToken(int startIndex, int endIndex, string content)
                {
                    this.StartIndex = startIndex;
                    this.EndIndex = endIndex;
                    this.Content = content;
                }

                /// <summary>
                /// Gets the index where the token starts in the parent string (the index of the opening bracket).
                /// </summary>
                public int StartIndex { get; private set; }

                /// <summary>
                /// Gets the index where the token ends in the parent string (the index of the closing bracket).
                /// </summary>
                public int EndIndex { get; private set; }

                /// <summary>
                /// Gets the string content of the token, excluding the enclosing square brackets.
                /// </summary>
                public string Content { get; private set; }
            }
        }

        #endregion Inner classes
    }
}
