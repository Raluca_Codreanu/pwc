﻿namespace ZPKTool.Business.Export
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Contains information about the exported data.
    /// </summary>
    [Serializable]
    public class ExportMetadata
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExportMetadata"/> class.
        /// </summary>
        public ExportMetadata()
        {
        }

        /// <summary>
        /// Gets or sets the format version of an exported file.
        /// </summary>        
        public decimal ExportVersion { get; set; }

        /// <summary>
        /// Gets or sets the version of the application that exported the data.
        /// The version format is the one used by .NET for assembly versioning (major.minor.build.revision)
        /// </summary>        
        public string AppVersion { get; set; }
    }
}
