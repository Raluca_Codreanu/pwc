﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.Business.Export
{
    /// <summary>
    /// Exposes the data imported from a file during an import operation, along with error information.
    /// </summary>
    /// <typeparam name="TEntity">The type of the imported entity.</typeparam>
    public interface ImportedData<out TEntity>
    {
        /// <summary>
        /// Gets the entity imported.
        /// </summary>
        TEntity Entity { get; }

        /// <summary>
        /// Gets the exception that occurred during import.
        /// </summary>            
        Exception Error { get; }

        /// <summary>
        /// Gets the cost calculation data that was imported with the entity. It can be null because it is not exported for all types of entities.
        /// This is normally used to calculate the entity's cost when the entity is not attached to a parent.
        /// </summary>        
        ExportedCalculationData CostCalculationData { get; }

        /// <summary>
        /// Gets the currencies data that was imported with the entity. It can be null because it is not exported for all types of entities.
        /// </summary>
        ICollection<Currency> Currencies { get; }

        /// <summary>
        /// Gets the base currency that was imported with the entity. It can be null because it is not exported for all types of entities.
        /// </summary>
        Currency BaseCurrency { get; }

        /// <summary>
        /// Gets the media collection. An item of this dictionary contains the Id of the parent entity (the Key) and the list of ImportedMedia objects which
        /// which represent the media of the given entity (the Value).
        /// </summary>
        Dictionary<Guid, IEnumerable<ImportedMedia>> MediaCollection { get; }

        /// <summary>
        /// Gets the path to the imported entity file.
        /// </summary>
        string SourceFile { get; }
    }
}
