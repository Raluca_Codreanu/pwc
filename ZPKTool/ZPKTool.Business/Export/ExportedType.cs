﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.Export
{
    /// <summary>
    /// Represents the mapping of a data type for export.
    /// </summary>
    public class ExportedType
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExportedType"/> class.
        /// </summary>
        public ExportedType()
        {
            this.Aliases = new List<Alias>();
            this.Values = new List<Value>();
            this.Collections = new List<Collection>();
        }

        /// <summary>
        /// Gets or sets the name of the data type mapped for export.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the name of the class used for converting older versions of this Value to the current version.
        /// This class must implement ZPKTool.Business.Export.IValueConverter and must be public.
        /// </summary>
        /// <value>The converter.</value>
        public string Converter { get; set; }

        /// <summary>
        /// Gets the aliases of the data type.
        /// </summary>        
        public ICollection<Alias> Aliases { get; private set; }

        /// <summary>
        /// Gets the single-value mappings.
        /// </summary>
        public ICollection<Value> Values { get; private set; }

        /// <summary>
        /// Gets the collection mappings.
        /// </summary>
        public ICollection<Collection> Collections { get; private set; }
    }
}
