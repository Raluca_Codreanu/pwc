namespace ZPKTool.Business.Export
{
    using System;

    /// <summary>
    /// Contains data that was exported with a model and which is necessary for calculating the cost of the model when is displayed by a "viewer" component.
    /// </summary>
    [Serializable]
    public class ExportedCalculationData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExportedCalculationData"/> class.
        /// </summary>
        public ExportedCalculationData()
        {
        }

        /// <summary>
        /// Gets or sets the depreciation period.
        /// </summary>
        public int DepreciationPeriod { get; set; }

        /// <summary>
        /// Gets or sets the depreciation rate (or interest rate).
        /// </summary>
        public decimal DepreciationRate { get; set; }

        /// <summary>
        /// Gets or sets the logistic cost ratio.
        /// </summary>        
        public decimal LogisticCostRatio { get; set; }

        /// <summary>
        /// Gets or sets the yearly production quantity.
        /// </summary>
        public decimal? YearlyProductionQuantity { get; set; }

        /// <summary>
        /// Gets or sets the parent project create date.
        /// </summary>
        public DateTime? ProjectCreateDate { get; set; }
    }
}