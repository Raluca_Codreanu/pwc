﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.Business.Export
{
    /// <summary>
    /// Contains information about the imported media objects
    /// </summary>
    public class ImportedMedia
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ImportedMedia"/> class.
        /// </summary>
        /// <param name="sourceMedia">The source media.</param>
        /// <param name="cachedFilePath">The cached file path.</param>
        public ImportedMedia(Media sourceMedia, string cachedFilePath)
        {
            if (sourceMedia == null)
            {
                throw new ArgumentNullException("sourceMedia", "The source media can not be null");
            }

            CachedFilePath = cachedFilePath;
            OriginalFileName = sourceMedia.OriginalFileName;
            Type = (MediaType)Enum.ToObject(typeof(MediaType), sourceMedia.Type);
        }

        /// <summary>
        /// Gets the cached file path.
        /// </summary>
        /// <value>
        /// The cached file path.
        /// </value>
        public string CachedFilePath { get; private set; }

        /// <summary>
        /// Gets the name of the original file.
        /// </summary>
        /// <value>
        /// The name of the original file.
        /// </value>
        public string OriginalFileName { get; private set; }

        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public MediaType Type { get; private set; }
    }
}
