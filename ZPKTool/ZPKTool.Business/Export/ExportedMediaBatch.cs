﻿namespace ZPKTool.Business.Export
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Contains a batch of ExportedMedia objects.
    /// </summary>
    [Serializable]
    public class ExportedMediaBatch
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExportedMediaBatch"/> class.
        /// </summary>
        public ExportedMediaBatch()
        {
            this.Media = new Collection<ExportedMedia>();
        }

        /// <summary>
        /// Gets the media batch.
        /// </summary>
        public Collection<ExportedMedia> Media { get; private set; }
    }
}
