﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.Export
{
    /// <summary>
    /// Represents the mapping for export of a single-value member (field/property) of data type.
    /// </summary>
    public class Value
    {        
        /// <summary>
        /// Initializes a new instance of the <see cref="Value"/> class.
        /// </summary>
        public Value()
        {
            this.Aliases = new List<Alias>();
        }

        /// <summary>
        /// Gets or sets the name of the member.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the type of the member.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the method used to expose this member.
        /// </summary>        
        public MemberExposureType ExposedAs { get; set; }
                
        /// <summary>
        /// Gets the aliases of the member.
        /// </summary>        
        public ICollection<Alias> Aliases { get; private set; }
    }
}
