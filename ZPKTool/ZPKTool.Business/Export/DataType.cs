﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.Export
{
    /// <summary>
    /// The types of DataPieces
    /// </summary>
    public enum DataType
    {
        /// <summary>
        /// Represents a field.
        /// </summary>
        Field = 0,

        /// <summary>
        /// Represents a property.
        /// </summary>
        Property = 1,

        /// <summary>
        /// The represents a collection.
        /// </summary>
        Collection = 2
    }
}
