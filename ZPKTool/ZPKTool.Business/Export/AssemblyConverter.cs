﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.Business.Export
{
    /// <summary>
    /// Perform necessary conversions during the import of an Assembly.
    /// </summary>
    public class AssemblyConverter : IExportedValueConverter
    {
        /// <summary>
        /// Converts the specified value to the specified type.
        /// </summary>
        /// <param name="fieldName">Name of the field.</param>
        /// <param name="value">The value to convert.</param>
        /// <param name="targetType">The type to which to convert.</param>
        /// <returns>The converted value.</returns>
        public object Convert(string fieldName, object value, Type targetType)
        {
            if (fieldName == "AssemblingCountry" && value is Country)
            {
                Country country = (Country)value;
                return country.Name;
            }
            else if (fieldName == "AssemblingState" && value is CountryState)
            {
                CountryState state = (CountryState)value;
                return state.Name;
            }
            else
            {
                return value;
            }
        }
    }
}
