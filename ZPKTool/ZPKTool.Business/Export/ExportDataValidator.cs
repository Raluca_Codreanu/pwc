﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Common;
using ZPKTool.Data;

namespace ZPKTool.Business.Export
{
    /// <summary>
    /// Implements the validation for data objects (entities) and <see cref="ExportedObject"/> instances used
    /// to heal a model during import or export.
    /// </summary>
    public class ExportDataValidator : IExportDataValidator
    {
        /// <summary>
        /// Validates the specified <see cref="ExportedObject" /> during import.
        /// </summary>
        /// <param name="exportedObject">The exported object.</param>
        /// <param name="dataObjectType">The type of the data object represented by <paramref name="exportedObject" />.</param>
        /// <returns>
        /// True if the exported object is valid; otherwise false.
        /// </returns>
        /// <remarks>
        /// The method tests the following cases of invalid data being contained in en exported model:        
        ///   - objects with the IsDeleted flag set to true; they should not be imported because they will never be visible again.        
        ///   - ProcessStepAssemblyAmount instances with null value for the Assembly property or with the Assembly property not exported at all;
        ///   they should not be imported because they cause referential integrity errors when saved in the database.        
        ///   - ProcessStepPartAmount instances with null value for the Part property or with the Part property not exported at all;
        ///   they should not be imported because the cause referential integrity errors when saved in the database.
        /// </remarks>
        public bool ValidateExportedObject(ExportedObject exportedObject, Type dataObjectType)
        {
            if (exportedObject == null)
            {
                return true;
            }

            // Objects that were deleted to trash bin (have the IsDeleted flag set) should not be imported.
            string isDeletedPropName = ReflectionUtils.GetPropertyName<ITrashable>(trashable => trashable.IsDeleted);
            DataPiece isDeletedPiece =
                exportedObject.Data.FirstOrDefault(d => d.Type == DataType.Property && d.Key.Equals(isDeletedPropName, StringComparison.OrdinalIgnoreCase));
            if (isDeletedPiece != null && isDeletedPiece.Value is bool && (bool)isDeletedPiece.Value)
            {
                return false;
            }

            // If the assembly or part associated with a step amount is deleted then the amount should not be imported; this is necessary because
            // the ProcessStepAssemblyAmount and ProcessStepPartAmount do not have IsDeleted flag.
            // This code also has to account for some cases when the Assembly/Part property of the amount was not exported at all, not even with a null value.
            if (dataObjectType.Name == typeof(ProcessStepAssemblyAmount).Name)
            {
                string assemblyPropertyName = ReflectionUtils.GetPropertyName<ProcessStepAssemblyAmount>(a => a.Assembly);
                var assyRefPiece = exportedObject.Data.FirstOrDefault(d => d.Key.Equals(assemblyPropertyName, StringComparison.OrdinalIgnoreCase));
                if (assyRefPiece == null || assyRefPiece.Value == null)
                {
                    return false;
                }
            }
            else if (dataObjectType.Name == typeof(ProcessStepPartAmount).Name)
            {
                string partPropertyName = ReflectionUtils.GetPropertyName<ProcessStepPartAmount>(a => a.Part);
                var partRefPiece = exportedObject.Data.FirstOrDefault(d => d.Key.Equals(partPropertyName, StringComparison.OrdinalIgnoreCase));
                if (partRefPiece == null || partRefPiece.Value == null)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Validates the specified data object during export.
        /// </summary>
        /// <param name="dataObject">The data object.</param>
        /// <returns>
        /// True if the data object is valid, false otherwise.
        /// </returns>
        /// <remarks>
        /// This methods implements the healing of a data model before is exported and prevents the invalid data healed by <see cref="ValidateExportedObject"/>
        /// from being created again.
        /// </remarks>
        public bool ValidateDataObject(object dataObject)
        {
            // Objects that were deleted to trash bin (have the IsDeleted flag set) must not be exported.
            var trashableObject = dataObject as ITrashable;
            if (trashableObject != null && trashableObject.IsDeleted)
            {
                return false;
            }

            // If the assembly or part associated with a step amount is deleted, that is, is deleted to the trash bin or no longer exists in the database,
            // then the amount should not be exported; this is necessary because the assembly or part will not be exported too.
            var assemblyAmount = dataObject as ProcessStepAssemblyAmount;
            if (assemblyAmount != null)
            {
                if (assemblyAmount.Assembly == null || assemblyAmount.Assembly.IsDeleted)
                {
                    return false;
                }
            }
            else
            {
                var partAmount = dataObject as ProcessStepPartAmount;
                if (partAmount != null)
                {
                    if (partAmount.Part == null || partAmount.Part.IsDeleted)
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}