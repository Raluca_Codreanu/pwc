﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.Data;
using ZPKTool.DataAccess;

namespace ZPKTool.Business
{
    /// <summary>
    /// Creates a deep copy of an object. All the sub-objects owned by it are cloned while the object that are shared with others are just referenced.
    /// </summary>
    public class CloneManager
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The type of the shared entities which are linked to cloned objects during cloning (the shared objects are not cloned).
        /// This list of types should be kept in sync with the types handled by the <see cref="CloneManager.RetrieveReferenceForClone"/> method.
        /// <para />
        /// Currently it is used to skip objects of these types when parsing an entity's graph.
        /// </summary>
        private static readonly List<Type> sharedEntityTypes = new List<Type>()
        {
            typeof(MeasurementUnit),
            typeof(MaterialsClassification),
            typeof(MachinesClassification),
            typeof(ProcessStepsClassification),
            typeof(User),
            typeof(RawMaterialDeliveryType)
        };

        /// <summary>
        /// The data source manager associated with the object(s) that will be cloned. It is null for objects that do not belong to a data manager (are detached).
        /// </summary>
        private IDataSourceManager sourceDataManager;

        /// <summary>
        /// The data source manager that will be associated with the cloned object(s).
        /// </summary>
        private IDataSourceManager cloneDataManager;

        /// <summary>
        /// A value indicating whether this instance is used to clone objects for an Import operation.
        /// Currently it affects the cloning of Media.
        /// </summary>
        private bool cloneForImport;

        /// <summary>
        /// Initializes a new instance of the <see cref="CloneManager" /> class.
        /// Use this constructor when you need to clone objects without media.
        /// </summary>
        public CloneManager()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CloneManager" /> class.
        /// Use this constructor when you need to clone objects that do not belong to a data manager (are detached)
        /// or that are fully loaded (and the manager does not have to load anything for them).
        /// </summary>
        /// <param name="cloneDataManager">The data manager that will be associated with the cloned object(s).</param>
        /// <exception cref="System.ArgumentNullException">The clone data manager was null.</exception>
        public CloneManager(IDataSourceManager cloneDataManager)
        {
            if (cloneDataManager == null)
            {
                throw new ArgumentNullException("cloneDataManager", "The clone data manager was null.");
            }

            this.cloneDataManager = cloneDataManager;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CloneManager" /> class.
        /// Use this constructor when you need to clone objects that do not belong to a data manager (are detached)
        /// or that are fully loaded (and the manager does not have to load anything for them).
        /// </summary>
        /// <param name="cloneDataManager">The data manager that will be associated with the cloned object(s).</param>
        /// <param name="cloneForImport">Indicates whether this instance will be used for an Import operation.</param>
        /// <exception cref="System.ArgumentNullException">The clone data manager was null.</exception>
        public CloneManager(IDataSourceManager cloneDataManager, bool cloneForImport)
            : this(cloneDataManager)
        {
            this.cloneForImport = cloneForImport;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CloneManager" /> class.
        /// </summary>
        /// <param name="sourceDataManager"
        /// >The data manager associated with the object(s) that will be cloned. It can be null for objects that do not belong to a data manager (are detached).
        /// </param>
        /// <param name="cloneDataManager">The data manager that will be associated with the cloned object(s).</param>
        /// <exception cref="System.ArgumentNullException">The clone data manager was null.</exception>
        public CloneManager(IDataSourceManager sourceDataManager, IDataSourceManager cloneDataManager)
        {
            if (cloneDataManager == null)
            {
                throw new ArgumentNullException("cloneDataManager", "The clone data manager was null.");
            }

            this.sourceDataManager = sourceDataManager;
            this.cloneDataManager = cloneDataManager;
        }

        /// <summary>
        /// Gets the clone of cycle time calculation.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>A clone of the cycle time calculation or NULL if it was null.</returns>
        public static CycleTimeCalculation Clone(CycleTimeCalculation source)
        {
            if (source == null)
            {
                return null;
            }

            CycleTimeCalculation clone = new CycleTimeCalculation();
            source.CopyValuesTo(clone);

            return clone;
        }

        /// <summary>
        /// Gets the clone of project folder.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>The clone of the project folder.</returns>
        public ProjectFolder Clone(ProjectFolder source)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source", "The folder to clone was null.");
            }

            ProjectFolder clone = new ProjectFolder();
            source.CopyValuesTo(clone);

            foreach (ProjectFolder item in source.ChildrenProjectFolders.Where(p => !p.IsDeleted))
            {
                ProjectFolder projectFolder = this.Clone(item);
                projectFolder.ParentProjectFolder = clone;
                clone.ChildrenProjectFolders.Add(projectFolder);
            }

            foreach (Project item in source.Projects.Where(p => !p.IsDeleted))
            {
                Project project = this.Clone(item);
                project.ProjectFolder = clone;
                clone.Projects.Add(project);
            }

            return clone;
        }

        /// <summary>
        /// Gets the clone of project.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>The clone of the project or NULL if it was null.</returns>
        public Project Clone(Project source)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source", "The project to clone was null.");
            }

            Project clone = new Project();
            source.CopyValuesTo(clone);

            foreach (var currency in source.Currencies)
            {
                Currency currencyClone = this.Clone(currency);
                clone.Currencies.Add(currencyClone);
            }

            if (clone.Currencies != null)
            {
                clone.BaseCurrency = clone.Currencies.FirstOrDefault(c => c.IsSameAs(source.BaseCurrency));
            }

            this.UpdateSharedReferences(source, clone, false);

            if (source.Customer != null)
            {
                var customer = this.Clone(source.Customer);
                customer.Projects.Add(clone);
                clone.Customer = customer;
            }

            if (source.OverheadSettings != null)
            {
                var settings = source.OverheadSettings.Copy();
                settings.Projects.Add(clone);
                clone.OverheadSettings = settings;
            }

            this.CloneMediaCollectionOfEntity(source, clone, true);

            foreach (Part part in source.Parts.Where(p => !p.IsDeleted))
            {
                Part partClone = this.Clone(part);
                partClone.Project = clone;
                clone.Parts.Add(partClone);
            }

            foreach (Assembly assy in source.Assemblies.Where(a => !a.IsDeleted))
            {
                Assembly assemblyClone = this.Clone(assy);
                assemblyClone.Project = clone;
                clone.Assemblies.Add(assemblyClone);
            }

            return clone;
        }

        /// <summary>
        /// Gets the clone of supplier.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>The clone of the supplier or NULL if it was null.</returns>
        public Customer Clone(Customer source)
        {
            if (source == null)
            {
                return null;
            }

            Customer clone = new Customer();
            source.CopyValuesTo(clone);

            return clone;
        }

        /// <summary>
        /// Gets the clone of a part.
        /// </summary>
        /// <param name="part">The part to clone.</param>
        /// <param name="keepId">if set to true the clone will have the same Id as the source.</param>
        /// <returns>A clone of the part, or NULL if the part is null.</returns>
        public Part Clone(Part part, bool keepId = false)
        {
            return Clone(part, true, keepId);
        }

        /// <summary>
        /// Gets the clone of a part.
        /// </summary>
        /// <param name="part">The part to clone.</param>
        /// <param name="cloneVids">Specifies if the videos are to be cloned</param>
        /// <param name="keepId">if set to true the clone will have the same Id as the source.</param>
        /// <returns>A clone of the part, or NULL if the part is null.</returns>
        public Part Clone(Part part, bool cloneVids, bool keepId)
        {
            if (part == null)
            {
                return null;
            }

            Part clone;

            if (part is RawPart)
            {
                clone = new RawPart();
            }
            else
            {
                clone = new Part();
            }

            part.CopyValuesTo(clone);

            if (keepId)
            {
                clone.Guid = part.Guid;
            }

            foreach (var calculation in part.TransportCostCalculations)
            {
                clone.TransportCostCalculations.Add(Clone(calculation));
            }

            if (part.CountrySettings != null)
            {
                var countrySettings = part.CountrySettings.Copy();
                countrySettings.Parts.Add(clone);
                clone.CountrySettings = countrySettings;
            }

            if (part.OverheadSettings != null)
            {
                var overheadSettings = part.OverheadSettings.Copy();
                overheadSettings.Parts.Add(clone);
                clone.OverheadSettings = overheadSettings;
            }

            if (part.Manufacturer != null)
            {
                var manufacturer = this.Clone(part.Manufacturer);
                manufacturer.Parts.Add(clone);
                clone.Manufacturer = manufacturer;
            }

            this.UpdateSharedReferences(part, clone, false);
            this.CloneMediaCollectionOfEntity(part, clone, cloneVids);

            foreach (RawMaterial material in part.RawMaterials.Where(m => !m.IsDeleted))
            {
                RawMaterial rawMaterialClone = this.Clone(material, keepId);
                rawMaterialClone.Part = clone;
                clone.RawMaterials.Add(rawMaterialClone);
            }

            foreach (Commodity commodity in part.Commodities.Where(c => !c.IsDeleted))
            {
                Commodity commodityClone = this.Clone(commodity, keepId);
                commodityClone.Part = clone;
                clone.Commodities.Add(commodityClone);
            }

            if (part.Process != null)
            {
                var process = this.Clone(part.Process, new Dictionary<Guid, Assembly>(), new Dictionary<Guid, Part>(), keepId);
                process.Parts.Add(clone);
                clone.Process = process;
            }

            if (part.RawPart != null)
            {
                var rawPartClone = this.Clone(part.RawPart, cloneVids, keepId);
                rawPartClone.ParentOfRawPart.Add(clone);
                clone.RawPart = rawPartClone;
            }

            return clone;
        }

        /// <summary>
        /// Gets the clone of assembly.
        /// </summary>
        /// <param name="assembly">The assembly to clone.</param>
        /// <param name="keepId">if set to true the clone will have the same Id as the source.</param>
        /// <returns>A clone of the assembly, or NULL if the assembly is null.</returns>
        public Assembly Clone(Assembly assembly, bool keepId = false)
        {
            return Clone(assembly, true, keepId);
        }

        /// <summary>
        /// Gets the clone of assembly.
        /// </summary>
        /// <param name="assembly">The assembly to clone.</param>
        /// <param name="cloneVids">Specifies if the videos are to be cloned</param>
        /// <param name="keepId">if set to true the clone will have the same Id as the source.</param>
        /// <returns>A clone of the assembly, or NULL if the assembly is null.</returns>
        public Assembly Clone(Assembly assembly, bool cloneVids, bool keepId)
        {
            if (assembly == null)
            {
                return null;
            }

            Assembly clone = new Assembly();
            assembly.CopyValuesTo(clone);

            if (keepId)
            {
                clone.Guid = assembly.Guid;
            }

            foreach (var calculation in assembly.TransportCostCalculations)
            {
                clone.TransportCostCalculations.Add(Clone(calculation));
            }

            if (assembly.CountrySettings != null)
            {
                var settings = assembly.CountrySettings.Copy();
                settings.Assemblies.Add(clone);
                clone.CountrySettings = settings;
            }

            if (assembly.OverheadSettings != null)
            {
                var settings = assembly.OverheadSettings.Copy();
                settings.Assemblies.Add(clone);
                clone.OverheadSettings = settings;
            }

            if (assembly.Manufacturer != null)
            {
                var manufacturer = this.Clone(assembly.Manufacturer);
                manufacturer.Assemblies.Add(clone);
                clone.Manufacturer = manufacturer;
            }

            this.CloneMediaCollectionOfEntity(assembly, clone, cloneVids);
            this.UpdateSharedReferences(assembly, clone, false);

            Dictionary<Guid, Part> clonedSubParts = new Dictionary<Guid, Part>();
            foreach (Part part in assembly.Parts.Where(p => !p.IsDeleted))
            {
                Part partClone = this.Clone(part, cloneVids, keepId);
                partClone.Assembly = clone;
                clone.Parts.Add(partClone);

                clonedSubParts.Add(part.Guid, partClone);
            }

            Dictionary<Guid, Assembly> clonedSubAssemblies = new Dictionary<Guid, Assembly>();
            foreach (Assembly subAssy in assembly.Subassemblies.Where(a => !a.IsDeleted))
            {
                Assembly assemblyClone = this.Clone(subAssy, cloneVids, keepId);
                assemblyClone.ParentAssembly = clone;
                clone.Subassemblies.Add(assemblyClone);

                clonedSubAssemblies.Add(subAssy.Guid, assemblyClone);
            }

            if (assembly.Process != null)
            {
                var process = this.Clone(assembly.Process, clonedSubAssemblies, clonedSubParts, keepId);
                process.Assemblies.Add(clone);
                clone.Process = process;
            }

            return clone;
        }

        /// <summary>
        /// Gets the clone of manufacturer.
        /// </summary>
        /// <param name="source">The source to clone.</param>
        /// <returns>The clone of the manufacturer, or NULL if it was null.</returns>
        public Manufacturer Clone(Manufacturer source)
        {
            if (source == null)
            {
                return null;
            }

            Manufacturer clone = new Manufacturer();
            source.CopyValuesTo(clone);

            return clone;
        }

        /// <summary>
        /// Gets the clone of a raw material.
        /// </summary>
        /// <param name="rawMaterial">The raw material to clone.</param>
        /// <param name="keepId">if set to true the clone will have the same Id as the source.</param>
        /// <returns>The clone of the raw material, or NULL if the raw material was null.</returns>
        public RawMaterial Clone(RawMaterial rawMaterial, bool keepId = false)
        {
            if (rawMaterial == null)
            {
                return null;
            }

            RawMaterial clone = new RawMaterial();
            rawMaterial.CopyValuesTo(clone);

            if (keepId)
            {
                clone.Guid = rawMaterial.Guid;
            }

            if (rawMaterial.Manufacturer != null)
            {
                var manufacturer = Clone(rawMaterial.Manufacturer);
                manufacturer.RawMaterials.Add(clone);
                clone.Manufacturer = manufacturer;
            }

            this.CloneMediaOfEntity(rawMaterial, clone);
            this.UpdateSharedReferences(rawMaterial, clone, false);

            return clone;
        }

        /// <summary>
        /// Gets the clone of a commodity.
        /// </summary>
        /// <param name="commodity">The commodity to clone.</param>
        /// <param name="keepId">if set to true the clone will have the same Id as the source.</param>
        /// <returns>The clone of the commodity, or NULL if the commodity is null.</returns>
        public Commodity Clone(Commodity commodity, bool keepId = false)
        {
            if (commodity == null)
            {
                return null;
            }

            Commodity clone = new Commodity();
            commodity.CopyValuesTo(clone);

            if (keepId)
            {
                clone.Guid = commodity.Guid;
            }

            if (commodity.Manufacturer != null)
            {
                var manufacturer = this.Clone(commodity.Manufacturer);
                manufacturer.Commodities.Add(clone);
                clone.Manufacturer = manufacturer;
            }

            this.CloneMediaOfEntity(commodity, clone);
            this.UpdateSharedReferences(commodity, clone, false);

            return clone;
        }

        /// <summary>
        /// Gets the clone of a consumable.
        /// </summary>
        /// <param name="consumable">The consumable top clone.</param>
        /// <param name="keepId">if set to true the clone will have the same Id as the source.</param>
        /// <returns>The clone of the consumable or NULL if it was null.</returns>
        public Consumable Clone(Consumable consumable, bool keepId = false)
        {
            if (consumable == null)
            {
                return null;
            }

            Consumable clone = new Consumable();
            consumable.CopyValuesTo(clone);

            if (keepId)
            {
                clone.Guid = consumable.Guid;
            }

            this.UpdateSharedReferences(consumable, clone, false);

            if (consumable.Manufacturer != null)
            {
                var manufacturer = this.Clone(consumable.Manufacturer);
                manufacturer.Consumables.Add(clone);
                clone.Manufacturer = manufacturer;
            }

            return clone;
        }

        /// <summary>
        /// Gets the clone of the die.
        /// </summary>
        /// <param name="die">The die to clone.</param>
        /// <param name="keepId">if set to true the clone will have the same Id as the source.</param>
        /// <returns>The clone of the die or NULL if it was null</returns>
        public Die Clone(Die die, bool keepId = false)
        {
            if (die == null)
            {
                return null;
            }

            Die clone = new Die();
            die.CopyValuesTo(clone);

            if (keepId)
            {
                clone.Guid = die.Guid;
            }

            if (die.Manufacturer != null)
            {
                var manufacturer = this.Clone(die.Manufacturer);
                manufacturer.Dies.Add(clone);
                clone.Manufacturer = manufacturer;
            }

            this.CloneMediaOfEntity(die, clone);

            return clone;
        }

        /// <summary>
        /// Gets the clone of machine.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="keepId">if set to true the clone will have the same Id as the source.</param>
        /// <returns>The clone of the machine or NULL if it was null.</returns>
        public Machine Clone(Machine source, bool keepId = false)
        {
            if (source == null)
            {
                return null;
            }

            Machine clone = new Machine();
            source.CopyValuesTo(clone);

            if (keepId)
            {
                clone.Guid = source.Guid;
            }

            if (source.Manufacturer != null)
            {
                var manufacturer = this.Clone(source.Manufacturer);
                manufacturer.Machines.Add(clone);
                clone.Manufacturer = manufacturer;
            }

            this.UpdateSharedReferences(source, clone, false);
            this.CloneMediaOfEntity(source, clone);

            return clone;
        }

        /// <summary>
        /// Gets the clone of the measurement unit.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>The clone of the measurement unit or NULL if it was null.</returns>
        public MeasurementUnit Clone(MeasurementUnit source)
        {
            if (source == null)
            {
                return null;
            }

            MeasurementUnit clone = new MeasurementUnit();
            source.CopyValuesTo(clone);

            this.UpdateSharedReferences(source, clone, false);

            return clone;
        }

        /// <summary>
        /// Gets the clone of the materials classification.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>The clone of the materials classification or NULL if it was null.</returns>
        public MaterialsClassification Clone(MaterialsClassification source)
        {
            if (source == null)
            {
                return null;
            }

            MaterialsClassification clone = new MaterialsClassification();
            source.CopyValuesTo(clone);

            this.UpdateSharedReferences(source, clone, false);

            return clone;
        }

        /// <summary>
        /// Gets the clone of the machines classification.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>The clone of the machines classification or NULL if it was null.</returns>
        public MachinesClassification Clone(MachinesClassification source)
        {
            if (source == null)
            {
                return null;
            }

            MachinesClassification clone = new MachinesClassification();
            source.CopyValuesTo(clone);

            this.UpdateSharedReferences(source, clone, false);

            return clone;
        }

        /// <summary>
        /// Gets the clone of the process steps classification.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>The clone of the process steps classification or NULL if it was null.</returns>
        public ProcessStepsClassification Clone(ProcessStepsClassification source)
        {
            if (source == null)
            {
                return null;
            }

            ProcessStepsClassification clone = new ProcessStepsClassification();
            source.CopyValuesTo(clone);

            this.UpdateSharedReferences(source, clone, false);

            return clone;
        }

        /// <summary>
        /// Gets the clone of the user.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>The clone of the user or NULL if it was null.</returns>
        public User Clone(User source)
        {
            if (source == null)
            {
                return null;
            }

            User clone = new User();
            source.CopyValuesTo(clone);

            this.UpdateSharedReferences(source, clone, false);

            return clone;
        }

        /// <summary>
        /// Gets the clone of the raw material delivery type.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>The clone of the raw material delivery type or NULL if it was null.</returns>
        public RawMaterialDeliveryType Clone(RawMaterialDeliveryType source)
        {
            if (source == null)
            {
                return null;
            }

            RawMaterialDeliveryType clone = new RawMaterialDeliveryType();
            source.CopyValuesTo(clone);

            this.UpdateSharedReferences(source, clone, false);

            return clone;
        }

        /// <summary>
        /// Gets the clone of the currency.
        /// </summary>
        /// <param name="source">The currency to clone.</param>
        /// <param name="keepId">If set to true, the clone will have the same Id as the source.</param>
        /// <returns>
        /// The clone of the currency or NULL if it was null.
        /// </returns>
        public Currency Clone(Currency source, bool keepId = false)
        {
            if (source == null)
            {
                return null;
            }

            var clone = new Currency();
            source.CopyValuesTo(clone);
            if (keepId)
            {
                clone.Guid = source.Guid;
            }

            this.UpdateSharedReferences(source, clone, false);

            return clone;
        }

        /// <summary>
        /// Gets the clone of transport cost calculation.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>A clone of transport cost calculation or NULL if it was null.</returns>
        public TransportCostCalculation Clone(TransportCostCalculation source)
        {
            if (source == null)
            {
                return null;
            }

            TransportCostCalculation clone = new TransportCostCalculation();
            clone.TransportCostCalculationSetting = Clone(source.TransportCostCalculationSetting);
            source.CopyValuesTo(clone);

            return clone;
        }

        /// <summary>
        /// Gets the clone of transport cost calculation setting.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>A clone of transport cost calculation setting or NULL if it was null.</returns>
        public TransportCostCalculationSetting Clone(TransportCostCalculationSetting source)
        {
            if (source == null)
            {
                return null;
            }

            TransportCostCalculationSetting clone = new TransportCostCalculationSetting();
            source.CopyValuesTo(clone);

            return clone;
        }

        #region MediaCloning

        /// <summary>
        /// Clone the media object of the entity
        /// </summary>
        /// <param name="source">Source entity with media object.</param>
        /// <param name="destination">Destination object.</param>
        private void CloneMediaOfEntity(IEntityWithMedia source, IEntityWithMedia destination)
        {
            // TODO: Relationship fix-up for the cloned Media is a bit hard here and is left out for now (setting the Media's reference to the parent)
            if (this.cloneForImport)
            {
                // Media must not be cloned during during an Import process because it leads to high memory consumption when cloning
                // an object graph containing many Media objects.
                // Move the media from source to destination through a temp variable because if the destination object is attached to a context, by sharing
                // the same media object, the source object will be implicitly attached to that context.
                Media media = source.Media;
                source.Media = null;
                destination.Media = media;
            }
            else if (source.Media != null)
            {
                // The source object's media collection is loaded -> clone its content
                destination.Media = source.Media.Copy();
            }
            else if (this.sourceDataManager != null)
            {
                // If the source object is not saved in the database yet, it is pointless to try to get its media from the database                
                var sourceRepository = this.sourceDataManager.Repository(source.GetType());
                if (sourceRepository.IsAdded(source))
                {
                    return;
                }

                try
                {
                    MediaManager mediaManager = new MediaManager(this.sourceDataManager);
                    Media sourceMedia = mediaManager.GetPictureOrVideo(source);
                    if (sourceMedia != null)
                    {
                        destination.Media = sourceMedia.Copy();
                    }
                }
                catch (DataAccessException ex)
                {
                    log.Error("Data error occurred during media cloning.", ex);
                    throw new BusinessException(ex);
                }
            }
        }

        /// <summary>
        /// Clone the media collection of the entity
        /// </summary>
        /// <param name="source">Source entity with media collection.</param>
        /// <param name="destination">Destination object.</param>
        /// <param name="cloneVids">Specifies if the videos are to be cloned</param>
        private void CloneMediaCollectionOfEntity(IEntityWithMediaCollection source, IEntityWithMediaCollection destination, bool cloneVids)
        {
            // TODO: Relationship fix-up for the cloned Media is a bit hard here and is left out for now (setting the Media's reference to the parent)

            // Media must not be cloned during during an Import process because it leads to high memory consumption when cloning
            // an object graph containing many Media objects.
            if (this.cloneForImport)
            {
                foreach (Media media in source.Media.ToList())
                {
                    if (media.Type == MediaType.Video && !cloneVids)
                    {
                        // video will not be cloned
                        continue;
                    }

                    // First we have to remove the media from source because if the destination object is attached to a context, by sharing the same media object,
                    // the source object will be implicitly attached to that context.
                    source.Media.Remove(media);
                    destination.Media.Add(media);
                }
            }
            else if (/*source.MediaCollection.IsLoaded ||*/ source.Media.Count > 0)
            {
                // The source object's media collection is loaded -> clone its content
                foreach (Media item in source.Media)
                {
                    if (item.Type == MediaType.Video && !cloneVids)
                    {
                        // video will not be cloned
                        continue;
                    }

                    destination.Media.Add(item.Copy());
                }
            }
            else if (this.sourceDataManager != null)
            {
                // If the source object is not saved in the database yet, it is pointless to try to get its media from the database                
                var sourceRepository = this.sourceDataManager.Repository(source.GetType());
                if (sourceRepository.IsAdded(source))
                {
                    return;
                }

                try
                {
                    MediaManager mediaManager = new MediaManager(this.sourceDataManager);
                    var mediaList = mediaManager.GetAllMedia(source);
                    foreach (Media media in mediaList)
                    {
                        if (media.Type == MediaType.Video && !cloneVids)
                        {
                            // video will not be cloned
                            continue;
                        }

                        destination.Media.Add(media.Copy());
                    }
                }
                catch (DataAccessException ex)
                {
                    log.Error("Data error occurred during media cloning.", ex);
                    throw new BusinessException(ex);
                }
            }
        }

        #endregion MediaCloning

        #region Process & ProcessStep

        /// <summary>
        /// Gets the clone of process step.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="cloneType">Represents the type the clone should have. This parameter is used to change the type of the clone Process Step.
        /// The type must derive from ProcessStep.
        /// If is null the clone will have the same type as the source.</param>
        /// <param name="keepId">if set to true the clone will have the same Id as the source.</param>
        /// <returns>The clone of the step or NULL if it was null.</returns>
        public ProcessStep Clone(ProcessStep source, Type cloneType, bool keepId = false)
        {
            return Clone(source, cloneType, false, null, null, keepId);
        }

        /// <summary>
        /// Gets the clone of a process.
        /// </summary>
        /// <param name="process">The process to clone.</param>
        /// <param name="parentSubAssemblyClones">
        /// The process parent's cloned sub-assemblies and the id of the original assemblies. Used for cloning ProcessStepAssemblyAmount objects.
        /// </param>
        /// <param name="parentSubPartClones">
        /// The process parent's cloned sub-parts and the id of the original parts. Used for cloning ProcessStepPartAmount objects.
        /// </param>
        /// <param name="keepId">if set to true the clone will have the same Id as the source.</param>
        /// <returns>Returns a clone of the Process or NULL if it was null.</returns>
        private Process Clone(
            Process process,
            Dictionary<Guid, Assembly> parentSubAssemblyClones,
            Dictionary<Guid, Part> parentSubPartClones,
            bool keepId = false)
        {
            if (process == null)
            {
                return null;
            }

            Process clone = new Process();
            foreach (ProcessStep item in process.Steps)
            {
                ProcessStep cloneStep = Clone(
                    item,
                    item.GetType(),
                    true,
                    parentSubAssemblyClones,
                    parentSubPartClones,
                    keepId);
                if (cloneStep != null)
                {
                    clone.Steps.Add(cloneStep);
                }
            }

            return clone;
        }

        /// <summary>
        /// Gets the clone of process step.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="cloneType">Represents the type the clone should have. This parameter is used to change the type of the clone Process Step.
        /// The type must derive from ProcessStep.
        /// If is null the clone will have the same type as the source.</param>
        /// <param name="clonePartAndAssemblyAmounts">
        /// Indicates whether to clone the AssemblyAmounts and the PartAmounts collections of the step. Usually, when cloning a Process the amounts are also cloned
        /// but when cloning individual steps to be used in other processes they are not.
        /// </param>
        /// <param name="parentSubAssemblyClones">
        /// The parent entity's cloned sub-assemblies and the id of the original assemblies. Used for cloning ProcessStepAssemblyAmount objects.
        /// </param>
        /// <param name="parentSubPartClones">
        /// The parent entity's cloned sub-parts and the id of the original parts. Used for cloning ProcessStepPartAmount objects.
        /// </param>
        /// <param name="keepId">if set to true the clone will have the same Id as the source.</param>
        /// <returns>The clone of the step or NULL if it was null.</returns>
        private ProcessStep Clone(
            ProcessStep source,
            Type cloneType,
            bool clonePartAndAssemblyAmounts,
            Dictionary<Guid, Assembly> parentSubAssemblyClones,
            Dictionary<Guid, Part> parentSubPartClones,
            bool keepId = false)
        {
            if (source == null)
            {
                return null;
            }

            if (cloneType == null)
            {
                cloneType = source.GetType();
            }

            ProcessStep clone = (ProcessStep)Activator.CreateInstance(cloneType);
            source.CopyValuesTo(clone);

            if (keepId)
            {
                clone.Guid = source.Guid;
            }

            this.UpdateSharedReferences(source, clone, false);

            if (clone is AssemblyProcessStep)
            {
                foreach (Commodity commodity in source.Commodities.Where(c => !c.IsDeleted))
                {
                    Commodity commodityClone = this.Clone(commodity, keepId);
                    commodityClone.ProcessStep = clone;
                    clone.Commodities.Add(commodityClone);
                }
            }

            foreach (Consumable consumable in source.Consumables.Where(c => !c.IsDeleted))
            {
                Consumable consumableClone = this.Clone(consumable, keepId);
                consumableClone.ProcessStep = clone;
                clone.Consumables.Add(consumableClone);
            }

            foreach (Die die in source.Dies.Where(d => !d.IsDeleted))
            {
                Die dieCLone = this.Clone(die, keepId);
                dieCLone.ProcessStep = clone;
                clone.Dies.Add(dieCLone);
            }

            foreach (Machine machine in source.Machines.Where(m => !m.IsDeleted))
            {
                Machine machineClone = this.Clone(machine, keepId);
                machineClone.ProcessStep = clone;
                clone.Machines.Add(machineClone);
            }

            foreach (CycleTimeCalculation calculation in source.CycleTimeCalculations)
            {
                CycleTimeCalculation calculationClone = Clone(calculation);
                calculationClone.ProcessStep = clone;
                clone.CycleTimeCalculations.Add(calculationClone);
            }

            // Clone Assembly and Part amounts which exist only in assembly processes.
            if (clonePartAndAssemblyAmounts && source is AssemblyProcessStep && clone is AssemblyProcessStep)
            {
                this.CloneAssemblyAndPartAmounts(
                    source as AssemblyProcessStep,
                    clone as AssemblyProcessStep,
                    parentSubAssemblyClones,
                    parentSubPartClones,
                    keepId);
            }

            this.CloneMediaOfEntity(source, clone);

            return clone;
        }

        /// <summary>
        /// Clones the assembly and part amounts of an assembly process step.
        /// </summary>
        /// <param name="step">The assembly process step.</param>
        /// <param name="stepClone">The assembly process step's clone, created without cloning the amounts.</param>
        /// <param name="parentSubAssemblyClones">
        /// The step parent entity's sub-assembly clones that were created in advance and will be attached to the amounts.
        /// </param>
        /// <param name="parentSubPartClones">
        /// The step parent's sub-part clones that were created in advance and will be attached to the amounts.
        /// </param>
        /// <param name="keepId">if set to true the clone will have the same Id as the source.</param> 
        private void CloneAssemblyAndPartAmounts(
            AssemblyProcessStep step,
            AssemblyProcessStep stepClone,
            Dictionary<Guid, Assembly> parentSubAssemblyClones,
            Dictionary<Guid, Part> parentSubPartClones,
            bool keepId = false)
        {
            foreach (ProcessStepPartAmount partAmount in step.PartAmounts)
            {
                // If the part referred by the process step part amount is deleted do not clone it
                if (partAmount.Part != null && partAmount.Part.IsDeleted)
                {
                    continue;
                }

                // Determine the id of the part referred to by partAmount. It is either the partAmount.PartGuid or partAmount.Part.Guid, depending on how
                // the parent entity was loaded.
                Guid partId = Guid.Empty;
                if (partAmount.PartGuid != Guid.Empty)
                {
                    partId = partAmount.PartGuid;
                }
                else if (partAmount.Part != null && partAmount.Part.Guid != Guid.Empty)
                {
                    partId = partAmount.Part.Guid;
                }
                else
                {
                    log.Error("Cloning: could not determine the id of a part referred by a ProcessStepPartAmount instance.");
                }

                // Get the cloned part to link to the cloned ProcessStepPartAmount.
                Part clonedPart;
                if (parentSubPartClones.TryGetValue(partId, out clonedPart))
                {
                    ProcessStepPartAmount amountClone = new ProcessStepPartAmount();
                    partAmount.CopyValuesTo(amountClone);

                    amountClone.Part = clonedPart;
                    clonedPart.ProcessStepPartAmounts.Add(amountClone);
                    amountClone.ProcessStep = stepClone;
                    stepClone.PartAmounts.Add(amountClone);

                    if (keepId)
                    {
                        amountClone.Guid = partAmount.Guid;
                    }
                }
                else
                {
                    log.Error("Could not find clone of part '{0}' in the parent's cloned sub-parts while cloning ProcessStepPartAmount.", partAmount.PartGuid);
                }
            }

            foreach (ProcessStepAssemblyAmount assyAmount in step.AssemblyAmounts)
            {
                // If the assembly referred by the process step assembly amount is deleted do not clone it
                if (assyAmount.Assembly != null && assyAmount.Assembly.IsDeleted)
                {
                    continue;
                }

                // Determine the id of the assembly referred to by assyAmount. It is either the assyAmount.AssemblyGuid or assyAmount.Assembly.Guid,
                // depending on how the parent entity was loaded.
                Guid assyId = Guid.Empty;
                if (assyAmount.AssemblyGuid != Guid.Empty)
                {
                    assyId = assyAmount.AssemblyGuid;
                }
                else if (assyAmount.Assembly != null && assyAmount.Assembly.Guid != Guid.Empty)
                {
                    assyId = assyAmount.Assembly.Guid;
                }
                else
                {
                    log.Error("Cloning: could not determine the id of an assembly referred by a ProcessStepAssemblyAmount instance.");
                }

                // Get the cloned assembly to link to the cloned ProcessStepAssemblyAmount.
                Assembly clonedAssy;
                if (parentSubAssemblyClones.TryGetValue(assyId, out clonedAssy))
                {
                    ProcessStepAssemblyAmount amountClone = new ProcessStepAssemblyAmount();
                    assyAmount.CopyValuesTo(amountClone);

                    amountClone.Assembly = clonedAssy;
                    clonedAssy.ProcessStepAssemblyAmounts.Add(amountClone);
                    amountClone.ProcessStep = stepClone;
                    stepClone.AssemblyAmounts.Add(amountClone);

                    if (keepId)
                    {
                        amountClone.Guid = assyAmount.Guid;
                    }
                }
                else
                {
                    log.Error("Could not find clone of assembly '{0}' in the parent's cloned sub-assemblies while cloning ProcessStepAssemblyAmount.", assyAmount.AssemblyGuid);
                }
            }
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Update the references to shared objects of a specified entity.
        /// <para />
        /// Specifically, it retrieves instances of the shared objects from the clone data context which then replace the corresponding objects from the entity. 
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="updateSubObjects">if set to true the update of shared references is applied to all objects in the target's graph.</param>
        public void UpdateSharedReferences(object entity, bool updateSubObjects)
        {
            this.InternalUpdateSharedReferences(entity, entity, updateSubObjects, new List<object>());
        }

        /// <summary>
        /// Update the references to shared objects of a specified "target" object by taking them from a source object.
        /// <para />
        /// This method is used in the cloning process.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="target">The target.</param>
        /// <param name="updateSubObjects">if set to true the update of shared references is applied to all objects in the target's graph.</param>
        private void UpdateSharedReferences(object source, object target, bool updateSubObjects)
        {
            this.InternalUpdateSharedReferences(source, target, updateSubObjects, new List<object>());
        }

        /// <summary>
        /// Update the references to shared objects of a specified "target" object by taking them from a source object.
        /// <para />
        /// The shared objects are not taken directly from the source object, they are retrieved from the database using the clone data context
        /// and based on their id in the source object.
        /// <para />
        /// The source and target can be the same object which has the effect of replacing the object's shared references with
        /// fresh instances retrieved from the clone data context.
        /// </summary>
        /// <param name="source">The source object.</param>
        /// <param name="target">The target object.</param>
        /// <param name="updateTargetSubObjects">if set to true the update of shared references is applied to all objects in the target's graph.</param>
        /// <param name="visitedTargetObjects">This parameter is used internally during recursion and it should be set to an empty list when called externally.        
        /// It is used to avoid infinite cycles when exploring the object graph of the <paramref name="target" /> parameter.</param>
        private void InternalUpdateSharedReferences(
            object source,
            object target,
            bool updateTargetSubObjects,
            List<object> visitedTargetObjects)
        {
            if (source == null || target == null)
            {
                return;
            }

            if (visitedTargetObjects.Contains(target))
            {
                return;
            }
            else
            {
                visitedTargetObjects.Add(target);
            }

            Project targetProject = target as Project;
            if (targetProject != null)
            {
                Project sourceProject = source as Project;
                targetProject.ProjectLeader = this.RetrieveReferenceForClone(sourceProject.ProjectLeader);
                targetProject.ResponsibleCalculator = this.RetrieveReferenceForClone(sourceProject.ResponsibleCalculator);
            }
            else
            {
                Part targetPart = target as Part;
                if (targetPart != null)
                {
                    Part sourcePart = source as Part;
                    targetPart.CalculatorUser = this.RetrieveReferenceForClone(sourcePart.CalculatorUser);
                    targetPart.MeasurementUnit = this.RetrieveReferenceForClone(sourcePart.MeasurementUnit);
                }
                else
                {
                    Assembly targetAssy = target as Assembly;
                    if (targetAssy != null)
                    {
                        Assembly sourceAssy = source as Assembly;
                        targetAssy.MeasurementUnit = this.RetrieveReferenceForClone(sourceAssy.MeasurementUnit);
                    }
                    else
                    {
                        RawMaterial targetMaterial = target as RawMaterial;
                        if (targetMaterial != null)
                        {
                            RawMaterial sourceMaterial = source as RawMaterial;

                            targetMaterial.DeliveryType = this.RetrieveReferenceForClone(sourceMaterial.DeliveryType);
                            targetMaterial.MaterialsClassificationL1 = this.RetrieveReferenceForClone(sourceMaterial.MaterialsClassificationL1);
                            targetMaterial.MaterialsClassificationL2 = this.RetrieveReferenceForClone(sourceMaterial.MaterialsClassificationL2);
                            targetMaterial.MaterialsClassificationL3 = this.RetrieveReferenceForClone(sourceMaterial.MaterialsClassificationL3);
                            targetMaterial.MaterialsClassificationL4 = this.RetrieveReferenceForClone(sourceMaterial.MaterialsClassificationL4);
                            targetMaterial.ParentWeightUnitBase = this.RetrieveReferenceForClone(sourceMaterial.ParentWeightUnitBase);
                            targetMaterial.PriceUnitBase = this.RetrieveReferenceForClone(sourceMaterial.PriceUnitBase);
                            targetMaterial.QuantityUnitBase = this.RetrieveReferenceForClone(sourceMaterial.QuantityUnitBase);
                        }
                        else
                        {
                            Commodity targetCommodity = target as Commodity;
                            if (targetCommodity != null)
                            {
                                Commodity sourceCommodity = source as Commodity;
                                targetCommodity.WeightUnitBase = this.RetrieveReferenceForClone(sourceCommodity.WeightUnitBase);
                            }
                            else
                            {
                                Consumable targetConsumable = target as Consumable;
                                if (targetConsumable != null)
                                {
                                    Consumable sourceConsumable = source as Consumable;

                                    targetConsumable.PriceUnitBase = this.RetrieveReferenceForClone(sourceConsumable.PriceUnitBase);
                                    targetConsumable.AmountUnitBase = this.RetrieveReferenceForClone(sourceConsumable.AmountUnitBase);
                                }
                                else
                                {
                                    Machine targetMachine = target as Machine;
                                    if (targetMachine != null)
                                    {
                                        Machine sourceMachine = source as Machine;

                                        targetMachine.ClassificationLevel4 = this.RetrieveReferenceForClone(sourceMachine.ClassificationLevel4);
                                        targetMachine.MainClassification = this.RetrieveReferenceForClone(sourceMachine.MainClassification);
                                        targetMachine.TypeClassification = this.RetrieveReferenceForClone(sourceMachine.TypeClassification);
                                        targetMachine.SubClassification = this.RetrieveReferenceForClone(sourceMachine.SubClassification);
                                    }
                                    else
                                    {
                                        ProcessStep targetStep = target as ProcessStep;
                                        if (targetStep != null)
                                        {
                                            ProcessStep sourceStep = source as ProcessStep;

                                            targetStep.Type = this.RetrieveReferenceForClone(sourceStep.Type);
                                            targetStep.SubType = this.RetrieveReferenceForClone(sourceStep.SubType);
                                            targetStep.CycleTimeUnit = this.RetrieveReferenceForClone(sourceStep.CycleTimeUnit);
                                            targetStep.ProcessTimeUnit = this.RetrieveReferenceForClone(sourceStep.ProcessTimeUnit);
                                            targetStep.SetupTimeUnit = this.RetrieveReferenceForClone(sourceStep.SetupTimeUnit);
                                            targetStep.MaxDownTimeUnit = this.RetrieveReferenceForClone(sourceStep.MaxDownTimeUnit);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (updateTargetSubObjects)
            {
                foreach (var child in this.GetChildEntities(target))
                {
                    this.InternalUpdateSharedReferences(child, child, updateTargetSubObjects, visitedTargetObjects);
                }
            }
        }

        /// <summary>
        /// Gets the child entities of a specified entity.
        /// </summary>
        /// <remarks>
        /// It get only the direct children of the specified entity, it does not return the children's children and so on.
        /// </remarks>
        /// <param name="entity">The entity.</param>
        /// <returns>The list of children.</returns>
        private List<object> GetChildEntities(object entity)
        {
            DataEntities dataContext = null;
            if (this.cloneDataManager != null)
            {
                dataContext = this.cloneDataManager.EntityContext;
            }
            else if (this.sourceDataManager != null)
            {
                dataContext = this.sourceDataManager.EntityContext;
            }
            else
            {
                throw new InvalidOperationException("Neither source or clone data manager is set. It is not possible to obtain entity-related information in this situation. Re-evaluate your usage of the cloning manager.");
            }

            List<object> children = new List<object>();
            Type entityType = entity.GetType();

            IEnumerable<string> referenceProperties;
            IEnumerable<string> collectionProperties;
            dataContext.GetNavigationProperties(entity, out referenceProperties, out collectionProperties);

            // Retrieve the objects pointed to by the navigation properties. Also, ignore the navigation properties that lead to shared objects
            // (like User, MeasurementUnit, classifications, etc.) because they lead the entity graph parsing through entity graphs that are linked
            // to the shared objects right now in the db context but have no relationship with the entity being cloned.
            foreach (var navProperty in referenceProperties)
            {
                var propertyInfo = entityType.GetProperty(navProperty);
                if (propertyInfo.CanRead && !sharedEntityTypes.Contains(propertyInfo.PropertyType))
                {
                    object propertyValue = propertyInfo.GetValue(entity, null);
                    if (propertyValue != null)
                    {
                        children.Add(propertyValue);
                    }
                }
            }

            foreach (var navProperty in collectionProperties)
            {
                var propertyInfo = entityType.GetProperty(navProperty);
                if (propertyInfo.CanRead)
                {
                    object propertyValue = propertyInfo.GetValue(entity, null);
                    if (propertyValue != null)
                    {
                        // the navigation property is a collection
                        var enumerableValue = propertyValue as IEnumerable;
                        if (enumerableValue != null)
                        {
                            foreach (object child in enumerableValue)
                            {
                                if (child != null && !sharedEntityTypes.Contains(child.GetType()))
                                {
                                    children.Add(child);
                                }
                            }
                        }
                    }
                }
            }

            return children;
        }

        /// <summary>
        /// Retrieves a reference from the clone data manager to be set to in a cloned object.
        /// If the shared entity is released, the reference is retrieved from the db as the unreleased version of the entity
        /// </summary>
        /// <typeparam name="T">The type of the reference</typeparam>
        /// <param name="entity">The entity whose reference to get.</param>
        /// <returns>The reference or null if it does not exist in the clone data manager.</returns>
        private T RetrieveReferenceForClone<T>(T entity) where T : class
        {
            if (entity == null)
            {
                return null;
            }

            // first test if the shared entity is released, meaning that the reference must be taken as the non-released entity by the name
            // the code is used when copying from released projects to my projects.
            MeasurementUnit measurementUnit = entity as MeasurementUnit;
            if (measurementUnit != null)
            {
                if (cloneDataManager == null)
                {
                    return this.Clone(measurementUnit) as T;
                }
                else
                {
                    if (measurementUnit.IsReleased)
                    {
                        return this.cloneDataManager.MeasurementUnitRepository.GetByName(measurementUnit.Name) as T;
                    }
                    else
                    {
                        // if the shared entity is not released, get the normal reference based on the object.
                        return this.cloneDataManager.MeasurementUnitRepository.GetByKey(measurementUnit.Guid) as T;
                    }
                }
            }

            MaterialsClassification materialClassification = entity as MaterialsClassification;
            if (materialClassification != null)
            {
                if (cloneDataManager == null)
                {
                    return this.Clone(materialClassification) as T;
                }
                else
                {
                    if (materialClassification.IsReleased)
                    {
                        return this.cloneDataManager.MaterialsClassificationRepository.GetByName(materialClassification.Name) as T;
                    }
                    else
                    {
                        // if the shared entity is not released, get the normal reference based on the object.
                        return this.cloneDataManager.MaterialsClassificationRepository.GetByKey(materialClassification.Guid) as T;
                    }
                }
            }

            MachinesClassification machineClassification = entity as MachinesClassification;
            if (machineClassification != null)
            {
                if (cloneDataManager == null)
                {
                    return this.Clone(machineClassification) as T;
                }
                else
                {
                    if (machineClassification.IsReleased)
                    {
                        return this.cloneDataManager.MachinesClassificationRepository.GetByName(machineClassification.Name) as T;
                    }
                    else
                    {
                        // if the shared entity is not released, get the normal reference based on the object.
                        return this.cloneDataManager.MachinesClassificationRepository.GetByKey(machineClassification.Guid) as T;
                    }
                }
            }

            ProcessStepsClassification stepClassification = entity as ProcessStepsClassification;
            if (stepClassification != null)
            {
                if (cloneDataManager == null)
                {
                    return this.Clone(stepClassification) as T;
                }
                else
                {
                    if (stepClassification.IsReleased)
                    {
                        return this.cloneDataManager.ProcessStepsClassificationRepository.GetByName(stepClassification.Name) as T;
                    }
                    else
                    {
                        // if the shared entity is not released, get the normal reference based on the object.
                        return this.cloneDataManager.ProcessStepsClassificationRepository.GetByKey(stepClassification.Guid) as T;
                    }
                }
            }

            User user = entity as User;
            if (user != null)
            {
                if (cloneDataManager == null)
                {
                    return this.Clone(user) as T;
                }
                else
                {
                    if (user.IsReleased)
                    {
                        return this.cloneDataManager.UserRepository.GetByUsername(user.Username, false) as T;
                    }
                    else
                    {
                        // if the shared entity is not released, get the normal reference based on the object.
                        return this.cloneDataManager.UserRepository.GetByKey(user.Guid) as T;
                    }
                }
            }

            RawMaterialDeliveryType deliveryType = entity as RawMaterialDeliveryType;
            if (deliveryType != null)
            {
                if (cloneDataManager == null)
                {
                    return this.Clone(deliveryType) as T;
                }
                else
                {
                    if (deliveryType.IsReleased)
                    {
                        return this.cloneDataManager.RawMaterialDeliveryTypeRepository.FindByName(deliveryType.Name) as T;
                    }
                    else
                    {
                        // if the shared entity is not released, get the normal reference based on the object.
                        return this.cloneDataManager.RawMaterialDeliveryTypeRepository.GetByKey(deliveryType.Guid) as T;
                    }
                }
            }

            return null;
        }

        #endregion Helpers
    }
}
