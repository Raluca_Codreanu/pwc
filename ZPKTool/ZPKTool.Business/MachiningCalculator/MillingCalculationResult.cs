﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.MachiningCalculator
{
    /// <summary>
    /// Holds the result of the calculation for a Milling Operation.
    /// </summary>
    public class MillingCalculationResult
    {
        /// <summary>
        /// Gets or sets the calculated machining volume.
        /// </summary>
        public decimal? MachiningVolume { get; set; }

        /// <summary>
        /// Gets or sets the calculated turning speed.
        /// </summary>
        public decimal? TurningSpeed { get; set; }

        /// <summary>
        /// Gets or sets the calculated feed rate.
        /// </summary>
        public decimal? FeedRatePerTooth { get; set; }

        /// <summary>
        /// Gets or sets the calculated milling time.
        /// </summary>
        public decimal? MillingTime { get; set; }

        /// <summary>
        /// Gets or sets the calculated process time.
        /// </summary>
        public decimal? GrossProcessTime { get; set; }
    }
}
