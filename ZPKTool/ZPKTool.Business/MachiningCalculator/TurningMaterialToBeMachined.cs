﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.MachiningCalculator
{
    /// <summary>
    /// Represents a material to be machined during a Turning operation.
    /// </summary>
    public class TurningMaterialToBeMachined
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TurningMaterialToBeMachined"/> class.
        /// </summary>
        public TurningMaterialToBeMachined()
        {
            this.MachiningParams = new Dictionary<TurningMachiningType, MachiningParameters>();
        }

        /// <summary>
        /// Gets or sets the unique identifier of the material.
        /// </summary>
        public int MaterialID { get; set; }

        /// <summary>
        /// Gets or sets the name of the material.
        /// </summary>
        public string MaterialName { get; set; }

        /// <summary>
        /// Gets the machining parameters for the material. The machining parameters depend on the machining type.
        /// </summary>        
        public Dictionary<TurningMachiningType, MachiningParameters> MachiningParams { get; private set; }

        /// <summary>
        /// The machining parameters for a material during the Turning operation.
        /// </summary>
        public class MachiningParameters
        {
            /// <summary>
            /// Gets or sets the cutting speed of the tool on the material to be machined.
            /// </summary>
            public decimal CuttingSpeed { get; set; }

            /// <summary>
            /// Gets or sets the cutting feed.
            /// </summary>
            public decimal CuttingFeed { get; set; }

            /// <summary>
            /// Gets or sets the cutting feed.
            /// </summary>
            public decimal CuttingFeedVariable1 { get; set; }

            /// <summary>
            /// Gets or sets the cutting feed.
            /// </summary>
            public decimal CuttingFeedVariable2 { get; set; }
        }
    }
}
