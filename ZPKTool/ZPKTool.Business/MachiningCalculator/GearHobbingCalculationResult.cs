﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.MachiningCalculator
{
    /// <summary>
    /// Holds the result of the calculation for a Gear Hobbing Operation.
    /// </summary>
    public class GearHobbingCalculationResult
    {
        /// <summary>
        /// Gets or sets the Calculated Turning Speed of Milling Tool
        /// </summary>
        public decimal? TurningSpeedOfMillingTool { get; set; }

        /// <summary>
        /// Gets or sets the Calculated Feed Rate per Tooth
        /// </summary>
        public decimal? FeedRatePerTooth { get; set; }

        /// <summary>
        /// Gets or sets the Resulting Milling Time 
        /// </summary>
        public decimal? MillingTime { get; set; }

        /// <summary>
        /// Gets or sets the calculated process time.
        /// </summary>
        public decimal? GrossProcessTime { get; set; }

        /// <summary>
        /// Gets or sets the calculated machining volume
        /// </summary>
        public decimal? MachiningVolume { get; set; }
    }
}
