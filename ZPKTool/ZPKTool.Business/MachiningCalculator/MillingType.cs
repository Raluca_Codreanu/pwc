﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.MachiningCalculator
{
    /// <summary>
    /// The type of a Milling Operation
    /// </summary>
    public enum MillingType
    {
        /// <summary>
        /// Rough Machining type
        /// </summary>
        RoughMachining = 1,

        /// <summary>
        /// Fine Machining type
        /// </summary>
        FineMachining = 2
    }
}
