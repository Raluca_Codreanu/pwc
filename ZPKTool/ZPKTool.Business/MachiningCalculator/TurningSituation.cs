﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.MachiningCalculator
{
    /// <summary>
    /// The situation in a Turning Operation
    /// </summary>
    public enum TurningSituation
    {
        /// <summary>
        /// Continuous Cut
        /// </summary>
        ContinuousCut = 1,

        /// <summary>
        /// Moderate Intersections
        /// </summary>
        ModerateIntersections = 2,

        /// <summary>
        /// Unfavourable Intersections
        /// </summary>
        UnfavourableIntersections = 3
    }
}
