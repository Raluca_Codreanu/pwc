﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.MachiningCalculator
{
    /// <summary>
    /// The knowledgebase data for the Drilling Operation.
    /// </summary>
    public class DrillingKnowledgebaseData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DrillingKnowledgebaseData"/> class.
        /// </summary>
        public DrillingKnowledgebaseData()
        {
            this.MaterialsToMachine = new List<DrillingMaterialToBeMachined>();
        }

        /// <summary>
        /// Gets or sets the default Machine Feed Speed (X/Y)
        /// </summary>
        public decimal DefaultMachineFeedSpeed { get; set; }

        /// <summary>
        /// Gets or sets the default Start Stop Time for Machine Feed
        /// </summary>
        public decimal DefaultStartStopTimeForMachineFeed { get; set; }

        /// <summary>
        /// Gets or sets the default Drilling Diameter / Thread Diameter
        /// </summary>
        public decimal DefaultDrillingDiameter { get; set; }

        /// <summary>
        /// Gets or sets the default Drilling Depth
        /// </summary>
        public decimal DefaultDrillingDepth { get; set; }

        /// <summary>
        /// Gets or sets the default Number of Holes
        /// </summary>
        public int DefaultHolesNumber { get; set; }

        /// <summary>
        /// Gets or sets the default Average Distance between Holes
        /// </summary>
        public decimal DefaultAvgHolesDistance { get; set; }

        /// <summary>
        /// Gets or sets the default tool change time.
        /// </summary>
        public decimal DefaultToolChangeTime { get; set; }

        /// <summary>
        /// Gets the materials to be machined along with the tool materials.
        /// </summary>
        public ICollection<DrillingMaterialToBeMachined> MaterialsToMachine { get; private set; }
    }
}
