﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.MachiningCalculator
{
    /// <summary>
    /// The knowledgebase data for the Milling Operation.
    /// </summary>
    public class MillingKnowledgebaseData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MillingKnowledgebaseData"/> class.
        /// </summary>
        public MillingKnowledgebaseData()
        {
            this.MaterialsToMachine = new List<MillingMaterialToBeMachined>();            
        }

        /// <summary>
        /// Gets or sets the Machine Feed Speed
        /// </summary>
        public decimal DefaultMachineFeedSpeed { get; set; }

        /// <summary>
        /// Gets or sets the Cut Depth (Length or Depth of Cutting Interference)
        /// </summary>
        public decimal DefaultCutDepth { get; set; }

        /// <summary>
        /// Gets or sets the Number of Teeth
        /// </summary>
        public decimal DefaultTeethNumber { get; set; }

        /// <summary>
        /// Gets or sets the Milling Tool Diameter
        /// </summary>
        public decimal DefaultMillingToolDiameter { get; set; }

        /// <summary>
        /// Gets or sets the Milling Tool Width
        /// </summary>
        public decimal DefaultMillingToolWidth { get; set; }

        /// <summary>
        /// Gets or sets the Start Stop Time for Machine Feed
        /// </summary>
        public decimal DefaultStartStopTimeForMachineFeed { get; set; }

        /// <summary>
        /// Gets or sets the Machine Feed length (Tool movement to/from Start position)
        /// </summary>
        public decimal DefaultMachineFeedLength { get; set; }

        /// <summary>
        /// Gets or sets the Milling length (Length of volume to be milled )
        /// </summary>
        public decimal DefaultMillingLength { get; set; }

        /// <summary>
        /// Gets or sets the Milling width (Width of volume to be milled)
        /// </summary>
        public decimal DefaultMillingWidth { get; set; }

        /// <summary>
        /// Gets or sets the Milling Depth
        /// </summary>
        public decimal DefaultMillingDepth { get; set; }

        /// <summary>
        /// Gets the The materials to be machined.
        /// </summary>
        public ICollection<MillingMaterialToBeMachined> MaterialsToMachine { get; private set; }
                
        /// <summary>
        /// Gets or sets the default tool change time.
        /// </summary>
        public decimal DefaultToolChangeTime { get; set; }
    }
}
