﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.MachiningCalculator
{
    /// <summary>
    /// Holds the result of the calculation for a Grinding Operation.
    /// </summary>
    public class GrindingCalculationResult
    {
        /// <summary>
        /// Gets or sets the Calculated  Turning Speed of Grinding Tool 
        /// </summary>
        public decimal? TurningSpeedOfGrindingTool { get; set; }

        /// <summary>
        /// Gets or sets the Calculated Feed Rate 
        /// </summary>
        public decimal? FeedRate { get; set; }

        /// <summary>
        /// Gets or sets the Resulting λ(ke) (Average Grain Distance)
        /// </summary>
        public decimal? AverageGainDistance { get; set; }

        /// <summary>
        /// Gets or sets the Resulting h(m)
        /// </summary>
        public decimal? HM { get; set; }

        /// <summary>
        ///  Gets or sets the Resulting Grinding Time 
        /// </summary>
        public decimal? GrindingTime { get; set; }

        /// <summary>
        /// Gets or sets the calculated process time.
        /// </summary>
        public decimal? GrossProcessTime { get; set; }

        /// <summary>
        /// Gets or sets the calculated machining volume.
        /// </summary>
        public decimal? MachiningVolume { get; set; }
    }
}
