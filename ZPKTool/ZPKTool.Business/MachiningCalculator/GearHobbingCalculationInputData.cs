﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.MachiningCalculator
{
    /// <summary>
    /// The input data for calculating the times of a Gear Hobbing Operation.
    /// </summary>
    public class GearHobbingCalculationInputData
    {
        /// <summary>
        /// Gets or sets the Material to be Machined
        /// </summary>
        public GearHobbingMaterialToBeMachined Material { get; set; }

        /// <summary>
        /// Gets or sets the Milling Tool Type
        /// </summary>
        public GearHobbingMillingToolType MillingToolType { get; set; }

        /// <summary>
        /// Gets or sets the Machine Feed Speed
        /// </summary>
        public decimal? MachineFeedSpeed { get; set; }

        /// <summary>
        /// Gets or sets the Milling Type
        /// </summary>
        public MillingType MillingType { get; set; }

        /// <summary>
        /// Gets or sets the Cut Depth (Length or Depth of Cutting Interference)
        /// </summary>
        public decimal? CutDepth { get; set; }

        /// <summary>
        /// Gets or sets the Number of Teeth at Hob
        /// </summary>
        public decimal? TeethNumber { get; set; }

        /// <summary>
        /// Gets or sets the Milling Tool Diameter
        /// </summary>
        public decimal? MillingToolDiameter { get; set; }

        /// <summary>
        /// Gets or sets the Start Stop Time for Machine Feed
        /// </summary>
        public decimal? StartStopTimeForMachineFeed { get; set; }

        /// <summary>
        /// Gets or sets the Machine Feed length (Tool movement to/from Start position)
        /// </summary>
        public decimal? MachineFeedLength { get; set; }

        /// <summary>
        /// Gets or sets the Gear Diameter
        /// </summary>
        public decimal? GearDiameter { get; set; }

        /// <summary>
        /// Gets or sets the Gear width (Width of volume to be milled)
        /// </summary>
        public decimal? GearWidth { get; set; }

        /// <summary>
        /// Gets or sets the Gear Modul
        /// </summary>
        public decimal? GearModul { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the calculation should be performed in Expert mode or Knowledgebase mode.
        /// </summary>
        public bool ExpertModeCalculation { get; set; }

        /// <summary>
        /// Gets or sets the feed rate.
        /// </summary>
        public decimal? FeedRatePerTooth { get; set; }

        /// <summary>
        /// Gets or sets the tool change time.
        /// </summary>
        public decimal? ToolChangeTime { get; set; }

        /// <summary>
        /// Gets or sets the inputted turning speed.
        /// </summary>
        public decimal? TurningSpeed { get; set; }
    }
}
