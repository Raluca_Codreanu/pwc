﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.MachiningCalculator
{
    /// <summary>
    /// The knowledgebase data for the Gear Hobbing Operation.
    /// </summary>
    public class GearHobbingKnowledgebaseData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GearHobbingKnowledgebaseData"/> class.
        /// </summary>
        public GearHobbingKnowledgebaseData()
        {
            this.MaterialsToMachine = new List<GearHobbingMaterialToBeMachined>();
        }

        /// <summary>
        /// Gets or sets the default Machine Feed Speed (X/Y)
        /// </summary>
        public decimal DefaultMachineFeedSpeed { get; set; }

        /// <summary>
        /// Gets or sets the default Cut Depth (Length or Depth of Cutting Interference)
        /// </summary>
        public decimal DefaultCutDepth { get; set; }

        /// <summary>
        /// Gets or sets the default Number of Tooth at Hob
        /// </summary>
        public decimal DefaultTeethNumber { get; set; }

        /// <summary>
        /// Gets or sets the default Milling Tool Diameter
        /// </summary>
        public decimal DefaultMillingToolDiameter { get; set; }

        /// <summary>
        /// Gets or sets the default Start Stop Time for Machine Feed
        /// </summary>
        public decimal DefaultStartStopTimeForMachineFeed { get; set; }

        /// <summary>
        /// Gets or sets the default MachinFeed length (Tool movement to/from Start position)
        /// </summary>
        public decimal DefaultMachineFeedLength { get; set; }

        /// <summary>
        /// Gets or sets the default Gear Diameter 
        /// </summary>
        public decimal DefaultGearDiameter { get; set; }

        /// <summary>
        /// Gets or sets the default Gear width (Width of volume to be milled)
        /// </summary>
        public decimal DefaultGearWidth { get; set; }

        /// <summary>
        /// Gets or sets the default Gear Modul
        /// </summary>
        public decimal DefaultGearModul { get; set; }

        /// <summary>
        /// Gets or sets the default tool change time.
        /// </summary>
        public decimal DefaultToolChangeTime { get; set; }

        /// <summary>
        /// Gets the the materials to be machined.
        /// </summary>
        public ICollection<GearHobbingMaterialToBeMachined> MaterialsToMachine { get; private set; }
    }
}
