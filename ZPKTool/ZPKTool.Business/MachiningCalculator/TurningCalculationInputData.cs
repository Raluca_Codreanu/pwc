﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.MachiningCalculator
{
    /// <summary>
    /// The input data for a Turning Operation calculation.
    /// </summary>
    public class TurningCalculationInputData
    {
        /// <summary>
        /// Gets or sets the machining type for the operation.
        /// </summary>
        public TurningMachiningType MachiningType { get; set; }

        /// <summary>
        /// Gets or sets the material to be machined.
        /// </summary>
        public TurningMaterialToBeMachined Material { get; set; }

        /// <summary>
        /// Gets or sets the type of the turning operation.
        /// </summary>
        public TurningType TurningType { get; set; }

        /// <summary>
        /// Gets or sets the machine feed speed.
        /// </summary>
        public decimal? MachineFeedSpeed { get; set; }

        /// <summary>
        /// Gets or sets the situation of the turning operation.
        /// </summary>
        public TurningSituation TurningSituation { get; set; }

        /// <summary>
        /// Gets or sets the cutting depth.
        /// </summary>
        public decimal? CutDepth { get; set; }

        /// <summary>
        /// Gets or sets the Start Stop Time For Machine Feed
        /// </summary>
        public decimal? StartStopTimeForMachineFeed { get; set; }

        /// <summary>
        /// Gets or sets the Machine Feed Length.
        /// </summary>
        public decimal? MachineFeedLength { get; set; }

        /// <summary>
        /// Gets or sets the turning diameter.
        /// </summary>
        public decimal? TurningDiameter { get; set; }

        /// <summary>
        /// Gets or sets the turning length.
        /// </summary>
        public decimal? TurningLengh { get; set; }

        /// <summary>
        /// Gets or sets the turning depth.
        /// </summary>
        public decimal? TurningDepth { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the calculation should be performed in Expert mode or Knowledgebase mode.
        /// </summary>
        public bool ExpertModeCalculation { get; set; }

        /// <summary>
        /// Gets or sets the feed rate inputted by the user.
        /// </summary>
        public decimal? FeedRate { get; set; }

        /// <summary>
        /// Gets or sets the tool change time.
        /// </summary>
        public decimal? ToolChangeTime { get; set; }

        /// <summary>
        /// Gets or sets the feed rate inputted by the user.
        /// </summary>
        public decimal? TurningSpeed { get; set; }
    }
}
