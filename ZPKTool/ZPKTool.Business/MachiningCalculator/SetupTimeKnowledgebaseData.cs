﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.MachiningCalculator
{
    /// <summary>
    /// The knowledgebase data for the Setup Time Operation
    /// </summary>
    public class SetupTimeKnowledgebaseData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SetupTimeKnowledgebaseData"/> class.
        /// </summary>
        public SetupTimeKnowledgebaseData()
        {
        }

        /// <summary>
        /// Gets or sets the default Set Up Time in the Machine.
        /// </summary>
        public decimal DefaultSetupTimeInTheMachine { get; set; }

        /// <summary>
        /// Gets or sets the default Set Up Time outside of the machine.
        /// </summary>
        public decimal DefaultSetupTimeOutsideTheMachine { get; set; }
    }
}
