﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.MachiningCalculator
{
    /// <summary>
    /// The milling type of a Gear Hobbing Operation
    /// </summary>
    public enum GearHobbingMillingToolType
    {
        /// <summary>
        /// The hob tool.
        /// </summary>
        Hob = 1
    }
}
