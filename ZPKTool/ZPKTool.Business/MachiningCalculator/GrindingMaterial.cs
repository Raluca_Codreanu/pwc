﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.MachiningCalculator
{
    /// <summary>
    /// Material used during the grinding operation.
    /// </summary>
    public class GrindingMaterial
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GrindingMaterial"/> class.
        /// </summary>
        public GrindingMaterial()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GrindingMaterial"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="qbase">The Q base value.</param>
        /// <param name="vcbase">The VC base value.</param>
        public GrindingMaterial(string name, decimal qbase, decimal vcbase)
        {
            this.Name = name;
            this.QBase = qbase;
            this.VCBase = vcbase;
        }

        /// <summary>
        /// Gets or sets the name of the material.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the Q base value.
        /// </summary>
        public decimal QBase { get; set; }

        /// <summary>
        /// Gets or sets the VC base value.
        /// </summary>
        public decimal VCBase { get; set; }
    }
}
