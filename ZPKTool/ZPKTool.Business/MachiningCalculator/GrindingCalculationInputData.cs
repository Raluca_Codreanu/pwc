﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.MachiningCalculator
{
    /// <summary>
    /// The input data for calculating the times of a Grinding Operation.
    /// </summary>
    public class GrindingCalculationInputData
    {
        /// <summary>
        /// Gets or sets the Machine Feed Speed (X/Y)
        /// </summary>
        public decimal? MachineFeedSpeed { get; set; }

        /// <summary>
        /// Gets or sets the Grinding Grain Size
        /// </summary>
        public decimal? GrindingGrainSize { get; set; }

        /// <summary>
        /// Gets or sets the Cutting depth per cycle
        /// </summary>
        public decimal? CuttingDepthPerCycle { get; set; }

        /// <summary>
        /// Gets or sets the Grinding Tool Diameter
        /// </summary>
        public decimal? GrindingToolDiameter { get; set; }

        /// <summary>
        /// Gets or sets the Grinding Tool Width
        /// </summary>
        public decimal? GrindingToolWidth { get; set; }

        /// <summary>
        /// Gets or sets the Start Stop Time for Machine Feed
        /// </summary>
        public decimal? StartStopTimeForMachineFeed { get; set; }

        /// <summary>
        /// Gets or sets the MachinFeed length (Tool movement to/from Start position)
        /// </summary>
        public decimal? MachineFeedLength { get; set; }

        /// <summary>
        /// Gets or sets the Grinding Part Diameter (Diameter of Part to be grinded)
        /// </summary>
        public decimal? GrindingPartDiameter { get; set; }

        /// <summary>
        /// Gets or sets the Grinding Length (Length of volume to be grinded)
        /// </summary>
        public decimal? GrindingLength { get; set; }

        /// <summary>
        /// Gets or sets the Grinding width (Width of volume to be grinded)
        /// </summary>
        public decimal? GrindingWidth { get; set; }

        /// <summary>
        /// Gets or sets the Grinding Depth (Depth of volume to be grinded)
        /// </summary>
        public decimal? GrindingDepth { get; set; }

        /// <summary>
        /// Gets or sets the material to be machined.
        /// </summary>
        public GrindingMaterial Material { get; set; }

        /// <summary>
        /// Gets or sets the type of the grinding operation.
        /// </summary>
        public GrindingOperationType OperationType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the calculation should be performed in Expert mode or Knowledgebase mode.
        /// </summary>
        public bool ExpertModeCalculation { get; set; }

        /// <summary>
        /// Gets or sets the feed rate.
        /// </summary>
        public decimal? FeedRate { get; set; }

        /// <summary>
        /// Gets or sets the tool change time.
        /// </summary>
        public decimal? ToolChangeTime { get; set; }

        /// <summary>
        /// Gets or sets the inputted turning speed.
        /// </summary>
        public decimal? TurningSpeed { get; set; }
    }
}
