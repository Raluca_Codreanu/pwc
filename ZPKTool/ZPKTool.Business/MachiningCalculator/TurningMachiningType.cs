﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.MachiningCalculator
{
    /// <summary>
    /// The MachiningType in a Turning operation.
    /// </summary>
    public enum TurningMachiningType
    {
        /// <summary>
        /// Finest Feed machining type
        /// </summary>
        FinestFeed = 1,

        /// <summary>
        /// Fine Feed machining type
        /// </summary>
        FineFeed = 2,

        /// <summary>
        /// Chipping machining type
        /// </summary>
        Chipping = 3,

        /// <summary>
        /// Rough Chipping machining type
        /// </summary>
        RoughChipping = 4
    }
}
