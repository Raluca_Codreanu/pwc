﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.MachiningCalculator
{
    /// <summary>
    /// Represents the combination of a material to be machined and a tool material.
    /// </summary>
    public class MaterialToolMaterialCombination
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MaterialToolMaterialCombination"/> class.
        /// </summary>
        public MaterialToolMaterialCombination()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MaterialToolMaterialCombination"/> class.
        /// </summary>
        /// <param name="materialName">Name of the material.</param>
        /// <param name="toolMaterialName">Name of the tool material.</param>
        /// <param name="cuttingSpeed">The cutting speed.</param>
        /// <param name="cuttingFeed">The cutting feed.</param>
        public MaterialToolMaterialCombination(string materialName, string toolMaterialName, decimal cuttingSpeed, decimal cuttingFeed)
        {
            this.MaterialName = materialName;
            this.ToolMaterialName = toolMaterialName;
            this.CuttingSpeed = cuttingSpeed;
            this.CuttingFeed = cuttingFeed;
        }

        /// <summary>
        /// Gets or sets the name of the material to be machined.
        /// </summary>
        public string MaterialName { get; set; }

        /// <summary>
        /// Gets or sets the name of the tool material.
        /// </summary>
        public string ToolMaterialName { get; set; }

        /// <summary>
        /// Gets the name of the MachiningMaterial combination.
        /// </summary>
        public string Name
        {
            get { return this.MaterialName + " (" + this.ToolMaterialName + ")"; }
        }

        /// <summary>
        /// Gets or sets the cutting speed of the tool on the material to be machined.
        /// </summary>
        public decimal CuttingSpeed { get; set; }

        /// <summary>
        /// Gets or sets the cutting feed.
        /// </summary>
        public decimal CuttingFeed { get; set; }
    }
}
