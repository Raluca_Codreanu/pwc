﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.MachiningCalculator
{
    /// <summary>
    /// Holds the result of the calculation for a Turning Operation.
    /// </summary>
    public class TurningCalculationResult
    {
        /// <summary>
        /// Gets or sets the Calculated  Turning Speed
        /// </summary>
        public decimal? TurningSpeed { get; set; }

        /// <summary>
        /// Gets or sets the Calculated Feed Rate
        /// </summary>
        public decimal? CalculatedFeedRate { get; set; }

        /// <summary>
        /// Gets or sets the Resulting Turning Time 
        /// </summary>
        public decimal? TurningTime { get; set; }

        /// <summary>
        /// Gets or sets the calculated process time.
        /// </summary>
        public decimal? GrossProcessTime { get; set; }

        /// <summary>
        /// Gets or sets the machining volume.
        /// </summary>
        public decimal? MachiningVolume { get; set; }
    }
}
