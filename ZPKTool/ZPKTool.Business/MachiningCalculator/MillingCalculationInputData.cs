﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.MachiningCalculator
{
    /// <summary>
    /// The input data for calculating the times of a Milling Operation.
    /// </summary>
    public class MillingCalculationInputData
    {
        /// <summary>
        /// Gets or sets the material to be machined.
        /// </summary>
        public MillingMaterialToBeMachined Material { get; set; }

        /// <summary>
        /// Gets or sets the type of the milling tool used.
        /// </summary>
        public MillingToolType ToolType { get; set; }

        /// <summary>
        /// Gets or sets the Machine Feed Speed
        /// </summary>
        public decimal? MachineFeedSpeed { get; set; }

        /// <summary>
        /// Gets or sets the type of the milling operation.
        /// </summary>
        public MillingType MillingType { get; set; }

        /// <summary>
        /// Gets or sets the Cut Depth (Length or Depth of Cutting Interference)
        /// </summary>
        public decimal? CutDepth { get; set; }

        /// <summary>
        /// Gets or sets the Number of Teeth
        /// </summary>
        public decimal? TeethNumber { get; set; }

        /// <summary>
        /// Gets or sets the Milling Tool Diameter
        /// </summary>
        public decimal? MillingToolDiameter { get; set; }

        /// <summary>
        /// Gets or sets the Milling Tool Width
        /// </summary>
        public decimal? MillingToolWidth { get; set; }

        /// <summary>
        /// Gets or sets the Start Stop Time for Machine Feed
        /// </summary>
        public decimal? StartStopTimeForMachineFeed { get; set; }

        /// <summary>
        /// Gets or sets the Machine Feed length (Tool movement to/from Start position)
        /// </summary>
        public decimal? MachineFeedLength { get; set; }

        /// <summary>
        /// Gets or sets the Milling length (Length of volume to be milled )
        /// </summary>
        public decimal? MillingLength { get; set; }

        /// <summary>
        /// Gets or sets the Milling width (Width of volume to be milled)
        /// </summary>
        public decimal? MillingWidth { get; set; }

        /// <summary>
        /// Gets or sets the Milling width (Width of volume to be milled)
        /// </summary>
        public decimal? MillingDepth { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the calculation should be performed in Expert mode or Knowledgebase mode.
        /// </summary>
        public bool ExpertModeCalculation { get; set; }

        /// <summary>
        /// Gets or sets the feed rate inputted by the user.
        /// </summary>
        public decimal? FeedRatePerTooth { get; set; }

        /// <summary>
        /// Gets or sets the tool change time.
        /// </summary>
        public decimal? ToolChangeTime { get; set; }

        /// <summary>
        /// Gets or sets the turning speed inputted by the user.
        /// </summary>
        public decimal? TurningSpeed { get; set; }
    }
}
