﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.MachiningCalculator
{
    /// <summary>
    /// The type of a drilling operation.
    /// </summary>    
    public enum DrillingType
    {
        /// <summary>
        /// Through Hole type
        /// </summary>
        ThroughHole = 1,

        /// <summary>
        /// Blind Hole type
        /// </summary>
        BlindHole = 2,

        /// <summary>
        /// Counterbore type
        /// </summary>
        Counterbore = 3,

        /// <summary>
        /// Tap type
        /// </summary>
        Tap = 4,

        /// <summary>
        /// Reaming type
        /// </summary>
        Reaming = 5
    }
}
