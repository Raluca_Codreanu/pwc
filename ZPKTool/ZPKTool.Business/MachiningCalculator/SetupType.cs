﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.MachiningCalculator
{
    /// <summary>
    /// The type of a Setup Operation.
    /// </summary>
    public enum SetupType
    {
        /// <summary>
        /// Part positioning
        /// </summary>
        PartPositioning = 1,

        /// <summary>
        /// Part clamping
        /// </summary>
        PartClamping = 2,

        /// <summary>
        /// Part transport
        /// </summary>
        PartTransport = 3,

        /// <summary>
        /// Machine preparation
        /// </summary>
        MachinePreparation = 4,

        /// <summary>
        /// Machine setup
        /// </summary>
        MachineSetup = 5,

        /// <summary>
        /// Die setup type.
        /// </summary>
        DieSetUp = 6
    }
}
