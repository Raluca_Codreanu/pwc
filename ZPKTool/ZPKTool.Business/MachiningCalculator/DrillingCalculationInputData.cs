﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.MachiningCalculator
{
    /// <summary>
    /// The input data for calculating the times of a Drilling Operation.
    /// </summary>
    public class DrillingCalculationInputData
    {
        /// <summary>
        /// Gets or sets the material to be machined.
        /// </summary>
        public DrillingMaterialToBeMachined Material { get; set; }

        /// <summary>
        /// Gets or sets the type of the drilling operation.
        /// </summary>
        public DrillingType DrillingType { get; set; }

        /// <summary>
        /// Gets or sets the Machine Feed Speed.
        /// </summary>
        public decimal? MachineFeedSpeed { get; set; }

        /// <summary>
        /// Gets or sets the Start Stop Time for Machine Feed
        /// </summary>
        public decimal? StartStopTimeForMachineFeed { get; set; }

        /// <summary>
        /// Gets or sets the Drilling Diameter / Threat Diameter
        /// </summary>
        public decimal? DrillingDiameter { get; set; }

        /// <summary>
        /// Gets or sets the Drilling Depth
        /// </summary>
        public decimal? DrillingDepth { get; set; }

        /// <summary>
        /// Gets or sets the Number of Holes
        /// </summary>
        public int? HolesNumber { get; set; }

        /// <summary>
        /// Gets or sets the Average Distance between Holes
        /// </summary>
        public decimal? AvgHolesDistance { get; set; }

        /// <summary>
        /// Gets or sets the tool change time.
        /// </summary>
        public decimal? ToolChangeTime { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to calculate the Resulting Drilling Time including boring.
        /// </summary>
        public bool Boring { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the calculation should be performed in Expert mode or Knowledgebase mode.
        /// </summary>
        public bool ExpertModeCalculation { get; set; }

        /// <summary>
        /// Gets or sets the turning speed inputted by the user.
        /// </summary>
        public decimal? TurningSpeed { get; set; }
    }
}
