﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.MachiningCalculator
{
    /// <summary>
    /// The type of a grinding operation.
    /// </summary>
    public enum GrindingOperationType
    {
        /// <summary>
        /// The surface grinding collinear operation.
        /// </summary>
        SurfaceGrindingColinear = 1,

        /// <summary>
        /// The surface grinding perpendicular operation.
        /// </summary>
        SurfaceGrindingPerpendicular = 2,

        /// <summary>
        /// The cylindrical grinding axial outer operation.
        /// </summary>
        CylindricalGrindingAxialOuter = 3,

        /// <summary>
        /// The cylindrical grinding radial outer operation.
        /// </summary>
        CylindricalGrindingRadialOuter = 4,

        /// <summary>
        /// The cylindrical grinding axial inner operation.
        /// </summary>
        CylindricalGrindingAxialInner = 5
    }
}
