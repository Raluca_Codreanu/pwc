﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.MachiningCalculator
{
    /// <summary>
    /// Represents a material to be machined during a Drilling operation.
    /// </summary>
    public class DrillingMaterialToBeMachined
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DrillingMaterialToBeMachined"/> class.
        /// </summary>
        public DrillingMaterialToBeMachined()
        {
        }

        /// <summary>
        /// Gets or sets the unique identifier of the material.
        /// </summary>        
        public int MaterialID { get; set; }

        /// <summary>
        /// Gets or sets the name of the material.
        /// </summary>
        public string MaterialName { get; set; }

        /// <summary>
        /// Gets or sets the type of the material from which the drilling tool is made.
        /// </summary>
        public string ToolMaterialType { get; set; }

        /// <summary>
        /// Gets the display name of the material which is the combination of material and tool material names.
        /// </summary>
        public string Name
        {
            get { return this.MaterialName + " (" + this.ToolMaterialType + ")"; }
        }

        /// <summary>
        /// Gets or sets the cutting speed of the tool on the material to be machined.
        /// </summary>
        public decimal CuttingSpeed { get; set; }

        /// <summary>
        /// Gets or sets the cutting feed.
        /// </summary>
        public decimal CuttingFeed { get; set; }
    }
}
