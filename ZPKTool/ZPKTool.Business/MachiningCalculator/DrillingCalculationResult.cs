﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.MachiningCalculator
{
    /// <summary>
    /// Holds the result of the calculation for a Drilling Operation.
    /// </summary>
    public class DrillingCalculationResult
    {
        /// <summary>
        /// Gets or sets the calculated machining volume.
        /// </summary>
        public decimal? MachiningVolume { get; set; }

        /// <summary>
        /// Gets or sets the calculated turning speed.
        /// </summary>
        public decimal? TurningSpeed { get; set; }

        /// <summary>
        /// Gets or sets the calculated drilling time.
        /// </summary>
        public decimal? DrillingTime { get; set; }

        /// <summary>
        /// Gets or sets the calculated process time.
        /// </summary>
        public decimal? GrossProcessTime { get; set; }
    }
}
