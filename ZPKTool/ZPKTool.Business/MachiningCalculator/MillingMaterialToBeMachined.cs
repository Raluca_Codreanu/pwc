﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.MachiningCalculator
{
    /// <summary>
    /// Represents a material to be machined during a Milling operation.
    /// </summary>
    public class MillingMaterialToBeMachined
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MillingMaterialToBeMachined"/> class.
        /// </summary>
        public MillingMaterialToBeMachined()
        {
            this.MachiningParams = new Dictionary<MillingToolType, MachiningParameters>();
        }

        /// <summary>
        /// Gets or sets the unique identifier of the material.
        /// </summary>        
        public int MaterialID { get; set; }

        /// <summary>
        /// Gets or sets the name of the material.
        /// </summary>
        public string MaterialName { get; set; }

        /// <summary>
        /// Gets or sets the type of the material from which the drilling tool is made.
        /// </summary>
        public string ToolMaterialType { get; set; }

        /// <summary>
        /// Gets the display name of the material which is the combination of material and tool material names.
        /// </summary>
        public string Name
        {
            get { return this.MaterialName + " (" + this.ToolMaterialType + ")"; }
        }

        /// <summary>
        /// Gets the machining parameters for the material. The machining parameters depend on the milling tool type.
        /// </summary>        
        public Dictionary<MillingToolType, MachiningParameters> MachiningParams { get; private set; }

        /// <summary>
        /// The machining parameters for a material during the Turning operation.
        /// </summary>
        public class MachiningParameters
        {
            /// <summary>
            /// Gets or sets the cutting speed of the tool on the material to be machined.
            /// </summary>
            public decimal CuttingSpeed { get; set; }

            /// <summary>
            /// Gets or sets the cutting feed.
            /// </summary>
            public decimal CuttingFeed { get; set; }

            /// <summary>
            /// Gets or sets the tool type factor.
            /// </summary>
            public decimal ToolTypeFactor { get; set; }
        }
    }
}
