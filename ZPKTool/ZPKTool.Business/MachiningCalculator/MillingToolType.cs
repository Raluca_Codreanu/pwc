﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.MachiningCalculator
{
    /// <summary>
    /// The types of milling tools.
    /// </summary>
    public enum MillingToolType
    {
        /// <summary>
        /// The cylindrical cutter tool.
        /// </summary>
        CylindricalCutter = 0,

        /// <summary>
        /// The shell end mill tool.
        /// </summary>
        ShellEndMill = 1,

        /// <summary>
        /// The milling head tool.
        /// </summary>
        MillingHead = 2,

        /// <summary>
        /// The end mill cutter tool.
        /// </summary>
        EndMillCutter = 3,

        /// <summary>
        /// The side mill cutter tool.
        /// </summary>
        SideMillCutter = 4,

        /// <summary>
        /// The milling saw tool.
        /// </summary>
        MillingSaw = 5
    }
}
