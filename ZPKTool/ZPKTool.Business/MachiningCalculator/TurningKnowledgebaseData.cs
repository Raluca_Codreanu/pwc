﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.MachiningCalculator
{
    /// <summary>
    /// The knowledgebase data for the Turning Operation.
    /// </summary>
    public class TurningKnowledgebaseData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TurningKnowledgebaseData"/> class.
        /// </summary>
        public TurningKnowledgebaseData()
        {
            this.MaterialsToMachine = new List<TurningMaterialToBeMachined>();
        }

        /// <summary>
        /// Gets or sets the Default Machine Feed Speed
        /// </summary>
        public decimal DefaultMachineFeedSpeed { get; set; }

        /// <summary>
        /// Gets or sets the Default Cut Depth
        /// </summary>
        public decimal DefaultCutDepth { get; set; }

        /// <summary>
        /// Gets or sets the Default Start Stop Time for Machine Feed
        /// </summary>
        public decimal DefaultStartStopTimeForMachineFeed { get; set; }

        /// <summary>
        /// Gets or sets the Default Machine Feed Length
        /// </summary>
        public decimal DefaultMachineFeedLength { get; set; }

        /// <summary>
        /// Gets or sets the Default Turning Diameter
        /// </summary>
        public decimal DefaultTurningDiameter { get; set; }

        /// <summary>
        /// Gets or sets the Default Turning length.
        /// </summary>
        public decimal DefaultTurningLengh { get; set; }

        /// <summary>
        /// Gets or sets the Default Turning Depth
        /// </summary>
        public decimal DefaultTurningDepth { get; set; }

        /// <summary>
        /// Gets or sets the default tool change time.
        /// </summary>
        public decimal DefaultToolChangeTime { get; set; }

        /// <summary>
        /// Gets the materials to be machined.
        /// </summary>
        public ICollection<TurningMaterialToBeMachined> MaterialsToMachine { get; private set; }
    }
}
