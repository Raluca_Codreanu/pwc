﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Common;

namespace ZPKTool.Business.MachiningCalculator
{
    /// <summary>
    /// The knowledgebase data for the Grinding Operation.
    /// </summary>  
    public class GrindingKnowledgebaseData
    {
        /// <summary>
        /// The Effective Grain-size data (Source: Hoffmann).
        /// The tuples in the list contain the Grain Size, Cutting Depth and the associated value (the corresponding effective grain size).
        /// </summary>
        private List<Tuple<decimal, decimal, decimal>> effectiveGrainSizeData;

        /// <summary>
        /// Initializes a new instance of the <see cref="GrindingKnowledgebaseData"/> class.
        /// </summary>
        public GrindingKnowledgebaseData()
        {
            this.GrainSizeValues = new List<decimal>();
            this.GrindingDepthValues = new List<decimal>();
            this.MaterialsToMachine = new List<GrindingMaterial>();
            this.effectiveGrainSizeData = new List<Tuple<decimal, decimal, decimal>>();
        }

        /// <summary>
        /// Gets or sets the default Machine Feed Speed (X/Y)
        /// </summary>
        public decimal DefaultMachineFeedSpeed { get; set; }

        /// <summary>
        /// Gets or sets the default Grinding Grain Size
        /// </summary>
        public decimal DefaultGrindingGrainSize { get; set; }

        /// <summary>
        /// Gets or sets the default Cutting depth per cycle
        /// </summary>
        public decimal DefaultCuttingDepthPerCycle { get; set; }

        /// <summary>
        /// Gets or sets the default Grinding Tool Diameter
        /// </summary>
        public decimal DefaultGrindingToolDiameter { get; set; }

        /// <summary>
        /// Gets or sets the default Grinding Tool Width
        /// </summary>
        public decimal DefaultGrindingToolWidth { get; set; }

        /// <summary>
        /// Gets or sets the default Start Stop Time for Machine Feed
        /// </summary>
        public decimal DefaultStartStopTimeForMachineFeed { get; set; }

        /// <summary>
        /// Gets or sets the default Machine Feed length (Tool movement to/from Start position)
        /// </summary>
        public decimal DefaultMachineFeedLength { get; set; }

        /// <summary>
        /// Gets or sets the default Grinding Part Diameter (Diameter of Part to be grinded)
        /// </summary>
        public decimal DefaultGrindingPartDiameter { get; set; }

        /// <summary>
        /// Gets or sets the default Grinding Length (Length of volume to be grinded)
        /// </summary>
        public decimal DefaultGrindingLength { get; set; }

        /// <summary>
        /// Gets or sets the default Grinding width (Width of volume to be grinded)
        /// </summary>
        public decimal DefaultGrindingWidth { get; set; }

        /// <summary>
        /// Gets or sets the default Grinding Depth (Depth of volume to be grinded)
        /// </summary>
        public decimal DefaultGrindingDepth { get; set; }

        /// <summary>
        /// Gets the list of values for the "Grinding Grain Size" selector.
        /// </summary>
        public ICollection<decimal> GrainSizeValues { get; private set; }

        /// <summary>
        /// Gets the list of values for the "Cutting Depth per Cycle" selector.
        /// </summary>
        public ICollection<decimal> GrindingDepthValues { get; private set; }

        /// <summary>
        /// Gets the list of materials to be machined.
        /// </summary>
        public ICollection<GrindingMaterial> MaterialsToMachine { get; private set; }

        /// <summary>
        /// Gets or sets the default tool change time.
        /// </summary>
        public decimal DefaultToolChangeTime { get; set; }

        /// <summary>
        /// Initializes the effective grain-size list.
        /// The triplet values are Grain Size, Cutting Depth, Effective Grain Size
        /// </summary>
        /// <param name="data">The effective grain-size data to initialize with.</param>
        public void InitializeEffectiveGrainsizeData(List<Tuple<decimal, decimal, decimal>> data)
        {
            this.effectiveGrainSizeData = data;
        }

        /// <summary>
        /// Gets the effective grain-size corresponding to a grain size and a cutting depth.
        /// </summary>
        /// <param name="grainSize">The grain size.</param>
        /// <param name="cuttingDepth">The cutting depth.</param>
        /// <returns>The effective grain size corresponding to a grain size and a cutting depth.</returns>
        public decimal GetEffectiveGrainSize(decimal grainSize, decimal cuttingDepth)
        {
            foreach (Tuple<decimal, decimal, decimal> value in this.effectiveGrainSizeData)
            {
                if (value.Item1 == grainSize && value.Item2 == cuttingDepth)
                {
                    return value.Item3;
                }
            }

            return 0;
        }
    }
}
