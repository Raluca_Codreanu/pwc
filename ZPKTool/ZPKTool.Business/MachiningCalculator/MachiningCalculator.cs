﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using ZPKTool.Common;

namespace ZPKTool.Business.MachiningCalculator
{
    /// <summary>
    /// Contains the machining calculation logic used by MachiningCalculator.
    /// </summary>
    public class MachiningCalculator
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Initializes a new instance of the <see cref="MachiningCalculator"/> class.
        /// </summary>
        /// <exception cref="BusinessException">An error occurred while loading the knowledge-base.</exception>
        public MachiningCalculator()
        {
            this.Knowledgebase = new Knowledgebase();
        }

        /// <summary>
        /// Gets the knowledgebase that holds data needed for calculations.
        /// </summary>        
        public Knowledgebase Knowledgebase { get; private set; }

        /// <summary>
        /// Calculates the drilling operation values.
        /// </summary>
        /// <param name="inputData">The input data.</param>
        /// <returns>An object containing the results.</returns>
        public DrillingCalculationResult CalculateDrillingOperation(DrillingCalculationInputData inputData)
        {
            DrillingCalculationResult result = new DrillingCalculationResult();

            try
            {
                // Calculate machining volume
                result.MachiningVolume = inputData.DrillingDepth * Pow2(inputData.DrillingDepth) * 3.14m * 0.25m;

                // Calculate turning speed.
                if (inputData.Material != null && inputData.DrillingDiameter != 0m)
                {
                    result.TurningSpeed = inputData.Material.CuttingSpeed * 1000m / (3.14m * inputData.DrillingDiameter);
                }

                decimal? turningSpeed = inputData.ExpertModeCalculation ? inputData.TurningSpeed : result.TurningSpeed;

                // Calculate drilling time
                decimal value1 = 0m;
                if (inputData.DrillingType == DrillingType.ThroughHole)
                {
                    value1 = 1.2m;
                }
                else if (inputData.DrillingType == DrillingType.BlindHole)
                {
                    value1 = 1.05m;
                }
                else if (inputData.DrillingType == DrillingType.Counterbore)
                {
                    value1 = 1m;
                }
                else
                {
                    value1 = 1m;
                }

                decimal value2 = 0m;
                if (inputData.DrillingType == DrillingType.Tap)
                {
                    value2 = 0.5m;
                }
                else if (inputData.DrillingType == DrillingType.Reaming)
                {
                    value2 = 0.3m;
                }
                else
                {
                    value2 = 1m;
                }

                if (inputData.Material != null)
                {
                    decimal? subResult1 = inputData.Material.CuttingFeed + (0.012m * inputData.DrillingDiameter) + (0.1226m * turningSpeed / 60m);
                    if (subResult1 != 0m && value2 != 0m)
                    {
                        result.DrillingTime = value1 * (inputData.DrillingDepth / subResult1) / value2;
                        if (inputData.Boring &&
                           (inputData.DrillingType == DrillingType.BlindHole || inputData.DrillingType == DrillingType.ThroughHole))
                        {
                            result.DrillingTime *= 0.6m;
                        }
                    }
                }

                // Calculate cycle time
                if (inputData.MachineFeedSpeed != 0m)
                {
                    result.GrossProcessTime = (result.DrillingTime * inputData.HolesNumber) +
                        ((inputData.AvgHolesDistance / (inputData.MachineFeedSpeed * 1000m / 60m)) * inputData.HolesNumber) +
                        (inputData.HolesNumber * inputData.StartStopTimeForMachineFeed * 2m);
                    result.GrossProcessTime += inputData.ToolChangeTime;
                }
            }
            catch (ArithmeticException ex)
            {
                log.WarnException("Math error occurred while calculating Drilling Operation.", ex);
            }

            return result;
        }

        /// <summary>
        /// Calculates the values of a Turning Operation.
        /// </summary>
        /// <param name="input">The input data.</param>
        /// <returns>The result.</returns>
        public TurningCalculationResult CalculateTurningOperation(TurningCalculationInputData input)
        {
            TurningCalculationResult result = new TurningCalculationResult();

            try
            {
                decimal materialCuttingFeed = 0m;
                decimal materialCuttingSpeed = 0m;

                // Select the material's cutting speed and cutting feed based on the machining type.
                if (input.Material != null)
                {
                    TurningMaterialToBeMachined.MachiningParameters machiningParams = null;
                    input.Material.MachiningParams.TryGetValue(input.MachiningType, out machiningParams);
                    if (machiningParams != null)
                    {
                        materialCuttingFeed = machiningParams.CuttingFeed;
                        materialCuttingSpeed = machiningParams.CuttingSpeed;
                    }
                }

                // Calculate the turning speed
                if (input.TurningDiameter != 0m)
                {
                    result.TurningSpeed = (materialCuttingSpeed * 1000m) / (3.14m * input.TurningDiameter);
                }

                // Calculate the feed rate
                result.CalculatedFeedRate = materialCuttingFeed * 1.14m;

                decimal? feedRate = input.ExpertModeCalculation ? input.FeedRate : result.CalculatedFeedRate;
                decimal? turningSpeed = input.ExpertModeCalculation ? input.TurningSpeed : result.TurningSpeed;

                // Calculate the turning time
                decimal? value1 = null;
                if (input.TurningType == TurningType.Axial)
                {
                    if (input.TurningLengh.HasValue && feedRate.HasValue && feedRate != 0m &&
                        input.TurningDepth.HasValue && input.CutDepth.HasValue && input.CutDepth != 0m &&
                        input.MachineFeedSpeed != 0m && turningSpeed != 0m)
                    {
                        value1 = (Math.Ceiling(input.TurningLengh.Value / feedRate.Value) *
                            Math.Ceiling(input.TurningDepth.Value / input.CutDepth.Value) / (turningSpeed / 60m)) +
                            (Math.Ceiling(input.TurningDepth.Value / input.CutDepth.Value) * (input.TurningLengh / (input.MachineFeedSpeed * 1000m / 60m)));
                    }
                }
                else if (input.TurningType == TurningType.Radial)
                {
                    if (input.TurningDepth.HasValue && feedRate.HasValue && feedRate != 0m &&
                        input.TurningLengh.HasValue && input.CutDepth.HasValue && input.CutDepth != 0m &&
                        input.MachineFeedSpeed != 0 && turningSpeed != 0)
                    {
                        value1 = ((Math.Ceiling(input.TurningDepth.Value / feedRate.Value) *
                            Math.Ceiling(input.TurningLengh.Value / input.CutDepth.Value) / (turningSpeed / 60m)) * 1.05m)
                            + (Math.Ceiling(input.TurningLengh.Value / input.CutDepth.Value) * input.TurningDepth / (input.MachineFeedSpeed * 1000m / 60m));
                    }
                }

                decimal value2 = 0;
                if (input.TurningSituation == TurningSituation.ContinuousCut)
                {
                    value2 = 1m;
                }
                else if (input.TurningSituation == TurningSituation.ModerateIntersections)
                {
                    value2 = 0.75m;
                }
                else
                {
                    value2 = 0.6m;
                }

                result.TurningTime = value1 / value2;

                // Calculate the cycle time
                if (input.MachineFeedSpeed != 0m)
                {
                    result.GrossProcessTime = result.TurningTime + (2 * input.StartStopTimeForMachineFeed)
                        + (input.MachineFeedLength / (input.MachineFeedSpeed * 1000m / 60m));
                    result.GrossProcessTime += input.ToolChangeTime;
                }

                // Calculate machining volume
                result.MachiningVolume = input.TurningLengh *
                    ((Pow2(input.TurningDiameter) * 3.14m * 0.25m) - (Pow2(input.TurningDiameter - input.TurningDepth) * 3.14m * 0.25m));
            }
            catch (ArithmeticException ex)
            {
                log.WarnException("Math error occurred while calculating Turning Operation.", ex);
            }

            return result;
        }

        /// <summary>
        /// Calculates the values of a Milling Operation.
        /// </summary>
        /// <param name="input">The input data.</param>
        /// <returns>The result.</returns>
        public MillingCalculationResult CalculateMillingOperation(MillingCalculationInputData input)
        {
            MillingCalculationResult result = new MillingCalculationResult();

            try
            {
                // Calculate the machining volume
                result.MachiningVolume = input.MillingLength * input.MillingWidth * input.MillingDepth;

                decimal materialCuttingFeed = 0m;
                decimal materialCuttingSpeed = 0m;
                decimal toolTypeFactor = 0m;

                // Select the material's cutting speed and cutting feed based on the machining type.
                if (input.Material != null)
                {
                    MillingMaterialToBeMachined.MachiningParameters machiningParams = null;
                    input.Material.MachiningParams.TryGetValue(input.ToolType, out machiningParams);
                    if (machiningParams != null)
                    {
                        materialCuttingFeed = machiningParams.CuttingFeed;
                        materialCuttingSpeed = machiningParams.CuttingSpeed;
                        toolTypeFactor = machiningParams.ToolTypeFactor;
                    }
                }

                // Calculate the turning speed
                if (input.Material != null && input.MillingToolDiameter != 0m)
                {
                    result.TurningSpeed = materialCuttingSpeed * 1000m / (3.14m * input.MillingToolDiameter);
                }

                // Calculate the feed rate
                if (input.Material != null)
                {
                    result.FeedRatePerTooth = materialCuttingFeed * 1.07m;
                }

                decimal? feedRatePerTooth = input.ExpertModeCalculation ? input.FeedRatePerTooth : result.FeedRatePerTooth;
                decimal? turningSpeed = input.ExpertModeCalculation ? input.TurningSpeed : result.TurningSpeed;

                // This value will be used in place of the inputted MillingToolWidth and its value is MillingToolDiameter if the tool type
                // is Cylindrical Cutter or Side Milling Cutter, otherwise the value is MillingToolWidth
                decimal? toolWidth = input.MillingToolWidth;
                if (input.ToolType == MillingToolType.CylindricalCutter || input.ToolType == MillingToolType.SideMillCutter)
                {
                    toolWidth = input.MillingToolDiameter;
                }

                // Calculate the milling time
                if (turningSpeed != 0m && feedRatePerTooth != 0m && input.TeethNumber != 0m &&
                    input.MillingWidth.HasValue && toolWidth.HasValue && toolWidth != 0m &&
                    input.MillingDepth.HasValue && input.CutDepth.HasValue && input.CutDepth != 0m)
                {
                    result.MillingTime = (input.MillingLength / (turningSpeed * feedRatePerTooth * input.TeethNumber)) * 60m *
                        Math.Ceiling((input.MillingWidth.Value * 1.05m) / toolWidth.Value) *
                        Math.Ceiling(input.MillingDepth.Value / input.CutDepth.Value) * toolTypeFactor;
                }

                // Calculate the cycle time
                if (input.MillingDepth.HasValue && input.CutDepth.HasValue && input.CutDepth != 0m &&
                    input.MillingWidth.HasValue && toolWidth.HasValue && toolWidth != 0m &&
                    input.MachineFeedSpeed != 0m)
                {
                    result.GrossProcessTime = result.MillingTime + (Math.Ceiling(input.MillingDepth.Value / input.CutDepth.Value) *
                        Math.Ceiling(input.MillingWidth.Value / toolWidth.Value) * input.StartStopTimeForMachineFeed) +
                        (input.MachineFeedLength / (input.MachineFeedSpeed * 1000m / 60m));
                    result.GrossProcessTime += input.ToolChangeTime;
                }
            }
            catch (ArithmeticException ex)
            {
                log.WarnException("Math error occurred while calculating a Milling Operation.", ex);
            }

            return result;
        }

        /// <summary>
        /// Calculates the values of a Gear Hobbing Operation.
        /// </summary>
        /// <param name="input">The input data.</param>
        /// <returns>The result.</returns>
        public GearHobbingCalculationResult CalculateGearHobbingOperation(GearHobbingCalculationInputData input)
        {
            GearHobbingCalculationResult result = new GearHobbingCalculationResult();

            try
            {
                // Calculate the turning speed
                if (input.Material != null && input.MillingToolDiameter != 0)
                {
                    result.TurningSpeedOfMillingTool = input.Material.CuttingSpeed * 1000m / (3.14m * input.MillingToolDiameter);
                }

                // Calculate the feed rate per tooth
                if (input.Material != null)
                {
                    result.FeedRatePerTooth = input.Material.CuttingFeed * 1.07m;
                }

                decimal? feedRatePerTooth = input.ExpertModeCalculation ? input.FeedRatePerTooth : result.FeedRatePerTooth;
                decimal? turningSpeed = input.ExpertModeCalculation ? input.TurningSpeed : result.TurningSpeedOfMillingTool;

                // Calculate the milling time
                if (feedRatePerTooth != 0m && input.GearModul != 0m && turningSpeed != 0m &&
                    input.Material != null && input.Material.CuttingFeed != 0m && input.TeethNumber != 0m)
                {
                    result.MillingTime = input.GearModul / feedRatePerTooth * (input.GearDiameter / (input.GearModul * input.TeethNumber)) /
                        turningSpeed * 60m * (input.GearWidth / (input.Material.CuttingFeed * 10));
                }

                // Calculate the cycle time
                if (input.GearModul.HasValue && input.CutDepth.HasValue && input.CutDepth != 0m &&
                    input.GearWidth.HasValue && input.MillingToolDiameter.HasValue && input.MillingToolDiameter != 0m &&
                    input.MachineFeedSpeed != 0m)
                {
                    result.GrossProcessTime = result.MillingTime + (Math.Ceiling(input.GearModul.Value / input.CutDepth.Value) *
                        Math.Ceiling(input.GearWidth.Value / input.MillingToolDiameter.Value) * input.StartStopTimeForMachineFeed) +
                        (input.MachineFeedLength / (input.MachineFeedSpeed * 1000));
                    result.GrossProcessTime += input.ToolChangeTime;
                }

                // Calculate the machining volume
                result.MachiningVolume = input.GearDiameter * Pow2(input.GearWidth) * input.GearModul;
            }
            catch (ArithmeticException ex)
            {
                log.WarnException("Error occurred while calculating a Gear Hobbing Operation.", ex);
            }

            return result;
        }

        /// <summary>
        /// Calculates the values of a Grinding Operation.
        /// </summary>
        /// <param name="input">The input data.</param>
        /// <returns>The result.</returns>
        public GrindingCalculationResult CalculateGrindingOperation(GrindingCalculationInputData input)
        {
            GrindingCalculationResult result = new GrindingCalculationResult();

            try
            {
                // Calculate the turning speed.
                if (input.Material != null && input.GrindingToolDiameter != 0m)
                {
                    result.TurningSpeedOfGrindingTool = input.Material.VCBase * 1000m / (3.14m * input.GrindingToolDiameter);
                }

                // Calculate the feed rate
                if (input.Material != null && input.Material.QBase != 0m)
                {
                    result.FeedRate = input.Material.VCBase / (input.Material.QBase / 60m);
                }

                decimal? feedRate = input.ExpertModeCalculation ? input.FeedRate : result.FeedRate;
                decimal? turningSpeed = input.ExpertModeCalculation ? input.TurningSpeed : result.TurningSpeedOfGrindingTool;

                // Calculate the Average Grain Distance
                if (input.GrindingGrainSize.HasValue && input.CuttingDepthPerCycle.HasValue)
                {
                    result.AverageGainDistance =
                        this.Knowledgebase.GrindingData.GetEffectiveGrainSize(input.GrindingGrainSize.Value, input.CuttingDepthPerCycle.Value);
                }

                // Calculate the HM value
                if (turningSpeed != 0m && feedRate != 0m && input.GrindingToolDiameter != 0m)
                {
                    if (input.OperationType == GrindingOperationType.SurfaceGrindingColinear ||
                        input.OperationType == GrindingOperationType.SurfaceGrindingPerpendicular)
                    {
                        result.HM = Sqrt(input.CuttingDepthPerCycle / input.GrindingToolDiameter) *
                            (result.AverageGainDistance / (turningSpeed / feedRate));
                    }
                    else if (input.OperationType == GrindingOperationType.CylindricalGrindingAxialOuter)
                    {
                        if (input.GrindingPartDiameter != 0m)
                        {
                            result.HM = Sqrt(input.CuttingDepthPerCycle * ((1 / input.GrindingToolDiameter) + (1 / input.GrindingPartDiameter))) *
                                (result.AverageGainDistance / (turningSpeed / feedRate));
                        }
                    }
                    else if (input.OperationType == GrindingOperationType.CylindricalGrindingAxialInner)
                    {
                        if (input.GrindingPartDiameter != 0m)
                        {
                            result.HM = Sqrt(input.CuttingDepthPerCycle * ((1 / input.GrindingToolDiameter) - (1 / input.GrindingPartDiameter))).GetValueOrDefault()
                                * (result.AverageGainDistance / (turningSpeed / feedRate));
                        }
                    }
                    else
                    {
                        // GrindingOperationType.CylindricalGrindingRadialOuter
                        if (input.GrindingPartDiameter != 0m)
                        {
                            result.HM = Sqrt(input.CuttingDepthPerCycle * ((1 / input.GrindingToolDiameter) + (1 / input.GrindingPartDiameter))) *
                                (result.AverageGainDistance / (turningSpeed / feedRate));
                        }
                    }
                }

                // Calculate the Grinding Time
                if (input.GrindingLength.HasValue && result.HM.HasValue &&
                    input.GrindingWidth.HasValue && input.GrindingToolWidth.HasValue && input.GrindingToolWidth != 0m &&
                    input.GrindingDepth.HasValue && input.CuttingDepthPerCycle.HasValue && input.CuttingDepthPerCycle != 0m)
                {
                    if (result.HM.Value != 0m)
                    {
                        result.GrindingTime = (Math.Ceiling(input.GrindingLength.Value / result.HM.Value) *
                            Math.Ceiling(input.GrindingWidth.Value / input.GrindingToolWidth.Value) *
                            Math.Ceiling(input.GrindingDepth.Value / input.CuttingDepthPerCycle.Value)) /
                            (input.MachineFeedSpeed * 1000m) * 60m;
                    }
                    else
                    {
                        result.GrindingTime = 0m;
                    }
                }

                // Calculate the Cycle Time
                if (input.GrindingWidth.HasValue && input.GrindingToolWidth.HasValue && input.GrindingToolWidth != 0m &&
                    input.GrindingDepth.HasValue && input.CuttingDepthPerCycle.HasValue && input.CuttingDepthPerCycle != 0m)
                {
                    result.GrossProcessTime = ((Math.Ceiling(input.GrindingWidth.Value / input.GrindingToolWidth.Value) *
                        Math.Ceiling(input.GrindingDepth.Value / input.CuttingDepthPerCycle.Value)) * input.StartStopTimeForMachineFeed) + result.GrindingTime;
                    result.GrossProcessTime += input.ToolChangeTime;
                }

                // Calculate the machining volume.
                result.MachiningVolume = input.GrindingLength * Pow2(input.GrindingWidth) * input.GrindingDepth;
            }
            catch (ArithmeticException ex)
            {
                log.WarnException("Error occurred while calculating a Grinding Operation.", ex);
            }

            return result;
        }

        /// <summary>
        /// Raises a decimal number at power 2
        /// </summary>
        /// <param name="d">The decimal.</param>
        /// <returns>A decimal number.</returns>
        private static decimal? Pow2(decimal? d)
        {
            return d * d;
        }

        /// <summary>
        /// Gets the square root of a decimal.
        /// </summary>
        /// <param name="d">The decimal.</param>
        /// <returns>The square root.</returns>
        private static decimal? Sqrt(decimal? d)
        {
            if (!d.HasValue)
            {
                return null;
            }

            double sqrt = Math.Sqrt(Convert.ToDouble(d.Value));
            if (!double.IsNaN(sqrt))
            {
                return Convert.ToDecimal(sqrt);
            }
            else
            {
                return null;
            }
        }
    }
}
