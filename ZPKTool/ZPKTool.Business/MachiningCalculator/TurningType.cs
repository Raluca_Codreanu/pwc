﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.MachiningCalculator
{
    /// <summary>
    /// The type of a Turning Operation
    /// </summary>
    public enum TurningType
    {
        /// <summary>
        /// Axial turning
        /// </summary>
        Axial = 1,

        /// <summary>
        /// Radial turning
        /// </summary>
        Radial = 2
    }
}
