﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Text;
using OfficeOpenXml;
using ZPKTool.Common;

namespace ZPKTool.Business.MachiningCalculator
{
    /// <summary>
    /// This class contains the data necessary for machining calculations.
    /// </summary>
    public class Knowledgebase
    {
        /// <summary>
        /// The path to the file containing the knowledgebase data.
        /// </summary>
        private const string KnowledgeBaseFilePath = @".\Resources\MachiningCalculatorKB.xlsx";

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();
                
        /// <summary>
        /// Initializes a new instance of the <see cref="Knowledgebase"/> class.
        /// </summary>
        /// <exception cref="BusinessException">An error occurred while loading the knowledge base data from the file.</exception>
        public Knowledgebase()
        {
            Initialize();
        }

        /// <summary>
        /// Gets the data for the Setup Time operation.
        /// </summary>
        public SetupTimeKnowledgebaseData SetupTimeData { get; private set; }

        /// <summary>
        /// Gets the data needed for the Drilling Operation.
        /// </summary>
        public DrillingKnowledgebaseData DrillingData { get; private set; }

        /// <summary>
        /// Gets the Turning knowledgebase data.
        /// </summary>
        public TurningKnowledgebaseData TurningData { get; private set; }

        /// <summary>
        /// Gets the Milling knowledgebase data.
        /// </summary>
        public MillingKnowledgebaseData MillingData { get; private set; }

        /// <summary>
        /// Gets the Gear Hobbing knowledgebase data.
        /// </summary>
        public GearHobbingKnowledgebaseData GearHobbingData { get; private set; }

        /// <summary>
        /// Gets the Grinding knowledgebase data.
        /// </summary>
        public GrindingKnowledgebaseData GrindingData { get; private set; }

        /// <summary>
        /// Initializes the knowledgebase data.
        /// </summary>
        /// <exception cref="BusinessException">An error occurred while loading the data from the file.</exception>
        private void Initialize()
        {
            SetupTimeData = new SetupTimeKnowledgebaseData();
            DrillingData = new DrillingKnowledgebaseData();
            TurningData = new TurningKnowledgebaseData();
            MillingData = new MillingKnowledgebaseData();
            GearHobbingData = new GearHobbingKnowledgebaseData();
            GrindingData = new GrindingKnowledgebaseData();

            try
            {
                MemoryStream dataStream = new MemoryStream(Properties.Resources.MachiningCalculatorKB);
                using (ExcelPackage package = new ExcelPackage(dataStream))
                {
                    InitializeSetupTimeData(package.Workbook);
                    InitializeDrillingData(package.Workbook);
                    InitializeTurningData(package.Workbook);
                    InitializeMillingData(package.Workbook);
                    InitializeGearHobbingData(package.Workbook);
                    InitializeGrindingData(package.Workbook);
                }
            }
            catch (Exception ex)
            {
                log.ErrorException("An error occurred while reading the Machining Calculator KB data.", ex);
                throw new BusinessException(ErrorCodes.MachiningCalculatorKBLoadUnknownError);
            }
        }

        /// <summary>
        /// Initializes the setup time data.
        /// </summary>
        /// <param name="workbook">The workbook from which to take the data for initialization.</param>
        private void InitializeSetupTimeData(ExcelWorkbook workbook)
        {
            ExcelWorksheet otherDataSheet = workbook.Worksheets["SetupTime_Other"];
            if (otherDataSheet == null)
            {
                log.Warn("Machining calculator KB init: SetupTime_Other worksheet not found.");
                return;
            }

            this.SetupTimeData.DefaultSetupTimeInTheMachine = ConvertTo<decimal>(otherDataSheet.Cells["B3"].Value, 0m);
            this.SetupTimeData.DefaultSetupTimeOutsideTheMachine = ConvertTo<decimal>(otherDataSheet.Cells["B4"].Value, 0m);
        }

        /// <summary>
        /// Initializes the drilling data part of the knowledgebase.
        /// </summary>
        /// <param name="workbook">The workbook from which to take the data for initialization.</param>
        [SuppressMessage("Microsoft.StyleCop.CSharp.SpacingRules", "SA1002:SemicolonsMustBeSpacedCorrectly", Justification = "The for statement with 2 consecutive semicolons is correct.")]
        private void InitializeDrillingData(ExcelWorkbook workbook)
        {
            ExcelWorksheet otherDataSheet = workbook.Worksheets["Drilling_Other"];
            if (otherDataSheet == null)
            {
                log.Warn("Machining calculator KB init: Drilling_Other worksheet not found.");
                return;
            }

            // Init the default values
            this.DrillingData.DefaultAvgHolesDistance = ConvertTo<decimal>(otherDataSheet.Cells["B8"].Value, 0m);
            this.DrillingData.DefaultDrillingDepth = ConvertTo<decimal>(otherDataSheet.Cells["B6"].Value, 0m);
            this.DrillingData.DefaultDrillingDiameter = ConvertTo<decimal>(otherDataSheet.Cells["B5"].Value, 0m);
            this.DrillingData.DefaultHolesNumber = ConvertTo<int>(otherDataSheet.Cells["B7"].Value, 0);
            this.DrillingData.DefaultMachineFeedSpeed = ConvertTo<decimal>(otherDataSheet.Cells["B3"].Value, 0m);
            this.DrillingData.DefaultStartStopTimeForMachineFeed = ConvertTo<decimal>(otherDataSheet.Cells["B4"].Value, 0m);
            this.DrillingData.DefaultToolChangeTime = ConvertTo<decimal>(otherDataSheet.Cells["B9"].Value, 0m);

            ExcelWorksheet drillingMaterialsSheet = workbook.Worksheets["Drilling_Materials"];
            if (drillingMaterialsSheet == null)
            {
                log.Warn("Machining calculator KB init: Drilling_Materials worksheet not found.");
                return;
            }

            // Read the materials data
            for (int rowNo = 2; ; rowNo++)
            {
                // Check if we have reached an empty row and stop reading data if so
                if (drillingMaterialsSheet.Cells[rowNo, 1, rowNo, 5].Count() == 0)
                {
                    break;
                }

                int materialID;
                bool materialIDValid = ConvertTo<int>(drillingMaterialsSheet.Cells[rowNo, 1].Value, out materialID);
                if (!materialIDValid)
                {
                    log.Warn("Row {0} from the Drilling Materials knowledge base contains an invalid material ID.", rowNo);
                    continue;
                }

                DrillingMaterialToBeMachined material = new DrillingMaterialToBeMachined();

                material.MaterialID = materialID;
                material.MaterialName = ConvertTo<string>(drillingMaterialsSheet.Cells[rowNo, 2].Value, string.Empty);
                material.ToolMaterialType = ConvertTo<string>(drillingMaterialsSheet.Cells[rowNo, 3].Value, string.Empty);
                material.CuttingSpeed = ConvertTo<decimal>(drillingMaterialsSheet.Cells[rowNo, 4].Value, 0m);
                material.CuttingFeed = ConvertTo<decimal>(drillingMaterialsSheet.Cells[rowNo, 5].Value, 0m);

                this.DrillingData.MaterialsToMachine.Add(material);
            }
        }

        /// <summary>
        /// Initializes the Turning data part of the knowledgebase.
        /// </summary>
        /// <param name="workbook">The workbook from which to take the data for initialization.</param>
        [SuppressMessage("Microsoft.StyleCop.CSharp.SpacingRules", "SA1002:SemicolonsMustBeSpacedCorrectly", Justification = "The for statement with 2 consecutive semicolons is correct.")]
        private void InitializeTurningData(ExcelWorkbook workbook)
        {
            ExcelWorksheet otherDataSheet = workbook.Worksheets["Turning_Other"];
            if (otherDataSheet == null)
            {
                log.Warn("Machining calculator KB init: Turning_Other worksheet not found.");
                return;
            }

            this.TurningData.DefaultCutDepth = ConvertTo<decimal>(otherDataSheet.Cells["B4"].Value, 0m);
            this.TurningData.DefaultMachineFeedLength = ConvertTo<decimal>(otherDataSheet.Cells["B6"].Value, 0m);
            this.TurningData.DefaultMachineFeedSpeed = ConvertTo<decimal>(otherDataSheet.Cells["B3"].Value, 0m);
            this.TurningData.DefaultStartStopTimeForMachineFeed = ConvertTo<decimal>(otherDataSheet.Cells["B5"].Value, 0m);
            this.TurningData.DefaultTurningDepth = ConvertTo<decimal>(otherDataSheet.Cells["B9"].Value, 0m);
            this.TurningData.DefaultTurningDiameter = ConvertTo<decimal>(otherDataSheet.Cells["B7"].Value, 0m);
            this.TurningData.DefaultTurningLengh = ConvertTo<decimal>(otherDataSheet.Cells["B8"].Value, 0m);
            this.TurningData.DefaultToolChangeTime = ConvertTo<decimal>(otherDataSheet.Cells["B10"].Value, 0m);

            ExcelWorksheet materialsSheet = workbook.Worksheets["Turning_Materials"];
            if (materialsSheet == null)
            {
                log.Warn("Machining calculator KB init: Turning_Materials worksheet not found.");
                return;
            }

            // Read the materials data
            for (int rowNo = 2; ; rowNo++)
            {
                // Check if we have reached an empty row and stop reading data if so
                if (materialsSheet.Cells[rowNo, 1, rowNo, 8].Count() == 0)
                {
                    break;
                }

                // Read the material data from the current row and store it in the Turning knowledge base.
                string machType = ConvertTo<string>(materialsSheet.Cells[rowNo, 1].Value, null);
                TurningMachiningType machiningType;
                if (Enum.TryParse(machType, out machiningType))
                {
                    int materialID;
                    bool materialIDValid = ConvertTo<int>(materialsSheet.Cells[rowNo, 2].Value, out materialID);
                    if (!materialIDValid)
                    {
                        log.Warn("Row {0} from the Turning Materials knowledge base contains an invalid material ID.", rowNo);
                        continue;
                    }

                    string materialName;
                    bool materialNameValid = ConvertTo<string>(materialsSheet.Cells[rowNo, 3].Value, out materialName);
                    if (!materialNameValid)
                    {
                        log.Warn("Row {0} from the Turning Materials knowledge base contains an invalid material name.", rowNo);
                        continue;
                    }

                    string toolMaterialType = ConvertTo<string>(materialsSheet.Cells[rowNo, 4].Value, null);
                    decimal cuttingSpeed = ConvertTo<decimal>(materialsSheet.Cells[rowNo, 5].Value, 0m);
                    decimal cuttingFeedVariable1 = ConvertTo<decimal>(materialsSheet.Cells[rowNo, 6].Value, 0m);
                    decimal cuttingFeedVariable2 = ConvertTo<decimal>(materialsSheet.Cells[rowNo, 7].Value, 0m);
                    decimal cuttingFeed = ConvertTo<decimal>(materialsSheet.Cells[rowNo, 8].Value, 0m);

                    // The materials are present multiple times in the materials knowledgebase in order to give them machining parameters for each machining type,
                    // so create a new material only if is not already created.
                    TurningMaterialToBeMachined material = this.TurningData.MaterialsToMachine.FirstOrDefault(m => m.MaterialID == materialID);
                    if (material == null)
                    {
                        material = new TurningMaterialToBeMachined();
                        material.MaterialID = materialID;
                        material.MaterialName = string.IsNullOrEmpty(toolMaterialType) ? materialName : materialName + " (" + toolMaterialType + ")";
                        this.TurningData.MaterialsToMachine.Add(material);
                    }

                    TurningMaterialToBeMachined.MachiningParameters machiningParams;
                    if (!material.MachiningParams.TryGetValue(machiningType, out machiningParams))
                    {
                        machiningParams = new TurningMaterialToBeMachined.MachiningParameters();
                        material.MachiningParams.Add(machiningType, machiningParams);
                    }

                    machiningParams.CuttingFeed = cuttingFeed;
                    machiningParams.CuttingFeedVariable1 = cuttingFeedVariable1;
                    machiningParams.CuttingFeedVariable2 = cuttingFeedVariable2;
                    machiningParams.CuttingSpeed = cuttingSpeed;
                }
                else
                {
                    log.Warn("Row {0} from the Turning Materials knowledge base contains an invalid machining type.", rowNo);
                }
            }
        }

        /// <summary>
        /// Initializes the Milling data part of the knowledgebase.
        /// </summary>
        /// <param name="workbook">The workbook from which to take the data for initialization.</param>
        [SuppressMessage("Microsoft.StyleCop.CSharp.SpacingRules", "SA1002:SemicolonsMustBeSpacedCorrectly", Justification = "The for statement with 2 consecutive semicolons is correct.")]
        private void InitializeMillingData(ExcelWorkbook workbook)
        {
            ExcelWorksheet otherDataSheet = workbook.Worksheets["Milling_Other"];
            if (otherDataSheet == null)
            {
                log.Warn("Machining calculator KB init: Milling_Other worksheet not found.");
                return;
            }

            this.MillingData.DefaultCutDepth = ConvertTo<decimal>(otherDataSheet.Cells["B4"].Value, 0m);
            this.MillingData.DefaultMachineFeedLength = ConvertTo<decimal>(otherDataSheet.Cells["B9"].Value, 0m);
            this.MillingData.DefaultMachineFeedSpeed = ConvertTo<decimal>(otherDataSheet.Cells["B3"].Value, 0m);
            this.MillingData.DefaultMillingDepth = ConvertTo<decimal>(otherDataSheet.Cells["B12"].Value, 0m);
            this.MillingData.DefaultMillingLength = ConvertTo<decimal>(otherDataSheet.Cells["B10"].Value, 0m);
            this.MillingData.DefaultMillingToolDiameter = ConvertTo<decimal>(otherDataSheet.Cells["B6"].Value, 0m);
            this.MillingData.DefaultMillingToolWidth = ConvertTo<decimal>(otherDataSheet.Cells["B7"].Value, 0m);
            this.MillingData.DefaultMillingWidth = ConvertTo<decimal>(otherDataSheet.Cells["B11"].Value, 0m);
            this.MillingData.DefaultStartStopTimeForMachineFeed = ConvertTo<decimal>(otherDataSheet.Cells["B8"].Value, 0m);
            this.MillingData.DefaultTeethNumber = ConvertTo<decimal>(otherDataSheet.Cells["B5"].Value, 0m);
            this.MillingData.DefaultToolChangeTime = ConvertTo<decimal>(otherDataSheet.Cells["B13"].Value, 0m);

            // Init the materials data            
            ExcelWorksheet materialsSheet = workbook.Worksheets["Milling_Materials"];
            if (materialsSheet == null)
            {
                log.Warn("Machining calculator KB init: Milling_Materials worksheet not found.");
                return;
            }

            // Read the materials data
            for (int rowNo = 2; ; rowNo++)
            {
                // Check if we have reached an empty row and stop reading data if so
                if (materialsSheet.Cells[rowNo, 1, rowNo, 7].Count() == 0)
                {
                    break;
                }

                // Read the material data from the current row and store it in the Milling knowledge base.
                string crtToolTypeName = ConvertTo<string>(materialsSheet.Cells[rowNo, 1].Value, null);
                MillingToolType crtToolType;
                if (Enum.TryParse(crtToolTypeName, out crtToolType))
                {
                    int materialID;
                    bool materialIDValid = ConvertTo<int>(materialsSheet.Cells[rowNo, 3].Value, out materialID);
                    if (!materialIDValid)
                    {
                        log.Warn("Row {0} from the Milling Materials knowledge base contains an invalid material ID.", rowNo);
                        continue;
                    }

                    decimal toolTypeFactor = ConvertTo<decimal>(materialsSheet.Cells[rowNo, 2].Value, 0m);
                    string materialName = ConvertTo<string>(materialsSheet.Cells[rowNo, 4].Value, string.Empty);
                    string toolMaterial = ConvertTo<string>(materialsSheet.Cells[rowNo, 5].Value, string.Empty);
                    decimal cuttingSpeed = ConvertTo<decimal>(materialsSheet.Cells[rowNo, 6].Value, 0m);
                    decimal cuttingFeed = ConvertTo<decimal>(materialsSheet.Cells[rowNo, 7].Value, 0m);

                    // The materials are present multiple times in the materials knowledgebase in order to give them machining parameters for each tool type,
                    // so create a new material only if is not already created.
                    MillingMaterialToBeMachined material = this.MillingData.MaterialsToMachine.FirstOrDefault(m => m.MaterialID == materialID);
                    if (material == null)
                    {
                        material = new MillingMaterialToBeMachined();
                        material.MaterialID = materialID;
                        material.MaterialName = materialName;
                        material.ToolMaterialType = toolMaterial;
                        this.MillingData.MaterialsToMachine.Add(material);
                    }

                    MillingMaterialToBeMachined.MachiningParameters machiningParams;
                    if (!material.MachiningParams.TryGetValue(crtToolType, out machiningParams))
                    {
                        machiningParams = new MillingMaterialToBeMachined.MachiningParameters();
                        material.MachiningParams.Add(crtToolType, machiningParams);
                    }

                    machiningParams.CuttingFeed = cuttingFeed;
                    machiningParams.CuttingSpeed = cuttingSpeed;
                    machiningParams.ToolTypeFactor = toolTypeFactor;
                }
                else
                {
                    log.Warn("Row {0} from the Milling Materials knowledge base contains an invalid tool type.", rowNo);
                }
            }
        }

        /// <summary>
        /// Initializes the gear hobbing data.
        /// </summary>
        /// <param name="workbook">The workbook from which to take the data for initialization.</param>
        [SuppressMessage("Microsoft.StyleCop.CSharp.SpacingRules", "SA1002:SemicolonsMustBeSpacedCorrectly", Justification = "The for statement with 2 consecutive semicolons is correct.")]
        private void InitializeGearHobbingData(ExcelWorkbook workbook)
        {
            ExcelWorksheet otherDataSheet = workbook.Worksheets["GearHobbing_Other"];
            if (otherDataSheet == null)
            {
                log.Warn("Machining calculator KB init: GearHobbing_Other worksheet not found.");
                return;
            }

            this.GearHobbingData.DefaultMachineFeedSpeed = ConvertTo<decimal>(otherDataSheet.Cells["B3"].Value, 0m);
            this.GearHobbingData.DefaultCutDepth = ConvertTo<decimal>(otherDataSheet.Cells["B4"].Value, 0m);
            this.GearHobbingData.DefaultTeethNumber = ConvertTo<decimal>(otherDataSheet.Cells["B5"].Value, 0m);
            this.GearHobbingData.DefaultMillingToolDiameter = ConvertTo<decimal>(otherDataSheet.Cells["B6"].Value, 0m);
            this.GearHobbingData.DefaultStartStopTimeForMachineFeed = ConvertTo<decimal>(otherDataSheet.Cells["B7"].Value, 0m);
            this.GearHobbingData.DefaultMachineFeedLength = ConvertTo<decimal>(otherDataSheet.Cells["B8"].Value, 0m);
            this.GearHobbingData.DefaultGearDiameter = ConvertTo<decimal>(otherDataSheet.Cells["B9"].Value, 0m);
            this.GearHobbingData.DefaultGearWidth = ConvertTo<decimal>(otherDataSheet.Cells["B10"].Value, 0m);
            this.GearHobbingData.DefaultGearModul = ConvertTo<decimal>(otherDataSheet.Cells["B11"].Value, 0m);
            this.GearHobbingData.DefaultToolChangeTime = ConvertTo<decimal>(otherDataSheet.Cells["B12"].Value, 0m);

            ExcelWorksheet materialsSheet = workbook.Worksheets["GearHobbing_Materials"];
            if (materialsSheet == null)
            {
                log.Warn("Machining calculator KB init: GearHobbing_Materials worksheet not found.");
                return;
            }

            // Read the materials data
            for (int rowNo = 2; ; rowNo++)
            {
                // Check if we have reached an empty row and stop reading data if so
                if (materialsSheet.Cells[rowNo, 1, rowNo, 5].Count() == 0)
                {
                    break;
                }

                int materialID;
                bool materialIDValid = ConvertTo<int>(materialsSheet.Cells[rowNo, 1].Value, out materialID);
                if (!materialIDValid)
                {
                    log.Warn("Row {0} from the Gear Hobbing Materials knowledge base contains an invalid material ID.", rowNo);
                    continue;
                }

                GearHobbingMaterialToBeMachined material = new GearHobbingMaterialToBeMachined();

                material.MaterialID = materialID;
                material.MaterialName = ConvertTo<string>(materialsSheet.Cells[rowNo, 2].Value, string.Empty);
                material.ToolMaterialType = ConvertTo<string>(materialsSheet.Cells[rowNo, 3].Value, string.Empty);
                material.CuttingSpeed = ConvertTo<decimal>(materialsSheet.Cells[rowNo, 4].Value, 0m);
                material.CuttingFeed = ConvertTo<decimal>(materialsSheet.Cells[rowNo, 5].Value, 0m);

                this.GearHobbingData.MaterialsToMachine.Add(material);
            }
        }

        /// <summary>
        /// Initializes the grinding data.
        /// </summary>
        /// <param name="workbook">The workbook from which to take the data for initialization.</param>
        private void InitializeGrindingData(ExcelWorkbook workbook)
        {
            ExcelWorksheet otherDataSheet = workbook.Worksheets["Grinding_Other"];
            if (otherDataSheet == null)
            {
                log.Warn("Machining calculator KB init: Grinding_Other worksheet not found.");
                return;
            }

            // Init default values
            this.GrindingData.DefaultCuttingDepthPerCycle = ConvertTo<decimal>(otherDataSheet.Cells["B5"].Value, 0m);
            this.GrindingData.DefaultGrindingDepth = ConvertTo<decimal>(otherDataSheet.Cells["B13"].Value, 0m);
            this.GrindingData.DefaultGrindingGrainSize = ConvertTo<decimal>(otherDataSheet.Cells["B4"].Value, 0m);
            this.GrindingData.DefaultGrindingLength = ConvertTo<decimal>(otherDataSheet.Cells["B11"].Value, 0m);
            this.GrindingData.DefaultGrindingPartDiameter = ConvertTo<decimal>(otherDataSheet.Cells["B10"].Value, 0m);
            this.GrindingData.DefaultGrindingToolDiameter = ConvertTo<decimal>(otherDataSheet.Cells["B6"].Value, 0m);
            this.GrindingData.DefaultGrindingToolWidth = ConvertTo<decimal>(otherDataSheet.Cells["B7"].Value, 0m);
            this.GrindingData.DefaultGrindingWidth = ConvertTo<decimal>(otherDataSheet.Cells["B12"].Value, 0m);
            this.GrindingData.DefaultMachineFeedLength = ConvertTo<decimal>(otherDataSheet.Cells["B9"].Value, 0m);
            this.GrindingData.DefaultMachineFeedSpeed = ConvertTo<decimal>(otherDataSheet.Cells["B3"].Value, 0m);
            this.GrindingData.DefaultStartStopTimeForMachineFeed = ConvertTo<decimal>(otherDataSheet.Cells["B8"].Value, 0m);
            this.GrindingData.DefaultToolChangeTime = ConvertTo<decimal>(otherDataSheet.Cells["B14"].Value, 0m);

            // Initialize the Effective GrainSize data
            List<Tuple<decimal, decimal, decimal>> effectiveGrainSizeData = new List<Tuple<decimal, decimal, decimal>>();

            bool stopParsing = false;
            int rowNo = 4;
            do
            {
                // Read each grain size (on rows)
                object grainSizeValue = otherDataSheet.Cells[rowNo, 7].Value;
                if (grainSizeValue == null)
                {
                    stopParsing = true;
                }
                else
                {
                    decimal grainSize = 0;
                    bool grainSizeValid = false;
                    try
                    {
                        grainSize = Convert.ToDecimal(grainSizeValue);
                        grainSizeValid = true;
                    }
                    catch
                    {
                    }

                    if (grainSizeValid)
                    {
                        this.GrindingData.GrainSizeValues.Add(grainSize);

                        // Read each grinding depth(on columns)
                        bool stop2 = false;
                        int colNo = 8;
                        do
                        {
                            object grindingDepthVal = otherDataSheet.Cells[3, colNo].Value;
                            if (grindingDepthVal == null)
                            {
                                stop2 = true;
                            }
                            else
                            {
                                decimal grindingDepth = 0;
                                bool grindingDepthValid = false;
                                try
                                {
                                    grindingDepth = Convert.ToDecimal(grindingDepthVal);
                                    grindingDepthValid = true;
                                }
                                catch
                                {
                                }

                                if (grindingDepthValid)
                                {
                                    if (!this.GrindingData.GrindingDepthValues.Contains(grindingDepth))
                                    {
                                        this.GrindingData.GrindingDepthValues.Add(grindingDepth);
                                    }

                                    // Read the effective grain size
                                    decimal effectiveGrainSize = ConvertTo<decimal>(otherDataSheet.Cells[rowNo, colNo].Value, 0m);

                                    effectiveGrainSizeData.Add(new Tuple<decimal, decimal, decimal>(grainSize, grindingDepth, effectiveGrainSize));
                                }
                            }

                            colNo++;
                        }
                        while (!stop2);
                    }
                }

                rowNo++;
            }
            while (!stopParsing);

            this.GrindingData.InitializeEffectiveGrainsizeData(effectiveGrainSizeData);

            // initialize the materials data            
            ExcelWorksheet materialsSheet = workbook.Worksheets["Grinding_Materials"];
            if (materialsSheet == null)
            {
                log.Warn("Machining calculator KB init: Grinding_Materials worksheet not found.");
                return;
            }

            stopParsing = false;
            rowNo = 2;
            do
            {
                object materialName = materialsSheet.Cells[rowNo, 1].Value;
                object qbase = materialsSheet.Cells[rowNo, 2].Value;
                object vcbase = materialsSheet.Cells[rowNo, 3].Value;

                if (materialName == null && qbase == null && vcbase == null)
                {
                    stopParsing = true;
                }
                else
                {
                    GrindingMaterial material = new GrindingMaterial();
                    material.Name = ConvertTo<string>(materialName, string.Empty);
                    material.QBase = ConvertTo<decimal>(qbase, 0m);
                    material.VCBase = ConvertTo<decimal>(vcbase, 0m);
                    this.GrindingData.MaterialsToMachine.Add(material);
                }

                rowNo++;
            }
            while (!stopParsing);
        }

        /// <summary>
        /// Converts an object to a given type without throwing conversion errors.
        /// If the conversion fails it returns the value specified by the <paramref name="defaultValue"/> parameter.
        /// </summary>
        /// <typeparam name="T">The type to convert to.</typeparam>
        /// <param name="obj">The object to convert.</param>
        /// <param name="defaultValue">The value returned in case of a conversion error.</param>
        /// <returns>An instance of T.</returns>
        private T ConvertTo<T>(object obj, T defaultValue)
        {
            try
            {
                return (T)Convert.ChangeType(obj, typeof(T));
            }
            catch
            {
            }

            return defaultValue;
        }

        /// <summary>
        /// Converts an object to a specified type.
        /// If the conversion fails it returns false and <paramref name="convertedValue"/> is assigned the default value of type T.
        /// </summary>
        /// <typeparam name="T">The type to convert to.</typeparam>
        /// <param name="obj">The object to convert.</param>
        /// <param name="convertedValue">The converted value.</param>
        /// <returns>True if the conversion succeeded, false otherwise.</returns>
        private bool ConvertTo<T>(object obj, out T convertedValue)
        {
            try
            {
                convertedValue = (T)Convert.ChangeType(obj, typeof(T));
                return true;
            }
            catch
            {
                convertedValue = default(T);
                return false;
            }
        }
    }
}
