﻿using System;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;

namespace ZPKTool.Business
{
    /// <summary>
    /// Manages the deletion of entities and Trash Bin related operations.
    /// </summary>
    public class TrashManager
    {
        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The data manager used to save changes.
        /// </summary>
        private IDataSourceManager dataSourceManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="TrashManager"/> class.
        /// </summary>
        /// <param name="dataSourceManager">The data manager that will save the changes made using this instance.</param>
        /// <exception cref="ArgumentNullException"><paramref name="dataSourceManager"/> was null</exception>
        public TrashManager(IDataSourceManager dataSourceManager)
        {
            if (dataSourceManager == null)
            {
                throw new ArgumentNullException("dataSourceManager", "The data source manager was null");
            }

            this.dataSourceManager = dataSourceManager;
        }

        /// <summary>
        /// Deletes the specified entity to the trash bin.
        /// </summary>
        /// <param name="entity">The entity.</param>
        public void DeleteToTrashBin(object entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "The entity was null.");
            }

            try
            {
                if (entity as ITrashable == null)
                {
                    throw new NotSupportedException("The entity can not be deleted to trash bin");
                }

                // get the entity from the current context
                object freshEntity = this.GetEntityByObject(entity);
                if (freshEntity == null)
                {
                    return;
                }

                // If the entity is in the deleted state, the delete operation must not be performed                
                var entityRepository = this.dataSourceManager.Repository(freshEntity.GetType());
                if (entityRepository.IsDeleted(freshEntity))
                {
                    return;
                }

                if (entityRepository.IsAdded(freshEntity))
                {
                    throw new NotSupportedException("An entity in Added state ca not be delete to trash bin.");
                }

                // move the entity to trash bin
                ITrashable trashableEntity = null;
                ProjectFolder folder = freshEntity as ProjectFolder;
                if (folder != null)
                {
                    trashableEntity = this.dataSourceManager.ProjectFolderRepository.GetProjectFolderFull(folder.Guid);
                }

                Project project = freshEntity as Project;
                if (project != null)
                {
                    trashableEntity = this.dataSourceManager.ProjectRepository.GetProjectFull(project.Guid);
                }

                Assembly assy = freshEntity as Assembly;
                if (assy != null)
                {
                    trashableEntity = this.dataSourceManager.AssemblyRepository.GetAssemblyFull(assy.Guid);
                }

                Part part = freshEntity as Part;
                if (part != null)
                {
                    trashableEntity = this.dataSourceManager.PartRepository.GetPartFull(part.Guid);
                }

                Machine machine = freshEntity as Machine;
                if (machine != null)
                {
                    trashableEntity = this.dataSourceManager.MachineRepository.GetById(machine.Guid);
                }

                RawMaterial material = freshEntity as RawMaterial;
                if (material != null)
                {
                    trashableEntity = this.dataSourceManager.RawMaterialRepository.GetById(material.Guid);
                }

                Commodity commodity = freshEntity as Commodity;
                if (commodity != null)
                {
                    trashableEntity = this.dataSourceManager.CommodityRepository.GetById(commodity.Guid);
                }

                Consumable consumable = freshEntity as Consumable;
                if (consumable != null)
                {
                    trashableEntity = this.dataSourceManager.ConsumableRepository.GetById(consumable.Guid);
                }

                Die die = freshEntity as Die;
                if (die != null)
                {
                    trashableEntity = this.dataSourceManager.DieRepository.GetById(die.Guid);
                }

                if (trashableEntity == null)
                {
                    throw new BusinessException(ErrorCodes.ItemNotFound);
                }

                this.dataSourceManager.TrashBinRepository.DeleteToTrashBin(trashableEntity);
            }
            catch (DataAccessException ex)
            {
                log.ErrorException("Error while deleting an entity to the trash bin.", ex);
                throw new BusinessException(ex);
            }
        }

        /// <summary>
        /// Permanently deletes from the trash bin the entity specified by its corresponding trash bin item.
        /// </summary>
        /// <param name="item">The trash bin item corresponding to the entity to delete.</param>
        public void PermanentlyDeleteEntity(TrashBinItem item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item", "The item was null.");
            }

            try
            {
                // Delete the entity.
                TrashBinItemType itemType = (TrashBinItemType)item.EntityType;
                switch (itemType)
                {
                    case TrashBinItemType.ProjectFolder:
                        this.dataSourceManager.ProjectFolderRepository.DeleteByStoredProcedure(new ProjectFolder() { Guid = item.EntityGuid });
                        break;

                    case TrashBinItemType.Project:
                        this.dataSourceManager.ProjectRepository.DeleteByStoredProcedure(new Project() { Guid = item.EntityGuid });
                        break;

                    case TrashBinItemType.Assembly:
                        this.dataSourceManager.AssemblyRepository.DeleteByStoredProcedure(new Assembly() { Guid = item.EntityGuid });
                        break;

                    case TrashBinItemType.Part:
                    case TrashBinItemType.RawPart:
                        this.dataSourceManager.PartRepository.DeleteByStoredProcedure(new Part() { Guid = item.EntityGuid });
                        break;

                    case TrashBinItemType.RawMaterial:
                        this.dataSourceManager.RawMaterialRepository.DeleteByStoredProcedure(new RawMaterial() { Guid = item.EntityGuid });
                        break;

                    case TrashBinItemType.Commodity:
                        this.dataSourceManager.CommodityRepository.DeleteByStoredProcedure(new Commodity() { Guid = item.EntityGuid });
                        break;

                    case TrashBinItemType.Consumable:
                        this.dataSourceManager.ConsumableRepository.DeleteByStoredProcedure(new Consumable() { Guid = item.EntityGuid });
                        break;

                    case TrashBinItemType.Die:
                        this.dataSourceManager.DieRepository.DeleteByStoredProcedure(new Die() { Guid = item.EntityGuid });
                        break;

                    case TrashBinItemType.Machine:
                        this.dataSourceManager.MachineRepository.DeleteByStoredProcedure(new Machine() { Guid = item.EntityGuid });
                        break;

                    default:
                        break;
                }

                TrashBinItem dbItem = this.dataSourceManager.TrashBinRepository.GetByKey(item.Guid);
                if (dbItem != null)
                {
                    this.dataSourceManager.TrashBinRepository.DeleteByStoredProcedure(dbItem);
                }
            }
            catch (DataAccessException ex)
            {
                log.ErrorException("Failed to delete an entity from the trash bin.", ex);
                throw new BusinessException(ex);
            }
        }

        /// <summary>
        /// Restores from the trash bin the entity specified by its corresponding trash bin item.
        /// </summary>
        /// <param name="item">The item representing the entity to restore.</param>
        /// <param name="recoverParentToo">Indicates whether to load or not the parent item if it's deleted.</param>
        /// <returns>
        /// The restored trash bin item which is different from the input item if its object had a deleted parent, in which case the parent was also restored.
        /// null - if the trash bin item has a parent also deleted and recoverParentToo is "false"
        /// </returns>
        /// <exception cref="System.ArgumentNullException">The item was null</exception>
        /// <exception cref="BusinessException">Thrown if a BusinessException occurs</exception>
        public TrashBinItem RestoreEntity(TrashBinItem item, bool recoverParentToo = false)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item", "The item was null");
            }

            try
            {
                ITrashable entityToRecover = null;
                TrashBinItem deletedParentItem = CheckIfParentAlsoDeleted(item);

                if (deletedParentItem != null)
                {
                    if (recoverParentToo)
                    {
                        entityToRecover = LoadEntityForDeletion(deletedParentItem);
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    entityToRecover = LoadEntityForDeletion(item);
                }

                if (entityToRecover == null)
                {
                    throw new BusinessException(ErrorCodes.ItemNotFound, string.Empty);
                }

                this.dataSourceManager.TrashBinRepository.RestoreFromTrashBin(entityToRecover);

                return deletedParentItem != null ? deletedParentItem : item;
            }
            catch (DataAccessException ex)
            {
                log.ErrorException("Failed to restore an entity from the trash bin.", ex);
                throw new BusinessException(ex);
            }
        }

        /// <summary>
        /// Permanently deletes all items from the trash bin.
        /// </summary>
        /// <exception cref="BusinessConcurrencyException">Thrown if an Optimistic Concurrency Exception occurs 
        /// and needs the user's attention.</exception>
        /// <exception cref="BusinessException">Thrown if a BusinessException occurs.</exception>
        public void EmptyTrashBin()
        {
            try
            {
                var items = GetAllTrashBinItems();
                foreach (TrashBinItem item in items)
                {
                    // delete without committing
                    PermanentlyDeleteEntity(item);
                }
            }
            catch (DataAccessException ex)
            {
                log.ErrorException("Failed to empty the trash bin.", ex);
                throw new BusinessException(ex);
            }
        }

        /// <summary>
        /// Get the number of items in the trash bin.
        /// </summary>
        /// <returns>An integer.</returns>
        /// <exception cref="BusinessException">Error occurred while retrieving the count.</exception>
        public int GetItemsCount()
        {
            try
            {
                return this.dataSourceManager.TrashBinRepository.GetItemCount(SecurityManager.Instance.CurrentUser.Guid);
            }
            catch (DataAccessException exception)
            {
                log.ErrorException("Failed to get the trash bin item count.", exception);
                throw new BusinessException(exception);
            }
        }

        /// <summary>
        /// Get the items in the trash bin.
        /// </summary>
        /// <returns>The collection of trash bin items.</returns>
        public Collection<TrashBinItem> GetAllTrashBinItems()
        {
            try
            {
                Guid userId = SecurityManager.Instance.CurrentUser.Guid;
                return this.dataSourceManager.TrashBinRepository.GetAll(userId, true);
            }
            catch (DataAccessException ex)
            {
                log.ErrorException("Failed to get all trash bin items.", ex);
                throw new BusinessException(ex);
            }
        }

        /// <summary>
        /// Permanently deletes and entity and all its sub-entities from the database using store procedures.
        /// </summary>
        /// <param name="entity">The entity to be deleted.</param>
        public void DeleteByStoreProcedure(object entity)
        {
            ProjectFolder folder = entity as ProjectFolder;
            if (folder != null)
            {
                this.dataSourceManager.ProjectFolderRepository.DeleteByStoredProcedure(folder);
                this.dataSourceManager.ProjectFolderRepository.DetachAll(folder);
                return;
            }

            Project project = entity as Project;
            if (project != null)
            {
                this.dataSourceManager.ProjectRepository.DeleteByStoredProcedure(project);
                this.dataSourceManager.ProjectRepository.DetachAll(project);
                return;
            }

            Assembly assy = entity as Assembly;
            if (assy != null)
            {
                this.dataSourceManager.AssemblyRepository.DeleteByStoredProcedure(assy);
                this.dataSourceManager.AssemblyRepository.DetachAll(assy);
                return;
            }

            Part part = entity as Part;
            if (part != null)
            {
                this.dataSourceManager.PartRepository.DeleteByStoredProcedure(part);
                this.dataSourceManager.PartRepository.DetachAll(part);
                return;
            }

            Machine machine = entity as Machine;
            if (machine != null)
            {
                this.dataSourceManager.MachineRepository.DeleteByStoredProcedure(machine);
                this.dataSourceManager.MachineRepository.DetachAll(machine);
                return;
            }

            RawMaterial material = entity as RawMaterial;
            if (material != null)
            {
                this.dataSourceManager.RawMaterialRepository.DeleteByStoredProcedure(material);
                this.dataSourceManager.RawMaterialRepository.DetachAll(material);
                return;
            }

            Commodity commodity = entity as Commodity;
            if (commodity != null)
            {
                this.dataSourceManager.CommodityRepository.DeleteByStoredProcedure(commodity);
                this.dataSourceManager.CommodityRepository.DetachAll(commodity);
                return;
            }

            Consumable consumable = entity as Consumable;
            if (consumable != null)
            {
                this.dataSourceManager.ConsumableRepository.DeleteByStoredProcedure(consumable);
                this.dataSourceManager.ConsumableRepository.DetachAll(consumable);
                return;
            }

            Die die = entity as Die;
            if (die != null)
            {
                this.dataSourceManager.DieRepository.DeleteByStoredProcedure(die);
                this.dataSourceManager.DieRepository.DetachAll(die);
                return;
            }

            ProcessStep step = entity as ProcessStep;
            if (step != null)
            {
                // TODO: make a store procedure for process step
                // get the entity full before deleting it from the database.
                ProcessStep fullStep = this.dataSourceManager.ProcessStepRepository.GetProcessStepFull(step.Guid);
                this.dataSourceManager.ProcessStepRepository.RemoveAll(fullStep);
                this.dataSourceManager.SaveChanges();
                return;
            }

            Manufacturer manufacturer = entity as Manufacturer;
            if (manufacturer != null)
            {
                // TODO: make a store procedure for manufacturer
                // get the entity full before deleting it from the database.
                var fullManufacturer = this.dataSourceManager.ManufacturerRepository.GetById(manufacturer.Guid);
                this.dataSourceManager.ManufacturerRepository.RemoveAll(fullManufacturer);
                this.dataSourceManager.SaveChanges();
                return;
            }

            Customer supplier = entity as Customer;
            if (supplier != null)
            {
                // TODO: make a store procedure for supplier
                // get the entity full before deleting it from the database.
                Customer fullSupplier = this.dataSourceManager.SupplierRepository.FindById(supplier.Guid);
                this.dataSourceManager.SupplierRepository.RemoveAll(fullSupplier);
                this.dataSourceManager.SaveChanges();
                return;
            }

            throw new NotImplementedException();
        }

        #region Helpers

        /// <summary>
        /// Retrieves the object from the current context. Supports entities which do not have an entity key but have a valid id.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>The copy of the input entity from the current context</returns>
        private object GetEntityByObject(object entity)
        {
            ProjectFolder folder = entity as ProjectFolder;
            if (folder != null)
            {
                return this.dataSourceManager.ProjectFolderRepository.GetByKey(folder.Guid);
            }

            Project project = entity as Project;
            if (project != null)
            {
                return this.dataSourceManager.ProjectRepository.GetByKey(project.Guid);
            }

            Assembly assy = entity as Assembly;
            if (assy != null)
            {
                return this.dataSourceManager.AssemblyRepository.GetByKey(assy.Guid);
            }

            Part part = entity as Part;
            if (part != null)
            {
                return this.dataSourceManager.PartRepository.GetByKey(part.Guid);
            }

            Machine machine = entity as Machine;
            if (machine != null)
            {
                return this.dataSourceManager.MachineRepository.GetByKey(machine.Guid);
            }

            RawMaterial material = entity as RawMaterial;
            if (material != null)
            {
                return this.dataSourceManager.RawMaterialRepository.GetByKey(material.Guid);
            }

            Commodity commodity = entity as Commodity;
            if (commodity != null)
            {
                return this.dataSourceManager.CommodityRepository.GetByKey(commodity.Guid);
            }

            Consumable consumable = entity as Consumable;
            if (consumable != null)
            {
                return this.dataSourceManager.ConsumableRepository.GetByKey(consumable.Guid);
            }

            Die die = entity as Die;
            if (die != null)
            {
                return this.dataSourceManager.DieRepository.GetByKey(die.Guid);
            }

            ProcessStep step = entity as ProcessStep;
            if (step != null)
            {
                return this.dataSourceManager.ProcessStepRepository.GetByKey(step.Guid);
            }

            Manufacturer manufacturer = entity as Manufacturer;
            if (manufacturer != null)
            {
                return this.dataSourceManager.ManufacturerRepository.GetByKey(manufacturer.Guid);
            }

            Customer supplier = entity as Customer;
            if (supplier != null)
            {
                return this.dataSourceManager.SupplierRepository.GetByKey(supplier.Guid);
            }

            return null;
        }

        /// <summary>
        /// Loads the entity corresponding to a trash bin item with the purpose of being deleted.
        /// </summary>
        /// <param name="trashBinItem">The trash bin item.</param>
        /// <returns>The loaded entity.</returns>
        private ITrashable LoadEntityForDeletion(TrashBinItem trashBinItem)
        {
            if (trashBinItem == null)
            {
                throw new ArgumentNullException("trashBinItem", "The trash bin item was null.");
            }

            TrashBinItemType itemType = (TrashBinItemType)trashBinItem.EntityType;
            switch (itemType)
            {
                case TrashBinItemType.Project:
                    return this.dataSourceManager.ProjectRepository.GetProjectFull(trashBinItem.EntityGuid);

                case TrashBinItemType.ProjectFolder:
                    return this.dataSourceManager.ProjectFolderRepository.GetProjectFolderFull(trashBinItem.EntityGuid);

                case TrashBinItemType.Assembly:
                    return this.dataSourceManager.AssemblyRepository.GetAssemblyFull(trashBinItem.EntityGuid);

                case TrashBinItemType.Part:
                case TrashBinItemType.RawPart:
                    return this.dataSourceManager.PartRepository.GetPartFull(trashBinItem.EntityGuid);

                case TrashBinItemType.RawMaterial:
                    return this.dataSourceManager.RawMaterialRepository.GetById(trashBinItem.EntityGuid);

                case TrashBinItemType.Commodity:
                    return this.dataSourceManager.CommodityRepository.GetById(trashBinItem.EntityGuid);

                case TrashBinItemType.Consumable:
                    return this.dataSourceManager.ConsumableRepository.GetById(trashBinItem.EntityGuid);

                case TrashBinItemType.Die:
                    return this.dataSourceManager.DieRepository.GetById(trashBinItem.EntityGuid);

                case TrashBinItemType.Machine:
                    return this.dataSourceManager.MachineRepository.GetById(trashBinItem.EntityGuid);
            }

            log.Error("Full loading of its corresponding entity is not implemented for the trash bin item type {0}.", itemType);
            throw new BusinessException(ErrorCodes.InternalError);
        }

        /// <summary>
        /// Checks if the parent of a deleted item is also deleted.
        /// </summary>
        /// <param name="item">The item to check.</param>
        /// <returns>The trash bin item corresponding to the deleted parent or null if its parent is not deleted.</returns>
        private TrashBinItem CheckIfParentAlsoDeleted(TrashBinItem item)
        {
            if (item == null)
            {
                return null;
            }

            object topMostDeletedParent = null;

            TrashBinItemType entityType = (TrashBinItemType)item.EntityType;
            switch (entityType)
            {
                case TrashBinItemType.Assembly:
                    topMostDeletedParent = CheckAssemblyTreeForDeletedParents(item.EntityGuid);
                    break;

                case TrashBinItemType.Commodity:
                    topMostDeletedParent = CheckCommodityTreeForDeletedParents(item.EntityGuid);
                    break;

                case TrashBinItemType.Consumable:
                    topMostDeletedParent = CheckConsumableTreeForDeletedParents(item.EntityGuid);
                    break;

                case TrashBinItemType.Die:
                    topMostDeletedParent = CheckDieTreeForDeletedParents(item.EntityGuid);
                    break;

                case TrashBinItemType.Machine:
                    topMostDeletedParent = CheckMachineTreeForDeletedParents(item.EntityGuid);
                    break;

                case TrashBinItemType.Part:
                    topMostDeletedParent = CheckPartTreeForDeletedParents(item.EntityGuid);
                    break;

                case TrashBinItemType.RawPart:
                    topMostDeletedParent = CheckRawPartTreeForDeletedParents(item.EntityGuid);
                    break;

                case TrashBinItemType.Project:
                    topMostDeletedParent = CheckProjectTreeForDeletedParents(item.EntityGuid);
                    break;

                case TrashBinItemType.ProjectFolder:
                    topMostDeletedParent = CheckFolderTreeForDeletedParents(item.EntityGuid);
                    break;

                case TrashBinItemType.RawMaterial:
                    topMostDeletedParent = CheckRawMaterialTreeForDeletedParents(item.EntityGuid);
                    break;

                default:
                    break;
            }

            TrashBinItem parentTrashBinItem = null;
            IIdentifiable parent = topMostDeletedParent as IIdentifiable;
            if (parent != null && item.EntityGuid != parent.Guid)
            {
                parentTrashBinItem = this.dataSourceManager.TrashBinRepository.GetItemOfObject(parent.Guid);
            }

            return parentTrashBinItem;
        }

        /// <summary>
        /// Checks a folder's parent hierarchy for deleted parents and identifies the top-most one.
        /// </summary>
        /// <param name="folderId">The folder id.</param>
        /// <returns>The top-most deleted parent or null if no parent is deleted.</returns>
        private ProjectFolder CheckFolderTreeForDeletedParents(Guid folderId)
        {
            try
            {
                // TODO: used a table-valued-function to get the ids of all parent folders and load them in 1 query.
                ProjectFolder topDeletedFolder = new ProjectFolder() { Guid = folderId };
                while (true)
                {
                    var query = from f in this.dataSourceManager.EntityContext.ProjectFolderSet
                                        .Include(f => f.ParentProjectFolder)
                                where f.Guid == topDeletedFolder.Guid
                                select f;

                    topDeletedFolder = query.AsNoTracking().FirstOrDefault();
                    if (topDeletedFolder != null && topDeletedFolder.ParentProjectFolder != null && topDeletedFolder.ParentProjectFolder.IsDeleted)
                    {
                        topDeletedFolder = topDeletedFolder.ParentProjectFolder;
                    }
                    else
                    {
                        break;
                    }
                }

                return topDeletedFolder;
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw BusinessUtils.ParseDataException(ex, this.dataSourceManager.DatabaseId);
            }
        }

        /// <summary>
        /// Checks a project's parent hierarchy for deleted parents and identifies the top-most one.
        /// </summary>
        /// <param name="projectId">The project's id.</param>
        /// <returns>The top-most deleted parent or null if no parent is deleted.</returns>
        private object CheckProjectTreeForDeletedParents(Guid projectId)
        {
            try
            {
                var query = from proj in this.dataSourceManager.EntityContext.ProjectSet
                                .Include(p => p.ProjectFolder)
                            where proj.Guid == projectId
                            select proj;

                Project project = query.AsNoTracking().FirstOrDefault();

                if (project != null && project.ProjectFolder != null && project.ProjectFolder.IsDeleted)
                {
                    return CheckFolderTreeForDeletedParents(project.ProjectFolder.Guid);
                }
                else
                {
                    return project;
                }
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw BusinessUtils.ParseDataException(ex, this.dataSourceManager.DatabaseId);
            }
        }

        /// <summary>
        /// Checks an assembly's parent hierarchy for deleted parents and identifies the top-most one.
        /// </summary>
        /// <param name="assyId">The assembly's id.</param>
        /// <returns>The top-most deleted parent or null if no parent is deleted.</returns>
        private object CheckAssemblyTreeForDeletedParents(Guid assyId)
        {
            try
            {
                var query = from assy in this.dataSourceManager.EntityContext.AssemblySet
                                .Include(a => a.ParentAssembly)
                                .Include(a => a.Project)
                            where assy.Guid == assyId
                            select assy;

                Assembly assembly = query.AsNoTracking().FirstOrDefault();
                if (assembly == null)
                {
                    return null;
                }

                if (assembly.ParentAssembly != null && assembly.ParentAssembly.IsDeleted)
                {
                    return CheckAssemblyTreeForDeletedParents(assembly.ParentAssembly.Guid);
                }
                else if (assembly.Project != null && assembly.Project.IsDeleted)
                {
                    return CheckProjectTreeForDeletedParents(assembly.Project.Guid);
                }
                else
                {
                    return assembly;
                }
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw BusinessUtils.ParseDataException(ex, this.dataSourceManager.DatabaseId);
            }
        }

        /// <summary>
        /// Checks a Part's parent hierarchy for deleted parents and identifies the top-most one.
        /// </summary>
        /// <param name="partId">The Part's id.</param>
        /// <returns>The top-most deleted parent or null if no parent is deleted.</returns>
        private object CheckPartTreeForDeletedParents(Guid partId)
        {
            try
            {
                var query = from p in this.dataSourceManager.EntityContext.PartSet
                                .Include(p => p.Assembly)
                                .Include(p => p.Project)
                            where p.Guid == partId
                            select p;

                Part part = query.AsNoTracking().FirstOrDefault();
                if (part == null)
                {
                    return null;
                }

                if (part.Assembly != null && part.Assembly.IsDeleted)
                {
                    return CheckAssemblyTreeForDeletedParents(part.Assembly.Guid);
                }
                else if (part.Project != null && part.Project.IsDeleted)
                {
                    return CheckProjectTreeForDeletedParents(part.Project.Guid);
                }
                else
                {
                    return part;
                }
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw BusinessUtils.ParseDataException(ex, this.dataSourceManager.DatabaseId);
            }
        }

        /// <summary>
        /// Checks a RawPart's parent hierarchy for deleted parents and identifies the top-most one.
        /// </summary>
        /// <param name="rawPartId">The RawPart's id.</param>
        /// <returns>The top-most deleted parent or null if no parent is deleted.</returns>
        private object CheckRawPartTreeForDeletedParents(Guid rawPartId)
        {
            try
            {
                var query = from part in this.dataSourceManager.EntityContext.PartSet
                                .Include(p => p.RawPart)
                            where part.RawPart.Guid == rawPartId
                            select part;

                Part parentPart = query.AsNoTracking().FirstOrDefault();
                if (parentPart == null)
                {
                    return null;
                }

                if (parentPart != null && parentPart.IsDeleted)
                {
                    return CheckPartTreeForDeletedParents(parentPart.Guid);
                }
                else
                {
                    return parentPart.RawPart;
                }
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw BusinessUtils.ParseDataException(ex, this.dataSourceManager.DatabaseId);
            }
        }

        /// <summary>
        /// Checks a Machine's parent hierarchy for deleted parents and identifies the top-most one.
        /// </summary>
        /// <param name="machineId">The Machine's id.</param>
        /// <returns>The top-most deleted parent or null if no parent is deleted.</returns>
        private object CheckMachineTreeForDeletedParents(Guid machineId)
        {
            try
            {
                DataEntities context = this.dataSourceManager.EntityContext;
                var query = from mach in context.MachineSet
                            join ps in context.ProcessStepSet on mach.ProcessStep equals ps
                            join proc in context.ProcessSet on ps.Process equals proc
                            join assy in context.AssemblySet on proc equals assy.Process into assyGrop
                            join part in context.PartSet on proc equals part.Process into partGroup
                            where mach.Guid == machineId
                            select new
                            {
                                Machine = mach,
                                ParentAssembly = assyGrop.FirstOrDefault(),
                                ParentPart = partGroup.FirstOrDefault()
                            };

                var result = query.AsNoTracking().FirstOrDefault();
                if (result == null)
                {
                    return null;
                }

                if (result.ParentAssembly != null && result.ParentAssembly.IsDeleted)
                {
                    return CheckAssemblyTreeForDeletedParents(result.ParentAssembly.Guid);
                }
                else if (result.ParentPart != null && result.ParentPart.IsDeleted)
                {
                    return CheckPartTreeForDeletedParents(result.ParentPart.Guid);
                }
                else
                {
                    return result.Machine;
                }
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw BusinessUtils.ParseDataException(ex, this.dataSourceManager.DatabaseId);
            }
        }

        /// <summary>
        /// Checks a Die's parent hierarchy for deleted parents and identifies the top-most one.
        /// </summary>
        /// <param name="dieId">The Die's id.</param>
        /// <returns>The top-most deleted parent or null if no parent is deleted.</returns>
        private object CheckDieTreeForDeletedParents(Guid dieId)
        {
            try
            {
                DataEntities context = this.dataSourceManager.EntityContext;
                var query = from die in context.DieSet
                            join ps in context.ProcessStepSet on die.ProcessStep equals ps
                            join proc in context.ProcessSet on ps.Process equals proc
                            join assy in context.AssemblySet on proc equals assy.Process into assyGrop
                            join part in context.PartSet on proc equals part.Process into partGroup
                            where die.Guid == dieId
                            select new
                            {
                                Die = die,
                                ParentAssembly = assyGrop.FirstOrDefault(),
                                ParentPart = partGroup.FirstOrDefault()
                            };

                var result = query.AsNoTracking().FirstOrDefault();
                if (result == null)
                {
                    return null;
                }

                if (result.ParentAssembly != null && result.ParentAssembly.IsDeleted)
                {
                    return CheckAssemblyTreeForDeletedParents(result.ParentAssembly.Guid);
                }
                else if (result.ParentPart != null && result.ParentPart.IsDeleted)
                {
                    return CheckPartTreeForDeletedParents(result.ParentPart.Guid);
                }
                else
                {
                    return result.Die;
                }
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw BusinessUtils.ParseDataException(ex, this.dataSourceManager.DatabaseId);
            }
        }

        /// <summary>
        /// Checks a Consumable's parent hierarchy for deleted parents and identifies the top-most one.
        /// </summary>
        /// <param name="consumableId">The Consumable's id.</param>
        /// <returns>The top-most deleted parent or null if no parent is deleted.</returns>
        private object CheckConsumableTreeForDeletedParents(Guid consumableId)
        {
            try
            {
                DataEntities context = this.dataSourceManager.EntityContext;
                var query = from consumable in context.ConsumableSet
                            join ps in context.ProcessStepSet on consumable.ProcessStep equals ps
                            join proc in context.ProcessSet on ps.Process equals proc
                            join assy in context.AssemblySet on proc equals assy.Process into assyGrop
                            join part in context.PartSet on proc equals part.Process into partGroup
                            where consumable.Guid == consumableId
                            select new
                            {
                                Consumable = consumable,
                                ParentAssembly = assyGrop.FirstOrDefault(),
                                ParentPart = partGroup.FirstOrDefault()
                            };

                var result = query.AsNoTracking().FirstOrDefault();
                if (result == null)
                {
                    return null;
                }

                if (result.ParentAssembly != null && result.ParentAssembly.IsDeleted)
                {
                    return CheckAssemblyTreeForDeletedParents(result.ParentAssembly.Guid);
                }
                else if (result.ParentPart != null && result.ParentPart.IsDeleted)
                {
                    return CheckPartTreeForDeletedParents(result.ParentPart.Guid);
                }
                else
                {
                    return result.Consumable;
                }
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw BusinessUtils.ParseDataException(ex, this.dataSourceManager.DatabaseId);
            }
        }

        /// <summary>
        /// Checks a Commodity's parent hierarchy for deleted parents and identifies the top-most one.
        /// </summary>
        /// <param name="commodityId">The Commodity's id.</param>
        /// <returns>The top-most deleted parent or null if no parent is deleted.</returns>
        private object CheckCommodityTreeForDeletedParents(Guid commodityId)
        {
            try
            {
                DataEntities context = this.dataSourceManager.EntityContext;
                var query = from commodity in context.CommoditySet
                            join ps in context.ProcessStepSet on commodity.ProcessStep equals ps into psGroup
                            join proc in context.ProcessSet on psGroup.FirstOrDefault().Process equals proc into procGroup
                            join assy in context.AssemblySet on procGroup.FirstOrDefault() equals assy.Process into assyGrop
                            join part in context.PartSet on commodity.Part equals part into partGroup
                            where commodity.Guid == commodityId
                            select new
                            {
                                Commodity = commodity,
                                ParentAssembly = assyGrop.FirstOrDefault(),
                                ParentPart = partGroup.FirstOrDefault()
                            };

                var result = query.AsNoTracking().FirstOrDefault();
                if (result == null)
                {
                    return null;
                }

                if (result.ParentAssembly != null && result.ParentAssembly.IsDeleted)
                {
                    return CheckAssemblyTreeForDeletedParents(result.ParentAssembly.Guid);
                }
                else if (result.ParentPart != null && result.ParentPart.IsDeleted)
                {
                    return CheckPartTreeForDeletedParents(result.ParentPart.Guid);
                }
                else
                {
                    return result.Commodity;
                }
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw BusinessUtils.ParseDataException(ex, this.dataSourceManager.DatabaseId);
            }
        }

        /// <summary>
        /// Checks a RawMaterial's parent hierarchy for deleted parents and identifies the top-most one.
        /// </summary>
        /// <param name="rawMaterialId">The RawMaterial's id.</param>
        /// <returns>The top-most deleted parent or null if no parent is deleted.</returns>
        private object CheckRawMaterialTreeForDeletedParents(Guid rawMaterialId)
        {
            try
            {
                var query = from material in this.dataSourceManager.EntityContext.RawMaterialSet
                                .Include(m => m.Part)
                            where material.Guid == rawMaterialId
                            select material;

                RawMaterial rawMaterial = query.AsNoTracking().FirstOrDefault();
                if (rawMaterial == null)
                {
                    return null;
                }

                if (rawMaterial.Part != null && rawMaterial.Part.IsDeleted)
                {
                    return CheckPartTreeForDeletedParents(rawMaterial.Part.Guid);
                }
                else
                {
                    return rawMaterial;
                }
            }
            catch (System.Data.DataException ex)
            {
                log.ErrorException("Data error.", ex);
                throw BusinessUtils.ParseDataException(ex, this.dataSourceManager.DatabaseId);
            }
        }

        #endregion
    }
}