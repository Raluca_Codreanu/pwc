﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.IO;
using System.Linq;
using Ionic.Zip;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;

namespace ZPKTool.Business
{
    /// <summary>
    /// Contains methods for managing Media entities using the same <see cref="IDataSourceManager"/> instance.
    /// </summary>
    public class MediaManager
    {
        #region Attributes

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The data manager used to save changes.
        /// </summary>
        private IDataSourceManager dataSourceManager;

        #endregion Attributes

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MediaManager"/> class.
        /// </summary>
        /// <param name="dataSourceManager">The data manager that will save the changes made using this instance.</param>
        /// <exception cref="ArgumentNullException"><paramref name="dataSourceManager"/> was null</exception>
        public MediaManager(IDataSourceManager dataSourceManager)
        {
            if (dataSourceManager == null)
            {
                throw new ArgumentNullException("dataSourceManager", "The data source manager was null.");
            }

            this.dataSourceManager = dataSourceManager;
        }

        #endregion Constructor

        #region Get Media

        /// <summary>
        /// Return meta data information about the image/video of an entity.        
        /// </summary>
        /// <param name="entity">The entity object.</param>
        /// <returns>A media meta data object.</returns>
        public Collection<MediaMetaData> GetPictureOrVideoMetaData(object entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "The entity was null.");
            }

            try
            {
                var mediaTypes = new List<MediaType>() { MediaType.Image, MediaType.Video };
                var metaDataList = this.dataSourceManager.MediaRepository.GetMediaMetaData(entity, mediaTypes).ToList();
                var deletedMedia = this.dataSourceManager.MediaRepository.GetLocalEntities(EntityState.Deleted).ToList();
                metaDataList.RemoveAll(m => deletedMedia.FirstOrDefault(d => d.Guid == m.Guid) != null);

                return new Collection<MediaMetaData>(metaDataList);
            }
            catch (DataAccessException exception)
            {
                throw new BusinessException(exception);
            }
        }

        /// <summary>
        /// Gets all the pictures of the current entity object
        /// </summary>
        /// <param name="entity">The entity object</param>
        /// <returns>A list of Media references, representing all the media entities which belong to the entity parameter</returns>
        public Collection<Media> GetPictures(object entity)
        {
            if (entity == null)
            {
                return null;
            }

            try
            {
                var mediaList = this.dataSourceManager.MediaRepository.GetMedia(entity, new List<MediaType>() { MediaType.Image }).ToList();
                var deletedMedia = this.dataSourceManager.MediaRepository.GetLocalEntities(EntityState.Deleted).ToList();
                mediaList.RemoveAll(m => deletedMedia.FirstOrDefault(d => d.Guid == m.Guid) != null);

                return new Collection<Media>(mediaList);
            }
            catch (DataAccessException exception)
            {
                throw new BusinessException(exception);
            }
        }

        /// <summary>
        /// Returns the picture of a specified entity.        
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// A media object containing the picture or null if it has no picture.
        /// </returns>
        public Media GetPicture(object entity)
        {
            if (entity == null)
            {
                return null;
            }

            try
            {
                var media = this.dataSourceManager.MediaRepository.GetMedia(entity, new List<MediaType>() { MediaType.Image }).ToList();
                var deletedMedia = this.dataSourceManager.MediaRepository.GetLocalEntities(EntityState.Deleted).ToList();
                media.RemoveAll(m => deletedMedia.FirstOrDefault(d => d.Guid == m.Guid) != null);

                return media.FirstOrDefault();
            }
            catch (DataAccessException exception)
            {
                throw new BusinessException(exception);
            }
        }

        /// <summary>
        /// Get all media belonging to a specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>A list of detached media objects.</returns>
        public Collection<Media> GetAllMedia(object entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "The entity was null.");
            }

            try
            {
                var mediaTypes = new List<MediaType>() { MediaType.Document, MediaType.Image, MediaType.Video };
                var media = this.dataSourceManager.MediaRepository.GetMedia(entity, mediaTypes).ToList();
                var deletedMedia = this.dataSourceManager.MediaRepository.GetLocalEntities(EntityState.Deleted).ToList();
                media.RemoveAll(m => deletedMedia.FirstOrDefault(d => d.Guid == m.Guid) != null);

                return new Collection<Media>(media);
            }
            catch (DataAccessException ex)
            {
                throw new BusinessException(ex);
            }
        }

        /// <summary>
        /// Gets the media for the specified process steps.
        /// </summary>
        /// <param name="processSteps">The process steps.</param>
        /// <returns>
        /// A dictionary with the key containing the step id and the value being its associated media.
        /// If a step does not have media its id is not present in the dictionary with a null value associated to it.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">The process steps list was null.</exception>
        /// <exception cref="BusinessException">A data error occurred.</exception>        
        public Dictionary<Guid, Media> GetPicturesForProcessSteps(IEnumerable<ProcessStep> processSteps)
        {
            if (processSteps == null)
            {
                throw new ArgumentNullException("processSteps", "The process steps list was null.");
            }

            if (!processSteps.Any())
            {
                return new Dictionary<Guid, Media>();
            }

            try
            {
                return this.dataSourceManager.MediaRepository.GetMediaForProcessSteps(processSteps);
            }
            catch (DataAccessException ex)
            {
                throw new BusinessException(ex);
            }
        }

        /// <summary>
        /// Returns the picture or video of a specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// A media object containing the picture or video.
        /// </returns>
        public Media GetPictureOrVideo(object entity)
        {
            if (entity == null)
            {
                return null;
            }

            try
            {
                var media = this.dataSourceManager.MediaRepository.GetMedia(entity, new List<MediaType>() { MediaType.Image, MediaType.Video });
                var dbMedia = media.FirstOrDefault();

                // check if the retrieved media isn't already present in the current context and in the deleted state.
                if (dbMedia != null)
                {
                    var deletedMedia = this.dataSourceManager.MediaRepository.GetLocalEntities(EntityState.Deleted);
                    var delMedia = deletedMedia.FirstOrDefault(m => m.Guid == dbMedia.Guid);

                    if (delMedia != null)
                    {
                        // The media is in the deleted state in the context so do not return it.
                        return null;
                    }
                }

                return dbMedia;
            }
            catch (DataAccessException exception)
            {
                throw new BusinessException(exception);
            }
        }

        #endregion Get Media

        #region Add Media

        /// <summary>
        /// Adds the specified media to the given entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="newMedia">The media.</param>
        public void AddEntityMedia(IEntityWithMediaCollection entity, IEnumerable<Media> newMedia)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "The entity was null.");
            }

            if (newMedia == null)
            {
                throw new ArgumentNullException("newMedia", "the new media was null.");
            }

            foreach (Media media in newMedia)
            {
                IOwnedObject ownedObj = entity as IOwnedObject;
                if (ownedObj != null)
                {
                    media.Owner = ownedObj.Owner;
                }
                else
                {
                    media.Owner = null;
                }

                IMasterDataObject masterObj = entity as IMasterDataObject;
                if (masterObj != null)
                {
                    media.IsMasterData = masterObj.IsMasterData;
                }
                else
                {
                    media.IsMasterData = false;
                }

                entity.Media.Add(media);
            }
        }

        /// <summary>
        /// Add a media to the given entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="newMedia">The new media to add.</param>
        public void AddEntityMedia(IEntityWithMedia entity, Media newMedia)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "The entity was null.");
            }

            if (newMedia == null)
            {
                throw new ArgumentNullException("newMedia", "the new media was null.");
            }

            IOwnedObject ownedObj = entity as IOwnedObject;
            if (ownedObj != null)
            {
                newMedia.Owner = ownedObj.Owner;
            }
            else
            {
                newMedia.Owner = null;
            }

            IMasterDataObject masterObj = entity as IMasterDataObject;
            if (masterObj != null)
            {
                newMedia.IsMasterData = masterObj.IsMasterData;
            }
            else
            {
                newMedia.IsMasterData = false;
            }

            entity.Media = newMedia;
        }

        #endregion Add Media

        #region Delete Media

        /// <summary>
        /// Deletes the media object
        /// </summary>
        /// <param name="metaData">The information regarding the media object to delete.</param>
        public void DeleteMedia(MediaMetaData metaData)
        {
            if (metaData == null)
            {
                throw new ArgumentNullException("metaData", "The 'metaData'param was null.");
            }

            try
            {
                this.dataSourceManager.MediaRepository.DeleteMedia(metaData);
            }
            catch (DataAccessException exception)
            {
                throw new BusinessException(exception);
            }
        }

        /// <summary>
        /// Deletes the specified media.
        /// </summary>
        /// <param name="media">The media.</param>
        public void DeleteMedia(Media media)
        {
            if (media == null)
            {
                throw new ArgumentNullException("media", "The media was null.");
            }

            try
            {
                this.dataSourceManager.MediaRepository.RemoveAll(media);
            }
            catch (DataAccessException exception)
            {
                throw new BusinessException(exception);
            }
        }

        /// <summary>
        /// Deletes an array of media objects
        /// </summary>
        /// <param name="metaData">List of meta data for the media to be deleted.</param>
        public void DeleteBatchMedia(IEnumerable<MediaMetaData> metaData)
        {
            if (metaData == null)
            {
                throw new ArgumentNullException("metaData", "The list of meta data was null.");
            }

            try
            {
                foreach (MediaMetaData meta in metaData)
                {
                    this.dataSourceManager.MediaRepository.DeleteMedia(meta);
                }
            }
            catch (DataAccessException exception)
            {
                throw new BusinessException(exception);
            }
        }

        /// <summary>
        /// Deletes all images and video of a specified entity
        /// </summary>
        /// <param name="entity">The entity object</param>
        public void DeleteAllImagesAndVideo(object entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "The entity was null.");
            }

            try
            {
                this.dataSourceManager.MediaRepository.DeleteAllMedia(entity, new List<MediaType> { MediaType.Image, MediaType.Video });
            }
            catch (DataAccessException ex)
            {
                throw new BusinessException(ex);
            }
        }

        /// <summary>
        /// Deletes all media of a specified entity (images, video and documents).
        /// </summary>
        /// <param name="entity">The entity whose media to delete.</param>
        public void DeleteAllMedia(object entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "The entity was null.");
            }

            try
            {
                this.dataSourceManager.MediaRepository.DeleteAllMedia(
                    entity,
                    new List<MediaType> { MediaType.Image, MediaType.Video, MediaType.Document });
            }
            catch (DataAccessException ex)
            {
                throw new BusinessException(ex);
            }
        }

        #endregion Delete Media

        #region Get Documents

        /// <summary>
        /// Get the meta data info of the documents belonging to a specified entity.
        /// The entity's context is auto-detected, which always fails for detached entities.
        /// </summary>
        /// <param name="entity">The entity object containing documents.</param>
        /// <returns>A list of media meta data info about the documents.</returns>
        public Collection<MediaMetaData> GetDocumentsMetaData(object entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "The entity was null.");
            }

            try
            {
                return this.dataSourceManager.MediaRepository.GetMediaMetaData(entity, new List<MediaType>() { MediaType.Document });
            }
            catch (DataAccessException exception)
            {
                throw new BusinessException(exception);
            }
        }

        /// <summary>
        /// Writes the media content in a specified file.
        /// </summary>
        /// <param name="mediaMetaData">Media meta data information</param>
        /// <param name="path">Path on the disk where to save the file</param>
        public void WriteMediaContentToFile(MediaMetaData mediaMetaData, string path)
        {
            try
            {
                var media = this.dataSourceManager.MediaRepository.GetMedia(mediaMetaData, true);
                this.WriteMediaContentToFile(media, path);
            }
            catch (DataAccessException ex)
            {
                throw new BusinessException(ex);
            }
        }

        /// <summary>
        /// Writes the media content to file.
        /// </summary>
        /// <param name="media">The media.</param>
        /// <param name="path">The path.</param>
        public void WriteMediaContentToFile(Media media, string path)
        {
            if (media != null && media.Content != null)
            {
                try
                {
                    using (MemoryStream contentMemoryStream = new MemoryStream())
                    {
                        contentMemoryStream.Write(media.Content, 0, media.Content.Length);
                        contentMemoryStream.Position = 0;

                        if (ZipFile.IsZipFile(contentMemoryStream, false))
                        {
                            contentMemoryStream.Position = 0;
                            using (MemoryStream extractedContentMemoryStream = new MemoryStream())
                            {
                                using (ZipFile zip = ZipFile.Read(contentMemoryStream))
                                {
                                    if (zip.Entries.Count == 1)
                                    {
                                        ZipEntry entry = zip.FirstOrDefault();
                                        entry.Extract(extractedContentMemoryStream);
                                        media.Content = extractedContentMemoryStream.ToArray();
                                    }
                                }
                            }
                        }
                    }

                    using (FileStream destFile = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.None, 8192))
                    {
                        destFile.Write(media.Content, 0, media.Content.Length);
                    }
                }
                catch (Exception ex)
                {
                    log.ErrorException("Failed to write file.", ex);
                    throw new BusinessException(ErrorCodes.FileWriteFail);
                }
            }
        }

        #endregion Get Documents
    }
}
