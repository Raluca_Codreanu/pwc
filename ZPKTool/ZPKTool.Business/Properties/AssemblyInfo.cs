﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ZPKTool.Business")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("ZPKTool.Business")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("4fd3702a-3201-464d-8423-f6c6b1ee9289")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]

[assembly: CLSCompliant(true)]

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("ZPKTool.Business.Tests,PublicKey=002400000480000094000000060200000024000052534131000400000100010061a3168959fb049ab95afc4c707c40ca16b55c141cacfe5f7f7455d877e2cd74d3f5a0ccac5eb8c9f79fdecde9470960f783b6f168704f66f770f89e0d60f30817d4232c95861c61a0fcb4c5c25884ebe30b30990d2b21ce6c6d1ace19fcfe42900998551c430b6b5fb43cb055dbaaffdd0a9db09072a441f0d541594b85a88a")]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("ZPKTool.Calculations.Tests,PublicKey=002400000480000094000000060200000024000052534131000400000100010061a3168959fb049ab95afc4c707c40ca16b55c141cacfe5f7f7455d877e2cd74d3f5a0ccac5eb8c9f79fdecde9470960f783b6f168704f66f770f89e0d60f30817d4232c95861c61a0fcb4c5c25884ebe30b30990d2b21ce6c6d1ace19fcfe42900998551c430b6b5fb43cb055dbaaffdd0a9db09072a441f0d541594b85a88a")]