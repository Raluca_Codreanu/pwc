﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Data;

namespace ZPKTool.Business
{
    /// <summary>
    /// Contains state information and event data associated with an CurrentUserUICurrencyChanged event.
    /// </summary>
    public class UICurrencyChangedArgs : EventArgs
    {
        /// <summary>
        /// The UI currency that have been replaced.
        /// </summary>
        private Currency oldCurrency;

        /// <summary>
        /// The UI currency that has been selected.
        /// </summary>
        private Currency newCurrency;

        /// <summary>
        /// Initializes a new instance of the <see cref="UICurrencyChangedArgs"/> class.
        /// </summary>
        /// <param name="oldCurrency">The old currency.</param>
        /// /// <param name="newCurrency">The new currency.</param>
        public UICurrencyChangedArgs(Currency oldCurrency, Currency newCurrency)
        {
            this.oldCurrency = oldCurrency;
            this.newCurrency = newCurrency;
        }

        /// <summary>
        /// Gets the UI currency that have been replaced (the old UI currency).
        /// </summary>        
        public Currency OldCurrency
        {
            get { return this.oldCurrency; }
        }

        /// <summary>
        /// Gets the UI currency that has been selected (the new UI currency).
        /// </summary>        
        public Currency NewCurrency
        {
            get { return this.newCurrency; }
        }
    }
}