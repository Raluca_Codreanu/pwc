﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.TransportCostCalculator
{
    /// <summary>
    /// The carrier used for each transport cost calculator's cargo.
    /// </summary>
    public class Carrier
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Carrier" /> class.
        /// </summary>
        public Carrier()
        {
        }

        /// <summary>
        /// Gets or sets the carrier's unique identifier.
        /// </summary>
        public short Id { get; set; }

        /// <summary>
        /// Gets or sets the carrier's description (name).
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the carrier's length.
        /// </summary>
        public decimal Length { get; set; }

        /// <summary>
        /// Gets or sets the carrier's width.
        /// </summary>
        public decimal Width { get; set; }

        /// <summary>
        /// Gets or sets the carrier's height.
        /// </summary>
        public decimal Height { get; set; }

        /// <summary>
        /// Gets or sets the carrier's net weight.
        /// </summary>
        public decimal NetWeight { get; set; }

        /// <summary>
        /// Gets or sets the carrier's load capacity.
        /// </summary>
        public decimal LoadCapacity { get; set; }
    }
}
