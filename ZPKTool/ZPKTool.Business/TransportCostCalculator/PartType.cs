﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.TransportCostCalculator
{
    /// <summary>
    /// The part types for the purpose of packaging.
    /// </summary>
    public enum PartType
    {
        /// <summary>
        /// The single component part type.
        /// </summary>
        SingleComponent = 0,

        /// <summary>
        /// The bulk goods part type.
        /// </summary>
        BulkGoods = 1
    }
}
