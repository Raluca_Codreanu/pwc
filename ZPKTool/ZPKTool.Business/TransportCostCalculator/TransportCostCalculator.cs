﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Business.Resources;
using ZPKTool.Common;

namespace ZPKTool.Business.TransportCostCalculator
{
    /// <summary>
    /// Contains the logic for transport cost calculations.
    /// </summary>
    public class TransportCostCalculator
    {
        /// <summary>
        /// Gets the carriers list.
        /// </summary>
        /// <returns>The list of carriers.</returns>
        public static ICollection<Carrier> GetCarriers()
        {
            List<Carrier> carriers = new List<Carrier>();

            carriers.Add(new Carrier { Id = 0, Description = LocalizedResources.CarrierType_Manual });
            carriers.Add(new Carrier { Id = 1, Description = LocalizedResources.CarrierType_Normal, Length = 800, Width = 1200, Height = 800, NetWeight = 85, LoadCapacity = 1000 });
            carriers.Add(new Carrier { Id = 2, Description = LocalizedResources.CarrierType_HalfHeight, Length = 800, Width = 1200, Height = 500, NetWeight = 55, LoadCapacity = 700 });
            carriers.Add(new Carrier { Id = 3, Description = LocalizedResources.CarrierType_Pallet, Length = 800, Width = 1200, Height = 144, NetWeight = 25, LoadCapacity = 1000 });

            return carriers;
        }

        /// <summary>
        /// Gets the transporters list.
        /// </summary>
        /// <returns>The list of transporters.</returns>
        public static ICollection<Transporter> GetTransporters()
        {
            List<Transporter> transporters = new List<Transporter>();

            transporters.Add(new Transporter { Id = 0, Description = LocalizedResources.TransporterType_Manual });
            transporters.Add(new Transporter { Id = 1, Description = LocalizedResources.TransporterType_TruckTrailer, Length = 13600m, Width = 2480m, Height = 2500m, ParkingSlots = 34, MaxLoadCapacity = 25m });
            transporters.Add(new Transporter { Id = 2, Description = LocalizedResources.TransporterType_MegaTrailer, Length = 13600m, Width = 2450m, Height = 3000m, ParkingSlots = 34, MaxLoadCapacity = 26m });
            transporters.Add(new Transporter { Id = 3, Description = LocalizedResources.TransporterType_Truck7t, Length = 7200m, Width = 2480m, Height = 2500m, ParkingSlots = 17, MaxLoadCapacity = 7m });
            transporters.Add(new Transporter { Id = 4, Description = LocalizedResources.TransporterType_Truck3_5t, Length = 6200m, Width = 2480m, Height = 2500m, ParkingSlots = 15, MaxLoadCapacity = 3.5m });
            transporters.Add(new Transporter { Id = 5, Description = LocalizedResources.TransporterType_VanType, Length = 4000m, Width = 2000m, Height = 2200m, MaxLoadCapacity = 1.5m });

            return transporters;
        }

        /// <summary>
        /// Calculates the values related to transport cost.
        /// </summary>
        /// <param name="inputData">The input data.</param>
        /// <returns>An object containing the results.</returns>
        public TransportCostCalculationResult CalculateTransportCost(TransportCostCalculationInputData inputData)
        {
            TransportCostCalculationResult result = new TransportCostCalculationResult();

            // Calculate annual quantity.
            result.AnnualQty = decimal.Multiply(inputData.DaysPerYear, inputData.DailyNeededQty);

            // Calculate volume from bouncing box.
            result.BouncingBoxVolume = (inputData.BouncingBoxLength * inputData.BouncingBoxWidth * inputData.BouncingBoxHeight) / 1000000000m;

            // Calculate volume from carrier / packaging size.
            result.CarrierVolume = (inputData.CarrierLength * inputData.CarrierWidth * inputData.CarrierHeight) / 1000000000m;

            // Calculate parts pro carrier / package carrier.
            if (result.BouncingBoxVolume != 0m)
            {
                if (inputData.PartType == PartType.SingleComponent)
                {
                    result.PartsPerCarrier = Math.Ceiling(result.CarrierVolume / result.BouncingBoxVolume * 0.8m * (1m + inputData.Interference));
                }
                else if (inputData.PartType == PartType.BulkGoods)
                {
                    result.PartsPerCarrier = Math.Ceiling(result.CarrierVolume / result.BouncingBoxVolume * inputData.PackingDensity);
                }
            }

            // Calculate package carrier / packaging per release.
            if (result.PartsPerCarrier != 0)
            {
                result.PackageCarrierPerPackaging = inputData.DailyNeededQty / result.PartsPerCarrier;
            }

            // Calculate the annual number of package carrier.
            result.AnnualNumberOfPackageCarrier = Math.Ceiling(result.PackageCarrierPerPackaging * inputData.DaysPerYear);

            // Calculate the transporter volume.
            result.TransporterVolume = Math.Floor((inputData.TransporterLength * inputData.TransporterWidth * inputData.TransporterHeight) / 1000000000m);

            // Calculate max theo.
            if (result.CarrierVolume != 0m)
            {
                result.Maxtheo = Math.Floor(result.TransporterVolume / result.CarrierVolume);
            }

            // Calculate needed transports for release.
            if (inputData.MaxPackageCarrier != 0m)
            {
                result.NeededTransportsForRelease = result.PackageCarrierPerPackaging / inputData.MaxPackageCarrier;
            }

            // Calculate annual number of transports.
            result.AnnualNumberOfTransports = Math.Ceiling(inputData.DaysPerYear * result.NeededTransportsForRelease);

            // Calculate carrier weight.
            result.CarrierWeight = inputData.MaxPackageCarrier * inputData.NetWeight / 1000m;

            // Calculate parts weight.  
            result.PartsWeight = (inputData.PartWeight + inputData.PackageWeight) * result.PartsPerCarrier * inputData.MaxPackageCarrier / 1000m;

            // Calculate total weight.
            result.TotalWeight = result.CarrierWeight + result.PartsWeight;

            // Calculate annual packaging cost total.
            result.PackagingCost = result.AnnualQty * inputData.CostByPackage;

            // Calculate annual packaging cost total per pcs.
            if (result.AnnualQty != 0m)
            {
                result.PackagingCostPerPcs = result.PackagingCost / result.AnnualQty;
            }

            // Calculate filling degree transporter per transport.
            if (result.AnnualNumberOfTransports != 0m && inputData.MaxPackageCarrier != 0m)
            {
                result.FillingDegreeTransporter = (result.AnnualNumberOfPackageCarrier / result.AnnualNumberOfTransports) / inputData.MaxPackageCarrier;
            }

            // Calculate annual transport cost total.
            result.TransportCost = result.AnnualNumberOfTransports * inputData.CostPerKm * inputData.Distance * 2m * result.FillingDegreeTransporter;

            // Calculate annual transport cost total per pcs.
            if (result.AnnualQty != 0m)
            {
                result.TransportCostPerPcs = result.TransportCost / result.AnnualQty;
            }

            return result;
        }
    }
}
