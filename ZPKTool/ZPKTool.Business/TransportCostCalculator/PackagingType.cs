﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.TransportCostCalculator
{
    /// <summary>
    /// The types of packaging.
    /// </summary>
    public enum PackagingType
    {
        /// <summary>
        /// The individually packing type.
        /// </summary>
        IndividualPacking = 0,

        /// <summary>
        /// The multipack type.
        /// </summary>
        Multipack = 1
    }
}
