﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.TransportCostCalculator
{
    /// <summary>
    /// Holds the result of the calculations for transport cost.
    /// </summary>
    public class TransportCostCalculationResult
    {
        /// <summary>
        /// Gets or sets the annual transported quantity.
        /// </summary>
        public decimal AnnualQty { get; set; }

        /// <summary>
        /// Gets or sets the volume of the carrier.
        /// </summary>
        public decimal CarrierVolume { get; set; }

        /// <summary>
        /// Gets or sets the volume of the carrier. 
        /// </summary>
        public decimal BouncingBoxVolume { get; set; }

        /// <summary>
        /// Gets or sets the parts per carrier per package carrier.
        /// </summary>
        public decimal PartsPerCarrier { get; set; }

        /// <summary>
        /// Gets or sets the package carrier per packaging for release.
        /// </summary>
        public decimal PackageCarrierPerPackaging { get; set; }

        /// <summary>
        /// Gets or sets the annual number of package carrier.
        /// </summary>
        public decimal AnnualNumberOfPackageCarrier { get; set; }

        /// <summary>
        /// Gets or sets the maximum theoretical value for maximum package carrier per packaging field.
        /// </summary>
        public decimal Maxtheo { get; set; }

        /// <summary>
        /// Gets or sets the needed transports for release.
        /// </summary>
        public decimal NeededTransportsForRelease { get; set; }

        /// <summary>
        /// Gets or sets the annual number of transports.
        /// </summary>
        public decimal AnnualNumberOfTransports { get; set; }

        /// <summary>
        /// Gets or sets the percentage of filling degree transporter per transport.
        /// </summary>
        public decimal FillingDegreeTransporter { get; set; }

        /// <summary>
        /// Gets or sets the weight of the carrier.
        /// </summary>
        public decimal CarrierWeight { get; set; }

        /// <summary>
        /// Gets or sets the weight of parts, including packaging.
        /// </summary>
        public decimal PartsWeight { get; set; }

        /// <summary>
        /// Gets or sets the total weight.
        /// </summary>
        public decimal TotalWeight { get; set; }

        /// <summary>
        /// Gets or sets the total annual packaging cost.
        /// </summary>
        public decimal PackagingCost { get; set; }

        /// <summary>
        /// Gets or sets the total annual packaging cost [per piece].
        /// </summary>
        public decimal PackagingCostPerPcs { get; set; }

        /// <summary>
        /// Gets or sets the total annual transport cost.
        /// </summary>
        public decimal TransportCost { get; set; }

        /// <summary>
        /// Gets or sets the total annual transport cost [per piece].
        /// </summary>
        public decimal TransportCostPerPcs { get; set; }

        /// <summary>
        /// Gets or sets the volume of the transporter (cargo).
        /// </summary>
        public decimal TransporterVolume { get; set; }
    }
}
