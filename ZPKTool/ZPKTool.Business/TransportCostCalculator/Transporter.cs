﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.TransportCostCalculator
{
    /// <summary>
    /// The transporter used for each transport cost calculator's route.
    /// </summary>
    public class Transporter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Transporter" /> class.
        /// </summary>
        public Transporter()
        {
        }

        /// <summary>
        /// Gets or sets the transporter's unique identifier.
        /// </summary>
        public short Id { get; set; }

        /// <summary>
        /// Gets or sets the transporter's description (name).
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the transporter's length.
        /// </summary>
        public decimal Length { get; set; }

        /// <summary>
        /// Gets or sets the transporter's width.
        /// </summary>
        public decimal Width { get; set; }

        /// <summary>
        /// Gets or sets the transporter's height.
        /// </summary>
        public decimal Height { get; set; }

        /// <summary>
        /// Gets or sets the needed parking slots for the transporter.
        /// </summary>
        public int ParkingSlots { get; set; }

        /// <summary>
        /// Gets or sets the transporter's maximum load capacity.
        /// </summary>
        public decimal MaxLoadCapacity { get; set; }
    }
}
