﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.TransportCostCalculator
{
    /// <summary>
    /// The input data for calculating the transport cost of an Assembly / Part.
    /// </summary>
    public class TransportCostCalculationInputData
    {
        /// <summary>
        /// Gets or sets the part's weight.
        /// </summary>
        public decimal PartWeight { get; set; }

        /// <summary>
        /// Gets or sets the distance from source to destination.
        /// </summary>
        public decimal Distance { get; set; }

        /// <summary>
        /// Gets or sets the days worked in an year.
        /// </summary>
        public int DaysPerYear { get; set; }

        /// <summary>
        /// Gets or sets the daily needed quantity.
        /// </summary>
        public int DailyNeededQty { get; set; }

        /// <summary>
        /// Gets or sets the type of part.
        /// </summary>
        public PartType PartType { get; set; }

        /// <summary>
        /// Gets or sets the type of parts packaging.
        /// </summary>
        public PackagingType PackagingType { get; set; }

        /// <summary>
        /// Gets or sets the cost of a package.
        /// </summary>
        public decimal CostByPackage { get; set; }

        /// <summary>
        /// Gets or sets the weight of a package.
        /// </summary>
        public decimal PackageWeight { get; set; }

        /// <summary>
        /// Gets or sets the length of a bouncing box. 
        /// </summary>
        public decimal BouncingBoxLength { get; set; }

        /// <summary>
        /// Gets or sets the interference percentage if the part is a single component.
        /// </summary>
        public decimal Interference { get; set; }

        /// <summary>
        /// Gets or sets the width of the bouncing box.
        /// </summary>
        public decimal BouncingBoxWidth { get; set; }

        /// <summary>
        /// Gets or sets the height of the bouncing box.
        /// </summary>
        public decimal BouncingBoxHeight { get; set; }

        /// <summary>
        /// Gets or sets the density percentage of packing if there are bulk goods.
        /// </summary>
        public decimal PackingDensity { get; set; }

        /// <summary>
        /// Gets or sets the length of the carrier.
        /// </summary>
        public decimal CarrierLength { get; set; }

        /// <summary>
        /// Gets or sets the height of the carrier.
        /// </summary>
        public decimal CarrierHeight { get; set; }

        /// <summary>
        /// Gets or sets the width of the carrier.
        /// </summary>
        public decimal CarrierWidth { get; set; }
        
        /// <summary>
        /// Gets or sets the net weight of the carrier.
        /// </summary>
        public decimal NetWeight { get; set; }
        
        /// <summary>
        /// Gets or sets the maximum package carrier per packaging.
        /// </summary>
        public int MaxPackageCarrier { get; set; }

        /// <summary>
        /// Gets or sets the length of the transporter.
        /// </summary>
        public decimal TransporterLength { get; set; }

        /// <summary>
        /// Gets or sets the width of the transporter.
        /// </summary>
        public decimal TransporterWidth { get; set; }

        /// <summary>
        /// Gets or sets the height of the transporter.
        /// </summary>
        public decimal TransporterHeight { get; set; }

        /// <summary>
        /// Gets or sets the cost per kilometer charged for the transport.
        /// </summary>
        public decimal CostPerKm { get; set; }
    }
}
