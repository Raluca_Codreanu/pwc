﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZPKTool.Business;
using ZPKTool.Common;

namespace ZPKTool.Business
{
    /// <summary>
    /// WeakEventManager implementation for the CurrentUserUICurrencyChanged event of the SecurityManager.
    /// </summary>
    public class UICurrencyChangedEventManager : WeakEventManagerBase<UICurrencyChangedEventManager, SecurityManager>
    {
        /// <summary>
        /// Attaches the event handler.
        /// </summary>
        /// <param name="source">The source to which to attach.</param>
        protected override void StartListening(SecurityManager source)
        {
            source.CurrentUserUICurrencyChanged += DeliverEvent;
        }

        /// <summary>
        /// Detaches the event handler.
        /// </summary>
        /// <param name="source">The source from which to detach.</param>
        protected override void StopListening(SecurityManager source)
        {
            source.CurrentUserUICurrencyChanged -= DeliverEvent;
        }
    }
}