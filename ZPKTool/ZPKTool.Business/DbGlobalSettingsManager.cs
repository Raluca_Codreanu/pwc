﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using ZPKTool.Data;
using ZPKTool.DataAccess;

namespace ZPKTool.Business
{
    /// <summary>
    /// Manages the global settings stored in the database.
    /// </summary>
    public sealed class DbGlobalSettingsManager
    {
        #region Attributes

        /// <summary>
        /// Contains the global settings cache (one entry for each database)
        /// </summary>
        private static ConcurrentDictionary<DbIdentifier, CachedSettingsData> cachedSettings = new ConcurrentDictionary<DbIdentifier, CachedSettingsData>();

        /// <summary>
        /// The database from which this instance manages the global settings.
        /// </summary>
        private DbIdentifier databaseId;

        #endregion Attributes

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DbGlobalSettingsManager"/> class.
        /// </summary>
        /// <param name="databaseId">The database from which to manage the settings.</param>
        public DbGlobalSettingsManager(DbIdentifier databaseId)
        {
            this.databaseId = databaseId;
        }

        #endregion Constructor

        /// <summary>
        /// Clears the cached settings. They will be reloaded when accessed next time.
        /// </summary>
        public static void ClearCache()
        {
            cachedSettings.Clear();
        }

        /// <summary>
        /// Gets the global settings.
        /// </summary>
        /// <returns>
        /// An data structure containing the global settings.
        /// </returns>
        public DbGlobalSettings Get()
        {
            var cachedData = LoadAndCache(databaseId);
            return CreateFromCacheItem(cachedData);
        }

        /// <summary>
        /// Saves the global settings values from the specified instance back into the database.
        /// </summary>
        /// <param name="settings">The global setting values to save.</param>
        /// <exception cref="System.ArgumentNullException">The settings were null.</exception>
        public void Save(DbGlobalSettings settings)
        {
            if (settings == null)
            {
                throw new ArgumentNullException("settings", "The settings were null.");
            }

            var cachedSettingsItem = LoadAndCache(databaseId);
            var settingsType = settings.GetType();

            foreach (var setting in cachedSettingsItem.GlobalSettings)
            {
                var propertyInfo = settingsType.GetProperty(setting.Key);
                if (propertyInfo != null)
                {
                    var newValue = propertyInfo.GetValue(settings, null);
                    var newValueStr = Convert.ToString(newValue, CultureInfo.InvariantCulture);
                    if (setting.Value != newValueStr)
                    {
                        setting.Value = newValueStr;
                    }
                }
            }

            cachedSettingsItem.DataManager.SaveChanges();
        }

        /// <summary>
        /// Loads the global settings and returns a cache data item for them.
        /// </summary>
        /// <param name="databaseId">The database identifier.</param>
        /// <returns>A cache item</returns>
        private CachedSettingsData LoadAndCache(DbIdentifier databaseId)
        {
            // TODO: implement an expiration policy for the cache (using a .net class like MemoryCache offers this by default).
            CachedSettingsData cachedData;
            if (!cachedSettings.TryGetValue(databaseId, out cachedData))
            {
                var dataManager = DataAccessFactory.CreateDataSourceManager(databaseId);
                var globalSettings = dataManager.GlobalSettingsRepository.GetAllGlobalSettings();

                cachedData = new CachedSettingsData()
                {
                    DatabaseId = databaseId,
                    DataManager = dataManager,
                    GlobalSettings = globalSettings
                };

                cachedSettings.TryAdd(databaseId, cachedData);
            }

            return cachedData;
        }

        /// <summary>
        /// Creates a <see cref="DbGlobalSettings"/> instance from the specified cached data item.
        /// </summary>
        /// <param name="cachedData">The cached data.</param>
        /// <returns>A <see cref="DbGlobalSettings"/> object containing the settings from the specified cache data item.</returns>
        private DbGlobalSettings CreateFromCacheItem(CachedSettingsData cachedData)
        {
            var settings = new DbGlobalSettings();
            var settingsType = settings.GetType();

            foreach (var setting in cachedData.GlobalSettings)
            {
                var propertyInfo = settingsType.GetProperty(setting.Key);
                if (propertyInfo != null)
                {
                    var convertedValue = Convert.ChangeType(setting.Value, propertyInfo.PropertyType, CultureInfo.InvariantCulture);
                    propertyInfo.SetValue(settings, convertedValue, null);
                }
            }

            return settings;
        }

        /// <summary>
        /// Contains the cached information for a database's global settings.
        /// </summary>
        private class CachedSettingsData
        {
            /// <summary>
            /// Gets or sets a value that identifies the database for which this cached info exists.
            /// </summary>
            public DbIdentifier DatabaseId { get; set; }

            /// <summary>
            /// Gets or sets the cached global settings.
            /// </summary>
            public IEnumerable<GlobalSetting> GlobalSettings { get; set; }

            /// <summary>
            /// Gets or sets the data manager used to obtain the cached global settings.
            /// </summary>
            public IDataSourceManager DataManager { get; set; }
        }
    }
}
