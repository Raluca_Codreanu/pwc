﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;
using ZPKTool.Common;
using ZPKTool.DataAccess;

namespace ZPKTool.Business
{
    /// <summary>
    /// This class contains business utility methods.
    /// </summary>
    public static class BusinessUtils
    {
        /// <summary>
        /// Parses a <see cref="DataException"/> to extract meaningful information and wraps the information and the exception in a <see cref="BusinessException"/>.
        /// </summary>
        /// <param name="ex">The exception.</param>
        /// <param name="databaseId">The database that was accessed when the data exception occurred.</param>
        /// <returns>
        /// True if the entity exists, false otherwise.
        /// </returns>        
        public static BusinessException ParseDataException(DataException ex, DbIdentifier databaseId)
        {
            return new BusinessException(DataAccess.Utils.ParseDataException(ex, databaseId));
        }
    }
}
