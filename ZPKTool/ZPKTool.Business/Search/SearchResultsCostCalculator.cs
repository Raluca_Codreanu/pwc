﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using ZPKTool.Calculations.CostCalculation;
using ZPKTool.Data;
using ZPKTool.DataAccess;

namespace ZPKTool.Business.Search
{
    /// <summary>
    /// Calculates the cost of entities from the search results.
    /// </summary>
    public static class SearchResultsCostCalculator
    {
        /// <summary>
        /// The amount of time , in minutes, for which a cost is cached.
        /// <remarks>
        /// The default is 5 minutes.
        /// </remarks>
        /// </summary>
        private const int CostCacheTime = 5;

        /// <summary>
        /// The cached costs of the searched entities.
        /// </summary>
        private static List<CachedEntityCost> cachedCosts = new List<CachedEntityCost>();

        /// <summary>
        /// Calculates the total cost for a given entity.
        /// </summary>
        /// <param name="assembly">The entity for which to calculate the cost.</param>
        /// <param name="databaseId">The database from which to obtain the entities implied in cost calculation.</param>
        /// <returns>The calculated cost which represents the total cost of entity. </returns>
        public static decimal? CalculateCostOfSearchResultEntity(Assembly assembly, DbIdentifier databaseId)
        {
            if (assembly.IsMasterData)
            {
                return null;
            }

            using (var dataManager = DataAccessFactory.CreateDataSourceManager(databaseId))
            {
                decimal? cost = null;
                var parentProject = dataManager.ProjectRepository.GetParentProject(assembly, true);
                if (parentProject != null)
                {
                    var costIsCached = GetCachedCost(assembly.Guid, out cost);
                    if (!costIsCached)
                    {
                        // Fully load the assembly's top parent. It is necessary for the cost calculation.
                        var topParentAssemblyId = dataManager.AssemblyRepository.GetTopParentAssemblyId(assembly.Guid);
                        var fullAssembly = dataManager.AssemblyRepository.GetAssemblyFull(topParentAssemblyId);

                        var assyCalculationParams = new AssemblyCostCalculationParameters()
                        {
                            ProjectDepreciationPeriod = parentProject.DepreciationPeriod.GetValueOrDefault(),
                            ProjectDepreciationRate = parentProject.DepreciationRate.GetValueOrDefault(),
                            ProjectLogisticCostRatio = parentProject.LogisticCostRatio,
                            ProjectCreateDate = CostCalculationHelper.GetProjectCreateDate(parentProject)
                        };

                        var calculator = CostCalculatorFactory.GetCalculator(fullAssembly.CalculationVariant);
                        var result = calculator.CalculateAssemblyCost(fullAssembly, assyCalculationParams);
                        cost = result.Summary.TargetCost;

                        // Cache the cost.
                        CacheCost(assembly.Guid, cost);
                    }
                }

                return cost;
            }
        }

        /// <summary>
        /// Calculates the total cost for a given entity.
        /// </summary>
        /// <param name="part">The entity for which to calculate the cost.</param>
        /// <param name="databaseId">The database from which to obtain the entities implied in cost calculation.</param>
        /// <returns>The calculated cost which represents the total cost of entity. </returns>
        public static decimal? CalculateCostOfSearchResultEntity(Part part, DbIdentifier databaseId)
        {
            if (part.IsMasterData)
            {
                return null;
            }

            using (var dataManager = DataAccessFactory.CreateDataSourceManager(databaseId))
            {
                decimal? cost = null;
                var parentProject = dataManager.ProjectRepository.GetParentProject(part, true);
                if (parentProject != null)
                {
                    var costIsCached = GetCachedCost(part.Guid, out cost);
                    if (!costIsCached)
                    {
                        // Fully load the part's top parent. It is necessary for the cost calculation.
                        var topPrentAssemblyId = dataManager.AssemblyRepository.GetTopParentAssemblyId(part.Guid);
                        if (topPrentAssemblyId != Guid.Empty)
                        {
                            dataManager.AssemblyRepository.GetAssemblyFull(topPrentAssemblyId);
                        }
                        else
                        {
                            // If the part is a raw part, fully load its parent part else fully load the part it self.
                            var parentPartId = part.Guid;
                            var rawPart = part as RawPart;
                            if (rawPart != null)
                            {
                                parentPartId = dataManager.PartRepository.GetParentOfRawPartId(rawPart.Guid);
                            }

                            dataManager.PartRepository.GetPartFull(part.Guid);
                        }

                        part = dataManager.PartRepository.GetByKey(part.Guid);

                        var partCalcParams = new PartCostCalculationParameters()
                        {
                            ProjectDepreciationPeriod = parentProject.DepreciationPeriod.GetValueOrDefault(),
                            ProjectDepreciationRate = parentProject.DepreciationRate.GetValueOrDefault(),
                            ProjectLogisticCostRatio = parentProject.LogisticCostRatio,
                            ProjectCreateDate = CostCalculationHelper.GetProjectCreateDate(parentProject)
                        };

                        var calculator = CostCalculatorFactory.GetCalculator(part.CalculationVariant);
                        var result = calculator.CalculatePartCost(part, partCalcParams);
                        cost = result.Summary.TargetCost;

                        // Cache the cost.
                        CacheCost(part.Guid, cost);
                    }
                }

                return cost;
            }
        }

        /// <summary>
        /// Calculates the total cost for a given entity.
        /// </summary>
        /// <param name="material">The entity for which to calculate the cost.</param>
        /// <param name="databaseId">The database from which to obtain the entities implied in cost calculation.</param>
        /// <returns>The calculated cost which represents the total cost of entity. </returns>
        public static decimal? CalculateCostOfSearchResultEntity(RawMaterial material, DbIdentifier databaseId)
        {
            if (material.IsMasterData)
            {
                return null;
            }

            decimal? cost = null;
            var costIsCached = GetCachedCost(material.Guid, out cost);
            if (!costIsCached)
            {
                var dataManager = DataAccessFactory.CreateDataSourceManager(databaseId);
                material = dataManager.RawMaterialRepository.GetById(material.Guid, true);

                if (material != null
                    && material.Part != null)
                {
                    var calculator = CostCalculatorFactory.GetCalculator(material.Part.CalculationVariant);
                    var result = calculator.CalculateRawMaterialCost(material, null);
                    cost = result.NetCost;

                    // Cache the cost.
                    CacheCost(material.Guid, cost);
                }
            }

            return cost;
        }

        /// <summary>
        /// Calculates the total cost for a given entity.
        /// </summary>
        /// <param name="commodity">The entity for which to calculate the cost.</param>
        /// <param name="databaseId">The database from which to obtain the entities implied in cost calculation.</param>
        /// <returns>The calculated cost which represents the total cost of entity. </returns>
        public static decimal? CalculateCostOfSearchResultEntity(Commodity commodity, DbIdentifier databaseId)
        {
            if (commodity.IsMasterData)
            {
                return null;
            }

            decimal? cost = null;
            var costIsCached = GetCachedCost(commodity.Guid, out cost);
            if (!costIsCached)
            {
                var dataManager = DataAccessFactory.CreateDataSourceManager(databaseId);
                var calculationVariant = string.Empty;
                Assembly parentAssembly = null;

                // Try to get the part parent of the commodity. If the part is null the the parent must be a step.
                var parentPart = dataManager.PartRepository.GetParentPart(commodity);
                if (parentPart != null)
                {
                    calculationVariant = parentPart.CalculationVariant;
                }
                else
                {
                    // The commodity is probably in a step. Get the parent Assembly/Part of the process in which the commodity exists.
                    var dbContext = dataManager.EntityContext;
                    var query = from comm in dbContext.CommoditySet
                                join processStep in dbContext.ProcessStepSet on comm.ProcessStep equals processStep
                                join process in dbContext.ProcessSet on processStep.Process equals process
                                join assy in dbContext.AssemblySet on process equals assy.Process into parentAssyGroup
                                join part in dbContext.PartSet on process equals part.Process into parentPartGroup
                                where comm.Guid == commodity.Guid
                                select new
                                {
                                    ParentAssembly = parentAssyGroup.FirstOrDefault(),
                                    ParentPart = parentPartGroup.FirstOrDefault(),
                                };

                    try
                    {
                        var queryResult = query.AsNoTracking().FirstOrDefault();

                        if (queryResult != null)
                        {
                            parentAssembly = queryResult.ParentAssembly;
                            parentPart = queryResult.ParentPart;
                        }
                    }
                    catch (System.Data.DataException ex)
                    {
                        throw BusinessUtils.ParseDataException(ex, databaseId);
                    }
                    catch (DataAccessException ex)
                    {
                        throw new BusinessException(ex);
                    }

                    if (parentAssembly != null)
                    {
                        calculationVariant = parentAssembly.CalculationVariant;
                    }
                    else if (parentPart != null)
                    {
                        calculationVariant = parentPart.CalculationVariant;
                    }
                }

                if (parentAssembly != null
                    || parentPart != null)
                {
                    var calculator = CostCalculatorFactory.GetCalculator(calculationVariant);
                    var result = calculator.CalculateCommodityCost(commodity, new CommodityCostCalculationParameters());
                    cost = result.Cost;

                    // Cache the cost.
                    CacheCost(commodity.Guid, cost);
                }
            }

            return cost;
        }

        /// <summary>
        /// Calculates the total cost for a given entity.
        /// </summary>
        /// <param name="consumable">The entity for which to calculate the cost.</param>
        /// <param name="databaseId">The database from which to obtain the entities implied in cost calculation.</param>
        /// <returns>
        /// The calculated cost which represents the total cost of entity.
        /// </returns>
        public static decimal? CalculateCostOfSearchResultEntity(Consumable consumable, DbIdentifier databaseId)
        {
            if (consumable.IsMasterData)
            {
                return null;
            }

            decimal? cost = null;
            var costIsCached = GetCachedCost(consumable.Guid, out cost);
            if (!costIsCached)
            {
                var dataManager = DataAccessFactory.CreateDataSourceManager(databaseId);
                var dbContext = dataManager.EntityContext;
                var query = from cons in dbContext.ConsumableSet
                            join processStep in dbContext.ProcessStepSet on cons.ProcessStep equals processStep
                            join process in dbContext.ProcessSet on processStep.Process equals process
                            join assy in dbContext.AssemblySet on process equals assy.Process into parentAssyGroup
                            join part in dbContext.PartSet on process equals part.Process into parentPartGroup
                            where cons.Guid == consumable.Guid
                            select new
                            {
                                ParentAssembly = parentAssyGroup.FirstOrDefault(),
                                ParentPart = parentPartGroup.FirstOrDefault(),
                            };

                Assembly parentAssembly = null;
                Part parentPart = null;

                try
                {
                    var queryResult = query.AsNoTracking().FirstOrDefault();

                    if (queryResult != null)
                    {
                        parentAssembly = queryResult.ParentAssembly;
                        parentPart = queryResult.ParentPart;
                    }
                }
                catch (DataException ex)
                {
                    throw BusinessUtils.ParseDataException(ex, databaseId);
                }
                catch (DataAccessException ex)
                {
                    throw new BusinessException(ex);
                }

                var calculationVariant = string.Empty;
                if (parentAssembly != null)
                {
                    calculationVariant = parentAssembly.CalculationVariant;
                }
                else if (parentPart != null)
                {
                    calculationVariant = parentPart.CalculationVariant;
                }

                if (parentAssembly != null
                    || parentPart != null)
                {
                    var calculator = CostCalculatorFactory.GetCalculator(calculationVariant);
                    var result = calculator.CalculateConsumableCost(consumable, new ConsumableCostCalculationParameters());
                    cost = result.Cost;

                    // Cache the cost.
                    CacheCost(consumable.Guid, cost);
                }
            }

            return cost;
        }

        /// <summary>
        /// Calculates the total cost for a given entity.
        /// </summary>
        /// <param name="die">The die entity.</param>
        /// <param name="databaseId">The database from which to obtain the entities implied in cost calculation.</param>
        /// <returns>The calculated cost which represents the total cost of entity.</returns>
        public static decimal? CalculateCostOfSearchResultEntity(Die die, DbIdentifier databaseId)
        {
            if (die.IsMasterData)
            {
                return null;
            }

            decimal? cost = null;
            var costIsCached = GetCachedCost(die.Guid, out cost);
            if (!costIsCached)
            {
                // Get the step to which the die belongs and its parent assembly or part.
                var dataManager = DataAccessFactory.CreateDataSourceManager(databaseId);
                var context = dataManager.EntityContext;
                var query = from d in context.DieSet
                            join step in context.ProcessStepSet on d.ProcessStep.Guid equals step.Guid
                            join process in context.ProcessSet on step.Process.Guid equals process.Guid
                            join assembly in context.AssemblySet on process.Guid equals assembly.Process.Guid into parentAssyGroup
                            join part in context.PartSet on process.Guid equals part.Process.Guid into parentPartGroup
                            where d.Guid == die.Guid
                            select new
                            {
                                ParentAssembly = parentAssyGroup.FirstOrDefault(),
                                ParentPart = parentPartGroup.FirstOrDefault()
                            };

                Assembly parentAssembly = null;
                Part parentPart = null;

                try
                {
                    var queryResult = query.AsNoTracking().FirstOrDefault();

                    if (queryResult != null)
                    {
                        parentAssembly = queryResult.ParentAssembly;
                        parentPart = queryResult.ParentPart;
                    }
                }
                catch (DataException ex)
                {
                    throw BusinessUtils.ParseDataException(ex, databaseId);
                }
                catch (DataAccessException ex)
                {
                    throw new BusinessException(ex);
                }

                var calculationVariant = string.Empty;
                ICostCalculator costCalculator = null;
                var parentLifeCycleProductionQty = 0m;

                if (parentAssembly != null)
                {
                    calculationVariant = parentAssembly.CalculationVariant;
                    costCalculator = CostCalculatorFactory.GetCalculator(calculationVariant);
                    parentLifeCycleProductionQty = costCalculator.CalculateNetLifetimeProductionQuantity(parentAssembly);
                }
                else if (parentPart != null)
                {
                    calculationVariant = parentPart.CalculationVariant;
                    costCalculator = CostCalculatorFactory.GetCalculator(calculationVariant);
                    parentLifeCycleProductionQty = costCalculator.CalculateNetLifetimeProductionQuantity(parentPart);
                }

                if (parentAssembly != null
                    || parentPart != null)
                {
                    var dieCalculationParameters = new DieCostCalculationParameters() { ParentLifecycleProductionQuantity = parentLifeCycleProductionQty };
                    var result = costCalculator.CalculateDieCost(die, dieCalculationParameters);
                    cost = result.Cost;

                    // Cache the cost.
                    CacheCost(die.Guid, cost);
                }
            }

            return cost;
        }

        /// <summary>
        /// Calculates the total cost for a given entity.
        /// </summary>
        /// <param name="machine">The entity for which to calculate the cost.</param>
        /// <param name="databaseId">The database from which to obtain the entities implied in cost calculation.</param>
        /// <returns>The calculated cost which represents the total cost of entity. </returns>
        public static MachineCost CalculateCostOfSearchResultEntity(Machine machine, DbIdentifier databaseId)
        {
            if (machine.IsMasterData)
            {
                return null;
            }

            MachineCost cost = null;
            var costIsCached = GetCachedCost(machine.Guid, out cost);
            if (!costIsCached)
            {
                var dataManager = DataAccessFactory.CreateDataSourceManager(databaseId);
                var parentProject = dataManager.ProjectRepository.GetParentProject(machine, true);

                // Get the machine's parent assembly or part to get its calculation variant and country setting.
                var query = from mach in dataManager.EntityContext.MachineSet
                                .Include("ProcessStep.Process.Assemblies.CountrySettings")
                                .Include("ProcessStep.Process.Parts.CountrySettings")
                            where mach.Guid == machine.Guid
                            select mach;

                try
                {
                    machine = query.AsNoTracking().FirstOrDefault();
                }
                catch (DataException ex)
                {
                    throw BusinessUtils.ParseDataException(ex, databaseId);
                }
                catch (DataAccessException ex)
                {
                    throw new BusinessException(ex);
                }

                if (parentProject != null
                    && machine.ProcessStep != null
                    && machine.ProcessStep.Process != null)
                {
                    var calculationVariant = string.Empty;
                    CountrySetting parentCountrySetting = null;

                    var parentAssembly = machine.ProcessStep.Process.Assemblies.FirstOrDefault();
                    if (parentAssembly != null)
                    {
                        calculationVariant = parentAssembly.CalculationVariant;
                        parentCountrySetting = parentAssembly.CountrySettings;
                    }

                    var parentPart = machine.ProcessStep.Process.Parts.FirstOrDefault();
                    if (parentPart != null)
                    {
                        calculationVariant = parentPart.CalculationVariant;
                        parentCountrySetting = parentPart.CountrySettings;
                    }

                    var machineCalculationParameters = new MachineCostCalculationParameters();
                    machineCalculationParameters.CountrySettingsAirCost = parentCountrySetting.AirCost;
                    machineCalculationParameters.CountrySettingsEnergyCost = parentCountrySetting.EnergyCost;
                    machineCalculationParameters.CountrySettingsWaterCost = parentCountrySetting.WaterCost;
                    machineCalculationParameters.CountrySettingsRentalCostProductionArea = parentCountrySetting.ProductionAreaRentalCost;

                    machineCalculationParameters.ProjectDepreciationPeriod = parentProject.DepreciationPeriod.GetValueOrDefault();
                    machineCalculationParameters.ProjectDepreciationRate = parentProject.DepreciationRate.GetValueOrDefault();
                    machineCalculationParameters.ProjectCreateDate = CostCalculationHelper.GetProjectCreateDate(parentProject);

                    machineCalculationParameters.ProcessStepHoursPerShift = machine.ProcessStep.HoursPerShift.GetValueOrDefault();
                    machineCalculationParameters.ProcessStepProductionWeeksPerYear = machine.ProcessStep.ProductionWeeksPerYear.GetValueOrDefault();
                    machineCalculationParameters.ProcessStepShiftsPerWeek = machine.ProcessStep.ShiftsPerWeek.GetValueOrDefault();
                    machineCalculationParameters.ProcessStepExtraShiftsPerWeek = machine.ProcessStep.ExtraShiftsNumber.GetValueOrDefault();

                    var calculator = CostCalculatorFactory.GetCalculator(calculationVariant);
                    cost = calculator.CalculateMachineCost(machine, machineCalculationParameters);

                    // Cache the cost.
                    CacheCost(machine.Guid, cost);
                }
            }

            return cost;
        }

        /// <summary>
        /// Gets the cost of a specified entity from the internal cache.
        /// </summary>
        /// <typeparam name="T">The type of the object representing the cost.</typeparam>
        /// <param name="key">The key based on which to retrieve the cost.</param>
        /// <param name="cost">The cost.</param>
        /// <returns>
        /// The cost if it is found in the cache and is not expired; otherwise, returns null.
        /// </returns>
        private static bool GetCachedCost<T>(Guid key, out T cost)
        {
            var entityCost = cachedCosts.FirstOrDefault(c => c.Key == key);
            if (entityCost == null)
            {
                cost = default(T);
                return false;
            }

            if ((DateTime.Now - entityCost.Timestamp).Minutes <= CostCacheTime)
            {
                // Return the cost from the cache.
                cost = (T)entityCost.Cost;
                return true;
            }
            else
            {
                // The cost cache has expired so remove it from cache.
                cachedCosts.Remove(entityCost);
                cost = default(T);
                return false;
            }
        }

        /// <summary>
        /// Caches internally the specified cost.
        /// </summary>
        /// <param name="key">The key that identifies the cost in the cache.</param>
        /// <param name="cost">The cost to cache.</param>
        private static void CacheCost(Guid key, object cost)
        {
            // If a cost with the specified key is already cached remove it from the cache.
            var costInCache = cachedCosts.FirstOrDefault(c => c.Key == key);
            if (costInCache != null)
            {
                cachedCosts.Remove(costInCache);
            }

            var cacheEntry = new CachedEntityCost(key, cost, DateTime.Now);
            cachedCosts.Add(cacheEntry);
        }

        /// <summary>
        /// Represents an entity cost in the cache.
        /// </summary>
        private class CachedEntityCost
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="CachedEntityCost"/> class.
            /// </summary>
            /// <param name="key">The key that identifies the cost.</param>
            /// <param name="cost">The cost.</param>
            /// <param name="timestamp">The timestamp associated with this cache entry.</param>
            public CachedEntityCost(Guid key, object cost, DateTime timestamp)
            {
                this.Key = key;
                this.Cost = cost;
                this.Timestamp = timestamp;
            }

            /// <summary>
            /// Gets the key that identifies the cached cost. Usually is the id of the entity to which the cost belongs.
            /// </summary>
            public Guid Key { get; private set; }

            /// <summary>
            /// Gets the cost.
            /// </summary>
            public object Cost { get; private set; }

            /// <summary>
            /// Gets the time stamp when the cost was last calculated.
            /// </summary>
            public DateTime Timestamp { get; private set; }
        }
    }
}