﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;

namespace ZPKTool.Business.Search
{
    /// <summary>
    /// Handles the advanced search feature; the search should be done against the local database when searching in master data entities (public data) and in user's data (private data),
    /// but must be done also against the central DB if it is possible to search in released projects (public data, available only when online) or in other projects (projects of the other users).
    /// </summary>
    public class AdvancedSearchManager
    {
        #region Fields

        /// <summary>
        /// The entity set mappings.
        /// </summary>
        private static IEnumerable<ModelEntitySetMapping> entitySetMappings;

        /// <summary>
        /// The association set mappings.
        /// </summary>
        private static IEnumerable<ModelAssociationSetMapping> assocationSetMappings;

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes static members of the <see cref="AdvancedSearchManager" /> class.
        /// </summary>
        static AdvancedSearchManager()
        {
            InitializeMappings();
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Gets or sets the configurations.
        /// </summary>
        public IEnumerable<AdvancedSearchBasicConfiguration> Configurations { get; set; }

        /// <summary>
        /// Gets or sets the type of the selected entity.
        /// </summary>
        public Type SelectedEntityType { get; set; }

        /// <summary>
        /// Gets or sets the scope selection.
        /// </summary>
        public SearchScopeFilter ScopeSelection { get; set; }

        /// <summary>
        /// Gets or sets the consumable amount measurement unit.
        /// </summary>
        public MeasurementUnit ConsumableAmountUnit { get; set; }

        #endregion Properties

        #region Initializations

        /// <summary>
        /// Initializes the mappings between the model and the database.
        /// </summary>
        private static void InitializeMappings()
        {
            string mslSchemaDocument = null;
            using (var mslFileStream = System.Reflection.Assembly.GetAssembly(typeof(DataEntities)).GetManifestResourceStream("DataModel.msl"))
            {
                mslSchemaDocument = new StreamReader(mslFileStream).ReadToEnd();
            }

            var dataModelMsl = XDocument.Parse(mslSchemaDocument);
            XNamespace mslNamespace = dataModelMsl.Root.Name.Namespace;
            var mappingFragmentXName = XName.Get("MappingFragment", mslNamespace.NamespaceName);
            var scalarPropertyXName = XName.Get("ScalarProperty", mslNamespace.NamespaceName);

            var entitySetMap = new List<ModelEntitySetMapping>();
            foreach (var fragment in dataModelMsl.Descendants(mappingFragmentXName))
            {
                var typeName = fragment.Parent.Attribute("TypeName").Value;
                var className = string.Empty;
                if (typeName.Contains('('))
                {
                    className = typeName.Substring(typeName.IndexOf('.') + 1, typeName.IndexOf(')') - typeName.IndexOf('.') - 1);
                }
                else
                {
                    className = typeName.Substring(typeName.IndexOf('.') + 1, typeName.Length - typeName.IndexOf('.') - 1);
                }

                var tableName = fragment.Attribute("StoreEntitySet");
                foreach (var property in fragment.Elements(scalarPropertyXName))
                {
                    entitySetMap.Add(new ModelEntitySetMapping { EntityName = className, TableName = tableName.Value, PropertyName = property.Attribute("Name").Value, ColumnName = property.Attribute("ColumnName").Value });
                }
            }

            // Some mappings need to be manually specified because they are hard to get.
            entitySetMap.Add(new ModelEntitySetMapping() { EntityName = "Project", TableName = "Projects", PropertyName = "ProjectLeaderGuid", ColumnName = "ProjectLeaderGuid" });
            entitySetMap.Add(new ModelEntitySetMapping() { EntityName = "Project", TableName = "Projects", PropertyName = "ResponsableCalculatorGuid", ColumnName = "ResponsableCalculatorGuid" });
            entitySetMap.Add(new ModelEntitySetMapping() { EntityName = "Part", TableName = "Parts", PropertyName = "CalculatorGuid", ColumnName = "CalculatorGuid" });
            entitySetMap.Add(new ModelEntitySetMapping() { EntityName = "RawMaterial", TableName = "RawMaterials", PropertyName = "DeliveryTypeGuid", ColumnName = "DeliveryTypeGuid" });
            entitySetMap.Add(new ModelEntitySetMapping() { EntityName = "Die", TableName = "Dies", PropertyName = "WearPercentage", ColumnName = "Wear" });
            entitySetMap.Add(new ModelEntitySetMapping() { EntityName = "Die", TableName = "Dies", PropertyName = "KValue", ColumnName = "Maintenance" });
            entitySetMap.Add(new ModelEntitySetMapping() { EntityName = "Machine", TableName = "Machines", PropertyName = "RenewableEnergyConsumptionRatio", ColumnName = "FossilEnergyConsumptionRatio" });
            entitySetMap.Add(new ModelEntitySetMapping() { EntityName = "Machine", TableName = "Machines", PropertyName = "MainClassificationGuid", ColumnName = "MainClassificationGuid" });
            entitySetMap.Add(new ModelEntitySetMapping() { EntityName = "Machine", TableName = "Machines", PropertyName = "TypeClassificationGuid", ColumnName = "TypeClassificationGuid" });
            entitySetMap.Add(new ModelEntitySetMapping() { EntityName = "Machine", TableName = "Machines", PropertyName = "SubClassificationGuid", ColumnName = "SubClassificationGuid" });
            entitySetMap.Add(new ModelEntitySetMapping() { EntityName = "Machine", TableName = "Machines", PropertyName = "ClassificationLevel4Guid", ColumnName = "ClassificationLevel4Guid" });
            entitySetMap.Add(new ModelEntitySetMapping() { EntityName = "RawMaterial", TableName = "RawMaterials", PropertyName = "ClassificationLevel1Guid", ColumnName = "ClassificationLevel1Guid" });
            entitySetMap.Add(new ModelEntitySetMapping() { EntityName = "RawMaterial", TableName = "RawMaterials", PropertyName = "ClassificationLevel2Guid", ColumnName = "ClassificationLevel2Guid" });
            entitySetMap.Add(new ModelEntitySetMapping() { EntityName = "RawMaterial", TableName = "RawMaterials", PropertyName = "ClassificationLevel3Guid", ColumnName = "ClassificationLevel3Guid" });
            entitySetMap.Add(new ModelEntitySetMapping() { EntityName = "RawMaterial", TableName = "RawMaterials", PropertyName = "ClassificationLevel4Guid", ColumnName = "ClassificationLevel4Guid" });

            string ssdlSchemaDocument = null;
            using (var ssdlStream = System.Reflection.Assembly.GetAssembly(typeof(DataEntities)).GetManifestResourceStream("DataModel.ssdl"))
            {
                ssdlSchemaDocument = new StreamReader(ssdlStream).ReadToEnd();
            }

            var dataModelSsdl = XDocument.Parse(ssdlSchemaDocument);
            XNamespace ssdlNamespace = "http://schemas.microsoft.com/ado/2009/02/edm/ssdl";
            var associationXName = XName.Get("Association", ssdlNamespace.NamespaceName);
            var principalXName = XName.Get("Principal", ssdlNamespace.NamespaceName);
            var dependentXName = XName.Get("Dependent", ssdlNamespace.NamespaceName);
            var propertyRefXName = XName.Get("PropertyRef", ssdlNamespace.NamespaceName);

            var assocationSetMap = new List<ModelAssociationSetMapping>();
            foreach (var fragment in dataModelSsdl.Descendants(associationXName))
            {
                var principalTable = fragment.Descendants(principalXName).FirstOrDefault();
                var principalColumn = principalTable.Descendants(propertyRefXName).FirstOrDefault();
                var dependentTable = fragment.Descendants(dependentXName).FirstOrDefault();
                var dependentColumn = dependentTable.Descendants(propertyRefXName).FirstOrDefault();

                assocationSetMap.Add(new ModelAssociationSetMapping { PrincipalTableName = principalTable.Attribute("Role").Value, PrincipalColumnName = principalColumn.Attribute("Name").Value, DependentTableName = dependentTable.Attribute("Role").Value, DependentColumnName = dependentColumn.Attribute("Name").Value });
            }

            entitySetMappings = entitySetMap;
            assocationSetMappings = assocationSetMap;
        }

        #endregion Initializations

        /// <summary>
        /// Performs the search by building and executing a SQL statement that returns the entities that match the specified criterias.
        /// </summary>
        /// <returns>The results that match the specified criterias.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Justification = "Complexity can not be reduced.")]
        public Collection<AdvancedSearchResult> Search()
        {
            // Get the searched table name.
            string searchedTableName = entitySetMappings.FirstOrDefault(m => m.EntityName == this.SelectedEntityType.Name).TableName;

            var selectStatement = new StringBuilder("INSERT INTO @Results(Id) SELECT " + searchedTableName + ".Guid FROM " + searchedTableName + " ");
            var whereConditions = new StringBuilder(" WHERE ");
            var joinConditions = new List<string>();

            foreach (var item in this.Configurations.Where(i => i.FirstValue != null).OrderBy(i => i.PropertyName))
            {
                // Properties that appear more that one time are grouped together using the logical operation OR and a bracket that contains them is opened.
                var firstItem = this.Configurations.FirstOrDefault(i => i.PropertyName == item.PropertyName && i.FirstValue != null);
                var lastItem = this.Configurations.LastOrDefault(i => i.PropertyName == item.PropertyName && i.FirstValue != null);
                if (firstItem == item
                     && item != lastItem)
                {
                    whereConditions.Append("(");
                }

                if (item.SearchedProperties.Count > 1)
                {
                    whereConditions.Append("(");
                }

                for (var crtIteration = 0; crtIteration < item.SearchedProperties.Count(); crtIteration++)
                {
                    var currentPropertyName = item.SearchedProperties[crtIteration];

                    var entitySetMapping = entitySetMappings.FirstOrDefault(m => m.EntityName + "." + m.PropertyName == currentPropertyName);
                    if (entitySetMapping == null)
                    {
                        continue;
                    }

                    if (entitySetMapping.TableName != searchedTableName)
                    {
                        var associationSetMapping = assocationSetMappings.FirstOrDefault(t => t.PrincipalTableName == entitySetMapping.TableName && t.DependentTableName == searchedTableName);
                        if (associationSetMapping == null)
                        {
                            associationSetMapping = assocationSetMappings.FirstOrDefault(t => t.PrincipalTableName == searchedTableName && t.DependentTableName == entitySetMapping.TableName);
                            if (associationSetMapping == null)
                            {
                                continue;
                            }
                        }

                        // Verify if the join exists before adding it.
                        var join = " LEFT OUTER JOIN " + entitySetMapping.TableName + " ON " + associationSetMapping.PrincipalTableName + "." + associationSetMapping.PrincipalColumnName + " = " + associationSetMapping.DependentTableName + "." + associationSetMapping.DependentColumnName + " ";
                        if (!joinConditions.Contains(join))
                        {
                            joinConditions.Add(join);
                        }
                    }

                    if (NumericUtils.IsNumber(item.FirstValue))
                    {
                        var firstValue = Convert.ToDecimal(item.FirstValue, CultureInfo.InvariantCulture);
                        var secondValue = Convert.ToDecimal(item.SecondValue, CultureInfo.InvariantCulture);

                        if (currentPropertyName.Contains(ReflectionUtils.GetPropertyName<Machine>(m => m.RenewableEnergyConsumptionRatio)))
                        {
                            firstValue = 1 - firstValue;
                            secondValue = 1 - secondValue;
                        }

                        if (item.SecondValue == null)
                        {
                            int decimalsNo = 0;
                            string firstValueString = Convert.ToString(firstValue, CultureInfo.InvariantCulture);
                            var decimalSeparatorIndex = firstValueString.IndexOf(".");
                            if (decimalSeparatorIndex > -1 && decimalSeparatorIndex < firstValueString.Length - 1)
                            {
                                // Count the number of decimals from the first value.
                                firstValueString = firstValueString.TrimEnd('0');
                                decimalsNo = firstValueString.Substring(decimalSeparatorIndex + 1).Length;
                            }

                            // The entities that can be scrambled can only be searched by value and not by range.
                            string firstValueConverted = firstValue.ToString(CultureInfo.InvariantCulture);
                            if (currentPropertyName.Contains("Scrambled"))
                            {
                                var firstValueScrambled = MasterDataScrambler.ScrambleNumber((decimal)item.FirstValue).ToString(CultureInfo.InvariantCulture);
                                if (decimalsNo > 0)
                                {
                                    whereConditions.Append(" ROUND(" + entitySetMapping.TableName + "." + entitySetMapping.ColumnName + ", " + decimalsNo + ", 1) = CASE WHEN " + entitySetMapping.TableName + ".IsScrambled = 'True' THEN " + firstValueScrambled + " ELSE " + firstValueConverted + " END");
                                }
                                else
                                {
                                    whereConditions.Append(entitySetMapping.TableName + "." + entitySetMapping.ColumnName + " = CASE WHEN " + entitySetMapping.TableName + ".IsScrambled = 'True' THEN " + firstValueScrambled + " ELSE " + firstValueConverted + " END");
                                }
                            }
                            else
                            {
                                if (decimalsNo > 0)
                                {
                                    whereConditions.Append(" ROUND(" + entitySetMapping.TableName + "." + entitySetMapping.ColumnName + ", " + decimalsNo + ", 1) = " + firstValueConverted);
                                }
                                else
                                {
                                    whereConditions.Append(entitySetMapping.TableName + "." + entitySetMapping.ColumnName + " = " + firstValueConverted);
                                }
                            }
                        }
                        else
                        {
                            string firstValueConverted = firstValue.ToString(CultureInfo.InvariantCulture);
                            string secondValueConverted = secondValue.ToString(CultureInfo.InvariantCulture);
                            whereConditions.Append(entitySetMapping.TableName + "." + entitySetMapping.ColumnName + " BETWEEN " + firstValueConverted + " AND " + secondValueConverted);
                        }

                        if (item.ConsumableAmountUnit != null)
                        {
                            whereConditions.Append(" AND " + entitySetMapping.TableName + ".AmountUnitGuid = '" + item.ConsumableAmountUnit.Guid + "'");
                        }
                    }

                    if (item.FirstValue is DateTime)
                    {
                        if (item.SecondValue == null)
                        {
                            whereConditions.Append("DATEDIFF(d, " + entitySetMapping.TableName + "." + entitySetMapping.ColumnName + ", '" + ((DateTime)item.FirstValue).ToString(CultureInfo.InvariantCulture) + "') = 0 ");
                        }
                        else
                        {
                            whereConditions.Append(entitySetMapping.TableName + "." + entitySetMapping.ColumnName + " BETWEEN '" + ((DateTime)item.FirstValue).ToString(CultureInfo.InvariantCulture) + "' AND DATEADD(dd, 1, '" + ((DateTime)item.SecondValue).ToString(CultureInfo.InvariantCulture) + "')");
                        }
                    }

                    if (item.FirstValue is bool)
                    {
                        whereConditions.Append(entitySetMapping.TableName + "." + entitySetMapping.ColumnName + " = '" + item.FirstValue + "'");
                    }

                    if (item.FirstValue is string)
                    {
                        var searchedText = item.FirstValue.ToString();

                        if (currentPropertyName.Contains("Scrambled"))
                        {
                            var searchedTextScrambled = MasterDataScrambler.ScrambleText(searchedText);
                            whereConditions.Append(entitySetMapping.TableName + "." + entitySetMapping.ColumnName + " LIKE CASE WHEN " + entitySetMapping.TableName + ".IsScrambled = 'True' THEN N'%" + searchedTextScrambled + "%' ELSE N'%" + searchedText + "%' END");
                        }
                        else
                        {
                            // For properties that represent a name replace all the non alpha numeric characters with '%'.
                            // This is done to increases the number of search results.
                            if (currentPropertyName.Contains(".Name"))
                            {
                                // For entities of type machine separate alphabetic characters from numbers by a ' '.
                                // This is done to increases the number of search results.
                                if (this.SelectedEntityType == typeof(Machine))
                                {
                                    var modifiedSearchText = new StringBuilder();
                                    for (int index = 1; index < searchedText.Length; index++)
                                    {
                                        var currentChar = searchedText[index];
                                        var previousChar = searchedText[index - 1];

                                        modifiedSearchText = modifiedSearchText.Append(previousChar);

                                        if ((char.IsLetter(currentChar) && char.IsDigit(previousChar))
                                            || (char.IsDigit(currentChar) && char.IsLetter(previousChar)))
                                        {
                                            modifiedSearchText = modifiedSearchText.Append(" ");
                                        }
                                    }

                                    modifiedSearchText.Append(searchedText[searchedText.Length - 1]);
                                    searchedText = modifiedSearchText.ToString();
                                }

                                var regExpression = new Regex("[^a-zA-Z0-9]");
                                searchedText = regExpression.Replace(searchedText, "%");
                                searchedText = searchedText.Replace(' ', '%');
                            }

                            whereConditions.Append(entitySetMapping.TableName + "." + entitySetMapping.ColumnName + " LIKE N'%" + searchedText + "%'");
                        }
                    }

                    if (item.FirstValue is Guid)
                    {
                        whereConditions.Append(entitySetMapping.TableName + "." + entitySetMapping.ColumnName + " = '" + item.FirstValue + "'");
                    }

                    var ids = item.FirstValue as Collection<Guid>;
                    if (ids != null
                        && crtIteration < ids.Count())
                    {
                        whereConditions.Append(entitySetMapping.TableName + "." + entitySetMapping.ColumnName + " = '" + ids[crtIteration] + "'");
                    }

                    if (item.SearchedProperties.Count > 1)
                    {
                        var valuesList = item.FirstValue as IList;
                        if (valuesList != null
                            && crtIteration < valuesList.Count
                            && valuesList[crtIteration] != null)
                        {
                            whereConditions.Append(" AND ");
                        }
                        else if (valuesList == null
                                && item.FirstValue != null)
                        {
                            whereConditions.Append(" OR ");
                        }
                    }
                }

                // Remove the last OR and close the bracket.
                if (item.SearchedProperties.Count > 1)
                {
                    whereConditions.Remove(whereConditions.Length - 4, 4);
                    whereConditions.Append(")");
                }

                // Properties that appear more that one time are grouped together using the logical operation OR and the bracket that contains them is closed.
                if (lastItem == item
                    && lastItem != firstItem)
                {
                    whereConditions.Append(")");
                }

                if (item != lastItem)
                {
                    whereConditions.Append(" OR ");
                }
                else if (whereConditions.Length > 7)
                {
                    whereConditions.Append(" AND ");
                }
            }

            // The join conditions are added to the select statement.
            foreach (var join in joinConditions)
            {
                selectStatement.Append(join);
            }

            selectStatement.Append(whereConditions.ToString());
            if (typeof(ITrashable).IsAssignableFrom(this.SelectedEntityType))
            {
                selectStatement.Append(searchedTableName + ".IsDeleted = 'False' AND ");
            }

            if (this.SelectedEntityType == typeof(RawPart))
            {
                selectStatement.Append(searchedTableName + ".IsRawPart = 'True' AND ");
            }
            else if (this.SelectedEntityType == typeof(Part))
            {
                selectStatement.Append(searchedTableName + ".IsRawPart = 'False' AND ");
            }

            bool searchInMyProjects = (this.ScopeSelection & SearchScopeFilter.MyProjects) == SearchScopeFilter.MyProjects;
            bool searchInReleasedProjects = (this.ScopeSelection & SearchScopeFilter.ReleasedProjects) == SearchScopeFilter.ReleasedProjects;
            bool searchInOtherProjects = (this.ScopeSelection & SearchScopeFilter.OtherUsersProjects) == SearchScopeFilter.OtherUsersProjects;
            bool searchInMasterData = (this.ScopeSelection & SearchScopeFilter.MasterData) == SearchScopeFilter.MasterData;

            var currentUserGuid = SecurityManager.Instance.CurrentUser.Guid;
            bool centralIsOnline = DatabaseHelper.IsCentralDatabaseOnline();

            var localStatement = new StringBuilder(selectStatement.ToString());
            var centralStatement = new StringBuilder(selectStatement.ToString());

            var isLocalBracketAdded = false;
            var isCentralBracketAdded = false;

            if (searchInMyProjects)
            {
                if (SecurityManager.Instance.CurrentUserHasRight(Right.ProjectsAndCalculate))
                {
                    localStatement.Append(isLocalBracketAdded == true ? string.Empty : "(");
                    isLocalBracketAdded = true;
                    localStatement.Append("(" + searchedTableName + ".OwnerGuid = '" + currentUserGuid + "') OR ");
                }
            }

            if (searchInReleasedProjects)
            {
                if (typeof(IMasterDataObject).IsAssignableFrom(this.SelectedEntityType))
                {
                    localStatement.Append(isLocalBracketAdded == true ? string.Empty : "(");
                    isLocalBracketAdded = true;
                    localStatement.Append("(" + searchedTableName + ".OwnerGuid IS NULL AND " + searchedTableName + ".IsMasterData = 'False') OR ");
                }

                if (typeof(IReleasable).IsAssignableFrom(this.SelectedEntityType))
                {
                    localStatement.Append(isLocalBracketAdded == true ? string.Empty : "(");
                    isLocalBracketAdded = true;
                    localStatement.Append("(" + searchedTableName + ".IsReleased = 'True') OR ");
                }
            }

            var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
            var otherEntitiesId = dsm.GetOtherUsersEntitiesId(SecurityManager.Instance.CurrentUser.Guid, searchedTableName);

            if (searchInOtherProjects
                && otherEntitiesId.Count > 0)
            {
                centralStatement.Append(isCentralBracketAdded == true ? string.Empty : "(");
                isCentralBracketAdded = true;
                centralStatement.Append("(" + searchedTableName + ".Guid IN (");

                foreach (var entityId in otherEntitiesId)
                {
                    centralStatement.Append("'" + entityId + "', ");
                }

                centralStatement.Remove(centralStatement.Length - 2, 2);
                centralStatement.Append(")) OR ");
            }

            if (searchInMasterData
                && typeof(IMasterDataObject).IsAssignableFrom(this.SelectedEntityType))
            {
                // If the central database is not online the master data is searched on the local database.
                if (centralIsOnline)
                {
                    centralStatement.Append(isCentralBracketAdded == true ? string.Empty : "(");
                    isCentralBracketAdded = true;
                    centralStatement.Append("(" + searchedTableName + ".IsMasterData = 'True') OR ");
                }
                else
                {
                    localStatement.Append(isLocalBracketAdded == true ? string.Empty : "(");
                    isLocalBracketAdded = true;
                    localStatement.Append("(" + searchedTableName + ".IsMasterData = 'True') OR ");
                }
            }

            if (isLocalBracketAdded)
            {
                localStatement.Remove(localStatement.Length - 3, 3);
                localStatement.Append(")");
            }
            else
            {
                localStatement.Remove(localStatement.Length - 4, 4);
            }

            if (isCentralBracketAdded)
            {
                centralStatement.Remove(centralStatement.Length - 3, 3);
                centralStatement.Append(")");
            }
            else
            {
                centralStatement.Remove(centralStatement.Length - 4, 4);
            }

            var statement = this.CreateSqlSelectScript(localStatement.ToString(), searchedTableName);

            var results = new Collection<AdvancedSearchResult>();

            var localResults = new List<Tuple<object, Currency>>();
            if (searchInMyProjects
                || searchInReleasedProjects)
            {
                var localResultIds = this.ExecuteStatement(statement, new SqlConnection(ConnectionConfiguration.LocalDbSQLConnectionString));
                localResults = this.GetEntitiesFromDb(DbIdentifier.LocalDatabase, localResultIds, this.SelectedEntityType);

                foreach (var result in localResults)
                {
                    results.Add(new AdvancedSearchResult() { Entity = result.Item1, BaseCurrency = result.Item2, DatabaseId = DbIdentifier.LocalDatabase });
                }
            }

            statement = this.CreateSqlSelectScript(centralStatement.ToString(), searchedTableName);

            var centralResults = new List<Tuple<object, Currency>>();
            if ((searchInMasterData
                && typeof(IMasterDataObject).IsAssignableFrom(this.SelectedEntityType))
                    || (searchInOtherProjects
                    && otherEntitiesId.Count > 0))
            {
                if (centralIsOnline)
                {
                    var centralResultIds = this.ExecuteStatement(statement, new SqlConnection(ConnectionConfiguration.CentralDbSQLConnectionString));
                    centralResults = this.GetEntitiesFromDb(DbIdentifier.CentralDatabase, centralResultIds, this.SelectedEntityType);
                }

                foreach (var result in centralResults)
                {
                    results.Add(new AdvancedSearchResult() { Entity = result.Item1, BaseCurrency = result.Item2, DatabaseId = DbIdentifier.CentralDatabase });
                }
            }

            return results;
        }

        /// <summary>
        /// Creates the SQL script that selects the ids of the searched entities and the ids of their base currencies.
        /// </summary>
        /// <param name="statement">The statement.</param>
        /// <param name="searchedTableName">Name of the searched table.</param>
        /// <returns>A SQL script</returns>
        private string CreateSqlSelectScript(string statement, string searchedTableName)
        {
            var script = @"DECLARE @CurrentId uniqueidentifier
                DECLARE @ParentId uniqueidentifier
                DECLARE @DefaultBaseCurrencyId uniqueidentifier
                DECLARE @TempId uniqueidentifier
                DECLARE @Results TABLE(Id uniqueidentifier, BaseCurrencyId uniqueidentifier NULL) "

                + statement

                + @"DECLARE EntitiesId CURSOR FOR SELECT Id FROM @Results

                SELECT @DefaultBaseCurrencyId = Guid FROM Currencies WHERE IsMasterData = 'True' AND IsoCode = '" + Constants.DefaultCurrencyIsoCode + @"'

                OPEN EntitiesId
                FETCH NEXT FROM EntitiesId INTO @CurrentId

                WHILE @@Fetch_Status = 0
                BEGIN
                    SET @ParentId = NULL
                    SET @TempId = NULL

                    EXEC [dbo].[FindTopParentId]
                        @childId = @CurrentId,
                        @childTableName = " + searchedTableName + @",
                        @parentType = 1,
                        @parentId = @ParentId OUTPUT

                    IF @ParentId IS NOT NULL
                    BEGIN
                        SELECT @TempId = BaseCurrencyId FROM Projects WHERE Projects.Guid = @ParentId
                        UPDATE @Results SET BaseCurrencyId = @TempId WHERE Id = @CurrentId
                    END                    
                    
                FETCH NEXT FROM EntitiesId INTO @CurrentId
                END
    
                CLOSE EntitiesId
                DEALLOCATE EntitiesId

                UPDATE @Results SET BaseCurrencyId = @DefaultBaseCurrencyId WHERE BaseCurrencyId IS NULL

                SELECT * FROM @Results";

            return script;
        }

        /// <summary>
        /// Executes the statement using an SQL command.
        /// </summary>
        /// <param name="statement">The select statement.</param>
        /// <param name="connection">The sql connection.</param>
        /// <returns>A list of tuples that contains the id of the entity and the id of its associated base currency.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "The query string is not composed using data from user input.")]
        private List<Tuple<Guid, Guid>> ExecuteStatement(string statement, SqlConnection connection)
        {
            var resultIds = new List<Tuple<Guid, Guid>>();

            using (var sqlConnection = connection)
            {
                using (var sqlCommand = new SqlCommand())
                {
                    sqlCommand.Connection = sqlConnection;
                    sqlCommand.CommandTimeout = 180;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = statement;
                    sqlConnection.Open();

                    // Read the data returned by the select statement.
                    using (var reader = sqlCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            resultIds.Add(new Tuple<Guid, Guid>(reader.GetGuid(0), reader.GetGuid(1)));
                        }
                    }
                }
            }

            return resultIds;
        }

        /// <summary>
        /// Gets the entities from database.
        /// </summary>
        /// <param name="databaseId">The database id.</param>
        /// <param name="entitiesData">The entities id.</param>
        /// <param name="entityType">Type of the entity.</param>
        /// <returns>Returns a list of entities.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Justification = "Complexity can not be reduced.")]
        private List<Tuple<object, Currency>> GetEntitiesFromDb(DbIdentifier databaseId, List<Tuple<Guid, Guid>> entitiesData, Type entityType)
        {
            var dsm = DataAccessFactory.CreateDataSourceManager(databaseId);
            var entities = new List<object>();

            if (entityType == typeof(Assembly))
            {
                entities.AddRange(dsm.AssemblyRepository.GetByIds(entitiesData.Select(e => e.Item1)).OrderBy(item => item.Name));
            }
            else if (entityType == typeof(Commodity))
            {
                entities.AddRange(dsm.CommodityRepository.GetByIds(entitiesData.Select(e => e.Item1)).OrderBy(item => item.Name));
            }
            else if (entityType == typeof(Consumable))
            {
                entities.AddRange(dsm.ConsumableRepository.GetByIds(entitiesData.Select(e => e.Item1)).OrderBy(item => item.Name));
            }
            else if (entityType == typeof(Die))
            {
                entities.AddRange(dsm.DieRepository.GetByIds(entitiesData.Select(e => e.Item1)).OrderBy(item => item.Name));
            }
            else if (entityType == typeof(Machine))
            {
                entities.AddRange(dsm.MachineRepository.GetByIds(entitiesData.Select(e => e.Item1)).OrderBy(item => item.Name));
            }
            else if (entityType == typeof(Part)
                    || entityType == typeof(RawPart))
            {
                entities.AddRange(dsm.PartRepository.GetByIds(entitiesData.Select(e => e.Item1)).OrderBy(item => item.Name));
            }
            else if (entityType == typeof(ProcessStep))
            {
                entities.AddRange(dsm.ProcessStepRepository.GetByIds(entitiesData.Select(e => e.Item1)).OrderBy(item => item.Name));
            }
            else if (entityType == typeof(Project))
            {
                entities.AddRange(dsm.ProjectRepository.GetByIds(entitiesData.Select(e => e.Item1)).OrderBy(item => item.Name));
            }
            else if (entityType == typeof(RawMaterial))
            {
                entities.AddRange(dsm.RawMaterialRepository.GetByIds(entitiesData.Select(e => e.Item1)).OrderBy(item => item.Name));
            }
            else
            {
                return null;
            }

            // Gets the base currencies, without duplicates.
            var baseCurrencies = dsm.CurrencyRepository.GetCurrencies(entitiesData.Select(e => e.Item2));

            var result = new List<Tuple<object, Currency>>();
            foreach (var entity in entitiesData)
            {
                result.Add(new Tuple<object, Currency>(entities.FirstOrDefault(e => (e as IIdentifiable).Guid == entity.Item1), baseCurrencies.FirstOrDefault(c => c.Guid == entity.Item2)));
            }

            return result;
        }

        /// <summary>
        /// The model entity set mapping class.
        /// </summary>
        private class ModelEntitySetMapping
        {
            /// <summary>
            /// Gets or sets the name of the entity.
            /// </summary>
            public string EntityName { get; set; }

            /// <summary>
            /// Gets or sets the name of the table.
            /// </summary>
            public string TableName { get; set; }

            /// <summary>
            /// Gets or sets the name of the property.
            /// </summary>
            public string PropertyName { get; set; }

            /// <summary>
            /// Gets or sets the name of the column.
            /// </summary>
            public string ColumnName { get; set; }
        }

        /// <summary>
        /// The model association set mapping class.
        /// </summary>
        private class ModelAssociationSetMapping
        {
            /// <summary>
            /// Gets or sets the name of the principal table.
            /// </summary>
            public string PrincipalTableName { get; set; }

            /// <summary>
            /// Gets or sets the name of the principal column.
            /// </summary>
            public string PrincipalColumnName { get; set; }

            /// <summary>
            /// Gets or sets the name of the dependent table.
            /// </summary>
            public string DependentTableName { get; set; }

            /// <summary>
            /// Gets or sets the name of the dependent column.
            /// </summary>
            public string DependentColumnName { get; set; }
        }
    }
}