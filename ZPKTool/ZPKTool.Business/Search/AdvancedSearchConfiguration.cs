﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using ZPKTool.Data;

namespace ZPKTool.Business.Search
{
    /// <summary>
    /// The advanced search basic configuration class.
    /// </summary>
    public class AdvancedSearchBasicConfiguration
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AdvancedSearchBasicConfiguration" /> class.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="firstValue">The first value.</param>
        /// <param name="secondValue">The second value.</param>
        /// <param name="searchedProperties">The searched properties.</param>
        /// <param name="consumableAmountUnit">The consumable amount measurement unit.</param>
        public AdvancedSearchBasicConfiguration(
            string propertyName,
            object firstValue,
            object secondValue,
            Collection<string> searchedProperties,
            MeasurementUnit consumableAmountUnit)
        {
            this.PropertyName = propertyName;
            this.FirstValue = firstValue;
            this.SecondValue = secondValue;
            this.SearchedProperties = new Collection<string>(searchedProperties);
            this.ConsumableAmountUnit = consumableAmountUnit;
        }

        /// <summary>
        /// Gets the name of the property.
        /// </summary>
        public string PropertyName { get; private set; }

        /// <summary>
        /// Gets the first value.
        /// </summary>
        public object FirstValue { get; private set; }

        /// <summary>
        /// Gets the second value.
        /// </summary>
        public object SecondValue { get; private set; }

        /// <summary>
        /// Gets the searched properties.
        /// </summary>
        public Collection<string> SearchedProperties { get; private set; }

        /// <summary>
        /// Gets the consumable amount measurement unit.
        /// </summary>
        public MeasurementUnit ConsumableAmountUnit { get; private set; }
    }
}