﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;

namespace ZPKTool.Business.Search
{
    /// <summary>
    /// Handles the search feature; the search should be done against the local database when searching 
    /// in master data entities (public data) and in user's data (private data), but must be done also
    /// against the central DB if it is possible to search in released projects (public data, available 
    /// only when online) or in other projects (projects of the other users).
    /// </summary>
    /// TODO: should not be singleton (for example, you can't run parallel searches this way).
    public sealed class SearchManager
    {
        #region Constants

        /// <summary>
        /// The timeout in seconds for search operations
        /// </summary>
        private const int CommandTimeoutForSearchOperation = 60;

        /// <summary>
        /// Define the wildcard character to mach one or more character
        /// This character is used for searching on the GUI
        /// </summary>
        public const char DefaultUserWildcardMany = '*';

        /// <summary>
        /// Define the wildcard character to mach exactly one character
        /// This character is used for searching on the GUI
        /// </summary>
        public const char DefaultUserWildcardOne = '?';

        /// <summary>
        /// Define the wildcard character to mach one or more character
        /// This character is used for creating the SQL select
        /// </summary>
        public const char DefaultSQLWildcardMany = '%';

        /// <summary>
        /// Define the wildcard character to mach exactly one character
        /// This character is used for creating the SQL select
        /// </summary>
        public const char DefaultSQLWildcardOne = '_';

        #endregion

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The only instance of this class.
        /// </summary>
        private static readonly SearchManager instance = new SearchManager();

        /// <summary>
        /// List of properties which are ignored when searching because they are created in the model to mask the scrambled fields
        /// </summary>
        private static List<string> ignoredProperties = new List<string> { "YieldStrength", "RuptureStrength", "MaxElongation", "GlassTransitionTemperature", "Density", "Rx", "Rm" };

        /// <summary>
        /// Prevents a default instance of the <see cref="SearchManager" /> class from being created.
        /// </summary>
        private SearchManager()
        {
        }

        /// <summary>
        /// Gets the only instance of this class.
        /// </summary>        
        public static SearchManager Instance
        {
            get { return instance; }
        }

        #region Generic Methods

        /// <summary>
        /// Searches for entities that contain the searchText in at least one of their string fields.
        /// </summary>
        /// <typeparam name="T">The type of the searched entities.</typeparam>
        /// <param name="searchParameters">The parameters used to search for</param>
        /// <param name="stopSearch">if set to <c>true</c> stop the search.</param>
        /// <returns>
        /// The simple search result on the local and central database
        /// </returns>
        public SimpleSearchResult<T> SearchEntities<T>(SimpleSearchParameters searchParameters, ref bool stopSearch)
        {
            if (searchParameters == null)
            {
                throw new ArgumentNullException("searchParameters", "The search parameters were null.");
            }

            if (string.IsNullOrEmpty(searchParameters.SearchText))
            {
                throw new ArgumentException("The searched text was null or empty.", "searchParameters");
            }

            log.Info("----- Starting search operation. -----");

            var result = new SimpleSearchResult<T>();

            // stop the search before local or central search is performed, return new empty lists.
            if (stopSearch)
            {
                result.ClearResults();
                return result;
            }

            List<T> centralDataResults = new List<T>();
            Guid currentUserGuid = SecurityManager.Instance.CurrentUser.Guid;
            Task centralDbSearchTask = null;

            bool centralIsOnline = DatabaseHelper.IsCentralDatabaseOnline();
            log.Info("Central server is online: {0}.", centralIsOnline);

            if (centralIsOnline)
            {
                // Search in centralDB, stop the search before the search has started.
                if (stopSearch)
                {
                    result.ClearResults();
                    return result;
                }

                // Search in the central database
                SearchTextParams centralSearchParams = new SearchTextParams();
                centralSearchParams.CurrentUserGuid = currentUserGuid;
                centralSearchParams.SearchText = searchParameters.SearchText;
                centralSearchParams.GetDistinctResults = searchParameters.GetDistinctResults;

                // Other users projects and master data is searched in the central server.
                if ((searchParameters.ScopeFilters & SearchScopeFilter.OtherUsersProjects) == SearchScopeFilter.OtherUsersProjects)
                {
                    centralSearchParams.ScopeFilters |= SearchScopeFilter.OtherUsersProjects;
                }

                if ((searchParameters.ScopeFilters & SearchScopeFilter.MasterData) == SearchScopeFilter.MasterData)
                {
                    centralSearchParams.ScopeFilters |= SearchScopeFilter.MasterData;
                }

                if (SecurityManager.Instance.CurrentUserHasRole(Role.Admin))
                {
                    // central search in all others users' data
                    centralSearchParams.IsAdmin = true;
                }
                else if (SecurityManager.Instance.CurrentUserHasRole(Role.User) || SecurityManager.Instance.CurrentUserHasRole(Role.KeyUser))
                {
                    // central search in data for which the user is the responsible calculator or leader
                    centralSearchParams.IsUser = true;
                }
                else if (SecurityManager.Instance.CurrentUserHasRole(Role.ProjectLeader))
                {
                    // central search in data for which the user is leader
                    centralSearchParams.IsProjectLeader = true;
                }

                // Perform the search only if at least 1 filter was specified, otherwise the search will always
                // return 0 results but an sql query will still be performed.
                if (centralSearchParams.ScopeFilters != SearchScopeFilter.None)
                {
                    if (stopSearch)
                    {
                        result.ClearResults();
                        return result;
                    }

                    centralDbSearchTask = Task.Factory.StartNew(() =>
                    {
                        log.Info("Searching in the central database (async).");

                        centralDataResults = SearchText<T>(centralSearchParams, DbIdentifier.CentralDatabase);

                        log.Info("Finished searching in the central database.");
                    });
                }
            }

            if (stopSearch)
            {
                result.ClearResults();
                return result;
            }

            SearchTextParams localSearchParams = new SearchTextParams();
            localSearchParams.CurrentUserGuid = currentUserGuid;
            localSearchParams.SearchText = searchParameters.SearchText;
            localSearchParams.GetDistinctResults = searchParameters.GetDistinctResults;

            localSearchParams.ScopeFilters = SearchScopeFilter.None;
            if ((searchParameters.ScopeFilters & SearchScopeFilter.MyProjects) == SearchScopeFilter.MyProjects)
            {
                if (SecurityManager.Instance.CurrentUserHasRight(Right.ProjectsAndCalculate))
                {
                    localSearchParams.ScopeFilters |= SearchScopeFilter.MyProjects;
                }
            }

            if ((searchParameters.ScopeFilters & SearchScopeFilter.ReleasedProjects) == SearchScopeFilter.ReleasedProjects)
            {
                localSearchParams.ScopeFilters |= SearchScopeFilter.ReleasedProjects;
            }

            // Master data is searched in the local database only if the central database is not online.
            if (!centralIsOnline && (searchParameters.ScopeFilters & SearchScopeFilter.MasterData) == SearchScopeFilter.MasterData)
            {
                localSearchParams.ScopeFilters |= SearchScopeFilter.MasterData;
            }

            // Execute the search in the local db
            if (localSearchParams.ScopeFilters != SearchScopeFilter.None)
            {
                log.Info("Searching in the local database.");
                try
                {
                    // Perform the search only if at least 1 filter was specified, otherwise the search will always
                    // return 0 results but an sql query will still be performed.
                    var localSearchResult = SearchText<T>(localSearchParams, DbIdentifier.LocalDatabase);
                    foreach (var res in localSearchResult)
                    {
                        result.LocalData.Add(res);
                    }
                }
                catch (DataAccessException)
                {
                }

                log.Info("Finished searching in the local database.");
            }

            if (centralDbSearchTask != null)
            {
                log.Info("Waiting for the central database search to finish.");
                try
                {
                    centralDbSearchTask.Wait();
                }
                catch (AggregateException)
                {
                }
            }

            foreach (var res in centralDataResults)
            {
                result.CentralData.Add(res);
            }

            // Stop the search and drop the results, returns an empty list.
            if (stopSearch)
            {
                result.ClearResults();
                return result;
            }

            log.Info("----- Search operation finished. -----");

            return result;
        }

        /// <summary>
        /// Searches Entities based on the specified text.
        /// The text can contain wildcards and can be matched against any relevant text property
        /// </summary>
        /// <typeparam name="T">The type of the entity object</typeparam>
        /// <param name="searchParams">The search parameters.</param>
        /// <param name="context">The context.</param>
        /// <returns>
        /// A list of instances conforming to the given restrictions.
        /// </returns>
        private List<T> SearchText<T>(SearchTextParams searchParams, DbIdentifier context)
        {
            IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(context);

            // Save the current command timeout and set it to the value defined for search operations
            int? oldTimeout = dataManager.EntityContext.CommandTimeout;
            dataManager.EntityContext.CommandTimeout = CommandTimeoutForSearchOperation;

            try
            {
                if (typeof(T) == typeof(RawPart))
                {
                    var query = GetSearchTextRawPartQuery(dataManager.EntityContext, searchParams);
                    ((ObjectQuery)query).MergeOption = MergeOption.NoTracking;
                    return query.ToList().Cast<T>().ToList();
                }
                else
                {
                    IQueryable<T> query = GetSearchTextQueryForType<T>(dataManager.EntityContext, searchParams);

                    ((ObjectQuery)query).MergeOption = MergeOption.NoTracking;
                    List<T> results = query.ToList();
                    return results;
                }
            }
            catch (System.Data.DataException ex)
            {
                log.Error("Data error during search.", ex);
                throw BusinessUtils.ParseDataException(ex, context);
            }
            finally
            {
                // restore the old timeout
                dataManager.EntityContext.CommandTimeout = oldTimeout;
            }
        }

        /// <summary>
        /// Gets the type of the search text query for.
        /// </summary>
        /// <typeparam name="T">The type of the entity item</typeparam>
        /// <param name="context">The context.</param>
        /// <param name="queryParams">The query parameters.</param>
        /// <returns>A query on the requested object set</returns>
        private IQueryable<T> GetSearchTextQueryForType<T>(DataEntities context, SearchTextParams queryParams)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context", "The data context was null.");
            }

            IQueryable<T> query;

            Type type = typeof(T);
            if (type == typeof(Assembly))
            {
                query = (IQueryable<T>)GetSearchTextAssemblyQuery(context, queryParams);
            }
            else if (type == typeof(Commodity))
            {
                query = (IQueryable<T>)GetSearchTextCommodityQuery(context, queryParams);
            }
            else if (type == typeof(Consumable))
            {
                query = (IQueryable<T>)GetSearchTextConsumableQuery(context, queryParams);
            }
            else if (type == typeof(Machine))
            {
                query = (IQueryable<T>)GetSearchTextMachineQuery(context, queryParams);
            }
            else if (type == typeof(Part))
            {
                query = (IQueryable<T>)GetSearchTextPartQuery(context, queryParams);
            }
            else if (type == typeof(Project))
            {
                query = (IQueryable<T>)GetSearchTextProjectQuery(context, queryParams);
            }
            else if (type == typeof(RawMaterial))
            {
                query = (IQueryable<T>)GetSearchTextRawMaterialQuery(context, queryParams);
            }
            else if (type == typeof(ProcessStep))
            {
                query = (IQueryable<T>)GetSearchTextProcessStepQuery(context, queryParams);
            }
            else if (type == typeof(Die))
            {
                query = (IQueryable<T>)GetSearchTextDieQuery(context, queryParams);
            }
            else
            {
                throw new DataAccessException(ErrorCodes.InternalError, "Invalid type for search text query request!");
            }

            return query;
        }

        #endregion

        #region Search Queries

        /// <summary>
        /// Gets the search text assembly query.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="queryParams">The query parameters.</param>
        /// <returns>A query for the assembly set.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.SpacingRules", "SA1008:OpeningParenthesisMustBeSpacedCorrectly", Justification = "The parenthesis spacing is intentional, to achieve better linq query readability.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.SpacingRules", "SA1009:ClosingParenthesisMustBeSpacedCorrectly", Justification = "The parenthesis spacing is intentional, to achieve better linq query readability.")]
        private IQueryable<Assembly> GetSearchTextAssemblyQuery(DataEntities context, SearchTextParams queryParams)
        {
            ObjectQuery<Assembly> source = context.CreateObjectQuery<Assembly>();
            source = UpdateSearchTextQuerySource<Assembly>(source, queryParams.SearchText);

            bool searchInMyProjects = (queryParams.ScopeFilters & SearchScopeFilter.MyProjects) == SearchScopeFilter.MyProjects;
            bool searchInReleasedProjects = (queryParams.ScopeFilters & SearchScopeFilter.ReleasedProjects) == SearchScopeFilter.ReleasedProjects;
            bool searchInOtherProjects = (queryParams.ScopeFilters & SearchScopeFilter.OtherUsersProjects) == SearchScopeFilter.OtherUsersProjects;
            bool searchInMasterData = (queryParams.ScopeFilters & SearchScopeFilter.MasterData) == SearchScopeFilter.MasterData;

            // Search in Other Users Projects using a stored procedure because Linq lacks support for recursive queries.            
            Collection<Guid> otherUsersEntitiesIds = new Collection<Guid>();
            if (searchInOtherProjects)
            {
                var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
                otherUsersEntitiesIds = dsm.GetOtherUsersEntitiesId(SecurityManager.Instance.CurrentUser.Guid, "Assemblies");
            }

            IQueryable<Assembly> query = (from assembly in source
                                              .Include(a => a.Manufacturer)
                                              .Include(a => a.Owner)
                                          where !assembly.IsDeleted &&
                                          (
                                            (searchInMyProjects && assembly.Owner.Guid == queryParams.CurrentUserGuid) ||
                                            (searchInReleasedProjects && assembly.Owner == null && assembly.IsMasterData == false) ||
                                            (searchInOtherProjects && otherUsersEntitiesIds.Contains(assembly.Guid)) ||
                                            (searchInMasterData && assembly.IsMasterData)
                                          )
                                          select assembly).OrderBy(p => p.Name);

            if (queryParams.GetDistinctResults)
            {
                query = query.GroupBy(x => x.Name, (key, group) => group.FirstOrDefault());
            }

            return query;
        }

        /// <summary>
        /// Gets the search text commodity query.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="queryParams">The query parameters.</param>
        /// <returns>A query for the Commodities set.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.SpacingRules", "SA1008:OpeningParenthesisMustBeSpacedCorrectly", Justification = "The parenthesis spacing is intentional, to achieve better linq query readability.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.SpacingRules", "SA1009:ClosingParenthesisMustBeSpacedCorrectly", Justification = "The parenthesis spacing is intentional, to achieve better linq query readability.")]
        private IQueryable<Commodity> GetSearchTextCommodityQuery(DataEntities context, SearchTextParams queryParams)
        {
            ObjectQuery<Commodity> source = context.CreateObjectQuery<Commodity>();
            source = UpdateSearchTextQuerySource<Commodity>(source, queryParams.SearchText);

            bool searchInMyProjects = (queryParams.ScopeFilters & SearchScopeFilter.MyProjects) == SearchScopeFilter.MyProjects;
            bool searchInReleasedProjects = (queryParams.ScopeFilters & SearchScopeFilter.ReleasedProjects) == SearchScopeFilter.ReleasedProjects;
            bool searchInOtherProjects = (queryParams.ScopeFilters & SearchScopeFilter.OtherUsersProjects) == SearchScopeFilter.OtherUsersProjects;
            bool searchInMasterData = (queryParams.ScopeFilters & SearchScopeFilter.MasterData) == SearchScopeFilter.MasterData;

            Collection<Guid> otherUsersEntitiesIds = new Collection<Guid>();
            if (searchInOtherProjects)
            {
                // Search in Other Users Projects using a stored procedure because Linq lacks support for recursive queries.
                var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
                otherUsersEntitiesIds = dsm.GetOtherUsersEntitiesId(SecurityManager.Instance.CurrentUser.Guid, "Commodities");
            }

            IQueryable<Commodity> query = (from commodity in source
                                               .Include(c => c.Manufacturer)
                                               .Include(c => c.Owner)
                                           where !commodity.IsDeleted &&
                                           (
                                             (searchInMyProjects && commodity.Owner.Guid == queryParams.CurrentUserGuid) ||
                                             (searchInReleasedProjects && commodity.Owner == null && commodity.IsMasterData == false) ||
                                             (searchInOtherProjects && otherUsersEntitiesIds.Contains(commodity.Guid)) ||
                                             (searchInMasterData && commodity.IsMasterData)
                                           )
                                           select commodity).OrderBy(p => p.Name);

            if (queryParams.GetDistinctResults)
            {
                query = query.GroupBy(x => x.Name, (key, group) => group.FirstOrDefault());
            }

            return query;
        }

        /// <summary>
        /// Gets the search text consumable query.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="queryParams">The query params.</param>
        /// <returns>A query for the Consumables set.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.SpacingRules", "SA1008:OpeningParenthesisMustBeSpacedCorrectly", Justification = "The parenthesis spacing is intentional, to achieve better linq query readability.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.SpacingRules", "SA1009:ClosingParenthesisMustBeSpacedCorrectly", Justification = "The parenthesis spacing is intentional, to achieve better linq query readability.")]
        private IQueryable<Consumable> GetSearchTextConsumableQuery(DataEntities context, SearchTextParams queryParams)
        {
            ObjectQuery<Consumable> source = context.CreateObjectQuery<Consumable>();
            source = UpdateSearchTextQuerySource<Consumable>(source, queryParams.SearchText);

            bool searchInMyProjects = (queryParams.ScopeFilters & SearchScopeFilter.MyProjects) == SearchScopeFilter.MyProjects;
            bool searchInReleasedProjects = (queryParams.ScopeFilters & SearchScopeFilter.ReleasedProjects) == SearchScopeFilter.ReleasedProjects;
            bool searchInOtherProjects = (queryParams.ScopeFilters & SearchScopeFilter.OtherUsersProjects) == SearchScopeFilter.OtherUsersProjects;
            bool searchInMasterData = (queryParams.ScopeFilters & SearchScopeFilter.MasterData) == SearchScopeFilter.MasterData;

            Collection<Guid> otherUsersEntitiesIds = new Collection<Guid>();
            if (searchInOtherProjects)
            {
                // Search in Other Users Projects using a stored procedure because Linq lacks support for recursive queries.
                var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
                otherUsersEntitiesIds = dsm.GetOtherUsersEntitiesId(SecurityManager.Instance.CurrentUser.Guid, "Consumables");
            }

            IQueryable<Consumable> query = (from consumable in source
                                                .Include(c => c.AmountUnitBase)
                                                .Include(c => c.PriceUnitBase)
                                                .Include(c => c.Manufacturer)
                                                .Include(c => c.Owner)
                                            where !consumable.IsDeleted &&
                                            (
                                                (searchInMyProjects && consumable.Owner.Guid == queryParams.CurrentUserGuid) ||
                                                (searchInReleasedProjects && consumable.Owner == null && consumable.IsMasterData == false) ||
                                                (searchInOtherProjects && otherUsersEntitiesIds.Contains(consumable.Guid)) ||
                                                (searchInMasterData && consumable.IsMasterData)
                                            )
                                            select consumable).OrderBy(p => p.Name);

            if (queryParams.GetDistinctResults)
            {
                query = query.GroupBy(x => x.Name, (key, group) => group.FirstOrDefault());
            }

            return query;
        }

        /// <summary>
        /// Gets the search text die query.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="queryParams">The query params.</param>
        /// <returns>A query for the Die set.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.SpacingRules", "SA1008:OpeningParenthesisMustBeSpacedCorrectly", Justification = "The parenthesis spacing is intentional, to achieve better linq query readability.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.SpacingRules", "SA1009:ClosingParenthesisMustBeSpacedCorrectly", Justification = "The parenthesis spacing is intentional, to achieve better linq query readability.")]
        private IQueryable<Die> GetSearchTextDieQuery(DataEntities context, SearchTextParams queryParams)
        {
            ObjectQuery<Die> source = context.CreateObjectQuery<Die>();
            source = UpdateSearchTextQuerySource<Die>(source, queryParams.SearchText);

            bool searchInMyProjects = (queryParams.ScopeFilters & SearchScopeFilter.MyProjects) == SearchScopeFilter.MyProjects;
            bool searchInReleasedProjects = (queryParams.ScopeFilters & SearchScopeFilter.ReleasedProjects) == SearchScopeFilter.ReleasedProjects;
            bool searchInOtherProjects = (queryParams.ScopeFilters & SearchScopeFilter.OtherUsersProjects) == SearchScopeFilter.OtherUsersProjects;
            bool searchInMasterData = (queryParams.ScopeFilters & SearchScopeFilter.MasterData) == SearchScopeFilter.MasterData;

            Collection<Guid> otherUsersEntitiesIds = new Collection<Guid>();
            if (searchInOtherProjects)
            {
                // Search in Other Users Projects using a stored procedure because Linq lacks support for recursive queries.
                var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
                otherUsersEntitiesIds = dsm.GetOtherUsersEntitiesId(SecurityManager.Instance.CurrentUser.Guid, "Dies");
            }

            IQueryable<Die> query = (from die in source
                                         .Include(d => d.Manufacturer)
                                         .Include(d => d.Owner)
                                     where !die.IsDeleted &&
                                     (
                                         (searchInMyProjects && die.Owner.Guid == queryParams.CurrentUserGuid) ||
                                         (searchInReleasedProjects && die.Owner == null && die.IsMasterData == false) ||
                                         (searchInOtherProjects && otherUsersEntitiesIds.Contains(die.Guid)) ||
                                         (searchInMasterData && die.IsMasterData)
                                     )
                                     select die).OrderBy(p => p.Name);

            if (queryParams.GetDistinctResults)
            {
                query = query.GroupBy(x => x.Name, (key, group) => group.FirstOrDefault());
            }

            return query;
        }

        /// <summary>
        /// Gets the search text machine query.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="queryParams">The query params.</param>
        /// <returns>A search text query for the Machine set.</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.SpacingRules", "SA1008:OpeningParenthesisMustBeSpacedCorrectly", Justification = "The parenthesis spacing is intentional, to achieve better linq query readability.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.SpacingRules", "SA1009:ClosingParenthesisMustBeSpacedCorrectly", Justification = "The parenthesis spacing is intentional, to achieve better linq query readability.")]
        private IQueryable<Machine> GetSearchTextMachineQuery(DataEntities context, SearchTextParams queryParams)
        {
            ObjectQuery<Machine> source = context.CreateObjectQuery<Machine>();
            source = UpdateSearchTextQuerySource<Machine>(source, queryParams.SearchText);

            bool searchInMyProjects = (queryParams.ScopeFilters & SearchScopeFilter.MyProjects) == SearchScopeFilter.MyProjects;
            bool searchInReleasedProjects = (queryParams.ScopeFilters & SearchScopeFilter.ReleasedProjects) == SearchScopeFilter.ReleasedProjects;
            bool searchInOtherProjects = (queryParams.ScopeFilters & SearchScopeFilter.OtherUsersProjects) == SearchScopeFilter.OtherUsersProjects;
            bool searchInMasterData = (queryParams.ScopeFilters & SearchScopeFilter.MasterData) == SearchScopeFilter.MasterData;

            Collection<Guid> otherUsersEntitiesIds = new Collection<Guid>();
            if (searchInOtherProjects)
            {
                // Search in Other Users Projects using a stored procedure because Linq lacks support for recursive queries.
                var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
                otherUsersEntitiesIds = dsm.GetOtherUsersEntitiesId(SecurityManager.Instance.CurrentUser.Guid, "Machines");
            }

            IQueryable<Machine> query = (from machine in source
                                             .Include(m => m.Manufacturer)
                                             .Include(m => m.Owner)
                                         where !machine.IsDeleted &&
                                         (
                                             (searchInMyProjects && machine.Owner.Guid == queryParams.CurrentUserGuid) ||
                                             (searchInReleasedProjects && machine.Owner == null && machine.IsMasterData == false) ||
                                             (searchInOtherProjects && otherUsersEntitiesIds.Contains(machine.Guid)) ||
                                             (searchInMasterData && machine.IsMasterData)
                                         )
                                         select machine).OrderBy(p => p.Name);

            if (queryParams.GetDistinctResults)
            {
                query = query.GroupBy(x => x.Name, (key, group) => group.FirstOrDefault());
            }

            return query;
        }

        /// <summary>
        /// Gets the search text part query.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="queryParams">The query params.</param>
        /// <returns>A query for searching text inside the Parts set</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.SpacingRules", "SA1008:OpeningParenthesisMustBeSpacedCorrectly", Justification = "The parenthesis spacing is intentional, to achieve better linq query readability.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.SpacingRules", "SA1009:ClosingParenthesisMustBeSpacedCorrectly", Justification = "The parenthesis spacing is intentional, to achieve better linq query readability.")]
        private IQueryable<Part> GetSearchTextPartQuery(DataEntities context, SearchTextParams queryParams)
        {
            ObjectQuery<Part> source = context.CreateObjectQuery<Part>();
            source = UpdateSearchTextQuerySource<Part>(source, queryParams.SearchText);

            bool searchInMyProjects = (queryParams.ScopeFilters & SearchScopeFilter.MyProjects) == SearchScopeFilter.MyProjects;
            bool searchInReleasedProjects = (queryParams.ScopeFilters & SearchScopeFilter.ReleasedProjects) == SearchScopeFilter.ReleasedProjects;
            bool searchInOtherProjects = (queryParams.ScopeFilters & SearchScopeFilter.OtherUsersProjects) == SearchScopeFilter.OtherUsersProjects;
            bool searchInMasterData = (queryParams.ScopeFilters & SearchScopeFilter.MasterData) == SearchScopeFilter.MasterData;

            Collection<Guid> otherUsersEntitiesIds = new Collection<Guid>();
            if (searchInOtherProjects)
            {
                // Search in Other Users Projects using a stored procedure because Linq lacks support for recursive queries.
                var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
                otherUsersEntitiesIds = dsm.GetOtherUsersEntitiesId(SecurityManager.Instance.CurrentUser.Guid, "Parts");
            }

            IQueryable<Part> query = (from part in source
                                          .Include(p => p.Manufacturer)
                                          .Include(p => p.Owner)
                                      where !part.IsDeleted && !(part is RawPart) &&
                                      (
                                          (searchInMyProjects && part.Owner.Guid == queryParams.CurrentUserGuid) ||
                                          (searchInReleasedProjects && part.Owner == null && part.IsMasterData == false) ||
                                          (searchInOtherProjects && otherUsersEntitiesIds.Contains(part.Guid)) ||
                                          (searchInMasterData && part.IsMasterData)
                                      )
                                      select part).OrderBy(p => p.Name);

            if (queryParams.GetDistinctResults)
            {
                query = query.GroupBy(x => x.Name, (key, group) => group.FirstOrDefault());
            }

            return query;
        }

        /// <summary>
        /// Gets the search text RawMaterial query.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="queryParams">The query params.</param>
        /// <returns>A query for searching text inside the RawMaterial set</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.SpacingRules", "SA1008:OpeningParenthesisMustBeSpacedCorrectly", Justification = "The parenthesis spacing is intentional, to achieve better linq query readability.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.SpacingRules", "SA1009:ClosingParenthesisMustBeSpacedCorrectly", Justification = "The parenthesis spacing is intentional, to achieve better linq query readability.")]
        private IQueryable<ProcessStep> GetSearchTextProcessStepQuery(DataEntities context, SearchTextParams queryParams)
        {
            ObjectQuery<ProcessStep> source = context.CreateObjectQuery<ProcessStep>();
            source = UpdateSearchTextQuerySource<ProcessStep>(source, queryParams.SearchText);

            bool searchInMyProjects = (queryParams.ScopeFilters & SearchScopeFilter.MyProjects) == SearchScopeFilter.MyProjects;
            bool searchInReleasedProjects = (queryParams.ScopeFilters & SearchScopeFilter.ReleasedProjects) == SearchScopeFilter.ReleasedProjects;
            bool searchInOtherProjects = (queryParams.ScopeFilters & SearchScopeFilter.OtherUsersProjects) == SearchScopeFilter.OtherUsersProjects;
            bool searchInMasterData = (queryParams.ScopeFilters & SearchScopeFilter.MasterData) == SearchScopeFilter.MasterData;

            Collection<Guid> otherUsersEntitiesIds = new Collection<Guid>();
            if (searchInOtherProjects)
            {
                // Search in Other Users Projects using a stored procedure because Linq lacks support for recursive queries.
                var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
                otherUsersEntitiesIds = dsm.GetOtherUsersEntitiesId(SecurityManager.Instance.CurrentUser.Guid, "ProcessSteps");
            }

            IQueryable<ProcessStep> query = (from step in source
                                                 .Include(s => s.Owner)
                                                 .Include(s => s.Type)
                                                 .Include(s => s.SubType)
                                             where
                                             (
                                                 (searchInMyProjects && step.Owner.Guid == queryParams.CurrentUserGuid) ||
                                                 (searchInReleasedProjects && step.Owner == null && step.IsMasterData == false) ||
                                                 (searchInOtherProjects && otherUsersEntitiesIds.Contains(step.Guid)) ||
                                                 (searchInMasterData && step.IsMasterData)
                                             )
                                             select step).OrderBy(p => p.Name);

            if (queryParams.GetDistinctResults)
            {
                query = query.GroupBy(x => x.Name, (key, group) => group.FirstOrDefault());
            }

            return query;
        }

        /// <summary>
        /// Gets the search text project query.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="queryParams">The query params.</param>
        /// <returns>A query for searching text inside the Project set</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.SpacingRules", "SA1008:OpeningParenthesisMustBeSpacedCorrectly", Justification = "The parenthesis spacing is intentional, to achieve better linq query readability.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.SpacingRules", "SA1009:ClosingParenthesisMustBeSpacedCorrectly", Justification = "The parenthesis spacing is intentional, to achieve better linq query readability.")]
        private IQueryable<Project> GetSearchTextProjectQuery(DataEntities context, SearchTextParams queryParams)
        {
            ObjectQuery<Project> source = context.CreateObjectQuery<Project>();
            source = UpdateSearchTextQuerySource<Project>(source, queryParams.SearchText);

            bool searchInMyProjects = (queryParams.ScopeFilters & SearchScopeFilter.MyProjects) == SearchScopeFilter.MyProjects;
            bool searchInReleasedProjects = (queryParams.ScopeFilters & SearchScopeFilter.ReleasedProjects) == SearchScopeFilter.ReleasedProjects;
            bool searchInOtherProjects = (queryParams.ScopeFilters & SearchScopeFilter.OtherUsersProjects) == SearchScopeFilter.OtherUsersProjects;

            Collection<Guid> otherUsersEntitiesIds = new Collection<Guid>();
            if (searchInOtherProjects)
            {
                // Search in Other Users Projects using a stored procedure because Linq lacks support for recursive queries.
                var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
                otherUsersEntitiesIds = dsm.GetOtherUsersEntitiesId(SecurityManager.Instance.CurrentUser.Guid, "Projects");
            }

            IQueryable<Project> query = (from project in source
                                             .Include(p => p.Customer)
                                             .Include(p => p.Owner)
                                             .Include(p => p.ProjectFolder)
                                         where !project.IsDeleted &&
                                         (
                                             (searchInMyProjects && project.Owner.Guid == queryParams.CurrentUserGuid) ||
                                             (searchInReleasedProjects && project.IsReleased) ||
                                             (searchInOtherProjects && otherUsersEntitiesIds.Contains(project.Guid))
                                             /* || put master data search condition here, if projects will ever be part of master data*/
                                         )
                                         select project).OrderBy(p => p.Name);

            if (queryParams.GetDistinctResults)
            {
                query = query.GroupBy(x => x.Name, (key, group) => group.FirstOrDefault());
            }

            return query;
        }

        /// <summary>
        /// Gets the search text RawMaterial query.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="queryParams">The query params.</param>
        /// <returns>A query for searching text inside the RawMaterial set</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.SpacingRules", "SA1008:OpeningParenthesisMustBeSpacedCorrectly", Justification = "The parenthesis spacing is intentional, to achieve better linq query readability.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.SpacingRules", "SA1009:ClosingParenthesisMustBeSpacedCorrectly", Justification = "The parenthesis spacing is intentional, to achieve better linq query readability.")]
        private IQueryable<RawMaterial> GetSearchTextRawMaterialQuery(DataEntities context, SearchTextParams queryParams)
        {
            ObjectQuery<RawMaterial> source = context.CreateObjectQuery<RawMaterial>();
            source = UpdateSearchTextQuerySource<RawMaterial>(source, queryParams.SearchText);

            bool searchInMyProjects = (queryParams.ScopeFilters & SearchScopeFilter.MyProjects) == SearchScopeFilter.MyProjects;
            bool searchInReleasedProjects = (queryParams.ScopeFilters & SearchScopeFilter.ReleasedProjects) == SearchScopeFilter.ReleasedProjects;
            bool searchInOtherProjects = (queryParams.ScopeFilters & SearchScopeFilter.OtherUsersProjects) == SearchScopeFilter.OtherUsersProjects;
            bool searchInMasterData = (queryParams.ScopeFilters & SearchScopeFilter.MasterData) == SearchScopeFilter.MasterData;

            Collection<Guid> otherUsersEntitiesIds = new Collection<Guid>();
            if (searchInOtherProjects)
            {
                // Search in Other Users Projects using a stored procedure because Linq lacks support for recursive queries.
                var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
                otherUsersEntitiesIds = dsm.GetOtherUsersEntitiesId(SecurityManager.Instance.CurrentUser.Guid, "RawMaterials");
            }

            IQueryable<RawMaterial> query = (from rawMaterial in source
                                                 .Include(m => m.Manufacturer)
                                                 .Include(m => m.Owner)
                                             where !rawMaterial.IsDeleted &&
                                             (
                                                 (searchInMyProjects && rawMaterial.Owner.Guid == queryParams.CurrentUserGuid) ||
                                                 (searchInReleasedProjects && rawMaterial.Owner == null && rawMaterial.IsMasterData == false) ||
                                                 (searchInOtherProjects && otherUsersEntitiesIds.Contains(rawMaterial.Guid)) ||
                                                 (searchInMasterData && rawMaterial.IsMasterData)
                                             )
                                             select rawMaterial).OrderBy(p => p.Name);

            if (queryParams.GetDistinctResults)
            {
                query = query.GroupBy(x => x.Name, (key, group) => group.FirstOrDefault());
            }

            return query;
        }

        /// <summary>
        /// Gets the search text raw part query.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="queryParams">The query params.</param>
        /// <returns>A query for searching text inside the RawPart set</returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.SpacingRules", "SA1008:OpeningParenthesisMustBeSpacedCorrectly", Justification = "The parenthesis spacing is intentional, to achieve better linq query readability.")]
        [SuppressMessage("Microsoft.StyleCop.CSharp.SpacingRules", "SA1009:ClosingParenthesisMustBeSpacedCorrectly", Justification = "The parenthesis spacing is intentional, to achieve better linq query readability.")]
        private IQueryable<Part> GetSearchTextRawPartQuery(DataEntities context, SearchTextParams queryParams)
        {
            ObjectQuery<Part> source = context.CreateObjectQuery<Part>();
            source = UpdateSearchTextQuerySource<Part>(source, queryParams.SearchText);

            bool searchInMyProjects = (queryParams.ScopeFilters & SearchScopeFilter.MyProjects) == SearchScopeFilter.MyProjects;
            bool searchInReleasedProjects = (queryParams.ScopeFilters & SearchScopeFilter.ReleasedProjects) == SearchScopeFilter.ReleasedProjects;
            bool searchInOtherProjects = (queryParams.ScopeFilters & SearchScopeFilter.OtherUsersProjects) == SearchScopeFilter.OtherUsersProjects;
            bool searchInMasterData = (queryParams.ScopeFilters & SearchScopeFilter.MasterData) == SearchScopeFilter.MasterData;

            Collection<Guid> otherUsersEntitiesIds = new Collection<Guid>();
            if (searchInOtherProjects)
            {
                // Search in Other Users Projects using a stored procedure because Linq lacks support for recursive queries.
                var dsm = DataAccessFactory.CreateDataSourceManager(DbIdentifier.CentralDatabase);
                otherUsersEntitiesIds = dsm.GetOtherUsersEntitiesId(SecurityManager.Instance.CurrentUser.Guid, "Parts");
            }

            IQueryable<Part> query = (from rawPart in source
                                          .Include(rp => rp.Owner)
                                      join part in context.PartSet on rawPart.Guid equals part.RawPart.Guid into parentParts
                                      where rawPart is RawPart && !rawPart.IsDeleted &&
                                      (
                                         (searchInMyProjects && rawPart.Owner.Guid == queryParams.CurrentUserGuid) ||
                                         (searchInReleasedProjects && rawPart.Owner == null && rawPart.IsMasterData == false) ||
                                         (searchInOtherProjects && otherUsersEntitiesIds.Contains(rawPart.Guid)) ||
                                         (searchInMasterData && rawPart.IsMasterData)
                                      )
                                      select rawPart).OrderBy(p => p.Name);

            if (queryParams.GetDistinctResults)
            {
                query = query.GroupBy(x => x.Name, (key, group) => group.FirstOrDefault());
            }

            return (query as ObjectQuery<Part>).Include("Manufacturer");
        }

        #endregion

        #region Search Helper

        /// <summary>
        /// Updates the search text query source.
        /// </summary>
        /// <typeparam name="T">Type of searchable object</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="inputSearchText">The search text.</param>
        /// <returns>The source updated with the "where" clause</returns>
        private ObjectQuery<T> UpdateSearchTextQuerySource<T>(ObjectQuery<T> source, string inputSearchText)
        {
            if (!string.IsNullOrEmpty(inputSearchText))
            {
                // Splits the words inputted by the user in separate search texts.
                List<string> searchTexts = SplitSearchText<T>(inputSearchText);
                List<string> searchTexts2 = SplitSearchText<T>(inputSearchText);

                string whereClause = BuildWherePropertyLikeSQLStatement(typeof(T), searchTexts);

                // add the Where clause to the source QueryObject
                source = source.Where(whereClause);

                List<string> wildTexts = PrepareWildcardString(searchTexts, false);

                for (int index = 0; index < searchTexts.Count; index++)
                {
                    if (whereClause.Contains("pattern" + index))
                    {
                        source.Parameters.Add(new ObjectParameter("pattern" + index, wildTexts[index]));
                    }
                }

                wildTexts = PrepareWildcardString(searchTexts2, true);

                for (int index = 0; index < searchTexts.Count; index++)
                {
                    if (whereClause.Contains("patternScrambled" + index))
                    {
                        source.Parameters.Add(new ObjectParameter("patternScrambled" + index, wildTexts[index]));
                    }
                }
            }

            return source;
        }

        /// <summary>
        /// Splits the search text inputted by the user.
        /// </summary>
        /// <remarks>
        /// The Split is done by whitespaces and by word groups delimited by quotes.
        /// Input "Word 1 Word 2" Machine1 100 kn
        /// will be split in : "Word 1 Word 2","Machine1" "100 kn".
        /// </remarks>
        /// <typeparam name="T">The type of the object</typeparam>
        /// <param name="searchText">The search text.</param>
        /// <returns>
        /// A list of strings representing the words you are searching for.
        /// </returns>
        private List<string> SplitSearchText<T>(string searchText)
        {
            // Work on the copy of the search text.
            string sourceSplitText = searchText;
            List<string> searchTexts = new List<string>();

            // Match string delimited by "" (Quotes), multiple groups can exist: "match1" "match 2".
            // and match to the collection and then remove the text from the sourceSplitText.
            string regexQuotesPattern = "\".+?\"";

            MatchCollection matchQuotesCollection = Regex.Matches(sourceSplitText, regexQuotesPattern);
            foreach (Match match in matchQuotesCollection)
            {
                string matchQuotesString = match.Value;

                // Remove ' " ' from the start and from the end
                matchQuotesString = matchQuotesString.Substring(1, matchQuotesString.Count() - 2);

                searchTexts.Add(matchQuotesString);
                sourceSplitText = sourceSplitText.Replace(match.Value, string.Empty);
            }

            // Match Machine Special Strings that include "kn", "mm", "kg/l", "rpm", "m/min", "1/min"
            if (typeof(T) == typeof(Machine))
            {
                string[] specialStringsSearchMachine = { "kn", "mm", "kg/l", "rpm", "m/min", "1/min" };

                // "\s" = Matches any whitespace character (spaces, tabs, line breaks) 
                // "+" = Matches 1 or more of the preceding token. This is a greedy match, and will match as many characters as possible before satisfying the next token.
                // "|" = Alternation. Equivalent of "or". Matches the full expression before or after the |.
                var regexMatchPattern = string.Empty;

                for (int index = 0; index < specialStringsSearchMachine.Count(); index++)
                {
                    // "\w+" = Matches words
                    // "\s*" = matches 0 or multiple whitespaces.
                    regexMatchPattern = regexMatchPattern + @"\w+\s*" + specialStringsSearchMachine[index];

                    if (index != specialStringsSearchMachine.Count() - 1)
                    {
                        regexMatchPattern = regexMatchPattern + "|";
                    }
                }

                MatchCollection matchCollection = Regex.Matches(sourceSplitText, regexMatchPattern);
                foreach (Match match in matchCollection)
                {
                    searchTexts.Add(match.Value);
                    sourceSplitText = sourceSplitText.Replace(match.Value, string.Empty);
                }
            }

            // Splits the remaining source search text delimited by spaces.
            searchTexts.AddRange(sourceSplitText.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).ToList());
            return searchTexts;
        }

        /// <summary>
        /// Modifies the string given as a search text to be used in a SQL statement
        /// The original text contains wildcards relevant for the UI (e.g. '*' '?')
        /// The output text will contain wildcards relevant for SQLServer (e.g. '%', '_')
        /// The result will also be wrapped in '%'
        /// </summary>
        /// <param name="userStrings">The user strings.</param>
        /// <param name="scrambleString">Specifies if the value should be scrambled or not</param>
        /// <returns>
        /// String prepared for use in a SQL statement
        /// </returns>
        private List<string> PrepareWildcardString(List<string> userStrings, bool scrambleString)
        {
            for (int index = 0; index < userStrings.Count; index++)
            {
                // convert "one" wildcards
                userStrings[index] = userStrings[index].Replace(DefaultUserWildcardMany, DefaultSQLWildcardMany);

                // convert "many" wildcards
                userStrings[index] = userStrings[index].Replace(DefaultUserWildcardOne, DefaultSQLWildcardOne);

                if (!userStrings[index].Contains(DefaultSQLWildcardMany) && !userStrings[index].Contains(DefaultSQLWildcardOne))
                {
                    // wrap text in "many" wildcards if it contains only text; if it contains any wildcard we consider it prepared.
                    // if the value is scrambled in the database, the search value is also scrambled in order to compare it with the db value
                    if (scrambleString)
                    {
                        userStrings[index] = string.Format("{0}{1}{2}", DefaultSQLWildcardMany, MasterDataScrambler.ScrambleText(userStrings[index]), DefaultSQLWildcardMany);
                    }
                    else
                    {
                        userStrings[index] = string.Format("{0}{1}{2}", DefaultSQLWildcardMany, userStrings[index], DefaultSQLWildcardMany);
                    }
                }
            }

            return userStrings;
        }

        /// <summary>
        /// Create the Where clause to be used in the generic search algorithm
        /// </summary>
        /// <param name="type">The type of the entity.</param>
        /// <param name="searchTexts">The search text.</param>
        /// <returns>
        /// A string similar to:
        /// "it.Name LIKE @pattern || it.Description LIKE @pattern || it.Machinename LIKE @pattern"
        /// </returns>
        private string BuildWherePropertyLikeSQLStatement(Type type, List<string> searchTexts)
        {
            var propertyInfos = type.GetProperties().ToList();

            // define the result and initialize with empty
            string result = string.Empty;
            for (int index = 0; index < searchTexts.Count; index++)
            {
                string whereClause = string.Empty;
                if (type == typeof(Machine))
                {
                    string regexMatchPattern = @"\w+\s*" + "kn";
                    MatchCollection matchQuotesCollection = Regex.Matches(searchTexts[index], regexMatchPattern);
                    foreach (Match match in matchQuotesCollection)
                    {
                        string matchQuotesString = match.Value;
                        searchTexts[index] = searchTexts[index].Replace(match.Value, string.Empty).Trim();
                        matchQuotesString = matchQuotesString.ToLower().Replace("kn", string.Empty);
                        matchQuotesString = matchQuotesString.Trim();

                        whereClause = whereClause + string.Format("(it.LockingForce = '{0}' || it.PressCapacity = '{0}')", matchQuotesString);
                        whereClause = whereClause + "&&";
                    }

                    regexMatchPattern = @"\w+\s*" + "mm";
                    matchQuotesCollection = Regex.Matches(searchTexts[index], regexMatchPattern);
                    foreach (Match match in matchQuotesCollection)
                    {
                        string matchQuotesString = match.Value;
                        searchTexts[index] = searchTexts[index].Replace(match.Value, string.Empty).Trim();
                        matchQuotesString = matchQuotesString.ToLower().Replace("mm", string.Empty);
                        matchQuotesString = matchQuotesString.Trim();

                        whereClause = whereClause + string.Format(
                            "(it.MountingCubeLength = '{0}' || it.MountingCubeWidth = '{0}' || it.MountingCubeHeight = '{0}' || it.MaxDiameterRodPerChuckMaxThickness = '{0}')",
                            matchQuotesString);
                        whereClause = whereClause + "&&";
                    }

                    regexMatchPattern = @"\w+\s*" + "kg/l";
                    matchQuotesCollection = Regex.Matches(searchTexts[index], regexMatchPattern);
                    foreach (Match match in matchQuotesCollection)
                    {
                        string matchQuotesString = match.Value;
                        searchTexts[index] = searchTexts[index].Replace(match.Value, string.Empty).Trim();
                        matchQuotesString = matchQuotesString.ToLower().Replace("kg/l", string.Empty);
                        matchQuotesString = matchQuotesString.Trim();
                        whereClause = whereClause + string.Format("(it.MaxPartsWeightPerCapacity = '{0}')", matchQuotesString);
                        whereClause = whereClause + "&&";
                    }

                    regexMatchPattern = @"\w+\s*" + "rpm";
                    matchQuotesCollection = Regex.Matches(searchTexts[index], regexMatchPattern);
                    foreach (Match match in matchQuotesCollection)
                    {
                        string matchQuotesString = match.Value;
                        searchTexts[index] = searchTexts[index].Replace(match.Value, string.Empty).Trim();
                        matchQuotesString = matchQuotesString.ToLower().Replace("rpm", string.Empty);
                        matchQuotesString = matchQuotesString.Trim();
                        whereClause = whereClause + string.Format("(it.MaximumSpeed = '{0}')", matchQuotesString);
                        whereClause = whereClause + "&&";
                    }

                    regexMatchPattern = @"\w+\s*" + "m/min";
                    matchQuotesCollection = Regex.Matches(searchTexts[index], regexMatchPattern);
                    foreach (Match match in matchQuotesCollection)
                    {
                        string matchQuotesString = match.Value;
                        searchTexts[index] = searchTexts[index].Replace(match.Value, string.Empty).Trim();
                        matchQuotesString = matchQuotesString.ToLower().Replace("m/min", string.Empty);
                        matchQuotesString = matchQuotesString.Trim();
                        whereClause = whereClause + string.Format("(it.RapidFeedPerCuttingSpeed = '{0}')", matchQuotesString);
                        whereClause = whereClause + "&&";
                    }

                    regexMatchPattern = @"\w+\s*" + "1/min";
                    matchQuotesCollection = Regex.Matches(searchTexts[index], regexMatchPattern);
                    foreach (Match match in matchQuotesCollection)
                    {
                        string matchQuotesString = match.Value;
                        searchTexts[index] = searchTexts[index].Replace(match.Value, string.Empty).Trim();
                        matchQuotesString = matchQuotesString.ToLower().Replace("1/min", string.Empty);
                        matchQuotesString = matchQuotesString.Trim();
                        whereClause = whereClause + string.Format("(it.StrokeRate = '{0}')", matchQuotesString);
                        whereClause = whereClause + "&&";
                    }

                    if (whereClause.Length > 2)
                    {
                        whereClause = whereClause.Remove(whereClause.Length - 2);
                        whereClause = "(" + whereClause + ")";
                    }
                }

                if (string.IsNullOrWhiteSpace(whereClause))
                {
                    result += CreateQueryForSearchTextForIndex(type, propertyInfos, index);
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(searchTexts[index]))
                    {
                        result += "(" + whereClause + ")";
                    }
                    else
                    {
                        result += "(" + whereClause + "&&" + CreateQueryForSearchTextForIndex(type, propertyInfos, index) + ")";
                    }
                }

                result += "||";
            }

            return result.Remove(result.Length - 2);
        }

        /// <summary>
        /// Creates the index of the query for search text from.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="propertyInfos">The property infos.</param>
        /// <param name="index">The index.</param>
        /// <returns>The query built.</returns>
        private string CreateQueryForSearchTextForIndex(Type type, List<System.Reflection.PropertyInfo> propertyInfos, int index)
        {
            string result = "(";

            // iterate the properties and build the result
            foreach (var info in propertyInfos)
            {
                // check that the current property has the correct type - string
                if (info.PropertyType.Name.Equals("String") && !ignoredProperties.Contains(info.Name))
                {
                    if (ignoredProperties.Contains(info.Name.Replace("Scrambled", string.Empty)))
                    {
                        result = string.Format("{0}it.{1} LIKE @patternScrambled{2} || ", result, info.Name, index);
                    }
                    else
                    {
                        result = string.Format("{0}it.{1} LIKE @pattern{2} || ", result, info.Name, index);
                    }
                }
                else if (info.Name.ToLower() == "manufacturer")
                {
                    result = string.Format("{0}it.{1}.Name LIKE @pattern{2} || ", result, info.Name, index);
                }
            }

            // Add custom search fields as is necessary for some types
            if (type == typeof(Machine))
            {
                result = string.Format("{0}it.MainClassification.Name LIKE @pattern{1} || ", result, index);
                result = string.Format("{0}it.SubClassification.Name LIKE @pattern{1} || ", result, index);
                result = string.Format("{0}it.TypeClassification.Name LIKE @pattern{1} || ", result, index);
                result = string.Format("{0}it.ClassificationLevel4.Name LIKE @pattern{1} || ", result, index);
            }
            else if (type == typeof(RawMaterial))
            {
                result = string.Format("{0}it.MaterialsClassificationL1.Name LIKE @pattern{1} || ", result, index);
                result = string.Format("{0}it.MaterialsClassificationL2.Name LIKE @pattern{1} || ", result, index);
                result = string.Format("{0}it.MaterialsClassificationL3.Name LIKE @pattern{1} || ", result, index);
                result = string.Format("{0}it.MaterialsClassificationL4.Name LIKE @pattern{1} || ", result, index);
            }
            else if (type == typeof(ProcessStep))
            {
                result = string.Format("{0}it.Type.Name LIKE @pattern{1} || ", result, index);
                result = string.Format("{0}it.SubType.Name LIKE @pattern{1} || ", result, index);
            }

            result = result.Remove(result.Length - 4);
            result += ")";

            return result;
        }

        #endregion
    }
}
