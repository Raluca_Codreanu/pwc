﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.Search
{
    /// <summary>
    /// Represents the classes (or categories) of data on which a search operation can be performed.    
    /// </summary>
    /// <remarks>
    /// This enumeration represents a set of flags, so its values must be powers of 2.
    /// </remarks>
    [Flags]
    public enum SearchScopeFilter
    {
        /// <summary>
        /// Search nowhere.
        /// </summary>
        None = 0,

        /// <summary>
        /// Search in the logged in user's projects.
        /// </summary>
        MyProjects = 1,

        /// <summary>
        /// Search in the released projects.
        /// </summary>
        ReleasedProjects = 2,

        /// <summary>
        /// Search in the projects belonging to other users which the logged in user is allowed to see.
        /// </summary>
        OtherUsersProjects = 4,

        /// <summary>
        /// Search in master data.
        /// </summary>
        MasterData = 8,
    }
}
