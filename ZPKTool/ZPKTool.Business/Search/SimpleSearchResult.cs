﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.Search
{
    /// <summary>
    /// Represents the result of the simple search, containing the data from the local and central database
    /// </summary>
    /// <typeparam name="T">The type of the entities in result</typeparam>
    public class SimpleSearchResult<T>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SimpleSearchResult{T}"/> class.
        /// </summary>
        public SimpleSearchResult()
        {
            LocalData = new List<T>();
            CentralData = new List<T>();
        }

        /// <summary>
        /// Gets the result from the local database
        /// </summary>
        public ICollection<T> LocalData { get; private set; }

        /// <summary>
        /// Gets the result from the central database
        /// </summary>
        public ICollection<T> CentralData { get; private set; }

        /// <summary>
        /// Clear the search results
        /// </summary>
        public void ClearResults()
        {
            this.LocalData.Clear();
            this.CentralData.Clear();
        }
    }
}
