﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.Search
{
    /// <summary>    
    /// Represents the filter options in the search process.
    /// </summary>
    /// <remarks>
    /// This enumeration represents a set of flags, so its values must be powers of 2.
    /// </remarks>
    [Flags]
    public enum SearchFilter
    {
        /// <summary>
        /// No item
        /// </summary>
        None = 0,

        /// <summary>
        /// The item represents a Project.
        /// </summary>
        Projects = 1,

        /// <summary>
        /// The item represents an Assembly.
        /// </summary>
        Assemblies = 2,

        /// <summary>
        /// The item represents a Part.
        /// </summary>
        Parts = 4,

        /// <summary>
        /// The item represents a Raw Material.
        /// </summary>
        RawMaterials = 8,

        /// <summary>
        /// The item represents a Commodity.
        /// </summary>
        Commodities = 16,

        /// <summary>
        /// The item represents a Machine
        /// </summary>
        Machines = 32,

        /// <summary>
        /// The item represents a Consumable
        /// </summary>
        Consumables = 64,

        /// <summary>
        /// The item represents a Die
        /// </summary>
        Dies = 128,

        /// <summary>
        /// The item represents a Process Step
        /// </summary>
        ProcessSteps = 256,

        /// <summary>
        /// The item represents the Raw Part
        /// </summary>
        RawParts = 512
    }    
}
