﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.Search
{
    /// <summary>
    /// Represents the parameters used for simple search  
    /// </summary>
    public class SimpleSearchParameters
    {
        /// <summary>
        /// Gets or sets the text to search for.
        /// </summary>
        public string SearchText { get; set; }

        /// <summary>
        /// Gets or sets the filters that indicate in what category of data to search.
        /// </summary>
        public SearchScopeFilter ScopeFilters { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the search results should be distinct
        /// The search result are distinct if they have distinct name
        /// </summary>
        public bool GetDistinctResults { get; set; }
    }
}
