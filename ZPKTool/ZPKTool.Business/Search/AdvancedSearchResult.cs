﻿using System;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.DataAccess;

namespace ZPKTool.Business.Search
{
    /// <summary>
    /// The advanced search result class that contains the entity and some additional data.
    /// </summary>
    public class AdvancedSearchResult
    {
        /// <summary>
        /// Gets or sets the entity.
        /// </summary>
        public object Entity { get; set; }

        /// <summary>
        /// Gets or sets the base currency.
        /// </summary>
        public object BaseCurrency { get; set; }

        /// <summary>
        /// Gets or sets the database id.
        /// </summary>
        public DbIdentifier DatabaseId { get; set; }
    }
}