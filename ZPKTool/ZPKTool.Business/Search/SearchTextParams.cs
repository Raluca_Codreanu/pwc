﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business.Search
{
    /// <summary>
    /// Define the parameters for searching text
    /// </summary>
    public class SearchTextParams
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SearchTextParams"/> class.
        /// </summary>
        public SearchTextParams()
        {
            this.SearchText = string.Empty;
            this.CurrentUserGuid = Guid.Empty;
            this.IsUser = false;
            this.IsAdmin = false;
            this.IsProjectLeader = false;
            this.ScopeFilters = SearchScopeFilter.None;
        }

        /// <summary>
        /// Gets or sets the search text.
        /// </summary>
        public string SearchText { get; set; }

        /// <summary>
        /// Gets or sets the current user's guid.
        /// </summary>
        public Guid CurrentUserGuid { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance has user role.
        /// </summary>
        public bool IsUser { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is admin.
        /// </summary>
        public bool IsAdmin { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user performing the search has the Project Leader role.
        /// </summary>
        public bool IsProjectLeader { get; set; }

        /// <summary>
        /// Gets or sets the filters that indicate in what category of data to search.
        /// </summary>
        public SearchScopeFilter ScopeFilters { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the search results should be distinct
        /// The search result are distinct if they have distinct name
        /// </summary>
        public bool GetDistinctResults { get; set; }
    }
}
