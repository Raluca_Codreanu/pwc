﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPKTool.Business
{
    /// <summary>
    /// Stores the information needed to create an error message for the yearly production quantity property.
    /// </summary>
    public class YearlyProductionQtyError
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="YearlyProductionQtyError"/> class.
        /// </summary>
        public YearlyProductionQtyError()
        {
        }

        /// <summary>
        /// Gets or sets the entity that has an error in its yearly production quantity property.
        /// </summary>
        public object Entity { get; set; }

        /// <summary>
        /// Gets or sets the name of the entity which has an error.
        /// </summary>
        public string EntityName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the entity's parent has the needed quantity value smaller then the entity's quantity or not.
        /// </summary>
        public bool IsValueSmaller { get; set; }
    }
}
