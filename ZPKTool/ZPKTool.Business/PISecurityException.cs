﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ZPKTool.Common;

namespace ZPKTool.Business
{
    /// <summary>
    /// Exception thrown by the security manager.
    /// </summary>
    [Serializable]
    public class PISecurityException : ZPKException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PISecurityException"/> class.
        /// </summary>
        public PISecurityException()
        { 
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PISecurityException"/> class.
        /// </summary>
        /// <param name="errorCode">The error code.</param>
        public PISecurityException(string errorCode)
            : base(errorCode)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PISecurityException"/> class.
        /// </summary>
        /// <param name="errorCode">The error code.</param>
        /// <param name="ex">The exception wrapped by this instance.</param>
        public PISecurityException(string errorCode, Exception ex)
            : base(string.Empty, ex)
        {
            ErrorCode = errorCode;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PISecurityException"/> class.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the exception being thrown.</param>
        /// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the source or destination.</param>
        protected PISecurityException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
