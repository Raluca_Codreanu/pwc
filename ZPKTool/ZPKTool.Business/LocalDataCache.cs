﻿using System;
using System.Collections.Generic;
using System.Linq;
using ZPKTool.Common;
using ZPKTool.Data;
using ZPKTool.DataAccess;

namespace ZPKTool.Business
{
    /// <summary>
    /// Caches some of the local database data that is very frequently accessed but changes rarely or can be changed only through the sync process.
    /// </summary>
    public static class LocalDataCache
    {
        // TODO: the initialization and access to the cache is not thread safe.

        /// <summary>
        /// The logging service.
        /// </summary>
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The cache of measurement units.
        /// </summary>
        private static List<MeasurementUnit> measurementUnitsCache;

        /// <summary>
        /// The cached instance of basic settings.
        /// </summary>
        private static BasicSetting basicSettingsCache;

        /// <summary>
        /// Initializes the cache of local settings from the database. If the cache is already initialized it does nothing.
        /// </summary>
        /// TODO: this is a temporary solution to performance problems that appeared because the cost calculator needs to read some settings from the local db.
        public static void Initialize()
        {
            InitializeMeasurementUnitsCache();
            InitializeBasicSettingsCache();
        }

        /// <summary>
        /// Initializes the cache of local settings with the specified values. If the cache is already initialized it does nothing.
        /// </summary>
        /// <remarks>
        /// This was necessary for Unit Testing the cost calculations.
        /// </remarks>
        /// <param name="newValue">The new basic settings value.</param>
        /// <param name="newUnits">The new measurement units.</param>
        internal static void Initialize(BasicSetting newValue, IEnumerable<MeasurementUnit> newUnits)
        {
            if (measurementUnitsCache == null)
            {
                measurementUnitsCache = newUnits.ToList();
            }

            if (basicSettingsCache == null)
            {
                basicSettingsCache = newValue;
            }

            // Pass the basic settings to the cost calculator factory (pass a copy).
            var settingsCopy = new BasicSetting();
            basicSettingsCache.CopyValuesTo(settingsCopy);
            Calculations.CostCalculation.CostCalculatorFactory.SetBasicSettings(settingsCopy);
        }

        /// <summary>
        /// Refreshes the cached local settings from the data source.
        /// </summary>
        public static void Refresh()
        {
            // Back up the settings before attempting the refresh.
            var measurementUnitsCacheBackup = measurementUnitsCache;
            var cachedBasicSettingsBackup = basicSettingsCache;

            try
            {
                // Reset and re-initialize the cache.
                measurementUnitsCache = null;
                basicSettingsCache = null;
                Initialize();
            }
            catch
            {
                // Restore the backed up cache because an error occurred during the re-initialization.
                measurementUnitsCache = measurementUnitsCacheBackup;
                basicSettingsCache = cachedBasicSettingsBackup;

                throw;
            }
        }

        /// <summary>
        /// Retrieves the basic settings from the cache of the local database.
        /// To be used when you just need to read the settings and the call can be made multiple times in short periods.
        /// </summary>
        /// <returns>The basic settings</returns>
        public static BasicSetting GetBasicSettings()
        {
            InitializeBasicSettingsCache();
            return basicSettingsCache;
        }

        /// <summary>
        /// Retrieves the cached list of measurement units from the database.
        /// </summary>
        /// <returns>The list of measurement units</returns>
        public static List<MeasurementUnit> GetMeasurementUnits()
        {
            InitializeMeasurementUnitsCache();
            return measurementUnitsCache != null ? measurementUnitsCache.ToList() : new List<MeasurementUnit>();
        }

        /// <summary>
        /// Initializes the measurement units cache.
        /// </summary>
        private static void InitializeMeasurementUnitsCache()
        {
            if (measurementUnitsCache == null)
            {
                try
                {
                    IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
                    measurementUnitsCache = dataManager.MeasurementUnitRepository.GetAll().ToList();
                }
                catch (DataAccessException exception)
                {
                    log.ErrorException("Could not get the measurement units for caching.", exception);
                    throw new BusinessException(exception);
                }
            }
        }

        /// <summary>
        /// Initializes the basic settings cache.
        /// </summary>
        private static void InitializeBasicSettingsCache()
        {
            if (basicSettingsCache == null)
            {
                try
                {
                    IDataSourceManager dataManager = DataAccessFactory.CreateDataSourceManager(DbIdentifier.LocalDatabase);
                    basicSettingsCache = dataManager.BasicSettingsRepository.GetBasicSettings(true);

                    // Pass the basic settings to the cost calculator factory (pass a copy).
                    var settingsCopy = new BasicSetting();
                    basicSettingsCache.CopyValuesTo(settingsCopy);
                    Calculations.CostCalculation.CostCalculatorFactory.SetBasicSettings(settingsCopy);
                }
                catch (DataAccessException exception)
                {
                    log.ErrorException("Could not get the basic settings for caching.", exception);
                    throw new BusinessException(exception);
                }
            }
        }
    }
}
